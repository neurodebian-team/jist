
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.plugins.PlugInGeneric;

import javax.swing.WindowConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInJISTProcessManager.
 */
public class PlugInJISTProcessManager implements PlugInGeneric{
	
	/** The Constant CATEGORY. */
	public static final String[] CATEGORY = {"JIST"};
	
	/**
	 * Instantiates a new plug in jist process manager.
	 */
	public PlugInJISTProcessManager(){
		super();

	}
	
	/**
	 * This method is inherited from MIPAV and should not be overridden.
	 */
	public final void run() {
		JistLogger.logOutput(JistLogger.INFO, "Starting JIST ProcessManager Plugin.");
//		ProcessManager.main(new String[]{});
		ProcessManager tool = ProcessManager.getInstance();		
//		PipelineLayoutTool tool=PipelineLayoutTool.getInstance();
		tool.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		PipeLibrary.getInstance().loadPreferences();
		JistPreferences.getPreferences().addListener(tool);
		tool.addWindowListener(tool);
		tool.historyChange(JistPreferences.getPreferences());
		tool.setVisible(true);
		tool.setCloseOnlyOnExit(true);
	}
}
