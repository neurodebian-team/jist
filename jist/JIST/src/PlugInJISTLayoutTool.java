
import edu.jhu.ece.iacl.jist.pipeline.gui.PipelineLayoutTool;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.plugins.PlugInGeneric;

import javax.swing.WindowConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInJISTLayoutTool.
 */
public class PlugInJISTLayoutTool implements PlugInGeneric{
	
	/** The Constant CATEGORY. */
	public static final String[] CATEGORY = {"JIST"};
	
	/**
	 * Instantiates a new plug in jist layout tool.
	 */
	public PlugInJISTLayoutTool(){
		super();

	}
	
	/**
	 * This method is inherited from MIPAV and should not be overridden.
	 */
	public final void run() {		
		JistLogger.logOutput(JistLogger.INFO, "Starting JIST LayoutTool Plugin.");
		PipelineLayoutTool tool=PipelineLayoutTool.getInstance();
		tool.setVisible(true);
		tool.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		tool.setCloseOnlyOnExit(true);
	}
}
