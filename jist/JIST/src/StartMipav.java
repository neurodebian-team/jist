import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import gov.nih.mipav.view.Preferences;
 
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/*
 * This is a wrapper class for running JIST in MIPAV.
 * If this is the first time a user has run the program on a particular machine it
 * 	first prompts the user to accept the licensing agreement,
 * 	second prompts the user to fill out the Mipav registration form,
 * Then calls the main method of MipavMain.
 */
public class StartMipav {

	//Initialized to the main methods String [] args, so that the arguments can be passed to MipavMain
	static String [] mainArgs;
	
	//Used to create a registry entry so that the license agreement and registration form need only be filled out once
//	static Preferences prefs;
	
	//These two strings are used by prefs
//	static String node = "mipav.preferences";
	static String hasRunBefore = "HAS_RUN_BEFORE";
	
	/*
	 * Called by the main method
	 * Checks the registry to see if the licensing agreement has been accepted already
	 * If yes, calls the run() method to start Mipav
	 * If no, calls Screen1() to open the licensing agreement window
	 */
	private static void hasRunBefore()
	{		
	    if(!Preferences.read())
	    	System.err.println("Cannot read preferences! - Asking registration questions...");
	    
	    String val=Preferences.getProperty("hasRunBefore");
	    System.out.println("HasRunBefore: "+val);System.out.flush();
	    if(val==null)
	    	val="false";
	    boolean goToMipav = Boolean.valueOf(val);//prefs.getBoolean(hasRunBefore, false);
	    if(goToMipav){
	    	run();
	    }
	    else{
	    	Screen1();
	    }
	}
	
	/*
	 * Displays the licensing agreement
	 * If agreed to, calls Screen2() to display the MIPAV registration window
	 * If not agreed to, tells the user that they must accept the licensing agreement
	 */
    private static void Screen1(){  
    	
        final JFrame f = new JFrame("Click-Wrap Agreement for the Transfer of Software");

		//p contains all components. 
        	//Will be added to a JScrollPane() later, which is then added to f
			//Creating a scrollable window
		final JPanel p = new JPanel();
		p.setLayout(new GridBagLayout());
        
        /*
         * SET UP FOR ALL FIELDS
         * Set up the license text box, get the license text, and make the box scrollable
         */
        JEditorPane EULAtext = new JEditorPane();
        EULAtext.setEditable(false);
        
        java.net.URL helpURL = StartMipav.class.getResource("license.html");
        if (helpURL != null) {
            try {
                EULAtext.setPage(helpURL);
            } catch (IOException e) {
                System.err.println("Attempted to read a bad URL: " + helpURL);
            }
        } else {
            System.err.println("Couldn't find file");
        }

        JScrollPane scrollPane = new JScrollPane(EULAtext);
        scrollPane.setVerticalScrollBarPolicy(
                        JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(450, 300));
        scrollPane.setMinimumSize(new Dimension(10, 10));

        /*
         * Set up two radio buttons for "I accept" and "I do not accept" options
         */
        final String agree = "agree";
        final String doNotAgree = "doNotAgree";
        
        JRadioButton acceptEULA = new JRadioButton("I accept the terms of the licensing agreement.");
        acceptEULA.setActionCommand(agree);
        
        JRadioButton doNotAcceptEULA = new JRadioButton("I do not accept the terms of the licensing agreement.");
        doNotAcceptEULA.setActionCommand(doNotAgree);
        doNotAcceptEULA.setSelected(true);
        
        /*
         * Set up a button group for the accept and reject buttons 
         * For use by the continue buttons ActionListener below
         */
        final ButtonGroup acceptOrReject = new ButtonGroup();
        acceptOrReject.add(acceptEULA);
        acceptOrReject.add(doNotAcceptEULA);
              
        /*
         * Set up the Continue button and manage its behavior 
         * Based on whether the 'accept' or 'do no accept' radio button is selected
         */
        JButton cont = new JButton("Continue");
        cont.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
                String command = acceptOrReject.getSelection().getActionCommand();
                if (command == doNotAgree) {
                    JOptionPane.showMessageDialog(f,
	            	    "You must accept the license agreement in order to use this software.",
	            	    "Warning",
	            	    JOptionPane.WARNING_MESSAGE);
                }
                if (command == agree)
                {
                	f.dispose();
                	Screen2();
                }
        	}
        });
        
        /*
         * Set up the Cancel button
         * Warns the user that they must accept the license agreement on close
         */
        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		JOptionPane.showMessageDialog(f,
	        	    "You must accept the license agreement in order to use this software.",
	        	    "Warning",
            	    JOptionPane.WARNING_MESSAGE);
        		f.dispose();
        	}
        });
        
        /*
         * Set up the GridBag to display all ui components
         */
        GridBagConstraints c = new GridBagConstraints();
        
        c.gridx = 1;
        c.gridy = 1;
        c.insets = new Insets(5,5,5,5);
        c.weightx = c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = GridBagConstraints.REMAINDER;
        p.add(scrollPane, c);

        //c.gridx = 1;
        c.gridy = 4;
        c.weightx = c.weighty = 0.0;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_START;
        p.add(acceptEULA, c);
        
        //c.gridx = 1;
        c.gridy = 5;
        p.add(doNotAcceptEULA,c);
        
        c.gridwidth = GridBagConstraints.RELATIVE;
        c.anchor = GridBagConstraints.LAST_LINE_END;
        c.gridx = 4;
        c.gridy = 7;
        p.add(cancel,c);
        
        c.gridx = 5;
        c.gridy = 7;
        c.gridwidth = GridBagConstraints.REMAINDER;
        p.add(cont, c);
        
        JScrollPane scrollFrame = new JScrollPane(p);
        f.add(scrollFrame);
 
        f.pack();
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.setVisible(true);
    }
    
    /*
     * Called by Screen2()
     * Checks that the input email is well-formed
     * i.e. something@something.something
     */
    public static boolean isValidEmailAddress(String email){
    	Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
    	Matcher m = p.matcher(email);
    	return m.matches();
    }

    /*
     * Displays the Mipav Registration window
     * If fields are filled out correctly, sends the form to the database
     * If no name or a badly formed email are given, prompts the user to fix these issues
     */
    private static void Screen2()
    {
    	//Actual Window
    	final JFrame f = new JFrame("MIPAV Registration Survey");
    	
    	//p contains all components. 
    		//Will be added to a JScrollPane() later, which is then added to f
    		//Creating a scrollable window
    	final JPanel p = new JPanel();
    	
    	p.setLayout(new GridBagLayout());  
        
        /*
         * SET UP FOR ALL FIELDS
         */
        JLabel titleLabel = new JLabel("Mipav Registration Survey");
        titleLabel.setFont(titleLabel.getFont().deriveFont(20.0f));
        
        /*
         * Set up for name labels and field
         */
        JLabel nameLabel = new JLabel("Name (required)");
        JLabel nameDescription = new JLabel("What is your name?");
        nameDescription.setForeground(Color.GRAY);
        final JTextField nameField = new JTextField(20);
        
        /*
         * Set up for email labels and fields
         */
        JLabel emailLabel = new JLabel("Email (required)");
        JLabel emailDescription = new JLabel("A valid email is required.");
        emailDescription.setForeground(Color.GRAY);
        final JTextField emailField = new JTextField(20);
        
        /*
         * Set up for primary institution label and field
         */
        JLabel primaryInsitutionLabel = new JLabel("Institution of Primary Affiliation") ;
        final JTextField primaryInsitutionField = new JTextField(20);
        
        /*
         * Set up for type of institution radio button set
         */
        //Initialize ActionCommand strings
        final String gov = "US Government";
        final String uni = "University";
        final String nonprofit = "Non-Profit Organization";
        final String com = "Commercial";
        final String oth = "Other";
        
        //Label for the radio button set
        JLabel typeOfInstitution = new JLabel("Type of Institution");
        
        //Initialize all radio buttons and corresponding ActionCommands
        JRadioButton USGov = new JRadioButton("US Government");
        USGov.setActionCommand(gov);       
        JRadioButton university = new JRadioButton("University");
        university.setActionCommand(uni);       
        JRadioButton nonProfitOrg = new JRadioButton("Non-Profit Organization");
        nonProfitOrg.setActionCommand(nonprofit);       
        JRadioButton commercial = new JRadioButton("Commercial");
        commercial.setActionCommand(com);       
        JRadioButton other = new JRadioButton("Other");
        other.setActionCommand(oth);
        
        //Add all buttons to the button group 
        //For use by the submit button's ActionListener
        final ButtonGroup instTypeGroup = new ButtonGroup();
        instTypeGroup.add(USGov);
        instTypeGroup.add(university);
        instTypeGroup.add(nonProfitOrg);
        instTypeGroup.add(commercial);
        instTypeGroup.add(other);
        
        /*
         * Interest in MIPAV label and Text Area
         */
        JLabel interestInMipavLabel = new JLabel("Interest in MIPAV");
        final JTextArea interestInMipavText = new JTextArea(10,38);
        JScrollPane mipavScrollPane = new JScrollPane(interestInMipavText);
        
        /*
         * Interest in JIST label and Text Area
         */
        JLabel interestInJistLabel = new JLabel("Interst in JIST");
        final JTextArea interestInJistText = new JTextArea(10,38);
        JScrollPane jistScrollPane = new JScrollPane(interestInJistText);
        
        /*
         * Sources of funding label and Text Area
         */
        JLabel fundingLabel = new JLabel("Sources of Research Funding");
        final JTextArea fundingText = new JTextArea(10,38);
        JScrollPane fundingScrollPane = new JScrollPane(fundingText);
        
        /*
         * Initializes the submit button
         * Takes in and validates the data from the form
         * Dictates the Submit button's behavior based on this data
         */
        JButton submit = new JButton("Submit");
        submit.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		//Get data from all fields
        		boolean validName = false;
        		boolean validEmail = false;
        		String name, email, institutionName, institutionType, 
    			mipavIterest, jistInterest, fundingSource;
        		
        		institutionType = "";
        		
    			name = nameField.getText();    			
    			if(name.length() > 0){
    				validName = true;
    			}

    			email = emailField.getText();
    			if(email.length() > 0  && isValidEmailAddress(email)){
    				validEmail = true;
    			}
        		
    			institutionName = primaryInsitutionField.getText();
        		
        		try{
        			institutionType = instTypeGroup.getSelection().getActionCommand();
        		}
        		catch (NullPointerException e1){}
        		
        		mipavIterest = interestInMipavText.getText();       		
        		jistInterest = interestInJistText.getText();        		
        		fundingSource = fundingText.getText();
        		
        		//If validName and validEmail, send the data to the database and start Mipav
        		if(validName && validEmail)
        		{        			
        			Preferences.setProperty("hasRunBefore", "true");
        			//SEND DATA TO URL
        			try { 
        				// Construct data
        				
        				String data = URLEncoder.encode("entry.0.single", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8"); 
        				data += "&" + URLEncoder.encode("entry.1.single", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8"); 
        				data += "&" + URLEncoder.encode("entry.2.single", "UTF-8") + "=" + URLEncoder.encode(institutionName, "UTF-8");
        				data += "&" + URLEncoder.encode("entry.3.group", "UTF-8") + "=" + URLEncoder.encode(institutionType, "UTF-8");
        				data += "&" + URLEncoder.encode("entry.5.single", "UTF-8") + "=" + URLEncoder.encode(mipavIterest, "UTF-8");
        				data += "&" + URLEncoder.encode("entry.6.single", "UTF-8") + "=" + URLEncoder.encode(jistInterest, "UTF-8");
        				data += "&" + URLEncoder.encode("entry.7.single", "UTF-8") + "=" + URLEncoder.encode(fundingSource, "UTF-8");
        				
        				// Send data 
        				URL url = new URL("http://spreadsheets.google.com/formResponse?formkey=dEd6cDM5NVhHY3lIa0NORDBVekctc2c6MQ&amp;ifq"); 
        				URLConnection conn = url.openConnection(); 
        				conn.setDoOutput(true); 
        				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
        				wr.write(data); wr.flush(); 
        				
        				// Get the response 
        				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
        				String line; 
        				while ((line = rd.readLine()) != null) { 
        					// Process line...
        					} 
        				wr.close(); rd.close(); 
        				} 
        			catch (Exception e1) { } 
        			//END SEND DATA
        			
        			f.dispose();
            		run();
        		}
        		
        		//If invalid name or invalid email, spawn a dialog box telling the user to fix these issues
        		else
        		{
        			if(!validName && !validEmail)
        			{
        				JOptionPane.showMessageDialog(f,
        	            	    "You must input a valid name and email address.",
        	            	    "Warning",
        	            	    JOptionPane.WARNING_MESSAGE);
        			}
        			else if(!validName)
        			{
        				JOptionPane.showMessageDialog(f,
        	            	    "Name field cannot be left blank.",
        	            	    "Warning",
        	            	    JOptionPane.WARNING_MESSAGE);
        			}
        			else if(!validEmail)
        			{
        				JOptionPane.showMessageDialog(f,
        	            	    "You must input a valid email address.",
        	            	    "Warning",
        	            	    JOptionPane.WARNING_MESSAGE);
        			}
        		}
        	}
        });
        
        /*
         * Set up the GridBag to display all ui components
         */
        GridBagConstraints c = new GridBagConstraints();
        int index = 1;
        int institutionIndex = 3;
        int vertPadding = 5;
        int horizPadding = 5;
        
        c.gridx = 1;
        c.gridy = index++;
        c.gridwidth = 3;
        c.insets = new Insets(vertPadding,horizPadding,vertPadding,horizPadding);
        c.anchor = GridBagConstraints.NORTH;
        p.add(titleLabel,c);
        
        c.gridx = 1;
        c.gridy = index++;
        c.gridwidth = 1;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        c.anchor = GridBagConstraints.LINE_START;
        p.add(nameLabel, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        p.add(nameDescription, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        p.add(nameField, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(vertPadding,horizPadding,0,horizPadding);
        p.add(emailLabel, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        p.add(emailDescription, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        p.add(emailField, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(vertPadding,horizPadding,0,horizPadding);
        p.add(primaryInsitutionLabel, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        p.add(primaryInsitutionField, c);
        
        c.gridx = 2;
        c.gridy = institutionIndex++;
        c.gridheight = 2;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        p.add(typeOfInstitution, c);
        
        //c.gridx = 2;
        c.gridy = institutionIndex++;
        p.add(USGov, c);
        
        //c.gridx = 2;
        c.gridy = institutionIndex++;
        p.add(university, c);
        
        //c.gridx = 2;
        c.gridy = institutionIndex++;
        p.add(nonProfitOrg, c);
        
        //c.gridx = 2;
        c.gridy = institutionIndex++;
        p.add(commercial, c);
        
        //c.gridx = 2;
        c.gridy = institutionIndex++;
        p.add(other, c);
        
        c.gridx = 1;
        c.gridy = index++;
        c.gridheight = 1;
        c.insets = new Insets(vertPadding,horizPadding,0,horizPadding);
        p.add(interestInMipavLabel, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.gridwidth = 3;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        p.add(mipavScrollPane, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(vertPadding,horizPadding,0,horizPadding);
        p.add(interestInJistLabel, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        p.add(jistScrollPane, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(vertPadding,horizPadding,0,horizPadding);
        p.add(fundingLabel, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(0,horizPadding,0,horizPadding);
        p.add(fundingScrollPane, c);
        
        //c.gridx = 1;
        c.gridy = index++;
        c.insets = new Insets(vertPadding,horizPadding,vertPadding,horizPadding);
        p.add(submit, c);
    	
        JScrollPane scrollFrame = new JScrollPane(p);
        f.add(scrollFrame); 
        
    	f.pack();
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.setVisible(true);
    }
    
    /*
     * Called by createAndShowGUI if 
     *	the "I accept" radio button is selected AND the continue button is clicked
     * Note that the licensing window is disposed of by createAndShowGUI
     * Calls the run() method that starts Mipav
     */
    private static void run()   
	{
		MipavMain.main(mainArgs);
	}

	public static void main(String[] args)
	{
		mainArgs = args;
		
		 //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                hasRunBefore();
            }
        });
	}
}
