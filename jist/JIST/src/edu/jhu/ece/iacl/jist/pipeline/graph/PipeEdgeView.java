/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.BasicStroke;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Point2D;

import org.jgraph.JGraph;
import org.jgraph.graph.CellView;
import org.jgraph.graph.CellViewRenderer;
import org.jgraph.graph.EdgeRenderer;
import org.jgraph.graph.EdgeView;
import org.jgraph.graph.GraphCellEditor;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.PortView;

/**
 * Cell view to render edge.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeEdgeView extends EdgeView {
	
	/**
	 * The Class PipeEdgeRenderer.
	 */
	protected class PipeEdgeRenderer extends EdgeRenderer {
		
		/**
		 * Instantiates a new pipe edge renderer.
		 */
		public PipeEdgeRenderer() {
			super();
		}

		/**
		 * Paint the renderer.
		 * 
		 * @param g
		 *            the g
		 */
		public void paint(Graphics g) {
			if (view.isLeaf()) {
				Shape edgeShape = view.getShape();
				// Sideeffect: beginShape, lineShape, endShape
				if (edgeShape != null) {
					Graphics2D g2 = (Graphics2D) g;
					g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
					int c = BasicStroke.CAP_BUTT;
					int j = BasicStroke.JOIN_MITER;
					setOpaque(false);
					// Cannot paint supper JComponent, what implications will
					// this have?
					// super.paint(g);
					translateGraphics(g);
					g.setColor(getForeground());
					if (lineWidth > 0) {
						g2.setStroke(new BasicStroke(lineWidth, c, j));
						if ((gradientColor != null) && !preview) {
							g2.setPaint(new GradientPaint(0, 0, getBackground(), getWidth(), getHeight(),
									gradientColor, true));
						}
						if (view.beginShape != null) {
							if (beginFill) {
								g2.fill(view.beginShape);
							}
							g2.draw(view.beginShape);
						}
						if (view.endShape != null) {
							if (endFill) {
								g2.fill(view.endShape);
							}
							g2.draw(view.endShape);
						}
						if (lineDash != null) {
							g2.setStroke(new BasicStroke(lineWidth, c, j, 10.0f, lineDash, dashOffset));
						}
						if (view.lineShape != null) {
							g2.draw(view.lineShape);
						}
					}
					if (selected) { // Paint Selected
						g2.setStroke(GraphConstants.SELECTION_STROKE);
						g2.setColor(highlightColor);
						if (view.beginShape != null) {
							g2.draw(view.beginShape);
						}
						if (view.lineShape != null) {
							g2.draw(view.lineShape);
						}
						if (view.endShape != null) {
							g2.draw(view.endShape);
						}
					}
					g2.setStroke(new BasicStroke(1));
					g.setFont((extraLabelFont != null) ? extraLabelFont : getFont());
					Object[] labels = GraphConstants.getExtraLabels(view.getAllAttributes());
					JGraph graph = (JGraph) this.graph.get();
					if (labels != null) {
						for (int i = 0; i < labels.length; i++) {
							Point2D p = getExtraLabelPosition(view, i);
							paintLabel(g, graph.convertValueToString(labels[i]), p, false || !simpleExtraLabels);
						}
					}
					if (graph.getEditingCell() != view.getCell()) {
						g.setFont(getFont());
						Object label = graph.convertValueToString(view);
						if (label != null) {
							Object obj = points.get(0);
							// Add extra catch to prevent rendering of label for
							// a collapsed group
							if (obj instanceof PortView) {
								if(view!=null){
									Point2D p = getLabelPosition(view);
									paintLabel(g, label.toString(), p, true);
								}
							}
						}
					}
				}
			} else {
				paintSelectionBorder(g);
			}
		}
	}

	/** The pipe renderer. */
	protected PipeEdgeRenderer pipeRenderer = new PipeEdgeRenderer();

	/**
	 * Instantiates a new pipe edge view.
	 * 
	 * @param obj
	 *            the obj
	 */
	public PipeEdgeView(Object obj) {
		super(obj);
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.AbstractCellView#getEditor()
	 */
	public GraphCellEditor getEditor() {
		return new PipeEdgeEditor();
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.EdgeView#getPointLocation(int)
	 */
	protected Point2D getPointLocation(int index) {
		if(points!=null){
			Object obj = points.get(index);
			if (obj instanceof Point2D) {
				return (Point2D) obj;
			} else if (obj instanceof PortView) {
				CellView vertex = ((CellView) obj).getParentView();
				if (vertex != null) {
					return getCenterPoint(vertex);
				}
			}
		}
		return null;
	}

	/**
	 * Returns a renderer for the class.
	 * 
	 * @return the renderer
	 */
	public CellViewRenderer getRenderer() {
		return pipeRenderer;
	}
}
