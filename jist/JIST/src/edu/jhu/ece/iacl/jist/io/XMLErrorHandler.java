package edu.jhu.ece.iacl.jist.io;


import gov.nih.mipav.model.file.FileOME;
import gov.nih.mipav.model.file.FileXML;
import gov.nih.mipav.view.Preferences;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


// TODO: Auto-generated Javadoc
/**
 * Error handler to report errors and warnings from the XML parser. Implements standard SAX ErrorHandler methods, see
 * SAX docs for more info.
 * 
 * @see  FileOME
 * @see  FileProject
 * @see  MipavFileVOI
 * @see  FileXML
 */
public class XMLErrorHandler implements ErrorHandler {

    //~ Methods --------------------------------------------------------------------------------------------------------

    /**
     * Handles parse exception errors by passing the parse exception up as a SAXException.
     * 
     * @param spe the spe
     * 
     * @throws  SAXException  passed up with the parse exception info
     * @throws SAXException the SAX exception
     */
    public void error(SAXParseException spe) throws SAXException {
        String message = "Error: " + getParseExceptionInfo(spe);

        throw new SAXException(message);
    }

    /**
     * Handles parse exception fatal errors by passing the parse exception up as a SAXException.
     * 
     * @param spe the spe
     * 
     * @throws  SAXException  passed up with the parse exception info
     * @throws SAXException the SAX exception
     */
    public void fatalError(SAXParseException spe) throws SAXException {
        String message = "Fatal Error: " + getParseExceptionInfo(spe);

        throw new SAXException(message);
    }

    /**
     * Handles parse exception warnings by outputting them to the debug window.
     * 
     * @param spe the spe
     * 
     * @throws  SAXException  not reported for warnings
     * @throws SAXException the SAX exception
     */
    public void warning(SAXParseException spe) throws SAXException {
    	//FIXME should not use direct access to MIPAV's dialogs
        Preferences.debug("Warning: " + getParseExceptionInfo(spe), Preferences.DEBUG_FILEIO);
    }

    /**
     * Returns a string describing parse exception details.
     * 
     * @param spe the spe
     * 
     * @return  a string containing information about the exception
     */
    private String getParseExceptionInfo(SAXParseException spe) {
        String systemId = spe.getSystemId();

        if (systemId == null) {
            systemId = "null";
        }

        String info = "URI=" + systemId + " Line=" + spe.getLineNumber() + ": " + spe.getMessage();

        return info;
    }
}
