package edu.jhu.ece.iacl.jist.structures.matrix;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.structures.vector.VectorN;
import edu.jhu.ece.iacl.jist.structures.vector.VectorX;

/**
 * Vector Matrix 3d implementation
 * Vectors use Vector 3d for storage.
 * 
 * @author Blake Lucas
 */
public class VectorMatrixX implements MatrixX{
	
	/** Vector Matrix 3d access This is not good encapsulation and will be deprectated in future iterations! Please use getters and setters when accessing matrix3d. */
	protected Number mat[][][][];
	
	/** The components. */
	private int rows,cols,slices,components;

	/**
	 * Instantiates a new vector matrix x.
	 * 
	 * @param d1 the d1
	 * @param d2 the d2
	 * @param d3 the d3
	 * @param a the a
	 */
	public VectorMatrixX(int d1,int d2,int d3,VectorX a){
		mat=new Number[d1][d2][d3][a.size()];
		rows=d1;
		cols=d2;
		slices=d3;		
		if(a!=null){
			components=a.size();
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					for(int k=0;k<slices;k++){
						mat[i][j][k]=a.toArray().clone();
					}
				}
			}
		}
	}
	
	/**
	 * Instantiates a new vector matrix x.
	 * 
	 * @param d1 the d1
	 * @param d2 the d2
	 * @param d3 the d3
	 * @param d4 the d4
	 */
	public VectorMatrixX(int d1,int d2,int d3,int d4){
		mat=new Number[d1][d2][d3][d4];
		rows=d1;
		cols=d2;
		slices=d3;		
		components=d4;
	}
	/*
	public VectorMatrixX(ImageData vol){
		rows=vol.getRows();
		slices=vol.getSlices();
		cols=vol.getCols();
		components=vol.getComponents();
		mat=new Number[rows][cols][slices][components];
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					mat[i][j][k]=vol.getVector(i,j,k).toArray();
				}
			}
		}
	}*//*
	public CubicVolume createVolume(){
		CubicVolume vol=new CubicVolumeMipav(VoxelType.DOUBLE,rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					vol.set(i,j,k,mat[i][j][k]);
				}
			}
		}
		return vol;
	}*/
	/*
	public VectorMatrixX(int d1,int d2,int d3){
		this(d1,d2,d3,null);
	}
	*/
	/**
	 * Instantiates a new vector matrix x.
	 * 
	 * @param m the m
	 */
	public VectorMatrixX(Number[][][][] m){
		mat=m;
		rows=mat.length;
		cols=mat[0].length;		slices=mat[0][0].length;
		components=mat[0][0][0].length;
	}
	
	/**
	 * Instantiates a new vector matrix x.
	 * 
	 * @param m the m
	 */
	public VectorMatrixX(VectorX[][][] m){
		rows=m.length;
		cols=m[0].length;
		slices=m[0][0].length;
		components=m[0][0][0].size();
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					mat[i][j][k]=m[i][j][k].toArray();
				}
			}
		}
	}
	
	/**
	 * Gets the rows.
	 * 
	 * @return the rows
	 */
	public int getRows(){
		return rows;
	}
	
	/**
	 * Gets the cols.
	 * 
	 * @return the cols
	 */
	public int getCols(){
		return cols;
	}
	
	/**
	 * Gets the slices.
	 * 
	 * @return the slices
	 */
	public int getSlices(){
		return slices;
	}
	
	/**
	 * To array4d.
	 * 
	 * @return the number[][][][]
	 */
	public Number[][][][] toArray4d(){
		return mat;
	}
	
	/**
	 * Sets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * @param a the a
	 */
	public void set(int i,int j,int k,VectorX a){
		mat[i][j][k]=a.toArray().clone();
	}
	
	/**
	 * Sets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * @param l the l
	 * @param val the val
	 */
	public void set(int i,int j,int k,int l,Number val){
		mat[i][j][k][l]=val;
	}
	
	/**
	 * Gets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * @param l the l
	 * 
	 * @return the number
	 */
	public Number get(int i,int j,int k,int l){
		return mat[i][j][k][l];
	}
	
	/**
	 * Gets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * 
	 * @return the vector x
	 */
	public VectorX get(int i,int j,int k){
		return new VectorN(mat[i][j][k]);
	}
	
	/**
	 * Adds the.
	 * 
	 * @param a the a
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX add(double a) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).add(a));
				}
			}
		}
		return M;
	}
	
	/**
	 * Sub.
	 * 
	 * @param a the a
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX sub(double a) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).sub(a));
				}
			}
		}
		return M;
	}
	
	/**
	 * Mul.
	 * 
	 * @param a the a
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX mul(double a) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).mul(a));
				}
			}
		}
		return M;
	}
	
	/**
	 * Div.
	 * 
	 * @param a the a
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX div(double a) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).div(a));
				}
			}
		}
		return M;
	}
	
	/**
	 * Adds the.
	 * 
	 * @param m the m
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX add(VectorMatrixX m) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).add(m.get(i,j,k)));
				}
			}
		}
		return M;
	}
	
	/**
	 * Sub.
	 * 
	 * @param m the m
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX sub(VectorMatrixX m) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).sub(m.get(i,j,k)));
				}
			}
		}
		return M;
	}
	
	/**
	 * Mul.
	 * 
	 * @param m the m
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX mul(VectorMatrixX m) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).mul(m.get(i,j,k)));
				}
			}
		}
		return M;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public VectorMatrixX clone(){
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k));
				}
			}
		}
		return M;	
	}
	
	/**
	 * Div.
	 * 
	 * @param m the m
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX div(VectorMatrixX m) {
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).div(m.get(i,j,k)));
				}
			}
		}
		return M;
	}
	
	/**
	 * Normalize.
	 * 
	 * @return the vector matrix x
	 */
	public VectorMatrixX normalize(){
		VectorMatrixX M=new VectorMatrixX(rows,cols,slices,components);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					VectorX v=get(i,j,k).clone();
					v=v.normalize();
					M.set(i,j,k,v);
				}
			}
		}
		return M;
	}
	
	/**
	 * Mag.
	 * 
	 * @return the image data
	 */
	public ImageData mag(){
		ImageData M=new ImageDataMipav("magnitude",VoxelType.FLOAT,rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k).mag().floatValue());
				}
			}
		}
		return M;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String text="{rows="+rows+" cols="+cols+" slices="+slices+"}\n";
		text+="{";
		for(int k=0;k<slices;k++){
			text+="[";
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					text+=get(i,j,k)+" ";
				}
				if(i!=cols-1)text+=";\n";
			}
			text+="]\n";
		}	
		text+="}";
		return text;
	}
	
	/**
	 * Debugging routine for printing non-zero vector matrix elements.
	 * 
	 * @return the double
	 *//*
	public void printNonZeroString(int max){
		System.out.println(getClass().getCanonicalName()+"\t"+"{rows="+rows+" cols="+cols+" slices="+slices+"}");
		int count=0;

		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					VectorX v=mat[i][j][k];
					if(v.getX().doubleValue()!=0||v.getY().doubleValue()!=0||v.getZ().doubleValue()!=0){

						System.out.println(getClass().getCanonicalName()+"\t"+"("+i+","+j+","+k+") "+get(i,j,k));
						count++;
						if(count>=max)return;
					}
				}
			}
		}	
	}*/
	public double talley(){
		double count=0;
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					for(int k=0;k<slices;k++){
						for(int l=0;l<components;l++){
							count+=get(i,j,k,l).doubleValue();
						}
					}
				}
			}
		return count;
	}
	
	/**
	 * Gets the components.
	 * 
	 * @return the components
	 */
	public int getComponents() {
		return components;
	}
	/*
	public VectorX[][][] toMatrix(){
		return mat;
	}*/
}
