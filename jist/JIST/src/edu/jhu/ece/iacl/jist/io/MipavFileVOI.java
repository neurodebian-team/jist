package edu.jhu.ece.iacl.jist.io;

import gov.nih.mipav.MipavCoordinateSystems;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.file.FileUtility;
import gov.nih.mipav.model.file.FileVOI;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.ModelStorageBase;
import gov.nih.mipav.model.structures.TransMatrix;
import gov.nih.mipav.model.structures.VOI;
import gov.nih.mipav.model.structures.VOIBase;
import gov.nih.mipav.view.Preferences;
import gov.nih.mipav.view.ViewImageFileFilter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import WildMagic.LibFoundation.Mathematics.Vector3f;

// TODO: Auto-generated Javadoc
/**
 * VOI reader/writer. This classes incorporates the ability to write/read VOIs in three formats (MIPAV *.voi, Nuages,
 * and MIPAV *.Xml . The first is a simple text format in which the points of the VOIs are stored in text format. The
 * VOIs can be also stored in a second format, also a text format, termed a Nauges. Nauges is a program that forms
 * triangles (i.e., tesselates) from contours. Therefore, if one wishes to render a surface of a VOI, first save the VOI
 * in Nauges format and supply the VOI file to the Nauges executable. MIPAV's lastest file format is based on XML and
 * should be used in the future.
 * 
 * <p>Note: A single VOI per file. However, a VOI can be formed from multiple contours.</p>
 * 
 * @version  0.1 Feb 24, 1998
 * @author   Matthew J. McAuliffe, Ph.D.
 * @see      VOI
 */
public class MipavFileVOI extends FileVOI {
	
	/** The file. */
	File file;
	
	/** The image. */
	ModelImage image;
	
	/** The file dir. */
	String fileName, fileDir;
	//~ Static fields/initializers -------------------------------------------------------------------------------------

	/** The tab character (makes writing out files cleaner-looking). */
	private static final String TAB = "\t";

	/** The W3C XML schema. */
	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

	/** The charset the XML file is written in. */
	private static final String XML_ENCODING = "UTF-8";

	/** The XML version description header. */
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\""
			+ XML_ENCODING + "\"?>";

	/** The MIPAV XML header comment. */
	private static final String VOI_HEADER = "<!-- MIPAV VOI file -->";

	/** The Constant TEXT_HEADER. */
	private static final String TEXT_HEADER = "<!-- MIPAV Annotation file -->";

	/**
	 * Instantiates a new mipav file voi.
	 * 
	 * @param fName the f name
	 * @param fDir the f dir
	 * @param img the img
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public MipavFileVOI(String fName, String fDir, ModelImage img)
			throws IOException {
		super(fName, fDir, img);
		this.file = new File(fDir, fName);
		this.image = img;
		this.fileName = fName;
		this.fileDir = fDir;
	}

	/* (non-Javadoc)
	 * @see gov.nih.mipav.model.file.FileVOI#writeXML(gov.nih.mipav.model.structures.VOI, boolean)
	 */
	public void writeXML(VOI voi, boolean saveAllContours) throws IOException {
		int i, j, k, m;
//		int length;
		int nPts;
		int nContours, nActiveContours;
		float[] x = new float[100];
		float[] y = new float[100];
		float[] z = new float[100];
		Vector<VOIBase> contours;

		FileWriter fw;
		//TODO: Make sure other local instances of bw do not cause NullPointerExceptions
		//BufferedWriter already exists in FileXML.   FileVOI extends FileXML as of version 2209
		//single copy is necessary for correct implementation of openTag(), closeTag(), etc
		//BufferedWriter bw;

		while (file.exists() == true) {
			int response = JOptionPane.showConfirmDialog(null, file.getName()
					+ " exists. Overwrite?", "File exists",
					JOptionPane.YES_NO_OPTION);

			if (response == JOptionPane.YES_OPTION) {
				file.delete();
				file = new File(fileDir + fileName);

				break;
			} else {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Save VOI as");
				chooser.setCurrentDirectory(file);

				chooser.addChoosableFileFilter(new ViewImageFileFilter(
						new String[] { "xml", "voi" }));

				int returnVal = chooser.showSaveDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					fileName = chooser.getSelectedFile().getName();
					fileDir = String.valueOf(chooser.getCurrentDirectory())
							+ File.separatorChar;
					file = new File(fileDir + fileName);
				} else {
					return;
				}
			}
		}

		try {

			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			contours = voi.getCurves();
//			length = voi.getCurves();

			bw.write(XML_HEADER);
			bw.newLine();
			bw.write(VOI_HEADER);
			bw.newLine();

			openTag("VOI xmlns:xsi=\"" + W3C_XML_SCHEMA + "-instance\"", true);
			closedTag("Unique-ID", Integer.toString(voi.getUID()));
			closedTag("Curve-type", Integer.toString(voi.getCurveType()));
			closedTag("Color", Integer.toString(voi.getColor().getAlpha())
					+ "," + Integer.toString(voi.getColor().getRed()) + ","
					+ Integer.toString(voi.getColor().getGreen()) + ","
					+ Integer.toString(voi.getColor().getBlue()));

			closedTag("Thickness", Integer.toString(voi.getThickness()));

			if ((voi.getCurveType() == VOI.CONTOUR)
					|| (voi.getCurveType() == VOI.POLYLINE)
					|| (voi.getCurveType() == VOI.POINT)) {

				// run through once and count how many TOTAL contours are there
				int totalContours = 0;

				long totalPoints = 0;
				int index;

//				for (i = 0; i < length; i++) {
					nContours = 1;//contours[i].size();
					totalContours += nContours;

					for (index = 0; index < nContours; index++) {
						totalPoints += ((VOIBase) contours.elementAt(index))
								.size();
					}

//				}

				// System.err.println(getClass().getCanonicalName()+"TOTAL CONTOURS of VOI " + voi.getName() + ": " + totalContours);
				// System.err.println(getClass().getCanonicalName()+"Number of points for all contours combined: " + totalPoints);

				Vector pointVector = new Vector();

				// add all contours to a vector for sorting

//				for (i = 0; i < contours.length; i++) {
					nContours = contours.size();

					for (k = 0; k < nContours; k++) {

						if ((saveAllContours || ((VOIBase) contours
								.elementAt(k)).isActive())) {
							pointVector.add(new VOISortItem(
									(VOIBase) contours.elementAt(k), Integer
											.parseInt(((VOIBase) contours
													.elementAt(k)).getLabel()),
									0));

						}
					}
//				}

				Collections.sort(pointVector, new VOIComparator());

				int pSize = pointVector.size();
				VOISortItem tempVOIItem = null;
				VOIBase tempBase = null;

				boolean saveAsLPS = Preferences
						.is(Preferences.PREF_VOI_LPS_SAVE);
				//System.err.println(getClass().getCanonicalName()+"SaveAsLPS: " + saveAsLPS);
				boolean is2D = (image.getNDims() == 2);

				for (i = 0; i < pSize; i++) {

					tempVOIItem = (VOISortItem) pointVector.elementAt(i);
					tempBase = tempVOIItem.getVOIBase();

					openTag("Contour", true);

					// save old format if image dim is 2
					if (is2D || !saveAsLPS) {
						closedTag("Slice-number", Integer.toString(tempVOIItem
								.getSlice()));
					}

					nPts = tempBase.size();

					if (nPts > x.length) {
						x = new float[nPts];
						y = new float[nPts];
						z = new float[nPts];
					}

					tempBase.exportArrays(x, y, z);

					if (!is2D && saveAsLPS) {
						Vector3f ptIn = new Vector3f();
						Vector3f ptOut = new Vector3f();
						int slice = tempVOIItem.getSlice();

						for (m = 0; m < nPts; m++) {
							ptIn.X = x[m];
							ptIn.Y = y[m];
							ptIn.Z = slice;
							//Do not round point coordinates!
							fileToScanner(ptIn, ptOut, image);

							//System.err.println(getClass().getCanonicalName()+"Pt in: " + ptIn + ", Pt out: "		+ ptOut);
							closedTag("Pt", Float.toString(ptOut.X) + ","
									+ Float.toString(ptOut.Y) + ","
									+ Float.toString(ptOut.Z));
						}
					} else {
						// image dim is 2 (or SaveAsLPS false).... save to old format

						for (m = 0; m < nPts; m++) {
							closedTag("Pt", Float.toString(x[m]) + ","
									+ Float.toString(y[m]));
						}

					}

					openTag("Contour", false);
				}

				pointVector.removeAllElements();
				pointVector = null;

			} else {

//				for (i = 0; i < length; i++) {
					nContours = contours.size();
					nActiveContours = 0;

					if (saveAllContours) {
						nActiveContours = nContours;
					} else {

						for (j = 0; j < nContours; j++) {

							if (((VOIBase) contours.elementAt(j)).isActive()) {
								nActiveContours++;
							}
						}
					}

					if (nActiveContours > 0) {

						for (j = 0; j < nContours; j++) {

							if (saveAllContours
									|| ((VOIBase) contours.elementAt(j))
											.isActive()) {
								openTag("Contour", true);

								// save old format if image dim is 2
								if (image.getNDims() == 2) {
									closedTag("Slice-number", Integer
											.toString(0));
								}

								nPts = ((Vector) (contours.elementAt(j)))
										.size();

								if (x.length < nPts) {
									x = new float[nPts];
									y = new float[nPts];
									z = new float[nPts];
								}

								((VOIBase) (contours.elementAt(j)))
										.exportArrays(x, y, z);

								if (image.getNDims() > 2) {
									Vector3f ptIn = new Vector3f();
									Vector3f ptOut = new Vector3f();
//									int slice = i;

									for (m = 0; m < nPts; m++) {
										ptIn.X = x[m];
										ptIn.Y = y[m];
										ptIn.Z = z[m];

										closedTag("Pt", Float.toString(ptOut.X)
												+ "," + Float.toString(ptOut.Y)
												+ "," + Float.toString(ptOut.Z));
									}
								} else {
									// image is 2d

									for (k = 0; k < nPts; k++) {
										closedTag("Pt", Float.toString(x[k])
												+ "," + Float.toString(y[k]));
									}
								}

								openTag("Contour", false);
							}
						}
					}
				}
//			}

			openTag("VOI", false);
			bw.close();
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"CAUGHT EXCEPTION WITHIN writeXML() of FileVOI");
			e.printStackTrace();
		}
	}

	/**
	 * DOCUMENT ME!.
	 */
	private class VOISortItem {

		/** DOCUMENT ME!. */
		private int index;

		/** DOCUMENT ME!. */
		private int slice;

		/** DOCUMENT ME!. */
		private VOIBase vBase;

		/**
		 * Creates a new VOISortItem object.
		 * 
		 * @param base the base
		 * @param idx the idx
		 * @param z the z
		 */
		public VOISortItem(VOIBase base, int idx, int z) {
			vBase = base;
			index = idx;
			slice = z;
		}

		/**
		 * DOCUMENT ME!.
		 * 
		 * @return  DOCUMENT ME!
		 */
		public int getIndex() {
			return index;
		}

		/**
		 * DOCUMENT ME!.
		 * 
		 * @return  DOCUMENT ME!
		 */
		public int getSlice() {
			return slice;
		}

		/**
		 * DOCUMENT ME!.
		 * 
		 * @return  DOCUMENT ME!
		 */
		public VOIBase getVOIBase() {
			return vBase;
		}

	}

	/**
	 * DOCUMENT ME!.
	 */
	private class VOIComparator implements Comparator {

		/**
		 * DOCUMENT ME!.
		 * 
		 * @param o1 the o1
		 * @param o2 the o2
		 * 
		 * @return  DOCUMENT ME!
		 */
		public int compare(Object o1, Object o2) {
			int a = ((VOISortItem) o1).getIndex();
			int b = ((VOISortItem) o2).getIndex();

			if (a < b) {
				return -1;
			} else if (a > b) {
				return 1;
			} else {
				return 0;
			}
		}

	}

	/**
	 * File to scanner.
	 * 
	 * @param kInput the k input
	 * @param kOutput the k output
	 * @param kImage the k image
	 */
	public static final void fileToScanner(Vector3f kInput, Vector3f kOutput,
			ModelImage kImage) {
		float[] afAxialOrigin = kImage.getOrigin(0, FileInfoBase.AXIAL);

		if ((kImage.getMatrixHolder()
				.containsType(TransMatrix.TRANSFORM_SCANNER_ANATOMICAL))
				|| (kImage.getFileInfo()[0].getFileFormat() == FileUtility.DICOM)) {

			float[] afResolutions = kImage.getResolutions(0);

			TransMatrix dicomMatrix = (TransMatrix) (((ModelImage) kImage)
					.getMatrix().clone());

			// System.err.println(getClass().getCanonicalName()+"DICOM MATRIX (fileToScanner): " + dicomMatrix);
			// System.err.println(getClass().getCanonicalName()+"axial origin: " + afAxialOrigin[0] + ", " + afAxialOrigin[1] + ", " + afAxialOrigin[2]);

			// Finally convert the point to axial millimeter DICOM space.
			dicomMatrix.transform(new double[]{kInput.X * afResolutions[0],kInput.Y * afResolutions[1], kInput.Z * afResolutions[2]},
					new double[]{kOutput.X,kOutput.Y,kOutput.Z});
		} else {
			//System.err.println(getClass().getCanonicalName()+"not dicom");
			float[] afAxialRes = kImage.getResolutions(0, FileInfoBase.AXIAL);

			fileToPatient(kInput, kOutput, kImage,
					FileInfoBase.AXIAL, false);

			kOutput.X *= afAxialRes[0];
			kOutput.Y *= afAxialRes[1];
			kOutput.Z *= afAxialRes[2];

			boolean[] axisFlip = MipavCoordinateSystems.getAxisFlip(kImage,
					FileInfoBase.AXIAL, true);

			if (axisFlip[0]) {
				kOutput.X = -kOutput.X;
			}

			if (axisFlip[1]) {
				kOutput.Y = -kOutput.Y;
			}

			if (axisFlip[2]) {
				kOutput.Z = -kOutput.Z;
			}
		}

		// Returned point represents the current position in coronal, sagittal, axial order (L/R, A/P, I/S axis space)
		kOutput.X += afAxialOrigin[0];
		kOutput.Y += afAxialOrigin[1];
		kOutput.Z += afAxialOrigin[2];

	}

	/**
	 * File to patient.
	 * 
	 * @param pIn the in
	 * @param pOut the out
	 * @param image the image
	 * @param orientation the orientation
	 * @param bFlip the b flip
	 */
	public static final void fileToPatient(Vector3f pIn, Vector3f pOut,
			ModelStorageBase image, int orientation, boolean bFlip) {

		// axisOrder represents the mapping of FileCoordinates volume axes to PatientCoordinates axes
		int[] axisOrder = MipavCoordinateSystems.getAxisOrder(image,
				orientation);

		// axisFlip represents whether to invert the axes after they are reordered
		boolean[] axisFlip = MipavCoordinateSystems.getAxisFlip(image,
				orientation);

		// extents gets the image extents re-mapped to PatientCoordinates
		int[] extents = image.getExtents(orientation);

		// The modelPoint
		float[] modelPoint = new float[3];
		modelPoint[0] = pIn.X;
		modelPoint[1] = pIn.Y;
		modelPoint[2] = pIn.Z;

		// transformed patientPoint
		float[] patientPoint = new float[3];

		// First reorder the point indices based on the axisOrder re-mapping from FileCoordinates to PatientCoordinates
		// space
		for (int i = 0; i < 3; i++) {
			patientPoint[i] = modelPoint[axisOrder[i]];
		}

		// Then invert the point, using the appropriate extents
		for (int i = 0; i < 3; i++) {

			if (axisFlip[i] && bFlip) {
				patientPoint[i] = extents[i] - patientPoint[i] - 1;
			}
		}

		// assign the transformed point to pOut
		pOut.X = patientPoint[0];
		pOut.Y =patientPoint[1];
		pOut.Z =patientPoint[2];
	}
}
