/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.event.MouseInputAdapter;

import org.jgraph.JGraph;
import org.jgraph.graph.CellView;
import org.jgraph.graph.DefaultGraphModel;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmGroupCell;

/**
 * Manages the folding and unfolding of groups.
 */
public class JGraphFoldingManager extends MouseInputAdapter {
	
	/**
	 * Called when the mouse button is released to see if a collapse or expand
	 * request has been made.
	 * 
	 * @param graph
	 *            the graph
	 * @param pt
	 *            the pt
	 * @return the group by folding handle
	 */
	public static CellView getGroupByFoldingHandle(JGraph graph, Point2D pt) {
		CellView[] views = graph.getGraphLayoutCache().getCellViews();
		for (CellView element : views) {
			Point2D containerPoint = graph.fromScreen((Point2D) pt.clone());
			if (element.getBounds().contains(containerPoint.getX(), containerPoint.getY())) {
				Rectangle2D rectBounds = element.getBounds();
				containerPoint.setLocation(containerPoint.getX() - rectBounds.getX(), containerPoint.getY()
						- rectBounds.getY());
				Component renderer = element.getRendererComponent(graph, false, false, false);
				if ((renderer instanceof PipeAlgorithmGroupView.ActivityRenderer)
						&& DefaultGraphModel.isGroup(graph.getModel(), element.getCell())) {
					PipeAlgorithmGroupView.ActivityRenderer group = (PipeAlgorithmGroupView.ActivityRenderer) renderer;
					if (group.inHitRegion(containerPoint)) {
						return element;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Called when the mouse button is released to see if a collapse or expand
	 * request has been made.
	 * 
	 * @param e
	 *            the e
	 */
	public void mouseReleased(MouseEvent e) {
		if (e.getSource() instanceof JGraph) {
			final JGraph graph = (JGraph) e.getSource();
			CellView view = getGroupByFoldingHandle(graph, e.getPoint());
			if (view != null) {
				if (view.isLeaf()) {
					Object obj = view.getCell();
					if (obj instanceof AlgorithmGroupCell) {
						graph.getGraphLayoutCache().expand(new Object[] { obj });
						graph.getGraphLayoutCache().toBack(new Object[] { obj });
						// ((AlgorithmGroupCell)obj).setPortVisibility(graph,
						// false);
					}
				} else {
					Object obj = view.getCell();
					if (obj instanceof AlgorithmGroupCell) {
						// ((AlgorithmGroupCell)obj).setPortVisibility(graph,
						// true);
						graph.getGraphLayoutCache().collapse(new Object[] { obj });
					}
				}
			}
			e.consume();
		}
	}
}
