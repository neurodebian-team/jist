package edu.jhu.ece.iacl.jist.io;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.vecmath.Color3f;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import gov.nih.mipav.view.ViewJProgressBar;
import gov.nih.mipav.view.renderer.J3D.model.structures.ModelTriangleMesh;

// TODO: Auto-generated Javadoc
/**
 * The Class SurfaceVrmlReaderWriter.
 */
public class SurfaceVrmlReaderWriter extends SurfaceReaderWriter{
	
	/** The Constant readerWriter. */
	protected static final SurfaceVrmlReaderWriter readerWriter=new SurfaceVrmlReaderWriter();
	
	/**
	 * Gets the single instance of SurfaceVrmlReaderWriter.
	 * 
	 * @return single instance of SurfaceVrmlReaderWriter
	 */
	public static SurfaceVrmlReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new surface vrml reader writer.
	 */
	public SurfaceVrmlReaderWriter(){
		super(new FileExtensionFilter(new String[]{"wrl"}));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter#readObject(java.io.File)
	 */
	protected EmbeddedSurface readObject(File f) {
		RandomAccessFile in;
		try {
			in = new RandomAccessFile(f, "r");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		if(!MipavController.isQuiet()){
			ViewJProgressBar progress = new ViewJProgressBar("Loading surface", "Loading surface", 0, 100, false, null, null);
			try {
				ModelTriangleMesh mesh=ModelTriangleMesh.loadVRMLMesh(in, progress, 0, 1, true);
				if(mesh.getName()==null||mesh.getName().length()==0){
					mesh.setName(FileReaderWriter.getFileName(f));
				}
				return new EmbeddedSurface(mesh);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} finally {
				progress.dispose();
			}
		} else {
			System.err.println(getClass().getCanonicalName()+"Cannot read VRML files in quiet mode.");
			System.exit(1);
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter#writeObject(edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface, java.io.File)
	 */
	protected File writeObject(EmbeddedSurface mesh, File f) {
		f = new File(f.getParent(), mesh.getName() + ".wrl");
		try {
			ModelTriangleMesh.saveAsVRML(f.getCanonicalPath(),new ModelTriangleMesh[]{mesh},false,new int[]{1,1,1},new float[]{0,0,0},new float[]{1,1,1},new Color3f(Color.WHITE));
			return f;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

}
