/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeSourceFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeSourceFactory.SourceCell;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;

// TODO: Auto-generated Javadoc
/**
 * Module for generating parameters used as input to algorithm modules.
 * 
 * @author Blake Lucas
 */
public abstract class PipeSource extends PipeModule {
	
	/** Port for parent source. */
	protected PipeParentPort parentPort;
	
	/** Port for child source. */
	protected PipeChildPort childPort;
	
	/** Flag for source is reset after iterating through all parameter values. */
	transient protected boolean isReset = false;
	
	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);
		return val;
//		throw new RuntimeException("xml sources not yet implemented");

	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
//		throw new RuntimeException("Not implemented yet");
		this.label = inputParams.getLabel();
		this.name = inputParams.getName();
		parentPort = new PipeParentPort();
		parentPort.setOwner(this);
		childPort = new PipeChildPort();
		childPort.setOwner(this);
		inputPorts = (Vector) inputParams.getAllVisibleDescendants();
		for (PipePort port : inputPorts) {
			port.setOwner(this);
		}
		inputParams.getInputView().addObserver(this);
		outputPorts = (Vector) outputParams.getAllVisibleDescendants();
		for (PipePort port : outputPorts) {
			port.setOwner(this);
		}
		outputParams.setLabel(inputParams.getLabel());
		outputParams.setName(inputParams.getName());
		init(null);
	}
	/**
	 * Constructor for source creates all associated input and output
	 * parameters.
	 */
	public PipeSource() {
		super();
		this.inputParams = createInputParams();
		this.outputParams = createOutputParams();
		this.label = inputParams.getLabel();
		this.name = inputParams.getName();
		parentPort = new PipeParentPort();
		parentPort.setOwner(this);
		childPort = new PipeChildPort();
		childPort.setOwner(this);
		inputPorts = (Vector) inputParams.getAllVisibleDescendants();
		for (PipePort port : inputPorts) {
			port.setOwner(this);
		}
		inputParams.getInputView().addObserver(this);
		outputPorts = (Vector) outputParams.getAllVisibleDescendants();
		for (PipePort port : outputPorts) {
			port.setOwner(this);
		}
		outputParams.setLabel(inputParams.getLabel());
		outputParams.setName(inputParams.getName());
		init(null);
	}

	/**
	 * Create input parameters for this source.
	 * 
	 * @return input parameters
	 */
	protected abstract ParamCollection createInputParams();

	/**
	 * Create new graph cell for this source.
	 * 
	 * @return the pipe module cell
	 */
	public SourceCell createModuleCell() {
		return new PipeSourceFactory.SourceCell(getLabel(), this);
	}

	/**
	 * Create output parameters for this source.
	 * 
	 * @return output parameters
	 */
	protected abstract ParamCollection createOutputParams();

	/**
	 * Disconnect this module and all connections to this module.
	 */
	public void disconnect() {
		super.disconnect();
		childPort.disconnect();
		parentPort.disconnect();
	}

	/**
	 * Get port for child connection.
	 * 
	 * @return child port
	 */
	public PipeChildPort getChildPort() {
		return childPort;
	}

	/**
	 * Get parameter representing the output to this source.
	 * 
	 * @return output parameter
	 */
	public abstract ParamModel getOutputParam();

	/**
	 * Get port for parent connection.
	 * 
	 * @return parent port
	 */
	public PipeParentPort getParentPort() {
		return parentPort;
	}

	/**
	 * Find the root source starting from this source.
	 * 
	 * @return the root
	 */
	public PipeSource getRoot() {
		Vector<PipePort> ports = childPort.getIncomingPorts();
		if (ports.size() > 0) {
			return ((PipeParentPort) ports.firstElement()).getOwner().getRoot();
		} else {
			return this;
		}
	}

	/**
	 * Returns true if there are more parameter values.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		boolean ret = false;
		for (PipePort port : getParentPort().getOutgoingPorts()) {
			if (port instanceof PipeChildPort) {
				PipeSource src = ((PipeChildPort) port).getOwner();
				if (src.hasNext()) {
					ret = true;
				}
			}
		}
		return ret;
	}

	/**
	 * Initialize fields that could not be deserialized.
	 * 
	 * @param graph the graph
	 * 
	 * @return the pipe module cell
	 */
	public PipeModuleCell init(PipeJGraph graph) {
		parentPort.setPortType(PipePort.type.OUTPUT);
		childPort.setPortType(PipePort.type.INPUT);
		for (PipePort port : inputPorts) {
			port.setPortType(PipePort.type.INPUT);
		}
		for (PipePort port : outputPorts) {
			port.setPortType(PipePort.type.OUTPUT);
		}
		isReset = false;
		if (graph != null) {
			return graph.createGraphNode(this);
		} else {
			return null;
		}
	}

	/**
	 * Returns true if this source is a root.
	 * 
	 * @return true, if checks if is root
	 */
	public boolean isRoot() {
		return (!this.getChildPort().isConnected());
	}

	/**
	 * Iterate through all parameter values and all child parameter values.
	 * 
	 * @return True if there are more elements to iterate through
	 */
	public boolean iterate() {
		boolean ret = false;
		boolean allReset = true;
		for (PipePort port : getParentPort().getOutgoingPorts()) {
			if (port instanceof PipeChildPort) {
				PipeSource src = ((PipeChildPort) port).getOwner();
				if (src.iterate()) {
					ret = true;
				}
				if (!src.isReset) {
					allReset = false;
				}
			}
		}
		if (allReset) {
			for (PipePort port : getParentPort().getOutgoingPorts()) {
				if (port instanceof PipeChildPort) {
					PipeSource src = ((PipeChildPort) port).getOwner();
					src.reset();
				}
			}
			return false;
		}
		return ret;
	}
	
	/**
	 * Reset this source and all child sources.
	 */
	public void reset() {
		isReset = false;
		for (PipePort port : getParentPort().getOutgoingPorts()) {
			if (port instanceof PipeChildPort) {
				PipeSource src = ((PipeChildPort) port).getOwner();
				src.reset();
			}
		}
	}
	public String reconcileAndMerge() throws InvalidJistMergeException {
//		throw new InvalidJistMergeException("Not able to merge sources yet");
//		return "";
		return "Source merge not currently necessary."; 
	}
	
	public boolean xmlEncodeConnections(Document document, Element parent) {
		boolean val = super.xmlEncodeConnections(document,parent);
		if(parentPort.xmlEncodeConnections(document,parent))
			val=true;
		if(childPort.xmlEncodeConnections(document,parent))
			val=true;
		return val;
	}

}
