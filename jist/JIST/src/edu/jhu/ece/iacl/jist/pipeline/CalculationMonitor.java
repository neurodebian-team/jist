/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import gov.nih.mipav.view.ViewJProgressBar;

import java.awt.Dimension;

// TODO: Auto-generated Javadoc
/**
 * Monitor a calculation during its execution.
 * 
 * @author Blake Lucas
 */
public class CalculationMonitor implements Runnable {
	
	/** interval to poll for progress. */
	private static long pollInterval = 3000;

	/**
	 * Set interval at which the algorithm is polled. The default is 3 seconds.
	 * 
	 * @param interval Poll interval in milliseconds
	 */
	public static void setPollInterval(long interval) {
		pollInterval = interval;
	}

	/** progress bar to display. */
	private ViewJProgressBar progressBar;
	
	/** collection of observable calculations. */
	private ObservableCalculation calcCollection;
	
	/** The root calculation. */
	private AbstractCalculation rootCalculation;
	
	/** flag indicating calculation is running. */
	private boolean running;
	
	/** algorithm root to monitor. */
	private ProcessingAlgorithm algo;
	
	/** thread used to update progress. */
	private Thread th = null;

	/**
	 * Default constructor with algorithm and progress bar.
	 * 
	 * @param algo the algorithm
	 * @param progressBar the progress bar
	 */
	public CalculationMonitor(ProcessingAlgorithm algo, ViewJProgressBar progressBar) {
		this(algo, progressBar, null);

	}

	/**
	 * Default constructor with progress bar, observable calculation, and
	 * algorithm.
	 * 
	 * @param progressBar progress bar
	 * @param calc algorithm to monitor
	 * @param algo the algorithm
	 */
	public CalculationMonitor(ProcessingAlgorithm algo, ViewJProgressBar progressBar, AbstractCalculation calc) {
		this.algo = algo;
		this.progressBar = progressBar;
		this.calcCollection = new ObservableCalculation("Algorithm Calculation", this);
		if (calc != null) {
			this.calcCollection.add(calc);
		}
	}

	/**
	 * Get summary of performance. Marks all calculations as completed if they
	 * have not been marked previously.
	 * 
	 * @return the performance
	 */
	public PerformanceSummary getPerformance() {
		if (this.calcCollection != null) {
			calcCollection.markCompleted();
			return calcCollection.getPerformance();
		} else {
			return null;
		}
	}

	/**
	 * Get time stamp in milliseconds.
	 * 
	 * @return time in milliseconds
	 */
	public long getTimeStamp() {
		return algo.getTimeStamp();
	}

	/**
	 * Start observing a calculation.
	 * 
	 * @param calc the calculation
	 */
	public void observe(AbstractCalculation calc) {
		this.rootCalculation = calc;
		calc.setMonitor(this);
		calcCollection.add(calc);
		if (running) {
			this.stop();
		}
		this.start();
	}

	/**
	 * Periodically poll the algorithm to determine its current progress.
	 */
	public void run() {
		running = true;
		while (running) {
			updateProgress();
			System.out.flush();
			System.err.flush();
			try {
				Thread.sleep(pollInterval);
			} catch (InterruptedException e) {
				running = false;
				System.err.println(getClass().getCanonicalName()+"Calculation monitor shutting down by interrupt.");
				System.err.flush();
				//System.err.println(getClass().getCanonicalName()+e.getMessage());
			}
		}
		//		running = false;
		if (progressBar != null) {
			progressBar.updateValue((int) Math.round(rootCalculation.getCurrentProgress() * 100));
			progressBar.setMessage(rootCalculation.getLabel() + " complete.");
		}
		System.err.println(getClass().getCanonicalName()+"CalculationMonitor finished naturally.");System.err.flush();
	}

	/**
	 * Set root calculation for this monitor.
	 * 
	 * @param calc the calculation
	 */
	public void setObservableCalculation(AbstractCalculation calc) {
		this.rootCalculation = calc;
		calcCollection = new ObservableCalculation("Algorithm Calculation", this);
		calcCollection.add(calc);
	}

	/**
	 * Start calculation monitor.
	 */
	public synchronized void start() {
//		System.err.println(getClass().getCanonicalName()+"Waiting to start Calculation Monitor");System.err.flush();

//		System.err.println(getClass().getCanonicalName()+"Starting Calculation Monitor");System.err.flush();			
		if(!running) {
			running = true;
		} else {
			System.err.println(getClass().getCanonicalName()+"CalculationMonitor is already running. Ignoring.");
			return;
		}

		if(th==null) {			
			th = new Thread(this);
			th.setName("CalculationMonitor-"+th.getId()); //aid in debugging
			th.setPriority(Thread.MIN_PRIORITY);
		} else {
			System.err.println(getClass().getCanonicalName()+"CalculationMonitor is non-null. Warning.");
		}
		if (progressBar != null) {
			progressBar.setVisible(true);
			progressBar.setMaximumSize(new Dimension(320, 500));
			progressBar.setTitle(rootCalculation.getLabel());
			progressBar.pack();
		}
		th.start();

//		System.err.println(getClass().getCanonicalName()+"Started Calculation Monitor");System.err.flush();
	}

	/**
	 * Stop progress monitoring.
	 */
	public synchronized void stop() {
		if(!running || th==null) {
			System.err.println(getClass().getCanonicalName()+"Calculation Monitor not running. Punting stop.");System.err.flush();
			return;
		}
//		System.err.println(getClass().getCanonicalName()+"Waiting to stop Calculation Monitor");System.err.flush();


//		System.err.println(getClass().getCanonicalName()+"Stopping Calculation Monitor.");System.err.flush();
		this.running = false;

		if (th != null) {			
			try {
				//th.join();
				if(th.isAlive()) {
					th.interrupt();
					th.join(200);
					if(th.isAlive())
						System.err.println(getClass().getCanonicalName()+"Calculation monitor join failed.");				
				}
			} catch (InterruptedException e) {
				System.err.println(getClass().getCanonicalName()+"Calculation monitor stop interrupted.");
				System.err.println(getClass().getCanonicalName()+e.getMessage());
			}
			//			try {
			//				th.join();
			//				Thread.sleep(1000);
			//			} catch (InterruptedException e) {
			//				System.err.println(getClass().getCanonicalName()+e.getMessage());
			//			}
		}
		// should not be needed with the new handling of progress bars in Mipav
		// (3.01+)
		if (progressBar != null) {
			progressBar.setVisible(false);
		}
		th=null;
//		System.err.println(getClass().getCanonicalName()+"Stopped Calculation Monitor");System.err.flush();
	}

	/**
	 * Update progress bar and print progress information.
	 */
	private void updateProgress() { 
		if (progressBar != null) {
			progressBar.updateValue((int) Math.round(rootCalculation.getCurrentProgress() * 100));
			String str = rootCalculation.getCurrentLabel();
			progressBar.setTitle(rootCalculation.getLabel());
			if (str.length() > 0) {
				progressBar.setMessage(str);
			}
			progressBar.pack();
		} else {
			Runtime r = Runtime.getRuntime();
			String str = rootCalculation.getCurrentLabel();
			PipeRunner.writeStatus((int) Math.round(rootCalculation.getCurrentProgress() * 100) + " "
					+ ((r.totalMemory() - r.freeMemory()) / (1 << 20)) + " " + ((r.totalMemory()) / (1 << 20)) + " "
					+ str);
		}
	}
}
