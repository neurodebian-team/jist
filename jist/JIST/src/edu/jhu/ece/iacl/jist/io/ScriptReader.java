package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import gov.nih.mipav.model.scripting.ParsedActionLine;
import gov.nih.mipav.model.scripting.ParserEngine;
import gov.nih.mipav.model.scripting.ParserException;

// TODO: Auto-generated Javadoc
/**
 * The Class ScriptReader.
 */
public class ScriptReader extends FileReaderWriter<List<ParsedActionLine>>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/**
	 * Instantiates a new script reader.
	 */
	public ScriptReader() {
		super(new FileExtensionFilter(new String[]{"sct"}));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected List<ParsedActionLine> readObject(File f) {
		try {
			ParserEngine parser=new ParserEngine(f.getAbsolutePath(),true);
			LinkedList<ParsedActionLine> lines=new LinkedList<ParsedActionLine>();
			while(parser.hasMoreLinesToParse()){
				lines.add(parser.parseNextLine());
			}
			return lines;
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(List<ParsedActionLine> obj, File f) {
		// TODO Auto-generated method stub
		return null;
	}

}
