package edu.jhu.ece.iacl.jist.pipeline.parser;

import java.awt.Dimension;
import java.io.File;
import java.util.Date;
import java.util.Vector;

import javax.swing.JFileChooser;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamLong;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmMipavAdapter;
import gov.nih.mipav.model.scripting.ParsedActionLine;
import gov.nih.mipav.model.scripting.ParserEngine;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.parameters.Parameter;
import gov.nih.mipav.model.scripting.parameters.ParameterBoolean;
import gov.nih.mipav.model.scripting.parameters.ParameterDouble;
import gov.nih.mipav.model.scripting.parameters.ParameterFloat;
import gov.nih.mipav.model.scripting.parameters.ParameterInt;
import gov.nih.mipav.model.scripting.parameters.ParameterList;
import gov.nih.mipav.model.scripting.parameters.ParameterLong;
import gov.nih.mipav.model.scripting.parameters.ParameterShort;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.model.scripting.parameters.ParameterUShort;

public class MipavScriptParser implements ScriptParser {
	/**
	 * Parse MIPAV script
	 */
	public PipeAlgorithm openPipeAlgorithm() {
		JFileChooser loadDialog = new JFileChooser("Open MIPAV Script");
		loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(new String[] { "sct" }));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return parsePipeAlgorithm(loadDialog.getSelectedFile());
		} else {
			return null;
		}
	}

	/**
	 * Parse MIPAV script
	 */
	public PipeAlgorithm parsePipeAlgorithm(File f) {
		if (!FileReaderWriter.getFileExtension(f).equals("sct")) {
			return null;
		}
		try {
			MedicAlgorithmMipavAdapter mipavAdapter = new MedicAlgorithmMipavAdapter();
			ParamCollection inputParams = mipavAdapter.getInput();
			ParamCollection outputParams = mipavAdapter.getOutput();
			String label = FileReaderWriter.getFileName(f);
			String name = label.replaceAll(" ", "_");
			inputParams.setLabel(label);
			inputParams.setName(name);
			outputParams.setLabel(label);
			outputParams.setName(name);
			ParserEngine parser = new ParserEngine(f.getAbsolutePath(), true);
			ParamVolume volParam;
			ParamString scriptFile = new ParamString("MIPAV Script");
			String script = StringReaderWriter.getInstance().read(f);
			script = script.replace("\n", "<BR>");
			scriptFile.setValue(script);
			scriptFile.setHidden(true);
			inputParams.add(scriptFile);
			int count = 0;
			mipavAdapter.getAlgorithmInformation().init(mipavAdapter);
			mipavAdapter.getAlgorithmInformation().setCreationDate(new Date(f.lastModified()));
			while (parser.hasMoreLinesToParse()) {
				ParsedActionLine parsedLine = parser.parseNextLine();
				if (parsedLine != null) {
					String action = parsedLine.getAction();
					if (action.equals("CloseFrame") || action.equals("SaveImage") || action.equals("SaveImageAs")
							|| action.equals("SaveAll") || action.equals("Exit")) {
						continue;
					}
					ParamCollection algoLine = new ParamCollection(action + " (" + (++count) + ")");
					ParameterTable paramTable = parsedLine.getParameterTable();
					Parameter[] params = paramTable.getParameters();
					for (Parameter param : params) {
						switch (param.getType()) {
						case Parameter.PARAM_BOOLEAN:
							algoLine.add(new ParamBoolean(param.getLabel(), ((ParameterBoolean) param).getValue()));
							break;
						case Parameter.PARAM_DOUBLE:
							algoLine.add(new ParamDouble(param.getLabel(), ((ParameterDouble) param).getValue()));
							break;
						case Parameter.PARAM_FLOAT:
							algoLine.add(new ParamFloat(param.getLabel(), ((ParameterFloat) param).getValue()));
							break;
						case Parameter.PARAM_INT:
							algoLine.add(new ParamInteger(param.getLabel(), ((ParameterInt) param).getValue()));
							break;
						case Parameter.PARAM_USHORT:
							algoLine.add(new ParamInteger(param.getLabel(), ((ParameterUShort) param).getValue()));
							break;
						case Parameter.PARAM_SHORT:
							algoLine.add(new ParamInteger(param.getLabel(), ((ParameterShort) param).getValue()));
							break;
						case Parameter.PARAM_LONG:
							algoLine.add(new ParamLong(param.getLabel(), (int) ((ParameterLong) param).getValue()));
							break;
						case Parameter.PARAM_EXTERNAL_IMAGE:
							volParam = new ParamVolume(param.getValueString(),null,-1,-1,-1,-1);
							volParam.setLabel(param.getLabel());
							algoLine.add(volParam);
							break;
						case Parameter.PARAM_STRING:
							algoLine.add(new ParamString(param.getLabel(), param.getValueString()));
							break;
						case Parameter.PARAM_LIST:
							ParameterList list = ((ParameterList) param);
							for (Parameter cparam : (Vector<Parameter>) list.getList()) {
								switch (list.getListType()) {
								case Parameter.PARAM_BOOLEAN:
									algoLine.add(new ParamBoolean(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterBoolean) cparam).getValue()));
									break;
								case Parameter.PARAM_DOUBLE:
									algoLine.add(new ParamDouble(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterDouble) cparam).getValue()));
									break;
								case Parameter.PARAM_FLOAT:
									algoLine.add(new ParamFloat(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterFloat) cparam).getValue()));
									break;
								case Parameter.PARAM_INT:
									algoLine.add(new ParamInteger(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterInt) cparam).getValue()));
									break;
								case Parameter.PARAM_USHORT:
									algoLine.add(new ParamInteger(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterUShort) cparam).getValue()));
									break;
								case Parameter.PARAM_SHORT:
									algoLine.add(new ParamInteger(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterShort) cparam).getValue()));
									break;
								case Parameter.PARAM_LONG:
									algoLine.add(new ParamLong(param.getLabel() + "_" + cparam.getLabel(),
											((ParameterLong) cparam).getValue()));
									break;
								case Parameter.PARAM_EXTERNAL_IMAGE:
									volParam = new ParamVolume(param.getLabel() + "_" + cparam.getValueString(),null,-1,-1,-1,-1);
									volParam.setLabel(cparam.getLabel());
									algoLine.add(volParam);
									break;
								case Parameter.PARAM_STRING:
									algoLine.add(new ParamString(param.getLabel() + "_" + cparam.getLabel(), cparam
											.getValueString()));
									break;
								}
							}
							break;
						}
					}
					inputParams.add(algoLine);
				}
			}
			PipeAlgorithm algo = new PipeAlgorithm();
			algo.setAlgorithm(mipavAdapter);
			Dimension d = PipeModuleCell.getPreferredSize(inputParams, outputParams);
			return algo;
		} catch (ParserException e) {
			System.err.println(getClass().getCanonicalName()+f.getAbsolutePath() + ":" + e.getMessage());
		}
		return null;
	}
}