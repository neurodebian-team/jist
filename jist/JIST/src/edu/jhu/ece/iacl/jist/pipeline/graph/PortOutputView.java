/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Polygon;
import java.awt.Shape;

import org.jgraph.graph.CellViewRenderer;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Cell view renderer for output ports.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PortOutputView extends PipePortView {
	
	/**
	 * The Class ActivityRenderer.
	 */
	public class ActivityRenderer extends PipePortView.ActivityRenderer {
		
		/**
		 * Instantiates a new activity renderer.
		 */
		public ActivityRenderer() {
			super();
			setForeground(Color.gray);
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipePortView.ActivityRenderer#createShape()
		 */
		public Shape createShape() {
			Dimension d = this.getSize();
			Polygon poly = new Polygon();
			poly.addPoint(2, 1);
			poly.addPoint(d.width - 1, 1);
			poly.addPoint(Math.round((d.width) / 2.0f) + 1, d.height - 2);
			return poly;
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipePortView.ActivityRenderer#isCompatible()
		 */
		protected boolean isCompatible() {
			PipePort src = graph.getPortToCheck();
			PipePort dest = ((PipeModulePort) view.getCell()).getPort();
			if (src == null) {
				return false;
			}
			if (src == dest) {
				return false;
			}
			if (dest.isOutputPort() && src.isInputPort()) {
				return PipeConnector.isCompatible(dest, src);
			} else {
				return false;
			}
		}
	}

	/** The renderer. */
	protected transient ActivityRenderer renderer = new ActivityRenderer();

	/**
	 * Instantiates a new port output view.
	 */
	public PortOutputView() {
		super();
	}

	/**
	 * Instantiates a new port output view.
	 * 
	 * @param cell
	 *            the cell
	 */
	public PortOutputView(Object cell) {
		super(cell);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipePortView#getRenderer()
	 */
	public CellViewRenderer getRenderer() {
		return this.renderer;
	}
}
