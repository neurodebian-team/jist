package edu.jhu.ece.iacl.jist.structures.vector;
// TODO: Auto-generated Javadoc

/**
 * The Class Vector4.
 */
public class Vector4 implements VectorX{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The w. */
	public Number x,y,z,w;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getX()
	 */
	public Number getX(){return x;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getY()
	 */
	public Number getY(){return y;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getZ()
	 */
	public Number getZ(){return z;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getW()
	 */
	public Number getW(){return w;}	
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#size()
	 */
	public int size(){return 4;}
	
	/**
	 * Instantiates a new vector4.
	 * 
	 * @param m the m
	 */
	public Vector4(Number m[]){
		x=m[0];
		y=m[1];
		z=m[2];
		w=m[3];
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#get(int)
	 */
	public Number get(int i){
		switch(i){
			case 0:return x;
			case 1:return y;
			case 2:return z;
			case 3:return w;
			default:return null;
		}
	}
	
	/**
	 * Instantiates a new vector4.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param w the w
	 */
	public Vector4(Number x,Number y,Number z,Number w){
		this.x=x;
		this.y=y;
		this.z=z;
		this.w=w;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setX(java.lang.Number)
	 */
	public void setX(Number x){this.x=x;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setY(java.lang.Number)
	 */
	public void setY(Number y){this.y=y;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setZ(java.lang.Number)
	 */
	public void setZ(Number z){this.z=z;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setW(java.lang.Number)
	 */
	public void setW(Number w){this.w=w;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#toArray()
	 */
	public Number[] toArray() {
		return new Number[]{x,y,z};
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(double)
	 */
	public Vector4 add(double a) {
		return new Vector4(getX().doubleValue()+a,getY().doubleValue()+a,getZ().doubleValue()+a,getW().doubleValue()+a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(double)
	 */
	public Vector4 sub(double a) {
		return new Vector4(getX().doubleValue()-a,getY().doubleValue()-a,getZ().doubleValue()-a,getW().doubleValue()-a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(double)
	 */
	public Vector4 mul(double a) {
		return new Vector4(getX().doubleValue()*a,getY().doubleValue()*a,getZ().doubleValue()*a,getW().doubleValue()*a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(double)
	 */
	public Vector4 div(double a) {
		return new Vector4(getX().doubleValue()/a,getY().doubleValue()/a,getZ().doubleValue()/a,getW().doubleValue()/a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector4 add(VectorX v) {
		return new Vector4(getX().doubleValue()+v.getX().doubleValue(),getY().doubleValue()+v.getY().doubleValue(),getZ().doubleValue()+v.getZ().doubleValue(),getW().doubleValue()+v.getW().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector4 sub(VectorX v) {
		return new Vector4(getX().doubleValue()-v.getX().doubleValue(),getY().doubleValue()-v.getY().doubleValue(),getZ().doubleValue()-v.getZ().doubleValue(),getW().doubleValue()-v.getW().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector4 mul(VectorX v) {
		return new Vector4(getX().doubleValue()*v.getX().doubleValue(),getY().doubleValue()*v.getY().doubleValue(),getZ().doubleValue()*v.getZ().doubleValue(),getW().doubleValue()*v.getW().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector4 div(VectorX v) {
		return new Vector4(getX().doubleValue()/v.getX().doubleValue(),getY().doubleValue()/v.getY().doubleValue(),getZ().doubleValue()/v.getZ().doubleValue(),getW().doubleValue()/v.getW().doubleValue());
	}
	
	/**
	 * Sets the.
	 * 
	 * @param v the v
	 */
	public void set(Vector4 v){
		x=v.x;
		y=v.y;
		z=v.z;
		w=v.w;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#normalize()
	 */
	public Vector4 normalize(){
		double mag=mag().doubleValue();
		if(mag>0)return div(mag); else return this;	
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mag()
	 */
	public Double mag() {
		return new Double(Math.sqrt(x.doubleValue()*x.doubleValue()+y.doubleValue()*y.doubleValue()+z.doubleValue()*z.doubleValue()+w.doubleValue()*w.doubleValue()));
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Vector4 clone(){
		return new Vector4(x,y,z,w);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return ("["+x+","+y+","+z+","+w+"]");
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#set(java.lang.Number, int)
	 */
	public void set(Number value, int i) {
		switch(i){
			case 0:x=value;
			case 1:y=value;
			case 2:z=value;
			case 3:w=value;
		}
	}

}
