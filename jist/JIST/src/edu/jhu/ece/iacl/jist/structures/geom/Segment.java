/**
 * 
 */
package edu.jhu.ece.iacl.jist.structures.geom;

// TODO: Auto-generated Javadoc
/**
 * The Class Segment.
 * 
 * @author Blake Lucas
 */
public class Segment {
	
	/** The v2. */
	public Vertex v1,v2;
	
	/**
	 * Instantiates a new segment.
	 * 
	 * @param v1 the v1
	 * @param v2 the v2
	 */
	protected Segment(Vertex v1,Vertex v2){
		this.v1=v1;
		this.v2=v2;
	}
	
	/**
	 * To array.
	 * 
	 * @return the vertex[]
	 */
	public Vertex[] toArray(){
		return new Vertex[]{v1,v2};
	}
	
	/**
	 * Gets the opposite.
	 * 
	 * @param v the v
	 * 
	 * @return the opposite
	 */
	public Vertex getOpposite(Vertex v){ 
		return (v==v1)?v2:v1;
	}
	
	/**
	 * Contains.
	 * 
	 * @param v the v
	 * 
	 * @return true, if successful
	 */
	public boolean contains(Vertex v){
		return (v1==v||v2==v);
	}
	
	/**
	 * Contains.
	 * 
	 * @param V1 the v1
	 * @param V2 the v2
	 * 
	 * @return true, if successful
	 */
	public boolean contains(Vertex V1,Vertex V2){
		return ((v1==V1&&v2==V2)||(v1==V2&&v2==V1));
	}
	
	/**
	 * Flip.
	 */
	public void flip(){
		Vertex tmp=v1;
		v1=v2;
		v2=tmp;
	}
	
	/**
	 * Gets the vector.
	 * 
	 * @return the vector
	 */
	public Vector3 getVector(){
		Vector3 vec=new Vector3();
		vec.sub(v2,v1);
		return vec;
	}
}
