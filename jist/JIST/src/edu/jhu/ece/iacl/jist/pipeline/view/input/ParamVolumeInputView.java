/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Volume Parameter Input View allows the user to select a currently opened
 * image from a combobox. The user can also open an existing image by selecting
 * BROWSE_TEXT If an image is opened or closed using MIPAV, the combobox will be
 * updated to reflect the changes.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeInputView extends ParamInputView implements ActionListener, Refreshable {

	/**
	 * List box item that displays the image name.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class VolumeListBoxItem implements Comparable<VolumeListBoxItem> {

		/** The name. */
		public String name;

		/** The file. */
		public File file;

		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(VolumeListBoxItem obj) {
			return name.compareTo(obj.name);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object obj) {
			return file.equals(((VolumeListBoxItem) obj).file);
		}
	}

	/** The Constant BROWSE_TEXT. */
	private static final String BROWSE_TEXT = "Browse";

	/** The Constant BLANK_TEXT. */
	private static final String BLANK_TEXT = "-----NONE-----";
	/** Field for selecting image input. */
	protected JComboBox field;

	/** The image list size. */
	protected int imageListSize = -1;

	/** The browse button. */
	protected JButton browseButton;

	/**
	 * Construct combox to select a currently opened image.
	 * 
	 * @param param
	 *            the parameter
	 */
	public ParamVolumeInputView(ParamVolume param) {
		super(param);
		field = new JComboBox();
		browseButton = new JButton(BROWSE_TEXT);
		browseButton.setPreferredSize(defaultNumberFieldDimension);
		// Create combo box with default values
		updateComboBox(null);
		JPanel smallPane = new JPanel(new BorderLayout());
		smallPane.add(field, BorderLayout.CENTER);
		field.setMinimumSize(defaultNumberFieldDimension);
		JPanel myPanel = new JPanel(new BorderLayout());
		myPanel.add(browseButton, BorderLayout.EAST);
		smallPane.add(myPanel, BorderLayout.SOUTH);
		buildLabelAndParam(smallPane);
		browseButton.addActionListener(this);
		// Create refresher to monitor for MIPAV changes
		Refresher.getInstance().add(this);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(browseButton)) {
			// Browse selected, open file dialog

			ArrayList<ImageData> imgs = MipavController.openFileDialog(getParameter().getValue());
			Refresher.getInstance().pauseAll();
			if ((imgs != null) && (imgs.size() > 0)) {
				// Select first image
				setSelected(imgs.get(0));
			} else {
				// Select no image
				setSelected(null);
			}
			Refresher.getInstance().resumeAll();
		} else if (event.getSource().equals(field)) {
			// Get selected item
			if(!updateLock){
				String selectedImage = (String) field.getSelectedItem();
				selectedImage = (String) field.getSelectedItem();
				if (selectedImage != null) {
					// An image is selected
					if (!selectedImage.equals(BLANK_TEXT)) {
						// Set parameter value to selected Image
						getParameter().setValue(selectedImage);
						notifyObservers(param, this);
					} else {
						// Clear parameter value
						getParameter().clear();
						notifyObservers(param, this);
					}
				}
			}
		}
	}

	/**
	 * Commit changes to parameter.
	 */
	public /*synchronized*/ void commit() {
		if(field.getSelectedIndex()>0){
			getParameter().setValue((String)field.getSelectedItem());
		} else {
			getParameter().clear();
		}
	}

	/**
	 * Get volume parameter.
	 * 
	 * @return the parameter
	 */
	public ParamVolume getParameter() {
		return (ParamVolume) param;
	}

	/**
	 * Refresh volume selection input by rebuilding combobox.
	 */
	public /*synchronized*/ void refresh() {
		commit();
		updateComboBox(null);
	}

	/**
	 * Set the selected ModelImage in the combobox.
	 * 
	 * @param img
	 *            model image
	 */
	public void setSelected(ImageData img) {
		updateComboBox((img != null) ? img.getName() : null);
	}

	/**
	 * Update view with value from parameter.
	 */
	public void update() {
		setSelected(getParameter().getImageData());
	}
	private static boolean updateLock=false;
	/**
	 * Update combobox selection.
	 * 
	 * @param item
	 *            selected item
	 * @return true if update was necessary
	 */
	protected boolean updateComboBox(String item) {
		if(updateLock)return false;
		updateLock=true;
		Vector<String> imageNames = MipavController.getImageNames();
		field.removeActionListener(this);
		// Rebuild selection box if an item is selected
		if (imageNames.size() != imageListSize) {
			// Remember current selected item if no selected item is specified
			if (item == null) {
				item = (String) field.getSelectedItem();
			}
			imageNames.insertElementAt(BLANK_TEXT, 0);
			imageListSize = imageNames.size() - 1;
			field.removeAllItems();
			for (String name : imageNames) {
				field.addItem(name);
			}
			// Try to select image if list was originally empty
		} else {
			// Nothing to update
			if (item == null) {
				// Enable listener
				field.addActionListener(this);
				updateLock=false;
				return false;
			}
		}
		if (item != null) {
			if (!item.equals(BLANK_TEXT)) {
				// Update selected item as long as it is not "Browse"
				field.setSelectedItem(item);
				getParameter().setValue(item);
				notifyObservers(param, this);
			} else {
				// No item selected, set parameter to null
				getParameter().clear();
				field.setSelectedIndex(0);
				notifyObservers(param, this);
			}
			// Enable listener
			field.addActionListener(this);
			updateLock=false;
			return true;
		} else {
			// Enable listener
			field.addActionListener(this);
			updateLock=false;
			return false;
		}
	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return field;
	}
}
