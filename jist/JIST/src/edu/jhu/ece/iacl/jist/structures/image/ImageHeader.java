/**
 * 
 */
package edu.jhu.ece.iacl.jist.structures.image;

import gov.nih.mipav.model.file.FileInfoBase;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageHeader.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 * Sagittal da
 */
public class ImageHeader implements Cloneable{

	/**
	 * The Enum MeasurementUnit.
	 */
	public enum MeasurementUnit {


		/** The NULL. */
		NULL(gov.nih.mipav.model.file.FileInfoBase.UNKNOWN_MEASURE),
		/** The UNKNOW n_ measure. */
		UNKNOWN_MEASURE(gov.nih.mipav.model.file.FileInfoBase.UNKNOWN_MEASURE),
		/** The INCHES. */
		INCHES(gov.nih.mipav.model.file.FileInfoBase.INCHES),
		/** The CENTIMETERS. */
		CENTIMETERS(gov.nih.mipav.model.file.FileInfoBase.CENTIMETERS),
		/** The ANGSTROMS. */
		ANGSTROMS(gov.nih.mipav.model.file.FileInfoBase.ANGSTROMS),
		/** The NANOMETERS. */
		NANOMETERS(gov.nih.mipav.model.file.FileInfoBase.NANOMETERS),
		/** The MICROMETERS. */
		MICROMETERS(gov.nih.mipav.model.file.FileInfoBase.MICROMETERS),
		/** The MILLIMETERS. */
		MILLIMETERS(gov.nih.mipav.model.file.FileInfoBase.MILLIMETERS),
		/** The METERS. */
		METERS(gov.nih.mipav.model.file.FileInfoBase.METERS),
		/** The KILOMETERS. */
		KILOMETERS(gov.nih.mipav.model.file.FileInfoBase.KILOMETERS),
		/** The MILES. */
		MILES(gov.nih.mipav.model.file.FileInfoBase.MILES),
		/** The NANOSEC. */
		NANOSEC(gov.nih.mipav.model.file.FileInfoBase.NANOSEC),
		/** The MICROSEC. */
		MICROSEC(gov.nih.mipav.model.file.FileInfoBase.MICROSEC),
		/** The MILLISEC. */
		MILLISEC(gov.nih.mipav.model.file.FileInfoBase.MILLISEC),
		/** The SECONDS. */
		SECONDS(gov.nih.mipav.model.file.FileInfoBase.SECONDS),
		/** The MINUTES. */
		MINUTES(gov.nih.mipav.model.file.FileInfoBase.MINUTES),
		/** The HOURS. */
		HOURS(gov.nih.mipav.model.file.FileInfoBase.HOURS),
		/** The HZ. */
		HZ(gov.nih.mipav.model.file.FileInfoBase.HZ),
		/** The PPM. */
		PPM(gov.nih.mipav.model.file.FileInfoBase.PPM),
		/** The RADS. */
		RADS(gov.nih.mipav.model.file.FileInfoBase.RADS),
		/** The DEGREES. */
		DEGREES(gov.nih.mipav.model.file.FileInfoBase.DEGREES);

		private final int code;
		MeasurementUnit(int c) {code =c;};

		private int mipavCode() {return code;}

		public static MeasurementUnit lookupMipavCode(int i) {
			for (MeasurementUnit p : MeasurementUnit.values()) {
				if(i==p.mipavCode())
					return p;
			}
			return null;
		};

	}

	/** The Constant measurementUnitNames. */
	public static final String[] measurementUnitNames={"NULL","UNKNOWN","INCHES","CENTIMETERS","ANGSTROMS","NANOMETERS","MICROMETERS","MILLIMETERS","METERS","KILOMETERS","MILES","NANOSEC","MICROSEC","MILLISEC","SECONDS","MINUTES","HOURS","HZ","PPM","RADS","DEGREES"};

	/**
	 * The Enum ImageModality.
	 */
	public enum ImageModality {
		/** The UNKNOWN. */
		UNKNOWN,
		/** The BIOMAGENETI c_ imaging. */
		BIOMAGENETIC_IMAGING,
		/** The COLO r_ flo w_ doppler. */
		COLOR_FLOW_DOPPLER, 
		/** The COMPUTE d_ radiography. */
		COMPUTED_RADIOGRAPHY,
		/** The COMPUTE d_ tomography. */
		COMPUTED_TOMOGRAPHY,
		/** The DUPLE x_ doppler. */
		DUPLEX_DOPPLER, 
		/** The DIAPHANOGRAPHY. */
		DIAPHANOGRAPHY,
		/** The DIGITA l_ radiography. */
		DIGITAL_RADIOGRAPHY,
		/** The ENDOSCOPY. */
		ENDOSCOPY,
		/** The GENERA l_ microscopy. */
		GENERAL_MICROSCOPY,
		/** The HARDCODY. */
		HARDCODY,
		/** The INTRAORA l_ radiography. */
		INTRAORAL_RADIOGRAPHY,
		/** The LASE r_ surfac e_ scan. */
		LASER_SURFACE_SCAN,
		/** The MAGNETI c_ resonanc e_ angiography. */
		MAGNETIC_RESONANCE_ANGIOGRAPHY, 
		/** The MAMMOGRAPHY. */
		MAMMOGRAPHY, 
		/** The MAGNETI c_ resonance. */
		MAGNETIC_RESONANCE,
		/** The MAGNETI c_ resonanc e_ spectroscopy. */
		MAGNETIC_RESONANCE_SPECTROSCOPY ,
		/** The NUCLEA r_ medicine. */
		NUCLEAR_MEDICINE, 
		/** The OTHER. */
		OTHER, 
		/** The POSITRO n_ emissio n_ tomography. */
		POSITRON_EMISSION_TOMOGRAPHY, 
		/** The PANORAMI c_ xray. */
		PANORAMIC_XRAY, 
		/** The RADI o_ fluoroscopy. */
		RADIO_FLUOROSCOPY,
		/** The RADIOGRAPHI c_ imaging. */
		RADIOGRAPHIC_IMAGING,
		/** The RADIOTHERAP y_ dose. */
		RADIOTHERAPY_DOSE , 
		/** The RADIOTHERAP y_ image. */
		RADIOTHERAPY_IMAGE,
		/** The RADIOTHERAP y_ plan. */
		RADIOTHERAPY_PLAN ,
		/** The RADIOTHERAP y_ record. */
		RADIOTHERAPY_RECORD , 
		/** The RADIOTHERAP y_ structur e_ set. */
		RADIOTHERAPY_STRUCTURE_SET, 
		/** The SLID e_ microscopy. */
		SLIDE_MICROSCOPY, 
		/** The SINGL e_ photo n_ emissio n_ compute d_ tomography. */
		SINGLE_PHOTON_EMISSION_COMPUTED_TOMOGRAPHY, 
		/** The THERMOGRAPHY. */
		THERMOGRAPHY ,
		/** The ULTRASOUND. */
		ULTRASOUND , 
		/** The XRA y_ angiography. */
		XRAY_ANGIOGRAPHY, 
		/** The EXTERNA l_ camer a_ photography. */
		EXTERNAL_CAMERA_PHOTOGRAPHY,
		/** The RE d_ free. */
		RED_FREE,
		/** The FA. */
		FA}

	/**
	 * The Enum AxisOrientation.
	 */
	public enum AxisOrientation{ 
		/** The UNKNOWN. */
		UNKNOWN,
		/** The R2 l_ type. */
		R2L_TYPE , 
		/** The L2 r_ type. */
		L2R_TYPE, 
		/** The P2 a_ type. */
		P2A_TYPE,
		/** The A2 p_ type. */
		A2P_TYPE,
		/** The I2 s_ type. */
		I2S_TYPE , 
		/** The S2 i_ type. */
		S2I_TYPE;

		static AxisOrientation correctForDirection(AxisOrientation in, int direction) {
			if(true)
				return in; 
			else {
				switch(in) {
				/** The UNKNOWN. */
				case UNKNOWN: 
					return UNKNOWN;
					/** The R2 l_ type. */
				case R2L_TYPE:
					if(direction<0)
						return L2R_TYPE;
					else 
						return R2L_TYPE;
					/** The L2 r_ type. */
				case L2R_TYPE:
					if(direction>0)
						return L2R_TYPE;
					else 
						return R2L_TYPE;
					/** The P2 a_ type. */
				case P2A_TYPE:
					if(direction<0)
						return A2P_TYPE;
					else 
						return P2A_TYPE;

					/** The A2 p_ type. */
				case A2P_TYPE:
					if(direction>0)
						return A2P_TYPE;
					else 
						return P2A_TYPE;
					/** The I2 s_ type. */
				case I2S_TYPE:
					if(direction<0)
						return S2I_TYPE;
					else 
						return I2S_TYPE;

					/** The S2 i_ type. */
				case S2I_TYPE:
					if(direction>0)
						return S2I_TYPE;
					else 
						return I2S_TYPE;
				default: 
					return UNKNOWN;

				}
			}
		}
	};

	/** The Constant axisOrientationNames. */
	public static final String[] axisOrientationNames={"Unknown","Right to Left","Left to Right","Posterior to Anterior","Anterior to Posterior","Inferior to Superior","Superior to Inferior"};

	/**
	 * The Enum ImageOrientation.
	 */
	public enum ImageOrientation{
		/** The AXIAL. */
		AXIAL,
		/** The CORONAL. */
		CORONAL,
		/** The SAGITTAL. */
		SAGITTAL,
		/** The UNKNOWN. */
		UNKNOWN};

		/** The Constant imageOrientationNames. */
		public static final String[] imageOrientationNames={"Axial","Coronal","Sagittal","Unkown"};

		/**
		 * The Enum Compression.
		 */
		public enum Compression{
			/** The NONE. */
			NONE,
			/** The ZIP. */
			ZIP,
			/** The GZIP. */
			GZIP};

			/**
			 * The Enum Endianess.
			 */
			public enum Endianess{
				/** The LITTLE. */
				LITTLE,
				/** The BIG. */
				BIG};

				/** The axis orientation. */
				protected AxisOrientation[] axisOrientation;

				/** The image orientation. */
				protected ImageOrientation imageOrientation;

				/** The modality. */
				protected ImageModality modality;

				/** The origin. */
				protected float[] origin;

				/** The dim resolutions. */
				protected float[] dimResolutions;

				/** The units of measure. */
				protected MeasurementUnit[] unitsOfMeasure;

				/** The compression type. */
				protected Compression compressionType;

				/** The endianess. */
				protected Endianess endianess;

				/** The slice thickness. */
				protected float sliceThickness ;

				/** The default header. */
				protected boolean defaultHeader;

				/**
				 * Instantiates a new image header.
				 */
				public ImageHeader(){
					axisOrientation=new AxisOrientation[]{AxisOrientation.UNKNOWN,AxisOrientation.UNKNOWN,AxisOrientation.UNKNOWN};
					imageOrientation=ImageOrientation.UNKNOWN;
					modality=ImageModality.UNKNOWN;
					origin=new float[]{0,0,0};
					dimResolutions=new float[]{1.0f,1.0f,1.0f,1.0f,1.0f};
					unitsOfMeasure=new MeasurementUnit[]{MeasurementUnit.MILLIMETERS, MeasurementUnit.MILLIMETERS, MeasurementUnit.MILLIMETERS, MeasurementUnit.SECONDS, MeasurementUnit.UNKNOWN_MEASURE};
					compressionType=Compression.NONE;
					endianess=Endianess.LITTLE;
					sliceThickness=0;
					defaultHeader = true;
				}

				/**
				 * Instantiates a new image header.
				 * 
				 * @param fileInfo the file info
				 */
				public ImageHeader(FileInfoBase fileInfo){
					int[] field;
					field=fileInfo.getAxisOrientation();
					axisOrientation=new AxisOrientation[3];
					if(field!=null){
						axisOrientation[0]=AxisOrientation.correctForDirection(AxisOrientation.values()[field[0]],fileInfo.getAxisDirection()[0]);
						axisOrientation[1]=AxisOrientation.correctForDirection(AxisOrientation.values()[field[1]],fileInfo.getAxisDirection()[0]);
						axisOrientation[2]=AxisOrientation.correctForDirection(AxisOrientation.values()[field[2]],fileInfo.getAxisDirection()[0]);
					}
					imageOrientation=ImageOrientation.values()[fileInfo.getImageOrientation()];
					modality=ImageModality.values()[fileInfo.getModality()];

					if(fileInfo.getOrigin()!=null)origin=Arrays.copyOf(fileInfo.getOrigin(),fileInfo.getOrigin().length);
					dimResolutions=Arrays.copyOf(fileInfo.getResolutions(),fileInfo.getResolutions().length);
					int[] units=fileInfo.getUnitsOfMeasure();
					unitsOfMeasure=new MeasurementUnit[units.length];
					for(int i=0;i<units.length;i++){
						unitsOfMeasure[i]=MeasurementUnit.lookupMipavCode(units[i]);
					}
					compressionType=Compression.values()[fileInfo.getCompressionType()];
					endianess=(fileInfo.getEndianess())?Endianess.BIG:Endianess.LITTLE;
					sliceThickness=fileInfo.getSliceThickness();
					defaultHeader = false;
				}

				/**
				 * Copy all info to.
				 * 
				 * @param fileInfo the file info
				 */
				public void copyAllInfoTo(FileInfoBase fileInfo){
					fileInfo.setAxisOrientation(new int[]{axisOrientation[0].ordinal(),axisOrientation[1].ordinal(),axisOrientation[2].ordinal()});
					//					fileInfo.setAxisOrientation(new int[]{axisOrientation[0].ordinal(),axisOrientation[1].ordinal(),axisOrientation[2].ordinal()});
					fileInfo.setImageOrientation(imageOrientation.ordinal());
					fileInfo.setModality(modality.ordinal());		
					fileInfo.setCompressionType(compressionType.ordinal());
					fileInfo.setEndianess((endianess==Endianess.BIG));
					fileInfo.setSliceThickness(sliceThickness);

					float []ori = fileInfo.getOrigin();
					for(int i=0;i<ori.length && i<origin.length;i++)
						fileInfo.setOrigin(origin[i],i);
					float []res = fileInfo.getResolutions();
					for(int i=0;i<res.length && i<dimResolutions.length;i++)
						fileInfo.setResolutions(dimResolutions[i],i);
					int[] units=new int[fileInfo.getUnitsOfMeasure().length];
					for(int i=0;i<units.length && i<unitsOfMeasure.length;i++){
						units[i]=unitsOfMeasure[i].mipavCode();
					}
				}

				/**
				 * Copy basic info to.
				 * 
				 * @param fileInfo the file info
				 */
				public void copyBasicInfoTo(FileInfoBase fileInfo){
					fileInfo.setAxisOrientation(new int[]{axisOrientation[0].ordinal(),axisOrientation[1].ordinal(),axisOrientation[2].ordinal()});
					fileInfo.setImageOrientation(imageOrientation.ordinal());
					fileInfo.setModality(modality.ordinal());
					fileInfo.setSliceThickness(sliceThickness);
					float []ori = fileInfo.getOrigin();
					for(int i=0;i<ori.length && i<origin.length;i++)
						fileInfo.setOrigin(origin[i],i);
					float []res = fileInfo.getResolutions();
					for(int i=0;i<res.length && i<dimResolutions.length;i++)
						fileInfo.setResolutions(dimResolutions[i],i);
					int[] units=new int[fileInfo.getUnitsOfMeasure().length];
					for(int i=0;i<units.length && i<unitsOfMeasure.length;i++){
						units[i]=unitsOfMeasure[i].mipavCode();
					}
					fileInfo.setUnitsOfMeasure(units);

					//		defaultHeader = false;
				}

				/* (non-Javadoc)
				 * @see java.lang.Object#clone()
				 */
				public ImageHeader clone(){
					ImageHeader header=new ImageHeader();
					header.axisOrientation=new AxisOrientation[3];
					header.axisOrientation[0]=axisOrientation[0];
					header.axisOrientation[1]=axisOrientation[1];
					header.axisOrientation[2]=axisOrientation[2];
					header.imageOrientation=imageOrientation;
					header.modality=modality;
					header.origin=origin;
					header.dimResolutions=Arrays.copyOf(dimResolutions,dimResolutions.length);
					header.unitsOfMeasure=Arrays.copyOf(unitsOfMeasure,unitsOfMeasure.length);
					header.compressionType=compressionType;
					header.endianess=endianess;
					header.sliceThickness=sliceThickness;
					header.defaultHeader = defaultHeader;
					return header;
				}

				/**
				 * Gets the axis orientation.
				 * 
				 * @return the axisOrientation
				 */
				public AxisOrientation[] getAxisOrientation() {
					return axisOrientation;
				}

				/**
				 * Sets the axis orientation.
				 * 
				 * @param axisOrientation the axisOrientation to set
				 */
				public void setAxisOrientation(AxisOrientation[] axisOrientation) {
					this.axisOrientation = axisOrientation;
					defaultHeader=false;
				}

				/**
				 * Gets the image orientation.
				 * 
				 * @return the imageOrientation
				 */
				public ImageOrientation getImageOrientation() {
					return imageOrientation;
				}

				/**
				 * Sets the image orientation.
				 * 
				 * @param imageOrientation the imageOrientation to set
				 */
				public void setImageOrientation(ImageOrientation imageOrientation) {
					defaultHeader=false;
					this.imageOrientation = imageOrientation;
				}

				/**
				 * Gets the modality.
				 * 
				 * @return the modality
				 */
				public ImageModality getModality() {
					return modality;
				}

				/**
				 * Sets the modality.
				 * 
				 * @param modality the modality to set
				 */
				public void setModality(ImageModality modality) {
					defaultHeader=false;
					this.modality = modality;
				}

				/**
				 * Gets the origin.
				 * 
				 * @return the origin
				 */
				public float[] getOrigin() {
					return origin;
				}

				/**
				 * Sets the origin.
				 * 
				 * @param origin the origin to set
				 */
				public void setOrigin(float[] origin) {
					defaultHeader=false;
					this.origin = origin;
				}

				/**
				 * Gets the dim resolutions.
				 * 
				 * @return the dimResolutions
				 */
				public float[] getDimResolutions() {
					return dimResolutions;
				}

				/**
				 * Sets the dim resolutions.
				 * 
				 * @param dimResolutions the dimResolutions to set
				 */
				public void setDimResolutions(float[] dimResolutions) {
					defaultHeader=false;
					this.dimResolutions = dimResolutions;
				}

				/**
				 * Gets the units of measure.
				 * 
				 * @return the unitsOfMeasure
				 */
				public MeasurementUnit[] getUnitsOfMeasure() {
					return unitsOfMeasure;
				}

				/**
				 * Sets the units of measure.
				 * 
				 * @param unitsOfMeasure the unitsOfMeasure to set
				 */
				public void setUnitsOfMeasure(MeasurementUnit[] unitsOfMeasure) {
					defaultHeader=false;
					this.unitsOfMeasure = unitsOfMeasure;
				}

				/**
				 * Gets the compression type.
				 * 
				 * @return the compressionType
				 */
				public Compression getCompressionType() {
					return compressionType;
				}

				/**
				 * Sets the compression type.
				 * 
				 * @param compressionType the compressionType to set
				 */
				public void setCompressionType(Compression compressionType) {
					defaultHeader=false;
					this.compressionType = compressionType;
				}

				/**
				 * Gets the endianess.
				 * 
				 * @return the endianess
				 */
				public Endianess getEndianess() {
					return endianess;
				}

				/**
				 * Sets the endianess.
				 * 
				 * @param endianess the endianess to set
				 */
				public void setEndianess(Endianess endianess) {
					defaultHeader=false;
					this.endianess = endianess;
				}

				/**
				 * Gets the slice thickness.
				 * 
				 * @return the sliceThickness
				 */
				public float getSliceThickness() {
					return sliceThickness;
				}

				/**
				 * Sets the slice thickness.
				 * 
				 * @param sliceThickness the sliceThickness to set
				 */
				public void setSliceThickness(float sliceThickness) {
					defaultHeader=false;
					this.sliceThickness = sliceThickness;
				}

				/**
				 * Checks if is default header.
				 * 
				 * @return true, if is default header
				 */
				public boolean isDefaultHeader() {
					return defaultHeader;	
				}

				/**
				 * Copy geometry from one image header into this one. Does not alter image dimensions.
				 * 
				 * @param copyme the header from which to copy data
				 */
				public void copyGeometry(ImageHeader copyme) {
					setAxisOrientation(copyme.getAxisOrientation());
					setDimResolutions(copyme.getDimResolutions());
					setImageOrientation(copyme.getImageOrientation());
					setOrigin(copyme.getOrigin());
					setSliceThickness(copyme.getSliceThickness());
					setUnitsOfMeasure(copyme.getUnitsOfMeasure());

				}

				/**
				 * NOTE: This method is a stub.
				 * @param hdr - Image header to compare current header with
				 * @return true - geometries are equivalent. false - they are not equal
				 */
				public boolean hasComparableGeometry(ImageHeader hdr) {
					for(int i=0;i<dimResolutions.length;i++) {
						if(dimResolutions[i]!=hdr.dimResolutions[i])
							return false;		
						if(origin[i]!=hdr.origin[i])
							return false;
					}
					if(getSliceThickness()!=hdr.getSliceThickness())
						return false;	
					if(axisOrientation!=hdr.axisOrientation)
						return false;
					if(imageOrientation!=hdr.imageOrientation)
						return false;
					for(int i=0;i<unitsOfMeasure.length;i++)
						if(unitsOfMeasure[i]!=hdr.unitsOfMeasure[i])
							return false;
					return true;
				}


				/**
				 * Compares the orientation of this header with another header.
				 * @param hdr
				 * @return true - orientations are the same. false otherwise.
				 */
				public boolean isSameOrientation(ImageHeader hdr){
					int thisnumdims = this.dimResolutions.length;
					int tgtnumdims = hdr.dimResolutions.length;
					if(thisnumdims != tgtnumdims){
						return false;
					}
					for(int i=0; i<thisnumdims; i++){
						if(this.axisOrientation[i]!=hdr.getAxisOrientation()[i]){
							return false;
						}
					}
					return true;
				}

				/**
				 * Compares the resolution of this header with another header.
				 * @param hdr
				 * @return true - resolutions are the same. false otherwise.
				 */
				public boolean isSameResolution(ImageHeader hdr){
					int thisnumdims = this.dimResolutions.length;
					int tgtnumdims = hdr.dimResolutions.length;
					if(thisnumdims != tgtnumdims){
						return false;
					}
					for(int i=0; i<thisnumdims; i++){
						if(this.dimResolutions[i]!=hdr.getDimResolutions()[i]){
							return false;
						}
					}
					return true;
				}

}
