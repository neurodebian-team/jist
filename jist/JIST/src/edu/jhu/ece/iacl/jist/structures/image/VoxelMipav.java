package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;
import java.text.NumberFormat;

import edu.jhu.ece.iacl.jist.structures.data.Indexable;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.ModelStorageBase;

// TODO: Auto-generated Javadoc
/**
 * An adapter for each element of ModelImage in Mipav so that data does not have
 * to be copied to and from the Model Image class.
 * 
 * @author Blake Lucas
 */
public class VoxelMipav extends Voxel implements Indexable<Voxel> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The img. */
	private ModelStorageBase img;

	/** The position. */
	private int position;

	/** The x. */
	private int x;

	/** The y. */
	private int y;

	/** The z. */
	private int z;
	
	/** The chain. */
	private int chain;
	
	/** The index. */
	private int index;

	/** The type. */
	protected VoxelType type;

	/** The vox. */
	protected Voxel vox = null;

	/**
	 * Instantiates a new voxel mipav.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param img the img
	 * @param type the type
	 */
	public VoxelMipav(int x, int y, int z, ModelStorageBase img, VoxelType type) {
		int dimExtents[] = img.getExtents();
		position = (z * (dimExtents[0] * dimExtents[1])) + (y * dimExtents[0])
				+ x;
		this.img = img;
		this.type = type;
		this.x = x;
		this.y = y;
		this.z = z;

	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getBoolean()
	 */
	@Override
	public boolean getBoolean() {
		return img.getBoolean(position);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getShort()
	 */
	@Override
	public short getShort() {
		if (type == VoxelType.UBYTE)
			return getUByte();
		return img.getShort(position);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getUByte()
	 */
	public short getUByte() {
		return img.getUByte(position);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getFloat()
	 */
	public float getFloat() {
		if (type == VoxelType.UBYTE)
			return getUByte();
		return img.getFloat(position);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getColor()
	 */
	@Override
	public Color getColor() {
		return new Color((int) img.getUByte(position), (int) img
				.getUByte(position + 1), (int) img.getUByte(position + 2),
				(int) img.getUByte(position + 3));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getDouble()
	 */
	@Override
	public double getDouble() {
		if (type == VoxelType.UBYTE) {
			return getUByte();
		}
		return img.getDouble(position);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getInt()
	 */
	@Override
	public int getInt() {
		if (type == VoxelType.UBYTE)
			return getUByte();
		return img.getInt(position);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#clone()
	 */
	@Override
	public Voxel clone() {
		return new VoxelMipav(x, y, z, img, type);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	@Override
	public void set(Voxel v) {
		img.set(position, v);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(boolean)
	 */
	@Override
	public void set(boolean vox) {
		img.set(position, vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(short)
	 */
	@Override
	public void set(short vox) {
		img.set(position, vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(int)
	 */
	@Override
	public void set(int vox) {
		img.set(position, vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(java.awt.Color)
	 */
	@Override
	public void set(Color vox) {
		img.set(position, (byte) vox.getRed());
		img.set(position + 1, (byte) vox.getGreen());
		img.set(position + 2, (byte) vox.getBlue());
		img.set(position + 3, (byte) vox.getAlpha());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(double)
	 */
	@Override
	public void set(double vox) {
		img.set(position, vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#add(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	@Override
	public Voxel add(Voxel v) {
		return createVoxel().add(v);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#sub(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	@Override
	public Voxel sub(Voxel v) {
		return createVoxel().sub(v);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#mul(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	@Override
	public Voxel mul(Voxel v) {
		return createVoxel().mul(v);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#div(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	@Override
	public Voxel div(Voxel v) {
		return createVoxel().div(v);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#neg()
	 */
	@Override
	public Voxel neg() {
		return createVoxel().neg();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#toString()
	 */
	@Override
	public String toString() {
		NumberFormat format = NumberFormat.getNumberInstance();
		format.setMaximumFractionDigits(5);
		format.setMinimumFractionDigits(5);
		double d = img.getDouble(position);
		if (Math.round(d) == d)
			return (long) d + "";
		else
			return (format.format(d));
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Voxel arg0) {
		return createVoxel().compareTo(arg0);
	}

	/**
	 * Creates the voxel.
	 * 
	 * @return the voxel
	 */
	private Voxel createVoxel() {
		if (vox == null) {
			switch (img.getType()) {
			case ModelImage.ARGB:
				vox = new VoxelColor(getColor());
				break;
			case ModelImage.W_LOCKED:
				vox = new VoxelUByte(getUByte());
				break;
			case ModelImage.BYTE:
				vox = new VoxelUByte(getUByte());
				break;
			case ModelImage.SHORT:
				vox = new VoxelShort(getShort());
				break;
			case ModelImage.DOUBLE:
				vox = new VoxelDouble(getDouble());
				break;
			case ModelImage.BOOLEAN:
				vox = new VoxelBoolean(getBoolean());
				break;
			case ModelImage.INTEGER:
				vox = new VoxelInt(getInt());
				break;
			case ModelImage.FLOAT:
				vox = new VoxelFloat(getFloat());
				break;
			default:
				vox = new VoxelDouble(getDouble());
			}
		} else {
			switch (img.getType()) {
			case ModelImage.ARGB:
				vox.set(getColor());
				break;
			case ModelImage.W_LOCKED:
				vox.set(getUByte());
				break;
			case ModelImage.BYTE:
				vox.set(getUByte());
				break;
			case ModelImage.SHORT:
				vox.set(getShort());
				break;
			case ModelImage.DOUBLE:
				vox.set(getDouble());
				break;
			case ModelImage.BOOLEAN:
				vox.set(getBoolean());
				break;
			case ModelImage.INTEGER:
				vox.set(getInt());
				break;
			case ModelImage.FLOAT:
				vox.set(getFloat());
				break;
			default:
				vox.set(getDouble());
			}
		}
		return vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setRefPosition(int, int, int)
	 */
	public void setRefPosition(int i, int j, int k) {
		x = i;
		y = j;
		z = k;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getRow()
	 */
	public int getRow() {
		return x;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getColumn()
	 */
	public int getColumn() {
		return y;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getSlice()
	 */
	public int getSlice() {
		return z;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setIndex(int)
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getIndex()
	 */
	public int getIndex() {
		return index;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getType()
	 */
	public VoxelType getType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getChainIndex()
	 */
	public int getChainIndex() {
		return chain;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setChainIndex(int)
	 */
	public void setChainIndex(int chainIndex) {
		chain=chainIndex;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getValue()
	 */
	public Comparable getValue() {
		return this.getDouble();
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setValue(java.lang.Comparable)
	 */
	public void setValue(Comparable obj) {
		if(obj instanceof Double){
			set((Double)obj);
		} else if(obj instanceof Integer){
			set((Integer)obj);
		} else if(obj instanceof Byte){
			set((Byte)obj);
		} else if(obj instanceof Color){
			set((Color)obj);
		} else if(obj instanceof Voxel){
			set((Voxel)obj);
		} else if(obj instanceof Short){
			set((Short)obj);
		}
	}
}
