/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;

/**
 * Module cell to display on graph.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public abstract class PipeModuleCell extends DefaultGraphCell {
	
	/** The Constant defaultSize. */
	protected static final Dimension defaultSize = new Dimension(150, 70);
	
	/** The Constant hspace. */
	protected static final int hspace = 14;

	/**
	 * Get preferred size for module as a function of the number of incoming and
	 * outgoing ports.
	 * 
	 * @param numInput
	 *            number of incoming ports
	 * @param numOutput
	 *            number of outgoing ports
	 * @return the preferred size
	 */
	public static Dimension getPreferredSize(int numInput, int numOutput) {
		return new Dimension((int) Math.max(numOutput * hspace, Math.max(defaultSize.getWidth(), numInput * hspace)),
				(int) defaultSize.getHeight());
	}

	/**
	 * Get preferred size for module as a function of the input and output
	 * parameters.
	 * 
	 * @param inputParams
	 *            input parameters
	 * @param outputParams
	 *            output parameters
	 * @return the preferred size
	 */
	public static Dimension getPreferredSize(ParamCollection inputParams, ParamCollection outputParams) {
		int inSize = 0;
		for (PipePort mod : inputParams.getAllDescendants()) {
			if (mod.isConnectible()) {
				inSize++;
			}
		}
		int outSize = 0;
		for (PipePort mod : outputParams.getAllDescendants()) {
			if (mod.isConnectible()) {
				outSize++;
			}
		}
		return getPreferredSize(inSize, outSize);
	}

	/** The pipe. */
	protected PipeModule pipe;
	
	/** The preferred size. */
	protected Dimension preferredSize = new Dimension(150, 70);

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            name of module
	 * @param pipe
	 *            module
	 */
	public PipeModuleCell(String name, PipeModule pipe) {
		super(name);
		this.pipe = pipe;
		pipe.setModuleCell(this);
		this.getAttributes().applyMap(createCellAttributes());
		createPorts();
	}

	/**
	 * Create cell attributes that are default with all cells.
	 * 
	 * @return the map
	 */
	protected Map createCellAttributes() {
		Map map = new Hashtable();
		// Add a Bounds Attribute to the Map
		// Make sure the cell is resized on insert
		GraphConstants.setResize(map, false);
		GraphConstants.setSizeable(map, false);
		// Add a nice looking gradient background
		GraphConstants.setGradientColor(map, Color.red);
		// Add a Border Color Attribute to the Map
		GraphConstants.setBorderColor(map, Color.black);
		// Add a White Background
		GraphConstants.setBackground(map, Color.white);
		GraphConstants.setForeground(map, Color.black);
		// Make Vertex Opaque
		GraphConstants.setOpaque(map, true);
		GraphConstants.setRouting(map, new AvoidanceRouting());
		return map;
	}

	/**
	 * Create ports for module.
	 */
	protected void createPorts() {
		Vector<PipePort> inputPorts = pipe.getInputPorts();
		Vector<PipePort> outputPorts = pipe.getOutputPorts();
		int inSize = 0;
		for (PipePort mod : inputPorts) {
			if (mod.isConnectible()) {
				inSize++;
			}
		}
		int outSize = 0;
		for (PipePort mod : outputPorts) {
			if (mod.isConnectible()) {
				outSize++;
			}
		}
		preferredSize = getPreferredSize(inSize, outSize);
		int i = 0;
		for (PipePort port : inputPorts) {
			if (port.isConnectible()) {
				add(new PipeModulePort(GraphConstants.PERMILLE * (i + 0.5) / inSize, 0.10 * GraphConstants.PERMILLE,
						port));
				i++;
			}
		}
		i = 0;
		for (PipePort port : outputPorts) {
			if (port.isConnectible()) {
				add(new PipeModulePort(GraphConstants.PERMILLE * (i + 0.5) / outSize, 0.93 * GraphConstants.PERMILLE,
						port));
				i++;
			}
		}
	}

	/**
	 * Module class that is stored in this cell.
	 * 
	 * @return the module class
	 */
	public Class getModuleClass() {
		return (pipe != null) ? pipe.getClass() : null;
	}

	/**
	 * Get pipe module that is stored in this cell.
	 * 
	 * @return the pipe module
	 */
	public PipeModule getPipeModule() {
		return pipe;
	}

	/**
	 * Set the size of this module.
	 * 
	 * @param bounds
	 *            dimensions
	 */
	public void setBounds(Rectangle2D bounds) {
		GraphConstants.setBounds(this.getAttributes(), bounds);
		pipe.setBounds(bounds);
	}

	/**
	 * Set the location of the module.
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 */
	public void setLocation(int x, int y) {
		Rectangle2D bounds = new Rectangle2D.Double(x, y, preferredSize.getWidth(), preferredSize.getHeight());
		GraphConstants.setBounds(this.getAttributes(), bounds);
		setBounds(bounds);
	}
}
