package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;

// TODO: Auto-generated Javadoc
/**
 * Color Voxel Type.
 * 
 * @author Blake Lucas
 */
public class VoxelColor extends Voxel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3030644025869797985L;

	/** The vox. */
	private Color vox;

	/** The hsb. */
	float[] hsb = new float[3];

	/**
	 * Convert.
	 */
	private void convert() {
		hsb = Color.RGBtoHSB(vox.getRed(), vox.getGreen(), vox.getBlue(), hsb);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getBoolean()
	 */
	public boolean getBoolean() {
		return !vox.equals(Color.BLACK);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getShort()
	 */
	public short getShort() {
		convert();
		return (short) Math.round(255.0 * hsb[2]);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getColor()
	 */
	public Color getColor() {
		return vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getDouble()
	 */
	public double getDouble() {
		convert();
		return hsb[2];
	}

	/**
	 * Instantiates a new voxel color.
	 * 
	 * @param c the c
	 */
	public VoxelColor(Color c) {
		set(c);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#clone()
	 */
	public Voxel clone() {
		return new VoxelColor(vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public void set(Voxel v) {
		vox = v.getColor();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(boolean)
	 */
	public void set(boolean vox) {
		this.vox = (vox) ? Color.WHITE : Color.BLACK;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(short)
	 */
	public void set(short vox) {
		this.vox = new Color(vox, vox, vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(java.awt.Color)
	 */
	public void set(Color vox) {
		this.vox = vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(double)
	 */
	public void set(double vox) {
		this.vox = Color.getHSBColor(1.0f, 1.0f, (float) vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getInt()
	 */
	public int getInt() {
		convert();
		return (int) Math.round(255 * hsb[2]);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(int)
	 */
	public void set(int vox) {
		this.vox = new Color(vox, vox, vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#add(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelColor add(Voxel v) {
		Color c2 = v.getColor();
		return new VoxelColor(new Color(Math.min(255, vox.getRed()
				+ c2.getRed()), Math.min(255, vox.getGreen() + c2.getGreen()),
				Math.min(255, vox.getBlue() + c2.getBlue())));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#sub(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public Voxel sub(Voxel v) {
		Color c2 = v.getColor();
		return new VoxelColor(new Color(
				Math.max(0, vox.getRed() - c2.getRed()), Math.max(0, vox
						.getGreen()
						- c2.getGreen()), Math.max(0, vox.getBlue()
						- c2.getBlue())));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#mul(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public Voxel mul(Voxel v) {
		Color c2 = v.getColor();
		return new VoxelColor(new Color(Math.min(255, vox.getRed()
				* c2.getRed() / 255), Math.min(255, vox.getGreen()
				* c2.getGreen() / 255), Math.min(255, vox.getBlue()
				* c2.getBlue() / 255)));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#div(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public Voxel div(Voxel v) {
		Color c2 = v.getColor();
		return new VoxelColor(new Color(Math.max(0, (255 * vox.getRed())
				/ c2.getRed()), Math.max(0, (255 * vox.getGreen())
				/ c2.getGreen()), Math.max(0, (255 * vox.getBlue())
				/ c2.getBlue())));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#neg()
	 */
	public Voxel neg() {
		return new VoxelColor(new Color(255 - vox.getRed(), 255 - vox
				.getGreen(), 255 - vox.getBlue()));
	}

	/**
	 * Instantiates a new voxel color.
	 */
	public VoxelColor() {
		this.vox = Color.BLACK;
	}

	/**
	 * Instantiates a new voxel color.
	 * 
	 * @param v the v
	 */
	public VoxelColor(Voxel v) {
		set(v);
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Voxel obj) {
		return (int) (this.getShort() - ((Voxel) obj).getShort());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#toString()
	 */
	public String toString() {
		return ("[" + vox.getRed() + "," + vox.getGreen() + "," + vox.getBlue() + "]");
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getType()
	 */
	public VoxelType getType() {
		return VoxelType.COLOR;
	}
}
