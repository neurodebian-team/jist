package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;


public class PipeVolumeCollectionExternalSource extends PipeExternalSource{
	public PipeVolumeCollectionExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamVolumeCollection("Default Volumes"));
		group.setLabel("External Volumes");
		group.setName("extvols");
		group.setCategory("Externalize.Volume");
		return group;
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volumes");
		group.add(valParam = new ParamVolumeCollection("Volumes"));
		return group;
	}
}
