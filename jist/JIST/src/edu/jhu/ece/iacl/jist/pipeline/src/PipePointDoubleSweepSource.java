/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import javax.vecmath.Point3d;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Sweep through range of double values.
 * 
 * @author Blake Lucas
 */
public class PipePointDoubleSweepSource extends PipeSource {
	
	/** The start. */
	transient protected Point3d start;
	
	/** The end. */
	transient protected Point3d end;
	
	/** The inc. */
	transient protected Point3d inc;
	
	/** The val. */
	transient protected Point3d val;
	
	/** The val param. */
	protected ParamPointDouble startParam, endParam, incParam, valParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("startParam");
//		startParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("endParam");
//		endParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("incParam");
//		incParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("valParam");
//		valParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		startParam = (ParamPointDouble) inputParams.getFirstChildByName("Start Value");
		endParam = (ParamPointDouble) inputParams.getFirstChildByName("End Value");
		incParam = (ParamPointDouble) inputParams.getFirstChildByName("Increment");
		valParam = (ParamPointDouble) outputParams.getFirstChildByName("Float");

//		startParam = new ParamPointDouble();
//		startParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"startParam"));
//		endParam = new ParamPointDouble();
//		endParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"endParam"));
//		incParam = new ParamPointDouble();
//		incParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"incParam"));
//		valParam = new ParamPointDouble();
//		valParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"valParam"));
		inc=new Point3d();
		getParentPort().setParameter(valParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipePointDoubleSweepSource() {
		super();
		inc=new Point3d();
		getParentPort().setParameter(valParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Point Sweep");
		group.setName("pointsweep");
		group.add(startParam = new ParamPointDouble("Start Value"));
		group.add(endParam = new ParamPointDouble("End Value"));
		group.add(incParam = new ParamPointDouble("Increment"));
		group.setCategory("Point");
		return group;
	}

	/**
	 * Create ouptut parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float");
		group.add(valParam = new ParamPointDouble("Float"));
		group.setCategory("Point");
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamModel getOutputParam() {
		return valParam;
	}

	/**
	 * Return true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() 
				|| (((inc.x > 0) && (val.x <= end.x)) || ((inc.x < 0) && (val.x >= end.x))
				&& ((inc.y > 0) && (val.y <= end.y)) || ((inc.y < 0) && (val.y >= end.y))
				&& ((inc.z > 0) && (val.z <= end.z)) || ((inc.z < 0) && (val.z >= end.z))));
	}

	/**
	 * Iterate through double values.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if ((((inc.x > 0) && (val.x <= end.x)) || ((inc.x < 0) && (val.x >= end.x)))
						&& (((inc.y > 0) && (val.y <= end.y)) || ((inc.y < 0) && (val.y >= end.y)))
						&& (((inc.z > 0) && (val.z <= end.z)) || ((inc.z < 0) && (val.z >= end.z)))) {
					valParam.setValue(val);
					push();
					val.add(inc);
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			reset();
			isReset = true;
			return false;
		}
	}

	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		start = startParam.getValue();
		end = endParam.getValue();
		inc=new Point3d();
		Point3d incAmt=incParam.getValue();
		inc.x = Math.signum(end.x - start.x) * incAmt.x;
		inc.y = Math.signum(end.y - start.y) * incAmt.y;
		inc.z = Math.signum(end.z - start.z) * incAmt.z;
		val = start;
		valParam.setValue(val);
		val.add(inc);
		push();
	}
}
