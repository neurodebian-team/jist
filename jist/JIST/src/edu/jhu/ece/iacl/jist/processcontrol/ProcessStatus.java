package edu.jhu.ece.iacl.jist.processcontrol;

// TODO: Auto-generated Javadoc
/**
 * The Enum ProcessStatus.
 */
public enum ProcessStatus {
	
	/** The READY. */
	READY, 
	
	/** The QUEUE d_ active. */
	QUEUED_ACTIVE, 
	
	/** The SYSTE m_ o n_ hold. */
	SYSTEM_ON_HOLD, 
	
	/** The USE r_ o n_ hold. */
	USER_ON_HOLD,
	
	/** The USE r_ syste m_ o n_ hold. */
	USER_SYSTEM_ON_HOLD,
	
	/** The RUNNING. */
	RUNNING, 
	
	/** The SYSTE m_ suspended. */
	SYSTEM_SUSPENDED,
	
	/** The USE r_ suspended. */
	USER_SUSPENDED,
	
	/** The USE r_ syste m_ suspended. */
	USER_SYSTEM_SUSPENDED,
	
	/** The DONE. */
	DONE,
	
	/** The FAILED. */
	FAILED,
	
	/** The UNDETERMINE. */
	UNDETERMINE
}
