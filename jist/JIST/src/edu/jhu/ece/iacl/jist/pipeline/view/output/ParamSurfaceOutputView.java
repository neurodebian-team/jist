/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.output;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Font3D;
import javax.media.j3d.FontExtrusion;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.GraphicsConfigTemplate3D;
import javax.media.j3d.LineAttributes;
import javax.media.j3d.Material;
import javax.media.j3d.OrientedShape3D;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.QuadArray;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Text3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.geom.Point3;

/**
 * Simple 3D viewer for surfaces.
 * 
 * @author Blake Lucas
 */
public class ParamSurfaceOutputView extends ParamOutputView implements MouseListener, ActionListener {
	// number of tic marks along axes
	/** The tick marks. */
	protected int tickMarks = 10;
	// canvas
	/** The canvas. */
	private Canvas3D canvas = null;
	// frame
	/** The frame. */
	private JFrame frame;
	
	/** The bounds. */
	protected double[] bounds = { -1, 1, -1, 1, -1, 1 };// set min/max X,
	// min/max Y, and
	// min/max Z for the
	// display
	/** The limits. */
	protected double[] limits = { -1, 1, -1, 1, -1, 1 };// set min/max X,
	// min/max Y, and
	// min/max Z for the
	// Graph2D
	// object center
	/** The center. */
	protected Vector3d center = new Vector3d();
	// object scale
	/** The scale. */
	protected Vector3d scale = new Vector3d();
	// axis color
	/** The axis color. */
	private Color3f axisColor = new Color3f(1.0f, 1.0f, 1.0f);
	// mesh color
	/** The mesh color. */
	private Color3f meshColor = new Color3f(0.5f, 0.0f, 0.0f);
	// light directions
	/** The light directions. */
	private Vector3f[] lightDirections = { new Vector3f(1, 0, 0), new Vector3f(0, 1, 0), new Vector3f(0, 0, 1),
			new Vector3f(-1, 0, 0), new Vector3f(0, -1, 0), new Vector3f(0, 0, -1) };
	
	/** The universe. */
	private SimpleUniverse universe;
	// mesh
	/** The mesh. */
	private GeometryArray mesh;
	
	/** The rendered. */
	boolean rendered = false;
	// button to view surface
	/** The button. */
	JButton button = new JButton();

	/**
	 * Default constructor.
	 */
	public ParamSurfaceOutputView() {
		super();
	}

	/**
	 * Default constructor.
	 * 
	 * @param surf
	 *            surface
	 * @param title
	 *            frame title
	 */
	public ParamSurfaceOutputView(GeometryArray surf, String title) {
		this();
		frame.setName(title);
		setMesh(surf);
	}

	/**
	 * Default constructor.
	 * 
	 * @param param
	 *            surface parameter
	 */
	public ParamSurfaceOutputView(ParamSurface param) {
		this.param = param;
		BorderLayout layout = new BorderLayout();
		layout.setHgap(5);
		layout.setVgap(5);
		setLayout(layout);
		// Create label
		label = new JLabel("<HTML><B>" + param.getLabel() + "</B></HTML>");
		label.setAlignmentX(0);
		label.setAlignmentY(0);
		label.setVerticalTextPosition(SwingConstants.TOP);
		label.setHorizontalTextPosition(SwingConstants.LEFT);
		// Create parameter description field
		button = new JButton("View");
		button.setPreferredSize(ParamInputView.defaultNumberFieldDimension);
		button.addActionListener(this);
		field = new JLabel(((param.getSurface() != null) ? param.getSurface().getName() : "null"));
		field.setAlignmentX(0);
		field.setAlignmentY(0);
		field.setVerticalTextPosition(SwingConstants.TOP);
		field.setHorizontalTextPosition(SwingConstants.LEFT);
		add(label, BorderLayout.WEST);
		add(field, BorderLayout.CENTER);
		add(button, BorderLayout.EAST);
		// init();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(button)) {
			setMesh(getParameter().getSurface());
			render();
			frame.setTitle(param.getLabel() + " [" + getParameter().getSurface().getName() + "]");
		}
	}

	/**
	 * Compute bounds.
	 */
	private void computeBounds() {
		int sz = mesh.getVertexCount();
		double large = 1E30;
		double[] limits = new double[] { large, -large, large, -large, large, -large };
		Point3 p = new Point3();
		for (int i = 0; i < sz; i++) {
			mesh.getCoordinate(i, p);
			limits[0] = Math.min(p.x, limits[0]);
			limits[1] = Math.max(p.x, limits[1]);
			limits[2] = Math.min(p.y, limits[2]);
			limits[3] = Math.max(p.y, limits[3]);
			limits[4] = Math.min(p.z, limits[4]);
			limits[5] = Math.max(p.z, limits[5]);
		}
		setBounds(limits);
	}

	/**
	 * Create axis labels.
	 * 
	 * @return the node
	 */
	private javax.media.j3d.Node createAxisLabels() {
		BranchGroup guides = new BranchGroup();
		int flags = GeometryArray.COORDINATES | GeometryArray.COLOR_3 | GeometryArray.NORMALS;
		// Draw Axes for the Graph2D
		QuadArray cube = new QuadArray(16, flags);
		Point3d[] coordinates = new Point3d[16];
		Vector3f[] norms = new Vector3f[16];
		coordinates[0] = new Point3d(bounds[0], bounds[2], bounds[4]);
		coordinates[1] = new Point3d(bounds[1], bounds[2], bounds[4]);
		coordinates[2] = new Point3d(bounds[1], bounds[3], bounds[4]);
		coordinates[3] = new Point3d(bounds[0], bounds[3], bounds[4]);
		coordinates[4] = new Point3d(bounds[0], bounds[2], bounds[5]);
		coordinates[5] = new Point3d(bounds[0], bounds[2], bounds[4]);
		coordinates[6] = new Point3d(bounds[0], bounds[3], bounds[4]);
		coordinates[7] = new Point3d(bounds[0], bounds[3], bounds[5]);
		coordinates[8] = new Point3d(bounds[1], bounds[2], bounds[5]);
		coordinates[9] = new Point3d(bounds[0], bounds[2], bounds[5]);
		coordinates[10] = new Point3d(bounds[0], bounds[3], bounds[5]);
		coordinates[11] = new Point3d(bounds[1], bounds[3], bounds[5]);
		coordinates[12] = new Point3d(bounds[1], bounds[2], bounds[4]);
		coordinates[13] = new Point3d(bounds[1], bounds[2], bounds[5]);
		coordinates[14] = new Point3d(bounds[1], bounds[3], bounds[5]);
		coordinates[15] = new Point3d(bounds[1], bounds[3], bounds[4]);
		Color3f[] colors = new Color3f[16];
		for (int i = 0; i < 16; i++) {
			norms[i] = new Vector3f(0, 0, 1);
			colors[i] = axisColor;
		}
		cube.setNormals(0, norms);
		cube.setCoordinates(0, coordinates);
		cube.setColors(0, colors);
		Appearance appear = new Appearance();
		PolygonAttributes polyAttr = new PolygonAttributes(PolygonAttributes.POLYGON_LINE, PolygonAttributes.CULL_NONE,
				0.0f);
		appear.setPolygonAttributes(polyAttr);
		appear.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.NICEST, 0.3f));
		Shape3D shape = new Shape3D(cube);
		shape.setAppearance(appear);
		guides.addChild(shape);
		float offset = 0.08f;
		// Draw labels for the Graph2D
		guides.addChild(createTextLabel(String.format("(%.2f,%.2f,%.2f)", limits[0], limits[2], limits[4]),
				(float) bounds[0], (float) bounds[2], (float) bounds[4] - offset));
		guides.addChild(createTextLabel(String.format("%.2f", limits[1]), (float) bounds[1], (float) bounds[2],
				(float) bounds[4] - offset));
		guides.addChild(createTextLabel(String.format("%.2f", limits[3]), (float) bounds[0],
				(float) bounds[3] - offset, (float) bounds[4] - offset));
		guides.addChild(createTextLabel(String.format("%.2f", limits[5]), (float) bounds[0], (float) bounds[2],
				(float) bounds[5] + offset));
		/*
		 * guides.addChild(createTextLabel("(" + limits[0] + "," + limits[2] +
		 * "," + limits[4] + ")", (float) bounds[0], (float) bounds[2], (float)
		 * bounds[4] - 0.08f));
		 * 
		 * guides.addChild(createTextLabel("(" + limits[0] + "," + limits[3] +
		 * "," + limits[4] + ")", (float) bounds[0], (float) bounds[3], (float)
		 * bounds[4] - 0.08f));
		 * 
		 * guides.addChild(createTextLabel("(" + limits[1] + "," + limits[2] +
		 * "," + limits[4] + ")", (float) bounds[1], (float) bounds[2], (float)
		 * bounds[4] - 0.08f));
		 * 
		 * guides.addChild(createTextLabel("(" + limits[1] + "," + limits[3] +
		 * "," + limits[4] + ")", (float) bounds[1], (float) bounds[3], (float)
		 * bounds[4] - 0.08f));
		 * 
		 * guides.addChild(createTextLabel("(" + limits[0] + "," + limits[2] +
		 * "," + limits[5] + ")", (float) bounds[0], (float) bounds[2], (float)
		 * bounds[5]));
		 * 
		 * guides.addChild(createTextLabel("(" + limits[0] + "," + limits[3] +
		 * "," + limits[5] + ")", (float) bounds[0], (float) bounds[3], (float)
		 * bounds[5])); guides.addChild(createTextLabel("(" + limits[1] + "," +
		 * limits[2] + "," + limits[5] + ")", (float) bounds[1], (float)
		 * bounds[2], (float) bounds[5])); guides.addChild(createTextLabel("(" +
		 * limits[1] + "," + limits[3] + "," + limits[5] + ")", (float)
		 * bounds[1], (float) bounds[3], (float) bounds[5]));
		 */
		guides.addChild(createTextLabel("x", (float) (bounds[0] + bounds[1]) / 2.0f, (float) bounds[2],
				(float) bounds[4] - 0.1f));
		guides.addChild(createTextLabel("y", (float) bounds[0], (float) (bounds[2] + bounds[3]) / 2.0f,
				(float) bounds[4] - 0.1f));
		guides.addChild(createTextLabel("z", (float) bounds[0] - 0.1f, (float) bounds[2] - 0.1f,
				(float) (bounds[4] + bounds[5]) / 2.0f));
		guides.addChild(createTickMarks(new Color3f(1.0f, 0.0f, 0.0f), (float) bounds[0], (float) bounds[2],
				(float) bounds[4], (float) bounds[0], (float) bounds[3], (float) bounds[4]));
		guides.addChild(createTickMarks(new Color3f(0.0f, 1.0f, 0.0f), (float) bounds[0], (float) bounds[2],
				(float) bounds[4], (float) bounds[1], (float) bounds[2], (float) bounds[4]));
		guides.addChild(createTickMarks(new Color3f(0.0f, 0.0f, 1.0f), (float) bounds[0], (float) bounds[2],
				(float) bounds[4], (float) bounds[0], (float) bounds[2], (float) bounds[5]));
		return guides;
	}

	/**
	 * Create back face appearance.
	 * 
	 * @return the appearance
	 */
	private Appearance createBackAppearance() {
		Appearance appear = new Appearance();
		Material material = new Material(new Color3f(new Color(0.0f, 0.0f, 0.0f)), new Color3f(0, 0, 0), new Color3f(
				0.0f, 0.0f, 0.0f), new Color3f(0.0f, 0.0f, 0.0f), 0);
		PolygonAttributes polyAttr = new PolygonAttributes(PolygonAttributes.POLYGON_LINE, PolygonAttributes.CULL_BACK,
				0.5f);
		appear.setLineAttributes(new LineAttributes(2, LineAttributes.PATTERN_SOLID, true));
		appear.setPolygonAttributes(polyAttr);
		appear.setMaterial(material);
		return appear;
	}

	/**
	 * Create bounding mox.
	 * 
	 * @param xmin
	 *            min X
	 * @param ymin
	 *            min Y
	 * @param zmin
	 *            min Z
	 * @param xmax
	 *            max X
	 * @param ymax
	 *            max Y
	 * @param zmax
	 *            max Z
	 * @return the geometry array
	 */
	private GeometryArray createBox(double xmin, double ymin, double zmin, double xmax, double ymax, double zmax) {
		QuadArray box = new QuadArray(24, GeometryArray.COORDINATES);
		Point3d verts[] = new Point3d[24];
		// front face
		verts[0] = new Point3d(xmax, ymin, zmax);
		verts[1] = new Point3d(xmax, ymax, zmax);
		verts[2] = new Point3d(xmin, ymax, zmax);
		verts[3] = new Point3d(xmin, ymin, zmax);
		// back face
		verts[4] = new Point3d(xmin, ymin, zmin);
		verts[5] = new Point3d(xmin, ymax, zmin);
		verts[6] = new Point3d(xmax, ymax, zmin);
		verts[7] = new Point3d(xmax, ymin, zmin);
		// right face
		verts[8] = new Point3d(xmax, ymin, zmin);
		verts[9] = new Point3d(xmax, ymax, zmin);
		verts[10] = new Point3d(xmax, ymax, zmax);
		verts[11] = new Point3d(xmax, ymin, zmax);
		// left face
		verts[12] = new Point3d(xmin, ymin, zmax);
		verts[13] = new Point3d(xmin, ymax, zmax);
		verts[14] = new Point3d(xmin, ymax, zmin);
		verts[15] = new Point3d(xmin, ymin, zmin);
		// top face
		verts[16] = new Point3d(xmax, ymax, zmax);
		verts[17] = new Point3d(xmax, ymax, zmin);
		verts[18] = new Point3d(xmin, ymax, zmin);
		verts[19] = new Point3d(xmin, ymax, zmax);
		// bottom face
		verts[20] = new Point3d(xmin, ymin, zmax);
		verts[21] = new Point3d(xmin, ymin, zmin);
		verts[22] = new Point3d(xmax, ymin, zmin);
		verts[23] = new Point3d(xmax, ymin, zmax);
		box.setCoordinates(0, verts);
		return box;
	}

	/**
	 * Create font appearance.
	 * 
	 * @return the appearance
	 */
	private Appearance createFrontAppearance() {
		Appearance appear = new Appearance();
		Material material = new Material(new Color3f(new Color(0.1f, 0.1f, 0.1f)), new Color3f(), new Color3f(0.7f,
				0.7f, 0.7f), new Color3f(0.1f, 0.1f, 0.1f), 64.0f);
		PolygonAttributes polyAttr = new PolygonAttributes(PolygonAttributes.POLYGON_FILL, PolygonAttributes.CULL_BACK,
				0.0f);
		appear.setPolygonAttributes(polyAttr);
		appear.setMaterial(material);
		return appear;
	}

	/**
	 * Create lights.
	 * 
	 * @return lights
	 */
	private javax.media.j3d.Node createLight() {
		Color3f directColor = meshColor;
		BoundingSphere bsphere = new BoundingSphere(new Point3d(), 2.0);
		TransformGroup tg = new TransformGroup();
		DirectionalLight directLight;
		for (Vector3f direction : lightDirections) {
			directLight = new DirectionalLight();
			directLight.setInfluencingBounds(bsphere);
			directLight.setDirection(direction);
			directLight.setColor(directColor);
			tg.addChild(directLight);
		}
		AmbientLight ambientLight = new AmbientLight();
		ambientLight.setInfluencingBounds(bsphere);
		ambientLight.setColor(directColor);
		tg.addChild(ambientLight);
		return tg;
	}

	/**
	 * Create shape to render.
	 * 
	 * @return shape
	 */
	private javax.media.j3d.Node createShape() {
		Shape3D shape1 = new Shape3D(mesh);
		shape1.setAppearance(createFrontAppearance());
		// Shape3D shape2 = new Shape3D(mesh);
		// shape2.setAppearance(createBackAppearance());
		Transform3D trans = new Transform3D();
		Vector3d tmp1 = (Vector3d) center.clone();
		tmp1.negate();
		trans.setTranslation(tmp1);
		trans.setScale(scale);
		TransformGroup shapeTG = new TransformGroup(trans);
		shapeTG.addChild(shape1);
		// shapeTG.addChild(shape2);
		return shapeTG;
	}

	/**
	 * Create text label at location.
	 * 
	 * @param str
	 *            label
	 * @param x
	 *            X
	 * @param y
	 *            Y
	 * @param z
	 *            Z
	 * @return 3d node
	 */
	private javax.media.j3d.Node createTextLabel(String str, float x, float y, float z) {
		float textSize = 0.04f;
		Transform3D scalar = new Transform3D();
		scalar.setScale(textSize);
		TransformGroup tg = new TransformGroup(scalar);
		Font3D f3d = new Font3D(new Font("Arial", Font.PLAIN, JistPreferences.getPreferences().getSmFontSize()/2), new FontExtrusion());
		Text3D txt = new Text3D(f3d, str, new Point3f(x / textSize, y / textSize, z / textSize), Text3D.ALIGN_CENTER,
				Text3D.PATH_RIGHT);
		OrientedShape3D textShape = new OrientedShape3D();
		textShape.setGeometry(txt);
		textShape.setAlignmentMode(OrientedShape3D.ROTATE_ABOUT_POINT);
		Appearance appear = new Appearance();
		appear.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.NICEST, 0.3f));
		textShape.setAppearance(appear);
		Point3f rotationPt = new Point3f(x / textSize, y / textSize, z / textSize);
		textShape.setRotationPoint(rotationPt);
		tg.addChild(textShape);
		return tg;
	}

	/**
	 * Create tick marks.
	 * 
	 * @param col
	 *            color
	 * @param x
	 *            min X
	 * @param y
	 *            min Y
	 * @param z
	 *            min Z
	 * @param x2
	 *            max X
	 * @param y2
	 *            max Y
	 * @param z2
	 *            max Z
	 * @return 3d node
	 */
	private javax.media.j3d.Node createTickMarks(Color3f col, float x, float y, float z, float x2, float y2, float z2) {
		BranchGroup bg = new BranchGroup();
		float xInc = (x2 - x) / tickMarks, yInc = (y2 - y) / tickMarks, zInc = (z2 - z) / tickMarks;
		for (int i = 0; i <= tickMarks; i++) {
			Appearance appear = new Appearance();
			Material material = new Material(new Color3f(0.1f, 0.1f, 0.1f), col, col, new Color3f(1.0f, 1.0f, 1.0f),
					64.0f);
			appear.setMaterial(material);
			Sphere sph = new Sphere(0.01f, Primitive.GENERATE_NORMALS, appear);
			Transform3D trans = new Transform3D();
			trans.setTranslation(new Vector3f(x + i * xInc, y + i * yInc, z + i * zInc));
			TransformGroup tg = new TransformGroup(trans);
			tg.addChild(sph);
			bg.addChild(tg);
		}
		return bg;
	}

	/**
	 * Get screenshot.
	 * 
	 * @return the image
	 */
	public Image getImage() {
		return canvas.getOffScreenBuffer().getImage();
	}

	/**
	 * Get surface parameter.
	 * 
	 * @return surfae parameter
	 */
	public ParamSurface getParameter() {
		return (ParamSurface) param;
	}

	/**
	 * Get preferred view of mesh.
	 * 
	 * @return the preferred view
	 */
	private TransformGroup getPreferredView() {
		Transform3D rotate = new Transform3D();
		Transform3D rotateT = new Transform3D();
		rotate.rotX(-Math.PI / 6.0d);
		rotateT.rotZ(Math.PI / 4.0d);
		rotate.mul(rotateT);
		rotate.setScale(0.5);
		TransformGroup tg = new TransformGroup(rotate);
		return tg;
	}

	/**
	 * Initialize 3d scene.
	 */
	private void init() {
		GraphicsConfigTemplate3D template = new GraphicsConfigTemplate3D();
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = env.getDefaultScreenDevice();
		GraphicsConfiguration config = device.getBestConfiguration(template);
		canvas = new Canvas3D(config);
		canvas.setPreferredSize(new Dimension(640, 480));
		universe = new SimpleUniverse(canvas);
		frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.add(canvas, BorderLayout.CENTER);
		frame.pack();
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent e) {
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Render mesh.
	 */
	public void render() {
		if (rendered) {
			frame.setVisible(true);
			return;
		}
		init();
		// create the basic universe
		canvas.stopRenderer();
		ViewingPlatform viewingPlatform = universe.getViewingPlatform();
		viewingPlatform.setNominalViewingTransform();
		// add orbit behavior to the viewing platform
		OrbitBehavior orbit = new OrbitBehavior(canvas, OrbitBehavior.REVERSE_ALL);
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
		orbit.setSchedulingBounds(bounds);
		// add some geometry
		BranchGroup graph3d = new BranchGroup();
		TransformGroup tg = getPreferredView();
		graph3d.addChild(tg);
		tg.addChild(createShape());
		tg.addChild(createAxisLabels());
		tg.addChild(createLight());
		graph3d.compile();
		canvas.addMouseListener(this);
		viewingPlatform.setViewPlatformBehavior(orbit);
		universe.addBranchGraph(graph3d);
		canvas.startRenderer();
		rendered = true;
		frame.setVisible(true);
	}

	/**
	 * Reset 3d universe.
	 */
	public void reset() {
		universe.cleanup();
		universe = new SimpleUniverse(canvas);
	}

	/**
	 * Set axis color.
	 * 
	 * @param axisColor
	 *            axis color
	 */
	public void setAxisColor(Color3f axisColor) {
		this.axisColor = axisColor;
	}

	/**
	 * Set object bounds from limits.
	 * 
	 * @param limits
	 *            the limits
	 */
	private void setBounds(double[] limits) {
		this.limits = limits;
		double max = Math.max(Math.max(limits[1] - limits[0], limits[3] - limits[2]), limits[5] - limits[4]);
		scale.x = 2 / max;
		scale.y = 2 / max;
		scale.z = 2 / max;
		center.x = scale.x * 0.5 * (limits[0] + limits[1]);
		center.y = scale.y * 0.5 * (limits[2] + limits[3]);
		center.z = scale.z * 0.5 * (limits[4] + limits[5]);
		bounds[0] = -(limits[1] - limits[0]) / max;
		bounds[1] = (limits[1] - limits[0]) / max;
		bounds[2] = -(limits[3] - limits[2]) / max;
		bounds[3] = (limits[3] - limits[2]) / max;
		bounds[4] = -(limits[5] - limits[4]) / max;
		bounds[5] = (limits[5] - limits[4]) / max;
	}

	/**
	 * set mesh to render.
	 * 
	 * @param ga
	 *            geometry array
	 */
	public void setMesh(GeometryArray ga) {
		// GeometryInfo gi=new GeometryInfo(ga);
		// NormalGenerator ng = new NormalGenerator();
		// ng.generateNormals(gi);
		// mesh=gi.getGeometryArray();
		mesh = ga;
		computeBounds();
	}

	/**
	 * set mesh color.
	 * 
	 * @param meshColor
	 *            mesh color
	 */
	public void setMeshColor(Color3f meshColor) {
		this.meshColor = meshColor;
	}

	/**
	 * Update label text.
	 */
	public void update() {
		File f = getParameter().getValue();
		if (f == null) {
			EmbeddedSurface surf = getParameter().getSurface();
			if (surf != null) {
				field.setText(surf.getName());
			} else {
				field.setText("null");
			}
		} else {
			field.setText(FileReaderWriter.getFileName(f));
		}
	}
}
