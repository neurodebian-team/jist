/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamFileCollectionInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamFileCollectionOutputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.parameters.ParameterException;
import gov.nih.mipav.model.scripting.parameters.ParameterFactory;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.view.dialogs.AlgorithmParameters;

/**
 * File Collection Parameter Factory.
 * 
 * @author Blake Lucas
 */
public class ParamFileCollectionFactory extends ParamFactory {
	
	/** The param. */
	private ParamFileCollection param;

	/**
	 * Instantiates a new param file collection factory.
	 */
	public ParamFileCollectionFactory() {
	}

	/**
	 * Instantiates a new param file collection factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamFileCollectionFactory(ParamFileCollection param) {
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#createMipavParameter(gov.nih.mipav.view.dialogs.AlgorithmParameters)
	 */
	public void createMipavParameter(AlgorithmParameters scriptParams) throws ParserException {
		ParamFileCollection param = getParameter();
		List<File> fileList = param.getValue();
		String strArray="";
		for (File f : fileList) {
			String val=encodeValue(f.getAbsolutePath());
			if(val.length()>0){
				strArray+= val+",";
			}
		}
		strArray=strArray.substring(0, Math.max(0,strArray.length()-1));
		scriptParams.getParams().put(ParameterFactory.newParameter(encodeName(param.getName()), strArray));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof ParamFactory) {
			return this.equals(((ParamFactory) obj).getParameter());
		} else if (obj instanceof ParamFileCollection) {
			return this.getParameter().getValue().equals(((ParamFileCollection) obj).getValue());
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamFileCollectionInputView(param);
		}
		return inputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamFileCollectionOutputView(param);
		}
		return outputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getParameter()
	 */
	public ParamFileCollection getParameter() {
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#importMipavParameter(gov.nih.mipav.model.scripting.parameters.ParameterTable)
	 */
	public void importMipavParameter(ParameterTable paramTable) throws ParameterException {
		LinkedList<File> decodedNames = new LinkedList<File>();
		String[] vals=paramTable.getString(encodeName(getParameter().getName())).split(",");
		for (Object obj : vals) {
			String val=decodeValue((String) obj);
			if(val.length()>0){
				decodedNames.add(new File(val));
			}
		}
		getParameter().setValue(decodedNames);
	}
}
