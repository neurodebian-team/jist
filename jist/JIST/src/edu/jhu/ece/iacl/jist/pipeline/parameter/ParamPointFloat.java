/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamPointFloatFactory;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * 3D Point parameter for floating point.
 * 
 * @author Blake Lucas
 */
public class ParamPointFloat extends ParamModel<Point3f> implements ParamPoint {
	
	/** The px. */
	protected ParamFloat px;
	
	/** The py. */
	protected ParamFloat py;
	
	/** The pz. */
	protected ParamFloat pz;

	/**
	 * Default constructor.
	 */
	public ParamPointFloat() {
		super();
		px = new ParamFloat("x");
		py = new ParamFloat("y");
		pz = new ParamFloat("z");
		this.factory = new ParamPointFloatFactory(this);
	}

	/**
	 * Constructor.
	 * 
	 * @param p
	 *            point
	 */
	public ParamPointFloat(Point3f p) {
		this();
		px.setValue(p.x);
		py.setValue(p.y);
		pz.setValue(p.z);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamPointFloat(String name) {
		this();
		this.setName(name);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 * @param p
	 *            point
	 */
	public ParamPointFloat(String name, Point3f p) {
		this();
		this.setName(name);
		px.setValue(p.x);
		py.setValue(p.y);
		pz.setValue(p.z);
	}

	/**
	 * Clone object.
	 * 
	 * @return the param point float
	 */
	public ParamPointFloat clone() {
		ParamPointFloat param = new ParamPointFloat();
		param.px = px.clone();
		param.py = py.clone();
		param.pz = pz.clone();
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#compareTo(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel)
	 */
	public int compareTo(ParamModel model) {
		return (model instanceof ParamPoint) ? px.compareTo(((ParamPoint) model).getParamX()) : 1;
	}

	/**
	 * Get X coordinate.
	 * 
	 * @return X coordinate
	 */
	public ParamFloat getParamX() {
		return px;
	}

	/**
	 * Get Y coordinate.
	 * 
	 * @return Y coordinate
	 */
	public ParamFloat getParamY() {
		return py;
	}

	/**
	 * Get Z coordinate.
	 * 
	 * @return Z coordinate
	 */
	public ParamFloat getParamZ() {
		return pz;
	}

	/**
	 * Get float point.
	 * 
	 * @return the value
	 */
	public Point3f getValue() {
		return new Point3f(px.getFloat(), py.getFloat(), pz.getFloat());
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(1);
		connectible = true;
		factory = new ParamPointFloatFactory(this);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#isCompatible(edu.jhu.ece.iacl.jist.pipeline.PipePort)
	 */
	public boolean isCompatible(PipePort model) {
		return (model instanceof ParamPoint);
	}

	/**
	 * Set double point value.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(Point3d value) throws InvalidParameterValueException {
		if (value != null) {
			px.setValue(value.x);
			py.setValue(value.y);
			pz.setValue(value.z);
		} else {
			throw new InvalidParameterValueException(this, value);
		}
	}

	/**
	 * Set float point value.
	 * 
	 * @param value
	 *            point
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(Point3f value) throws InvalidParameterValueException {
		if (value != null) {
			px.setValue(value.x);
			py.setValue(value.y);
			pz.setValue(value.z);
		} else {
			throw new InvalidParameterValueException(this, value);
		}
	}

	/**
	 * Set integer point value.
	 * 
	 * @param value
	 *            point
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(Point3i value) throws InvalidParameterValueException {
		if (value != null) {
			px.setValue(value.x);
			py.setValue(value.y);
			pz.setValue(value.z);
		} else {
			throw new InvalidParameterValueException(this, value);
		}
	}

	/**
	 * Get description of point.
	 * 
	 * @return the string
	 */
	public String toString() {
		return "(" + px.getFloat() + "," + py.getFloat() + "," + pz.getFloat() + ")";
	}

	/**
	 * Validate point coordinates.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		px.validate();
		py.validate();
		pz.validate();
	}
	
	@Override
	public String getHumanReadableDataType() {
		return "float point: semi-colon separated list of 3 coordinates";
	}
	public String getXMLValue() {
		Point3f pt =getValue();
		return pt.x+";"+pt.y+";"+pt.z;
	};
	
	@Override
	public void setXMLValue(String arg) {
		String []args=arg.trim().split("[;]+");
		if(args.length!=3)
			throw new InvalidParameterValueException(this,"Cannot find three entries:"+arg);
		setValue(new Point3f(Float.valueOf(args[0]),Float.valueOf(args[1]),Float.valueOf(args[2])));
	};
	
	public String probeDefaultValue() {	
		return getXMLValue();
	}
	
	@Override
	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);		 
		 Element em;				
		 em = document.createElement("px");		
		 px.xmlEncodeParam(document, em);
		 parent.appendChild(em);	
		 em = document.createElement("py");		
		 py.xmlEncodeParam(document, em);
		 parent.appendChild(em);	
		 em = document.createElement("pz");		
		 pz.xmlEncodeParam(document, em);
		 parent.appendChild(em);	
		 return true;
	 }
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		px = new ParamFloat();
		px.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(parent, "px"));
		py = new ParamFloat();
		py.xmlDecodeParam(document, JistXMLUtil.xmlReadElement(parent, "py"));
		pz = new ParamFloat();
		pz.xmlDecodeParam(document, JistXMLUtil.xmlReadElement(parent, "pz"));		
	}
}
