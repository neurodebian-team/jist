package edu.jhu.ece.iacl.jist.structures.geom;

import java.util.HashSet;

import javax.media.j3d.GeometryArray;

// TODO: Auto-generated Javadoc
/**
 * The Class Surface.
 */
public abstract class Surface{
	
	/** The faces. */
	protected HashSet<Face> faces;
	
	/** The vertices. */
	protected HashSet<Vertex> vertices;
	
	/** The edges. */
	protected HashSet<Edge> edges;
	
	/**
	 * Instantiates a new surface.
	 * 
	 * @param vertexCount the vertex count
	 */
	public Surface(int vertexCount){
		faces=new HashSet<Face>(vertexCount/3);
		vertices=new HashSet<Vertex>(vertexCount);
		edges=new HashSet<Edge>(vertexCount/2);
	}
	
	/**
	 * Gets the vertices.
	 * 
	 * @return the vertices
	 */
	public Vertex[] getVertices() {
		Vertex[] verts=new Vertex[vertices.size()];
		vertices.toArray(verts);
		return verts;
	}

	/**
	 * Gets the edges.
	 * 
	 * @return the edges
	 */
	public Edge[] getEdges() {
		Edge es[]=new Edge[edges.size()];
		edges.toArray(es);
		return es;
	}

	/**
	 * Gets the faces.
	 * 
	 * @return the faces
	 */
	public Face[] getFaces() {
		Face[] fs=new Face[faces.size()];
		faces.toArray(fs);
		return fs;
	}
	
	/**
	 * Adds the.
	 * 
	 * @param f the f
	 */
	public void add(Face f) {
		//f.add(this);
		faces.add(f);
		Vertex[] vs=f.getVertices();
		Edge[] es=f.getEdges();
		for(Vertex v:vs){
			if(!vertices.contains(v))vertices.add(v);
		}
		for(Edge e:es){
			if(!edges.contains(e))edges.add(e);
		}
	}
	
	/**
	 * Gets the geometry array.
	 * 
	 * @return the geometry array
	 */
	public abstract GeometryArray getGeometryArray();
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "[faces="+faces.size()+",edges="+edges.size()+",vertices="+vertices.size()+"]";
	}
}
