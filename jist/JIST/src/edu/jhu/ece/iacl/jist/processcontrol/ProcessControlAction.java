package edu.jhu.ece.iacl.jist.processcontrol;

// TODO: Auto-generated Javadoc
/**
 * The Enum ProcessControlAction.
 */
public enum ProcessControlAction {
	
	/** The SUSPEND. */
	SUSPEND,
	
	/** The RESUME. */
	RESUME,
	
	/** The HOLD. */
	HOLD,
	
	/** The RELEASE. */
	RELEASE, 
	
	/** The TERMINATE. */
	TERMINATE

}

