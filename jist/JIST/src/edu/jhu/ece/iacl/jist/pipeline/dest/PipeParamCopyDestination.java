/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.dest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeDestination;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.parameter.CompatibilityChecker;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Destination for copying output files to specified directory location.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeParamCopyDestination extends PipeDestination {
	
	/**
	 * Check for compatibility between output ports and this module's input
	 * port.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	private class DataChecker implements CompatibilityChecker {
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.CompatibilityChecker#isCompatible(edu.jhu.ece.iacl.jist.pipeline.PipePort)
		 */
		public boolean isCompatible(PipePort model) {
			return ((model instanceof ParamFile) || (model instanceof ParamFileCollection));
		}
	}
	/**
	 * Get input parameter
	 */
	public ParamCollection getInputParam(){
		return (ParamCollection)selectedParams;
	}
	/** The param summary file. */
	protected ParamObject<Object[][]> paramSummaryFile;
	
	/** The file name. */
	protected ParamString fileName;
	
	/** The file type. */
//	protected ParamOption fileType;
	
	/** The file output dir. */
	protected ParamFile fileOutputDir;
	
	/** The data objects. */
	transient protected ArrayList<Object[]> dataObjects;
	
	/** The append name. */
	protected ParamBoolean appendName;
	
	protected boolean xmlEncodeModule(Document document, Element parent) {
		super.xmlEncodeModule(document, parent);
		Element em;	
//		em = document.createElement("fileName");		
//		fileName.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		em = document.createElement("paramSummaryFile");		
//		paramSummaryFile.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		em = document.createElement("fileType");		
//		fileType.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		em = document.createElement("fileOutputDir");		
//		fileOutputDir.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		em = document.createElement("appendName");		
//		appendName.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//	
		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		
		fileName = (ParamString) inputParams.getFirstChildByName("File Name");
		fileOutputDir = (ParamFile) inputParams.getFirstChildByName("Destination Directory");
		appendName  = (ParamBoolean) inputParams.getFirstChildByName("Append Experiment Name");
		paramSummaryFile  = (ParamObject) outputParams.getFirstChildByName("Summary File");
//		fileName = new ParamString();
//		fileName.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileName"));
//		paramSummaryFile = new ParamObject<Object[][]>();
//		paramSummaryFile.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"paramSummaryFile"));
//		fileType = new ParamOption();
//		fileType.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileType"));
//		fileOutputDir = new ParamFile();
//		fileOutputDir.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileOutputDir"));
//		appendName = new ParamBoolean();
//		appendName.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"appendName"));

	}
	
	/**
	 * Clean up after copying files.
	 */
	public void complete() {
		Object[][] objs = new Object[dataObjects.size()][getInputParam().size()];
		for (int i = 0; i < dataObjects.size(); i++) {
			objs[i] = dataObjects.get(i);
		}
		paramSummaryFile.setObject(objs);
		paramSummaryFile.setFileName(fileName.getValue());
	}

	/**
	 * Copy a file from source to destination.
	 * 
	 * @param src
	 *            source file
	 * @param dest
	 *            destination file
	 * @return true if copy is successful
	 */
	public static boolean copyFile(File src, File dest) {
		try {
			FileChannel ic = new FileInputStream(src).getChannel();
			FileChannel oc = new FileOutputStream(dest).getChannel();
			ic.transferTo(0, ic.size(), oc);
			oc.close();
			ic.close();
			System.out.println("PPCD"+"\t"+"COPIED " + src + " TO " + dest);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Saves a volume to fileOutputDir
	 * @param vol the volume that will be saved
	 * @param ext the filetype extension
	 * @param outputDir where the file will be saved
	 * @return the file if successful, null otherwise
	 */
	private File copyVolume(ImageData vol, String ext, File outputDir)
	{
		ImageDataReaderWriter instance = ImageDataReaderWriter.getInstance();
		String oldext = instance.getExtensionFilter().getPreferredExtension();
		instance.getExtensionFilter().setPreferredExtension(ext);
		File ret =  instance.write(vol, outputDir);
		instance.getExtensionFilter().setPreferredExtension(oldext);
		return ret;
	}
	
	/**
	 * 
	 * Takes a filename and returns the extension
	 * 
	 * @param s the filename
	 * @return the extension at the end of the filename
	 */
	private static String getVolumeExtension(String s)
	{
		int index = (s.lastIndexOf("."));
		if (index >= 0) 
		{
			return s.substring(index + 1, (s.length()));
		} 
		return "";	
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createInputParams() {
		selectedParams = new ParamCollection("Parameters");
		ParamCollection input = new ParamCollection();
		input.setLabel("Copy Files");
		input.setName("copy");
		input.add(fileName = new ParamString("File Name", "copy_summary"));
		input.add(fileOutputDir = new ParamFile("Destination Directory", MipavController.getDefaultWorkingDirectory(),
				DialogType.DIRECTORY));
		input.add(appendName = new ParamBoolean("Append Experiment Name", true));
		input.setCategory("Output");
		File f = JistPreferences.getPreferences().getLastDirectory();
		return input;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createOutputParams() {
		ParamCollection output = new ParamCollection();
		output.add(paramSummaryFile = new ParamObject("Summary File", ParamFile.DialogType.FILE));
		fileOutputDir.setMandatory(false);
		paramSummaryFile.setReaderWriter(new ArrayObjectTxtReaderWriter());
		return output;
	}

	/**
	 * Initialize module cell.
	 * 
	 * @param graph
	 *            the graph
	 * @return the pipe module cell
	 */
	public PipeModuleCell init(PipeJGraph graph) {
		PipeModuleCell cell = super.init(graph);
		getInputParam().setChecker(new DataChecker());
		return cell;
	}

	/**
	 * Iterate through all active experiments and the save the output of each to
	 * a specified location.
	 * 
	 * @param monitor
	 *            the monitor
	 */
	public void iterate(ProgressMonitor monitor) {
		Vector<ParamModel> allParams = getInputParam().getAllDescendants();
		Object[] objs = new Object[allParams.size()];
		//Get specified location
		File outputDir = fileOutputDir.getValue();
		if (dataObjects.size() == 0) {
			String[] labels = new String[allParams.size()];
			labels[0] = "Context Name";
			for (int i = 0; i < allParams.size(); i++) {
				ParamModel mod = allParams.get(i);
				labels[i] = mod.getLabel() + " (" + mod.getOwner().getLabel() + ")";
			}
			dataObjects.add(labels);
		}
		int i = 0;
		for (ParamModel model : allParams) {
			if (model instanceof ParamPerformance) {
				objs[i] = ((ParamPerformance) model).getValue().getTotalElapsedCPUTime() / 1000.0;
				i++;
				continue;
			} else if (model instanceof ParamVolume) {
				objs[i] = ((ParamVolume) model).getValue().getAbsolutePath();
				
				if (outputDir.exists()) {
					
					//Get volume extension and set as preferred
					((ParamVolume) model).getExtensionFilter().setPreferredExtension(getVolumeExtension((String) objs[i]));
					
					ImageData vol = ((ParamVolume) model).getImageData();
					//Gets the name of the process
					ExecutionContext context = model.getOwner().getCurrentContext();
					if (appendName.getValue() && context != null) {
						vol.setName(vol.getName() + "_" + context.getContextShortName());
					}
					
					//Writes the volume
					ImageDataMipav v2;
					if(vol instanceof ImageDataMipav) {
						v2= (ImageDataMipav)vol;
					} else {
					v2 = (new ImageDataMipav(vol));
					vol.dispose();
					}
					File f = copyVolume(v2, ((ParamVolume) model).getExtensionFilter().getPreferredExtension(), outputDir);
					monitor.setNote(f.getName());
//					v2.dispose();
					v2=null;
//					vol.dispose();
					vol=null;// bl 2008.10.26
//					((ParamVolume) model).dispose(); // bl 2008.10.26
//					MipavController.clearReigstry();
					
				}
			
				i++;
				continue;
			} else if (model instanceof ParamFile) {
				objs[i] = ((ParamFile) model).getValue().getAbsolutePath();
				if (outputDir.exists()) {
					File f = ((ParamFile) model).getValue();
					String ext = FileReaderWriter.getFileExtension(f);
					String name = FileReaderWriter.getFileName(f);
					ExecutionContext context = model.getOwner().getCurrentContext();
					File newFile = new File(outputDir, name
							+ ((appendName.getValue() && context != null) ? "_" + edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(context.getContextShortName()) : "")
							+ "." + ext);
					monitor.setNote(newFile.getName());
					copyFile(f, newFile);
				}
				i++;
				continue;
			} else if (model instanceof ParamVolumeCollection) {
				ParamVolumeCollection collection = ((ParamVolumeCollection) model);
				objs[i] = collection.getValue().toString();
				if (outputDir.exists()) {
					//Iterate through every volume in the collection
					List<ImageData> list = collection.getImageDataList();
					for (int j = 0; j < collection.getFileList().size(); j++) {
						File img = (File) collection.getFileList().get(j);
						ImageData vol = list.get(j);
						ExecutionContext context = model.getOwner().getCurrentContext();
						if (appendName.getValue() && context != null) {
							vol.setName(vol.getName() + "_" + context.getContextShortName());
						}
						//Set correct extension
						((ParamVolumeCollection) model).getExtensionFilter().setPreferredExtension(getVolumeExtension(img.getAbsolutePath()));
								
						//Writes a File
						File f = copyVolume(vol, ((ParamVolumeCollection) model).getExtensionFilter().getPreferredExtension(), outputDir);
						monitor.setNote(f.getName());
//						MipavController.clearReigstry();
					}					
				}
				i++;
				continue;
			} else if (model instanceof ParamFileCollection) {
				ParamFileCollection collection = ((ParamFileCollection) model);
				objs[i] = collection.getValue().toString();
				if (outputDir.exists()) {
					for (File f : collection.getValue()) {
						String ext = FileReaderWriter.getFileExtension(f);
						String name = FileReaderWriter.getFileName(f);
						ExecutionContext context = model.getOwner().getCurrentContext();
						File newFile = new File(outputDir, name
								+ ((appendName.getValue() && context != null) ? "_" + edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(context.getContextShortName())
										: "") + "." + ext);
						monitor.setNote(newFile.getName());
						copyFile(f, newFile);
					}
				}
				i++;
				continue;
			}
			String str = model.toString();
			try {
				objs[i] = Integer.parseInt(str);
				i++;
				continue;
			} catch (NumberFormatException e) {
			}
			try {
				objs[i] = Double.parseDouble(str);
				i++;
				continue;
			} catch (NumberFormatException e) {
			}
			objs[i] = str;
			i++;
		}
		dataObjects.add(objs);
	}

	/**
	 * Reset data objects to be saved.
	 */
	public void reset() {
		dataObjects = new ArrayList<Object[]>();
	}
	
	
}
