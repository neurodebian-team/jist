/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import org.jgraph.graph.CellViewRenderer;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.VertexRenderer;
import org.jgraph.graph.VertexView;

/**
 * Cell view renderer for algorithm.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeAlgorithmView extends VertexView {
	
	/**
	 * The Class ActivityRenderer.
	 */
	public static class ActivityRenderer extends VertexRenderer {
		
		/** Default handle bounds for renderer, '+' or '-'. */
		public static Rectangle handle = new Rectangle(0, 0, 10, 10);
		
		/**
		 * Specifies whether the current view is a rich text value, and if the
		 * image should be stretched.
		 */
		protected boolean isGroup = false;
		
		/** Holds the background and foreground of the graph. */
		protected Color handleColor = Color.white, graphForeground = Color.gray;

		/**
		 * Return a slightly larger preferred size than for a rectangle.
		 * 
		 * @return the preferred size
		 */
		public Dimension getPreferredSize() {
			Dimension d = super.getPreferredSize();
			d.width += d.height / 5;
			return d;
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.VertexRenderer#paint(java.awt.Graphics)
		 */
		public void paint(Graphics g) {
			int b = borderWidth;
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			Graphics2D g2 = (Graphics2D) g;
			Dimension d = getSize();
			boolean tmp = selected;
			int roundRectArc = PipeAlgorithmView.getArcSize(d.width - b, d.height - b);
			g.setColor(this.getBackground());
			// g.fillRect(b / 2, b / 2, d.width,d.height);
			if (super.isOpaque()) {
				g.setColor(super.getBackground());
				if ((gradientColor != null) && !preview) {
					setOpaque(false);
					g2.setPaint(new GradientPaint(0, 0, getBackground(), getWidth(), getHeight(), gradientColor, true));
				}
				g.fillRoundRect(b / 2, b / 2 + PipePortView.portSize, d.width - (int) (b * 1.5), d.height
						- (int) (b * 1.5) - 2 * PipePortView.portSize, roundRectArc, roundRectArc);
			}
			try {
				setBorder(null);
				setOpaque(false);
				selected = false;
				super.paint(g);
			} finally {
				selected = tmp;
			}
			if (bordercolor != null) {
				g.setColor(bordercolor);
				g2.setStroke(new BasicStroke(b));
				g.drawRoundRect(b / 2, b / 2 + PipePortView.portSize, d.width - (int) (b * 1.5), d.height
						- (int) (b * 1.5) - 2 * PipePortView.portSize, roundRectArc, roundRectArc);
			}
			if (selected) {
				g2.setStroke(GraphConstants.SELECTION_STROKE);
				g.setColor(highlightColor);
				g.drawRoundRect(b / 2, b / 2 + PipePortView.portSize, d.width - (int) (b * 1.5), d.height
						- (int) (b * 1.5) - 2 * PipePortView.portSize, roundRectArc, roundRectArc);
			}
			if (isGroup) {
				g2.setStroke(new BasicStroke(b));
				g.setColor(handleColor);
				g.fill3DRect(handle.x, handle.y, handle.width, handle.height, true);
				g.setColor(graphForeground);
				g.drawRect(handle.x, handle.y, handle.width, handle.height);
				g.drawLine(handle.x + 1, handle.y + handle.height / 2, handle.x + handle.width - 2, handle.y
						+ handle.height / 2);
				if (view.isLeaf()) {
					g.drawLine(handle.x + handle.width / 2, handle.y + 1, handle.x + handle.width / 2, handle.y
							+ handle.height - 2);
				}
			}
		}
	}

	/** The renderer. */
	public static transient ActivityRenderer renderer = new ActivityRenderer();

	// todo public Point2D getPerimeterPoint(Point source, Point p) {
	/**
	 * getArcSize calculates an appropriate arc for the corners of the rectangle
	 * for boundary size cases of width and height.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @return the arc size
	 */
	public static int getArcSize(int width, int height) {
		int arcSize;
		// The arc width of a activity rectangle is 1/8th of the larger
		// of the two of the dimensions passed in, but at most 1/2
		// of the smaller of the two. 1/8 because it looks nice and 1/2
		// so the arc can complete in the given dimension
		/*
		 * if (width <= height) { arcSize = height / 8; if (arcSize > (width /
		 * 2)) { arcSize = width / 2; } } else { arcSize = width / 8; if
		 * (arcSize > (height / 2)) { arcSize = height / 2; } }
		 */
		return 18;
	}

	/**
	 * Instantiates a new pipe algorithm view.
	 */
	public PipeAlgorithmView() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.AbstractCellView#getCell()
	 */
	public PipeModuleCell getCell() {
		return (PipeModuleCell) super.getCell();
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.VertexView#getRenderer()
	 */
	public CellViewRenderer getRenderer() {
		return renderer;
	}
}