package edu.jhu.ece.iacl.jist.structures.matrix;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

/**
 * Matrix 3d implementation
 * Values are always stored as double.
 * 
 * @author Blake Lucas
 */
public class Matrix3 implements MatrixX,Cloneable{
	
	/** Matrix 3d access This is not good encapsulation and will be deprectated in future iterations! Please use getters and setters when accessing matrix3d. */
	public double mat[][][];
	
	/** The slices. */
	private int rows,cols,slices;

	/**
	 * Instantiates a new matrix3.
	 * 
	 * @param d1 the d1
	 * @param d2 the d2
	 * @param d3 the d3
	 */
	public Matrix3(int d1,int d2,int d3){
		this(d1,d2,d3,0);

	}
	
	/**
	 * Instantiates a new matrix3.
	 * 
	 * @param d1 the d1
	 * @param d2 the d2
	 * @param d3 the d3
	 * @param a the a
	 */
	public Matrix3(int d1,int d2,int d3,double a){
		mat=new double[d1][d2][d3];
		rows=d1;
		cols=d2;
		slices=d3;
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					mat[i][j][k]=a;
				}
			}
		}
	}
	
	/**
	 * Instantiates a new matrix3.
	 * 
	 * @param vol the vol
	 */
	public Matrix3(ImageData vol){
		rows=vol.getRows();
		slices=vol.getSlices();
		cols=vol.getCols();
		mat=new double[rows][cols][slices];
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					mat[i][j][k]=vol.getDouble(i,j,k);
				}
			}
		}
	}
	
	/**
	 * Creates the volume.
	 * 
	 * @return the image data
	 */
	public ImageData createVolume(){
		ImageData vol=new ImageDataMipav("magnitude",VoxelType.DOUBLE,rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					vol.set(i,j,k,mat[i][j][k]);
				}
			}
		}
		return vol;
	}
	
	/**
	 * Instantiates a new matrix3.
	 * 
	 * @param m the m
	 */
	public Matrix3(double[][][] m){
		mat=m;
		rows=mat.length;
		if(rows>0){
			cols=m[0].length;
			slices=(cols>0)?m[0][0].length:0;
		} else cols=slices=0;
	}
	
	/**
	 * Gets the rows.
	 * 
	 * @return the rows
	 */
	public int getRows(){
		return rows;
	}
	
	/**
	 * Gets the cols.
	 * 
	 * @return the cols
	 */
	public int getCols(){
		return cols;
	}
	
	/**
	 * Gets the slices.
	 * 
	 * @return the slices
	 */
	public int getSlices(){
		return slices;
	}
	
	/**
	 * To array.
	 * 
	 * @return the double[][][]
	 */
	public double[][][] toArray(){
		return mat;
	}
	
	/**
	 * Sets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * @param a the a
	 */
	public void set(int i,int j,int k,double a){
		mat[i][j][k]=a;
	}
	
	/**
	 * Gets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * 
	 * @return the double
	 */
	public double get(int i,int j,int k){
		return mat[i][j][k];
	}

	/**
	 * Gets the matrix2d.
	 * 
	 * @return the matrix2d
	 */
	public Matrix2[] getMatrix2d(){
		Matrix2[] mats=new Matrix2[getSlices()];	
		for(int i=0;i<mats.length;i++){
			mats[i]=new Matrix2(mat,i);
		}
		return mats;
	}
	
	/**
	 * Gets the matrix2d.
	 * 
	 * @param i the i
	 * 
	 * @return the matrix2d
	 */
	public Matrix2 getMatrix2d(int i){
		return new Matrix2(mat,i);
	}

	/**
	 * Adds the.
	 * 
	 * @param a the a
	 * 
	 * @return the matrix3
	 */
	public Matrix3 add(double a) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)+a);
				}
			}
		}
		return M;
	}
	
	/**
	 * Sub.
	 * 
	 * @param a the a
	 * 
	 * @return the matrix3
	 */
	public Matrix3 sub(double a) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)-a);
				}
			}
		}
		return M;
	}
	
	/**
	 * Mul.
	 * 
	 * @param a the a
	 * 
	 * @return the matrix3
	 */
	public Matrix3 mul(double a) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)*a);
				}
			}
		}
		return M;
	}
	
	/**
	 * Div.
	 * 
	 * @param a the a
	 * 
	 * @return the matrix3
	 */
	public Matrix3 div(double a) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)/a);
				}
			}
		}
		return M;
	}
	
	/**
	 * Adds the.
	 * 
	 * @param m the m
	 * 
	 * @return the matrix3
	 */
	public Matrix3 add(Matrix3 m) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)+m.get(i,j,k));
				}
			}
		}
		return M;
	}
	
	/**
	 * Sub.
	 * 
	 * @param m the m
	 * 
	 * @return the matrix3
	 */
	public Matrix3 sub(Matrix3 m) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)-m.get(i,j,k));
				}
			}
		}
		return M;
	}
	
	/**
	 * Mul.
	 * 
	 * @param m the m
	 * 
	 * @return the matrix3
	 */
	public Matrix3 mul(Matrix3 m) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)*m.get(i,j,k));
				}
			}
		}
		return M;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Matrix3 clone(){
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k));
				}
			}
		}
		return M;	
	}
	
	/**
	 * Div.
	 * 
	 * @param m the m
	 * 
	 * @return the matrix3
	 */
	public Matrix3 div(Matrix3 m) {
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,get(i,j,k)/m.get(i,j,k));
				}
			}
		}
		return M;
	}
	
	/**
	 * Normalize matrix.
	 * 
	 * @return new normalized matrix
	 */
	public Matrix3 normalize(){
		double min=1E100;
		double max=-1E100;
		double val;
		double scale;
		Matrix3 M=new Matrix3(rows,cols,slices);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					val=get(i,j,k);
					min=Math.min(min,val);
					max=Math.max(max,val);
				}
			}
		}
		scale=((max-min)>0)?(1.0/(max-min)):1;
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					M.set(i,j,k,(get(i,j,k)-min)*scale);
				}
			}
		}
		return M;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String text="{rows="+rows+" cols="+cols+" slices="+slices+"}\n";
		text+="{";
		for(int k=0;k<slices;k++){
			text+="\n[";
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					text+=get(i,j,k)+" ";
				}
				if(i!=cols-1)text+=";\n";
			}
			text+="]\n";
		}	
		text+="}\n";
		return text;
	}
	
	/**
	 * Debugging routine for printing non-zero matrix elements.
	 * 
	 * @param max the max
	 */
	public void printNonZeroString(int max){
		System.out.println(getClass().getCanonicalName()+"\t"+"{rows="+rows+" cols="+cols+" slices="+slices+"}");
		int count=0;
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					if(get(i,j,k)!=0){
						System.out.println(getClass().getCanonicalName()+"\t"+"("+i+","+j+","+k+") "+get(i,j,k));
						count++;
						if(count>=max)return;
					}
				}
			}
		}	
	}
}
