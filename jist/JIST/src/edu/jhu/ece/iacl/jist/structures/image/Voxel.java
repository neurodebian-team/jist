package edu.jhu.ece.iacl.jist.structures.image;
import java.awt.Color;
import java.io.Serializable;

import edu.jhu.ece.iacl.jist.structures.vector.VectorX;

/**
 * Generic Voxel Type.
 * 
 * @author Blake Lucas
 */
public abstract class Voxel extends Number implements Cloneable,Comparable<Voxel>,Serializable{

	/**
	 * Round.
	 * 
	 * @param v the v
	 * @param prec the prec
	 * 
	 * @return the voxel
	 */
	public static Voxel round(Voxel v,int prec){
		double exp=Math.pow(10,prec);
		v.set(Math.round(v.getDouble()*exp)/exp);
		return v;
	}
	
	/**
	 * Determine restriction level for a particular Voxel Data Type
	 * The higher the value, the more restrictive.
	 * 
	 * @param type the type
	 * 
	 * @return the restriction
	 */
	public static int getRestriction(VoxelType type){
		if(type==null)return -1;
		switch(type){
			case BOOLEAN:return 1;
			case UBYTE:return 2;
			case SHORT:return 3;
			case USHORT:return 3;
			case INT:return 4;
			case COLOR:return 4;
			case FLOAT:return 5;
			case DOUBLE:return 6;
			case VECTORX:return 7;
			default:return 0;
		}
	}
	
	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public abstract VoxelType getType();
	
	/**
	 * Gets the boolean.
	 * 
	 * @return the boolean
	 */
	public abstract boolean getBoolean();
	
	/**
	 * Gets the short.
	 * 
	 * @return the short
	 */
	public abstract short getShort();
	
	/**
	 * Gets the color.
	 * 
	 * @return the color
	 */
	public abstract Color getColor();
	
	/**
	 * Gets the double.
	 * 
	 * @return the double
	 */
	public abstract double getDouble();
	
	/**
	 * Gets the vector.
	 * 
	 * @return the vector
	 */
	public VectorX getVector(){
		return null;
	}
	
	/**
	 * Sets the.
	 * 
	 * @param v the v
	 */
	public void set(VectorX v){
	}
	
	/**
	 * Gets the float.
	 * 
	 * @return the float
	 */
	public float getFloat(){
		return (float)getDouble();
	}
	
	/**
	 * Gets the u byte.
	 * 
	 * @return the u byte
	 */
	public short getUByte(){return getShort();}
	
	/**
	 * Gets the int.
	 * 
	 * @return the int
	 */
	public abstract int getInt();
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public abstract Voxel clone();
	
	/**
	 * Sets the.
	 * 
	 * @param v the v
	 */
	public abstract void set(Voxel v);
	
	/**
	 * Sets the.
	 * 
	 * @param vox the vox
	 */
	public abstract void set(boolean vox);
	
	/**
	 * Sets the.
	 * 
	 * @param vox the vox
	 */
	public abstract void set(short vox);
	
	/**
	 * Sets the.
	 * 
	 * @param vox the vox
	 */
	public void set(byte vox){
		short b=vox;
		if(b<0)set(255+b); else set(b);
	}
	
	/**
	 * Sets the.
	 * 
	 * @param a the a
	 */
	public void set(float a){
		set((double)a);
	}
	
	/**
	 * Sets the.
	 * 
	 * @param vox the vox
	 */
	public abstract void set(int vox);
	
	/**
	 * Sets the.
	 * 
	 * @param vox the vox
	 */
	public abstract void set(Color vox);
	
	/**
	 * Sets the.
	 * 
	 * @param vox the vox
	 */
	public abstract void set(double vox);
	
	/**
	 * Adds the.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel
	 */
	public abstract Voxel add(Voxel v);
	
	/**
	 * Sub.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel
	 */
	public abstract Voxel sub(Voxel v);
	
	/**
	 * Mul.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel
	 */
	public abstract Voxel mul(Voxel v);
	
	/**
	 * Div.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel
	 */
	public abstract Voxel div(Voxel v);
	
	/**
	 * Neg.
	 * 
	 * @return the voxel
	 */
	public abstract Voxel neg();
	
	/**
	 * Instantiates a new voxel.
	 */
	public Voxel(){}
	
	/**
	 * Instantiates a new voxel.
	 * 
	 * @param v the v
	 */
	public Voxel(Voxel v){set(v);}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o){
		if(o instanceof Voxel){
			return (compareTo((Voxel)o)==0);
		} else {
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Number#intValue()
	 */
	public int intValue() {
		return getInt();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Number#longValue()
	 */
	public long longValue() {
		return getInt();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Number#floatValue()
	 */
	public float floatValue() {
		return (float)getFloat();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Number#byteValue()
	 */
	public byte byteValue(){
		return (byte)getUByte();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Number#shortValue()
	 */
	public short shortValue(){
		return (short)getUByte();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Number#doubleValue()
	 */
	public double doubleValue() {
		return getDouble();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public abstract String toString();
}
