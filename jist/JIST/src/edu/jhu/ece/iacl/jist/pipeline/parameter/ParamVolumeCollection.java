/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeCollectionFactory;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavPointer;
import edu.jhu.ece.iacl.jist.structures.image.Voxel;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;
import gov.nih.mipav.model.structures.ModelImage;

/**
 * Volume collection stores a collection of files. The restrictions on the files
 * are set to be the same as the collection.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeCollection extends ParamFileCollection {
	
	/** The type. */
	protected VoxelType type;
	
	/** The rows. */
	protected int rows = -1;
	
	/** The cols. */
	protected int cols = -1;
	
	/** The slices. */
	protected int slices = -1;
	
	/** The components. */
	protected int components = -1;

	public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);
		 Element em;			
		 em = document.createElement("type");		
		 em.appendChild(document.createTextNode(type+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("rows");		
		 em.appendChild(document.createTextNode(rows+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("cols");		
		 em.appendChild(document.createTextNode(cols+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("slices");		
		 em.appendChild(document.createTextNode(slices+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("components");		
		 em.appendChild(document.createTextNode(components+""));
		 parent.appendChild(em);
		 
		 return true;
	 }
	
	public void xmlDecodeParam(Document document, Element el) {
		super.xmlDecodeParam(document, el);
		try {
		type = VoxelType.valueOf(JistXMLUtil.xmlReadTag(el, "type"));
		} catch (IllegalArgumentException e){
			type=null;
		}
		rows = Integer.valueOf(JistXMLUtil.xmlReadTag(el, "rows"));
		cols = Integer.valueOf(JistXMLUtil.xmlReadTag(el, "cols"));
		slices = Integer.valueOf(JistXMLUtil.xmlReadTag(el, "slices"));
		components = Integer.valueOf(JistXMLUtil.xmlReadTag(el, "components"));
		
	}
	/**
	 * Default constructor.
	 */
	public ParamVolumeCollection() {
		super();
		readerWriter = new ImageDataReaderWriter();
		extensionFilter = readerWriter.getExtensionFilter();
		this.setMaxIncoming(-1);
		fileParams = new Vector<ParamFile>();
		this.factory = new ParamVolumeCollectionFactory(this);
	}
	
	
	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamVolumeCollection(String name) {
		this();
		setName(name);
	
	}

	/**
	 * Construct volume collection with specified restrictions.
	 * 
	 * @param name
	 *            string name
	 * @param type
	 *            voxel type restriction
	 */
	public ParamVolumeCollection(String name, VoxelType type) {
		this(type);
		setName(name);
	}

	/**
	 * Construct volume collection parameter with a restriction on the
	 * dimensions and data type.
	 * 
	 * @param name
	 *            volume name
	 * @param type
	 *            data type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamVolumeCollection(String name, VoxelType type, int rows, int cols, int slices, int components) {
		this(type, rows, cols, slices, components);
		setName(name);
		
	}

	/**
	 * Constructor.
	 * 
	 * @param type
	 *            voxel type restriction
	 */
	public ParamVolumeCollection(VoxelType type) {
		this();		
		this.type = type;		
	}

	/**
	 * Construct volume collection parameter with a restriction on the
	 * dimensions and data type.
	 * 
	 * @param type
	 *            data type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamVolumeCollection(VoxelType type, int rows, int cols, int slices, int components) {
		this(type);
		this.rows = rows;
		this.slices = slices;
		this.cols = cols;
		this.components = components;
	}


	/**
	 * Add a new value to the collection.
	 * 
	 * @param value
	 *            the value
	 * @return the param volume
	 */
	public void add(Object value) {
		ParamVolume param;
		if (value instanceof ParamVolume) {
			fileParams.add(param = (ParamVolume) value);
		} else {
			param = create(value);
			fileParams.add(param);
		}		
	}

	/**
	 * Remove all files from collection.
	 */
	public void clear() {
		fileParams.clear();
	}

	/**
	 * Clone object.
	 * 
	 * @return the param volume collection
	 */
	public ParamVolumeCollection clone() {
		ParamVolumeCollection param = new ParamVolumeCollection();
		param.rows = rows;
		param.slices = slices;
		param.components = components;
		param.cols = cols;
		param.type = type;
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.fileParams = new Vector<ParamFile>(fileParams.size());
		for (ParamFile p : fileParams) {
			param.fileParams.add(p.clone());
		}
		return param;
	}

	/**
	 * Compare restriction of one volume collection to another.
	 * 
	 * @param model
	 *            the model
	 * @return the int
	 */
	public int compareTo(ParamModel model) {
		if (!(model instanceof ParamVolumeCollection)) {
			return 1;
		}
		List<ParamFile> models = ((ParamVolumeCollection) model).getParameters();
		int ret = Integer.MIN_VALUE;
		for (int i = 0; i < models.size(); i++) {
			ParamVolume modelVol = (ParamVolume) models.get(i);
			int tmp = 0;
			// This field is mandatory and thus immediately more restrictive
			if (!this.isHidden() && modelVol.isHidden()) {
				tmp = 1;
			}
			// Compare the dimensions to see if they match
			if (((rows == -1) || (rows == modelVol.getRows())) && ((cols == -1) || (cols == modelVol.getCols()))
					&& ((slices == -1) || (slices == modelVol.getSlices()))
					&& ((components == -1) || (components == modelVol.getComponents()))) {
				// Compare the data types for the files
				tmp = (int) Math.signum(Voxel.getRestriction(modelVol.getVoxelType()) - Voxel.getRestriction(type));
			} else {
				// this volume is more restrictive than the model
				tmp = 1;
			}
			ret = Math.max(tmp, ret);
		}
		return ret;
	}

	/**
	 * Create a new ParamVolume with the same restrictions as the collection and
	 * the specified value.
	 * 
	 * @param value
	 *            the value
	 * @return the param volume
	 */
	public ParamVolume create(Object value) {
		ParamVolume param = new ParamVolume(getName(), type, rows, cols, slices, components);
		if (value instanceof String) {
			// Either a file or a MIPAV image name. Let's try name first
			ModelImage img = MipavController.getImageByName((String)value);
			if(img!=null) {
				JistLogger.logOutput(JistLogger.FINE, "Creating ParamVolume by ModelImage Name:"+(String)value);
				ImageData dat = new ImageDataMipavPointer(img);
				param.setValue(dat);
				param.setName(dat.getName());
			} else {
			//Ok, default to file/uri/etc
			param.setValue((String) value);
			param.setName((new File((String) value)).getName());
			}
		} else if (value instanceof File) {
			param.setValue((File) value);
			param.setName(((File) value).getName());
		} else if (value instanceof ImageData) {
			param.setValue((ImageData) value);
			param.setName(((ImageData) value).getName());
		}
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	

	/**
	 * Get column restriction.
	 * 
	 * @return the cols
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Get component restriction.
	 * 
	 * @return the components
	 */
	public int getComponents() {
		return components;
	}

	/**
	 * Get list of volume file locations.
	 * 
	 * @return the files
	 */
	public List<File> getFileList() {
		ArrayList<File> list = new ArrayList<File>();
		for (ParamFile vol : fileParams) {
			list.add(((ParamVolume) vol).getValue());
		}
		return list;
	}

	/**
	 * Get list of model image names.
	 * 
	 * @return the image names
	 */
	public List<String> getImageNames() {
		ArrayList<String> list = new ArrayList<String>();
		for (ParamFile vol : fileParams) {
			if (vol != null) {		
				list.add(((ParamVolume)vol).getName());
			}
		}
		return list;
	}

	/**
	 * Get list of model images.
	 * 
	 * @return the model images
	 */
	/*public List<ModelImage> getModelImageCacheList() {
		ArrayList<ModelImage> list = new ArrayList<ModelImage>();
		for (ParamFile vol : fileParams) {
			list.add(((ParamVolume) vol).getModelImageCache());
		}
		return list;
	}
*/
	/**
	 * Get row restriction.
	 * 
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Get slice restriction.
	 * 
	 * @return the slices
	 */
	public int getSlices() {
		return slices;
	}

	/**
	 * Get voxel type restriction.
	 * 
	 * @return the type
	 */
	public VoxelType getType() {
		return type;
	}

	/**
	 * Get list of volume files.
	 * 
	 * @return the value
	 */
	public List<File> getValue() {
		return getFileList();
	}

	/**
	 * Get file.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	public File getValue(int i) {
		if (i < fileParams.size()) {
			return fileParams.get(i).getValue();
		} else {
			return null;
		}
	}

	
	public ParamVolume getParamVolume(int i) {
		if (i < fileParams.size()) {
			return (ParamVolume) fileParams.get(i);
		} else {
			return null;
		}
	}
	
	/**
	 * Get list of volumes.
	 * 
	 * @return the volumes
	 */
	public List<ImageData> getImageDataList() {
		ArrayList<ImageData> list = new ArrayList<ImageData>();
		for (ParamFile vol : fileParams) {
			list.add(((ParamVolume) vol).getImageData());
		}
		return list;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(-1);
		connectible = true;
		factory = new ParamVolumeCollectionFactory(this);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection#isCompatible(edu.jhu.ece.iacl.jist.pipeline.PipePort)
	 */
	public boolean isCompatible(PipePort model) {
		return ((model instanceof ParamVolume) || (model instanceof ParamVolumeCollection));
	}


	/**
	 * Get size of volume collection.
	 * 
	 * @return the int
	 */
	public int size() {
		return fileParams.size();
	}



	/**
	 * Validate that the volume meets all restrictions.
	 * 
	 * @throws InvalidParameterException
	 *             parameter does not meet value restrictions
	 */
	public void validate() throws InvalidParameterException {
		if (!isHidden() && (fileParams.size() == 0)) {
			throw new InvalidParameterException(this, "No Volume Specified.");
		}
		for (ParamFile vol : fileParams) {
			vol.setLoadAndSaveOnValidate(loadAndSaveOnValidate);
			vol.validate();
		}
	}
	
	public void writeAndFreeNow(ProcessingAlgorithm src) {
		if(!loadAndSaveOnValidate) {
			System.out.println(getClass().getCanonicalName()+"\t"+"writeAndFreeNow: volumeCollection: "+getName());
			List<ParamVolume> params = getParamVolumeList();
			for(ParamVolume param : params) {
				System.out.println(getClass().getCanonicalName()+"\t"+"\t"+param.getName());
				param.setLoadAndSaveOnValidate(loadAndSaveOnValidate);
				param.writeAndFreeNow(src);
			}			
		}
	}
	
	public List<ParamVolume> getParamVolumeList() {
		ArrayList<ParamVolume> list = new ArrayList<ParamVolume>();
		for (ParamFile vol : fileParams) {
			list.add(((ParamVolume) vol));
		}
		return list;
	}
	
}
