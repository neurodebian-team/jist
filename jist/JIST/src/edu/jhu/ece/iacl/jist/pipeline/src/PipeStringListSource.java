/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMultiOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of double values.
 * 
 * @author Blake Lucas
 */
public class PipeStringListSource extends PipeListSource {
	
	/** The string param. */
	protected ParamString stringParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("stringParam");
//		stringParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		stringParam = (ParamString)outputParams.getFirstChildByName("String");
//		stringParam = new ParamString();
//		stringParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"stringParam"));
		getParentPort().setParameter(stringParam);
	}
	/**
	 * Default constructor.
	 */
	public PipeStringListSource() {
		super();
		getParentPort().setParameter(stringParam);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.src.PipeListSource#createInputParams()
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("String File List");
		group.setName("stringlist");
		group.setCategory("String");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float");
		group.add(stringParam = new ParamString("String"));
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#getOutputParam()
	 */
	public ParamString getOutputParam() {
		return stringParam;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.src.PipeListSource#getValue(int)
	 */
	protected Object getValue(int i) {
		if (off < data[i].length) {
			return (data[i][off]);
		} else {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.src.PipeListSource#setValue(java.lang.Object)
	 */
	protected void setValue(Object obj) {
		stringParam.setValue(obj.toString());
	}
}
