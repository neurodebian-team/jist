/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmNode;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeDestinationFactory.DestinationNode;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeSourceFactory.SourceNode;
import edu.jhu.ece.iacl.jist.pipeline.tree.PackageNode;

/**
 * Panel for searching modules
 * 
 * @author Min Chen
 */
public class ModuleSearchPanel extends JPanel implements ActionListener, KeyListener {

	private DefaultTreeModel originalTree = null;
	/** Text box to put search terms. */
	private JTextField searchField;	

	/** Button to push to search */
	private JButton searchButton;
	private JButton cancelButton;
	
	private Window parentFrame;

	/** Dragdown box to determine where to search */
	private JComboBox searchType;

	/** Available options in searchType */
	String[] searchOptions= { "By Name", "By Description", "By Category", "By Author","By Class" } ;


	/** The panel. */
	private static ModuleSearchPanel panel = null;

	/**
	 * Get singleton reference to tree panel.
	 * 
	 * @return the instance
	 */
	public static ModuleSearchPanel getInstance(Window parent,JTree jtree) {
		if (panel == null) {
			panel = new ModuleSearchPanel(parent,jtree);
		}
		return panel;
	}

	/** The layout. */
//	LayoutPanel layout;
//	DefaultTreeModel ((DefaultTreeModel)jTreeModel.getModel());
	JTree jTreeModel;
	/**
	 * Default constructor.
	 */
	protected ModuleSearchPanel(Window parent, JTree jTreeModel) {
		createPane();
//		this.layout = LayoutPanel.getInstance();
		parentFrame=parent;
//		((DefaultTreeModel)jTreeModel.getModel()) = (DefaultTreeModel)jTreeModel.getModel();
		this.jTreeModel =jTreeModel;
	}

	/**
	 * Create panel to display preferences.
	 */
	protected void createPane() {
		this.setLayout(new BorderLayout());
		GridBagConstraints c = new GridBagConstraints();
		JPanel small = new JPanel();
		BoxLayout layout = new BoxLayout(small, BoxLayout.PAGE_AXIS);
		small.setLayout(layout);
		this.add(small, BorderLayout.NORTH);
		JPanel itemPane = new JPanel(new GridBagLayout());

		Vector<String> searchOptionsVector = new Vector<String>(Arrays.asList(searchOptions));

		c.fill = GridBagConstraints.HORIZONTAL;		
		itemPane.add(searchType = new JComboBox(searchOptionsVector), c);
		c.weightx=1;
		c.weighty=1;
		itemPane.add(searchField = new JTextField(20), c);
		c.weightx=0;
		c.weighty=0;
		itemPane.add(searchButton = new JButton("Filter"), c);
		
		itemPane.add(cancelButton = new JButton("Reset"), c);
		cancelButton.setEnabled(false);
		small.add(itemPane);

		searchButton.addActionListener(this);
		cancelButton.addActionListener(this);
		searchField.addKeyListener(this);
	}		

	private DefaultTreeModel copyTree(DefaultTreeModel src) {
		return new DefaultTreeModel(deepCopy((DefaultMutableTreeNode)src.getRoot()));
		//		int children = src.getChildCount(src.getRoot());
		//		DefaultTreeModel ret=null;
		//		for(int c =0;c<children;c++) {
		//			TreeNode n = (TreeNode) src.getChild(src.getRoot(), c);
		//			TreeNode n2 = null;
		//			if(n instanceof DefaultMutableTreeNode) {
		//				 n2 = deepCopy((DefaultMutableTreeNode)n);
		//			} else {
		//				System.out.println(n.getClass().toString());
		//			}
		//			if(c==0) {				
		//				ret = new DefaultTreeModel(n2);
		//			} else
		//				ret.insertNodeInto((MutableTreeNode) n2, (MutableTreeNode)ret.getRoot(), c-1);
		//		}
		//		return ret;
	}

	private DefaultMutableTreeNode deepCopy(DefaultMutableTreeNode n) {
		DefaultMutableTreeNode n2 = (DefaultMutableTreeNode)n.clone();//new DefaultMutableTreeNode(n.getUserObject(),n.getAllowsChildren());

		if(n.getAllowsChildren()) {
			for(int i=0;i<n.getChildCount();i++) {
				n2.add(deepCopy((DefaultMutableTreeNode)n.getChildAt(i)));
			}
		}		
		return n2;
	}

	public void actionPerformed(ActionEvent evt) {
		try {

			parentFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			DefaultMutableTreeNode root = (DefaultMutableTreeNode) ((DefaultTreeModel)jTreeModel.getModel()).getRoot();
			if(originalTree==null) { 
				originalTree=copyTree(((DefaultTreeModel)jTreeModel.getModel()));
				cancelButton.setEnabled(true);
			};
			if (evt.getSource() == searchButton) {

				int searchTypeIndex = searchType.getSelectedIndex(); 
				switch(searchTypeIndex){

				case 0://Search by Module Name
					filterTreeByName(root,searchField.getText());
					((DefaultTreeModel)jTreeModel.getModel()).nodeStructureChanged(root);
					break;

				case 1://Search by Module Descriptions
					filterTreeByDescription(root,searchField.getText());
					((DefaultTreeModel)jTreeModel.getModel()).nodeStructureChanged(root);
					break;

				case 2://Search by Category	
					filterTreeByCategory(root,searchField.getText(),"");
					((DefaultTreeModel)jTreeModel.getModel()).nodeStructureChanged(root);				
					break;

				case 3://Search by Author	
					filterTreeByAuthor(root,searchField.getText());
					((DefaultTreeModel)jTreeModel.getModel()).nodeStructureChanged(root);				
					break;

				case 4://Search by Class
					filterTreeByClass(root,searchField.getText());
					((DefaultTreeModel)jTreeModel.getModel()).nodeStructureChanged(root);				
					break;
				}
				
				expandTree();
			} else if(evt.getSource()==cancelButton) {			
				// DROP EXISTING TREE
				//			while(((DefaultTreeModel)jTreeModel.getModel()).getChildCount(root)>0) {
				//				MutableTreeNode obj = (MutableTreeNode) ((DefaultTreeModel)jTreeModel.getModel()).getChild(root, 0);
				//				((DefaultTreeModel)jTreeModel.getModel()).removeNodeFromParent(obj);
				//			}
				root.removeAllChildren();
				// REPLACE WITH ORIGINAL TREE
				int cnt =0;
				while(originalTree.getChildCount(originalTree.getRoot())>0) {
					root.add((DefaultMutableTreeNode)originalTree.getChild(originalTree.getRoot(), 0));
					//				((DefaultTreeModel)jTreeModel.getModel()).insertNodeInto(
					//						(DefaultMutableTreeNode)originalTree.getChild(originalTree.getRoot(), 0),root, 0);
					//				cnt++;
				}
				((DefaultTreeModel)jTreeModel.getModel()).nodeStructureChanged(root);
				//			while(((DefaultTreeModel)jTreeModel.getModel()).getChildCount(root)>cnt) {
				//				MutableTreeNode obj = (MutableTreeNode) ((DefaultTreeModel)jTreeModel.getModel()).getChild(root, cnt);
				//				((DefaultTreeModel)jTreeModel.getModel()).removeNodeFromParent(obj);
				//			}
				originalTree=null;
				cancelButton.setEnabled(false);
			}
		} 	finally {
			parentFrame.setCursor(Cursor.getDefaultCursor());
		}


	}

	private void expandTree() {

		  int row = 0;
		    while (row < jTreeModel.getRowCount()) {
		    	jTreeModel.expandRow(row);
		        row++;
		    }
	}
		
	

	private void filterTreeByName(DefaultMutableTreeNode root, String text) {
		String prefix = "(?i).*";
		String suffix = ".*";	
		Vector<DefaultMutableTreeNode> v = new Vector<DefaultMutableTreeNode>();
		for(int i=0;i<root.getChildCount();i++)
			v.add((DefaultMutableTreeNode)root.getChildAt(i));
		for(DefaultMutableTreeNode c : v)
			filterTreeByName(c,text);
		//		}

		if(root.isRoot()) {
			// Don't do anything special. 
			System.out.println("root");
		} else if(root.isLeaf()) {						
			if(root instanceof AlgorithmNode) {
				if(!((AlgorithmNode)root).getName().matches(prefix+text+suffix)) {
					root.removeFromParent();
				} else {
					//					System.out.println("Keep : "+((AlgorithmNode)root).getName());
				}

			} else if(root instanceof PackageNode) {

				root.removeFromParent();

			} else if(root instanceof SourceNode) {
				if(!((SourceNode)root).getName().matches(prefix+text+suffix)) {
					root.removeFromParent();
				} else {
					//					System.out.println("Keep : "+((SourceNode)root).getName());
				}
			} else if(root instanceof DestinationNode) {
				if(!((DestinationNode)root).getName().matches(prefix+text+suffix)) {
					root.removeFromParent();
				}else {
					//					System.out.println("Keep : "+((DestinationNode)root).getName());
				}
			} else if(root instanceof DefaultMutableTreeNode) {
				root.removeFromParent();
			}else {
				System.out.println(root.getClass().toString());
			}
		} else {
			if(root.getChildCount()==0)
				root.removeFromParent();
		}
	}

	private void filterTreeByDescription(DefaultMutableTreeNode root, String text) {
		String prefix = "(?i).*";
		String suffix = ".*";	
		Vector<DefaultMutableTreeNode> v = new Vector<DefaultMutableTreeNode>();
		for(int i=0;i<root.getChildCount();i++)
			v.add((DefaultMutableTreeNode)root.getChildAt(i));
		for(DefaultMutableTreeNode c : v)
			filterTreeByDescription(c,text);

		if(root.isRoot()) { 
			System.out.println("root");
		} else if(root.isLeaf()) {						
			if(root instanceof AlgorithmNode) {
				try {
					
					//					PipeAlgorithmFactory p = new PipeAlgorithmFactory();
					//				AlgorithmInformation info =((PipeAlgorithmFactory)((AlgorithmNode)root).getUserObject()).createPipe().getAlgorithm().getAlgorithmInformation();
					//					PipeAlgorithmFactory.getAlgInformation(((AlgorithmNode)root).getMapFile()));				
					String str=((AlgorithmNode)root).getDescription();

					if(!(str.matches(prefix+text+suffix))) {
						root.removeFromParent();
					} else {
						//					System.out.println("Keep: "+root.getClass().toString());
					}
				} catch(NullPointerException e){
					root.removeFromParent();
				} catch(ClassCastException e) {
					root.removeFromParent();
				} 
			} else if(root instanceof PackageNode) {
				root.removeFromParent();
			} else if(root instanceof SourceNode) {
				root.removeFromParent();
			} else if(root instanceof DestinationNode) {
				root.removeFromParent();
			} else if(root instanceof DefaultMutableTreeNode) {
				root.removeFromParent();
			}else {
				System.out.println(root.getClass().toString());
			}
		} else {
			if(root.getChildCount()==0)
				root.removeFromParent();
		}
	}

	private void filterTreeByClass(DefaultMutableTreeNode root, String text) {
		String prefix = "(?i).*";
		String suffix = ".*";	
		Vector<DefaultMutableTreeNode> v = new Vector<DefaultMutableTreeNode>();
		for(int i=0;i<root.getChildCount();i++)
			v.add((DefaultMutableTreeNode)root.getChildAt(i));
		for(DefaultMutableTreeNode c : v)
			filterTreeByClass(c,text);

		if(root.isRoot()) { 
			System.out.println("root");
		} else if(root.isLeaf()) {						
			if(root instanceof AlgorithmNode) {
				try {
					//					PipeAlgorithmFactory p = new PipeAlgorithmFactory();
					//				AlgorithmInformation info =((PipeAlgorithmFactory)((AlgorithmNode)root).getUserObject()).createPipe().getAlgorithm().getAlgorithmInformation();
					//					PipeAlgorithmFactory.getAlgInformation(((AlgorithmNode)root).getMapFile()));
					PipeAlgorithmFactory fac =(new PipeAlgorithmFactory(((AlgorithmNode)root).getMapFile()));								
					String str=fac.getModuleClass().getCanonicalName();

					if(!(str.matches(prefix+text+suffix))) {
						root.removeFromParent();
					} else {
						//					System.out.println("Keep: "+root.getClass().toString());
					}
				} catch(NullPointerException e){
					root.removeFromParent();
				} catch(ClassCastException e) {
					root.removeFromParent();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if(root instanceof PackageNode) {
				root.removeFromParent();
			} else if(root instanceof SourceNode) {
				root.removeFromParent();
			} else if(root instanceof DestinationNode) {
				root.removeFromParent();
			} else if(root instanceof DefaultMutableTreeNode) {
				root.removeFromParent();
			}else {
				System.out.println(root.getClass().toString());
			}
		} else {
			if(root.getChildCount()==0)
				root.removeFromParent();
		}
	}


	private void filterTreeByCategory(DefaultMutableTreeNode root, String text,String treeSoFar) {
		String prefix = "(?i).*";
		String suffix = ".*";	
		Vector<DefaultMutableTreeNode> v = new Vector<DefaultMutableTreeNode>();
		for(int i=0;i<root.getChildCount();i++)
			v.add((DefaultMutableTreeNode)root.getChildAt(i));
		for(DefaultMutableTreeNode c : v)
			filterTreeByCategory(c,text,treeSoFar+"."+root.getUserObject().toString());

		if(root.isRoot()) { 
			System.out.println("root");
		} else if(root.isLeaf()) {						
			if(root instanceof AlgorithmNode) {
				try {
					//					PipeAlgorithmFactory p = new PipeAlgorithmFactory();
					//				AlgorithmInformation info =((PipeAlgorithmFactory)((AlgorithmNode)root).getUserObject()).createPipe().getAlgorithm().getAlgorithmInformation();
					//					PipeAlgorithmFactory.getAlgInformation(((AlgorithmNode)root).getMapFile()));
					//				PipeAlgorithmFactory fac =(new PipeAlgorithmFactory(((AlgorithmNode)root).getMapFile()));
					//				AlgorithmInformation info = fac.getAlgInformation();
					String str=treeSoFar;

					if(!(str.matches(prefix+text+suffix))) {
						root.removeFromParent();
					} else {
						//					System.out.println("Keep: "+root.getClass().toString());
					}
				} catch(NullPointerException e){
					root.removeFromParent();
				} catch(ClassCastException e) {
					root.removeFromParent();
				}
			} else if(root instanceof PackageNode) {
				root.removeFromParent();
			} else if(root instanceof SourceNode) {
				root.removeFromParent();
			} else if(root instanceof DestinationNode) {
				root.removeFromParent();
			} else if(root instanceof DefaultMutableTreeNode) {
				root.removeFromParent();
			}else {
				System.out.println(root.getClass().toString());
			}
		} else {
			if(root.getChildCount()==0)
				root.removeFromParent();
		}
	}
	private void filterTreeByAuthor(DefaultMutableTreeNode root, String text) {
		String prefix = "(?i).*";
		String suffix = ".*";	
		Vector<DefaultMutableTreeNode> v = new Vector<DefaultMutableTreeNode>();
		for(int i=0;i<root.getChildCount();i++)
			v.add((DefaultMutableTreeNode)root.getChildAt(i));
		for(DefaultMutableTreeNode c : v)
			filterTreeByAuthor(c,text);

		if(root.isRoot()) { 
			System.out.println("root");
		} else if(root.isLeaf()) {						
			if(root instanceof AlgorithmNode) {
				try {
					//					PipeAlgorithmFactory p = new PipeAlgorithmFactory();
					//				AlgorithmInformation info =((PipeAlgorithmFactory)((AlgorithmNode)root).getUserObject()).createPipe().getAlgorithm().getAlgorithmInformation();
					//					PipeAlgorithmFactory.getAlgInformation(((AlgorithmNode)root).getMapFile()));
					PipeAlgorithmFactory fac =(new PipeAlgorithmFactory(((AlgorithmNode)root).getMapFile()));				
					AlgorithmInformation info = fac.getAlgInformation();
					String str="";
					for(AlgorithmAuthor a : info.getAuthors()) {
					str+=a.toString()+ " ";
					}
					
					
					if(!(str.replaceAll("\\n", "").replaceAll("\\(", "").replaceAll("\\)", "").matches(prefix+text+suffix))) {
						root.removeFromParent();
					} else {
						//					System.out.println("Keep: "+root.getClass().toString());
					}
				} catch(NullPointerException e){
					root.removeFromParent();
				} catch(ClassCastException e) {
					root.removeFromParent();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if(root instanceof PackageNode) {
				root.removeFromParent();
			} else if(root instanceof SourceNode) {
				root.removeFromParent();
			} else if(root instanceof DestinationNode) {
				root.removeFromParent();
			} else if(root instanceof DefaultMutableTreeNode) {
				root.removeFromParent();
			}else {
				System.out.println(root.getClass().toString());
			}
		} else {
			if(root.getChildCount()==0)
				root.removeFromParent();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {		
		// TODO Auto-generated method stub
		if(e.getKeyChar()=='\n') {
			actionPerformed(new ActionEvent(searchButton, ActionEvent.ACTION_PERFORMED, ""));
		} else if(e.isControlDown() && e.getKeyChar()==18) {
			actionPerformed(new ActionEvent(cancelButton, ActionEvent.ACTION_PERFORMED, ""));
			searchField.setText("");
		}
		
	}

	public void setTree(JTree tree) {
		// TODO Auto-generated method stub
		this.jTreeModel=tree;
	}
}