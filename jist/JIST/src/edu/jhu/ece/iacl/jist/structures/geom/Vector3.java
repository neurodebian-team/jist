/**
 * 
 */
package edu.jhu.ece.iacl.jist.structures.geom;

import javax.vecmath.Vector3f;

// TODO: Auto-generated Javadoc
/**
 * The Class Vector3.
 * 
 * @author Blake Lucas
 */
public class Vector3 extends Vector3f{
	
	/**
	 * Instantiates a new vector3.
	 */
	public Vector3(){
		super();
	}
	
	/**
	 * Instantiates a new vector3.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public Vector3(double x,double y,double z){
		super((float)x,(float)y,(float)z);
	}
	
	/**
	 * Instantiates a new vector3.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public Vector3(float x,float y,float z){
		super(x,y,z);
	}
	
	/**
	 * Instantiates a new vector3.
	 * 
	 * @param d the d
	 */
	public Vector3(double[] d){
		super((float)d[0],(float)d[1],(float)d[2]);
	}

}
