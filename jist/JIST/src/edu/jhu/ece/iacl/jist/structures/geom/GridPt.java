package edu.jhu.ece.iacl.jist.structures.geom;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;

// TODO: Auto-generated Javadoc
/**
 * The Class GridPt.
 */
public class GridPt extends Point3i implements Comparable{

	/** The data. */
	protected double data;
	
	/**
	 * The Enum Connectivity.
	 */
	public enum Connectivity{/** The SIX. */
SIX, /** The EIGHTEEN. */
 EIGHTEEN, /** The TWENTYSIX. */
 TWENTYSIX };
	
	/** The conn. */
	protected Connectivity conn;
	
	/**
	 * Instantiates a new grid pt.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public GridPt(int x, int y, int z){
		super(x,y,z);
	}
	
	/**
	 * Instantiates a new grid pt.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param d the d
	 */
	public GridPt(int x, int y, int z, double d){
		super(x,y,z);
		this.data=d;
	}
	
	/**
	 * Instantiates a new grid pt.
	 * 
	 * @param pt the pt
	 */
	public GridPt(Point3f pt){
		super(Math.round(pt.x),Math.round(pt.y),Math.round(pt.z));
	}
	
	/**
	 * Instantiates a new grid pt.
	 * 
	 * @param pt the pt
	 * @param d the d
	 */
	public GridPt(Point3f pt, double d){
		super(Math.round(pt.x),Math.round(pt.y),Math.round(pt.z));
		this.data=d;
	}
	
	/**
	 * Instantiates a new grid pt.
	 * 
	 * @param pt the pt
	 */
	public GridPt(XYZ pt){
		super(Math.round(pt.x),Math.round(pt.y),Math.round(pt.z));
	}
	
	/**
	 * Instantiates a new grid pt.
	 * 
	 * @param pt the pt
	 * @param d the d
	 */
	public GridPt(XYZ pt, double d){
		super(Math.round(pt.x),Math.round(pt.y),Math.round(pt.z));
		this.data=d;
	}
	
	/**
	 * Magnitude.
	 * 
	 * @return the double
	 */
	public double magnitude(){
		return Math.sqrt((x*x)+(y*y)+(z*z));
	}
	
	/**
	 * Distance.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * 
	 * @return the double
	 */
	public double distance(double x, double y, double z){
		return distance(this.x,this.y,this.z,x,y,z);
	}
	
	/**
	 * Distance.
	 * 
	 * @param otherpt the otherpt
	 * 
	 * @return the double
	 */
	public double distance(GridPt otherpt){
		return distance(otherpt.x, otherpt.y, otherpt.z);
	}
	
	/**
	 * Distance.
	 * 
	 * @param otherpt the otherpt
	 * @param resX the res x
	 * @param resY the res y
	 * @param resZ the res z
	 * 
	 * @return the double
	 */
	public double distance(GridPt otherpt, float resX, float resY, float resZ){
		return distance(resX*otherpt.x, resY*otherpt.y, resZ*otherpt.z,resX*x, resY*y, resZ*z);
	}
	
	/**
	 * Distance.
	 * 
	 * @param otherpt the otherpt
	 * 
	 * @return the double
	 */
	public double distance(XYZ otherpt){
		return distance(x, y, z, otherpt.x, otherpt.y, otherpt.z);
	}
	
	/**
	 * Vector to.
	 * 
	 * @param otherpt the otherpt
	 * 
	 * @return the grid pt
	 */
	public GridPt vectorTo(GridPt otherpt){
		return new GridPt(otherpt.x-this.x, otherpt.y-this.y, otherpt.z-this.z);
	}
	
	/**
	 * Vector from.
	 * 
	 * @param otherpt the otherpt
	 * 
	 * @return the grid pt
	 */
	public GridPt vectorFrom(GridPt otherpt){
		return new GridPt(this.x-otherpt.x, this.y-otherpt.y, this.z-otherpt.z);
	}
	
	/**
	 * Gets the data.
	 * 
	 * @return the data
	 */
	public double getData(){
		return data;
	}
	
	/**
	 * Sets the data.
	 * 
	 * @param d the new data
	 */
	public void setData(float d){
		data=d;
	}
	
	/**
	 * Sets the connectivity.
	 * 
	 * @param c the new connectivity
	 */
	public void setConnectivity(Connectivity c){
		this.conn=c;
	}
	
	/**
	 * To polar coord.
	 * 
	 * @return the double[]
	 */
	public double[] toPolarCoord(){
		return toPolarCoord(this);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o){
		if(o.getClass()==this.getClass()){
//			if(this.data!=null && ((GridPt)o).data!=null){
//			if(new Float(data)==null || new Float(((GridPt)o).data)==null){
				if(this.x>((GridPt)o).x){
					return 1;
				}else if(this.x<((GridPt)o).x){
					return -1;
				}else{
					if(this.y>((GridPt)o).y){
						return 1;
					}else if(this.y<((GridPt)o).y){
						return -1;
					}else{
						if(this.z>((GridPt)o).z){
							return 1;
						}else if(this.z<((GridPt)o).z){
							return -1;
						}else{
							return 0;
						}
					}
				}
//			}else{
//				if(((GridPt)o).data==this.data){
//					return 0;
//				}else if(this.data > ((GridPt)o).data){
//					return 1;
//				}else{
//					return -1;	
//				}
//			}
		}else{
			return -1;
		}
	}
//	public int compareTo(Object o){
//		if(o.getClass()==this.getClass()){
//
//			if(((GridPt)o).data==this.data){
//				return 0;
//			}else if(this.data > ((GridPt)o).data){
//				return 1;
//			}else{
//				return -1;	
//			}
//
//		}else{
//			return -1;
//		}
//	}
	/**
 * To polar coord.
 * 
 * @param pt the pt
 * 
 * @return the double[]
 */
public static double[] toPolarCoord(GridPt pt){
		double[] pcoord = new double[3];
		pcoord[0] = Math.sqrt(pt.x*pt.x + pt.y*pt.y + pt.z*pt.z);
		pcoord[1] = Math.atan(pt.y/pt.x);
		pcoord[2] = Math.acos(pt.z/pcoord[0]);
		
		return pcoord;
	}
	
	/**
	 * To polar coord.
	 * 
	 * @param pt the pt
	 * 
	 * @return the double[]
	 */
	public static double[] toPolarCoord(Point3f pt){
		double[] pcoord = new double[3];
		pcoord[0] = Math.sqrt(pt.x*pt.x + pt.y*pt.y + pt.z*pt.z);
		if(pt.x>0 && pt.y>=0){
			pcoord[1] = Math.atan(pt.y/pt.x);
		}else if(pt.x>0 && pt.y<0){
			pcoord[1] = Math.atan(pt.y/pt.x)+2*Math.PI;
		}else if(pt.x<0 ){
			pcoord[1] = Math.atan(pt.y/pt.x)+Math.PI;
		}else if(pt.x==0 && pt.y>0){
			pcoord[1] = Math.PI/2;
		}
		else{
			pcoord[1] = 3*Math.PI/2;;
		}
		
		if(pt.z!=0){
			pcoord[2] = Math.atan(Math.sqrt(pt.x*pt.x+pt.y*pt.y)/pt.z);
		}else{
			pcoord[2] = 0;
		}
		return pcoord;
	}
	
	/**
	 * Sqrdistance.
	 * 
	 * @param otherpt the otherpt
	 * 
	 * @return the double
	 */
	public double sqrdistance(XYZ otherpt){
		return sqrdistance(x, y, z, otherpt.x, otherpt.y, otherpt.z); 
	}
	
	/**
	 * Distance.
	 * 
	 * @param x1 the x1
	 * @param y1 the y1
	 * @param z1 the z1
	 * @param x2 the x2
	 * @param y2 the y2
	 * @param z2 the z2
	 * 
	 * @return the double
	 */
	public static double distance(double x1, double y1, double z1, double x2, double y2, double z2){
		return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
	}
	
	/**
	 * Sqrdistance.
	 * 
	 * @param x1 the x1
	 * @param y1 the y1
	 * @param z1 the z1
	 * @param x2 the x2
	 * @param y2 the y2
	 * @param z2 the z2
	 * 
	 * @return the double
	 */
	public static double sqrdistance(double x1, double y1, double z1, double x2, double y2, double z2){
		return (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1);
	}
	
	/**
	 * Equals.
	 * 
	 * @param pt the pt
	 * 
	 * @return true, if successful
	 */
	public boolean equals(GridPt pt){
		return (pt.x==x)&&(pt.y==y)&&(pt.z==z);
	}
	
	/**
	 * Neighborhood6 c.
	 * 
	 * @return the grid pt[]
	 */
	public GridPt[] neighborhood6C(){
		return neighborhood6C(this);
	}
	
	/**
	 * Neighborhood18 c.
	 * 
	 * @return the grid pt[]
	 */
	public GridPt[] neighborhood18C(){
		return neighborhood18C(this);
	}
	
	/**
	 * Neighborhood26 c.
	 * 
	 * @return the grid pt[]
	 */
	public GridPt[] neighborhood26C(){
		return neighborhood26C(this);
	}
	
	/**
	 * Neighborhood.
	 * 
	 * @return the grid pt[]
	 */
	public GridPt[] neighborhood(){
		switch (conn){
		case SIX: return neighborhood6C(this);
		case EIGHTEEN: return neighborhood18C(this);
		case TWENTYSIX: return neighborhood26C(this);
		default: return neighborhood6C(this);
		}
	}
	
	/**
	 * Neighborhood6 c.
	 * 
	 * @param pt the pt
	 * 
	 * @return the grid pt[]
	 */
	public static GridPt[] neighborhood6C(GridPt pt){
		GridPt[] nbhd = new GridPt[6];
		
		nbhd[0] = new GridPt(pt.x-1,pt.y,pt.z);
		nbhd[1] = new GridPt(pt.x,pt.y-1,pt.z);
		nbhd[2] = new GridPt(pt.x,pt.y,pt.z-1);
		nbhd[3] = new GridPt(pt.x+1,pt.y,pt.z);
		nbhd[4] = new GridPt(pt.x,pt.y+1,pt.z);
		nbhd[5] = new GridPt(pt.x,pt.y,pt.z+1);

		return nbhd;
	}
	
	/**
	 * Neighborhood6 c.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * 
	 * @return the grid pt[]
	 */
	public static GridPt[] neighborhood6C(int i, int j, int k){
		GridPt[] nbhd = new GridPt[6];
		
		nbhd[0] = new GridPt(i-1,j,k);
		nbhd[1] = new GridPt(i,j-1,k);
		nbhd[2] = new GridPt(i,j,k-1);
		nbhd[3] = new GridPt(i+1,j,k);
		nbhd[4] = new GridPt(i,j+1,k);
		nbhd[5] = new GridPt(i,j,k+1);

		return nbhd;
	}
	
	/**
	 * Neighborhood18 c.
	 * 
	 * @param pt the pt
	 * 
	 * @return the grid pt[]
	 */
	public static GridPt[] neighborhood18C(GridPt pt){
		GridPt[] nbhd = new GridPt[18];
		
		nbhd[0] = new GridPt(pt.x-1,pt.y,pt.z);
		nbhd[1] = new GridPt(pt.x,pt.y-1,pt.z);
		nbhd[2] = new GridPt(pt.x,pt.y,pt.z-1);
		nbhd[3] = new GridPt(pt.x+1,pt.y,pt.z);
		nbhd[4] = new GridPt(pt.x,pt.y+1,pt.z);
		nbhd[5] = new GridPt(pt.x,pt.y,pt.z+1);
		
		nbhd[6] = new GridPt(pt.x-1,pt.y-1,pt.z);
		nbhd[7] = new GridPt(pt.x-1,pt.y,pt.z-1);
		nbhd[8] = new GridPt(pt.x-1,pt.y+1,pt.z);
		nbhd[9] = new GridPt(pt.x-1,pt.y,pt.z+1);

		nbhd[10]= new GridPt(pt.x+1,pt.y-1,pt.z);
		nbhd[11]= new GridPt(pt.x+1,pt.y,pt.z-1);
		nbhd[12]= new GridPt(pt.x+1,pt.y+1,pt.z);
		nbhd[13]= new GridPt(pt.x+1,pt.y,pt.z+1);
		
		nbhd[14]= new GridPt(pt.x,pt.y-1,pt.z);
		nbhd[15]= new GridPt(pt.x,pt.y,pt.z-1);
		nbhd[16]= new GridPt(pt.x,pt.y+1,pt.z);
		nbhd[17]= new GridPt(pt.x,pt.y,pt.z+1);
		
		return nbhd;
	}
	
	/**
	 * Neighborhood18 c.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * 
	 * @return the grid pt[]
	 */
	public static GridPt[] neighborhood18C(int i, int j, int k){
		GridPt[] nbhd = new GridPt[18];
		
		nbhd[0] = new GridPt(i-1,j,k);
		nbhd[1] = new GridPt(i,j-1,k);
		nbhd[2] = new GridPt(i,j,k-1);
		nbhd[3] = new GridPt(i+1,j,k);
		nbhd[4] = new GridPt(i,j+1,k);
		nbhd[5] = new GridPt(i,j,k+1);
		
		nbhd[6] = new GridPt(i-1,j-1,k);
		nbhd[7] = new GridPt(i-1,j,k-1);
		nbhd[8] = new GridPt(i-1,j+1,k);
		nbhd[9] = new GridPt(i-1,j,k+1);

		nbhd[10]= new GridPt(i+1,j-1,k);
		nbhd[11]= new GridPt(i+1,j,k-1);
		nbhd[12]= new GridPt(i+1,j+1,k);
		nbhd[13]= new GridPt(i+1,j,k+1);
		
		nbhd[14]= new GridPt(i,j-1,k);
		nbhd[15]= new GridPt(i,j,k-1);
		nbhd[16]= new GridPt(i,j+1,k);
		nbhd[17]= new GridPt(i,j,k+1);
		
		return nbhd;
	}
	
	/**
	 * Neighborhood26 c.
	 * 
	 * @param pt the pt
	 * 
	 * @return the grid pt[]
	 */
	public static GridPt[] neighborhood26C(GridPt pt){
		GridPt[] nbhd = new GridPt[26];
		
		nbhd[0] = new GridPt(pt.x-1,pt.y,pt.z);
		nbhd[1] = new GridPt(pt.x,pt.y-1,pt.z);
		nbhd[2] = new GridPt(pt.x,pt.y,pt.z-1);
		nbhd[3] = new GridPt(pt.x+1,pt.y,pt.z);
		nbhd[4] = new GridPt(pt.x,pt.y+1,pt.z);
		nbhd[5] = new GridPt(pt.x,pt.y,pt.z+1);
		
		nbhd[6] = new GridPt(pt.x-1,pt.y-1,pt.z);
		nbhd[7] = new GridPt(pt.x-1,pt.y,pt.z-1);
		nbhd[8] = new GridPt(pt.x-1,pt.y+1,pt.z);
		nbhd[9] = new GridPt(pt.x-1,pt.y,pt.z+1);

		nbhd[10]= new GridPt(pt.x+1,pt.y-1,pt.z);
		nbhd[11]= new GridPt(pt.x+1,pt.y,pt.z-1);
		nbhd[12]= new GridPt(pt.x+1,pt.y+1,pt.z);
		nbhd[13]= new GridPt(pt.x+1,pt.y,pt.z+1);
		
		nbhd[14]= new GridPt(pt.x,pt.y-1,pt.z);
		nbhd[15]= new GridPt(pt.x,pt.y,pt.z-1);
		nbhd[16]= new GridPt(pt.x,pt.y+1,pt.z);
		nbhd[17]= new GridPt(pt.x,pt.y,pt.z+1);
		
		nbhd[18]= new GridPt(pt.x-1,pt.y-1,pt.z-1);
		nbhd[19]= new GridPt(pt.x-1,pt.y-1,pt.z+1);
		nbhd[20]= new GridPt(pt.x-1,pt.y+1,pt.z-1);
		nbhd[21]= new GridPt(pt.x-1,pt.y+1,pt.z+1);
		
		nbhd[22]= new GridPt(pt.x+1,pt.y-1,pt.z-1);
		nbhd[23]= new GridPt(pt.x+1,pt.y-1,pt.z+1);
		nbhd[24]= new GridPt(pt.x+1,pt.y+1,pt.z-1);
		nbhd[25]= new GridPt(pt.x+1,pt.y+1,pt.z+1);
		
		return nbhd;
	}
	
	/**
	 * Neighborhood26 c.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * 
	 * @return the grid pt[]
	 */
	public static GridPt[] neighborhood26C(int i, int j, int k){
		GridPt[] nbhd = new GridPt[26];
		
		nbhd[0] = new GridPt(i-1,j,k);
		nbhd[1] = new GridPt(i,j-1,k);
		nbhd[2] = new GridPt(i,j,k-1);
		nbhd[3] = new GridPt(i+1,j,k);
		nbhd[4] = new GridPt(i,j+1,k);
		nbhd[5] = new GridPt(i,j,k+1);
		
		nbhd[6] = new GridPt(i-1,j-1,k);
		nbhd[7] = new GridPt(i-1,j,k-1);
		nbhd[8] = new GridPt(i-1,j+1,k);
		nbhd[9] = new GridPt(i-1,j,k+1);

		nbhd[10]= new GridPt(i+1,j-1,k);
		nbhd[11]= new GridPt(i+1,j,k-1);
		nbhd[12]= new GridPt(i+1,j+1,k);
		nbhd[13]= new GridPt(i+1,j,k+1);
		
		nbhd[14]= new GridPt(i,j-1,k);
		nbhd[15]= new GridPt(i,j,k-1);
		nbhd[16]= new GridPt(i,j+1,k);
		nbhd[17]= new GridPt(i,j,k+1);
		
		nbhd[18]= new GridPt(i-1,j-1,k-1);
		nbhd[19]= new GridPt(i-1,j-1,k+1);
		nbhd[20]= new GridPt(i-1,j+1,k-1);
		nbhd[21]= new GridPt(i-1,j+1,k+1);
		
		nbhd[22]= new GridPt(i+1,j-1,k-1);
		nbhd[23]= new GridPt(i+1,j-1,k+1);
		nbhd[24]= new GridPt(i+1,j+1,k-1);
		nbhd[25]= new GridPt(i+1,j+1,k+1);
		
		return nbhd;
	}
    
    /* (non-Javadoc)
     * @see javax.vecmath.Tuple3i#toString()
     */
    public String toString() {
        return "("+(x)+","+(y)+","+(z)+")";
    }
    
    /**
     * Round in bounds.
     * 
     * @param xmax the xmax
     * @param ymax the ymax
     * @param zmax the zmax
     * 
     * @return the grid pt
     */
    public GridPt roundInBounds(int xmax, int ymax, int zmax){
    	int x = this.x;
    	int y = this.y;
    	int z = this.z;
    	if(x<0){x=0;}
    	if(y<0){y=0;}
    	if(z<0){z=0;}
    	if(x>xmax){x=xmax;}
    	if(y>ymax){y=ymax;}
    	if(z>zmax){z=zmax;}
    	return new GridPt(x,y,z);
    }


	 public static int[] roundInBounds(int x, int y, int z, int xmax, int ymax, int zmax){
	 	if(x<0){x=0;}
		if(y<0){y=0;}
		if(z<0){z=0;}
		if(x>xmax-1){x=xmax-1;}
		if(y>ymax-1){y=ymax-1;}
		if(z>zmax-1){z=zmax-1;}
		return new int[]{x,y,z};
	 }


	 public static int[] roundInBounds(float x, float y, float z, int xmax, int ymax, int zmax ){
	 	return roundInBounds(Math.round(x),Math.round(y),Math.round(z),xmax,ymax,zmax);
	 }
    
    /**
     * If a Grid Point is out of bounds, a null element
     * will be put in its place.
     * 
     * @param list Array of Grid Points to keep in bounds
     * @param xlen Max x
     * @param ylen Max y
     * @param zlen Max z
     * 
     * @return the grid pt[]
     */
    public static GridPt[] onlyInBounds(GridPt[] list, int xlen, int ylen, int zlen){
    	GridPt[] out = new GridPt[list.length];
    	int i=0;
    	for(GridPt p: list){
    		out[i]=p;
        	if(p.x<0){out[i]=null;}
        	if(p.y<0){out[i]=null;}
        	if(p.z<0){out[i]=null;}
        	if(p.x>=xlen){out[i]=null;}
        	if(p.y>=ylen){out[i]=null;}
        	if(p.z>=zlen){out[i]=null;}
        	i++;
    	}
    	return out;
    }

	
}
