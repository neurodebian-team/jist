/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.net.URL;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeScheduler;

/**
 * Manager table that displays information about execution contexts.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ProcessManagerTable extends JPanel implements MouseListener, ActionListener, TableModelListener {
	
	/**
	 * Renderer for class name.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class ClassRenderer extends JPanel implements TableCellRenderer {
		
		/** The label. */
		JLabel label;

		/**
		 * Instantiates a new class renderer.
		 */
		public ClassRenderer() {
			super();
			FlowLayout flow = new FlowLayout();
			this.setLayout(flow);
			flow.setVgap(0);
			label = new JLabel();
			flow.setAlignment(FlowLayout.LEFT);
			label.setFont(new Font("Courier", Font.BOLD, JistPreferences.getPreferences().getMdFontSize()));
			label.setHorizontalTextPosition(SwingConstants.LEFT);
			this.add(label);
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean isBordered, int row, int column) {
			String name = (value!=null)?((Class) value).getSimpleName():"UNAVAILABLE";
			label.setText(name.replace("MedicAlgorithm", ""));
			if(value==null){
				label.setForeground(Color.RED.darker());
			} else {
				label.setForeground(Color.BLACK);
			}
			if (isSelected) {
				setBackground(table.getSelectionBackground());
				if (isBordered) {
					setBorder(BorderFactory.createLineBorder(table.getSelectionBackground().darker()));
				} else {
					setBorder(BorderFactory.createLineBorder(table.getSelectionBackground()));
				}
			} else {
				setBackground(table.getBackground());
				setBorder(BorderFactory.createLineBorder(table.getBackground()));
			}
			if(value!=null)setToolTipText(((Class) value).getCanonicalName());
			return this;
		}
	}
	protected static class TimeStampWrapper implements Comparable<TimeStampWrapper>{
		long cpuTime,actualTime;
		String label;
		public TimeStampWrapper(long cpuTime,long actualTime,String label){
			this.cpuTime=cpuTime;
			this.actualTime=actualTime;
			this.label=label;
		}
		public String toString(){
			return label;
		}
		public int compareTo(TimeStampWrapper wrap) {
			int ret=(int)Math.signum(actualTime-wrap.actualTime);
			if(ret==0){
				return (int)Math.signum(cpuTime-wrap.cpuTime);
			} else {
				return ret;
			}
		}
		
	}
	protected static class MemoryStampWrapper implements Comparable<MemoryStampWrapper>{
		int usedMemory,allocatedMemory;
		String label;
		public MemoryStampWrapper(int usedMemory,int totalMemory,String label){
			this.usedMemory=usedMemory;
			this.allocatedMemory=totalMemory;
			this.label=label;
		}
		public String toString(){
			return label;
		}
		public int compareTo(MemoryStampWrapper wrap) {
			int ret=(int)Math.signum(usedMemory-wrap.usedMemory);
			if(ret==0){
				return (int)Math.signum(allocatedMemory-wrap.allocatedMemory);
			} else {
				return ret;
			}
		}
		
	}
	/**
	 * The Class ProcessTableModel.
	 */
	public class ProcessTableModel extends AbstractTableModel {
		
		/** The column names. */
		protected String[] columnNames = new String[] { "Experiment", "Module", "Algorithm", "Priority", "Time (Actual / CPU)",
				"Memory (Used / Alloc.)", "Status", "Progress" };
		
		/** The column classes. */
		protected Class[] columnClasses = new Class[] { String.class, String.class, Class.class, Integer.class,
				TimeStampWrapper.class, MemoryStampWrapper.class, ExecutionContext.Status.class, JProgressBar.class };
		
		/** The rows. */
		public Vector<ExecutionContext> rows;

		/**
		 * Instantiates a new process table model.
		 */
		public ProcessTableModel() {
			super();
			rows = new Vector<ExecutionContext>();
		}

		/**
		 * Creates the cpu time.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public TimeStampWrapper createCpuTime(ExecutionContext context) {
			return new TimeStampWrapper(context.getCpuTime(),context.getActualTime(),context.getElapsedTimeDescription());
		}

		/**
		 * Creates the memory.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public MemoryStampWrapper createMemory(ExecutionContext context) {
			int used=context.getUsedMemory();
			int alloc=context.getAllocatedMemory();
			
			return new MemoryStampWrapper(context.getUsedMemory(),context.getAllocatedMemory(),((used>0)?used+" MB":" - ")+" / "+((alloc>0)?alloc+" MB":" - "));
		}

		/**
		 * Creates the pipe view.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public Object createPipeView(ExecutionContext context) {
			return context.getContextShortName();
		}

		/**
		 * Creates the process view.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public Object createProcessView(ExecutionContext context) {
			return context.getAlgorithm().getLabel();
		}

		/**
		 * Creates the progress.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public Object createProgress(ExecutionContext context) {
			return context.getStatusBar();
		}

		/**
		 * Creates the queue view.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public Object createQueueView(ExecutionContext context) {
			if (scheduler != null) {
				return scheduler.getQueuePosition(context);
			} else {
				return 0;
			}
		}

		/**
		 * Creates the status.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public Object createStatus(ExecutionContext context) {
			ExecutionContext.Status status=context.getStatus();
			//if(status!=ExecutionContext.Status.RUNNING&&context.isLocked())return ExecutionContext.Status.LOCKED;
			return (context.isOutOfSync()&&(status==ExecutionContext.Status.READY||status==ExecutionContext.Status.NOT_READY))?ExecutionContext.Status.OUT_OF_SYNC:status;
		}

		/**
		 * Creates the type view.
		 * 
		 * @param context
		 *            the context
		 * @return the object
		 */
		public Object createTypeView(ExecutionContext context) {
			return (context.getAlgorithm().getInputParams().getFirstChildByName("Algorithm").getValue());
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
		 */
		public Class getColumnClass(int c) {
			return columnClasses[c];
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		public int getColumnCount() {
			return columnNames.length;
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
		 */
		public String getColumnName(int col) {
			return columnNames[col];
		}

		/**
		 * Gets the row.
		 * 
		 * @param row
		 *            the row
		 * @return the row
		 */
		public ExecutionContext getRow(int row) {
			if ((row >= 0) && (row < rows.size())) {
				return rows.get(row);
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		public int getRowCount() {
			return rows.size();
		}

		/**
		 * Gets the rows.
		 * 
		 * @return the rows
		 */
		public Vector<ExecutionContext> getRows() {
			return rows;
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		public Object getValueAt(int row, int col) {
			if (row >= rows.size()) {
				return null;
			}
			ExecutionContext context = rows.get(row);
			switch (col) {
			case 0:
				return createPipeView(context);
			case 1:
				return createProcessView(context);
			case 2:
				return createTypeView(context);
			case 3:
				return createQueueView(context);
			case 4:
				return createCpuTime(context);
			case 5:
				return createMemory(context);
			case 6:
				return createStatus(context);
			case 7:
				return createProgress(context);
			default:
				return null;
			}
		}

		/**
		 * Update row.
		 * 
		 * @param context
		 *            the context
		 */
		public void updateRow(ExecutionContext context) {
			int index = rows.indexOf(context);
			int index2 = table.convertColumnIndexToView(index);
			if (index2 != -1) {
				index = index2;
			}
			if ((index >= 0) && (index < this.getRowCount())) {
				try {
					fireTableRowsUpdated(index, index);
				} catch(IndexOutOfBoundsException e){
					System.err.println(getClass().getCanonicalName()+"BAD ROW INDEX "+index+" "+this.getRowCount());
					//e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Render for progress.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class ProgressRenderer implements TableCellRenderer {
		
		/* (non-Javadoc)
		 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			return (JProgressBar) value;
		}
	}

	/**
	 * Renderer to execution status.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class StatusRenderer extends JPanel implements TableCellRenderer {
		
		/** The label. */
		JLabel label;

		/**
		 * Instantiates a new status renderer.
		 */
		public StatusRenderer() {
			super();
			FlowLayout flow = new FlowLayout();
			this.setLayout(flow);
			flow.setVgap(0);
			label = new JLabel();
			label.setFont(new Font("Arial", Font.BOLD, JistPreferences.getPreferences().getMdFontSize()));
			label.setHorizontalTextPosition(SwingConstants.CENTER);
			this.add(label);
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			label.setText(value.toString().replace("_", " "));
			Color c = Color.black;
			switch ((ExecutionContext.Status) value) {
			case NOT_READY:
				c = Color.lightGray;
				break;
			case READY:
				c = Color.gray;
				break;
			case RUNNING:
				c = Color.green.darker();
				break;
			case COMPLETED:
				c = Color.black;
				break;
			case FAILED:
				c = new Color(255, 69, 0);
				break;
			case OUT_OF_SYNC:
				c = Color.gray;
				break;
				/*
			case LOCKED:
				c = new Color(255, 69, 0);
				break;
				*/
			default:
				c = Color.black;
			}
			label.setForeground(c);
			if (isSelected) {
				setBackground(table.getSelectionBackground());
				if (hasFocus) {
					setBorder(BorderFactory.createLineBorder(table.getSelectionBackground().darker()));
				} else {
					setBorder(BorderFactory.createLineBorder(table.getSelectionBackground()));
				}
			} else {
				setBackground(table.getBackground());
				setBorder(BorderFactory.createLineBorder(table.getBackground()));
			}
			return this;
		}
	}

	/**
	 * Renderer for text.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class StringRenderer extends JPanel implements TableCellRenderer {
		
		/** The label. */
		JLabel label;

		/**
		 * Instantiates a new string renderer.
		 */
		public StringRenderer() {
			super();
			FlowLayout flow = new FlowLayout();
			this.setLayout(flow);
			flow.setVgap(0);
			label = new JLabel();
			flow.setAlignment(FlowLayout.LEFT);
			label.setFont(new Font("Arial", Font.PLAIN, JistPreferences.getPreferences().getMdFontSize()));
			label.setHorizontalTextPosition(SwingConstants.LEFT);
			this.add(label);
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			label.setText(value.toString());
			if (isSelected) {
				setBackground(table.getSelectionBackground());
				if (hasFocus) {
					setBorder(BorderFactory.createLineBorder(table.getSelectionBackground().darker()));
				} else {
					setBorder(BorderFactory.createLineBorder(table.getSelectionBackground()));
				}
			} else {
				setBackground(table.getBackground());
				setBorder(BorderFactory.createLineBorder(table.getBackground()));
			}
			return this;
		}
	}

	/** The proc table. */
	protected ProcessTableModel procTable;
	
	/** The table. */
	protected JTable table;
	
	/** The popup menu. */
	protected JPopupMenu popupMenu;
	
	/** The debug. */
	public Action run, stop, restart, promote, demote, clean, debug;
	
	/** The scheduler. */
	PipeScheduler scheduler;
	
	/** The debug item. */
	JMenuItem runItem, stopItem, restartItem, promoteItem, demoteItem, cleanItem, debugItem;
	
	/** The current contexts. */
	Vector<ExecutionContext> currentContexts;
	
	/** The out frame. */
	protected ProcessDebugInfoFrame outFrame;

	/**
	 * Default constructor.
	 */
	public ProcessManagerTable() {
		super(new GridLayout(1, 0));
		add(createTable());
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		currentContexts = getSelectedContexts();
		if (e.getSource() == runItem) {
			scheduler.run(currentContexts);
		} else if (e.getSource() == stopItem) {
			scheduler.stop(currentContexts);
		} else if (e.getSource() == promoteItem) {
			scheduler.promote(currentContexts);
			procTable.fireTableDataChanged();
		} else if (e.getSource() == demoteItem) {
			scheduler.demote(currentContexts);
			procTable.fireTableDataChanged();
		} else if (e.getSource() == restartItem) {
			int n = JOptionPane.showOptionDialog(this, "Are you sure you want to restart selected experiments?", null,
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (n == 0) {
				scheduler.restart(currentContexts);
			}
		} else if (e.getSource() == cleanItem) {
			int n = JOptionPane.showOptionDialog(this, "Are you sure you want to clean selected experiments?", null,
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (n == 0) {
				scheduler.clean(currentContexts,false);
			}
		} else if (e.getSource() == debugItem) {
			getOutFrame().setVisible(false);
			getOutFrame().setTitle("Debugging Information");
			for (ExecutionContext context : currentContexts) {
				File tf = new File(context.getOutputLocation(), "debug.out");
				File bf = new File(context.getOutputLocation(), "debug.err");
				if (tf.exists() && bf.exists()) {
					getOutFrame().load(context.getContextName(), tf, bf);
				}
			}
		}
		popupMenu.setVisible(false);
	}

	/**
	 * Creates the popup menu.
	 * 
	 * @return the j popup menu
	 */
	public JPopupMenu createPopupMenu() {
		JPopupMenu popupMenu = new JPopupMenu();
		runItem = new JMenuItem("Run");
		runItem.addActionListener(this);
		popupMenu.add(runItem);
		stopItem = new JMenuItem("Stop");
		stopItem.addActionListener(this);
		popupMenu.add(stopItem);
		restartItem = new JMenuItem("Restart");
		restartItem.addActionListener(this);
		popupMenu.add(restartItem);
		cleanItem = new JMenuItem("Clean");
		cleanItem.addActionListener(this);
		popupMenu.add(cleanItem);
		promoteItem = new JMenuItem("Promote");
		promoteItem.addActionListener(this);
		popupMenu.add(promoteItem);
		demoteItem = new JMenuItem("Demote");
		demoteItem.addActionListener(this);
		popupMenu.add(demoteItem);
		debugItem = new JMenuItem("View Debugging Info");
		debugItem.addActionListener(this);
		popupMenu.add(debugItem);
		return popupMenu;
	}

	/**
	 * Create scroll pane table.
	 * 
	 * @return the j scroll pane
	 */
	protected JScrollPane createTable() {
		table = new JTable(procTable = new ProcessTableModel());
		popupMenu = createPopupMenu();
		table.addMouseListener(this);
		// table.setPreferredScrollableViewportSize();
		table.setFillsViewportHeight(true);
		table.setDefaultRenderer(JProgressBar.class, new ProgressRenderer());
		table.setDefaultRenderer(ExecutionContext.Status.class, new StatusRenderer());
		table.setDefaultRenderer(Class.class, new ClassRenderer());
		table.setDefaultRenderer(String.class, new StringRenderer());
		table.setAutoCreateRowSorter(true);
		table.getModel().addTableModelListener(this);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		return scrollPane;
	}

	/**
	 * Create toolbar to control execution.
	 * 
	 * @return the j tool bar
	 */
	public JToolBar createToolBar() {
		JToolBar toolbar = new JToolBar();
		toolbar.setFloatable(false);
		toolbar.setPreferredSize(new Dimension(300, 40));
		URL url;
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/delete.gif");
		ImageIcon cleanIcon = new ImageIcon(url, "Clean");
		clean = new AbstractAction("Clean", cleanIcon) {
			public void actionPerformed(ActionEvent e) {
				cleanItem.doClick();
			}
		};
		clean.putValue(Action.SHORT_DESCRIPTION, "Clean");
		clean.setEnabled(false);
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/down.gif");
		ImageIcon demoteIcon = new ImageIcon(url, "Demote");
		demote = new AbstractAction("Demote", demoteIcon) {
			public void actionPerformed(ActionEvent e) {
				demoteItem.doClick();
			}
		};
		demote.putValue(Action.SHORT_DESCRIPTION, "Demote");
		demote.setEnabled(false);
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/up.gif");
		ImageIcon promoteIcon = new ImageIcon(url, "Promote");
		promote = new AbstractAction("Promote", promoteIcon) {
			public void actionPerformed(ActionEvent e) {
				promoteItem.doClick();
			}
		};
		promote.putValue(Action.SHORT_DESCRIPTION, "Promote");
		promote.setEnabled(false);
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/refresh.gif");
		ImageIcon restartIcon = new ImageIcon(url, "Restart");
		restart = new AbstractAction("Restart", restartIcon) {
			public void actionPerformed(ActionEvent e) {
				restartItem.doClick();
			}
		};
		restart.putValue(Action.SHORT_DESCRIPTION, "Restart");
		restart.setEnabled(false);
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/stop.gif");
		ImageIcon stopIcon = new ImageIcon(url, "Stop");
		stop = new AbstractAction("Stop", stopIcon) {
			public void actionPerformed(ActionEvent e) {
				stopItem.doClick();
			}
		};
		stop.putValue(Action.SHORT_DESCRIPTION, "Stop");
		stop.setEnabled(false);
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/play.gif");
		ImageIcon runIcon = new ImageIcon(url, "Run");
		runIcon.setDescription("Run");
		run = new AbstractAction("Run", runIcon) {
			public void actionPerformed(ActionEvent e) {
				runItem.doClick();
			}
		};
		run.putValue(Action.SHORT_DESCRIPTION, "Run");
		run.setEnabled(false);
		stop.setEnabled(false);
		url = getClass().getClassLoader().getResource("gov/nih/mipav/view/icons/multifile.gif");
		ImageIcon debugIcon = new ImageIcon(url, "Debug");
		debugIcon.setDescription("Debug");
		debug = new AbstractAction("Debug", debugIcon) {
			public void actionPerformed(ActionEvent e) {
				debugItem.doClick();
			}
		};
		debug.putValue(Action.SHORT_DESCRIPTION, "View Debugging Information");
		debug.setEnabled(false);
		toolbar.add(run);
		toolbar.add(stop);
		toolbar.add(promote);
		toolbar.add(demote);
		toolbar.add(restart);
		toolbar.add(debug);
		toolbar.add(clean);
		return toolbar;
	}

	/**
	 * Gets the out frame.
	 * 
	 * @return the out frame
	 */
	public ProcessDebugInfoFrame getOutFrame() {
		if (outFrame == null) {
			outFrame = new ProcessDebugInfoFrame();
		}
		return outFrame;
	}

	/**
	 * Set current selected contexts in table.
	 * 
	 * @return selected contexts
	 */
	public Vector<ExecutionContext> getSelectedContexts() {
		int[] index = table.getSelectedRows();
		Vector<ExecutionContext> list = new Vector<ExecutionContext>();
		for (int i : index) {
			if (i >= table.getRowCount()) {
				break;
			}
			list.add(procTable.getRow(table.convertRowIndexToModel(i)));
		}
		return list;
	}

	/**
	 * Get table that stores execution contexts.
	 * 
	 * @return table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * Gets the table model.
	 * 
	 * @return the table model
	 */
	public ProcessTableModel getTableModel() {
		return procTable;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent evt) {
		popupMenu.setVisible(false);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) {
		updateAvailableOptions();
		if ((e.getButton() == MouseEvent.BUTTON2) || (e.getButton() == MouseEvent.BUTTON3)) {
			popupMenu.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent e) {
	}

	/**
	 * Remove scheduler and clear rows.
	 */
	public void removeScheduler() {
		this.scheduler = null;
		procTable.rows.clear();
		procTable.fireTableDataChanged();
	}

	/**
	 * Set scheduler that will force updates to the manager table.
	 * 
	 * @param scheduler
	 *            scheduler
	 */
	public void setScheduler(PipeScheduler scheduler) {
		this.scheduler = scheduler;
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent arg0) {
		updateAvailableOptions();
	}

	/**
	 * Update list of available options based on the number of selected
	 * contexts.
	 */
	protected void updateAvailableOptions() {
		currentContexts = getSelectedContexts();
		if (currentContexts.size() == 0) {
			runItem.setEnabled(false);
			run.setEnabled(false);
			cleanItem.setEnabled(false);
			clean.setEnabled(false);
			stopItem.setEnabled(false);
			stop.setEnabled(false);
			restartItem.setEnabled(false);
			restart.setEnabled(false);
			promoteItem.setEnabled(false);
			promote.setEnabled(false);
			demoteItem.setEnabled(false);
			demote.setEnabled(false);
			debugItem.setEnabled(false);
			debug.setEnabled(false);
		} else {
			runItem.setEnabled(true);
			run.setEnabled(true);
			cleanItem.setEnabled(true);
			clean.setEnabled(true);
			stopItem.setEnabled(true);
			stop.setEnabled(true);
			restartItem.setEnabled(true);
			restart.setEnabled(true);
			promoteItem.setEnabled(true);
			promote.setEnabled(true);
			demoteItem.setEnabled(true);
			demote.setEnabled(true);
			debugItem.setEnabled(true);
			debug.setEnabled(true);
		}
	}
}
