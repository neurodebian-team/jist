/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Combo box to select single incoming port.
 * 
 * @author Blake Lucas
 */
public class PortComboBox extends JPanel implements ActionListener {
	
	/** The last port. */
	protected PipePort lastPort = null;
	
	/** The port. */
	protected PipePort port;
	
	/** The combo. */
	protected JComboBox combo;
	
	/** The panel. */
	private ParameterPanel panel;

	/**
	 * Constructor.
	 * 
	 * @param port
	 *            associated port
	 * @param ports
	 *            list of compatible ports
	 */
	public PortComboBox(PipePort port, Vector<PipePort> ports) {
		super(new BorderLayout());
		combo = new JComboBox();
		this.panel = ParameterPanel.getInstance();
		combo.setRenderer(new ConnectorRenderer(port));
		this.port = port;
		combo.setEditable(true);
		combo.setEditor(new ConnectionEditor(port));
		combo.addItem("---NONE---");
		for (PipePort p : ports) {
			combo.addItem(p);
		}
		if (port.isConnected()) {
			this.setSelectedPort((PipePort) port.getIncomingPorts().firstElement());
		}
		combo.addActionListener(this);
		add(new JLabel(port.getLabel()), BorderLayout.NORTH);
		add(combo, BorderLayout.CENTER);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() instanceof JComboBox) {
			panel.setIgnoreRefresh(true);
			PipePort selectedPort = getSelectedPort();
			if (selectedPort != null) {
				PipeConnector.connect(selectedPort, port);
			} else {
				port.disconnect();
			}
			panel.setIgnoreRefresh(false);
		}
	}

	/**
	 * Get selected port.
	 * 
	 * @return selected port
	 */
	public PipePort getSelectedPort() {
		Object obj = combo.getSelectedItem();
		if (obj instanceof PipePort) {
			return (PipePort) obj;
		} else {
			return null;
		}
	}

	/**
	 * Set selected port.
	 * 
	 * @param port
	 *            selected port
	 */
	public void setSelectedPort(PipePort port) {
		combo.setSelectedItem(port);
	}
}
