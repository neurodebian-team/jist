package edu.jhu.ece.iacl.jist.plugins.testing;


import java.awt.Dimension;


import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;


import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamLong;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamNumberSliderInputView;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class MedicAlgorithmDemo extends ProcessingAlgorithm{
	ParamCollection numbers;
	ParamBoolean booleanParam;

	ParamDouble doubleParam;
	ParamInteger intParam;
	ParamFloat floatParam;
	ParamLong longParam;

	ParamCollection points;
	ParamPointFloat pointfParam;
	ParamPointInteger pointiParam;
	ParamPointDouble pointdParam;

	ParamCollection data;
	ParamVolume volParam;
	ParamSurface surfParam;
	ParamFile fileParam;
	ParamObject<CurveCollection> objParam;

	ParamCollection collections;
	ParamNumberCollection numCollection;
	ParamFileCollection fileCollection;
	ParamSurfaceCollection surfCollection;
	ParamVolumeCollection volCollection;

	ParamCollection misc;
	ParamOption optParam;
	ParamString stringParam;
	ParamMatrix matrixParam;

	ParamString msg;


	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "A demonstration module with man examples for programmers to help understand the JIST environment.\nAlgorithm Version: " + revnum + "\n";


	/**
	 * Create input parameters for this plug-in
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		/*
		 * Set the Package group and category.
		 * Set the name and label for the algorithm.
		 */
		inputParams.setPackage("Base");
		inputParams.setCategory("Demonstration");
		inputParams.setLabel("Demonstration Module");
		inputParams.setName("Demonstration_Module");


		//Set the algorithm's information
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Blake Lucas","bclucas@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		//Create input parameters for all supported types
		numbers=new ParamCollection("Numbers");
		numbers.add(booleanParam=new ParamBoolean("Boolean",true));
		numbers.add(intParam=new ParamInteger("Integer",0,100000,1000));
		numbers.add(doubleParam=new ParamDouble("Double",0,100,32.5));
		numbers.add(floatParam=new ParamFloat("Float",0,100,10));
		numbers.add(longParam=new ParamLong("Long",0,100,2));
		inputParams.add(numbers);


		points=new ParamCollection("Points");
		points.add(pointfParam=new ParamPointFloat("Float Point",new Point3f(1,2,3)));
		points.add(pointdParam=new ParamPointDouble("Double Point",new Point3d(1,2,3)));
		points.add(pointiParam=new ParamPointInteger("Integer Point",new Point3i(1,2,3)));
		inputParams.add(points);


		data=new ParamCollection("Data Sources");
		data.add(volParam=new ParamVolume("Volume"));
		data.add(fileParam=new ParamFile("File",new FileExtensionFilter(new String[]{"java"})));
		data.add(objParam=new ParamObject<CurveCollection>("Curves",new CurveVtkReaderWriter()));
		data.add(surfParam=new ParamSurface("Surface"));
		inputParams.add(data);


		collections=new ParamCollection("Collections");
		collections.add(volCollection=new ParamVolumeCollection("Volumes"));
		collections.add(fileCollection=new ParamFileCollection("Files",new CurveVtkReaderWriter()));
		collections.add(surfCollection=new ParamSurfaceCollection("Surfaces"));
		inputParams.add(collections);


		misc=new ParamCollection("Misc.");
		misc.add(stringParam=new ParamString("String","hello world"));
		misc.add(optParam=new ParamOption("Option",new String[]{"Red","Green","Blue"}));
		misc.add(matrixParam=new ParamMatrix("Maitrx",3,3));
		inputParams.add(misc);


		//Change the input view
		longParam.setInputView(new ParamNumberSliderInputView(longParam));


		//Indicate whether fields are mandatory
		volCollection.setMandatory(false);
		fileCollection.setMandatory(false);
		surfCollection.setMandatory(false);
		volParam.setMandatory(false);
		fileParam.setMandatory(false);
		objParam.setMandatory(false);
		surfParam.setMandatory(false);
	}


	/**
	 * Create output parameters for this plug-in
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		//Create output parameters
		outputParams.add(msg=new ParamString("Message"));
	}


	/**
	 * Example algorithm
	 * @author Blake Lucas (bclucas@jhu.edu)
	 *
	 */
	protected static class MyAlgorithm extends AbstractCalculation{
		/**
		 * Construct algorithm
		 */
		int maxCount;
		public MyAlgorithm(int maxCount){
			super();
			this.maxCount=maxCount;
			setLabel("My Algorithm");
		}


		/**
		 * Construct algorithm
		 * @param parent parent algorithm
		 */
		public MyAlgorithm(AbstractCalculation parent){
			super(parent);
			setLabel("My Algorithm");
		}


		/**
		 * Call algorithm
		 */
		public void execute(){
			//Set the total number of completed units
			setTotalUnits(maxCount);
			for(int i=0;i<maxCount;i++){
				//Increment the number of completed units
				incrementCompletedUnits();
			}
			//Indicate that algorithm has completed to progress monitor
			markCompleted();
		}
	}


	/**
	 * Execute algorithm
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		//Initialize algorithm using parameter value
		MyAlgorithm algo=new MyAlgorithm(intParam.getInt());
		monitor.observe(algo);
		algo.execute();
		//Set the output value
		msg.setValue("All Done!");
	}
}
