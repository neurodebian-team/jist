/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.io.Serializable;

import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;

/**
 * Factory to create new graph cell modules, tree nodes, and pipes.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public abstract class PipeModuleFactory implements Serializable {
	// Name of module to display
	/** The name. */
	protected String name;
	protected String version;
	protected String description;
	// Module class.
	/** The c. */
	protected Class c;

	/**
	 * Instantiates a new pipe module factory.
	 */
	public PipeModuleFactory() {
		c = null;
		name = null;
		description="";
	}

	/**
	 * Instantiates a new pipe module factory.
	 * 
	 * @param name
	 *            the name
	 * @param c
	 *            the c
	 */
	public PipeModuleFactory(String name, String description, Class c) {
		this.c = c;
		this.name = name;
		this.description =description;
	}

	/**
	 * Create graph cell for internal frame.
	 * 
	 * @return graph cell
	 */
	public abstract PipeModuleCell createGraphCell();

	/**
	 * Create pipe module to run algorithm.
	 * 
	 * @return pipe module
	 */
	public abstract PipeModule createPipe();

	/**
	 * Create tree node for module panel.
	 * 
	 * @return tree node
	 */
	public abstract DraggableNode createTreeNode();

	/**
	 * Get class that identifies the module which could be a PipeModule.
	 * 
	 * @return the module class
	 */
	public Class getModuleClass() {
		return c;
	}

	/**
	 * Get the module's name. Version info is appended if available. 
	 * 
	 * @return the module name
	 */
	public String getModuleName() {
		if(version==null)
			return name;
		else
			return name+((version.length()>0)?" ("+version+")":"");
	}

	public String getDescription() {
		// TODO Auto-generated method stub
		return description;
	}
}
