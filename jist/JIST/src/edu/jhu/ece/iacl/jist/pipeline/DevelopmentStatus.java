package edu.jhu.ece.iacl.jist.pipeline;

// TODO: Auto-generated Javadoc
/**
 * The Enum DevelopmentStatus.
 */
public enum DevelopmentStatus {

/** The UNKNOWN. */
UNKNOWN,
/** The ALPHA. */
ALPHA,
/** The BETA. */
BETA,
/** The RC. */
RC,
/** The Release. */
Release,
/** The Deprecated. */
Dep,
/** The Not functional. */
NotFunctional
}
