/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavPointer;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.structures.ModelImage;

/**
 * Volume Parameter Input View allows the user to select a currently opened
 * image from a combobox. The user can also open an existing image by selecting
 * BROWSE_TEXT If an image is opened or closed using MIPAV, the combobox will be
 * updated to reflect the changes.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeURIInputView extends ParamInputView implements CaretListener,ActionListener, Refreshable, ListCellRenderer {
	/** The file field. */
	private JTextField field2;
	/** The browse button. */
	private JButton browseButton2;
	/**
	 * List box item that displays the image name.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */

	private DefaultListCellRenderer renderer; 
	public static class VolumeListBoxItem implements Comparable<VolumeListBoxItem> {
		
		/** The name. */
		public String name;
		
		/** The file. */
		public File file;

		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(VolumeListBoxItem obj) {
			return name.compareTo(obj.name);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object obj) {
			return file.equals(((VolumeListBoxItem) obj).file);
		}
	}

	/** The Constant OPEN_TEXT. */
	private static final String OPEN_TEXT = "Open";
	/** The Constant BROWSE_TEXT. */
	private static final String BROWSE_TEXT = "Browse";
	/** The Constant BLANK_TEXT. */
	private static final String BLANK_TEXT = "-----NONE-----";
	/** Field for selecting image input. */
	protected JComboBox field;
	
	/** The image list size. */
	protected int imageListSize = -1;
	
	/** The browse button. */
	protected JButton browseButton;

	/**
	 * Construct combox to select a currently opened image.
	 * 
	 * @param param
	 *            the parameter
	 */
	public ParamVolumeURIInputView(ParamVolume param) {
		super(param);
		field = new JComboBox();
		browseButton = new JButton(OPEN_TEXT);
		browseButton.setPreferredSize(defaultNumberFieldDimension);
		
		field2 = new JTextField();
		field2.setPreferredSize(defaultTextFieldDimension);
		browseButton2 = new JButton(BROWSE_TEXT);
		browseButton2.setPreferredSize(defaultNumberFieldDimension);
		
		
		
		// Create combo box with default values
		updateComboBox(null);

		renderer=new DefaultListCellRenderer();
		JPanel smallPane = new JPanel();
		smallPane.setLayout(new BoxLayout(smallPane, BoxLayout.Y_AXIS));
		smallPane.add(field2);
		JPanel myPanel = new JPanel(new BorderLayout());
		myPanel.add(browseButton2, BorderLayout.EAST);
		browseButton2.addActionListener(this);
		smallPane.add(myPanel);
		smallPane.add(field);
		field.setMinimumSize(defaultNumberFieldDimension);
		
		myPanel = new JPanel(new BorderLayout());
		myPanel.add(browseButton, BorderLayout.EAST);
		smallPane.add(myPanel);
		buildLabelAndParam(smallPane);
		browseButton.addActionListener(this);
		field.setRenderer(this);
		field2.addCaretListener(this);
		// Create refresher to monitor for MIPAV changes
		Refresher.getInstance().add(this);
	}
	/**
	 * Open file chooser to select file with specific extension.
	 * 
	 * @return absolute path of the file
	 */
	private File openFileChooser(File oldFile) {
		JFileChooser openDialog = new JFileChooser();
		openDialog.setSelectedFile(MipavController.getDefaultWorkingDirectory());
		openDialog.setDialogTitle("Select Image");
		openDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		openDialog.setFileFilter(getParameter().getExtensionFilter());
		openDialog.setAcceptAllFileFilterUsed(false);
		if ((oldFile != null) && oldFile.exists()) {
			openDialog.setSelectedFile(oldFile);
		} else {
			openDialog.setCurrentDirectory(JistPreferences.getPreferences().getLastDirectory());
		}
		openDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		int returnVal = openDialog.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			JistPreferences.getPreferences().setLastDirectory(openDialog.getSelectedFile().getParentFile());
			return openDialog.getSelectedFile();
		} else {
			return null;
		}
	}
	/* (non-Javadoc)
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(browseButton2)) {
			// Browse selected, open file dialog
			
			File f = openFileChooser(getParameter().getValue());
			if(f!=null){
				field.setSelectedIndex(0);
				getParameter().setValue(f);
				field2.setText(getParameter().getURI().toString());
				field2.setEditable(true);
			}
		} else if (event.getSource().equals(browseButton)) {
			// Browse selected, open file dialog
			
			ArrayList<ImageData> imgs = MipavController.openFileDialog(getParameter().getValue());
			Refresher.getInstance().pauseAll();
			if ((imgs != null) && (imgs.size() > 0)) {
				// Select first image
				setSelected(imgs.get(0));
			} else {
				// Select no image
				setSelected(null);
			}
			Refresher.getInstance().resumeAll();
		} else if (event.getSource().equals(field)) {
			// Get selected item
			if(!updateLock){
				String selectedImage = ((ListBoxElement)field.getSelectedItem()).getName();
				if (selectedImage != null) {
					// An image is selected
					if (!selectedImage.equals(BLANK_TEXT)) {
						// Set parameter value to selected Image
						
						getParameter().setValue(((ListBoxElement)field.getSelectedItem()).getImage());
						
						field2.setText(getParameter().getURI().toString());
						field2.setEditable(false);
						notifyObservers(param, this);
					} else {
						// Clear parameter value
						getParameter().clear();
						field2.setText("");
						field2.setEditable(true);
						notifyObservers(param, this);
					}
				}
			}
		}
	}


	/**
	 * Commit changes to parameter.
	 */
	public /*synchronized*/ void commit() {
		if(field.getSelectedIndex()>0){
			ImageDataMipav img= ((ListBoxElement)field.getSelectedItem()).getImage();
			getParameter().setValue(img);
			URI uri=getParameter().getURI();
			if(uri!=null){
				String str=uri.toString();
				if(!str.equals(field2.getText())){
					field2.setText(uri.toString());
				}
			}
			field2.setEditable(false);
		} else {
			if(!field2.isEditable()){
				getParameter().clear();
			}
		}
	}

	/**
	 * Get volume parameter.
	 * 
	 * @return the parameter
	 */
	public ParamVolume getParameter() {
		return (ParamVolume) param;
	}

	/**
	 * Refresh volume selection input by rebuilding combobox.
	 */
	public /*synchronized*/ void refresh() {
		commit();
		updateComboBox(null);
		}

	/**
	 * Set the selected ModelImage in the combobox.
	 * 
	 * @param img
	 *            model image
	 */
	public void setSelected(ImageData img) {
		updateComboBox((img != null) ? img.getName() : null);
	}

	/**
	 * Update view with value from parameter.
	 */
	public void update() {
		URI uri=getParameter().getURI();
		if(uri!=null){
			try {
			    field.setSelectedIndex(0);
			} catch(IllegalArgumentException e) {};
			field2.setText(uri.toString());
			field2.setEditable(true);
			getParameter().setValue(uri);
		}
		
	}
	private static boolean updateLock=false;
	/**
	 * Update combobox selection.
	 * 
	 * @param item
	 *            selected item
	 * @return true if update was necessary
	 */
	protected boolean updateComboBox(String item) {
		if(updateLock)return false;
		updateLock=true;
		Vector<String> imageNames = MipavController.getImageNames();
		field.removeActionListener(this);
		// Rebuild selection box if an item is selected
		if (imageNames.size() != imageListSize) {
			// Remember current selected item if no selected item is specified
			if (item == null&&field.getSelectedItem()!=null) {
				item = ((ListBoxElement)field.getSelectedItem()).getName();
			}
			imageNames.insertElementAt(BLANK_TEXT, 0);
			imageListSize = imageNames.size() - 1;
			field.removeAllItems();
			for (String name : imageNames) {
				ModelImage modelimg = MipavController.getImageByName(name);
				ImageDataMipav img;
				/*if(modelimg!=null) {
					img=new ImageDataMipavWrapper(modelimg);				
					field.addItem(new ListBoxElement(img,name,"<html><font color='#990000'>"+img.getName()+"</font> ["+img.getType().toString().toLowerCase()+" | ("+img.getRows()+", "+img.getCols()+", "+img.getSlices()+", "+img.getComponents()+")]</html>"));
					//field.addItem(new ListBoxElement(img,name,name));
				} else {
					// don't add
				}*/
				if(modelimg!=null) {
					img=new ImageDataMipavPointer(modelimg);				
					field.addItem(new ListBoxElement(img,name,"<html><font color='#990000'>"+
							img.getName()+"</font>["+
							img.getType().toString().toLowerCase()+" | ("+
							img.getRows()+","+img.getCols()+", "+img.getSlices()+
							","+img.getComponents()+")]</html>"));
							//field.addItem(new ListBoxElement(img,name,name));
				} else {
					//Add list entry for "NONE"
					field.addItem(new ListBoxElement(null,name,name));
				}
			}
			// Try to select image if list was originally empty
		} else {
			// Nothing to update
			if (item == null) {
				// Enable listener
				field.addActionListener(this);
				updateLock=false;
				return false;
			}
		}
		if (item != null) {
			if (!item.equals(BLANK_TEXT)) {
				// Update selected item as long as it is not "Browse"
				for(int i=0;i<field.getItemCount();i++){
					if(((ListBoxElement)field.getItemAt(i)).getName().equals(item)){
						field.setSelectedIndex(i);
						break;
					}
				}
				// this did point to an erroneous name (PLB), next line is ugly but seems to fix the issue
				//getParameter().setValue(item);
				File newVal=null;
				try {
					newVal = FileReaderWriter.getFullFileName(((ImageDataMipav)((ListBoxElement)field.getSelectedItem()).getImage()).getModelImageDirect());
				} catch(NullPointerException e) {
					// assume that selected image is no longer open. Don't worry. We'll just clear the value
				}
				
				getParameter().setValue( newVal);
				URI uri=getParameter().getURI();
				field2.setText((uri==null)?"":uri.toString());
				field2.setEditable(false);
				notifyObservers(param, this);
			} else {
				// No item selected, set parameter to null
				if(!field.isEditable()&&field.getSelectedIndex()!=0){
					getParameter().clear();		
					field2.setText("");
				}
				field2.setEditable(true);
				field.setSelectedIndex(0);
				notifyObservers(param, this);
			}
			// Enable listener
			field.addActionListener(this);
			updateLock=false;
			return true;
		} else {
			// Enable listener
			field.addActionListener(this);
			updateLock=false;
			return false;
		}
		
	}
	private static class ListBoxElement{
		String label;
		String name;
		ImageDataMipav img;
		public ListBoxElement(ImageDataMipav img,String name,String label){
			this.label=label;
			this.name=name;
			this.img=img;
		}
		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
		/**
		 * @param label the label to set
		 */
		public void setLabel(String label) {
			this.label = label;
		}
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		/**
		 * @return the image
		 */
		public ImageDataMipav getImage() {
			return img;
		}
		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return field;
	}
	/* (non-Javadoc)
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,boolean cellHasFocus) {
		ListBoxElement elem=(ListBoxElement)value;
		Component pane = renderer.getListCellRendererComponent(list, (elem!=null?elem.getLabel():null), index, isSelected, cellHasFocus);

		return pane;
	}
	public void caretUpdate(CaretEvent event) {
		if (event.getSource().equals(field2)) {
			if(field2.isEditable()&&field2.getText().length()>0){
				getParameter().setValue(field2.getText());
				notifyObservers(param, this);
			}
		}
	}
}
