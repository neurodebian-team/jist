package edu.jhu.ece.iacl.jist.structures.geom;


// TODO: Auto-generated Javadoc
/**
 * The Class Face.
 */
public abstract class Face implements Comparable<Face>{
	/*
	protected ArrayList<Surface> parents=new ArrayList<Surface>();
	public ArrayList<Surface> getParentSurfaces(){
		return parents;
	}
	public void add(Surface surf){
		parents.add(surf);
	}
	*/
	/**
	 * Gets the vertices.
	 * 
	 * @return the vertices
	 */
	public abstract Vertex[] getVertices();
	
	/**
	 * Gets the edges.
	 * 
	 * @return the edges
	 */
	public abstract Edge[] getEdges();
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Face o) {
		return this.hashCode()-o.hashCode();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj){
		return (this==obj);
	}
}
