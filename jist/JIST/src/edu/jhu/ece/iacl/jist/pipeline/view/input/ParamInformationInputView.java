/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.gui.PipelineLayoutTool;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInformation;

/**
 * File Parameter Input View creates a text field to enter a file and a browse
 * button to select a file. The file name can be mandatory or not mandatory. The
 * file extension filter can be set to only permit files with specific
 * extensions.
 * 
 * @author Blake Lucas
 */
public class ParamInformationInputView extends ParamInputView implements ActionListener {
	
	/** The edit dialog. */
	private JButton editDialog;

	/**
	 * Construct textfield to enter file name and browse button to select file.
	 * 
	 * @param param
	 *            the parameter
	 */
	public ParamInformationInputView(ParamInformation param) {
		super(param);
		editDialog = new JButton("View");
		editDialog.setPreferredSize(defaultNumberFieldDimension);
		editDialog.addActionListener(this);
		buildLabelAndParam(editDialog);
		update();
	}

	/**
	 * Open file dialog box when browse button is pressed.
	 * 
	 * @param event
	 *            browse button clicked
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(editDialog)) {
			if (MipavController.isQuiet()) {
				AlgorithmInformationDialog dialog = new AlgorithmInformationDialog(PipelineLayoutTool.getInstance(),
						getParameter().getValue());
			} else {
				AlgorithmInformationDialog dialog = new AlgorithmInformationDialog(null, getParameter().getValue());
			}
		}
	}

	/**
	 * Unimplemented. Changes can only be committed when user is viewing the
	 * information dialog.
	 */
	public void commit() {
		// TODO Auto-generated method stub
	}

	/**
	 * Get information parameter.
	 * 
	 * @return the parameter
	 */
	public ParamInformation getParameter() {
		return (ParamInformation) param;
	}

	/**
	 * Unimplemented.
	 */
	public void update() {
	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return editDialog;
	}
}
