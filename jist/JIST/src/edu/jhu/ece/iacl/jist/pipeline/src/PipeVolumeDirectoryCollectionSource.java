/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through all volumes in directory.
 * 
 * @author Blake Lucas
 */
public class PipeVolumeDirectoryCollectionSource extends PipeSource {
	
	/** The dir param. */
	protected ParamFile dirParam;
	
	/** The include param. */
	protected ParamBoolean includeParam;
	
	/** The filter rule. */
	protected ParamOption filterRule;
	
	/** The filter expression. */
	protected ParamString filterExpression;
	
	/** The file extension. */
	protected ParamString fileExtension;
	
	/** The search depth. */
	protected ParamInteger searchDepth;
	
	/** The file parameter */
	protected ParamVolumeCollection fileParam = new ParamVolumeCollection();
	
	/** The images. */
	transient protected Vector<File> fileList;
	
	/** The index. */
	transient protected int index=0;

	ParamVolumeCollection fileGroup;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
		Element em;		
//		
//		group.add(dirParam = new ParamFile("Directory", ParamFile.DialogType.DIRECTORY));
//		group.add(includeParam = new ParamBoolean("Include Sub-directories", true));
//		group.add(searchDepth = new ParamInteger("Max Directory Depth", 0, 100000, 1));
//		group.add(fileExtension = new ParamString("File Extension"));
//		fileExtension.setValue("xml");
//		group.add(filterRule = new ParamOption("Filter Rule", new String[] { "Contains", "Matches","Begins With", "Ends With" }));
//		group.add(filterExpression = new ParamString("Filter Expression"));
//		em = document.createElement("dirParam");
//		dirParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("includeParam");
//		includeParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("filterRule");
//		filterRule.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("filterExpression");
//		filterExpression.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("fileExtension");
//		fileExtension.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("searchDepth");
//		searchDepth.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
		em = document.createElement("fileParam");
		fileParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("fileGroup");
//		fileGroup.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
		
		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		dirParam = new ParamFile();
		dirParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"dirParam"));
		includeParam = new ParamBoolean();
		includeParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"includeParam"));
		filterRule = new ParamOption();
		filterRule.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filterRule"));
		filterExpression = new ParamString();
		filterExpression.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filterExpression"));
		fileExtension = new ParamString();
		fileExtension.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileExtension"));
		searchDepth = new ParamInteger();
		searchDepth.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"searchDepth"));
		fileParam = new ParamVolumeCollection();
		fileParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileParam"));
		fileGroup = new ParamVolumeCollection();
		fileGroup.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileGroup"));
		getParentPort().setParameter(fileGroup); //?
	}
	/**
	 * Returns true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || (index <0 ));
	}
	
	public boolean iterate() {
		if (hasNext()) {
			index++;
			return true;
		} else {
			reset();			
			return false;
		}
	}
	
	public void reset() {
		super.reset();		
		index=0;
	}

	
	protected void findMatchingFiles(){
		fileList = new Vector<File>();
		File dir = dirParam.getValue();
		if (dir == null) {
			return;
		}
		boolean recurse = includeParam.getValue();
		int rule = filterRule.getIndex();
		String exp = filterExpression.getValue();
		LinkedList<File> dirs = new LinkedList<File>();
		LinkedList<Integer> depths = new LinkedList<Integer>();
		String extension = fileExtension.getValue();
		int maxDepth = searchDepth.getInt();
		ProgressMonitor monitor= new ProgressMonitor(null, "Searching directory "+dir.getAbsolutePath(),"Searching...", 0,2);
		if (recurse) {
			dirs.add(dir);
			depths.add(0);
			File d;
			int depth;
			while (dirs.size() > 0) {
				d = dirs.remove();
				depth = depths.remove();
				if ((d == null) || (depth > maxDepth)) {
					continue;
				}
				File[] files = d.listFiles();
				if (files == null) {
					continue;
				}
				monitor.setProgress(1);
				if(monitor.isCanceled())break;
				monitor.setNote(d.getAbsolutePath());
				for (File f : files) {
					if (f.isDirectory()) {
						dirs.add(f);
						depths.add(depth + 1);
					} else {
						String ext = FileReaderWriter.getFileExtension(f);
						boolean accept = true;
						if (exp.length() > 0) {
							switch (rule) {
							case 0:
								if (FileReaderWriter.getFileName(f).indexOf(exp) < 0) {
									accept = false;
								}
								break;
							case 1:
								if (!FileReaderWriter.getFileName(f).matches(exp)) {
									accept = false;
								}
								break;
							case 2:
								if (!FileReaderWriter.getFileName(f).startsWith(exp)) {
									accept = false;
								}
								break;
							case 3:
								if (!FileReaderWriter.getFileName(f).endsWith(exp)) {
									accept = false;
								}
								break;
							}
						}
						if (accept && (ext.equals(extension) || (extension.length() == 0))&&f.isFile()) {
							fileList.add(f);
						}
					}
				}
			}
		} else {
			File[] files = dir.listFiles();
			for (File f : files) {
				String ext = FileReaderWriter.getFileExtension(f);
				boolean accept = true;
				if (exp.length() > 0) {
					switch (rule) {
					case 0:
						if (FileReaderWriter.getFileName(f).indexOf(exp) < 0) {
							accept = false;
						}
						break;
					case 1:
						if (!FileReaderWriter.getFileName(f).matches(exp)) {
							accept = false;
						}
						break;
					case 2:
						if (!FileReaderWriter.getFileName(f).startsWith(exp)) {
							accept = false;
						}
						break;
					case 3:
						if (!FileReaderWriter.getFileName(f).endsWith(exp)) {
							accept = false;
						}
						break;
					}
				}
				if (accept && (ext.equals(extension) || (extension.length() == 0))&&f.isFile()) {
					fileList.add(f);
				}
			}
		}
		monitor.close();
		Collections.sort(fileList);
	}
	
	
	/**
	 * Instantiates a new pipe volume directory surface.
	 */
	public PipeVolumeDirectoryCollectionSource() {
		super();		
		getParentPort().setParameter(fileGroup); //?
		
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume Collection Directory");
		group.setName("volumedirectory");
		group.add(dirParam = new ParamFile("Directory", ParamFile.DialogType.DIRECTORY));
		group.add(includeParam = new ParamBoolean("Include Sub-directories", true));
		group.add(searchDepth = new ParamInteger("Max Directory Depth", 0, 100000, 1));
		group.add(fileExtension = new ParamString("File Extension"));
		fileExtension.setValue("xml");
		group.add(filterRule = new ParamOption("Filter Rule", new String[] { "Contains", "Matches","Begins With", "Ends With" }));
		group.add(filterExpression = new ParamString("Filter Expression"));
		group.setCategory("Volume");
		return group;
			
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();		
		group.setLabel("Volume");
		group.add(fileGroup = new ParamVolumeCollection("Volume Collection"));
		return group;
	}


	@Override
	public ParamVolumeCollection getOutputParam() {
		System.out.println(getClass().getCanonicalName()+"\t"+"here");
		findMatchingFiles();
		fileGroup = new ParamVolumeCollection("VolDirColl");
		for(File file : fileList) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Debug: "+file.getName());
			fileGroup.add(file);	
		}				
		return fileGroup;
	}

}
