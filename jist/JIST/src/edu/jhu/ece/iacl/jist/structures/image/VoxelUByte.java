package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;

// TODO: Auto-generated Javadoc
/**
 * Unsigned Byte Voxel Type
 * Values are stored as signed byte, but recalled as unsigned byte.
 * 
 * @author Blake Lucas
 */
public class VoxelUByte extends Voxel{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The vox. */
	private byte vox;

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getBoolean()
	 */
	public boolean getBoolean() {
		return (vox!=0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getShort()
	 */
	public short getShort() {
		return getUByte();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getColor()
	 */
	public Color getColor() {
		return new Color(vox,vox,vox);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getFloat()
	 */
	public float getFloat() {
		return (float)(getUByte());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getDouble()
	 */
	public double getDouble() {
		return (double)(getUByte());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#clone()
	 */
	public Voxel clone() {
		return new VoxelUByte(vox);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public void set(Voxel v) {
		this.vox=(byte)v.getUByte();
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(byte)
	 */
	public void set(byte vox){
		this.vox=vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(boolean)
	 */
	public void set(boolean vox) {
		this.vox=(byte)((vox)?1:0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(short)
	 */
	@Override
	public void set(short vox) {
		this.vox=(byte)vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(int)
	 */
	public void set(int vox) {
		this.vox=(byte)vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(java.awt.Color)
	 */
	public void set(Color vox) {
		float[] hsb=new float[3];
		hsb=Color.RGBtoHSB(vox.getRed(),vox.getGreen(),vox.getBlue(),hsb);
		this.vox=(byte)Math.round(255.0*hsb[2]);		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(double)
	 */
	public void set(double vox) {
		this.vox=(byte)Math.round(vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#add(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelUByte add(Voxel v) {
		return new VoxelUByte((byte)(getUByte()+v.getUByte()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#sub(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelUByte sub(Voxel v) {
		return new VoxelUByte((byte)(getUByte()-v.getUByte()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#mul(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelUByte mul(Voxel v) {
		return new VoxelUByte((byte)(getUByte()*v.getUByte()));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#div(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelUByte div(Voxel v) {
		return new VoxelUByte((byte)(getUByte()/v.getUByte()));

	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#neg()
	 */
	public Voxel neg() {
		return new VoxelUByte((byte)(-getUByte()));
	}
	
	/**
	 * Instantiates a new voxel u byte.
	 * 
	 * @param b the b
	 */
	public VoxelUByte(short b){
		set(b);
	}
	
	/**
	 * Instantiates a new voxel u byte.
	 * 
	 * @param b the b
	 */
	public VoxelUByte(byte b){
		set(b);
	}
	
	/**
	 * Instantiates a new voxel u byte.
	 */
	public VoxelUByte(){
		this.vox=0;
	}
	
	/**
	 * Instantiates a new voxel u byte.
	 * 
	 * @param v the v
	 */
	public VoxelUByte(Voxel v){set(v);}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Voxel obj) {
		return (int)(this.getUByte()-((Voxel)obj).getUByte());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getInt()
	 */
	public int getInt() {
		return getUByte();
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getUByte()
	 */
	public short getUByte(){
		short b=vox;
		if(b<0)b=(short)(256+b);
		return b;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#toString()
	 */
	public String toString(){
		return (vox+"");
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getType()
	 */
	public VoxelType getType() {
		return VoxelType.UBYTE;
	}
}
