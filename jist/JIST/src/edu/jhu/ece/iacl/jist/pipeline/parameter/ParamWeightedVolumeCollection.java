/**
 * Java Image Science Toolkit (JIST) Image Analysis and Communications
 * Laboratory & Laboratory for Medical Image Computing & The Johns Hopkins
 * University http://www.nitrc.org/projects/jist/ This library is free software;
 * you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version. The
 * license is available for reading at: http://www.gnu.org/copyleft/lgpl.html
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.factory.ParamWeightedVolumeCollectionFactory;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Weighted volumes stores a collection of cubic volumes and weights associated
 * with each volume.
 * 
 * @author Blake Lucas
 */
public class ParamWeightedVolumeCollection<T> extends ParamVolumeCollection {
	/**
	 * Prototype class for weights
	 */
	protected ParamModel<T> prototype;

	public boolean xmlEncodeParam(Document document, Element parent) {
		super.xmlEncodeParam(document, parent);		 
		Element em;				
		em = document.createElement("prototype");
		if(prototype!=null) {			
			if(prototype.xmlEncodeParam(document, em))
				parent.appendChild(em);
		}		
		return true;
	}
	
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		Element el = JistXMLUtil.xmlReadElement(parent, "prototype");
		String classname = JistXMLUtil.xmlReadTag(el, "classname");
		try {
			prototype = (ParamModel)Class.forName(classname).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prototype.xmlDecodeParam(document, el);			
	}
	/**
	 * Cache of old weighted volumes
	 */	
	transient protected ArrayList<ParamFile> cachedParams;
	/**
	 * Construct a new weighted volume collection with no restrictions.
	 */
	public ParamWeightedVolumeCollection() {
		super();
		this.setMaxIncoming(-1);
		this.factory = new ParamWeightedVolumeCollectionFactory(this);
	}
	/**
	 * Construct a new weighted volume collection with specified restrictions.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamWeightedVolumeCollection(String name, ParamModel<T> prototype) {
		this(name,prototype,null);
	}
	/**
	 * Construct a new weighted volume collection with specified restrictions.
	 * 
	 * @param name
	 *            parameter name
	 * @param type
	 *            voxel type restriction
	 */
	public ParamWeightedVolumeCollection(String name, ParamModel<T> prototype, VoxelType type) {
		this(type, prototype);
		setName(name);
	}

	/**
	 * Construct a new weighted volume collection with specified restrictions.
	 * 
	 * @param name
	 *            parameter name
	 * @param type
	 *            voxel type restriction
	 * @param rows
	 *            rows restriction
	 * @param slices
	 *            slices restriction
	 * @param cols
	 *            columns restriction
	 * @param components
	 *            components restriction
	 */
	public ParamWeightedVolumeCollection(String name, ParamModel<T> prototype, VoxelType type, int rows, int cols, int slices,
			int components) {
		this(type, prototype, rows, cols, slices, components);
		setName(name);
	}

	/**
	 * Construct a new weighted volume collection with specified restrictions.
	 * 
	 * @param type
	 *            voxel type restriction
	 */
	public ParamWeightedVolumeCollection(VoxelType type, ParamModel<T> prototype) {
		super(type);
		cachedParams=new ArrayList<ParamFile>();
		this.setMaxIncoming(-1);
		this.factory = new ParamWeightedVolumeCollectionFactory(this);
		this.prototype = prototype;
	}

	/**
	 * Construct a new weighted volume collection with specified restrictions.
	 * 
	 * @param type
	 *            voxel type restriction
	 * @param rows
	 *            rows restriction
	 * @param slices
	 *            slices restriction
	 * @param cols
	 *            columns restriction
	 * @param components
	 *            components restriction
	 */
	public ParamWeightedVolumeCollection(VoxelType type, ParamModel<T> prototype, int rows, int cols, int slices, int components) {
		super(type);
		cachedParams=new ArrayList<ParamFile>();
		this.setMaxIncoming(-1);
		this.factory = new ParamWeightedVolumeCollectionFactory(this);
		this.rows = rows;
		this.slices = slices;
		this.cols = cols;
		this.components = components;
		this.prototype = prototype;
	}

	/**
	 * A new ParamWeightedVolume is created with the value set to the specified
	 * value.
	 * 
	 * @param value
	 *            parameter value
	 * @return weighted parameter volume
	 */
	public ParamWeightedVolume<T> create(Object value) {
		ParamWeightedVolume<T> param;
		if(cachedParams.size()>0){
			//Use cached parameter so that previous weight will be preserved even if the volume changes!
			param=(ParamWeightedVolume<T>)cachedParams.remove(0);
		} else {
			param = new ParamWeightedVolume<T>(getName(), prototype.clone(), type, rows, cols,slices, components);
		}
		if (value instanceof String) {
			param.setValue((String) value);
		} else if (value instanceof File) {
			param.setValue((File) value);
		} else if (value instanceof ImageData) {
			param.setValue((ImageData) value);
		}/* else if (value instanceof ModelImage) {
			param.setValue((ModelImage) value);
		}*/
		for (ParamFile vol : fileParams) {
			if (vol.equals(param)) {
				return (ParamWeightedVolume) vol;
			}
		}
		return param;
	}
	/**
	 * Get list of weights
	 * 
	 */
	public ArrayList<T> getWeightList() {
		ArrayList<T> weights=new ArrayList<T>();
		for(int i=0;i<size();i++){
			weights.add(getWeight(i));
		}
		return weights;
	}
	/**
	 * Get the volume weight for the specified volume index.
	 * 
	 * @param i
	 *            the i
	 * @return the weight
	 */
	public T getWeight(int i) {
		return getWeightedImage(i).getWeight();
	}

	/**
	 * Construct a new weighted volume collection with specified restrictions.
	 * 
	 * @param i
	 *            index
	 * @return the weighted volume
	 */
	public ParamWeightedVolume<T> getWeightedImage(int i) {
		return ((ParamWeightedVolume<T>) fileParams.get(i));
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		cachedParams=new ArrayList<ParamFile>();
		this.setMaxIncoming(-1);
		connectible = true;
		factory = new ParamWeightedVolumeCollectionFactory(this);
	}

	/**
	 * Set the volume weight for the specified volume index.
	 * 
	 * @param i
	 *            index
	 * @param weight
	 *            volume weight
	 */
	public void setWeight(int i, T weight) {
		getWeightedImage(i).setWeight(weight);
	}

	/**
	 * @return the prototype
	 */
	public ParamModel<T> getPrototype() {
		return prototype;
	}

	/**
	 * @param prototype
	 *            the prototype to set
	 */
	public void setPrototype(ParamModel<T> prototype) {
		this.prototype = prototype;
	}
	/**
	 * Clone object.
	 * 
	 * @return the param volume collection
	 */
	public ParamWeightedVolumeCollection<T> clone() {
		ParamWeightedVolumeCollection<T> param = new ParamWeightedVolumeCollection<T>(getName(),prototype,type);
		param.rows = rows;
		param.slices = slices;
		param.components = components;
		param.cols = cols;
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.fileParams = new Vector<ParamFile>(fileParams.size());
		for (ParamFile p : fileParams) {
			param.fileParams.add(((ParamWeightedVolume)p).clone());
		}
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}
	/**
	 * Get description of volumes.
	 * 
	 * @return the string
	 */
	public String toString() {

		String text = "<HTML><OL>";
		for (ParamFile file : fileParams) {
			text +="<LI>"+ file.toString() + "</LI>";
		}
		text+="</OL></HTML>";
		return text;
	}
	/**
	 * Clear list and add members to cache
	 */
	public void clear() {
		
		for(ParamFile f:fileParams){
			if(cachedParams!=null&&f!=null){
				cachedParams.add(((ParamWeightedVolume)f).clone());
			}
		}
		fileParams.clear();
	}
	public void addWeightedVolume(){
		ParamWeightedVolume newParam=new ParamWeightedVolume(prototype.clone());
		fileParams.add(newParam);
	}
	public void setUseConnector(boolean useWire) {
		boolean before=this.usingConnector();
		super.setUseConnector(useWire);
		if(before&&!useWire){
			clear();
		}
	}
	
	@Override
	public String getXMLValue() {
		return getXMLValue()+";;;"+prototype.getXMLValue();
	}
	
	@Override
	public void setXMLValue(String arg) {
		String []args = arg.trim().split(";;;");
		if(args.length!=2)
			throw new InvalidParameterValueException(this,"Cannot parse weighted volumes: "+arg);
		super.setXMLValue(args[0]);
		prototype.setXMLValue(args[1]);
	}
}
