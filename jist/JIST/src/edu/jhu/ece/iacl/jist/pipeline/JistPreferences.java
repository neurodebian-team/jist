/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.view.Preferences;

/**
 * MAPS preferences.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class JistPreferences {

	/** The debug level location. */
	public final static String []layoutExtensions = new String[]{
			"layout",
			"LayoutXML"};
	
	/** The Constant fileExtensionLayout. */
	private String fileExtensionLayout = layoutExtensions[1];

	/** The Constant fileExtensionModuleDefinition. */
	private static final String fileExtensionModuleDefinition = "module";

	/** The Constant fileNamePreferences. */
	private static final String fileNamePreferences = "JistUserPreferences.xml";

	/** Numeric revision for algorithm compatibility **/
	public static final float JIST_VERSION_ID = 1.1f;
	
	/**
	 * Gets the default layout extension.
	 * 
	 * @return the default layout extension
	 */
	public  String getDefaultLayoutExtension()  {
		return fileExtensionLayout;
	}

	public  String[] getAllValidLayoutExtensions()  {
		return layoutExtensions.clone();
	}

	/**
	 * Gets the default module extension.
	 * 
	 * @return the default module extension
	 */
	public static String getDefaultModuleExtension() {
		return fileExtensionModuleDefinition;
	}

	/**
	 * Gets the default preferences file name.
	 * 
	 * @return the default preferences file name
	 */
	public static String getDefaultPreferencesFileName() {
		return fileNamePreferences;
	}

	/**
	 * The Enum NamingConvention.
	 */
	public enum NamingConvention{
		/** The TREE. */
		TREE,
		/** The UNIVERSA l_ id. */
		UNIVERSAL_ID }

	/**
	 * Listens for changes in preferences.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static interface PreferenceListener {

		/**
		 * History change.
		 * 
		 * @param prefs the preferences
		 */
		public void historyChange(JistPreferences prefs);
	}

	/** Singleton reference to preferences. */
	private static JistPreferences prefs = null;

	/**
	 * Open file chooser to select directory.
	 * 
	 * @param oldDir the old directory
	 * 
	 * @return absolute path of the file
	 */
	public static File askForJavaExecutable(File oldDir) {
		JFileChooser openDialog = new JFileChooser();
		openDialog.setSelectedFile(MipavController.getDefaultWorkingDirectory());
		openDialog.setDialogTitle("Select Java Executable");
		openDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		File jreFile = new File(JistPreferences.getPreferences().getJre());
		if (jreFile != null) {
			openDialog.setSelectedFile(jreFile);
		}
		openDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		openDialog.setAcceptAllFileFilterUsed(false);
		openDialog.setFileFilter(new FileFilter() {
			public boolean accept(File file) {
				if (file.exists() && file.canExecute()) {
					return true;
				} else {
					return false;
				}
			}

			public String getDescription() {
				return "Executable";
			}
		});
		int returnVal = openDialog.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			MipavController.setDefaultWorkingDirectory(openDialog.getSelectedFile().getParentFile());
			File f = openDialog.getSelectedFile();
			if (f.canExecute()) {
				return f;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Reconstruct algorithm from string.
	 * 
	 * @param str the string
	 * 
	 * @return the maps preferences
	 */
	public static JistPreferences fromXML(String str) {
		XStream stream = new XStream();
		Object o = stream.fromXML(str);
		if (o == null) {
			return null;
		}
		if (o instanceof JistPreferences) {
			return (JistPreferences) o;
		} else {
			return null;
		}
	}

	/**
	 * Singleton method to get User Interface.
	 * 
	 * @return userInterface
	 */
	public static JistPreferences getPreferences() {
		if(PipeLibrary.getInstance().getLibraryPath()==null){
			PipeLibrary.getInstance().loadPreferences();
		}
		if (prefs == null) {
			prefs = new JistPreferences();
		}
		return prefs;
	}

	/**
	 * Returns true if MAPS has preferences already.
	 * 
	 * @return true if preferences exist
	 */
	public static boolean hasPreferences() {
		return (prefs != null);
	}

	/**
	 * Load existing modules and add them to the tree.
	 */
	public static void loadPreferences() {
		loadPreferences(false);
	}

	/**
	 * Load existing modules and add them to the tree.
	 */
	public static void loadPreferences(boolean silentFailure) {
		File libraryPath=PipeLibrary.getInstance().getLibraryPath();
		if(libraryPath==null){
			JistLogger.logError(JistLogger.WARNING, "Pipe library directory is not specified");
			return;
		}
		File prefFile = new File(libraryPath, getDefaultPreferencesFileName());
		if (!JistPreferences.hasPreferences()) {
			if (prefFile.exists()) {
				JistPreferences prefs = JistPreferences.read(prefFile);
				JistPreferences.setPreferences(prefs);
				JistLogger.logOutput(JistLogger.FINE, "Using preference file:" + prefFile);
			} else {			
				JistLogger.logError(JistLogger.CONFIG, "Using Default JIST Preferences. Could not find preference file:" + prefFile);
				JistPreferences.setPreferences(new JistPreferences());
			}
			JistLogger.logOutput(JistLogger.CONFIG, "Current JIST debug level:" + JistPreferences.getPreferences().getDebugLevel());
		} else {
			//System.out.println("jist.base"+"\t"+"Preferences already initialized");
//			JistLogger.logError(JistLogger.CONFIG, "Using Default JIST Preferences");
			// Already loaded
		}
		
	}

	/**
	 * Read preferences from file.
	 * 
	 * @param f the file
	 * 
	 * @return the maps preferences
	 */
	public static JistPreferences read(File f) {
		if ((f == null) || !f.exists()) {
			return null;
		}
		BufferedReader in;
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			String text = "";
			StringBuffer buff = new StringBuffer();
			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str + "\n");
			}
			text = buff.toString();
			in.close();
			// Reconstruct class from XML
			JistPreferences algo = fromXML(text);
			return algo;
		} catch (Exception e) {
			System.err.println("jist.base"+"Error occured while reading parameter file:\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Save preferences along library path as getDefaultPreferencesFileName().
	 * 
	 * @return true if preferences saved correctly
	 */
	public static boolean savePreferences() {
		return JistPreferences.getPreferences().write(
				new File(PipeLibrary.getInstance().getLibraryPath(), getDefaultPreferencesFileName()));
	}

	/**
	 * Set singleton reference to preferences.
	 * 
	 * @param prefs preferences
	 */
	public static void setPreferences(JistPreferences prefs) {
		JistPreferences.prefs = prefs;
	}

	/** Default maximum heap size. */
	protected int defaultMemory;

	/** Default maximum number of simultaneous processes. */
	protected int defaultMaxProcesses;

	/** history of recent layout files. */
	protected Vector<File> fileHistory;

	/** last directory inspected. */
	protected File lastDirectory;

	/** maximum number of files to display in history. */
	protected int maxHistorySize;

	/** location of java executable. */
	protected String jre;

	/** Preferred output extension for CubicVolumeReaderWriter. */
	protected String preferredextension;

	/** Preferred output compression mechanism. */
	protected String preferredCompression;

	/** Use Grid Engine. */
	protected boolean useGridEngine;

	/** namingConvention. */
	protected NamingConvention namingConvention;
	
	/** Debug Level **/ 
	protected int debugLevel; 

	public static int smFontSize ;
	public static int mdFontSize ;
	public static int lgFontSize ;
	
	/**
	 * Gets the naming convention.
	 * 
	 * @return the naming convention
	 */
	public NamingConvention getNamingConvention() {
		return namingConvention;
	}

	/**
	 * Sets the naming convention.
	 * 
	 * @param namingConvention the new naming convention
	 */
	public void setNamingConvention(NamingConvention namingConvention) {
		this.namingConvention = namingConvention;
	}

	/**
	 * Checks if is use grid engine.
	 * 
	 * @return true, if is use grid engine
	 */
	public boolean isUseGridEngine() {
		return useGridEngine;
	}

	/**
	 * Sets the use grid engine.
	 * 
	 * @param useGridEngine the new use grid engine
	 */
	public void setUseGridEngine(boolean useGridEngine) {
		System.out.println("jist.base"+"\t"+"Set UseGridEngine: "+useGridEngine);
		this.useGridEngine = useGridEngine;
	}

	/** Listeners that react to changes in preferences. */
	transient protected LinkedList<PreferenceListener> listeners;

	/** The debug level location. */
	public final static String []debugLevels = new String[]{
			"0 (severe)",
			"1 (warning)",
			"2 (info)",
			"3 (config)",
			"4 (fine)",
			"5 (finer)",
			"6 (finest)",
			"7 (INTENSIVE I)",
			"8 (INTENSIVE II)",
			"9 (INTENSIVE III)"};
	

	/**
	 * Default constructor.
	 */
	public JistPreferences() {		
		String heapSize = Preferences.getProperty(Preferences.PREF_MAX_HEAP_SIZE);
		defaultMemory = (heapSize!=null)?Integer.parseInt(heapSize):1024;
		defaultMaxProcesses = 2;
		maxHistorySize = 10;
		lastDirectory = null;
		jre = System.getProperty("java.home")+File.separator+"bin"+File.separator+"java";
		preferredextension = "xml";
		preferredCompression = "none";
		useGridEngine=false;
		namingConvention=null;
		fileHistory = new Vector<File>();
		debugLevel = 1; 
		smFontSize = 10;
		mdFontSize = 12;
		lgFontSize = 16;
		fileExtensionLayout = layoutExtensions[0];
	}

	/**
	 * Add file to history.
	 * 
	 * @param f file
	 */
	public void addFileToHistory(File f) {
		if (f.exists()) {
			fileHistory.remove(f);
			fileHistory.insertElementAt(f, 0);
			while (fileHistory.size() > maxHistorySize) {
				fileHistory.remove(fileHistory.size() - 1);
			}
		}
		if (listeners != null) {
			for (PreferenceListener listener : listeners) {
				listener.historyChange(this);
			}
		}
	}

	/**
	 * Add listener.
	 * 
	 * @param listener the listener
	 */
	public void addListener(PreferenceListener listener) {
		if (listeners == null) {
			listeners = new LinkedList<PreferenceListener>();
		}
		listeners.add(listener);
	}

	/**
	 * Get default maximum number of processes.
	 * 
	 * @return the default max processes
	 */
	public int getDefaultMaxProcesses() {
		return defaultMaxProcesses;
	}

	/**
	 * Get default maximum heap size.
	 * 
	 * @return heap size in MB
	 */
	public int getDefaultMemory() {
		return defaultMemory;
	}

	/**
	 * Get list of recently accessed files.
	 * 
	 * @return the file history
	 */
	public Vector<File> getFileHistory() {
		return fileHistory;
	}

	/**
	 * Get JRE executable.
	 * 
	 * @return the jre
	 */
	public String getJre() {
		return jre;
	}

	/**
	 * Get preferred extension (for CubicVolumeReaderWriter).
	 * 
	 * @return the preferred extension
	 */
	public String getPreferredExtension()
	{
		if(preferredextension==null)
			return "xml";
		else 
			return preferredextension;
	}

	/**
	 * Get preferred compression (for CubicVolumeReaderWriter).
	 * 
	 * @return the preferred compression
	 */
	public String getPreferredCompression()
	{
		if(preferredCompression==null)
			return "none";
		else 
			return preferredCompression;
	}
	
	/**
	 * Get last directory entered.
	 * 
	 * @return last directory
	 */
	public File getLastDirectory() {
		if (lastDirectory == null) {
			return MipavController.getDefaultWorkingDirectory();
		} else {
			return lastDirectory;
		}
	}

	/**
	 * Get maximum history size.
	 * 
	 * @return maximum history size
	 */
	public int getMaxHistorySize() {
		return maxHistorySize;
	}

	/**
	 * Remove listener.
	 * 
	 * @param listener the listener
	 */
	public void removeListener(PreferenceListener listener) {
		if (listeners == null) {
			listeners = new LinkedList<PreferenceListener>();
		}
		listeners.remove(listener);
	}

	/**
	 * Set default maximum number of processes.
	 * 
	 * @param defaultMaxProcesses maximum number of processes
	 */
	public void setDefaultMaxProcesses(int defaultMaxProcesses) {
		this.defaultMaxProcesses = defaultMaxProcesses;
	}

	/**
	 * Set default maximum heap size.
	 * 
	 * @param defaultMemory heap size in MB
	 */
	public void setDefaultMemory(int defaultMemory) {
		this.defaultMemory = defaultMemory;
	}

	/**
	 * Set JRE executable.
	 * 
	 * @param jre the jre
	 */
	public void setJre(String jre) {
		this.jre = jre;
	}

	/**
	 * Set the preferred extension (for CubicVolumeReaderWriter).
	 * 
	 * @param prefext the preferred extension
	 */
	public void setPreferredExtension(String prefext)
	{
		this.preferredextension = prefext;
		System.out.println("jist.base"+"\t"+"JistPreferences New Preference: " + prefext);
	}
	
	/**
	 * Set the preferred extension (for CubicVolumeReaderWriter).
	 * 
	 * @param prefext the preferred extension
	 */
	public void setPreferredCompression(String prefext)
	{
		this.preferredCompression = prefext;		
	}

	/**
	 * Set last directory entered.
	 * 
	 * @param lastDirectory the last directory
	 */
	public void setLastDirectory(File lastDirectory) {
		this.lastDirectory = lastDirectory;
	}

	/**
	 * Serialize class as XML.
	 * 
	 * @return the string
	 */
	public String toXML() {
		XStream stream = new XStream();
		return stream.toXML(this);
	}

	/**
	 * Write algorithm to file.
	 * 
	 * @param f output file
	 * 
	 * @return true, if write
	 */
	public boolean write(File f) {
		PrintWriter out;
		if (f == null) {
			return false;
		}
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(f)));
			String text = toXML();
			out.print(text);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.err.println("jist.base"+e.getMessage());
			return false;
		}
	}
	
	/**
	 * 
	 * Gets the preferred level for debug output: 
	 * 		0 = Error messages only 
	 * 		1 = Major events and warnings
	 * 		...
	 * 		6 = All routine messages.
	 * THE FOLLOW MODES MAY IMPACT PERFORMANCE. USE WITH CAUTION! 
	 * 		7 - Intense debug 1
	 * 		8 - Intense debug 2
	 * 		9 - Intense debug 3
	 * 
	 * @return debugLevel (int)
	 */
	public int getDebugLevel() {
		return debugLevel;
	}

	public void setDebugLevel(int selected) {
		debugLevel=selected;		
	}
	
	public int getSmFontSize() {
		return 10;//smFontSize;
	}
	
	public int getMdFontSize() {
		return 12;//mdFontSize;
	}
	public int getLgFontSize() {
		return 16;//lgFontSize;
	}
	
	public void setSmFontSize(int selected) {
		smFontSize=selected;		
	}
	
	public void setMdFontSize(int selected) {
		mdFontSize=selected;		
	}
	
	public void setLgFontSize(int selected) {
		lgFontSize=selected;		
	}

	public void setDefaultLayoutExtension(String string) {
		for(String s : layoutExtensions) {
			if(s.equalsIgnoreCase(string))
					fileExtensionLayout = s;
		}
	}

	

}