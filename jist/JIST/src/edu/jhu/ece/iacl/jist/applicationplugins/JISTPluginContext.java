package edu.jhu.ece.iacl.jist.applicationplugins;

public interface JISTPluginContext {
	public enum ContextType {
		// MENU ITEMS (Null execute "Object" passed)
		MENU_FILE, 
		MENU_EDIT,
		MENU_PROJECT, 
		MENU_HELP,
		MENU_PLUGINS,
		MENU_SCHEDULE,
		// "Right-Click" ITEMS (Reference to clicked object passed)
		CONTEXT_LAYOUT,
		CONTEXT_MODULE,
		CONTEXT_PIN, 
		CONTEXT_MODULE_TREE, 
		CONTEXT_EXECUTION
	}
	boolean isAvailable(); // is the module currently available based on system state (false = option not selectable)
	public  String getDisplayString(); //e.g., export SVG
	public  String getDisplayTooltip(); //e.g., save a screen shot of the current layout as a .SVG file
	public  String getMenuIdentifier(); //e.g., File, Edit, ContextClickLayout, ContextClickProcessManager,

}
