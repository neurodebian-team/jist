package edu.jhu.ece.iacl.jist.utility;

import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;


// TODO: Auto-generated Javadoc
/**
 * The Class FileUtil.
 */
public class FileUtil {
	
	   /** Saved STDERR filehandle */
    private static  PrintStream stderr = System.err;
    
    /** Saved STDOUT filehandle */
    private static  PrintStream stdout = System.out;
    
	/**
	 * Redirect stdout and stderr to files.
	 * 
	 * @param fout
	 *            file for stdout
	 * @param ferr
	 *            file for stderr
	 * 
	 * @return the prints the stream[]
	 */
	public static PrintStream[] redirect(File fout, File ferr) {
		if(stdout==null)
		{
		    stderr = System.err;
		    
		    stdout = System.out;
		}
		
		PrintStream out = null;
		PrintStream err = null;
//		stdout.println("Starting redirect");
		try {
			out = new PrintStream(new BufferedOutputStream(
					new FileOutputStream(fout)));
			err = new PrintStream(new BufferedOutputStream(
					new FileOutputStream(ferr)));
			System.out.flush();
			System.setOut(out);
			System.err.flush();
			System.setErr(err);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		stdout.println("Finished redirect");
		return new PrintStream[] { out, err };
	}
	
	public static void restoreRedirect() {
//		if(cachedOutErr!=null) {
//		stdout.println("Un-redirect");
		System.setOut(stdout);
		System.setErr(stderr);
//		stdout.println("Finished Un-redirect");
		stdout.flush();
//		}
	}

	
	/**
	 * Copy important file information between ModelImage structures,
	 * assuming all slices have same properties (uses only the first slice from
	 * the source).
	 * 
	 * @param image the image
	 * @param resultImage the result image
	 */
    public static final void updateFileInfo(ModelImage image, ModelImage resultImage) {
        FileInfoBase[] fileInfo;

        if (resultImage.getNDims() == 2) {
            fileInfo = resultImage.getFileInfo();
            
			fileInfo[0].setModality(image.getFileInfo()[0].getModality());
            fileInfo[0].setFileDirectory(image.getFileInfo()[0].getFileDirectory());
			fileInfo[0].setEndianess(image.getFileInfo()[0].getEndianess());
            fileInfo[0].setUnitsOfMeasure(image.getFileInfo()[0].getUnitsOfMeasure());
            fileInfo[0].setResolutions(image.getFileInfo()[0].getResolutions());
            fileInfo[0].setAxisOrientation(image.getFileInfo()[0].getAxisOrientation());
            fileInfo[0].setOrigin(image.getFileInfo()[0].getOrigin());
            fileInfo[0].setPixelPadValue(image.getFileInfo()[0].getPixelPadValue());
            fileInfo[0].setPhotometric(image.getFileInfo()[0].getPhotometric());
			
			fileInfo[0].setImageOrientation(image.getImageOrientation());
            
			fileInfo[0].setExtents(resultImage.getExtents());
            fileInfo[0].setMax(resultImage.getMax());
            fileInfo[0].setMin(resultImage.getMin());
            
        } else if (resultImage.getNDims() == 3) {
			//System.out.print("3:");
            fileInfo = resultImage.getFileInfo();

            for (int i = 0; i < resultImage.getExtents()[2]; i++) {
                fileInfo[i].setModality(image.getFileInfo()[0].getModality());
                fileInfo[i].setFileDirectory(image.getFileInfo()[0].getFileDirectory());
				fileInfo[i].setEndianess(image.getFileInfo()[0].getEndianess());
                fileInfo[i].setUnitsOfMeasure(image.getFileInfo()[0].getUnitsOfMeasure());
                fileInfo[i].setResolutions(image.getFileInfo()[0].getResolutions());
                fileInfo[i].setAxisOrientation(image.getFileInfo()[0].getAxisOrientation());
                fileInfo[i].setOrigin(image.getFileInfo()[0].getOrigin());
                fileInfo[i].setPixelPadValue(image.getFileInfo()[0].getPixelPadValue());
                fileInfo[i].setPhotometric(image.getFileInfo()[0].getPhotometric());
               
		        fileInfo[i].setImageOrientation(image.getImageOrientation());
				
				fileInfo[i].setExtents(resultImage.getExtents());
                fileInfo[i].setMax(resultImage.getMax());
                fileInfo[i].setMin(resultImage.getMin());
            }
        } else if (resultImage.getNDims() == 4) {
            //System.out.print("4:");
            fileInfo = resultImage.getFileInfo();

			int[] units = new int[4];
			float[] res = new float[4];
			for (int n=0;n<4;n++) {
				if (n<image.getNDims()) {
					units[n] = image.getFileInfo()[0].getUnitsOfMeasure()[n];
					res[n] = image.getFileInfo()[0].getResolutions()[n];
				} else {
					units[n] = image.getFileInfo()[0].getUnitsOfMeasure()[image.getNDims()-1];
					res[n] = image.getFileInfo()[0].getResolutions()[image.getNDims()-1];
				}					
			}
				
            for (int i = 0; i < (resultImage.getExtents()[2] * resultImage.getExtents()[3]); i++) {
                fileInfo[i].setModality(image.getFileInfo()[0].getModality());
                fileInfo[i].setFileDirectory(image.getFileInfo()[0].getFileDirectory());
                fileInfo[i].setEndianess(image.getFileInfo()[0].getEndianess());
                fileInfo[i].setAxisOrientation(image.getFileInfo()[0].getAxisOrientation());
                fileInfo[i].setOrigin(image.getFileInfo()[0].getOrigin());
                fileInfo[i].setPixelPadValue(image.getFileInfo()[0].getPixelPadValue());
                fileInfo[i].setPhotometric(image.getFileInfo()[0].getPhotometric());
				
				fileInfo[i].setUnitsOfMeasure(units);
                fileInfo[i].setResolutions(res);
                
				fileInfo[i].setImageOrientation(image.getImageOrientation());
                
				fileInfo[i].setExtents(resultImage.getExtents());
                fileInfo[i].setMax(resultImage.getMax());
                fileInfo[i].setMin(resultImage.getMin());
            }
        }
    }
    
    /** The Constant COMPRESSION_GZIP. */
    public static final int COMPRESSION_GZIP =FileInfoBase.COMPRESSION_GZIP;
    
    /** The Constant COMPRESSION_ZIP. */
    public static final int COMPRESSION_ZIP =FileInfoBase.COMPRESSION_ZIP;
    
    /** The Constant COMPRESSION_NONE. */
    public static final int COMPRESSION_NONE =FileInfoBase.COMPRESSION_NONE;
    
    /* Not working yet -- appears to be over written */ 
/**
     * Sets the model image compression.
     * 
     * @param resultImage the result image
     * @param compressionType the compression type
     */
    public static final void setModelImageCompression(ModelImage resultImage, int compressionType) {
	
	FileInfoBase[] fileInfo;

    if (resultImage.getNDims() == 2) {
        fileInfo = resultImage.getFileInfo();
        fileInfo[0].setCompressionType(compressionType);
        
    } else if (resultImage.getNDims() == 3) {
		//System.out.print("3:");
        fileInfo = resultImage.getFileInfo();

        for (int i = 0; i < resultImage.getExtents()[2]; i++) {
        	fileInfo[i].setCompressionType(compressionType);
        }
    } else if (resultImage.getNDims() == 4) {
        //System.out.print("4:");
        fileInfo = resultImage.getFileInfo();
        for (int i = 0; i < (resultImage.getExtents()[2] * resultImage.getExtents()[3]); i++) {
        	fileInfo[i].setCompressionType(compressionType);            
        }
    }
}
    
	/**
	 * Copy important file information between ModelImage structures,
	 * assuming all slices have same properties (uses only the first slice from
	 * the source).
	 * 
	 * @param info the info
	 * @param resultImage the result image
	 */
    public static final void updateFileInfo(FileInfoBase info, ModelImage resultImage) {
        FileInfoBase[] fileInfo;

        if (resultImage.getNDims() == 2) {
            fileInfo = resultImage.getFileInfo();
            
			fileInfo[0].setModality(info.getModality());
            fileInfo[0].setFileDirectory(info.getFileDirectory());
			fileInfo[0].setEndianess(info.getEndianess());
            fileInfo[0].setUnitsOfMeasure(info.getUnitsOfMeasure());
            fileInfo[0].setResolutions(info.getResolutions());
            fileInfo[0].setAxisOrientation(info.getAxisOrientation());
            fileInfo[0].setOrigin(info.getOrigin());
            fileInfo[0].setPixelPadValue(info.getPixelPadValue());
            fileInfo[0].setPhotometric(info.getPhotometric());
			
			fileInfo[0].setImageOrientation(info.getImageOrientation());
            
			fileInfo[0].setExtents(resultImage.getExtents());
            fileInfo[0].setMax(resultImage.getMax());
            fileInfo[0].setMin(resultImage.getMin());
            
        } else if (resultImage.getNDims() == 3) {
			//System.out.print("3:");
            fileInfo = resultImage.getFileInfo();

            for (int i = 0; i < resultImage.getExtents()[2]; i++) {
                fileInfo[i].setModality(info.getModality());
                fileInfo[i].setFileDirectory(info.getFileDirectory());
				fileInfo[i].setEndianess(info.getEndianess());
                fileInfo[i].setUnitsOfMeasure(info.getUnitsOfMeasure());
                fileInfo[i].setResolutions(info.getResolutions());
                fileInfo[i].setAxisOrientation(info.getAxisOrientation());
                fileInfo[i].setOrigin(info.getOrigin());
                fileInfo[i].setPixelPadValue(info.getPixelPadValue());
                fileInfo[i].setPhotometric(info.getPhotometric());
               
		        fileInfo[i].setImageOrientation(info.getImageOrientation());
				
				fileInfo[i].setExtents(resultImage.getExtents());
                fileInfo[i].setMax(resultImage.getMax());
                fileInfo[i].setMin(resultImage.getMin());
            }
        } else if (resultImage.getNDims() == 4) {
            //System.out.print("4:");
            fileInfo = resultImage.getFileInfo();

			int[] units = new int[4];
			float[] res = new float[4];
			for (int n=0;n<4;n++) {
				units[n] = info.getUnitsOfMeasure()[n];
				res[n] = info.getResolutions()[n];
			}
				
            for (int i = 0; i < (resultImage.getExtents()[2] * resultImage.getExtents()[3]); i++) {
                fileInfo[i].setModality(info.getModality());
                fileInfo[i].setFileDirectory(info.getFileDirectory());
                fileInfo[i].setEndianess(info.getEndianess());
                fileInfo[i].setAxisOrientation(info.getAxisOrientation());
                fileInfo[i].setOrigin(info.getOrigin());
                fileInfo[i].setPixelPadValue(info.getPixelPadValue());
                fileInfo[i].setPhotometric(info.getPhotometric());
				
				fileInfo[i].setUnitsOfMeasure(units);
                fileInfo[i].setResolutions(res);
                
				fileInfo[i].setImageOrientation(info.getImageOrientation());
                
				fileInfo[i].setExtents(resultImage.getExtents());
                fileInfo[i].setMax(resultImage.getMax());
                fileInfo[i].setMin(resultImage.getMin());
            }
        }
    }
    
    /**
     * Replace any potentially unsafe file system characters with "_".
     * 
     * @param filenameNoPath the filename no path
     * 
     * @return the string
     */
    public static String forceSafeFilename(String filenameNoPath) {
    	filenameNoPath = recoverUnsafeFilename(filenameNoPath); // Allow for symmetry - prevent % -> %25 -> %2525, etc.
    	try {
    		filenameNoPath = filenameNoPath.replaceAll("\\*", "%47"); // * is safe for URLS, but not for file names
    	return java.net.URLEncoder.encode(filenameNoPath,"UTF-8");
    	} catch (Exception e){
    		throw new RuntimeException("UTF-8 not supported!");
    	}
//    	return filenameNoPath.replaceAll("^[a-zA-Z0-9\\.-_]", "_");
    }
    
    /**
     * Recover unsafe filename.
     * 
     * @param filenameNoPath the filename no path
     * 
     * @return the string
     */
    public static String recoverUnsafeFilename(String filenameNoPath) {
    	try {
    	return java.net.URLDecoder.decode(filenameNoPath,"UTF-8");
    	} catch (Exception e){
    		throw new RuntimeException("UTF-8 not supported!");
    	}
    }
        
}
