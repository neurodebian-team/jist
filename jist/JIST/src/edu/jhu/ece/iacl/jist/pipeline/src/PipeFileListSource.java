/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of files.
 * 
 * @author Blake Lucas
 */
public class PipeFileListSource extends PipeListSource {
	
	/** The files param. */
	protected ParamFile filesParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);				
//		Element em;
//		
//		em = document.createElement("filesParam");
//		filesParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		return true;
		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);		
			
		filesParam =(ParamFile) outputParams.getFirstChildByName("File");// new ParamFile();
//		filesParam = new ParamFile();
//		filesParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filesParam"));
		getParentPort().setParameter(fileParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipeFileListSource() {
		super();
		getParentPort().setParameter(fileParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("File List");
		group.setName("filelist");
		group.setCategory("File");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File");
		group.add(filesParam = new ParamFile("File"));
		return group;
	}

	/**
	 * Get file parameters.
	 * 
	 * @return the output param
	 */
	public ParamFile getOutputParam() {
		return filesParam;
	}

	/**
	 * Get File value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected File getValue(int i) {
		if (off < data[i].length) {
			return new File((String) data[i][off]);
		} else {
			return null;
		}
	}

	/**
	 * Set file value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object obj) {
		filesParam.setValue((File) obj);
	}
}
