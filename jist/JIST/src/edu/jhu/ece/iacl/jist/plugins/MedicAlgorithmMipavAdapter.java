package edu.jhu.ece.iacl.jist.plugins;


import java.awt.Dimension;
import java.io.File;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;


import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.scripting.ImageVariableTable;
import gov.nih.mipav.model.scripting.ParsedActionLine;
import gov.nih.mipav.model.scripting.ParserEngine;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.ScriptRunner;
import gov.nih.mipav.model.scripting.parameters.Parameter;
import gov.nih.mipav.model.scripting.parameters.ParameterList;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.model.structures.ModelImage;


/**
 * The Class MedicAlgorithmMipavAdapter.
 */
public class MedicAlgorithmMipavAdapter extends ProcessingAlgorithm implements AbstractProcessingAlgorithmInterface{

	/** The volumes. */
	ParamVolumeCollection volumes;

	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param pipeFile the pipe file
	 */
	public void init(File pipeFile) {
		this.inputParams = (ParamCollection) ParamFactory.fromXML(pipeFile);
		this.outputParams.setName(inputParams.getName());
		this.outputParams.setLabel(inputParams.getLabel());
	}


	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param pipe the pipe
	 */
	public void init(PipeAlgorithm pipe) {
		this.inputParams = pipe.getInputParams();		
		this.outputParams.setName(inputParams.getName());
		this.outputParams.setLabel(inputParams.getLabel());
	}


	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "\nAlgorithm Version: " + revnum + "\n";


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("Base");
		inputParams.setLabel("MIPAV Script Adapter");
		inputParams.setName("MIPAV_Script_Adapter");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setDescription(shortDescription);
		info.add(new AlgorithmAuthor("Blake Lucas", "", ""));
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(volumes = new ParamVolumeCollection("Output Images"));
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ParamCollection inputParams = this.getInput();
		ParamCollection outputParams = this.getOutput();
		String scriptString = inputParams.getFirstChildByName("MIPAV Script")
				.getValue().toString();
		scriptString = scriptString.replace("<BR>", "\n");
		// Create output directory in case it hasn't been created yet
		File outputDir = new File(this.getOutputDirectory(), File.separatorChar
				+ this.getAlgorithmLabel());
		if (!outputDir.exists()) {
			outputDir.mkdir();
		}
		try {

			ParserEngine parser = new ParserEngine(scriptString, false);
			ParamVolume volParam;
			ModelImage img;
			LinkedList<ParamModel> scriptLines = inputParams
					.getAllDescendantsByClass(ParamCollection.class);
			Iterator<ParamModel> iter = scriptLines.iterator();
			Vector<String> imgList = new Vector<String>();
			StringBuffer newScript = new StringBuffer();
			Vector<ModelImage> miHandles = new Vector<ModelImage>();
			while (parser.hasMoreLinesToParse() && iter.hasNext()) {
				ParsedActionLine parsedLine = parser.parseNextLine();
				if (parsedLine != null) {
					ParamCollection algoLine = (ParamCollection) iter.next();
					String action = parsedLine.getAction();
					if (action.equals("CloseFrame")
							|| action.equals("SaveImage")|| action.equals("SaveImageAs")|| action.equals("SaveAll")|| action.equals("Exit"))
						continue;
					ParameterTable paramTable = parsedLine.getParameterTable();
					Parameter[] params = paramTable.getParameters();
					for (Parameter param : params) {
						switch (param.getType()) {
						case Parameter.PARAM_EXTERNAL_IMAGE:
							volParam = (ParamVolume) algoLine
									.getFirstChildByLabel(param.getLabel());
//							img = volParam.getModelImageIfOpen();							
//							imgList.add(img.getImageName());
//							throw new RuntimeException("Script adapter not implemented");
							ModelImage mi = null;
							if(volParam.getImageData() instanceof ImageDataMipav) {
								mi = ((ImageDataMipav)volParam.getImageData()).extractModelImage();
								volParam.dispose();
							} else {
								mi = volParam.getImageData().getModelImageCopy();
							}
							miHandles.add(mi);
							System.out.println("**********"+mi.getImageName());
							imgList.add(mi.getImageName());
							
							
							break;
						case Parameter.PARAM_LIST:
							ParameterList list = ((ParameterList) param);
							switch (list.getListType()) {
								case Parameter.PARAM_BOOLEAN:
								case Parameter.PARAM_DOUBLE:
								case Parameter.PARAM_FLOAT:
								case Parameter.PARAM_INT:
								case Parameter.PARAM_LONG:
								case Parameter.PARAM_SHORT:
								case Parameter.PARAM_STRING:
								case Parameter.PARAM_USHORT:
									for (Parameter cparam : (Vector<Parameter>) list.getList()) {
										cparam.setValue(algoLine.getFirstChildByName(
												param.getLabel()+"_"+cparam.getLabel()).toString());
									}
								break;
							}
							break;
						case Parameter.PARAM_BOOLEAN:
						case Parameter.PARAM_DOUBLE:
						case Parameter.PARAM_FLOAT:
						case Parameter.PARAM_INT:
						case Parameter.PARAM_LONG:
						case Parameter.PARAM_SHORT:
						case Parameter.PARAM_STRING:
						case Parameter.PARAM_USHORT:
							ParamModel mod = algoLine.getFirstChildByName(param
									.getLabel());
							if (mod == null) {
								System.err.println(getClass().getCanonicalName()+"COULD NOT FIND "
										+ param.getLabel() + " "
										+ param.getTypeString() + " "
										+ param.getValueString());
							}
							param.setValue(mod.toString());
							break;
						}

					}
				}
				newScript.append(parsedLine.convertToString() + "\n");
			}

			newScript.append("edu.jhu.ece.iacl.jist.dialogs.JDialogSaveAll(\"output_dir string "
					+ outputDir.getAbsolutePath() + "\")");

			File scriptFile = new File(outputDir, "script.sct");
			String str = newScript.toString();
			System.out.println(getClass().getCanonicalName()+"\t"+"------ MIPAV Script ------\n" + str+"\n------ End MIPAV Script ------");
			StringReaderWriter.getInstance().write(str, scriptFile);
			ScriptRunner.getReference().runScript(
						scriptFile.getAbsolutePath(), imgList, new Vector());
			ImageVariableTable imgTable = MipavController
					.getImageVariableTable();
			if (imgTable != null) {
				Enumeration keys = imgTable.keys();
				while (keys.hasMoreElements()) {
					String key = (String) keys.nextElement();
					ModelImage mimg = imgTable.getImage(key);
//					volumes.add(mimg);
					volumes.add(new ImageDataMipavWrapper(mimg));
				}
			}
//			for(ModelImage mi : miHandles) {
//				mi.disposeLocal();
//			}
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
