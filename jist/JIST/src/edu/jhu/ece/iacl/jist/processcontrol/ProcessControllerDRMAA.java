
package edu.jhu.ece.iacl.jist.processcontrol;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.ggf.drmaa.DrmaaException;
import org.ggf.drmaa.JobInfo;
import org.ggf.drmaa.JobTemplate;
import org.ggf.drmaa.Session;
 

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessControllerDRMAA.
 */
public class ProcessControllerDRMAA implements ProcessController {

	
	/** The Job id. */
	private String JobID; 
	
	/** The Job nam. */
	private String JobNam;
	
	/** The jt. */
	private JobTemplate jt; 
	
	/** The Job submit time. */
	private long JobSubmitTime; 
	
	/** The Job running time. */
	private long JobRunningTime;
	
	/** The exit code. */
	private Integer exitCode;
	
	/** The rand file name. */
	private UUID randFileName = UUID.randomUUID();
	
	/** The temp file. */
	private File tempFile = null;
	
	/** The Constant DATE_FORMAT. */
	private static final String DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";
	
	/** The session. */
	private Session session;
	
	/** The map. */
	private HashMap<String, String> map;
	
	//public ProcessControllerDRMAA(String Command)
	/**
	 * Instantiates a new process controller drmaa.
	 */
	public ProcessControllerDRMAA()
	{	
		session = DispatchController.getDRMAASession();
		map = new HashMap<String, String>(16, 1);  //create a hashmap to map env variable to Remote Env
		map.put("DISPLAY", ":99");
		//JOptionPane.showMessageDialog(null, "DISPLAY SET");
	}
	
	// New Code
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#Setup(java.util.List)
	 */
	public boolean Setup(List<String> CommandList) {
		//JOptionPane.showMessageDialog(null, "SETUP Called");
		System.out.println(getClass().getCanonicalName()+"\t"+"Setup PC-DRMAA");
		System.out.println(getClass().getCanonicalName()+"\t"+CommandList);
		StringWriter sw = new StringWriter();
		for(String  s : CommandList)  {
			if(s.contains(" ")) {
				sw.append("\""+s+"\"");
			} else {
				sw.append(s);
			}
			sw.append(" ");
		}
		String Command = sw.toString();
//		JOptionPane.showMessageDialog(null, "before Temp Set");
		System.out.println(getClass().getCanonicalName()+"\t"+Command);	
		//String tempFolder = System.getenv("SGE_JOBSCRIPT_TEMP"); 
		String tempFolder = System.getenv("SGE_JOBSCRIPT_TEMP");
//		JOptionPane.showMessageDialog(null, "Temp Dir Set");
		System.out.println(getClass().getCanonicalName()+"\t"+tempFolder);
		if(tempFolder == null) {
			System.out.println(getClass().getCanonicalName()+"\t"+"SGE_JOBSCRIPT_TEMP not set");
			return false;
		}
		/*if(tempFolder.isEmpty())
			return false;*/
		tempFile = new File(tempFolder+"/Run_"+nowTime()+"_"+randFileName.toString()+Math.random()+".sh"); // random script Execution File
		try{
			Writer fwriter = new BufferedWriter(new FileWriter(tempFile));
			fwriter.write("#!/bin/bash");
			fwriter.write("\n");
			fwriter.write("#$ -V");
			fwriter.write("\n");
//			fwriter.write("setenv SGE_JOBSCRIPT_TEMP "+tempFolder);
			fwriter.write("\n");
			fwriter.write("setenv DISPLAY $SGE_DISPLAY");			
			fwriter.write("\n");
			System.out.println(getClass().getCanonicalName()+"\t"+Command);
			fwriter.write(Command);
			fwriter.write("\n");
			fwriter.write("echo \"Done.\"\n");
			fwriter.close();
			}
		catch(IOException e) {
			//JOptionPane.showMessageDialog(null, "IO Error : " + e.getMessage());
			return false;
		}
		try{
			jt = session.createJobTemplate();
			jt.setNativeSpecification("-shell yes -b no ");
			jt.setRemoteCommand(tempFile.toString());
			jt.setJobEnvironment(map);
		}
		catch(DrmaaException e) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Exception" +e);
			return false;
		}
		return true;
	}
	
	/**
	 * Now time.
	 * 
	 * @return the string
	 */
	private static String nowTime()
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(cal.getTime());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getProcess()
	 */
	@Override
	public Process getProcess()
	{
		return null;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getSubmissionTime()
	 */
	@Override
	public long getSubmissionTime()
	{
		return JobSubmitTime;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override 
	public String toString()
	{
		return JobID;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getStatus()
	 */
	@Override
	public ProcessStatus getStatus() {
		// TODO Auto-generated method stub
		ProcessStatus JobStatus = null;
		try{
			Session session = DispatchController.getDRMAASession();
			int status = session.getJobProgramStatus(JobID);
			switch (status) {
			case Session.UNDETERMINED:
				JobStatus = ProcessStatus.UNDETERMINE;
				break;
			case Session.QUEUED_ACTIVE:
				JobStatus = ProcessStatus.QUEUED_ACTIVE;			
				break;
			case Session.SYSTEM_ON_HOLD:
				JobStatus = ProcessStatus.SYSTEM_ON_HOLD;	
				break;
			case Session.USER_ON_HOLD:
				JobStatus = ProcessStatus.USER_ON_HOLD;		
				break;
			case Session.USER_SYSTEM_ON_HOLD:
				JobStatus = ProcessStatus.USER_SYSTEM_ON_HOLD;				
				break;
			case Session.RUNNING:
				JobStatus = ProcessStatus.RUNNING;
				break;
			case Session.SYSTEM_SUSPENDED:
				JobStatus = ProcessStatus.SYSTEM_SUSPENDED;
				break;
			case Session.USER_SUSPENDED:
				JobStatus = ProcessStatus.USER_SUSPENDED;	
				break;
			case Session.USER_SYSTEM_SUSPENDED:
				JobStatus = ProcessStatus.USER_SYSTEM_SUSPENDED;;
				break;
			case Session.DONE:
				JobStatus = ProcessStatus.DONE;
				break;
			case Session.FAILED:
				JobStatus = ProcessStatus.FAILED;;
				break;
			}
		}catch (DrmaaException e)
		{
			System.out.println(getClass().getCanonicalName()+"\t"+"Exception:" + e);
		}
		return JobStatus;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getStderrFile()
	 */
	@Override
	public InputStream getStderrFile() {
		// TODO Auto-generated method stub
		File ErrorFile = null;
		try{
			ErrorFile = new File(jt.getErrorPath());
			} catch(DrmaaException e)
			{
				System.out.println(getClass().getCanonicalName()+"\t"+"Exception:" + e);
			}
			try {
		InputStream io = new FileInputStream(ErrorFile);
		return io;
			} catch(IOException e) {
				return null;
			}
		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getStdoutFile()
	 */
	@Override
	public InputStream getStdoutFile() {
		// TODO Auto-generated method stub
		File OutputFile = null;
		try{
			OutputFile = new File(jt.getOutputPath());
			System.out.println(getClass().getCanonicalName()+"\t"+jt.getOutputPath());
			} catch(DrmaaException e)
			{
				System.out.println(getClass().getCanonicalName()+"\t"+"Exception:" + e);
			}
			try {
				InputStream io = new FileInputStream(OutputFile);
				return io;
					} catch(Exception e) {
						return null;
					}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#submit()
	 */
	@Override
	public boolean submit() {
		// TODO Auto-generated method stub
		exitCode=null;
		   try{
			   	Session session = DispatchController.getDRMAASession();
		    	JobID = session.runJob(jt);	
		    	System.out.println(getClass().getCanonicalName()+"\t"+"Job" + JobID + "is submitted");
		        JobSubmitTime = System.currentTimeMillis();
		        return true;
		    }
			 catch (DrmaaException e)
			{
				System.out.println(getClass().getCanonicalName()+"\t"+"Exception:" + e);
				return false;
			}
		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#destroy()
	 */
	@Override
	public boolean destroy() {
		// TODO Auto-generated method stub
		try{
		DispatchController.getDRMAASession().control(JobID, Session.TERMINATE);
		//tempFile.delete();
		}catch(DrmaaException e)
		{
			System.out.println(getClass().getCanonicalName()+"\t"+"Exception:" + e);
			return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getExitCode()
	 */
	@Override 
	public Integer getExitCode()
	{
		return exitCode; 
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#waitFor()
	 */
	@Override 
	public int waitFor()
	{
		try{
		JobInfo info = DispatchController.getDRMAASession().wait(JobID, Session.TIMEOUT_WAIT_FOREVER);
		exitCode = 0;
		}catch(DrmaaException e)
		{
		exitCode = -1;	
		}
		return exitCode;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#controlJob(edu.jhu.ece.iacl.jist.processcontrol.ProcessControlAction)
	 */
	@Override
	public void controlJob(ProcessControlAction Action)
	{
		String JobStatus = "";
		Session session = DispatchController.getDRMAASession();
		
		try{		
		if(Action == Action.SUSPEND ){
			session.control(JobID, Session.SUSPEND);
				JobStatus = "suspended";
		}
		else if(Action == Action.RELEASE ){
				session.control(JobID, Session.RELEASE);
				JobStatus = "Released";
		}
		else if(Action == Action.HOLD ){
				session.control(JobID, Session.HOLD);
				JobStatus = "Put on Hold";
		}
		else if (Action == Action.RESUME ){
				session.control(JobID, Session.RESUME);
				JobStatus = "Resumed";
		}
		else if (Action == Action.TERMINATE){
				session.control(JobID, Session.TERMINATE);
				JobStatus = "KILLED";
		System.out.println(getClass().getCanonicalName()+"\t"+JobStatus);
		}
		}catch(DrmaaException e)
		{
			System.out.println(getClass().getCanonicalName()+"\t"+"Exception:" + e);
		}			
	
	}

}
