package edu.jhu.ece.iacl.jist.structures.fiber;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.LEFileReader;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.structures.geom.BndBox;
import edu.jhu.ece.iacl.jist.structures.geom.CurvePath;
import edu.jhu.ece.iacl.jist.structures.geom.IntersectResult;
import edu.jhu.ece.iacl.jist.structures.geom.PT;
import edu.jhu.ece.iacl.jist.structures.geom.Polygon;
import edu.jhu.ece.iacl.jist.structures.image.RGB;
import edu.jhu.ece.iacl.jist.structures.image.USbyte;


// TODO: Auto-generated Javadoc
/**
 * The Class Fiber.
 */
public class Fiber{
    
    /** The markers. */
    Map markers;
    
    /** The bnd box. */
    BndBox bndBox;
    
    /** The length. */
    int length;
    
    /** The status. */
    byte status;
    
    /** The color. */
    RGB color;
    
    /** The sel len start. */
    int selLenStart;
    
    /** The sel len end. */
    int selLenEnd;
    
    /** The xyz pt roi. */
    XYZ xyzPtROI;
    
    /** The xyz chain. */
    XYZ xyzChain[];
    
    /** The verbose. */
    boolean verbose = false;
    
    /**
     * Instantiates a new fiber.
     */
    public Fiber(){
    }
    
    /**
     * Instantiates a new fiber.
     * 
     * @param chain the chain
     */
    public Fiber(XYZ[] chain){
    	this.xyzChain = chain;
    	this.length=chain.length;
    }

    /**
     * Read.
     * 
     * @param fp the fp
     * @param version the version
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void read(LEFileReader fp, int version) throws IOException {
    	USbyte usb = new USbyte();
        bndBox = new BndBox();
        length = fp.readInt();			
        status = (byte)fp.readByte();	
        color = RGB.read(fp);	
      
        selLenStart = fp.readInt();		
        selLenEnd = fp.readInt();	
        
        if(verbose){
        	System.out.println(getClass().getCanonicalName()+"\t"+"length is: " + length);
        	System.out.println(getClass().getCanonicalName()+"\t"+"status is: " + status);
        	System.out.println(getClass().getCanonicalName()+"\t"+"Red is: " + usb.signed2unsigned(color.r));
        	System.out.println(getClass().getCanonicalName()+"\t"+"Green is: " + usb.signed2unsigned(color.g));
        	System.out.println(getClass().getCanonicalName()+"\t"+"Blue is: " + usb.signed2unsigned(color.b));
            System.out.println(getClass().getCanonicalName()+"\t"+"selLenStart is: " + selLenStart);
            System.out.println(getClass().getCanonicalName()+"\t"+"selLenEnd is: " + selLenEnd);
        }
        
        if(version>=2005) {
            xyzPtROI = XYZ.read(fp);
            System.out.println(getClass().getCanonicalName()+"\t"+"Reading xyzPTROI");
        } else {
            xyzPtROI = null;
        }
        if(length>1000) {
            System.out.println(getClass().getCanonicalName()+"\t"+"length>1000 is " +length);
        }
        xyzChain = new XYZ[length];
        for(int i=0;i<length;i++) {
            xyzChain[i] = XYZ.read(fp);
            bndBox.union(xyzChain[i]);
            
            if(verbose){
              System.out.println(getClass().getCanonicalName()+"\t"+"Reading point " +i);
            	System.out.println(getClass().getCanonicalName()+"\t"+"x is " +xyzChain[i].x);
            	System.out.println(getClass().getCanonicalName()+"\t"+"y is " +xyzChain[i].y);
            	System.out.println(getClass().getCanonicalName()+"\t"+"z is " +xyzChain[i].z);
            }
//            
        }
        if (verbose){System.out.println(getClass().getCanonicalName()+"\t"+"*****************************");}
    }
    
    /**
     * Gets the length.
     * 
     * @return the length
     */
    public int getLength(){
    	return length;
    }
    
    /**
     * Gets the chain.
     * 
     * @return the chain
     */
    public XYZ[] getChain(){
    	return xyzChain;
    }
    
    /**
     * Gets the color.
     * 
     * @return the color
     */
    public RGB getColor(){
    	return color;
    }
    
    /**
     * Intersects.
     * 
     * @param p the p
     * 
     * @return true, if successful
     */
    public boolean intersects(Polygon p) {
        if(!bndBox.intersect(p.bndBox))
            return false;
        PT a = new PT(xyzChain[0].x,xyzChain[0].y,xyzChain[0].z);
        for(int i=1;i<xyzChain.length;i++) {
            PT b = new PT(xyzChain[i].x,xyzChain[i].y,xyzChain[i].z);
            if(p.intersect(a,b)) {
                return true;
            }
            a = b;
        }
        return false;
    }
    
    /**
     * Reporti intersect.
     * 
     * @param p the p
     * 
     * @return the intersect result
     */
    public IntersectResult reportiIntersect(Polygon p) {
        //if(xyzChain[0].y>160)
        //        System.out.println(getClass().getCanonicalName()+"\t"+"ouch");
        if(!bndBox.intersect(p.bndBox))
            return null;
        PT a = new PT(xyzChain[0].x,xyzChain[0].y,xyzChain[0].z);
        for(int i=1;i<xyzChain.length;i++) {
            PT b = new PT(xyzChain[i].x,xyzChain[i].y,xyzChain[i].z);
            IntersectResult r = p.reportIntersect(a,b);

            if(r!=null) {
                r.fractionalDistance = i;  //record which fiber segment hit the polygon
                return r;
            }
            a = b;
        }
        return null;
    }
    
    /**
     * Sets the marker point.
     * 
     * @param marker the marker
     * @param pt the pt
     */
    public void setMarkerPoint(double marker, int pt) {
        if(markers==null) {
            markers = new HashMap();
        }
        markers.put(new Integer(pt),new Double(marker));
    }

    /**
     * Gets the marker.
     * 
     * @param pt the pt
     * 
     * @return the marker
     */
    public double getMarker(int pt) {
        if(markers==null)
            return 0;
        Double m = (Double)markers.get(new Integer(pt));
        if(m==null)
            return 0;
        return m.intValue();
    }
    
    /**
     * Gets the xYZ chain.
     * 
     * @return the xYZ chain
     */
    public XYZ[] getXYZChain(){ return xyzChain; }
    
    /**
     * Equals.
     * 
     * @param a the a
     * 
     * @return true, if successful
     */
    public boolean equals(Fiber a){
    	for(int i=0; i<xyzChain.length; i++){
    		if (!xyzChain[i].equals(a.xyzChain[i])){
    			return false;
    		}
    	}
    	return true;
    }
    
    /**
     * Gets the seed point index.
     * 
     * @return the seed point index
     */
    public int getseedPointIndex(){
    	for(int i=0; i<length; i++){
    		if(xyzChain[i].x % 1 == .5){
    			if(xyzChain[i].y % 1 == .5){
    				if(xyzChain[i].z % 1 == .5){
    	    			return i;
    	    		}
        		}
    		}
    	}
    	return -1;
    }
    
    /**
     * Gets the seed point index2.
     * 
     * @return the seed point index2
     */
    public int getseedPointIndex2(){
    	for(int i=0; i<length; i++){
    		if(xyzChain[i].x % 1 == 0){
    			if(xyzChain[i].y % 1 == 0){
    				if(xyzChain[i].z % 1 == 0){
    	    			return i;
    	    		}
        		}
    		}
    	}
    	return -1;
    }
    
    /**
     * To volume.
     * 
     * @param dims the dims
     * 
     * @return the boolean[][][]
     */
    public boolean[][][] toVolume(int[] dims){
    	boolean[][][] vol = new boolean[dims[0]][dims[1]][dims[2]];	
    	for(int i=0; i<length; i++){
    		vol[Math.round(xyzChain[i].x)][Math.round(xyzChain[i].y)][Math.round(xyzChain[i].z)] = true;
    	}
    	return vol;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString(){
    	String s = "";
    	for(int i=0; i<xyzChain.length; i++){
    		s = s+ xyzChain[i]+ ", ";
    	}
    	return s;
    }
    
    /**
     * To curve path.
     * 
     * @return the curve path
     */
    public CurvePath toCurvePath(){
    	CurvePath cp=new CurvePath();
    	for(XYZ pt:xyzChain){
    		cp.add(pt);
    	}
    	return cp;
    }
    
    /**
     * Length.
     * 
     * @param resX the res x
     * @param resY the res y
     * @param resZ the res z
     * 
     * @return the double
     */
    public double length(double resX, double resY, double resZ){
    	double length=0;
    	for(int i=1; i<xyzChain.length; i++){
    		XYZ pt1=xyzChain[i].deepClone();
    		XYZ pt2=xyzChain[i-1].deepClone();
//    		System.out.println(getClass().getCanonicalName()+"\t"+"Before scale Pt1: "+pt1);
//    		System.out.println(getClass().getCanonicalName()+"\t"+"Before scale Pt2: "+pt2);
    		
    		pt1.scale(resX, resY, resZ);
    		pt2.scale(resX, resY, resZ);
//    		System.out.println(getClass().getCanonicalName()+"\t"+"Pt1: "+pt1);
//    		System.out.println(getClass().getCanonicalName()+"\t"+"Pt2: "+pt2);
    		
    		length=length+pt1.distance(pt2);
    	}
    	return length;
    }
    
    /**
     * Scale for visualization.
     * 
     * @param xs the xs
     * @param ys the ys
     * @param zs the zs
     * 
     * @return the fiber
     */
    public Fiber scaleForVisualization(float xs, float ys, float zs){
    	for(int i=0; i<xyzChain.length; i++){
    		xyzChain[i].scale(xs,ys,zs);
    	}
    	return this;
    }
    
    /**
     * Scale for visualization.
     * 
     * @param in the in
     * @param xs the xs
     * @param ys the ys
     * @param zs the zs
     * 
     * @return the fiber[]
     */
    public static Fiber[] scaleForVisualization(Fiber[] in, float xs, float ys, float zs){
    	Fiber[] out = new Fiber[in.length];
    	for(int i=0; i<in.length; i++){
    		out[i]=in[i].scaleForVisualization(xs, ys, zs);
    	}
    	return out;
    }
    
    /**
     * Make in bounds.
     * 
     * @param maxx the maxx
     * @param maxy the maxy
     * @param maxz the maxz
     */
    public void makeInBounds(int maxx, int maxy, int maxz){
    	boolean hitpos = false;
		int endind = xyzChain.length;
		int startind = 0;
    	for(int i=0; i<xyzChain.length; i++){
    		if(xyzChain[i].x<0 || xyzChain[i].x>=maxx ||
    		   xyzChain[i].y<0 || xyzChain[i].y>=maxy ||
    		   xyzChain[i].z<0 || xyzChain[i].z>=maxy){
    			if(hitpos){
    				break;
    			}
    		}else if(!hitpos){
    			startind=i;
				hitpos=true;
    		}
    		endind=i;
    	}
//    	System.out.println(getClass().getCanonicalName()+"\t"+"Startind: " +startind);
//    	System.out.println(getClass().getCanonicalName()+"\t"+"Endind: " +endind);
    	XYZ[] chain = new XYZ[endind-startind];
    	int k=0;
    	for(int j=startind; j<endind; j++){
    		chain[k]=xyzChain[j];
    		k++;
    	}
    	xyzChain=chain;
    	length=chain.length;
    }
    
    /**
     * Make fibers in bounds.
     * 
     * @param in the in
     * @param maxx the maxx
     * @param maxy the maxy
     * @param maxz the maxz
     * 
     * @return the fiber[]
     */
    public static Fiber[] makeFibersInBounds(Fiber[] in,int maxx, int maxy, int maxz){
    	System.out.println("Fiber"+"\t"+"Working! makeFibersInBounds" );
    	Fiber[] out = in;
    	System.out.println("Fiber"+"\t"+out.length );
    	for(int i=0; i<out.length; i++){
    		out[i].makeInBounds(maxx, maxy, maxz);
    	}
    	return out;
    }
    
    /**
     * From vector.
     * 
     * @param vect the vect
     * 
     * @return the fiber[]
     */
    public static Fiber[] fromVector(Vector<Fiber> vect){
    	vect.trimToSize();
    	Fiber[] out = new Fiber[vect.size()];
    	vect.toArray(out);
    	return out;
    }
    
    /**
     * Write vtk.
     * 
     * @param writeme the writeme
     * @param filename the filename
     */
    public static void writeVTK(Fiber[] writeme, String filename){
    	MipavController.setQuiet(true);
    	FiberCollection fc = new FiberCollection();
    	fc.getFromFiberArray(writeme);
//    	System.out.println(getClass().getCanonicalName()+"\t"+"Really writing.");
    	CurveVtkReaderWriter.getInstance().write(fc.toCurveCollection(), new File(filename));
    }
    
}