package edu.jhu.ece.iacl.jist.structures.image;

public class ImageDataIntent {
	public static final int NIFTI_INTENT_NONE=0;
	public static final int NIFTI_INTENT_CORREL=2;
	public static final int NIFTI_INTENT_TTEST=3;
	public static final int NIFTI_INTENT_FTEST= 4;
	public static final int NIFTI_INTENT_ZSCORE= 5;
	public static final int NIFTI_INTENT_CHISQ= 6;
	public static final int NIFTI_INTENT_BETA= 7;
	public static final int NIFTI_INTENT_BINOM= 8;
	public static final int NIFTI_INTENT_GAMMA= 9;
	public static final int NIFTI_INTENT_POISSON= 10;
	public static final int NIFTI_INTENT_NORMAL= 11;
	public static final int NIFTI_INTENT_FTEST_NONC= 12;
	public static final int NIFTI_INTENT_CHISQ_NONC= 13;
	public static final int NIFTI_INTENT_LOGISTIC= 14;
	public static final int NIFTI_INTENT_LAPLACE= 15;
	public static final int NIFTI_INTENT_UNIFORM= 16;
	public static final int NIFTI_INTENT_TTEST_NONC= 17;
	public static final int NIFTI_INTENT_WEIBULL= 18;
	public static final int NIFTI_INTENT_CHI= 19;
	public static final int NIFTI_INTENT_INVGAUSS= 20;
	public static final int NIFTI_INTENT_EXTVAL= 21;
	public static final int NIFTI_INTENT_PVAL= 22;
	public static final int NIFTI_INTENT_LOGPVAL= 23;
	public static final int NIFTI_INTENT_LOG10PVAL= 24;
	public static final int NIFTI_INTENT_ESTIMATE= 1001;
	public static final int NIFTI_INTENT_LABEL= 1002;
	public static final int NIFTI_INTENT_NEURONAME= 1003;
	public static final int NIFTI_INTENT_GENMATRIX= 1004;
	public static final int NIFTI_INTENT_SYMMATRIX= 1005;
	public static final int NIFTI_INTENT_DISPVECT= 1006;
	public static final int NIFTI_INTENT_VECTOR= 1007;
	public static final int NIFTI_INTENT_POINTSET= 1008;
	public static final int NIFTI_INTENT_TRIANGLE= 1009;
	public static final int NIFTI_INTENT_QUATERNION= 1010;
	public static final int NIFTI_INTENT_DIMLESS= 1011;
}
