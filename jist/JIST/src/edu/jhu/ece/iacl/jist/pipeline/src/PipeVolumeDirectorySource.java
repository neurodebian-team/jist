/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;

/**
 * Iterate through all volumes in directory.
 * 
 * @author Blake Lucas
 */
public class PipeVolumeDirectorySource extends PipeFileDirectorySource {
	/**
	 * Instantiates a new pipe volume directory surface.
	 */
	public PipeVolumeDirectorySource() {
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group=super.createInputParams();
		group.setLabel("Volume Directory");
		group.setName("volumedirectory");
		group.setCategory("Volume");
		fileExtension.setValue("xml");
		return group;
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume");
		group.add(fileParam = new ParamVolume("Volume"));
		return group;
	}

	public ParamVolume getOutputParam() {
		return (ParamVolume)fileParam;
	}
}
