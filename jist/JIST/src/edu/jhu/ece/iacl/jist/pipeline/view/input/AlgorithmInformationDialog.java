/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import gov.nih.mipav.view.MipavUtil;

/**
 * Dialog to view/edit information about the algorithm.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class AlgorithmInformationDialog extends javax.swing.JDialog implements ActionListener {

	/**
	 * Author dialog allows user to edit author information.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected static class AuthorDialog extends JDialog implements ActionListener {
		
		/** The author. */
		protected AlgorithmAuthor eauth = null;
		
		/** The cancel button. */
		protected JButton okButton, cancelButton;
		
		/** The website. */
		protected JTextField author, email, website;

		/**
		 * Default constructor.
		 */
		public AuthorDialog(boolean editable) {
			this(null,editable);
		}

		/**
		 * Default constructor.
		 * 
		 * @param eauth
		 *            existing author
		 */
		public AuthorDialog(AlgorithmAuthor eauth,boolean editable) {
			super((Frame) null, "Add Algorithm Author", true);
			JPanel myPanel = new JPanel(new GridBagLayout());
			myPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(), BorderFactory
					.createEmptyBorder(5, 5, 5, 5)));
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 0.5;
			c.weighty = 0.5;
			c.ipadx = 5;
			c.gridwidth = 1;
			c.gridheight = 1;
			c.anchor = GridBagConstraints.LINE_START;
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 0.1;
			myPanel.add(new JLabel("Author"), c);
			c.gridx = 1;
			c.gridy = 0;
			c.weightx = 0.9;
			myPanel.add(author = new JTextField(), c);
			c.gridx = 0;
			c.gridy = 1;
			c.weightx = 0.1;
			myPanel.add(new JLabel("E-mail"), c);
			c.gridx = 1;
			c.gridy = 1;
			c.weightx = 0.9;
			myPanel.add(email = new JTextField(), c);
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 0.1;
			myPanel.add(new JLabel("Website"), c);
			c.gridx = 1;
			c.gridy = 2;
			c.weightx = 0.9;
			myPanel.add(website = new JTextField(""), c);
			JPanel buttonPanel = new JPanel();
			okButton = new JButton("Ok");
			okButton.addActionListener(this);
			okButton.setMinimumSize(MipavUtil.defaultButtonSize);
			okButton.setPreferredSize(MipavUtil.defaultButtonSize);
			buttonPanel.add(okButton);
			cancelButton = new JButton("Cancel");
			cancelButton.setMinimumSize(MipavUtil.defaultButtonSize);
			cancelButton.setPreferredSize(MipavUtil.defaultButtonSize);
			cancelButton.addActionListener(this);
			buttonPanel.add(cancelButton);
			Container cont = getContentPane();
			LayoutManager layout = new BoxLayout(cont, BoxLayout.Y_AXIS);
			cont.setPreferredSize(new Dimension(300, 120));
			cont.setLayout(layout);
			cont.add(myPanel);
			cont.add(buttonPanel);
			pack();
			setLocationRelativeTo(this);
			if (eauth != null) {
				author.setText(eauth.getAuthor());
				email.setText(eauth.getEmail());
				website.setText(eauth.getWebsite());
			}
			if(!editable){
				author.setEditable(false);
				email.setEditable(false);
				website.setEditable(false);
			}
			setVisible(true);
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == okButton) {
				eauth = new AlgorithmAuthor(author.getText(), email.getText(), website.getText());
				setVisible(false);
			} else if (e.getSource() == cancelButton) {
				eauth = null;
				setVisible(false);
			}
		}

		/**
		 * Gets the author.
		 * 
		 * @return the author
		 */
		public AlgorithmAuthor getAuthor() {
			return eauth;
		}
	}

	/**
	 * Author list box displays author information.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected static class AuthorListBox extends InformationListBox<AlgorithmAuthor> {
		
		/**
		 * Instantiates a new author list box.
		 */
		public AuthorListBox(boolean editable) {
			super("Algorithm Authors",editable);
		}

		/**
		 * Add new author.
		 * 
		 * @return the algorithm author
		 */
		protected AlgorithmAuthor addItem() {
			AuthorDialog dialog = new AuthorDialog(editable);
			return dialog.getAuthor();
		}

		/**
		 * Edge author.
		 * 
		 * @param obj
		 *            the obj
		 * @return the algorithm author
		 */
		protected AlgorithmAuthor editItem(AlgorithmAuthor obj) {
			AuthorDialog dialog = new AuthorDialog(obj,editable);
			return dialog.getAuthor();
		}
	}

	/**
	 * Citation dialog allows user to edge citation information.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected static class CitationDialog extends JDialog implements ActionListener {
		
		/** The cite. */
		Citation cite = null;
		
		/** The cancel button. */
		JButton okButton, cancelButton;
		
		/** The citation. */
		JTextArea citation;

		/**
		 * Instantiates a new citation dialog.
		 */
		public CitationDialog(boolean editable) {
			this(null,editable);
		}

		/**
		 * Default constructor.
		 * 
		 * @param cite
		 *            citation
		 */
		public CitationDialog(Citation cite,boolean editable) {
			super((Frame) null, "Add Citation", true);
			JScrollPane pane = new JScrollPane();
			pane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Citation"),
					BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			citation = new JTextArea(4, 4);
			citation.setLineWrap(true);
			pane.setViewportView(citation);
			JPanel buttonPanel = new JPanel();
			okButton = new JButton("Ok");
			okButton.addActionListener(this);
			okButton.setMinimumSize(MipavUtil.defaultButtonSize);
			okButton.setPreferredSize(MipavUtil.defaultButtonSize);
			buttonPanel.add(okButton);
			cancelButton = new JButton("Cancel");
			cancelButton.setMinimumSize(MipavUtil.defaultButtonSize);
			cancelButton.setPreferredSize(MipavUtil.defaultButtonSize);
			cancelButton.addActionListener(this);
			buttonPanel.add(cancelButton);
			Container cont = getContentPane();
			LayoutManager layout = new BoxLayout(cont, BoxLayout.Y_AXIS);
			cont.setPreferredSize(new Dimension(400, 200));
			cont.setLayout(layout);
			cont.add(pane);
			cont.add(buttonPanel);
			pack();
			setLocationRelativeTo(this);
			if (cite != null) {
				citation.setText(cite.getText());
			}
			if(!editable){
				citation.setEditable(false);
			}
			setVisible(true);
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == okButton) {
				cite = new Citation(citation.getText());
				setVisible(false);
			} else if (e.getSource() == cancelButton) {
				cite = null;
				setVisible(false);
			}
		}

		/**
		 * Gets the citation.
		 * 
		 * @return the citation
		 */
		public Citation getCitation() {
			return cite;
		}
	}

	/**
	 * Citation list box displays citations formated as HTML.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected static class CitationListBox extends InformationListBox<Citation> {
		
		/**
		 * Default constructor.
		 */
		public CitationListBox(boolean editable) {
			super("Citations",editable);
		}

		/**
		 * Add new citation.
		 * 
		 * @return the citation
		 */
		protected Citation addItem() {
			CitationDialog dialog = new CitationDialog(editable);
			return dialog.getCitation();
		}

		/**
		 * Edit citation.
		 * 
		 * @param obj
		 *            the obj
		 * @return the citation
		 */
		protected Citation editItem(Citation obj) {
			CitationDialog dialog = new CitationDialog(obj,editable);
			return dialog.getCitation();
		}
	}

	/** Auto-generated main method to display this JDialog. */
	/*
	public static void main(String[] args) {
		AlgorithmInformation info = new AlgorithmInformation(new MedicAlgorithmBrainCutSurface());
		info.setAffiliation("IACL");
		info.setVersion("1.0");
		info.setCreationDate((new Date()));
		info.setWebsite("http://www.google.com");
		info.getAuthors().add(new AlgorithmAuthor("Blake Lucas", "bclucas@jhu.edu", ""));
		info.getCitations().add(new Citation("CITATION"));
		AlgorithmInformationDialog inst = new AlgorithmInformationDialog(null, info);
		System.out.println(getClass().getCanonicalName()+"\t"+info.toString());
		System.exit(0);
	}
	 */
	/**
	 * The buttons.
	 */ 
	private JButton okButton, cancelButton;
	/** The author list. */
	protected AuthorListBox authorList;
	/** The citation list. */
	protected CitationListBox citeList;
	/** The description box. */
	protected JTextArea descriptionBox;
	/** The text fields. */
	protected JTextField version, creationDate, website, affiliation;
	/** The algorithm information. */
	protected AlgorithmInformation info;

	/**
	 * Default constructor.
	 * 
	 * @param parent
	 *            parent frame
	 * @param info
	 *            algorithm information
	 */
	public AlgorithmInformationDialog(Frame parent, AlgorithmInformation info) {
		super(parent, "Algorithm Information", true);
		Container cont = getContentPane();
		this.info = info;
		LayoutManager layout = new BoxLayout(cont, BoxLayout.Y_AXIS);
		cont.setLayout(layout);
		cont.add(buildTitlePanel());
		cont.add(buildTextEntryPanel());
		cont.add(buildDescriptionBox());
		cont.add(buildListBoxPanel());
		cont.add(buildButtonPanel());
		populateGUI();
		pack();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			if(info.isEditable()){
				populateInfo();
			}
			setVisible(false);
		} else if (e.getSource() == cancelButton) {
			setVisible(false);
		}
	}

	/**
	 * Build button panel to select action.
	 * 
	 * @return panel
	 */
	protected JPanel buildButtonPanel() {
		JPanel myPanel = new JPanel();
		okButton = new JButton("Ok");
		okButton.addActionListener(this);
		okButton.setMinimumSize(MipavUtil.defaultButtonSize);
		okButton.setPreferredSize(MipavUtil.defaultButtonSize);
		myPanel.add(okButton);
		cancelButton = new JButton("Cancel");
		cancelButton.setMinimumSize(MipavUtil.defaultButtonSize);
		cancelButton.setPreferredSize(MipavUtil.defaultButtonSize);
		cancelButton.addActionListener(this);
		myPanel.add(cancelButton);
		return myPanel;
	}

	/**
	 * Build panel for description box.
	 * 
	 * @return pane
	 */
	protected JComponent buildDescriptionBox() {
		JScrollPane pane = new JScrollPane();
		pane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Description"),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		descriptionBox = new JTextArea(10, 5);
		descriptionBox.setLineWrap(true);
		pane.setViewportView(descriptionBox);
		return pane;
	}

	/**
	 * Build panel for list boxes.
	 * 
	 * @return panel
	 */
	protected JPanel buildListBoxPanel() {
		JPanel myPanel = new JPanel(new BorderLayout());
		myPanel.add(authorList = new AuthorListBox(info.isEditable()), BorderLayout.WEST);
		myPanel.add(citeList = new CitationListBox(info.isEditable()), BorderLayout.EAST);
		return myPanel;
	}

	/**
	 * Build panel to enter text fields.
	 * 
	 * @return text fields
	 */
	protected JPanel buildTextEntryPanel() {
		GridBagLayout layout;
		JPanel myPanel = new JPanel(layout = new GridBagLayout());
		myPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("General"), BorderFactory
				.createEmptyBorder(5, 5, 5, 5)));
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.weighty = 0.5;
		c.ipadx = 5;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.anchor = GridBagConstraints.LINE_START;
		JLabel label;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.1;
		myPanel.add(new JLabel("Creation Date"), c);
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 0.9;
		myPanel.add(label = new JLabel(), c);
		label.setFont(new Font("Arial", Font.PLAIN, JistPreferences.getPreferences().getMdFontSize()));
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		if (info.getCreationDate() != null) {
			label.setText(sdf.format(info.getCreationDate()));
		}
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.1;
		myPanel.add(new JLabel("Class"), c);
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.9;
		myPanel.add(label = new JLabel(info.getAlgorithmClassName()), c);
		label.setFont(new Font("Courier", Font.PLAIN, JistPreferences.getPreferences().getMdFontSize()));
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 0.1;
		myPanel.add(new JLabel("Version"), c);
		c.gridx = 1;
		c.gridy = 2;
		c.weightx = 0.9;
		myPanel.add(version = new JTextField(), c);
		c.gridx = 0;
		c.gridy = 3;
		c.weightx = 0.1;
		myPanel.add(new JLabel("Affiliation"), c);
		c.gridx = 1;
		c.gridy = 3;
		c.weightx = 0.9;
		myPanel.add(affiliation = new JTextField(), c);
		c.gridx = 0;
		c.gridy = 4;
		c.weightx = 0.1;
		myPanel.add(new JLabel("Website"), c);
		c.gridx = 1;
		c.gridy = 4;
		c.weightx = 0.9;
		myPanel.add(website = new JTextField(""), c);
		return myPanel;
	}

	/**
	 * Build panel to display algorithm name.
	 * 
	 * @return panel
	 */
	protected JPanel buildTitlePanel() {
		JPanel myPanel = new JPanel(new BorderLayout());
		myPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(), BorderFactory
				.createEmptyBorder(5, 5, 5, 5)));
		JLabel label;
		myPanel.add(label = new JLabel(info.getLabel() + " (" + info.getName() + ")"), BorderLayout.WEST);
		label.setFont(new Font("Times", Font.BOLD, JistPreferences.getPreferences().getLgFontSize()));
		return myPanel;
	}

	/**
	 * Populate GUI with values from algorithm information.
	 */
	protected void populateGUI() {
		if (info.getVersion() != null) {
			version.setText(info.getVersion());
		}
		if (info.getWebsite() != null) {
			website.setText(info.getWebsite());
		}
		if (info.getAffiliation() != null) {
			affiliation.setText(info.getAffiliation());
		}
		if (info.getDescription() != null) {
			descriptionBox.setText(info.getDescription());
		}
		if(!info.isEditable()){
			version.setEditable(false);
			website.setEditable(false);
			affiliation.setEditable(false);
			descriptionBox.setEditable(false);
		}
		authorList.setList((LinkedList<AlgorithmAuthor>) info.getAuthors().clone());
		citeList.setList((LinkedList<Citation>) info.getCitations().clone());
	}

	/**
	 * Populate algorithm information with values from GUI.
	 */
	protected void populateInfo() {
		info.setVersion(version.getText());
		info.setWebsite(website.getText());
		info.setAffiliation(affiliation.getText());
		info.setDescription(descriptionBox.getText());
		authorList.getList(info.getAuthors());
		citeList.getList(info.getCitations());
	}
}