/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.text.ParseException;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

import org.jgraph.JGraph;
import org.jgraph.graph.GraphCellEditor;

/**
 * The Class PipeEdgeRealEditor.
 */
public class PipeEdgeRealEditor extends AbstractCellEditor implements TableCellEditor, TreeCellEditor, GraphCellEditor {
	//
	// Instance Variables
	//
	/**
	 * The protected <code>EditorDelegate</code> class.
	 */
	protected class EditorDelegate implements ChangeListener, Serializable {
		
		/** The value of this cell. */
		protected Object value;

		/**
		 * Cancels editing. This method calls <code>fireEditingCanceled</code>.
		 */
		public void cancelCellEditing() {
			fireEditingCanceled();
		}

		/**
		 * Returns the value of this cell.
		 * 
		 * @return the value of this cell
		 */
		public Object getCellEditorValue() {
			return value;
		}

		/**
		 * Returns true if <code>anEvent</code> is <b>not</b> a
		 * <code>MouseEvent</code>. Otherwise, it returns true if the
		 * necessary number of clicks have occurred, and returns false
		 * otherwise.
		 * 
		 * @param anEvent
		 *            the event
		 * @return true if cell is ready for editing, false otherwise
		 * @see #setClickCountToStart
		 * @see #shouldSelectCell
		 */
		public boolean isCellEditable(EventObject anEvent) {
			if (anEvent instanceof MouseEvent) {
				return ((MouseEvent) anEvent).getClickCount() >= clickCountToStart;
			}
			return true;
		}

		/**
		 * Sets the value of this cell.
		 * 
		 * @param value
		 *            the new value of this cell
		 */
		public void setValue(Object value) {
			this.value = value;
		}

		/**
		 * Returns true to indicate that the editing cell may be selected.
		 * 
		 * @param anEvent
		 *            the event
		 * @return true
		 * @see #isCellEditable
		 */
		public boolean shouldSelectCell(EventObject anEvent) {
			return true;
		}

		/**
		 * Returns true to indicate that editing has begun.
		 * 
		 * @param anEvent
		 *            the event
		 * @return true, if start cell editing
		 */
		public boolean startCellEditing(EventObject anEvent) {
			return true;
		}

		/* (non-Javadoc)
		 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
		 */
		public void stateChanged(ChangeEvent evt) {
			// this.stopCellEditing();
		}

		/**
		 * Stops editing and returns true to indicate that editing has stopped.
		 * This method calls <code>fireEditingStopped</code>.
		 * 
		 * @return true
		 */
		public boolean stopCellEditing() {
			fireEditingStopped();
			return true;
		}
	}

	/** The Swing component being edited. */
	protected JComponent editorComponent;
	
	/**
	 * The delegate class which handles all methods sent from the
	 * <code>CellEditor</code>.
	 */
	protected EditorDelegate delegate;
	//
	// Constructors
	//
	/**
	 * An integer specifying the number of clicks needed to start editing. Even
	 * if <code>clickCountToStart</code> is defined as zero, it will not
	 * initiate until a click occurs.
	 */
	protected int clickCountToStart = 1;

	/**
	 * Constructs a <code>DefaultCellEditor</code> that uses a text field.
	 * 
	 * @param spinnerField
	 *            the spinner field
	 */
	public PipeEdgeRealEditor(final JSpinner spinnerField) {
		editorComponent = spinnerField;
		this.clickCountToStart = 1;
		delegate = new EditorDelegate() {
			public Object getCellEditorValue() {
				try {
					spinnerField.commitEdit();
				} catch (ParseException e) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
				}
				return spinnerField.getValue();
			}

			public void setValue(Object value) {
				if (value != null) {
					try {
						spinnerField.setValue(Integer.parseInt(value.toString()));
					} catch (IllegalArgumentException e) {
						System.err.println(getClass().getCanonicalName()+e.getMessage());
					}
				}
			}
		};
		spinnerField.addChangeListener(delegate);
	}

	//
	// Modifying
	//
	/**
	 * Forwards the message from the <code>CellEditor</code> to the
	 * <code>delegate</code>.
	 * 
	 * @see EditorDelegate#cancelCellEditing
	 */
	public void cancelCellEditing() {
		delegate.cancelCellEditing();
	}

	/**
	 * Forwards the message from the <code>CellEditor</code> to the
	 * <code>delegate</code>.
	 * 
	 * @return the cell editor value
	 * @see EditorDelegate#getCellEditorValue
	 */
	public Object getCellEditorValue() {
		return delegate.getCellEditorValue();
	}

	//
	// Override the implementations of the superclass, forwarding all methods
	// from the CellEditor interface to our delegate.
	//
	/**
	 * Returns the number of clicks needed to start editing.
	 * 
	 * @return the number of clicks needed to start editing
	 */
	public int getClickCountToStart() {
		return clickCountToStart;
	}

	/**
	 * Returns a reference to the editor component.
	 * 
	 * @return the editor <code>Component</code>
	 */
	public Component getComponent() {
		return editorComponent;
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.GraphCellEditor#getGraphCellEditorComponent(org.jgraph.JGraph, java.lang.Object, boolean)
	 */
	public Component getGraphCellEditorComponent(JGraph graph, Object value, boolean isSelected) {
		String stringValue = graph.convertValueToString(value);
		delegate.setValue(stringValue);
		return editorComponent;
	}

	//
	// Implementing the CellEditor Interface
	//
	/**
	 * Implements the <code>TableCellEditor</code> interface.
	 * 
	 * @param table
	 *            the table
	 * @param value
	 *            the value
	 * @param isSelected
	 *            the is selected
	 * @param row
	 *            the row
	 * @param column
	 *            the column
	 * @return the table cell editor component
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		delegate.setValue(value);
		return editorComponent;
	}

	/**
	 * Implements the <code>TreeCellEditor</code> interface.
	 * 
	 * @param tree
	 *            the tree
	 * @param value
	 *            the value
	 * @param isSelected
	 *            the is selected
	 * @param expanded
	 *            the expanded
	 * @param leaf
	 *            the leaf
	 * @param row
	 *            the row
	 * @return the tree cell editor component
	 */
	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded,
			boolean leaf, int row) {
		String stringValue = tree.convertValueToText(value, isSelected, expanded, leaf, row, false);
		delegate.setValue(stringValue);
		return editorComponent;
	}

	//
	// Implementing the TreeCellEditor Interface
	//
	/**
	 * Forwards the message from the <code>CellEditor</code> to the
	 * <code>delegate</code>.
	 * 
	 * @param anEvent
	 *            the an event
	 * @return true, if checks if is cell editable
	 * @see EditorDelegate#isCellEditable(EventObject)
	 */
	public boolean isCellEditable(EventObject anEvent) {
		return delegate.isCellEditable(anEvent);
	}

	/**
	 * Specifies the number of clicks needed to start editing.
	 * 
	 * @param count
	 *            an int specifying the number of clicks needed to start editing
	 * @see #getClickCountToStart
	 */
	public void setClickCountToStart(int count) {
		clickCountToStart = count;
	}

	//
	// Protected EditorDelegate class
	//
	/**
	 * Forwards the message from the <code>CellEditor</code> to the
	 * <code>delegate</code>.
	 * 
	 * @param anEvent
	 *            the an event
	 * @return true, if should select cell
	 * @see EditorDelegate#shouldSelectCell(EventObject)
	 */
	public boolean shouldSelectCell(EventObject anEvent) {
		return delegate.shouldSelectCell(anEvent);
	}

	// GraphCellEditor Interface
	/**
	 * Forwards the message from the <code>CellEditor</code> to the
	 * <code>delegate</code>.
	 * 
	 * @return true, if stop cell editing
	 * @see EditorDelegate#stopCellEditing
	 */
	public boolean stopCellEditing() {
		return delegate.stopCellEditing();
	}
} // End of class JCellEditor
