package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

// TODO: Auto-generated Javadoc
/**
 * The Class MipavSurfaceXMLReader.
 */
public class MipavSurfaceXMLReader {
	
	/** The vertices open tag. */
	static int verticesOpenTag;
	
	/** The vertices close tag. */
	static int verticesCloseTag;
	
	/** The faces open tag. */
	static int facesOpenTag;
	
	/** The faces close tag. */
	static int facesCloseTag;	
	//static ArrayList<String> str;
	/**
	 * Read surface file.
	 * 
	 * @param f the f
	 * 
	 * @return the array list
	 */
	public static ArrayList readSurfaceFile (File f){
		ArrayList<String> str = new ArrayList<String>();
		try {		
			// Create input stream from file
			FileReader reader = new FileReader(f);
			BufferedReader in = new BufferedReader(reader);
			String line;
			// Read file and store the string in a array 
			while ((line = in.readLine()) != null) {
				StringTokenizer tokenizer = new StringTokenizer(line, " ,\t,\n,\r,<,>");
				while(tokenizer.hasMoreTokens()){
					String token = tokenizer.nextToken();
					//System.out.println("jist.io"+"\t"+token);
					str.add(token);
				}
			}
			in.close();
			reader.close();
		}
		catch (IOException e) {
			System.out.println("jist.io"+"\t"+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
		    return null;
		}
        //determine the XML tags for vertices and faces
		for (int index = 0; index < str.size(); index++){		
			String arrayListData = str.get(index);
			//System.out.println("jist.io"+"\t"+arrayListData);
			if (arrayListData.equalsIgnoreCase("Vertices")){
				verticesOpenTag = index;
				System.out.println("jist.io"+"\t"+verticesOpenTag);
			}else if (arrayListData.equalsIgnoreCase("/Vertices")){
				verticesCloseTag = index;
				System.out.println("jist.io"+"\t"+verticesCloseTag);
			}else if (arrayListData.equalsIgnoreCase("Connectivity")){
				facesOpenTag = index;
				System.out.println("jist.io"+"\t"+facesOpenTag);
			}else if (arrayListData.equalsIgnoreCase("/Connectivity")){
				facesCloseTag = index;
				System.out.println("jist.io"+"\t"+facesCloseTag);
			}else{ 
				System.out.print("");
			}
		}
        return str;
	}
	
		
	/**
	 * Gets the vertices.
	 * 
	 * @param f the f
	 * 
	 * @return the vertices
	 */
	public static String[] getVertices(File f){ 
		ArrayList surfaceData = readSurfaceFile(f);
		int verticesStartIndex = verticesOpenTag +1;
		int verticesEndIndex = verticesCloseTag -1;
		int numOfVertices = (((verticesEndIndex - verticesStartIndex) + 1)/3);
        String[] vertices = new String[numOfVertices * 3];
        int vertexCounter = 0;
        for (int i = verticesStartIndex; i <= verticesEndIndex; i++){
        	vertices [vertexCounter] = (String)surfaceData.get(i);
        	vertexCounter++;
        }
		return vertices;
	}
	
	/**
	 * Gets the faces.
	 * 
	 * @param f the f
	 * 
	 * @return the faces
	 */
	public static String[] getFaces(File f){ 
		ArrayList surfaceData = readSurfaceFile(f);
		int facesStartIndex = facesOpenTag + 1;
		int facesEndIndex = facesCloseTag -1;
		int numOfFaces = (((facesEndIndex - facesStartIndex) + 1)/3);
        String[] faces = new String[numOfFaces * 3];
        int faceCounter = 0;
        for (int i = facesStartIndex; i <= facesEndIndex; i++){
        	faces [faceCounter] = (String)surfaceData.get(i);
        	faceCounter++;
        }
		return faces;
	}	

}
