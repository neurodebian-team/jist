/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of double values.
 * 
 * @author Blake Lucas
 */
public class PipeDoubleListSource extends PipeListSource {
	
	/** The double param. */
	protected ParamDouble doubleParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("doubleParam");
//		doubleParam.xmlEncodeParam(document, em);		
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		doubleParam = (ParamDouble) outputParams.getFirstChildByName("Float");
//		doubleParam = new ParamDouble();
//		doubleParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"doubleParam"));
		getParentPort().setParameter(doubleParam);
	}
	/**
	 * Double list source.
	 */
	public PipeDoubleListSource() {
		super();
		getParentPort().setParameter(doubleParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Float File List");
		group.setName("doublelist");
		group.setCategory("Number.Float");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float");
		group.add(doubleParam = new ParamDouble("Float"));
		return group;
	}

	/**
	 * Get output parameters.
	 * 
	 * @return the output param
	 */
	public ParamDouble getOutputParam() {
		return doubleParam;
	}

	/**
	 * Get double value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected Object getValue(int i) {
		if (off < data[i].length) {
			return (data[i][off]);
		} else {
			return null;
		}
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object obj) {
		doubleParam.setValue((Number) obj);
	}
}
