package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

// TODO: Auto-generated Javadoc
/**
 * The Class MipavSurfaceXMLWriter.
 */
public class MipavSurfaceXMLWriter {
	
	/** The Constant TAG_1. */
	static final String TAG_1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	/** The Constant TAG_1a. */
	static final String TAG_1a = "<!-- MIPAV header file -->";	
	
	/** The Constant TAG_2. */
	static final String TAG_2 = "<Surface xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
	
	/** The Constant TAG_3. */
	static final String TAG_3 = "\t<Unique-ID>22</Unique-ID>";
	
	/** The Constant TAG_4. */
	static final String TAG_4 = "\t<Material>";
	
	/** The Constant TAG_5. */
	static final String TAG_5 = "\t\t<Ambient>0.0 0.0 0.5</Ambient>";
	
	/** The Constant TAG_6. */
	static final String TAG_6 = "\t\t<Diffuse>0.0 0.0 0.5</Diffuse>";		
	
	/** The Constant TAG_7. */
	static final String TAG_7 = "\t\t<Emissive>0.0 0.0 0.0</Emissive>";		
	
	/** The Constant TAG_8. */
	static final String TAG_8 = "\t\t<Specular>0.0 0.0 0.0</Specular>";
	
	/** The Constant TAG_9. */
	static final String TAG_9 = "\t\t<Shininess>64.0</Shininess>";
	
	/** The Constant TAG_10. */
	static final String TAG_10 = "\t</Material>";		
	
	/** The Constant TAG_11. */
	static final String TAG_11 = "\t<Type>TMesh</Type>";
	
	/** The Constant TAG_12. */
	static final String TAG_12 = "\t<Opacity>0.0</Opacity>";
	
	/** The Constant TAG_13. */
	static final String TAG_13 = "\t<LevelDetail>100</LevelDetail>";
	
	/** The Constant TAG_14. */
	static final String TAG_14 = "\t<Mesh>";
	
	/** The Constant TAG_15. */
	static final String TAG_15 = "\t\t\t<Vertices>";
	
	/** The Constant TAG_16. */
	static final String TAG_16 = "</Vertices>";
	
	/** The Constant TAG_17. */
	static final String TAG_17 = "\t\t\t<Connectivity>";
	
	/** The Constant TAG_18. */
	static final String TAG_18 = "</Connectivity>";
	
	/** The Constant TAG_19. */
	static final String TAG_19 = "\t</Mesh>";
	
	/** The Constant TAG_20. */
	static final String TAG_20 = "</Surface>";
	
	/**
	 * From dx writer.
	 * 
	 * @param vertices the vertices
	 * @param faces the faces
	 * @param f the f
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public static void fromDxWriter(String[] vertices, String[] faces, File f, float X, float Y, float Z)
	{
		try
		{
			FileWriter write = new FileWriter(f);
			PrintWriter output = new PrintWriter(write);
			output.println(TAG_1);
			output.println(TAG_1a);			
			output.println(TAG_2);
			output.println(TAG_3);
			output.println(TAG_4);
			output.println(TAG_5);
			output.println(TAG_6);
			output.println(TAG_7);
			output.println(TAG_8);
			output.println(TAG_9);
			output.println(TAG_10);
			output.println(TAG_11);
			output.println(TAG_12);
			output.println(TAG_13);
			output.println(TAG_14);
			output.print(TAG_15);
	
			int VerticesCounter = 1;
			for (int i = 0; i < vertices.length - 1; i++)
			{
				String verticesData;
				if (VerticesCounter == 1)
				{
					float vertex_x = Float.parseFloat(vertices[i]);
					vertex_x = (float)((2/X)* vertex_x  - 1);
					verticesData = String.valueOf(vertex_x);
					output.print(verticesData + " ");
					VerticesCounter++;
				}
				else if (VerticesCounter == 2)
				{
					float vertex_y = Float.parseFloat(vertices[i]);
					vertex_y = (float)((2/Y)* vertex_y  - 1);
					verticesData = String.valueOf(vertex_y);
					output.print(verticesData + " ");
					VerticesCounter++;
				}
				else 
				{
					float vertex_z = Float.parseFloat(vertices[i]);
					vertex_z = (float)((2/Z)* vertex_z  - 1);
					verticesData = String.valueOf(vertex_z);
					output.print(verticesData + " ");
					VerticesCounter = 1;
				}
					
			}
			output.print(vertices[vertices.length-1]);
			output.println(TAG_16);
			output.print(TAG_17);
			
			int facesCounter = 1;
			for (int i = 0; i < faces.length; i++ )
			{
				String facesData = faces[i];
				if (facesCounter < faces.length-1)
				{
					output.print(facesData + " ");
					facesCounter++;
				}
				else
				{
					output.print(facesData);
					facesCounter = 1;
				}
					
			}
			output.println(TAG_18);
			output.println(TAG_19);
			output.println(TAG_20);
			output.close();
			write.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		return;
	}
	
	
	/**
	 * From free surfer writer.
	 * 
	 * @param vertices the vertices
	 * @param faces the faces
	 * @param f the f
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public static void fromFreeSurferWriter(String[] vertices, String[] faces, File f, float X, float Y, float Z)
	{
		try
		{
			FileWriter write = new FileWriter(f);
			PrintWriter output = new PrintWriter(write);
			output.println(TAG_1);
			output.println(TAG_1a);			
			output.println(TAG_2);
			output.println(TAG_3);
			output.println(TAG_4);
			output.println(TAG_5);
			output.println(TAG_6);
			output.println(TAG_7);
			output.println(TAG_8);
			output.println(TAG_9);
			output.println(TAG_10);
			output.println(TAG_11);
			output.println(TAG_12);
			output.println(TAG_13);
			output.println(TAG_14);
			output.print(TAG_15);
	
			int verticesCounter = 1;
			for (int i = 0; i < vertices.length - 2; i++)
			{
				String verticesData;
				if (verticesCounter == 1)
				{
					float vertex_x = Float.parseFloat(vertices[i]);
					vertex_x = vertex_x * (2/X);
					verticesData = String.valueOf(vertex_x);
					output.print(verticesData + " ");
					verticesCounter++;
				}
				else if (verticesCounter == 2)
				{
					float vertex_y = Float.parseFloat(vertices[i]);
					vertex_y = vertex_y * (2/Y);
					verticesData = String.valueOf(vertex_y);
					output.print(verticesData + " ");
					verticesCounter++;
				}
				else if(verticesCounter == 3)
				{
					float vertex_z = Float.parseFloat(vertices[i]);
					vertex_z = vertex_z * (2/Z);
					verticesData = String.valueOf(vertex_z);
					output.print(verticesData + " ");
					verticesCounter++;
				}
				else verticesCounter = 1;
					
			}
			output.print(vertices[vertices.length-2]);
			output.println(TAG_16);
			output.print(TAG_17);
			
			int facesCounter = 1;
			for (int i = 0; i < faces.length - 2; i++ )
			{
				String facesData = faces[i];
				if (facesCounter < 4)
				{
					output.print(facesData + " ");
					facesCounter++;	
				}
				else facesCounter = 1;
					
			}
			output.print(faces[faces.length-2]);
			output.println(TAG_18);
			output.println(TAG_19);
			output.println(TAG_20);
			output.close();
			write.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		return;
	}

}
