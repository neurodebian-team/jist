package edu.jhu.ece.iacl.jist.pipeline;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeAssertionTestDestination;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;

// TODO: Auto-generated Javadoc
/**
 * The Class PipeLayoutRunner.
 */
public class PipeLayoutRunner {
	
	/** The manager. */
	protected ProcessManager manager;
	
	/** The failed files. */
	protected ArrayList<File> failedFiles;
	
	/** The passed files. */
	protected ArrayList<File> passedFiles;
	
	/**
	 * Instantiates a new pipe layout runner.
	 */
	public PipeLayoutRunner(){
		manager=new ProcessManager();
		failedFiles=new ArrayList<File>();
		passedFiles=new ArrayList<File>();
	}
	
	/**
	 * Inits the.
	 */
	public void init(){
		MipavController.setQuiet(true);
		MipavController.init();
	}
	
	/**
	 * Dispose.
	 */
	public void dispose(){
		manager=null;
	}
	
	/**
	 * Gets the failed files.
	 * 
	 * @return the failed files
	 */
	public ArrayList<File> getFailedFiles(){
		return failedFiles;
	}
	
	/**
	 * Gets the passed files.
	 * 
	 * @return the passed files
	 */
	public ArrayList<File> getPassedFiles(){
		return passedFiles;
	}
	
	/**
	 * Invoke.
	 * 
	 * @param layoutFile the layout file
	 * @param outputDir the output dir
	 * 
	 * @return true, if successful
	 */
	public boolean invoke(File layoutFile,File outputDir){
		return invoke(layoutFile,outputDir);
	}
	
	/**
	 * Invoke.
	 * 
	 * @param layoutFile the layout file
	 * @param outputDir the output dir
	 * @param dataDir the data dir
	 * 
	 * @return true, if successful
	 */
	public boolean invoke(File layoutFile,File outputDir,File dataDir){
		PipeLayout layout=PipeLayout.read(layoutFile);
		if(dataDir!=null){
			PipeLayout.replacePath(layout, null, dataDir);
		}
		String testName=FileReaderWriter.getFileName(layoutFile);
		if(outputDir==null){
			outputDir=new File(layoutFile.getParent(),testName);
		}
		if(!outputDir.exists())outputDir.mkdir();
		layout.getRunParameters().setMaxProcs(1);
		layout.getRunParameters().setOutputDirectory(outputDir);
		manager.runAndWait(layout,false);
		boolean passed=true;
		boolean allPassed=true;
		File testResultFile=null;
		for(PipeModule mod:layout.getAllDescendantPipes()){
			if(mod instanceof PipeAssertionTestDestination){
				PipeAssertionTestDestination dest=((PipeAssertionTestDestination)mod);
				testResultFile=dest.getTestResultsFile();
				passed=dest.didAllTestsPass();
				if(passed){
					passedFiles.add(testResultFile);
				} else {
					allPassed=false;
					failedFiles.add(testResultFile);
					System.err.println("jist.base"+testName+": TEST FAILED, SEE "+testResultFile);
				}
			}
		}
		return allPassed;
	}
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args){
		File layoutFile=null,outputDir=null,dataDir=null;
		if(args.length>=2){
			layoutFile=new File(args[0]);
			outputDir=new File(args[1]);
			if(args.length>2){
				dataDir=new File(args[2]);
			}
			PipeLayoutRunner runner=new PipeLayoutRunner();
			runner.init();
			runner.invoke(layoutFile, outputDir, dataDir);
			for(File f:runner.getPassedFiles()){
				System.out.println("jist.base"+"\t"+"PASSED "+f.getAbsolutePath());
			}
			for(File f:runner.getFailedFiles()){
				System.out.println("jist.base"+"\t"+"FAILED "+f.getAbsolutePath());
			}
			System.exit(0);
		} else {
			System.err.println("jist.base"+"Insufficient Arguments");
		}
	}
}
