package edu.jhu.ece.iacl.jist.pipeline.dest;

import java.io.File;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.CompatibilityChecker;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;

public class PipeAssertionTestDestination extends PipeParamSummaryDestination{
	private class BooleanChecker implements CompatibilityChecker {
		public boolean isCompatible(PipePort model) {
			return (model instanceof ParamBoolean);
		}
	}
	protected ParamCollection createInputParams() {
		selectedParams = new ParamCollection("Parameters");
		getInputParam().setChecker(new BooleanChecker());
		ParamCollection input = new ParamCollection();
		input.setLabel("Assertion Test");
		input.setName("tester");
		input.add(fileName = new ParamString("File Name", "test_results"));
		input.setCategory("Output");
		return input;
	}
	transient Vector<ParamModel> srcOutputs;
	transient boolean allTestsPassed=false;
	public void iterate(ProgressMonitor monitor) {

		Vector<ParamModel> allParams=new Vector<ParamModel>();
		allParams.addAll(srcOutputs);
		allParams.addAll(getInputParam().getAllDescendants());
		Object[] objs = new Object[allParams.size()];
		if (dataObjects.size() == 0) {
			String[] labels = new String[allParams.size()];
			for (int i = 0; i < allParams.size(); i++) {
				ParamModel mod = allParams.get(i);
				labels[i] = mod.getLabel() + " (" + mod.getOwner().getLabel() + ")";
			}
			dataObjects.add(labels);
		}
		int i = 0;
		for (ParamModel model : allParams) {
			if (model instanceof ParamPerformance) {
				objs[i] = ((ParamPerformance) model).getValue().getTotalElapsedCPUTime() / 1000.0;
				i++;
				continue;
			} else if (model instanceof ParamFile) {
				objs[i] = ((ParamFile) model).getValue().getAbsolutePath();
				i++;
				continue;
			} else if (model instanceof ParamVolume) {
				objs[i] = ((ParamVolume) model).getValue().getAbsolutePath();
				i++;
				continue;
			} else if (model instanceof ParamBoolean) {
				boolean passed=((ParamBoolean)model).getValue();
				objs[i] = passed;
				if(!passed&&i>=srcOutputs.size()){
					allTestsPassed=false;
				}
				i++;
				continue;
			}
			String str = model.toString();
			try {
				objs[i] = Integer.parseInt(str);
				i++;
				continue;
			} catch (NumberFormatException e) {
			}
			try {
				objs[i] = Double.parseDouble(str);
				i++;
				continue;
			} catch (NumberFormatException e) {
			}
			objs[i] = str;
			i++;
		}
		dataObjects.add(objs);
	}
	public boolean didAllTestsPass(){
		return allTestsPassed;
	}
	public File getTestResultsFile(){
		return paramSummaryFile.getValue();
	}
	public void reset(){
		super.reset();
		srcOutputs=new Vector<ParamModel>();
		PipeLayout layout=getCurrentLayout();
		if(layout!=null){
			for(PipeModule mod:layout.getAllDescendantPipes()){
				if(mod instanceof PipeSource){
					srcOutputs.add(((PipeSource)mod).getOutputParam());
				}
			}
		}
		allTestsPassed=true;
	}
}
