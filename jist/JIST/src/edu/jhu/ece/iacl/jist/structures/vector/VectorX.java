package edu.jhu.ece.iacl.jist.structures.vector;

// TODO: Auto-generated Javadoc
/**
 * The Interface VectorX.
 */
public interface VectorX extends Cloneable{

	/**
	 * To array.
	 * 
	 * @return the number[]
	 */
	public Number[] toArray();
	
	/**
	 * Size.
	 * 
	 * @return the int
	 */
	public int size();
	
	/**
	 * Gets the.
	 * 
	 * @param i the i
	 * 
	 * @return the number
	 */
	public Number get(int i);
	
	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	public Number getX();
	
	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	public Number getY();
	
	/**
	 * Gets the z.
	 * 
	 * @return the z
	 */
	public Number getZ();
	
	/**
	 * Gets the w.
	 * 
	 * @return the w
	 */
	public Number getW();
	
	/**
	 * Mag.
	 * 
	 * @return the number
	 */
	public Number mag();
	
	/**
	 * Sets the x.
	 * 
	 * @param x the new x
	 */
	public void setX(Number x);
	
	/**
	 * Sets the y.
	 * 
	 * @param y the new y
	 */
	public void setY(Number y);
	
	/**
	 * Sets the z.
	 * 
	 * @param z the new z
	 */
	public void setZ(Number z);
	
	/**
	 * Sets the w.
	 * 
	 * @param w the new w
	 */
	public void setW(Number w);
	
	/**
	 * Sets the.
	 * 
	 * @param value the value
	 * @param i the i
	 */
	public void set(Number value,int i);
	
	/**
	 * Adds the.
	 * 
	 * @param a the a
	 * 
	 * @return the vector x
	 */
	public abstract VectorX add(double a);
	
	/**
	 * Sub.
	 * 
	 * @param a the a
	 * 
	 * @return the vector x
	 */
	public abstract VectorX sub(double a);
	
	/**
	 * Mul.
	 * 
	 * @param a the a
	 * 
	 * @return the vector x
	 */
	public abstract VectorX mul(double a);
	
	/**
	 * Div.
	 * 
	 * @param a the a
	 * 
	 * @return the vector x
	 */
	public abstract VectorX div(double a);
	
	/**
	 * Adds the.
	 * 
	 * @param v the v
	 * 
	 * @return the vector x
	 */
	public abstract VectorX add(VectorX v);
	
	/**
	 * Sub.
	 * 
	 * @param v the v
	 * 
	 * @return the vector x
	 */
	public abstract VectorX sub(VectorX v);
	
	/**
	 * Mul.
	 * 
	 * @param v the v
	 * 
	 * @return the vector x
	 */
	public abstract VectorX mul(VectorX v);
	
	/**
	 * Div.
	 * 
	 * @param v the v
	 * 
	 * @return the vector x
	 */
	public abstract VectorX div(VectorX v);
	
	/**
	 * Normalize.
	 * 
	 * @return the vector x
	 */
	public abstract VectorX normalize();
	
	/**
	 * Clone.
	 * 
	 * @return the vector x
	 */
	public abstract VectorX clone();
}
