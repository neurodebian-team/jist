
package edu.jhu.ece.iacl.jist.processcontrol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import org.ggf.drmaa.AlreadyActiveSessionException;
import org.ggf.drmaa.DrmaaException;
import org.ggf.drmaa.Session;
import org.ggf.drmaa.SessionFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class DispatchController.
 */
public class DispatchController {


    /** The session. */
    private static Session session;
    
    /** The job list. */
    private static ArrayList<ProcessController> jobList = new ArrayList<ProcessController>();

    //Returns DRMAA Session object
    /**
     * Gets the dRMAA session.
     * 
     * @return the dRMAA session
     */
    public static Session getDRMAASession()
    {
        return session; 
    }

    //Returns List of Jobs Created
    /**
     * Gets the job list.
     * 
     * @return the job list
     */
    public static ArrayList getJobList()
    {
        return jobList;
    }

    //Initialize a DRMAA Session
    //Returns whether a session is successfully initialized 
    /**
     * Inits the drmaa session.
     * 
     * @return true, if successful
     */
    public boolean initDRMAASession()
    {
    	if(System.getenv("LD_LIBRARY_PATH")==null)return false;
        boolean success;
        try{
        SessionFactory factory = SessionFactory.getFactory();
            session = factory.getSession();
            session.init("");
            success = true; 
            System.out.println(getClass().getCanonicalName()+"\t"+"Session has been initialized");
        }catch(AlreadyActiveSessionException e)
        {
            success = true;
        }
        catch(DrmaaException e)
        {
            System.out.println(getClass().getCanonicalName()+"\t"+"Exception" + e);
            success = false;
            System.out.println(getClass().getCanonicalName()+"\t"+"Session has not been initialized due to Exception:" + e);
        } catch(LinkageError e) {
            System.out.println(getClass().getCanonicalName()+"\t"+"UnsatisfiedLinkError" + e);
            success = false;
            System.out.println(getClass().getCanonicalName()+"\t"+"Session has not been initialized due to Exception:" + e);    
        }
        return success;    
    }

    /**
     * Restore drmaa session.
     * 
     * @param SessionIDFile the session id file
     * 
     * @return true, if successful
     */
    public boolean restoreDRMAASession(File SessionIDFile)
    {
        boolean success;
        SessionFactory factory = SessionFactory.getFactory();
        this.session = factory.getSession();
        try{
            BufferedReader inputfile = new BufferedReader(new FileReader(SessionIDFile));
            session.init(inputfile.readLine());
            inputfile.close();
            success = true;
        } catch (Exception e)
        {
            System.out.println(getClass().getCanonicalName()+"\t"+"Exception" + e);
            success = false;
        }    
        return success;
    }

    /**
     * Save drmaa session.
     * 
     * @param SessionIDFile the session id file
     * 
     * @return true, if successful
     */
    public boolean saveDRMAASession(File SessionIDFile) {

        boolean success;     
        try{
            FileWriter fstream = new FileWriter(SessionIDFile);
            BufferedWriter out = new BufferedWriter(fstream);
            if(session.getContact()==null)
            {
                System.out.println(getClass().getCanonicalName()+"\t"+"There is no active Session Running");
                success = false;
            }
            else 
            {
                out.write(session.getContact());
                out.close();
                System.out.println(getClass().getCanonicalName()+"\t"+"Session is saved to: " + SessionIDFile.toString());
                success = true;
            }        
        }catch(Exception e)
        {
            System.out.println(getClass().getCanonicalName()+"\t"+"Exception" + e);
            success = false;
        }    
        return success;
    }

    /**
     * Creates the job.
     * 
     * @param SGE the sGE
     * @param Command the command
     * 
     * @return the process controller
     */
    public ProcessController createJob(boolean SGE, List<String> Command)
    {
        //if there is a DRMAA session, return a ProcessControllerDRMAA
        //if there is no DRMAA session, return a ProcessControllerNative
         
        ProcessController p;
        if (SGE)
        {   
            p = new ProcessControllerDRMAA();
            if(!p.Setup(Command)) 
            {
                JOptionPane.showMessageDialog(null, "DRMAA Failed");
                System.err.println(getClass().getCanonicalName()+"Error when creating DRMAA Job, Please check Envoirnment Variable settings, Switch to Native");
            }
        }        
        else
        {
            p = new ProcessControllerNative(Command);            
        }
        jobList.add(p);
        return p;
    }



    /**
     * List all jobs.
     * 
     * @return the array list< process controller>
     */
    public ArrayList<ProcessController> listAllJobs() {
        jobList.size();
        for(int i=0; i<=jobList.size()-1; i++)
        {
            System.out.println(getClass().getCanonicalName()+"\t"+"Job ID: " + jobList.get(i).toString());
            System.out.println(getClass().getCanonicalName()+"\t"+"Status: " + jobList.get(i).getStatus());
        }
        return jobList;
    }


    /**
     * Close session.
     * 
     * @param terminateAllActiveJobs the terminate all active jobs
     */
    public void closeSession(boolean terminateAllActiveJobs)
    {
        int JobCreated = jobList.size();
        if(terminateAllActiveJobs)
        {
            try{
                for(int i=0; i<=JobCreated-1; i++)
                {
                    session.control(jobList.get(i).toString(), session.TERMINATE);
                }
                System.out.println(getClass().getCanonicalName()+"\t"+"All job are terminated");
                session.exit();
            }catch(DrmaaException e)
            {
                System.out.println(getClass().getCanonicalName()+"\t"+"Jobs Cannot be Termianted because Exception:" + e);
            }
        }
        else 
            try{
                session.synchronize(Collections.singletonList(Session.JOB_IDS_SESSION_ALL),
                        Session.TIMEOUT_WAIT_FOREVER, true);
                System.out.println(getClass().getCanonicalName()+"\t"+"ALL jobs are finished");
                session.exit();
                
            }catch(DrmaaException e)
            {
                System.out.println(getClass().getCanonicalName()+"\t"+"Exception" + e);
            }
    }



}
