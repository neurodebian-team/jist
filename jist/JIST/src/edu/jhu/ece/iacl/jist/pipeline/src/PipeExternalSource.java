/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Sweep through range of integers.
 * 
 * @author Blake Lucas
 */
public abstract class PipeExternalSource extends PipeSource {
	ParamModel valParam;
	ParamModel defaultValueParam;
	
	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
		Element em;
		em = document.createElement("valParam-NAME");
		em.appendChild(document.createTextNode(valParam.getName()));			
		parent.appendChild(em);	

		
		em = document.createElement("defaultValueParam-NAME");
		em.appendChild(document.createTextNode(defaultValueParam.getName()));			
		parent.appendChild(em);	

		return true;
	}
	
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		
		valParam = outputParams.getFirstChildByName(JistXMLUtil.xmlReadTag(el, "valParam-NAME"));
		defaultValueParam = outputParams.getFirstChildByName(JistXMLUtil.xmlReadTag(el, "defaultValueParam-NAME"));
//		em.appendChild(document.createTextNode(.getName()));			
//		parent.appendChild(em);	
//
//		
//		em = document.createElement("defaultValueParam-NAME");
//		em.appendChild(document.createTextNode(defaultValueParam.getName()));			
//		parent.appendChild(em);	
//		valParam = 
//			ParamModel.abstractXmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"valParam"));
//		defaultValueParam = 
//			ParamModel.abstractXmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"valParam"));
		getParentPort().setParameter(valParam);
	}
	
	/**
	 * Instantiates a new pipe integer sweep source.
	 */
	public PipeExternalSource() {
		super();
		getParentPort().setParameter(valParam);
	}

	public ParamModel getDefaultParam() {
		return defaultValueParam;
	}
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#getOutputParam()
	 */
	public ParamModel getOutputParam() {	
		return valParam;
	}
	transient private boolean hasNext;
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#hasNext()
	 */
	public boolean hasNext() {
		return (super.hasNext()||hasNext);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#iterate()
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (hasNext) {
					push();
					hasNext=false;
				} else {
					reset();
					isReset = true;
					
				}
			}
		} else {
			reset();
			isReset = true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#reset()
	 */
	public void reset() {
		super.reset();
		hasNext=true;
		push();
	}
}
