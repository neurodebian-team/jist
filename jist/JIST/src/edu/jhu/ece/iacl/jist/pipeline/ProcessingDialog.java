/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterValueException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInformation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import gov.nih.mipav.model.algorithms.AlgorithmBase;
import gov.nih.mipav.model.algorithms.AlgorithmInterface;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.parameters.Parameter;
import gov.nih.mipav.model.scripting.parameters.ParameterException;
import gov.nih.mipav.model.scripting.parameters.ParameterFactory;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.view.DialogDefaultsInterface;
import gov.nih.mipav.view.MipavUtil;
import gov.nih.mipav.view.Preferences;
import gov.nih.mipav.view.ViewJFrameBase;
import gov.nih.mipav.view.dialogs.AlgorithmParameters;
import gov.nih.mipav.view.dialogs.JDialogScriptableBase;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

// TODO: Auto-generated Javadoc
/**
 * <PRE>
 * 
 * Processing Dialog is a generic class that replaces all MIPAV plug-in dialogs
 * The dialogs are auto-generated based on the input and output parameters.
 * 
 * This class should not be extended. Any changes to this class will appear
 * globally across all plug-ins.
 * 
 * </PRE>
 * 
 * @author Blake Lucas
 */
public class ProcessingDialog extends JDialogScriptableBase implements AlgorithmInterface,
		DialogDefaultsInterface, ParamViewObserver, ComponentListener {
	
	/** The Constant serialVersionUID. */
	protected static final long serialVersionUID = 1L;
	
	/** Regular expression to find parameter information in a string *. */
	protected static final Pattern paramInfoRegex = Pattern.compile("^([$\\w_.-]+)\\s+([\\w_]+)\\s+(.*)$");
	
	/** Delimeter used for parameters *. */
	protected static final String paramListDelimiter = "\",\\s*\"";
	
	/** Input parameters to algorithm*. */
//	protected ParamCollection inputParams = null;
	
	/** Output parameters from algorithm. */
//	protected ParamCollection outputParams = null;
	
	/** View for input parameters. */
	protected ParamInputView inputView;
	
	/** View for output parameters. */
	protected ParamOutputView outputView;
	
	/** parent MIPAV frame. */
	protected ViewJFrameBase parentFrame;
	
	/** MIPAV user interface. */
//	protected MipavViewUserInterface userInterface;
	/** processing algorithm */
	protected ProcessingAlgorithm algorithm;
	
	/** Panel for pushing buttons. */
	protected JPanel buttonPanel;
	
	/** Save Directory. */
//	protected File saveDirectory = null;

	/**
	 * Empty constructor needed for dynamic instantiation.
	 */
	public ProcessingDialog() {
	}

	/**
	 * Constructor creates input and output parameters from algorithm and uses
	 * the algorithm name as title.
	 * 
	 * @param algorithm the algorithm
	 */
	public ProcessingDialog(ProcessingAlgorithm algorithm) {
		super();
		inputView = null;
		outputView = null;
		init(algorithm);
		loadDefaults();
	}

	/**
	 * Handle action events.
	 * 
	 * @param event dialog events
	 */
	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		// Okay clicked
		if (command.equalsIgnoreCase("Ok")) {
			clickOk();
		} else if (command.equalsIgnoreCase("Save As Algorithm Input")) {
			clickSaveInput(selectSaveInputFile());
		} else if (command.equalsIgnoreCase("Save As Module Definition")) {
			clickSaveDefinition(selectSaveDefinitionFile());
		} else if (command.equalsIgnoreCase("Save & Close")) {
			clickSaveAll(selectSaveDirectory());
			clickClose();
		} else if (command.equalsIgnoreCase("Open Algorithm Input")) {
			clickOpen(selectOpenFile());
		} else if (command.equalsIgnoreCase("Cancel") || command.equalsIgnoreCase("Close")) {
			clickClose();
		} else if (command.equalsIgnoreCase("Usage")) {
			// Display usage information
			displayHTML(algorithm.getUsageFile(), "Usage");
		} else if (command.equalsIgnoreCase("About")) {
			// Display about information
			displayHTML(algorithm.getAboutFile(), "About");
		}
	};

	/**
	 * Handle the termination of the algorithm. Open image viewers for
	 * ModelImage resources associated with the output parameters
	 * 
	 * @param algo algorithm
	 */
	public void algorithmPerformed(AlgorithmBase algo) {
		if (algo == algorithm) {
			if (algo.isCompleted()) {
				insertScriptLine();
				createOutputDialog();
			} else {
				dispose();
			}
		}
	}

	/**
	 * Build buttons that will appear on input pane.
	 * 
	 * @return the j panel
	 */
	protected JPanel buildInputButtons() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(buildOKButton());
		buttonPanel.add(buildCancelButton());
		return buttonPanel;
	}

	/**
	 * Build buttons that will appear on output pane.
	 * 
	 * @return the j panel
	 */
	protected JPanel buildOutputButtons() {
		JPanel buttonPanel = new JPanel();
		if (algorithm.getOutputDirectory() == null) {
			buttonPanel.add(buildSaveButton());
		}
		buttonPanel.add(buildCloseButton());
		return buttonPanel;
	}

	/**
	 * Build save button for output pane.
	 * 
	 * @return the j button
	 */
	private JButton buildSaveButton() {
		closeButton = new JButton("Save & Close");
		closeButton.addActionListener(this);
		closeButton.setToolTipText("Save output.");
		closeButton.setMinimumSize(MipavUtil.defaultButtonSize);
		Dimension d = MipavUtil.defaultButtonSize;
		d.setSize(140, d.getHeight());
		closeButton.setPreferredSize(d);
		closeButton.setFont(serif12B);
		return closeButton;
	}

	/**
	 * Start the algorithm; but first, save the input parameters as defaults.
	 * Run algorithm as minimum priority thread.
	 */
	public void callAlgorithm() {
		this.setVisible(false);
		// This forces dialog threads that observe MIPAV to shutdown
		if (inputView != null) {
			inputView.setVisible(false);
		}
		saveDefaults();
		
		saveInputBeforeRun(); //2009.06.21 changed to write .input prior to call
		
		// This is very important. Adding this object as a listener allows the
		// algorithm to
		// notify this object when it has completed or failed. See algorithm
		// performed event.
		// This is made possible by implementing AlgorithmedPerformed interface
		algorithm.addListener(this);
		if (!MipavController.isQuiet()) {
			createProgressBar(algorithm.getAlgorithmLabel(), algorithm);
		}
		try {
			if (runInSeparateThread && !MipavController.isQuiet()) {
				if (!algorithm.startMethod(Thread.MIN_PRIORITY)) {
					MipavController.displayError("A thread is already running on this object");
				}
			} else {
				algorithm.runAlgorithm();
				algorithmPerformed(algorithm);
			}
		} catch (OutOfMemoryError x) {
			System.gc();
			MipavController.displayError(algorithm.getName() + ": unable to allocate enough memory");
		}
	}

	/**
	 * Handle event when user clicks "Close".
	 */
	public void clickClose() {
		// Close dialog
		dispose();
//		algorithm.outputParams.dispose();
	}

	/**
	 * Handle event when user clicks "OK".
	 * 
	 * @return true, if click ok
	 */
	public boolean clickOk() {
		if (algorithm.isCompleted()) {
			// Nothing to do, close dialog
			dispose();
		} else {
			try {
				algorithm.inputParams.getInputView().commit();
				// Validate input parameters
				algorithm.inputParams.validate();
				if (!MipavController.isQuiet()) {
					File saveDirectory = selectSaveDirectory();
					if (/*(algorithm != null) && */(saveDirectory != null)) {
						algorithm.setOutputDirectory(saveDirectory);
					}
				}
				// Call algorithm
				if (algorithm.getOutputDirectory() != null) {
					callAlgorithm();
				} else {
					return false;
				}
				
			} catch (InvalidParameterException e) {
				if (MipavController.isQuiet()) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
					System.err.flush();
				} else {
					MipavController.displayError(e.getMessage());
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * Handle event when user clicks "Open".
	 * 
	 * @param file the file
	 * 
	 * @return true, if click open
	 */
	public boolean clickOpen(File file) {
		// Load input profile
		if (file != null) {
			if (load(file)) {
				return true;
			} else {
				if (MipavController.isQuiet()) {
					System.err.println(getClass().getCanonicalName()+"Could not open " + file.getAbsolutePath() + ".");
				} else {
					MipavController.displayError("Could not open " + file.getAbsolutePath() + ".");
				}
			}
		}
		return false;
	}

	/**
	 * Handle event when user clicks "Save Parameters and Resources".
	 * 
	 * @param saveDir the save dir
	 */
	public void clickSaveAll(File saveDir) {
		// Save all input and output parameters if they exist
		if (!saveAll(saveDir)) {
			MipavController.displayError(algorithm.getAlgorithmLabel() + " could not save information.");
		}
	}

	/**
	 * Handle event when user clicks "Save As Module Definition".
	 * 
	 * @param saveFile the save file
	 */
	public void clickSaveDefinition(File saveFile) {
		// Save all input and output parameters if they exist
		PipeAlgorithm pAlgo = new PipeAlgorithm(algorithm);
		pAlgo.write(saveFile);
	}

	/**
	 * Handle event when user clicks "Save As Algorithm Input".
	 * 
	 * @param saveFile the save file
	 */
	public void clickSaveInput(File saveFile) {
		// Save all input and output parameters if they exist
		if (saveInput(saveFile)) {
			if (algorithm.isCompleted()) {
				dispose();
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
	 */
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
	 */
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Shameless hack to circumvent bug in linux implementation that prevents
	 * window from being resized.
	 * 
	 * @param event the event
	 */
	public void componentResized(ComponentEvent event) {
//		Dimension oldd = this.getSize();
//		Dimension newd = algorithm.getPreferredSize();
//		if (newd != null) {
//			this.setPreferredSize(new Dimension((int) Math.max(oldd.getWidth(), newd.getWidth()), (int) Math.max(oldd
//					.getHeight(), newd.getHeight())));
//		}
		this.setPreferredSize(this.getSize());
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
	 */
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Create input dialog frame from input parameters.
	 */
	protected void createInputDialog() {
		if (!MipavController.isQuiet()) {
			this.setVisible(false);
			setTitle(algorithm.inputParams.getLabel());
			for(ParamModel mod:algorithm.getInput().getAllDescendantsByClass(ParamInformation.class)){
				((ParamInformation)mod).getValue().setEditable(false);
			}
			
			Container pane = getContentPane();
			pane.setLayout(new BorderLayout());
			this.setResizable(true);
			this.addComponentListener(this);
			this.setJMenuBar(createInputMenuBar());
			pane.setPreferredSize(algorithm.getPreferredSize());
			pane.add(inputView = algorithm.inputParams.getInputView(), BorderLayout.CENTER);
			pane.add(buttonPanel = buildInputButtons(), BorderLayout.SOUTH);
			this.addWindowListener(this);
			inputView.addObserver(this);
			inputView.update();
			this.pack();
			this.setVisible(true);
		}
	}

	/**
	 * Create menubar for input pane.
	 * 
	 * @return menu bar
	 */
	public JMenuBar createInputMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		// Create file and help menus
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		// JMenu helpMenu = new JMenu("Help");
		// helpMenu.setMnemonic(KeyEvent.VK_H);
		menuBar.add(fileMenu);
		// menuBar.add(helpMenu);
		// Add file menu items
		JMenuItem openItem = new JMenuItem("Open Algorithm Input", KeyEvent.VK_O);
		JMenuItem saveItem = new JMenuItem("Save As Algorithm Input", KeyEvent.VK_S);
		JMenuItem saveMapItem = new JMenuItem("Save As Module Definition", KeyEvent.VK_M);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(saveMapItem);
		openItem.addActionListener(this);
		saveItem.addActionListener(this);
		saveMapItem.addActionListener(this);
		// Add help menu items
		// JMenuItem contentsItem = new JMenuItem("Usage", KeyEvent.VK_U);
		// JMenuItem aboutItem = new JMenuItem("About", KeyEvent.VK_A);
		// helpMenu.add(contentsItem);
		// helpMenu.add(aboutItem);
		// contentsItem.addActionListener(this);
		// aboutItem.addActionListener(this);
		return menuBar;
	}

	/**
	 * Create output dialog frame from output parameters. This method reuses the
	 * input frame.
	 */
	private void createOutputDialog() {
		if (!MipavController.isQuiet()) {
			File saveDirectory = algorithm.getOutputDirectory();
			if (saveDirectory != null) {
				saveAll(saveDirectory);
			}
			this.setVisible(false);
			this.setResizable(true);
			this.remove(inputView);
			this.remove(buttonPanel);
			this.setJMenuBar(null);
			setTitle(algorithm.inputParams.getLabel());
			Container pane = getContentPane();
			pane.setPreferredSize(algorithm.getPreferredSize());
			pane.add(outputView = algorithm.outputParams.getOutputView(), BorderLayout.NORTH);
			pane.add(buttonPanel = buildOutputButtons(), BorderLayout.SOUTH);
			pack();
			this.setVisible(true);
			outputView.update();
		}
	}

	/**
	 * Display file that contains HTML.
	 * 
	 * @param file file name
	 * @param title frame title
	 */
	private void displayHTML(File file, String title) {
		JEditorPane editPane = new JEditorPane();
		JScrollPane scrollPane = new JScrollPane();
		JFrame frame = new JFrame(title);
		frame.add(scrollPane);
		scrollPane.setViewportView(editPane);
		scrollPane.setPreferredSize(new Dimension(640, 520));
		try {
			editPane.setPage(file.toURL());
		} catch (MalformedURLException e) {
			// FIXME Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// FIXME Auto-generated catch block
			e.printStackTrace();
		}
		editPane.setEditable(false);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Dispose of dialog.
	 */
	public void dispose() {
		if (inputView != null) {
			inputView.setVisible(false);
		}
		super.dispose();
	}

	/**
	 * Store result images in script runner.
	 * 
	 * Removed - script runner declared a hack. will be repared at a future time
	 */
	protected void doPostAlgorithmActions() {
		throw new RuntimeException("NOT IMPLEMENTED");
		/*
		for (ParamModel mod : algorithm.outputParams.getAllDescendants()) {
			if (mod instanceof ParamVolume) {
				ModelImage img = ((ParamVolume) mod).getModelImageCache();
				AlgorithmParameters.storeImageInRunner(img);
			} else if (mod instanceof ParamVolumeCollection) {
				List<ModelImage> images = ((ParamVolumeCollection) mod).getModelImageCacheList();
				for (ModelImage img : images) {
					if (img != null) {
						AlgorithmParameters.storeImageInRunner(img);
					}
				}
			}
		}
		*/
	}
	

	/**
	 * Get input view.
	 * 
	 * @return input view
	 */
	public ParamInputView getInputView() {
		return inputView;
	}

	/**
	 * Get output view.
	 * 
	 * @return output view
	 */
	public ParamOutputView getOutputView() {
		return outputView;
	}

	/**
	 * Get parent frame for Mipav.
	 * 
	 * @return parent frame
	 */
	public ViewJFrameBase getParentFrame() {
		return parentFrame;
	}

	/**
	 * Get user interface for Mipav.
	 * 
	 * @param pipeFile the pipe file
	 * 
	 * @return user interface
	 */
//	public MipavViewUserInterface getUserInterface() {
//		return userInterface;
//	}

	/**
	 * Initialize parameters and algorithm from file.
	 * 
	 * @param pipeFile
	 *            pipe file
	 */
	public void init(File pipeFile) {
		algorithm.init(pipeFile);
//		this.inputParams = algorithm.getInput();
//		this.outputParams = algorithm.getOutput();
		this.setName(algorithm.getAlgorithmLabel());
	}

	/**
	 * Initialize parameter and algorithm from existing algorithm.
	 * 
	 * @param pipe algorithm
	 */
	public void init(PipeAlgorithm pipe) {
		algorithm.init(pipe);
//		this.inputParams = algorithm.getInput();
//		this.outputParams = algorithm.getOutput();
		this.setName(algorithm.getAlgorithmLabel());
	}

	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param algorithm the algorithm
	 */
	private void init(ProcessingAlgorithm algorithm) {
		this.algorithm = algorithm;
//		this.inputParams = algorithm.getInput();
//		this.outputParams = algorithm.getOutput();
		this.setName(algorithm.getAlgorithmLabel());
	}

	/**
	 * Loads algorithm input profile from specified location into input
	 * parameters.
	 * 
	 * @param file the file
	 * 
	 * @return true if successful
	 */
	public boolean load(File file) {
		boolean ret = algorithm.inputParams.read(file);
		return ret;
	}

	/**
	 * Load defaults from MIPAV preferences.
	 */
	public void loadDefaults() {
		//FIXME Why is this outside of JistPreferenecs?!?
		String defaults = Preferences.getDialogDefaults(getName());
		ParameterTable paramList = new ParameterTable();
		if ((defaults != null) && !MipavController.isQuiet()) {
			System.out.println(getClass().getCanonicalName()+"\t"+"LOAD DEFAULTS " + defaults);
			String[] params = defaults.split(paramListDelimiter);
			for (int i = 0; i < params.length; i++) {
				if (params[i].startsWith("\"")) {
					params[i] = params[i].substring(1);
				}
				if (params[i].endsWith("\"")) {
					params[i] = params[i].substring(0, params[i].length() - 1);
				}
				Parameter param = null;
				String paramInfo = params[i];
				Matcher matcher = paramInfoRegex.matcher(paramInfo);
				while (matcher.find()) {
					if (matcher.groupCount() == 3) {
						try {
							param = ParameterFactory.parseParameter(matcher.group(1), matcher.group(2), matcher
									.group(3));
							paramList.put(param);
						} catch (ParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			Vector<ParamModel> children = algorithm.inputParams.getAllVisibleDescendants();
			for (ParamModel mod : children) {
				if (!((mod instanceof ParamVolume) || (mod instanceof ParamVolumeCollection))) {
					try {
						mod.importMipavParameter(paramList);
					} catch (ParameterException e) {
						System.err.println(getClass().getCanonicalName()+e.getMessage());
					} catch (InvalidParameterValueException e) {
						System.err.println(getClass().getCanonicalName()+e.getMessage());
					} catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Save all input and output parameters and their external resources.
	 * 2009.06.21 DO NOT SAVE INPUTs after run
	 * 
	 * @param saveDir the save dir
	 * 
	 * @return save completed successfully
	 */
	protected boolean saveAll(File saveDir) {
		if (algorithm != null&&algorithm.isCompleted()&&saveDir != null) {
			
			/*
			 * boolean ret=(outputParams.saveResources(saveDir) &&
			 * inputParams.write(new File(saveDir, outputParams .getName() +
			 * ".input")) && outputParams.write(new File( saveDir,
			 * outputParams.getName() + ".output")));
			 */
			// Only save output parameters
			boolean ret;
			if(algorithm.isRunningInSeparateProcess()){
				ret = algorithm.outputParams.saveResources(saveDir,false);
				if(!ret)
					System.out.println(getClass().getCanonicalName()+"\t"+"SaveAll Failing: Could not write resources.");
				ret = ret && algorithm.outputParams.write(new File(saveDir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(algorithm.outputParams.getName()) + ".output"));
				if(!ret)
					System.out.println(getClass().getCanonicalName()+"\t"+"SaveAll Failing: Could not write ouput.");
				/*ret = ret && inputParams.write(new File(saveDir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(inputParams.getName()) + ".input"));
				if(!ret)
					System.out.println(getClass().getCanonicalName()+"\t"+"SaveAll Failing: Could not write input.");
					*/
				if (ret) {
//					saveDirectory = saveDir;
					if (algorithm != null) {
						algorithm.setOutputDirectory(saveDir);
					}
				} else 
					System.out.println(getClass().getCanonicalName()+"\t"+"SaveAll Failing: Could not write i/o.");
			} else {
				// 2009.07.24 MUST WRITE --- Do not save resources if running in same thread in order to cut down on memory consumption and time
				ret =  algorithm.outputParams.write(
						new File(saveDir, 
								edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(algorithm.outputParams.getName()) + ".output"));
				ret=true;
			}
			return ret;
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"SaveAll Failing: Not ready");
			return false;
		}
	}
	
	/**
	 * Save all input params after run so that inputs can be checked for current consistency
	 * 2009.06.21 DO NOT SAVE INPUTs after run
	 * 
	 * @param saveDir the save dir
	 * 
	 * @return save completed successfully
	 */
	protected boolean saveInputBeforeRun() {
		System.out.println(getClass().getCanonicalName()+"\t"+"**************************************************************************");
		File saveDir = algorithm.getOutputDirectory();//saveDirectory;
//		if(saveDir== null && algorithm !=null)
//			algorithm.getOutputDirectory();
		if (algorithm != null&&saveDir != null) {
			boolean ret;
			if(algorithm.isRunningInSeparateProcess()){
				ret = algorithm.inputParams.write(new File(saveDir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(algorithm.inputParams.getName()) + ".input"));
				if(!ret) 
					System.out.println(getClass().getCanonicalName()+"\t"+"saveInputBeforeRun Failing: Could not write input.");			
			} else {
				//WRONG -- MUST SAVE -- Do not save resources if running in same thread in order to cut down on memory consumption and time
				ret = algorithm.inputParams.write(new File(saveDir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(algorithm.inputParams.getName()) + ".input")); 
				/*&& algorithm.outputParams.write(new File(saveDir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(algorithm.outputParams.getName()) + ".output"))*/;
			}
			return ret;
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"saveInputBeforeRun Failing: Not ready");
			System.out.println(getClass().getCanonicalName()+"\t"+"algorithm="+algorithm);
			System.out.println(getClass().getCanonicalName()+"\t"+"saveDir="+saveDir);
			return false;
		}
	}

	/**
	 * Saves input dialog settings to XML file. This method does not save the
	 * external associated resources for the input parameters.
	 */
	public void saveDefaults() {
		if (Preferences.isPreference(Preferences.PREF_SAVE_DEFAULTS) && (this.getOwner() != null)
				&& !MipavController.isQuiet()) {
			AlgorithmParameters scriptParams = new AlgorithmParameters();
			for (ParamModel mod : algorithm.inputParams.getAllDescendants()) {
				if (!((mod instanceof ParamVolume) || (mod instanceof ParamVolumeCollection))) {
					try {
						mod.createMipavParameter(scriptParams);
					} catch (ParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			String defaults = scriptParams.getParams().convertToString();
			Preferences.saveDialogDefaults(getName(), defaults);
			System.out.println(getClass().getCanonicalName()+"\t"+"SAVED DEFAULTS " + defaults);
		}
	}

	/**
	 * Save all input parameters to specified file.
	 * 
	 * @param saveFile the save file
	 * 
	 * @return save completed successfully
	 */
	protected boolean saveInput(File saveFile) {
		if (saveFile != null) {
			algorithm.inputParams.getInputView().commit();
			return algorithm.inputParams.write(saveFile);
		} else {
			return false;
		}
	}

	/**
	 * Loads algorithm input profile from specified location into input
	 * parameters. A file dialog box will appear for the user to specify the
	 * location.
	 * 
	 * @return true if successful
	 */
	private File selectOpenFile() {
		JFileChooser loadDialog = new JFileChooser("Specify Input Profile");
		loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(new String[] { "input" }));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			MipavController.setDefaultWorkingDirectory(loadDialog.getSelectedFile().getParentFile());
			return loadDialog.getSelectedFile();
		} else {
			return null;
		}
	}

	/**
	 * Save input parameters to file.
	 * 
	 * @return the file
	 */
	private File selectSaveDefinitionFile() {
		JFileChooser saveDialog = new JFileChooser("Specify Algorithm Definition File");
		File mapFile = algorithm.getMapFile();
		if (mapFile != null) {
			saveDialog.setSelectedFile(mapFile);
		} else {
			saveDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		}
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		saveDialog.setFileFilter(new FileExtensionFilter(new String[] { JistPreferences.getDefaultModuleExtension() }));
		saveDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = saveDialog.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File f = saveDialog.getSelectedFile();
			f = new File(f.getParent(), FileReaderWriter.getFileName(f) + "." +JistPreferences.getDefaultModuleExtension());
			MipavController.setDefaultWorkingDirectory(f.getParentFile());
			return f;
		} else {
			return null;
		}
	}

	/**
	 * Open dialog to specify output directory for parameters.
	 * 
	 * @return the file
	 */
	protected File selectSaveDirectory() {
		JFileChooser saveDialog = new JFileChooser("Specify Output Directory");
		saveDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		saveDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = saveDialog.showSaveDialog(this);
		if ((returnVal == JFileChooser.APPROVE_OPTION) && (saveDialog.getSelectedFile() != null)
				&& saveDialog.getSelectedFile().isDirectory()) {
			MipavController.setDefaultWorkingDirectory(saveDialog.getSelectedFile().getAbsoluteFile());
			return saveDialog.getSelectedFile().getAbsoluteFile();
		} else {
			return null;
		}
	}

	/**
	 * Save input parameters to file.
	 * 
	 * @return the file
	 */
	private File selectSaveInputFile() {
		JFileChooser saveDialog = new JFileChooser("Specify Algorithm Input File");
		saveDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		saveDialog.setFileFilter(new FileExtensionFilter(new String[] { "input" }));
		saveDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = saveDialog.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File f = saveDialog.getSelectedFile();
			f = new File(f.getParent(), FileReaderWriter.getFileName(f) + ".input");
			MipavController.setDefaultWorkingDirectory(f.getParentFile());
			return f;
		} else {
			return null;
		}
	}

	/**
	 * Set up the dialog GUI based on the parameters before running the
	 * algorithm as part of a script.
	 */
	protected void setGUIFromParams() {
		ParameterTable params = scriptParameters.getParams();
		String algoClass = params.getString("Algorithm");
		Class c = null;
		try {
			c = Class.forName(algoClass);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		if (c == null) {
			System.err.print("Could not find algorithm class");
			return;
		} else {
			ProcessingAlgorithm algo = null;
			try {
				algo = (ProcessingAlgorithm) c.newInstance();
				init(algo);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			Vector<ParamModel> children = algorithm.inputParams.getAllDescendants();
			for (ParamModel mod : children) {
				try {
					mod.importMipavParameter(params);
				} catch (ParameterException e) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			algorithm.inputParams.loadResources(algorithm.inputParams, null);
			algorithm.inputParams.getInputView().update();
			createInputDialog();
		}
	}

	/**
	 * Set directory to store output results.
	 * 
	 * @param f output directory
	 */
	public void setOutputDirectory(File f) {
//		this.saveDirectory = f;
//		if (algorithm != null) {
			algorithm.setOutputDirectory(f/*saveDirectory*/);
//		}
	}

	/**
	 * Set parent MIPAV frame.
	 * 
	 * @param parentFrame the parent frame
	 */
	public void setParentFrame(ViewJFrameBase parentFrame) {
		this.parentFrame = parentFrame;
	}

	/**
	 * Start dialog with input parameters from algorithm.
	 */
	public void start(/*ViewUserInterface userInterface*/) {
//		this.userInterface = userInterface;
		createInputDialog();		
	}

	/**
	 * Store the parameters from the dialog to record the execution of this
	 * algorithm.
	 * 
	 * @throws ParserException If there is a problem creating one of the new parameters.
	 */

	protected void storeParamsFromGUI() throws ParserException {
		throw new RuntimeException("NOT IMPLEMENTED");
		/*
		Vector<ParamModel> children = algorithm.inputParams.getAllDescendants();
		for (ParamModel child : children) {
			child.createMipavParameter(scriptParameters);
		}
		for (ParamModel mod : algorithm.outputParams.getAllDescendants()) {
			if (mod instanceof ParamVolume) {
				ModelImage img = ((ParamVolume) mod).getModelImageCache();
				if(img!=null){
					scriptParameters.storeImageInRecorder(img);
				}
			} else if (mod instanceof ParamVolumeCollection) {
				List<ModelImage> images = ((ParamVolumeCollection) mod).getModelImageCacheList();
				for (ModelImage img : images) {
					if (img != null) {
						scriptParameters.storeImageInRecorder(img);
					}
				}
			}
		}
		*/
	}

	/**
	 * An update to the parameters occurred.
	 * 
	 * @param model parameter
	 * @param view input view
	 */
	public void update(ParamModel model, ParamInputView view) {
		this.pack();
	}

	/**
	 * Make sure all threads shutdown when window is closed.
	 * 
	 * @param e window event
	 */
	public void windowClosing(WindowEvent e) {
		super.windowClosing(e);
		// Insure all input threads shutdown
		inputView.setVisible(false);
	}
}
