package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceLocation;

public class PipeSurfaceExternalSource extends PipeExternalSource{
	public PipeSurfaceExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamSurfaceLocation("Default Surface"));
		group.setLabel("External Surface");
		group.setName("extsurface");
		group.setCategory("Externalize.Surface");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Surface");
		group.add(valParam = new ParamSurfaceLocation("Surface"));
		return group;
	}
}
