/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.parameter.DataLabel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.DataLabelCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDataLabel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;

/**
 * Data Label Input View displays a combobox to select an image and a scroll
 * pane to display the discovered data labels for that image. There is also a
 * button to load labels from a file. This will also load the image associated
 * with that label file.
 * 
 * @author Blake Lucas
 */
public class ParamDataLabelInputView extends ParamVolumeInputView implements ParamViewObserver {
	
	/**
	 * Data Label Item to display in the scroll pane.
	 * 
	 */
	private class DataLabelItem extends JPanel implements CaretListener, ActionListener {
		
		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 1L;
		
		/** The type label. */
		private JComboBox typeLabel;
		
		/** The text label. */
		private JTextField textLabel;
		
		/** The intensity label. */
		private JTextField intensityLabel;
		
		/** The value label. */
		private JLabel valueLabel;
		
		/** The color label. */
		private JPanel colorLabel;
		
		/** The label. */
		private DataLabel label;

		/**
		 * Construct view to enter data label fields as originally implemented
		 * in XToads.
		 * 
		 * @param label
		 *            the label
		 * @param c
		 *            the color
		 */
		public DataLabelItem(DataLabel label, Color c) {
			this.label = label;
			// Create combobox for label type
			typeLabel = new JComboBox(DataLabel.types);
			typeLabel.setSelectedIndex(label.getType());
			textLabel = new JTextField(6);
			textLabel.setText(label.name);
			// Create text field to enter intensity value
			intensityLabel = new JTextField(3);
			intensityLabel.setText(label.getIntensity() + "");
			// Create id label
			valueLabel = new JLabel(label.getIntensity() + "");
			valueLabel.setPreferredSize(new Dimension(50, 20));
			// Create color representation of id value
			colorLabel = new JPanel();
			colorLabel.setBackground(c);
			colorLabel.setMaximumSize(new Dimension(20, 20));
			colorLabel.setPreferredSize(new Dimension(20, 20));
			this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			this.setMaximumSize(new Dimension(150, 20));
			this.setPreferredSize(new Dimension(150, 20));
			textLabel.addCaretListener(this);
			typeLabel.addActionListener(this);
			intensityLabel.addCaretListener(this);
			this.add(colorLabel);
			this.add(valueLabel);
			this.add(textLabel);
			this.add(typeLabel);
			this.add(intensityLabel);
		}

		/**
		 * Update type with selected label type.
		 * 
		 * @param event
		 *            combobox selection update
		 */
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == typeLabel) {
				label.setType(typeLabel.getSelectedIndex());
			}
		}

		/**
		 * Update label with current intensity value.
		 * 
		 * @param event
		 *            intensity value update
		 */
		public void caretUpdate(CaretEvent event) {
			if (event.getSource() == textLabel) {
				label.name = textLabel.getText();
			} else if (event.getSource() == intensityLabel) {
				label.setIntensity(Float.parseFloat(intensityLabel.getText()));
			}
		}
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -57801194197759561L;
	
	/** The scroll pane. */
	private JScrollPane scrollPane;
	
	/** The load button. */
	private JButton loadButton;

	/**
	 * Construct new scroll pane to display label information for image.
	 * 
	 * @param param
	 *            the parameter
	 */
	public ParamDataLabelInputView(ParamDataLabel param) {
		super(param);
		scrollPane = new JScrollPane();
		scrollPane.setMinimumSize(new Dimension(150, 320));
		JPanel smallPane = new JPanel();
		smallPane.setLayout(new BorderLayout(5, 5));
		smallPane.add(loadButton = new JButton("Load Label File"), BorderLayout.SOUTH);
		smallPane.add(scrollPane, BorderLayout.CENTER);
		smallPane.add(field, BorderLayout.NORTH);
		this.add(smallPane, BorderLayout.CENTER);
		loadButton.addActionListener(this);
	}

	/**
	 * Open file dialog when load button is pressed.
	 * 
	 * @param event
	 *            data label change event
	 */
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == loadButton) {
			// Select label file
			File f = openFileChooser();
			if (f != null) {
				// Open label file and update scroll pane
				((ParamDataLabel) param).openLabelFile(f);
				this.setSelected(((ParamDataLabel) param).getImageData());
			}
		} else if (event.getSource().equals(field)) {
			updateScrollPane();
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamVolumeInputView#getParameter()
	 */
	public ParamDataLabel getParameter() {
		return (ParamDataLabel) param;
	}

	/**
	 * Open file dialog to select label file.
	 * 
	 * @return selected file
	 */
	private File openFileChooser() {
		JFileChooser openDialog = new JFileChooser();
		openDialog.setDialogTitle("Select Label File");
		openDialog.setFileFilter(new FileExtensionFilter(new String[] { "label" }));
		openDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		File oldFile = getParameter().getValue();
		if ((oldFile != null) && oldFile.exists()) {
			openDialog.setSelectedFile(oldFile);
		} else {
			openDialog.setCurrentDirectory(JistPreferences.getPreferences().getLastDirectory());
		}
		openDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		int returnVal = openDialog.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			JistPreferences.getPreferences().setLastDirectory(openDialog.getSelectedFile().getParentFile());
			return openDialog.getSelectedFile();
		} else {
			return null;
		}
	}

	/**
	 * Update view with current parameter value.
	 */
	public void update() {
		super.update();
		updateScrollPane();
	}

	/**
	 * When an update to the parameter occurs, update the view.
	 * 
	 * @param model
	 *            parameter
	 * @param view
	 *            input view
	 */
	public void update(ParamModel model, ParamInputView view) {
		update();
	}

	/**
	 * Update combobox and scroll pane when requested.
	 * 
	 * @param item
	 *            selected item
	 * @return true, if update combo box
	 */
	protected boolean updateComboBox(String item) {
		if (super.updateComboBox(item)) {
			updateScrollPane();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Update scroll pane with current data labels.
	 */
	private /*synchronized*/ void updateScrollPane() {
		// No scroll pane created yet
		if (scrollPane == null) {
			return;
		}
		float intensity;
		float Imin = Float.MAX_VALUE, Imax = Float.MIN_VALUE;
		DataLabelCollection labels = getParameter().getDataLabels();
		// Create pane to view data label classes
		JPanel classView = new JPanel();
		classView.setLayout(new BorderLayout());
		// Create pane to layout class pane
		JPanel smallView = new JPanel();
		classView.add(smallView, BorderLayout.NORTH);
		if (labels != null) {
			smallView.setLayout(new GridLayout(labels.size(), 1));
			// Determine min and max label id
			for (DataLabel label : labels) {
				Imin = Math.min(label.getId(), Imin);
				Imax = Math.max(label.getId(), Imax);
			}
			// Create colors from normalized id values
			for (DataLabel label : labels) {
				intensity = label.getId();
				float b = (intensity - Imin) / (Imax - Imin);
				DataLabelItem item = new DataLabelItem(label, new Color(b, b, b));
				smallView.add(item);
			}
		}
		// Update scroll pane
		scrollPane.setViewportView(classView);
		scrollPane.revalidate();
	}
}
