package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

public class PipeFileCollectionListSource extends PipeListSource {
	
	private final File f = new File("C:\\catnapGC\\aaaaOut.txt");

	/** The volume collection param. */
	protected ParamFileCollection filecolParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("filecolParam");
//		filecolParam.xmlEncodeParam(document, em);		
//		
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		filecolParam = (ParamFileCollection)outputParams.getFirstChildByName("File Collection");
//		filecolParam = new ParamFileCollection();
//		filecolParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filecolParam"));
		getParentPort().setParameter(filecolParam);
	}
	/**
	 * Default constructor.
	 */
	public PipeFileCollectionListSource() {
		super();
		getParentPort().setParameter(filecolParam);
	}
	
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("File Collection List");
		group.setName("filecollectionlist");
		group.setCategory("File");
		return group;
	}
	
	@Override
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File Collection List");
		group.add(filecolParam = new ParamFileCollection("File Collection"));
		filecolParam.setExtensionFilter(new FileExtensionFilter());
		return group;
	}

	@Override
	public ParamFileCollection getOutputParam() {
		return filecolParam;
	}
	
	@Override
	protected ArrayList<File> getValue(int i) {
		if (off < data[i].length) {
			ArrayList<File> vollist = new ArrayList<File>();
			for(int j=0; j<data[i].length; j++){
				vollist.add(new File(data[i][j].toString().trim()));
			}
			return vollist;
		} else {
			return null;
		}
	}

	@Override
	protected void setValue(Object obj) {
		filecolParam.setValue((List<File>)obj);
	}
	
}
