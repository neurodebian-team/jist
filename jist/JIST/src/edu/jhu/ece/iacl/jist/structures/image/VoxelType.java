package edu.jhu.ece.iacl.jist.structures.image;

// TODO: Auto-generated Javadoc
/**
 * The Enum VoxelType.
 */
public enum VoxelType {
	/** The BYTE. */
	BYTE,	
	/** The UBYTE. */
UBYTE,/** The DOUBLE. */
DOUBLE,/** The FLOAT. */
FLOAT,/** The COLOR. */
COLOR,/** The COLO r_ ushort. */
COLOR_USHORT,/** The COLO r_ float. */
COLOR_FLOAT,/** The SHORT. */
SHORT,/** The USHORT. */
USHORT,/** The INT. */
INT,/** The BOOLEAN. */
/** The UINT. */
UINT,
/** The LONG. */
LONG,
BOOLEAN,/** The VECTORX. */
VECTORX};
