/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.util.UUID;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamHeaderFactory;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamHiddenFactory;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Header identifies plug-in.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamHeader extends ParamHidden<Class> implements JISTInternalParam {
	
	/** Canonical calss name, which may or may not exist. */
	protected String className;

	/** Version of MAPS that created this header. */
	protected float version;

	/** Least significant part of uuid **/
	protected String uuid;
	
	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);		 
		 Element em;				
		 em = document.createElement("header-className");				 
		 em.appendChild(document.createTextNode(className+""));
		 parent.appendChild(em);		
		 
		 em = document.createElement("header-version");				 
		 em.appendChild(document.createTextNode(version+""));
		 parent.appendChild(em);		
		 
		 em = document.createElement("header-uuid");				 
		 em.appendChild(document.createTextNode(uuid+""));
		 parent.appendChild(em);
				
		 return true;
	 }
	 
	 public void xmlDecodeParam(Document document, Element el) {
			super.xmlDecodeParam(document, el);
			className = (JistXMLUtil.xmlReadTag(el, "header-className"));
			version = Float.valueOf(JistXMLUtil.xmlReadTag(el, "header-version"));
			uuid = (JistXMLUtil.xmlReadTag(el, "header-uuid"));			
			
		}

	public void stampUUID(){
		UUID id=UUID.randomUUID();
		uuid=id.toString();
	}
	public void setUUID(String uuid){
		this.uuid=uuid;
	}
	public String getUUID(){
		return uuid;
	}
	/**
	 * Default constructor.
	 */
	public ParamHeader() {
		super();
		this.factory = new ParamHeaderFactory(this);
	}

	/**
	 * Instantiates a new param header.
	 * 
	 * @param name
	 *            the name
	 * @param c
	 *            the c
	 */
	public ParamHeader(String name, Class c) {
		super();
		this.factory = new ParamHeaderFactory(this);
		this.setName(name);
		this.setValue(c);
		this.stampUUID();
		this.version = ProcessingAlgorithm.JIST_VERSION;
	}

	/**
	 * Clone object.
	 * 
	 * @return the param header
	 */
	public ParamHeader clone() {
		ParamHeader param = new ParamHeader(getName(), getValue());
		param.setVersion(this.getVersion());
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.uuid=(uuid!=null)?new String(uuid):null;
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Compare values for compatibility.
	 * 
	 * @param obj
	 *            the obj
	 * @return the int
	 */
	public int compareTo(ParamModel obj) {
		if (obj instanceof ParamHeader) {
			if (((ParamHeader) obj).className.equals(this.className)) {
				return 0;
			} else {
				return 1;
			}
		}
		return 1;
	}

	/**
	 * Get plug-in class.
	 * 
	 * @return the value
	 */
	public Class getValue() {
		Class cls=findClass(className);
		if(cls!=null&&!cls.getCanonicalName().equals(className)){
			className=cls.getCanonicalName();
		}
		return cls;
	}
	public static Class findClass(String className){
		if (className != null) {
			try {
				return Class.forName(className);
			} catch (ClassNotFoundException e) {
				String nm=className.substring(className.lastIndexOf(".")+1,className.length());
				Vector<Class> matches=PipeLibrary.getAllClasses("edu", nm);
				if(matches.size()>0){
					if(matches.size()>1){
						System.out.println("jist.param"+"\t"+"Warning: More than one match for class "+nm);
						System.out.println("jist.param"+"\t"+"Matches: "+matches);
					}
					System.err.println("jist.base"+"Warning: Could not find exact match, using "+matches.firstElement()+" instead of "+className);
					return matches.firstElement();
				} else {
					System.err.println("jist.base"+"Error: Could not find class " + className);
					return null;
				}
			}
		} else {
			return null;
		}
	}
	/**
	 * Get MAPS version number.
	 * 
	 * @return the version
	 */
	public float getVersion() {
		return version;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		connectible = false;
		factory = new ParamHiddenFactory(this);
	}

	/**
	 * Set class.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(Class value) throws InvalidParameterValueException {
		this.className = (value!=null)?value.getCanonicalName():null;
	}

	/**
	 * Set class by canonical name.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(String value) throws InvalidParameterValueException {
		this.className = value;
	}

	/**
	 * Set MAPS version number.
	 * 
	 * @param version
	 *            the version
	 */
	public void setVersion(float version) {
		this.version = version;
	}

	/**
	 * Get class name.
	 * 
	 * @return the string
	 */
	public String toString() {
		return className;
	}

	/**
	 * Validate class.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if (className == null) {
			throw new InvalidParameterException(this);
		}
		try {
			Class.forName(className);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("jist.base"+"Class does not exist:" + className);
			throw new InvalidParameterException(this);
		}
	}
	public boolean equals(ParamModel<Class> mod){
		if(mod instanceof ParamHeader){
			ParamHeader header=(ParamHeader)mod;
//			if(this.className==null&&header.className==null){
//				if(this.uuid==null&&header.uuid==null)return true;
//				if(this.uuid==null||header.uuid==null)return false;
//				return (this.uuid.equals(header.uuid));
//			}
//			if(this.className==null&&header.className==null)return false;
			if(this.className.equals(header.className)){
//				if(this.uuid==null&&header.uuid==null)return true;
//				if(this.uuid==null||header.uuid==null)return false;
				return (this.uuid.equals(header.uuid));
			} else return false;
		} return super.equals(mod);		
	}
	
	@Override
	public String getHumanReadableDataType() {
		return "JIST INTERNAL Parameter. Report if this message is seen";
	}
	public String getXMLValue() {throw new RuntimeException("INTERNAL: Not Serializable"); };
	
	@Override
	public void setXMLValue(String arg) {
		throw new RuntimeException("INTERNAL: Not Serializable"); 
		
	};
	@Override
	public String probeDefaultValue() {
		return null;
	}
}
