package edu.jhu.ece.iacl.jist.pipeline.parser;

import java.io.File;

import javax.swing.JFileChooser;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.JistLayoutReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeExternalDestination;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeExternalSource;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmJistLayoutAdapter;

public class JistLayoutParser implements ScriptParser{

	public PipeAlgorithm openPipeAlgorithm() {
		JFileChooser loadDialog = new JFileChooser("Open JIST Layout");
		loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(JistPreferences.getPreferences().getAllValidLayoutExtensions() ));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return parsePipeAlgorithm(loadDialog.getSelectedFile());
		} else {
			return null;
		}
	}
	public PipeAlgorithm parsePipeAlgorithm(File f) {
		PipeLayout layout=PipeLayout.read(f);
		PipeAlgorithm algo = new PipeAlgorithm();
		MedicAlgorithmJistLayoutAdapter adapter = new MedicAlgorithmJistLayoutAdapter();
		ParamCollection inputParams = adapter.getInput();
		String label = FileReaderWriter.getFileName(f);
		String name = label.replaceAll(" ", "_");
		inputParams.setName(name);
		inputParams.setLabel(label);
		ParamCollection outputParams = adapter.getOutput();
		outputParams.setName(name);
		outputParams.setLabel(label);
		File location=layout.getRunParameters().getOutputDirectory();
		ParamObject<PipeLayout> scriptFile = new ParamObject<PipeLayout>("JIST Layout",new JistLayoutReaderWriter());
		scriptFile.setValue(f);
		scriptFile.setObject(layout);
		scriptFile.setHidden(true);
		inputParams.add(scriptFile);
		
		ParamBoolean outputDir = new ParamBoolean("Use Layout Output Directory",false);
		inputParams.add(outputDir);
		ParamBoolean showManager = new ParamBoolean("Show Process Manager",true);
		inputParams.add(showManager);
		for(PipeModule mod:layout.getAllDescendantPipes()){
			if(mod instanceof PipeExternalSource){
				ParamModel inval=((PipeExternalSource) mod).getOutputParam();
				inval.setName(mod.getLabel());
				inval.setValue(((PipeExternalSource) mod).getDefaultParam().getValue());
				inputParams.add(inval);
			} else if(mod instanceof PipeExternalDestination){
				ParamModel outval=((PipeExternalDestination) mod).getOutputParams().getValue(0);
				outval.setName(mod.getLabel());
				outputParams.add(outval);
			}
		}
		
		// Move the execution time parameter to end of parameters
		ParamModel execParam = outputParams.getValue(1);
		outputParams.remove(execParam);
		outputParams.add(execParam);
		algo.setAlgorithm(adapter);
		ParamCollection outputClone = outputParams.clone();
		outputClone.setHidden(true);
		inputParams.add(outputClone);
		return algo;
	}

}
