/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.factory.ParamStringFactory;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * String parameter.
 * 
 * @author Blake Lucas
 */
public class ParamString extends ParamModel<String> {

	/** The string. */
	protected String string;

	/**
	 * Constructor.
	 */
	public ParamString() {
		this("", null);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamString(String name) {
		this(name, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 * @param str
	 *            string
	 */
	public ParamString(String name, String str) {
		setName(name);
		this.string = str;
		this.factory = new ParamStringFactory(this);
	}

	/**
	 * Clone object.
	 * 
	 * @return the param string
	 */
	public ParamString clone() {
		ParamString param = new ParamString();
		if (string != null) {
			param.string = new String(string);
		}
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());		
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#compareTo(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel)
	 */
	public int compareTo(ParamModel model) {
		return (model instanceof ParamString) ? 0 : 1;
	}

	/**
	 * Get string.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return string;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(1);
		connectible = true;
		factory = new ParamStringFactory(this);
	}

	/**
	 * Set string.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(String value) throws InvalidParameterValueException {
		if (value == null) {
			throw new InvalidParameterValueException(this, "String is null");
		}
		this.string = value;
	}

	/**
	 * Get string.
	 * 
	 * @return the string
	 */
	public String toString() {
		return string;
	}

	/**
	 * validate string. String cannot be null or zero length.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if (string == null) {
			throw new InvalidParameterException(this, "String is null");
		}
		if (string.length() == 0) {
			throw new InvalidParameterException(this, "String length is zero");
		}
	}

	@Override
	public String getHumanReadableDataType() {
		return "string";
	}
	@Override
	public String getXMLValue() {
		return getValue();
	}
	@Override
	public void setXMLValue(String arg) {
		setValue(arg);
	}

	@Override
	public String probeDefaultValue() {	
		return string;
	}

	@Override
	public boolean xmlEncodeParam(Document document, Element parent) {
		super.xmlEncodeParam(document, parent);
		Element em;				
		em = document.createElement("string");		
		em.appendChild(document.createTextNode(string+""));
		parent.appendChild(em);	
		return true;
	}
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		string= JistXMLUtil.xmlReadTag(parent, "string");
	}
}
