/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.ImageIcon;

import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.PipeDestination;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;

/**
 * Factory to create destination cell modules.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeDestinationFactory extends PipeModuleFactory {
	
	/**
	 * Graph cell to represent destination.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class DestinationCell extends PipeModuleCell {
		
		/**
		 * Instantiates a new destination cell.
		 * 
		 * @param name
		 *            the name
		 * @param pipe
		 *            the pipe
		 */
		public DestinationCell(String name, PipeModule pipe) {
			super(name, pipe);
			PipeCellViewFactory.setViewClass(this.getAttributes(), PipeDestinationView.class.getCanonicalName());
			GraphConstants.setGradientColor(this.getAttributes(), Color.green.darker());
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.DefaultGraphCell#clone()
		 */
		public DestinationCell clone() {
			PipeDestinationFactory factory = new PipeDestinationFactory(pipe.getLabel(), "", pipe.getClass());
			DestinationCell cell = factory.createGraphCell();
			PipeModule pipeClone=cell.getPipeModule();
			((PipeDestination)pipe).getInputParams().getInputView().update();
			((PipeDestination)pipeClone).getInputParams().importParameter(pipe.getInputParams());
			((PipeDestination)pipeClone).getInputParams().getInputView().update();
			return cell;
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell#createPorts()
		 */
		protected void createPorts() {
			Vector<PipePort> inputPorts = pipe.getInputPorts();
			Vector<PipePort> outputPorts = pipe.getOutputPorts();
			preferredSize = new Dimension((int) Math.max(outputPorts.size() * hspace, Math.max(
					preferredSize.getWidth(), inputPorts.size() * hspace)), (int) preferredSize.getHeight());
			add(new PipeModulePort(GraphConstants.PERMILLE * (0.5), 0.10 * GraphConstants.PERMILLE, inputPorts
					.firstElement()));
		}
	}

	/**
	 * Tree node to destination.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class DestinationNode extends DraggableNode {
		
		/**
		 * Instantiates a new destination node.
		 * 
		 * @param factory
		 *            the factory
		 */
		public DestinationNode(PipeModuleFactory factory) {
			super(factory);
			this.icon=new ImageIcon(PlaceHolder.class.getResource("destination.png"));

		}
	}

	/**
	 * Instantiates a new pipe destination factory.
	 * 
	 * @param name
	 *            the name
	 * @param c
	 *            the c
	 */
	public PipeDestinationFactory(String name, String description, Class c) {
		super(name, description, c);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleFactory#createGraphCell()
	 */
	public DestinationCell createGraphCell() {
		PipeDestination p = createPipe();
		return p.createModuleCell();
	}

	/**
	 * Create module destination.
	 * 
	 * @return the pipe destination
	 */
	public PipeDestination createPipe() {
		PipeDestination p;
		try {
			p = (PipeDestination) c.newInstance();
			return p;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create tree node for destination.
	 * 
	 * @return the draggable node
	 */
	public DraggableNode createTreeNode() {
		return new DestinationNode(this);
	}
}
