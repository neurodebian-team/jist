/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamHiddenInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.parameters.ParameterException;
import gov.nih.mipav.model.scripting.parameters.ParameterFactory;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.view.dialogs.AlgorithmParameters;

/**
 * Performance parameter factory.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamPerformanceFactory extends ParamFactory {
	
	/** The param. */
	protected ParamPerformance param;

	/**
	 * Instantiates a new param performance factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamPerformanceFactory(ParamPerformance param) {
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#createMipavParameter(gov.nih.mipav.view.dialogs.AlgorithmParameters)
	 */
	public void createMipavParameter(AlgorithmParameters scriptParams) throws ParserException {
		ParamPerformance param = getParameter();
		scriptParams.getParams().put(
				ParameterFactory.newParameter(encodeName(param.getName()), encodeValue(param.toString())));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamHiddenInputView(param);
		}
		return inputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamOutputView(param);
		}
		return outputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getParameter()
	 */
	public ParamPerformance getParameter() {
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#importMipavParameter(gov.nih.mipav.model.scripting.parameters.ParameterTable)
	 */
	public void importMipavParameter(ParameterTable paramTable) throws ParameterException {
		// TODO Auto-generated method stub
	}
}
