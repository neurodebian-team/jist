/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parser;

import java.io.File;

import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;

/**
 * Interface for parsing third party script files.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public interface ScriptParser {
	
	/**
	 * Opens dialog box for user to select script file and then parses the file.
	 * 
	 * @return pipe algorithm
	 */
	public abstract PipeAlgorithm openPipeAlgorithm();

	/**
	 * Parses specified file.
	 * 
	 * @param f
	 *            script file
	 * @return pipe algorithm
	 */
	public abstract PipeAlgorithm parsePipeAlgorithm(File f);
}
