/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Input view to select multiple images.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeCollectionInputView extends ParamInputView implements ListSelectionListener, ActionListener,
		ListCellRenderer, Refreshable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4483721450186968746L;
	
	/** The add button. */
	protected JButton addButton;
	
	/** The field. */
	protected JList field;
	
	/** The image list size. */
	protected int imageListSize = -1;
	
	/** The list box entries. */
	protected DefaultListModel listBoxEntries;
	
	/** The list pane. */
	protected JPanel listPane;
	
	/** The scroll pane. */
	protected JScrollPane scrollPane;
	
	/** The up button. */
	protected JButton upButton;
	
	/** The down button. */
	protected JButton downButton;
	
	/**
	 * Default constructor.
	 * 
	 * @param param
	 *            volume collection
	 */
	public ParamVolumeCollectionInputView(ParamVolumeCollection param) {
		super(param);
		buildLabel(BorderLayout.NORTH);
		// Create list of entries that contain ParamVolume
		listBoxEntries = new DefaultListModel();
		// Create listbox entry field
		field = new JList(listBoxEntries);
		// Use custom entry renderer
		field.setCellRenderer(this);
		field.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// Create browse button to select images
		addButton = new JButton("Add");
		addButton.addActionListener(this);
		upButton = new JButton("Up");
		upButton.addActionListener(this);
		downButton = new JButton("Down");
		downButton.addActionListener(this);
		// Create scroll pane to display entries
		scrollPane = new JScrollPane();
		scrollPane.setMinimumSize(new Dimension(100, 30));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// Create list pane to layout listbox entries
		listPane = new JPanel(new BorderLayout());
		listPane.add(field, BorderLayout.CENTER);
		// Create pane to layout list pane
		JPanel smallPane = new JPanel(new BorderLayout());
		smallPane.add(listPane, BorderLayout.NORTH);
		scrollPane.setViewportView(smallPane);
		// Create list box
		// setSelected(param.getFiles());
		// Create pane to layout scroll pane and browse button
		smallPane = new JPanel(new BorderLayout(5, 5));
		smallPane.add(scrollPane, BorderLayout.CENTER);
		JPanel optButtons = new JPanel(new GridLayout(0, 3));
		optButtons.add(addButton);
		optButtons.add(upButton);
		optButtons.add(downButton);
		smallPane.add(optButtons, BorderLayout.SOUTH);
		smallPane.setPreferredSize(new Dimension(200, 150));
		add(smallPane, BorderLayout.CENTER);
		setMinimumSize(new Dimension(200, 150));
		// Create refresher to monitor for MIPAV changes
		Refresher.getInstance().add(this);
	}

	/**
	 * Select images to load when the browse button is clicked.
	 * 
	 * @param event
	 *            browse button clicked
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == addButton) {
			ArrayList<ImageData> imgs = MipavController.openFileDialog(null);
			setSelected(imgs);
		} else if (event.getSource() == upButton) {
			int index1 = field.getSelectedIndex();
			int index2 = (index1 - 1 + listBoxEntries.size()) % listBoxEntries.size();
			Object obj1 = listBoxEntries.elementAt(index1);
			Object obj2 = listBoxEntries.elementAt(index2);
			listBoxEntries.setElementAt(obj1, index2);
			listBoxEntries.setElementAt(obj2, index1);
			field.setSelectedIndex(index2);
		} else if (event.getSource() == downButton) {
			int index1 = field.getSelectedIndex();
			int index2 = (index1 + 1) % listBoxEntries.size();
			Object obj1 = listBoxEntries.elementAt(index1);
			Object obj2 = listBoxEntries.elementAt(index2);
			listBoxEntries.setElementAt(obj1, index2);
			listBoxEntries.setElementAt(obj2, index1);
			field.setSelectedIndex(index2);
		}
	}

	/**
	 * Commit changes to list.
	 */
	public void commit() {
		updateParameter();
	}

	/**
	 * Get index of image name in list box entries.
	 * 
	 * @param name
	 *            image name
	 * @return list box index
	 */
	protected int getIndexOf(String name) {
		int index = -1;
		for (int j = 0; j < listBoxEntries.size(); j++) {
			if (((ParamVolume) listBoxEntries.get(j)).equalVolume(name)) {
				index = j;
				break;
			}
		}
		return index;
	}

	/**
	 * Use a custom cell renderer that can interpret ParamVolumes.
	 * 
	 * @param list
	 *            listbox
	 * @param value
	 *            listbox entry
	 * @param index
	 *            selected index
	 * @param isSelected
	 *            is selected
	 * @param cellHasFocus
	 *            has focus
	 * @return the list cell renderer component
	 */
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		// Get the selected index. (The index parameter isn't
		// always valid, so just use the value.)
		ParamVolume param = (ParamVolume) value;
		JPanel pane = new JPanel();
		ImageData img=param.getImageData(false); //do not load if not loaded!
		JLabel listLabel;
		// Updated 10/14/2009
		if(img==null)
			listLabel = new JLabel("<html><font color='#990000'>"+param.getName()+"</font></html>");
		else
			listLabel = new JLabel("<html><font color='#990000'>"+img.getName()+"</font> ["+img.getType().toString().toLowerCase()+" | ("+img.getRows()+", "+img.getCols()+", "+img.getSlices()+", "+img.getComponents()+")]</html>");

		pane.setLayout(new BorderLayout());
		if (isSelected) {
			int[] indices=field.getSelectedIndices();
			int in=-1;
			for(int i=0;i<indices.length;i++){
				if(index==indices[i]){
					in=i;
					break;
				}
			}
			if(in!=-1){
				JLabel label;
				pane.add(label=new JLabel("["+(in+1)+"]"), BorderLayout.EAST);
				label.setHorizontalAlignment(JLabel.RIGHT);
			} 
			pane.setBackground(list.getSelectionBackground());
			pane.setForeground(list.getSelectionForeground());
		} else {
			pane.setBackground(list.getBackground());
			pane.setForeground(list.getForeground());
		}
		listLabel.setHorizontalAlignment(JLabel.LEFT);

		pane.add(listLabel, BorderLayout.CENTER);
		return pane;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView#getParameter()
	 */
	public ParamVolumeCollection getParameter() {
		return (ParamVolumeCollection) param;
	}

	/**
	 * Refresh view by rebuilding list box.
	 */
	public void refresh() {
		updateListBox(null);
	}

	/**
	 * Select the specified images in the combo box.
	 * 
	 * @param imgs
	 *            the images
	 */
	public void setSelected(List<ImageData> imgs) {
		ArrayList<String> names = new ArrayList<String>();
		if (imgs == null) {
			updateListBox(null);
		} else {
			for (ImageData img : imgs) {
				if (img != null) {
					names.add(img.getName());
				}
			}
			updateListBox(names);
		}
	}

	/**
	 * Update pane with new value from parameter.
	 */
	public void update() {
		Refresher.getInstance().pauseAll();
		// Do not process events while list box is being updated
		field.removeListSelectionListener(this);
		ParamVolumeCollection p = getParameter();

//		List<String> imageNames = p.getImageNames();
		List<ParamVolume> vols = p.getParamVolumeList();
		field.clearSelection();
		listBoxEntries.clear();
		for (ParamVolume vol : vols) {
				listBoxEntries.addElement(vol);
		}
		imageListSize = vols.size();
		// Do not process events while list box is being updated
		field.addListSelectionListener(this);
		
		setSelected(getParameter().getImageDataList());
		Refresher.getInstance().resumeAll();
	}

	/**
	 * Build list box to select multiple images.
	 * 
	 * @param items
	 *            the items
	 * @return true, if update list box
	 */
	protected boolean updateListBox(ArrayList<String> items) {
		if(imageListSize==-1){
			//If this the first time the list box is updated, check to see if parameter already contains selected images
			update();
			return true;
		}
		// Get all register images in MIPAV
		
		Vector<String> imageNames = MipavController.getImageNames();
		// Do not process events while list box is being updated
		field.removeListSelectionListener(this);

		ParamVolumeCollection p = getParameter();
		// Add images to list box if the list has changed
		if (imageNames.size() != imageListSize) {
			
			for (int i = 0; i < listBoxEntries.size(); i++) {
					listBoxEntries.removeElementAt(i--);
			}
			for (String name : imageNames) {
					listBoxEntries.addElement(p.create(name));
			}
			
			imageListSize = imageNames.size();
		} else {
			// Nothing has changed in the listbox
			if (items == null) {
				int indices[]=field.getSelectedIndices();
				field.clearSelection();
				field.setSelectedIndices(indices);
				field.addListSelectionListener(this);
				
				return false;
			}
		}
		// Get images currently in image collection
		List<ParamVolume>vols = p.getParamVolumeList();
		int indexes[] = new int[vols.size()];
		for (int i = 0; i < vols.size(); i++) {
			indexes[i] = getIndexOf(vols.get(i).getName());
		}
		
		if (items != null) {
			// Add existing selected images to selected image list
			int tmp[] = new int[items.size() + indexes.length];
			for (int i = 0; i < indexes.length; i++) {
				tmp[i] = indexes[i];
			}
			// Add specified selected images to image list
			for (int i = indexes.length; i < tmp.length; i++) {
				tmp[i] = getIndexOf(items.get(i - indexes.length));
			}
			indexes = tmp;
		}
		field.clearSelection();
		// Set selected images
		field.setSelectedIndices(indexes);
		updateParameter();
		field.addListSelectionListener(this);
		return true;
	}

	/**
	 * Update parameter value with selected items from list box.
	 */
	protected void updateParameter() {
		if(param.isConnected())return;
		Object[] items = field.getSelectedValues();
		ParamVolumeCollection p = getParameter();
		ArrayList<ParamVolume> ps = new ArrayList<ParamVolume>();

		for (Object item : items) {
			ps.add((ParamVolume) item);
		}
		boolean equals = true;
		if (p.size() != ps.size()) {
			equals = false;
		} else {
			List<File> files = p.getFileList();
			for (int i = 0; i < files.size(); i++) {
				File fs = ps.get(i).getValue();
				File f = files.get(i);
				if ((f != null) && (fs != null) && !fs.equals(f)) {
					equals = false;
					break;
				}
			}
		}
		if (!equals) {
			p.setValue(ps);
			notifyObservers(p, this);
		}
	}

	/**
	 * Update parameter when the list box selection changes.
	 * 
	 * @param event
	 *            selection changed
	 */
	public void valueChanged(ListSelectionEvent event) {
		updateParameter();

	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return field;
	}
}
