/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import javax.swing.JPanel;

import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;

// TODO: Auto-generated Javadoc
/**
 * Port that allows a Pipe Source to be a parent of another Pipe Source.
 * 
 * @author Blake Lucas
 */
public class PipeParentPort extends PipePort<Object> {
	
	/** The parameter. */
	transient protected ParamModel param = null;

	/**
	 * Parent ports are always initialized as output ports.
	 */
	public PipeParentPort() {
		super();
		setPortType(PipePort.type.OUTPUT);
	}

	/**
	 * Clone parent port.
	 * 
	 * @return the pipe parent port
	 */
	public PipeParentPort clone() {
		return new PipeParentPort();
	}

	/**
	 * Always returns "Parent".
	 * 
	 * @return the label
	 */
	public String getLabel() {
		return "Parent";
	}

	/**
	 * Always returns "parent".
	 * 
	 * @return the name
	 */
	public String getName() {
		return "parent";
	}

	/**
	 * Get source that owns this port.
	 * 
	 * @return the owner
	 */
	public PipeSource getOwner() {
		return (PipeSource) owner;
	}

	/**
	 * Get value from associated parameter.
	 * 
	 * @return the value
	 */
	public Object getValue() {
		// TODO Auto-generated method stub
		if (param != null) {
			return param.getValue();
		} else {
			return null;
		}
	}

	/**
	 * Unimplemented.
	 * 
	 * @return the view
	 */
	public JPanel getView() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Initialize port to always be a parent port.
	 */
	public void init() {
		setPortType(PipePort.type.OUTPUT);
	}

	/**
	 * Return true if other port is child port.
	 * 
	 * @param p the p
	 * 
	 * @return true, if checks if is compatible
	 */
	public boolean isCompatible(PipePort p) {
		return (p instanceof PipeChildPort);
	}

	/**
	 * Unimplemented.
	 * 
	 * @param label the label
	 */
	public void setLabel(String label) {
	}

	/**
	 * Unimplemented.
	 * 
	 * @param name the name
	 */
	public void setName(String name) {
	}

	/**
	 * Set output parameter that is associated with this parent port.
	 * 
	 * @param param parameter
	 */
	public void setParameter(ParamModel param) {
		this.param = param;
	}

	/**
	 * Unimplemented.
	 * 
	 * @param value the value
	 */
	public void setValue(Object value) {
	}

	/**
	 * Port is valid if it is connected to a child port.
	 * 
	 * @throws InvalidParameterException the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if (!isConnected()) {
			throw new InvalidParameterException(this);
		}
	}
}
