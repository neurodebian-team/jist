package edu.jhu.ece.iacl.jist.structures.image;

// TODO: Auto-generated Javadoc
/**
 * The Class test.
 */
public class test {

	/**
	 * The main method.
	 * 
	 * @param args the args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int R=100, C=100, S=100;
		ImageData foo = new ImageDataInt(R,C,S,1);
		
		System.out.println("test"+"\t"+"Non-native:");
		long tic = System.currentTimeMillis();
		double foobar = 0;
		for(int q=0;q<10;q++)
		for(int i=0;i<R;i++)
			for(int j=0;j<C;j++)
				for(int k=0;k<S;k++) {
					foobar = foo.get(i,j,k,1).doubleValue(); 
				}
		long toc = System.currentTimeMillis();
		System.out.println("test"+"\t"+"get().doubleValue(): "+(toc-tic));
		tic = System.currentTimeMillis();
		foobar = 0;
		for(int q=0;q<10;q++)
		for(int i=0;i<R;i++)
			for(int j=0;j<C;j++)
				for(int k=0;k<S;k++) {
					foobar = foo.getDouble(i,j,k,1); 
				}
		toc = System.currentTimeMillis();
		System.out.println("test"+"\t"+"getDouble(): "+(toc-tic));
		
foo = new ImageDataDouble(R,C,S,1);
		
		System.out.println("test"+"\t"+"Native conversion:");
		tic = System.currentTimeMillis();
		foobar = 0;
		for(int q=0;q<10;q++)
		for(int i=0;i<R;i++)
			for(int j=0;j<C;j++)
				for(int k=0;k<S;k++) {
					foobar = foo.get(i,j,k,1).doubleValue(); 
				}
		toc = System.currentTimeMillis();
		System.out.println("test"+"\t"+"get().doubleValue(): "+(toc-tic));
		tic = System.currentTimeMillis();
		foobar = 0;
		for(int q=0;q<10;q++)
		for(int i=0;i<R;i++)
			for(int j=0;j<C;j++)
				for(int k=0;k<S;k++) {
					foobar = foo.getDouble(i,j,k,1); 
				}
		toc = System.currentTimeMillis();
		System.out.println("test"+"\t"+"getDouble(): "+(toc-tic));
					
		
	}

}
