package edu.jhu.ece.iacl.jist.structures.geom;

import edu.jhu.ece.iacl.jist.structures.data.Indexable;

// TODO: Auto-generated Javadoc
/**
 * The Class VertexIndexed.
 */
public class VertexIndexed implements Indexable<Double>{
		
		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 1L;
		
		/** The i. */
		private int i;
		
		/** The chain. */
		private int chain;
		
		/** The index. */
		private int index;
		
		/** The value. */
		private double value;
		
		/**
		 * Instantiates a new vertex indexed.
		 * 
		 * @param value the value
		 */
		public VertexIndexed(double value){
			this.value=value;
		}
		
		/**
		 * Sets the position.
		 * 
		 * @param i the new position
		 */
		public void setPosition(int i){
			this.i=i;
		}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setIndex(int)
		 */
		public void setIndex(int index){
			this.index=index;
		}
		
		/**
		 * Sets the value.
		 * 
		 * @param val the new value
		 */
		public void setValue(double val){
			this.value=val;
		}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getValue()
		 */
		public Double getValue(){
			return value;
		}
		
		/**
		 * Gets the vertex id.
		 * 
		 * @return the vertex id
		 */
		public int getVertexId(){
			return i;
		}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getRow()
		 */
		public int getRow(){return i;}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getColumn()
		 */
		public int getColumn(){return 0;}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getSlice()
		 */
		public int getSlice(){return 0;}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getIndex()
		 */
		public int getIndex(){return index;}



		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getChainIndex()
		 */
		public int getChainIndex() {
			return chain;
		}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setChainIndex(int)
		 */
		public void setChainIndex(int chainIndex) {
			chain=chainIndex;
		}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setRefPosition(int, int, int)
		 */
		public void setRefPosition(int i, int j, int k) {
			this.i=0;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(Double val) {
			return (int)Math.signum(value-val);
		}
		
		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setValue(java.lang.Comparable)
		 */
		public void setValue(Comparable obj) {
			value=(Double)obj;
		}

	}
