package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
public class PipeSurfaceCollectionExternalSource extends PipeExternalSource{
	public PipeSurfaceCollectionExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamSurfaceCollection("Default Surfaces"));
		group.setLabel("External Surfaces");
		group.setName("extsurfs");
		group.setCategory("Externalize.Surface");
		return group;
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Surfaces");
		group.add(valParam = new ParamSurfaceCollection("Surfaces"));
		return group;
	}
}
