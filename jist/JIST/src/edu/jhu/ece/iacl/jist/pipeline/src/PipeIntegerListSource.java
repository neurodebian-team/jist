/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of integers.
 * 
 * @author Blake Lucas
 */
public class PipeIntegerListSource extends PipeListSource {
	
	/** The integer param. */
	protected ParamInteger integerParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);				
//		Element em;
//		
//		em = document.createElement("integerParam");
//		integerParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);		
		integerParam =(ParamInteger) outputParams.getFirstChildByName("Integer");// new ParamFile();
//		integerParam = new ParamInteger();
//		integerParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"integerParam"));
		getParentPort().setParameter(integerParam);
	}
	/**
	 * Default constructor.
	 */
	public PipeIntegerListSource() {
		super();
		getParentPort().setParameter(integerParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Integer File List");
		group.setName("integerlist");
		group.setCategory("Number.Integer");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Integer");
		group.add(integerParam = new ParamInteger("Integer"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamInteger getOutputParam() {
		return integerParam;
	}

	/**
	 * Get value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected Object getValue(int i) {
		if (off < data[i].length) {
			return (data[i][off]);
		} else {
			return null;
		}
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object obj) {
		integerParam.setValue((Number) obj);
	}
}
