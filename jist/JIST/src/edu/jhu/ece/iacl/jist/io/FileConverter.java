package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

// TODO: Auto-generated Javadoc
/**
 * The Class FileConverter.
 */
public class FileConverter {
	
	//static String inputFileName;
	//static String outputFileName;
	/** The image size_z. */
	static String imageSize_x, imageSize_y, imageSize_z;
	
	/** The faces. */
	static String [] vertices, faces;
	
	/** The Z. */
	static float X, Y, Z;
	
	/** The output file. */
	static File inputFile, outputFile;
	
	//Method that converts files with specified names
	//from one formats to another
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main (String[] args)
	{
		String inputFileName;
		String outputFileName;
		
		if (args.length == 5)
		{
			//get input file name and check if it exists
			inputFileName = args[0];
			inputFile = new File(inputFileName);
			boolean exists = inputFile.exists();
			if (exists != true)	
			{
				//file or directory does not exists
				System.out.print("File does not exists!");
				return;
			}	
			
			//create output file and check if it exists
			outputFileName = args[1];
			try
			{
				//outputFile = new File(outputFileName);
				// Create file if it does not exist
				outputFile = new File(outputFileName);
				boolean success = outputFile.createNewFile();
				if (success != true) 
				{
					// File already exists
					System.out.print("File already exists");
					return;
			    }
			} 
			catch (IOException e) 
			{
				System.out.println("jist.io"+"\t"+e.getMessage());
			}
			
			//Provide dimensions of the original Image (Size)
			imageSize_x = args[2];
			imageSize_y = args[3];
			imageSize_z = args[4];
			
		}
		else
		{
			//get input file name and check if it exists
			inputFileName = JOptionPane.showInputDialog("Enter input file name or path");
			inputFile = new File(inputFileName);
			boolean exists = inputFile.exists();
			if (exists != true)	
			{
				//file or directory does not exists
				System.out.print("File does not exists!");
				return;
			}
		
			//create output file and check if it exixts			
			outputFileName = JOptionPane.showInputDialog("Enter output file name or path");
			outputFile = new File(outputFileName);
			try
			{
				// Create file if it does not exist
				boolean success = outputFile.createNewFile();
				if (success != true) 
				{
					// File already exists
					System.out.print("File already exists");
					return;
			    }
			} 
			catch (IOException e) 
			{
				System.out.println("jist.io"+"\t"+e.getMessage());
			}
			
			//Select  the conversion type by entering the appropriate choice in the input dialog box
			//conversionType = JOptionPane.showInputDialog("Select an option\n1 = FreeSurferToMipav\n2 = MipavToFreeSurfer\n" +
			//"3 = DXToMipav\n4 = MipavToDX\n5 = FreeSurfeToDX\n6 = DXToFreeSurfer");				
			
			//Enter the dimensions of the original image (size)
			imageSize_x = JOptionPane.showInputDialog("Enter the x-Coordinate Value\nof the Original Image Size");
			imageSize_y = JOptionPane.showInputDialog("Enter the y-Coordinate Value\nof the Original Image Size");
			imageSize_z = JOptionPane.showInputDialog("Enter the z-Coordinate Value\nof the Original Image Size");
			
		}
		float X = Float.parseFloat(imageSize_x);
		float Y = Float.parseFloat(imageSize_y);
		float Z = Float.parseFloat(imageSize_z);
		
		int indexExt_1 = inputFileName.lastIndexOf(".");
		String inputExtension = inputFileName.substring((indexExt_1 +1));
		System.out.println("jist.io"+"\t"+inputExtension); //checking file extension
		
		int indexExt_2 = outputFileName.lastIndexOf(".");
		String outputExtension = outputFileName.substring((indexExt_2 +1));
		System.out.println("jist.io"+"\t"+outputExtension); //Checking file extension
		
		if (inputExtension.equalsIgnoreCase("xml") && outputExtension.equalsIgnoreCase("asc"))
		{
			
			vertices = MipavSurfaceXMLReader.getVertices(inputFile);
			faces  = MipavSurfaceXMLReader.getFaces(inputFile);
			FreeSurferAsciiWriter.fromMipavWriter(vertices, faces, outputFile, X, Y, Z);
			
		}
		else if (inputExtension.equalsIgnoreCase("dx") && outputExtension.equalsIgnoreCase("asc"))
		{
			vertices = DxSurfaceReader.getVertices(inputFile);
			faces  = DxSurfaceReader.getFaces(inputFile);
			FreeSurferAsciiWriter.fromDxWriter(vertices, faces, outputFile, X, Y, Z);
		}
		else if (inputExtension.equalsIgnoreCase("asc") && outputExtension.equalsIgnoreCase("dx"))
		{
			vertices = FreeSurferAsciiReader.getVertices(inputFile);
			faces  = FreeSurferAsciiReader.getFaces(inputFile);
			DxSurfaceWriter.fromFreeSurferWriter(vertices, faces, outputFile, X, Y, Z);
		}
		else if (inputExtension.equalsIgnoreCase("xml") && outputExtension.equalsIgnoreCase("dx"))
		{
			vertices = MipavSurfaceXMLReader.getVertices(inputFile);
			faces  = MipavSurfaceXMLReader.getFaces(inputFile);
			DxSurfaceWriter.fromMipavWriter(vertices, faces, outputFile, X, Y, Z);
		}
		else if (inputExtension.equalsIgnoreCase("dx") && outputExtension.equalsIgnoreCase("xml"))
		{
			vertices = DxSurfaceReader.getVertices(inputFile);
			faces  = DxSurfaceReader.getFaces(inputFile);
			MipavSurfaceXMLWriter.fromDxWriter(vertices, faces, outputFile, X, Y, Z);
		}
		else if (inputExtension.equalsIgnoreCase("asc") && outputExtension.equalsIgnoreCase("xml"))
		{
			vertices = FreeSurferAsciiReader.getVertices(inputFile);
			faces  = FreeSurferAsciiReader.getFaces(inputFile);
			MipavSurfaceXMLWriter.fromFreeSurferWriter(vertices, faces, outputFile, X, Y, Z);
		}
//		else if (inputExtension.equalsIgnoreCase("vtk"))
//		{
//			EmbeddedSurface surf = SurfaceVtkReaderWriter.getInstance().readObject(inputFile);
//			SurfaceReaderWriter.getInstance().write(surf, outputFile);
//		}
		else return;
		
		System.exit(0);
	}

}
