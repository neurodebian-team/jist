/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through all surfaces in file.
 * 
 * @author Blake Lucas
 */
public class PipeSurfaceListSource extends PipeListSource {
	
	/** The surf param. */
	protected ParamSurface surfParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("surfParam");
//		surfParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		surfParam = (ParamSurface)outputParams.getFirstChildByName("Surface");
//		surfParam = new ParamSurface();
//		surfParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"surfParam"));
		getParentPort().setParameter(surfParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipeSurfaceListSource() {
		super();
		getParentPort().setParameter(surfParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Surface File List");
		group.setName("surflist");
		group.setCategory("Surface");
		return group;
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Surface");
		group.add(surfParam = new ParamSurface("Surface"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamSurface getOutputParam() {
		return surfParam;
	}

	/**
	 * Get value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected File getValue(int i) {
		if (off < data[i].length) {
			return new File((String) data[i][off]);
		} else {
			return null;
		}
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object obj) {
		surfParam.setValue((File) obj);
	}
}
