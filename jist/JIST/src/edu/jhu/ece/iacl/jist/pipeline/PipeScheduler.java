/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext.Status;
import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext.Update;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.view.MipavUtil;

// TODO: Auto-generated Javadoc
/**
 * Pipe scheduler creates and schedules the execution of execution contexts.
 * 
 * @author Blake Lucas
 */
public class PipeScheduler implements Runnable,
ExecutionContext.ContextListener {

	/**
	 * Checks if is using worker.
	 * 
	 * @return true, if is using worker
	 */
	public boolean isUsingWorker() {
		return useWorker;
	}

	/**
	 * Sets the use worker.
	 * 
	 * @param useWorker the new use worker
	 */
	public void setUseWorker(boolean useWorker) {
		this.useWorker = useWorker;
	}

	/**
	 * The Class DestinationProcessor.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class DestinationProcessor extends SwingWorker<Void, Void> {

		/** The progress monitor. */
		ProgressMonitor pmonitor;

		/**
		 * Instantiates a new destination processor.
		 */
		public DestinationProcessor() {
		}

		/**
		 * Process destinations in background.
		 * 
		 * @return always returns null
		 * 
		 * @throws Exception 		 */
		protected Void doInBackground() {
			try {

				pmonitor = new ProgressMonitor(null, "Processing Destinations",
						"Processing...", 0, 0);
				processDestinations(pmonitor);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * Close progress monitor.
		 */
		public void done() {
			pmonitor.close();
			pmonitor = null;
		}

		/**
		 * Process destinations.
		 * 
		 * @param pmonitor the pmonitor
		 */
		protected void processDestinations(ProgressMonitor pmonitor) {			
			JistLogger.logOutput(JistLogger.INFO, "Processing destinations...");
			for (PipeSource src : srcRoots) {
				src.reset();
			}
			for (PipeDestination dest : dests) {
				dest.setCurrentLayout(layout);
				dest.reset();
			}
			boolean notReady = false;
			pmonitor.setMaximum(contextTable.size());
			int progress = 0;
			// for (Vector<ExecutionContext> row : contextTable) {
			JistLogger.logOutput(JistLogger.INFO, "Validating contexts...");
			for (int i = 0; i < contextTable.size(); i++) {
				JistLogger.logOutput(JistLogger.FINE, "For next context row: "+i);
				Vector<ExecutionContext> row = contextTable.get(i);
				if (pmonitor.isCanceled()) {
					break;
				}
				notReady = false;
				// for (ExecutionContext context : row) {
				for (int ii = 0; ii < row.size(); ii++) {
					ExecutionContext context = row.get(ii);
					if (context.getStatus() == Status.COMPLETED) {
						context.loadContextInputParams();
						context.loadContextOutputParams();
						context.getAlgorithm().setCurrentContext(context);
					} else {
						notReady = true;
					}
				}
				if (notReady) {
					continue;
				}
				pmonitor.setProgress(++progress);


				for (PipeDestination dest : dests) {
					JistLogger.logOutput(JistLogger.FINE, "Pulling destinations...");
					dest.pull(pmonitor);

					JistLogger.logOutput(JistLogger.FINE, "Completing destinations...");
					dest.complete();

					JistLogger.logOutput(JistLogger.FINE, "Save resources...");
					dest.getOutputParams().saveResources(
							layout.getRunParameters().getOutputDirectory(),false);

					JistLogger.logOutput(JistLogger.FINE, "Clean up: dispose...");
					dest.release();

					JistLogger.logOutput(JistLogger.FINE, "Next iteration...");



				} 
				//				JistLogger.logOutput(JistLogger.FINE, "Clean up: algorithms...");
				//				for (int ii = 0; ii < row.size(); ii++) {
				//					ExecutionContext context = row.get(ii);
				//					if (context.getStatus() == Status.COMPLETED) {			
				//						context.getAlgorithm().getOutputParams().dispose();
				//						context.getAlgorithm().setCurrentContext(null);
				//					} else {
				//						notReady = true;
				//					}
				//				}
				// Need 2 steps because multiple ports could connect to the same input
				//				for (PipeDestination dest : dests) {
				//					JistLogger.logOutput(JistLogger.FINE, "Clean up: releasePull...");
				//					dest.releasePull(pmonitor);
				//				}

				iterate(srcRoots);
			}
			JistLogger.logOutput(JistLogger.FINE, "Finished processing destinations...");
			JistLogger.logFlush();
						for (PipeDestination dest : dests) {
							dest.complete();
							dest.getOutputParams().saveResources(
									layout.getRunParameters().getOutputDirectory(),false);
							dest.getOutputParams().dispose(); // Clean up where not needed.
						}
						JistLogger.logOutput(JistLogger.FINE, "Finished saving destinations resources...");
						JistLogger.logFlush();
		}
	}

	/** pipeline layout to schedule. */
	protected PipeLayout layout;

	/** Queue to store contexts that are waiting to run. */
	protected Vector<ExecutionContext> waitingQueue;

	/** The waiting queue hash. */
	protected HashMap waitingQueueHash;

	/** Queue to store contexts that are currently running. */
	protected LinkedList<ExecutionContext> runningQueue;

	/** Queue to store completed contexts. */
	protected LinkedList<ExecutionContext> completedQueue;

	/** List storing all contexts. */
	protected LinkedList<ExecutionContext> schedule;

	/** Reference to process manager. */
	private ProcessManager parentFrame;

	/** Semaphore for locking queue. */
	protected boolean refreshQueuesLock = false;

	/** Thread for running a process. */
	protected Thread th;

	/** The th lock. */
	protected Object thLock = null;

	/** Table of dependent contexts for each scheduled context. */
	protected Vector<Vector<ExecutionContext>> contextTable;

	/** Flag indicating a request to stop the running process. */
	protected boolean stopped = false;

	/** The pause count. */
	protected int pauseCount = 0;

	/** Flag indicating a request to update input/output information about the experiments. */
	protected boolean requestTreeUpdate = false;

	/** Flag indicating the scheduler is initializing. */
	protected boolean initializing = false;

	/** Counter to indicate the number of initialized experiments. */
	protected int initializedCount = 0;

	/** Flag indicates the scheduler is running. */
	protected boolean runningFlag = false;

	/** List of all source roots. */
	protected Vector<PipeSource> srcRoots;

	/** List of all destinations. */
	protected Vector<PipeDestination> dests;

	/** Number of root nodes. */
	int rootCount = 0;

	/** Use worker to process destination. */
	protected boolean useWorker = true;

	/** indicate that scheduler has completed running and processing destinations. */
	protected boolean completed = false;

	/** Global ID. */
	protected String uuid;

	private boolean overrideRunOutOfProcess = false;
	public void setOverrideRunOutOfProcess(boolean val) {
		overrideRunOutOfProcess=val;
	}

	/**
	 * Constructor.
	 * 
	 * @param layout Layout to schedule
	 * @param parentFrame Parent frame that monitors scheduler
	 */
	public PipeScheduler(PipeLayout layout, ProcessManager parentFrame) {
		thLock = new Object();
		runningQueue = new LinkedList<ExecutionContext>();
		waitingQueue = new Vector<ExecutionContext>();
		waitingQueueHash = new HashMap();
		completedQueue = new LinkedList<ExecutionContext>();
		schedule = new LinkedList<ExecutionContext>();
		this.parentFrame = parentFrame;
		this.layout = layout;
		this.uuid = UUID.randomUUID().toString();

		this.contextTable = new Vector<Vector<ExecutionContext>>();
		layout.setScheduler(this);
	}

	/**
	 * Gets the uUID.
	 * 
	 * @return the uUID
	 */
	public String getUUID() {
		return uuid;
	}

	/**
	 * Create dependent contexts for specified parent contexts.
	 * 
	 * @param algoRoots parent contexts
	 * @param iter the iteration
	 */
	protected/* synchronized */void addDependencyContexts(
			Vector<PipeAlgorithm> algoRoots, int iter) {
		ExecutionContext context;
		Vector<PipeAlgorithm> queue = new Vector<PipeAlgorithm>();
		queue.addAll(algoRoots);
		// Perform breadth-first traversal of all trees starting at roots
		while (queue.size() > 0) {			
			PipeAlgorithm algo = queue.remove(0);			
			JistLogger.logOutput(JistLogger.FINE, "PipeScheduler added algorithm:"+algo.getName()+" (total size: "+queue.size()+")");
			// Check if parents of this algorithm have been traversed and
			// initialized yet
			if (isInputReady(algo)) {
				// System.out.println(getClass().getCanonicalName()+"\t"+queue.size()+")"+algo.getLabel());
				// Check if scenario currently exists
				context = doesScenarioExist(algo); // TODO : keep a list
				if (context != null) {
					// System.out.println(getClass().getCanonicalName()+"\t"+"USE EXISTIG FOR
					// "+context.getContextName()+"
					// "+context.getAlgorithm().getLabel());
					// Use existing context instead of creating new one
					useExistingContext(context);
				} else {
					// Create new context
					context = createNewContext(algo, iter);
				}
				// Add children to queue
				for (PipeModule child : algo.getChildren()) {
					if (child instanceof PipeAlgorithm) {
						if (!queue.contains(child)) { // TODO : keep a list
							queue.add((PipeAlgorithm) child);
						}
					}
				}
			} else {
				// Parents of algorithm have not been traversed, place node back
				// into queue
				queue.add(algo);
			}
		}
	}

	/**
	 * Clean output directory for specified contexts.
	 * 
	 * @param contexts the contexts
	 * @param forceRemoveLock the force remove lock
	 */
	public/* synchronized */void clean(Vector<ExecutionContext> contexts,
			boolean forceRemoveLock) {
		Collections.sort(contexts);
		// for (ExecutionContext context : contexts) {
		for (int i = 0; i < contexts.size(); i++) {
			ExecutionContext context = contexts.get(i);
			// context.clean(forceRemoveLock);
			context.clean();
		}
		// for (ExecutionContext context : getExecutionContexts()) {
		Vector<ExecutionContext> ec = getExecutionContexts();
		for (int i = 0; i < ec.size(); i++) {
			ExecutionContext context = ec.get(i);
			context.updateStatus();
		}
		// for (ExecutionContext context : contexts) {
		for (int i = 0; i < contexts.size(); i++) {
			ExecutionContext context = contexts.get(i);
			clean(context.getDependents(), forceRemoveLock);
		}
	}

	/**
	 * Kill all running processes. This is a blocking operation.
	 */
	/**
	 * Clean all experiments in output directory
	 */
	public void cleanAll() {
		CleanAllDialog dialog = new CleanAllDialog();
	}

	public void cleanAll(boolean quiet) {
		if(quiet) {
			cleanAll();
		} else {
			CleanAllDialog dialog = new CleanAllDialog(false);
			dialog.cleanNow();
			//		dialog.dispose();
		}
	}

	/**
	 * The Class CleanAllDialog.
	 */
	protected class CleanAllDialog extends JDialog implements ActionListener {

		/** The cancel button. */
		JButton okButton, cancelButton;

		/** The text area. */
		JTextArea textArea;

		/** The yes. */
		boolean yes = false;

		/** The delete list. */
		Vector<File> deleteList = new Vector<File>();

		/**
		 * Instantiates a new clean all dialog.
		 */
		public CleanAllDialog() {
			this(true);
		}

		public CleanAllDialog(boolean vis) {
			super();
			String text = init();
			JPanel myPanel = new JPanel(new BorderLayout());
			okButton = new JButton("Yes");
			okButton.addActionListener(this);
			okButton.setMinimumSize(MipavUtil.defaultButtonSize);
			okButton.setPreferredSize(MipavUtil.defaultButtonSize);
			cancelButton = new JButton("No");
			cancelButton.setMinimumSize(MipavUtil.defaultButtonSize);
			cancelButton.setPreferredSize(MipavUtil.defaultButtonSize);
			cancelButton.addActionListener(this);
			JPanel buttonPanel = new JPanel();
			buttonPanel.add(okButton);
			buttonPanel.add(cancelButton);
			myPanel.add(buttonPanel, BorderLayout.SOUTH);

			textArea = new JTextArea(10, 10);
			textArea.setText(text);
			textArea.setEditable(false);
			JScrollPane spane = new JScrollPane();
			spane.setViewportView(textArea);
			myPanel
			.add(
					new JLabel(
							"Are you sure you want to delete the contents of the following directories?"),
							BorderLayout.NORTH);
			myPanel.add(spane, BorderLayout.CENTER);
			myPanel.setBorder(BorderFactory.createTitledBorder("Clean All"));
			getContentPane().add(myPanel);
			this.setTitle("Warning: Attempting to clean output directory.");
			pack();
			setVisible(vis);
		}

		/**
		 * Inits the.
		 * 
		 * @return the string
		 */
		public String init() {
			stop();
			killAll();

			File rootDir = layout.getRunParameters().getOutputDirectory();
			File[] files = rootDir.listFiles();

			StringBuffer deleteString = new StringBuffer();
			for (File f : files) {
				if (f.isDirectory()) {
					String dirName = f.getName();
					int in = dirName.indexOf("exp-");
					if (in == 0) {
						deleteList.add(f);
						deleteString.append(f.getAbsolutePath() + "\n");
					}
				}
			}
			return deleteString.toString();
		}

		/**
		 * Clean.
		 */
		public void clean() {
			DeleteDirectoriesWorker worker = new DeleteDirectoriesWorker();
			worker.execute();		
		}

		/**
		 * Clean.
		 */
		public void cleanNow() {
			DeleteDirectoriesWorker worker = new DeleteDirectoriesWorker();
			worker.run();			
		}

		/**
		 * The Class DeleteDirectoriesWorker.
		 */
		protected class DeleteDirectoriesWorker extends SwingWorker<Void, Void> {

			/** The progress monitor. */
			ProgressMonitor pmonitor;

			/**
			 * Instantiates a new destination processor.
			 */
			public DeleteDirectoriesWorker() {
			}

			/**
			 * Delete directories in the output directory in background with a process monitor
			 * 
			 * @return always returns null
			 * 
			 * @throws Exception 			 */
			protected Void doInBackground() {
				refreshQueuesLock = true;
				initializing = true;

				initializedCount = 0;
				try {

					pmonitor = new ProgressMonitor(null,
							"Deleting contents of "
							+ layout.getRunParameters()
							.getOutputDirectory(),
							"Deleting...", 0, deleteList.size());
					pmonitor.setMillisToDecideToPopup(500);	
					int count=0;
					for (File f : deleteList) {
						if (pmonitor.isCanceled())
							break;

						ExecutionContext.deleteDir(f, pmonitor);
						pmonitor.setProgress(count++);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				LinkedList<ExecutionContext> allContexts = new LinkedList<ExecutionContext>();
				allContexts.addAll(completedQueue);
				allContexts.addAll(runningQueue);
				allContexts.addAll(waitingQueue);
				for (int i = 0; i < allContexts.size(); i++) {
					if (pmonitor.isCanceled())
						break;
					ExecutionContext context = allContexts.get(i);
					context.reset();
				}
				pmonitor.setMinimum(0);
				pmonitor.setMaximum(allContexts.size());
				for (int i = 0; i < allContexts.size(); i++) {
					if (pmonitor.isCanceled())
						break;
					initializedCount++;
					ExecutionContext context = allContexts.get(i);
					pmonitor.setNote("Resetting experiment "
							+ context.getContextName());
					pmonitor.setProgress(i);
					context.resetStatus();
				}

				return null;
			}

			/**
			 * Close progress monitor.
			 */
			public void done() {
				pmonitor.close();
				pmonitor = null;
				refreshQueuesLock = false;
				refreshQueues();
				requestTreeUpdate = true;
				initializing = false;
				parentFrame.updateStatusBar();
			}
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if (okButton == e.getSource()) {
				clean();
				setVisible(false);
			} else if (cancelButton == e.getSource()) {
				setVisible(false);

			}
		}
	}

	/**
	 * Update queues if a context's status changes.
	 * 
	 * @param context the context
	 * @param type the type
	 */
	public void contextUpdate(ExecutionContext context, Update type) {
		if (type == Update.Status) {
			if (!initializing) {
				refreshQueues();
			}
			context.updateAllTreeNodes();
			requestTreeUpdate = true;
		}
	}

	/**
	 * Create new context for specific algorithm.
	 * 
	 * @param algo algorithm
	 * @param iter experiment number id
	 * 
	 * @return new execution context
	 */
	protected/* synchronized */ExecutionContext createNewContext(
			PipeAlgorithm algo, int iter) {
		ExecutionContext context = null;
		String contextId;
		if (algo.isRoot()) {
			/*
			 * if(layout.getRunParameters().getNamingConvention()==NamingConvention
			 * .UNIVERSAL_ID){ String
			 * uuid=algo.getInputParams().getHeader().getUUID(); if(uuid!=null){
			 * contextId =
			 * uuid.substring(uuid.length()-12,uuid.length()).toUpperCase(); }
			 * else { char suffix = (char) ('A' + rootCount % 26);
			 * contextId=((rootCount >= 26) ? (1 + (rootCount / 26)) + ""+
			 * suffix : "" + suffix); } } else {
			 */
			char suffix = (char) ('A' + rootCount % 26);
			contextId = ((rootCount >= 26) ? (1 + (rootCount / 26)) + ""
					+ suffix : "" + suffix);
			// }
			context = new ExecutionContext(iter, contextId, layout, algo);
			if(overrideRunOutOfProcess)
				context.getAlgorithm().getAlgorithm().setRunningInSeparateProcess(false);
			waitingQueue.add(context);
			waitingQueueHash.put(context.getHashKey(), context);
			rootCount++;
			algo.setCurrentContext(context);
			context.addContextListener(this);
		} else {
			Vector<ExecutionContext> parentContexts = algo
			.getCurrentParentContexts();
			// Use first parent context to create context name for child
			ExecutionContext parentContext = parentContexts.firstElement();
			/*
			 * if(layout.getRunParameters().getNamingConvention()==NamingConvention
			 * .UNIVERSAL_ID){ String
			 * uuid=algo.getInputParams().getHeader().getUUID(); if(uuid!=null){
			 * contextId =
			 * uuid.substring(uuid.length()-12,uuid.length()).toUpperCase(); }
			 * else { contextId = createUniqueName(parentContext); } } else {
			 */
			contextId = createUniqueName(parentContext);
			// }
			int scenarioId = parentContext.getScenarioId();
			waitingQueue.add(context = new ExecutionContext(scenarioId,
					contextId, layout, algo));
			if(overrideRunOutOfProcess)
				context.getAlgorithm().getAlgorithm().setRunningInSeparateProcess(false);
			waitingQueueHash.put(context.getHashKey(), context);
			context.addContextListener(this);
			// for (ExecutionContext parent : parentContexts) {
			for (int i = 0; i < parentContexts.size(); i++) {
				ExecutionContext parent = parentContexts.get(i);
				context.addParent(parent);
			}
			algo.setCurrentContext(context);
		}
		if (context != null) {
			// Context created, add to manager table
			parentFrame.managerPane.getTableModel().rows.add(context);
			// Help override default run out of process

		}

		return context;
	}

	/**
	 * Create unique name for specified context. It is necessary that contexts
	 * have a unique name.
	 * 
	 * @param parentContext parent context
	 * 
	 * @return the string
	 */
	public/* synchronized */String createUniqueName(ExecutionContext parentContext) {
		int instanceCount = 1;
		String label = parentContext.getContextId() + "A";
		for (int i = 0; i < waitingQueue.size(); i++) {
			ExecutionContext p = waitingQueue.get(i);
			if ((p.getScenarioId() == parentContext.getScenarioId())
					&& p.getContextId().equals(label)) {
				char suffix = (char) ('A' + instanceCount % 26);
				label = parentContext.getContextId()
				+ ((instanceCount > 26) ? (1 + (instanceCount / 26))
						+ "" + suffix : "" + suffix);
				instanceCount++;
				i = -1;
			}
		}
		return label;
	}

	/**
	 * Demote position of processes in queue.
	 * 
	 * @param contexts the contexts
	 */
	public/* synchronized */void demote(Vector<ExecutionContext> contexts) {
		{
			// for (ExecutionContext context : contexts) {
			for (int i = 0; i < contexts.size(); i++) {
				ExecutionContext context = contexts.get(i);
				context.demote();
			}

			Collections.sort(waitingQueue);
		}
	}

	/**
	 * Remove and process context in waiting queue.
	 * 
	 * @return the execution context
	 */
	protected/* synchronized */ExecutionContext dequeue() {
		// /*synchronized*/ (thLock)
		{
			if (!stopped) {
				refreshQueues();
				Collections.sort(schedule);
				if (runningQueue.size() < layout.getRunParameters()
						.getMaxProcs()) {
					ExecutionContext context = (schedule.size() > 0) ? schedule
							.getFirst() : null;
							if ((context != null)
									&& (context.updateStatus() == Status.READY)) {
								schedule.remove();
								// Do not allow duplicates of a context in the running
								// queue
								if (!runningQueue.contains(context))
									runningQueue.add(context);
								return context;
							}
				}
			}
		}
		return null;
	}

	/**
	 * Dispose of scheduler by killing all current processes.
	 */
	public void dispose() {
		this.killAll();
	}

	/**
	 * Find existing context with the same parameters as the algorithm module if
	 * one exists.
	 * 
	 * @param algo Algorithm module
	 * 
	 * @return Existing context
	 */
	protected/* synchronized */ExecutionContext doesScenarioExist(
			PipeAlgorithm algo) {
		int i = 0;
		ParamCollection currentParams = algo.getInputParams();
		Class c = (algo).getAlgorithmClass();
		if (c == null)
			return null;
		ExecutionContext foundContext = null;
		String key = ExecutionContext.getHashKey(algo.getLabel(), c,
				currentParams);
		// System.out.println(getClass().getCanonicalName()+"\t"+key);
		foundContext = (ExecutionContext) waitingQueueHash.get(key);

		// for (ExecutionContext context : waitingQueue) {
		/*
		 * for(int ii=0;ii<waitingQueue.size();ii++) { ExecutionContext context
		 * = waitingQueue.get(ii); if
		 * (context.getAlgorithm().getLabel().equals(algo.getLabel()) &&
		 * c.equals(context.getAlgorithm().getAlgorithmClass())) { if
		 * (context.getContextInputParams().getFactory().equals(currentParams))
		 * { foundContext = context; break; } } }
		 */
		// Make sure that the parents are the same.
		if (foundContext != null) {
			Vector<ExecutionContext> foundParentContext = foundContext
			.getCallers();
			for (PipeModule parent : algo.getParents()) {
				if (parent instanceof PipeAlgorithm) {
					ExecutionContext context = parent.getCurrentContext();
					if (context == null) {
						return null;
					}
					if (!foundParentContext.contains(context)) {
						return null;
					}
				}
			}
		}
		return foundContext;
	}

	/**
	 * Enqueue all waiting processes.
	 */
	public/* synchronized */void enqueue() {
		schedule.clear();
		enqueue(waitingQueue);
	}

	/**
	 * Enqueue processes in queue.
	 * 
	 * @param contexts the contexts
	 */
	public/* synchronized */void enqueue(Vector<ExecutionContext> contexts) {
		// try {
		// for (ExecutionContext context : contexts) {
		for (int i = 0; i < contexts.size(); i++) {
			ExecutionContext context = contexts.get(i);
			if (!schedule.contains(context)) {
				schedule.add(context);
			}
		}
	}

	/**
	 * Get the number of completed experiments.
	 * 
	 * @return number of completed experiments
	 */
	public/* synchronized */int getCompletedCount() {
		int count = completedQueue.size();
		// for (ExecutionContext context : waitingQueue) {
		for (int i = 0; i < waitingQueue.size(); i++) {
			ExecutionContext context = waitingQueue.get(i);
			if (context.getStatus() == Status.COMPLETED) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Returns all execution contexts in queues.
	 * 
	 * @return the execution contexts
	 */
	private/* synchronized */Vector<ExecutionContext> getExecutionContexts() {
		Vector<ExecutionContext> allContext = new Vector<ExecutionContext>();
		allContext.addAll(completedQueue);
		allContext.addAll(runningQueue);
		allContext.addAll(waitingQueue);
		return allContext;
	}

	/**
	 * Get number of failed experiments.
	 * 
	 * @return number of failed experiments
	 */
	public/* synchronized */int getFailedCount() {
		int count = 0;		
		// for (ExecutionContext context : completedQueue) {
		for (int i = 0; i < completedQueue.size(); i++) {
			try {
				ExecutionContext context = completedQueue.get(i);

				if (context.getStatus() == Status.FAILED) {
					count++;
				}
			}catch(Exception e) {}

		}
		return count;
	}

	/**
	 * Get the total number of initialized experiments.
	 * 
	 * @return number of contexts
	 */
	public int getInitializedCount() {
		return initializedCount;
	}

	/**
	 * Get number of experiments that are not ready to run.
	 * 
	 * @return number of not ready experiments
	 */
	public/* synchronized */int getNotReadyCount() {
		int count = 0;
		// for (ExecutionContext context : waitingQueue) {
		for (int i = 0; i < waitingQueue.size(); i++) {
			ExecutionContext context = waitingQueue.get(i);
			if (context.getStatus() == Status.NOT_READY) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Get number of queued waiting to run experiments.
	 * 
	 * @return number of queued experiments
	 */
	public/* synchronized */int getQueuedCount() {
		return schedule.size();
	}

	/**
	 * Return queue position based on status and priority.
	 * 
	 * @param context the context
	 * 
	 * @return the queue position
	 */
	public/* synchronized */int getQueuePosition(ExecutionContext context) {
		Status stat = context.getStatus();
		if ((stat == Status.COMPLETED) || (stat == Status.FAILED) || (stat == Status.OUT_OF_SYNC)) {
			return -(completedQueue.indexOf(context) + 1);
		} else if ((stat == Status.READY) || (stat == Status.NOT_READY)) {
			int index = 0;
			for (int i = 0; i < waitingQueue.size(); i++) {
				ExecutionContext cont = waitingQueue.get(i);
				if (cont == context) {
					return index + 1;
				}
				index++;
			}
		}
		return 0;
	}

	/**
	 * Get number of experiments that are ready to run.
	 * 
	 * @return number of ready experiments
	 */
	public/* synchronized */int getReadyCount() {
		int count = 0;
		// for (ExecutionContext context : waitingQueue) {
		for (int i = 0; i < waitingQueue.size(); i++) {
			ExecutionContext context = waitingQueue.get(i);
			if (context.getStatus() == Status.READY) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Get number of running experiments.
	 * 
	 * @return number of running experiments
	 */
	public/* synchronized */int getRunningCount() {
		return runningQueue.size();
	}

	/**
	 * Get running queue for the scheduler.
	 * 
	 * @return the running queue
	 */
	public/* synchronized */LinkedList<ExecutionContext> getRunningQueue() {

		LinkedList<ExecutionContext> lightClone = new LinkedList<ExecutionContext>();
		for (int i = 0; i < runningQueue.size(); i++)
			lightClone.add(runningQueue.get(i));
		return lightClone;

	}

	/**
	 * Checks for running queue.
	 * 
	 * @return true, if successful
	 */
	public boolean hasRunningQueue() {
		return runningQueue != null;
	}

	/**
	 * Get number of waiting to run experiments.
	 * 
	 * @return number of waiting to run experiments
	 */
	public/* synchronized */int getWaitingCount() {
		return waitingQueue.size();
	}

	/**
	 * Generate list of contexts parsed on sources.
	 */
	public/* synchronized */void init() {
		System.err.println(getClass().getCanonicalName()+"PipeScheduler: init.");
		System.err.flush();
		Refresher.getInstance().pauseAll();
		initializing = true;
		initializedCount = 0;
		// Initialize queues
		runningQueue.clear();
		waitingQueue.clear();
		completedQueue.clear();
		schedule.clear();
		srcRoots = new Vector<PipeSource>();
		dests = new Vector<PipeDestination>();
		/** Progress monitor to appear when loading a layout. */
		ProgressMonitor progressMonitor = new ProgressMonitor(parentFrame,
				"Initializing Experiments", "Initializing...", 0, 0);
		progressMonitor.setMillisToDecideToPopup(1000);
		Vector<PipeAlgorithm> algoRoots = new Vector<PipeAlgorithm>();
		Vector<PipeAlgorithm> algos = new Vector<PipeAlgorithm>();
		Vector<PipeSource> srcs = new Vector<PipeSource>();
		// Find source and algorithm roots
		for (PipeModule mod : layout.getAllLeafPipes()) {
			if (mod.isRoot()) {
				if (mod instanceof PipeSource) {
					srcRoots.add((PipeSource) mod);
				} else if (mod instanceof PipeAlgorithm) {
					algoRoots.add((PipeAlgorithm) mod);
				}
			}
			if (mod instanceof PipeDestination) {
				dests.add((PipeDestination) mod);
			} else if (mod instanceof PipeAlgorithm) {
				algos.add((PipeAlgorithm) mod);
			} else if (mod instanceof PipeSource) {
				srcs.add((PipeSource) mod);
			}
			if (progressMonitor.isCanceled()) {
				break;
			}
		}
		int iter = 0;
		if (!progressMonitor.isCanceled()) {
			// Initialize all sources to their first value
			for (PipeSource src : srcRoots) {
				src.reset();
			}

			// Clear table that displays information on the context
			parentFrame.managerPane.getTableModel().rows.clear();
			// Create new contexts
			contextTable.clear();
			progressMonitor.setMaximum(waitingQueue.size());
			progressMonitor.setMaximum(1);
		}
		do {
			if (progressMonitor.isCanceled()) {
				break;
			}
			rootCount = 0;
			// System.out.print("Experiment " + iter + " ");
			// for (PipeSource src : srcs) {
			// System.out.print("[" + src.getOutputPorts().get(0).toString()
			// + "]");
			// }

			progressMonitor.setNote("Found " + (iter + 1) + " Experiments");
			progressMonitor.setProgress(0);

			/*
			 * System.out.println(getClass().getCanonicalName()+"\t"+""); System.out.flush();
			 */
			for (PipeAlgorithm algo : algos) {
				algo.setCurrentContext(null);
			}
			// Add all child contexts that depend on the root context
			// System.gc();
			// long mem1 =
			// Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
			addDependencyContexts(algoRoots, iter);
			// System.gc();
			// long mem2 =
			// Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
			// System.out.println(getClass().getCanonicalName()+"\t"+mem1+"\t"+mem2+"\t"+(mem2-mem1));
			requestTreeUpdate = true;
			// parentFrame.managerPane.getTableModel().fireTableDataChanged();
			// Keep track of dependencies
			Vector<ExecutionContext> contextRow = new Vector<ExecutionContext>();
			for (PipeAlgorithm mod : algos) {
				ExecutionContext con = mod.getCurrentContext();
				if (con != null) {
					contextRow.add(con);
				}
			}
			contextTable.add(contextRow);
			// Thread this method so table has an opportunity to update
			/*
			 * try { Thread.sleep(50); } catch (InterruptedException e) {
			 * e.printStackTrace(); }
			 */
			iter++;
			initializedCount = iter;

		} while (iterate(srcRoots));
		// Sort waiting queue by priority
		Collections.sort(waitingQueue);
		refreshQueuesLock = true;
		// Determine current status of context
		iter = 0;
		progressMonitor.setMinimum(0);
		progressMonitor.setMaximum(waitingQueue.size());
		int progress = 0;
		progressMonitor.setProgress(0);
		// for (ExecutionContext cont : waitingQueue) {
		for (int i = 0; i < waitingQueue.size(); i++) {
			ExecutionContext cont = waitingQueue.get(i);
			if (progressMonitor.isCanceled()) {
				break;
			}
			cont.resetStatus();
			progressMonitor.setProgress(progress++);
			progressMonitor.setNote("Experiment " + cont.getContextShortName()
					+ ": " + cont.getAlgorithm().getLabel());
		}

		refreshQueuesLock = false;
		Refresher.getInstance().resumeAll();
		// Refresh queues to reflect current status of contexts
		if (progressMonitor.isCanceled()) {
			ProcessManager.getInstance().close();
			initializedCount = 0;
			initializing = false;
			requestTreeUpdate = false;
		} else {
			refreshQueues();
			requestTreeUpdate = true;
			initializing = false;
		}
		progressMonitor.close();
	}

	/**
	 * Insert experiment into proper queue based on experiment status.
	 * 
	 * @param context experiment
	 */
	protected/* synchronized */void insertIntoProperQueue(
			ExecutionContext context) {
		switch (context.getStatus()) {
		case NOT_READY:
		case READY:
			if (!waitingQueue.contains(context)) {
				waitingQueue.add(context);
				waitingQueueHash.put(context.getHashKey(), context);
			}
			break;
		case RUNNING:
			if (!runningQueue.contains(context)) {
				runningQueue.add(context);
			}
			break;
		case OUT_OF_SYNC:
		case COMPLETED:
		case FAILED:
			if (!completedQueue.contains(context)) {
				completedQueue.add(context);
			}
			break;
		}
	}

	/**
	 * Indicates the scheduler is initializing.
	 * 
	 * @return true if initializing
	 */
	public boolean isInitializing() {
		return initializing;
	}

	/**
	 * Check that parent contexts are available before adding to queue.
	 * 
	 * @param child algorithm to check
	 * 
	 * @return true if algorithm is ready
	 */
	private/* synchronized */boolean isInputReady(PipeAlgorithm child) {
		// Check that parent contexts are available before adding to queue
		for (PipeModule parent : child.getParents()) {
			if ((parent instanceof PipeAlgorithm)
					&& (parent.getCurrentContext() == null)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Indicates that the scheduler is running.
	 * 
	 * @return true if the scheduler is running
	 */
	public boolean isRunning() {
		return runningFlag;
	}

	/**
	 * Iterate through all values for sources.
	 * 
	 * @param srcRoots source roots to iterate
	 * 
	 * @return true if there are more items to iterate
	 */
	protected/* synchronized */boolean iterate(Vector<PipeSource> srcRoots) {
		boolean allReset = true;
		for (PipeSource src : srcRoots) {
			if (!src.iterate()) {
				src.reset();
				src.isReset = true;
			}
			if (!src.isReset) {
				allReset = false;
			}
		}
		if (allReset) {
			for (PipeSource src : srcRoots) {
				src.reset();
			}
			return false;
		}
		return true;
	}

	/**
	 * Returns true if sources have move elements.
	 * 
	 * @return true, if checks for next
	 */
	protected/* synchronized */boolean hasNext() {
		if (srcRoots.size() == 0)
			return true;
		for (PipeSource src : srcRoots) {
			if (src.hasNext()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Kill all running processes. This is a blocking operation.
	 */
	public/* synchronized */void killAll() {
		stop();
		while (runningQueue.size() > 0) {
			ExecutionContext context = runningQueue.remove();
			context.stop();
			waitingQueue.add(context);
		}
		parentFrame.updateStatusBar();
	}

	/**
	 * Pausing the scheduler will prevent new execution contexts from starting
	 * or completing.
	 */
	public void pause() {
		// System.err.println(getClass().getCanonicalName()+"PipeScheduler: Waiting to pause.");
		// /*synchronized*/ (thLock)
		{
			// System.err.println(getClass().getCanonicalName()+"PipeScheduler: Paused.");
			pauseCount++;
			stopped = true;
		}
	}

	/**
	 * Process all destinations for this scheduler.
	 */
	public void processDestinations() {
		DestinationProcessor worker = new DestinationProcessor();

		if (useWorker) {
			worker.execute();
		} else {
			worker.doInBackground();
		}
	}

	/**
	 * Promote position of processes in queue.
	 * 
	 * @param contexts the contexts
	 */
	public/* synchronized */void promote(Vector<ExecutionContext> contexts) {
		System.err.println(getClass().getCanonicalName()+"PipeScheduler: Waiting to promote.");
		System.err.flush();
		// /*synchronized*/ (thLock)
		{
			System.err.println(getClass().getCanonicalName()+"PipeScheduler: promote.");
			System.err.flush();
			// for (ExecutionContext context : contexts) {
			for (int i = 0; i < contexts.size(); i++) {
				ExecutionContext context = contexts.get(i);
				context.promote();
			}
			Collections.sort(waitingQueue);
		}
	}

	/**
	 * Refresh queues based on current status of context.
	 */
	protected void refreshQueues() {
		//		
		// too much output
		// System.err.println(getClass().getCanonicalName()+"PipeScheduler: Waiting to refreshQueues.");System.err.flush();
		// /*synchronized*/ (thLock)
		if (refreshQueuesLock) { // need the lock semaphore so that this is
			// non-blocking
			return;
		}
		refreshQueuesLock = true;
		{
			// System.err.println(getClass().getCanonicalName()+"PipeScheduler: refreshQueues.");System.err.flush();
			// for (ExecutionContext context : completedQueue) {
			for (int i = 0; i < completedQueue.size(); i++) {
				ExecutionContext context = completedQueue.get(i);
				context.updateStatus();
			}
			// for (ExecutionContext context : runningQueue) {
			for (int i = 0; i < runningQueue.size(); i++) {
				ExecutionContext context = runningQueue.get(i);
				context.updateStatus();
			}
			// for (ExecutionContext context : waitingQueue) {
			for (int i = 0; i < waitingQueue.size(); i++) {
				ExecutionContext context = waitingQueue.get(i);
				context.updateStatus();
			}
			for (int i = 0; i < completedQueue.size(); i++) {
				ExecutionContext context = completedQueue.get(i);
				Status stat = context.getStatus();
				if ((stat != Status.COMPLETED) && (stat != Status.FAILED) && (stat != Status.OUT_OF_SYNC)) {
					completedQueue.remove(i--);
					insertIntoProperQueue(context);
				}
			}
			for (int i = 0; i < runningQueue.size(); i++) {
				ExecutionContext context = runningQueue.get(i);
				Status stat = context.getStatus();
				if (stat != Status.RUNNING) {
					runningQueue.remove(i--);
					insertIntoProperQueue(context);
				}
			}
			for (int i = 0; i < waitingQueue.size(); i++) {
				ExecutionContext context = waitingQueue.get(i);
				Status stat = context.getStatus();
				if ((stat != Status.NOT_READY) && (stat != Status.READY)) {
					waitingQueue.remove(i--);
					waitingQueueHash.remove(context.getHashKey());
					insertIntoProperQueue(context);
				}
			}
			Collections.sort(waitingQueue);
		}
		refreshQueuesLock = false;
	}

	/**
	 * Restart specified contexts.
	 * 
	 * @param contexts the contexts
	 */
	public void restart(Vector<ExecutionContext> contexts) {
		stop(contexts);
		clean(contexts, false);
		run(contexts);
	}

	/**
	 * Resume the scheduler.
	 */
	public void resume() {
		// System.err.println(getClass().getCanonicalName()+"PipeScheduler: Waiting to resume.");
		// /*synchronized*/ (thLock)
		{
			// System.err.println(getClass().getCanonicalName()+"PipeScheduler: Resume.");
			pauseCount--;
			if (pauseCount <= 0) {
				stopped = false;
				pauseCount = 0;
			}
		}

	}

	/**
	 * Schedule and run all experiments.
	 */
	public void run() {
		completed = false;
		runningFlag = true;
		// /*synchronized*/ (thLock)
		{
			pauseCount = 0;
			stopped = false;
		}
		do {
			ExecutionContext context = null;
			{
				context = dequeue();
			}

			if (context != null) {
				if(overrideRunOutOfProcess && context.getAlgorithm()!=null) 
					context.getAlgorithm().getAlgorithm().setRunningInSeparateProcess(false);
				context.start();
				// Wait for context to start running
				while (context.getStatus() == Status.READY) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						runningFlag = false;
						System.err
						.println("PipeScheduler run stopped by interrupted (inner loop).");
						System.err.flush();
						break;
					}
				}
			}
			if ((runningQueue.size() == 0) && (getReadyCount() == 0)) {
				break;
			}
			try {
				if (runningFlag)
					Thread.sleep(500);
			} catch (InterruptedException e) {
				runningFlag = false;
				System.err
				.println("PipeScheduler run stopped by interrupted (outer loop).");
				System.err.flush();
			}
		} while (runningFlag
				&& ((runningQueue.size() > 0) || (schedule.size() > 0)));
		// /*synchronized*/ (thLock) { //prevent start, stop while finishing...
		// runningFlag = false;
		{
			schedule.clear();
		}

		processDestinations();
		completed = true;
		System.err.println(getClass().getCanonicalName()+"PipeScheduler finished naturally.");
		System.err.flush();
		stop();
		// th=null;
		// }
	}

	/**
	 * Checks if is completed.
	 * 
	 * @return true, if is completed
	 */
	public boolean isCompleted() {
		return (!runningFlag && completed);
	}

	/**
	 * Enqueue and run processes.
	 * 
	 * @param contexts the contexts
	 */
	public void run(Vector<ExecutionContext> contexts) {
		// Warn if there are any FAILED/OUT_OF_SYNC
		boolean unClean = false;
		for(ExecutionContext context:contexts){
			if(context.getStatus()==Status.OUT_OF_SYNC
					||context.getStatus()==Status.FAILED){
				unClean=true;
			}
		}
		if(unClean){
			int n = JOptionPane.showOptionDialog(this.parentFrame, 
					"Some experiments are either out-of-sync or failed.\n" +
					"You must perform a clean operation to remove the old results\n" +
					"before a new run may be started.",
					null, JOptionPane.OK_OPTION, JOptionPane.WARNING_MESSAGE,
					null, null, null);

		}

		enqueue(contexts);
		if (!isRunning())
			start();
	}

	/**
	 * Start scheduler thread.
	 */
	public synchronized void start() {
		// System.err.println(getClass().getCanonicalName()+"Waiting to start PipeScheduler");
		// /*synchronized*/ (thLock)
		{
			// System.err.println(getClass().getCanonicalName()+"Starting PipeScheduler");
			if (runningFlag) {
				System.err.println(getClass().getCanonicalName()+"PipeScheduler already started. Ignoring.");
				return;
			}

			if (th == null) {
				runningFlag = true;
				th = new Thread(this);
				th.setName("Process Scheduler");
				th.start();
			} else {
				System.err
				.print("PipeScheduler: Starting a new thread before the last thread has been cleaned up");
			}
		}
		// System.err.println(getClass().getCanonicalName()+"Started PipeScheduler");
	}

	/**
	 * Stop scheduler thread. This is a blocking operation.
	 */
	public synchronized void stop() {
		// System.err.println(getClass().getCanonicalName()+"Waiting to stop PipeScheduler");System.err.flush();
		// /*synchronized*/ (thLock)
		{
			// System.err.println(getClass().getCanonicalName()+"Stopping PipeScheduler");System.err.flush();
			if (!isRunning()) {
				System.err.println(getClass().getCanonicalName()+"PipeScheduler : Already stopped.");
				return;
			}
			// System.err.println(getClass().getCanonicalName()+"PipeScheduler : Stopping.");
			System.err.flush();
			runningFlag = false;
			stopped = false;

			try {
				// th.join();
				// th.join(100);
				if (th.isAlive()) {
					th.interrupt();
					th.join(200);
					if (th.isAlive()) {
						System.err.println(getClass().getCanonicalName()+"PipeScheduler join failed.");
						System.err.flush();
					}
				}
			} catch (InterruptedException e) {
				System.err.println(getClass().getCanonicalName()+"PipeScheduler stop interrupted.");
				// System.err.println(getClass().getCanonicalName()+e.getMessage());
				System.err.flush();
			} finally {

				parentFrame.updateStatusBar();
				th = null;
				// System.err.println(getClass().getCanonicalName()+"Stopped PipeScheduler");System.err.flush();
			}

		}
	}

	/**
	 * Stop running specified contexts.
	 * 
	 * @param contexts the contexts
	 */
	public void stop(Vector<ExecutionContext> contexts) {
		// for (ExecutionContext context : contexts) {
		for (int i = 0; i < contexts.size(); i++) {
			try {
				ExecutionContext context = contexts.get(i);
				context.stop();
			}catch(Exception e) {}
		}
	}

	/**
	 * Update table describing.
	 */
	public void updateManagerTable() {
		if (requestTreeUpdate) {
			try {
				requestTreeUpdate = false;
				parentFrame.managerPane.getTableModel().fireTableDataChanged();				
			} catch (Exception e) {				
				JistLogger.logError(JistLogger.INFO, "Could not update manager view");
			}
		}
	}

	/**
	 * Load existing context into graph instead of creating a new one. This will
	 * also load all descendants that are also redundant.
	 * 
	 * @param c context
	 */
	protected void useExistingContext(ExecutionContext c) {
		if (c.getAlgorithm().getCurrentContext() == null) {
			c.getAlgorithm().setCurrentContext(c);
		}
	}

	/**
	 * Gets the elapsed cpu time.
	 * 
	 * @return the elapsed cpu time
	 */
	public long getElapsedCpuTime() {
		long elapsedTime = 0;
		try{
			// for(ExecutionContext context:completedQueue){
			for (int i = 0; i < completedQueue.size(); i++) {
				ExecutionContext context = completedQueue.get(i);
				elapsedTime += context.getCpuTime();
			}
			// for(ExecutionContext context:runningQueue){
			for (int i = 0; i < runningQueue.size(); i++) {
				ExecutionContext context = runningQueue.get(i);
				elapsedTime += context.getActualTime();
			}
		}catch(Exception e) {}
		return elapsedTime;
	}

	/**
	 * Gets the elapsed actual time.
	 * 
	 * @return the elapsed actual time
	 */
	public long getElapsedActualTime() {
		long elapsedTime = 0;
		try{
			// for(ExecutionContext context:completedQueue){
			for (int i = 0; i < completedQueue.size(); i++) {
				ExecutionContext context = completedQueue.get(i);
				elapsedTime += context.getActualTime();
			}
			// for(ExecutionContext context:runningQueue){
			for (int i = 0; i < runningQueue.size(); i++) {
				ExecutionContext context = runningQueue.get(i);
				elapsedTime += context.getActualTime();
			}
		}catch(Exception e) {}
		return elapsedTime;
	}
}
