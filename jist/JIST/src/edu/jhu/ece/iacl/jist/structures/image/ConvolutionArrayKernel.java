package edu.jhu.ece.iacl.jist.structures.image;
// TODO: Auto-generated Javadoc

/**
 * Array kernel interface.
 * 
 * @author Blake Lucas
 */
public interface ConvolutionArrayKernel {
	
	/**
	 * Gets the array.
	 * 
	 * @return the array
	 */
	public double[] getArray();
}
