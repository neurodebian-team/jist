package edu.jhu.ece.iacl.jist.structures.geom;

import java.util.HashSet;
import java.util.LinkedList;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;

import edu.jhu.ece.iacl.jist.algorithms.graphics.GeomUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class EmbeddedPointSet.
 */
public class EmbeddedPointSet {
	
	/** The points. */
	protected Point3f[] points;
	//Tetrahedron indexes
	/** The indexes. */
	protected int[] indexes;
	//Tetrahedron adjacencies
	/** The adjacencies. */
	protected int[] adjacencies;
	//Embedded Point Data
	/** The point data. */
	protected double[][] pointData;
	//Embedded Cell Data
	/** The cell data. */
	protected double[][] cellData;
	
	/** The name. */
	protected String name;
	
	/**
	 * Instantiates a new embedded point set.
	 */
	public EmbeddedPointSet(){
		
	}
	
	/**
	 * Gets the point count.
	 * 
	 * @return the point count
	 */
	public int getPointCount(){
		return points.length;
	}
	
	/**
	 * Gets the index count.
	 * 
	 * @return the index count
	 */
	public int getIndexCount(){
		return indexes.length;
	}
	
	/**
	 * Gets the tetrahedra count.
	 * 
	 * @return the tetrahedra count
	 */
	public int getTetrahedraCount(){
		return indexes.length/4;
	}
	
	/**
	 * Instantiates a new embedded point set.
	 * 
	 * @param pts the pts
	 */
	public EmbeddedPointSet(Point3f[] pts){
		this.points=pts;
	}
	
	/**
	 * Instantiates a new embedded point set.
	 * 
	 * @param pts the pts
	 * @param indexes the indexes
	 */
	public EmbeddedPointSet(Point3f[] pts,int[] indexes){
		this.indexes=indexes;
	}
	
	/**
	 * Instantiates a new embedded point set.
	 * 
	 * @param pts the pts
	 * @param indexes the indexes
	 * @param adjacencies the adjacencies
	 */
	public EmbeddedPointSet(Point3f[] pts,int[] indexes,int[] adjacencies){
		this.adjacencies=adjacencies;
	}
	
	/**
	 * Gets the points.
	 * 
	 * @return the points
	 */
	public Point3f[] getPoints() {
		return points;
	}
	
	/**
	 * Gets the point.
	 * 
	 * @param i the i
	 * 
	 * @return the point
	 */
	public Point3f getPoint(int i){
		return points[i];
	}
	
	/**
	 * Gets the tetrahedron.
	 * 
	 * @param i the i
	 * 
	 * @return the tetrahedron
	 */
	public int[] getTetrahedron(int i){
		i*=4;
		return new int[]{indexes[i],indexes[i+1],indexes[i+2],indexes[i+3]};
	}
	
	/**
	 * Gets the tetrahedron.
	 * 
	 * @param i the i
	 * @param tetra the tetra
	 * 
	 * @return the tetrahedron
	 */
	public void getTetrahedron(int i,int[] tetra){
		i*=4;
		for(int k=0;k<4;k++){
			tetra[k]=indexes[i+k];
		}
	}
	
	/**
	 * Gets the volume.
	 * 
	 * @return the volume
	 */
	public double getVolume(){
		int tetraCount=getTetrahedraCount();
		double vol=0;
		for(int i=0;i<tetraCount;i++){
			vol+=Math.abs(getVolume(i));
		}
		return vol;
	}
	
	/**
	 * Gets the volume.
	 * 
	 * @param i the i
	 * 
	 * @return the volume
	 */
	public double getVolume(int i){
		i*=4;
		Point3f rkV0=getPoint(indexes[i]);
		Point3f rkV1=getPoint(indexes[i+1]);
		Point3f rkV2=getPoint(indexes[i+2]);
		Point3f rkV3=getPoint(indexes[i+3]);
		
	    double fX1 = rkV0.x - rkV1.x;
	    double fY1 = rkV0.y - rkV1.y;
	    double fZ1 = rkV0.z - rkV1.z;
	    
	    double fX2 = rkV1.x - rkV2.x;
	    double fY2 = rkV1.y - rkV2.y;
	    double fZ2 = rkV1.z - rkV2.z;
	    
	    double fX3 = rkV2.x - rkV3.x;
	    double fY3 = rkV2.y - rkV3.y;
	    double fZ3 = rkV2.z - rkV3.z;
	    double fDet3 = -GeomUtil.determinant(fX1,fY1,fZ1,fX2,fY2,fZ2,fX3,fY3,fZ3);
	    return fDet3/6;
	}
	
	/**
	 * Sets the points.
	 * 
	 * @param points the new points
	 */
	public void setPoints(Point3f[] points) {
		this.points = points;
	}
	
	/**
	 * Gets the indexes.
	 * 
	 * @return the indexes
	 */
	public int[] getIndexes() {
		return indexes;
	}
	
	/**
	 * Sets the indexes.
	 * 
	 * @param indexes the new indexes
	 */
	public void setIndexes(int[] indexes) {
		this.indexes = indexes;
	}
	
	/**
	 * Gets the adjacencies.
	 * 
	 * @return the adjacencies
	 */
	public int[] getAdjacencies() {
		return adjacencies;
	}
	
	/**
	 * Sets the adjacencies.
	 * 
	 * @param adjacencies the new adjacencies
	 */
	public void setAdjacencies(int[] adjacencies) {
		this.adjacencies = adjacencies;
	}
	
	/**
	 * Gets the point data.
	 * 
	 * @return the point data
	 */
	public double[][] getPointData() {
		return pointData;
	}
	
	/**
	 * Gets the point data.
	 * 
	 * @param i the i
	 * 
	 * @return the point data
	 */
	public double[] getPointData(int i){
		return pointData[i]; 
	}
	
	/**
	 * Gets the point data.
	 * 
	 * @param i the i
	 * @param j the j
	 * 
	 * @return the point data
	 */
	public double getPointData(int i,int j){
		return pointData[i][j]; 
	}
	
	/**
	 * Sets the point data.
	 * 
	 * @param data the new point data
	 */
	public void setPointData(double[][] data) {
		this.pointData = data;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the cell data.
	 * 
	 * @return the cell data
	 */
	public double[][] getCellData() {
		return cellData;
	}
	
	/**
	 * Sets the cell data.
	 * 
	 * @param cellData the new cell data
	 */
	public void setCellData(double[][] cellData) {
		this.cellData = cellData;
	}
	
	/**
	 * Gets the hull.
	 * 
	 * @return the hull
	 */
	public EmbeddedSurface getHull(){
	    int riTQuantity = 0;
	    // Count the number of triangles that are not shared by two tetrahedra.
	    int i, iAdjQuantity = getIndexCount();
	    int[] m_aiAdjacent=getAdjacencies();
	    int[] m_aiIndex=getIndexes();
	    for (i = 0; i < iAdjQuantity; i++)
	    {
	        if (m_aiAdjacent[i] == -1){
	            riTQuantity++;
	        }
	    }
	    if (riTQuantity == 0){
	    	System.out.println(getClass().getCanonicalName()+"\t"+"NO VALID TRIANGLES, CANNOT CONSTRUCT HULL");
	        return null;
	    }
	    // Enumerate the triangles.
	    int[] raiIndex = new int[3*riTQuantity];
	    int k=0;
	    Point3f[] pts=getPoints();
	    Point3f[] verts=new Point3f[riTQuantity];
	    HashSet<Integer> hash=new HashSet<Integer>(riTQuantity,0.75f);
	    int r;
	    for (i = 0; i < iAdjQuantity; i++){
	        if (m_aiAdjacent[i] == -1){
	            int iTetra = i/4, iFace = i%4;
	            for (int j = 0; j < 4; j++){
	                if (j != iFace){
	                	raiIndex[k++]=r=m_aiIndex[4*iTetra+j];
	                	hash.add(r);
	                }
	            }
	            //Reverse direction
	            if ((iFace % 2) == 0){
	                int iSave = raiIndex[k-1];
	                raiIndex[k-1] = raiIndex[k-2];
	                raiIndex[k-2] = iSave;
	            }
	        }
	    }
	    k=0;
	    Point3f[] newPts=new Point3f[hash.size()];
	    for(int key:hash){
		    for(i=0;i<raiIndex.length;i++){
		    	if(key==raiIndex[i]){
		    		raiIndex[i]=k;
		    	}
		    }
		    newPts[k++]=pts[key];
	    }
	    EmbeddedSurface surf=new EmbeddedSurface(newPts,raiIndex);
	    if(getName()!=null){
	    	surf.setName(getName()+"_hull");
	    }
	    return surf;
	}
	
	/**
	 * The Class PointLocatorResult.
	 */
	public class PointLocatorResult{
	    
    	/** The intersect history. */
    	public LinkedList<Integer> intersectHistory=new LinkedList<Integer>();
	    
    	/** The last face v0. */
    	public int lastFaceV0 = -1;
	    
    	/** The last face v1. */
    	public int lastFaceV1 = -1;
	    
    	/** The last face v2. */
    	public int lastFaceV2 = -1;
	    
    	/** The last face opposite. */
    	public int lastFaceOpposite = -1;
	    
    	/** The last face opposite index. */
    	public int lastFaceOppositeIndex = -1;		
	}
	
	/** The presult. */
	PointLocatorResult presult;
	
	/**
	 * Gets the locator result.
	 * 
	 * @return the locator result
	 */
	public PointLocatorResult getLocatorResult(){
		return presult;
	}
	
	/**
	 * Gets the containing tetrahedron.
	 * 
	 * @param rkP the rk p
	 * 
	 * @return the containing tetrahedron
	 */
	public int getContainingTetrahedron(Point3f rkP){
	    // start at first tetrahedron in mesh
	    int iIndex = (presult!=null)?presult.intersectHistory.getLast():0;
	    presult=new PointLocatorResult();
	    int m_iSimplexQuantity=getTetrahedraCount();
	    // use tetrahedron faces as binary separating planes
	    int[] aiV =new int[4];
	    for (int i = 0; i < m_iSimplexQuantity; i++)
	    {
	    	presult.intersectHistory.add(iIndex);
	        getTetrahedron(i,aiV);

	        // <V1,V2,V3> counterclockwise when viewed outside tetrahedron
	        if (GeomUtil.relationToPlane(rkP,points[aiV[1]],points[aiV[2]],points[aiV[3]]) > 0)
	        {
	            iIndex = adjacencies[4*iIndex];
	            if (iIndex == -1)
	            {
	            	presult.lastFaceV0 = aiV[1];
	            	presult.lastFaceV1 = aiV[2];
	            	presult.lastFaceV2 = aiV[3];
	            	presult.lastFaceOpposite = aiV[0];
	            	presult.lastFaceOppositeIndex = 0;
	                return -1;
	            }
	            continue;
	        }

	        // <V0,V3,V2> counterclockwise when viewed outside tetrahedron
	        if (GeomUtil.relationToPlane(rkP,points[aiV[0]],points[aiV[2]],points[aiV[3]]) < 0)
	        {
	            iIndex = adjacencies[4*iIndex+1];
	            if (iIndex == -1)
	            {
	            	presult.lastFaceV0 = aiV[0];
	            	presult.lastFaceV1 = aiV[2];
	            	presult.lastFaceV2 = aiV[3];
	            	presult.lastFaceOpposite = aiV[1];
	            	presult.lastFaceOppositeIndex = 1;
	                return -1;
	            }
	            continue;
	        }

	        // <V0,V1,V3> counterclockwise when viewed outside tetrahedron
	        if (GeomUtil.relationToPlane(rkP,points[aiV[0]],points[aiV[1]],points[aiV[3]]) > 0)
	        {
	            iIndex = adjacencies[4*iIndex+2];
	            if (iIndex == -1)
	            {
	            	presult.lastFaceV0 = aiV[0];
	            	presult.lastFaceV1 = aiV[1];
	            	presult.lastFaceV2 = aiV[3];
	            	presult.lastFaceOpposite = aiV[2];
	            	presult.lastFaceOppositeIndex = 2;
	                return -1;
	            }
	            continue;
	        }

	        // <V0,V2,V1> counterclockwise when viewed outside tetrahedron
	        if (GeomUtil.relationToPlane(rkP,points[aiV[0]],points[aiV[1]],points[aiV[2]]) < 0)
	        {
	            iIndex = adjacencies[4*iIndex+3];
	            if (iIndex == -1)
	            {
	            	presult.lastFaceV0 = aiV[0];
	            	presult.lastFaceV1 = aiV[1];
	            	presult.lastFaceV2 = aiV[2];
	            	presult.lastFaceOpposite = aiV[3];
	            	presult.lastFaceOppositeIndex = 3;
	                return -1;
	            }
	            continue;
	        }

	        presult.lastFaceV0 = -1;
	        presult.lastFaceV1 = -1;
	        presult.lastFaceV2 = -1;
	        presult.lastFaceOppositeIndex = -1;
	        return iIndex;
	    }

	    return -1;
	}
	//Get barycentrics for specified tetrahedron
	/**
	 * Gets the barycentrics.
	 * 
	 * @param pt the pt
	 * @param id the id
	 * 
	 * @return the barycentrics
	 */
	public Vector4d getBarycentrics(Point3f pt,int id){
	    // Compute the vectors relative to V3 of the tetrahedron.
		id*=4;
		Point3f rkV0=points[indexes[id]];
		Point3f rkV1=points[indexes[id+1]];
		Point3f rkV2=points[indexes[id+2]];
		Point3f rkV3=points[indexes[id+3]];
		Vector4d afBary=new Vector4d();
		Vector3f akDiff[]=new Vector3f[4];
		akDiff[0]=new Vector3f();
		akDiff[0].sub(rkV0,rkV3);
		
		akDiff[1]=new Vector3f();
		akDiff[1].sub(rkV1,rkV3);
		
		akDiff[2]=new Vector3f();
		akDiff[2].sub(rkV2,rkV3);
		
		akDiff[3]=new Vector3f();
		akDiff[3].sub(pt,rkV3);


	    // If the vertices have large magnitude, the linear system of
	    // equations for computing barycentric coordinates can be
	    // ill-conditioned.  To avoid this, uniformly scale the tetrahedron
	    // edges to be of order 1.  The scaling of all differences does not
	    // change the barycentric coordinates.
	    double fMax = (double)0.0;
	    int i;
	    for (i = 0; i < 3; i++)
	    {
            fMax=Math.max(fMax,Math.abs(akDiff[i].x));
            fMax=Math.max(fMax,Math.abs(akDiff[i].y));
            fMax=Math.max(fMax,Math.abs(akDiff[i].z));
	    }

	    // Scale down only large data.
	    double fInvMax = (double)0.0;
	    if (fMax > (double)1.0)
	    {
	        fInvMax = ((double)1.0)/fMax;
	        for (i = 0; i < 4; i++)
	        {
	            akDiff[i].scale((float)fInvMax);
	        }
	    }

	    Vector3f kE1cE2 = new Vector3f();kE1cE2.cross(akDiff[1],akDiff[2]);
	    Vector3f kE2cE0 = new Vector3f();kE2cE0.cross(akDiff[2],akDiff[0]);
	    Vector3f kE0cE1 = new Vector3f();kE0cE1.cross(akDiff[0],akDiff[1]);
	    Vector3f tmp=new Vector3f();
	    tmp.cross(akDiff[1],akDiff[2]);
	    double fDet = akDiff[0].dot(tmp);
	    if (Math.abs(fDet) > 0)
	    {
	        double fInvDet = ((double)1.0)/fDet;
	        afBary.x = akDiff[3].dot(kE1cE2)*fInvDet;
	        afBary.y = akDiff[3].dot(kE2cE0)*fInvDet;
	        afBary.z = akDiff[3].dot(kE0cE1)*fInvDet;
	        afBary.w = (double)1.0 - afBary.x - afBary.y - afBary.z;
	    }
	    else
	    {
	        // The tetrahedron is potentially flat.  Determine the face of
	        // maximum area and compute barycentric coordinates with respect
	        // to that face.
	        Vector3f kE02 = new Vector3f();kE02.sub(rkV0,rkV2);
	        Vector3f kE12 = new Vector3f();kE12.sub(rkV1,rkV2);
	        if (fInvMax != (double)0.0)
	        {
	            kE02.scale((float)fInvMax);
	            kE12.scale((float)fInvMax);
	        }

	        Vector3f kE02cE12 = new Vector3f();kE02cE12.cross(kE02,kE12);
	        double fMaxSqrArea = kE02cE12.lengthSquared();
	        int iMaxIndex = 3;
	        double fSqrArea = kE0cE1.lengthSquared();
	        if (fSqrArea > fMaxSqrArea)
	        {
	            iMaxIndex = 0;
	            fMaxSqrArea = fSqrArea;
	        }
	        fSqrArea = kE1cE2.lengthSquared();
	        if (fSqrArea > fMaxSqrArea)
	        {
	            iMaxIndex = 1;
	            fMaxSqrArea = fSqrArea;
	        }
	        fSqrArea = kE2cE0.lengthSquared();
	        if (fSqrArea > fMaxSqrArea)
	        {
	            iMaxIndex = 2;
	            fMaxSqrArea = fSqrArea;
	        }

	        if (fMaxSqrArea > 0)
	        {
	            double fInvSqrArea = ((double)1.0)/fMaxSqrArea;
	            Vector3f kTmp=new Vector3f();
	            if (iMaxIndex == 0)
	            {
	                kTmp.cross(akDiff[3],akDiff[1]);
	                afBary.x = kE0cE1.dot(kTmp)*fInvSqrArea;
	                kTmp.cross(akDiff[0],akDiff[3]);
	                afBary.y = kE0cE1.dot(kTmp)*fInvSqrArea;
	                afBary.z = (double)0.0;
	                afBary.w = (double)1.0 - afBary.x - afBary.y;
	            }
	            else if (iMaxIndex == 1)
	            {
	                afBary.x = (double)0.0;
	                kTmp.cross(akDiff[3],akDiff[2]);
	                afBary.y = kE1cE2.dot(kTmp)*fInvSqrArea;
	                kTmp.cross(akDiff[1],akDiff[3]);
	                afBary.z = kE1cE2.dot(kTmp)*fInvSqrArea;
	                afBary.w = (double)1.0 - afBary.y - afBary.z;
	            }
	            else if (iMaxIndex == 2)
	            {
	                kTmp.cross(akDiff[2],akDiff[3]);
	                afBary.x = kE2cE0.dot(kTmp)*fInvSqrArea;
	                afBary.y = (double)0.0;
	                kTmp.cross(akDiff[3],akDiff[0]);
	                afBary.z = kE2cE0.dot(kTmp)*fInvSqrArea;
	                afBary.w = (double)1.0 - afBary.x - afBary.z;
	            }
	            else
	            {
	                akDiff[3].sub(pt, rkV2);
	                if (fInvMax != (double)0.0)
	                {
	                    akDiff[3].scale((float) fInvMax);
	                }

	                kTmp.cross(akDiff[3],kE12);
	                afBary.x = kE02cE12.dot(kTmp)*fInvSqrArea;
	                kTmp.cross(kE02,akDiff[3]);
	                afBary.y = kE02cE12.dot(kTmp)*fInvSqrArea;
	                afBary.z = (double)1.0 - afBary.x - afBary.y;
	                afBary.w = (double)0.0;
	            }
	        }
	        else
	        {
	            // The tetrahedron is potentially a sliver.  Determine the edge of
	            // maximum length and compute barycentric coordinates with respect
	            // to that edge.
	            double fMaxSqrLength = akDiff[0].lengthSquared();
	            iMaxIndex = 0;  // <V0,V3>
	            double fSqrLength = akDiff[1].lengthSquared();
	            if (fSqrLength > fMaxSqrLength)
	            {
	                iMaxIndex = 1;  // <V1,V3>
	                fMaxSqrLength = fSqrLength;
	            }
	            fSqrLength = akDiff[2].lengthSquared();
	            if (fSqrLength > fMaxSqrLength)
	            {
	                iMaxIndex = 2;  // <V2,V3>
	                fMaxSqrLength = fSqrLength;
	            }
	            fSqrLength = kE02.lengthSquared();
	            if (fSqrLength > fMaxSqrLength)
	            {
	                iMaxIndex = 3;  // <V0,V2>
	                fMaxSqrLength = fSqrLength;
	            }
	            fSqrLength = kE12.lengthSquared();
	            if (fSqrLength > fMaxSqrLength)
	            {
	                iMaxIndex = 4;  // <V1,V2>
	                fMaxSqrLength = fSqrLength;
	            }
	            Vector3f kE01 = new Vector3f();kE01.sub(rkV0, rkV1);
	            fSqrLength = kE01.lengthSquared();
	            if (fSqrLength > fMaxSqrLength)
	            {
	                iMaxIndex = 5;  // <V0,V1>
	                fMaxSqrLength = fSqrLength;
	            }

	            if (fMaxSqrLength > 0)
	            {
	                double fInvSqrLength = ((double)1.0)/fMaxSqrLength;
	                if (iMaxIndex == 0)
	                {
	                    // P-V3 = t*(V0-V3)
	                    afBary.x = akDiff[3].dot(akDiff[0])*fInvSqrLength;
	                    afBary.y = (double)0.0;
	                    afBary.z = (double)0.0;
	                    afBary.w = (double)1.0 - afBary.x;
	                }
	                else if (iMaxIndex == 1)
	                {
	                    // P-V3 = t*(V1-V3)
	                    afBary.x = (double)0.0;
	                    afBary.y = akDiff[3].dot(akDiff[1])*fInvSqrLength;
	                    afBary.z = (double)0.0;
	                    afBary.w = (double)1.0 - afBary.y;
	                }
	                else if (iMaxIndex == 2)
	                {
	                    // P-V3 = t*(V2-V3)
	                    afBary.x = (double)0.0;
	                    afBary.y = (double)0.0;
	                    afBary.z = akDiff[3].dot(akDiff[2])*fInvSqrLength;
	                    afBary.w = (double)1.0 - afBary.z;
	                }
	                else if (iMaxIndex == 3)
	                {
	                    // P-V2 = t*(V0-V2)
	                    akDiff[3].sub(pt,rkV2);
	                    if (fInvMax != (double)0.0)
	                    {
	                        akDiff[3].scale((float)fInvMax);
	                    }

	                    afBary.x = akDiff[3].dot(kE02)*fInvSqrLength;
	                    afBary.y = (double)0.0;
	                    afBary.z = (double)1.0 - afBary.x;
	                    afBary.w = (double)0.0;
	                }
	                else if (iMaxIndex == 4)
	                {
	                    // P-V2 = t*(V1-V2)
	                    akDiff[3].sub(pt,rkV2);
	                    if (fInvMax != (double)0.0)
	                    {
	                        akDiff[3].scale((float)fInvMax);
	                    }

	                    afBary.x = (double)0.0;
	                    afBary.y = akDiff[3].dot(kE12)*fInvSqrLength;
	                    afBary.z = (double)1.0 - afBary.y;
	                    afBary.w = (double)0.0;
	                }
	                else
	                {
	                    // P-V1 = t*(V0-V1)
	                    akDiff[3].sub(pt,rkV1);
	                    if (fInvMax != (double)0.0)
	                    {
	                        akDiff[3].scale((float)fInvMax);
	                    }

	                    afBary.x = akDiff[3].dot(kE01)*fInvSqrLength;
	                    afBary.y = (double)1.0 - afBary.x;
	                    afBary.z = (double)0.0;
	                    afBary.w = (double)0.0;
	                }
	            }
	            else
	            {
	                // The tetrahedron is a nearly a point, just return equal
	                // weights.
	                afBary.x = (double)0.25;
	                afBary.y = afBary.x;
	                afBary.z = afBary.x;
	                afBary.w = afBary.x;
	            }
	        }
	    }
	    return afBary;
	}
}
