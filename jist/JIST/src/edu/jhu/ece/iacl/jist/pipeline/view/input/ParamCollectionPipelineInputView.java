/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipeDestination;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.gui.LayoutPanel;
import edu.jhu.ece.iacl.jist.pipeline.gui.PortComboBox;
import edu.jhu.ece.iacl.jist.pipeline.gui.PortListBox;
import edu.jhu.ece.iacl.jist.pipeline.gui.PortToggleButton;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;

/**
 * Collection Parameter Input View creates a vertical list of child parameters.
 * If a child parameter is a ParamCollection, that parameter is turned into a
 * tab pane. This will build GUI dialogs recursively since a
 * ParamCollectionInputView is also a ParamInputView
 * 
 * @author Blake Lucas
 */
public class ParamCollectionPipelineInputView extends ParamInputView implements ParamViewObserver, ChangeListener {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4236640173087293194L;
	private ArrayList<PortListBox> listBoxes;
	/**
	 * Get list of ports that are compatible with incoming port.
	 * 
	 * @param inport
	 *            port
	 * @return list of compatible ports
	 */
	protected static Vector<PipePort> getCompatiblePorts(PipePort inport) {
		PipeModule inpipe = inport.getOwner();
		Vector<PipeModule> pipes = LayoutPanel.getInstance().getPipelineLayout().getAllLeafPipes();
		Vector<PipePort> ports = new Vector<PipePort>();
		for (PipeModule pipe : pipes) {
			if (!(pipe instanceof PipeDestination)) {
				if (pipe == inpipe) {
					continue;
				}
				for (PipePort port : pipe.getOutputPorts()) {
					if (PipeConnector.isCompatible(port, inport)) {
						ports.add(port);
					}
				}
			}
		}
		return ports;
	}

	/** current displayed panel. */
	protected JComponent curPanel;
	/** show toggle button. */
	protected boolean showToggle;
	/** The tab pane. */
	protected JTabbedPane tabPane;

	/**
	 * Instantiates a new parameter collection pipeline input view.
	 * 
	 * @param params
	 *            the parameters
	 * @param showToggle
	 *            the show toggle
	 */
	public ParamCollectionPipelineInputView(ParamModel params, boolean showToggle) {
		super(params);
		curPanel = null;
		this.showToggle = showToggle;
		listBoxes=new ArrayList<PortListBox>();
		init();
	}

	/**
	 * Commit changes to this parameter and its children.
	 */
	public void commit() {
		for (ParamModel param : getParameter().getChildren()) {
			//Only commit changes if input view is used
			if(!param.isConnected()){
				param.getInputView().commit();
			}
		}
		for(PortListBox list:listBoxes){
			list.commit();
		}
	}

	/**
	 * Get collection parameter.
	 * 
	 * @return the parameter
	 */
	public ParamCollection getParameter() {
		return (ParamCollection) param;
	}

	/**
	 * Initialize input view.
	 */
	public void init() {
		ParamCollection params = getParameter();
		int selected = -1;
		for(PortListBox list:listBoxes){
			list.removeObserver(this);
		}
		listBoxes.clear();
		if (curPanel != null) {
			if (tabPane != null) {
				// remove old change listener if one already exists
				selected = tabPane.getSelectedIndex();
				tabPane.removeChangeListener(this);
			}
			// remove existing panel so it can be rebuilt from scratch
			this.remove(curPanel);
		} else {
			this.setLayout(new GridLayout(1, 0));
		}
		tabPane = null;
		// create new panel
		curPanel = new JPanel();
		curPanel.setLayout(new BoxLayout(curPanel, BoxLayout.Y_AXIS));
		for (ParamModel param : params.getChildren()) {
			if (param.isHidden()) {
				continue;
			}
			if (!param.usingConnector() && (param instanceof ParamCollection)) {
				if (!(param.getInputView() instanceof ParamCollectionPipelineInputView)) {
					param.setInputView(new ParamCollectionPipelineInputView(param, showToggle));
				}
				// If tab pane does not exist, create one
				if (tabPane == null) {
					tabPane = new JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
					curPanel.add(tabPane);
				}
				// Get parameter group view
				ParamCollectionPipelineInputView group = (ParamCollectionPipelineInputView) param.getInputView();
				//Remove observer so that notification is not sent
				group.removeObserver(this);
				// Update the view to reflect changes
				group.update();
				// Make this an observer of the collection
				group.addObserver(this);
				// group.setPreferredSize(group.getMinimumSize());
				// Add group to tab pane
				JPanel smallPane = new JPanel(new BorderLayout(5, 5));
				smallPane.add(group, BorderLayout.NORTH);
				tabPane.add(param.getLabel(), smallPane);
			} else {
				// Add parameter to pane
				ParamInputView view = param.getInputView();
				JPanel smallPane;
				if (param.isConnectible()) {
					smallPane = new JPanel(new BorderLayout());
					if (showToggle) {
						smallPane.add(new PortToggleButton(param), BorderLayout.WEST);
					}
					if (param.usingConnector()) {
						smallPane.setBorder(BorderFactory.createLoweredBevelBorder());
						// Only handles cases where the number of incoming ports
						// is 1 or unlimited
						if (param.getMaxIncoming() == 1) {
							smallPane.add(new PortComboBox(param, getCompatiblePorts(param)), BorderLayout.CENTER);
						} else {
							PortListBox listBox=new PortListBox(param, getCompatiblePorts(param));
							listBoxes.add(listBox);
							smallPane.add(listBox, BorderLayout.CENTER);
							listBox.addObserver(this);
						}
					} else {
						smallPane.add(view, BorderLayout.CENTER);
					}
				} else {
					smallPane = view;
				}
				if (view.getLayoutConstraints() != null) {
					// Use layout constraints if available
					curPanel.add(smallPane, view.getLayoutConstraints());
				} else {
					curPanel.add(smallPane);
				}
				// Observe changes to child
				view.addObserver(this);
			}
		}
		if (tabPane != null) {
			if (selected != -1) {
				tabPane.setSelectedIndex(selected);
			}
			tabPane.addChangeListener(this);
		}
		if (curPanel != null) {
			this.add(curPanel);
			curPanel.revalidate();
		}
	}

	/**
	 * Set visibility for panel.
	 * 
	 * @param visible
	 *            the visible
	 */
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		for (ParamModel param : getParameter().getChildren()) {
			param.getInputView().setVisible(visible);
		}
	}

	/**
	 * The selected tab pane has changed so notify observers.
	 * 
	 * @param event
	 *            tab pane changed
	 */
	public void stateChanged(ChangeEvent event) {
		//Do not notify observers because no parameter values have changed.
		//notifyObservers(param, this);
	}

	/**
	 * An update to the parent will invoke updates to the children.
	 */
	public void update() {
		init();
		for (ParamModel param : getParameter().getChildren()) {
			param.getInputView().update();
		}
		for(PortListBox list:listBoxes){
			list.update();
		}
	}

	/**
	 * Notify parent observers.
	 * 
	 * @param model
	 *            parameter
	 * @param view
	 *            input view
	 */
	public void update(ParamModel model, ParamInputView view) {
		notifyObservers(model, view);
	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return null;
	}
}
