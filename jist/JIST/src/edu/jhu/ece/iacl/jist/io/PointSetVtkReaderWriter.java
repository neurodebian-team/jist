package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedPointSet;

// TODO: Auto-generated Javadoc
/**
 * The Class PointSetVtkReaderWriter.
 */
public class PointSetVtkReaderWriter extends FileReaderWriter<EmbeddedPointSet>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final PointSetVtkReaderWriter readerWriter=new PointSetVtkReaderWriter();
	
	/**
	 * Gets the single instance of PointSetVtkReaderWriter.
	 * 
	 * @return single instance of PointSetVtkReaderWriter
	 */
	public static PointSetVtkReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new point set vtk reader writer.
	 */
	public PointSetVtkReaderWriter(){
		super(new FileExtensionFilter(new String[]{"vtk"}));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(EmbeddedPointSet ps,File dir){
		String name = ps.getName();
		if(name==null||name.length()==0){
			name="solid";
		}
		File f = new File(dir, name + ".vtk");
		if((f=writeObject(ps,f))!=null){
			return f;
		} else {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected EmbeddedPointSet readObject(File f) {
		BufferedReader in;
		StringBuffer buff = new StringBuffer();
		EmbeddedPointSet ps=null;
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));

			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		Pattern header=Pattern.compile("POINTS\\s\\d+\\sfloat");
		Matcher m=header.matcher(buff);
		int vertexCount=0;
		int indexCount=0;
		Point3f[] points;
		int[] indices;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					vertexCount=Integer.parseInt(vals[vals.length-1]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE VERTEX COUNT");
					return null;
				}
			}
			points=new Point3f[vertexCount];
			System.out.println("jist.io"+"\t"+"VERTS "+vertexCount);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",vertexCount*3+2);

			for(int i=1;i<strs.length-1;i+=3){
				try {
					Point3f p=new Point3f();
					p.x=Float.parseFloat(strs[i]);
					p.y=Float.parseFloat(strs[i+1]);
					p.z=Float.parseFloat(strs[i+2]);
					points[(i-1)/3]=p;
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT VERTS");
					return null;
				}
			}
		} else return null;
		header=Pattern.compile("CELLS\\s+\\d+\\s+\\d+");
		m=header.matcher(buff);
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>1){
				try {
					indexCount=Integer.parseInt(vals[1]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE INDEX COUNT");
					return null;
				}
			}
			indices=new int[indexCount*4];
			System.out.println("jist.io"+"\t"+"INDICES "+indexCount);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",indexCount*5+2);	
			int count=0;
			for(int i=1;i<strs.length-1;i+=5){			
				try {
					indices[count++]=Integer.parseInt(strs[i+1]);
					indices[count++]=Integer.parseInt(strs[i+2]);
					indices[count++]=Integer.parseInt(strs[i+3]);
					indices[count++]=Integer.parseInt(strs[i+4]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT INDICES");
					return null;
				}
			}
		} else return null;
		header=Pattern.compile("POINT_DATA\\s+\\d+\\D+float\\s+\\d+\\nLOOKUP_TABLE\\s");
		m=header.matcher(buff);
		double[][] vertData=null;
		int count=0;
		int dim=0;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					count=Integer.parseInt(vals[1]);
					dim=Integer.parseInt(vals[2]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE DATA POINTS");
					return null;
				}
			}
			vertData=new double[count][dim];
			System.out.println("jist.io"+"\t"+"VERTEX DATA "+count+" by "+dim);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",count*dim+2);
			int index=0;
			for(int i=1;i<strs.length&&index<count*dim;i++){
				try {		
					vertData[index/dim][index%dim]=Double.parseDouble(strs[i]);
					index++;
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT DATA ["+strs[i]+"]");
					//return null;
				}
			}
			System.out.println("jist.io"+"\t"+index+" "+count);
		}
		header=Pattern.compile("CELL_DATA\\s+\\d+\\D+float\\s+\\d+\\nLOOKUP_TABLE\\s");
		m=header.matcher(buff);
		double[][] cellData=null;
		count=0;
		dim=0;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					count=Integer.parseInt(vals[1]);
					dim=Integer.parseInt(vals[2]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE DATA POINTS");
					return null;
				}
			}
			cellData=new double[count][dim];
			System.out.println("jist.io"+"\t"+"CELL DATA "+count+" by "+dim);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",count*dim+2);
			int index=0;
			for(int i=1;i<strs.length&&index<count*dim;i++){
				try {		
					cellData[index/dim][index%dim]=Double.parseDouble(strs[i]);
					index++;
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT DATA ["+strs[i]+"]");
					//return null;
				}
			}
			System.out.println("jist.io"+"\t"+index+" "+count);
		}
		ps=new EmbeddedPointSet(points,indices);
		if(vertData!=null){
			ps.setPointData(vertData);
		}
		if(cellData!=null){
			ps.setCellData(cellData);
		}
		ps.setName(FileReaderWriter.getFileName(f));
		return ps;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(EmbeddedPointSet ps,File f){
		try {
			BufferedWriter stream=new BufferedWriter(new FileWriter(f));
			int pointCount=ps.getPointCount();
			stream.append("# vtk DataFile Version 3.0\n"
					+ps.getName()+"\n"
					+"ASCII\n"
					+"DATASET UNSTRUCTURED_GRID\n"
					+"POINTS "+pointCount+" float\n");
			Point3f p=new Point3f();
			String tmp;
			for(int i=0;i<pointCount;i++){
				p=ps.getPoint(i);
				tmp=String.format("%.5f %.5f %.5f\n", p.x,p.y,p.z);
				stream.append(tmp);
			}
			int indexes[]=ps.getIndexes();
			if(indexes!=null&&indexes.length>0){

				int tetraCount=ps.getTetrahedraCount();
				stream.append("CELLS "+tetraCount+" "+tetraCount*5+"\n");
				for(int i=0;i<tetraCount;i++){
					stream.append("4 "+indexes[i*4]+" "+indexes[i*4+1]+" "+indexes[i*4+2]+" "+indexes[i*4+3]+"\n");
				}
			
				stream.append("CELL_TYPES "+tetraCount+"\n");
				for(int i=0;i<tetraCount;i++)stream.append("10 ");
				stream.append("\n");
			}
			double[][] scalars=ps.getPointData();
			if(scalars!=null&&scalars.length>0&&scalars[0].length>0){
				stream.append("POINT_DATA "+scalars.length+"\n"
						+"SCALARS EmbedVertex float "+scalars[0].length+"\n"
						+"LOOKUP_TABLE default\n");
				for(int i=0;i<scalars.length;i++){
					for(int j=0;j<scalars[i].length;j++){
						stream.append(scalars[i][j]+" ");
					} 
					stream.append("\n");
				}
			}
			double[][] cells=ps.getCellData();
			if(cells!=null&&cells.length>0&&cells[0].length>0){
				stream.append("CELL_DATA "+cells.length+"\n"
						+"SCALARS EmbedCell float "+cells[0].length+"\n"
						+"LOOKUP_TABLE default\n");
				for(int i=0;i<cells.length;i++){
					for(int j=0;j<cells[i].length;j++){
						stream.append(cells[i][j]+" ");
					} 
					stream.append("\n");
				}
			}
			stream.close();
			return f;
		} catch (IOException e) {
			System.err.println(getClass().getCanonicalName()+e.getMessage());
		}
		return null;
	}


}
