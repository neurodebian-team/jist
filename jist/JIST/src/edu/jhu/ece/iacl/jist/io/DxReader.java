package edu.jhu.ece.iacl.jist.io;


/**
 * The Class DxReader.
 */
public class DxReader {
	/*
	public static EmbeddedSurface readSurface(File f){
		BufferedReader in;
		StringBuffer buff = new StringBuffer();
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));
			
			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		Pattern header=Pattern.compile("rank\\s\\d+\\sshape\\s\\d+\\sitems\\s\\d+\\sdata\\sfollows");
		Matcher m=header.matcher(buff);
		int vertexCount=0;
		int indexCount=0;
		Point3f[] points;
		int[] indices;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>=4){
				try {
					vertexCount=Integer.parseInt(vals[3]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE VERTEX COUNT");
					return null;
				}
			}
			points=new Point3f[vertexCount];
			System.out.println("jist.io"+"\t"+"VERTS "+vertexCount);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",vertexCount*3+2);
			
			for(int i=1;i<strs.length-1;i+=3){
				try {
					Point3f p=new Point3f();
					p.x=Float.parseFloat(strs[i]);
					p.y=Float.parseFloat(strs[i+1]);
					p.z=Float.parseFloat(strs[i+2]);
					points[(i-1)/3]=p;
					//System.out.println("jist.io"+"\t"+i/3+")"+p);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT VERTS");
					return null;
				}
			}
		} else return null;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					indexCount=Integer.parseInt(vals[3]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE INDEX COUNT");
					return null;
				}
			}
			indices=new int[indexCount*3];
			System.out.println("jist.io"+"\t"+"INDICES "+indexCount);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",indexCount*3+2);			
			for(int i=1;i<strs.length-1;i+=3){			
				try {
					indices[i-1]=Integer.parseInt(strs[i+2]);
					indices[i]=Integer.parseInt(strs[i+1]);
					indices[i+1]=Integer.parseInt(strs[i]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT INDICES");
					return null;
				}
			}
		} else return null;
		EmbeddedSurface surf=new EmbeddedSurface(points,NormalGenerator.generate(points, indices),indices);
//		surf.setName(MipavReader.getFileName(f));
		//System.out.println("jist.io"+"\t"+text);
		return surf;
		//"object 1 class array type float rank 1 shape 3 items 387202 data follows";
	}
	
	public static double[][] readData(File f){
		BufferedReader in;
		StringBuffer buff = new StringBuffer();
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));
			
			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		Pattern header=Pattern.compile("items\\s\\d+\\sdata\\sfollows");
		Matcher m=header.matcher(buff);
		double[][] dat;
		int count=0;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					count=Integer.parseInt(vals[vals.length-1]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE DATA POINTS");
					return null;
				}
			}
			dat=new double[count][1];
			System.out.println("jist.io"+"\t"+"DATA POINTS "+count);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",count+2);
			
			for(int i=1;i<strs.length-1&&i<=dat.length;i++){
				try {
					dat[i-1][0]=Double.parseDouble(strs[i]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT DATA");
					return null;
				}
			}
		} else return null;
		


		return dat;
		//"object 1 class array type float rank 1 shape 3 items 387202 data follows";
	}
	*/
}