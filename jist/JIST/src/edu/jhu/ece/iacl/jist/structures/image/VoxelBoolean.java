package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;

// TODO: Auto-generated Javadoc
/**
 * Boolean Voxel Type.
 * 
 * @author Blake Lucas
 */
public class VoxelBoolean extends Voxel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6843914692644177564L;
	
	/** The vox. */
	private boolean vox;

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getBoolean()
	 */
	public boolean getBoolean() {
		return vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getShort()
	 */
	public short getShort() {
		return (byte) ((vox) ? 1 : 0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getColor()
	 */
	public Color getColor() {
		return (vox) ? Color.WHITE : Color.BLACK;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getDouble()
	 */
	public double getDouble() {
		return ((vox) ? 1.0 : 0.0);
	}

	/**
	 * Instantiates a new voxel boolean.
	 * 
	 * @param vox the vox
	 */
	public VoxelBoolean(boolean vox) {
		set(vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#clone()
	 */
	public Voxel clone() {
		return new VoxelBoolean(vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public void set(Voxel v) {
		this.vox = v.getBoolean();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#add(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelBoolean add(Voxel v) {
		return new VoxelBoolean(v.getBoolean() ^ getBoolean());
	}

	/**
	 * Or.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel boolean
	 */
	public VoxelBoolean or(Voxel v) {
		return new VoxelBoolean(v.getBoolean() | getBoolean());
	}

	/**
	 * And.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel boolean
	 */
	public VoxelBoolean and(Voxel v) {
		return new VoxelBoolean(v.getBoolean() & getBoolean());
	}

	/**
	 * Xor.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel boolean
	 */
	public VoxelBoolean xor(Voxel v) {
		return new VoxelBoolean(v.getBoolean() ^ getBoolean());
	}

	/**
	 * Nor.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel boolean
	 */
	public VoxelBoolean nor(Voxel v) {
		return new VoxelBoolean(!(v.getBoolean() | getBoolean()));
	}

	/**
	 * Nand.
	 * 
	 * @param v the v
	 * 
	 * @return the voxel boolean
	 */
	public VoxelBoolean nand(Voxel v) {
		return new VoxelBoolean(!(v.getBoolean() & getBoolean()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#sub(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelBoolean sub(Voxel v) {
		return null;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#mul(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelBoolean mul(Voxel v) {
		return new VoxelBoolean(v.getBoolean() & getBoolean());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#div(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelBoolean div(Voxel v) {
		return null;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#neg()
	 */
	public VoxelBoolean neg() {
		return new VoxelBoolean(!getBoolean());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(boolean)
	 */
	public void set(boolean vox) {
		this.vox = vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(short)
	 */
	public void set(short vox) {
		this.vox = (vox != 0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(java.awt.Color)
	 */
	public void set(Color vox) {
		this.vox = !vox.equals(Color.BLACK);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(double)
	 */
	public void set(double vox) {
		this.vox = (vox != 0.0);
	}

	/**
	 * Instantiates a new voxel boolean.
	 */
	public VoxelBoolean() {
		this.vox = false;
	}

	/**
	 * Instantiates a new voxel boolean.
	 * 
	 * @param v the v
	 */
	public VoxelBoolean(Voxel v) {
		set(v);
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Voxel obj) {
		return (int) (this.getShort() - ((Voxel) obj).getShort());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getInt()
	 */
	public int getInt() {
		return (vox) ? 1 : 0;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(int)
	 */
	public void set(int vox) {
		this.vox = (vox != 0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#toString()
	 */
	public String toString() {
		return (vox + "");
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getType()
	 */
	@Override
	public VoxelType getType() {
		return VoxelType.BOOLEAN;
	}

}
