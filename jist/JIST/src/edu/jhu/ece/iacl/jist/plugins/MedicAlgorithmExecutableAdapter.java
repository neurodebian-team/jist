package edu.jhu.ece.iacl.jist.plugins;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.InvalidJistMergeException;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamArgsOrder;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamExecutable;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSwitch;


/**
 * The Class MedicAlgorithmExecutableAdapter.
 */
public class MedicAlgorithmExecutableAdapter extends ProcessingAlgorithm implements AbstractProcessingAlgorithmInterface{
		
	/**
	 * Instantiates a new medic algorithm executable adapter.
	 */
	public MedicAlgorithmExecutableAdapter() {
		super();
	}

	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param pipeFile the pipe file
	 */
	public void init(File pipeFile) {
		this.inputParams = (ParamCollection) ParamFactory.fromXML(pipeFile);
		// Last parameter is output
		this.outputParams = (ParamCollection) inputParams.getValue(inputParams.size() - 1);
		this.inputParams.remove(outputParams);
		outputParams.setHidden(false);
		performance = (ParamPerformance) outputParams.getFirstChildByClass(ParamPerformance.class);
		this.outputParams.setName(inputParams.getName());
		this.outputParams.setLabel(inputParams.getLabel());
	}

	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param pipe the pipe
	 */
	public void init(PipeAlgorithm pipe) {
		this.inputParams = pipe.getInputParams();
		this.outputParams = pipe.getOutputParams();
		this.outputParams.setName(inputParams.getName());
		this.outputParams.setLabel(inputParams.getLabel());
		performance = (ParamPerformance) outputParams.getFirstChildByClass(ParamPerformance.class);
	}


	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "\nAlgorithm Version: " + revnum + "\n";


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("Base");
		inputParams.setLabel("LONI Pipeline Adapter");
		inputParams.setName("LONI_Pipeline_Adapter");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setDescription(shortDescription);
		info.add(new AlgorithmAuthor("Blake Lucas", "", ""));
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
	}


	/**
	 * Append name.
	 * 
	 * @param str the str
	 * @param offset the offset
	 * 
	 * @return the string
	 */
	private String appendName(String str, int offset) {
		int index = str.lastIndexOf('_');
		String name;
		if (index != -1) {
			name = str.substring(0, index) + "_" + offset;
		} else {
			name = str + "_" + offset;
		}
		return name;
	}


	/**
	 * The Class ProcessRunner.
	 */
	protected class ProcessRunner extends AbstractCalculation {
		
		/**
		 * Instantiates a new process runner.
		 * 
		 * @param name the name
		 */
		public ProcessRunner(String name) {
			super();
			setLabel(name);
		}


		/**
		 * Run.
		 * 
		 * @param command the command
		 * @param outputDir the output dir
		 * 
		 * @throws AlgorithmRuntimeException the algorithm runtime exception
		 */
		public void run(List<String> command, File outputDir) throws AlgorithmRuntimeException {
			ProcessBuilder builder = new ProcessBuilder(command);
			System.out.println(getClass().getCanonicalName()+"\t"+"COMMAND " + command);
			System.out.flush();
			builder.directory(outputDir);
			int exitVal = -1;
			Process process = null;
			BufferedReader stdoutStream = null;
			BufferedReader stderrStream = null;
			InputStreamReader isr1 = null, isr2 = null;
			String line;
			setTotalUnits(1);
			try {
				// start process
				process = builder.start();
				// capture stdout
				try {
					process.waitFor();
					stdoutStream = new BufferedReader(isr1 = new InputStreamReader(process.getInputStream()));
					while ((line = stdoutStream.readLine()) != null) {
						System.out.println(getClass().getCanonicalName()+"\t"+line);
					}
					stdoutStream.close();
					stderrStream = new BufferedReader(isr2 = new InputStreamReader(process.getErrorStream()));
					while ((line = stderrStream.readLine()) != null) {
						System.err.println(getClass().getCanonicalName()+line);
					}
					stderrStream.close();
					exitVal = process.exitValue();
					incrementCompletedUnits();
				} catch (IllegalThreadStateException e) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
					System.err.println(getClass().getCanonicalName()+" Illegal Exit Value " + exitVal);
					exitVal = -1;
					if (process != null)
						process.destroy();
					throw new AlgorithmRuntimeException(" Illegal Exit Value: " + exitVal + "\n" + e.getMessage());
				} catch (InterruptedException e) {
					System.err.println(getClass().getCanonicalName()+" InterruptedException: " + e.getMessage());
					exitVal = -1;
					if (process != null)
						process.destroy();
					throw new AlgorithmRuntimeException(" InterruptedException: " + e.getMessage());
					// If process hasn't exited and exit value returned, make
					// sure process terminates and object destroyed
				}
			} catch (IOException e) {
				// Process failed
				System.err.println(getClass().getCanonicalName()+"Process Failed " + e.getMessage());
				if (process != null)
					process.destroy();
				throw new AlgorithmRuntimeException("Process Failed: " + e.getMessage());
			} finally {
				if (process != null)
					process.destroy();
				markCompleted();
			}
		}
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ParamCollection inputParams = this.getInput();
		ParamCollection outputParams = this.getOutput();
		ParamArgsOrder argsOrder = (ParamArgsOrder) inputParams.getFirstChildByClass(ParamArgsOrder.class);
		ParamExecutable executable = (ParamExecutable) inputParams.getFirstChildByClass(ParamExecutable.class);
		ArrayList<String> args = argsOrder.getValue();
		LinkedList<String> command = new LinkedList<String>();
		command.add(executable.getValue().getAbsolutePath());
		// command.add("-inputs");
		ParamSwitch switchParam = null;
		boolean inputPart = true;
		// Create output directory in case it hasn't been created yet
		File outputDir = new File(this.getOutputDirectory(), File.separatorChar + this.getAlgorithmLabel());
		if (!outputDir.exists()) {
			outputDir.mkdir();
		}
		// If file does not have preferred extension, create a copy of the file
		// that does have the preferred extension and
		// save the file in that extension
		int count = 1;
		for (ParamModel param : inputParams.getChildren()) {
			if (param instanceof ParamObject) {
				ParamObject fileParam = (ParamObject) param;
				File f = fileParam.getValue();
				if (f != null) {
					String ext = FileReaderWriter.getFileExtension(f);
					String pext = fileParam.getExtensionFilter().getPreferredExtension();
					if (!ext.equalsIgnoreCase(pext)) {
						File newFile = new File(outputDir, "input_" + count + "." + pext);
						fileParam.writeObject(newFile);
						count++;
					}
				}
			}
		}
		// Create dummy output file names with proper extensions that will be
		// used to save output data
		count = 1;
		for (ParamModel mod : outputParams.getChildren()) {
			if (mod instanceof ParamFile) {
				FileExtensionFilter filter = ((ParamFile) mod).getExtensionFilter();
				if (filter != null) {
					mod.setValue(new File(outputDir, "output_" + count + "." + filter.getPreferredExtension()));
				} else {
					mod.setValue(new File(outputDir, "output_" + count));
				}
				count++;
			}
		}
		// Create command line to execute
		ParamModel param, p;
		int index = 0;
		boolean foundParam;
		for (String arg : args) {
			switchParam = null;
			ArrayList<ParamModel> candidates = new ArrayList<ParamModel>();
			index = 0;
			foundParam=false;
			while (true) {
				param = inputParams.getFirstChildByName(appendName(arg, index));
				if (param == null)
					break;
				foundParam=true;
				switchParam = (ParamSwitch) inputParams.getFirstChildByName("sw_" + appendName(arg, index));
				if (switchParam != null) {
					command.add(switchParam.toString());
				}
				command.add(param.toString());
				index++;
			}
			if(!foundParam){
				index = 0;				
				while (true) {
					param = outputParams.getFirstChildByName(appendName(arg, index));
					if (param == null)
						break;
					param = outputParams.getFirstChildByName(arg);
					if (param != null) {
						switchParam = (ParamSwitch) outputParams.getFirstChildByName("sw_" + appendName(arg, index));
						if (switchParam != null) {
							command.add(switchParam.toString());
						}
						command.add(param.toString());
					}
					index++;
				}
			}
		}
		ProcessRunner runner = new ProcessRunner(executable.getValue().getName());
		monitor.observe(runner);
		runner.run(command, outputDir);
		count = 1;
		File file;
		//If file output does not exist, then ignore output by setting value to null
		for (ParamModel mod : outputParams.getChildren()) {
			if (mod instanceof ParamFile) {
				FileExtensionFilter filter = ((ParamFile) mod).getExtensionFilter();
				if (filter != null) {
					file=new File(outputDir, "output_" + count + "." + filter.getPreferredExtension());
				} else {
					file=new File(outputDir, "output_" + count);
				}
				if(!file.exists()){
					((ParamFile)mod).setValue((File)null);
				}
				count++;
			}
		}
	}

	public String reconcileAndMerge(ParamCollection inputParams2,
			ParamCollection outputParams2) throws InvalidJistMergeException{
		String msg = super.reconcileAndMerge(inputParams2, outputParams2);		
		ParamExecutable executable = (ParamExecutable) inputParams.getFirstChildByClass(ParamExecutable.class);
		if(!executable.getValue().exists()) {
			throw new InvalidJistMergeException("Cannot find executable:"+executable.getValue());
		}		
		return msg;		
	}
}
