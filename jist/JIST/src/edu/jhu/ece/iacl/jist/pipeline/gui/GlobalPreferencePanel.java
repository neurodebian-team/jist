package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;

public class GlobalPreferencePanel extends JPanel implements ActionListener {

	/** The browse input. */
	private JButton browseInput;

	/** The direct input. */
	private JTextField dirInput;	
	
	/** The browse output. */
	private JButton browseJreOutput;

	/** The browse output. */
	private JButton browseLibraryOutput;
	
	/** The use relative. */
	private JCheckBox useRelative;	

	/** The jre location. */
	private JTextField jreField;	
	
	/** The lib location. */
	private JTextField libField;	
	
	/** The preferred extension */
	private JComboBox prefext;
	
	/** The preferred extension */
	private JComboBox prefcomp;
	
	private JSpinner debugLevelField;
	
	private JSpinner layoutPrefField;
	/**The Maps Preferences */
	private JistPreferences prefs;
	
	public GlobalPreferencePanel(JistPreferences maps) {
		super();
		createPane();
		init(maps);
	}

	/**
	 * Show dialog.
	 * 
	 * @param comp
	 *            the comp
	 * @return true, if successful
	 */
	public static boolean showDialog(Component comp, JistPreferences maps) {
		GlobalPreferencePanel panel = new GlobalPreferencePanel(maps);
		while (true) {
			int n = JOptionPane.showConfirmDialog(comp, panel, "Global Preferences",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (n == 0) {
				if (panel.update()) {
					return true;
				} else {
					JOptionPane.showMessageDialog(comp, "Invalid parameter.", "Run Parameter Error",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				return false;
			}
		}
	}	

	/**
	 * Update.
	 * 
	 * @return true, if successful
	 */
	protected boolean update() {
		//Save JRE
		String oldJre = JistPreferences.getPreferences().getJre();
		String newJre = jreField.getText();
		if(newJre!=null&&!newJre.equals(oldJre)){
			JistPreferences.getPreferences().setJre(newJre);			
		}
		
		//Save Preferred Extension
		JistPreferences.getPreferences().setPreferredExtension((String) prefext.getSelectedItem());
		JistPreferences.getPreferences().setPreferredCompression((String) prefcomp.getSelectedItem());

		//Save Library 
		File oldDir = PipeLibrary.getInstance().getLibraryPath();
		File newDir = new File(libField.getText());
		if (newDir!=null&&!newDir.equals(oldDir)){
			PipeLibrary.getInstance().setLibraryPath(newDir);
			if (PipelineLayoutTool.isInitialized()) {
				PipelineLayoutTool.getInstance().getModulePanel().rebuild();
			}
		}
		int selected = 0;
		for(String arg : JistPreferences.debugLevels) {
			if(arg.equals(debugLevelField.getValue().toString()))
				break;
			selected++;
		}
		
		JistPreferences.getPreferences().setDebugLevel(selected);
		
		JistPreferences.getPreferences().setDefaultLayoutExtension(layoutPrefField.getValue().toString());
		
		JistPreferences.savePreferences();
		return true;
	}
	
	
	/**
	 * Create panel to display preferences.
	 */
	protected void createPane() {
		this.setLayout(new BorderLayout());
		JPanel small = new JPanel();
		BoxLayout layout = new BoxLayout(small, BoxLayout.PAGE_AXIS);
		small.setLayout(layout);
		this.add(small, BorderLayout.NORTH);
		JPanel itemPane = new JPanel(new BorderLayout());

		JPanel relativePane = new JPanel(new BorderLayout());
		relativePane.add(dirInput = new JTextField(20), BorderLayout.CENTER);
		relativePane.add(browseInput = new JButton("Browse"), BorderLayout.EAST);
		relativePane.add(useRelative = new JCheckBox("Use Relative Path"), BorderLayout.SOUTH);
		useRelative.setEnabled(false);
		small.add(itemPane);

		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("JRE Location "), BorderLayout.WEST);
		itemPane.add(jreField = new JTextField(20), BorderLayout.CENTER);
		itemPane.add(browseJreOutput = new JButton("Browse"), BorderLayout.EAST);
		small.add(itemPane);
		
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Library Location "), BorderLayout.WEST);
		itemPane.add(libField = new JTextField(20), BorderLayout.CENTER);
		itemPane.add(browseLibraryOutput = new JButton("Browse"), BorderLayout.EAST);
		small.add(itemPane);
		
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Preferred Extension"), BorderLayout.WEST);
		Vector<String> extensions = ImageDataReaderWriter.getInstance().getExtensionFilter().getExtensions();
		itemPane.add(prefext = new JComboBox(extensions), BorderLayout.EAST);		
		small.add(itemPane);
		
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Preferred Compression"), BorderLayout.WEST);
		itemPane.add(prefcomp = new JComboBox(ImageDataReaderWriter.getInstance().getCompressionOptions()), BorderLayout.EAST);
		small.add(itemPane);
		
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Debug Level "), BorderLayout.WEST);
		
		SpinnerListModel debugModel = new SpinnerListModel(JistPreferences.debugLevels);
		itemPane.add(debugLevelField = new JSpinner(debugModel), BorderLayout.CENTER);		
		small.add(itemPane);
		
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Layout Format"), BorderLayout.WEST);
		SpinnerListModel layoutPref = new SpinnerListModel(JistPreferences.layoutExtensions);
		itemPane.add(layoutPrefField = new JSpinner(layoutPref), BorderLayout.CENTER);		
		small.add(itemPane);

		browseInput.addActionListener(this);
		browseJreOutput.addActionListener(this);
		browseLibraryOutput.addActionListener(this);
		useRelative.addActionListener(this);
	}		
	
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == browseInput) {
			JistPreferences.getPreferences().addFileToHistory(new File(dirInput.getText()));
			JistPreferences.savePreferences();
		}
		
		if (evt.getSource() == browseJreOutput) {
			File oldJre = new File(JistPreferences.getPreferences().getJre());
			File newJre = JistPreferences.askForJavaExecutable(oldJre);
			if (newJre != null) {
				jreField.setText(newJre.getAbsolutePath());
			}
		}
		if (evt.getSource() == browseLibraryOutput) {
			File oldDir = PipeLibrary.getInstance().getLibraryPath();
			File newDir = PipeLibrary.askForLibraryDirectory(oldDir);
			if(newDir!=null){
				libField.setText(newDir.getAbsolutePath());
			}
		}
	}
		
	/**
	 * Initialize preference panel for layout.
	 * 
	 */
	protected boolean init(JistPreferences pref) {
		prefs = pref;
		
		//Find index of default
		Vector<String> extensions = ImageDataReaderWriter.getInstance().getExtensionFilter().getExtensions();
		int i = 0;
		while((i < extensions.size()) && (extensions.get(i).compareTo(prefs.getPreferredExtension()) != 0))
		{
			i++;
		}
		prefext.setSelectedIndex(i);
		
		
		Vector<String> compressions = ImageDataReaderWriter.getInstance().getCompressionOptions();
		i = 0;
		while((i < compressions.size()) && (compressions.get(i).compareTo(prefs.getPreferredCompression()) != 0))
		{
			i++;
		}
		prefcomp.setSelectedIndex(i);
		libField.setText(PipeLibrary.getInstance().getLibraryPath().getAbsolutePath());
		//Initialize JRE
		jreField.setText(prefs.getJre());
		File f = new File(prefs.getJre());
				
		debugLevelField.setValue(JistPreferences.debugLevels[prefs.getDebugLevel()]);
		try {
		layoutPrefField.setValue(JistPreferences.getPreferences().getDefaultLayoutExtension());
		} catch (IllegalArgumentException e){};
		
		return f.exists();
	}
}