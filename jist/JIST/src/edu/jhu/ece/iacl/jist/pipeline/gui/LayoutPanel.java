/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.beans.PropertyVetoException;
import java.util.LinkedList;

import javax.swing.JDesktopPane;
import javax.swing.ProgressMonitor;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;

/**
 * Desktop pane for pipe layouts.
 * 
 * @author Blake Lucas
 */
public class LayoutPanel extends JDesktopPane implements InternalFrameListener {
	
	/** The Constant panel. */
	private static final LayoutPanel panel = new LayoutPanel();

	/**
	 * Gets the single instance of LayoutPanel.
	 * 
	 * @return single instance of LayoutPanel
	 */
	public static LayoutPanel getInstance() {
		return panel;
	}

	
	/** The frames. */
	protected LinkedList<PipeInternalFrame> frames;

	/**
	 * Default constructor.
	 */
	protected LayoutPanel() {
		super();
		setVisible(true);
		frames = new LinkedList<PipeInternalFrame>();
	}

	/**
	 * Add frame with particular name.
	 * 
	 * @param name
	 *            the name
	 */
	public PipeInternalFrame addFrame(String name) {
		PipeInternalFrame activeFrame;
		this.add(activeFrame = new PipeInternalFrame(name));
		frames.add(activeFrame);
		activeFrame.select();
		PipelineLayoutTool.getInstance().setMenuItemsEnabled(true);
		PipelineLayoutTool.getInstance().updateTitle();
		return activeFrame;
	}

	/**
	 * Add internal frame to desktop layout.
	 */
	public void addLayoutFrame() {
		
		PipeInternalFrame activeFrame=this.addFrame("Layout " + (frames.size() + 1));
		activeFrame.setLocation(50 * (frames.size() - 1), 50 * (frames.size() - 1));
		activeFrame.addInternalFrameListener(this);
		try {
			activeFrame.setMaximum(true);
		} catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PipelineLayoutTool.getInstance().updateTitle();
	}

	/**
	 * Return currently selected frame.
	 * 
	 * @return the active frame
	 */
	public PipeInternalFrame getActiveFrame() {
		return (PipeInternalFrame) this.getSelectedFrame();
	}

	/**
	 * Return active frames.
	 * 
	 * @return the frames
	 */
	public LinkedList<PipeInternalFrame> getFrames() {
		return frames;
	}

	/**
	 * Return all internal frames.
	 * 
	 * @return the internal frames
	 */
	public LinkedList<PipeInternalFrame> getInternalFrames() {
		return frames;
	}

	/**
	 * Get the layout for the current active frame.
	 * 
	 * @return the pipeline layout
	 */
	public PipeLayout getPipelineLayout() {
		return getActiveFrame().getPipelineLayout();
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.InternalFrameListener#internalFrameActivated(javax.swing.event.InternalFrameEvent)
	 */
	public void internalFrameActivated(InternalFrameEvent arg0) {
		PipelineLayoutTool.getInstance().updateTitle();
	}

	/**
	 * Remove frame from list if frame is closed.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void internalFrameClosed(InternalFrameEvent evt) {
		frames.remove(evt.getInternalFrame());
		if (frames.size() == 0) {
			PipelineLayoutTool.getInstance().setMenuItemsEnabled(false);
		}
		PipelineLayoutTool.getInstance().updateTitle();
	}

	/**
	 * Close frame if possible.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void internalFrameClosing(InternalFrameEvent evt) {
		((PipeInternalFrame) evt.getInternalFrame()).close();
		PipelineLayoutTool.getInstance().updateTitle();
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.InternalFrameListener#internalFrameDeactivated(javax.swing.event.InternalFrameEvent)
	 */
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.InternalFrameListener#internalFrameDeiconified(javax.swing.event.InternalFrameEvent)
	 */
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.InternalFrameListener#internalFrameIconified(javax.swing.event.InternalFrameEvent)
	 */
	public void internalFrameIconified(InternalFrameEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.InternalFrameListener#internalFrameOpened(javax.swing.event.InternalFrameEvent)
	 */
	public void internalFrameOpened(InternalFrameEvent arg0) {
		PipelineLayoutTool.getInstance().updateTitle();
	}

	/**
	 * Set the layout for the current active frame.
	 * 
	 * @param layout
	 *            the layout
	 * @param monitor
	 *            the monitor
	 */
	public void setPipelineLayout(PipeLayout layout, ProgressMonitor monitor) {
		getActiveFrame().setPipelineLayout(layout, monitor);
	}
}
