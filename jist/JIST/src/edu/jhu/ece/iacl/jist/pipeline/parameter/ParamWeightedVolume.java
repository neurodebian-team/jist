/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Weighted volume to store a volume and a weight associated with the volume.
 * 
 * @author Blake Lucas
 */
public class ParamWeightedVolume<T> extends ParamVolume {

	/** The weight. */
	protected ParamModel<T> weightParam;

	public boolean xmlEncodeParam(Document document, Element parent) {
		super.xmlEncodeParam(document, parent);		 
		Element em;				
		em = document.createElement("weightParam");
		if(weightParam!=null) {			
			if(weightParam.xmlEncodeParam(document, em))
				parent.appendChild(em);
		}		
		return true;
	}
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		Element el = JistXMLUtil.xmlReadElement(parent, "weightParam");
		String classname = JistXMLUtil.xmlReadTag(el, "classname");
		try {
			weightParam = (ParamModel)Class.forName(classname).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		weightParam.xmlDecodeParam(document, el);			
	}

	public ParamWeightedVolume() {
		this(null);
	}
	/**
	 * Inherits constructor from ParamVolume.
	 */
	public ParamWeightedVolume(ParamModel<T> weightParam) {
		super();
		this.weightParam=weightParam;
	}

	/**
	 * Inherits constructor from ParamVolume.
	 * 
	 * @param name
	 *            parameter name
	 * @param type
	 *            voxel type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamWeightedVolume(String name, ParamModel<T> weightParam,VoxelType type, int rows, int cols, int slices, int components) {
		super(name, type, rows, cols, slices, components);
		this.weightParam=weightParam;
		weightParam.setName(this.getName()+"_weight");
		weightParam.setLabel(this.getLabel()+" Weight");
	}

	/**
	 * Inherits constructor from ParamVolume.
	 * 
	 * @param type
	 *            voxel type restriction
	 */
	public ParamWeightedVolume(VoxelType type,ParamModel<T> weightParam) {
		super(type);
		this.weightParam=weightParam;

		weightParam.setName(this.getName()+"_weight");
		weightParam.setLabel(this.getLabel()+" Weight");
	}

	/**
	 * Inherits constructor from ParamVolume.
	 * 
	 * @param type
	 *            voxel type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamWeightedVolume(VoxelType type, ParamModel<T> weightParam,int rows, int cols, int slices, int components) {
		super(type, rows, cols, slices, components);
		this.weightParam=weightParam;

		weightParam.setName(this.getName()+"_weight");
		weightParam.setLabel(this.getLabel()+" Weight");
	}

	/**
	 * Clone object.
	 * 
	 * @return the param weighted volume
	 */
	public ParamWeightedVolume clone() {
		ParamWeightedVolume param = new ParamWeightedVolume(weightParam.clone());
		param.rows = rows;
		param.slices = slices;
		param.components = components;
		param.cols = cols;
		param.voxelType = voxelType;
		param.file = this.file;
		param.uri=this.uri;
		//		param.img=this.img;
		param.volume=this.volume;
		param.readerWriter = this.readerWriter;
		param.extensionFilter = this.extensionFilter;
		param.setName(this.getName());
		param.label=this.label;
		param.setValue(this.getValue());
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}
	public ParamModel<T> getWeightParameter(){
		return weightParam;
	}
	public ParamModel<T> createWeightParameter(){
		weightParam=weightParam.clone();
		return weightParam;
	}
	/**
	 * Get weight.
	 * 
	 * @return weight
	 */
	public T getWeight() {
		return weightParam.getValue();
	}

	/**
	 * Set weight.
	 * 
	 * @param weight
	 *            weight
	 */
	public void setWeight(T weight) {
		weightParam.setValue(weight);
	}

	/**
	 * Get description of volume and weight.
	 * 
	 * @return the string
	 */
	public String toString() {
		return super.toString()+ ": "+getWeightParameter().toString();
	}

	@Override
	public String getXMLValue() {
		// TODO Auto-generated method stub
		return getXMLValue()+";;;"+weightParam.getXMLValue();
	}

	@Override
	public void setXMLValue(String arg) {
		// TODO Auto-generated method stub
		String []args = arg.trim().split(";;;");
		if(args.length!=2)
			throw new InvalidParameterValueException(this,"Cannot parse weighted volumes: "+arg);
		super.setXMLValue(args[0]);
		weightParam.setXMLValue(args[1]);
	}
}
