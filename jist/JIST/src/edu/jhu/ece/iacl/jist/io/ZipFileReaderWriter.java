package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

// TODO: Auto-generated Javadoc
/**
 * The Class ZipFileReaderWriter.
 */
public class ZipFileReaderWriter extends FileReaderWriter{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final ZipFileReaderWriter readerWriter=new ZipFileReaderWriter();
	
	/**
	 * Gets the single instance of ZipFileReaderWriter.
	 * 
	 * @return single instance of ZipFileReaderWriter
	 */
	public static ZipFileReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new zip file reader writer.
	 */
	public ZipFileReaderWriter(){		
		super(new FileExtensionFilter(new String[] { "zip", "gz", "gzip" }));
	}
	
	/**
	 * Instantiates a new zip file reader writer.
	 * 
	 * @param filter the filter
	 */
	public ZipFileReaderWriter(FileExtensionFilter filter){
		super(filter);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	public File writeObject(Object obj, File f){
		
		//Now zip this file:
		try {
	        // Create the GZIP output stream
	        String outFilename = f.getAbsolutePath()+".gz";
	        GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(outFilename));
	    
	        // Open the input file
	        System.out.println("jist.io"+"\t"+f);
	        FileInputStream in = new FileInputStream(f);
	    
	        // Transfer bytes from the input file to the GZIP output stream
	        byte[] buf = new byte[1024];
	        int len;
	        while ((len = in.read(buf)) > 0) {
	            out.write(buf, 0, len);
	        }
	        in.close();
	    
	        // Complete the GZIP file
	        out.finish();
	        out.close();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
		return f;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	public File readObject(File f){	
		String fileinname = f.getName();
		File fileinpath = f.getParentFile();
		String dir = fileinpath.getName();
		
		if(fileinname.substring(fileinname.length()-3, fileinname.length())=="zip"){
			try {
				// Open the ZIP file
				ZipInputStream in = new ZipInputStream(new FileInputStream(fileinpath));

				while(in.available()>0){
					// Get the first entry
					ZipEntry entry = in.getNextEntry();

					// Open the output file
					String outFilename = dir+entry.getName();	
					FileOutputStream out = new FileOutputStream(outFilename);

					// Transfer bytes from the ZIP file to the output file
					byte[] buf = new byte[1024];
					int len;
					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
					//Close this output stream
					out.close();
				}

				//Close the input stream
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(fileinname.substring(fileinname.length()-2, fileinname.length())=="gz"){
			try {
				// Open the compressed file
				GZIPInputStream in = new GZIPInputStream(new FileInputStream(fileinpath));

				// Open the output file
				String outFilename = dir+fileinname.substring(0, fileinname.length()-3);
				FileOutputStream out = new FileOutputStream(outFilename);

				// Transfer bytes from the compressed file to the output file
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				// Close the file and stream
				in.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		return null;
	}
}
