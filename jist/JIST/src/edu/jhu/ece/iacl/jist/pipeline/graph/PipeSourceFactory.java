/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.ImageIcon;

import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;

/**
 * Factory for creating source modules.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeSourceFactory extends PipeModuleFactory {
	
	/**
	 * Source graph cell.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class SourceCell extends PipeModuleCell {
		
		/**
		 * Instantiates a new source cell.
		 * 
		 * @param name
		 *            the name
		 * @param pipe
		 *            the pipe
		 */
		public SourceCell(String name, PipeModule pipe) {
			super(name, pipe);
			PipeCellViewFactory.setViewClass(this.getAttributes(), PipeSourceView.class.getCanonicalName());
			GraphConstants.setGradientColor(this.getAttributes(), Color.blue);
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.DefaultGraphCell#clone()
		 */
		public PipeModuleCell clone() {
			PipeSourceFactory factory = 
				new PipeSourceFactory(pipe.getLabel(), "", pipe.getClass());
			SourceCell cell = factory.createGraphCell();
			PipeModule pipeClone=cell.getPipeModule();
			((PipeSource)pipe).getInputParams().getInputView().update();
			((PipeSource)pipeClone).getInputParams().importParameter(pipe.getInputParams());
			((PipeSource)pipeClone).getInputParams().getInputView().update();
			return cell;
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell#createPorts()
		 */
		protected void createPorts() {
			Vector<PipePort> inputPorts = pipe.getInputPorts();
			Vector<PipePort> outputPorts = pipe.getOutputPorts();
			preferredSize = new Dimension((int) Math.max(outputPorts.size() * hspace, Math.max(
					preferredSize.getWidth(), inputPorts.size() * hspace)), (int) preferredSize.getHeight());
			add(new PipeModulePort(GraphConstants.PERMILLE * 0.05, GraphConstants.PERMILLE * 0.51, ((PipeSource) pipe)
					.getChildPort()));
			add(new PipeModulePort(GraphConstants.PERMILLE * 0.97, GraphConstants.PERMILLE * 0.52, ((PipeSource) pipe)
					.getParentPort()));
			for (int i = 0; i < outputPorts.size(); i++) {
				add(new PipeModulePort(GraphConstants.PERMILLE * (i + 0.5) / outputPorts.size(),
						0.93 * GraphConstants.PERMILLE, outputPorts.get(i)));
			}
		}
	}

	/**
	 * Source tree node.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class SourceNode extends DraggableNode {
		
		/**
		 * Instantiates a new source node.
		 * 
		 * @param factory
		 *            the factory
		 */
		public SourceNode(PipeModuleFactory factory) {
			super(factory);
			this.icon=new ImageIcon(PlaceHolder.class.getResource("source.png"));

		}
	}

	/**
	 * Instantiates a new pipe source factory.
	 * 
	 * @param name
	 *            the name
	 * @param c
	 *            the c
	 */
	public PipeSourceFactory(String name, String description, Class c) {
		super(name, description, c);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Create graph cell for source.
	 * 
	 * @return the pipe module cell
	 */
	public SourceCell createGraphCell() {
		PipeSource p = createPipe();
		return p.createModuleCell();
	}

	/**
	 * Crete pipe module for source.
	 * 
	 * @return the pipe module
	 */
	public PipeSource createPipe() {
		try {
			PipeSource src = (PipeSource) c.newInstance();
			return src;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create tree node for source.
	 * 
	 * @return the draggable node
	 */
	public DraggableNode createTreeNode() {
		return new SourceNode(this);
	}
}
