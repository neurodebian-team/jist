package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class ArrayIntegerDxReaderWriter.
 */
public class ArrayIntegerDxReaderWriter extends ArrayIntegerReaderWriter{
	
	/** The Constant readerWriter. */
	protected static final ArrayIntegerDxReaderWriter readerWriter=new ArrayIntegerDxReaderWriter();
	
	/**
	 * Gets the single instance of ArrayIntegerDxReaderWriter.
	 * 
	 * @return single instance of ArrayIntegerDxReaderWriter
	 */
	public static ArrayIntegerDxReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new array integer dx reader writer.
	 */
	public ArrayIntegerDxReaderWriter(){
		super(new FileExtensionFilter(new String[]{"dx"}));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.ArrayIntegerReaderWriter#readObject(java.io.File)
	 */
	@Override
	protected int[][] readObject(File f) {
		BufferedReader in;
		StringBuffer buff = new StringBuffer();
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));
			
			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		Pattern header=Pattern.compile("items\\s+\\d+\\s+data\\s+follows");
		Matcher m=header.matcher(buff);
		int[][] dat;
		int count=0;
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					count=Integer.parseInt(vals[vals.length-1]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE DATA POINTS");
					return null;
				}
			}
			dat=new int[count][1];
			System.out.println("jist.io"+"\t"+"DATA POINTS "+count);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",count+2);
			
			for(int i=1;i<strs.length-1&&i<=dat.length;i++){
				try {
					dat[i-1][0]=Integer.parseInt(strs[i]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT DATA");
					return null;
				}
			}
			return dat;
		} else return null;
		
	}
	
	/**
	 * Write object.
	 * 
	 * @param dat the dat
	 * @param f the f
	 * 
	 * @return true, if successful
	 */
	protected boolean writeObject(double[][] dat,File f) {
		try {
			BufferedWriter data=new BufferedWriter(new FileWriter(f));
			data.append("items "+dat.length+" data follows\n");
			for(int i=0;i<dat.length;i++){
				for(int j=0;j<dat[i].length;j++){
					data.append(dat[i][j]+" ");
				} 
				data.append("\n");
			}
			data.close();
			return true;
		} catch (IOException e) {
			System.err.println(getClass().getCanonicalName()+e.getMessage());
			return false;
		}	
	}

}
