/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;

/**
 * Main class for creating pipelines.
 * 
 * @author Blake Lucas
 */
public class PipelineLayoutTool extends JFrame implements ActionListener, JistPreferences.PreferenceListener {
	String PLTTitle = "PLT";
	/**
	 * The Class InitializeLayout.
	 */
	protected class InitializeLayout extends SwingWorker<Void, Void> {
		
		/** The layout. */
		PipeLayout layout;
		
		/** The pmonitor. */
		ProgressMonitor pmonitor;
		
		/** The old frame. */
		PipeInternalFrame oldFrame;

		/**
		 * Instantiates a new initialize layout.
		 * 
		 * @param layout
		 *            the layout
		 */
		public InitializeLayout(PipeLayout layout) {
			this.layout = layout;
			pmonitor = new ProgressMonitor(PipelineLayoutTool.getInstance(), "Loading " + layout.getTitle(),
					"Loading....", 0, 0);
		}

		/* (non-Javadoc)
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		protected Void doInBackground() throws Exception {
			try {
				oldFrame = LayoutPanel.getInstance().getActiveFrame();
				LayoutPanel.getInstance().addLayoutFrame();
				LayoutPanel.getInstance().setPipelineLayout(layout, pmonitor);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see javax.swing.SwingWorker#done()
		 */
		public void done() {
			if ((oldFrame != null) && oldFrame.getTitle().equals("Layout 1") && !oldFrame.getPipelineLayout().isDirty()
					&& (oldFrame.getPipelineLayout().getFileLocation() == null)) {
				oldFrame.getPipelineLayout().setFileLocation(new File(""));
				oldFrame.close();
			}
			LayoutPanel.getInstance().getActiveFrame().select();
			pmonitor.close();
			LayoutPanel.getInstance().getPipelineLayout().setDirty(false);
		}
	}

	/** The tool. */
	private static PipelineLayoutTool tool = null;

	/**
	 * Get singleton reference to layout tool.
	 * 
	 * @return the instance
	 */
	public static PipelineLayoutTool getInstance() {
		if (tool == null) {
			tool = new PipelineLayoutTool();
			tool.init();
		}
		return tool;
	}
	public static boolean isInitialized(){
		return (tool!=null);
	}
	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		MipavController.setQuiet(true);
		MipavController.init();
		PipeLibrary.getInstance().loadPreferences();
		PipelineLayoutTool tool = PipelineLayoutTool.getInstance();
		tool.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		for (int i = 1; i < args.length; i++) {
			File f = new File(args[i]);
			PipeLayout layout = PipeLayout.read(f);
			if (layout != null) {
				tool.load(layout);
			}
		}
	}
	public ModuleTreePanel getModulePanel(){
		return modulePanel;
	}
	/** The module panel. */
	private ModuleTreePanel modulePanel;
	
	/** The param panel. */
	private ParameterPanel paramPanel;
	
	/** The layout panel. */
	private LayoutPanel layoutPanel;
	
	/** The module search panel */
	private ModuleSearchPanel searchPanel;
	
	//Let's not keeep track of these - let the user deal with it. 
//	/** The proc manager. */
//	private ArrayList<Process> procManagers = new ArrayList<Process>();
	
	/** The toggle items. */
	private ArrayList<JMenuItem> toggleItems;
	
	/** The recent files. */
	private JMenu recentFiles = null;
	
	/** The recent items. */
	Vector<JMenuItem> recentItems = new Vector<JMenuItem>();
	
	/** The paste item. */
	public JMenuItem removeItem, cutItem, copyItem, undoItem, redoItem, pasteItem, aboutItem;

	/** Disable system.exit on exit - causes close window nad and exit to be the same **/ 
	private boolean hideOnExit = false;
	public void setCloseOnlyOnExit(boolean flag) { hideOnExit = flag;};
	/**
	 * Default constructor.
	 */
	protected PipelineLayoutTool() {
		super();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt) {
		String name = ((JMenuItem) evt.getSource()).getText();
		if (evt.getSource() == cutItem) {
			layoutPanel.getActiveFrame().fireActionPerformed(layoutPanel.getActiveFrame().cut);
		} else if (evt.getSource() == copyItem) {
			layoutPanel.getActiveFrame().fireActionPerformed(layoutPanel.getActiveFrame().copy);
		} else if (evt.getSource() == pasteItem) {
			layoutPanel.getActiveFrame().fireActionPerformed(layoutPanel.getActiveFrame().paste);
		} else if (evt.getSource() == undoItem) {
			layoutPanel.getActiveFrame().fireActionPerformed(layoutPanel.getActiveFrame().undo);
		} else if (evt.getSource() == redoItem) {
			layoutPanel.getActiveFrame().fireActionPerformed(layoutPanel.getActiveFrame().redo);
		} else if (evt.getSource() == removeItem) {
			layoutPanel.getActiveFrame().fireActionPerformed(layoutPanel.getActiveFrame().remove);
		} else if (evt.getSource() == aboutItem) {
			license.JISTLicenseAbout.showDialog();
		}else if (name.equals("New")) {
			LayoutPanel.getInstance().addLayoutFrame();
		} else if (name.equals("Save As...")) {
			LayoutPanel.getInstance().getPipelineLayout().saveAs();
		} else if (name.equals("Save")) {
			LayoutPanel.getInstance().getPipelineLayout().save(true);
		} else if (name.equals("Close")) {
			LayoutPanel.getInstance().getActiveFrame().close();
		} else if (name.equals("Rebuild Library")) {
			modulePanel.rebuild();
		} /*else if (name.equals("Set Library Directory")) {
			File oldDir = PipeLibrary.getInstance().getLibraryPath();
			File newDir = PipeLibrary.askForLibraryDirectory(oldDir);
			if (newDir != null) {
				PipeLibrary.getInstance().setLibraryPath(newDir);
				if (!newDir.equals(oldDir)) {
					modulePanel.rebuild();
				}
			}
		}*/ /*else if (name.equals("Set JRE Location")) {
			File oldJre = new File(JistPreferences.getPreferences().getJre());
			File newJre = JistPreferences.askForJavaExecutable(oldJre);
			if (newJre != null) {
				JistPreferences.getPreferences().setJre(newJre.getAbsolutePath());
				JistPreferences.savePreferences();
			}
		}*/
		else if (name.equals("Global Preferences"))
		{
			if (GlobalPreferencePanel.showDialog(this, JistPreferences.getPreferences())) {
			} 
		}/*
		  else if (name.equals("Clear Image Registry")) {
			MipavController.clearReigstry();
		}*/ else if (name.equals("Open")) {
			PipeLayout layout = PipeLayout.open();
			load(layout);
		} else if (name.equals("Exit")) {
			if(hideOnExit)
				LayoutPanel.getInstance().getActiveFrame().close();
			else
				exit();
			return;
		} else if (name.equals("Process Manager")) {
			PipeLayout layout = LayoutPanel.getInstance().getActiveFrame().getPipelineLayout();
			if (layout.isDirty() || (layout.getFileLocation() == null)) {
				int n = JOptionPane.showOptionDialog(this, "Would you like to save " + layout.getTitle()
						+ " before continuing?", null, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
						null, null);
				if (n == 0) {
					layout.save(true);
				}
			}
			if (layout.getFileLocation() != null) {
				//Save preferences before starting process manager in the event that the preferred extension or other parameters have changed
				JistPreferences.savePreferences();
				ProcessBuilder builder = new ProcessBuilder(JistPreferences.getPreferences().getJre(), "-Xmx1024m",
						"-classpath", ExecutionContext.classpath, "StartJISTProcessManager",
						layout.getFileLocation().getAbsolutePath(), PipeLibrary.getInstance().getLibraryPath()
								.getAbsolutePath());
				
				System.out.println(getClass().getCanonicalName()+"\t"+builder.command());
				System.out.flush();
				try {
					builder.start();
//					procManagers.add(builder.start());
					/*
					InputStream is = procManager.getInputStream();
				       InputStreamReader isr = new InputStreamReader(is);
				       BufferedReader br = new BufferedReader(isr);
				       String line;			
				       for(int i=1;i<10;i++) {
				    	   Thread.sleep(100);
				    	   if(br.ready()) {
				    		   line = br.readLine();

				    		   System.out.println(getClass().getCanonicalName()+"\t"+line);
				    		   System.out.flush();
				    	   }
				       }*/
				       
				} catch (IOException e) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
				} catch (RuntimeException e) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
				} /*catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} */
			}
		} else if (name.equals("Layout Preferences")) {
			PipeLayout layout = LayoutPanel.getInstance().getActiveFrame().getPipelineLayout();
			if (LayoutPreferencePanel.showDialog(this, layout)) {
			}
		} else if (recentItems.contains(evt.getSource())) {
			PipeLayout layout = PipeLayout.read(new File(((JMenuItem) evt.getSource()).getText()));
			load(layout);
		}
		
		
	}

	/**
	 * Create menu items.
	 * 
	 * @return the j menu bar
	 */
	protected JMenuBar createMenuBar() {
		toggleItems = new ArrayList<JMenuItem>();
		JMenuBar menuBar = new JMenuBar();
		JMenuItem menuItem;
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuBar.add(menuFile);
		JMenu menuEdit = new JMenu("Edit");
		menuEdit.setMnemonic(KeyEvent.VK_E);
		menuBar.add(menuEdit);
		JMenu menuAction = new JMenu("Project");
		menuAction.setMnemonic(KeyEvent.VK_P);
		menuBar.add(menuAction);
		menuItem = new JMenuItem("New");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_N);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		menuItem = new JMenuItem("Open");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_O);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		recentFiles = new JMenu("Recent Files");
		menuFile.add(recentFiles);
		toggleItems.add(menuItem = new JMenuItem("Save"));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_S);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		toggleItems.add(menuItem = new JMenuItem("Save As..."));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
		menuItem.setMnemonic(KeyEvent.VK_A);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		toggleItems.add(menuItem = new JMenuItem("Close"));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_C);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		toggleItems.add(menuItem = new JMenuItem("Clear Image Registry"));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_R);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		menuItem = new JMenuItem("Exit");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_X);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		toggleItems.add(menuItem = new JMenuItem("Delete"));
		menuItem.addActionListener(this);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		menuItem.setMnemonic(KeyEvent.VK_D);
		menuEdit.add(menuItem);
		removeItem = menuItem;
		toggleItems.add(menuItem = new JMenuItem("Undo"));
		menuItem.addActionListener(this);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_U);
		menuEdit.add(menuItem);
		undoItem = menuItem;
		toggleItems.add(menuItem = new JMenuItem("Redo"));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_R);
		menuItem.addActionListener(this);
		menuEdit.add(menuItem);
		redoItem = menuItem;
		toggleItems.add(menuItem = new JMenuItem("Cut"));
		menuItem.addActionListener(this);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_T);
		menuEdit.add(menuItem);
		cutItem = menuItem;
		toggleItems.add(menuItem = new JMenuItem("Copy"));
		menuItem.addActionListener(this);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_C);
		menuEdit.add(menuItem);
		copyItem = menuItem;
		toggleItems.add(menuItem = new JMenuItem("Paste"));
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
		menuItem.addActionListener(this);
		menuEdit.add(menuItem);
		pasteItem = menuItem;
		toggleItems.add(menuItem = new JMenuItem("Layout Preferences"));
		menuItem.addActionListener(this);
		menuAction.add(menuItem);
		menuItem = new JMenuItem("Global Preferences");
		menuItem.addActionListener(this);
		menuAction.add(menuItem);
		menuItem = new JMenuItem("Rebuild Library");
		menuItem.addActionListener(this);
		menuAction.add(menuItem);
		/*
		menuItem = new JMenuItem("Set Library Directory");
		menuItem.addActionListener(this);
		menuAction.add(menuItem);
		*/
		toggleItems.add(menuItem = new JMenuItem("Process Manager"));
		menuItem.addActionListener(this);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_M);
		menuAction.add(menuItem);
		
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_H);
		menuBar.add(menuHelp);
		toggleItems.add(aboutItem = new JMenuItem("About"));
		aboutItem.addActionListener(this);			
		menuHelp.add(aboutItem);
		return menuBar;
	}

	/**
	 * Create split panes.
	 * 
	 * @return the component
	 */
	protected Component createPanes() {
		JSplitPane split1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSplitPane split2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		JSplitPane split3 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		paramPanel = ParameterPanel.getInstance();
		layoutPanel = LayoutPanel.getInstance();
		modulePanel = ModuleTreePanel.getInstance();
		searchPanel = ModuleSearchPanel.getInstance(this,modulePanel.tree);

		split3.setTopComponent(searchPanel);
		split3.setBottomComponent(modulePanel);
		split1.setTopComponent(split3);
		split1.setBottomComponent(paramPanel);
		split2.setLeftComponent(split1);
		split2.setRightComponent(layoutPanel);
		split1.setDividerLocation(300);
		split1.setResizeWeight(0.25);
		split1.setOneTouchExpandable(true);
		split2.setDividerLocation(400);
		split2.setResizeWeight(0.75);
		split2.setOneTouchExpandable(true);
		return split2;
	}

	/**
	 * Close all frames and exit safely.
	 */
	public void exit() {
		int n = JOptionPane.showOptionDialog(this, "Are you sure that you want to close the JIST Layout Tool?",null,JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if(n==0){
			LinkedList<PipeInternalFrame> frames = LayoutPanel.getInstance().getFrames();
			boolean success = true;
			while (frames.size() > 0) {
				if (!frames.getFirst().close()) {
					success = false;
					break;
				}
			}
			if (success) {
//				for(Process proc:procManagers){
//					proc.destroy();
//				}
//				procManagers.clear();
				JistPreferences.savePreferences();
				this.setVisible(false);
				if(getDefaultCloseOperation()==WindowConstants.DO_NOTHING_ON_CLOSE){
					System.exit(0);
				}
			}
		}
	}

	/**
	 * Change menu to reflect changes to history.
	 * 
	 * @param prefs
	 *            the prefs
	 */
	public void historyChange(JistPreferences prefs) {
		Vector<File> fileHistory = prefs.getFileHistory();
		for (JMenuItem item : recentItems) {
			item.removeActionListener(this);
		}
		recentItems.clear();
		JMenuItem item;
		recentFiles.removeAll();
		int i = 1;
		for (File f : fileHistory) {
			recentItems.add(item = new JMenuItem(f.getAbsolutePath()));
			item.addActionListener(this);
			if(i<10){
				item.setAccelerator(KeyStroke.getKeyStroke('0' + i, ActionEvent.CTRL_MASK));
			}
			recentFiles.add(item);
			i++;
		}
	}

	/**
	 * Initialize layout tool.
	 */
	public void init() {
		getContentPane().add(createPanes());

		PLTTitle = "JIST Pipeline Layout Tool "+
		"(v"+license.JISTLicenseAbout.getReleaseID()+"-"+license.JISTLicenseAbout.getReleaseBuildDate()
		+")";
		this.setTitle(PLTTitle);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				exit();
			}
		});
		setJMenuBar(createMenuBar());
		setLocation(0, 0);
		setPreferredSize(new Dimension(1200, 768));
		pack();
		setVisible(true);
		LayoutPanel.getInstance().addLayoutFrame();
		JistPreferences.getPreferences().addListener(this);
		historyChange(JistPreferences.getPreferences());
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				PlaceHolder.class.getResource("zoom.gif"))); /**SET ICON HERE**/
	}

	/**
	 * Load layout into layout pane.
	 * 
	 * @param layout
	 *            the layout
	 */
	public void load(PipeLayout layout) {
		if (layout != null) {
			InitializeLayout init = new InitializeLayout(layout);
			init.execute();
		}
	}

	/**
	 * Enable/Disable all layout specific menu items.
	 * 
	 * @param enable
	 *            true if enabled
	 */
	public void setMenuItemsEnabled(boolean enable) {
		updateEditMenu();
		for (JMenuItem item : toggleItems) {
			item.setEnabled(enable);
		}
	}

	/**
	 * Update menu according to the menu settings in the active frame.
	 */
	public void updateEditMenu() {
		PipeInternalFrame frame = layoutPanel.getActiveFrame();
		if (frame != null) {
			copyItem.setEnabled(frame.copy.isEnabled());
			cutItem.setEnabled(frame.cut.isEnabled());
			pasteItem.setEnabled(frame.paste.isEnabled());
			undoItem.setEnabled(frame.undo.isEnabled());
			redoItem.setEnabled(frame.redo.isEnabled());
			removeItem.setEnabled(frame.remove.isEnabled());
		} else {
			copyItem.setEnabled(false);
			cutItem.setEnabled(false);
			pasteItem.setEnabled(false);
			undoItem.setEnabled(false);
			redoItem.setEnabled(false);
			removeItem.setEnabled(false);
		}
	}
	public void updateTitle(){
		PipeInternalFrame activeFrame=layoutPanel.getActiveFrame();
		if(activeFrame!=null){
			this.setTitle(PLTTitle+" ["+PipeLibrary.getHostName()+" - "+ activeFrame.getTitle() + "]");
		} else {
			this.setTitle(PLTTitle+" ["+PipeLibrary.getHostName()+"]");
		}
	}
}
