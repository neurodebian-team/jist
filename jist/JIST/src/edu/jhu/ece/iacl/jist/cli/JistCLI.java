package edu.jhu.ece.iacl.jist.cli;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipePort.type;
import edu.jhu.ece.iacl.jist.pipeline.parameter.JISTInternalParam;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;

public class JistCLI {

	Options parserOptions;
	ProcessingAlgorithm module;
	PipeAlgorithm pipe;
	CommandLine cliCommand;
	static final String cvsversion = "$Revision: 1.6 $".replace("Revision: ", "").replace("$", "").replace(" ", "");
	static String VERSION = cvsversion;
	HashMap<String, ParamModel> paramList;

	public void addOption(String opt, boolean hasArg, String description) {
		parserOptions.addOption(opt, hasArg, description);
	}

	public String getOptionValue(String key) {

		String value = cliCommand.getOptionValue(key);
		if(value==null && cliCommand.hasOption(key))
			return "set";
		return value;

	}


	public static String uniqueString(String suggestedTag,HashMap<String, ParamModel> tags, String base) {
		String []split = suggestedTag.trim().split("\\s");
		String tag = base;
		int i=0;
		if(split.length>0 && split[0].length()>0) {
			tag = split[0];
			i++;
		}

		String curtag = tag;
		while(null!=tags.get(curtag)) {
			i=i+1;
			curtag = tag+i;
		}
		return curtag;
	}

	public JistCLI(ProcessingAlgorithm module) {
		this.module = module;
		parserOptions = new Options();
		ParamCollection inputs = module.getInput();
		paramList = new HashMap<String,ParamModel>();

		parseParamCollection((ParamCollection)inputs,paramList,parserOptions,"in",true);


		ParamCollection outputs = module.getOutput();
		parseParamCollection((ParamCollection)outputs,paramList,parserOptions,"out",false);

		ParamFile procdir = new ParamFile("Processing Directory (default current)",DialogType.DIRECTORY);
		procdir.setShortLabel("Proc");
		procdir.setMandatory(false);		
		parserOptions.addOption("xDir", true, "Request Output: "+procdir.getDescription()+" ["+procdir.getHumanReadableDataType()+"] (optional)");
		ParamFile outfile = new ParamFile("Results File (default output.txt)",DialogType.FILE);
		outfile.setShortLabel("Result");
		outfile.setMandatory(false);
		parserOptions.addOption("xFile", true, "Request Output: "+outfile.getDescription()+" ["+outfile.getHumanReadableDataType()+"] (optional)");
		parserOptions.addOption("h","help",false,"Print this message.");
		parserOptions.addOption("xml",false,"Prints an xml that is compatible as a 3D Slicer Module");
		addPreferenceOptions();
	}

	public JistCLI(PipeAlgorithm pipe) {
		this.pipe = pipe;
		parserOptions = new Options();
		ParamCollection inA= pipe.getInputParams();
		ParamCollection inputs = new ParamCollection();
		// Script parser sets the last input param to the output params. Need to clear
		for(int i=0;i<inA.size()-1;i++) {
			ParamModel p =inA.getValue(i);
			// Avoid these extra dummy parameters. These must be over-ridden.
			if(p.getName().equals("Use Layout Output Directory") && (p instanceof ParamBoolean))
				continue;
			if(p.getName().equals("Show Process Manager") && (p instanceof ParamBoolean))
				continue;
			inputs.add(p);			
		}

		ParamCollection outputs = (ParamCollection) inA.getValue(inA.size()-1);// pipe.getOutputParams();		

		paramList = new HashMap<String,ParamModel>();

		parseParamCollection((ParamCollection)inputs,paramList,parserOptions,"in",true);



		parseParamCollection((ParamCollection)outputs,paramList,parserOptions,"out",false);

		ParamFile procdir = new ParamFile("Processing Directory (default current)",DialogType.DIRECTORY);
		procdir.setShortLabel("Proc");
		procdir.setMandatory(false);		
		parserOptions.addOption("xDir", true, "Request Output: "+procdir.getDescription()+" ["+procdir.getHumanReadableDataType()+"] (optional)");
		ParamFile outfile = new ParamFile("Results File (default output.txt)",DialogType.FILE);
		outfile.setShortLabel("Result");
		outfile.setMandatory(false);
		parserOptions.addOption("xFile", true, "Request Output: "+outfile.getDescription()+" ["+outfile.getHumanReadableDataType()+"] (optional)");
		parserOptions.addOption("h","help",false,"Print this message.");
		addPreferenceOptions();
	}

	private void addPreferenceOptions(){
		parserOptions.addOption("xPrefExt",true,"Set Prefered Output Image Extension");//slicerXMLaddParamCollection() relies on this tag, update there if changed
		parserOptions.addOption("xDefaultMem",true,"Set Set default maximum heap size");
		parserOptions.addOption("xDebugLvl",true,"Set Debug output level[0-9]: 0 = Error messages only, 1 = Major events and warnings, ect. Lvl 5+ maybe cause performance decrease.");
		parserOptions.addOption("xJreLoc",true,"Set location of JRE to use");
		parserOptions.addOption("xMaxProcess",true,"Set default maximum number of processes.");
		parserOptions.addOption("xUseGridEng",true,"Sets the use of the Grid Engine[true/false]");
		parserOptions.addOption("xSavePrefChanges",false,"Saves any changes made to the JIST preference");
	}

	private void parseParamCollection(ParamCollection inputs,
			HashMap<String, ParamModel> paramList2, Options parserOptions2, String preTag,boolean isInput) {
		for(int i=0;i<inputs.size();i++) {
			ParamModel input = inputs.getValue(i);
			if(!input.isHidden() && !(input instanceof JISTInternalParam)) {
//				System.out.println("cli"+"\t"+"### "+i+" ### ----------------");
				//			System.out.println("cli"+"\t"+input.toXML());
				String tag = uniqueString(input.getShortLabel(),paramList,preTag);
				paramList.put(tag, input);

//				System.out.println("cli"+"\t"+tag+" "+input.getShortLabel());
				//				parserOptions.addOption(tag, true, input.getDescription()+" ["+input.getHumanReadableDataType()+"] "+(input.isMandatory()?"(required)":"(optional)"));
				String req ="";
				if(input.isMandatory()) {
					String val = input.probeDefaultValue();
					if(isInput) { 
						if(val==null)
							req = "(required)";
						else
							req = "(default="+val+")";
					}
				} else {
					req = "(optional)";
				}
				//parserOptions.addOption(preTag+tag, isInput, input.getDescription()+" ["+input.getHumanReadableDataType()+"] "+req);
				parserOptions.addOption(preTag+tag, true, input.getDescription()+" ["+input.getHumanReadableDataType()+"] "+req);
				input.setCliTag(preTag+tag);
//				System.out.println("cli"+"\t"+"foo:::"+preTag+tag+"|"+input.getOwner().getName());
				input.setPortType((!isInput?type.OUTPUT:type.INPUT));
			}
			if(input instanceof ParamCollection) {
				parseParamCollection((ParamCollection)input,paramList,parserOptions,preTag,isInput);
			}
		}

	}

	public String getHumanReadableHelpMessage() {

		StringWriter sw = new StringWriter();
		PrintWriter io = new PrintWriter(sw);
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		//		formatter.printHelp("safddsafasfd", parserOptions,true);
		AlgorithmInformation info=null;
		if(module!=null)
			info = module.getAlgorithmInformation();
		else 
			info = pipe.getAlgorithm().getAlgorithmInformation();
		formatter.printHelp(io,
				HelpFormatter.DEFAULT_WIDTH,
				(module==null?"layoutfile":module.getClass().getCanonicalName()),
				"\n" +
				info.getLabel()+" "+info.getVersion()+" "+info.getStatusString()+"\n" +
				info.getDescription()+"\n" +
				"\n",
				parserOptions,
				HelpFormatter.DEFAULT_LEFT_PAD,HelpFormatter.DEFAULT_DESC_PAD,
				"\n" +
				"Provided by: JIST (Java Image Science Toolkit) Command Line Interface version " + JistCLI.VERSION +
				"\n" +
				"http://www.nitrc.org/projects/jist/"
				,true); 		
		return sw.toString();
	}

	public String getSlicerXML(){
		String XML=tagWithXML("Developer Tools","category",true);

		AlgorithmInformation info = module.getAlgorithmInformation();
		XML = XML + tagWithXML(info.getLabel(),"title",false)+"\n";
		String Citations = "";

		for(int i = 0; i< info.getCitations().size();i++){
			Citations = Citations + info.getCitations().get(i).getText()+"\n";
		}
		XML = XML + tagWithXML(info.getDescription()+"\n"+Citations,"description",true);
		XML = XML + tagWithXML(info.getVersion()+"."+info.getStatusString(),"version",false)+"\n";

		String Authors = "";
		for(int i = 0; i< info.getCitations().size();i++){
			Authors = Authors + info.getAuthors().get(i).toString()+"\n";
		}
		XML = XML + tagWithXML(Authors, "contributor", true);
		XML = XML + tagWithXML(info.getWebsite(),"documentation-url",true);

		ParamCollection inputs = module.getInput();
		ParamCollection outputs = module.getOutput();

		//write inputs and outputs
		XML = XML + slicerXMLaddParamCollection(inputs, true);
		XML = XML + slicerXMLaddParamCollection(outputs, false);

		//add final tags
		XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + tagWithXML(XML, "executable", true);
		return XML;
	}

	public String slicerXMLaddParamCollection(ParamCollection inputs, boolean isInput){
		boolean paramFlag = false;
		boolean firstParam = true;
		String XML = "";
		for(int i = 0; i < inputs.size();i++){
			ParamModel inParam = inputs.getValue(i);

			if(inParam instanceof ParamCollection){

				//Close a parameter tag if one is open
				if(paramFlag){
					XML = XML + "</parameters>\n";
					paramFlag = false;
				}

				if(firstParam){
					XML = XML + "<parameters>\n";	
					firstParam=false;
				}
				else XML = XML + "<parameters advanced=\"true\">\n";

				XML = XML + tagWithXML(inParam.getName(), "label",false)+"\n";
				XML = XML + tagWithXML(inParam.getDescription(), "description",false)+"\n";
				XML = XML + slicerXMLparseCollection((ParamCollection)inParam, isInput);
				XML = XML + "</parameters>\n";
			}
			else if(!inParam.isHidden() && !(inParam instanceof JISTInternalParam)){
				//Start a parameter tag if one isn't open
				if(!paramFlag){

					if(firstParam){
						XML = XML + "<parameters>\n";	
						firstParam=false;
					}
					else XML = XML + "<parameters advanced=\"true\">\n";

					if(isInput){ 
						XML = XML + "<label>Main</label>\n";
						XML = XML + "<description>Main Parameters</description>\n";
					}
					else {
						XML = XML + "<label>Output</label>\n";
						XML = XML + "<description>Output Parameters</description>\n";
						XML = XML + "<string-enumeration>\n";
						XML = XML + "<label>Output File Type</label>\n";
						XML = XML + "<description>Output File Type</description>\n";
						XML = XML + "<longflag>--xPrefExt</longflag>\n";
						XML = XML + "<element>nrrd</element>\n";
						XML = XML + "<default>nrrd</default>\n";
						XML = XML + "</string-enumeration>\n";


					}
					paramFlag = true;
				}
				XML = XML + slicerXMLaddParam(inParam, isInput);
			}

		}
		//Close a parameter tag if one is open
		if(paramFlag){
			XML = XML + "</parameters>\n";
			paramFlag = false;
		}

		return XML;
	}

	public String slicerXMLparseCollection(ParamCollection inCollect, boolean isInput){

		String XML="";

		for(int i = 0; i < inCollect.size(); i++){
			ParamModel inParam = inCollect.getValue(i);
			if( inParam instanceof ParamCollection ){
				XML = XML + slicerXMLparseCollection((ParamCollection)inParam, isInput);
			}
			else if(!inParam.isHidden() && !(inParam instanceof JISTInternalParam)){
				XML = XML + slicerXMLaddParam(inParam, isInput);
			}
		}

		return XML;
	}

	public String slicerXMLaddParam(ParamModel inParam, boolean isInput){
		String XML = "";

		XML = XML + tagWithXML("--"+inParam.getCliTag(), "longflag", false) +"\n";
		XML = XML + tagWithXML(inParam.getName(), "label", false)+"\n";
		//XML = XML + tagWith(inParam.getDescription(), "description",false);

		String req ="";
		if(isInput) { 
			/*
	    if(inParam.isMandatory()) {

			req = "(required)";
		} else {
			req = "(optional)";
		}*/
		}		
		XML = XML + tagWithXML(inParam.getDescription() + req, "description",false);

		String paramType = inParam.getHumanReadableDataType();

		if(paramType.equals("file")){
			if(isInput)	XML = XML + "\n" + tagWithXML("input","channel",false);
			else XML = XML + "\n" + tagWithXML("output","channel",false);

			if(inParam instanceof ParamVolume)paramType = "image";
		}else if(paramType.contains("option")){
			String optionChoices[] = paramType.split(":|\\|");// look for ":" or "|" symbol as dividers
			for(int k = 1; k < optionChoices.length; k++){ //starts at 1 because first is always "option"
				XML = XML + "\n" + tagWithXML("\""+optionChoices[k]+"\"","element",false);
			}
			paramType = "string-enumeration";
		}else if(paramType.equals("boolean")){
			XML = XML + "\n" + tagWithXML("\"true\"","element",false);
			XML = XML + "\n" + tagWithXML("\"false\"","element",false);
			paramType = "string-enumeration";
		} 

		String val = inParam.probeDefaultValue();
		if(val != null){
		if(paramType.equals("string-enumeration") || paramType.equals("file") ) XML = XML + "\n"+tagWithXML("\""+val+"\"", "default", false);
		else XML = XML + "\n"+tagWithXML(val, "default", false);

		}

		XML =tagWithXML(XML, paramType, true);
		return XML;

	}

	public String tagWithXML(String in, String tag, boolean breakAtEnd){

		String XML = "<" + tag +">";
		if(breakAtEnd) XML = XML + "\n";
		XML = XML + in; 
		if(breakAtEnd) XML = XML + "\n";
		XML = XML + "</"  +tag + ">";
		if(breakAtEnd) XML = XML + "\n";

		return XML;
	}

	String parseStatusString = null; 
	boolean parseError;
	private File myOutFile;
	private File myOutDir;
	public boolean encounteredParseError() {
		return parseError;
	}

	public void unmarshal() {
		parseError = false;
		ParamCollection inputs;
		if(module!=null)
			inputs = module.getInput();
		else 
			inputs = pipe.getInputParams();
		ParamModel[] optionList = paramList.values().toArray(new ParamModel[0]);
		StringWriter parseStatus = new StringWriter();
		if(module!=null)
			parseStatus.append(module.getClass().getCanonicalName()+"\n");
		else
			parseStatus.append(pipe.getLabel()+"\n");	
		for(int i=0;i<optionList.length;i++) 
		{
			ParamModel option = optionList[i];
			if(!option.isHidden() && !(option instanceof JISTInternalParam)) {
				String tag = option.getCliTag();

				String arg = cliCommand.getOptionValue(tag);
				if((arg == null) && (option.isMandatory() && !option.isOutputPort()) && (null==option.probeDefaultValue())) {
					parseStatus.append("PARSE ERROR: Missing required tag: "+tag+"\n");
					parseError=true;
				}
				if(arg!=null) {
					option.setXMLValue(arg);
					parseStatus.append("\t"+tag+" = "+arg+" = "+option.toString()+"\n");
				}	else {
					parseStatus.append("\t"+tag+" = <default> = "+option.toString()+"\n");
				}
			}
		}	

		String procdir = cliCommand.getOptionValue("xDir");
		if(procdir==null) {
			try {
				procdir = new File(".").getCanonicalPath();
			} catch (IOException e) {
				procdir =null;
			}
		}  
		if(procdir == null) {
			parseStatus.append("PARSE ERROR: Unable to find canonical output directory: "+"outProc"+"\n");
			parseError=true;
		} else {
			parseStatus.append("\t"+"outProc"+" = "+procdir+"\n");
		}
		if(module!=null)
			module.setOutputDirectory(new File(procdir));
		myOutDir = new File(procdir);

		String outfile = cliCommand.getOptionValue("xFile");
		if(outfile==null) {
			outfile = "output.txt";
		}  
		if(outfile == null) {
			parseStatus.append("PARSE ERROR: Unable to set output meta file: "+"outResult"+"\n");
			parseError=true;
		} else {
			parseStatus.append("\t"+"outResult"+" = "+outfile+"\n");
		}
		if(module!=null)
			module.setOutputMetaFile(new File(outfile));
		myOutFile = new File(outfile);
		parseStatusString  = parseStatus.toString();	


		//Preference Options
		String preferedExt = cliCommand.getOptionValue("xPrefExt");
		if(preferedExt != null)JistPreferences.getPreferences().setPreferredExtension(preferedExt);

		if(cliCommand.getOptionValue("xDefaultMem") != null){
			int defaultMem = Integer.parseInt(cliCommand.getOptionValue("xDefaultMem"));
			if(defaultMem > 0 )JistPreferences.getPreferences().setDefaultMemory(defaultMem);
		}

		if(cliCommand.getOptionValue("xDebugLvl") != null){
			int debugLvl = Integer.parseInt(cliCommand.getOptionValue("xDebugLvl"));
			if(debugLvl >= 0 && debugLvl <= 9)JistPreferences.getPreferences().setDebugLevel(debugLvl);
		}

		String jreLoc = cliCommand.getOptionValue("xJreLoc");
		if(jreLoc != null)JistPreferences.getPreferences().setJre(jreLoc);

		if(cliCommand.getOptionValue("xMaxProcess") != null){
			int maxProcess = Integer.parseInt(cliCommand.getOptionValue("xMaxProcess"));
			if(maxProcess > 0 )JistPreferences.getPreferences().setDefaultMaxProcesses(maxProcess);
		}

		if(cliCommand.getOptionValue("xUseGridEng") != null){
			boolean useGridEng = Boolean.parseBoolean(cliCommand.getOptionValue("xUseGridEng"));
			JistPreferences.getPreferences().setUseGridEngine(useGridEng);
		}

		if(cliCommand.hasOption("xSavePrefChanges"))JistPreferences.savePreferences();

	}

	public ProcessingAlgorithm getModule() {
		return module;
	}

	public PipeAlgorithm getPipe() {
		return pipe;
	}

	public File getOutFile() {
		return myOutFile;
	}

	public File getOutDir() {
		return myOutDir;
	}

	public void parse(String[] args) throws ParseException {
		CommandLineParser parser = new PosixParser();
		cliCommand = parser.parse(parserOptions, args);
	}

	public boolean showHelp() {
		if(cliCommand!=null) {
			return cliCommand.hasOption("help");
		}
		return true;
	}

	public boolean checkSlicerXMLoption(){
		if(cliCommand!=null) {
			return cliCommand.hasOption("xml");
		}
		return false;		
	}

	public String getParseStatus() {
		// TODO Auto-generated method stub
		return parseStatusString;
	}
}
