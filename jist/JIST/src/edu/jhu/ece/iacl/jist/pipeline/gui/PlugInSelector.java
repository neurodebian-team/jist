/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleFactory;
import edu.jhu.ece.iacl.jist.pipeline.gui.ModuleTreePanel.DraggableTreeCellRenderer;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;
import gov.nih.mipav.view.MipavUtil;

/**
 * Plug-In to select any plug-in in the MAPS library.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PlugInSelector extends JDialog implements ActionListener, TreeSelectionListener, MouseListener,
		ComponentListener {
	String PISTitle = "Title Not Set";
	private static ModuleSearchPanel mySearch=null;
	/**
	 * Initalize plug-in library.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class InitializeWorker extends PipeLibrary.InitializeTask {
		
		/** The pane. */
		JScrollPane pane = null;

		/**
		 * Instantiates a new initialize worker.
		 * 
		 * @param parent
		 *            the parent
		 * @param pane
		 *            the pane
		 */
		public InitializeWorker(Component parent, JScrollPane pane) {
			super(parent);
			this.pane = pane;			
		}

		
		/**
		 * Build tree from modules.
		 */
		public void done() {
			top = library.getTreeRoot();
			tree = new JTree(top);
			tree.setEditable(false);
			if (treePaths != null) {
				tree.setSelectionPaths(treePaths);
			}
			pane.setViewportView(tree);
			DraggableTreeCellRenderer renderer = new DraggableTreeCellRenderer();
			tree.setCellRenderer(renderer);
			renderer.setLeafIcon(null);
			tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			tree.addTreeSelectionListener((TreeSelectionListener) parent);
			tree.addMouseListener((MouseListener) parent);
			mySearch.setTree(tree);
			pane.revalidate();
		}
	}

	/**
	 * Rebuild library.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class RebuildWorker extends PipeLibrary.RebuildTask {
		
		/**
		 * Instantiates a new rebuild worker.
		 * 
		 * @param parent
		 *            the parent
		 * @param remove
		 *            the remove
		 */
		public RebuildWorker(Component parent, boolean remove) {
			super(parent, remove);
		}

		/**
		 * Rebuild tree from modules.
		 */
		protected void done() {
			top = library.getTreeRoot();
			tree = new JTree(top);
			tree.setRootVisible(false);
			tree.setEditable(false);
			getContentPane().remove(oldPanel);
			getContentPane().add(oldPanel = createPanel());
			((JDialog) parent).invalidate();
			((JDialog) parent).pack();
		}
	}


	/** The tree paths. */
	protected static TreePath[] treePaths = null;

	/**
	 * Get selected module factory.
	 * 
	 * @param owner
	 *            the owner
	 * @return the pipe algorithm factory
	 */
	public static PipeAlgorithmFactory askForAlgorithm(Frame owner) {
		PlugInSelector sd = new PlugInSelector(owner);
		PipeAlgorithmFactory factory = sd.getSelectedFactory();
		if (factory != null) {
			return factory;
		} else {
			return null;
		}
	}

	/** The cancel button. */
	private JButton okButton, cancelButton;
	
	/** The algo. */
	protected DefaultMutableTreeNode top, algo;
	
	/** The tree. */
	protected JTree tree = null;
	
	/** The library. */
	protected PipeLibrary library;
	
	/** The old panel. */
	protected JPanel oldPanel;
	
	/** The selected factory. */
	protected PipeAlgorithmFactory selectedFactory;
	
	/** The set dir item. */
	protected JMenuItem rebuildItem, setDirItem;
	
	/** Insert module into active frame if a module is double clicked. */
	int mouseX, mouseY;

	/**
	 * Constructor.
	 * 
	 * @param owner
	 *            frame that owns this dialog
	 */
	protected PlugInSelector(Frame owner) {
		super(owner, "Title Not Set", true);
		PISTitle = "JIST Process Manager "+
		"(v"+license.JISTLicenseAbout.getReleaseID()+"-"+license.JISTLicenseAbout.getReleaseBuildDate()
		+")";
		setTitle(PISTitle);
		library = PipeLibrary.getInstance();
		this.setJMenuBar(createMenuBar());
		getContentPane().add(oldPanel = createPanel());
		pack();
		setLocationRelativeTo(owner);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.addComponentListener(this);
		setVisible(true);
	}

	/**
	 * Respond to if user presses Ok or Cancel.
	 * 
	 * @param e
	 *            the e
	 */
	public void actionPerformed(ActionEvent e) {
		if (okButton == e.getSource()) {
			if (tree.getSelectionPath() != null) {
				MutableTreeNode node = (MutableTreeNode) tree.getSelectionPath().getLastPathComponent();
				if ((node != null) && (node instanceof DraggableNode)) {
					PipeModuleFactory factory = ((DraggableNode) node).getFactory();
					if (factory instanceof PipeAlgorithmFactory) {
						setSelectedFactory((PipeAlgorithmFactory) factory);
					}
				}
			}
		} else if (cancelButton == e.getSource()) {
			treePaths = tree.getSelectionPaths();
			setVisible(false);
		} else if (rebuildItem == e.getSource()) {
			rebuild();
		} else if (setDirItem == e.getSource()) {
			File oldDir = library.getLibraryPath();
			File newDir = PipeLibrary.askForLibraryDirectory(oldDir);
			if (newDir != null) {
				library.setLibraryPath(newDir);
				if (!newDir.equals(oldDir)) {
					rebuild();
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
	 */
	public void componentHidden(ComponentEvent e) {
		treePaths = tree.getSelectionPaths();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
	 */
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentResized(java.awt.event.ComponentEvent)
	 */
	public void componentResized(ComponentEvent e) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
	 */
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
	}

	/**
	 * Create menu bar for plug-ins.
	 * 
	 * @return menu bar
	 */
	protected JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		JMenu menuFile = new JMenu("Actions");
		menuFile.setMnemonic(KeyEvent.VK_A);
		menuBar.add(menuFile);
		rebuildItem = new JMenuItem("Rebuild Library");
		rebuildItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		rebuildItem.setMnemonic(KeyEvent.VK_R);
		rebuildItem.addActionListener(this);
		menuFile.add(rebuildItem);
		setDirItem = new JMenuItem("Set Library Directory");
		setDirItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
		setDirItem.setMnemonic(KeyEvent.VK_L);
		setDirItem.addActionListener(this);
		menuFile.add(setDirItem);
		return menuBar;
	}

	/**
	 * Create dialog panel.
	 * 
	 * @return dialog panel
	 */
	protected JPanel createPanel() {
		JPanel myPanel = new JPanel(new BorderLayout());
		okButton = new JButton("Ok");
		okButton.addActionListener(this);
		okButton.setMinimumSize(MipavUtil.defaultButtonSize);
		okButton.setPreferredSize(MipavUtil.defaultButtonSize);
		cancelButton = new JButton("Cancel");
		cancelButton.setMinimumSize(MipavUtil.defaultButtonSize);
		cancelButton.setPreferredSize(MipavUtil.defaultButtonSize);
		cancelButton.addActionListener(this);
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		myPanel.add(buttonPanel, BorderLayout.SOUTH);
		myPanel.add(createTreePanel(), BorderLayout.CENTER);
		myPanel.add(mySearch=ModuleSearchPanel.getInstance(this,
				tree),BorderLayout.NORTH);
		return myPanel;
	}

	/**
	 * Create tree to display modules.
	 * 
	 * @return the j component
	 */
	protected JComponent createTreePanel() {
		JScrollPane pane = new JScrollPane();
		pane.setPreferredSize(new Dimension(350, 350));
		InitializeWorker worker = new InitializeWorker(this, pane);
		if (PipeLibrary.getInstance().getTreeRoot() != null) {
			worker.done();
		} else {
			worker.execute();
		}
		return pane;
	}

	/**
	 * Get selected module factory.
	 * 
	 * @return the selected factory
	 */
	public synchronized PipeAlgorithmFactory getSelectedFactory() {
		return selectedFactory;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent evt) {
		if (SwingUtilities.isLeftMouseButton(evt)) {
			if ((evt.getClickCount() == 2) && (tree.getSelectionPath() != null)) {
				MutableTreeNode node = (MutableTreeNode) tree.getSelectionPath().getLastPathComponent();
				if ((node != null) && (node instanceof DraggableNode)) {
					PipeModuleFactory factory = ((DraggableNode) node).getFactory();
					if (factory instanceof PipeAlgorithmFactory) {
						setSelectedFactory((PipeAlgorithmFactory) factory);
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Rebuild library.
	 */
	protected void rebuild() {
		int n = JOptionPane.showOptionDialog(this, "Would you like to clean existing module definitions as well?",
				null, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
		RebuildWorker worker = new RebuildWorker(this, (n == 0));
		worker.execute();
	}

	/**
	 * Set selected module factory and close dialog.
	 * 
	 * @param factory
	 *            the factory
	 */
	public void setSelectedFactory(PipeAlgorithmFactory factory) {
		synchronized(this) {
			selectedFactory = factory;
		}
		treePaths = tree.getSelectionPaths();
		setVisible(false);
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
	 */
	public void valueChanged(TreeSelectionEvent arg0) {
		// TODO Auto-generated method stub
	}
}
