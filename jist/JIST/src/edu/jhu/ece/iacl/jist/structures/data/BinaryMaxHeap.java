package edu.jhu.ece.iacl.jist.structures.data;

import java.util.PriorityQueue;

/**
 * Binary Max Heap uses Java implemention with reverse comparator.
 * 
 * @param <E> Comparable class
 * 
 * @author Blake Lucas
 */
public class BinaryMaxHeap <E extends Comparable<? super E>> extends PriorityQueue<E>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new binary max heap.
	 */
	public BinaryMaxHeap(){
		super(0,new ReverseComparator<E>());
	}
}

