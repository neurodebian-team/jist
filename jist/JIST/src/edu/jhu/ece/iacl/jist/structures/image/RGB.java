package edu.jhu.ece.iacl.jist.structures.image;

import java.io.IOException;

import edu.jhu.ece.iacl.jist.io.LEFileReader;

// TODO: Auto-generated Javadoc
/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Dec 6, 2005
 * Time: 4:44:08 PM
 * To change this template use Options | File Templates.
 */
public class RGB {
    
    /** The b. */
    public byte r, g, b;
    
    /** The usb. */
    private USbyte usb;
    
    /**
     * Instantiates a new rGB.
     * 
     * @param R the r
     * @param G the g
     * @param B the b
     */
    public RGB(byte R, byte G, byte B) {
    	usb = new USbyte();
        r= R; g=G; b=B;
    }
    
    /**
     * Read.
     * 
     * @param fp the fp
     * 
     * @return the rGB
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static RGB read(LEFileReader fp) throws IOException {
        byte r,g,b;
        r = (byte)fp.readByte();
        g = (byte)fp.readByte();
        b = (byte)fp.readByte();
        
        return new RGB(r,g,b);
    }
}
