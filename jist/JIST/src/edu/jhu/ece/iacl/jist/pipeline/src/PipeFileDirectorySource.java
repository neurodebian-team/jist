/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.InvalidJistMergeException;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through all files in specified directory.
 * 
 * @author Blake Lucas
 */
public class PipeFileDirectorySource extends PipeSource {

	/** The dir param. */
	protected ParamFile dirParam;

	/** The include param. */
	protected ParamBoolean includeParam;

	/** The filter rule. */
	protected ParamOption filterRule;

	/** The filter expression. */
	protected ParamString filterExpression;

	/** The file extension. */
	protected ParamString fileExtension;

	/** The search depth. */
	protected ParamInteger searchDepth;

	/** The file parameter */
	protected ParamFile fileParam;

	/** The images. */
	transient protected Vector<File> fileList;

	/** The index. */
	transient protected int index;

	
	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);				
//		Element em;
//		em = document.createElement("dirParam");
//		dirParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		em = document.createElement("includeParam");
//		includeParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);		
//		em = document.createElement("filterRule");
//		filterRule.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		em = document.createElement("filterExpression");
//		filterExpression.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		em = document.createElement("fileExtension");
//		fileExtension.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		em = document.createElement("searchDepth");
//		searchDepth.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		em = document.createElement("fileParam");
//		fileParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		
		
		dirParam =(ParamFile) inputParams.getFirstChildByName("Directory");// new ParamFile();
//		dirParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"dirParam"));
		includeParam =(ParamBoolean) inputParams.getFirstChildByName("Include Sub-directories");// new ParamBoolean();
//		includeParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"includeParam"));
		filterRule = (ParamOption) inputParams.getFirstChildByName("Filter Rule");//new ParamOption();
//		filterRule.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filterRule"));
		filterExpression = (ParamString) inputParams.getFirstChildByName("Filter Expression");//new ParamString();
//		filterExpression.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filterExpression"));
		fileExtension = (ParamString) inputParams.getFirstChildByName("File Extension");//new ParamString();
//		fileExtension.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileExtension"));
		searchDepth = (ParamInteger) inputParams.getFirstChildByName("Max Directory Depth");//new ParamInteger();
//		searchDepth.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"searchDepth"));
		fileParam =(ParamFile) outputParams.getFirstChildByName("File");// new ParamFile();
//		fileParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileParam"));
		getParentPort().setParameter(fileParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipeFileDirectorySource() {
		super();
		getParentPort().setParameter(fileParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File Directory");
		group.setName("filedirectory");
		group.setCategory("File");
		group.add(dirParam = new ParamFile("Directory", ParamFile.DialogType.DIRECTORY));
		group.add(includeParam = new ParamBoolean("Include Sub-directories", true));
		group.add(searchDepth = new ParamInteger("Max Directory Depth", 0, 100000, 1));
		group.add(fileExtension = new ParamString("File Extension"));
		fileExtension.setValue("txt");
		group.add(filterRule = new ParamOption("Filter Rule", new String[] { "Contains", "Matches","Begins With", "Ends With" }));
		group.add(filterExpression = new ParamString("Filter Expression"));
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the parameter collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File");
		group.add(fileParam = new ParamFile("File"));
		return group;
	}

	/**
	 * Get file parameter.
	 * 
	 * @return the output param
	 */
	public ParamFile getOutputParam() {
		return fileParam;
	}

	/**
	 * Returns true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || (index < fileList.size()));
	}

	/**
	 * Iterate through files.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (index < fileList.size()) {
					getOutputParam().setValue(fileList.get(index++));
					// System.out.println(getClass().getCanonicalName()+"\t"+"SET VALUE "+index+"/"+images.size()+"
					// "+getOutputParam().getFile());
					push();
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			// System.out.println(getClass().getCanonicalName()+"\t"+"RESET NO HAS NEXT");
			reset();
			isReset = true;
			return false;
		}
	}
	protected void findMatchingFiles(){
		fileList = new Vector<File>();
		File dir = dirParam.getValue();
		if (dir == null) {
			return;
		}
		boolean recurse = includeParam.getValue();
		int rule = filterRule.getIndex();
		String exp = filterExpression.getValue();
		LinkedList<File> dirs = new LinkedList<File>();
		LinkedList<Integer> depths = new LinkedList<Integer>();
		String extension = fileExtension.getValue();
		int maxDepth = searchDepth.getInt();
		ProgressMonitor monitor= new ProgressMonitor(null, "Searching directory "+dir.getAbsolutePath(),"Searching...", 0,2);
		monitor.setMillisToDecideToPopup(100);
		if (recurse) {
			dirs.add(dir);
			depths.add(0);
			File d;
			int depth;
			while (dirs.size() > 0) {
				d = dirs.remove();
				depth = depths.remove();
				if ((d == null) || (depth > maxDepth)) {
					continue;
				}
				File[] files = d.listFiles();
				if (files == null) {
					continue;
				}
				if(monitor.isCanceled())break;
				monitor.setProgress(1);
				monitor.setNote(d.getAbsolutePath());
				for (File f : files) {
					if (f.isDirectory()) {
						dirs.add(f);
						depths.add(depth + 1);
					} else {
						String ext = FileReaderWriter.getFileExtension(f);
						boolean accept = true;
						if (exp.length() > 0) {
							switch (rule) {
							case 0:
								if (FileReaderWriter.getFileName(f).indexOf(exp) < 0) {
									accept = false;
								}
								break;
							case 1:
								if (!FileReaderWriter.getFileName(f).matches(exp)) {
									accept = false;
								}
								break;
							case 2:
								if (!FileReaderWriter.getFileName(f).startsWith(exp)) {
									accept = false;
								}
								break;
							case 3:
								if (!FileReaderWriter.getFileName(f).endsWith(exp)) {
									accept = false;
								}
								break;
							}
						}
						if (accept && (ext.equals(extension) || (extension.length() == 0))&&f.isFile()) {
							fileList.add(f);
						}
					}
				}
			}
		} else {
			File[] files = dir.listFiles();
			for (File f : files) {
				String ext = FileReaderWriter.getFileExtension(f);
				boolean accept = true;
				if (exp.length() > 0) {
					switch (rule) {
					case 0:
						if (FileReaderWriter.getFileName(f).indexOf(exp) < 0) {
							accept = false;
						}
						break;
					case 1:
						if (!FileReaderWriter.getFileName(f).matches(exp)) {
							accept = false;
						}
						break;
					case 2:
						if (!FileReaderWriter.getFileName(f).startsWith(exp)) {
							accept = false;
						}
						break;
					case 3:
						if (!FileReaderWriter.getFileName(f).endsWith(exp)) {
							accept = false;
						}
						break;
					}
				}
				if (accept && (ext.equals(extension) || (extension.length() == 0))&&f.isFile()) {
					fileList.add(f);
				}
			}
		}
		monitor.close();
		Collections.sort(fileList);
	}
	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		findMatchingFiles();
		index = 0;
		if (fileList.size() > 0) {
			fileParam.setValue(fileList.get(index++));

			push();

		}
	}
	

}
