/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Invalid Parameter Exception is thrown when a parameter value does not
 * validate.
 * 
 * @author Blake Lucas
 */
public class InvalidParameterException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The param. */
	private PipePort param;
	
	/** The extra message. */
	private String extraMessage = "";

	/**
	 * Default constructor.
	 * 
	 * @param param
	 *            invalid parameter
	 */
	public InvalidParameterException(PipePort param) {
		this.param = param;
	}

	/**
	 * Defautl constructor.
	 * 
	 * @param param
	 *            invalid parameter
	 * @param extraMessage
	 *            additional message
	 */
	public InvalidParameterException(PipePort param, String extraMessage) {
		this.param = param;
		this.extraMessage = extraMessage;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	public String getMessage() {
		return "<HTML>" + param.getLabel() + ": Value " + param.toString() + " is invalid.<BR>" + extraMessage
				+ "</HTML>";
	}

	/**
	 * Gets the parameter.
	 * 
	 * @return the parameter
	 */
	public PipePort getParameter() {
		return param;
	}
}
