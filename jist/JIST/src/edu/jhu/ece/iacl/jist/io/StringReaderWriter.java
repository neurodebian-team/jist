package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

// TODO: Auto-generated Javadoc
/**
 * The Class StringReaderWriter.
 */
public class StringReaderWriter extends FileReaderWriter<String>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final StringReaderWriter readerWriter=new StringReaderWriter();
	
	/**
	 * Gets the single instance of StringReaderWriter.
	 * 
	 * @return single instance of StringReaderWriter
	 */
	public static StringReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new string reader writer.
	 */
	public StringReaderWriter(){
		super(new FileExtensionFilter(new String[]{}));
	}
	
	/**
	 * Instantiates a new string reader writer.
	 * 
	 * @param f the f
	 */
	public StringReaderWriter(FileExtensionFilter f) {super(f);};
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected String readObject(File f) {
		BufferedReader in;
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));
			StringBuffer buff = new StringBuffer();
			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
			in.close();
			return buff.toString();
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	@Override
	protected File writeObject(String str, File f) {
		try {
			BufferedWriter data=new BufferedWriter(new FileWriter(f));
			data.append(str);
			data.close();
			return f;
			} catch (IOException e) {
				System.err.println(getClass().getCanonicalName()+e.getMessage());
				return null;
			}	
	}

}
