/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.net.URI;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeFactory;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataIntent;
import edu.jhu.ece.iacl.jist.structures.image.Voxel;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Parameter volume specifies a CubicVolume with restrictions for the dimensions
 * and data type.
 * 
 * @author Blake Lucas
 */
public class ParamVolume extends ParamObject<ImageData> {

	/** The rows. */
	protected int rows = -1;

	/** The cols. */
	protected int cols = -1;

	/** The slices. */
	protected int slices = -1;

	/** The components. */
	protected int components = 1;

	/** The voxel type. */
	protected VoxelType voxelType;

	/** The image intent - need for interface to other utilities **/ 
	protected int imageIntent = ImageDataIntent.NIFTI_INTENT_NONE; 

	/** The dirty image. */
	//	protected transient boolean dirtyImage = false;

	/** The dirty volume. */
	//	protected transient boolean dirtyVolume = false;

	/** The img. */
	//	protected transient ModelImage img = null;

	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);
		 Element em;				
		 em = document.createElement("rows");		
		 em.appendChild(document.createTextNode(rows+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("cols");		
		 em.appendChild(document.createTextNode(cols+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("slices");		
		 em.appendChild(document.createTextNode(slices+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("components");		
		 em.appendChild(document.createTextNode(components+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("voxelType");		
		 em.appendChild(document.createTextNode(voxelType+""));
		 parent.appendChild(em);
		 
		 em = document.createElement("imageIntent");		
		 em.appendChild(document.createTextNode(imageIntent+""));
		 parent.appendChild(em);
		 return true;
	 }
	 
	 public void xmlDecodeParam(Document document, Element parent) {
			super.xmlDecodeParam(document, parent);
			rows= Integer.valueOf(JistXMLUtil.xmlReadTag(parent, "rows"));
			cols= Integer.valueOf(JistXMLUtil.xmlReadTag(parent, "cols"));
			slices= Integer.valueOf(JistXMLUtil.xmlReadTag(parent, "slices"));
			components= Integer.valueOf(JistXMLUtil.xmlReadTag(parent, "components"));
			try {
			voxelType= VoxelType.valueOf(JistXMLUtil.xmlReadTag(parent, "voxelType"));
			} catch(IllegalArgumentException e){
				voxelType=null;
			}
			imageIntent= Integer.valueOf(JistXMLUtil.xmlReadTag(parent, "imageIntent"));
								
		}
	 
	/** The volume. */
	protected transient ImageData volume = null;

	/**
	 * Default constructor.
	 */
	public ParamVolume() {
		super(DialogType.FILE);
		setReaderWriter(new ImageDataReaderWriter());
		if (extensionFilter == null) {
			setExtensionFilter(readerWriter.getExtensionFilter());
		}
		this.voxelType = null;
		this.factory = new ParamVolumeFactory(this);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamVolume(String name) {
		this(name, null);
		setName(name);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 * @param type
	 *            voxel type restriction
	 */
	public ParamVolume(String name, VoxelType type) {
		this(type);
		setName(name);
	}

	/**
	 * Construct volume parameter with a restriction on the dimensions and data
	 * type.
	 * 
	 * @param name
	 *            volume name
	 * @param type
	 *            data type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamVolume(String name, VoxelType type, int rows, int cols, int slices, int components) {
		this(type, rows, cols, slices, components);
		setName(name);
	}

	/**
	 * Construct volume parameter with a restriction on the data type.
	 * 
	 * @param type
	 *            data type restriction
	 */
	public ParamVolume(VoxelType type) {
		this();
		this.voxelType = type;
		this.factory = new ParamVolumeFactory(this);
	}

	/**
	 * Construct volume parameter with a restriction on the dimensions and data
	 * type.
	 * 
	 * @param type
	 *            data type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamVolume(VoxelType type, int rows, int cols, int slices, int components) {
		this(type);
		this.rows = rows;
		this.slices = slices;
		this.cols = cols;
		this.components = components;
	}



	/**
	 * Clear volume parameter.
	 */
	public void clear() {

		file = null;
		uri=null;		
		obj=null;
		volume = null;
		//		dirtyVolume = false;
		//		dirtyImage = false;
	}

	/**
	 * Clone object.
	 * 
	 * @return the param volume
	 */
	public ParamVolume clone() {
		ParamVolume param = new ParamVolume();
		param.rows = rows;
		param.slices = slices;
		param.components = components;
		param.cols = cols;
		param.voxelType = voxelType;
		param.file = this.file;
		param.uri=this.uri;
		/*
		if(obj==null && img != null) {

			obj = new ImageDataMipavWrapper(img);
		}*/
		//		param.obj=(ImageData)obj.clone();
		/*
		if(obj!=null) {
			param.obj = this.obj.clone();
			if(obj instanceof ImageDataMipav)
				param.img=((ImageDataMipav)param.obj).getModelImageDirect();
			else 
				param.img=param.obj.getModelImageCopy();
		}
		 */
		if(volume!=null)
		param.volume=this.volume.clone();
		else
			param.volume=null;
		param.readerWriter = this.readerWriter;
		param.extensionFilter = this.extensionFilter;
		param.setName(this.getName());
		param.label=this.label;
		param.setValue(this.getValue());
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;		
		return param;
	}

	/**
	 * Compare restrictions of parameters.
	 * 
	 * @param model
	 *            the model
	 * @return the int
	 */
	public int compareTo(ParamModel model) {
		if (model instanceof ParamVolume) {
			ParamVolume modelVol = (ParamVolume) model;
			// This field is mandatory and thus immediately more restrictive
			if (!this.isHidden() && modelVol.isHidden()) {
				return 1;
			}
			// Compare the dimensions to see if they match
			if (((rows == -1) || (rows == modelVol.rows)) && ((cols == -1) || (cols == modelVol.cols))
					&& ((slices == -1) || (slices == modelVol.slices))
					&& ((components == -1) || (components == modelVol.components))) {
				// Compare the data types for the volumes
				return (int) Math.signum(Voxel.getRestriction(voxelType) - Voxel.getRestriction(modelVol.voxelType));
			} else {
				// this volume is more restrictive than the model
				return 1;
			}
		}
		return 1;
	}

	/**
	 * Dispose of volume in memory.
	 */
	public void dispose() {
		JistLogger.logOutput(JistLogger.FINE, "Dispose volume: "+getName());		
		volume.dispose(); volume = null;
	}



	/**
	 * Compare parameter volumes by model image names.
	 * 
	 * @param obj
	 *            a parameter volume or a string image name
	 * @return the model image names for the two objects are the same
	 */
	public boolean equalVolume(Object obj) {
		if (obj instanceof ParamVolume) {
			return getName()==((ParamVolume)obj).getName();			
		} else if (obj instanceof String) {
			//// Updated 10/14/2009
			// Assume that the string is a ModelImageName. assume that the param name is the same as 
			// the modelimage name
			return getName().equals((String)obj);
		} else {
			return false;
		}
	}

	/**
	 * Get column restriction.
	 * 
	 * @return columns
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Get component restriction.
	 * 
	 * @return components
	 */
	public int getComponents() {
		return components;
	}

	@Deprecated/*
	public ModelImage getModelImageIfOpen(){
		throw new RuntimeException("getModelImageIfOpen: This function is known to be buggy and should not be used.");
		/*		Vector<ModelImage> images = MipavController.getImages();

		File f1=this.getValue();
		if(f1==null)return null;
		Refresher.getInstance().pauseAll();
		int in=f1.getAbsolutePath().lastIndexOf(".");
		String fileNoExt1=(in>=0)?f1.getAbsolutePath().substring(0,in):f1.getAbsolutePath();
		for (ModelImage img : images) {
			File f2 = FileReaderWriter.getFullFileName(img);
			in=(f2==null)?-1:f2.getAbsolutePath().lastIndexOf(".");
			String fileNoExt2=(in>=0)?f2.getAbsolutePath().substring(0,in):((f2!=null)?f2.getAbsolutePath():null);
			if (f2 != null && fileNoExt2.equals(fileNoExt1)) {
				img.getFileInfo(0).setFileDirectory(f1.getParent());
				MipavController.setDefaultWorkingDirectory(new File(f1
						.getParent()));
				Refresher.getInstance().resumeAll();
				return img;
			}
		}
		Refresher.getInstance().resumeAll();
		return null;
	 */
	//	}
	/**
	 * Get model image used to store volume.
	 * 
	 * @return the model image
	 */
	/*
	public ModelImage getModelImageCache() {
		file=super.getValue();
		if ((img == null) || dirtyImage) {
			if (volume != null) {
				// Create model image from Cubic volume if image has changed or
				// is
				// invalid
				img = ImageDataMipav.createModelImage(volume, img);
				// Set the current file location to the ModelImage's location
				file = FileReaderWriter.getFullFileName(img);
				this.uri=(file!=null)?file.toURI():null;
				dirtyImage = false;
			} else if (file != null) {
				if (readerWriter == null) {
					readerWriter = new CubicVolumeReaderWriter();
				}
				ImageData tmp=readerWriter.read(file);

				volume=tmp;					

				dirtyVolume = false;

			}
		}
		return img;
	}
	 */
	/**
	 * Get volume.
	 * 
	 * @return the object
	 */
	public ImageData getObject() {
		return getImageData();
	}

	/**
	 * Get row restriction.
	 * 
	 * @return rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Get slice restriction.
	 * 
	 * @return slices
	 */
	public int getSlices() {
		return slices;
	}

	/**
	 * Get file location of Model Image if it exists.
	 * 
	 * @return the value
	 */
	public File getValue() {
		file=super.getValue();
		if(file==null && volume!=null)
			return FileReaderWriter.getFullFileName(volume);
		return file;
	}

	public String getName() {
		String str = super.getName();
		if(str==null) {
			File f = getValue();
			if(f!=null)
				str = f.getAbsolutePath();
		}
		return str;
	}

	/**
	 * Get the CubicVolume specified.
	 * 
	 * @return volume
	 */
	/**
	 * Get the CubicVolume specified.
	 * 
	 * @return volume
	 */
	public ImageData getImageData() {
		return getImageData(true);
	}

	public ImageData getImageData(boolean loadIfNotLoaded) {
		file=super.getValue();
		if (((volume == null) || (volume.isNotAvailable())) && (file != null)) {
			if(!loadIfNotLoaded)
				return null;
			if (readerWriter == null) {
				readerWriter = new ImageDataReaderWriter();
			}
			volume=readerWriter.read(file);		
		}		
		return volume;
	}

	/**
	 * Get voxel type restriction.
	 * 
	 * @return type voxel type
	 */
	public VoxelType getVoxelType() {
		return voxelType;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(1);
		connectible = true;
		factory = new ParamVolumeFactory(this);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile#isCompatible(edu.jhu.ece.iacl.jist.pipeline.PipePort)
	 */
	public boolean isCompatible(PipePort model) {
		return ((model instanceof ParamVolume) || (model instanceof ParamVolumeCollection));
	}

	/**
	 * Set volume.
	 * 
	 * @param obj
	 *            the obj
	 */
	public void setObject(ImageData obj) {
		setValue(obj);
	}

	/**
	 * Set the value for the parameter. A volume parameter will accept a
	 * CubicVolume, the string name for the image, the file location of the
	 * image or a ModelImage.
	 * 
	 * @param value
	 *            parameter value
	 */
	public void setValue(ImageData value) {
		// Specify volume

		volume = value; //new ImageDataMipav(img);

		file = FileReaderWriter.getFullFileName(value);
		this.uri=(file!=null)?file.toURI():null;

	}

	/**
	 * Set volume filename.
	 * 
	 * @param file
	 *            the file
	 */
	public void setValue(File file) {
		if ((this.file != null) && this.file.equals(file)) {
			return;
		}
		if (this.file != null) {
			clear();
		}
		this.file = file;
		this.uri=(file!=null)?file.toURI():null;
		volume=null;
	}
	/**
	 * Set volume filename.
	 * 
	 * @param file
	 *            the file
	 */
	public void setValue(URI uri) {
		if(uri.toString().compareToIgnoreCase("null")==0) uri=null;
		if ((this.uri != null) && this.uri.equals(uri)) {
			return;
		}
		if (this.uri != null) {
			clear();
		}
		this.uri=uri;
		
		this.file=(uri!=null)?new File(uri):null;		
		volume=null;
	}
	//	/**
	//	 * Set volume.
	//	 * 
	//	 * @param value
	//	 *            model image
	//	 */
	//	public void setValue(ModelImage value) {
	//		// Set the model image
	//		img = value;
	//		// Need to update CubicVolume if getValue() is called
	//		dirtyVolume = true;
	//		dirtyImage = false;
	//		file = FileReaderWriter.getFullFileName(img);
	//
	//		this.uri=(file!=null)?file.toURI():null;
	//	}

	/**
	 * Set volume filename.
	 * 
	 * @param value
	 *            the value
	 */
	/*
	public void setValue(String value) {
		// Select image from Mipav registry
		ModelImage tmp = MipavController.getImageByName(value);
		if (tmp != null) {
			img = tmp;
			volume=new ImageDataMipav(img);
			// Need to update CubicVolume if getValue() is called
			dirtyVolume = false;
			dirtyImage = false;
			file = FileReaderWriter.getFullFileName(img);
			this.uri=(file!=null)?file.toURI():null;
		}
	}
	 */
	/**
	 * Get volume description.
	 * 
	 * @return the string
	 */
	public String toString() {
		file=super.getValue();
		if (file != null) {
			return file.getAbsolutePath();
		}
		if ((volume != null) && (volume.getName() != null)) {
			return volume.getName();
		} else {
			return getName(); // Rather than "none" 10/14/2009
		}
	}

	/**
	 * Validate volume.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		File f=getValue();
		if(f!=null&&f.exists()&&volume==null){
			return;
		} else {
			ImageData vol =getImageData(this.loadAndSaveOnValidate);
			if(vol==null && loadAndSaveOnValidate==false) {// Do not validate if the flag is set and volume is null
				if(f==null) {
					// Still need to report that an input hasn't been specified.
					throw new InvalidParameterException(this, "No Volume Specified.");
				}
				return;
			}
			if ((!this.isHidden() && (vol == null))
					|| ((vol != null) && (voxelType != null) && (vol.getType() != null) && ((Voxel
							.getRestriction(voxelType) - Voxel.getRestriction(vol.getType())) > 0))
							|| ((rows != -1) && (vol != null) && (rows != vol.getRows()))
							|| ((cols != -1) && (vol != null) && (cols != vol.getCols()))
							|| ((slices != -1) && (vol != null) && (slices != vol.getSlices()))
							|| ((components != -1) && (vol != null) && (components != vol.getComponents()))) {
				if (vol != null) {
					throw new InvalidParameterException(this, "Found: Type=" + vol.getType() + " Dimensions=("
							+ vol.getRows() + "," + vol.getCols() + "," + vol.getSlices() + "," + vol.getComponents()
							+ ")\nRequired: Type=" + voxelType + " Dimensions=(" + rows + "," + cols + "," + slices + ","
							+ components + ")");
				} else {
					throw new InvalidParameterException(this, "No Volume Specified.");
				}
			}
		}
	}

	public void writeAndFreeNow(ProcessingAlgorithm src) {
		File dir = new File(src.getOutputDirectory()+
				File.separator+
				edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(src.getAlgorithmName()));
		dir.mkdir();;
		writeAndFreeNow(dir);
	}
	
	public void writeAndFreeNow(File outDir) {
		// Do not write if the 
		if((!loadAndSaveOnValidate) && (getImageData(false)!=null)) {
			JistLogger.logOutput(JistLogger.INFO, "WriteAndFreeNow: "+getName());
			//			System.out.println(getClass().getCanonicalName()+"\t"+"writeAndFreeNow: volume: "+getName());
			ImageDataReaderWriter rw  = ((ImageDataReaderWriter) getReaderWriter());//ImageDataReaderWriter.getInstance();

			//		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
			File dir = outDir;
		
			File f1 = rw.getPreferredImageDataFileName(getImageData(), dir);

			// write out the result
			//			System.out.println(getClass().getCanonicalName()+"\t"+"Writing to file: " + f1);
			File f2 = rw.write(getImageData(), f1);
			volume.dispose(); volume=null;
			if(f2!=null){			
				setValue(f2);
				JistLogger.logOutput(JistLogger.INFO, "WriteAndFreeNow Complete.");
				//				System.out.print("Success!\n");
			}
		}
	}

	public int getDataIntent() {
		return this.imageIntent;
	}

	public void setDataIntent(int intent) {
		imageIntent = intent; 
	}
}
