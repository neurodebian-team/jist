/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.Port;

import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeChildPort;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipeParentPort;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.gui.LayoutPanel;

/**
 * Graph model that has been modified to clone cell modules correctly.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeGraphModel extends DefaultGraphModel {
	
	/* (non-Javadoc)
	 * @see org.jgraph.graph.DefaultGraphModel#cloneCells(java.lang.Object[])
	 */
	public Map cloneCells(Object[] cells) {
		Map map = new Hashtable();
		// Add Cells to Queue
		PipeLayout layout = LayoutPanel.getInstance().getPipelineLayout();
		for (int i = 0; i < cells.length; i++) {

			if (cells[i] instanceof PipeModuleCell) {
				PipeModuleCell cell = ((PipeModuleCell) cells[i]);
				PipeModuleCell cellClone = ((PipeModuleCell) cell.clone());
				map.put(cell, cellClone);
				PipeModule mod = cellClone.getPipeModule();
				LayoutPanel.getInstance().getActiveFrame().getGraph().createUniqueName(mod);
				if(mod instanceof PipeAlgorithm){
					((PipeAlgorithm)mod).getInputParams().getHeader().stampUUID();
				}
				layout.add(mod);
				Vector<PipePort> inPort = cell.getPipeModule().getInputPorts();
				Vector<PipePort> inPortClone = mod.getInputPorts();
				for (int j = 0; j < inPort.size(); j++) {
					if (inPort.get(j).getGraphPort() != null) {
						map.put(inPort.get(j).getGraphPort(), inPortClone.get(j).getGraphPort());
						i++;
					}
				}
				Vector<PipePort> outPort = cell.getPipeModule().getOutputPorts();
				Vector<PipePort> outPortClone = mod.getOutputPorts();
				for (int j = 0; j < outPort.size(); j++) {
					if (outPort.get(j).getGraphPort() != null) {
						map.put(outPort.get(j).getGraphPort(), outPortClone.get(j).getGraphPort());
						i++;
					}
				}
				if (cell.getPipeModule() instanceof PipeSource) {
					PipeChildPort childPort = ((PipeSource) cell.getPipeModule()).getChildPort();
					PipeParentPort parentPort = ((PipeSource) cell.getPipeModule()).getParentPort();
					PipeChildPort childPortClone = ((PipeSource) mod).getChildPort();
					PipeParentPort parentPortClone = ((PipeSource) mod).getParentPort();
					map.put(childPort.getGraphPort(), childPortClone.getGraphPort());
					map.put(parentPort.getGraphPort(), parentPortClone.getGraphPort());
					i += 2;
				}
			} else {
				map.put(cells[i], cloneCell(cells[i]));
			}
			
		}
		// Replace Parent and Anchors
		Iterator it = map.entrySet().iterator();
		Object obj, cell, parent;
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			obj = entry.getValue();
			cell = entry.getKey();
			// Replaces the cloned cell's parent with the parent's clone
			parent = getParent(cell);
			if (parent != null) {
				parent = map.get(parent);
			}
			if (parent != null) {
				((DefaultMutableTreeNode) parent).add((DefaultMutableTreeNode) obj);
			}
			// Replaces the anchors for ports
			if (obj instanceof Port) {
				Object anchor = ((Port) obj).getAnchor();
				if (anchor != null) {
					((Port) obj).setAnchor((Port) map.get(anchor));
				}
			}
		}
		return map;
	}
	
	/**
	 * Creates a shallow copy of the cell including a copy of the user object.
	 * Subclassers can override the cloneUserObject to provide a custom user
	 * object cloning mechanism.
	 * 
	 * @param cellObj
	 *            the cell obj
	 * @return the object
	 */
	protected Object cloneCell(Object cellObj) {
		System.out.println(getClass().getCanonicalName()+"\t"+"CLONE CELL "+cellObj.getClass().getCanonicalName()+" "+cellObj);
		if (cellObj instanceof DefaultGraphCell) {
			// Clones the cell
			DefaultGraphCell cell = (DefaultGraphCell) cellObj;
			DefaultGraphCell clone = (DefaultGraphCell) cell.clone();
			// Clones the user object
			clone.setUserObject(cloneUserObject(cell.getUserObject()));
			return clone;
		}
		return cellObj;
	}
	protected Object cloneUserObject(Object userObject) {
		System.out.println(getClass().getCanonicalName()+"\t"+"CLONE OBJECT "+userObject.getClass().getCanonicalName()+" "+userObject);
		return userObject;
	}
}
