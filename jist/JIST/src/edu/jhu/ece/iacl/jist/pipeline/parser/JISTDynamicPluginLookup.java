package edu.jhu.ece.iacl.jist.pipeline.parser;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;


public interface JISTDynamicPluginLookup {
	public void init(String str) throws AlgorithmRuntimeException;		
	public String getClassTag();
}
