/**
 * Java Image Science Toolkit (JIST) Image Analysis and Communications
 * Laboratory & Laboratory for Medical Image Computing & The Johns Hopkins
 * University http://www.nitrc.org/projects/jist/ This library is free software;
 * you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version. The
 * license is available for reading at: http://www.gnu.org/copyleft/lgpl.html
 */
package edu.jhu.ece.iacl.jist.pipeline.parser;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.JFileChooser;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.ParsingException;
import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithmGroup;
import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamArgsOrder;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamExecutable;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSwitch;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeDoubleSweepSource;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeFileCollectionSource;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeFileDirectorySource;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeSelectionSource;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeStringListSource;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeSurfaceCollectionSource;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeVolumeCollectionSource;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmExecutableAdapter;

/**
 * The Class LoniPipeParser.
 */
public class LoniPipeParser implements ScriptParser {
	/** The Constant MODULE_TAG. */
	public static final String MODULE_TAG = "module";
	/** The Constant MODULE_GROUP_TAG. */
	public static final String MODULE_GROUP_TAG = "moduleGroup";
	/** The Constant CONNECTIONS_TAG. */
	public static final String CONNECTIONS_TAG = "connections";
	/** The Constant CONNECTION_TAG. */
	public static final String CONNECTION_TAG = "connection";
	/** The Constant INPUT_TAG. */
	public static final String INPUT_TAG = "input";
	/** The Constant OUTPUT_TAG. */
	public static final String OUTPUT_TAG = "output";
	/** The Constant PIPELINE_TAG. */
	public static final String PIPELINE_TAG = "pipeline";
	/** The Constant FORMAT_TAG. */
	public static final String FORMAT_TAG = "format";
	/** The Constant ENUMERATION_TAG. */
	public static final String ENUMERATION_TAG = "enumeration";
	/** The Constant FILE_TYPE_TAG. */
	public static final String FILE_TYPE_TAG = "filetype";
	/** The Constant VALUE_TAG. */
	public static final String VALUE_TAG = "value";
	/** The Constant FILE_TYPES_TAG. */
	public static final String FILE_TYPES_TAG = "fileTypes";
	/** The Constant VALUES_TAG. */
	public static final String VALUES_TAG = "values";
	/** The Constant DATA_MODULE_TAG. */
	public static final String DATA_MODULE_TAG = "dataModule";
	/** The Constant AUTHOR_TAG. */
	public static final String AUTHOR_TAG = "author";
	/** The Constant AUTHORS_TAG. */
	public static final String AUTHORS_TAG = "executableAuthors";
	/** The Constant CITATIONS_TAG. */
	public static final String CITATIONS_TAG = "citations";
	/** The Constant CITATION_TAG. */
	public static final String CITATION_TAG = "citation";
	/** The Constant DATA_TAG. */
	public static final String DATA_TAG = "data";
	/** The Constant URI_TAG. */
	public static final String URI_TAG = "uri";
	/** The Constant METADATA_TAG. */
	public static final String METADATA_TAG = "metadata";
	/** The input aliases. */
	protected Hashtable<String, String> inputAliases;
	/** The output aliases. */
	protected Hashtable<String, String> outputAliases;

	/**
	 * Default constructor.
	 */
	public LoniPipeParser() {
	}

	/**
	 * Open layout from script file.
	 * 
	 * @param f
	 *            script file
	 * @return pipe layout
	 */
	public PipeLayout openLayout(File f) {
		if (FileReaderWriter.getFileExtension(f).equals("pipe")) {
			PipeLayout layout = parseLayout(f);
			layout.setFileLocation(new File(f.getParentFile(), FileReaderWriter.getFileName(f) + "."+JistPreferences.getPreferences().getDefaultLayoutExtension()));
			return layout;
		} else {
			return null;
		}
	}

	/**
	 * Open LONI script from file.
	 * 
	 * @return the pipe algorithm
	 */
	public PipeAlgorithm openPipeAlgorithm() {
		JFileChooser loadDialog = new JFileChooser("Open LONI Pipeline");
		loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(new String[] { "pipe" }));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return parsePipeAlgorithm(loadDialog.getSelectedFile());
		} else {
			return null;
		}
	}

	/**
	 * Parse connections.
	 * 
	 * @param elem
	 *            xml element
	 * @param algo
	 *            parent algorithm group
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected void parseConnections(Element elem, PipeAlgorithmGroup algo) throws IOException {
		Elements elems = elem.getChildElements();
		Element child;
		String source, sink;
		ParamModel inputParam;
		ParamModel outputParam;
		for (int i = 0; i < elems.size(); i++) {
			child = elems.get(i);
			if (CONNECTION_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				source = child.getAttributeValue("source");
				sink = child.getAttributeValue("sink");
				while (outputAliases.get(source) != null) {
					source = outputAliases.get(source);
				}
				while (inputAliases.get(sink) != null) {
					sink = inputAliases.get(sink);
				}
				outputParam = algo.getOutputParams().getFirstChildByName(source);
				inputParam = algo.getInputParams().getFirstChildByName(sink);
				if ((inputParam != null) && (outputParam != null)) {
					PipeConnector conn = PipeConnector.connect(outputParam, inputParam);
					if (conn == null) {
						System.err.println(getClass().getCanonicalName()+algo.getLabel() + " COULD NOT CONNECT "
								+ inputParam.getClass().getCanonicalName() + " " + inputParam.getName() + " "
								+ outputParam.getClass().getCanonicalName() + " " + outputParam.getName());
					}
				}
			}
		}
	}

	/**
	 * Parse connections.
	 * 
	 * @param elem
	 *            xml element
	 * @param layout
	 *            parent layout
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected void parseConnections(Element elem, PipeLayout layout) throws IOException {
		Elements elems = elem.getChildElements();
		Element child;
		String source, sink;
		ParamModel inputParam;
		ParamModel outputParam;
		for (int i = 0; i < elems.size(); i++) {
			child = elems.get(i);
			if (CONNECTION_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				source = child.getAttributeValue("source");
				sink = child.getAttributeValue("sink");
				while (outputAliases.get(source) != null) {
					source = outputAliases.get(source);
				}
				while (inputAliases.get(sink) != null) {
					sink = inputAliases.get(sink);
				}
				outputParam = layout.getModuleOutputParam(source);
				inputParam = layout.getModuleInputParam(sink);
				if ((inputParam != null) && (outputParam != null)) {
					PipeConnector conn = PipeConnector.connect(outputParam, inputParam);
					if (conn == null) {
						System.err.println(getClass().getCanonicalName()+"COULD NOT CONNECT " + inputParam.getClass().getCanonicalName() + " "
								+ inputParam.getName() + " " + outputParam.getClass().getCanonicalName() + " "
								+ outputParam.getName());
					}
				}
			}
		}
	}

	/**
	 * Parse data sources and connect them to modules.
	 * 
	 * @param elems
	 *            xml elements
	 * @return list of sources
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected ArrayList<PipeModule> parseDataSources(Elements elems) throws IOException {
		ArrayList<PipeModule> sources = new ArrayList<PipeModule>();
		PipeSource source;
		for (int i = 0; i < elems.size(); i++) {
			Element child = elems.get(i);
			if (DATA_MODULE_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				boolean isSource = (child.getAttributeValue("source").equalsIgnoreCase("true"));
				if (isSource) {
					int posX = Integer.parseInt(child.getAttributeValue("posX"));
					int posY = Integer.parseInt(child.getAttributeValue("posY"));
					String name = child.getAttributeValue("id");
					String label = child.getAttributeValue("name");
					String type = child.getAttributeValue("type");
					source = null;
					if (type.equalsIgnoreCase("Number")) {
						source = new PipeDoubleSweepSource();
					} else if (type.equalsIgnoreCase("String")) {
						source = new PipeStringListSource();
					} else if (type.equalsIgnoreCase("File")) {
						Elements fileTypes = child.getFirstChildElement(FILE_TYPES_TAG).getChildElements();
						ArrayList<String> extNames = new ArrayList<String>();
						for (int j = 0; j < fileTypes.size(); j++) {
							if (FILE_TYPE_TAG.equalsIgnoreCase(fileTypes.get(j).getQualifiedName())) {
								extNames.add(fileTypes.get(j).getAttributeValue("extension").toLowerCase());
							}
						}
						if (ModelImageReaderWriter.getInstance().getExtensionFilter().accept(extNames)
								|| extNames.contains("byte") || extNames.contains("float")) {
							source = new PipeVolumeCollectionSource();
						} else if (SurfaceReaderWriter.getInstance().getExtensionFilter().accept(extNames)) {
							source = new PipeSurfaceCollectionSource();
						} else {
							source = new PipeFileCollectionSource();
						}
					} else if (type.equalsIgnoreCase("Enumerated")) {
						source = new PipeSelectionSource();
					} else if (type.equalsIgnoreCase("Directory")) {
						source = new PipeFileDirectorySource();
					}
					if (source != null) {
						Elements childElems = child.getChildElements();
						for (int j = 0; j < childElems.size(); j++) {
							Element prop = childElems.get(j);
							if (OUTPUT_TAG.equalsIgnoreCase(prop.getQualifiedName())) {
								ParamModel outputPort = source.getOutputParams().getValue(0);
								outputPort.setName(prop.getAttributeValue("id"));
								outputPort.setLabel(prop.getAttributeValue("name"));
							}
						}
						source.setName(name);
						source.setLabel(label);
						source.setBounds(new Rectangle2D.Double(posX, posY, 150, 70));
						sources.add(source);
					}
				}
			}
		}
		return sources;
	}

	/**
	 * Parse layout.
	 * 
	 * @param f
	 *            script file
	 * @return pipe layout
	 */
	protected PipeLayout parseLayout(File f) {
		PipeAlgorithmGroup rootGroup = null;
		PipeLayout layout = new PipeLayout();
		inputAliases = new Hashtable<String, String>();
		outputAliases = new Hashtable<String, String>();
		try {
			Builder parser = new Builder();
			Document doc = parser.build(f.toURI().toString());
			Element root = doc.getRootElement();
			if (PIPELINE_TAG.equalsIgnoreCase(root.getQualifiedName())) {
				Element rootElem = root.getFirstChildElement(MODULE_GROUP_TAG);
				rootGroup = parseModuleGroup(rootElem);
				ArrayList<PipeModule> sources = parseDataSources(rootElem.getChildElements());
				Element connElem = root.getFirstChildElement(CONNECTIONS_TAG);
				layout.add(rootGroup);
				for (PipeModule mod : sources) {
					layout.add(mod);
				}
				if (connElem != null) {
					parseConnections(connElem, layout);
				}
				return layout;
			}
		} catch (ParsingException ex) {
			System.out.println(getClass().getCanonicalName()+"\t"+f.toURI().toString() + " is not well-formed.");
			System.out.println(getClass().getCanonicalName()+"\t"+ex.getMessage());
		} catch (IOException ex) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Due to an IOException, the parser could not print " + f.toURI().toString());
		}
		return null;
	}
	private String correctLocation(String location){
		
		int index = location.lastIndexOf("//");
		if (index >= 0) {
			location = location.substring(index + 1, location.length());
		}
		
		return location;
	}
	/**
	 * Parse element.
	 * 
	 * @param elem
	 *            xml element
	 * @return pipe algorithm
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected PipeAlgorithm parseModule(Element elem) throws IOException {
		Elements elems = elem.getChildElements();
		String label = elem.getAttributeValue("name");
		String name = label.replaceAll(" ", "_");
		String desc = elem.getAttributeValue("description");
		String affiliation = elem.getAttributeValue("package");
		String version = elem.getAttributeValue("version");
		PipeAlgorithm algo = new PipeAlgorithm();
		MedicAlgorithmExecutableAdapter adapter = new MedicAlgorithmExecutableAdapter();
		int posX = Integer.parseInt(elem.getAttributeValue("posX"));
		int posY = Integer.parseInt(elem.getAttributeValue("posY"));
		ParamCollection inputParams = adapter.getInput();
		inputParams.setName(name);
		inputParams.setLabel(label);
		ParamCollection outputParams = adapter.getOutput();
		outputParams.setName(name);
		outputParams.setLabel(label);
		Element child;
		String location = correctLocation(elem.getAttributeValue("location"));
		AlgorithmInformation info = adapter.getAlgorithmInformation();
		info.init(adapter);
		info.setDescription(desc);
		info.setAffiliation(affiliation);
		info.setVersion(version);
		ParamFile exec = new ParamExecutable(new File(location));
		inputParams.add(exec);
		LinkedList<AlgorithmAuthor> authors = info.getAuthors();
		LinkedList<Citation> citations = info.getCitations();
		Hashtable<Integer, String> orderHash = new Hashtable<Integer, String>();
		for (int i = 0; i < elems.size(); i++) {
			child = elems.get(i);
			if (INPUT_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				parseParam(child, inputParams, orderHash);
			} else if (OUTPUT_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				parseParam(child, outputParams, orderHash);
			} else if (AUTHORS_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				Elements children = child.getChildElements();
				for (int j = 0; j < children.size(); j++) {
					child = children.get(j);
					if (AUTHOR_TAG.equalsIgnoreCase(child.getQualifiedName())) {
						authors.add(new AlgorithmAuthor(child.getAttributeValue("fullName"), child
								.getAttributeValue("email"), child.getAttributeValue("website")));
					}
				}
			} else if (CITATIONS_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				Elements children = child.getChildElements();
				for (int j = 0; j < children.size(); j++) {
					child = children.get(j);
					if (CITATION_TAG.equalsIgnoreCase(child.getQualifiedName())) {
						citations.add(new Citation(child.getValue()));
					}
				}
			} else if (URI_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				info.setWebsite(child.getValue());
			} else if (METADATA_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				Elements children = child.getChildElements();
				for (int j = 0; j < children.size(); j++) {
					child = children.get(j);
					if (DATA_TAG.equalsIgnoreCase(child.getQualifiedName())
							&& child.getAttributeValue("key").equalsIgnoreCase("__creationDateKey")) {
						info.setCreationDate(new Date(child.getAttributeValue("value")));
					}
				}
			}
		}
		// Move the execution time parameter to end of parameters
		ParamModel execParam = outputParams.getValue(1);
		outputParams.remove(execParam);
		outputParams.add(execParam);
		Vector<Integer> keys = new Vector(orderHash.keySet());
		Collections.sort(keys);
		ArrayList<String> vals = new ArrayList<String>();
		for (int key : keys) {
			vals.add(orderHash.get(key));
		}
		inputParams.add(new ParamArgsOrder("Argument Order", vals));
		algo.setAlgorithm(adapter);
		Dimension d = PipeModuleCell.getPreferredSize(inputParams, outputParams);
		algo.setBounds(new Rectangle2D.Double(posX, posY, d.getWidth(), d.getHeight()));
		ParamCollection outputClone = outputParams.clone();
		outputClone.setHidden(true);
		inputParams.add(outputClone);
		return algo;
	}

	/**
	 * Parse module from file.
	 * 
	 * @param f
	 *            script file
	 * @return pipe algorithm
	 */
	protected PipeAlgorithm parseModule(File f) {
		PipeAlgorithmGroup rootGroup = null;
		inputAliases = new Hashtable<String, String>();
		outputAliases = new Hashtable<String, String>();
		System.out.println(getClass().getCanonicalName()+"\t"+"READING " + f);
		try {
			Builder parser = new Builder();
			Document doc = parser.build(f.toURI().toString());
			Element root = doc.getRootElement();
			if (PIPELINE_TAG.equalsIgnoreCase(root.getQualifiedName())) {
				Element rootElem = root.getFirstChildElement(MODULE_GROUP_TAG);
				rootGroup = parseModuleGroup(rootElem);
				Element connElem = root.getFirstChildElement(CONNECTIONS_TAG);
				if (connElem != null) {
					parseConnections(connElem, rootGroup);
				}
				if (rootGroup.size() == 0) {
					return null;
				} else if (rootGroup.size() == 1) {
					return rootGroup.getGroupModules().firstElement();
				} else {
					return rootGroup;
				}
			}
		} catch (ParsingException ex) {
			System.out.println(getClass().getCanonicalName()+"\t"+f.toURI().toString() + " is not well-formed.");
			System.out.println(getClass().getCanonicalName()+"\t"+ex.getMessage());
			ex.printStackTrace();
		} catch (IOException ex) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Due to an IOException, the parser could not print " + f.toURI().toString());
		}
		return rootGroup;
	}

	/**
	 * parse algorithm group.
	 * 
	 * @param elem
	 *            xml element
	 * @return pipe algorithm group
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected PipeAlgorithmGroup parseModuleGroup(Element elem) throws IOException {
		String name = elem.getAttribute("name").getValue();
		PipeAlgorithmGroup group = new PipeAlgorithmGroup();
		group.setName(name);
		group.setLabel(name);
		Elements elems = elem.getChildElements();
		Element child;
		int posX = Integer.parseInt(elem.getAttributeValue("posX"));
		int posY = Integer.parseInt(elem.getAttributeValue("posY"));
		group.setBounds(new Rectangle2D.Double(posX, posY, -1, -1));
		for (int i = 0; i < elems.size(); i++) {
			child = elems.get(i);
			if (MODULE_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				group.add(parseModule(elems.get(i)));
			} else if (MODULE_GROUP_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				group.add(parseModuleGroup(elems.get(i)));
			} else if (INPUT_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				inputAliases.put(child.getAttributeValue("id"), child.getAttributeValue("link"));
			} else if (OUTPUT_TAG.equalsIgnoreCase(child.getQualifiedName())) {
				outputAliases.put(child.getAttributeValue("id"), child.getAttributeValue("link"));
			}
		}
		return group;
	}

	private String appendName(String str, int offset) {
		int index = str.lastIndexOf('_');
		String name;
		if (index != -1) {
			name = str.substring(0, index) + "_" + offset;
		} else {
			name = str + "_" + offset;
		}
		return name;
	}

	/**
	 * Parse particular parameter.
	 * 
	 * @param elem
	 *            xml element
	 * @param params
	 *            parent collection
	 * @param orderHash
	 *            parameter order hash
	 */
	protected void parseParam(Element elem, ParamCollection params, Hashtable<Integer, String> orderHash) {
		String name = elem.getAttributeValue("id");
		String label = elem.getAttributeValue("name");
		boolean enabled = elem.getAttributeValue("enabled").equals("true");
		boolean required = elem.getAttributeValue("required").equals("true");
		int order = Integer.parseInt(elem.getAttributeValue("order"));
		orderHash.put(order, name);
		String switchName = (elem.getAttribute("switch") != null) ? elem.getAttributeValue("switch") : null;
		boolean switchSpaced = (elem.getAttribute("switchSpaced") != null) ? elem.getAttributeValue("switchSpaced")
				.equals("true") : false;
		if (switchName != null) {
			params.add(new ParamSwitch("sw_" + name, switchName, switchSpaced));
		}
		Element child = elem.getFirstChildElement(FORMAT_TAG);
		String type = child.getAttributeValue("type");
		int cardinality = Integer.parseInt(child.getAttributeValue("cardinality"));
		LinkedList<ParamModel> newParams = new LinkedList<ParamModel>();
		ParamModel param;
		ArrayList<String> valueStrs = null;
		if (elem.getFirstChildElement(VALUES_TAG) != null) {
			Elements values = elem.getFirstChildElement(VALUES_TAG).getChildElements();
			valueStrs = new ArrayList<String>();
			for (int j = 0; j < values.size(); j++) {
				if (VALUE_TAG.equalsIgnoreCase(values.get(j).getQualifiedName())) {
					valueStrs.add(values.get(j).getValue());
				}
			}
		}
		if (type.equalsIgnoreCase("Number")) {
			if (cardinality < 0) {
				newParams.add(param = new ParamNumberCollection(name));
				param.setLabel(label);
				if(valueStrs!=null){
					for(int j=0;j<valueStrs.size();j++){
						((ParamNumberCollection)param).add(valueStrs.get(j));
					}
				}
			} else {
				for (int i = 0; i < cardinality; i++) {
					newParams.add(param = new ParamDouble(appendName(name, i)));
					param.setLabel(label + ((cardinality < 2) ? "" : " (" + (i + 1) + ")"));
					if (valueStrs != null && i < valueStrs.size()) {
						((ParamDouble) param).setValue(valueStrs.get(i));
					}
				}
			}
		} else if (type.equalsIgnoreCase("String")) {
			if (cardinality < 0) {
				newParams.add(param = new ParamObject<Object[][]>(name, new ArrayObjectTxtReaderWriter()));
				param.setLabel(label);
			} else {
				for (int i = 0; i < cardinality; i++) {
					newParams.add(param = new ParamString(appendName(name, i)));
					param.setLabel(label + ((cardinality < 2) ? "" : " (" + (i + 1) + ")"));
					if (valueStrs != null && i < valueStrs.size()) {
						((ParamString) param).setValue(valueStrs.get(i));
					}
				}
			}
		} else if (type.equalsIgnoreCase("File")) {
			if (child.getFirstChildElement(FILE_TYPES_TAG) != null) {
				Elements fileTypes = child.getFirstChildElement(FILE_TYPES_TAG).getChildElements();
				Vector<String> extNames = new Vector<String>();
				for (int j = 0; j < fileTypes.size(); j++) {
					if (FILE_TYPE_TAG.equalsIgnoreCase(fileTypes.get(j).getQualifiedName())) {
						extNames.add(fileTypes.get(j).getAttributeValue("extension").toLowerCase());
					}
				}
				if (cardinality < 0) {
					if (ModelImageReaderWriter.getInstance().getExtensionFilter().accept(extNames)
							|| extNames.contains("byte") || extNames.contains("float")) {
						param = new ParamVolumeCollection(name,null,-1,-1,-1,-1);
						((ParamVolumeCollection) param).getExtensionFilter().setPreferredExtension(
								extNames.firstElement());
					} else if (SurfaceReaderWriter.getInstance().getExtensionFilter().accept(extNames)) {
						param = new ParamSurfaceCollection(name);
						((ParamSurfaceCollection) param).getExtensionFilter().setPreferredExtension(
								extNames.firstElement());
					} else {
						param = new ParamFileCollection(name);
					}
					if (param != null) {
						newParams.add(param);
						param.setLabel(label);
					}
					if(valueStrs!=null){
						for(int j=0;j<valueStrs.size();j++){
							((ParamFileCollection)param).add(correctLocation(valueStrs.get(j)));
						}
					}
				} else {
					for (int i = 0; i < cardinality; i++) {
						if (ModelImageReaderWriter.getInstance().getExtensionFilter().accept(extNames)
								|| extNames.contains("byte") || extNames.contains("float")) {
							param = new ParamVolume(appendName(name, i),null,-1,-1,-1,-1);
							((ParamVolume) param).getExtensionFilter().setPreferredExtension(extNames.firstElement());
						} else if (SurfaceReaderWriter.getInstance().getExtensionFilter().accept(extNames)) {
							param = new ParamSurface(appendName(name, i));
							((ParamSurface) param).getExtensionFilter().setPreferredExtension(extNames.firstElement());
						} else {
							param = new ParamFile(appendName(name, i), new FileExtensionFilter(extNames));
						}
						if (param != null) {
							param.setLabel(label + ((cardinality < 2) ? "" : " (" + (i + 1) + ")"));
							newParams.add(param);
						}
						if (valueStrs != null && i < valueStrs.size()) {
							((ParamFile) param).setValue(correctLocation(valueStrs.get(i)));
						}
					}
				}
			} else {
				if (cardinality < 0) {
					newParams.add(param = new ParamFileCollection(name));
					param.setLabel(label);
					if(valueStrs!=null){
						for(int j=0;j<valueStrs.size();j++){
							((ParamFileCollection)param).add(correctLocation(valueStrs.get(j)));
						}
					}
				} else {
					for (int i = 0; i < cardinality; i++) {
						newParams.add(param = new ParamFile(appendName(name, i)));
						param.setLabel(label + ((cardinality < 2) ? "" : " (" + (i + 1) + ")"));
						if (valueStrs != null && i < valueStrs.size()) {
							((ParamFile) param).setValue(correctLocation(valueStrs.get(i)));
						}
					}
				}
			}
		} else if (type.equalsIgnoreCase("Enumerated")) {
			Elements enums = child.getChildElements();
			ArrayList<String> enumNames = new ArrayList<String>();
			for (int j = 0; j < enums.size(); j++) {
				if (ENUMERATION_TAG.equalsIgnoreCase(enums.get(j).getQualifiedName())) {
					enumNames.add(enums.get(j).getValue().trim());
				}
			}
			param=new ParamOption(name, enumNames);
			param.setLabel(label);
			newParams.add(param);
			
		} else if (type.equalsIgnoreCase("Directory")) {
			param=new ParamFile(name, DialogType.DIRECTORY);
			param.setLabel(label);
			newParams.add(param);
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"COULD NOT IDENTIFY TYPE " + type);
		}
		for (ParamModel pr : newParams) {
			pr.setMandatory(required);
			//Do not hide disabled parameters because the user may want to set them!
			//pr.setHidden(!enabled);
			params.add(pr);
		}
	}

	/**
	 * Parse LONI script from file.
	 * 
	 * @param f
	 *            the f
	 * @return the pipe algorithm
	 */
	public PipeAlgorithm parsePipeAlgorithm(File f) {
		if (FileReaderWriter.getFileExtension(f).equals("pipe")) {
			PipeAlgorithm group = parseModule(f);
			return group;
		} else {
			return null;
		}
	}
}
