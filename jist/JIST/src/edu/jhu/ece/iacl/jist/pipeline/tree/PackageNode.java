/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.tree;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmNode;

/**
 * A package node is a directory that contains processing nodes.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PackageNode extends DefaultMutableTreeNode implements Transferable {
	
	/** The Constant PACKAGE_FLAVOR. */
	final public static DataFlavor PACKAGE_FLAVOR = new DataFlavor(PackageNode.class, "Module Package");
	
	/** The dir. */
	protected File dir = null;
	
	/** The editable. */
	protected boolean editable;

	/**
	 * Default constructor.
	 * 
	 * @param dir
	 *            directory that package node corresponds to.
	 */
	public PackageNode(File dir) {
		this(dir, dir.getName());
	}

	/**
	 * Default constructor.
	 * 
	 * @param dir
	 *            directory that this node corresponds to.
	 * @param name
	 *            name of package
	 */
	public PackageNode(File dir, String name) {
		super(name);
		editable = true;
		this.dir = dir;
	}

	/**
	 * Get associated directory.
	 * 
	 * @return the directory
	 */
	public File getDirectory() {
		return dir;
	}

	/**
	 * implements Transferable interface.
	 * 
	 * @param df
	 *            the df
	 * @return the transfer data
	 * @throws UnsupportedFlavorException
	 *             the unsupported flavor exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
		if (df.equals(PACKAGE_FLAVOR)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(df);
		}
	}

	/**
	 * implements Transferable interface.
	 * 
	 * @return the transfer data flavors
	 */
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] { PACKAGE_FLAVOR };
	}

	/**
	 * Returns true if the data flavor is package type.
	 * 
	 * @param df
	 *            the df
	 * @return true, if checks if is data flavor supported
	 */
	public boolean isDataFlavorSupported(DataFlavor df) {
		return df.equals(PACKAGE_FLAVOR);
	}

	/**
	 * Returns true if package name is editable.
	 * 
	 * @return true if edtiable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * Move all children of this package into the directory corresponding to
	 * this package if they are not already in that directory.
	 */
	public void moveAllChildrenIntoDirectory() {
		for (int i = 0; i < this.getChildCount(); i++) {
			TreeNode child = this.getChildAt(i);
			if (child instanceof AlgorithmNode) {
				moveIntoDirectory((AlgorithmNode) child);
			} else if (child instanceof PackageNode) {
				File newDir = new File(dir, ((PackageNode) child).getDirectory().getName());
				if (newDir.mkdir()) {
					File oldDir = ((PackageNode) child).getDirectory();
					((PackageNode) child).setDirectory(newDir);
					((PackageNode) child).moveAllChildrenIntoDirectory();
					oldDir.delete();
				}
			}
		}
	}

	/**
	 * Move node into the directory corresponding to this package node.
	 * 
	 * @param node
	 *            process node
	 * @return true if move successful
	 */
	public boolean moveIntoDirectory(AlgorithmNode node) {
		File mapFile = ((PipeAlgorithmFactory) node.getFactory()).getMapFile();
		File newFile = new File(dir, mapFile.getName());
		boolean ret = mapFile.renameTo(newFile);
		if (ret) {
			((PipeAlgorithmFactory) node.getFactory()).setMapFile(newFile);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Move package node into the directory corresponding to this package.
	 * 
	 * @param node
	 *            package node
	 * @return true, if move into directory
	 */
	public boolean moveIntoDirectory(PackageNode node) {
		File newDir = new File(node.getDirectory(), dir.getName());
		if (newDir.mkdir()) {
			File oldDir = dir;
			dir = newDir;
			moveAllChildrenIntoDirectory();
			oldDir.delete();
			return true;
		} else {
			System.err.println(getClass().getCanonicalName()+"Could not create directory " + newDir);
			return false;
		}
	}

	/**
	 * Set associated directory.
	 * 
	 * @param dir
	 *            the dir
	 */
	public void setDirectory(File dir) {
		this.dir = dir;
	}

	/**
	 * Set editable.
	 * 
	 * @param editable
	 *            true if use can edit package name
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * Set the user object for this package node, which should be the directory
	 * name.
	 * 
	 * @param userObject
	 *            the user object
	 */
	public void setUserObject(Object userObject) {
		if (!editable) {
			return;
		}
		if (dir != null) {
			File newDir = new File(dir.getParentFile(), userObject.toString());
			if (newDir.mkdir()) {
				File oldDir = dir;
				dir = newDir;
				super.setUserObject(userObject);
				moveAllChildrenIntoDirectory();
				oldDir.delete();
			}
		} else {
			super.setUserObject(userObject);
		}
	}
}
