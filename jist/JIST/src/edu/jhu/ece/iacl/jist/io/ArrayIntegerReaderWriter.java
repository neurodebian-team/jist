package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.util.Vector;

// TODO: Auto-generated Javadoc
/**
 * The Class ArrayIntegerReaderWriter.
 */
public class ArrayIntegerReaderWriter extends FileReaderWriter<int[][]>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant arrayReaderWriters. */
	private static final ArrayIntegerReaderWriter[]arrayReaderWriters=new ArrayIntegerReaderWriter[]{
		new ArrayIntegerTxtReaderWriter(),
		new ArrayIntegerDxReaderWriter()
	};
	
	/** The Constant readerWriter. */
	protected static final ArrayIntegerReaderWriter readerWriter=new ArrayIntegerReaderWriter();
	
	/**
	 * Gets the single instance of ArrayIntegerReaderWriter.
	 * 
	 * @return single instance of ArrayIntegerReaderWriter
	 */
	public static ArrayIntegerReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new array integer reader writer.
	 * 
	 * @param filter the filter
	 */
	public ArrayIntegerReaderWriter(FileExtensionFilter filter){
		super(filter);
	}
	
	/**
	 * Instantiates a new array integer reader writer.
	 */
	public ArrayIntegerReaderWriter() {
		super(new FileExtensionFilter());
		Vector<String> exts=new Vector<String>();
		for(ArrayIntegerReaderWriter reader:arrayReaderWriters){
			exts.addAll(reader.extensionFilter.getExtensions());
		}
		extensionFilter.setExtensions(exts);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected int[][] readObject(File f){
		for(ArrayIntegerReaderWriter reader:arrayReaderWriters){
			if(reader.accept(f)){
				return reader.readObject(f);
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(int[][] obj,File f){
		for(ArrayIntegerReaderWriter writer:arrayReaderWriters){
			if(writer.accept(f)){
				return writer.writeObject(obj,f);
			}
		}
		return null;
	}
}
