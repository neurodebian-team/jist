/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Sweep through range of integers.
 * 
 * @author Blake Lucas
 */
public class PipeIntegerSweepSource extends PipeSource {
	
	/** The start. */
	transient protected int start;
	
	/** The end. */
	transient protected int end;
	
	/** The inc. */
	transient protected int inc;
	
	/** The val. */
	transient protected int val;
	
	/** The val param. */
	protected ParamInteger startParam, endParam, incParam, valParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("startParam");
//		startParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("endParam");
//		endParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("incParam");
//		incParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("valParam");
//		valParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		startParam = (ParamInteger) inputParams.getFirstChildByName("Start Value");
		endParam = (ParamInteger) inputParams.getFirstChildByName("End Value");
		incParam = (ParamInteger) inputParams.getFirstChildByName("Increment");
		valParam = (ParamInteger) outputParams.getFirstChildByName("Integer");

//		startParam = new ParamInteger();
//		startParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"startParam"));
//		endParam = new ParamInteger();
//		endParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"endParam"));
//		incParam = new ParamInteger();
//		incParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"incParam"));
//		valParam = new ParamInteger();
//		valParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"valParam"));
		getParentPort().setParameter(valParam);
	}
	/**
	 * Instantiates a new pipe integer sweep source.
	 */
	public PipeIntegerSweepSource() {
		super();
		getParentPort().setParameter(valParam);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createInputParams()
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Integer Sweep");
		group.setName("intsweep");
		group.add(startParam = new ParamInteger("Start Value"));
		group.add(endParam = new ParamInteger("End Value"));
		group.add(incParam = new ParamInteger("Increment"));
		group.setCategory("Number.Integer");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Integer");
		group.add(valParam = new ParamInteger("Integer"));
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#getOutputParam()
	 */
	public ParamInteger getOutputParam() {
		return valParam;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#hasNext()
	 */
	public boolean hasNext() {
		return (super.hasNext() || ((inc > 0) && (val <= end)) || ((inc < 0) && (val >= end)));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#iterate()
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (((inc > 0) && (val <= end)) || ((inc < 0) && (val >= end))) {
					valParam.setValue(val);
					push();
					val += inc;
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			reset();
			isReset = true;
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#reset()
	 */
	public void reset() {
		super.reset();
		start = startParam.getInt();
		end = endParam.getInt();
		inc = (int) Math.signum(end - start) * incParam.getInt();
		val = start;
		valParam.setValue(val);
		val += inc;
		push();
	}
}
