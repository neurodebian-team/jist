/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;

/**
 * Toggle button to select whether to use a connection or not use a connection.
 * 
 * @author Blake Lucas
 */
public class PortToggleButton extends JToggleButton implements ActionListener {
	
	/** The port. */
	public PipePort port;
	
	/** The pipe. */
	public PipeModule pipe;
	
	/** The disconnect icon. */
	ImageIcon connectIcon = null, disconnectIcon = null;

	/**
	 * Constructor.
	 * 
	 * @param port
	 *            assocaited port
	 */
	public PortToggleButton(PipePort port) {
		super("");
		pipe = port.getOwner();
		boolean selected = port.usingConnector();
		if (connectIcon == null) {
			URL connectUrl = 
					PlaceHolder.class.getResource("connecton.gif");
			connectIcon = new ImageIcon(connectUrl);
			connectUrl = PlaceHolder.class.getResource("connectoff.gif");
			disconnectIcon = new ImageIcon(connectUrl);
		}
		if (selected) {
			this.setIcon(connectIcon);
		} else {
			this.setIcon(disconnectIcon);
		}
		this.setSelected(selected);
		this.port = port;
		this.addActionListener(this);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() instanceof PortToggleButton) {
			if (isSelected()) {
				setIcon(connectIcon);
				port.setUseConnector(true);
				// System.out.println(getClass().getCanonicalName()+"\t"+pipe.getInputParams());
				if (pipe == null) {
					System.out.println(getClass().getCanonicalName()+"\t"+"PIPE NULL");
				}
				if (pipe.getInputParams() == null) {
					System.out.println(getClass().getCanonicalName()+"\t"+"PARAMS NULL");
				}
				if (pipe.getInputParams().getInputView() == null) {
					System.out.println(getClass().getCanonicalName()+"\t"+"VIEW NULL");
				}
				pipe.getInputParams().getInputView().update();
			} else {
				port.setUseConnector(false);
				if (pipe == null) {
					System.out.println(getClass().getCanonicalName()+"\t"+"PIPE NULL");
				}
				if (pipe.getInputParams() == null) {
					System.out.println(getClass().getCanonicalName()+"\t"+"PARAMS NULL");
				}
				if (pipe.getInputParams().getInputView() == null) {
					System.out.println(getClass().getCanonicalName()+"\t"+"VIEW NULL");
				}
				pipe.getInputParams().getInputView().update();
				setIcon(disconnectIcon);
			}
		}
	}
}
