/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Vector;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInformation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import gov.nih.mipav.view.ViewJFrameBase;

// TODO: Auto-generated Javadoc
/**
 * <PRE>
 * 
 * Processing Dialog is a generic class that replaces all MIPAV plug-in dialogs
 * The dialogs are auto-generated based on the input and output parameters.
 * 
 * This class should not be extended. Any changes to this class will appear
 * globally across all plug-ins.
 * 
 * </PRE>
 * 
 * @author Blake Lucas
 */
public class ProcessingGroupDialog extends ProcessingDialog{
	
	/** The pipe algorithm. */
	protected PipeAlgorithmGroup pipeAlgorithm;
	
	/**
	 * Empty constructor needed for dynamic instantiation.
	 */
	public ProcessingGroupDialog() {
	}

	/**
	 * Constructor creates input and output parameters from algorithm and uses
	 * the algorithm name as title.
	 * 
	 * @param pipeAlgorithm the algorithm
	 */
	public ProcessingGroupDialog(PipeAlgorithmGroup pipeAlgorithm) {
		super();
		inputView = null;
		outputView = null;
		this.pipeAlgorithm=pipeAlgorithm;
		init(pipeAlgorithm);
		loadDefaults();
	}

	/**
	 * Handle event when user clicks "Close".
	 */
	public void clickClose() {
		// Close dialog
		dispose();
//		algorithm.outputParams.dispose();
	}

	/**
	 * Handle event when user clicks "OK".
	 * 
	 * @return true, if click ok
	 */
	public boolean clickOk() {
			try {
				algorithm.inputParams.getInputView().commit();
				// Validate input parameters
				algorithm.inputParams.validate();
				if (!MipavController.isQuiet()) {
					File saveDirectory = selectSaveDirectory();
					algorithm.setOutputDirectory(saveDirectory);
					PipeLayout layout=new PipeLayout(pipeAlgorithm);
					layout.setOutputDir(saveDirectory);
					File pipeFile=new File(saveDirectory,pipeAlgorithm.getLabel()+"."+JistPreferences.getPreferences().getDefaultLayoutExtension());
					layout.write(pipeFile, true);
					this.setVisible(false);
					// This forces dialog threads that observe MIPAV to shutdown
					if (inputView != null) {
						inputView.setVisible(false);
					}
					saveDefaults();
					ProcessManager.getInstance().setExitOnClose(false);
					ProcessManager.getInstance().run(layout,false);
				}

			} catch (InvalidParameterException e) {
				if (MipavController.isQuiet()) {
					System.err.println(getClass().getCanonicalName()+e.getMessage());
					System.err.flush();
				} else {
					MipavController.displayError(e.getMessage());
				}
				return false;
			}
		return true;
	}

	/**
	 * Handle event when user clicks "Open".
	 * 
	 * @param file the file
	 * 
	 * @return true, if click open
	 */
	public boolean clickOpen(File file) {
		// Load input profile
		if (file != null) {
			if (load(file)) {
				return true;
			} else {
				if (MipavController.isQuiet()) {
					System.err.println(getClass().getCanonicalName()+"Could not open " + file.getAbsolutePath() + ".");
				} else {
					MipavController.displayError("Could not open " + file.getAbsolutePath() + ".");
				}
			}
		}
		return false;
	}

	/**
	 * Handle event when user clicks "Save Parameters and Resources".
	 * 
	 * @param saveDir the save dir
	 */
	public void clickSaveAll(File saveDir) {
		// Save all input and output parameters if they exist
		if (!saveAll(saveDir)) {
			MipavController.displayError(algorithm.getAlgorithmLabel() + " could not save information.");
		}
	}

	/**
	 * Handle event when user clicks "Save As Module Definition".
	 * 
	 * @param saveFile the save file
	 */
	public void clickSaveDefinition(File saveFile) {
		// Save all input and output parameters if they exist
		PipeAlgorithm pAlgo = new PipeAlgorithm(algorithm);
		pAlgo.write(saveFile);
	}

	/**
	 * Handle event when user clicks "Save As Algorithm Input".
	 * 
	 * @param saveFile the save file
	 */
	public void clickSaveInput(File saveFile) {
		// Save all input and output parameters if they exist
		saveInput(saveFile);
		/*
		if (algorithm.isCompleted()) {
			dispose();
		}
		*/
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
	 */
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
	 */
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Shameless hack to circumvent bug in linux implementation that prevents
	 * window from being resized.
	 * 
	 * @param event the event
	 */
	public void componentResized(ComponentEvent event) {
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
	 */
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Create input dialog frame from input parameters.
	 */
	protected void createInputDialog() {
		if (!MipavController.isQuiet()) {
			this.setVisible(false);
			setTitle(algorithm.inputParams.getLabel());
			for(ParamModel mod:algorithm.inputParams.getAllDescendantsByClass(ParamInformation.class)){
				((ParamInformation)mod).getValue().setEditable(false);
			}
			
			Container pane = getContentPane();
			pane.setLayout(new BorderLayout());
			pane.setPreferredSize(new Dimension(640,480));
			this.setResizable(true);
			this.addComponentListener(this);
			this.setJMenuBar(createInputMenuBar());
			pane.add(inputView = algorithm.inputParams.getInputView(), BorderLayout.CENTER);
			pane.add(buttonPanel = buildInputButtons(), BorderLayout.SOUTH);
			this.addWindowListener(this);
			inputView.addObserver(this);
			inputView.update();
			this.pack();
			this.setVisible(true);
		}
	}

	/**
	 * Create menubar for input pane.
	 * 
	 * @return menu bar
	 */
	public JMenuBar createInputMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		// Create file and help menus
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		// JMenu helpMenu = new JMenu("Help");
		// helpMenu.setMnemonic(KeyEvent.VK_H);
		menuBar.add(fileMenu);
		// menuBar.add(helpMenu);
		// Add file menu items
		JMenuItem openItem = new JMenuItem("Open Algorithm Input", KeyEvent.VK_O);
		JMenuItem saveItem = new JMenuItem("Save As Algorithm Input", KeyEvent.VK_S);
		JMenuItem saveMapItem = new JMenuItem("Save As Module Definition", KeyEvent.VK_M);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(saveMapItem);
		openItem.addActionListener(this);
		saveItem.addActionListener(this);
		saveMapItem.addActionListener(this);
		// Add help menu items
		// JMenuItem contentsItem = new JMenuItem("Usage", KeyEvent.VK_U);
		// JMenuItem aboutItem = new JMenuItem("About", KeyEvent.VK_A);
		// helpMenu.add(contentsItem);
		// helpMenu.add(aboutItem);
		// contentsItem.addActionListener(this);
		// aboutItem.addActionListener(this);
		return menuBar;
	}

	/**
	 * Create output dialog frame from output parameters. This method reuses the
	 * input frame.
	 */
	private void createOutputDialog() {
		if (!MipavController.isQuiet()) {
			File saveDirectory = algorithm.getOutputDirectory(); 
			if (saveDirectory != null) {
				saveAll(saveDirectory);
			}
			this.setVisible(false);
			this.setResizable(true);
			this.remove(inputView);
			this.remove(buttonPanel);
			this.setJMenuBar(null);
			setTitle(algorithm.inputParams.getLabel());
			Container pane = getContentPane();
			pane.setPreferredSize(algorithm.getPreferredSize());
			pane.add(outputView = algorithm.outputParams.getOutputView(), BorderLayout.NORTH);
			pane.add(buttonPanel = buildOutputButtons(), BorderLayout.SOUTH);
			pack();
			this.setVisible(true);
			outputView.update();
		}
	}

	/**
	 * Display file that contains HTML.
	 * 
	 * @param file file name
	 * @param title frame title
	 */
	private void displayHTML(File file, String title) {
		JEditorPane editPane = new JEditorPane();
		JScrollPane scrollPane = new JScrollPane();
		JFrame frame = new JFrame(title);
		frame.add(scrollPane);
		scrollPane.setViewportView(editPane);
		scrollPane.setPreferredSize(new Dimension(640, 520));
		try {
			editPane.setPage(file.toURL());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		editPane.setEditable(false);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Dispose of dialog.
	 */
	public void dispose() {
		if (inputView != null) {
			inputView.setVisible(false);
		}
		super.dispose();
	}

	/**
	 * Store result images in script runner.
	 */
	protected void doPostAlgorithmActions() {
		throw new RuntimeException("NOT IMPLEMENTED");
		/*
		for (ParamModel mod : algorithm.outputParams.getAllDescendants()) {
			if (mod instanceof ParamVolume) {
				ModelImage img = ((ParamVolume) mod).getModelImageCache();
				AlgorithmParameters.storeImageInRunner(img);
			} else if (mod instanceof ParamVolumeCollection) {
				List<ModelImage> images = ((ParamVolumeCollection) mod).getModelImageCacheList();
				for (ModelImage img : images) {
					if (img != null) {
						AlgorithmParameters.storeImageInRunner(img);
					}
				}
			}
		}
		*/
	}

	/**
	 * Get input view.
	 * 
	 * @return input view
	 */
	public ParamInputView getInputView() {
		return inputView;
	}

	/**
	 * Get output view.
	 * 
	 * @return output view
	 */
	public ParamOutputView getOutputView() {
		return outputView;
	}

	/**
	 * Get parent frame for Mipav.
	 * 
	 * @return parent frame
	 */
	public ViewJFrameBase getParentFrame() {
		return parentFrame;
	}

	/**
	 * Get user interface for Mipav.
	 * 
	 * @param pipeFile the pipe file
	 * 
	 * @return user interface
	 */
//	public MipavViewUserInterface getUserInterface() {
//		return userInterface;
//	}

	/**
	 * Initialize parameters and algorithm from file.
	 * 
	 * @param pipeFile
	 *            pipe file
	 */
	public void init(File pipeFile) {
		algorithm.init(pipeFile);
		this.algorithm.inputParams = algorithm.getInput();
		this.algorithm.outputParams = algorithm.getOutput();
		this.setName(algorithm.getAlgorithmLabel());
	}

	/**
	 * Initialize parameter and algorithm from existing algorithm.
	 * 
	 * @param pipe algorithm
	 */
	public void init(PipeAlgorithm pipe) {
		this.algorithm.inputParams = pipe.getInputParams();
		this.algorithm.outputParams = pipe.getOutputParams();
		for (PipeModule p : ((PipeAlgorithmGroup)pipe).getAllLeafModules()) {
			Vector<PipePort> ports = p.getInputPorts();
			pipe.getInputParams().init();
			
			for (PipePort port : ports) {
				if (port.isConnected()) {
					if(port instanceof ParamModel){
						((ParamModel)port).setMandatory(false);
					}
				}
			}
			
		}
		this.setName(pipe.getLabel());

	}


}
