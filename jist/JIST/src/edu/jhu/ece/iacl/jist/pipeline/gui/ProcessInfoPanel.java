/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.GridLayout;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.tree.ProcessNode;
import edu.jhu.ece.iacl.jist.pipeline.tree.ProcessNode.Hierarchy;

/**
 * Display process information.
 * 
 * @author Blake Lucas
 */
public class ProcessInfoPanel extends JPanel implements TreeSelectionListener, ListSelectionListener {
	
	/** The manager. */
	ProcessManagerTable manager;
	
	/** The type. */
	ProcessNode.Hierarchy type;
	
	/** The last context. */
	ExecutionContext lastContext;
	
	/** The restart. */
	public Action play, pause, stop, restart;
	
	/** The scroll pane. */
	JScrollPane scrollPane;

	/**
	 * Instantiates a new process info panel.
	 * 
	 * @param manager
	 *            the manager
	 * @param type
	 *            the type
	 */
	public ProcessInfoPanel(ProcessManagerTable manager, ProcessNode.Hierarchy type) {
		super(new GridLayout(1, 0));
		this.type = type;
		createTree();
		this.manager = manager;
		manager.getTable().getSelectionModel().addListSelectionListener(this);
	}

	/**
	 * Creates the tree.
	 */
	private void createTree() {
		scrollPane = new JScrollPane();
		this.add(scrollPane);
	}

	/**
	 * Erase cache storing last context.
	 * 
	 * @param context
	 *            the context
	 */
	public void eraseCacheContext(ExecutionContext context) {
		if (lastContext == context) {
			updateTree(lastContext);
		}
		lastContext = null;
	}

	/**
	 * Refresh current context.
	 */
	public void refreshCurrentContext() {
		if (lastContext != null) {
			updateTree(lastContext);
		}
	}

	/**
	 * Update parameter tree using execution context.
	 * 
	 * @param context
	 *            execution context
	 */
	public void updateTree(ExecutionContext context) {
		if (context != null) {
			if (context.getAncestorTree() == null) {
				context.createTreeNodes(this);
			}
			JTree tree = (type == Hierarchy.Ancestors) ? context.getAncestorTree() : context.getDescendantTree();
			scrollPane.setViewportView(tree);
			tree.updateUI();
		} else {
			scrollPane.setViewportView(new JPanel());
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent evt) {
		ExecutionContext context = (manager.getSelectedContexts().size() > 0) ? manager.getSelectedContexts()
				.firstElement() : null;
		if (context != lastContext) {
			updateTree(context);
			lastContext = context;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
	 */
	public void valueChanged(TreeSelectionEvent arg0) {
		// TODO Auto-generated method stub
	}
}
