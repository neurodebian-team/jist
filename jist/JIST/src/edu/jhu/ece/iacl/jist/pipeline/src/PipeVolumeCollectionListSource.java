package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

public class PipeVolumeCollectionListSource extends PipeListSource {

	/** The volume collection param. */
	protected ParamVolumeCollection volcolParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("volcolParam");
//		volcolParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);					
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		volcolParam = (ParamVolumeCollection)outputParams.getFirstChildByName("Volume Collection");
//		volcolParam = new ParamVolumeCollection();
//		volcolParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"volcolParam"));
		getParentPort().setParameter(volcolParam);
	}
	/**
	 * Default constructor.
	 */
	public PipeVolumeCollectionListSource() {
		super();
		getParentPort().setParameter(volcolParam);
	}
	
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Volume Collection List");
		group.setName("volumecollectionlist");
		group.setCategory("Volume");
		return group;
	}
	
	@Override
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume Collection List");
		group.add(volcolParam = new ParamVolumeCollection("Volume Collection"));
		return group;
	}

	@Override
	public ParamVolumeCollection getOutputParam() {
		return volcolParam;
	}
	
	@Override
	protected ArrayList<File> getValue(int i) {
		if (off < data[i].length) {
			ArrayList<File> vollist = new ArrayList<File>();
			for(int j=0; j<data[i].length; j++){
				vollist.add(new File(data[i][j].toString().trim()));
			}
			return vollist;
		} else {
			return null;
		}
	}

	@Override
	protected void setValue(Object obj) {
		volcolParam.setValue((List<File>)obj);
	}

}
