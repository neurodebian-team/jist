package edu.jhu.ece.iacl.jist.structures.tensor;

import edu.jhu.ece.iacl.jist.structures.vector.VectorX;

// TODO: Auto-generated Javadoc
/**
 * Tensor with 2 vectors.
 * 
 * @author Blake Lucas
 */
public class Tensor2 implements TensorX {
	
	/** The t2. */
	public VectorX t1, t2;
}
