/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ProgressMonitor;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamWeightedVolumesURIInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Weighted Volume Parameter Factory.
 * 
 * @author Blake Lucas
 */
public class ParamWeightedVolumeCollectionFactory extends ParamVolumeCollectionFactory {
	
	/** The param. */
	private ParamWeightedVolumeCollection param;
	
	/** The resources. */
	private ImageData[] resources = null;

	/**
	 * Instantiates a new param weighted volumes factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamWeightedVolumeCollectionFactory(ParamWeightedVolumeCollection param) {
		super();
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeCollectionFactory#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof ParamFactory) {
			return this.equals(((ParamFactory) obj).getParameter());
		} else if (obj instanceof ParamWeightedVolumeCollection) {
			return this.getParameter().getFileList().equals(((ParamWeightedVolumeCollection) obj).getFileList());
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeCollectionFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamWeightedVolumesURIInputView(param);
		}
		return inputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeCollectionFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamOutputView(param);
		}
		return outputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeCollectionFactory#getParameter()
	 */
	public ParamWeightedVolumeCollection getParameter() {
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#importParameter(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel)
	 */
	public boolean importParameter(ParamModel foreign) {
		// this parameter is less than or as restrictive as model parameter
		if (param.compareTo(foreign) <= 0) {
			/*
			param.setValue(((ParamWeightedVolumeCollection) foreign).getValue());
			List<ParamFile> vols = ((ParamWeightedVolumeCollection) foreign).getParameters();
			for (int i = 0; i < vols.size(); i++) {
				param.setWeight(i, ((ParamWeightedVolume) vols.get(i)).getWeight());
			}
			*/
			List<ParamFile> vols = ((ParamWeightedVolumeCollection) foreign).getParameters();
			ArrayList<ParamFile> volsCopy=new ArrayList<ParamFile>(vols.size());			
			for (int i = 0; i < vols.size(); i++) {
				volsCopy.add(vols.get(i));
			}
			param.setValue(volsCopy);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Load Model Image from file specified in foreign parameter.
	 * 
	 * @param foreignCollection
	 *            foreign parameter collection
	 * @param monitor
	 *            the monitor
	 * @return external resources loaded correctly
	 */
	public boolean loadResources(ParamModel foreignCollection, ProgressMonitor monitor) {
		if (!param.isHidden() && (foreignCollection instanceof ParamWeightedVolumeCollection)) {
			ParamInputView view = getInputView();
			if (view instanceof Refreshable) {
				Refresher.getInstance().remove(((Refreshable) view));
			}
			List<ParamFile> vols = ((ParamWeightedVolumeCollection) foreignCollection).getParameters();
			ArrayList<ParamFile> volsCopy=new ArrayList<ParamFile>(vols.size());
			
			for (int i = 0; i < vols.size(); i++) {
				volsCopy.add(vols.get(i));
			}
			param.setValue(volsCopy);
			view.update();
			if (view instanceof Refreshable) {
				Refresher.getInstance().add(((Refreshable) view));
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save Model Image to specified directory.
	 * 
	 * @param dir
	 *            save directory
	 * @return external resources saved correctly
	 */
	public boolean saveResources(File dir, boolean saveSubDirectoryOverride) {
		super.saveResources(dir, saveSubDirectoryOverride);
		boolean ret = true;
		if (resources == null) {
			List<ParamFile> volumes = param.getParameters();
			resources = new ImageData[volumes.size()];
			for (int i = 0; i < resources.length; i++) {
				resources[i] = ((ParamVolume) volumes.get(i)).getImageData();
			}
		}
		if (resources != null) {
			List<ParamFile> volumes = param.getParameters();
			for (int i = 0; i < resources.length; i++) {
				File f = ImageDataReaderWriter.getInstance().write(resources[i], dir);
				volumes.get(i).setValue(f);
				ret = ((f != null) && ret);
			}
			return true;
		}
		if(!ret)
			System.out.println(getClass().getCanonicalName()+"\t"+"ParamWeightedVolumeCollectionFactory: Resource Save Failed.");
		return ret;
	}

}
