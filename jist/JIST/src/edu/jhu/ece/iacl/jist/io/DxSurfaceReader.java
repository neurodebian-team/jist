package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

// TODO: Auto-generated Javadoc
/**
 * The Class DxSurfaceReader.
 */
public class DxSurfaceReader {
	
	/** The vertices end index. */
	static int verticesEndIndex;
	
	/**
	 * Read surface file.
	 * 
	 * @param f the f
	 * 
	 * @return the array list
	 */
	public static ArrayList readSurfaceFile (File f){
        ArrayList<String> str = new ArrayList<String>();
		try {		
			// Create input stream from file
			FileReader reader = new FileReader(f);
			BufferedReader in = new BufferedReader(reader);
			String line;
			// Read file and store the string in a array 
			while ((line = in.readLine()) != null) {
				StringTokenizer tokenizer = new StringTokenizer(line);
				while(tokenizer.hasMoreTokens()){
					String token = tokenizer.nextToken();
					//System.out.println("jist.io"+"\t"+token);
					str.add(token);
				}
				
			}
			in.close();
			reader.close();
		} catch (IOException e) {
			System.out.println("jist.io"+"\t"+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
		    return null;
		}
        return str;
	}
		
	/**
	 * Gets the vertices.
	 * 
	 * @param f the f
	 * 
	 * @return the vertices
	 */
	public static String[] getVertices(File f){ 
		ArrayList surfaceData = readSurfaceFile(f);
		int numOfVertices = Integer.parseInt((String)surfaceData.get(11));
		//System.out.println("jist.io"+"\t"+numOfVertices);
		int verticesStartIndex = 14;
        verticesEndIndex = (verticesStartIndex + (numOfVertices * 3) - 1);
        String[] vertices = new String[numOfVertices*3];
        int vertexCounter = 0;
        for (int i = verticesStartIndex; i <= verticesEndIndex; i++){
        	vertices [vertexCounter] = (String)surfaceData.get(i);
        	vertexCounter++;	
        }
		return vertices;
	}
	
	/**
	 * Gets the faces.
	 * 
	 * @param f the f
	 * 
	 * @return the faces
	 */
	public static String[] getFaces(File f){ 
		ArrayList surfaceData = readSurfaceFile(f);
        int numOfFaces = Integer.parseInt((String)surfaceData.get((verticesEndIndex +16)));
        //System.out.println("jist.io"+"\t"+numOfFaces);
        int facesStartIndex = verticesEndIndex + 19;
        int facesEndIndex = (facesStartIndex + (numOfFaces * 3) - 1);
        String[] faces = new String[numOfFaces*3];
        int faceCounter = 0;
        for (int i = facesStartIndex; i <= facesEndIndex; i++){
        	faces [faceCounter] = (String)surfaceData.get(i);
        	faceCounter++;
        }
		return faces;
	}	

}
