/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.PerformanceSummary;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamPerformanceFactory;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Parameter to store algorithm performance information.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamPerformance extends ParamModel<PerformanceSummary> {

	/** The performance. */
	protected PerformanceSummary performance;

	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);	
		 if(performance==null) 
			 return true;
		 Element em;				
		 em = document.createElement("performance");				 
		 if(performance.xmlEncodeParam(document,em))
			 parent.appendChild(em);			
		 else 
			 return true;
		 
			
		 return true;
	 }
	 
	 public void xmlDecodeParam(Document document, Element parent) {
			super.xmlDecodeParam(document, parent);			
			performance = new PerformanceSummary();
			performance.xmlDecodeParam(document,
					JistXMLUtil.xmlReadElement(parent, "performance")
					);
		}
	
	 public ParamPerformance() {this("invalid");};
	 
	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamPerformance(String name) {
		super();
		this.setName(name);
		init();
	}

	/**
	 * Clone object.
	 * 
	 * @return the param performance
	 */
	public ParamPerformance clone() {
		ParamPerformance param = new ParamPerformance(getName());
		param.performance = (performance != null) ? performance.clone() : null;
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Unimplemented.
	 * 
	 * @param o
	 *            the o
	 * @return the int
	 */
	public int compareTo(ParamModel o) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Get algorithm performance.
	 * 
	 * @return the value
	 */
	public PerformanceSummary getValue() {
		return performance;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		connectible = true;
		this.factory = new ParamPerformanceFactory(this);
	}

	/**
	 * Set algorithm performance.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(PerformanceSummary value) throws InvalidParameterValueException {
		this.performance = value;
	}

	/**
	 * Get performance description.
	 * 
	 * @return the string
	 */
	public String toString() {
		if (performance != null) {
			return performance.toString();
		} else {
			return "";
		}
	}

	/**
	 * Unimplemented.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		// TODO Auto-generated method stub
	}
	
	@Override
	public String getHumanReadableDataType() {
		return "string";
	}
	@Override
	public String getXMLValue() {
		// TODO Auto-generated method stub
		return toXML().replaceAll("[\r\n]*","");
	}
	@Override
	public void setXMLValue(String arg) {
		throw new RuntimeException("setXMLValue - Performance Summary - NOT SUPPORTED");

	}
	
	public String probeDefaultValue() {	
		return null;
	}
}
