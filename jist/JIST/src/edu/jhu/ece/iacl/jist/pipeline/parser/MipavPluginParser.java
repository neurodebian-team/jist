package edu.jhu.ece.iacl.jist.pipeline.parser;


import java.awt.Dimension;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.Date;
import java.util.Vector;

import javax.swing.JFileChooser;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.InvalidJistMergeException;
import edu.jhu.ece.iacl.jist.pipeline.MipavViewUserInterface;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHeader;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInformation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamLong;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmMipavAdapter;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.scripting.ImageVariableTable;
import gov.nih.mipav.model.scripting.ParsedActionLine;
import gov.nih.mipav.model.scripting.ParserEngine;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.ScriptRunner;
import gov.nih.mipav.model.scripting.parameters.Parameter;
import gov.nih.mipav.model.scripting.parameters.ParameterBoolean;
import gov.nih.mipav.model.scripting.parameters.ParameterDouble;
import gov.nih.mipav.model.scripting.parameters.ParameterFloat;
import gov.nih.mipav.model.scripting.parameters.ParameterInt;
import gov.nih.mipav.model.scripting.parameters.ParameterList;
import gov.nih.mipav.model.scripting.parameters.ParameterShort;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.model.scripting.parameters.ParameterUShort;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.view.dialogs.ActionDiscovery;
import gov.nih.mipav.view.dialogs.ActionMetadata;

public class MipavPluginParser extends ProcessingAlgorithm implements JISTDynamicPluginLookup {

	private static final String classFileTag = "MIPAV-CLASS-FILE";
	private ActionDiscovery plugin; 

	/*public void setMapFile(File className) {
		try {
			System.out.println("&&&&&&&&&&&&&&"+className.toString());
			init((ActionDiscovery)Class.forName(className.toString()).newInstance());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	public MipavPluginParser(){};
	public MipavPluginParser(ActionDiscovery plugin) {
		init(plugin);
	}

	private void init(ActionDiscovery plugin) {
		this.plugin = plugin;		
		inputParams = new ParamCollection();
		// Add header to algorithm
		inputParams.add(new ParamHeader("Algorithm", this.getClass()));
		// Add information to algorithm
		inputParams.add(new ParamInformation(algorithmInformation = new AlgorithmInformation(this)));
		createInputParameters(inputParams);
		outputParams = new ParamCollection();
		outputParams.add(new ParamHeader("Algorithm", this.getClass()));
		//Create output parameters
		createOutputParameters(outputParams);
		outputParams.setLabel(inputParams.getLabel());
		outputParams.setName(inputParams.getName());
		outputParams.add(performance = new ParamPerformance("Execution Time"));
		algorithmInformation.init(this);

	}

	/**
	 * Parse MIPAV Plugin Class
	 */
	static public PipeAlgorithm parsePipeAlgorithm(ActionDiscovery plugin) {		
		PipeAlgorithm algo = new PipeAlgorithm();
		ProcessingAlgorithm alg = new MipavPluginParser(plugin);
		algo.setAlgorithm(alg);
		algo.setMapFile(new File(plugin.getClass().getCanonicalName()));
		return algo;		
	}

	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		if(plugin==null)
			return;

		String mipavClass = plugin.getClass().getCanonicalName();
		ParamString mipavClassParam = new ParamString(classFileTag,mipavClass);
		mipavClassParam.setHidden(true);
		inputParams.add(mipavClassParam);		

		ActionMetadata meta = plugin.getActionMetadata();
		inputParams.setName(meta.getName());
		inputParams.setLabel(meta.getLabel());
		inputParams.setPackage("MIPAV."+meta.getCategory());
		AlgorithmInformation info = getAlgorithmInformation();
		String []authors = meta.getAuthors();
		String []aff = meta.getAffiliation();
		for(int i=0;i<authors.length;i++)
			info.add(new AlgorithmAuthor(authors[0],aff[0], ""));		
		info.setAffiliation(meta.getAffiliation()[0]);		
		info.setDescription(meta.getDescription());
		info.setLongDescription(meta.getDescriptionLong());
		info.setVersion(meta.getVersion());	
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.Release);
		info.setWebsite(meta.getWebsite());	

		final ParameterTable inParams = plugin.createInputParameters();
		for (final Parameter param : inParams.getParameters()) {
			System.out.println("\tIN:  "+param.getLabel()+"\t"+param.isOptional()+"\t"+param.getTypeString()+"\t"+param.getValueString()+"\t");
			switch (param.getType()) {
			case Parameter.PARAM_BOOLEAN:
				inputParams.add(new ParamBoolean(param.getLabel(),Boolean.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_DOUBLE:
				inputParams.add(new ParamDouble(param.getLabel(),Double.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_FLOAT:
				inputParams.add(new ParamFloat(param.getLabel(),Float.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_INT:
				inputParams.add(new ParamInteger(param.getLabel(),Integer.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_LONG:
				inputParams.add(new ParamInteger(param.getLabel(),Integer.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_SHORT:
				inputParams.add(new ParamInteger(param.getLabel(),Integer.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_STRING:
				inputParams.add(new ParamString(param.getLabel(),String.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_USHORT:
				inputParams.add(new ParamInteger(param.getLabel(),Integer.valueOf(param.getValueString()))); 
				break;
			case Parameter.PARAM_LIST:
				inputParams.add(new ParamInteger(param.getLabel()+"unsupportedList",0)); 
				break;
			case Parameter.PARAM_EXTERNAL_IMAGE:
				inputParams.add(new ParamVolume(param.getLabel(),null,-1,-1,-1,-1)); 
				break;
			case Parameter.PARAM_IMAGE:
				break;
			default:
				System.err.println("Unrecognized parameter type: " + param.getTypeString());
			}
		}
	}

	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		if(plugin==null)
			return;
		try {
			final ParameterTable outParams = plugin.createOutputParameters();
			for (final Parameter param : outParams.getParameters()) {
				System.out.println("\tOUT: "+param.getLabel()+"\t"+param.getTypeString()+"\t"+param.getValueString());			
				switch (param.getType()) {
				case Parameter.PARAM_BOOLEAN:
					outputParams.add(new ParamBoolean(param.getLabel())); 
					break;
				case Parameter.PARAM_DOUBLE:
					outputParams.add(new ParamDouble(param.getLabel())); 
					break;
				case Parameter.PARAM_FLOAT:
					outputParams.add(new ParamFloat(param.getLabel())); 
					break;
				case Parameter.PARAM_INT:
					outputParams.add(new ParamInteger(param.getLabel())); 
					break;
				case Parameter.PARAM_LONG:
					outputParams.add(new ParamInteger(param.getLabel())); 
					break;
				case Parameter.PARAM_SHORT:
					outputParams.add(new ParamInteger(param.getLabel())); 
					break;
				case Parameter.PARAM_STRING:
					outputParams.add(new ParamString(param.getLabel())); 
					break;
				case Parameter.PARAM_USHORT:
					outputParams.add(new ParamInteger(param.getLabel())); 
					break;
				case Parameter.PARAM_LIST:
					outputParams.add(new ParamInteger(param.getLabel()+"unsupportedList",Integer.valueOf(param.getValueString()))); 
					break;
				case Parameter.PARAM_EXTERNAL_IMAGE:
				case Parameter.PARAM_IMAGE:
					outputParams.add(new ParamVolume(param.getLabel(),null,-1,-1,-1,-1));
					break;
				default:
					System.err.println("Unrecognized parameter type: " + param.getTypeString());
				}

			}	
		} catch (Exception e) {
			try {
				System.err.println("Cannot create output parameters for: "+plugin.toString());
				
			} catch (Exception e2) {
				
			}
			return ;
		}	
	}

	@Override
	protected void execute(CalculationMonitor monitor)
	throws AlgorithmRuntimeException {		
		// System.out.println("Initial Input parameters:");
		// System.out.println(inputParams.convertToString());
		if(plugin==null) {
			ParamString str = (ParamString)inputParams.getFirstChildByName(classFileTag);
			initPlugin(str.getValue());
		}
		// System.out.println("Initial Output parameters:");
		// System.out.println(outputParams.convertToString());
		Vector<ModelImage> modelImageCache = new Vector<ModelImage>();
		try {
			// prep the global script runner, which manages the image table
			final ScriptRunner scriptRunner = ScriptRunner.getReference();
			scriptRunner.setRunning(true);
			scriptRunner.setImageTable(new ImageVariableTable());

			// get the input and output parameters of the dialog (which are not filled with any values)
			final ParameterTable mipavInputParams = plugin.createInputParameters();            
			final ParameterTable mipavOutputParams = plugin.createOutputParameters();

			// set the other parameters
			String val = null;
			int curImageNum = 1;

			for (Parameter mipavParam : mipavInputParams.getParameters()) {
				ParamModel jistParam = inputParams.getFirstChildByName(mipavParam.getLabel());
				switch(mipavParam.getType()) {
				case Parameter.PARAM_BOOLEAN:									
				case Parameter.PARAM_DOUBLE:
				case Parameter.PARAM_FLOAT:
				case Parameter.PARAM_INT:
				case Parameter.PARAM_LONG:
				case Parameter.PARAM_SHORT:
				case Parameter.PARAM_STRING:
				case Parameter.PARAM_USHORT:
					mipavParam.setValue(jistParam.getValue().toString());
					break;
				case Parameter.PARAM_LIST:
					System.out.println("NOT SUPPORTED:"+mipavParam.getLabel()); 
					break;
				case Parameter.PARAM_EXTERNAL_IMAGE:					 
				case Parameter.PARAM_IMAGE:
					mipavParam.setValue("$image" + curImageNum);

					ModelImage img = null;
					ImageData vol = ((ParamVolume)jistParam).getImageData();
					if(vol instanceof ImageDataMipav) {
						img = ((ImageDataMipav)vol).extractModelImage();
						vol.dispose();
					} else {
						img = new ImageDataMipav(vol).extractModelImage();
						vol.dispose();
					} 	

					modelImageCache.add(img);
					// map the name input image we have opened to the image placeholder ($image1)
					scriptRunner.storeImage(img.getImageName());
					curImageNum++;

					break;
				default:
					System.err.println("Unrecognized parameter type: " + mipavParam.getTypeString());
				}
			}

				// run the script with the parameters we have set up
				plugin.scriptRun(mipavInputParams);

				for (Parameter mipavOutParam : mipavOutputParams.getParameters()) {
					ParamModel jistParam = outputParams.getFirstChildByName(mipavOutParam.getLabel());
					switch(mipavOutParam.getType()) {
					case Parameter.PARAM_BOOLEAN:									
					case Parameter.PARAM_DOUBLE:
					case Parameter.PARAM_FLOAT:
					case Parameter.PARAM_INT:
					case Parameter.PARAM_LONG:
					case Parameter.PARAM_SHORT:
					case Parameter.PARAM_STRING:
					case Parameter.PARAM_USHORT:
						jistParam.setValue(mipavOutParam.getValueString());
						break;
					case Parameter.PARAM_LIST:
						System.out.println("NOT SUPPORTED:"+mipavOutParam.getLabel()); 
						break;
					case Parameter.PARAM_EXTERNAL_IMAGE:					 
					case Parameter.PARAM_IMAGE:
						String outputImageName = plugin.getOutputImageName(mipavOutParam.getLabel());
						ModelImage img = MipavViewUserInterface.getReference().getRegisteredImageByName(outputImageName);
						((ParamVolume)jistParam).setValue(new ImageDataMipavWrapper(img));	                    	
						break;
					default:
						System.err.println("Unrecognized parameter type: " + mipavOutParam.getTypeString());
					}

				}
			
		} catch (final ParserException e) {
			e.printStackTrace();
		}
		for(ModelImage img : modelImageCache) {
			img.disposeLocal();
		}
	}
	private void initPlugin(String str) throws AlgorithmRuntimeException {
		try {
			plugin = (ActionDiscovery)Class.forName(str).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new AlgorithmRuntimeException("Cannot instantiate class: "+str);
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
			throw new AlgorithmRuntimeException("Cannot instantiate class: "+str);
		} catch (ClassNotFoundException e) {
			throw new AlgorithmRuntimeException("Cannot instantiate class: "+str);
		}
	}
	public void init(String str) throws AlgorithmRuntimeException {
		initPlugin(str);
		init(plugin);
	}
	public String getClassTag() {return classFileTag;}


	public String reconcileAndMerge(ParamCollection inputParams2,
			ParamCollection outputParams2) throws InvalidJistMergeException{
		try {
			init(inputParams2.getFirstChildByLabel(classFileTag).getValue().toString());
		} catch (AlgorithmRuntimeException e) {
			throw new InvalidJistMergeException("Unable to instantiate MIPAV plugin class");
		} 
		String message="";
		message = message+reconcileParameters(inputParams,inputParams2);
		message = message+reconcileParameters(outputParams,outputParams2);
		return message;		
	}
}
