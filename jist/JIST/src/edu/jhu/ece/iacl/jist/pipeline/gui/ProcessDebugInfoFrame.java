/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;

/**
 * Display debug information for stdout and stderr.
 * 
 * @author Blake Lucas
 */
public class ProcessDebugInfoFrame extends JFrame implements ActionListener {
	
	/**
	 * Worker to load stdout/stderr in a safe threaded way.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class FileReaderWorker extends SwingWorker<Void, Void> {
		
		/** The text area. */
		JTextArea textArea;
		
		/** The file. */
		File file;
		
		/** The monitor. */
		ProgressMonitor monitor;

		/**
		 * Instantiates a new file reader worker.
		 * 
		 * @param parent
		 *            the parent
		 * @param textArea
		 *            the text area
		 * @param f
		 *            the f
		 */
		public FileReaderWorker(Component parent, JTextArea textArea, File f) {
			this.textArea = textArea;
			this.file = f;
			monitor = new ProgressMonitor(parent, "Reading file " + f.getAbsolutePath(), "Reading...", 0, 100);
		}

		/* (non-Javadoc)
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		protected Void doInBackground() throws Exception {
			BufferedReader in;
			textArea.setText("");
			try {
				// Create input stream from file
				in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				StringBuffer buff = new StringBuffer();
				String str;
				// Read file as string
				int line = 0;
				monitor.setProgress(0);
				while ((str = in.readLine()) != null) {
					if (monitor.isCanceled()) {
						break;
					}
					monitor.setProgress(50);
					monitor.setNote("Reading Line " + (++line));
					textArea.append(str + "\n");
				}
				monitor.setProgress(100);
				in.close();
			} catch (Exception e) {
				System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n" + e.getMessage());
				e.printStackTrace();
				return null;
			}
			textArea.setCaretPosition(textArea.getText().length());
			monitor.close();
			return null;
		}
	}

	/**
	 * Split plane to display stdout and stderr.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class FileText extends JSplitPane {
		
		/** The bottom text area. */
		public JTextArea topTextArea, bottomTextArea;
		
		/** The bottom file. */
		public File topFile, bottomFile;

		/**
		 * Instantiates a new file text.
		 * 
		 * @param tf
		 *            the tf
		 * @param bf
		 *            the bf
		 */
		public FileText(File tf, File bf) {
			super(JSplitPane.VERTICAL_SPLIT);
			JScrollPane topScroll, bottomScroll;
			setTopComponent(topScroll = new JScrollPane());
			setBottomComponent(bottomScroll = new JScrollPane());
			setDividerLocation(300);
			setOneTouchExpandable(true);
			setResizeWeight(0.5);
			this.topFile = tf;
			this.bottomFile = bf;
			topTextArea = new JTextArea();
			topScroll.setViewportView(topTextArea);
			topTextArea.setEditable(false);
			bottomTextArea = new JTextArea();
			bottomScroll.setViewportView(bottomTextArea);
			bottomTextArea.setEditable(false);
			topScroll.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("STDOUT"),
					BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			bottomScroll.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("STDERR"),
					BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			refresh();
		}

		/**
		 * Refresh.
		 */
		public void refresh() {
			FileReaderWorker topWorker = new FileReaderWorker(this, topTextArea, topFile);
			topWorker.execute();
			FileReaderWorker bottomWorker = new FileReaderWorker(this, bottomTextArea, bottomFile);
			bottomWorker.execute();
		}
	}

	/** The tab pane. */
	JTabbedPane tabPane;
	
	/** The bottom tab pane. */
	JTabbedPane bottomTabPane;
	
	/** The refresh button. */
	JButton refreshButton;

	/**
	 * Default constructor.
	 */
	public ProcessDebugInfoFrame() {
		super("Debug Information");
		this.setLayout(new GridLayout(1, 0));
		JPanel smallPane = new JPanel();
		smallPane.setLayout(new BoxLayout(smallPane, BoxLayout.Y_AXIS));
		smallPane.add(tabPane = new JTabbedPane());
		smallPane.add(refreshButton = new JButton("Refresh"));
		refreshButton.addActionListener(this);
		this.add(smallPane);
		this.setPreferredSize(new Dimension(600, 600));
		this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		this.setVisible(false);
		this.pack();
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				PlaceHolder.class.getResource("zoom.gif"))); /**SET ICON HERE**/
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == refreshButton) {
			refresh();
		}
	}

	/**
	 * Load stdout and stderr from specified files.
	 * 
	 * @param name
	 *            name of frame
	 * @param tf
	 *            stdout file
	 * @param bf
	 *            stderr file
	 */
	public void load(String name, File tf, File bf) {
		if (!this.isVisible()) {
			tabPane.removeAll();
		}
		this.setVisible(true);
		FileText text;
		tabPane.addTab(name, text = new FileText(tf, bf));
	}

	/**
	 * Refresh file display.
	 */
	public void refresh() {
		((FileText) tabPane.getSelectedComponent()).refresh();
	}
}
