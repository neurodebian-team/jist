/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.jgraph.JGraph;
import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.event.GraphModelEvent.GraphModelChange;
import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphLayoutCache;

import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithmGroup;
import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule.PipeListener;
import edu.jhu.ece.iacl.jist.pipeline.gui.ParameterPanel;
import edu.jhu.ece.iacl.jist.pipeline.gui.PipeInternalFrame;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;

/**
 * MAPS customized graph.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeJGraph extends JGraph implements DropTargetListener, PipeListener, GraphModelListener {
	
	/** The parent. */
	private PipeInternalFrame parent;
	
	/** The layout. */
	private PipeLayout layout;
	
	/** The port check. */
	protected PipePort portCheck;
	
	/** The current location. */
	Point currentLocation = null;

	/**
	 * References the folding manager.
	 * 
	 * @param parent
	 *            the parent
	 */
	// Construct the Graph using the Model as its Data Source
	public PipeJGraph(PipeInternalFrame parent) {
		super();
		GraphLayoutCache layoutCache = new GraphLayoutCache(new PipeGraphModel(), new DefaultCellViewFactory(), true);
		this.setGraphLayoutCache(layoutCache);
		// Make Ports Visible by Default
		setPortsVisible(true);
		// Use the Grid (but don't make it Visible)
		setGridEnabled(false);
		// Set the Grid Size to 10 Pixel
		setGridSize(6);
		// Set the Tolerance to 2 Pixel
		setTolerance(2);
		// Accept edits if click on background
		setInvokesStopCellEditing(true);
		setGroupsEditable(true);
		// Allows control-drag
		setCloneable(true);
		// Jump to default port on connect
		setJumpToDefaultPort(true);
		setConnectable(true);
		setXorEnabled(false);
		getGraphLayoutCache().setMovesParentsOnCollapse(true);
		setUI(new PipeGraphUI());
		setHandleSize(0);
		Color c = Color.black;
		setLockedHandleColor(Color.black);
		setHandleColor(Color.black);
		setHighlightColor(new Color(128, 0, 0));
		GraphConstants.SELECTION_STROKE = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f);
		this.parent = parent;
		DropTarget dropTarget = new DropTarget(this, this);
		layout = new PipeLayout(parent);
		setEditable(true);
		portCheck = null;
		getGraphLayoutCache().setFactory(new PipeCellViewFactory(this));
		// getGraphLayoutCache().setResizesParentsOnCollapse(true);
		getGraphLayoutCache().setMovesChildrenOnExpand(true);
		// getGraphLayoutCache().setCollapseXScale(0.5);
		// getGraphLayoutCache().setCollapseYScale(0.5);
		this.setPortsScaled(true);
		this.getModel().addGraphModelListener(this);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipePort.PortListener#connectAction(edu.jhu.ece.iacl.jist.pipeline.PipePort)
	 */
	public void connectAction(PipePort owner) {
		if (owner.isInputPort() && (owner.getIncomingConnectors().size() > 0)) {
			for (PipeConnector wire : (Vector<PipeConnector>) owner.getIncomingConnectors()) {
				if (wire.getGraphEdge() == null) {
					if ((wire.getSource().getGraphPort() != null) && (wire.getDestination().getGraphPort() != null)) {
						// Do not create edge unless port view has been created
						// for wire;
						PipeModuleEdge edge = new PipeModuleEdge(wire);
						getGraphLayoutCache().insertEdge(edge, wire.getSource().getGraphPort(),
								wire.getDestination().getGraphPort());
					}
				}
			}
		}
	}

	// Hook for subclassers
	/**
	 * Creates the default graph cell.
	 * 
	 * @param node
	 *            the node
	 * @return the default graph cell
	 */
	protected DefaultGraphCell createDefaultGraphCell(DraggableNode node) {
		DefaultGraphCell cell = new DefaultGraphCell(node.getName());
		// Add one Floating Port
		cell.addPort();
		return cell;
	}

	/**
	 * Creates the graph node.
	 * 
	 * @param mod
	 *            the mod
	 * @return the pipe module cell
	 */
	public PipeModuleCell createGraphNode(PipeModule mod) {
		PipeModuleCell cell = mod.createModuleCell();
		cell.getPipeModule().addListener(this);
		if (!(mod instanceof PipeAlgorithmGroup)) {
			if (mod.getBounds() != null) {
				cell.setBounds(mod.getBounds());
			} else {
				cell.setLocation(0, 0);
			}
		}
		getGraphLayoutCache().insert(cell);
		PipeModule pipe = cell.getPipeModule();
		createUniqueName(pipe);
		pipe.addListener(ParameterPanel.getInstance());
		return cell;
	}

	/**
	 * Creates the unique name.
	 * 
	 * @param pipe
	 *            the pipe
	 * @return true, if successful
	 */
	public boolean createUniqueName(PipeModule pipe) {
		int instanceCount = 0;
		String label = pipe.getLabel();
		Vector<PipeModule> pipes = layout.getAllDescendantPipes();
		for (int i = 0; i < pipes.size(); i++) {
			PipeModule p = pipes.get(i);
			if ((p != pipe) && p.getLabel().equals(label)) {
				instanceCount++;
				label = pipe.getLabel() + " (" + (instanceCount + 1) + ")";
				i = -1;
			}
		}
		if (instanceCount > 0) {
			pipe.setLabel(label);
			return true;
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipePort.PortListener#disconnectAction(edu.jhu.ece.iacl.jist.pipeline.PipePort, edu.jhu.ece.iacl.jist.pipeline.PipePort, edu.jhu.ece.iacl.jist.pipeline.PipeConnector)
	 */
	public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire) {
		if ((wire.getGraphEdge() != null) && (wire.getGraphEdge().getConnector() == wire)) {
			getGraphLayoutCache().remove(new Object[] { wire.getGraphEdge() });
		}
	}

	/**
	 * Dispose.
	 */
	public void dispose() {
		((PipeMarqueeHandler) this.getMarqueeHandler()).dispose();
	}

	/* (non-Javadoc)
	 * @see java.awt.dnd.DropTargetListener#dragEnter(java.awt.dnd.DropTargetDragEvent)
	 */
	public void dragEnter(DropTargetDragEvent e) {
		DataFlavor[] flavors = e.getCurrentDataFlavors();
		for (DataFlavor flavor : flavors) {
			if (flavor.equals(DraggableNode.MODULE_FLAVOR)) {
				e.acceptDrag(DnDConstants.ACTION_MOVE);
				return;
			}
		}
		e.rejectDrag();
	}

	/* (non-Javadoc)
	 * @see java.awt.dnd.DropTargetListener#dragExit(java.awt.dnd.DropTargetEvent)
	 */
	public void dragExit(DropTargetEvent e) {
	}

	/**
	 * DropTaregetListener interface method.
	 * 
	 * @param e
	 *            the e
	 */
	public void dragOver(DropTargetDragEvent e) {
		DataFlavor[] flavors = e.getCurrentDataFlavors();
		for (DataFlavor flavor : flavors) {
			if (flavor.equals(DraggableNode.MODULE_FLAVOR)) {
				e.acceptDrag(DnDConstants.ACTION_MOVE);
				return;
			}
		}
		e.rejectDrag();
	}

	/**
	 * DropTargetListener interface method - What we do when drag is released.
	 * 
	 * @param e
	 *            the e
	 */
	public void drop(DropTargetDropEvent e) {
		if (parent.isSelected()) {
			e.acceptDrop(DnDConstants.ACTION_MOVE);
		} else {
			e.rejectDrop();
			return;
		}
		// Construct Vertex with no Label
		// cast into appropriate data type
		Transferable tr = e.getTransferable();
		DraggableNode node;
		try {
			node = (DraggableNode) tr.getTransferData(DraggableNode.MODULE_FLAVOR);
			Point cursorLocationBis = e.getLocation();
			insert(node, cursorLocationBis);
			e.getDropTargetContext().dropComplete(true);
		} catch (UnsupportedFlavorException e1) {
			System.err.println(getClass().getCanonicalName()+"ATTEMPT TO DROP ITEM ON GRAPH:" + e1.getMessage());
		} catch (IOException e1) {
			System.err.println(getClass().getCanonicalName()+"ATTEMPT TO DROP ITEM ON GRAPH:" + e1.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.dnd.DropTargetListener#dropActionChanged(java.awt.dnd.DropTargetDragEvent)
	 */
	public void dropActionChanged(DropTargetDragEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Gets the current location.
	 * 
	 * @return the current location
	 */
	public Point getCurrentLocation() {
		return currentLocation;
	}

	/**
	 * Gets the pipeline layout.
	 * 
	 * @return the pipeline layout
	 */
	public PipeLayout getPipelineLayout() {
		return layout;
	}

	/**
	 * Get port to check for compatibility.
	 * 
	 * @return the port to check
	 */
	public PipePort getPortToCheck() {
		return portCheck;
	}

	/* (non-Javadoc)
	 * @see org.jgraph.event.GraphModelListener#graphChanged(org.jgraph.event.GraphModelEvent)
	 */
	public void graphChanged(GraphModelEvent e) {
		GraphModelChange change = e.getChange();
		layout.setDirty(true);
		if (change.getChanged() != null) {
			Object[] changed = change.getChanged();
			boolean update = false;
			// Cell property changed
			for (Object obj : changed) {
				if (obj instanceof PipeModuleCell) {
					PipeModuleCell cell = ((PipeModuleCell) obj);
					Rectangle2D bounds = GraphConstants.getBounds(cell.getAttributes());
					String label = (String) cell.getUserObject();
					// Update label if changed
					if (!label.equals(cell.getPipeModule().getLabel())) {
						cell.getPipeModule().setLabel(label);
						// Only permit unique names to be updated
						createUniqueName(cell.getPipeModule());
						update = true;
					}
					if (bounds != null) {
						cell.setBounds(bounds);
					}
				}
				// Handle case if edge is updated or cloned
				if (obj instanceof PipeModuleEdge) {
					PipeModuleEdge edge = ((PipeModuleEdge) obj);
					if (edge.getConnector() == null) {
						PipeConnector connector=PipeConnector.connect(((PipeModulePort) edge.getSource()).getPort(), ((PipeModulePort) edge
								.getTarget()).getPort(), edge);
						edge.setEditable((connector.getSource() instanceof ObjectCollection)
								&& !(connector.getDestination() instanceof ObjectCollection));
						update = true;
					} else {
						try {
							int newVal = Integer.parseInt(((PipeModuleEdge) obj).getUserObject().toString());
							int oldVal = ((PipeModuleEdge) obj).getConnector().getSourceIndex();
							// Update the source index for a connector
							if (oldVal != newVal) {
								((PipeModuleEdge) obj).getConnector().setSourceIndex(newVal);
								if (oldVal != -1) {
									update = true;
								}
							}
						} catch (NumberFormatException evt) {
						}
					}
				}
			}
			// Refresh parameter panel if a change occurred to a cell
			if (update) {
				ParameterPanel.getInstance().refreshView();
			}
		}
		if (change.getInserted() != null) {
			Object[] inserted = change.getInserted();
			boolean update = false;
			for (Object obj : inserted) {
				if (obj instanceof PipeModuleCell) {
					PipeModule mod = ((PipeModuleCell) obj).getPipeModule();
					mod.addListener(ParameterPanel.getInstance());
					mod.addListener(this);
					layout.add(mod);
					update = true;
				}
				if (obj instanceof PipeModuleEdge) {
					PipeModuleEdge edge = ((PipeModuleEdge) obj);
					if (edge.getConnector() == null) {
						PipeConnector connector=PipeConnector.connect(((PipeModulePort) edge.getSource()).getPort(), ((PipeModulePort) edge
								.getTarget()).getPort(), edge);
						edge.setEditable((connector.getSource() instanceof ObjectCollection)
								&& !(connector.getDestination() instanceof ObjectCollection));
						update = true;
					}
				}
			}
			if (update) {
				ParameterPanel.getInstance().refreshView();
			}
		}
		// If item is removed update cells
		if (change.getRemoved() != null) {
			Object[] disconnect = change.getRemoved();
			boolean removeCell = false;
			for (Object obj : disconnect) {
				// If edge is removed, remove connector as well
				if (obj instanceof PipeModuleEdge) {
					PipeModuleEdge edge = (PipeModuleEdge) obj;
					PipeConnector connector = edge.getConnector();
					edge.setConnector(null);
					if (connector != null) {
						connector.disconnect();
					}
				}
				// If cell is removed, disconnect cell and remove cell from
				// layout
				if (obj instanceof PipeModuleCell) {
					PipeModuleCell cell = (PipeModuleCell) obj;
					cell.getPipeModule().disconnect();
					layout.remove(cell.getPipeModule());
					removeCell = true;
				}
			}
			// If removal occurred, update parameter panel
			if (removeCell) {
				ParameterPanel.getInstance().refreshView();
			}
		}
	}

	/**
	 * Insert.
	 * 
	 * @param node
	 *            the node
	 * @param cursorLocationBins
	 *            the cursor location bins
	 */
	public void insert(DraggableNode node, Point cursorLocationBins) {
		PipeModule mod = node.getFactory().createPipe();
		if (mod != null) {
			currentLocation = cursorLocationBins;
			PipeModuleCell cell = mod.init(this);
			Rectangle2D bounds = getCellBounds(cell);
			cell.setLocation((int) (cursorLocationBins.x / getScale()), (int) (cursorLocationBins.y / getScale()));
			getGraphLayoutCache().collapse(new Object[] { cell });
		}
	}

	// Refresh graph
	/**
	 * Refresh.
	 */
	public void refresh() {	
		setOffscreenValid(false);
		repaint();
	}

	/**
	 * Sets the pipeline layout.
	 * 
	 * @param layout
	 *            the layout
	 * @param monitor
	 *            the monitor
	 */
	public void setPipelineLayout(PipeLayout layout, ProgressMonitor monitor) {
		this.layout = layout;
		layout.setFrame(parent);
		ParameterPanel.getInstance().setIgnoreRefresh(true);
		layout.init(this, monitor);
		ParameterPanel.getInstance().setIgnoreRefresh(false);
		ParameterPanel.getInstance().refreshView();
		layout.setDirty(false);
	}

	/**
	 * Set port to check for compatibility.
	 * 
	 * @param port
	 *            the port
	 */
	public void setPortToCheck(PipePort port) {
		this.portCheck = port;
	}
}
