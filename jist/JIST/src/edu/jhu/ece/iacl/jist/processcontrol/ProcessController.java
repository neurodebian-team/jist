package edu.jhu.ece.iacl.jist.processcontrol;

import java.io.InputStream;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Interface ProcessController.
 */
public interface  ProcessController {
	 
 	/**
 	 * Gets the status.
 	 * 
 	 * @return the status
 	 */
 	ProcessStatus getStatus();
	 
 	/**
 	 * Gets the submission time.
 	 * 
 	 * @return the submission time
 	 */
 	long getSubmissionTime();
	 
 	/**
 	 * Gets the exit code.
 	 * 
 	 * @return the exit code
 	 */
 	Integer getExitCode();
	 
 	/**
 	 * Submit.
 	 * 
 	 * @return true, if successful
 	 */
 	boolean submit();
	 
 	/**
 	 * Destroy.
 	 * 
 	 * @return true, if successful
 	 */
 	boolean destroy();
	 
	 /**
 	 * Gets the process.
 	 * 
 	 * @return the process
 	 */
 	Process getProcess();
	 
 	/**
 	 * Wait for.
 	 * 
 	 * @return the int
 	 * 
 	 * @throws InterruptedException the interrupted exception
 	 */
 	int waitFor() throws InterruptedException;

	 /**
 	 * Gets the stdout file.
 	 * 
 	 * @return the stdout file
 	 */
 	InputStream getStdoutFile();
	 
 	/**
 	 * Gets the stderr file.
 	 * 
 	 * @return the stderr file
 	 */
 	InputStream getStderrFile();
	 
	 /**
 	 * Control job.
 	 * 
 	 * @param Action the action
 	 */
 	void controlJob(ProcessControlAction Action);
	 
 	/**
 	 * To string.
 	 * 
 	 * @return the string
 	 */
 	String toString();
	//... etc. 
	/**
	 * Setup.
	 * 
	 * @param command the command
	 * 
	 * @return true, if successful
	 */
	boolean Setup(List<String> command);
}
