package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;

// TODO: Auto-generated Javadoc
/**
 * Integer Voxel Type.
 * 
 * @author Blake Lucas
 */
public class VoxelInt extends Voxel{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7230401369189581953L;
	
	/** The vox. */
	private int vox;

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getBoolean()
	 */
	public boolean getBoolean() {
		return (vox!=0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getShort()
	 */
	public short getShort() {
		return (short)vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getColor()
	 */
	public Color getColor() {
		return new Color(vox,vox,vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getDouble()
	 */
	public double getDouble() {
		return (double)(vox);
	}
	
	/**
	 * Instantiates a new voxel int.
	 * 
	 * @param vox the vox
	 */
	public VoxelInt(int vox){
		this.vox=vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#clone()
	 */
	public Voxel clone() {
		return new VoxelInt(vox);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public void set(Voxel v) {
		this.vox=v.getShort();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(boolean)
	 */
	@Override
	public void set(boolean vox) {
		this.vox=(int)((vox)?1:0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(short)
	 */
	@Override
	public void set(short vox) {
		this.vox=vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(int)
	 */
	public void set(int vox) {
		this.vox=(int)vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(java.awt.Color)
	 */
	public void set(Color vox) {
		float[] hsb=new float[3];
		hsb=Color.RGBtoHSB(vox.getRed(),vox.getGreen(),vox.getBlue(),hsb);
		this.vox=(int)Math.round(255.0*hsb[2]);		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(double)
	 */
	public void set(double vox) {
		this.vox=(int)Math.round(vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#add(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelInt add(Voxel v) {
		return new VoxelInt((int)(getShort()+v.getShort()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#sub(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelInt sub(Voxel v) {
		return new VoxelInt((int)(getShort()-v.getShort()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#mul(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelInt mul(Voxel v) {
		return new VoxelInt((int)(getShort()*v.getShort()));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#div(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelInt div(Voxel v) {
		return new VoxelInt((int)(getShort()/v.getShort()));

	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#neg()
	 */
	public Voxel neg() {
		return new VoxelInt((int)(-getShort()));
	}
	
	/**
	 * Instantiates a new voxel int.
	 * 
	 * @param b the b
	 */
	public VoxelInt(short b){
		set(b);
	}
	
	/**
	 * Instantiates a new voxel int.
	 */
	public VoxelInt(){
		this.vox=0;
	}
	
	/**
	 * Instantiates a new voxel int.
	 * 
	 * @param v the v
	 */
	public VoxelInt(Voxel v){set(v);}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Voxel obj) {
		return (int)(this.getShort()-((Voxel)obj).getShort());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getInt()
	 */
	public int getInt() {
		return vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#toString()
	 */
	public String toString(){
		return (vox+"");
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getType()
	 */
	public VoxelType getType() {
		return VoxelType.INT;
	}
}
