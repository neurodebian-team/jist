/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jgraph.JGraph;
import org.jgraph.graph.CellView;
import org.jgraph.graph.CellViewRenderer;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.EdgeView;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.VertexRenderer;
import org.jgraph.graph.VertexView;

/**
 * Algorithm group cell view.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeAlgorithmGroupView extends VertexView {
	
	/**
	 * The Class ActivityRenderer.
	 */
	public static class ActivityRenderer extends VertexRenderer {
		
		/** Default handle bounds for renderer, '+' or '-'. */
		public static Rectangle handle = new Rectangle(0, 0, 10, 10);
		
		/**
		 * Specifies whether the current view is a rich text value, and if the
		 * image should be stretched.
		 */
		protected boolean isGroup = false;
		
		/** Holds the background and foreground of the graph. */
		protected Color handleColor = Color.white, graphForeground = Color.gray;

		/**
		 * Return a slightly larger preferred size than for a rectangle.
		 * 
		 * @return the preferred size
		 */
		public Dimension getPreferredSize() {
			Dimension d = super.getPreferredSize();
			d.width += d.height / 5;
			return d;
		}

		/**
		 * Overrides the parent implementation to return the value component
		 * stored in the user object instead of this renderer if a value
		 * component exists. This applies some of the values installed to this
		 * renderer to the value component (border, opaque) if the latter is a
		 * JComponent.
		 * 
		 * @param graph
		 *            the graph
		 * @param view
		 *            the view
		 * @param sel
		 *            the sel
		 * @param focus
		 *            the focus
		 * @param preview
		 *            the preview
		 * @return Returns a configured renderer for the specified view.
		 */
		public Component getRendererComponent(JGraph graph, CellView view, boolean sel, boolean focus, boolean preview) {
			isGroup = DefaultGraphModel.isGroup(graph.getModel(), view.getCell());
			return super.getRendererComponent(graph, view, sel, focus, preview);
		}

		/**
		 * Detect whether or not a point has hit the group/ungroup image.
		 * 
		 * @param pt
		 *            the point to check
		 * @return whether or not the point lies within the handle
		 */
		public boolean inHitRegion(Point2D pt) {
			return handle.contains(pt.getX(), pt.getY());
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.VertexRenderer#paint(java.awt.Graphics)
		 */
		public void paint(Graphics g) {
			int b = borderWidth;
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			Graphics2D g2 = (Graphics2D) g;
			Dimension d = getSize();
			boolean tmp = selected;
			int roundRectArc = PipeAlgorithmGroupView.getArcSize(d.width - b, d.height - b);
			g.setColor(this.getBackground());
			// g.fillRect(b / 2, b / 2, d.width,d.height);
			if (super.isOpaque()) {
				g.setColor(super.getBackground());
				g.fillRect(b / 2, b / 2, d.width - (int) (b * 1.5), d.height - (int) (b * 1.5));
			}
			try {
				setBorder(null);
				setOpaque(false);
				selected = false;
				super.paint(g);
			} finally {
				selected = tmp;
			}
			g2.setStroke(GraphConstants.SELECTION_STROKE);
			if (selected) {
				g.setColor(highlightColor);
			} else {
				g.setColor(Color.gray);
			}
			g.drawRect(b / 2, b / 2, d.width - (int) (b * 1.5), d.height - (int) (b * 1.5));
			if (isGroup) {
				g2.setStroke(new BasicStroke(b));
				g.setColor(handleColor);
				g.fill3DRect(handle.x, handle.y, handle.width, handle.height, true);
				g.setColor(graphForeground);
				g.drawRect(handle.x, handle.y, handle.width, handle.height);
				g.drawLine(handle.x + 1, handle.y + handle.height / 2, handle.x + handle.width - 2, handle.y
						+ handle.height / 2);
				if (view.isLeaf()) {
					g.drawLine(handle.x + handle.width / 2, handle.y + 1, handle.x + handle.width / 2, handle.y
							+ handle.height - 2);
				}
			}
		}
	}

	/** The renderer. */
	public static transient ActivityRenderer renderer = new ActivityRenderer();

	// todo public Point2D getPerimeterPoint(Point source, Point p) {
	/**
	 * getArcSize calculates an appropriate arc for the corners of the rectangle
	 * for boundary size cases of width and height.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @return the arc size
	 */
	public static int getArcSize(int width, int height) {
		int arcSize;
		// The arc width of a activity rectangle is 1/8th of the larger
		// of the two of the dimensions passed in, but at most 1/2
		// of the smaller of the two. 1/8 because it looks nice and 1/2
		// so the arc can complete in the given dimension
		/*
		 * if (width <= height) { arcSize = height / 8; if (arcSize > (width /
		 * 2)) { arcSize = width / 2; } } else { arcSize = width / 8; if
		 * (arcSize > (height / 2)) { arcSize = height / 2; } }
		 */
		return 18;
	}

	/**
	 * Default constructor.
	 */
	public PipeAlgorithmGroupView() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.AbstractCellView#getCell()
	 */
	public PipeModuleCell getCell() {
		return (PipeModuleCell) super.getCell();
	}

	/**
	 * Returns the intersection of the bounding rectangle and the straight line
	 * between the source and the specified point p. The specified point is
	 * expected not to intersect the bounds.
	 * 
	 * @param edge
	 *            the edge
	 * @param source
	 *            the source
	 * @param p
	 *            the p
	 * @return the perimeter point
	 */
	public Point2D getPerimeterPoint(EdgeView edge, Point2D source, Point2D p) {
		Rectangle2D bounds = this.getBounds();
		Point2D p2;
		PipePortView src = (PipePortView) (((PipeEdgeView) edge).getSource());
		PipePortView dest = (PipePortView) (((PipeEdgeView) edge).getTarget());
		if ((src != null) && (dest == null)) {
			p2 = new Point2D.Double(bounds.getCenterX(), bounds.getMinY());
		} else if ((src == null) && (dest != null)) {
			p2 = new Point2D.Double(bounds.getCenterX(), bounds.getMaxY());
		} else if ((src == null) && (dest == null)) {
			if (p.getY() < bounds.getCenterY()) {
				p2 = new Point2D.Double(bounds.getCenterX(), bounds.getMinY());
			} else {
				p2 = new Point2D.Double(bounds.getCenterX(), bounds.getMaxY());
			}
		} else {
			p2 = p;
		}
		if (getRenderer() instanceof VertexRenderer) {
			return ((VertexRenderer) getRenderer()).getPerimeterPoint(this, source, p2);
		}
		return super.getPerimeterPoint(edge, source, p2);
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.VertexView#getRenderer()
	 */
	public CellViewRenderer getRenderer() {
		return renderer;
	}
}