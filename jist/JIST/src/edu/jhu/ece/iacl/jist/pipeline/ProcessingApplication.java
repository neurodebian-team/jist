/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory;
import edu.jhu.ece.iacl.jist.pipeline.gui.PlugInSelector;
import gov.nih.mipav.plugins.PlugInGeneric;
import gov.nih.mipav.view.MipavUtil;

import java.io.File;
import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * Processing Application for MAPS. This also functions as a generic MIPAV
 * plug-in. Methods in this class should not be overridden. Instead, all
 * algorithm specific customizations should be made in Processing Algorithm.
 * 
 * @author Blake Lucas
 */
public class ProcessingApplication implements PlugInGeneric {
	
	/** Common dialog class for input and output user interface. */
	private ProcessingDialog dialog;
	
	/** Algorithm to be executed by this processing module. */
	private ProcessingAlgorithm algorithm;
	
	/**
	 * Default constructor.
	 */
	public ProcessingApplication() {
		super();
	}
	
	/**
	 * Specify the processing algorithm to be executed by this module.
	 * 
	 * @param algorithm processing algorithm
	 */
	public ProcessingApplication(ProcessingAlgorithm algorithm) {
		super();
		this.algorithm = algorithm;
		dialog = new ProcessingDialog(algorithm);
	}

	/**
	 * Get the algorithm used by this processing module.
	 * 
	 * @return processing algorithm
	 */
	public ProcessingAlgorithm getAlgorithm() {
		return algorithm;
	}

	/**
	 * Get the dialog used by this processing module.
	 * 
	 * @return processing dialog
	 */
	public ProcessingDialog getDialog() {
		if (dialog == null) {
			PipeAlgorithmFactory factory = PlugInSelector.askForAlgorithm(MipavViewUserInterface.getReference()
					.getMainFrame());
			if (factory != null) {
				PipeAlgorithm pipe = factory.createPipe();
				if (pipe == null) {
					MipavUtil.displayError("Could Not Create Input Dialog!");
				} else {
					if (pipe instanceof PipeAlgorithmGroup) {
						dialog = new ProcessingGroupDialog((PipeAlgorithmGroup)pipe);
					} else {
						this.algorithm = pipe.getAlgorithm();
						dialog = new ProcessingDialog(algorithm);
						dialog.init(pipe);
					}
				}
			}
		}
		return dialog;
	}

	/**
	 * This method is inherited from MIPAV and should not be overridden.
	 */
	public final void run() {
		ProcessingDialog d = getDialog();		
		if (d != null) {
			d.start(/*MipavViewUserInterface.getReference()*/);
		}
	}
	
	public void runInProcess() throws IOException {
		File outDir = algorithm.getOutputDirectory();
		algorithm.runAlgorithm();
		algorithm.setOutputDirectory(outDir);
		algorithm.saveResources(true);
		algorithm.writeSummaryFile();
//		MipavController.clearReigstry();
	}
}
