/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.util.LinkedList;
import java.util.Vector;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleEdge;

// TODO: Auto-generated Javadoc
/**
 * A Pipe Connector provides a connection between an input and output port.
 * 
 * @author Blake Lucas
 */
public class PipeConnector {
	
	/**
	 * Connect source port to destination.
	 * 
	 * @param head source port
	 * @param tail destination port
	 * 
	 * @return the pipe connector
	 */
	public static PipeConnector connect(PipePort head, PipePort tail) {
		return connect(head, tail, null);
	}

	/**
	 * Connect source port to destination.
	 * 
	 * @param head source port
	 * @param tail destination port
	 * @param edge graph edge
	 * 
	 * @return the pipe connector
	 */
	public static PipeConnector connect(PipePort head, PipePort tail, PipeModuleEdge edge) {
		if (tail.isInputPort()) {
			return (isCompatible(head, tail) && !head.isConnectedTo(tail)) ? new PipeConnector(head, tail, edge) : null;
		} else {
			return (isCompatible(tail, head) && !tail.isConnectedTo(head)) ? new PipeConnector(tail, head, edge) : null;
		}
	}

	/**
	 * Returns true if source port can be safely connected to destination port
	 * and the connection will not create loops in the graph.
	 * 
	 * @param head source port
	 * @param tail destination port
	 * 
	 * @return true if compatible
	 */
	public static boolean isCompatible(PipePort head, PipePort tail) {
		if (!head.isOutputPort() || !tail.isInputPort()) {
			return false;
		}
		if (head.getOwner() == tail.getOwner()) {
			return false;
		}
		if (!tail.isCompatible(head)) {
			return false;
		}
		// Detect if connection will create loops
		if (head instanceof PipeParentPort) {
			// Search among parent/child ports
			LinkedList<PipeSource> pipeQueue = new LinkedList<PipeSource>();
			PipeSource startPipe = (PipeSource) head.getOwner();
			pipeQueue.add((PipeSource) tail.getOwner());
			while (pipeQueue.size() > 0) {
				PipeSource pipe = pipeQueue.removeFirst();
				PipePort out = pipe.getParentPort();
				for (PipePort in : (Vector<PipePort>) out.getOutgoingPorts()) {
					PipeModule owner = in.getOwner();
					if (owner == startPipe) {
						return false;
					}
					if (!pipeQueue.contains(owner)) {
						pipeQueue.add((PipeSource) owner);
					}
				}
			}
		} else {
			// Search among input/output ports
			LinkedList<PipeModule> pipeQueue = new LinkedList<PipeModule>();
			PipeModule startPipe = head.getOwner();
			pipeQueue.add(tail.getOwner());
			while (pipeQueue.size() > 0) {
				PipeModule pipe = pipeQueue.removeFirst();
				for (PipePort out : pipe.getOutputPorts()) {
					for (PipePort in : (Vector<PipePort>) out.getOutgoingPorts()) {
						PipeModule owner = in.getOwner();
						if (owner == startPipe) {
							return false;
						}
						if (!pipeQueue.contains(owner)) {
							pipeQueue.add(owner);
						}
					}
				}
			}
		}
		return true;
	}

	/** source port. */
	protected PipePort src;
	
	/** destination port. */
	protected PipePort dest;
	
	/** index into source if port can accept multiple connections. */
	protected int srcIndex;
	
	/** graph edge representing this connector. */
	transient protected PipeModuleEdge gedge;

	/**
	 * Default constructor.
	 */
	public PipeConnector() {
		this(null, null);
	}

	/**
	 * Default constructor.
	 * 
	 * @param head source port
	 * @param tail destination port
	 */
	protected PipeConnector(PipePort head, PipePort tail) {
		this(head, tail, null);
	}

	/**
	 * Create connection between a source and destination.
	 * 
	 * @param head source port
	 * @param tail destination port
	 * @param edge graph edge
	 */
	protected PipeConnector(PipePort head, PipePort tail, PipeModuleEdge edge) {
		// If another connection cannot be added to the destination, remove the
		// last connection
		// and use this one instead
		if ((tail.getMaxIncoming() > 0) && (tail.getIncomingConnectors().size() >= tail.getMaxIncoming())) {
			tail.disconnect((PipeConnector) tail.getIncomingConnectors().lastElement());
		}
		// If another connection cannot be added to the source, remove the last
		// connection
		// and use this one instead
		if ((head.getMaxOutgoing() > 0) && (head.getOutgoingConnectors().size() >= head.getMaxOutgoing())) {
			head.disconnect((PipeConnector) head.getOutgoingConnectors().lastElement());
		}
		this.gedge = edge;
		if (gedge != null) {
			gedge.setConnector(this);
		}
		this.src = head;
		this.dest = tail;
		// Indicate no source index used
		this.srcIndex = -1;
		head.setUseConnector(true);
		tail.setUseConnector(true);
		head.outgoingConnectors.add(this);
		tail.incomingConnectors.add(this);
		head.notifyListenersOfConnection();
		tail.notifyListenersOfConnection();
	}

	/**
	 * Remove this connection from the source and destination port.
	 */
	public void disconnect() {
		PipePort tmpHead = src;
		PipePort tmpTail = dest;
		if (src != null) {
			src.outgoingConnectors.remove(this);
		}
		if (dest != null) {
			dest.incomingConnectors.remove(this);
		}
		this.src = null;
		this.dest = null;
		if ((tmpHead != null) && (tmpTail != null)) {
			tmpHead.notifyListenersOfDisconnection(tmpTail, this);
		}
		this.gedge = null;
	}

	/**
	 * Get destination port.
	 * 
	 * @return destination port
	 */
	public PipePort getDestination() {
		return dest;
	}

	/**
	 * Get graph edge.
	 * 
	 * @return graph edge
	 */
	public PipeModuleEdge getGraphEdge() {
		return gedge;
	}

	/**
	 * Get source port.
	 * 
	 * @return source port
	 */
	public PipePort getSource() {
		return src;
	}

	/**
	 * Get source index if port accepts multiple connections.
	 * 
	 * @return the source index
	 */
	public int getSourceIndex() {
		return srcIndex;
	}

	/**
	 * Set destination port.
	 * 
	 * @param dst destination port
	 */
	public void setDestination(PipePort dst) {
		this.dest = dst;
	}

	/**
	 * Set graph edge used to render this connector.
	 * 
	 * @param edge the edge
	 */
	public void setGraphEdge(PipeModuleEdge edge) {
		this.gedge = edge;
	}

	/**
	 * Set source port.
	 * 
	 * @param src source port
	 */
	public void setSource(PipePort src) {
		this.src = src;
	}

	/**
	 * Set the index into the source if the source is a variable collection of
	 * different parameters.
	 * 
	 * @param i index
	 */
	public void setSourceIndex(int i) {
		this.srcIndex = i;
		if (gedge != null) {
			gedge.setUserObject(i + "");
		}
	}

	/**
	 * Get description of connection.
	 * 
	 * @return the string
	 */
	public String toString() {
		return "[" + ((src != null) ? src.getLabel() : "null") + "," + ((dest != null) ? dest.getLabel() : "null")
				+ "]";
	}
}
