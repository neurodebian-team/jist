/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolumeCollection;

/**
 * Input view to specify a collection of volumes with a specific weight. This
 * class is particularly useful for data fusion algorithms.
 * 
 * @author Blake Lucas
 */
public class ParamWeightedVolumesURIInputView extends ParamVolumeURICollectionInputView implements ListCellRenderer {
	
	/** The Constant listDimension. */
	public static final Dimension listDimension = new Dimension(50, 20);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -153709250569376985L;
	
	/** The text fields. */
	protected ArrayList<JComponent> textFields = new ArrayList<JComponent>();
	
	/** The weight pane. */
	protected JPanel weightPane;

	/**
	 * Construct multiple image selection scroll pane with weight inputs.
	 * 
	 * @param param
	 *            the parameter
	 */
	public ParamWeightedVolumesURIInputView(ParamVolumeCollection param) {
		super(param);
		// Create pane to layout weight entry fields
		weightPane = new JPanel();
		weightPane.setLayout(new BoxLayout(weightPane, BoxLayout.Y_AXIS));
		fileListPane.add(weightPane, BorderLayout.EAST);
		// Create header to describe image list box
	}


	/**
	 * Use custom render for list components so that they are the same size as
	 * the weight entry field.
	 * 
	 * @param list
	 *            listbox
	 * @param value
	 *            listbox entry
	 * @param index
	 *            selected index
	 * @param isSelected
	 *            is selected
	 * @param cellHasFocus
	 *            has focus
	 * @return the list cell renderer component
	 */
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		Component pane = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		pane.setMinimumSize(listDimension);
		pane.setPreferredSize(listDimension);
		return pane;
	}

	/**
	 * Get weighted volume parameter.
	 * 
	 * @return the parameter
	 */
	public ParamWeightedVolumeCollection getParameter() {
		return (ParamWeightedVolumeCollection) this.param;
	}

	/**
	 * Build list box with selected items.
	 * 
	 * @param items
	 *            Selected Items A null value will use the previous selected
	 *            items
	 * @return true, if update list box
	 */
	protected /*synchronized*/ boolean updateListBox() {
		if (super.updateImageListBox()) {
			// No textfields for weights exist yet so do nothing
			if (textFields == null) {
				return true;
			}
			updateWeightPanel();
			return true;
		} else {
			return false;
		}
	}

	private void updateWeightPanel(){

		textFields.clear();
		// Remove all weight fields
		weightPane.removeAll();
		// Create a new weight entry field for each listbox component
		for (int i = 0; i < fileListBoxEntries.size(); i++) {
			// Create new text field from list box entry
			ParamWeightedVolume vol = (ParamWeightedVolume) fileListBoxEntries.get(i);
			if(vol.getValue()!=null){
				ParamInputView view=vol.getWeightParameter().getInputView();
				for(ParamViewObserver obs:getObservers()){
					view.addObserver(obs);
				}
				JComponent comp=view.getField();
				textFields.add(comp);
				// Set preferred size
				 comp.setPreferredSize(ParamInputView.defaultNumberFieldDimension);
				weightPane.add( comp);

			}
		}
		// Update scrollPane
		fileScrollPane.revalidate();
	}
	/**
	 * Select images to load when the browse button is clicked.
	 * 
	 * @param event
	 *            browse button clicked
	 */
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		updateWeightPanel();
	}
	/**
	 * Update pane with new value from parameter.
	 */
	public void update() {
		super.update();
		updateWeightPanel();
	}
	public void commit(){
		super.commit();
		ParamWeightedVolumeCollection p=getParameter();
		for (int i = 0; i < fileListBoxEntries.size(); i++) {
			// Create new text field from list box entry
			ParamWeightedVolume vol = (ParamWeightedVolume) fileListBoxEntries.get(i);
			vol.getInputView().commit();
			((ParamWeightedVolume)(p.getParameters()).get(i)).setWeight(vol.getWeight());
		}
	}
}
