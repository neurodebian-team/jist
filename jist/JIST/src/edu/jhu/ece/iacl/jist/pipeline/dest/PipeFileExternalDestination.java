/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.dest;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;


/**
 * Destination to collect a summary of parameter values for all experiments.
 * 
 */
public class PipeFileExternalDestination extends PipeExternalDestination {
	protected ParamFileCollection outputFileCol;
	protected boolean xmlEncodeModule(Document document, Element parent) {
		return super.xmlEncodeModule(document, parent);
//		Element em;	
//		em = document.createElement("outputFileCol");		
//		outputFileCol.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		outputFileCol = (ParamFileCollection) outputParams.getFirstChildByName("Output File");
//		outputFileCol = new ParamFileCollection();
//		outputFileCol.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"outputFileCol"));
	}
	/**
	 * Clean up destination after iterating though all experiments.
	 */
	public void complete() {
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createInputParams() {
		selectedParams = new ParamFile("Parameters",new FileExtensionFilter(new String[]{}));
		ParamCollection input = new ParamCollection();
		input.setLabel("External File");
		input.setName("extfile");
		input.setCategory("Externalize");
		return input;
	}
	/**
	 * Get input parameter
	 */
	public ParamFile getInputParam(){
		return (ParamFile)selectedParams;
	}
	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createOutputParams() {
		ParamCollection output = new ParamCollection();
		output.add(outputFileCol=new ParamFileCollection("Output File",new FileExtensionFilter(new String[]{})));
		return output;
	}

	/**
	 * Iterate through objects.
	 * 
	 * @param monitor
	 *            the monitor
	 */
	public void iterate(ProgressMonitor monitor) {
		outputFileCol.add(getInputParam().getValue());
	}

	public void reset() {
		outputFileCol.clear();	
	}
	
}
