/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;

/**
 * Iterate through all surfaces in a directory.
 * 
 * @author Blake Lucas
 */
public class PipeSurfaceDirectorySurface extends PipeFileDirectorySource {
	/**
	 * Instantiates a new pipe surface directory surface.
	 */
	public PipeSurfaceDirectorySurface() {
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group=super.createInputParams();
		group.setLabel("Surface Directory");
		group.setName("surfacedirectory");
		group.setCategory("Surface");
		fileExtension.setValue("vtk");
		return group;
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Surface");
		group.add(fileParam = new ParamSurface("Surface"));
		return group;
	}

	public ParamSurface getOutputParam() {
		return (ParamSurface)fileParam;
	}
}
