/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout.RunParameters;
import edu.jhu.ece.iacl.jist.processcontrol.DispatchController;

/**
 * Preference panel.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class LayoutPreferencePanel extends JPanel implements ActionListener {
	
	/**
	 * Select dir.
	 * 
	 * @param oldDir
	 *            the old dir
	 * @return the file
	 */
	private static File selectDir(File oldDir) {
		JFileChooser loadDialog = new JFileChooser("Specify Directory");
		if (oldDir!=null&&oldDir.exists()) {
			loadDialog.setCurrentDirectory(oldDir);
		} else {
			loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		}
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = loadDialog.showOpenDialog(null);
		if ((returnVal == JFileChooser.APPROVE_OPTION) && loadDialog.getSelectedFile().isDirectory()) {
			return loadDialog.getSelectedFile();
		} else {
			return null;
		}
	}

	/**
	 * Show dialog.
	 * 
	 * @param comp
	 *            the comp
	 * @param layout
	 *            the layout
	 * @return true, if successful
	 */
	public static boolean showDialog(Component comp, PipeLayout layout) {
		LayoutPreferencePanel panel = new LayoutPreferencePanel(layout);
		while (true) {
			int n = JOptionPane.showConfirmDialog(comp, panel, "Preferences [" + layout.getTitle() + "]",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (n == 0) {
				if (panel.update()) {
					return true;
				} else {
					JOptionPane.showMessageDialog(comp, "Invalid parameter.", "Run Parameter Error",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				return false;
			}
		}
	}

	/** The browse input. */
	private JButton browseInput;
	
	/** The browse output. */
	private JButton browseOutput;
	
	/** The dir input. */
	private JTextField dirInput;
	
	/** The dir output. */
	private JTextField dirOutput;
	
	/** The use grid Engine. */
	private JCheckBox useGridEngine;

	/** The use relative. */
	private JCheckBox useRelative;
	
	/** The heap size. */
	private JSpinner heapSize;
	
	/** The sim procs. */
	private JSpinner simProcs;
	
	/** The params. */
	private RunParameters params;
	
	/** The layout. */
	private PipeLayout layout;
	
	/** naming convention */
	//private JComboBox namingConvention;

	/**
	 * Constructor.
	 * 
	 * @param layout
	 *            layout
	 */
	public LayoutPreferencePanel(PipeLayout layout) {
		super();
		createPane();
		this.layout = layout;
		init(layout);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == browseInput) {
			params.setInputDirectory(selectDir(params.getInputDirectory()));
			if(params.getInputURI()!=null){
				dirInput.setText(params.getInputURI().toString());
			}
		} else if (evt.getSource() == browseOutput) {
			File f=selectDir(params.getOutputDirectory());
			layout.setOutputDir(f);
			if(params.getOutputURI()!=null){
				dirOutput.setText(params.getOutputURI().toString());
			}
		} else if (evt.getSource() == useRelative) {
			enableRelative(useRelative.isSelected());
		}
	}

	/**
	 * Create panel to display preferences.
	 */
	protected void createPane() {
		this.setLayout(new BorderLayout());
		JPanel small = new JPanel();
		BoxLayout layout = new BoxLayout(small, BoxLayout.PAGE_AXIS);
		small.setLayout(layout);
		this.add(small, BorderLayout.NORTH);
		JPanel itemPane = new JPanel(new BorderLayout());
		//itemPane.add(new JLabel("Relative Path "), BorderLayout.WEST);
		JPanel relativePane = new JPanel(new BorderLayout());
		relativePane.add(dirInput = new JTextField(20), BorderLayout.CENTER);
		relativePane.add(browseInput = new JButton("Browse"), BorderLayout.EAST);
		relativePane.add(useRelative = new JCheckBox("Use Relative Path"), BorderLayout.SOUTH);
		useRelative.setEnabled(false);
		//itemPane.add(relativePane, BorderLayout.CENTER);
		small.add(itemPane);
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Output Path "), BorderLayout.WEST);
		itemPane.add(dirOutput = new JTextField(20), BorderLayout.CENTER);
		itemPane.add(browseOutput = new JButton("Browse"), BorderLayout.EAST);
		small.add(itemPane);
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Max Heap Size"), BorderLayout.WEST);
		itemPane.add(heapSize = new JSpinner(new SpinnerNumberModel(1024, 16, 1000000, 20)), BorderLayout.EAST);
		small.add(itemPane);
		itemPane = new JPanel(new BorderLayout());
		//int maxProcs = Runtime.getRuntime().availableProcessors();
		//itemPane.add(new JLabel("Simultaneous Processes ("+maxProcs+" Available CPU"+((maxProcs>1)?"s":"")+")"), BorderLayout.WEST);
		itemPane.add(new JLabel("Simultaneous Processes "), BorderLayout.WEST);
		itemPane.add(simProcs = new JSpinner(new SpinnerNumberModel(4, 1, 100, 1)), BorderLayout.EAST);
		small.add(itemPane);
		/*
		itemPane = new JPanel(new BorderLayout());
		itemPane.add(new JLabel("Output Directory Naming Convention"), BorderLayout.WEST);
		itemPane.add(namingConvention =new JComboBox(new String[]{"Tree Hierarchy","Universal ID"}), BorderLayout.EAST);
		small.add(itemPane);
		*/
		itemPane = new JPanel(new BorderLayout());
		String GridString = ((new DispatchController()).initDRMAASession())?"(Online)":"(Offline)";
		itemPane.add(new JLabel("Use Grid Engine If Available "+GridString), BorderLayout.WEST);
		itemPane.add(useGridEngine = new JCheckBox("",false), BorderLayout.EAST);
		small.add(itemPane);
		
		
		browseInput.addActionListener(this);
		browseOutput.addActionListener(this);
		useRelative.addActionListener(this);
	}

	/**
	 * Enable relative.
	 * 
	 * @param enabled
	 *            the enabled
	 */
	protected void enableRelative(boolean enabled) {
		useRelative.setSelected(enabled);
		dirInput.setEnabled(enabled);
		browseInput.setEnabled(enabled);
		if (!enabled) {
			layout.setInputDir((File)null);
		}
	}

	/**
	 * Initialize preference panel for layout.
	 * 
	 * @param layout
	 *            the layout
	 */
	protected void init(PipeLayout layout) {
		params = layout.getRunParameters();
		dirInput.setText((params.getInputURI() != null) ? params.getInputURI().toString() : "");
		dirOutput.setText((params.getOutputURI() != null) ? params.getOutputURI().toString() : "");
		heapSize.setValue(params.getMaxHeap());
		simProcs.setValue(params.getMaxProcs());
		useGridEngine.setSelected(params.isUseGridEngine());
		/*
		NamingConvention convention=params.getNamingConvention();
		if(convention!=null){
			namingConvention.setSelectedIndex(params.getNamingConvention().ordinal());
		}
		*/
		enableRelative((params.getInputDirectory() != null));
	}

	/**
	 * Update.
	 * 
	 * @return true, if successful
	 */
	protected boolean update() {
		try {
			layout.setMaxHeap(((Integer) heapSize.getValue()).intValue());
			layout.setMaxProcs(((Integer) simProcs.getValue()).intValue());
			
		} catch (NumberFormatException e) {
			return false;
		}
		//layout.setNamingConvention(NamingConvention.values()[namingConvention.getSelectedIndex()]);
		layout.setUseGridEngine(useGridEngine.isSelected());
		File f;
		URI uri;
		if (useRelative.isSelected()) {
			
			try {
				f = new File(uri=new URI(dirInput.getText()));
			} catch (URISyntaxException e) {
				f = new File(dirInput.getText());
				uri=f.toURI();
			}
			if (f.exists() && f.isDirectory()) {
				layout.setInputDir(uri);
			} else {
				return false;
			}
		} else {
			layout.setInputDir((File)null);
		}
		try {
			f = new File(uri=new URI(dirOutput.getText()));
		} catch (URISyntaxException e) {
			f = new File(dirOutput.getText());
			uri=f.toURI();
		}
		if (!f.exists()) {
			f.mkdir();
		}
		if (f.exists() && f.isDirectory()) {
			layout.setOutputDir(uri);
		} else {
			return false;
		}
		return true;
	}
}
