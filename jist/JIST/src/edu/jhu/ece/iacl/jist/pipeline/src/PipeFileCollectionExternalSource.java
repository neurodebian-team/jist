package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;

public class PipeFileCollectionExternalSource extends PipeExternalSource{
	public PipeFileCollectionExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamFileCollection("Default Files",new FileExtensionFilter(new String[]{})));
		group.setLabel("External Files");
		group.setName("extfiles");
		group.setCategory("Externalize.File");
		return group;
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Files");
		group.add(valParam = new ParamFileCollection("Files",new FileExtensionFilter(new String[]{})));
		return group;
	}
}
