/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import java.io.File;

import javax.swing.ProgressMonitor;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamVolumeURIInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamVolumeOutputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Volume Parameter Factory.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeFactory extends ParamFileFactory {
	
	/** The param. */
	private ParamVolume param;
	
	/** The resource. */
	private ImageData resource = null;

	/**
	 * Instantiates a new param volume factory.
	 */
	public ParamVolumeFactory() {
	}

	/**
	 * Instantiates a new param volume factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamVolumeFactory(ParamVolume param) {
		super();
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileFactory#createMipavParameter(gov.nih.mipav.view.dialogs.AlgorithmParameters)
	 *//*
	public void createMipavParameter(AlgorithmParameters scriptParams) throws ParserException {
		ParamVolume vol = getParameter();
		scriptParams.storeImage(vol.getModelImageCache(), encodeName(vol.getName()));
	}*/

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof ParamFactory) {
			return this.equals(((ParamFactory) obj).getParameter());
		} else if (obj instanceof ParamVolume) {
			if (this.getParameter().getValue() == null) {
				return true;
			}
			return ((this.getParameter().getValue() != null) && this.getParameter().getValue().equals(
					((ParamVolume) obj).getValue()));
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamVolumeURIInputView(param);
		}
		return inputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamVolumeOutputView(param);
		}
		return outputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileFactory#getParameter()
	 */
	public ParamVolume getParameter() {
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileFactory#importMipavParameter(gov.nih.mipav.model.scripting.parameters.ParameterTable)
	 */
	/*public void importMipavParameter(ParameterTable paramTable) throws ParameterException {
		getParameter().setValue(paramTable.getImage(encodeName(getParameter().getName())));
	}*/

	/**
	 * Load Model Image from file specified in foreign parameter.
	 * 
	 * @param foreign
	 *            foreign parameters
	 * @param monitor
	 *            the monitor
	 * @return resources loaded correctly
	 */
	public boolean loadResources(ParamModel foreign, ProgressMonitor monitor) {
		if (!param.isHidden() && (foreign instanceof ParamVolume)) {
			ParamInputView view = getInputView();
			if (view instanceof Refreshable) {
				Refresher.getInstance().remove(((Refreshable) view));
			}
			param.setValue(((ParamVolume) foreign).getValue());
			view.update();
			if (view instanceof Refreshable) {
				Refresher.getInstance().add(((Refreshable) view));
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save Model Image to specified directory.
	 * 
	 * @param dir
	 *            save directory
	 * @return resources saved correctly
	 */
	public boolean saveResources(File dir, boolean overRidesubDirectory) {
		ImageData resource = param.getImageData();
		if ((resource != null) && (param.getReaderWriter() != null)) {
			File f = null;
			
			if ((f = param.getReaderWriter().write(resource, dir)) != null) {
				param.setValue(f);
				return true;
			} else {
				System.out.println(getClass().getCanonicalName()+"\t"+param);
				System.out.println(getClass().getCanonicalName()+"\t"+param.getReaderWriter().getClass());
				
				System.out.println(getClass().getCanonicalName()+"\t"+"ParamVolumeFactory: Resource Save Failed.");
				return false;
			}
		} else {
			return true;
		}
	}
}
