/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.factory.ParamHiddenFactory;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Command line switch parameter.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamSwitch extends ParamHidden<String>  implements JISTInternalParam{
	
	/** The switch string. */
	protected String switchString;
	
	/** The switch spaced. */
	protected boolean switchSpaced;

	 @Override
	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);
		 Element em;				
		 em = document.createElement("switchString");		
		 em.appendChild(document.createTextNode(switchString+""));
		 parent.appendChild(em);
		 em = document.createElement("switchSpaced");		
		 em.appendChild(document.createTextNode(switchSpaced+""));
		 parent.appendChild(em);
		 return true;
	 }
	 
		public void xmlDecodeParam(Document document, Element parent) {
			super.xmlDecodeParam(document, parent);
			switchString= JistXMLUtil.xmlReadTag(parent, "switchString");
			switchSpaced= Boolean.valueOf(JistXMLUtil.xmlReadTag(parent, "switchSpaced"));
		}
		
	/**
	 * Default constructor.
	 */
	public ParamSwitch() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 * @param switchString
	 *            switch string
	 */
	public ParamSwitch(String name, String switchString) {
		this(name, switchString, true);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 * @param switchString
	 *            switch string
	 * @param switchSpaced
	 *            flag to indicate whether to put a space after switch
	 */
	public ParamSwitch(String name, String switchString, boolean switchSpaced) {
		super();
		this.setName(name);
		this.switchSpaced = switchSpaced;
		this.switchString = switchString;
	}

	/**
	 * Clone object.
	 * 
	 * @return the param switch
	 */
	public ParamSwitch clone() {
		ParamSwitch param = new ParamSwitch(getName(), this.switchString, this.switchSpaced);
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#compareTo(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel)
	 */
	public int compareTo(ParamModel obj) {
		if (obj instanceof ParamSwitch) {
			if (((ParamSwitch) obj).switchString.equals(this.switchString)) {
				return 0;
			} else {
				return 1;
			}
		}
		return 1;
	}

	/**
	 * Get switch string.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return switchString;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		connectible = true;
		factory = new ParamHiddenFactory(this);
	}

	/**
	 * Returns true if there is a space after the switch.
	 * 
	 * @return true if spaced
	 */
	public boolean isSwitchSpaced() {
		return switchSpaced;
	}

	/**
	 * Indicates whether there should be a space after the switch.
	 * 
	 * @param switchSpaced
	 *            true if spaced
	 */
	public void setSwitchSpaced(boolean switchSpaced) {
		this.switchSpaced = switchSpaced;
	}

	/**
	 * Set switch string.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(String value) throws InvalidParameterValueException {
		this.switchString = value;
	}

	/**
	 * Get switch description.
	 * 
	 * @return the string
	 */
	public String toString() {
		return switchString + ((isSwitchSpaced()) ? " " : "");
	}

	/**
	 * Unimplemented.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
	}
	
	@Override
	public String getHumanReadableDataType() {
		return "deprecated: DO NOT USE. REPORT THIS MESSAGE IF SEEN";
	}
public String getXMLValue() {throw new RuntimeException("INTERNAL: Not Serializable"); };
	
	@Override
	public void setXMLValue(String arg) {
		throw new RuntimeException("INTERNAL: Not Serializable"); 
		
	};
	
	@Override
	public String probeDefaultValue() {	
		return null;
	}
}
