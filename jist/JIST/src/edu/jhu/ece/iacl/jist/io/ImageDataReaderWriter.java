/**
 * Java Image Science Toolkit (JIST) // Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;

import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;

// TODO: Auto-generated Javadoc
/**
 * The Class CubicVolumeReaderWriter.
 */
public class ImageDataReaderWriter extends FileReaderWriter<ImageData> implements JistPreferences.PreferenceListener{
	protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	
	/** The Constant vtkReaderWriter. */
	protected static final VolumeVtkReaderWriter vtkReaderWriter=new VolumeVtkReaderWriter();

	/** The Constant readerWriter. */
	protected static final ImageDataReaderWriter readerWriter=new ImageDataReaderWriter(); 

	protected Vector<String> compressionOptions;

	private String prefCompression;
	/**
	 * Gets the single instance of CubicVolumeReaderWriter.
	 * 
	 * @return single instance of CubicVolumeReaderWriter
	 */
	public static ImageDataReaderWriter getInstance(){
		return readerWriter;
	}

	/** The rw. */
	private static ModelImageReaderWriter rw=ModelImageReaderWriter.getInstance();

	/**
	 * Instantiates a new cubic volume reader writer.
	 */
	public ImageDataReaderWriter(){
		super();
		this.extensionFilter=new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions);
		this.extensionFilter.getExtensions().addAll(vtkReaderWriter.getExtensionFilter().getExtensions());
		//Get preferred extension 
		// OVERRIDDEN BELOW
		this.extensionFilter.setPreferredExtension(JistPreferences.getPreferences().getPreferredExtension());
		compressionOptions = new Vector<String>(); 
		compressionOptions.add("none");
		compressionOptions.add("gzip");
		compressionOptions.add("bzip2");
		prefCompression = JistPreferences.getPreferences().getPreferredCompression();
		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	@Override
	protected ImageData readObject(File f) {
		if(vtkReaderWriter.accept(f)){
			return vtkReaderWriter.readObject(f);
		} else {
			ModelImage img=rw.readObject(f);
			if(img!=null){
				// This is REQUIRED so that the image
				// may be freed upon the finalize method.
				JistLogger.logOutput(JistLogger.FINE, "SUCCESS - Read file : "+f);
				return new ImageDataMipavWrapper(img); 
			} else {
				JistLogger.logError(JistLogger.FINE, "ERROR - Failed to read file : "+f);
				return null;
			}
		}
	}

	public File getPreferredImageDataFileName(ImageData img, File dir) {
		String name = img.getName();		
		String compression = getPreferredCompression();
		String []noExt = new String[]{""}; // Do not update the extension for "special" types
		File f =null;
		if(compression.equalsIgnoreCase("none")) {
			f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
					/*extensionFilter.getPreferredExtension()*/
					extensionFilter.getPreferredExtension());			
		} else if(compression.equalsIgnoreCase("gzip")) {
			if(Arrays.asList(noExt).contains(extensionFilter.getPreferredExtension())) {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension());
			} else {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension()+".gz");
			}
		} else if(compression.equalsIgnoreCase("bzip2")) {
			if(Arrays.asList(noExt).contains(extensionFilter.getPreferredExtension())) {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension());
			} else {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension()+".bz2");
			}
		}		
		else {
			JistLogger.logError(JistLogger.WARNING, "Warning, unknown compression type: "+compression+". Defaulting to none.");
			f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
					/*extensionFilter.getPreferredExtension()*/
					extensionFilter.getPreferredExtension());
		}		
		return f;
	}
	
	// We need to have the ability to get the filename that is written versus
	// the specified filename. This is due to MIPAV's xml format. Compression
	// is requested by adding a .gz or .bz2 extension, but the xml file is NOT
	// compressed. 
	public File getPreferredImageDataFileNameActuallyWritten(ImageData img, File dir) {
		String name = img.getName();		
		String compression = getPreferredCompression();
		String []noExt = new String[]{"xml"}; // Do not update the extension for "special" types
		File f =null;
		if(compression.equalsIgnoreCase("none")) {
			f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
					/*extensionFilter.getPreferredExtension()*/
					extensionFilter.getPreferredExtension());			
		} else if(compression.equalsIgnoreCase("gzip")) {
			if(Arrays.asList(noExt).contains(extensionFilter.getPreferredExtension())) {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension());
			} else {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension()+".gz");
			}
		} else if(compression.equalsIgnoreCase("bzip2")) {
			if(Arrays.asList(noExt).contains(extensionFilter.getPreferredExtension())) {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension());
			} else {
				f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
						/*extensionFilter.getPreferredExtension()*/
						extensionFilter.getPreferredExtension()+".bz2");
			}
		}		
		else {
			JistLogger.logError(JistLogger.WARNING, "Warning, unknown compression type: "+compression+". Defaulting to none.");
			f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+
					/*extensionFilter.getPreferredExtension()*/
					extensionFilter.getPreferredExtension());
		}		
		return f;
	}
	
	public static void deleteImageFile(File f) {
		File f2;
		if(f.exists()) f.delete();
		// XML
		if(f.toString().endsWith(".xml")) {
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".raw");
			if(f2.exists()) f2.delete();
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".raw.gz");
			if(f2.exists()) f2.delete();
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".raw.bz2");
			if(f2.exists()) f2.delete();
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".raw.zip");
			if(f2.exists()) f2.delete();
		}
		// hdr / img 
		if(f.toString().endsWith(".hdr")) {
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".img");
			if(f2.exists()) f2.delete();
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".img.gz");
			if(f2.exists()) f2.delete();
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".img.bz2");
			if(f2.exists()) f2.delete();
			f2 = new File((f.toString().substring(0, (int) (f.toString().length()-4)))+".img.zip");
			if(f2.exists()) f2.delete();
		}
		
	}
	
	public static void updateFileSettings(ModelImage img) {

		String compression = JistPreferences.getPreferences().getPreferredCompression();
		FileInfoBase []fib = img.getFileInfo();
		if(compression.equalsIgnoreCase("none")) {
			for(int i=0;i<fib.length;i++)
				fib[i].setCompressionType(FileInfoBase.COMPRESSION_NONE);
		} else if(compression.equalsIgnoreCase("bzip2")) {
			for(int i=0;i<fib.length;i++)
				fib[i].setCompressionType(FileInfoBase.COMPRESSION_BZIP2);
		} else if(compression.equalsIgnoreCase("gzip")) {
			for(int i=0;i<fib.length;i++)
				fib[i].setCompressionType(FileInfoBase.COMPRESSION_GZIP);

		} else {
			JistLogger.logError(JistLogger.WARNING, "Unknonw Compression Type:"+compression);
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(ImageData img,File dir){
		File f = getPreferredImageDataFileName(img, dir);
		if((f=writeObject(img,f))!=null){
			JistLogger.logOutput(JistLogger.FINE, "SUCCESS - Wrote file : "+f);
			return f;
		} else {
			JistLogger.logError(JistLogger.FINE, "ERROR - Failed to write file : "+f);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(ImageData obj, File f) {
		if(vtkReaderWriter.accept(f)){
			System.out.println("sadfsafdsadfsadfsadfsadfsaf: "+"VTK");
			return vtkReaderWriter.writeObject(obj,f);
		} else {
			if (obj instanceof ImageDataMipav) {	
				ModelImage img = ((ImageDataMipav)obj).getModelImageDirect();
				updateFileSettings(img);
				boolean success = (f=rw.writeObject(img,f))!=null;
				if(success)
					JistLogger.logOutput(JistLogger.FINE, "SUCCESS - Wrote file : "+f);
				else 
					JistLogger.logError(JistLogger.FINE, "ERROR - Failed to write file : "+f);
				return f;
			} else {				
				ModelImage img=ImageDataMipav.createModelImage(obj);
				updateFileSettings(img);
				boolean success = (f=rw.writeObject(img,f))!=null;
				img.disposeLocal();
				if(success)
					JistLogger.logOutput(JistLogger.FINE, "SUCCESS - Wrote file : "+f);
				else 
					JistLogger.logError(JistLogger.FINE, "ERROR - Failed to write file : "+f);

				return f;
				//				return rw.writeObject(ImageDataMipav.createModelImage(obj),f);
			}


		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.JistPreferences.PreferenceListener#historyChange(edu.jhu.ece.iacl.jist.pipeline.JistPreferences)
	 */
	public void historyChange(JistPreferences prefs) {
		// TODO Auto-generated method stub

	}

	public Vector<String> getCompressionOptions() {
		// TODO Auto-generated method stub
		return this.compressionOptions;
	}
	
	public String getPreferredCompression() {
		return prefCompression;
	}
	
	public void setPreferredCompression(String comp) throws InvalidParameterException {
		for(String s : compressionOptions) {
			if(s.equalsIgnoreCase(comp)) {
				prefCompression = s;
				return;
			}
		}
		throw new InvalidParameterException(null,"Invalid compression  option:"+comp);
	}
}
