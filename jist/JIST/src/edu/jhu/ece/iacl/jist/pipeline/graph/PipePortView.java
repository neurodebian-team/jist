/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Point2D;

import org.jgraph.graph.CellViewRenderer;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.PortRenderer;
import org.jgraph.graph.PortView;

import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;

/**
 * Cell view for port.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public abstract class PipePortView extends PortView {
	
	/**
	 * The Class ActivityRenderer.
	 */
	public abstract class ActivityRenderer extends PortRenderer {
		
		/**
		 * Instantiates a new activity renderer.
		 */
		public ActivityRenderer() {
			super();
			setForeground(Color.gray);
		}

		/**
		 * Creates the shape.
		 * 
		 * @return the shape
		 */
		public abstract Shape createShape();

		/**
		 * Checks if is attempting to connect.
		 * 
		 * @return true, if is attempting to connect
		 */
		public boolean isAttemptingToConnect() {
			PipePort src = graph.getPortToCheck();
			return (src != null);
		}

		/**
		 * Checks if is compatible.
		 * 
		 * @return true, if is compatible
		 */
		protected abstract boolean isCompatible();

		/**
		 * Checks if is connected.
		 * 
		 * @return true, if is connected
		 */
		public boolean isConnected() {
			PipePort dest = ((PipeModulePort) view.getCell()).getPort();
			return (((dest != null) && dest.isConnected()));
		}

		/* (non-Javadoc)
		 * @see java.awt.Component#isValid()
		 */
		public boolean isValid() {
			if(view==null)return false;
			PipePort dest = ((PipeModulePort) view.getCell()).getPort();
			if ((dest != null) && dest.isInputPort()) {
				try {
					dest.validate();
					return true;
				} catch (InvalidParameterException e) {
				}
			}
			return false;
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.PortRenderer#paint(java.awt.Graphics)
		 */
		public void paint(Graphics g) {
			Dimension d = getSize();
			boolean compatible = isCompatible();
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			((Graphics2D) g).setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			Shape poly = createShape();
			g.setColor(graphBackground);
			if (compatible && preview) {
				g.setPaintMode();
				((Graphics2D) g).fill(poly);
			}
			if (xorEnabled) {
				g.setXORMode(graphBackground);
			}
			if (preview) {
				g.fill3DRect(0, 0, d.width, d.height, true);
			}
			boolean offset = (GraphConstants.getOffset(view.getAllAttributes()) != null);
			boolean valid = isValid();
			if (valid && (!isAttemptingToConnect() || (isAttemptingToConnect() && !compatible))) {
				g.setColor(validColor);
			} else {
				g.setColor(compatibleColor);
			}
			if (isConnected() && ((isAttemptingToConnect() && !compatible) || !isAttemptingToConnect())) {
				g.setColor(connectedColor);
			}
			if (!offset || compatible || isConnected() || valid) {
				((Graphics2D) g).fill(poly);
			} else {
				g.setColor(getForeground());
				((Graphics2D) g).draw(poly);
			}
		}
	}

	/** The Constant portSize. */
	public static final int portSize = 12;
	
	/** The compatible color. */
	protected static Color compatibleColor = new Color(0, 128, 0);
	
	/** The connected color. */
	protected static Color connectedColor = Color.black;
	
	/** The valid color. */
	protected static Color validColor = Color.gray;
	
	/** The Constant labelAngle. */
	private static final double labelAngle = Math.PI / 4;
	
	/** The label size. */
	protected Dimension labelSize = new Dimension(500, 500);
	
	/** The graph. */
	protected PipeJGraph graph;

	/**
	 * Default constructor.
	 */
	public PipePortView() {
		super();
		setPortSize(portSize);
	}

	/**
	 * Constructor.
	 * 
	 * @param cell
	 *            module cell that owns this port
	 */
	public PipePortView(Object cell) {
		super(cell);
		setPortSize(portSize);
	}

	/**
	 * Draw port label.
	 * 
	 * @param g
	 *            the g
	 * @return the dimension
	 */
	protected Dimension drawPortLabel(Graphics2D g) {
		DefaultPort modcell = (DefaultPort) getCell();
		PipePort pport = (PipePort) modcell.getUserObject();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		Font f = GraphConstants.getFont(getAllAttributes());
		g.setFont(f);
		FontMetrics metrics = g.getFontMetrics();
		int sw = metrics.stringWidth(pport.getLabel());
		int sh = metrics.getHeight();
		int voff = metrics.getDescent();
		Point2D offset = GraphConstants.getLabelPosition(getAllAttributes());
		double l = Math.sqrt(Math.pow(sw + 5, 2) + Math.pow(sh + 5, 2));
		Dimension d = new Dimension((int) Math.ceil(offset.getX() + l * Math.cos(labelAngle)), (int) Math.ceil(offset
				.getY()
				+ l * Math.sin(labelAngle)));
		if (pport.isInputPort()) {
			g.translate(offset.getX(), offset.getY() + labelSize.getWidth());
			g.rotate(-labelAngle);
		} else {
			g.translate(offset.getX(), offset.getY());
			g.rotate(labelAngle);
		}
		g.setColor(GraphConstants.getBackground(getAllAttributes()));
		g.fillRoundRect(-2, voff - sh - 2, sw + 4, sh + 4, 3, 3);
		g.setColor(GraphConstants.getBorderColor(getAllAttributes()));
		g.drawRoundRect(-2, voff - sh - 2, sw + 4, sh + 4, 3, 3);
		g.setColor(GraphConstants.getForeground(getAllAttributes()));
		g.setFont(f);
		g.drawString(pport.getLabel(), 0, 0);
		if (pport.isInputPort()) {
			g.rotate(labelAngle);
			g.translate(-offset.getX(), -offset.getY());
		} else {
			g.rotate(-labelAngle);
			g.translate(-offset.getX(), -offset.getY());
		}
		return d;
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.PortView#getRenderer()
	 */
	public abstract CellViewRenderer getRenderer();

	/**
	 * Set graph that contains this port.
	 * 
	 * @param graph
	 *            the graph
	 */
	public void setGraph(PipeJGraph graph) {
		this.graph = graph;
	}
}
