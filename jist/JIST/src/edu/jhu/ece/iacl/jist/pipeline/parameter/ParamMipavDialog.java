/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.awt.Frame;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.MipavViewUserInterface;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamMipavDialogFactory;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.view.MipavUtil;
import gov.nih.mipav.view.ViewJFrameImage;
import gov.nih.mipav.view.dialogs.JDialogScriptableBase;

/**
 * Parameter for storing reference to mipav dialog.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamMipavDialog extends ParamModel<Class> implements JISTInternalParam {
	// dialog class
	/** The dialog class. */
	protected Class dialogClass;
	
	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);		 
		 Element em;				
		 em = document.createElement("dialogClass");			 		 
		 em.appendChild(document.createTextNode(dialogClass.toString()+""));
		 parent.appendChild(em);				 		 
			
		 return true;
	 }
	 
	 public void xmlDecodeParam(Document document, Element el) {
			super.xmlDecodeParam(document, el);
			try {
				dialogClass = Class.forName((JistXMLUtil.xmlReadTag(el, "dialogClass")));
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				dialogClass=null;
			}
					
			
		}
	 
	/** The dialog. */
	protected transient JDialogScriptableBase dialog = null;

	/**
	 * Constructor.
	 * 
	 * @param c
	 *            mipav dialog class
	 */
	public ParamMipavDialog(Class c) {
		setValue(c);
		init();
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamMipavDialog(String name) {
		setName(name);
		init();
	}

	public ParamMipavDialog() {this("invalid");};
	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter
	 * @param c
	 *            mipav dialog class
	 */
	public ParamMipavDialog(String name, Class c) {
		this(c);
		setName(name);
	}

	/**
	 * Clone object.
	 * 
	 * @return the param mipav dialog
	 */
	public ParamMipavDialog clone() {
		ParamMipavDialog param = new ParamMipavDialog(dialogClass);
		param.label=this.label;
		param.setValue(this.getValue());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Create dialog from class.
	 * 
	 * @return the dialog
	 */
	public JDialogScriptableBase getDialog() {
		if ((dialog == null) && (dialogClass != null)) {
			try {
				ViewJFrameImage imgFrame = MipavViewUserInterface.getReference().getActiveImageFrame();
				if (imgFrame != null) {
					/*
					 * Constructor[] constructors=dialogClass.getConstructors();
					 * for(Constructor c:constructors){
					 * System.out.println(getClass().getCanonicalName()+"\t"+c.toGenericString()); }
					 */
					Constructor constructor = dialogClass.getConstructor(Frame.class, ModelImage.class);
					dialog = (JDialogScriptableBase) constructor.newInstance(MipavViewUserInterface.getReference()
							.getMainFrame(), imgFrame.getActiveImage());
					if (!dialog.isVisible()) {
						dialog = null;
					}
				} else {
					MipavUtil.displayError("You must open an image before running this plug-in.");
				}
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		return dialog;
	}

	/**
	 * Get mipav dialog class.
	 * 
	 * @return the value
	 */
	public Class getValue() {
		return dialogClass;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		connectible = false;
		this.factory = new ParamMipavDialogFactory(this);
	}

	/**
	 * Set mipav dialog class.
	 * 
	 * @param c
	 *            the c
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(Class c) throws InvalidParameterValueException {
		if (JDialogScriptableBase.class.equals(c.getSuperclass())) {
			if (c != dialogClass) {
				dialog = null;
			}
			this.dialogClass = c;
		} else {
			throw new InvalidParameterValueException(this, "Super Class is not JDialog Scriptable Base");
		}
	}

	/**
	 * get description of parameter.
	 * 
	 * @return the string
	 */
	public String toString() {
		return (dialogClass == null) ? "null" : dialogClass.getSimpleName();
	}

	/**
	 * Validate class parameter.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if ((dialogClass == null) || !JDialogScriptableBase.class.equals(dialogClass.getSuperclass())) {
			throw new InvalidParameterException(this);
		}
	}
	
	@Override
	public String getHumanReadableDataType() {
		return "JIST INTERNAL CLASS: Report if seen";
	}
	public String getXMLValue() {throw new RuntimeException("INTERNAL: Not Serializable"); };
	
	@Override
	public void setXMLValue(String arg) {
		throw new RuntimeException("INTERNAL: Not Serializable"); 
		
	};
	
	@Override
	public String probeDefaultValue() {
		return null;
	}
}
