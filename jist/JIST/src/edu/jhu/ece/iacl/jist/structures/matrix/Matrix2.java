package edu.jhu.ece.iacl.jist.structures.matrix;

// TODO: Auto-generated Javadoc
/**
 * Matrix 2D implementation.
 * 
 * @author Blake Lucas
 */
public class Matrix2 extends Jama.Matrix implements MatrixX {	
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public Matrix2(int arg0, int arg1) {
		super(arg0, arg1);
	}
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 */
	public Matrix2(int arg0,int arg1,double arg2){
		super(arg0,arg1,arg2);
	}
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public Matrix2(double[] arg0,int arg1){
		super(arg0,arg1);
	}
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param arg0 the arg0
	 */
	public Matrix2(double[][] arg0){
		super(arg0);
	}
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 */
	public Matrix2(double[][] arg0,int arg1,int arg2){
		super(arg0,arg1,arg2);
	}
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param m the m
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 * @param arg3 the arg3
	 */
	public Matrix2(double[][][] m,int arg1,int arg2,int arg3){
		super(arg1,arg2);
		for(int i=0;i<arg1;i++){
			for(int j=0;j<arg2;j++){
				set(i,j,m[i][j][arg3]);
			}
		}
	}	
	
	/**
	 * Instantiates a new matrix2.
	 * 
	 * @param m the m
	 * @param arg3 the arg3
	 */
	public Matrix2(double[][][] m,int arg3){
		super(m.length,(m.length>0)?m[0].length:0);
		for(int i=0;i<getRowDimension();i++){
			for(int j=0;j<getColumnDimension();j++){
				set(i,j,m[i][j][arg3]);
			}
		}
	}
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
}
