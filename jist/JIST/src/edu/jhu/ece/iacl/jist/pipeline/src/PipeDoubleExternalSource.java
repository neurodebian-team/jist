package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;

public class PipeDoubleExternalSource extends PipeExternalSource{
	
	public PipeDoubleExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamDouble("Default"));
		group.setLabel("External Float");
		group.setName("extfloat");
		group.setCategory("Externalize.Number.Float");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float");
		group.add(valParam = new ParamDouble("Float"));
		return group;
	}
}
