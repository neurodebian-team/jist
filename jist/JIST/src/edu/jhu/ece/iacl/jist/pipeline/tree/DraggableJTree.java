/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.tree;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.peer.DragSourceContextPeer;
import java.awt.event.InputEvent;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmNode;

/**
 * Tree that allows items to be dragged.
 * 
 * @author Blake Lucas
 */
public class DraggableJTree extends JTree implements TreeSelectionListener, DragGestureListener, DragSourceListener,
		DropTargetListener {
	
	/** The Selected tree path. */
	protected TreePath SelectedTreePath = null;
	
	/** The Selected node. */
	protected DefaultMutableTreeNode SelectedNode = null;
	
	/** The drag source. */
	private DragSource dragSource = null;
	
	/** The tree model. */
	private DefaultTreeModel treeModel;

	/**
	 * Default constructor.
	 * 
	 * @param treeModel
	 *            tree model that will contain draggable objects
	 * @param root
	 *            tree model root
	 */
	public DraggableJTree(DefaultTreeModel treeModel, DefaultMutableTreeNode root) {
		super(treeModel);
		this.treeModel = treeModel;
		addTreeSelectionListener(this);
		dragSource = new DragSource() {
			protected DragSourceContext createDragSourceContext(DragSourceContextPeer dscp, DragGestureEvent dgl,
					Cursor dragCursor, Image dragImage, Point imageOffset, Transferable t, DragSourceListener dsl) {
				return new DragSourceContext(dscp, dgl, dragCursor, dragImage, imageOffset, t, dsl) {
					protected void updateCurrentCursor(int dropOp, int targetAct, int status) {
						if (((targetAct == DragSourceContext.OVER) && (status == DragSourceContext.OVER))
								|| ((targetAct == DragSourceContext.ENTER) && (status == DragSourceContext.ENTER))) {
							setCursor(DragSource.DefaultMoveDrop);
						} else {
							setCursor(DragSource.DefaultMoveNoDrop);
						}
					}
				};
			}
		};
		DragGestureRecognizer dgr = dragSource.createDefaultDragGestureRecognizer(this, // DragSource
				DnDConstants.ACTION_MOVE, // specifies valid actions
				this // DragGestureListener
				);
		/*
		 * Eliminates right mouse clicks as valid actions - useful especially if
		 * you implement a JPopupMenu for the JTree
		 */
		dgr.setSourceActions(dgr.getSourceActions() & ~InputEvent.BUTTON3_MASK);
		/*
		 * First argument: Component to associate the target with Second
		 * argument: DropTargetListener
		 */
		setDropTarget(new DropTarget(this, this));
	}

	/**
	 * DragSourceListener interface method.
	 * 
	 * @param dsde
	 *            the dsde
	 */
	public void dragDropEnd(DragSourceDropEvent dsde) {
	}

	// /////////////////////// Interface stuff ////////////////////
	/**
	 * DragSourceListener interface method.
	 * 
	 * @param dsde
	 *            the dsde
	 */
	public void dragEnter(DragSourceDragEvent dsde) {
	}

	/**
	 * Accepts drag action on tree if destination is a package.
	 * 
	 * @param dtde
	 *            the dtde
	 */
	public void dragEnter(DropTargetDragEvent dtde) {
		TreeNode node = getNodeForEvent(dtde);
		if (node instanceof PackageNode) {
			dtde.acceptDrag(dtde.getDropAction());
		} else {
			dtde.rejectDrag();
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.dnd.DragSourceListener#dragExit(java.awt.dnd.DragSourceEvent)
	 */
	public void dragExit(DragSourceEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * Unimplemented.
	 * 
	 * @param dte
	 *            the dte
	 */
	public void dragExit(DropTargetEvent dte) {
	}

	/**
	 * DragGestureListener interface method.
	 * 
	 * @param e
	 *            the e
	 */
	public void dragGestureRecognized(DragGestureEvent e) {
		// Get the selected node
		DefaultMutableTreeNode dragNode = getSelectedNode();
		if ((dragNode != null) && ((dragNode instanceof DraggableNode) || (dragNode instanceof PackageNode))) {
			// Get the Transferable Object
			Transferable transferable = (Transferable) dragNode;
			/* ********************** CHANGED ********************** */
			// Select the appropriate cursor;
			if (e.getDragAction() == DnDConstants.ACTION_MOVE) {
				dragSource.startDrag(e, DragSource.DefaultCopyNoDrop, transferable, this);
			}
			// In fact the cursor is set to NoDrop because once an action is
			// rejected
			// by a dropTarget, the dragSourceListener are no more invoked.
			// Setting the cursor to no drop by default is so more logical,
			// because
			// when the drop is accepted by a component, then the cursor is
			// changed by the
			// dropActionChanged of the default DragSource.
			/* ****************** END OF CHANGE ******************** */
		}
	}

	/**
	 * DragSourceListener interface method.
	 * 
	 * @param dsde
	 *            the dsde
	 */
	public void dragOver(DragSourceDragEvent dsde) {
	}

	/**
	 * Accepts drag action on tree if destination is package.
	 * 
	 * @param dtde
	 *            the dtde
	 */
	public void dragOver(DropTargetDragEvent dtde) {
		TreeNode node = getNodeForEvent(dtde);
		if (node instanceof PackageNode) {
			dtde.acceptDrag(dtde.getDropAction());
		} else {
			dtde.rejectDrag();
		}
	}

	/**
	 * Drop object on tree.
	 * 
	 * @param dtde
	 *            the dtde
	 */
	public void drop(DropTargetDropEvent dtde) {
		Point pt = dtde.getLocation();
		DropTargetContext dtc = dtde.getDropTargetContext();
		TreePath parentpath = getClosestPathForLocation(pt.x, pt.y);
		DefaultMutableTreeNode parent = (DefaultMutableTreeNode) parentpath.getLastPathComponent();
		if (!(parent instanceof PackageNode)) {
			dtde.rejectDrop();
			return;
		}
		Transferable tr = dtde.getTransferable();
		Object obj = null;
		try {
			obj = tr.getTransferData(DraggableNode.MODULE_FLAVOR);
		} catch (UnsupportedFlavorException e1) {
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			obj = tr.getTransferData(PackageNode.PACKAGE_FLAVOR);
		} catch (UnsupportedFlavorException e1) {
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if (obj instanceof AlgorithmNode) {
			AlgorithmNode node = (AlgorithmNode) obj;
			DefaultMutableTreeNode oldNode = (DefaultMutableTreeNode) getSelectionPath().getLastPathComponent();
			dtde.acceptDrop(dtde.getDropAction());
			if (parent instanceof PackageNode) {
				if (((PackageNode) parent).moveIntoDirectory(node)) {
					treeModel.removeNodeFromParent(oldNode);
					treeModel.insertNodeInto(node, parent, 0);
				}
			}
			dtde.getDropTargetContext().dropComplete(true);
		}
		if (obj instanceof PackageNode) {
			PackageNode node = (PackageNode) obj;
			DefaultMutableTreeNode oldNode = (DefaultMutableTreeNode) getSelectionPath().getLastPathComponent();
			dtde.acceptDrop(dtde.getDropAction());
			if (parent instanceof PackageNode) {
				if (!oldNode.isNodeDescendant(parent) && !oldNode.equals(parent)) {
					int n = JOptionPane.showOptionDialog(this, "Are you sure you want to move the package "
							+ node.getUserObject() + " into the directory " + parent.getUserObject() + "?", null,
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
					if (n == 0) {
						if (node.moveIntoDirectory((PackageNode) parent)) {
							treeModel.removeNodeFromParent(oldNode);
							treeModel.insertNodeInto(node, parent, 0);
						}
					}
				}
			}
			dtde.getDropTargetContext().dropComplete(true);
		}
	}

	/**
	 * DragSourceListener interface method.
	 * 
	 * @param dsde
	 *            the dsde
	 */
	public void dropActionChanged(DragSourceDragEvent dsde) {
	}

	/**
	 * Unimplemeted.
	 * 
	 * @param dtde
	 *            the dtde
	 */
	public void dropActionChanged(DropTargetDragEvent dtde) {
	}

	/**
	 * Get tree node for cursor location.
	 * 
	 * @param dtde
	 *            drag event
	 * @return tree node
	 */
	private TreeNode getNodeForEvent(DropTargetDragEvent dtde) {
		Point p = dtde.getLocation();
		DropTargetContext dtc = dtde.getDropTargetContext();
		JTree tree = (JTree) dtc.getComponent();
		TreePath path = tree.getClosestPathForLocation(p.x, p.y);
		return (TreeNode) path.getLastPathComponent();
	}

	/**
	 * Returns The selected node.
	 * 
	 * @return the selected node
	 */
	public DefaultMutableTreeNode getSelectedNode() {
		return SelectedNode;
	}

	/**
	 * TreeSelectionListener - sets selected node.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void valueChanged(TreeSelectionEvent evt) {
		SelectedTreePath = evt.getNewLeadSelectionPath();
		if (SelectedTreePath == null) {
			SelectedNode = null;
			return;
		}
		SelectedNode = (DefaultMutableTreeNode) SelectedTreePath.getLastPathComponent();
	}
}
