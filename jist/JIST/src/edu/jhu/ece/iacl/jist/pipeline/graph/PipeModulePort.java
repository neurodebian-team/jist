/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Point2D;

import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeChildPort;
import edu.jhu.ece.iacl.jist.pipeline.PipeParentPort;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Graph module port.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeModulePort extends DefaultPort {
	
	/**
	 * Instantiates a new pipe module port.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param port
	 *            the port
	 */
	public PipeModulePort(double x, double y, PipePort port) {
		super(port);
		port.setGraphPort(this);
		applyPortAttributes(x, y, port);
	}

	/**
	 * Create port attributes.
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @param port
	 *            pipe port
	 */
	protected void applyPortAttributes(double x, double y, PipePort port) {
		AttributeMap map = this.getAttributes();
		GraphConstants.setBackground(map, Color.white);
		GraphConstants.setForeground(map, Color.black);
		GraphConstants.setBorderColor(map, Color.gray);
		GraphConstants.setRouting(map, new AvoidanceRouting());
		
		GraphConstants.setFont(map, new Font("Arial", Font.PLAIN, JistPreferences.getPreferences().getMdFontSize()));
		if (port instanceof PipeChildPort) {
			GraphConstants.setLabelPosition(map, new Point2D.Double(5, -10));
			PipeCellViewFactory.setPortClass(map, PortChildView.class.getCanonicalName());
		} else if (port instanceof PipeParentPort) {
			GraphConstants.setLabelPosition(map, new Point2D.Double(5, 33));
			PipeCellViewFactory.setPortClass(map, PortParentView.class.getCanonicalName());
		} else {
			if (port.isInputPort()) {
				GraphConstants.setLabelPosition(map, new Point2D.Double(5, -10));
				PipeCellViewFactory.setPortClass(map, PortInputView.class.getCanonicalName());
			} else {
				GraphConstants.setLabelPosition(map, new Point2D.Double(5, 33));
				PipeCellViewFactory.setPortClass(map, PortOutputView.class.getCanonicalName());
			}
		}
		GraphConstants.setOpaque(map, true);
		GraphConstants.setOffset(map, new Point2D.Double(x, y));
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.DefaultPort#clone()
	 */
	public PipeModulePort clone() {
		Point2D p = GraphConstants.getOffset(this.getAttributes());
		return new PipeModulePort(p.getX(), p.getY(), (this.getPort()).clone());
	}

	/**
	 * Get pipe port for graph port.
	 * 
	 * @return pipe port
	 */
	public PipePort getPort() {
		return (PipePort) getUserObject();
	}
}
