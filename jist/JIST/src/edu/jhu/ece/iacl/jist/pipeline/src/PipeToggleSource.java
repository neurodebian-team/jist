/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Toggle between true/false.
 * 
 * @author Blake Lucas
 */
public class PipeToggleSource extends PipeSource {
	
	/** The start. */
	protected int start;
	
	/** The end. */
	protected int end;
	
	/** The inc. */
	protected int inc;
	
	/** The val. */
	protected int val;
	
	/** The val param. */
	protected ParamBoolean valParam;
	
	/** The toggle. */
	boolean toggle;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
		Element em;
//		em = document.createElement("valParam");
//		valParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
		
//		/** The start. */
//		protected int start;
//		
		em = document.createElement("start");		
		em.appendChild(document.createTextNode(start+""));
		parent.appendChild(em);
//		/** The end. */
//		protected int end;
		em = document.createElement("end");		
		em.appendChild(document.createTextNode(end+""));
		parent.appendChild(em);
//		
//		/** The inc. */
//		protected int inc;
		em = document.createElement("inc");		
		em.appendChild(document.createTextNode(inc+""));
		parent.appendChild(em);
//		
//		/** The val. */
//		protected int val;
		em = document.createElement("val");		
		em.appendChild(document.createTextNode(val+""));
		parent.appendChild(em);

//		/** The toggle. */
//		boolean toggle;
		em = document.createElement("toggle");		
		em.appendChild(document.createTextNode(toggle+""));
		parent.appendChild(em);

		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		valParam = (ParamBoolean)outputParams.getFirstChildByName("Toggle");
//		valParam = new ParamBoolean();
//		valParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"valParam"));
		/** The start. */
//		protected int start;
//		
		start= Integer.valueOf(JistXMLUtil.xmlReadTag(el, "start"));
//		/** The end. */
//		protected int end;
		end= Integer.valueOf(JistXMLUtil.xmlReadTag(el, "end"));
//		
//		/** The inc. */
//		protected int inc;
		inc= Integer.valueOf(JistXMLUtil.xmlReadTag(el, "inc"));
//		
//		/** The val. */
//		protected int val;
		val= Integer.valueOf(JistXMLUtil.xmlReadTag(el, "val"));
//		
//		boolean toggle;
		toggle= Boolean.valueOf(JistXMLUtil.xmlReadTag(el, "toggle"));
		getParentPort().setParameter(valParam);
	}
	
	
	/**
	 * Default constructor.
	 */
	public PipeToggleSource() {
		super();
		getParentPort().setParameter(valParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Toggle");
		group.setName("toggle");
		group.setCategory("Selection");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Toggle");
		group.add(valParam = new ParamBoolean("Toggle"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamBoolean getOutputParam() {
		return valParam;
	}

	/**
	 * Returns true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || toggle);
	}

	/**
	 * Iterate.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (toggle) {
					valParam.setValue(toggle);
					push();
					toggle = !toggle;
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			reset();
			isReset = true;
			return false;
		}
	}

	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		toggle = false;
		valParam.setValue(toggle);
		toggle = !toggle;
		push();
	}
}
