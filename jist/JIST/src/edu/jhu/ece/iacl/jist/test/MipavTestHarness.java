package edu.jhu.ece.iacl.jist.test;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.MipavViewUserInterface;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingApplication;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHeader;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parser.JISTDynamicPluginLookup;
import gov.nih.mipav.view.ViewJFrameImage;
import gov.nih.mipav.view.ViewUserInterface;

import java.io.File;
import java.util.Vector;

// TODO: Auto-generated Javadoc
/**
 * The Class MipavTestHarness.
 */
public class MipavTestHarness{
	
	/** The pwd. */
	private static String pwd=System.getProperty("user.dir");
	
	/** The ui. */
	private ViewUserInterface ui;
	
	/** The plug. */
	private ProcessingApplication plug;
	
	/** The VISIBLE. */
	private boolean VISIBLE=true;
	
	/**
	 * Instantiates a new mipav test harness.
	 * 
	 * @param plug the plug
	 */
	public MipavTestHarness(ProcessingApplication plug){
		this(new String[]{});
		this.plug=plug;
	}
	
	/**
	 * Instantiates a new mipav test harness.
	 */
	public MipavTestHarness(){
		this(new String[]{});
	}
	
	/**
	 * Instantiates a new mipav test harness.
	 * 
	 * @param args the args
	 */
	public MipavTestHarness(String args[]){
        ui=MipavViewUserInterface.create();	
        
	}
	
	/**
	 * Gets the present working directory.
	 * 
	 * @return the present working directory
	 */
	public static String getPresentWorkingDirectory(){
		return pwd;
	}
	
	/**
	 * Open.
	 * 
	 * @param fileName the file name
	 */
	public void open(String fileName){
		ui.openImageFrame(fileName);

	}
	
	/**
	 * Invoke.
	 * 
	 * @param algo the algo
	 */
	public void invoke(ProcessingAlgorithm algo){
		invoke(plug=new ProcessingApplication(algo){});
	}


	/**
	 * Execute.
	 * 
	 * @param f the f
	 * @param saveDir the save dir
	 */
	public void execute(File f,File saveDir){
		
		plug.getDialog().clickOpen(f);
		plug.getDialog().clickOk();
		if(saveDir!=null)plug.getDialog().clickSaveAll(saveDir.getAbsoluteFile());
		plug.getDialog().clickClose();
	}
	
	/**
	 * Invoke.
	 * 
	 * @param plug the plug
	 */
	public void invoke(ProcessingApplication plug){
		ui.setVisible(VISIBLE);
		this.plug=plug;
		plug.run();
	}
	
	/**
	 * Invoke.
	 * 
	 * @param inputFile the input file
	 * @param outputDir the output dir
	 */
	public void invoke(File inputFile,File outputDir){
    	ParamCollection tmp=(ParamCollection)ParamFactory.fromXML(inputFile);
    	Class c=null;
    	for(ParamModel child:tmp.getChildren()){
    		if(child instanceof ParamHeader){
    			c=((ParamHeader)child).getValue();
    			break;
    		}
    	}
    	if(c==null){
    		System.err.print("Could not find algorithm class");
    		return;
    	} else {
	    	ProcessingAlgorithm algo =null;
	    	try {
				algo = (ProcessingAlgorithm)c.newInstance();
				if(algo instanceof JISTDynamicPluginLookup) {
					JISTDynamicPluginLookup algoI = (JISTDynamicPluginLookup)algo;
					String str = ((ParamString)tmp.getFirstChildByName(algoI.getClassTag())).getValue();
					algoI.init(str);
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (AlgorithmRuntimeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			VISIBLE=false;
			invoke(algo);
			execute(inputFile,outputDir);	
				
    	}
	}
    
    /**
     * Close.
     */
    protected void close() { 
        Vector frames=ui.getImageFrameVector();
        ViewJFrameImage[] frameList=new ViewJFrameImage[frames.size()];
        int i=0;
        for(Object obj:frames){
        	frameList[i++]=(ViewJFrameImage)obj;
        }
        for(ViewJFrameImage frame:frameList){
        	System.out.println(getClass().getCanonicalName()+"\t"+"REMOVING FRAME "+frame.getTitle());
            ui.unregisterFrame(frame);
        	frame.close();
        }
        System.gc();
    } 
    
    /**
     * The main method.
     * 
     * @param args the arguments
     */
    public static void main(String[] args){
    	MipavTestHarness harness=new MipavTestHarness();
    	harness.invoke(new File(args[0]), new File(args[1]));
    	System.exit(0);
    }
}
