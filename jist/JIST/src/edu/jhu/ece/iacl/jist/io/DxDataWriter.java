package edu.jhu.ece.iacl.jist.io;


import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Class DxDataWriter.
 */
public class DxDataWriter {
	
	/**
	 * Write vector int.
	 * 
	 * @param out the out
	 * @param vec the vec
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void writeVectorInt(DataOutputStream out, int[] vec) throws IOException{
		
		for(int i=0; i<vec.length;i++){
//			System.out.println("jist.io"+"\t"+String.valueOf(vec[i]) +"\n");
			out.writeChars(String.valueOf(vec[i]) +"\n");
		}
		
	}
	
	/**
	 * Write vector int.
	 * 
	 * @param out the out
	 * @param vec the vec
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void writeVectorInt(BufferedWriter out, int[] vec) throws IOException{
		
		for(int i=0; i<vec.length;i++){
//			System.out.println("jist.io"+"\t"+String.valueOf(vec[i]) +"\n");
			out.append(String.valueOf(vec[i]) +"\n");
		}
		
	}
	
	/**
	 * Write ascii.
	 * 
	 * @param out the out
	 * @param towrite the towrite
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void writeASCII(DataOutputStream out, String towrite) throws IOException{
		out.writeChars(towrite);
	}
	
	
	/**
	 * Write.
	 * 
	 * @param dat the dat
	 * @param f the f
	 * 
	 * @return true, if successful
	 */
	public static boolean write(double[][] dat,File f){
		try { 
			BufferedWriter out=new BufferedWriter(new FileWriter(f));
			String dxheader1 = "object \"data\" class array type byte rank 0 items ";
//			System.out.println("jist.io"+"\t"+dxheader1);
			dxheader1 = dxheader1 + dat.length + "\n";
			String dxheader2 = "data follows\n";
			
			out.append(dxheader1);
			out.append(dxheader2);

			for(int i=0;i<dat.length;i++){
				for(int j=0;j<dat[i].length;j++){
					out.append((byte)dat[i][j]+" ");
				} 
				out.append("\n");
			}
			out.close();	
			return true;
		} catch (IOException e) {
			System.err.println("jist.base"+e.getMessage());
			return false;
		}		
	}
	
	/**
	 * Write.
	 * 
	 * @param dat the dat
	 * @param f the f
	 * 
	 * @return true, if successful
	 */
	public static boolean write(int[][] dat,File f){
		try { 
			BufferedWriter out=new BufferedWriter(new FileWriter(f));
			String dxheader1 = "object \"data\" class array type byte rank 0 items ";
			System.out.println("jist.io"+"\t"+dxheader1);
			dxheader1 = dxheader1 + dat.length + "\n";
			String dxheader2 = "data follows\n";
			
			out.append(dxheader1);
			out.append(dxheader2);

			for(int i=0;i<dat.length;i++){
				for(int j=0;j<dat[i].length;j++){
					out.append((byte)dat[i][j]+" ");
				} 
				out.append("\n");
			}
			out.close();	
			return true;
		} catch (IOException e) {
			System.err.println("jist.base"+e.getMessage());
			return false;
		}		
	}
	
	/**
	 * Write.
	 * 
	 * @param dat the dat
	 * @param f the f
	 * 
	 * @return true, if successful
	 */
	public static boolean write(int[] dat,File f){
		try { 
			BufferedWriter out=new BufferedWriter(new FileWriter(f));
			String dxheader1 = "object \"data\" class array type byte rank 0 items ";
			System.out.println("jist.io"+"\t"+dxheader1);
			dxheader1 = dxheader1 + dat.length + "\n";
			String dxheader2 = "data follows\n";

			out.append(dxheader1);
			out.append(dxheader2);

			for(int i=0;i<dat.length;i++){
				if((byte)dat[i]>=0){
					out.append(((byte)dat[i])+" ");
				}else{
//					System.out.println("jist.io"+"\t"+"started with: " + (byte)dat[i]);
//					System.out.println("jist.io"+"\t"+"added: " + ((byte)(-dat[i])));
					out.append(((byte)(-dat[i]))+" ");
				}
				out.append("\n");
			}
			out.close();	
			return true;
		} catch (IOException e) {
			System.err.println("jist.base"+e.getMessage());
			return false;
		}		
	}
	
//	public static void writeData(BufferedWriter out, double[][] data) throws IOException{
//		out.append(String.format("object \"data\" class array type byte rank 0 items %d data follows\n", ));
//	
//		
//	}
//	

}
