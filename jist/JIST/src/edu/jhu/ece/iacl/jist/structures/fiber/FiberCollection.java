package edu.jhu.ece.iacl.jist.structures.fiber;

import java.util.Vector;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;

// TODO: Auto-generated Javadoc
/**
 * The Class FiberCollection.
 */
public class FiberCollection extends Vector<Fiber>{
	
	/** The name. */
	protected String name;
	
	/** The dimensions. */
	protected Point3i dimensions;
	
	/** The resolutions. */
	protected Point3f resolutions;
	
	/**
	 * Instantiates a new fiber collection.
	 */
	public FiberCollection(){
		super();
		setName("Fibers");
	}
	
	/**
	 * Instantiates a new fiber collection.
	 * 
	 * @param num the num
	 */
	public FiberCollection(int num){
		super(num);
		setName("Fibers");
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the from fiber array.
	 * 
	 * @param fibers the fibers
	 * 
	 * @return the from fiber array
	 */
	public void getFromFiberArray(Fiber[] fibers){
		this.clear();
		for(Fiber f:fibers){
			this.add(f);
		}
	}
	
	/**
	 * To curve collection.
	 * 
	 * @return the curve collection
	 */
	public CurveCollection toCurveCollection(){
		CurveCollection cc=new CurveCollection();
		for(Fiber fbr:this){
			cc.add(fbr.toCurvePath());
		}
		cc.setName(this.getName());
		return cc;
	}
	
	/**
	 * Gets the dimensions.
	 * 
	 * @return the dimensions
	 */
	public Point3i getDimensions() {
		return dimensions;
	}
	
	/**
	 * Sets the dimensions.
	 * 
	 * @param dimensions the new dimensions
	 */
	public void setDimensions(Point3i dimensions) {
		this.dimensions = dimensions;
	}
	
	/**
	 * Gets the resolutions.
	 * 
	 * @return the resolutions
	 */
	public Point3f getResolutions() {
		return resolutions;
	}
	
	/**
	 * Sets the resolutions.
	 * 
	 * @param resolutions the new resolutions
	 */
	public void setResolutions(Point3f resolutions) {
		this.resolutions = resolutions;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Vector#toString()
	 */
	public String toString(){
		return getName();
	}
}