package edu.jhu.ece.iacl.jist.io;

import java.io.File;

import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class JistLayoutReaderWriter.
 */
public class JistLayoutReaderWriter extends FileReaderWriter<PipeLayout>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/**
	 * Instantiates a new jist layout reader writer.
	 */
	public JistLayoutReaderWriter(){
		super(new FileExtensionFilter(JistPreferences.getPreferences().getAllValidLayoutExtensions()));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected PipeLayout readObject(File f) {
		return PipeLayout.read(f);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(PipeLayout layout, File f) {
		return layout.write(f, false)?f:null;
	}

}
