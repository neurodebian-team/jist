/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import java.io.File;
import java.util.List;

import javax.swing.ProgressMonitor;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamVolumeURICollectionInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamVolumeCollectionOutputView;
import gov.nih.mipav.model.scripting.parameters.ParameterException;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.model.structures.ModelImage;

/**
 * Volume collection parameter factory.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamVolumeCollectionFactory extends ParamFileCollectionFactory {
	
	/** The param. */
	private ParamVolumeCollection param;

	/**
	 * Instantiates a new param volume collection factory.
	 */
	public ParamVolumeCollectionFactory() {
	}

	/**
	 * Instantiates a new param volume collection factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamVolumeCollectionFactory(ParamVolumeCollection param) {
		super();
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory#createMipavParameter(gov.nih.mipav.view.dialogs.AlgorithmParameters)
	 */
/*	public void createMipavParameter(AlgorithmParameters scriptParams) throws ParserException {
		ParamVolumeCollection vols = getParameter();
		List<ModelImage> imgs = vols.getModelImageCacheList();
		String paramLabel = vols.getName();
		int i = 0;
		for (ModelImage img : imgs) {
			String var = scriptParams.storeImage(img, encodeName(paramLabel + "_" + (i++)));
		}
	}
*/
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof ParamFactory) {
			return this.equals(((ParamFactory) obj).getParameter());
		} else if (obj instanceof ParamVolumeCollection) {
			return this.getParameter().getFileList().equals(((ParamVolumeCollection) obj).getFileList());
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamVolumeURICollectionInputView(param);
		}
		return inputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamVolumeCollectionOutputView(param);
		}
		return outputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory#getParameter()
	 */
	public ParamVolumeCollection getParameter() {
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory#importMipavParameter(gov.nih.mipav.model.scripting.parameters.ParameterTable)
	 */
	public void importMipavParameter(ParameterTable paramTable) throws ParameterException {
		String paramLabel = getParameter().getName();
		int i = 0;
		ModelImage img = null;
		while (true) {
			img = null;
			try {
				img = paramTable.getImage(encodeName(paramLabel + "_" + (i++)));
				if (img == null) {
					break;
				}
				getParameter().add(img);
			} catch (ParameterException e) {
				break;
			}
		}
	}

	/**
	 * Load Model Image from file specified in foreign parameter.
	 * 
	 * @param foreignCollection
	 *            foreign collection of parameters
	 * @param monitor
	 *            the monitor
	 * @return external resources loaded correctly
	 */
	public boolean loadResources(ParamModel foreignCollection, ProgressMonitor monitor) {
		if (foreignCollection instanceof ParamVolumeCollection) {
			ParamInputView view = getInputView();
			if (view instanceof Refreshable) {
				Refresher.getInstance().remove(((Refreshable) view));
			}
			
			List<File> files = ((ParamVolumeCollection) foreignCollection).getFileList();
			getParameter().setValue(files);
			/*
			List<CubicVolume> vols=new ArrayList<CubicVolume>();
			if(monitor!=null){
				monitor.setProgress(0);
				monitor.setMaximum(files.size());
				
			}
			int count=0;
			for(File f:files){
				if(monitor!=null){
					if(monitor.isCanceled())break;
					monitor.setNote("Opening "+f.getName());
				}
				
				if(f.exists()){
					
				}
				CubicVolume img=CubicVolumeReaderWriter.getInstance().read(f);
				if(monitor!=null)monitor.setProgress(count++);
				if(img!=null){
					vols.add(img);
				}
			}
			
			if(monitor!=null)monitor.setProgress(count);
			param.setValue(vols);
			*/
			view.update();
			if (view instanceof Refreshable) {
				Refresher.getInstance().add(((Refreshable) view));
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save Surfaces to specified directory.
	 * 
	 * @param dir
	 *            save directory
	 * @return resources saved correctly
	 */
	public boolean saveResources(File dir, boolean saveSubDirectoryOverride) {
		super.saveResources(dir, saveSubDirectoryOverride);
		boolean ret = true;
		List<ParamFile> volumes = param.getParameters();
		for (ParamFile vol : volumes) {
			if (!vol.saveResources(dir,saveSubDirectoryOverride)) {
				ret = false;
				System.out.println(getClass().getCanonicalName()+"\t"+"ParamVolumeCollectionFactory: Resource Save Failed.");
			}
		}
		return ret;
	}
}
