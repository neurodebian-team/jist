/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import javax.vecmath.Point3d;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of points.
 * 
 * @author Blake Lucas
 */
public class PipePointListSource extends PipeListSource {
	
	/** The point param. */
	protected ParamPointDouble pointParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("pointParam");
//		pointParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		pointParam = (ParamPointDouble) outputParams.getFirstChildByName("Point");
//		pointParam = new ParamPointDouble();
//		pointParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"pointParam"));
		getParentPort().setParameter(pointParam);
	}
	
	
	/**
	 * Default constructor.
	 */
	public PipePointListSource() {
		super();
		getParentPort().setParameter(pointParam);
	}

	/**
	 * Create Input Parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Point File List");
		group.setName("pointlist");
		group.setCategory("Point");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Point");
		group.add(pointParam = new ParamPointDouble("Point"));
		return group;
	}

	/**
	 * Get parameter.
	 * 
	 * @return the output param
	 */
	public ParamPointDouble getOutputParam() {
		return pointParam;
	}

	/**
	 * Get value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected Point3d getValue(int i) {
		if (off + 2 < data[i].length) {
			return new Point3d(((Number) data[i][off]).doubleValue(), ((Number) data[i][off + 1]).doubleValue(),
					((Number) data[i][off + 2]).doubleValue());
		} else {
			return null;
		}
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object obj) {
		pointParam.setValue((Point3d) obj);
	}
}
