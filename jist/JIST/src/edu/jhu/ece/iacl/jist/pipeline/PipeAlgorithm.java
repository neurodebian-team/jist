/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Vector;

import javax.swing.JFileChooser;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmCell;
import edu.jhu.ece.iacl.jist.pipeline.gui.PipelineLayoutTool;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHeader;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamCollectionOutputView;

// TODO: Auto-generated Javadoc
/**
 * A pipe module for processing algorithms.
 * 
 * @author Blake Lucas
 */
public class PipeAlgorithm extends PipeModule implements Cloneable {
	
	/**
	 * Reconstruct algorithm from string.
	 * 
	 * @param str the string
	 * 
	 * @return the pipe algorithm
	 */
	public static PipeAlgorithm fromXML(String str) {
		XStream stream = new XStream();
		Object o =null;
		try {
			o = stream.fromXML(str);
		} catch(ConversionException e){
			System.err.println("jist.base"+e.getMessage());
		}
		if (o == null) {
			return null;
		}
		if (o instanceof PipeAlgorithm) {
			return (PipeAlgorithm) o;
		} else {
			return null;
		}
	}

	/**
	 * Open file from user specified location.
	 * 
	 * @param parent parent component
	 * 
	 * @return algorithm
	 */
	public static PipeAlgorithm open(Component parent) {
		return read(selectOpenFile(parent));
	}

	/**
	 * Read algorithm from file.
	 * 
	 * @param f the file
	 * 
	 * @return the pipe algorithm
	 */
	public static PipeAlgorithm read(File f) {
		if ((f == null) || !f.exists() || !FileReaderWriter.getFileExtension(f).equals(JistPreferences.getDefaultModuleExtension())) {
			return null;
		}
		BufferedReader in;
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			String text = "";
			StringBuffer buff = new StringBuffer();
			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str + "\n");
			}
			text = buff.toString();
			in.close();
			// Reconstruct class from XML

			PipeAlgorithm algo = fromXML(text);
			//Create new UUID for this algorithm
			algo.getInputParams().getHeader().stampUUID();
			if(algo!=null){
				algo.setMapFile(f);
				algo.init(null);
			}
			return algo;
		} catch (Exception e) {
			System.err.println("jist.base"+"Error occured while reading parameter file:\n" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Select file to open.
	 * 
	 * @param parent parent component
	 * 
	 * @return selected file
	 */
	private static File selectOpenFile(Component parent) {
		JFileChooser loadDialog = new JFileChooser("Specify Pipeline Library");
		loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(new String[] { JistPreferences.getDefaultModuleExtension() }));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showOpenDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return loadDialog.getSelectedFile();
		} else {
			return null;
		}
	}

	/** processing algorithm. */
	transient protected ProcessingAlgorithm algo;
	
	/**
	 * Gets the default jvm args for the algorithm unit that is encapsulated by this class 
	 * 
	 * @return the default jvm args, null if no-specific requests.
	 */
	public String []getDefaultJVMArgs() {
		if(algo==null) {
			return new String[]{"No Algo"};
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"JVM Class Name: "+algo.getClass().getCanonicalName());
			return algo.getDefaultJVMArgs();
		}
			
	}
	/** graph cell to represent algorithm. */
	transient protected AlgorithmCell lastCell = null;
	
	/** parent graph cell if this algorithm is in a group. */
	transient protected PipeAlgorithmGroup parent = null;
	
	/** file that stores this module definition. */
	transient protected File mapFile = null;
	
	/**
	 * Gets the category.
	 * 
	 * @return the category
	 */
	public String getCategory(){
		return inputParams.getCategory();
	}
	
	/**
	 * Gets the package.
	 * 
	 * @return the package
	 */
	public String getPackage(){
		return inputParams.getPackage();
	}
	
	/**
	 * Default constructor.
	 */
	public PipeAlgorithm() {
		super();
	}

	/**
	 * The Constructor.
	 * 
	 * @param algo Processing Algorithm to wrap with this pipe module
	 */
	public PipeAlgorithm(ProcessingAlgorithm algo) {
		setAlgorithm(algo);
	}

	/**
	 * Clone algorithm by serializing and deserializing module.
	 * 
	 * @return the pipe algorithm
	 * 
	 * @throws CloneNotSupportedException the clone not supported exception
	 */
	public PipeAlgorithm clone() throws CloneNotSupportedException {
		PipeAlgorithm algoClone = fromXML(toXML());
		return algoClone;
	}

	/**
	 * Create Processing Algorithm from the algorithm class.
	 * 
	 * @return Null if unable to create processing algorithm
	 */
	protected ProcessingAlgorithm createAlgorithm() {
		Class c = getAlgorithmClass();
		if (c == null) {
			System.err.print("Could not find algorithm class");
			return null;
		} else {
			ProcessingAlgorithm algo = null;
			try {
				algo = (ProcessingAlgorithm) c.newInstance();
				algo.setMapFile(this.mapFile);
				return algo;
			} catch (InstantiationException e) {
				System.err.println("jist.base"+"Could not Instantiate " + c);
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.err.println("jist.base"+"Illegal Access " + c);
			} catch (Exception e) {
				System.err.println("jist.base"+"General Error " + e.getMessage());
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Create cell to render this module.
	 * 
	 * @return the algorithm cell
	 */
	public AlgorithmCell createModuleCell() {
		return new AlgorithmCell(getLabel(), this);
	}

	/**
	 * Get Processing Algorithm wrapped by this pipe module.
	 * 
	 * @return the algorithm
	 */
	public ProcessingAlgorithm getAlgorithm() {
		if (algo == null) {
			algo = createAlgorithm();
		}
		return algo;
	}

	/**
	 * Get the Processing Algorithm class from the input parameters.
	 * 
	 * @return the algorithm class
	 */
	public Class getAlgorithmClass() {
		Class c = null;
		for (ParamModel child : inputParams.getChildren()) {
			if (child instanceof ParamHeader) {
				c = ((ParamHeader) child).getValue();
				break;
			}
		}
		return c;
	}

	/**
	 * Gets the current parent contexts.
	 * 
	 * @return the current parent contexts
	 */ 
	public Vector<ExecutionContext> getCurrentParentContexts() {
		Vector<ExecutionContext> foundParentContext = new Vector<ExecutionContext>();
		for (PipeModule parent : getParents()) {
			if ((parent instanceof PipeAlgorithm) && (parent.getCurrentContext() != null)) {
				foundParentContext.add(parent.getCurrentContext());
			}
		}
		return foundParentContext;
	}

	/**
	 * Get file that stores this algorithm.
	 * 
	 * @return map file
	 */
	public File getMapFile() {
		return mapFile;
	}

	/**
	 * Get parent module if this is a member of a group.
	 * 
	 * @return parent group module
	 */
	public PipeAlgorithmGroup getParentGroup() {
		return parent;
	}

	/**
	 * Initialize transient fields that could not be deserialized.
	 * 
	 * @param graph the graph
	 * 
	 * @return the pipe module cell
	 */
	public PipeModuleCell init(PipeJGraph graph) {
		for (PipePort port : inputPorts) {
			port.setPortType(PipePort.type.INPUT);
			port.setOwner(this);
		}
		for (PipePort port : outputPorts) {
			port.setPortType(PipePort.type.OUTPUT);
			port.setOwner(this);
		}
		this.getInputParams().getInputView().addObserver(this);
		this.getOutputParams().init();
		if (graph != null) {
			return graph.createGraphNode(this);
		} else {
			return null;
		}
	}

	/**
	 * Return true if this module doesn't have any parents who are Processing
	 * Algorithms.
	 * 
	 * @return true, if checks if is root
	 */
	public boolean isRoot() {
		for (PipePort port : getInputPorts()) {
			if (((Vector<PipePort>) port.getIncomingConnectors()).size() > 0) {
				for (PipePort incoming : (Vector<PipePort>) port.getIncomingPorts()) {
					if (incoming.getOwner() instanceof PipeAlgorithm) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Save algorithm in specified user location.
	 * 
	 * @return true, if save as
	 */
	public boolean saveAs() {
		return write(selectSaveFile());
	}

	/**
	 * Write input parameters to file.
	 * 
	 * @param f the file
	 * 
	 * @return true if successful
	 */
	public boolean saveInputParams(File f) {
		return inputParams.write(f);
	}

	/**
	 * Write output parameters to file.
	 * 
	 * @param f the file
	 * 
	 * @return true if successful
	 */
	public boolean saveOutputParams(File f) {
		return outputParams.write(f);
	}

	/**
	 * Select location to save algorithm.
	 * 
	 * @return file location
	 */
	protected File selectSaveFile() {
		JFileChooser saveDialog = new JFileChooser("Specify Pipeline Library");
		File mapFile = getMapFile();
		if (mapFile != null) {
			saveDialog.setSelectedFile(mapFile);
		} else {
			File file = PipeLibrary.getInstance().getLibraryPath();
			saveDialog.setCurrentDirectory(file);
			saveDialog.setSelectedFile(new File(file, this.getLabel() + "." + JistPreferences.getDefaultModuleExtension()));
		}
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		saveDialog.setFileFilter(new FileExtensionFilter(new String[] { JistPreferences.getDefaultModuleExtension() }));
		saveDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = saveDialog.showSaveDialog(PipelineLayoutTool.getInstance());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File f = saveDialog.getSelectedFile();
			f = new File(f.getParent(), FileReaderWriter.getFileName(f) + "." + JistPreferences.getDefaultModuleExtension());
			return f;
		} else {
			return null;
		}
	}

	/**
	 * Automatically creates all the input and output ports using the input and
	 * output parameters in the Processing Algorithm.
	 * 
	 * @param algo the algorithm
	 */
	public void setAlgorithm(ProcessingAlgorithm algo) {
		inputParams = algo.getInput();
		outputParams = algo.getOutput();
		this.label = inputParams.getLabel();
		this.name = inputParams.getName();
		inputPorts = (Vector) inputParams.getAllVisibleDescendants();
		for (PipePort port : inputPorts) {
			port.setPortType(PipePort.type.INPUT);
			port.setOwner(this);
		}
		algo.getInput().getInputView().addObserver(this);
		outputParams.init();
		outputPorts = (Vector) outputParams.getAllVisibleDescendants();
		for (PipePort port : outputPorts) {
			port.setPortType(PipePort.type.OUTPUT);
			port.setOwner(this);
		}
		this.algo = algo;
		// init();
	}

	/**
	 * Set file that stores this algorithm.
	 * 
	 * @param mapFile map file
	 */
	public void setMapFile(File mapFile) {
		this.mapFile = mapFile;
	}

	/**
	 * Set parent module.
	 * 
	 * @param parent parent module
	 */
	public void setParentGroup(PipeAlgorithmGroup parent) {
		this.parent = parent;
	}

	/**
	 * Returns algorithm label.
	 * 
	 * @return the string
	 */
	public String toString() {
		return this.getLabel();
	}

	/**
	 * Serialize class as XML.
	 * 
	 * @return the string
	 */
	public String toXML() {
		XStream stream = new XStream();
		return stream.toXML(this);
	}

	/**
	 * Write algorithm to file.
	 * 
	 * @param f output file
	 * 
	 * @return true, if write
	 */
	public boolean write(File f) {
		PrintWriter out;
		if (f == null) {
			return false;
		}
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(f)));
			String text = toXML();
			out.print(text);
			out.flush();
			out.close();
			System.out.println(getClass().getCanonicalName()+"\t"+"WROTE " + f);
			return true;
		} catch (IOException e) {
			MipavController.displayError("Could not save " + f + "\n" + e.getMessage());
			return false;
		}
	}

	@Override
	public String reconcileAndMerge() throws InvalidJistMergeException {

		String message = "";
		try {
			
			PipeAlgorithm alg = this.getClass().newInstance();
			if(alg==null) {
				// Class could not be found
				throw new InvalidJistMergeException("Cannot find class: "+getClass());
			}
				
			if(getAlgorithmClass()==null) {
				// Class could not be found
				throw new InvalidJistMergeException("Cannot find algorithm class: "+this.getAlgorithmClass());
			}
			alg.setAlgorithm((ProcessingAlgorithm)this.getAlgorithmClass().newInstance());
			message = alg.getAlgorithm().reconcileAndMerge(inputParams,outputParams);
//			alg.init(null);
					
			this.inputParams = alg.getAlgorithm().inputParams;
			this.outputParams = alg.getAlgorithm().outputParams;
			
			/*this.inputPorts = alg.inputPorts;
			this.outputPorts = alg.outputPorts;*/
			/*this.inputHash = alg.inputHash;
			this.outputHash = alg.outputHash;*/
			
			inputParams.init();
			inputPorts = (Vector) inputParams.getAllVisibleDescendants();
			for (PipePort port : inputPorts) {
				port.setPortType(PipePort.type.INPUT);
				port.setOwner(this);
			}			
			outputParams.init();
			outputPorts = (Vector) outputParams.getAllVisibleDescendants();
			for (PipePort port : outputPorts) {
				port.setPortType(PipePort.type.OUTPUT);
				port.setOwner(this);
			}
						
		} catch (InstantiationException e) {
			throw new InvalidJistMergeException("Cannot instantiate class.");
		} catch (IllegalAccessException e) {
			throw new InvalidJistMergeException("Cannot instantiate class.");
		}

		return "Merge Successful."+"\n"+message+"\nEnd Merge Report.";
	}

	
}
