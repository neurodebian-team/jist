/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Sweep through range of double values.
 * 
 * @author Blake Lucas
 */
public class PipeDoubleSweepSource extends PipeSource {
	
	/** The start. */
	transient protected double start;
	
	/** The end. */
	transient protected double end;
	
	/** The inc. */
	transient protected double inc;
	
	/** The val. */
	transient protected double val;
	
	/** The val param. */
	protected ParamDouble startParam, endParam, incParam, valParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("startParam");
//		startParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("endParam");
//		endParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("incParam");
//		incParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("valParam");
//		valParam.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		startParam = (ParamDouble) inputParams.getFirstChildByName("Start Value");
		endParam = (ParamDouble) inputParams.getFirstChildByName("End Value");
		incParam = (ParamDouble) inputParams.getFirstChildByName("Increment");
		valParam = (ParamDouble) outputParams.getFirstChildByName("Float");
//		startParam = new ParamDouble();
//		startParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"startParam"));
//		endParam = new ParamDouble();
//		endParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"endParam"));
//		incParam = new ParamDouble();
//		incParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"incParam"));
//		valParam = new ParamDouble();
//		valParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"valParam"));
		getParentPort().setParameter(valParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipeDoubleSweepSource() {
		super();
		
		getParentPort().setParameter(valParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float Sweep");
		group.setName("floatsweep");
		group.add(startParam = new ParamDouble("Start Value"));
		group.add(endParam = new ParamDouble("End Value"));
		group.add(incParam = new ParamDouble("Increment"));
		group.setCategory("Number.Float");
		return group;
	}

	/**
	 * Create ouptut parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float");
		group.add(valParam = new ParamDouble("Float"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamModel getOutputParam() {
		return valParam;
	}

	/**
	 * Return true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || ((inc > 0) && (val <= end)) || ((inc < 0) && (val >= end)));
	}

	/**
	 * Iterate through double values.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (((inc > 0) && (val <= end)) || ((inc < 0) && (val >= end))) {
					valParam.setValue(val);
					push();
					val += inc;
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			reset();
			isReset = true;
			return false;
		}
	}

	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		start = startParam.getDouble();
		end = endParam.getDouble();
		inc = Math.signum(end - start) * incParam.getDouble();
		val = start;
		valParam.setValue(val);
		val += inc;
		push();
	}
}
