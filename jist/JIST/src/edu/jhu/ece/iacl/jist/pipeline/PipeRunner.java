/**
 * Java Image Science Toolkit (JIST) Image Analysis and Communications
 * Laboratory & Laboratory for Medical Image Computing & The Johns Hopkins
 * University http://www.nitrc.org/projects/jist/ This library is free software;
 * you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version. The
 * license is available for reading at: http://www.gnu.org/copyleft/lgpl.html
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.UUID;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHeader;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parser.JISTDynamicPluginLookup;
import edu.jhu.ece.iacl.jist.utility.FileUtil;

// TODO: Auto-generated Javadoc
/**
 * Pipe Runner is responsible for executing a particular pipe algorithm given an
 * input parameter file.
 * 
 * @author Blake Lucas
 */
public class PipeRunner {

	/** Writer to writer stdout. */
	static BufferedWriter outWriter = null;

	/**
	 * Main method to execute algorithm specified by input parameters.
	 * 
	 * @param args
	 *            The first argument should be the input parameter file
	 */
	public static void main(String[] args) {
		File pipeFile = null;
		boolean showOutput = false;
		// Set relative or absolute path if one is specified
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("-show")) {
				showOutput = true;
				pipeFile = new File(args[1]);
			} else {
				pipeFile = new File(args[0]);
			}
			// Run pipe file
			run(pipeFile, showOutput);
			System.exit(0);
		} else {
			System.err.println("jist.base"+"Insufficient arguments");
		}

		// Accept multiple parameters
	}

	/**
	 * Run.
	 * 
	 * @param pipeFile
	 *            the pipe file
	 * @param showOutput
	 *            the show output
	 */
	public static void run(File pipeFile, boolean showOutput) {
//		System.err.println("jist.base"+"on");
//			System.out.println(getClass().getCanonicalName()+"\t"+"like whatever");
	
		
		PipeRunner harness = new PipeRunner();
		// Set relative or absolute path if one is specified
		// Redirect stdout and stderr to debug.out and debug.err
		String pipeName = FileReaderWriter.getFileName(pipeFile);
		File pipeDir = new File(pipeFile.getParentFile(), pipeName
				+ File.separator);
		if (!pipeDir.exists()) {
			pipeDir.mkdir();
		}
		File debugOut = new File(pipeDir, "debug.out");
		File debugErr = new File(pipeDir, "debug.err");
//		outWriter = new BufferedWriter(new OutputStreamWriter(
//				new BufferedOutputStream(System.out)));
		PrintStream[] streams = null;
		if (!showOutput) { // Bug fix to NITRC bug #3929
			outWriter = new BufferedWriter(new OutputStreamWriter(new
BufferedOutputStream(System.out)));
			streams = FileUtil.redirect(debugOut, debugErr);
		}
		try {
			// Invoke plug-in
			harness.invoke(pipeFile, pipeDir);
			// Close true stdout stream
//			try {
//				outWriter.flush();
//				outWriter.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.flush();
			System.out.flush();
//			if (!showOutput) {
//				for (PrintStream stream : streams) {
//					stream.flush();
//					stream.close();
//				}
//			}
		} finally {
			// Close redirected streams
			if (!showOutput) {
				for (PrintStream stream : streams) {
					stream.flush();
					stream.close();
				}
				FileUtil.restoreRedirect();
			}
		}
	}


	/**
	 * Write status to true stdout instead of file.
	 * 
	 * @param status
	 *            the status
	 */
	public static void writeStatus(String status) {
		// Write to stdout
		try {
			if (outWriter != null) {
				outWriter.write(status + "\n");
				outWriter.flush();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Execute a specified ProcessingApplication.
	 * 
	 * @param plug
	 *            application
	 * @param f
	 *            input parameter file
	 * @param saveDir
	 *            output directory
	 */
	public void execute(ProcessingApplication plug, File f, File saveDir) {
		plug.getDialog().setOutputDirectory(saveDir);
		plug.getDialog().init(f);
		if (plug.getDialog().clickOpen(f)) {
			plug.getAlgorithm().setOutputDirectory(saveDir);
			if (plug.getDialog().clickOk()) {
				if (saveDir != null) {
					plug.getDialog().clickSaveAll(saveDir.getAbsoluteFile());
				}
			} else {
				System.err.println("jist.base"+"COULD NOT RUN " + plug.getAlgorithm());
			}
			plug.getDialog().clickClose();
//			MipavController.clearReigstry();
		} else {
			System.err.println("jist.base"+"COULD NOT OPEN " + f);
		}
	}

	/**
	 * Invoke parameter file and save output to specified directory.
	 * 
	 * @param pipeFile
	 *            parameter file
	 * @param saveDir
	 *            specified directory
	 */
	public void invoke(File pipeFile, File saveDir) {		
		// Hide Mipav GUI
		MipavController.setQuiet(true);
		MipavController.init();

		String tempDir = gov.nih.mipav.view.Preferences.getFileTempDir();
		if (tempDir == null) {
			tempDir = System.getProperty("user.home") + File.separator + "mipav" + File.separator + "tempDir"
			+ File.separator;
		} else {
			String []strs = tempDir.split("RandomTempDir-");
			if(strs.length>1)
				tempDir = strs[0];
		}
		File currentDir = new File(tempDir); 

		File newDir = new File(currentDir, "RandomTempDir-"+UUID.randomUUID().toString()); 
		if (newDir.mkdirs()) {
			gov.nih.mipav.view.Preferences.setFileTempDir(newDir.toString());
		 	newDir.deleteOnExit();
		}

		try {
		// Load preferences
		PipeLibrary.getInstance().loadPreferences(true);
		
		// This might induce preference loading --- be sure to load preferences with force SILENT BEFORE
		ParamCollection tmp = (ParamCollection) ParamFactory.fromXML(pipeFile);
		Class c = null;
		// Find Main Class
		ParamHeader header = tmp.getHeader();
		if (header != null) {
			c = header.getValue();
		} else {
			System.err.println("jist.base"+"COULD NOT FIND HEADER CLASS " + tmp);
		}
		if (c == null) {
			System.err.print("Could not find algorithm class");
			return;
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"Found Class " + c);
			ProcessingAlgorithm algo = null;
			try {
				algo = (ProcessingAlgorithm) c.newInstance();
				if(algo instanceof JISTDynamicPluginLookup) {
					JISTDynamicPluginLookup algoI = (JISTDynamicPluginLookup)algo;
					String str = ((ParamString)tmp.getFirstChildByName(algoI.getClassTag())).getValue();
					algoI.init(str);
				}
			} catch (InstantiationException e) {
				System.err.println("jist.base"+"Could not Instantiate " + c);
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.err.println("jist.base"+"Illegal Access " + c);
			} catch (Exception e) {
				System.err.println("jist.base"+"General Error " + e.getMessage());
				e.printStackTrace();
			}
			// Create new instance of processing algorithm
			ProcessingApplication plug = invoke(algo);
			// Execute algorithm with specified input
			execute(plug, pipeFile, saveDir);
		}
		} finally {
			newDir.delete();	
		}
		
	}

	/**
	 * Invoke processing application.
	 * 
	 * @param algo
	 *            the algorithm
	 * 
	 * @return the processing application
	 */
	public ProcessingApplication invoke(ProcessingAlgorithm algo) {
		ProcessingApplication plug = new ProcessingApplication(algo);
		invoke(plug);
		return plug;
	}

	/**
	 * Invoke plug-in by calling run algorithm.
	 * 
	 * @param module
	 *            algorithm to execute
	 */
	public void invoke(ProcessingApplication module) {
		module.run();
	}
}