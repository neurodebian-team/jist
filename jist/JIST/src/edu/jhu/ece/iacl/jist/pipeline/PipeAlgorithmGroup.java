/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.Vector;

import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmGroupCell;
import edu.jhu.ece.iacl.jist.pipeline.gui.ParameterPanel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;

// TODO: Auto-generated Javadoc
/**
 * Algorithm group is an aggregation of PipeAlgorithms. An algorithm group is
 * only a container, it's not directly executable.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeAlgorithmGroup extends PipeAlgorithm {
	
	/** Collection of PipeAlgorithms. */
	protected Vector<PipeAlgorithm> moduleGroup;

	/**
	 * Default constructor.
	 */
	public PipeAlgorithmGroup() {
		super();
		moduleGroup = new Vector<PipeAlgorithm>();
		inputParams = new ParamCollection();
		outputParams = new ParamCollection();
		this.setLabel("Algorithm Group");
		this.name = "algo_group";
	}

	/**
	 * Add algorithm to collection.
	 * 
	 * @param algo algorithm
	 */
	public void add(PipeAlgorithm algo) {
		inputParams.add(algo.getInputParams());
		outputParams.add(algo.getOutputParams());
		algo.parent = this;
		moduleGroup.add(algo);
	}

	/**
	 * Return clone of this group.
	 * 
	 * @return the pipe algorithm group
	 * 
	 * @throws CloneNotSupportedException the clone not supported exception
	 */
	public PipeAlgorithmGroup clone() throws CloneNotSupportedException {
		// Deep Clone
		PipeAlgorithmGroup algoClone = (PipeAlgorithmGroup) fromXML(toXML());
		return algoClone;
	}

	/**
	 * Create Processing Algorithm from the algorithm class.
	 * 
	 * @return Null if unable to create processing algorithm
	 */
	protected ProcessingAlgorithm createAlgorithm() {
		return null;
	}

	/**
	 * Create cell to render this module.
	 * 
	 * @return the algorithm group cell
	 */
	public AlgorithmGroupCell createModuleCell() {
		AlgorithmGroupCell cell = new AlgorithmGroupCell(getLabel(), this);
		GraphConstants.setGroupOpaque(cell.getAttributes(), true);
		GraphConstants.setEditable(cell.getAttributes(), true);
		GraphConstants.setGradientColor(cell.getAttributes(), Color.WHITE);
		Rectangle2D bounds = getBounds();
		if (bounds != null) {
			bounds = new Rectangle2D.Double(bounds.getX(), bounds.getY(), 150, 50);
			GraphConstants.setBounds(cell.getAttributes(), bounds);
		}
		return cell;
	}

	/**
	 * Always returns null. An algorithm group does not have an algorithm class.
	 * 
	 * @return the algorithm class
	 */
	public Class getAlgorithmClass() {
		return null;
	}

	/**
	 * Get all descendant modules in group.
	 * 
	 * @return all descendant modules in group
	 */
	public Vector<PipeAlgorithm> getAllDescendantModules() {
		Vector<PipeAlgorithm> allModules = new Vector<PipeAlgorithm>();
		for (PipeAlgorithm mod : moduleGroup) {
			allModules.add(mod);
			if (mod instanceof PipeAlgorithmGroup) {
				allModules.addAll(((PipeAlgorithmGroup) mod).getAllDescendantModules());
			}
		}
		return allModules;
	}

	/**
	 * Get all leaf modules.
	 * 
	 * @return all lead modules
	 */
	public Vector<PipeAlgorithm> getAllLeafModules() {
		Vector<PipeAlgorithm> allModules = new Vector<PipeAlgorithm>();
		for (PipeAlgorithm mod : moduleGroup) {
			if (mod instanceof PipeAlgorithmGroup) {
				allModules.addAll(((PipeAlgorithmGroup) mod).getAllLeafModules());
			} else {
				allModules.add(mod);
			}
		}
		return allModules;
	}

	/**
	 * Get all modules in group.
	 * 
	 * @return modules in group
	 */
	public Vector<PipeAlgorithm> getGroupModules() {
		return moduleGroup;
	}

	/**
	 * Initialize transient fields that could not be deserialized.
	 * 
	 * @param graph Layout graph if one is available, null otherwise
	 * 
	 * @return the pipe module cell
	 */
	public PipeModuleCell init(PipeJGraph graph) {
		this.getInputParams().getInputView().addObserver(this);
		this.getOutputParams().init();
		this.addListener(ParameterPanel.getInstance());
		AlgorithmGroupCell cell = null;
		Vector<AlgorithmCell> cells = new Vector<AlgorithmCell>();
		//Initialize port types since these are not serialized
		for (PipePort port : inputPorts) {
			port.setPortType(PipePort.type.INPUT);
			port.setOwner(this);
		}
		for (PipePort port : outputPorts) {
			port.setPortType(PipePort.type.OUTPUT);
			port.setOwner(this);
		}
		//Initialize group members
		for (PipeModule pipe : moduleGroup) {
			if (pipe instanceof PipeAlgorithm) {
				((PipeAlgorithm) pipe).parent = this;
			}
			Vector<PipePort> ports = pipe.getInputPorts();
			for (PipePort port : ports) {
				port.setPortType(PipePort.type.INPUT);
			}
			ports = pipe.getOutputPorts();
			for (PipePort port : ports) {
				port.setPortType(PipePort.type.OUTPUT);
			}
			pipe.getInputParams().init();
			pipe.getOutputParams().init();
			if (graph != null) {
				pipe.getInputParams().getInputView().addObserver(pipe);
			}
			//Add members to list of cells
			cells.add((AlgorithmCell) pipe.init(graph));
		}
		if (graph != null) {
			graph.createUniqueName(this);
			cell = this.createModuleCell();
			cell.setChildrenCells(cells);
			//Insert group into graph
			graph.getGraphLayoutCache().insertGroup(cell, cells.toArray());
			// Initialized Connectors
			for (PipeModule pipe : getAllLeafModules()) {
				Vector<PipePort> ports = pipe.getInputPorts();
				for (PipePort port : ports) {
					if (port instanceof ParamModel) {
						if (graph != null) {
							((ParamModel) port).getInputView().update();
						}
					}
					port.addListener(pipe);
					if (port.getIncomingConnectors().size() > 0) {
						port.notifyListenersOfConnection();
						port.setUseConnector(true);
					}
				}
				ports = pipe.getOutputPorts();
				for (PipePort port : ports) {
					port.addListener(pipe);
					port.notifyListenersOfConnection();
				}
			}
			//Push group to back
			graph.getGraphLayoutCache().toBack(new Object[] { cell });
		}
		return cell;
	}

	/**
	 * Remove algorithm from collection.
	 * 
	 * @param algo algorithm
	 */
	public void remove(PipeAlgorithm algo) {
		if (algo instanceof PipeAlgorithmGroup) {
			((PipeAlgorithmGroup) algo).removeAllChildren();
		}
		if (moduleGroup.remove(algo)) {
			algo.parent = null;
			inputParams.remove(algo.getInputParams());
			outputParams.remove(algo.getOutputParams());
		}
	}

	/**
	 * Remove all algorithms from collection.
	 */
	public void removeAllChildren() {
		while (moduleGroup.size() > 0) {
			remove(moduleGroup.firstElement());
		}
	}

	/**
	 * Save group to user selected file.
	 * 
	 * @return true, if save as
	 */
	public boolean saveAs() {
		File f = selectSaveFile();
		if (f == null) {
			return false;
		}
		try {
			PipeAlgorithmGroup algoClone = this.clone();
			// Remove connections that extend outside the current group
			Vector<PipePort> inPorts = new Vector<PipePort>();
			Vector<PipeAlgorithm> allModules = algoClone.getAllLeafModules();
			for (PipeModule mod : allModules) {
				inPorts.addAll(mod.getInputPorts());
			}
			Vector<PipePort> outPorts = new Vector<PipePort>();
			for (PipeModule mod : allModules) {
				outPorts.addAll(mod.getOutputPorts());
			}
			for (PipePort port : outPorts) {
				Vector<PipeConnector> connectors = port.getOutgoingConnectors();
				for (int i = 0; i < connectors.size(); i++) {
					PipeConnector conn = connectors.get(i);
					if (!allModules.contains(conn.getDestination().getOwner())) {
						conn.disconnect();
						i--;
					}
				}
			}
			for (PipePort port : inPorts) {
				Vector<PipeConnector> connectors = port.getIncomingConnectors();
				for (int i = 0; i < connectors.size(); i++) {
					PipeConnector conn = connectors.get(i);
					if (!allModules.contains(conn.getSource().getOwner())) {
						conn.disconnect();
						i--;
					}
				}
			}
			return algoClone.write(f);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Returns size of collection.
	 * 
	 * @return collection size
	 */
	public int size() {
		return moduleGroup.size();
	}
}
