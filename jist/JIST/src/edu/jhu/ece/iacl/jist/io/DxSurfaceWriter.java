package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

// TODO: Auto-generated Javadoc
/**
 * The Class DxSurfaceWriter.
 */
public class DxSurfaceWriter {
	
	/** The Constant TEXT_1. */
	static final String TEXT_1 = "object 1 class array type float rank 1 shape 3 items ";
	
	/** The Constant TEXT_2. */
	static final String TEXT_2 = " data follows";
	
	/** The Constant TEXT_3. */
	static final String TEXT_3 = "attribute \"dep\" string \"positions\"";
	
	/** The Constant TEXT_4. */
	static final String TEXT_4 = "object 2 class array type int rank 1 shape 3 items ";
	
	/** The Constant TEXT_5. */
	static final String TEXT_5 = " data follows";
	
	/** The Constant TEXT_6. */
	static final String TEXT_6 = "attribute \"ref\" string \"positions\"";
	
	/** The Constant TEXT_7. */
	static final String TEXT_7 = "attribute \"element type\" string \"triangles\"";
	
	/** The Constant TEXT_8. */
	static final String TEXT_8 = "attribute \"dep\" string \"connections\"";
	
	/** The Constant TEXT_9. */
	static final String TEXT_9 = "#";
	
	/** The Constant TEXT_10. */
	static final String TEXT_10 = "object \"default\" field";
	
	/** The Constant TEXT_11. */
	static final String TEXT_11 = "    component \"positions\"  1";
	
	/** The Constant TEXT_12. */
	static final String TEXT_12 = "    component \"connections\"  2";
	
	/** The Constant TEXT_13. */
	static final String TEXT_13 = "end";
	
	/** The write. */
	static FileWriter write;
	
	/** The output. */
	static PrintWriter output;
	
	/** The file name. */
	static String fileName;
	
	/** The num of vertices. */
	static int numOfVertices;
	
	/** The num of faces. */
	static int numOfFaces;
	
	/**
	 * From free surfer writer.
	 * 
	 * @param vertices the vertices
	 * @param faces the faces
	 * @param f the f
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public static void fromFreeSurferWriter(String[] vertices, String[] faces, File f, float X, float Y, float Z)
	{
	    String pattern = "###.0000";
		DecimalFormat df = new DecimalFormat(pattern);
		String results;
		try
		{	
			write = new FileWriter(f);
			PrintWriter output = new PrintWriter(write);

			numOfVertices = (int)vertices.length/4;
		    System.out.println("jist.io"+"\t"+numOfVertices); //Checking numOfVertices
		    numOfFaces = (int)faces.length/4;
		    System.out.println("jist.io"+"\t"+numOfFaces); //Checking numOfVertices
		    output.println(TEXT_1 + numOfVertices + TEXT_2);
			
			int verticesCounter = 1;
		    for (int i = 0; i < vertices.length; i++ )
			{
					
				//String verticesData;
				if (verticesCounter == 1)
				{
					float vertex_x = Float.parseFloat(vertices[i]);
					vertex_x = vertex_x + (X/2);
					results = df.format(vertex_x);
					output.print(results + "  ");
					verticesCounter++;
				}
				else if (verticesCounter == 2)
				{
					float vertex_y = Float.parseFloat(vertices[i]);
					vertex_y = vertex_y + (Y/2);
					results = df.format(vertex_y);
					output.print(results + "  ");
					verticesCounter++;
				}
				else if (verticesCounter == 3)
				{
					float vertex_z = Float.parseFloat(vertices[i]);
					vertex_z = vertex_z + (Z/2);
					results = df.format(vertex_z);
					output.println(results);
					verticesCounter++;
				}
				else verticesCounter = 1;
					
			}
			
			output.println();
			output.println(TEXT_3);
			output.println(TEXT_4 + numOfFaces + TEXT_5);
			
			int facesCounter = 1;
			for (int i = 0; i < faces.length; i++ )
			{
				String facesData = faces[i];
				if (facesCounter < 3)
				{
					output.print(facesData + "  ");
					facesCounter++;
				}
				else if(facesCounter == 3)
				{
					output.println(facesData);
					facesCounter++;
				}
				else facesCounter = 1;
			}
			
			output.println();
			output.println(TEXT_6);
			output.println(TEXT_7);
			output.println(TEXT_8);
			output.println(TEXT_9);
			output.println(TEXT_10);
			output.println(TEXT_11);
			output.println(TEXT_12);
			output.println(TEXT_13);
			
			output.close();
			write.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		return;
	}
	
	/**
	 * From mipav writer.
	 * 
	 * @param vertices the vertices
	 * @param faces the faces
	 * @param f the f
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public static void fromMipavWriter(String[] vertices, String[] faces, File f, float X, float Y, float Z)
	{
		String pattern = "###.0000";
		DecimalFormat df = new DecimalFormat(pattern);
		String results;
		try
		{	
			write = new FileWriter(f);
			PrintWriter output = new PrintWriter(write);

			numOfVertices = (int)vertices.length/3;
		    System.out.println("jist.io"+"\t"+numOfVertices); //Checking numOfVertices
		    numOfFaces = (int)faces.length/3;
		    System.out.println("jist.io"+"\t"+numOfFaces); //Checking numOfVertices
		    output.println(TEXT_1 + numOfVertices + TEXT_2);
			
			int verticesCounter = 1;
		    for (int i = 0; i < vertices.length; i++ )
			{
		    	if (verticesCounter < 4)
				{
					
					if (verticesCounter == 1)
					{
						float vertex_x = Float.parseFloat(vertices[i]);
						vertex_x = (float)((X/2) * vertex_x + 128);
						results = df.format(vertex_x);
						output.print(results + "  ");
						verticesCounter++;
					}
					else if (verticesCounter == 2)
					{
						float vertex_y = Float.parseFloat(vertices[i]);
						vertex_y = (float)((Y/2) * vertex_y + 128);
						results = df.format(vertex_y);
						output.print(results + "  ");
						verticesCounter++;
					}
					else 
					{
						float vertex_z = Float.parseFloat(vertices[i]);
						vertex_z = (float)((Z/2) * vertex_z + 128);
						results = df.format(vertex_z);
						output.println(results);
						verticesCounter = 1;
					}
				}
					
			}
			
			output.println();
			output.println(TEXT_3);
			output.println(TEXT_4 + numOfFaces + TEXT_5);
			
			int facesCounter = 1;
			for (int i = 0; i < faces.length; i++ )
			{
				String facesData = faces[i];
				if (facesCounter < 3)
				{
					output.print(facesData + "  ");
					facesCounter++;
				}
				else
				{
					output.println(facesData);
					facesCounter = 1;
				}
			}
			
			output.println();
			output.println(TEXT_6);
			output.println(TEXT_7);
			output.println(TEXT_8);
			output.println(TEXT_9);
			output.println(TEXT_10);
			output.println(TEXT_11);
			output.println(TEXT_12);
			output.println(TEXT_13);
			
			output.close();
			write.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("jist.io"+"\t"+e.getMessage());
		}
		return;
	}

}	


