/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ProgressMonitor;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.UndoableEditEvent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import org.jgraph.JGraph;
import org.jgraph.event.GraphSelectionEvent;
import org.jgraph.event.GraphSelectionListener;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphUndoManager;

import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithmGroup;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.graph.JGraphFoldingManager;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeMarqueeHandler;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmGroupCell;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;

/**
 * Internal frame to display graph of pipeline.
 * 
 * @author Blake Lucas
 */
public class PipeInternalFrame extends JInternalFrame implements GraphSelectionListener, KeyListener,
		PropertyChangeListener {
	// This will change the source of the actionevent to graph.
	/**
	 * The Class EventRedirector.
	 */
	public class EventRedirector extends AbstractAction {
		
		/** The action. */
		protected Action action;
		
		/** The item. */
		protected JMenuItem item = null;

		// Construct the "Wrapper" Action
		/**
		 * Instantiates a new event redirector.
		 * 
		 * @param a
		 *            the a
		 */
		public EventRedirector(Action a) {
			super("", (ImageIcon) a.getValue(Action.DEFAULT));
			this.action = a;
			this.item = item;
		}

		// Redirect the Actionevent
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			e = new ActionEvent(graph, e.getID(), e.getActionCommand(), e.getModifiers());
			action.actionPerformed(e);
		}
	}

	/** The graph. */
	protected PipeJGraph graph;
	
	/** The undo manager. */
	protected GraphUndoManager undoManager;
	
	/** The toolbar. */
	protected JToolBar toolbar;
	
	/** The expand. */
	public Action undo, redo, remove, group, ungroup, tofront, toback, cut, copy, paste, collapse, expand;
	
	/** The folding manager. */
	protected JGraphFoldingManager foldingManager;

	/**
	 * Default constructor.
	 * 
	 * @param name
	 *            frame name
	 */
	public PipeInternalFrame(String name) {
		super(name, true, true, true, true);
		JScrollPane pane = new JScrollPane(graph = createGraph(), ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setSize(new Dimension(500, 500));
		// Use Border Layout
		getContentPane().setLayout(new BorderLayout());
		// Add a ToolBar
		getContentPane().add(createToolBar(), BorderLayout.NORTH);
		// Add the Graph as Center Component
		getContentPane().add(pane, BorderLayout.CENTER);
		// Use a Custom Marquee Handler
		graph.setMarqueeHandler(new PipeMarqueeHandler(graph));
		foldingManager = new JGraphFoldingManager();
		graph.addMouseListener(foldingManager);
		setVisible(true);
		this.addPropertyChangeListener(JInternalFrame.IS_CLOSED_PROPERTY, this);
		this.addPropertyChangeListener(JInternalFrame.IS_SELECTED_PROPERTY, this);
	}

	/**
	 * Close current frame.
	 * 
	 * @return true, if close
	 */
	public boolean close() {
		if (getPipelineLayout().close()) {
			this.dispose();
			ParameterPanel.getInstance().refreshView();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create graph and install listeners.
	 * 
	 * @return the pipe j graph
	 */
	protected PipeJGraph createGraph() {
		PipeJGraph graph = new PipeJGraph(this);
		// Create a GraphUndoManager which also Updates the ToolBar
		undoManager = new GraphUndoManager() {
			// Override Superclass
			public void undoableEditHappened(UndoableEditEvent e) {
				// First Invoke Superclass
				super.undoableEditHappened(e);
				// Then Update Undo/Redo Buttons
				updateHistoryButtons();
			}
		};
		installListeners(graph);
		return graph;
	}

	/**
	 * Create toolbar to manipulate graph.
	 * 
	 * @return the j tool bar
	 */
	protected JToolBar createToolBar() {
		toolbar = new JToolBar();
		toolbar.setFloatable(false);
		// Undo
		toolbar.addSeparator();
		URL undoUrl = PlaceHolder.class.getResource("undo.gif");
		ImageIcon undoIcon = new ImageIcon(undoUrl);
		undo = new AbstractAction("", undoIcon) {
			public void actionPerformed(ActionEvent e) {
				undo();
			}
		};
		undo.setEnabled(false);
		toolbar.add(undo);
		// Redo
		URL redoUrl = PlaceHolder.class.getResource("redo.gif");
		ImageIcon redoIcon = new ImageIcon(redoUrl);
		redo = new AbstractAction("", redoIcon) {
			public void actionPerformed(ActionEvent e) {
				redo();
			}
		};
		redo.setEnabled(false);
		toolbar.add(redo);
		//
		// Edit Block
		//
		toolbar.addSeparator();
		Action action;
		URL url;
		// Copy
		action = javax.swing.TransferHandler // JAVA13:
				// org.jgraph.plaf.basic.TransferHandler
				.getCopyAction();
		url = PlaceHolder.class.getResource("copy.gif");
		toolbar.add(copy = new EventRedirector(action));
		copy.putValue(Action.LARGE_ICON_KEY, new ImageIcon(url));
		// Paste
		action = javax.swing.TransferHandler // JAVA13:
				// org.jgraph.plaf.basic.TransferHandler
				.getPasteAction();
		url = PlaceHolder.class.getResource("paste.gif");
		action.putValue(Action.DEFAULT, new ImageIcon(url));
		paste = new EventRedirector(action);
		paste.putValue(Action.LARGE_ICON_KEY, new ImageIcon(url));
		toolbar.add(paste);
		// Cut
		action = javax.swing.TransferHandler // JAVA13:
				// org.jgraph.plaf.basic.TransferHandler
				.getCutAction();
		url = PlaceHolder.class.getResource("cut.gif");
		toolbar.add(cut = new EventRedirector(action));
		cut.putValue(Action.LARGE_ICON_KEY, new ImageIcon(url));
		// Remove
		URL removeUrl = PlaceHolder.class.getResource("delete.gif");
		ImageIcon removeIcon = new ImageIcon(removeUrl);
		remove = new AbstractAction("", removeIcon) {
			public void actionPerformed(ActionEvent e) {
				if (!graph.isSelectionEmpty()) {
					Object[] cells = graph.getSelectionCells();
					cells = graph.getDescendants(cells);
					graph.getModel().remove(cells);
				}
			}
		};
		remove.setEnabled(false);
		toolbar.add(remove);
		// To Front
		toolbar.addSeparator();
		URL toFrontUrl = PlaceHolder.class.getResource("tofront.gif");
		ImageIcon toFrontIcon = new ImageIcon(toFrontUrl);
		tofront = new AbstractAction("", toFrontIcon) {
			public void actionPerformed(ActionEvent e) {
				if (!graph.isSelectionEmpty()) {
					toFront(graph.getSelectionCells());
				}
			}
		};
		tofront.setEnabled(false);
		toolbar.add(tofront);
		// To Back
		URL toBackUrl = PlaceHolder.class.getResource("toback.gif");
		ImageIcon toBackIcon = new ImageIcon(toBackUrl);
		toback = new AbstractAction("", toBackIcon) {
			public void actionPerformed(ActionEvent e) {
				if (!graph.isSelectionEmpty()) {
					toBack(graph.getSelectionCells());
				}
			}
		};
		toback.setEnabled(false);
		toolbar.add(toback);
		// Zoom Std
		toolbar.addSeparator();
		URL zoomUrl = PlaceHolder.class.getResource("zoom.gif");
		ImageIcon zoomIcon = new ImageIcon(zoomUrl);
		toolbar.add(new AbstractAction("", zoomIcon) {
			public void actionPerformed(ActionEvent e) {
				graph.setScale(1.0);
			}
		});
		// Zoom In
		URL zoomInUrl = PlaceHolder.class.getResource("zoomin.gif");
		ImageIcon zoomInIcon = new ImageIcon(zoomInUrl);
		toolbar.add(new AbstractAction("", zoomInIcon) {
			public void actionPerformed(ActionEvent e) {
				graph.setScale(2 * graph.getScale());
			}
		});
		// Zoom Out
		URL zoomOutUrl = PlaceHolder.class.getResource("zoomout.gif");
		ImageIcon zoomOutIcon = new ImageIcon(zoomOutUrl);
		toolbar.add(new AbstractAction("", zoomOutIcon) {
			public void actionPerformed(ActionEvent e) {
				graph.setScale(graph.getScale() / 2);
			}
		});
		// Group
		toolbar.addSeparator();
		URL groupUrl = PlaceHolder.class.getResource("group.gif");
		ImageIcon groupIcon = new ImageIcon(groupUrl);
		group = new AbstractAction("", groupIcon) {
			public void actionPerformed(ActionEvent e) {
				group(graph.getSelectionCells());
			}
		};
		group.setEnabled(false);
		toolbar.add(group);
		// Ungroup
		URL ungroupUrl = PlaceHolder.class.getResource("ungroup.gif");
		ImageIcon ungroupIcon = new ImageIcon(ungroupUrl);
		ungroup = new AbstractAction("", ungroupIcon) {
			public void actionPerformed(ActionEvent e) {
				ungroup(graph.getSelectionCells());
			}
		};
		toolbar.add(ungroup);
		// Collapse
		collapse = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				for (Object obj : graph.getSelectionCells()) {
					if (obj instanceof AlgorithmGroupCell) {
						// ((AlgorithmGroupCell)obj).setPortVisibility(graph,
						// true);
						graph.getGraphLayoutCache().collapse(new Object[] { obj });
					}
				}
			}
		};
		url = PlaceHolder.class.getResource("collapse.gif");
		collapse.putValue(Action.SMALL_ICON, new ImageIcon(url));
		collapse.setEnabled(false);
		toolbar.add(collapse);
		// Expand
		expand = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				for (Object obj : graph.getSelectionCells()) {
					if (obj instanceof AlgorithmGroupCell) {
						graph.getGraphLayoutCache().expand(new Object[] { obj });
						// ((AlgorithmGroupCell)obj).setPortVisibility(graph,
						// false);
					}
				}
			}
		};
		url = PlaceHolder.class.getResource("expand.gif");
		expand.putValue(Action.SMALL_ICON, new ImageIcon(url));
		expand.setEnabled(false);
		toolbar.add(expand);
		ungroup.setEnabled(false);
		copy.setEnabled(false);
		cut.setEnabled(false);
		return toolbar;
	}

	/**
	 * Dispose of graph.
	 */
	public void dispose() {
		graph.dispose();
		super.dispose();
	}

	/**
	 * Fire action performed.
	 * 
	 * @param act
	 *            the act
	 */
	public void fireActionPerformed(Action act) {
		act.actionPerformed(new ActionEvent(toolbar, (int) System.currentTimeMillis(), null));
	}

	/**
	 * Gets the copy.
	 * 
	 * @return Returns the copy.
	 */
	public Action getCopy() {
		return copy;
	}

	/**
	 * Gets the cut.
	 * 
	 * @return Returns the cut.
	 */
	public Action getCut() {
		return cut;
	}

	/**
	 * Gets the graph.
	 * 
	 * @return the graph
	 */
	public PipeJGraph getGraph() {
		return graph;
	}

	/**
	 * Gets the paste.
	 * 
	 * @return Returns the paste.
	 */
	public Action getPaste() {
		return paste;
	}

	/**
	 * Get layout for frame.
	 * 
	 * @return the pipeline layout
	 */
	public PipeLayout getPipelineLayout() {
		return graph.getPipelineLayout();
	}

	/**
	 * Gets the remove.
	 * 
	 * @return Returns the remove.
	 */
	public Action getRemove() {
		return remove;
	}

	/**
	 * Gets the toback.
	 * 
	 * @return Returns the toback.
	 */
	public Action getToback() {
		return toback;
	}

	/**
	 * Gets the tofront.
	 * 
	 * @return Returns the tofront.
	 */
	public Action getTofront() {
		return tofront;
	}

	// Create a Group that Contains the Cells
	/**
	 * Group cells together.
	 * 
	 * @param cells
	 *            the cells
	 */
	public void group(Object[] cells) {
		// Order Cells by Model Layering
		cells = graph.order(cells);
		// If Any Cells in View
		if ((cells != null) && (cells.length > 0)) {
			PipeAlgorithmGroup algoGroup = new PipeAlgorithmGroup();
			Vector<PipeAlgorithmFactory.AlgorithmCell> childCells = new Vector<PipeAlgorithmFactory.AlgorithmCell>();
			for (Object cell : cells) {
				if (cell instanceof PipeAlgorithmFactory.AlgorithmCell) {
					algoGroup.add((PipeAlgorithm) (((PipeAlgorithmFactory.AlgorithmCell) cell).getPipeModule()));
					childCells.add((AlgorithmCell) cell);
				}
			}
			AlgorithmGroupCell group = algoGroup.createModuleCell();
			Rectangle2D bounds = graph.getCellBounds(cells);
			if (bounds != null) {
				bounds = new Rectangle2D.Double(bounds.getX(), bounds.getY(), 150, 50);
				GraphConstants.setBounds(group.getAttributes(), bounds);
			}
			group.setChildrenCells(childCells);
			LayoutPanel.getInstance().getActiveFrame().getGraph().createUniqueName(algoGroup);
			// Insert into model
			graph.getGraphLayoutCache().insertGroup(group, childCells.toArray());
			graph.getGraphLayoutCache().toBack(new Object[] { group });
			// group.setPortVisibility(graph,false);
		}
	}

	/**
	 * Insert draggable node into graph.
	 * 
	 * @param node
	 *            the node
	 */
	public void insert(DraggableNode node) {
		Rectangle b = graph.getBounds();
		insert(node, new Point((int) ((b.getWidth() / 2 - 75)), (int) ((b.getHeight() / 2 - 75))));
	}

	/**
	 * Insert node into graph.
	 * 
	 * @param node
	 *            the node
	 * @param cursorLocationBis
	 *            cursor location
	 */
	public void insert(DraggableNode node, Point cursorLocationBis) {
		graph.insert(node, cursorLocationBis);
	}

	// Hook for subclassers
	/**
	 * Install listeners.
	 * 
	 * @param graph
	 *            the graph
	 */
	protected void installListeners(JGraph graph) {
		// Add Listeners to Graph
		//
		// Register UndoManager with the Model
		graph.getModel().addUndoableEditListener(undoManager);
		// Update ToolBar based on Selection Changes
		graph.getSelectionModel().addGraphSelectionListener(this);
		// Listen for Delete Keystroke when the Graph has Focus
		graph.addKeyListener(this);
	}

	/**
	 * Internal frame activated.
	 * 
	 * @param arg0
	 *            the arg0
	 */
	public void internalFrameActivated(InternalFrameEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
		// Listen for Delete Key Press
		if (e.getKeyCode() == KeyEvent.VK_DELETE) {
			// Execute Remove Action on Delete Key Press
			remove.actionPerformed(null);
		}
	}

	//
	// KeyListener for Delete KeyStroke
	//
	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}

	/**
	 * Refresh parameter view if frame has been selected.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName() == JInternalFrame.IS_SELECTED_PROPERTY) {
			ParameterPanel.getInstance().refreshView();
		}
	}

	/**
	 * Redo the last Change to the Model or the View.
	 */
	public void redo() {
		try {
			// MipavUtil.displayError("Disabled because redo is not a stable
			// feature yet.");
			// Disabled until bugs are resolved!
			undoManager.redo(graph.getGraphLayoutCache());
		} catch (CannotRedoException ex) {
			System.err.println(getClass().getCanonicalName()+"REDO " + ex);
			// ex.printStackTrace();
		} finally {
			updateHistoryButtons();
		}
	}

	/**
	 * Select this frame.
	 */
	public void select() {
		firePropertyChange(JInternalFrame.IS_SELECTED_PROPERTY, false, true);
	}

	/**
	 * Sets the copy.
	 * 
	 * @param copy
	 *            The copy to set.
	 */
	public void setCopy(Action copy) {
		this.copy = copy;
	}

	/**
	 * Sets the cut.
	 * 
	 * @param cut
	 *            The cut to set.
	 */
	public void setCut(Action cut) {
		this.cut = cut;
	}

	/**
	 * Set whether pipeline is editable. This will effect the parameter panel
	 * and module panel too.
	 * 
	 * @param enabled
	 *            the enabled
	 */
	public void setEnabled(boolean enabled) {
		graph.setEnabled(enabled);
		ParameterPanel.getInstance().setEnabled(enabled);
		ModuleTreePanel.getInstance().setEnabled(enabled);
	}

	/**
	 * Sets the paste.
	 * 
	 * @param paste
	 *            The paste to set.
	 */
	public void setPaste(Action paste) {
		this.paste = paste;
	}

	/**
	 * Set layout for graph.
	 * 
	 * @param layout
	 *            the layout
	 * @param monitor
	 *            the monitor
	 */
	public void setPipelineLayout(PipeLayout layout, ProgressMonitor monitor) {
		graph.setPipelineLayout(layout, monitor);
	}

	/**
	 * Sets the remove.
	 * 
	 * @param remove
	 *            The remove to set.
	 */
	public void setRemove(Action remove) {
		this.remove = remove;
	}

	/**
	 * Sets the toback.
	 * 
	 * @param toback
	 *            The toback to set.
	 */
	public void setToback(Action toback) {
		this.toback = toback;
	}

	/**
	 * Sets the tofront.
	 * 
	 * @param tofront
	 *            The tofront to set.
	 */
	public void setTofront(Action tofront) {
		this.tofront = tofront;
	}

	/**
	 * Sends the Specified Cells to Back.
	 * 
	 * @param c
	 *            cells
	 */
	public void toBack(Object[] c) {
		graph.getGraphLayoutCache().toBack(c);
	}

	/**
	 * Brings the Specified Cells to Front.
	 * 
	 * @param c
	 *            the c
	 */
	public void toFront(Object[] c) {
		graph.getGraphLayoutCache().toFront(c);
	}

	/**
	 * Undo the last Change to the Model or the View.
	 */
	public void undo() {
		try {
			// MipavUtil.displayError("Disabled because undo is not a stable
			// feature yet.");
			// Disabled until bugs are resolved!
			undoManager.undo(graph.getGraphLayoutCache());
		} catch (CannotUndoException ex) {
			System.err.println(getClass().getCanonicalName()+"UNDO " + ex);
		} finally {
			updateHistoryButtons();
		}
	}

	/**
	 * Ungroup the Groups in Cells and Select the Children.
	 * 
	 * @param cells
	 *            the cells
	 */
	public void ungroup(Object[] cells) {
		for (Object obj : cells) {
			if (obj instanceof AlgorithmGroupCell) {
				graph.getGraphLayoutCache().expand(new Object[] { obj });
				LayoutPanel.getInstance().getPipelineLayout().ungroup(
						(PipeAlgorithmGroup) ((AlgorithmGroupCell) obj).getPipeModule());
			}
		}
		graph.getGraphLayoutCache().ungroup(cells);
	}

	// Hook for subclassers
	/**
	 * Uninstall listeners.
	 * 
	 * @param graph
	 *            the graph
	 */
	protected void uninstallListeners(JGraph graph) {
		graph.getModel().removeUndoableEditListener(undoManager);
		graph.getSelectionModel().removeGraphSelectionListener(this);
		graph.removeKeyListener(this);
	}

	/**
	 * Update Undo/Redo Button State based on Undo Manager.
	 */
	protected void updateHistoryButtons() {
		// The View Argument Defines the Context
		undo.setEnabled(undoManager.canUndo(graph.getGraphLayoutCache()));
		redo.setEnabled(undoManager.canRedo(graph.getGraphLayoutCache()));
	}

	// From GraphSelectionListener Interface
	/* (non-Javadoc)
	 * @see org.jgraph.event.GraphSelectionListener#valueChanged(org.jgraph.event.GraphSelectionEvent)
	 */
	public void valueChanged(GraphSelectionEvent e) {
		// Group Button only Enabled if more than One Cell Selected
		group.setEnabled(graph.getSelectionCount() > 1);
		// Update Button States based on Current Selection
		boolean enabled = !graph.isSelectionEmpty();
		Object[] cells = graph.getSelectionCells();
		if (cells.length > 0) {
			for (Object cell : cells) {
				if (cell instanceof PipeModuleCell) {
					ParameterPanel.getInstance().select(((PipeModuleCell) cell).getPipeModule());
					break;
				}
			}
		}
		remove.setEnabled(enabled);
		ungroup.setEnabled(enabled);
		tofront.setEnabled(enabled);
		toback.setEnabled(enabled);
		copy.setEnabled(enabled);
		cut.setEnabled(enabled);
		collapse.setEnabled(enabled);
		expand.setEnabled(enabled);
	}
	public void setTitle(String title){
		super.setTitle(title);
		PipelineLayoutTool.getInstance().updateTitle();
	}
}
