package edu.jhu.ece.iacl.jist.structures.tensor;

import edu.jhu.ece.iacl.jist.structures.vector.VectorX;

// TODO: Auto-generated Javadoc
/**
 * Tensor with 3 vectors.
 * 
 * @author Blake Lucas
 */
public class Tensor3 implements TensorX {
	
	/** The t3. */
	public VectorX t1, t2, t3;
}
