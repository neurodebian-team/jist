package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;
import java.io.IOException;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.ModelStorageBase;
import gov.nih.mipav.view.ViewJFrameImage;

// TODO: Auto-generated Javadoc
/**
 * Cubic volume adapter to MIPAV ModelImage type. Model Images can be wrapped in
 * this class without copying data values and preserving header information.
 * 
 * @author Blake Lucas
 */
public class ImageDataMipav extends ImageData {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7378590185009607038L;

	/**
	 * Copy data to model image.
	 * 
	 * @param vol the vol
	 * @param img the img
	 */
	public static void copyDataToModelImage(ImageData vol, ModelImage img) {
		int rows = vol.getRows();
		int cols = vol.getCols();
		int slices = vol.getSlices();
		int comps = vol.getComponents();
		if (vol.getType() == VoxelType.COLOR) {			
			Color c;
			if (vol.getSlices() < 2 && vol.getComponents() < 2) {
				//				COLOR 2-D
				byte[] buffer = new byte[rows * cols * 4];
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						c = vol.getColor(i, j);
						if (c != null) {
							buffer[4 * ((j * rows) + i)] = (byte) c.getAlpha();
							buffer[4 * ((j * rows) + i) + 1] = (byte) c.getRed();
							buffer[4 * ((j * rows) + i) + 2] = (byte) c.getGreen();
							buffer[4 * ((j * rows) + i) + 3] = (byte) c.getBlue();
						} else {
							buffer[4 * ((j * rows) + i)] = 0;
							buffer[4 * ((j * rows) + i) + 1] = 0;
							buffer[4 * ((j * rows) + i) + 2] = 0;
							buffer[4 * ((j * rows) + i) + 3] = 0;
						}
					}
				}
				try {
					img.importData(0, buffer, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else { 
				// not COLOR 2-D
				if (vol.getComponents() > 1) {
					// COLOR 4-D
					byte[] buffer = new byte[rows * cols * slices * comps * 4];
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								for (int l = 0; l < comps; l++) {
									c = vol.getColor(i, j, k, l);
									if (c != null) {
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i)] = (byte) c
										.getAlpha();
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1] = (byte) c
										.getRed();
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2] = (byte) c
										.getGreen();
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3] = (byte) c
										.getBlue();
									} else {
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i)] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3] = 0;
									}
								}
							}
						}
					}
					try {
						img.importData(0, buffer, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					// Color 3-D
					byte[] buffer = new byte[rows * cols * slices * 4];
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								c = vol.getColor(i, j, k);
								if (c != null) {
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i)] = (byte) c.getAlpha();
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 1] = (byte) c.getRed();
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 2] = (byte) c.getGreen();
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 3] = (byte) c.getBlue();
								} else {
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i)] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 1] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 2] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 3] = 0;
								}
							}
						}
					}
					try {
						img.importData(0, buffer, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else if (vol.getType() == VoxelType.COLOR_FLOAT) {
			Color c;
			float[] cArray = new float[4];
			if (vol.getSlices() < 2  && vol.getComponents() < 2) {
				// COLOR-FLOAT 2D
				float[] buffer = new float[rows * cols * 4];
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						c = vol.getColor(i, j);
						c.getRGBComponents(cArray);
						if (c != null) {
							buffer[4 * ((j * rows) + i)] = cArray[3];
							buffer[4 * ((j * rows) + i) + 1] = cArray[0];
							buffer[4 * ((j * rows) + i) + 2] = cArray[1];
							buffer[4 * ((j * rows) + i) + 3] = cArray[2];
						} else {
							buffer[4 * ((j * rows) + i)] = 0;
							buffer[4 * ((j * rows) + i) + 1] = 0;
							buffer[4 * ((j * rows) + i) + 2] = 0;
							buffer[4 * ((j * rows) + i) + 3] = 0;
						}
					}
				}
				try {
					img.importData(0, buffer, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				if (vol.getComponents() > 1) {
					//					Color Float 4-D
					float[] buffer = new float[rows * cols * slices * comps * 4];
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								for (int l = 0; l < comps; l++) {
									c = vol.getColor(i, j, k, l);
									c.getRGBComponents(cArray);
									if (c != null) {
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i)] = cArray[3];
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1] = cArray[0];
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2] = cArray[1];
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3] = cArray[2];
									} else {
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i)] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3] = 0;
									}
								}
							}
						}
					}
					try {
						img.importData(0, buffer, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					// Color-float 3D
					float[] buffer = new float[rows * cols * slices * 4];
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								c = vol.getColor(i, j, k);
								c.getRGBComponents(cArray);
								if (c != null) {
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i)] = cArray[3];
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 1] = cArray[0];
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 2] = cArray[1];
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 3] = cArray[2];
								} else {
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i)] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 1] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 2] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 3] = 0;
								}
							}
						}
					}
					try {
						img.importData(0, buffer, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else if (vol.getType() == VoxelType.COLOR_USHORT) {
			Color c;
			float[] cArray = new float[4];
			if (vol.getSlices() < 2  && vol.getComponents() < 2) {
				// Color short 2-D
				short[] buffer = new short[rows * cols * 4];
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						c = vol.getColor(i, j);
						c.getRGBComponents(cArray);
						if (c != null) {
							buffer[4 * ((j * rows) + i)] = (short) Math.round(((cArray[3] < 0) ? cArray[3] + 65536 : cArray[3]) * 65535);
							buffer[4 * ((j * rows) + i) + 1] = (short) Math.round(((cArray[0] < 0) ? cArray[0] + 65536 : cArray[0]) * 65535);
							buffer[4 * ((j * rows) + i) + 2] = (short) Math.round(((cArray[1] < 0) ? cArray[1] + 65536 : cArray[1]) * 65535);
							buffer[4 * ((j * rows) + i) + 3] = (short) Math.round(((cArray[2] < 0) ? cArray[2] + 65536 : cArray[2]) * 65535);
						} else {
							buffer[4 * ((j * rows) + i)] = 0;
							buffer[4 * ((j * rows) + i) + 1] = 0;
							buffer[4 * ((j * rows) + i) + 2] = 0;
							buffer[4 * ((j * rows) + i) + 3] = 0;
						}
					}
				}
			} else {
				// color short 4-d
				if (vol.getComponents() > 1) {
					short[] buffer = new short[rows * cols * slices * comps * 4];
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								for (int l = 0; l < comps; l++) {
									c = vol.getColor(i, j, k, l);
									c.getRGBComponents(cArray);
									if (c != null) {
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i)] = (short) Math
										.round(((cArray[3] < 0) ? cArray[3] + 65536 : cArray[3]) * 65535);
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1] = (short) Math
										.round(((cArray[0] < 0) ? cArray[0] + 65536 : cArray[0]) * 65535);
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2] = (short) Math
										.round(((cArray[1] < 0) ? cArray[1] + 65536 : cArray[1]) * 65535);
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3] = (short) Math
										.round(((cArray[2] < 0) ? cArray[2] + 65536 : cArray[2]) * 65535);
									} else {
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i)] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2] = 0;
										buffer[4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3] = 0;
									}
								}
							}
						}
					}
					try {
						img.importData(0, buffer, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					// color short 3-D
					short[] buffer = new short[rows * cols * slices * 4];
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								c = vol.getColor(i, j, k);
								c.getRGBComponents(cArray);
								if (c != null) {
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i)] = (short) Math
									.round(((cArray[3] < 0) ? cArray[3] + 65536 : cArray[3]) * 65535);
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 1] = (short) Math
									.round(((cArray[0] < 0) ? cArray[0] + 65536 : cArray[0]) * 65535);
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 2] = (short) Math
									.round(((cArray[1] < 0) ? cArray[1] + 65536 : cArray[1]) * 65535);
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 3] = (short) Math
									.round(((cArray[2] < 0) ? cArray[2] + 65536 : cArray[2]) * 65535);
								} else {
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i)] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 1] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 2] = 0;
									buffer[4 * ((k * (rows * cols)) + (j * rows) + i) + 3] = 0;
								}
							}
						}
					}
					try {
						img.importData(0, buffer, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {

			if (vol.getSlices() < 2 && vol.getComponents()<2) {
				// Not color 2D

				for (int i = 0; i < vol.getRows(); i++) {
					for (int j = 0; j < vol.getCols(); j++) {
						img.set(i, j, vol.getDouble(i, j));
					}
				}
			} else {
				if (vol.getComponents() > 1) {
					// Not color 4-D

					for (int i = 0; i < vol.getRows(); i++) {
						for (int j = 0; j < vol.getCols(); j++) {
							for (int k = 0; k < vol.getSlices(); k++) {
								for (int l = 0; l < vol.getComponents(); l++) {
									img.set(i, j, k, l, vol.get(i, j, k, l));
								}
							}
						}
					}
				} else {
					// not color 3-D

					for (int i = 0; i < vol.getRows(); i++) {
						for (int j = 0; j < vol.getCols(); j++) {
							for (int k = 0; k < vol.getSlices(); k++) {
								img.set(i, j, k, vol.getDouble(i, j, k));
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Creates the model image.
	 * 
	 * @param vol the vol
	 * 
	 * @return the model image
	 */
	public static ModelImage createModelImage(ImageData vol) {
		JistLogger.logOutput(JistLogger.INFO,"Create ModelImage from from Existing: " + vol.getName());
		// If image does not exist, create a new one
		ModelImage img;

		// create a new ModelImage to store volume data
		if (vol.getComponents() > 1) {
			img = new ModelImage(ImageDataMipav.getMipavType(vol.getType()), new int[] { vol.getRows(),
				vol.getCols(), vol.getSlices(), vol.getComponents() }, vol.getName());
		} else {
			img = new ModelImage(ImageDataMipav.getMipavType(vol.getType()), new int[] { vol.getRows(),
				vol.getCols(), vol.getSlices() }, vol.getName());
		}
		for (FileInfoBase base : img.getFileInfo()) {
			// Always use Big Endian!
			base.setEndianess(true);
		}
		if (vol.getName() != null) {
			MipavController.setModelImageName(img,vol.getName());			
		}
		copyDataToModelImage(vol, img);
		if (vol.getHeader() != null) {
			for (FileInfoBase base : img.getFileInfo()) {
				vol.getHeader().copyBasicInfoTo(base);
				if(vol.getHeader().isDefaultHeader()) {
					System.out.println("ImageDataMipav"+"\t"+"*****************************************************");
					System.out.println("ImageDataMipav"+"\t"+"* Warning: Creating Model Image with Default Header: "+vol.getName());
					System.out.println("ImageDataMipav"+"\t"+"*****************************************************");
				}
			} 
		} else {
			System.out.println("ImageDataMipav"+"\t"+"*****************************************************************");
			System.out.println("ImageDataMipav"+"\t"+"* Warning: Creating Model Image with NO Header (assume default) *");
			System.out.println("ImageDataMipav"+"\t"+"*****************************************************************");
		}
		img.calcMinMax();
		return img;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#setHeader(edu.jhu.ece.iacl.jist.structures.image.ImageHeader)
	 */
	public void setHeader(ImageHeader info) {
		this.imageHeader = info.clone();
		boolean copy=false;
		for (FileInfoBase base : img.getFileInfo()) {
			if(base==null)continue;
			info.copyBasicInfoTo(base);
			copy=true;
			if(info.isDefaultHeader()) {
				System.out.println(getClass().getCanonicalName()+"\t"+"***********************************************");
				System.out.println(getClass().getCanonicalName()+"\t"+"* Warning: Setting header with Default Header *");
				System.out.println(getClass().getCanonicalName()+"\t"+"***********************************************");
			}
		} 
		if(!copy){
			System.err.println(getClass().getCanonicalName()+"Could not copy file header into fileInfoBase for "+getName());
		}
	}

	/**
	 * Creates the model image.
	 * 
	 * @param vol the vol
	 * @param img the img
	 * 
	 * @return the model image
	 */
	public static ModelImage createModelImage(ImageData vol, ModelImage img) {
		if (img == null) {
			img = createModelImage(vol);
		} else {
			copyDataToModelImage(vol, img);
			if (vol.getHeader() != null) {
				for (FileInfoBase base : img.getFileInfo()) {
					vol.getHeader().copyBasicInfoTo(base);
					if(vol.getHeader().isDefaultHeader()) {
						System.out.println("ImageDataMipav"+"\t"+"*****************************************************");
						System.out.println("ImageDataMipav"+"\t"+"* Warning: Creating Model Image with Default Header :"+vol.getName()+" // "+img.getImageName());
						System.out.println("ImageDataMipav"+"\t"+"*****************************************************");
					}
				}
			}else {
				System.out.println("ImageDataMipav"+"\t"+"*****************************************************************");
				System.out.println("ImageDataMipav"+"\t"+"* Warning: Creating Model Image with NO Header (assume default) *");
				System.out.println("ImageDataMipav"+"\t"+"*****************************************************************");
			}
		}
		return img;
	}

	/**
	 * Get MIPAV voxel type for specified voxel type MIPAV types are ambiguous
	 * because they use integers instead of enums This function is not reflexive
	 * for Vector types since MIPAV doesn't have a Vector type.
	 * 
	 * @param type the type
	 * 
	 * @return the mipav type
	 */
	public static int getMipavType(VoxelType type) {
		switch (type) {
		case FLOAT:
			return ModelStorageBase.FLOAT;
		case DOUBLE:
			return ModelStorageBase.DOUBLE;
		case INT:
			return ModelStorageBase.INTEGER;
		case UBYTE:
			return ModelStorageBase.UBYTE;
		case SHORT:
			return ModelStorageBase.SHORT;
		case USHORT:
			return ModelStorageBase.USHORT;
		case COLOR:
			return ModelStorageBase.ARGB;
		case COLOR_USHORT:
			return ModelStorageBase.ARGB_USHORT;
		case COLOR_FLOAT:
			return ModelStorageBase.ARGB_FLOAT;
		case BOOLEAN:
			return ModelStorageBase.BOOLEAN;
		case VECTORX:
			return ModelStorageBase.FLOAT;
		default:
			return -1;
		}
	}

	/**
	 * Get voxel type for specified MIPAV voxel type MIPAV types are ambiguous
	 * because they use integers instead of enums.
	 * 
	 * @param mipavType the mipav type
	 * 
	 * @return the voxel type
	 */
	public static VoxelType getVoxelType(int mipavType) {
		switch (mipavType) {
		case ModelStorageBase.BOOLEAN:
			return VoxelType.BOOLEAN;
		case ModelStorageBase.BYTE: 
			return VoxelType.BYTE;
		case ModelStorageBase.UBYTE:
			return VoxelType.UBYTE;
		case ModelStorageBase.SHORT:
			return VoxelType.SHORT;
		case ModelStorageBase.USHORT:
			return VoxelType.USHORT;
		case ModelStorageBase.INTEGER:
			return VoxelType.INT;
		case ModelStorageBase.UINTEGER:
			JistLogger.logError(JistLogger.SEVERE, "Data type UINT not currently support.");
			return null;
		case ModelStorageBase.LONG:
			return VoxelType.LONG;			
		case ModelStorageBase.FLOAT:
			return VoxelType.FLOAT;
		case ModelStorageBase.DOUBLE:
			return VoxelType.DOUBLE;
		case ModelStorageBase.ARGB:
			return VoxelType.COLOR;
		case ModelStorageBase.ARGB_USHORT:
			return VoxelType.COLOR_USHORT;
		case ModelStorageBase.ARGB_FLOAT:
			return VoxelType.COLOR_FLOAT;
		case ModelStorageBase.COMPLEX:
			JistLogger.logError(JistLogger.SEVERE, "Data type complex not currently support.");
			return null;
		case ModelStorageBase.DCOMPLEX:
			JistLogger.logError(JistLogger.SEVERE, "Data type complex not currently support.");
			return null;
		default:
			return null;
		}
	}

	/** The img. */
	protected ModelImage img;

	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param vol the vol
	 */
	public ImageDataMipav(ImageData vol) {
		JistLogger.logOutput(JistLogger.INFO,"New ImageData Copy from Existing: " + vol.getName());
		ImageDataMipavPrivate(createModelImage(vol));
	}

	public ImageDataMipav(ModelImage img) {
		JistLogger.logOutput(JistLogger.INFO,"Cloning ImageDataMipav: " + img.getImageName());
		ImageDataMipavPrivate((ModelImage)img.clone());
	}
	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param img the img
	 */
	public void ImageDataMipavPrivate(ModelImage img) {		
		this.img = img;
		if (img == null) {
			return;
		}
		this.name = img.getImageName();
		
		int extents[] = img.getExtents();
		type = getVoxelType(img.getType());
		if (img.getFileInfo()!=null&&img.getFileInfo().length > 0) {
			setHeader(new ImageHeader(img.getFileInfo(0)));
		}
		if (extents != null) {
			rows = (extents.length > 0) ? extents[0] : 0;
			cols = (extents.length > 1) ? extents[1] : 0;
			slices = (extents.length > 2) ? extents[2] : 0;
			components = (extents.length > 3) ? extents[3] : 0;
		} else {
			rows = cols = slices = components = 0;
		}
	}

	/*
	 * public CubicVolumeMipav(VoxelType type, int rows, int cols, int slices) {
	 * this(Math.random()+"",type,rows,cols,slices);
	 * //MipavController.registerImage(img); this.type = type; } public
	 * CubicVolumeMipav(VoxelType type, int rows, int cols, int slices, int
	 * components) { this(null,type,rows,cols,slices,components);
	 * //MipavController.registerImage(img); this.type = type; }
	 */

	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param name the name
	 * @param rows the rows
	 * @param cols the cols
	 * @param slices the slices
	 */
	public ImageDataMipav(String name, int rows, int cols) {
		this(name,rows,cols,1,1);
	}

	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param name the name
	 * @param rows the rows
	 * @param cols the cols
	 * @param slices the slices
	 */
	public ImageDataMipav(String name, int rows, int cols, int slices) {
		this(name,rows,cols,slices,1);
	}

	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param name the name
	 * @param rows the rows
	 * @param cols the cols
	 * @param slices the slices
	 * @param components the components
	 */
	public ImageDataMipav(String name, int rows, int cols, int slices, int components) {
		ImageDataMipavPrivate(new ModelImage(ImageDataMipav.getMipavType(VoxelType.FLOAT), new int[] { rows, cols, slices,
			components }, name));
		// MipavController.registerImage(img);
		this.type = getVoxelType(this.img.getType());
	}

	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param name the name
	 * @param type the type
	 * @param rows the rows
	 * @param cols the cols
	 * @param slices the slices
	 */
	public ImageDataMipav(String name, VoxelType type, int rows, int cols, int slices) {		
		ImageDataMipavPrivate(new ModelImage(ImageDataMipav.getMipavType(type), new int[] { rows, cols, slices }, name));
		// MipavController.registerImage(img);
		
		setName(name);
		this.type = type;
	}

	/**
	 * Instantiates a new image data mipav.
	 * 
	 * @param name the name
	 * @param type the type
	 * @param rows the rows
	 * @param cols the cols
	 * @param slices the slices
	 * @param components the components
	 */
	public ImageDataMipav(String name, VoxelType type, int rows, int cols, int slices, int components) {
		ImageDataMipavPrivate(new ModelImage(ImageDataMipav.getMipavType(type), new int[] { rows, cols, slices, components }, name));
		// MipavController.registerImage(img);
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#clone()
	 */
	public ImageData clone() {
		JistLogger.logOutput(JistLogger.INFO,"Cloning ImageDataMipav: " + img.getImageName());
		ImageDataMipav v;
		int sz = img.getSize();
		switch (type) {
		case BYTE:
			return new ImageDataByte(this);
		case DOUBLE:
			return new ImageDataDouble(this);
		case COLOR:
			return new ImageDataColor(this);
		case INT:
			return new ImageDataInt(this);
		 			
		case LONG:
			return new ImageDataInt(this);
		
		case FLOAT:
			return new ImageDataFloat(this);
		case UBYTE:
			return new ImageDataUByte(this);
		case BOOLEAN:
			v = this.mimic();
			for (int i = 0; i < sz; i++) {
				v.img.set(i, img.getBoolean(i));
			}
			return v;
		case SHORT:
			v = this.mimic();
			for (int i = 0; i < sz; i++) {
				v.img.set(i, img.getShort(i));
			}
			return v;
		case USHORT:
			v = this.mimic();
			for (int i = 0; i < sz; i++) {
				v.img.set(i, img.getUShort(i));
			}
			return v;
		case UINT:			
		case VECTORX:
		default:
			JistLogger.logError(JistLogger.SEVERE, "Type not supported.");
			return null;
		}
	}



	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#get(int, int, int, int)
	 */
	public Number get(int i, int j, int k, int l) {		
		if (type == VoxelType.UBYTE) {
			return getUByte(i, j, k, l);
		}		
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Float.NaN;
		case 2:			
			return img.get(i, j);
		case 3: 
			return img.get(i, j, k);
		case 4: 			
		default: 			
			return img.get(i, j, k, l);
		}		
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getBoolean(int, int, int, int)
	 */
	public boolean getBoolean(int i, int j, int k, int l) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Boolean.FALSE ;
		case 2:
			return img.getBoolean(i, j);
		case 3: 
			return img.getBoolean(i, j, k);
		case 4: 			
		default: 
			return img.getBoolean(i, j, k, l);
		}		
	}



	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getColor(int, int, int, int)
	 */
	public Color getColor(int i, int j, int k, int l) {
		switch (img.getType()) {
		case ModelStorageBase.ARGB:
			int alphab = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i));
			int redb = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1);
			int greenb = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2);
			int blueb = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3);
			return new Color(redb, greenb, blueb, alphab);
		case ModelStorageBase.ARGB_USHORT:
			int alphas = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i));
			int reds = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1);
			int greens = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2);
			int blues = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3);
			return new Color(reds / 65535.0f, greens / 65535.0f, blues / 65535.0f, alphas / 65535.0f);
		case ModelStorageBase.ARGB_FLOAT:
			int alphaf = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i));
			int redf = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1);
			int greenf = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2);
			int bluef = img.getInt(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3);
			return new Color(redf, greenf, bluef, alphaf);
		default:
			return Color.BLACK;
		}
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getDouble(int, int, int, int)
	 */
	public double getDouble(int i, int j, int k, int l) {
		if (type == VoxelType.UBYTE) {
			return getUByte(i, j, k, l);
		}
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Double.NaN;
		case 2:
			return img.getDouble(i, j);
		case 3: 
			return img.getDouble(i, j, k);
		case 4: 			
		default: 
			return img.getDouble(i, j, k, l);
		}				
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getFloat(int, int, int, int)
	 */
	public float getFloat(int i, int j, int k, int l) {
		if (type == VoxelType.UBYTE) {
			return getUByte(i, j, k, l);
		}
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Float.NaN;
		case 2:
			return img.getFloat(i, j);
		case 3: 
			return img.getFloat(i, j, k);
		case 4: 			
		default: 
			return img.getFloat(i, j, k, l);
		}

	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getInt(int, int, int, int)
	 */
	public int getInt(int i, int j, int k, int l) {
		if (type == VoxelType.UBYTE) {
			return getUByte(i, j, k, l);
		}
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Integer.MAX_VALUE;
		case 2:
			return img.getInt(i, j);
		case 3: 
			return img.getInt(i, j, k);
		case 4: 			
		default: 
			return img.getInt(i, j, k, l);
		}	

	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getModelImage()
	 */
	public ModelImage getModelImageCopy() {
		JistLogger.logOutput(JistLogger.INFO,"Cloning ImageDataMipav in getModelImageCopy: " + img.getImageName());
		return (ModelImage)img.clone();
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getShort(int, int, int, int)
	 */
	public short getShort(int i, int j, int k, int l ) {
		if (type == VoxelType.UBYTE) {
			return getUByte(i, j, k, l);
		}
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Short.MAX_VALUE;
		case 2:
			return img.getShort(i, j);
		case 3: 
			return img.getShort(i, j, k);
		case 4: 			
		default: 
			return img.getShort(i, j, k, l);
		}	
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#getUByte(int, int, int, int)
	 */
	public short getUByte(int i, int j, int k, int l) {
		int val =255;
		switch(img.getExtents().length) {
		case 2:
			val=img.getInt(i, j);break;
		case 3: 
			val=img.getInt(i, j, k);break;
		case 4: 			
		default: 
			val=img.getInt(i, j, k, l);break;
		}
		if(val<0) val += 256;
		return (short)(val<0?0:val>255?255:val);
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#mimic()
	 */
	public ImageDataMipav mimic() {
		ImageDataMipav vol;
		if (components > 1) {
			vol = new ImageDataMipav(this.getName(), type, rows, cols, slices, components);
		} else {
			vol = new ImageDataMipav(this.getName(), type, rows, cols, slices);
		}
		vol.setHeader(this.getHeader());
		return vol;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#mimic(int, int, int, int)
	 */
	public ImageDataMipav mimic(int rows, int cols, int slices, int components) {
		ImageDataMipav vol;
		if (components > 1) {
			vol = new ImageDataMipav(this.getName(), type, rows, cols, slices, components);
		} else {
			vol = new ImageDataMipav(this.getName(), type, rows, cols, slices);
		}
		vol.setHeader(this.getHeader());
		return vol;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(int, int, int, int, boolean)
	 */
	public void set(int i, int j, int k, int l, boolean a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}	

	}

	/**
	 * Sets the.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * @param l the l
	 * @param a the a
	 */
	public void set(int i, int j, int k, int l, byte a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}			
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(int, int, java.awt.Color)
	 */
	public void set(int i, int j, Color c) {
		float[] cArray;
		switch (img.getType()) {
		case ModelStorageBase.ARGB:
			img.set(4 * ((j * rows) + i), (byte) c.getAlpha());
			img.set(4 * ((j * rows) + i) + 1, (byte) c.getRed());
			img.set(4 * ((j * rows) + i) + 2, (byte) c.getGreen());
			img.set(4 * ((j * rows) + i) + 3, (byte) c.getBlue());
			break;
		case ModelStorageBase.ARGB_USHORT:
			cArray = new float[4];
			c.getRGBComponents(cArray);
			img.set(4 * ((j * rows) + i), (short) Math.round(65535.0f * ((cArray[3] < 0) ? cArray[3] + 65536 : cArray[3])));
			img.set(4 * ((j * rows) + i) + 1, (short) Math.round(65535.0f * ((cArray[0] < 0) ? cArray[0] + 65536 : cArray[0])));
			img.set(4 * ((j * rows) + i) + 2, (short) Math.round(65535.0f * ((cArray[1] < 0) ? cArray[1] + 65536 : cArray[1])));
			img.set(4 * ((j * rows) + i) + 3, (short) Math.round(65535.0f * ((cArray[2] < 0) ? cArray[2] + 65536 : cArray[2])));
			break;
		case ModelStorageBase.ARGB_FLOAT:
			cArray = new float[4];
			c.getRGBComponents(cArray);
			img.set(4 * ((j * rows) + i), cArray[3]);
			img.set(4 * ((j * rows) + i) + 1, cArray[0]);
			img.set(4 * ((j * rows) + i) + 2, cArray[1]);
			img.set(4 * ((j * rows) + i) + 3, cArray[2]);
			break;
		default:
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(int, int, int, int, double)
	 */
	public void set(int i, int j, int k, int l, double a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}	
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(int, int, int, int, float)
	 */
	public void set(int i, int j, int k, int l, float a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}	
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(int, int, int, int, int)
	 */
	public void set(int i, int j, int k, int l, int a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}	
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(int, int, int, int, java.awt.Color)
	 */
	public void set(int i, int j, int k, int l, Color c) {
		float[] cArray;
		switch (img.getType()) {
		case ModelStorageBase.ARGB:
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i), (byte) c.getAlpha());
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1, (byte) c.getRed());
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2, (byte) c.getGreen());
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3, (byte) c.getBlue());
			break;
		case ModelStorageBase.ARGB_USHORT:
			cArray = new float[4];
			c.getRGBComponents(cArray);
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i), (short) Math
					.round(65535.0f * ((cArray[3] < 0) ? cArray[3] + 65536 : cArray[3])));
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1, (short) Math
					.round(65535.0f * ((cArray[0] < 0) ? cArray[0] + 65536 : cArray[0])));
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2, (short) Math
					.round(65535.0f * ((cArray[1] < 0) ? cArray[1] + 65536 : cArray[1])));
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3, (short) Math
					.round(65535.0f * ((cArray[2] < 0) ? cArray[2] + 65536 : cArray[2])));
			break;
		case ModelStorageBase.ARGB_FLOAT:
			cArray = new float[4];
			c.getRGBComponents(cArray);
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i), cArray[3]);
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 1, cArray[0]);
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 2, cArray[1]);
			img.set(4 * ((l * (rows * cols * slices)) + (k * (rows * cols)) + (j * rows) + i) + 3, cArray[2]);
			break;
		default:
		}
	}

	/**
	 * Set vector Component.
	 * 
	 * @param i the i
	 * @param j the j
	 * @param k the k
	 * @param l the l
	 * @param a the a
	 */
	//	public void set(int i, int j, int k, int l, Number x) {
	//	img.set(i, j, k, l, x);
	//	}


	public void set(int i, int j, int k, int l, short a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}	
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public void set(Voxel a) {
		if (a == null) {
			return;
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					switch(img.getExtents().length) {
					case 0: 
					case 1: return;
					case 2:
						img.set(i, j,a.floatValue());
					case 3: 
						img.set(i, j, k, a.floatValue());
					case 4: 			
					default: 
						img.set(i, j, k, 0, a.floatValue());
					}						
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#setName(java.lang.String)
	 */
	public void setName(String name) {
		super.setName(name);
		if (name != null) {
			MipavController.setModelImageName(img,name);			
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.ImageData#dispose()
	 */
	@Override
	public void dispose() {
		if(img!=null) {
			JistLogger.logOutput(JistLogger.INFO,"Disposing of ImageDataMipav: " + img.getImageName());
			ViewJFrameImage frame = img.getParentFrame();
			if(frame!=null)
				frame.close();
			//			MipavController.removeFromReigstry(img);
			img.disposeLocal();
		}		

		img=null;
	}

	protected void finalize() throws Throwable {

		try {

			dispose();

		} catch(Exception e) {

		}

		finally {	        
			super.finalize();	        	        
		}
	}

	protected ImageDataMipav() {
	}

	public ModelImage getModelImageDirect() {
		return img;
	}

	public ModelImage extractModelImage() {
		ModelImage tmp = img;
		img = null;
		return tmp;
	};

	public boolean isNotAvailable() {
		return img==null;
	}

	public void set(int i, int j, int k, int l, Number a) {
		switch(img.getExtents().length) {
		case 0: 
		case 1: return;
		case 2:
			img.set(i, j,a);
		case 3: 
			img.set(i, j, k, a);
		case 4: 			
		default: 
			img.set(i, j, k, l, a);
		}			
	}

	@Override
	public byte getByte(int i, int j, int k, int l) {		
		switch(img.getExtents().length) {
		case 0: 
		case 1: return Byte.MAX_VALUE;
		case 2:
			return img.getByte(i, j);
		case 3: 
			return img.getByte(i, j, k);
		case 4: 			
		default: 
			return img.getByte(i, j, k, l);
		}		}
}
