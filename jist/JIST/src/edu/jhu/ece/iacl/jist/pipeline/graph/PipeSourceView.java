/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import org.jgraph.graph.CellViewRenderer;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.VertexRenderer;
import org.jgraph.graph.VertexView;

/**
 * Cell view renderer for sources.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeSourceView extends VertexView {
	
	/**
	 * The Class ActivityRenderer.
	 */
	public static class ActivityRenderer extends VertexRenderer {
		
		/**
		 * Return a slightly larger preferred size than for a rectangle.
		 * 
		 * @return the preferred size
		 */
		public Dimension getPreferredSize() {
			Dimension d = super.getPreferredSize();
			d.width += d.width / 8;
			d.height += d.height / 2;
			return d;
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.VertexRenderer#paint(java.awt.Graphics)
		 */
		public void paint(Graphics g) {
			int b = borderWidth;
			Graphics2D g2 = (Graphics2D) g;
			Dimension dbox = getSize();
			int xOffset = (dbox.width - dbox.height + 2 * PipePortView.portSize) / 2;
			Dimension d = new Dimension(dbox.height - 2 * PipePortView.portSize, dbox.height - 2
					* PipePortView.portSize);
			boolean tmp = selected;
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			int center = dbox.height / 2;
			final int textSize = 20;
			g.setColor(Color.black);
			g.drawRoundRect(1 + PipePortView.portSize, center - textSize / 2 + 1, dbox.width - 2
					* PipePortView.portSize - 2, textSize - 2, textSize, textSize);
			if (selected) {
				g2.setStroke(GraphConstants.SELECTION_STROKE);
				g.setColor(highlightColor);
				g.drawRoundRect(1 + PipePortView.portSize, center - textSize / 2 + 1, dbox.width - 2
						* PipePortView.portSize - 2, textSize - 2, textSize, textSize);
			}
			if (super.isOpaque()) {
				g.setColor(super.getBackground());
				if ((gradientColor != null) && !preview) {
					g2.setPaint(new GradientPaint(0, 0, getBackground(), getWidth(), getHeight(), gradientColor, true));
				}
				g.fillOval(b - 1 + xOffset, b - 1 + PipePortView.portSize, d.width - b, d.height - b);
			}
			if (bordercolor != null) {
				g.setColor(bordercolor);
				g2.setStroke(new BasicStroke(b));
				g.drawOval(b - 1 + xOffset, b - 1 + PipePortView.portSize, d.width - b, d.height - b);
			}
			if (selected) {
				g2.setStroke(GraphConstants.SELECTION_STROKE);
				g.setColor(highlightColor);
				g.drawOval(b - 1 + xOffset, b - 1 + PipePortView.portSize, d.width - b, d.height - b);
			}
			if (super.isOpaque()) {
				g.setColor(super.getBackground());
				if ((gradientColor != null) && !preview) {
					setOpaque(false);
					g2.setPaint(new GradientPaint(0, 0, getBackground(), getWidth(), getHeight(), gradientColor, true));
				}
				g.fillRoundRect(2 + PipePortView.portSize, center - textSize / 2 + 2, dbox.width - 2
						* PipePortView.portSize - 3, textSize - 3, textSize, textSize);
			}
			try {
				setBorder(null);
				setOpaque(false);
				selected = false;
				super.paint(g);
			} finally {
				selected = tmp;
			}
		}
	}

	/** The renderer. */
	public static transient ActivityRenderer renderer = new ActivityRenderer();

	/**
	 * Instantiates a new pipe source view.
	 */
	public PipeSourceView() {
		super();
	}

	/**
	 * Instantiates a new pipe source view.
	 * 
	 * @param cell
	 *            the cell
	 */
	public PipeSourceView(Object cell) {
		super(cell);
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.VertexView#getRenderer()
	 */
	public CellViewRenderer getRenderer() {
		return renderer;
	}
}
