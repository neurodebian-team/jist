/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamWeightedVolumesInputView;
/**
 * Allow user to select more than one incoming port.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PortListBox extends ParamInputView implements ListSelectionListener, ParamViewObserver {
	
	/** The ports. */
	protected Vector<PipePort> ports;
	
	/** The port. */
	protected PipePort port;
	
	/** The listmodel. */
	protected DefaultListModel listmodel;
	
	/** The field. */
	protected JList field;
	
	/** The list pane. */
	protected JPanel listPane;
	
	/** The label. */
	protected JLabel label;
	
	/** The scroll pane. */
	protected JScrollPane scrollPane;
	
	/** The panel. */
	ParameterPanel panel;
	
	/** The weight pane. */
	protected JPanel weightPane=null;

	
	/** The text fields. */
	protected List<ParamFile> textFields;
	private void updateWeightPanel(){
		if(weightPane==null)return;
		// Remove all weight fields
		weightPane.removeAll();
		ParamWeightedVolumeCollection param=((ParamWeightedVolumeCollection)port);
		List<ParamFile> lst=textFields=param.getParameters();
		int[] selectedIndices=field.getSelectedIndices();
		while(lst.size()>selectedIndices.length){
			lst.remove(selectedIndices.length);
		}
		
		while(lst.size()<selectedIndices.length){
			param.addWeightedVolume();
		}
		JComponent comp;
		
		// Create a new weight entry field for each listbox component
		Vector<PipePort> ports=port.getIncomingPorts();
		for (int i = 0; i < listmodel.size(); i++) {
			// Create new text field from list box entry
			if(field.isSelectedIndex(i)){				
				int in=ports.indexOf(listmodel.get(i));
				ParamWeightedVolume vol = ((ParamWeightedVolume)lst.get(in));
				vol.setValue((File)null);
				ParamInputView view=vol.getWeightParameter().getInputView();
				view.addObserver(this);
				comp=view.getField();	
				comp.setPreferredSize(ParamWeightedVolumesInputView.listDimension);
			} else {
				comp=new JLabel();
				comp.setPreferredSize(new Dimension(1,ParamWeightedVolumesInputView.listDimension.height));
				comp.setBackground(field.getBackground());
				comp.setForeground(field.getForeground());
			}
			// Set preferred size
			
			weightPane.add(comp);
			
		}
		
		weightPane.updateUI();
		// Update scrollPane
		scrollPane.revalidate();
	}
	/**
	 * Constructor.
	 * 
	 * @param port
	 *            associate port
	 * @param ports
	 *            compatible ports
	 */
	public PortListBox(PipePort port, Vector<PipePort> ports) {
		super((ParamModel)port);
		this.setLayout(new BorderLayout());
		field = new JList();
		this.panel = ParameterPanel.getInstance();
		label = new JLabel(port.getLabel());
		field.setModel(listmodel = new DefaultListModel());
		field.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(100, 100));
		
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		field.setCellRenderer(new ConnectorRenderer(port));
		// Create list pane to layout listbox entries
		listPane = new JPanel(new BorderLayout());
		listPane.add(field, BorderLayout.CENTER);
		// Create pane to layout list pane
		JPanel smallPane = new JPanel(new BorderLayout());
		smallPane.add(listPane, BorderLayout.NORTH);
		smallPane.setMinimumSize(new Dimension(100, 100));
		scrollPane.setViewportView(smallPane);
		this.setLayout(new BorderLayout());
		add(label, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		this.ports = ports;
		this.port = port;
		for (PipePort p : ports) {
			listmodel.addElement(p);
		}
		Vector<PipePort> selectedPorts = port.getIncomingPorts();
		int[] selected = new int[selectedPorts.size()];
		int index, i = 0;
		for (PipePort p : selectedPorts) {
			index = ports.indexOf(p);
			if (index >= 0) {
				selected[i++] = index;
			}
		}
		this.setPreferredSize(new Dimension(150, 100));
		field.setSelectedIndices(selected);
		field.addListSelectionListener(this);
		if(port instanceof ParamWeightedVolumeCollection){
			weightPane = new JPanel();
			weightPane.setLayout(new BoxLayout(weightPane, BoxLayout.Y_AXIS));
			weightPane.setBackground(field.getBackground());
			weightPane.setForeground(field.getForeground());
			listPane.add(weightPane, BorderLayout.EAST);
		}
		valueChanged(null);
	}

	/**
	 * Action performed.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void actionPerformed(ActionEvent evt) {
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent evt) {
		int[] indices = field.getSelectedIndices();
		Vector<PipePort> currentlyConnected = new Vector<PipePort>();
		panel.setIgnoreRefresh(true);
		for (PipePort p : ports) {
			if (p.isConnectedTo(port)) {
				currentlyConnected.add(p);
			}
		}
		PipePort tmp;
		for (int index : indices) {
			tmp = ports.get(index);
			if (!tmp.isConnectedTo(port)) {
				PipeConnector.connect(tmp, port);
			}
			currentlyConnected.remove(tmp);
		}
		for (PipePort p : currentlyConnected) {
			p.disconnect(port);
		}
		int i = 0;
		field.removeListSelectionListener(this);
		field.clearSelection();
		field.setSelectedIndices(indices);
		field.addListSelectionListener(this);
		updateWeightPanel();
		panel.setIgnoreRefresh(false);
	}
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel, edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	public void update(ParamModel model, ParamInputView view) {
		notifyObservers(model, view);
	}
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView#commit()
	 */
	public void commit() {
		if(textFields!=null){
			for(ParamFile f:textFields){
				f.getInputView().commit();
				if(f instanceof ParamWeightedVolume){
					((ParamWeightedVolume)f).getWeightParameter().getInputView().commit();
				}
			}
		}
	}
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView#getField()
	 */
	public JComponent getField() {
		// TODO Auto-generated method stub
		return weightPane;
	}
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView#update()
	 */
	public void update() {
		if(textFields!=null){
			for(ParamFile f:textFields){
				f.getInputView().update();
			}
		}
	}
}
