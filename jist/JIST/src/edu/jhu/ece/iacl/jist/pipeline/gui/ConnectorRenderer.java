/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamWeightedVolumesInputView;

/**
 * Rendering class for connector with index.
 * 
 * @author Blake Lucas
 */
public class ConnectorRenderer implements ListCellRenderer, ChangeListener {
	
	/** The index selection. */
	JSpinner indexSelection;
	
	/** The dest port. */
	PipePort destPort;

	/**
	 * Constructor.
	 * 
	 * @param port
	 *            index owner
	 */
	public ConnectorRenderer(PipePort port) {
		this.destPort = port;
		indexSelection = new JSpinner(new SpinnerNumberModel(0, 0, 1000000, 1));
		indexSelection.setPreferredSize(new Dimension(45, 20));
		indexSelection.addChangeListener(this);
	}

	/**
	 * Render index as spinner box.
	 * 
	 * @param list
	 *            the list
	 * @param value
	 *            the value
	 * @param index
	 *            the index
	 * @param isSelected
	 *            the is selected
	 * @param cellHasFocus
	 *            the cell has focus
	 * @return the list cell renderer component
	 */
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		// Get the selected index. (The index parameter isn't
		// always valid, so just use the value.)
		JLabel listLabel = new JLabel();
		JPanel pane = new JPanel(new BorderLayout());
		int in=-1;
		if (value instanceof PipePort) {
			PipePort port = (PipePort) value;
			if(isSelected){
				Vector<PipePort> ports=destPort.getIncomingPorts();
				if(ports.size()>1){
					in=ports.indexOf(port);
					pane.add(new JLabel(((in<0)?"":" ["+(in+1)+"]")),BorderLayout.EAST);
				}
			}
			listLabel.setText(port.getOwner().getLabel() + "::" + port.getLabel());

			if (value instanceof ObjectCollection) {
				PipeConnector wire = port.getOutgoingConnector(destPort);
				if ((wire != null) && (wire.getSourceIndex() != -1)) {
					listLabel.setText(port.getOwner().getLabel() + "::" + port.getLabel() + " ("
							+ wire.getSourceIndex()+")");
				}
			}
		} else {
			listLabel.setHorizontalAlignment(JLabel.RIGHT);
			listLabel.setText(value.toString());
		}
		pane.add(listLabel,BorderLayout.CENTER);
		
		if (isSelected) {
			pane.setBackground(list.getSelectionBackground());
			pane.setForeground(list.getSelectionForeground());
		} else {
			pane.setBackground(list.getBackground());
			pane.setForeground(list.getForeground());
		}
		pane.setMinimumSize(ParamWeightedVolumesInputView.listDimension);
		pane.setPreferredSize(ParamWeightedVolumesInputView.listDimension);
		return pane;
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
	}
}
