package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;

public class PipeDoubleCollectionExternalSource extends PipeExternalSource{
	
	public PipeDoubleCollectionExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamNumberCollection("Default"));
		group.setLabel("External Float Collection");
		group.setName("extfloats");
		group.setCategory("Externalize.Number.Float");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float Collection");
		group.add(valParam = new ParamNumberCollection("Float Collection"));
		return group;
	}
}
