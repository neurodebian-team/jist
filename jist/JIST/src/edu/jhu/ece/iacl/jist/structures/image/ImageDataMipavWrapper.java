package edu.jhu.ece.iacl.jist.structures.image;

import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.structures.ModelImage;

public class ImageDataMipavWrapper extends ImageDataMipav {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4829242269116830395L;

	public ImageDataMipavWrapper(ModelImage vol) {		
		super();
		JistLogger.logOutput(JistLogger.INFO,"Wrapping Model Image: " + vol.getImageName());
		ImageDataMipavPrivate(vol);
	}

}
