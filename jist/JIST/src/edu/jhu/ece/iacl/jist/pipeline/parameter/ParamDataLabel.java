/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamDataLabelFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Data Label Parameter.
 * 
 * @author Blake Lucas
 */
public class ParamDataLabel extends ParamVolume {
	// Do not serialize data labels
	/** The labels. */
	protected transient DataLabelCollection labels;

	/** The label file. */
	protected File labelFile = null;

	public boolean xmlEncodeParam(Document document, Element parent) {
		super.xmlEncodeParam(document, parent);		 
		Element em;				

		em = document.createElement("labelFile");				 
		em.appendChild(document.createTextNode(labelFile+""));
		parent.appendChild(em);
		return true;
	}
	
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		
		labelFile = new File(JistXMLUtil.xmlReadTag(parent, "labelFile"));
		
	}

	
	/**
	 * Default constructor.
	 */
	public ParamDataLabel() {
		super();
		this.factory = new ParamDataLabelFactory(this);
	}

	/**
	 * Construct data labels parameter with no restrictions.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamDataLabel(String name) {
		super(name);
		this.factory = new ParamDataLabelFactory(this);
	}

	/**
	 * Construct data label parameter with restriction on voxel type.
	 * 
	 * @param name
	 *            parameter name
	 * @param type
	 *            voxel type restriction
	 */
	public ParamDataLabel(String name, VoxelType type) {
		super(name, type);
		this.factory = new ParamDataLabelFactory(this);
	}

	/**
	 * Construct new data label the specified restrictions.
	 * 
	 * @param name
	 *            parameter name
	 * @param type
	 *            voxel type restriction
	 * @param rows
	 *            rows restriction
	 * @param cols
	 *            columns restriction
	 * @param slices
	 *            slices restriction
	 * @param components
	 *            components restriction
	 */
	public ParamDataLabel(String name, VoxelType type, int rows, int cols, int slices, int components) {
		super(name, type, rows, cols, slices, components);
		this.factory = new ParamDataLabelFactory(this);
	}

	/**
	 * Default constructor.
	 * 
	 * @param type
	 *            the type
	 */
	public ParamDataLabel(VoxelType type) {
		super(type);
		this.factory = new ParamDataLabelFactory(this);
	}

	/**
	 * Default constructor.
	 * 
	 * @param type
	 *            voxel type
	 * @param rows
	 *            rows
	 * @param cols
	 *            columns
	 * @param slices
	 *            slices
	 * @param components
	 *            components
	 */
	public ParamDataLabel(VoxelType type, int rows, int cols, int slices, int components) {
		super(type, rows, cols, slices, components);
	}

	/**
	 * Add data label to collection.
	 * 
	 * @param label
	 *            the label
	 */
	public void add(DataLabel label) {
		labels.add(label);
	}

	/**
	 * Clone this object.
	 * 
	 * @return the param data label
	 */
	public ParamDataLabel clone() {
		ParamDataLabel param = new ParamDataLabel();
		param.rows = rows;
		param.slices = slices;
		param.components = components;
		param.cols = cols;
		param.voxelType = voxelType;
		param.file = this.file;
		param.uri = this.uri;
		param.readerWriter = this.readerWriter;
		param.extensionFilter = this.extensionFilter;
		param.labelFile = this.labelFile;
		param.labels = (DataLabelCollection) this.labels.clone();
		param.setName(this.getName());
		param.label=this.label;
		param.setValue(this.getValue());
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Create default labels based on volume values.
	 */
	public void createDefaultLabels() {
		ImageData vol = getImageData();
		if (vol == null) {
			return;
		}
		int rows = vol.getRows();
		int cols = vol.getCols();
		int slices = vol.getSlices();
		if (labels == null) {
			labels = new DataLabelCollection();
		} else {
			labels.clear();
		}
		labels.setFile(getValue());
		DataLabel tmp = new DataLabel(0);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					if (labels.size() >= DataLabel.MAX_LABELS) {
						i = rows;
						j = cols;
						k = slices;
						break;
					}
					float val = vol.getFloat(i, j, k);
					tmp.setId((int) val);
					if (!labels.contains(tmp)) {
						labels.add(new DataLabel(val));
					}
				}
			}
		}
		Collections.sort(labels);
	}

	/**
	 * Get label collection.
	 * 
	 * @return label collection
	 */
	public DataLabelCollection getDataLabels() {
		return labels;
	}

	/**
	 * Get location of label file.
	 * 
	 * @return label file
	 */
	public File getLabelFile() {
		return labelFile;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(1);
		connectible = true;
		factory = new ParamDataLabelFactory(this);
	}

	/**
	 * Open label file and load it into parameter.
	 */
	public void openLabelFile() {
		openLabelFile(getLabelFile());
	}

	/**
	 * Open the specified label file and load it into the parameter.
	 * 
	 * @param file
	 *            the file
	 */
	public void openLabelFile(File file) {
		labels = DataLabelCollection.read(file);
		if (labels != null) {
			setValue(labels.getFile());
			setLabelFile(file);
		}
	}

	/**
	 * Set the label file location for this parameter.
	 * 
	 * @param labelFile
	 *            the label file
	 */
	public void setLabelFile(File labelFile) {
		this.labelFile = labelFile;
	}

	/**
	 * Set volume.
	 * 
	 * @param value
	 *            the value
	 */
	public void setValue(ImageData value) {
		super.setValue(value);
		updateLabels();
	}

	/**
	 * The set value method accepts all value types inherited from ParamVolume
	 * plus DataLabelCollection. A volume parameter will accept a CubicVolume,
	 * the string name for the image, the file location of the image or a
	 * ModelImage.
	 * 
	 * @param value
	 *            parameter value
	 */
	public void setValue(DataLabelCollection value) {
		this.labels = value;
	}

	/**
	 * Set file location.
	 * 
	 * @param value
	 *            the value
	 */
	public void setValue(File value) {
		super.setValue(value);
		updateLabels();
	}

	/**
	 * set model image.
	 * 
	 * @param value
	 *            the value
	 *//*
	public void setValue(ModelImage value) {
		super.setValue(value);
		updateLabels();
	}*/

	/**
	 * Set file location.
	 * 
	 * @param value
	 *            the value
	 */
	public void setValue(String value) {
		super.setValue(value);
		updateLabels();
	}

	/**
	 * Get description.
	 * 
	 * @return the string
	 */
	public String toString() {
		DataLabelCollection labels = getDataLabels();
		return ((labels != null) && (labels.getFile() != null)) ? ("<HTML>" + labels.getFile().getName()
				+ "<BR>LABELS=" + labels.size() + "</HTML>") : "Unknown";
	}

	/**
	 * Update labels and create them as needed.
	 */
	private void updateLabels() {
		if ((labels == null) && (getValue() != null)) {
			createDefaultLabels();
		}
		if ((labels != null) && (labels.getFile() != null) && !labels.getFile().equals(getValue())) {
			createDefaultLabels();
		}
	}

	/**
	 * Validate that the data labels exist.
	 * 
	 * @throws InvalidParameterException
	 *             parameter does not meet value restriction
	 */
	public void validate() throws InvalidParameterException {
		super.validate();
		if (getDataLabels() == null) {
			throw new InvalidParameterException(this, "Data Labels Invalid");
		}
	}
}
