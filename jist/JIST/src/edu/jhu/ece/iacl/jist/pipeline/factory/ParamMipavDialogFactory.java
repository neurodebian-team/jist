/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMipavDialog;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamMipavDialogInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.parameters.ParameterException;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.view.dialogs.AlgorithmParameters;

/**
 * Mipav Dialog Parameter Factory.
 * 
 * @author Blake Lucas
 */
public class ParamMipavDialogFactory extends ParamFactory {
	
	/** The param. */
	private ParamMipavDialog param;

	/**
	 * Instantiates a new param mipav dialog factory.
	 */
	public ParamMipavDialogFactory() {
	}

	/**
	 * Construct factory for specified parameter.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamMipavDialogFactory(ParamMipavDialog param) {
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#createMipavParameter(gov.nih.mipav.view.dialogs.AlgorithmParameters)
	 */
	public void createMipavParameter(AlgorithmParameters scriptParams) throws ParserException {
	}

	/**
	 * Get parameter input view.
	 * 
	 * @return input view
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamMipavDialogInputView(param);
		}
		return inputView;
	}

	/**
	 * Get parameter output view.
	 * 
	 * @return output view
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamOutputView(param);
		}
		return outputView;
	}

	/**
	 * Get factory's parameter.
	 * 
	 * @return file parameter
	 */
	public ParamMipavDialog getParameter() {
		return param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#importMipavParameter(gov.nih.mipav.model.scripting.parameters.ParameterTable)
	 */
	public void importMipavParameter(ParameterTable paramTable) throws ParameterException {
		// TODO Auto-generated method stub
	}
}
