/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import javax.swing.JPanel;

import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;

// TODO: Auto-generated Javadoc
/**
 * Port that allows a Pipe Source to be a child of another Pipe Source.
 * 
 * @author Blake Lucas
 */
public class PipeChildPort extends PipePort<Object> {
	
	/**
	 * Port is always initialized as an input port.
	 */
	public PipeChildPort() {
		super();
		setPortType(PipePort.type.INPUT);
	}

	/**
	 * Clone port.
	 * 
	 * @return the pipe child port
	 */
	public PipeChildPort clone() {
		return new PipeChildPort();
	}

	/**
	 * Always returns "Child".
	 * 
	 * @return the label
	 */
	public String getLabel() {
		return "Child";
	}

	/**
	 * Always returns "child".
	 * 
	 * @return the name
	 */
	public String getName() {
		return "child";
	}

	/**
	 * Get source module that owns this port.
	 * 
	 * @return the owner
	 */
	public PipeSource getOwner() {
		return (PipeSource) owner;
	}

	/**
	 * Unimplemented.
	 * 
	 * @return the value
	 */
	public Object getValue() {
		return null;
	}

	/**
	 * Unimplemented.
	 * 
	 * @return the view
	 */
	public JPanel getView() {
		return null;
	}

	/**
	 * Initialize child port as always an input port.
	 */
	public void init() {
		setPortType(PipePort.type.INPUT);
	}

	/**
	 * Only allow child ports to connect to parent ports.
	 * 
	 * @param p the port
	 * 
	 * @return true, if checks if is compatible
	 */
	public boolean isCompatible(PipePort p) {
		return (p instanceof PipeParentPort);
	}

	/**
	 * Unimplemented.
	 * 
	 * @param label the label
	 */
	public void setLabel(String label) {
	}

	/**
	 * Unimplemented.
	 * 
	 * @param name the name
	 */
	public void setName(String name) {
	}

	/**
	 * Unimplemented.
	 * 
	 * @param value the value
	 */
	public void setValue(Object value) {
	}

	/**
	 * Valid if connected.
	 * 
	 * @throws InvalidParameterException the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if (!isConnected()) {
			throw new InvalidParameterException(this);
		}
	}
}
