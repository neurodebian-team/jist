/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.ToolTipManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


/**
 * Input view to select multiple images.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeURICollectionInputView extends ParamInputView implements ListSelectionListener, ActionListener,
		ListCellRenderer, Refreshable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4483721450186968746L;
	
	/** The add button. */
	private JButton addButton;
	
	/** The remove button. */
	private JButton removeButton;
	
	/** The up button. */
	private JButton upButton;
	
	/** The down button. */
	private JButton downButton;
	
	/** The open button. */
	private JButton openButton;
	
	/** The move button. */
	private JButton moveButton;
	
	/** The image list size. */
	protected int imageListSize = -1;
	
	/** The file field. */
	protected JList fileField;
	
	/** The list box entries. */
	protected DefaultListModel fileListBoxEntries;
	
	/** The list pane. */
	protected JPanel fileListPane;
	
	/** The scroll pane. */
	protected JScrollPane fileScrollPane;


	/** The file field. */
	protected JList imageField;
	
	/** The list box entries. */
	protected DefaultListModel imageListBoxEntries;
	
	/** The list pane. */
	protected JPanel imageListPane;
	
	/** The scroll pane. */
	protected JScrollPane imageScrollPane;

	/**
	 * Default constructor.
	 * 
	 * @param param
	 *            parameters
	 */
	public ParamVolumeURICollectionInputView(ParamVolumeCollection param) {
		super(param);
		
		BorderLayout layout = new BorderLayout();
		layout.setHgap(10);
		layout.setVgap(5);
		setLayout(layout);
		
		// Create list of entries that contain ParamFile
		fileListBoxEntries = new DefaultListModel();
		// Create listbox entry field
		fileField = new JList(fileListBoxEntries);
		// Use custom entry renderer
		fileField.setCellRenderer(this);
		fileField.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// Create browse button to select images
		addButton = new JButton("Add");
		addButton.addActionListener(this);
		upButton = new JButton("Up");
		upButton.addActionListener(this);
		downButton = new JButton("Down");
		downButton.addActionListener(this);
		removeButton = new JButton("Remove");
		removeButton.addActionListener(this);
		// Create scroll pane to display entries
		fileScrollPane = new JScrollPane();
		fileScrollPane.setMinimumSize(new Dimension(100, 30));
		fileScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		fileScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// Create list pane to layout listbox entries
		fileListPane = new JPanel(new BorderLayout());
		fileListPane.add(fileField, BorderLayout.CENTER);
		// Create pane to layout list pane
		JPanel fileSmallPane = new JPanel(new BorderLayout());
		fileSmallPane.add(fileListPane, BorderLayout.NORTH);
		fileScrollPane.setViewportView(fileSmallPane);
		// Create list box
		// Create pane to layout scroll pane and browse button
		fileSmallPane = new JPanel(new BorderLayout(5, 5));
		ToolTipManager.sharedInstance().registerComponent(this);
		ToolTipManager.sharedInstance().setInitialDelay(100);
		JLabel label=new JLabel(param.getLabel());
		String desc=param.getDescription();
		if(desc!=null)this.setToolTipText(desc);
		fileSmallPane.add(label, BorderLayout.NORTH);
		fileSmallPane.add(fileScrollPane, BorderLayout.CENTER);
		JPanel fileOptButtons = new JPanel(new GridLayout(0, 2));
		fileOptButtons.add(addButton);
		fileOptButtons.add(removeButton);
		fileOptButtons.add(upButton);
		fileOptButtons.add(downButton);
		fileSmallPane.add(fileOptButtons, BorderLayout.SOUTH);
		fileSmallPane.setPreferredSize(new Dimension(200, 150));

		
		// Create list of entries that contain ParamVolume
		imageListBoxEntries = new DefaultListModel();
		// Create listbox entry field
		imageField = new JList(imageListBoxEntries);
		// Use custom entry renderer
		imageField.setCellRenderer(new ImageCellRenderer());
		imageField.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// Create browse button to select images
		openButton = new JButton("Open");
		openButton.addActionListener(this);
		moveButton = new JButton("<HTML>Select &rsaquo;&rsaquo;&rsaquo;</HTML>");
		moveButton.addActionListener(this);
		// Create scroll pane to display entries
		imageScrollPane = new JScrollPane();
		imageScrollPane.setMinimumSize(new Dimension(100, 30));
		imageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		imageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// Create list pane to layout listbox entries
		imageListPane = new JPanel(new BorderLayout());
		imageListPane.add(imageField, BorderLayout.CENTER);
		// Create pane to layout list pane
		JPanel imageSmallPane = new JPanel(new BorderLayout());
		imageSmallPane.add(imageListPane, BorderLayout.NORTH);
		imageScrollPane.setViewportView(imageSmallPane);
		// Create list box
		// setSelected(param.getFiles());
		// Create pane to layout scroll pane and browse button
		imageSmallPane = new JPanel(new BorderLayout(5, 5));
		imageSmallPane.add(new JLabel("Image Registry"), BorderLayout.NORTH);
		imageSmallPane.add(imageScrollPane, BorderLayout.CENTER);
		imageSmallPane.setPreferredSize(new Dimension(200, 150));
		JPanel imageOptButtons = new JPanel(new GridLayout(0, 2));
		imageOptButtons.add(openButton);
		imageOptButtons.add(moveButton);
		JLabel spacer=new JLabel("");
		spacer.setPreferredSize(openButton.getPreferredSize());
		
		imageOptButtons.add(spacer);
		imageOptButtons.add(spacer);
		imageSmallPane.add(imageOptButtons, BorderLayout.SOUTH);

		
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setOneTouchExpandable(true);
		splitPane.setResizeWeight(0.5);
		splitPane.setLeftComponent(imageSmallPane);
		splitPane.setRightComponent(fileSmallPane);
		add(splitPane, BorderLayout.CENTER);
		setMinimumSize(new Dimension(100, 150));
		// Create refresher to monitor for MIPAV changes
		Refresher.getInstance().add(this);
	}
	private class ImageCellRenderer implements ListCellRenderer{
		public ImageCellRenderer(){
			
		}
		/**
		 * Use a custom cell renderer that can interpret ParamVolumes.
		 * 
		 * @param list
		 *            listbox
		 * @param value
		 *            listbox entry
		 * @param index
		 *            selected index
		 * @param isSelected
		 *            is selected
		 * @param cellHasFocus
		 *            has focus
		 * @return the list cell renderer component
		 */
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			
			
			// Get the selected index. (The index parameter isn't
			// always valid, so just use the value.)
			ParamVolume param = (ParamVolume) value;
			JPanel pane = new JPanel();
			ImageData img=param.getImageData(false); //do not load if not loaded!
			JLabel listLabel;
			// Updated 10/14/2009
			if(img==null)
				listLabel = new JLabel("<html><font color='#990000'>"+param.getName()+"</font></html>");
			else
				listLabel = new JLabel("<html><font color='#990000'>"+img.getName()+"</font> ["+img.getType().toString().toLowerCase()+" | ("+img.getRows()+", "+img.getCols()+", "+img.getSlices()+", "+img.getComponents()+")]</html>");
			pane.setLayout(new BorderLayout());
			if (isSelected) {
				pane.setBackground(list.getSelectionBackground());
				pane.setForeground(list.getSelectionForeground());
			} else {
				pane.setBackground(list.getBackground());
				pane.setForeground(list.getForeground());
			}
			listLabel.setHorizontalAlignment(JLabel.LEFT);
			pane.add(listLabel, BorderLayout.CENTER);
			return pane;
		}
	}
	/**
	 * Select files to load when the browse button is clicked.
	 * 
	 * @param event
	 *            browse button clicked
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == openButton) {
			ArrayList<ImageData> imgs = MipavController.openFileDialog(null);
			updateImageListBox();
		} else if (event.getSource() == addButton) {
			File[] files = openFileChooser();
			ParamVolumeCollection p=getParameter();
			if (files != null) {
				for (File f : files) {
					fileListBoxEntries.addElement(p.create(f));
				}
			}
			commit();
		} else if (event.getSource() == removeButton) {
			Object[] selected = fileField.getSelectedValues();
			for (Object obj : selected) {
				fileListBoxEntries.removeElement(obj);
			}
			commit();
		} else if (event.getSource() == upButton) {
			int index1 = fileField.getSelectedIndex();
			int index2 = (index1 - 1 + fileListBoxEntries.size()) % fileListBoxEntries.size();
			Object obj1 = fileListBoxEntries.elementAt(index1);
			Object obj2 = fileListBoxEntries.elementAt(index2);
			fileListBoxEntries.setElementAt(obj1, index2);
			fileListBoxEntries.setElementAt(obj2, index1);
			fileField.setSelectedIndex(index2);
			commit();
		} else if (event.getSource() == downButton) {
			int index1 = fileField.getSelectedIndex();
			int index2 = (index1 + 1) % fileListBoxEntries.size();
			Object obj1 = fileListBoxEntries.elementAt(index1);
			Object obj2 = fileListBoxEntries.elementAt(index2);
			fileListBoxEntries.setElementAt(obj1, index2);
			fileListBoxEntries.setElementAt(obj2, index1);
			fileField.setSelectedIndex(index2);
			commit();
		} else if(event.getSource()==moveButton){
			for(Object obj:imageField.getSelectedValues()){
				fileListBoxEntries.addElement(((ParamVolume)obj).clone());
			}
			commit();
		}
	}
	/**
	 * Get index of image name in list box entries.
	 * 
	 * @param name
	 *            image name
	 * @return list box index
	 */
	protected int getIndexOf(String name) {
		int index = -1;
		for (int j = 0; j < imageListBoxEntries.size(); j++) {
			if (((ParamVolume) imageListBoxEntries.get(j)).equalVolume(name)) {
				index = j;
				break;
			}
		}
		return index;
	}
	/**
	 * Build list box to select multiple images.
	 * 
	 * @param items
	 *            the items
	 * @return true, if update list box
	 */
	protected boolean updateImageListBox() {
		Vector<String> imageNames = MipavController.getImageNames();
		// Do not process events while list box is being updated
		imageField.removeListSelectionListener(this);
		ParamVolumeCollection p = getParameter();
		// Add images to list box if the list has changed
		if (imageNames.size() != imageListSize) {
			for (int i = 0; i < imageListBoxEntries.size(); i++) {
				imageListBoxEntries.removeElementAt(i--);
			}
			for (String name : imageNames) {
				imageListBoxEntries.addElement(p.create(name));
			}
			imageListSize = imageListBoxEntries.getSize();//imageNames.size();
		} else {
			int indices[]=imageField.getSelectedIndices();
			imageField.clearSelection();
			imageField.setSelectedIndices(indices);
			imageField.addListSelectionListener(this);
			return false;
		}
		imageField.addListSelectionListener(this);
		return true;
	}
	/**
	 * Refresh view by rebuilding list box.
	 */
	public void refresh() {
		updateImageListBox();
	}
	/**
	 * Commit changes to this parameter view.
	 */
	public void commit() {
		updateParameter();
	}

	/**
	 * Use a custom cell renderer that can interpret ParamFiles.
	 * 
	 * @param list
	 *            list box
	 * @param value
	 *            list box entry
	 * @param index
	 *            selected index
	 * @param isSelected
	 *            is selected
	 * @param cellHasFocus
	 *            has focus
	 * @return the list cell renderer component
	 */
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		// Get the selected index. (The index parameter isn't
		// always valid, so just use the value.)
		File f = (value instanceof ParamFile) ? ((ParamFile) value).getValue() : (File) value;
		if(f==null&&value instanceof ParamVolume){
//			ModelImage img=((ParamVolume)value).getModelImageCache();
			f=FileReaderWriter.getFullFileName(((ParamVolume)value).getImageData());
		}
		if(f==null)return new JPanel();
		JLabel listLabel = new JLabel((f!=null)?f.getName():"null");
		JPanel pane = new JPanel(new BorderLayout());
		if (isSelected) {
			pane.setBackground(list.getSelectionBackground());
			pane.setForeground(list.getSelectionForeground());
		} else {
			pane.setBackground(list.getBackground());
			pane.setForeground(list.getForeground());
		}
		pane.add(listLabel,BorderLayout.CENTER);
		return pane;
	}

	/**
	 * Get file parameter.
	 * 
	 * @return the parameter
	 */
	public ParamVolumeCollection getParameter() {
		return (ParamVolumeCollection) param;
	}

	/**
	 * Open file chooser to select file with specific extension.
	 * 
	 * @return absolute path of the file
	 */
	private File[] openFileChooser() {
		JFileChooser openDialog = new JFileChooser();
		FileExtensionFilter filter = (getParameter().getExtensionFilter() != null) ? getParameter()
				.getExtensionFilter() : null;
		if (filter != null) {
			openDialog.setFileFilter(filter);
		}
		openDialog.setSelectedFile(MipavController.getDefaultWorkingDirectory());
		openDialog.setMultiSelectionEnabled(true);
		openDialog.setDialogTitle("Select File");
		openDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		openDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		int returnVal = openDialog.showOpenDialog(this);
		File files[] = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			files = openDialog.getSelectedFiles();
			return files;
		} else {
			return null;
		}
	}

	/**
	 * Update pane with new value from parameter.
	 */
	public void update() {
		ParamVolumeCollection p = getParameter();
		// Get current volumes in volume collection
		List<ParamFile> vols = p.getParameters();
		fileListBoxEntries.clear();
		for (ParamFile vol : vols) {
			fileListBoxEntries.addElement(vol);
		}
	}

	/**
	 * Update parameter value with selected items from list box.
	 */
	protected void updateParameter() {
		ParamFileCollection p = getParameter();
		p.clear();
		Enumeration en = fileListBoxEntries.elements();
		while (en.hasMoreElements()) {
			Object obj = en.nextElement();
			if (obj instanceof ParamFile) {
				p.add(((ParamFile) obj).getValue());
			} else {
				p.add((File) obj);
			}
		}
		notifyObservers(p, this);
	}

	/**
	 * Update parameter when the list box selection changes.
	 * 
	 * @param event
	 *            selection changed
	 */
	public void valueChanged(ListSelectionEvent event) {
		updateParameter();
	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return fileField;
	}

}
