/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of files.
 * 
 * @author Blake Lucas
 */
public class PipeFileCollectionSource extends PipeSource {
	
	/** The filecollection. */
	protected ParamFileCollection filecollection;
	
	/** The file param. */
	protected ParamFile fileParam;
	
	/** The selection. */
	transient protected List<File> selection;
	
	/** The index. */
	transient protected int index;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("filecollection");
//		filecollection.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		em = document.createElement("fileParam");
//		fileParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);			
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		filecollection = (ParamFileCollection)inputParams.getFirstChildByName("Files");
		fileParam = (ParamFile)outputParams.getFirstChildByName("File");
//		filecollection = new ParamFileCollection();
//		filecollection.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"filecollection"));
//		fileParam = new ParamFile();
//		fileParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileParam"));
		getParentPort().setParameter(fileParam);
	}
	
	
	/**
	 * Default constructor.
	 */
	public PipeFileCollectionSource() {
		super();
		getParentPort().setParameter(fileParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File Collection");
		group.setName("filecollection");
		group.setCategory("File");
		group.add(filecollection = new ParamFileCollection("Files"));
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File");
		group.add(fileParam = new ParamFile("File"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamFile getOutputParam() {
		return fileParam;
	}

	/**
	 * Returns true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || (index < selection.size()));
	}

	/**
	 * Iterate through files.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (index < selection.size()) {
					fileParam.setValue(selection.get(index++));
					push();
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			reset();
			isReset = true;
			return false;
		}
	}

	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		selection = filecollection.getValue();
		index = 0;
		if (selection.size() > 0) {
			fileParam.setValue(selection.get(index++));
		} else {
			index++;
		}
		push();
	}
}
