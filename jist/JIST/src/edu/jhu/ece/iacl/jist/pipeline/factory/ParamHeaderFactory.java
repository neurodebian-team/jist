package edu.jhu.ece.iacl.jist.pipeline.factory;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHeader;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHidden;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;

public class ParamHeaderFactory extends ParamHiddenFactory{

	public ParamHeaderFactory(ParamHidden param) {
		super(param);
	}
	public boolean importParameter(ParamModel foreign) {
		if(super.importParameter(foreign)){
			((ParamHeader)this.getParameter()).setUUID(((ParamHeader)foreign).getUUID());
			return true;
		} else return false;
	}
}
