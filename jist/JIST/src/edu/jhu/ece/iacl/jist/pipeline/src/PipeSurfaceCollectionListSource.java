package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

public class PipeSurfaceCollectionListSource extends PipeListSource {

	/** The volume collection param. */
	protected ParamSurfaceCollection surfcolParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("surfcolParam");
//		surfcolParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		surfcolParam = (ParamSurfaceCollection)outputParams.getFirstChildByName("Surface Collection");
//		surfcolParam = new ParamSurfaceCollection();
//		surfcolParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"surfcolParam"));
		getParentPort().setParameter(surfcolParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipeSurfaceCollectionListSource() {
		super();
		getParentPort().setParameter(surfcolParam);
	}
	
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Surface Collection List");
		group.setCategory("Surface");
		group.setName("surfacecollectionlist");
		return group;
	}
	
	@Override
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Surface Collection List");
		group.add(surfcolParam = new ParamSurfaceCollection("Surface Collection"));
		return group;
	}

	@Override
	public ParamSurfaceCollection getOutputParam() {
		return surfcolParam;
	}
	
	@Override
	protected ArrayList<File> getValue(int i) {
		if (off < data[i].length) {
			ArrayList<File> surflist = new ArrayList<File>();
			for(int j=0; j<data[i].length; j++){
				surflist.add(new File(data[i][j].toString().trim()));
			}
			return surflist;
		} else {
			return null;
		}
	}

	@Override
	protected void setValue(Object obj) {
		surfcolParam.setValue((List<File>)obj);
	}

}
