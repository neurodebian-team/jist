package edu.jhu.ece.iacl.jist.structures.geom;

import edu.jhu.ece.iacl.jist.structures.data.Indexable;

// TODO: Auto-generated Javadoc
/**
 * The Class EdgeIndexed.
 */
public class EdgeIndexed implements Indexable<Double>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The edge id. */
	private int edgeId;
	
	/** The v1. */
	private int v1;
	
	/** The v2. */
	private int v2;
	
	/** The chain. */
	private int chain;
	
	/** The index. */
	private int index;
	
	/** The value. */
	private double value;
	
	/**
	 * Instantiates a new edge indexed.
	 * 
	 * @param value the value
	 */
	public EdgeIndexed(double value){
		this.value=value;
	}
	
	/**
	 * Sets the position.
	 * 
	 * @param edgeId the edge id
	 * @param v1 the v1
	 * @param v2 the v2
	 */
	public void setPosition(int edgeId,int v1,int v2){
		this.edgeId=edgeId;
		this.v1=v1;
		this.v2=v2;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setIndex(int)
	 */
	public void setIndex(int index){
		this.index=index;
	}
	
	/**
	 * Sets the value.
	 * 
	 * @param val the new value
	 */
	public void setValue(double val){
		this.value=val;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getValue()
	 */
	public Double getValue(){
		return value;
	}
	
	/**
	 * Gets the v1.
	 * 
	 * @return the v1
	 */
	public int getV1(){
		return v1;
	}
	
	/**
	 * Gets the v2.
	 * 
	 * @return the v2
	 */
	public int getV2(){
		return v2;
	}
	
	/**
	 * Gets the edge index.
	 * 
	 * @return the edge index
	 */
	public int getEdgeIndex(){
		return edgeId;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getRow()
	 */
	public int getRow(){return edgeId;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getColumn()
	 */
	public int getColumn(){return 0;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getSlice()
	 */
	public int getSlice(){return 0;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getIndex()
	 */
	public int getIndex(){return index;}



	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#getChainIndex()
	 */
	public int getChainIndex() {
		return chain;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setChainIndex(int)
	 */
	public void setChainIndex(int chainIndex) {
		chain=chainIndex;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setRefPosition(int, int, int)
	 */
	public void setRefPosition(int i, int j, int k) {
		this.edgeId=0;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Double val) {
		return (int)Math.signum(value-val);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.data.Indexable#setValue(java.lang.Comparable)
	 */
	public void setValue(Comparable obj) {
		value=(Double)obj;
	}

}
