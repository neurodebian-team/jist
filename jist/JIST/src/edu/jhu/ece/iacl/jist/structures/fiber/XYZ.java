package edu.jhu.ece.iacl.jist.structures.fiber;


import java.io.IOException;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.io.LEFileReader;
import edu.jhu.ece.iacl.jist.structures.geom.PT;



// TODO: Auto-generated Javadoc
/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Dec 6, 2005
 * Time: 4:44:08 PM
 * To change this template use Options | File Templates.
 */
public class XYZ extends PT {
    //public float x,y,z;
    /**
     * Instantiates a new xYZ.
     */
    public XYZ() {
        super(0f,0f,0f);
    }
    
    /**
     * Instantiates a new xYZ.
     * 
     * @param X the x
     * @param Y the y
     * @param Z the z
     */
    public XYZ(float X, float Y, float Z) {
        super(X,Y,Z);
    }
    
    /**
     * Read.
     * 
     * @param fp the fp
     * 
     * @return the xYZ
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static XYZ read(LEFileReader fp) throws IOException {
        float x,y,z;
        x = fp.readFloat();
        y = fp.readFloat();
        z = fp.readFloat();
        return new XYZ(x,y,z);
    }
    
    /**
     * Distance.
     * 
     * @param pt the pt
     * 
     * @return the double
     */
    public double distance(XYZ pt){
    	return Math.sqrt((x-pt.x)*(x-pt.x) + (y-pt.y)*(y-pt.y) + (z-pt.z)*(z-pt.z));
    }
    
    /**
     * Scale.
     * 
     * @param a the a
     */
    public void scale(double a){
    	x=(float)(x*a);
    	y=(float)(y*a);
    	z=(float)(z*a);
    }
    
    /**
     * Scale.
     * 
     * @param a the a
     * @param b the b
     * @param c the c
     */
    public void scale(float a, float b, float c){
    	x=(float)(x*a);
    	y=(float)(y*b);
    	z=(float)(z*c);
    }
    
    /**
     * Scale.
     * 
     * @param a the a
     * @param b the b
     * @param c the c
     */
    public void scale(double a, double b, double c){
    	x=(float)(x*a);
    	y=(float)(y*b);
    	z=(float)(z*c);
    }
    
    /**
     * To point3f.
     * 
     * @return the point3f
     */
    public Point3f toPoint3f(){
    	return new Point3f(x,y,z);
    }
    
    /* (non-Javadoc)
     * @see edu.jhu.ece.iacl.jist.structures.geom.PT#toString()
     */
    public String toString(){
    	return "("+this.x+","+this.y+","+this.z+")";
    }
    
    /**
     * Deep clone.
     * 
     * @return the xYZ
     */
    public XYZ deepClone(){
    	return new XYZ(x,y,z);
    }

}
