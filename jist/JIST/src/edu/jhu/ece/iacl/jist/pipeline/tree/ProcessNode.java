/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.tree;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;

/**
 * Node to display in tree.
 * 
 * @author Blake Lucas
 */
public class ProcessNode extends DefaultMutableTreeNode {
	
	/**
	 * The Enum Hierarchy.
	 */
	public enum Hierarchy {
		
		/** The Ancestors. */
		Ancestors, 
 /** The Descendants. */
 Descendants
	};

	/**
	 * Instantiates a new process node.
	 */
	public ProcessNode() {
	}

	/**
	 * Render process node as HTML.
	 * 
	 * @param context
	 *            the context
	 * @param hi
	 *            the hi
	 */
	public ProcessNode(ExecutionContext context, Hierarchy hi) {
		setUserObject("<HTML><B>" + context.getContextShortName() + "</B> (" + context.getAlgorithm().getLabel()
				+ ")</HTML>");
		MutableTreeNode input, output;
		ParamCollection inputParams = context.getContextInputParams();
		if (inputParams != null) {
			this.add(input = inputParams.createTreeNode());
			input.setUserObject("<HTML><I><B><font color='#990000'>Input</font><B></I></HTML>");
		}
		ParamCollection outputParams = context.getOutputParamsFromFile();
		if (outputParams != null) {
			this.add(output = outputParams.createTreeNode());
			output.setUserObject("<HTML><I><B><font color='#000099'>Output</font><B></I></HTML>");
		}
		if (hi == Hierarchy.Descendants) {
			for (ExecutionContext child : context.getDependents()) {
				this.add(new ProcessNode(child, hi));
			}
		} else if (hi == Hierarchy.Ancestors) {
			for (ExecutionContext child : context.getCallers()) {
				this.add(new ProcessNode(child, hi));
			}
		}
	}
}
