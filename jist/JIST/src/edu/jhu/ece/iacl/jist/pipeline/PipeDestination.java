/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeDestinationFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeDestinationFactory.DestinationCell;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

// TODO: Auto-generated Javadoc
/**
 * Pipe Module for processing destinations.
 * 
 * @author Blake Lucas
 */
public abstract class PipeDestination extends PipeModule {
	
	/** Parameter to store collection of other parameters. */
	protected ParamModel selectedParams;
	
	/** Reference to current layout to be used when processing destinations. */
	private transient PipeLayout currentLayout;
	
	/**
	 * Gets the current layout.
	 * 
	 * @return the current layout
	 */
	public PipeLayout getCurrentLayout() {
		return currentLayout;
	}

	/**
	 * Sets the current layout.
	 * 
	 * @param currentLayout the new current layout
	 */
	public void setCurrentLayout(PipeLayout currentLayout) {
		this.currentLayout = currentLayout;
	}

	/**
	 * Instantiates a new pipe destination.
	 */
	public PipeDestination() {
		super();
		this.inputParams = createInputParams();
		this.outputParams = createOutputParams();
		this.label = inputParams.getLabel();
		this.name = inputParams.getName();
		inputPorts = new Vector<PipePort>();
		outputPorts = new Vector<PipePort>();

		selectedParams.setUseConnector(true);
		inputPorts.add(selectedParams);
		inputPorts.addAll(inputParams.getAllVisibleDescendants());
		inputParams.add(selectedParams);
		for (PipePort port : inputPorts) {
			port.setOwner(this);
		}
		inputParams.getInputView().addObserver(this);
		outputPorts = (Vector) outputParams.getAllVisibleDescendants();
		for (PipePort port : outputPorts) {
			port.setOwner(this);
		}
		outputParams.setLabel(inputParams.getLabel());
		outputParams.setName(inputParams.getName());
		reset();
		init(null);
	}

	/**
	 * Execute after iterating is complete.
	 */
	protected abstract void complete();

	/**
	 * Create input parameters for this destination.
	 * 
	 * @return input parameters
	 */
	protected abstract ParamCollection createInputParams();

	/**
	 * Create graph cell to render destination.
	 * 
	 * @return the destination cell
	 */
	public DestinationCell createModuleCell() {
		return new PipeDestinationFactory.DestinationCell(getLabel(), this);
	}

	/**
	 * Create output parameters for this destination.
	 * 
	 * @return output parameters
	 */
	protected abstract ParamCollection createOutputParams();

	/**
	 * Initialize destination and create graph cell.
	 * 
	 * @param graph the graph
	 * 
	 * @return destination graph cell
	 */
	public PipeModuleCell init(PipeJGraph graph) {
		selectedParams.setUseConnector(true);
		for (PipePort port : inputPorts) {
			port.setPortType(PipePort.type.INPUT);
		}
		for (PipePort port : outputPorts) {
			port.setPortType(PipePort.type.OUTPUT);
		}
		if (graph != null) {
			return graph.createGraphNode(this);
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the input param.
	 * 
	 * @return the input param
	 */
	public ParamModel getInputParam(){
		return selectedParams;
	}
	
	/**
	 * Iterate through destination values.
	 * 
	 * @param monitor the monitor
	 */
	protected abstract void iterate(ProgressMonitor monitor);

	/**
	 * Pull value from layout my forwarding values along connectors and iterate.
	 * 
	 * @param monitor progress monitor for iterator
	 */
	public void pull(ProgressMonitor monitor) {
		if(selectedParams instanceof ParamCollection){
			((ParamCollection)selectedParams).clear();
		} 
		for (PipeConnector conn : ((Vector<PipeConnector>)selectedParams.getIncomingConnectors())) {
			PipeModule.forward(conn.getSource(), conn.getDestination());
		}
		iterate(monitor);
	}
		
	/**
	 * Reset destination after iterating through all values.
	 */
	protected abstract void reset();
	
	// Should only be called when it is actually desired to free any loaded resources. 
	public void release() {
		if(selectedParams instanceof ParamCollection){
			releaseInternal((ParamCollection)selectedParams);
		} 
		releaseInternal(inputParams);
		releaseInternal(outputParams);
	}
	
	private void releaseInternal(ParamCollection col) {
		for(ParamModel p : col.getAllDescendants()) {
			if(p instanceof ParamCollection)
				releaseInternal((ParamCollection)p);
			else {
				if(p instanceof ParamVolume) {
					JistLogger.logOutput(JistLogger.FINE, "Destimation DISPOSE of Volume: "+p.getName());
					((ParamVolume)p).getImageData().dispose();
				}					
			}
		}
	}
		
	protected boolean xmlEncodeModule(Document document, Element parent) {
		super.xmlEncodeModule(document, parent);
		Element em;
		
		/** Label to display for this parameter. */
		em = document.createElement("selectedParams");		
		selectedParams.xmlEncodeParam(document, em);
		parent.appendChild(em);
		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		selectedParams = ParamModel.abstractXmlDecodeParam(document,
				JistXMLUtil.xmlReadElement(el,"selectedParams"));
		
		
	
		this.label = inputParams.getLabel();
		this.name = inputParams.getName();
		inputPorts = new Vector<PipePort>();
		outputPorts = new Vector<PipePort>();
		selectedParams.setUseConnector(true);
		inputPorts.add(selectedParams);
		inputPorts.addAll(inputParams.getAllVisibleDescendants());
		inputParams.add(selectedParams);
		for (PipePort port : inputPorts) {
			port.setOwner(this);
		}
		inputParams.getInputView().addObserver(this);
		outputPorts = (Vector) outputParams.getAllVisibleDescendants();
		for (PipePort port : outputPorts) {
			port.setOwner(this);
		}
		outputParams.setLabel(inputParams.getLabel());
		outputParams.setName(inputParams.getName());
		reset();
		init(null);
	}
	
	public String reconcileAndMerge() throws InvalidJistMergeException {
		return "Destination merge not currently necessary."; //throw new InvalidJistMergeException("Not able to merge destinations yet");
	}
}
