/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.output;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.view.ViewJFrameImage;

/**
 * Output view for a volume parameter.
 * 
 * @author Blake Lucas
 */
public class ParamVolumeOutputView extends ParamOutputView implements ActionListener {
	// view button
	/** The button. */
	protected JButton button;
	// view image frame
	/** The frame. */
	protected ViewJFrameImage frame;

	/**
	 * Default constructor.
	 * 
	 * @param param
	 *            volume parameter
	 */
	public ParamVolumeOutputView(ParamVolume param) {
		this.param = param;
		BorderLayout layout = new BorderLayout();
		layout.setHgap(5);
		layout.setVgap(5);
		setLayout(layout);
		// Create label
		label = new JLabel("<HTML><B>" + param.getLabel() + "</B></HTML>");
		label.setAlignmentX(0);
		label.setAlignmentY(0);
		label.setVerticalTextPosition(SwingConstants.TOP);
		label.setHorizontalTextPosition(SwingConstants.LEFT);
		// Create parameter description field
		button = new JButton("View");
		button.setPreferredSize(ParamInputView.defaultNumberFieldDimension);
		button.addActionListener(this);
		field = new JLabel();
		field.setAlignmentX(0);
		field.setAlignmentY(0);
		field.setVerticalTextPosition(SwingConstants.TOP);
		field.setHorizontalTextPosition(SwingConstants.LEFT);
		add(label, BorderLayout.WEST);
		add(field, BorderLayout.CENTER);
		// Do not permit VIEW button until issue with memory cleanup is resolved
		add(button, BorderLayout.EAST);
		frame = null;
		this.update();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(button)) {
			ImageData img = getParameter().getImageData();
			ModelImage copyImg = null;
			if (img != null) {
				String name = img.getName() + "-view";
				if (MipavController.getImageByName(name) == null) {
					try {
						copyImg  = img.getModelImageCopy();
						MipavController.setModelImageName(copyImg,name);						
					} catch (Exception e) {
						JistLogger.logError(JistLogger.SEVERE, "ERROR OCCURED WHILE VIEWING IMAGE:" + e.getMessage());
						JistLogger.logError(JistLogger.SEVERE,e.getStackTrace().toString());
					} finally {
						if ((copyImg != null) && (copyImg.getFileInfo() != null)) {
							copyImg.calcMinMax();
							frame = new ViewJFrameImage(copyImg);
						}
					}
				}
			}
		}
	}

	/**
	 * Get volume parameter.
	 * 
	 * @return volume parameter
	 */
	public ParamVolume getParameter() {
		return (ParamVolume) param;
	}

	/**
	 * Update view from parameter.
	 */
	public void update() {
		File f = getParameter().getValue();
		if (f == null) {
			ImageData vol = getParameter().getImageData();
			if (vol != null) {
				field.setText(vol.getName());
			} else {
				field.setText("null");
			}
		} else {
			field.setText(FileReaderWriter.getFileName(f));
		}
	}
}
