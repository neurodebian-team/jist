package edu.jhu.ece.iacl.jist.structures.geom;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.VOI;

// TODO: Auto-generated Javadoc
/**
 * The Class MipavVOI.
 */
public class MipavVOI extends VOI{
	
	/** The img. */
	protected ModelImage img;
	
	/**
	 * Instantiates a new mipav voi.
	 * 
	 * @param name the name
	 * @param dim the dim
	 */
	public MipavVOI(String name,int dim){
		super((short)0,name,dim, 0); //Added 0 to constructor for VOI
	}

	/**
	 * Sets the volume.
	 * 
	 * @param img the new volume
	 */
	public void setVolume(ModelImage img){
		this.img=img;
	}
	
	/**
	 * Sets the volume.
	 * 
	 * @param img the new volume
	 */
	public void setVolume(ImageData img){
		this.img=(new ImageDataMipav(img)).extractModelImage();
	}
	
	/**
	 * Gets the image.
	 * 
	 * @return the image
	 */
	public ModelImage getImage(){
		return img;
	}
}
