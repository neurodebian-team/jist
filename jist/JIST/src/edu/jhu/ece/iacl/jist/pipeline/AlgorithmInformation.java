/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamHeader;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Information that describes the algorithm, including its authors, citations,
 * and version information.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class AlgorithmInformation implements Comparable<AlgorithmInformation>,
Cloneable {




	/**
	 * Set whether information can be edited.
	 * 
	 * @param editable the editable
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * Returns true if editable.
	 * 
	 * @return true if editable.
	 */
	public boolean isEditable() {
		return editable;
	}
	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public DevelopmentStatus getStatus() {
		return devStatus;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status the new status
	 */
	public void setStatus(DevelopmentStatus status) {
		devStatus = status;
	}

	/**
	 * The Class AlgorithmAuthor.
	 */
	public static class AlgorithmAuthor implements Cloneable {

		/** The website. */
		protected String website;

		/** The author. */
		protected String author;

		/** The email. */
		protected String email;

		/**
		 * Instantiates a new algorithm author.
		 * 
		 * @param author the author
		 * @param email the email
		 * @param website the website
		 */
		public AlgorithmAuthor(String author, String email, String website) {
			this.author = author;
			this.email = email;
			this.website = website;
		}

		/**
		 * Clone author information.
		 * 
		 * @return the algorithm author
		 */
		public AlgorithmAuthor clone() {
			AlgorithmAuthor eauth = new AlgorithmAuthor(author, email, website);
			return eauth;
		}

		/**
		 * Gets the author.
		 * 
		 * @return the author
		 */
		public String getAuthor() {
			return author;
		}

		/**
		 * Gets the email.
		 * 
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * Gets the website.
		 * 
		 * @return the website
		 */
		public String getWebsite() {
			return website;
		}

		/**
		 * Sets the author.
		 * 
		 * @param author the new author
		 */
		public void setAuthor(String author) {
			this.author = author;
		}

		/**
		 * Sets the email.
		 * 
		 * @param email the new email
		 */
		public void setEmail(String email) {
			this.email = email;
		}

		/**
		 * Sets the website.
		 * 
		 * @param website the new website
		 */
		public void setWebsite(String website) {
			this.website = website;
		}

		/**
		 * Serialize author as string.
		 * 
		 * @return the string
		 */
		public String toString() {
			return (((author != null) && (author.length() > 0)) ? author + " "
					: "")
					+ (((email != null) && (email.length() > 0)) ? "(" + email
							+ ") " : "")
							+ (((website != null) && (website.length() > 0)) ? website
									: "");
		}

		public void xmlEncodeParam(Document document, Element parent) {
			Element em;
			em = document.createElement("author");		
			em.appendChild(document.createTextNode(author+""));
			parent.appendChild(em);

			em = document.createElement("email");		
			em.appendChild(document.createTextNode(email+""));
			parent.appendChild(em);

			em = document.createElement("website");		
			em.appendChild(document.createTextNode(website+""));
			parent.appendChild(em);

		}

		public void xmlDecodeParam(Document document, Element item) {
			author = JistXMLUtil.xmlReadTag(item, "author");
			email = JistXMLUtil.xmlReadTag(item, "email");
			website = JistXMLUtil.xmlReadTag(item, "website");

		}
	}

	/**
	 * The Class Citation.
	 */
	public static class Citation implements Cloneable {

		/** The citation. */
		protected String citation;

		/**
		 * Instantiates a new citation.
		 * 
		 * @param cite the cite
		 */
		public Citation(String cite) {
			setText(cite);
		}

		/**
		 * Clone citation.
		 * 
		 * @return the citation
		 */
		public Citation clone() {
			Citation cite = new Citation(this.citation);
			return cite;
		}

		/**
		 * Gets the text.
		 * 
		 * @return the text
		 */
		public String getText() {
			return citation.replace("<BR>", "\n");
		}

		/**
		 * Sets the text.
		 * 
		 * @param citation the new text
		 */
		public void setText(String citation) {			
			citation = citation.replace("\n", "<BR>");
			this.citation = citation;
		}

		/**
		 * Serialize citation.
		 * 
		 * @return the string
		 */
		public String toString() {
			return "<HTML><PRE>" + getText() + "</PRE></HTML>";
		}

		public void xmlEncodeParam(Document document, Element parent) {

			Element em;
			em = document.createElement("citation");		
			em.appendChild(document.createTextNode(citation+""));
			parent.appendChild(em);



		}

		public void xmlDecodeParam(Document document, Element item) {
			citation = JistXMLUtil.xmlReadTag(item, "citation");

		}
	}

	/** Algorithm authors. */
	protected LinkedList<AlgorithmAuthor> authors;

	/** The citations. */
	protected LinkedList<Citation> citations;

	/** The creation date. */
	protected Date creationDate;

	/** The version. */
	protected String version;

	/** The description. */
	protected String description;

	/** The long description. */
	protected String longdescription;

	/** The website. */
	protected String website;

	/** The institute affiliation. */
	protected String affiliation;

	/** name and label for parameter. */
	protected String name, label;

	/** The algorithm class. */
	protected String algorithmClass;

	/** The Category. */
	protected DevelopmentStatus devStatus = DevelopmentStatus.UNKNOWN;

	/** Editable. */
	protected boolean editable;
	/**
	 * Instantiates a new algorithm information.
	 * 
	 * @param algo the algorithm
	 */
	public AlgorithmInformation(ProcessingAlgorithm algo) {
		this.creationDate = null;
		this.version = "";
		this.website = "";
		this.devStatus=DevelopmentStatus.UNKNOWN;
		this.editable = true;
		authors = new LinkedList<AlgorithmAuthor>();
		citations = new LinkedList<Citation>();
		init(algo);
	}

	/**
	 * Instantiates a new algorithm information.
	 * 
	 * @param name the name
	 * @param label the label
	 * @param c the class
	 */
	public AlgorithmInformation(String name, String label, String c) {
		this.creationDate = null;
		this.version = "";
		this.devStatus=DevelopmentStatus.UNKNOWN;
		this.website = "";
		this.editable = true;
		authors = new LinkedList<AlgorithmAuthor>();
		citations = new LinkedList<Citation>();
		this.name = name;
		this.label = label;
		this.algorithmClass = c;
	}

	/**
	 * Adds the.
	 * 
	 * @param author the author
	 */
	public void add(AlgorithmAuthor author) {
		authors.add(author);
	}

	/**
	 * Adds the.
	 * 
	 * @param cite the citation
	 */
	public void add(Citation cite) {
		citations.add(cite);
	}

	/**
	 * Clone information.
	 * 
	 * @return the algorithm information
	 */
	public AlgorithmInformation clone() {
		AlgorithmInformation info = new AlgorithmInformation(name, label,
				algorithmClass);
		info.editable = this.editable;
		info.setAuthors((LinkedList<AlgorithmAuthor>) authors.clone());
		info.setCitations((LinkedList<Citation>) citations.clone());
		info.setCreationDate(this.getCreationDate());
		info.setVersion(this.getVersion());
		info.setStatus(this.getStatus());
		info.setDescription(this.getDescription());
		info.setWebsite(this.getWebsite());
		info.setAffiliation(this.getAffiliation());
		return info;
	}

	/**
	 * Unimplemented.
	 * 
	 * @param o the o
	 * 
	 * @return the int
	 */
	public int compareTo(AlgorithmInformation o) {
		return 0;
	}

	/**
	 * Gets the affiliation.
	 * 
	 * @return the affiliation
	 */
	public String getAffiliation() {
		return affiliation;
	}

	/**
	 * Gets the algorithm class.
	 * 
	 * @return the algorithm class
	 */
	public Class getAlgorithmClass() {
		return ParamHeader.findClass(algorithmClass);
	}

	/**
	 * Gets the algorithm class name.
	 * 
	 * @return the algorithm class
	 */
	public String getAlgorithmClassName() {
		return algorithmClass;
	}

	/**
	 * Gets the authors.
	 * 
	 * @return the authors
	 */
	public LinkedList<AlgorithmAuthor> getAuthors() {
		return authors;
	}

	/**
	 * Gets the citations.
	 * 
	 * @return the citations
	 */
	public LinkedList<Citation> getCitations() {
		return citations;
	}

	/**
	 * Gets the creation date.
	 * 
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the Long description.
	 * 
	 * @return the longdescription
	 */
	public String getLongDescription() {
		return longdescription;
	}

	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Gets the website.
	 * 
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Initialize the information from the algorithm.
	 * 
	 * @param algo the algorithm
	 */
	public void init(ProcessingAlgorithm algo) {
		this.name = algo.getAlgorithmName();
		this.label = algo.getAlgorithmLabel();
		this.algorithmClass = algo.getClass().getCanonicalName();
	}

	/**
	 * Sets the affiliation.
	 * 
	 * @param affiliation the new affiliation
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	/**
	 * Sets the algorithm class.
	 * 
	 * @param algoClass the new algorithm class
	 */
	public void setAlgorithmClass(Class algoClass) {
		this.algorithmClass = algoClass.getCanonicalName();
	}

	/**
	 * Sets the authors.
	 * 
	 * @param authors the new authors
	 */
	public void setAuthors(LinkedList<AlgorithmAuthor> authors) {
		this.authors = authors;
	}

	/**
	 * Sets the citations.
	 * 
	 * @param citations the new citations
	 */
	public void setCitations(LinkedList<Citation> citations) {
		this.citations = citations;
	}

	/**
	 * Sets the creation date.
	 * 
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the long description.
	 * 
	 * @param longdescription the longdescription
	 */
	public void setLongDescription(String longdescription) {
		this.longdescription = longdescription;
	}

	/**
	 * Sets the label.
	 * 
	 * @param label the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Sets the website.
	 * 
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * Gets the status string.
	 * 
	 * @return the status string
	 */
	public String getStatusString() {
		if(devStatus==null)return "";
		switch(devStatus) {
		case ALPHA: 
			return "alpha";
		case BETA:
			return "beta";
		case RC:
			return "RC";
		case Release:
			return "R";
		case Dep:
			return "D";
		case NotFunctional:
			return "NOT FUNCTIONAL";
		case UNKNOWN:
			return "unk";
		default:
			return "";
		}
	}

	/**
	 * Serialize information as string.
	 * 
	 * @return the string
	 */
	public String toString() {
		String desc = "<HTML><PRE><B>Name:</B>"
			+ getLabel()
			+ " ("
			+ getName()
			+ ")"
			+ ((version != null && version.length() > 0) ? " v" + version
					: "") + getStatusString();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		desc += ((creationDate != null) ? "\n<B>Creation Date:</B>"
				+ sdf.format(creationDate) : "");
		desc += ((description != null && description.length() > 0) ? "\n<B>Description:</B>"
				+ description
				: "");
		desc += ((website != null && website.length() > 0) ? "\n<B>Website:</B>"
				+ website
				: "");
		desc += ((affiliation != null && affiliation.length() > 0) ? "\n<B>Affiliation:</B>"
				+ affiliation
				: "");
		if (authors.size() > 0) {
			desc += "\n<B>Authors:</B>";
		}
		int i = 0;
		for (AlgorithmAuthor author : authors) {
			desc += "\n[" + (++i) + "] " + author.toString();
		}
		if (citations.size() > 0) {
			desc += "\n<B>Citations:</B>";
		}
		i = 0;
		for (Citation citation : citations) {
			desc += "\n[" + (++i) + "] " + citation.getText();
		}
		desc += "</PRE></HTML>";
		return desc;
	}

	/**
	 * Equals.
	 * 
	 * @param info the info
	 * 
	 * @return true, if successful
	 */
	public boolean equals(AlgorithmInformation info){
		if(this.version==null&&info.version==null)return true;
		if(this.version==null||info.version==null)return false;
		return this.getVersion().equals(info.getVersion());
	}



	public void xmlDecodeParam(Document document, Element parent) {




		authors = new LinkedList<AlgorithmAuthor>();
		Element tm = JistXMLUtil.xmlReadElement(parent,"authors");
		if(tm!=null) {
			Vector<Element> nl = JistXMLUtil.xmlReadElementList(tm,"author");
			for(Element el : nl) {
				AlgorithmAuthor a = new AlgorithmAuthor(null,null,null);
				a.xmlDecodeParam(document, el);				
				authors.add(a);
			}			
		}		


		citations = new LinkedList<Citation>();
		
		
		 tm = JistXMLUtil.xmlReadElement(parent,"citations");
		if(tm!=null) {
			Vector<Element> nl = JistXMLUtil.xmlReadElementList(tm,"cite");
			for(Element el : nl) {
				Citation a = new Citation("");
				a.xmlDecodeParam(document, el);				
				citations.add(a);
			}			
		}	
			
		//			
		//			/** The creation date. */
		//			protected Date creationDate;
		try {
		creationDate = new Date(JistXMLUtil.xmlReadTag(parent, "creationDate"));
		} catch(java.lang.IllegalArgumentException e) {
			creationDate=null;	
		}

		version = (JistXMLUtil.xmlReadTag(parent, "version"));

		description = (JistXMLUtil.xmlReadTag(parent, "description"));
		longdescription = (JistXMLUtil.xmlReadTag(parent, "longdescription"));
		website = (JistXMLUtil.xmlReadTag(parent, "website"));
		affiliation = (JistXMLUtil.xmlReadTag(parent, "affiliation"));
		name = (JistXMLUtil.xmlReadTag(parent, "name"));
		label = (JistXMLUtil.xmlReadTag(parent, "label"));
		affiliation = (JistXMLUtil.xmlReadTag(parent, "affiliation"));


		algorithmClass = (JistXMLUtil.xmlReadTag(parent, "algorithmClass"));
		devStatus = DevelopmentStatus.valueOf(JistXMLUtil.xmlReadTag(parent, "devStatus"));
		editable = Boolean.valueOf(JistXMLUtil.xmlReadTag(parent, "editable"));
	}

	public boolean xmlEncodeParam(Document document, Element parent) {

		Element em; 




		//		 /** Algorithm authors. */
		//			protected LinkedList<AlgorithmAuthor> authors;
		boolean val = false;
		em = document.createElement("authors");	
		for(AlgorithmAuthor c: authors) {
			Element em2 = document.createElement("author");
			c.xmlEncodeParam( document, em2);						
			em.appendChild(em2);
			val = true;

		}
		if(val)
			parent.appendChild(em);
		//			
		//			/** The citations. */
		//			protected LinkedList<Citation> citations;
		val=false;
		em = document.createElement("citations");	
		for(Citation c: citations) {
			Element em2 = document.createElement("cite");
			c.xmlEncodeParam( document, em2);						
			em.appendChild(em2);
			val = true;
		}
		if(val)
			parent.appendChild(em);
		//			
		//			/** The creation date. */
		//			protected Date creationDate;
		em = document.createElement("creationDate");		
		em.appendChild(document.createTextNode(creationDate+""));
		parent.appendChild(em);
		//			
		//			/** The version. */
		//			protected String version;
		em = document.createElement("version");		
		em.appendChild(document.createTextNode(version+""));
		parent.appendChild(em);
		//			
		//			/** The description. */
		//			protected String description;
		em = document.createElement("description");		
		em.appendChild(document.createTextNode(description+""));
		parent.appendChild(em);
		//			
		//			/** The long description. */
		//			protected String longdescription;
		em = document.createElement("longdescription");		
		em.appendChild(document.createTextNode(longdescription+""));
		parent.appendChild(em);
		//			
		//			/** The website. */
		//			protected String website;
		em = document.createElement("website");		
		em.appendChild(document.createTextNode(website+""));
		parent.appendChild(em);
		//			
		//			/** The institute affiliation. */
		//			protected String affiliation;
		em = document.createElement("affiliation");		
		em.appendChild(document.createTextNode(affiliation+""));
		parent.appendChild(em);
		//			
		//			/** name and label for parameter. */
		//			protected String name, label;
		em = document.createElement("name");		
		em.appendChild(document.createTextNode(name+""));
		parent.appendChild(em);
		em = document.createElement("label");		
		em.appendChild(document.createTextNode(label+""));
		parent.appendChild(em);
		//			
		//			/** The algorithm class. */
		//			protected String algorithmClass;
		em = document.createElement("algorithmClass");		
		em.appendChild(document.createTextNode(algorithmClass+""));
		parent.appendChild(em);
		//
		//			/** The Category. */
		//			protected DevelopmentStatus devStatus = DevelopmentStatus.UNKNOWN;
		em = document.createElement("devStatus");		
		em.appendChild(document.createTextNode(devStatus+""));
		parent.appendChild(em);
		//			
		//			/** Editable. */
		//			protected boolean editable;

		em = document.createElement("editable");		
		em.appendChild(document.createTextNode(editable+""));
		parent.appendChild(em);

		return true;
	}

}
