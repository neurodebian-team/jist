/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeDestination;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeDestinationFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeSourceFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmNode;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableJTree;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;
import edu.jhu.ece.iacl.jist.pipeline.tree.PackageNode;

/**
 * Panel for selecting module components.
 * 
 * @author Blake Lucas
 */
public class ModuleTreePanel extends JScrollPane implements MouseListener, TreeSelectionListener {
	
	/**
	 * The Class DraggableTreeCellRenderer.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class DraggableTreeCellRenderer extends DefaultTreeCellRenderer {
		
		/**
		 * Instantiates a new draggable tree cell renderer.
		 */
		public DraggableTreeCellRenderer() {
			super();
		}

		/* (non-Javadoc)
		 * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
		 */
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selection, boolean expanded,
				boolean leaf, int row, boolean hasFocus) {
			if (value instanceof PackageNode) {
				DraggableTreeCellRenderer label=(DraggableTreeCellRenderer)super.getTreeCellRendererComponent(tree, value.toString(), selection, expanded, leaf, row,hasFocus);
				label.setFont(new Font("Arial",Font.BOLD,JistPreferences.getPreferences().getMdFontSize()));
				return label;
			} else {
				if(value instanceof DraggableNode){
					DraggableTreeCellRenderer label=(DraggableTreeCellRenderer)super.getTreeCellRendererComponent(tree, value.toString(), selection, expanded, leaf, row,hasFocus);
					label.setIcon(((DraggableNode)value).getIcon());
					label.setIconTextGap(2);
					setToolTipText(((DraggableNode)value).getDescription());
					return label;
				} else {
					return super.getTreeCellRendererComponent(tree, value.toString(), selection, expanded, leaf, row,hasFocus);
				}
			}
		}
	}

	/**
	 * Initialize module tree.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class InitializeWorker extends PipeLibrary.InitializeTask {
		
		/** The pane. */
		JScrollPane pane = null;

		/**
		 * Instantiates a new initialize worker.
		 * 
		 * @param parent
		 *            the parent
		 */
		public InitializeWorker(Component parent) {
			super(parent);
		}

		/**
		 * Build tree from modules.
		 */
		protected void done() {
			if (algo != null) {
				treeModel.removeNodeFromParent(src);
				treeModel.removeNodeFromParent(dest);
				treeModel.removeNodeFromParent(algo);
			}
			algo = PipeLibrary.getInstance().getTreeRoot();
			discoverModules();
			treeModel.insertNodeInto(algo, top, 0);
			treeModel.insertNodeInto(src, top, 1);
			treeModel.insertNodeInto(dest, top, 2);
			tree.expandPath(new TreePath(new Object[] { top, algo }));
			tree.expandPath(new TreePath(new Object[] { top, src }));
			tree.expandPath(new TreePath(new Object[] { top, dest }));
			tree.revalidate();
		}
	}
	
	public void expandTree() {
		  int row = 0;
		    while (row < tree.getRowCount()) {
		        tree.expandRow(row);
		        row++;
		    }
	}
	

	/**
	 * Rebuild tree.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class RebuildWorker extends PipeLibrary.RebuildTask {
		
		/** The pane. */
		JScrollPane pane = null;

		/**
		 * Instantiates a new rebuild worker.
		 * 
		 * @param parent
		 *            the parent
		 * @param remove
		 *            the remove
		 */
		public RebuildWorker(Component parent, boolean remove) {
			super(parent, remove);
		}

		/**
		 * Remove existing tree and rebuild.
		 */
		protected void done() {
			treeModel.removeNodeFromParent(src);
			treeModel.removeNodeFromParent(dest);
			treeModel.removeNodeFromParent(algo);
			algo = PipeLibrary.getInstance().getTreeRoot();


			discoverModules();
			treeModel.insertNodeInto(algo, top, 0);
			treeModel.insertNodeInto(src, top, 1);
			treeModel.insertNodeInto(dest, top, 2);
			tree.expandPath(new TreePath(new Object[] { top, algo }));
			tree.expandPath(new TreePath(new Object[] { top, src }));
			tree.expandPath(new TreePath(new Object[] { top, dest }));
			tree.revalidate();
		}
	}

	/** The panel. */
	private static ModuleTreePanel panel = null;

	/**
	 * Get singleton reference to tree panel.
	 * 
	 * @return the instance
	 */
	public static ModuleTreePanel getInstance() {
		if (panel == null) {
			panel = new ModuleTreePanel();
		}
		return panel;
	}

	/** The top. */
	DefaultMutableTreeNode src, algo, dest, top;
	
	/** The tree model. */
	DefaultTreeModel treeModel;
	
	/** The tree. */
	DraggableJTree tree;
	
	/** The layout. */
	LayoutPanel layout;
	
	/** Insert module into active frame if a module is double clicked. */
	int mouseX, mouseY;

	/**
	 * Default constructor.
	 */
	protected ModuleTreePanel() {
		createTree();
		init();
		this.layout = LayoutPanel.getInstance();
	}

	/**
	 * Create popup menu to edit module.
	 * 
	 * @param evt
	 *            mouse event
	 */
	protected void createPopupMenu(MouseEvent evt) {
		JPopupMenu menu = new JPopupMenu();
		mouseX = evt.getX();
		mouseY = evt.getY();
		TreePath parentpath = tree.getClosestPathForLocation(mouseX, mouseY);
		if(parentpath==null)return;
		DefaultMutableTreeNode parent = (DefaultMutableTreeNode) parentpath.getLastPathComponent();
		if (parent instanceof PackageNode) {
			menu.add(new AbstractAction("New") {
				public void actionPerformed(ActionEvent e) {
					TreePath parentpath = tree.getClosestPathForLocation(mouseX, mouseY);
					DefaultMutableTreeNode parent = (DefaultMutableTreeNode) parentpath.getLastPathComponent();
					if (parent instanceof PackageNode) {
						File dir = ((PackageNode) parent).getDirectory();
						File newDir = new File(dir, "New Package");
						if (newDir.mkdir()) {
							DefaultMutableTreeNode selectedNode;
							(treeModel).insertNodeInto(selectedNode = new PackageNode(newDir), parent, 0);
							tree.startEditingAtPath(new TreePath(selectedNode.getPath()));
						} else {
							System.err.println(getClass().getCanonicalName()+"Could Not Create Directory " + newDir.getAbsolutePath());
						}
					}
				}
			});
		}
		menu.add(new AbstractAction("Edit") {
			public void actionPerformed(ActionEvent e) {
				tree.startEditingAtPath(tree.getLeadSelectionPath());
			}
		});
		menu.add(new AbstractAction("Delete") {
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLeadSelectionPath()
						.getLastPathComponent();
				// Delete Package Node
				if (selectedNode instanceof PackageNode) {
					int n = JOptionPane.showOptionDialog(tree, "Are you sure you want to delete "
							+ ((PackageNode) selectedNode).getDirectory().getAbsolutePath() + " ?", null,
							JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
					if (n == 0) {
						if (ExecutionContext.deleteDir(((PackageNode) selectedNode).getDirectory(),null)) {
							treeModel.removeNodeFromParent(selectedNode);
						} else {
							System.err.println(getClass().getCanonicalName()+"Could Not Delete Directory "
									+ ((PackageNode) selectedNode).getDirectory());
						}
					}
				}
				if (selectedNode instanceof AlgorithmNode) {
					int n = JOptionPane.showOptionDialog(tree, "Are you sure you want to delete "
							+ ((AlgorithmNode) selectedNode).getMapFile().getAbsolutePath() + " ?", null,
							JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
					if (n == 0) {
						if (((AlgorithmNode) selectedNode).getMapFile().delete()) {
							treeModel.removeNodeFromParent(selectedNode);
						} else {
							System.err.println(getClass().getCanonicalName()+"Could Not Delete Directory "
									+ ((AlgorithmNode) selectedNode).getMapFile().delete());
						}
					}
				}
			}
		});
		menu.show(tree, evt.getX(), evt.getY());
	}

	/**
	 * Create module tree.
	 */
	private void createTree() {
		top = new DefaultMutableTreeNode("<HTML><B>Modules</B></HTML>");
		src = new DefaultMutableTreeNode("<HTML><B>Sources</B></HTML>");
		dest = new DefaultMutableTreeNode("<HTML><B>Destinations</B></HTML>");
		treeModel = new DefaultTreeModel(top);
		tree = new DraggableJTree(treeModel, top);
		this.setViewportView(tree);
		tree.setEditable(true);
		DraggableTreeCellRenderer renderer = new DraggableTreeCellRenderer();
		renderer.setLeafIcon(null);
		// renderer.setClosedIcon(null);
		// renderer.setOpenIcon(null);
		tree.setCellRenderer(renderer);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.addMouseListener(this);
		tree.addTreeSelectionListener(this);
		tree.setRootVisible(false);
		ToolTipManager.sharedInstance().registerComponent(tree);
	}

	/**
	 * Discover all modules that can be included in the tree and add them to the
	 * tree.
	 */
	private void discoverModules() {
		
		if(src!=null)src.removeAllChildren();
		if(dest!=null)dest.removeAllChildren();
		Vector<Class> classes = PipeLibrary.getAllClasses("edu.jhu.ece.iacl");
		LinkedList<PipeSourceFactory> srcList=new LinkedList<PipeSourceFactory>();
		LinkedList<PipeDestinationFactory> destList=new LinkedList<PipeDestinationFactory>();
		LinkedList<String> srcRoots=new LinkedList<String>();
		for (Class c : classes) {
			try {
				Class superClass=c;
				boolean foundSource=false;
				boolean foundDest=false;
				while(superClass.getSuperclass()!=null){
					superClass=superClass.getSuperclass();
					if(PipeSource.class.equals(superClass)){
						foundSource=true;
						break;
					}
					if(PipeDestination.class.equals(superClass)){
						foundDest=true;
						break;
					}
				
				}
				if (foundSource) {
					PipeSource al = (PipeSource) c.newInstance();
					PipeSourceFactory factory = new PipeSourceFactory(al.getLabel(),"", c);
					srcList.add(factory);
					String category=al.getInputParams().getCategory();
					insertIntoPath(src,category,factory.createTreeNode());
					
				} else if (foundDest) {
					PipeDestination al = (PipeDestination) c.newInstance();					
					PipeDestinationFactory factory = new PipeDestinationFactory(al.getLabel(), "",c);
					destList.add(factory);
					String category=al.getInputParams().getCategory();
					insertIntoPath(dest,category,factory.createTreeNode());
					
				}
			} catch (InstantiationException e) {
				// e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		/*
		//Collections.sort(srcList,new PipeLibrary.CompareFactories());
		//Collections.reverse(srcList);
		Collections.sort(destList,new PipeLibrary.CompareFactories());
		Collections.reverse(destList);
		
		for(PipeSourceFactory factory:srcList){
			treeModel.insertNodeInto(factory.createTreeNode(), src, 0);
		}
		for(PipeDestinationFactory factory:destList){
			treeModel.insertNodeInto(factory.createTreeNode(), dest, 0);
		}
	
		*/
	}
	public void insertIntoPath(DefaultMutableTreeNode root,String category,DefaultMutableTreeNode node){
		if(category==null){
			treeModel.insertNodeInto(node,root,0);
			return;
		}
		int index=category.indexOf(".");
		String headCategory,tailCategory;
		if(index<0){
			headCategory=category;
			tailCategory="";
		} else {
			headCategory=category.substring(0, index);
			tailCategory=category.substring(index+1,category.length());
		}
		DefaultMutableTreeNode newRoot=null;
		for(int i=0;i<root.getChildCount();i++){
			String name=((DefaultMutableTreeNode)root.getChildAt(i)).getUserObject().toString();
			if(name.equals(headCategory)){
				newRoot=(DefaultMutableTreeNode)root.getChildAt(i);
				break;
			} 
		}
		if(newRoot==null){
			treeModel.insertNodeInto(newRoot=new DefaultMutableTreeNode(headCategory), root, 0);
		}
		if(tailCategory.length()>0){
			insertIntoPath(newRoot, tailCategory,node);
		} else {
			treeModel.insertNodeInto(node,newRoot,0);
		}
	}
	/**
	 * Force initialization even if the module tree already exists.
	 */
	public void forceInit() {
		InitializeWorker worker = new InitializeWorker(PipelineLayoutTool.getInstance());
		worker.execute();
	}

	/**
	 * Initialize module panel by loading library.
	 */
	public void init() {
		InitializeWorker worker = new InitializeWorker(PipelineLayoutTool.getInstance());
		if (PipeLibrary.getInstance().getTreeRoot() != null) {
			worker.done();
		} else {
			worker.execute();
		}
	}
	public void mouseClicked(MouseEvent evt) {
		if (SwingUtilities.isLeftMouseButton(evt)) {
			if ((evt.getClickCount() == 2) && (tree.getSelectionPath() != null)) {
				MutableTreeNode node = (MutableTreeNode) tree.getSelectionPath().getLastPathComponent();
				if ((node != null) && (node instanceof DraggableNode) && (layout.getActiveFrame() != null)) {
					layout.getActiveFrame().insert((DraggableNode) node);
					evt.consume();
				}
			}
		} else if (SwingUtilities.isRightMouseButton(evt)) {
			createPopupMenu(evt);
			evt.consume();
		}
	}
	public void mouseEntered(MouseEvent evt) {
	}
	public void mouseExited(MouseEvent evt) {
	}
	public void mousePressed(MouseEvent arg0) {
	}
	public void mouseReleased(MouseEvent arg0) {
	}

	/**
	 * Rebuild module tree.
	 */
	public void rebuild() {
		int n = JOptionPane.showOptionDialog(PipelineLayoutTool.getInstance(),
				"Would you like to delete ALL existing module definitions in the library before rebuilding?", null, JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE, null, null, null);
		rebuild((n == 0));
	}

	/**
	 * Rebuild module tree.
	 * 
	 * @param remove
	 *            true indicates that existing modules should be removed
	 */
	public void rebuild(boolean remove) {
		RebuildWorker worker = new RebuildWorker(PipelineLayoutTool.getInstance(), remove);
		worker.execute();
	}

	/**
	 * Indicate that the module tree can be edited.
	 * 
	 * @param enabled
	 *            the enabled
	 */
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		tree.setEnabled(enabled);
	}
	public void valueChanged(TreeSelectionEvent evt) {
	}
}
