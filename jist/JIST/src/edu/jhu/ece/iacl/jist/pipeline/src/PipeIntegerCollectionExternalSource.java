package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;

public class PipeIntegerCollectionExternalSource extends PipeExternalSource{
	public PipeIntegerCollectionExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamNumberCollection("Default"));
		group.setLabel("External Integer Collection");
		group.setName("extints");
		group.setCategory("Externalize.Number.Integer");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Integer Collection");
		group.add(valParam = new ParamNumberCollection("Integer Collection"));
		return group;
	}
}
