/**
 * 
 */
package edu.jhu.ece.iacl.jist.structures.geom;

import javax.vecmath.Point3f;

// TODO: Auto-generated Javadoc
/**
 * The Class Point3.
 * 
 * @author Blake Lucas
 */
public class Point3 extends Point3f{
	
	/**
	 * Instantiates a new point3.
	 */
	public Point3(){
		super();
	}
	
	/**
	 * Instantiates a new point3.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public Point3(double x,double y,double z){
		super((float)x,(float)y,(float)z);
	}
	
	/**
	 * Instantiates a new point3.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public Point3(float x,float y,float z){
		super(x,y,z);
	}
	
	/**
	 * Instantiates a new point3.
	 * 
	 * @param d the d
	 */
	public Point3(double[] d){
		super((float)d[0],(float)d[1],(float)d[2]);
	}
	
	/* (non-Javadoc)
	 * @see javax.vecmath.Tuple3f#toString()
	 */
	public String toString(){
		return "("+x+","+y+","+z+")";
	}
}
