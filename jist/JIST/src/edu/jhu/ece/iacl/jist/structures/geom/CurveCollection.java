package edu.jhu.ece.iacl.jist.structures.geom;

import java.util.LinkedList;

import javax.vecmath.Point3f;

// TODO: Auto-generated Javadoc
/**
 * The Class CurveCollection.
 */
public class CurveCollection extends LinkedList<Curve>{
	
	/** The name. */
	protected String name;
	
	/** The line data. */
	protected double[][] lineData=null;
	
	/**
	 * Instantiates a new curve collection.
	 */
	public CurveCollection(){
		super();
		name=null;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name){
		this.name=name;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Sets the line data.
	 * 
	 * @param data the new line data
	 */
	public void setLineData(double[][] data){
		this.lineData=data;
	}
	
	/**
	 * Gets the line data.
	 * 
	 * @return the line data
	 */
	public double[][] getLineData(){
		if(lineData==null){
			double[][] data=new double[size()][1];
			for(int i=0;i<data.length;i++){
				data[i][0]=get(i).getValue();
			}
			return data;			
		} else {
			return lineData;
		}
	}
	
	/**
	 * Gets the curves.
	 * 
	 * @return the curves
	 */
	public Point3f[][] getCurves(){
		Point3f[][] curves=new Point3f[size()][0];
		for(int i=0;i<curves.length;i++){
			curves[i]=get(i).getCurve();
		}
		return curves;
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#toString()
	 */
	public String toString(){
		return (name!=null)?name:"lines";
	}
}
