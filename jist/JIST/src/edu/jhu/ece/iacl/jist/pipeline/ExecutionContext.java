/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JProgressBar;
import javax.swing.JTree;
import javax.swing.ProgressMonitor;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import edu.jhu.ece.iacl.jist.pipeline.PipeLayout.RunParameters;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterValueException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.tree.ProcessNode;
import edu.jhu.ece.iacl.jist.pipeline.tree.ProcessNode.Hierarchy;
import edu.jhu.ece.iacl.jist.processcontrol.DispatchController;
import edu.jhu.ece.iacl.jist.processcontrol.ProcessController;
import edu.jhu.ece.iacl.jist.processcontrol.ProcessStatus;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

// TODO: Auto-generated Javadoc
/**
 * An Execution Context provides threaded execution of a processing algorithm
 * with a specified set of input parameters.
 * 
 * @author Blake Lucas
 */
public class ExecutionContext implements Runnable, Comparable<ExecutionContext> {

	/**
	 * Listener to monitor for updates to an execution context.
	 * 
	 * @author Blake Lucas
	 */
	public static interface ContextListener {

		/**
		 * Fires when the statuedu.jhu.ece.iacl.jist.pipeline.PipeRunners or progress of an execution context changes.
		 * 
		 * @param context Execution Context that has been updated
		 * @param type Either a status update or progress update
		 */
		public void contextUpdate(ExecutionContext context, Update type);
	};

	// Execution status
	/**
	 * The Enum Status.
	 */
	public enum Status {

		/** The RUNNING. */
		RUNNING,

		/** The READY. */
		READY,

		/** The NOT READY. */
		NOT_READY,

		/** The COMPLETED. */
		COMPLETED,

		/** The FAILED. */
		FAILED,

		/** Out of SYNC. */
		OUT_OF_SYNC
		// ,
		/** LOCKED */
		// LOCKED
	}

	/**
	 * The Enum Update.
	 */
	public enum Update {

		/** The Status. */
		Status,

		/** The Progress. */
		Progress
	}

	/** Java class path. */
	public static final String classpath = System
	.getProperty("java.class.path");

	/**
	 * Delete all contents in a directory.
	 * 
	 * @param dir directory
	 * @param monitor the monitor
	 * 
	 * @return true if delete successful
	 */
	public static boolean deleteDir(File dir, ProgressMonitor monitor) {
		File candir;
		try {
			candir = dir.getCanonicalFile();
		} catch (IOException e) {
			System.err.println("jist.base"+e.getMessage());
			return false;
		}
		File[] files = candir.listFiles();
		//		if (monitor != null && files != null) {
		//			monitor.setMinimum(0);
		//			monitor.setMaximum(files.length);
		//		}
		int count = 0;
		if (files != null) {
			for (File file : files) {
				if (monitor != null) {
					//					monitor.setProgress(count++);
					monitor.setNote(file.getAbsolutePath());
					if (monitor.isCanceled())
						return false;
				}
				boolean deleted = file.delete();
				if (!deleted) {
					if (file.isDirectory()) {
						deleteDir(file, monitor);
					}
				}
			}
		}
		return dir.delete();
	}

	/** The execution status. */
	private Status status = null;

	/** SGE or Native Wrapper. */
	private ProcessController Proc = null;

	/** progress bar to monitor execution status. */
	protected JProgressBar statusBar;

	/** Elapsed cpu time in milliseconds. */
	protected long cpuTime;

	/** Elapsed actual time in milliseconds. */
	protected long actualTime;

	/** Used memory in MB. */
	protected int usedMemory;

	/** Allocated memory in MB. */
	protected int allocatedMemory;

	/** Running process. */
	protected Process proc;

	/** Start time. */
	private long actualStartTime = 0;

	/** Layout process associated with this context. */
	protected PipeLayout layout;

	/** Algorithm associated with this context. */
	protected PipeAlgorithm algo;

	/** The context name. */
	protected String contextName;

	/** The input file. */
	protected File inputFile;

	/** The input parameters. */
	protected ParamCollection inputParams;

	/** Cached parameters to compare against when checking for changes. */
	protected ParamCollection cachedInputParams;

	/** The output parameters. */
	protected ParamCollection outputParams;

	/** Execution contexts that forward to this context. */
	protected Vector<ExecutionContext> callers;

	/** Execution contexts that are dependent on the output from this context. */
	protected Vector<ExecutionContext> dependents;

	/** Identifying scenario number. */
	protected int scenarioId;

	/** Identifying experiment number. */
	protected String contextId;

	/** Run parameters extracted from the layout. */
	// protected RunParameters runParams;
	/** Running process. */
	protected Process process;

	/** Root directory for scenario. */
	protected File scenarioRoot = null;;

	/** Root directory for algorithm. */
	protected File algoRoot = null;

	/** Flag indicating the thread is running. */
	protected boolean runningFlag = false;

	/** Running thread. */
	protected Thread th = null;

	/** The Constant treeGraphLock. */
	protected final static Object treeGraphLock = new Object(); // Lock all
	// objects on
	// update
	/** Tree representing context ancestors. */
	protected JTree ancestorTree = null;

	/** Tree representing context descendants. */
	protected JTree descendantTree = null;

	/** Root node for ancestors. */
	protected DefaultMutableTreeNode ancestorTop = null;

	/** Root node for descendants. */
	protected DefaultMutableTreeNode descendantTop = null;

	/** The context listeners. */
	transient Vector<ContextListener> contextListeners = new Vector<ContextListener>();

	/** The execution priority. */
	protected int priority = 0;

	/** Out of sync flag. */
	protected boolean outOfSync = false;
	private boolean outOfSyncDiskAlreadyChecked = false;

	private void resetOutOfSyncCheck() {outOfSyncDiskAlreadyChecked=false;};
	/**
	 * Execution lock.
	 * 
	 * @param scenario the scenario
	 * @param context the context
	 * @param layout the layout
	 * @param algo the algo
	 */
	// protected boolean runLock = false;
	/** Lock name */
	// protected String lockName;
	/**
	 * Construct a new execution context. The combination of scenario and
	 * context id should be unique
	 * 
	 * @param scenario
	 *            scenario id
	 * @param context
	 *            context id
	 * @param layout
	 *            graph layout associated with this context
	 * @param algo
	 *            pipe algorithm associated with this context
	 */
	public ExecutionContext(int scenario, String context, PipeLayout layout,
			PipeAlgorithm algo) {
		// treeGraphLock = new Object();
		try {
		this.layout = layout;
		this.algo = algo;
		this.scenarioId = scenario;
		this.contextId = context;
		this.contextName = String.format("exp-%04d-%s", scenarioId, contextId);
		// this.lockName = String.format("lock-%04d-%s", scenarioId, contextId);
		this.callers = new Vector<ExecutionContext>();
		this.dependents = new Vector<ExecutionContext>();
		this.inputFile = new File(layout.getRunParameters()
				.getOutputDirectory(), contextName + ".input");
		statusBar = new JProgressBar();
		this.inputParams = algo.getInputParams().clone();
		cachedInputParams = inputParams;
		File root = layout.getRunParameters().getOutputDirectory();
		statusBar.setMinimum(0);
		statusBar.setMaximum(100);
		statusBar.setStringPainted(true);
		scenarioRoot = new File(root.getAbsoluteFile(), String.format(
				"exp-%04d", scenarioId)
				+ File.separator);
		algoRoot = new File(scenarioRoot.getAbsoluteFile(), contextName
				+ File.separator);
		File pipeFile = new File(scenarioRoot, contextName + ".input");
		if (pipeFile.exists()) {
			pipeFile.delete();
		}
		saveInputContext();
		resetOutOfSyncCheck();
		setStatus(Status.NOT_READY);
		// this.status=Status.NOT_READY;
		} catch (RuntimeException e) {
			if(algo!=null)
			JistLogger.logError(JistLogger.SEVERE, "Failed to initialize context for: "+algo.getName()+" "+algo.getClass().getCanonicalName());
			else
				JistLogger.logError(JistLogger.SEVERE, "Failed to initialize context for: "+null);
			throw e;
		}
	}

	/**
	 * Add child execution context that pulls data from this context.
	 * 
	 * @param mod the execution context (sync on tree)
	 */
	public void addChild(ExecutionContext mod) {
		synchronized (treeGraphLock) {
			if (!dependents.contains(mod)) {
				dependents.add(mod);
			}
		}
	}

	/**
	 * Add context listener.
	 * 
	 * @param listener listener
	 */
	public void addContextListener(ContextListener listener) {
		synchronized (treeGraphLock) {
			if (!getContextListeners().contains(listener)) {
				getContextListeners().add(listener);
			}
		}
	}

	/**
	 * Add parent execution context that pushes data to this context.
	 * 
	 * @param mod the execution context
	 */
	public void addParent(ExecutionContext mod) {
		synchronized (treeGraphLock) {
			if (!callers.contains(mod)) {
				mod.addChild(this);
				callers.add(mod);
			}
		}
	}

	/**
	 * Remove the output directory and anything in it. Reset status for context.
	 */
	public void clean(/* boolean forceRemoveLock */) {
		if (algoRoot.exists()) {
			deleteDir(algoRoot, null);
			// reset(forceRemoveLock);
			reset();
		}
		resetStatus();
	}

	/**
	 * Reset status for context.
	 */
	public void reset(/* boolean forceRemoveLock */) {
		outOfSync = false;
		outputParams = null;
		inputParams = cachedInputParams;
		cpuTime = 0;
		actualTime = 0;
		allocatedMemory = 0;
		usedMemory = 0;
		/*
		 * if (forceRemoveLock) forceRemoveLock(); else removeLock();
		 */
	}

	/**
	 * Compare execution contexts by status for sorting purposes.
	 * 
	 * @param context the execution context
	 * 
	 * @return the indicator
	 */
	public int compareTo(ExecutionContext context) {
		// Status status1 = (this.isLocked()) ? Status.LOCKED : this.status;
		// Status status2 = (context.isLocked()) ? Status.LOCKED :
		// context.status;
		int comp = this.status.compareTo(context.status);
		if (comp == 0) {
			comp = (int) Math
			.signum(this.getPriority() - context.getPriority());
			if (comp == 0) {
				return this.getContextName()
				.compareTo(context.getContextName());
			}
		}
		return comp;
	}

	/**
	 * Create ancestor and descendant tree and set the listener for those trees.
	 * 
	 * @param listener tree event listener (sync tree)
	 */
	public void createTreeNodes(TreeSelectionListener listener) {
		synchronized (treeGraphLock) {
			ancestorTree = new JTree(ancestorTop = new DefaultMutableTreeNode(
			"<HTML><B>Ancestors</B></HTML>"));
			DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
			renderer.setLeafIcon(null);
			renderer.setClosedIcon(null);
			renderer.setOpenIcon(null);
			ancestorTree.setCellRenderer(renderer);
			ancestorTree.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
			ancestorTree.addTreeSelectionListener(listener);
			descendantTree = new JTree(
					descendantTop = new DefaultMutableTreeNode(
					"<HTML><B>Descendants</B></HTML>"));
			renderer = new DefaultTreeCellRenderer();
			renderer.setLeafIcon(null);
			renderer.setClosedIcon(null);
			renderer.setOpenIcon(null);
			descendantTree.setCellRenderer(renderer);
			descendantTree.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
			descendantTree.addTreeSelectionListener(listener);
			updateTreeNodes();
		}
	}

	/**
	 * Decrease context queue priority.
	 */
	public void demote() {
		priority++;
		notifyListeners(Update.Status);
	}

	/**
	 * Check if contexts are equal by comparing context names.
	 * 
	 * @param obj the execution context
	 * 
	 * @return true, if equals
	 */
	public boolean equals(Object obj) {
		if (obj instanceof ExecutionContext) {
			return (contextName.equals(((ExecutionContext) obj).contextName));
		}
		return false;
	}

	/**
	 * Get algorithm.
	 * 
	 * @return algorithm
	 */
	public PipeAlgorithm getAlgorithm() {
		return algo;
	}

	/**
	 * Get amount of allocated memory.
	 * 
	 * @return the allocated memory
	 */
	public int getAllocatedMemory() {
		return allocatedMemory;
	}

	/**
	 * Get ancestor tree.
	 * 
	 * @return ancestor tree
	 */
	public JTree getAncestorTree() {
		return ancestorTree;
	}

	/**
	 * Get contexts that call this context.
	 * 
	 * @return contexts (copy)
	 */
	public Vector<ExecutionContext> getCallers() {
		synchronized (treeGraphLock) {
			Vector<ExecutionContext> lightClone = new Vector<ExecutionContext>(
					callers.size());
			for (int i = 0; i < callers.size(); i++)
				lightClone.add(callers.get(i));
			return lightClone;
		}
	}

	/**
	 * Get context id.
	 * 
	 * @return id
	 */
	public String getContextId() {
		return contextId;
	}

	/**
	 * Get context input parameters.
	 * 
	 * @return input parameters
	 */
	public ParamCollection getContextInputParams() {
		return inputParams;
	}

	/**
	 * Get listeners for this context.
	 * 
	 * @return listeners
	 */
	private Vector<ContextListener> getContextListeners() {
		if (contextListeners == null) {
			contextListeners = new Vector<ContextListener>();
		}
		return contextListeners;
	}

	/**
	 * Get context name.
	 * 
	 * @return context name
	 */
	public String getContextName() {
		return contextName;
	}

	/**
	 * Get context name.
	 * 
	 * @return context name
	 */
	public String getContextShortName() {
		return String.format("%04d-%s", scenarioId, contextId);
	}

	/**
	 * Get elapsed cpu time in milliseconds.
	 * 
	 * @return cpu time
	 */
	public String getElapsedTimeDescription() {
		if (status == Status.RUNNING) {

			actualTime = Math.max(0, System.currentTimeMillis()
					- actualStartTime);
			return PerformanceSummary.formatTime(actualTime);
		} else {
			return PerformanceSummary.formatTime(actualTime) + " / "
			+ PerformanceSummary.formatTime(cpuTime);
		}
	}

	/**
	 * Get current contexts that call this context.
	 * 
	 * @return parent contexts
	 */
	private Vector<ExecutionContext> getCurrentParentContexts() {
		return getAlgorithm().getCurrentParentContexts();
	}

	/**
	 * Get contexts that depend on this context.
	 * 
	 * @return contexts (copy)
	 */
	public Vector<ExecutionContext> getDependents() {
		synchronized (treeGraphLock) {

			Vector<ExecutionContext> lightClone = new Vector<ExecutionContext>(
					dependents.size());
			for (int i = 0; i < dependents.size(); i++)
				lightClone.add(dependents.get(i));
			return lightClone;

		}
	}

	/**
	 * Get descendant tree.
	 * 
	 * @return descendant tree
	 */
	public JTree getDescendantTree() {
		return descendantTree;
	}

	/**
	 * Get initial tree path by finding first child element.
	 * 
	 * @param node root tree node
	 * 
	 * @return get tree path
	 */
	protected TreePath getInitPath(TreeNode node) {
		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
		nodes.add(node);
		while (node.getChildCount() > 0) {
			node = node.getChildAt(0);
			nodes.add(node);
		}
		TreePath path = new TreePath(nodes.toArray());
		return path;
	}

	/**
	 * Get directory where input parameters are located.
	 * 
	 * @return input directory
	 */
	public File getInputLocation() {
		return scenarioRoot;
	}

	/**
	 * Load parameters from file.
	 * 
	 * @return the input parameters from file
	 */
	public/* synchronized */ParamCollection getInputParamsFromFile() {
		synchronized (treeGraphLock) {
			File pipeFile = new File(scenarioRoot, contextName + ".input");
			ParamCollection input = (ParamCollection) ParamFactory
			.fromXML(pipeFile);
			if (input != null) {
				input.init();
			}
			return input;
		}
	}

	/**
	 * Get layout owner.
	 * 
	 * @return layout
	 */
	public PipeLayout getLayout() {
		return layout;
	}

	/**
	 * Get directory where output will be placed.
	 * 
	 * @return directory
	 */
	public File getOutputLocation() {
		return algoRoot;
	}

	/**
	 * Load output parameters from file.
	 * 
	 * @return output parameters
	 */
	public/* synchronized */ParamCollection getOutputParamsFromFile() {
		synchronized (treeGraphLock) {
			if (outputParams != null) {
				return outputParams;
			}
			if (inputParams == null)
				return null;
			File pipeFile = new File(algoRoot,
					edu.jhu.ece.iacl.jist.utility.FileUtil
					.forceSafeFilename(inputParams.getName())
					+ ".output");
			ParamCollection output = (ParamCollection) ParamFactory
			.fromXML(pipeFile);
			if (output == null) {

				return null;
			}
			try {
				// Validate output parameters
				for (ParamModel model : output.getAllVisibleDescendants()) {
					// Do not do full validation of volume parameters so that
					// the volumes do not have to be loaded into memory
					if (model.isMandatory() && !model.isHidden()) {
						if (model instanceof ParamVolume) {
							if (((ParamVolume) model).getValue() == null) {
								return null;
							}
						} else if (model instanceof ParamVolumeCollection) {
							if (((ParamVolumeCollection) model).getFileList()
									.size() == 0) {
								return null;
							}
						} else {
							model.validate();
						}
					}
				}
				this.outputParams = output;
				return output;
			} catch (InvalidParameterException e) {
				System.err.println("jist.base"+pipeFile + "::" + e.getMessage());
			}
			return null;
		}
	}

	/**
	 * Get priority.
	 * 
	 * @return priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * Get running process.
	 * 
	 * @return process
	 */
	public Process getProcess() {
		return proc;
	}

	/**
	 * Get scenario id.
	 * 
	 * @return scenario id
	 */
	public int getScenarioId() {
		return scenarioId;
	}

	/**
	 * Get execution status.
	 * 
	 * @return status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Get progress bar.
	 * 
	 * @return progress bar
	 */
	public JProgressBar getStatusBar() {
		return statusBar;
	}

	/**
	 * Get used memory.
	 * 
	 * @return used memory
	 */
	public int getUsedMemory() {
		return usedMemory;
	}

	/**
	 * Check if algorithm has completed by checking if the output file exists.
	 * 
	 * @return true if completed
	 */
	protected boolean isCompleted() {
		synchronized (treeGraphLock) {
			if (inputParams == null)
				return false;
			for (ExecutionContext caller : callers) {
				if (caller.getStatus() != Status.COMPLETED) {
					return false;
				}
			}
			if(status==Status.OUT_OF_SYNC) {
				return false;
			}
			if (status == Status.FAILED) {
				return false;
			}
			if (status == Status.COMPLETED) {
				return true;
			}
			if (!algoRoot.exists()) {
				return false;
			}
			File outputFile = new File(algoRoot.getAbsoluteFile(),
					edu.jhu.ece.iacl.jist.utility.FileUtil
					.forceSafeFilename(inputParams.getName())
					+ ".output");
			return (outputFile.exists() && checkInSync());
		}
	}

	/**
	 * Check if two input files are in sync.
	 * 
	 * @return true, if check in sync
	 */
	protected boolean checkInSync() {
		synchronized (treeGraphLock) {

			if(outOfSyncDiskAlreadyChecked) {			
				return !outOfSync;
			}
			outOfSyncDiskAlreadyChecked=true;
			
			File inputFile = new File(algoRoot.getAbsoluteFile(),
					edu.jhu.ece.iacl.jist.utility.FileUtil
					.forceSafeFilename(inputParams.getName())
					+ ".input");
			File pipeFile = new File(scenarioRoot, contextName + ".input");
			if (pipeFile.exists() && inputFile.exists()) {
				ParamModel mod1 = (ParamModel) ParamFactory.fromXML(pipeFile);
				ParamModel mod2 = (ParamModel) ParamFactory.fromXML(inputFile);
				if (mod1 == null || mod2 == null) {
					outOfSync = true;
					return false;
				} else {
					if (mod1.equals(mod2)) {
						outOfSync = false;
						return true;
					} else {
						outOfSync = true;
						return false;
					}
				}
			} else {
				outOfSync = false;
				return true;
			}
		}
	}

	/**
	 * Return true if file is out of sync.
	 * 
	 * @return true, if checks if is out of sync
	 */
	public boolean isOutOfSync() {
		return outOfSync;
	}

	/*
	 * public boolean isLocked() { File lockFile = new File(scenarioRoot,
	 * lockName); return (lockFile.exists()); }
	 * 
	 * public String getLockID() { File lockFile = new File(scenarioRoot,
	 * lockName); if (lockFile.exists()) { String uuid =
	 * StringReaderWriter.getInstance().read(lockFile) .trim(); return uuid; }
	 * else return null; }
	 * 
	 * public boolean createLock() { if (!isLocked()) {
	 * 
	 * File lockFile = new File(scenarioRoot, lockName); return
	 * (StringReaderWriter.getInstance().write( layout.getScheduler().getUUID(),
	 * lockFile) != null); } else return false; } public boolean
	 * removeLock(boolean supressDialog) { File lockFile = new
	 * File(scenarioRoot, lockName); if (!lockFile.exists()) return true; if
	 * (layout.getScheduler().getUUID().equals(getLockID())) { return
	 * lockFile.delete(); } else { if (!supressDialog) { int n = JOptionPane
	 * .showOptionDialog( null, "Experiment " + contextName +
	 * " could not be unlocked. Would you like to force it to be unlocked?",
	 * null, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
	 * null, null); if (n == 0) { return lockFile.delete(); } else {
	 * System.err.println("jist.base"+"Could not remove lock " +
	 * layout.getScheduler().getUUID() + " " + getLockID()); return false; } }
	 * else { return false; } } }
	 * 
	 * public boolean removeLock() { return removeLock(false); }
	 * 
	 * public boolean forceRemoveLock() { File lockFile = new File(scenarioRoot,
	 * lockName); if (!lockFile.exists()) return true; return lockFile.delete();
	 * }
	 */
	/**
	 * Check if algorithm is ready to run by validating the input parameters for
	 * this context that are read from an input file.
	 * 
	 * @return true, if checks if is ready
	 */
	protected boolean isReady() {
		synchronized (treeGraphLock) {
			if (status == Status.OUT_OF_SYNC) {
				return false;
			}
			if (status == Status.FAILED) {
				return false;
			}
			for (ExecutionContext caller : callers) {
				if (caller.getStatus() != Status.COMPLETED) {
					return false;
				}
			}
			try {
				if (inputParams == null)
					return false;
				for (ParamModel model : inputParams.getAllVisibleDescendants()) {
					if (model.isMandatory()) {
						if (model instanceof ParamVolume) {
							if (((ParamVolume) model).getValue() == null) {
								return false;
							} else {
								// ParamVolumes should not be ready if their inputs do not exist or are not files
								if ((!((ParamVolume) model).getValue().exists()) ||
										!((ParamVolume) model).getValue().isFile())
									return false;

							}
						} else if (model instanceof ParamVolumeCollection) {
							// ParamVolume collections should have at least one valid entry
							List<File> fileList = ((ParamVolumeCollection) model).getFileList();
							if (fileList
									.size() == 0) {
								return false;
							} else {
								boolean any = false;
								for(File f : fileList) {
									if(f!=null) {
										if((f.exists())&&(f.isFile())) 										
											any = true;	
									}									
								}
								if(!any) return any;
							}
						} else {
							model.validate();
						}
					}
				}
				return true;
			} catch (InvalidParameterException e) {
				System.err.println("jist.base"+"Not ready to run:" + getContextName()
						+ "::" + e.getMessage());
			}
			return false;
		}
	}

	/**
	 * Copy parameters for this context into pipe graph.
	 * 
	 * @return true, if load context input params
	 */
	protected boolean loadContextInputParams() {
		synchronized (treeGraphLock) {
			try {
				Hashtable<String, ParamModel> original = algo.getInputHash();
				Hashtable<String, ParamModel> current = getContextInputParams()
				.getDescendantChildrenHash();
				Enumeration<String> keys = current.keys();
				while (keys.hasMoreElements()) {
					String key = keys.nextElement();
					ParamModel curPort = current.get(key);
					PipePort orPort = original.get(key);
					if (orPort != null) {
						PipeModule.forward(curPort, orPort);
					} else {
						System.err.println("jist.base"+getContextName()
								+ ":: Could not locate parameter " + key
								+ " for input");
					}
				}
				return true;
			} catch (InvalidParameterValueException e) {
				System.err.println("jist.base"+"Load Input Params:" + getContextName()
						+ "::" + e.getMessage());
			}
		}
		return false;
	}

	/**
	 * Copy output parameters from this context into pipe graph.
	 * 
	 * @return true if context output parameters loaded without error
	 */
	protected boolean loadContextOutputParams() {
		synchronized (treeGraphLock) {
			try {
				ParamCollection output = getOutputParamsFromFile();
				if (output == null) {
					System.err.println("jist.base"+getContextName()
							+ ": Could not find output parameters");
					return false;
				}
				Hashtable<String, ParamModel> original = algo.getOutputHash();
				Hashtable<String, ParamModel> current = output
				.getDescendantChildrenHash();
				Enumeration<String> keys = current.keys();
				while (keys.hasMoreElements()) {
					String key = keys.nextElement();
					ParamModel curPort = current.get(key);
					PipePort orPort = original.get(key);
					if (orPort != null) {
						PipeModule.forward(curPort, orPort);
					} else {
						System.err.println("jist.base"+getContextName()
								+ ":: Could not locate parameter " + key
								+ " for output");
					}
				}
				return true;
			} catch (InvalidParameterValueException e) {
				System.err.println("jist.base"+getContextName() + ": Load Output Params :"
						+ e.getMessage());
			}
		}
		return false;
	}

	/**
	 * Copy parameters from the pipe graph into this context.
	 * 
	 * @return true, if load layout input params
	 */
	protected boolean loadLayoutInputParams() {
		synchronized (treeGraphLock) {
			try {
				Hashtable<String, ParamModel> original = algo.getInputHash();
				Hashtable<String, ParamModel> current = getContextInputParams()
				.getDescendantChildrenHash();
				Enumeration<String> keys = current.keys();
				while (keys.hasMoreElements()) {
					String key = keys.nextElement();
					ParamModel curPort = current.get(key);
					PipePort orPort = original.get(key);
					if (orPort != null) {
						PipeModule.forward(orPort, curPort);
					} else {
						System.err.println("jist.base"+getContextName()
								+ ":: Could not locate parameter " + key
								+ " for input");
					}
				}
				return true;
			} catch (InvalidParameterValueException e) {
				System.err.println("jist.base"+"Load Layout Params:" + getContextName()
						+ "::" + e.getMessage());
			}
		}
		return false;
	}

	/**
	 * Notify listeners of change.
	 * 
	 * @param type type of change
	 */
	public void notifyListeners(Update type) {
		synchronized (treeGraphLock) {
			for (ContextListener listener : contextListeners) {
				listener.contextUpdate(this, type);
			}
		}
	}

	/**
	 * Parse status string from processing algorithm.
	 * 
	 * @param status command line status
	 */
	private void parseStatusInfo(String status) {
		String[] args = status.split(" ");
		int lenOffset = 0;
		if (args.length > 3) {
			try {
				setUsedMemory(Integer.parseInt(args[1]));
			} catch (NumberFormatException e) {
				System.err.println("jist.base"+"Could not parse used memory: " + args[1]);
			}
			try {
				setAllocatedMemory(Integer.parseInt(args[2]));
			} catch (NumberFormatException e) {
				System.err.println("jist.base"+"Could not parse allocated memory: "
						+ args[1]);
			}
			lenOffset = args[0].length() + args[1].length() + args[2].length()
			+ 3;
			try {
				int val = Integer.parseInt(args[0]);
				statusBar.setValue(val);
			} catch (NumberFormatException e) {
				System.err.println("jist.base"+"Could not parse: " + args[0]);
			}
			statusBar.setString(status.substring(lenOffset, status.length()));
			notifyListeners(Update.Progress);
		}
	}

	/**
	 * Increase context queue priority.
	 */
	public void promote() {
		priority--;
		notifyListeners(Update.Status);
	}

	/**
	 * Push output parameters to input parameters.
	 * 
	 * @return true if push succeeded
	 */
	public boolean push() {
		synchronized (treeGraphLock) {
			// Prevent scheduler from scheduling new contexts while push is
			// occuring
			layout.getScheduler().pause();
			if (synchronizedPush()) {
				// System.err.println("jist.base"+"dependents.length "+dependents.size());System.err.flush();
				for (ExecutionContext dependent : dependents) {
					dependent.updateStatus();
				}
				// Resume scheduler
				layout.getScheduler().resume();
				return true;
			} else {
				layout.getScheduler().resume();
				return false;
			}
		}
	}

	/**
	 * Remove all context listeners.
	 */
	public void removeAllContextListeners() {
		synchronized (treeGraphLock) {
			getContextListeners().removeAllElements();
		}
	}

	/**
	 * Remove context listener.
	 * 
	 * @param listener listener
	 */
	public void removeListener(ContextListener listener) {
		synchronized (treeGraphLock) {
			getContextListeners().remove(listener);
		}
	}

	/**
	 * Reset status for this contexts and its dependents.
	 */
	public void resetStatus() {
		synchronized (treeGraphLock) {
			setStatus(Status.NOT_READY);
			this.resetOutOfSyncCheck();
			updateStatus();
			Vector<ExecutionContext> dependents = getDependents();
		}
		for (int i = 0; i < dependents.size(); i++) {
			dependents.get(i).resetStatus();
		}
	}

	/**
	 * Restart algorithm by stopping, cleaning, and then starting the algorithm.
	 */
	public void restart() {
		stop();
		// Clean not needed - now part of start().
		start();
	}

	/**
	 * Thread method for running the algorithm.
	 */
	public void run() {
		// try {
		if (this.updateStatus() != Status.READY) {
			stop();
			return;
		}
		//Delete existing experiment for running experiment again
		clean();
		saveInputContext();
		if (!isReady()) {
			System.err.println("jist.base"+contextName
					+ ":: Saved input context but not ready");
			setStatus(Status.FAILED);
			stop();
		}
		File pipeFile = saveInputContext();
		if (pipeFile == null) {
			System.err.println("jist.base"+contextName + "::Pipe file could not be saved");
			System.err.flush();
			setStatus(Status.FAILED);
			stop();
			return;
		}
		if (!run(pipeFile)) {
			System.err.println("jist.base"+contextName + "::Execution failed");
			System.err.flush();
		}
		if (!isCompleted()) {
			System.err.println("jist.base"+contextName + "::Could not complete process");
			System.err.flush();
			outputParams = null;
			setStatus(Status.FAILED);
			stop();
			return;
		}
		setStatus(Status.COMPLETED);

		System.err.println("jist.base"+"ExecutionContext finished naturally.");
		System.err.flush();

		stop();
		// th = null;
	}

	/**
	 * Invoke algorithm process.
	 * 
	 * @param pipeFile the pipe file
	 * 
	 * @return true, if run
	 */
	private boolean run(File pipeFile) {
		return run(pipeFile,false);
	}
	private boolean run(File pipeFile,boolean showOutput) {
		try {
		if (this.getAlgorithm().getAlgorithm().isRunningInSeparateProcess()) {
			return runInSeparateProcess(pipeFile);
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+getContextName() + ": Run in same process: "
					+ pipeFile);
			actualStartTime = System.currentTimeMillis();
			runningFlag = true;
			PipeRunner.run(pipeFile, showOutput);
			runningFlag = false;
			setActualTime(System.currentTimeMillis() - actualStartTime);
			return true;
		}
		} finally {
			resetOutOfSyncCheck();
		}
	}

	/**
	 * Run in separate process.
	 * 
	 * @param pipeFile the pipe file
	 * 
	 * @return true, if successful
	 */
	private boolean runInSeparateProcess(File pipeFile) {


		/*
		 * if (!createLock()) { System.err.println("jist.base"+"Could not acquire lock for "
		 * + contextName); return false; }
		 */
		actualStartTime = System.currentTimeMillis();

		RunParameters runParams = layout.getRunParameters();
		DispatchController control = new DispatchController();
		// Build process
		String jre = JistPreferences.getPreferences().getJre();

		boolean SGE = false;
		if (layout.getRunParameters().isUseGridEngine())
			SGE = true;
		if (SGE) {
			if (System.getenv("LD_LIBRARY_PATH") == null) {
				SGE = false;
				System.out
				.println("Unable to use SGE without LD_LIBRARY_PATH set.");
			}
		}
		if (SGE) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Trying DRMAA");
			System.out.flush();
			if (!control.initDRMAASession()) {
				SGE = false;
				System.out.println(getClass().getCanonicalName()+"\t"+"Init DRMMA NOT okay");
				System.out.flush();
			} else
				System.out.println(getClass().getCanonicalName()+"\t"+"Init DRMMA okay");
			System.out.flush();
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"Create Job");
		System.out.flush();
		String []jvmargs=algo.getDefaultJVMArgs();
		if(jvmargs==null) {
			System.out.println(getClass().getCanonicalName()+"\t"+"runInSeparateProcess: No default JVM arguments");
			jvmargs = new String[0];
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"runInSeparateProcess: Default JVM arguments");
			for(int i=0;i<jvmargs.length;i++) {
				System.out.println(getClass().getCanonicalName()+"\t"+jvmargs[i]);
			}
		}		
		if ((runParams.getInputDirectory() != null)
				&& (layout.getFileLocation() != null)) {
			String []cmd = new String[] { jre,
					"-Xmx" + String.valueOf(runParams.getMaxHeap()) + "m",
					"-classpath", classpath,
					" edu.jhu.ece.iacl.jist.pipeline.PipeRunner",
					pipeFile.getAbsolutePath(),
					runParams.getInputDirectory().getAbsolutePath(),
					layout.getFileLocation().getParent() };
			String []finalcmd = new String[cmd.length+jvmargs.length];
			finalcmd[0]=cmd[0];
			for(int i=0;i<jvmargs.length;i++) 
				finalcmd[i+1]=jvmargs[i];
			for(int i=1;i<cmd.length;i++)
				finalcmd[jvmargs.length+i]=cmd[i];
			Proc = control.createJob(SGE, Arrays.asList(finalcmd));
		} else {
			String []cmd=new String[] { jre,
					"-Xmx" + String.valueOf(runParams.getMaxHeap()) + "m",
					"-classpath", classpath,
					"edu.jhu.ece.iacl.jist.pipeline.PipeRunner",
					pipeFile.getAbsolutePath() };
			String []finalcmd = new String[cmd.length+jvmargs.length];
			finalcmd[0]=cmd[0];
			for(int i=0;i<jvmargs.length;i++) 
				finalcmd[i+1]=jvmargs[i];
			for(int i=1;i<cmd.length;i++)
				finalcmd[jvmargs.length+i]=cmd[i];
			Proc = control.createJob(SGE, Arrays.asList(finalcmd));
		}

		th.setPriority(Thread.MIN_PRIORITY);

		int exitVal = -1;

		ProcessStatusParser statusParser = null;

		try {
			// start process
			System.out.println(getClass().getCanonicalName()+"\t"+"Submit job");
			System.out.flush();
			Proc.submit();
			statusParser = new ProcessStatusParser(Proc);
			if (Proc.getStatus() == ProcessStatus.RUNNING
					|| Proc.getStatus() == ProcessStatus.QUEUED_ACTIVE)
				runningFlag = true;

			System.out.println(getClass().getCanonicalName()+"\t"+"Job submitted");
			System.out.flush();
			// capture stdout

			statusParser.start(); // start monitoring in a separate thread
			System.out.println(getClass().getCanonicalName()+"\t"+Proc.getStatus());
			setStatus(Status.RUNNING);
			System.out.println(getClass().getCanonicalName()+"\t"+"Running, right?");
			System.out.flush();
			// do {
			//			
			// try {
			//					
			// exitVal = Proc.getExitCode();
			// System.out.println(getClass().getCanonicalName()+"\t"+exitVal);
			// } catch (IllegalThreadStateException e) {
			// //JOptionPane.showMessageDialog(null, "2nd catch block" +
			// e.getMessage());
			// System.err.println("jist.base"+getContextName() + ": " + e.getMessage());
			// System.err.println("jist.base"+getContextName() + ": Illegal Exit Value " +
			// exitVal);
			// runningFlag = false;
			// exitVal = -1;
			// } catch (InterruptedException e) {
			// //JOptionPane.showMessageDialog(null, "3rd catch block" +
			// e.getMessage());
			// System.err.println("jist.base"+getContextName() + " InterruptedException: " +
			// e.getMessage());
			// runningFlag = false;
			// exitVal = -1;
			// // If process hasn't exited and exit value returned, make
			// // sure process terminates and object destroyed
			// }
			// } while ((exitVal == -1) && runningFlag);

			try {

				Proc.waitFor();
				exitVal = Proc.getExitCode();
				System.out.println(getClass().getCanonicalName()+"\t"+"Job finished with exit code=" + exitVal);
				System.out.flush();
				//				System.out.println(getClass().getCanonicalName()+"\t"+exitVal);
			} catch (IllegalThreadStateException e) {
				System.err.println("jist.base"+getContextName() + ": " + e.getMessage());
				System.err.println("jist.base"+getContextName() + ": Illegal Exit Value "
						+ exitVal);
				runningFlag = false;
				exitVal = -1;
			} catch (InterruptedException e) {
				System.err.println("jist.base"+getContextName() + " InterruptedException: "
						+ e.getMessage());
				System.err.println("jist.base"+e.getStackTrace());
				System.err.flush();
				runningFlag = false;
				exitVal = -1;
				// If process hasn't exited and exit value returned, make
				// sure process terminates and object destroyed
			}

			boolean ret = runningFlag;
			runningFlag = false;
			setActualTime(System.currentTimeMillis() - actualStartTime);
			// removeLock(true);
			return ret;
		} catch (Exception e) {
			// Process failed
			// JOptionPane.showMessageDialog(null, "catch block" +
			// e.getMessage());
			System.err.println("jist.base"+"Process Failed " + getContextName() + "::"
					+ e.getMessage());
		} finally {
			if (statusParser != null)
				statusParser.stopParsing();
			if (!Proc.destroy()) {
				System.err.println("jist.base"+"Unable to destoy process.");
				System.err.flush();
			}
		}
		runningFlag = false;
		// removeLock(true);
		return false;
	}

	/**
	 * The Class ProcessStatusParser.
	 */
	protected class ProcessStatusParser extends Thread {

		/** The stream. */
		BufferedReader stream = null;

		/** The running. */
		protected boolean running = false;

		/**
		 * Instantiates a new process status parser.
		 * 
		 * @param process the process
		 */
		public ProcessStatusParser(ProcessController process) {
			super();
			try {
				this.stream = new BufferedReader(new InputStreamReader(process
						.getStdoutFile()));
			} catch (Exception e) {
				System.out.println(getClass().getCanonicalName()+"\t"+"Not a problem when using grid:");
				System.out.flush();
				e.printStackTrace();
				stream = null;
			}
		}

		/**
		 * Stop parsing.
		 */
		public void stopParsing() {
			running = false;
		}

		/* (non-Javadoc)
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			if (stream == null)
				return;
			String line = null;
			running = true;
			try {
				while (running && runningFlag
						&& (line = stream.readLine()) != null) {
					if (!running)
						break;
					parseStatusInfo(line);
					if (!stream.ready()) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							running = false;
							System.err
							.println("ExecutionContext shutting down by interrupt.");
							System.err.flush();
						}
					}
				}
				stream.close();
			} catch (IOException e) {
				System.err
				.println("ExecutionContext: Stream closed unexepctedly for "
						+ getContextName());
				System.err.flush();
			}
		}
	}

	/**
	 * Save input parameters to file.
	 * 
	 * @return input file that was saved
	 */
	protected File saveInputContext() {
		synchronized (treeGraphLock) {
			RunParameters runParams = layout.getRunParameters();
			// System.err.println("jist.base"+"ExecutionContext: Waiting to saveInputContext.");System.err.flush();
			// synchronized(thLock)
			{
				// System.err.println("jist.base"+"ExecutionContext: saveInputContext.");System.err.flush();
				if (!runParams.getOutputDirectory().exists()) {
					runParams.getOutputDirectory().mkdir();
				}
				if (!scenarioRoot.exists()) {
					scenarioRoot.mkdir();
				}
				File pipeFile = new File(scenarioRoot, contextName + ".input");
				// System.err.println("jist.base"+pipeFile);System.err.flush();
				if (inputParams.write(pipeFile)) {
					return pipeFile;
				} else {
					System.err.println("jist.base"+contextName + ":: Could not write "
							+ pipeFile.toString());
					return null;
				}
			}
		}
	}

	/**
	 * Set algorithm.
	 * 
	 * @param module algorithm
	 */
	public void setAlgorithm(PipeAlgorithm module) {
		this.algo = module;
	}

	/**
	 * Set amount of allocated memory.
	 * 
	 * @param totalMemory the total memory
	 */
	public void setAllocatedMemory(int totalMemory) {
		this.allocatedMemory = totalMemory;
	}

	/**
	 * Set context id.
	 * 
	 * @param contextId context id
	 */
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	/**
	 * Set elapsed cpu time in milliseconds.
	 * 
	 * @param cpuTime time
	 */
	public void setCpuTime(long cpuTime) {
		this.cpuTime = cpuTime;
	}

	/**
	 * Set elapsed actual time in milliseconds.
	 * 
	 * @param actualTime time
	 */
	public void setActualTime(long actualTime) {
		this.actualTime = actualTime;
	}

	/**
	 * Gets the cpu time.
	 * 
	 * @return the cpu time
	 */
	public long getCpuTime() {
		return cpuTime;
	}

	/**
	 * Gets the actual time.
	 * 
	 * @return the actual time
	 */
	public long getActualTime() {
		return actualTime;
	}

	/**
	 * Set layout owner.
	 * 
	 * @param layout layout owner
	 */
	public void setLayout(PipeLayout layout) {
		this.layout = layout;
	}

	/**
	 * Set execution priority.
	 * 
	 * @param priority priority
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * Set running process.
	 * 
	 * @param proc process
	 */
	public void setProcess(Process proc) {
		this.proc = proc;
	}

	/**
	 * Set scenario id.
	 * 
	 * @param scenarioId scenario id
	 */
	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}

	/**
	 * Set status for context. If algorithm is completed, push data from output
	 * to child inputs
	 * 
	 * @param status the status
	 */
	public void setStatus(Status status) {
		if(status==this.status)
			return; // NO CHANGE OCCURED
		this.status = status;
		if (status == Status.COMPLETED) {
			inputParams = getInputParamsFromFile(); // SLOW - now only called once
			push();
			if (outputParams != null) {
				ParamModel perform = outputParams
				.getFirstChildByName("Execution Time");
				if ((perform != null) && (perform instanceof ParamPerformance)) {
					PerformanceSummary summary = ((ParamPerformance) perform)
					.getValue();
					if (summary != null) {
						setCpuTime(summary.getTotalElapsedCPUTime());
						setActualTime(summary.getTotalElapsedActualTime());
						setUsedMemory(summary.getPeakUsedMemory());
						setAllocatedMemory(summary.getPeakAllocatedMemory());
					}
				}
			}
			statusBar.setValue(100);
			statusBar.setString("100%");
		} else if (status != Status.RUNNING) {
			statusBar.setValue(0);
			statusBar.setString(" ");
		}
		if ((status == Status.FAILED) || (status == Status.COMPLETED) || (status==Status.OUT_OF_SYNC)) {
			// th = null;
			runningFlag = false;
		}
		notifyListeners(Update.Status);
	}

	/**
	 * Set used memory.
	 * 
	 * @param usedMemory used memory
	 */
	public void setUsedMemory(int usedMemory) {
		this.usedMemory = usedMemory;
	}

	/**
	 * Start running the algorithm.
	 */
	public synchronized void start() {
		// System.err.println("jist.base"+"Waiting to start ExecutionContext");
		// System.err.flush();
		// synchronized(thLock)
		{
			// System.err.println("jist.base"+"Start ExecutionContext"); System.err.flush();
			if (runningFlag) {
				System.err
				.println("ExecutionContext is already running. Ignoring.");
				return;
			}
			runningFlag = true;

			/****************************************************
			 * 1) Create an experiment with two modules where A feeds information to B
			 * 2) Execute the pipeline
			 * 3) Return to the layout and change a parameter in A
			 * 4) Reload the layout in the process manager
			 * 5) A will appear out-of-sync and B will be not ready, press play on A
			 * 6) A will complete and B will immediately show that it has completed since the result was not flushed before starting A
			 *
			 * Solution:
			 * Add a clean() statement in start() within ExecutionContext. the clean() statement in restart() is now redundant, so that can be removed.
			 */
			clean(/* false */);

			if (th != null) {
				System.err.println("jist.base"+"Warning: ExecutionContext thread not null");
			}
			th = null;
			th = new Thread(this);
			th.setName(contextName);
			th.start();

		}
		// System.err.println("jist.base"+"Started ExecutionContext"); System.err.flush();
	}

	/**
	 * Force algorithm to stop running.
	 */
	public synchronized void stop() {
		// System.err.println("jist.base"+"Waiting to stop ExecutionContext");
		// System.err.flush();

		// synchronized(thLock)
		{
			// System.err.println("jist.base"+"Stop ExecutionContext"); System.err.flush();
			if (!runningFlag) {
				th = null;
				return;
			}
			System.err.println("jist.base"+"EC:" + getContextName() + ": Stopping.");
			// System.err.flush();
			runningFlag = false;

			try {
				Proc.destroy();

				// th.join();
				// th.join(100);
				if (th.isAlive()) {
					th.interrupt();
					th.join(200);
					if (th.isAlive()) {
						System.err.println("jist.base"+"ExecutionContext join failed.");
						System.err.flush();
					}
				}

				if ((status == Status.COMPLETED) || (status == Status.FAILED) /* why? */
						|| (status == Status.RUNNING)) {
					setStatus(Status.READY);
				}
			} catch (InterruptedException e) {
				System.err.println("jist.base"+getContextName()
						+ ": Interruption in termination");
				// System.err.println("jist.base"+"PipeScheduler stop interrupted.");
				// System.err.println("jist.base"+e.getMessage());
				System.err.flush();
			} catch (IllegalThreadStateException e) {
				System.err.println("jist.base"+getContextName()
						+ ": Not terminated cleanly");
				System.err.println("jist.base"+e.getMessage());
				System.err.flush();
			}

			th = null;
		}
		// System.err.println("jist.base"+"Stoped ExecutionContext"); System.err.flush();
	}

	/**
	 * Push data from output parameters into child contexts.
	 * 
	 * @return true if parameters pushed without error
	 */
	protected boolean synchronizedPush() {
		synchronized (treeGraphLock) {
			// Load output parameters into graph
			if (!loadContextOutputParams()) {
				System.err.println("jist.base"+getContextName()
						+ ": Could not load output parameters");
				return false;
			}

			for (ExecutionContext dependent : dependents) {
				// Load input parameters for child contexts into graph
				if (!dependent.loadContextInputParams()) {
					System.err
					.println(dependent.getContextName()
							+ ": Could not copy paramters from context to graph");
					return false;
				}
				// Push output parameters to child input parameters
				for (PipePort outport : algo.getOutputPorts()) {
					for (PipePort inport : (Vector<PipePort>) outport
							.getOutgoingPorts()) {
						PipeModule.forward(outport, inport);
					}
				}
				// Copy input parameters from graph to context
				if (!dependent.loadLayoutInputParams()) {
					System.err
					.println(dependent.getContextName()
							+ ": Could not copy paramters from graph to context");
					return false;
				}
				// Save input parameters to file
				if (dependent.saveInputContext() == null) {
					System.err.println("jist.base"+dependent.getContextName()
							+ ": Could not save input parameters");
					return false;
				}

			}
			return true;
		}
	}

	/**
	 * Get context name and algorithm label.
	 * 
	 * @return the string
	 */
	public String toString() {
		String label = getAlgorithm().getLabel();
		return "Pipe " + String.format("%04d-%s", scenarioId, contextId) + " ("
		+ label + ")";
	}

	/**
	 * Update all tree nodes for all callers and dependents.
	 */
	protected void updateAllTreeNodes() {
		synchronized (treeGraphLock) {
			updateTreeNodes();
			for (ExecutionContext caller : callers) {
				caller.updateTreeNodes();
			}
			for (ExecutionContext dependent : dependents) {
				dependent.updateTreeNodes();
			}
		}
	}

	/**
	 * Update the status of this context by checking if algorithm is ready and
	 * completed.
	 * 
	 * @return current status
	 */
	public Status updateStatus() {

		if ((status == Status.RUNNING) || (status == Status.FAILED) || (status==Status.OUT_OF_SYNC)) {
			return status;
		}
		// If context current status is completed, or output file indicates
		// status is completed
		if ((status == Status.COMPLETED) || isCompleted()) {
			// Current status is not completed, so set status to completed
			if (status != Status.COMPLETED) {
				setStatus(Status.COMPLETED);
			}
		} else {
			if(!checkInSync()) {
				if (status != Status.OUT_OF_SYNC) {
					setStatus(Status.OUT_OF_SYNC);
				}
			}else{

				if (isReady()) {
					if (status != Status.READY) {
						setStatus(Status.READY);
					}
				} else {
					if (status != Status.NOT_READY) {
						setStatus(Status.NOT_READY);
					}
				}
			}
		}
		return status;
	}

	/**
	 * Update tree nodes if ancestors or descendants change.
	 */
	protected void updateTreeNodes() {
		synchronized (treeGraphLock) {
			if (ancestorTop != null) {
				ancestorTop.removeAllChildren();
				ancestorTop.add(new ProcessNode(this, Hierarchy.Ancestors));
				ancestorTree.setSelectionPath(getInitPath(ancestorTop));
			}
			if (descendantTop != null) {
				descendantTop.removeAllChildren();
				descendantTop.add(new ProcessNode(this, Hierarchy.Descendants));
				descendantTree.setSelectionPath(getInitPath(descendantTop));
			}
		}
	}

	/**
	 * Gets the hash key.
	 * 
	 * @return the hash key
	 */
	public String getHashKey() {
		// TODO Auto-generated method stub
		return getHashKey(this.getAlgorithm().getLabel(), getAlgorithm()
				.getAlgorithmClass(), getContextInputParams());
	}

	/**
	 * Gets the hash key.
	 * 
	 * @param label the label
	 * @param c the c
	 * @param currentParams the current params
	 * 
	 * @return the hash key
	 */
	public static String getHashKey(String label, Class c,
			ParamCollection currentParams) {
		// TODO Auto-generated method stub
		return label.toString() + "::" + c.toString() + "::"
		+ currentParams.toString();
	}
}
