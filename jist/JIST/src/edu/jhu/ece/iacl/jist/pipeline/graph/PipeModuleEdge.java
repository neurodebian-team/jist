/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.util.Hashtable;
import java.util.Map;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;

/**
 * Edge cell to display on graph.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeModuleEdge extends DefaultEdge {
	
	/** The Constant avoidanceRouting. */
	transient protected final static AvoidanceRouting avoidanceRouting = new AvoidanceRouting();
	
	/** The connector. */
	protected PipeConnector connector;
	
	/**
	 * Create a clone of the cell. The cloning of the user object is deferred to
	 * the cloneUserObject() method. The source and target references are set to
	 * null.
	 * 
	 * @return Object a clone of this object.
	 */
	public PipeModuleEdge clone() {
		PipeModuleEdge edge = new PipeModuleEdge();
		edge.source = null;
		edge.target = null;
		return edge;
	}
	
	/**
	 * Instantiates a new pipe module edge.
	 */
	protected PipeModuleEdge() {
		super();
		getAttributes().applyMap(createEdgeAttributes());
	}
	
	/**
	 * Instantiates a new pipe module edge.
	 * 
	 * @param connector
	 *            the connector
	 */
	public PipeModuleEdge(PipeConnector connector) {
		super();
		this.connector = connector;
		this.connector.setGraphEdge(this);
		getAttributes().applyMap(createEdgeAttributes());
		setEditable((connector.getSource() instanceof ObjectCollection)
				&& !(connector.getDestination() instanceof ObjectCollection));
	}

	/**
	 * Creates the edge attributes.
	 * 
	 * @return the map
	 */
	protected Map createEdgeAttributes() {
		Map map = new Hashtable();
		// Add a Line End Attribute
		GraphConstants.setRouting(map, avoidanceRouting);
		GraphConstants.setLineEnd(map, GraphConstants.ARROW_NONE);
		GraphConstants.setLineStyle(map, GraphConstants.STYLE_ORTHOGONAL);
		GraphConstants.setEndFill(map, true);
		GraphConstants.setEndSize(map, 8);
		GraphConstants.setLineColor(map, Color.black);
		GraphConstants.setBendable(map, true);
		GraphConstants.setEditable(map, true);
		GraphConstants.setBackground(map, Color.white);
		GraphConstants.setOpaque(map, true);
		// GraphConstants.setLabelPosition(map, new
		// Point2D.Double(GraphConstants.PERMILLE*0.85,-10));
		// Add a label along edge attribute
		return map;
	}

	/**
	 * Get connector associated with edge.
	 * 
	 * @return connector
	 */
	public PipeConnector getConnector() {
		return connector;
	}

	/**
	 * Set connector associated with edge.
	 * 
	 * @param connector
	 *            connector
	 */
	public void setConnector(PipeConnector connector) {
		this.connector = connector;
	}

	/**
	 * Set whether connector will have spinner and be editable.
	 * 
	 * @param editable
	 *            true if editable
	 */
	public void setEditable(boolean editable) {
		if (editable) {
			GraphConstants.setEditable(this.getAttributes(), true);
			int index = getConnector().getSourceIndex();
			if (index == -1) {
				index = getConnector().getSource().getOutgoingConnectors().size() - 1;
			}
			getConnector().setSourceIndex(index);
		} else {
			GraphConstants.setEditable(this.getAttributes(), false);
			this.setUserObject("");
		}
	}
}
