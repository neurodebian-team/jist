/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Number Parameter storage.
 * 
 * @author Blake Lucas
 */
public abstract class ParamNumber extends ParamModel<Number> {
	
	/** The Constant MIN_DOUBLE_VALUE. */
	public static final double MIN_DOUBLE_VALUE = -1E20;
	
	/** The Constant MAX_DOUBLE_VALUE. */
	public static final double MAX_DOUBLE_VALUE = 1E20;
	
	/** The Constant MIN_FLOAT_VALUE. */
	public static final float MIN_FLOAT_VALUE = -1E20f;
	
	/** The Constant MAX_FLOAT_VALUE. */
	public static final float MAX_FLOAT_VALUE = 1E20f;
	
	/** The Constant MIN_INT_VALUE. */
	public static final int MIN_INT_VALUE = -1000000000;
	
	/** The Constant MAX_INT_VALUE. */
	public static final int MAX_INT_VALUE = 1000000000;
	
	/** The Constant MIN_LONG_VALUE. */
	public static final long MIN_LONG_VALUE = -1000000000;
	
	/** The Constant MAX_LONG_VALUE. */
	public static final long MAX_LONG_VALUE = 1000000000;
	
	/** The value. */
	protected Number value;
	
	/** The min. */
	protected Number min;
	
	/** The max. */
	protected Number max;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#clone()
	 */
	public abstract ParamNumber clone();
	
	/**
	 * Get the number value as double.
	 * 
	 * @return double value
	 */
	public double getDouble() {
		return value.doubleValue();
	}

	/**
	 * Get the number value as float.
	 * 
	 * @return float value
	 */
	public float getFloat() {
		return value.floatValue();
	}

	/**
	 * Get the number value as int.
	 * 
	 * @return integer value
	 */
	public int getInt() {
		return value.intValue();
	}

	/**
	 * Get the number value as long.
	 * 
	 * @return long value
	 */
	public long getLong() {
		return value.longValue();
	}

	/**
	 * Get maximum possible value.
	 * 
	 * @return the max
	 */
	public Number getMax() {
		return max;
	}

	/**
	 * Get minimum possible value.
	 * 
	 * @return the min
	 */
	public Number getMin() {
		return min;
	}

	/**
	 * Get the number value.
	 * 
	 * @return number value
	 */
	public Number getValue() {
		return value;
	}

	/**
	 * Check compatiblity between parameters.
	 * 
	 * @param model
	 *            the model
	 * @return true, if checks if is compatible
	 */
	public boolean isCompatible(PipePort model) {
		return (model instanceof ParamNumber);
	}

	/**
	 * Set the parameter. The value must be of Number type.
	 * 
	 * Now institutes type-safe censoring (clamping).(12/2008 bl)
	 * 
	 * @param value
	 *            number value
	 */
	public void setValue(Number value) {
		if(value.doubleValue()>max.doubleValue())
			value=max;
		else if(value.doubleValue()<min.doubleValue())
			value=min;			
		this.value = value;
	}

	/**
	 * Set the parameter. The value can be a String
	 * 
	 * @param str
	 *            the str
	 */
	public abstract void setValue(String str);

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#toString()
	 */
	public String toString() {
		return String.format("%6f", value.doubleValue());
	}
	public boolean equals(ParamModel<Number> model){
		Number value1=this.getValue();
		Number value2=model.getValue();
		if(value1==null&&value2==null)return true;
		if(value1==null||value2==null)return false;
		if(value1.doubleValue()==value2.doubleValue()){
			return true;
		} else {
			System.err.println(getClass().getCanonicalName()+"NUMBER "+this.getLabel()+": "+value1+" NOT EQUAL TO "+model.getLabel()+": "+value2);
			return false;
		}
	}
	
	 public String probeDefaultValue() {
		 return getValue().toString();
	 }
	 
	 @Override
	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);
		 Element em;				
		 em = document.createElement("value");		
		 em.appendChild(document.createTextNode(value+""));
		 parent.appendChild(em);
		 em = document.createElement("min");		
		 em.appendChild(document.createTextNode(min+""));
		 parent.appendChild(em);
		 em = document.createElement("max");		
		 em.appendChild(document.createTextNode(max+""));
		 parent.appendChild(em);
		 return true;
	 }
	 
	 
}
