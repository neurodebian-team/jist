package edu.jhu.ece.iacl.jist.structures.data;

import java.util.Comparator;

/**
 * Reverse comparator used for Binary Max Heap.
 * 
 * @param <T>  * 
 * @author Blake Lucas
 */
public class ReverseComparator<T extends Comparable<? super T>> implements Comparator<T>{
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(T obj0, T obj1) {
		return obj1.compareTo(obj0);
	}
};