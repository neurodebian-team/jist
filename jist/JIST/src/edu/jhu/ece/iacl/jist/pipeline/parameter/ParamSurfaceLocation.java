package edu.jhu.ece.iacl.jist.pipeline.parameter;

import edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileFactory;

public class ParamSurfaceLocation extends ParamFile{
	/**
	 * Default constructor.
	 */
	public ParamSurfaceLocation() {
		super(DialogType.FILE);
		setExtensionFilter(SurfaceReaderWriter.getInstance().getExtensionFilter());
		this.factory = new ParamFileFactory(this);
	}
	public ParamSurfaceLocation(String name){
		this();
		setName(name);
	}
}
