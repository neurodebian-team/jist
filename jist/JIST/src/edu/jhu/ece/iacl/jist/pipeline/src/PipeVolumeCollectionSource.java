/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;

/**
 * Iterate through list of volumes.
 * 
 * @author Blake Lucas
 */
public class PipeVolumeCollectionSource extends PipeFileCollectionSource {
	/**
	 * Default constructor.
	 */
	public PipeVolumeCollectionSource() {
		super();
		getParentPort().setParameter(fileParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume Collection");
		group.setName("volcollection");
		group.add(filecollection = new ParamVolumeCollection("Volumes"));
		group.setCategory("Volume");
		return group;
	}
	
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		filecollection = (ParamFileCollection)inputParams.getFirstChildByName("Volumes");
		fileParam = (ParamFile)outputParams.getFirstChildByName("Volume");
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume");
		group.add(fileParam = new ParamVolume("Volume"));
		return group;
	}
}
