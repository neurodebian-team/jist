/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of floats.
 * 
 * @author Blake Lucas
 */
public class PipeDoubleCollectionListSource extends PipeListCollectionSource {

	/** The float param. */
	protected ParamNumberCollection floatParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("floatParam");
//		floatParam.xmlEncodeParam(document, em);		
//		parent.appendChild(em);
//					
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		
		floatParam = (ParamNumberCollection) outputParams.getFirstChildByName("Floats");
//		floatParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"floatParam"));
		getParentPort().setParameter(floatParam);
	}
	/**
	 * Default constructor.
	 */
	public PipeDoubleCollectionListSource() {
		super();
		getParentPort().setParameter(floatParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Float Collection File List");
		group.setName("doublecollectionlist");
		group.setCategory("Number.Float");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Float");
		group.add(floatParam = new ParamNumberCollection("Floats"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamNumberCollection getOutputParam() {
		return floatParam;
	}

	/**
	 * Get value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected Object[] getValue(int i) {
		return (data[i]);
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object[] objArray) {
		for (Object obj : objArray) {
			try {
				floatParam.add(new Double(obj.toString()));
			} catch (NumberFormatException e) {
				System.err.println(getClass().getCanonicalName()+e.getMessage());
			}
		}
	}
}
