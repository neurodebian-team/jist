package edu.jhu.ece.iacl.jist.dialogs;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import gov.nih.mipav.model.algorithms.AlgorithmBase;
import gov.nih.mipav.model.algorithms.AlgorithmInterface;
import gov.nih.mipav.model.scripting.ImageVariableTable;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.ScriptRunner;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.view.dialogs.JDialogScriptableBase;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Enumeration;

// TODO: Auto-generated Javadoc
/**
 * The Class JDialogSaveAll.
 */
public class JDialogSaveAll extends JDialogScriptableBase implements AlgorithmInterface {
	
	/** The output dir. */
	protected File outputDir;
	
	/**
	 * Instantiates a new j dialog save all.
	 */
	public JDialogSaveAll(){
		
	}
	
	/* (non-Javadoc)
	 * @see gov.nih.mipav.view.dialogs.JDialogScriptableBase#callAlgorithm()
	 */
	protected void callAlgorithm() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see gov.nih.mipav.view.dialogs.JDialogScriptableBase#setGUIFromParams()
	 */
	protected void setGUIFromParams() {
		outputDir=new File(scriptParameters.getParams().getString("output_dir"));
	}

	/* (non-Javadoc)
	 * @see gov.nih.mipav.view.dialogs.JDialogScriptableBase#storeParamsFromGUI()
	 */
	protected void storeParamsFromGUI() throws ParserException {
		// TODO Auto-generated method stub
		
	}
    
    /* (non-Javadoc)
     * @see gov.nih.mipav.view.dialogs.JDialogScriptableBase#doPostAlgorithmActions()
     */
    protected void doPostAlgorithmActions() {
//    	throw new RuntimeException("NOT IMPLEMENTED");
		ImageVariableTable imgTable=ScriptRunner.getReference().getImageTable();
		ModelImageReaderWriter readerWriter=new ModelImageReaderWriter();
		if(imgTable!=null){
			Enumeration keys=imgTable.keys();
			while(keys.hasMoreElements()){
				String key=(String)keys.nextElement();
				ModelImage mimg=imgTable.getImage(key);
				System.out.println(getClass().getCanonicalName()+"\t"+"SAVING "+key+" "+mimg.getImageName());
				readerWriter.write(mimg, outputDir);
			}
		}
		//The image variable table is only visible during a script, so remember the object in case it becomes invisible
		MipavController.setImageVariableTable(imgTable);
		
    }
	
	/* (non-Javadoc)
	 * @see gov.nih.mipav.model.algorithms.AlgorithmInterface#algorithmPerformed(gov.nih.mipav.model.algorithms.AlgorithmBase)
	 */
	public void algorithmPerformed(AlgorithmBase algorithm) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
