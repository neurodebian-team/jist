/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.sql.Time;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

// TODO: Auto-generated Javadoc
/**
 * Summary of start and stop times for different tasks within an algorithm. This
 * class also records the used and total memory at the time a task completes.
 * 
 * @author Blake Lucas
 */
public class PerformanceSummary implements Cloneable {

	/**
	 * The Class TimeAndMemoryStamp.
	 */
	protected static class TimeAndMemoryStamp implements Cloneable {

		/** The task name. */
		public String taskName;

		/** The cpu start time. */
		public Time start;

		/** The cpu stop time. */
		public Time stop;

		/** Elapsed time in milliseconds. */
		public long elapsed;

		/** Used memory in MB. */
		public int usedMemory;

		/** Allocated memory in MB. */
		public int allocatedMemory;

		/** The actual start time. */
		public Time actualStart;

		/** The actual stop time. */
		public Time actualStop;

		/** The actual elapsed time. */
		public long actualElapsed;

		/**
		 * Default constructor.
		 */
		public TimeAndMemoryStamp() {
			Runtime r = Runtime.getRuntime();
			usedMemory = (int) ((r.totalMemory() - r.freeMemory()) / (1 << 20));
			allocatedMemory = (int) ((r.totalMemory()) / (1 << 20));
		}

		/**
		 * Default constructor.
		 * 
		 * @param name task name
		 * @param cpuStartTime start time in milliseconds
		 * @param cpuStopTime stop time in milliseconds
		 * @param actualStartTime the actual start time
		 * @param actualStopTime the actual stop time
		 */
		public TimeAndMemoryStamp(String name, long cpuStartTime, long cpuStopTime,long actualStartTime, long actualStopTime) {
			this();
			this.taskName = name;
			this.start = new Time(cpuStartTime);
			this.stop = new Time(cpuStopTime);
			this.actualStart=new Time(actualStartTime);
			this.actualStop=new Time(actualStopTime);
			this.elapsed = cpuStopTime - cpuStartTime;
			this.actualElapsed = actualStopTime - actualStartTime;
		}

		/**
		 * Returns clone of time and memory stamp.
		 * 
		 * @return the time and memory stamp
		 */
		public TimeAndMemoryStamp clone() {
			TimeAndMemoryStamp tstamp = new TimeAndMemoryStamp();
			tstamp.elapsed = elapsed;
			tstamp.actualElapsed=actualElapsed;
			tstamp.start = (Time) start.clone();
			tstamp.stop = (Time) stop.clone();
			tstamp.actualStart = (Time) actualStart.clone();
			tstamp.actualStop = (Time) actualStop.clone();
			tstamp.usedMemory = usedMemory;
			tstamp.allocatedMemory = allocatedMemory;
			tstamp.taskName = new String(taskName);
			return tstamp;
		}

		public void xmlEncodeParam(Document document, Element parent) {
			Element em;
			//			tstamp.elapsed = elapsed;
			em = document.createElement("elapsed");		
			em.appendChild(document.createTextNode(elapsed+""));
			parent.appendChild(em);
			//			tstamp.actualElapsed=actualElapsed;
			em = document.createElement("actualElapsed");		
			em.appendChild(document.createTextNode(actualElapsed+""));
			parent.appendChild(em);
			//			tstamp.start = (Time) start.clone();
			em = document.createElement("start");		
			em.appendChild(document.createTextNode(start+""));
			parent.appendChild(em);
			//			tstamp.stop = (Time) stop.clone();
			em = document.createElement("stop");		
			em.appendChild(document.createTextNode(stop+""));
			parent.appendChild(em);
			//			tstamp.actualStart = (Time) actualStart.clone();
			em = document.createElement("actualStart");		
			em.appendChild(document.createTextNode(actualStart+""));
			parent.appendChild(em);
			//			tstamp.actualStop = (Time) actualStop.clone();
			em = document.createElement("actualStop");		
			em.appendChild(document.createTextNode(actualStop+""));
			parent.appendChild(em);
			//			tstamp.usedMemory = usedMemory;
			em = document.createElement("usedMemory");		
			em.appendChild(document.createTextNode(usedMemory+""));
			parent.appendChild(em);
			//			tstamp.allocatedMemory = allocatedMemory;
			em = document.createElement("allocatedMemory");		
			em.appendChild(document.createTextNode(allocatedMemory+""));
			parent.appendChild(em);
			//			tstamp.taskName = new String(taskName);
			em = document.createElement("taskName");		
			em.appendChild(document.createTextNode(taskName+""));
			parent.appendChild(em);
			//			

		}

		public void xmlDecodeParam(Document document, Element item) {

			elapsed = Long.valueOf(JistXMLUtil.xmlReadTag(item, "elapsed"));
			actualElapsed = Long.valueOf(JistXMLUtil.xmlReadTag(item, "actualElapsed"));
			start = Time.valueOf(JistXMLUtil.xmlReadTag(item, "start"));
			stop = Time.valueOf(JistXMLUtil.xmlReadTag(item, "stop"));
			actualStart = Time.valueOf(JistXMLUtil.xmlReadTag(item, "actualStart"));
			actualStop = Time.valueOf(JistXMLUtil.xmlReadTag(item, "actualStop"));
			usedMemory= Integer.valueOf(JistXMLUtil.xmlReadTag(item, "usedMemory"));
			allocatedMemory= Integer.valueOf(JistXMLUtil.xmlReadTag(item, "allocatedMemory"));
			taskName= (JistXMLUtil.xmlReadTag(item, "taskName"));			
		}
	}

	/** List of time and memory stamps for different algorithm steps. */
	protected ArrayList<TimeAndMemoryStamp> timeAndMemStamps;

	/** Children performance summaries. */
	protected ArrayList<PerformanceSummary> children;

	/** Thread to get cpu time. */
	transient protected ThreadMXBean tb;

	/**
	 * Default constructor.
	 */
	public PerformanceSummary() {
		children = new ArrayList<PerformanceSummary>();
		timeAndMemStamps = new ArrayList<TimeAndMemoryStamp>();
		tb = ManagementFactory.getThreadMXBean();
	}

	/**
	 * Returns clone of performance summary.
	 * 
	 * @return the performance summary
	 */
	public PerformanceSummary clone() {
		PerformanceSummary summary = new PerformanceSummary();
		summary.timeAndMemStamps = (ArrayList<TimeAndMemoryStamp>) timeAndMemStamps.clone();
		;
		summary.children = (ArrayList<PerformanceSummary>) children.clone();
		return summary;
	}

	/**
	 * Format millisecond time to be in terms of days, hrs, minutes, and
	 * seconds.
	 * 
	 * @param time elapsed time in milliseconds
	 * 
	 * @return time string
	 */
	public static String formatTime(long time) {
		if(time==0){
			return " - ";
		}
		long days = time / (24 * 60 * 60 * 1000);
		long hours = (time % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000);
		long minutes = (time % (60 * 60 * 1000)) / (60 * 1000);
		double seconds = (time % (60 * 1000)) / (1000.0);
		String ret="";
		if (days > 0) {
			ret+=String.format("%2d day"+((days>1)?"s":""),days);
		}
		if (hours > 0) {
			ret+=String.format(((ret.length()>0)?" ":"")+"%2d hr"+((hours>1)?"s":""),hours);
		}
		if (minutes > 0) {
			ret+=String.format(((ret.length()>0)?" ":"")+"%2d min",minutes);
		}
		ret+=String.format(((ret.length()>0)?" ":"")+"%2.2f sec", seconds);
		return ret;
	}

	/**
	 * Get list of elapsed times in msec.
	 * 
	 * @return the all elapsed times
	 */
	public ArrayList<Long> getAllElapsedCPUTimes() {
		ArrayList<Long> times = new ArrayList<Long>();
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			times.add(stamp.elapsed);
		}
		for (PerformanceSummary child : children) {
			times.addAll(child.getAllElapsedCPUTimes());
		}
		return times;
	}

	/**
	 * Get list of elapsed times in msec.
	 * 
	 * @return the all elapsed times
	 */
	public ArrayList<Long> getAllElapsedActualTimes() {
		ArrayList<Long> times = new ArrayList<Long>();
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			times.add(stamp.actualElapsed);
		}
		for (PerformanceSummary child : children) {
			times.addAll(child.getAllElapsedActualTimes());
		}
		return times;
	}

	/**
	 * Get list of task names.
	 * 
	 * @return the all task names
	 */
	public ArrayList<String> getAllTaskNames() {
		ArrayList<String> tasks = new ArrayList<String>();
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			tasks.add(stamp.taskName);
		}
		for (PerformanceSummary child : children) {
			tasks.addAll(child.getAllTaskNames());
		}
		return tasks;
	}

	/**
	 * Get peak allocated memory.
	 * 
	 * @return the peak allocated memory
	 */
	public int getPeakAllocatedMemory() {
		int maxMem = 0;
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			maxMem = Math.max(stamp.allocatedMemory, maxMem);
		}
		return maxMem;
	}

	/**
	 * Get peak used memory.
	 * 
	 * @return the peak used memory
	 */
	public int getPeakUsedMemory() {
		int maxMem = 0;
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			maxMem = Math.max(stamp.usedMemory, maxMem);
		}
		return maxMem;
	}

	/**
	 * Return long summary of elapsed time.
	 * 
	 * @return the summary
	 */
	public String getSummary() {
		String ret = "---------- Performance Summary ----------\n";
		long totalElapsed = 0;
		long totalActualElapsed=0;
		int i = 0;
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			String taskName = stamp.taskName;
			totalElapsed += stamp.elapsed;
			totalActualElapsed+=stamp.actualElapsed;
			ret += ((i + 1) + ") " + taskName + " -- ELAPSED TIME: Actual: " + formatTime(stamp.actualElapsed)+" / CPU: "+formatTime(stamp.elapsed) + "\n");
			i++;
		}
		ret += ("\nTOTAL ELAPSED TIME: Actual: " + formatTime(totalActualElapsed)+" / CPU: "+formatTime(totalElapsed) + "\n");
		Runtime r = Runtime.getRuntime();
		ret += "CURRENT MEMORY: Used " +  ((r.totalMemory() - r.freeMemory()) / (1 << 20)) + " MB, Free "
		+  (r.freeMemory() / (1 << 20)) + " MB, Total " + (r.totalMemory()/ (1 << 20)) + " MB\n";
		return ret;
	}

	/**
	 * Get cumulative elapsed time for all tasks in msec.
	 * 
	 * @return the total elapsed time
	 */
	public long getTotalElapsedCPUTime() {
		long total = 0;
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			total += stamp.elapsed;
		}
		return total;
	}

	/**
	 * Get cumulative elapsed time for all tasks in msec.
	 * 
	 * @return the total elapsed time
	 */
	public long getTotalElapsedActualTime() {
		long total = 0;
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			total += stamp.actualElapsed;
		}
		return total;
	}

	/**
	 * Add child performance summary to this summary.
	 * 
	 * @param child the child
	 */
	public void record(PerformanceSummary child) {
		children.add(child);
	}

	/**
	 * Record completion of task.
	 * 
	 * @param label taskName
	 * @param startTime Start Time in msec
	 * @param stopTime Stop Time in msec
	 * @param realStartTime the real start time
	 * @param realStopTime the real stop time
	 */
	public void record(String label, long startTime, long stopTime,long realStartTime,long realStopTime) {
		TimeAndMemoryStamp tstamp = new TimeAndMemoryStamp(label, startTime, stopTime,realStartTime,realStopTime);
		if (label == null) {
			label = "TASK " + (timeAndMemStamps.size() + 1);
		}
		timeAndMemStamps.add(tstamp);
	}

	/**
	 * Return short summary of elapsed time.
	 * 
	 * @return the string
	 */
	public String toString() {
		long totalElapsed = 0;
		long realTotalElapsed=0;
		for (TimeAndMemoryStamp stamp : timeAndMemStamps) {
			totalElapsed += stamp.elapsed;
			realTotalElapsed += stamp.actualElapsed;
		}
		return "Actual: "+formatTime(realTotalElapsed)+" / CPU: "+formatTime(totalElapsed);
	}

	public boolean xmlEncodeParam(Document document, Element parent) {

		Element em; 
		boolean val = false;
		em = document.createElement("timeAndMemStamps");	
		for(TimeAndMemoryStamp c: timeAndMemStamps) {
			Element em2 = document.createElement("stamp");
			c.xmlEncodeParam( document, em2);						
			em.appendChild(em2);
			val = true;
		}
		if(val)
			parent.appendChild(em);

		/** Children performance summaries. */
		val = false;
		em = document.createElement("children");	
		for(PerformanceSummary c: children) {
			Element em2 = document.createElement("child");
			c.xmlEncodeParam( document, em2);						
			em.appendChild(em2);
			val = true;
		}
		if(val)
			parent.appendChild(em);
		return true;


	}

	public void xmlDecodeParam(Document document, Element parent) {
		timeAndMemStamps = new ArrayList<TimeAndMemoryStamp>();
		Element tm = JistXMLUtil.xmlReadElement(parent,"timeAndMemStamps");
		if(tm!=null) {
			Vector<Element> nl = JistXMLUtil.xmlReadElementList(tm,"stamp");
			for(Element el : nl) {
				TimeAndMemoryStamp a = new TimeAndMemoryStamp();
				a.xmlDecodeParam(document, el);				
				timeAndMemStamps.add(a);
			}			
		}		

		children = new ArrayList<PerformanceSummary>();
		Element ch = JistXMLUtil.xmlReadElement(parent,"children");
		if(ch!=null) {
			Vector<Element> nl = JistXMLUtil.xmlReadElementList(ch,"child");
			for(Element el : nl) {
				PerformanceSummary a = new PerformanceSummary();
				a.xmlDecodeParam(document, (Element)el);				
				children.add(a);

			}			
		}

	}
}
