package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.text.NumberFormat;
import java.util.Vector;

// TODO: Auto-generated Javadoc
/**
 * The Class ArrayDoubleReaderWriter.
 */
public class ArrayDoubleReaderWriter extends FileReaderWriter<double[][]>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The numformat. */
	private NumberFormat numformat;
	
	/** The Constant arrayReaderWriters. */
	private static final ArrayDoubleReaderWriter[]arrayReaderWriters=new ArrayDoubleReaderWriter[]{
		new ArrayDoubleTxtReaderWriter(),
		new ArrayDoubleDxReaderWriter()
	};
	
	/** The Constant readerWriter. */
	protected static final ArrayDoubleReaderWriter readerWriter=new ArrayDoubleReaderWriter();
	
	/**
	 * Gets the single instance of ArrayDoubleReaderWriter.
	 * 
	 * @return single instance of ArrayDoubleReaderWriter
	 */
	public static ArrayDoubleReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new array double reader writer.
	 * 
	 * @param filter the filter
	 */
	public ArrayDoubleReaderWriter(FileExtensionFilter filter){
		super(filter);
	}
	
	/**
	 * Instantiates a new array double reader writer.
	 */
	public ArrayDoubleReaderWriter() {
		super(new FileExtensionFilter());
		Vector<String> exts=new Vector<String>();
		for(ArrayDoubleReaderWriter reader:arrayReaderWriters){
			exts.addAll(reader.extensionFilter.getExtensions());
		}
		extensionFilter.setExtensions(exts);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected double[][] readObject(File f){
		for(ArrayDoubleReaderWriter reader:arrayReaderWriters){
			if(reader.accept(f)){
				return reader.readObject(f);
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(double[][] obj,File f){
		for(ArrayDoubleReaderWriter writer:arrayReaderWriters){
			if(writer.accept(f)){
				return writer.writeObject(obj,f);
			}
		}
		return null;
	}
	
	/**
	 * Sets the number format.
	 * 
	 * @param numformat the new number format
	 */
	public void setNumberFormat(NumberFormat numformat){
		this.numformat=numformat;
	}
}
