package edu.jhu.ece.iacl.jist.structures.geom;


// TODO: Auto-generated Javadoc
/**
 * The Class EmbeddedCurvePath.
 */
public class EmbeddedCurvePath extends CurvePath{
	
	/** The data. */
	double data=0;
	
	/**
	 * Instantiates a new embedded curve path.
	 */
	public EmbeddedCurvePath(){
		super();
	}
	
	/**
	 * Sets the value.
	 * 
	 * @param val the new value
	 */
	public void setValue(double val){
		this.data=val;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.geom.CurvePath#getValue()
	 */
	public double getValue() {
		return data;
	}
}
