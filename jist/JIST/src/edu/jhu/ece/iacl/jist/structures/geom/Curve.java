package edu.jhu.ece.iacl.jist.structures.geom;

import javax.vecmath.Point3f;

// TODO: Auto-generated Javadoc
/**
 * The Interface Curve.
 */
public interface Curve {
	
	/**
	 * Gets the curve.
	 * 
	 * @return the curve
	 */
	public Point3f[] getCurve();
	
	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public double getValue();

}
