/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of integers.
 * 
 * @author Blake Lucas
 */
public class PipeIntegerCollectionListSource extends PipeListCollectionSource {
	
	/** The integer param. */
	protected ParamNumberCollection integerParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);				
		Element em;
		
//		em = document.createElement("integerParam");
//		integerParam.xmlEncodeParam(document, em);				
//		parent.appendChild(em);
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);		
		integerParam =(ParamNumberCollection) outputParams.getFirstChildByName("Integers");// new ParamFile();
//		integerParam = new ParamNumberCollection();
//		integerParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"integerParam"));
		getParentPort().setParameter(integerParam);
	}
	/**
	 * Default constructor.
	 */
	public PipeIntegerCollectionListSource() {
		super();
		getParentPort().setParameter(integerParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Integer Collection File List");
		group.setName("integercollectionlist");
		group.setCategory("Number.Integer");
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Integer");
		group.add(integerParam = new ParamNumberCollection("Integers"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamNumberCollection getOutputParam() {
		return integerParam;
	}

	/**
	 * Get value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected Object[] getValue(int i) {
		return (data[i]);
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object[] objArray) {
		for(Object obj:objArray){
			try {
				integerParam.add(new Integer(obj.toString()));
			} catch (NumberFormatException e){
				System.err.println(getClass().getCanonicalName()+e.getMessage());
			}
		}
	}
}
