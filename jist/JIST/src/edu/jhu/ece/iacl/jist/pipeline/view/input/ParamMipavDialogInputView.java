/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.view.input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMipavDialog;
import gov.nih.mipav.view.dialogs.JDialogScriptableBase;

/**
 * Displays a mipav dialog to input information.
 * 
 * @author Blake Lucas
 */
public class ParamMipavDialogInputView extends ParamInputView implements ActionListener {
	
	/** The dialog. */
	private JDialogScriptableBase dialog;
	
	/** The view dialog. */
	private JButton viewDialog;

	/**
	 * Default constructor.
	 * 
	 * @param param
	 *            mipav dialog parameter
	 */
	public ParamMipavDialogInputView(ParamMipavDialog param) {
		super(param);
		viewDialog = new JButton("View Dialog");
		viewDialog.addActionListener(this);
		buildLabelAndParam(viewDialog);
		update();
	}

	/**
	 * Display mipav dialog when user clicks view button.
	 * 
	 * @param event
	 *            the event
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(viewDialog)) {
			if (dialog == null) {
				dialog = getParameter().getDialog();
			}
		}
	}

	/**
	 * Unimplemented.
	 */
	public void commit() {
		// TODO Auto-generated method stub
	}

	/**
	 * Get mipav dialog parameter.
	 * 
	 * @return the parameter
	 */
	public ParamMipavDialog getParameter() {
		return (ParamMipavDialog) param;
	}

	/**
	 * Unimplemented.
	 */
	public void update() {
	}
	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return null;
	}
}
