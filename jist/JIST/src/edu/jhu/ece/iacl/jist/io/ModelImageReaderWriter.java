/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.io;

import java.io.File;

import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.file.FileIO;
import gov.nih.mipav.model.file.FileWriteOptions;
import gov.nih.mipav.model.structures.ModelImage;

// TODO: Auto-generated Javadoc
/**
 * The Class ModelImageReaderWriter.
 */
public class ModelImageReaderWriter extends FileReaderWriter<ModelImage> {
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
    /** The supported file extensions. */
    public static String[] supportedFileExtensions = new String[] {"tif", "tiff", "lsm", "stk", "jpeg",

		"jpg", "bmp", "gif", "pict", "pic", "png", "psd", "dib", "tga",

		"xbm", "xpm", "xml", "pcx", "ico", "cur", "mgh", "mgz", "raw",

		"img", "nii", "nhdr", "nrrd", "ima", "dcm", "bin", "map", "mnc",

		"avi", "imc", "oly", "qt", "mov", "head", "brik", "ics", "ids",

		"hdr", "spm", "fits", "dm3", "tmg", "mrc", "wu", "sig", "gedno",

		"log", "ct", "info", "info~", "voi", "afni","par","parv2","rec","frec", "jp2",
		
		"gz","bz2"};
	
	/** The Constant readerWriter. */
	protected static final ModelImageReaderWriter readerWriter = new ModelImageReaderWriter();

	/**
	 * Gets the single instance of ModelImageReaderWriter.
	 * 
	 * @return single instance of ModelImageReaderWriter
	 */
	public static ModelImageReaderWriter getInstance() {
		return readerWriter;
	}

	/**
	 * Instantiates a new model image reader writer.
	 */
	public ModelImageReaderWriter() {
		super(new FileExtensionFilter(supportedFileExtensions));
		
		//Get preferred extension 
		this.extensionFilter.setPreferredExtension(JistPreferences.getPreferences().getPreferredExtension());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected ModelImage readObject(File f) {

			return readQuiet(f);
	}

	
	/**
	 * Read quiet.
	 * 
	 * @param f the f
	 * 
	 * @return the model image
	 */
	private static ModelImage readQuiet(File f) {
		ModelImage image = null;
		Refresher.getInstance().pauseAll();
		int index = f.getAbsolutePath().lastIndexOf(File.separatorChar);
		if (index <= 0) {
			Refresher.getInstance().resumeAll();
			return null;
		}
		for (ModelImage img : MipavController.getImages()) {
			File fimg=FileReaderWriter.getFullFileName(img);
			if (fimg!=null&&fimg.equals(f)) {
				Refresher.getInstance().resumeAll();
				JistLogger.logOutput(JistLogger.INFO,"Opening Volume by Cloning Volume in Memory: " + f.getAbsolutePath());
				return (ModelImage)img.clone();
			}
		}
		
		FileIO fileio = new FileIO();
		fileio.setQuiet(true);
		String fileName=f.getName();
		String fileDir=f.getParentFile().getAbsolutePath()+File.separatorChar;
		image = fileio.readImage(fileName,fileDir);
		Refresher.getInstance().resumeAll();
		if (image != null) {
			JistLogger.logOutput(JistLogger.INFO,"Opening Volume from Disk: " + f.getAbsolutePath());			
		} else {
			JistLogger.logError(JistLogger.SEVERE,"FAILED to Open Volume from Disk: " + f.getAbsolutePath());
		}
		
		return image;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(ModelImage img, File dir) {
		String name = img.getImageName();
		File f = new File(dir, name + "."+/*extensionFilter.getPreferredExtension()*/
				JistPreferences.getPreferences().getPreferredExtension());
		if ((f=writeObject(img, f))!=null) {
			JistLogger.logOutput(JistLogger.FINE, "Success - Wrote file : "+f);
			return f;
		} else {
			JistLogger.logError(JistLogger.FINE, "ERROR - Failed to write file : "+f);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(ModelImage img, File f) {
		return writeQuiet(img, f);
		/*if (MipavController.isQuiet())
			return writeQuiet(img, f);
		else
			return writeVerbose(img, f);
			*/
	}
	
	/**
	 * Write quiet.
	 * 
	 * @param img the img
	 * @param f the f
	 * 
	 * @return true, if successful
	 */
	private static File writeQuiet(ModelImage img,File f){
		Refresher.getInstance().pauseAll();
/*
		PrintStream oldOut = System.out;
		PrintStream oldErr = System.err;
		PrintStream tmpOut = null, tmpErr = null;
		File outFile = new File("temp.out");
		File errFile = new File("temp.err");
		try {
			System.setOut(tmpOut = new PrintStream(outFile));
			System.setErr(tmpErr = new PrintStream(errFile));
		} catch (FileNotFoundException e) {
//			e.printStackTrace();
			//don't warn the user here.
		}
		*/
		FileIO fileio = new FileIO();
		fileio.setQuiet(true);
		System.out.println("%% "+f.getName());
		try {
		System.out.println("%% "+f.getParentFile().getAbsolutePath());
		} catch (NullPointerException e) {
			
		}
		System.out.println("== "+f);
		System.out.flush();
		FileWriteOptions options=new FileWriteOptions(f.getName(),f.getParentFile().getAbsolutePath()+File.separator,true);

		options.setOptionsSet(true);
		int[] exts=img.getExtents();
		
		if(exts.length>2){
			options.setEndSlice(exts[2]-1);
			if(exts.length>3){
				options.setEndTime(exts[3]-1);
			}
		}
		
		if(f.exists())f.delete();
		fileio.writeImage(img,options);
		
		Refresher.getInstance().resumeAll();
		if (f != null) {
			JistLogger.logOutput(JistLogger.FINE, "SUCCESS - Wrote file : "+f);
			
		} else 
			JistLogger.logError(JistLogger.FINE, "ERROR - Failed to write file : "+f);
		
		// Need to hack file detection in order to remove compressed extension from XML files. 
		if(f.toString().toLowerCase().endsWith(".xml.gz")) {
			String s = f.toString();
			File f2 = new File(s.substring(0, s.length()-3));
			
			if (f2.exists())
				return f2;
			else return null;
			
			
		} else if(f.toString().toLowerCase().endsWith(".xml.bz2")) {
			String s = f.toString();
			File f2 = new File(s.substring(0, s.length()-4));
			if (f2.exists())
				return f2;
			else return null;
		} else {
			if (f.exists())
				return f;
			else return null;
		}
		
	
		
	}
	
	/**
	 * Write verbose.
	 * 
	 * @param img the img
	 * @param f the f
	 * 
	 * @return true, if successful
	 */
	@Deprecated
	private static boolean writeVerbose(ModelImage img, File f) {
		Refresher.getInstance().pauseAll();
		FileIO fileio = new FileIO();
		fileio.setQuiet(true);
		FileWriteOptions options=new FileWriteOptions(f.getName(),f.getParentFile().getAbsolutePath()+File.separator,true);
		options.setOptionsSet(true);
		int[] exts=img.getExtents();
		if(exts.length>2){
			
			options.setEndSlice(exts[2]-1);
			if(exts.length>3){
				options.setEndTime(exts[3]-1);
			}
		}
		if(f.exists())f.delete();
		fileio.writeImage(img,options);
		Refresher.getInstance().resumeAll();
		if (f != null) {
			JistLogger.logOutput(JistLogger.FINE, "SUCCESS - Wrote file : "+f);
		} else
			JistLogger.logError(JistLogger.FINE, "ERROR - Failed to write file : "+f);
		
		return (f.exists());
	}
}
