package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;

// TODO: Auto-generated Javadoc
/**
 * The Class RibbonVtkReaderWriter.
 */
public class RibbonVtkReaderWriter extends FileReaderWriter<CurveCollection>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final RibbonVtkReaderWriter readerWriter=new RibbonVtkReaderWriter();
	
	/**
	 * Gets the single instance of RibbonVtkReaderWriter.
	 * 
	 * @return single instance of RibbonVtkReaderWriter
	 */
	public static RibbonVtkReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new ribbon vtk reader writer.
	 */
	public RibbonVtkReaderWriter(){
		super(new FileExtensionFilter(new String[]{"vtk"}));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected CurveCollection readObject(File f) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	@Override
	protected File writeObject(CurveCollection collection, File f) {
		try {
			Point3f[][] curves=collection.getCurves();
			int[][] indices=new int[curves.length][0];
			int pointCount=0;
			for(int i=0;i<curves.length;i++){
				indices[i]=new int[curves[i].length];
				pointCount+=curves[i].length;
			}
			BufferedWriter data=new BufferedWriter(new FileWriter(f));
			data.append("# vtk DataFile Version 2.0\n"
					+"Splines\n"
					+"ASCII\n"
					+"DATASET UNSTRUCTURED_GRID\n"
					+"POINTS "+pointCount+" float\n");
			int i=0,j=0;
			Point3f p;
			int index=0;
			for(i=0;i<curves.length;i++){
				for(j=0;j<curves[i].length;j++){
					p=curves[i][j];
					data.append(p.x+" "+p.y+" "+p.z+"\n");
					indices[i][j]=index++;
				}
			}
			data.append("CELLS "+curves.length/2+" "+(pointCount+curves.length)+"\n");
			for(i=0;i<indices.length;i+=2){
				int[] curve1=indices[i];
				int[] curve2=indices[i+1];
				
				data.append(curve2.length+curve1.length+" ");
				for(j=0;j<curve1.length+curve2.length;j++){
					if(j%2==0){
						data.append(curve1[j/2]+" ");
					} else {
						data.append(curve2[j/2]+" ");
					}
				}
				data.append("\n");
			}
			
			data.append("CELL_TYPES "+curves.length/2+"\n");
			for(i=0;i<curves.length/2;i++)data.append("6 ");
			data.append("\nCELL_DATA "+curves.length/2+"\n"
					+"SCALARS Value float 1\n"
					+"LOOKUP_TABLE default\n");
			for(i=0;i<collection.size();i+=2){
				data.append(collection.get(i).getValue()+" ");
			}
			data.append("\n");
			data.close();
			return f;
		} catch (IOException e) {
			System.err.println(getClass().getCanonicalName()+e.getMessage());
		}
		return null;
	}

}
