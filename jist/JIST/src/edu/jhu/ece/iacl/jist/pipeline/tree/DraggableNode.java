/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.tree;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleFactory;

/**
 * A draggable node is a tree node that the user can drag.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public abstract class DraggableNode extends DefaultMutableTreeNode implements Transferable {
	
	/** The Constant MODULE_FLAVOR. */
	final public static DataFlavor MODULE_FLAVOR = new DataFlavor(DraggableNode.class, "Draggable Module");
	
	/** The factory. */
	protected PipeModuleFactory factory;
	
	/** The icon. */
	protected ImageIcon icon;
	
	/**
	 * Gets the icon.
	 * 
	 * @return the icon
	 */
	public ImageIcon getIcon(){
		return icon;
	}
	
	/**
	 * Default constructor.
	 */
	public DraggableNode() {
		super();
	}

	/**
	 * Default constructor.
	 * 
	 * @param factory
	 *            module factory
	 */
	public DraggableNode(PipeModuleFactory factory) {
		super(factory.getModuleName());
		this.factory = factory;
	}

	/**
	 * Get module factory.
	 * 
	 * @return the factory
	 */
	public PipeModuleFactory getFactory() {
		return factory;
	}

	/**
	 * Get module name for this node.
	 * 
	 * @return module name
	 */
	public String getName() {
		return factory.getModuleName();
	}
	
	public String getDescription() {
		return factory.getDescription();
	}


	/**
	 * implements Transferable interface.
	 * 
	 * @param df
	 *            the df
	 * @return the transfer data
	 * @throws UnsupportedFlavorException
	 *             the unsupported flavor exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
		if (df.equals(MODULE_FLAVOR)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(df);
		}
	}

	/**
	 * implements Transferable interface.
	 * 
	 * @return the transfer data flavors
	 */
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] { MODULE_FLAVOR };
	}

	/**
	 * Returns true if data flavor is module type.
	 * 
	 * @param df
	 *            the df
	 * @return true, if checks if is data flavor supported
	 */
	public boolean isDataFlavorSupported(DataFlavor df) {
		return df.equals(MODULE_FLAVOR);
	}
}
