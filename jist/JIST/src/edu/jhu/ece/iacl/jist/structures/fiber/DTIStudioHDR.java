package edu.jhu.ece.iacl.jist.structures.fiber;

import java.io.IOException;

import edu.jhu.ece.iacl.jist.io.LEFileReader;

// TODO: Auto-generated Javadoc
/**
 * The Class DTIStudioHDR.
 */
public class DTIStudioHDR {
    
    /** The Fiber nr. */
    public int FiberNR;
    
    /** The Fiber len max. */
    public int FiberLenMax;
    
    /** The Fiber len mean. */
    public float FiberLenMean;
    
    /** The Image size. */
    public int ImageSize[];
    
    /** The Voxel size. */
    public float VoxelSize[];
    
    /** The slice_ori. */
    public String slice_ori;
    
    /** The slice_seq. */
    public String slice_seq;
    
    /** The version num. */
    public int versionNum;
    
    /**
     * Instantiates a new dTI studio hdr.
     */
    public DTIStudioHDR() {
        ImageSize = new int[3];
        VoxelSize = new float[3];
    }
    
    /**
     * Instantiates a new dTI studio hdr.
     * 
     * @param imsize the imsize
     * @param voxsize the voxsize
     */
    public DTIStudioHDR(int[] imsize, float[] voxsize) {
        ImageSize = imsize;
        VoxelSize = voxsize;
    }

    /**
     * Read string.
     * 
     * @param fp the fp
     * @param len the len
     * 
     * @return the string
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String readString(LEFileReader fp, int len) throws IOException {
        if(len>0) {
            char c =(char)fp.readByte();
            return c+readString(fp,len-1);
        }
        return "";
    }

    /**
     * Read.
     * 
     * @param fp the fp
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void read(LEFileReader fp) throws IOException  {

        String FiberFileTag = readString(fp,8);
        if(FiberFileTag.compareTo("FiberDat")!=0)
            throw new IOException("Incompatible file type");


        FiberNR = fp.readInt();	 		
        FiberLenMax = fp.readInt(); 	
        FiberLenMean =fp.readFloat();	
        ImageSize[0]= fp.readInt();			
        ImageSize[1]= fp.readInt();		
        ImageSize[2]= fp.readInt();		
        VoxelSize[0]= fp.readFloat();	
        
        System.out.println(getClass().getCanonicalName()+"\t"+"Fiber NRis: " + FiberNR);
        System.out.println(getClass().getCanonicalName()+"\t"+"FLM is: " + FiberLenMax);
        System.out.println(getClass().getCanonicalName()+"\t"+"FLMn is: " + FiberLenMean);
        System.out.println(getClass().getCanonicalName()+"\t"+"Image Size 0 is: " + ImageSize[0]);	
        System.out.println(getClass().getCanonicalName()+"\t"+"Image Size 1 is: " + ImageSize[1]);
        System.out.println(getClass().getCanonicalName()+"\t"+"Image Size 2 is: " + ImageSize[2]);
        System.out.println(getClass().getCanonicalName()+"\t"+"Voxel Size 0 is: " + VoxelSize[0]);
        
//        int test = fp.readIntBE();
//        VoxelSize[0]=Float.intBitsToFloat(test);
//        System.out.println(getClass().getCanonicalName()+"\t"+"read int and got: " +Integer.toBinaryString(test));
//        System.out.println(getClass().getCanonicalName()+"\t"+"Read float 1f and got: " + Float.floatToIntBits(fp.readFloat()));
        
        
        VoxelSize[1]= fp.readFloat();	
        VoxelSize[2]= fp.readFloat();	
        int so= fp.readInt();	
        
        System.out.println(getClass().getCanonicalName()+"\t"+"Voxel Size 1 is: " + VoxelSize[1]);
        System.out.println(getClass().getCanonicalName()+"\t"+"Voxel Size 2 is: " + VoxelSize[2]);
        System.out.println(getClass().getCanonicalName()+"\t"+"orientation is: " + so);
        switch(so){
            case 0:
                slice_ori = "Coronal";
                break;
            case 1:
                slice_ori = "Axial";
                break;
            case 2:
                slice_ori = "Sagittal";
                break;
            default:
                slice_ori = "Unknown";
                break;
        }

        so=fp.readInt();				
        System.out.println(getClass().getCanonicalName()+"\t"+"slice sequence is: " + so);
        
        switch(so) {
            case 0: slice_seq = "Positive";
                break;
            case 1: slice_seq = "Negative";
                break;
            default: slice_seq = "Unknown";
                break;
        }

        String version=readString(fp,8);
        if(0==version.compareTo("2005."))
            versionNum=2005;
        else
            versionNum=2000;

        //System.out.println(getClass().getCanonicalName()+"\t"+"************ END OF HEADER ************");
        fp.seek(128);
    }
}
