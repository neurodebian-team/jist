package edu.jhu.ece.iacl.jist.cli;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import org.apache.commons.cli.ParseException;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeExternalDestination;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parser.JistLayoutParser;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeExternalSource;

public class runLayout {


	public static void main(String []args) {

		String layoutName = args[0];
		try { 
			PipeAlgorithm module = new JistLayoutParser().parsePipeAlgorithm(new File(layoutName));
			JistCLI cli = new JistCLI(module);
			cli.addOption("xClean", false, "Remove all previously generated results in the output directory");
			try {
				cli.parse(args);
			} catch (ParseException e) {

				System.out.println("cli"+"\t"+"####################################################################");
				System.out.println("cli"+"\t"+"Parse error: "+e.getMessage());
				System.out.println("cli"+"\t"+"####################################################################");
				if(cli.showHelp()) {
					System.out.println("cli"+"\t"+cli.getHumanReadableHelpMessage());
					if(args.length<=2)
						System.exit(0);
				}
				System.exit(-1);
			}

			if(cli.showHelp()) {
				System.out.println("cli"+"\t"+cli.getHumanReadableHelpMessage());
				if(args.length<=2)
					System.exit(0);
			}

			System.out.println("cli"+"\t"+"####################################################################");
			System.out.println("cli"+"\t"+"Initializing MIPAV/JIST Framework");
			// Hide Mipav GUI
			MipavController.setQuiet(true);
			MipavController.init();
			// Load preferences
			PipeLibrary.getInstance().loadPreferences(true);

			System.out.println("cli"+"\t"+"####################################################################");
			System.out.println("cli"+"\t"+"Interpretting command line arguments");
			cli.unmarshal();
			PipeAlgorithm pipe = cli.getPipe();			
			System.out.println("cli"+"\t"+"####################################################################");
			System.out.print(cli.getParseStatus());
			System.out.println("cli"+"\t"+"####################################################################");
			if(cli.encounteredParseError()) {
				System.out.println("cli"+"\t"+"Exiting with errors.");
				System.exit(-1);
			}

			boolean status = bootstrap(pipe,cli.getOutDir(),cli.getOutFile(),cli.getOptionValue("xClean")!=null);
			System.out.println("cli"+"\t"+"####################################################################");
			System.out.println("cli"+"\t"+"Done: "+layoutName);
			if(status)
				System.out.println("cli"+"\t"+"SUCCESS");
			else
				System.out.println("cli"+"\t"+"FAILED");
			System.out.println("cli"+"\t"+"####################################################################");

			System.exit(0);


		} catch (Exception e) {
			System.out.println("cli"+"\t"+"Usage: edu.jhu.ece.iacl.jist.cli.run [classname] -help");
			System.out.println("cli"+"\t"+"Usage: edu.jhu.ece.iacl.jist.cli.run [classname] [run options]");
			System.out.println("cli"+"\t"+"PARSE Error: "+e.getMessage());
			e.printStackTrace();
			System.exit(-1);
		}
		System.exit(0);
	}


	public static boolean bootstrap(PipeAlgorithm pipe, File outDir, File outFile, boolean clean) {

		PipeLayout layout=((ParamObject<PipeLayout>)pipe.getInputParams().getFirstChildByLabel("JIST Layout")).getObject();
		layout.getRunParameters().setOutputDirectory(outDir);

		layout.getRunParameters().setMaxProcs(1);	// Use 1 CPU
		//		 Should be taken care of by unmarshal 
		Vector<PipeModule> descendants=layout.getAllDescendantPipes();
		for(PipeModule mod:descendants){			
			if(mod instanceof PipeExternalSource){
				ParamModel targetVal=((PipeExternalSource) mod).getOutputParam();					
				targetVal.setOwner(null);
			}
		}
		ProcessManager pm=new ProcessManager();
		pm.setShowManager(false);					// Always hide
		if(clean)
			pm.cleanAllQuietly(layout);


		if(pm.runAndWait(layout,true)){
			pm.processDestinations();			
			ParamModel s=null;
			try {
				FileWriter fw = new FileWriter(outFile);
				ParamCollection outputs = pipe.getInputParams(); outputs = (ParamCollection) outputs.getValue(outputs.size()-1);

				for(PipeModule mod:layout.getAllDescendantPipes()){
					if(mod instanceof PipeExternalDestination) {
						PipeExternalDestination mod2 = (PipeExternalDestination)mod;
						for(int i=0;i<outputs.size();i++) {
							ParamModel p = outputs.getValue(i);
							if(p.getName().equals(mod2.getOutputParams().getValue(0).getName())) {
								fw.write(p.getCliTag()+ "=" + mod2.getInputParam().getXMLValue()+"\n");
								System.out.println("cli"+"\t"+p.getCliTag()+ "=" + mod2.getInputParam().getXMLValue()+"\n");
							}
						}
					}					
				}
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			pm.forceQuit();

			return true;
		} else {
			// Failed 
			pm.forceQuit();
			
			return false;
		}
		
	}

}
