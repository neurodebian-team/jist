package edu.jhu.ece.iacl.jist.cli;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.Vector;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;

public class discover {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length<1 || args[0].equalsIgnoreCase("--help")|| args[0].equalsIgnoreCase("-h")) {
			System.out.println("cli"+"\t"+"edu.jhu.ece.iacl.jist.cli.discover -h : print this message");
			System.out.println("cli"+"\t"+"edu.jhu.ece.iacl.jist.cli.discover [classpath to search]");
			System.out.println("cli"+"\t"+"");
			System.out.println("cli"+"\t"+"Search the provided classpath for classes that extend ProcessingAlgorithm.");
			System.out.println("cli"+"\t"+"Results are provided in a two column tab delimited format");
			System.out.println("cli"+"\t"+"The left column is the human readable name. The left column is the full class name.");
			System.out.println("cli"+"\t"+"The full path name may be passed to edu.jhu.ece.iacl.jist.cli.run to describe");
			System.out.println("cli"+"\t"+"command line parameters or run via the command line.");
			System.out.println("cli"+"\t"+"");
			System.out.println("cli"+"\t"+"Java Image Science Toolkit (JIST) Command Line Interface v"+JistCLI.VERSION);
			
		}
			
		if(args.length<1)
			return;
		
		String[] strs = args[0].split(File.pathSeparator);

		
		try {
			PrintStream err = System.err;
			System.setErr(null);
			Vector<Class> found =getAllClasses(strs, ProcessingAlgorithm.class);
			System.setErr(err);
			for(Class<ProcessingAlgorithm> mod : found) {
				try {
					ProcessingAlgorithm alg = mod.newInstance();
					String name = alg.getAlgorithmName().replace("\t", " ");					
					System.out.print(name+" ");
					for(int i=name.length();i<39;i++)
						System.out.print(" ");
					System.out.print("\t");
					System.out.println("cli"+"\t"+mod.getCanonicalName());
				} catch (IllegalAccessException e) {} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("cli"+"\t"+"HUH?:"+mod.getCanonicalName());
				} 
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Vector<Class> getAllClasses(String []strs,Class ofType) throws IOException {
		//		String[] strs = System.getProperty("java.class.path").split(File.pathSeparator);
		LinkedList<File> dirs = new LinkedList<File>();
		LinkedList<String> packs = new LinkedList<String>();
		Vector<Class> classes = new Vector<Class>();
		String pckg;
		for (String str : strs) {
//			System.out.println("cli"+"\t"+str);
			File f = new File(str);//, rootPckg.replace(".", File.separator));
			if (f.exists() && f.isDirectory()) {
				//				System.out.println("cli"+"\t"+"\tdir");
				dirs.add(f);
				packs.add("");
			}
		}
		while (dirs.size() > 0) {
			File dir = dirs.removeFirst();
			String pack = packs.removeFirst();
			File[] files = dir.listFiles();
			// Test all files in directory
			for (File f : files) {
				if (f.isDirectory()) {
					dirs.add(f);
					if (pack.length() > 0) {
						packs.add(pack + "." + FileReaderWriter.getFileName(f));
					} else {
						packs.add(FileReaderWriter.getFileName(f));
					}
					//					System.out.println("cli"+"\t"+"DIR: "+f.getCanonicalPath()+"\t"+packs.getLast());
				} else if (FileReaderWriter.getFileExtension(f).equals("class")) {

					if (pack.length() > 0) {
						pckg = pack + "." + FileReaderWriter.getFileName(f);
					} else {
						pckg = FileReaderWriter.getFileName(f);
					}
					try {
						Class c = Class.forName(pckg).asSubclass(ofType);
						
						if (c != null) {
							if(!Modifier.isAbstract(c.getModifiers()))
								classes.add(c);
						}
					} catch (ClassCastException e) {
					} catch (ClassNotFoundException e) {					
					} catch (UnsatisfiedLinkError e) {
					} catch (VerifyError e) {
					} catch (Error e) {
					}
				}
			}
		}
		return classes;
	}
}
