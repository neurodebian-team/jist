package edu.jhu.ece.iacl.jist.plugins;


import java.awt.Dimension;
import java.io.File;
import java.util.Vector;


import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.InvalidJistMergeException;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeExternalDestination;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamExecutable;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.src.PipeExternalSource;


/**
 * The Class MedicAlgorithmJistLayoutAdapter.
 */
public class MedicAlgorithmJistLayoutAdapter extends ProcessingAlgorithm implements AbstractProcessingAlgorithmInterface {

	/** The volumes. */
	ParamVolumeCollection volumes;

	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param pipeFile the pipe file
	 */
	public void init(File pipeFile) {
		this.inputParams = (ParamCollection) ParamFactory.fromXML(pipeFile);
		// Last parameter is output
		this.outputParams = (ParamCollection) inputParams.getValue(inputParams.size() - 1);
		this.inputParams.remove(outputParams);
		outputParams.setHidden(false);
		performance = (ParamPerformance) outputParams.getFirstChildByClass(ParamPerformance.class);
		this.outputParams.setName(inputParams.getName());
		this.outputParams.setLabel(inputParams.getLabel());
	}


	/**
	 * Initialize parameters and algorithm.
	 * 
	 * @param pipe the pipe
	 */
	public void init(PipeAlgorithm pipe) {
		this.inputParams = pipe.getInputParams();
		this.outputParams = pipe.getOutputParams();
		this.outputParams.setName(inputParams.getName());
		this.outputParams.setLabel(inputParams.getLabel());
		performance = (ParamPerformance) outputParams.getFirstChildByClass(ParamPerformance.class);
	}


	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "\nAlgorithm Version: " + revnum + "\n";


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("Base");
		inputParams.setLabel("JIST Adapter");
		inputParams.setName("JIST_Adapter");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setDescription(shortDescription);
		info.add(new AlgorithmAuthor("Blake Lucas", "", ""));
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
	
		PipeLayout layout=((ParamObject<PipeLayout>)inputParams.getFirstChildByLabel("JIST Layout")).getObject();
		

		Vector<PipeExternalSource> extSources=new Vector<PipeExternalSource>();
		Vector<PipeExternalDestination> extDests=new Vector<PipeExternalDestination>();
		
		if(!((ParamBoolean)inputParams.getFirstChildByLabel("Use Layout Output Directory")).getValue()){
			String name=inputParams.getName();
			File outputDir=new File(this.getOutputDirectory(),name);
			if(!outputDir.exists()){
				outputDir.mkdir();
			}
			layout.getRunParameters().setOutputDirectory(outputDir);
		}
		layout.getRunParameters().setMaxProcs(1);
		Vector<PipeModule> descendants=layout.getAllDescendantPipes();
		for(PipeModule mod:descendants){
			if(mod instanceof PipeExternalSource){
				ParamModel targetVal=((PipeExternalSource) mod).getOutputParam();
				ParamModel sourceVal=inputParams.getFirstChildByName(mod.getLabel());
				if(sourceVal!=null){					
					PipeModule.forward(sourceVal, targetVal);
				}
			}
		}
		ProcessManager pm=new ProcessManager();
		pm.setShowManager(((ParamBoolean)inputParams.getFirstChildByLabel("Show Process Manager")).getValue());
		if(pm.runAndWait(layout, false)){
			pm.forceQuit();
			for(PipeModule mod:descendants){
				if(mod instanceof PipeExternalDestination){
					ParamModel sourceVal=((PipeExternalDestination) mod).getOutputParams().getValue(0);
					ParamModel targetVal=outputParams.getFirstChildByName(mod.getLabel());
					if(targetVal!=null){
						PipeModule.forward(sourceVal, targetVal);
					}
				}
			}
		} else {
			pm.forceQuit();
		}
	}
	
	public String reconcileAndMerge(ParamCollection inputParams2,
			ParamCollection outputParams2) throws InvalidJistMergeException{
		String msg = super.reconcileAndMerge(inputParams2, outputParams2);		
		PipeLayout layout=((ParamObject<PipeLayout>)inputParams.getFirstChildByLabel("JIST Layout")).getObject();
		
		if(!layout.validateAndVerify()) {
			throw new InvalidJistMergeException("Cannot validate nested layout object:"+layout.getTitle());
		}		
		return msg;		
	}
}
