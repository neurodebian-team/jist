package edu.jhu.ece.iacl.jist.structures.image;

import java.awt.Color;

// TODO: Auto-generated Javadoc
/**
 * Short Voxel Type.
 * 
 * @author Blake Lucas
 */
public class VoxelShort extends Voxel{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7710136393027394664L;
	
	/** The vox. */
	private short vox;

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getBoolean()
	 */
	public boolean getBoolean() {
		return (vox!=0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getShort()
	 */
	public short getShort() {
		return vox;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getColor()
	 */
	public Color getColor() {
		return new Color(vox,vox,vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getDouble()
	 */
	public double getDouble() {
		return (double)(vox);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#clone()
	 */
	public Voxel clone() {
		return new VoxelShort(vox);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public void set(Voxel v) {
		this.vox=v.getShort();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(boolean)
	 */
	@Override
	public void set(boolean vox) {
		this.vox=(short)((vox)?1:0);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(short)
	 */
	@Override
	public void set(short vox) {
		this.vox=vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(int)
	 */
	public void set(int vox) {
		this.vox=(short)vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(java.awt.Color)
	 */
	public void set(Color vox) {
		float[] hsb=new float[3];
		hsb=Color.RGBtoHSB(vox.getRed(),vox.getGreen(),vox.getBlue(),hsb);
		this.vox=(short)Math.round(255.0*hsb[2]);		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#set(double)
	 */
	public void set(double vox) {
		this.vox=(short)Math.round(vox);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#add(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelShort add(Voxel v) {
		return new VoxelShort((short)(getShort()+v.getShort()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#sub(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelShort sub(Voxel v) {
		return new VoxelShort((short)(getShort()-v.getShort()));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#mul(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelShort mul(Voxel v) {
		return new VoxelShort((short)(getShort()*v.getShort()));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#div(edu.jhu.ece.iacl.jist.structures.image.Voxel)
	 */
	public VoxelShort div(Voxel v) {
		return new VoxelShort((short)(getShort()/v.getShort()));

	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#neg()
	 */
	public Voxel neg() {
		return new VoxelShort((short)(-getShort()));
	}
	
	/**
	 * Instantiates a new voxel short.
	 * 
	 * @param b the b
	 */
	public VoxelShort(short b){
		set(b);
	}
	
	/**
	 * Instantiates a new voxel short.
	 */
	public VoxelShort(){
		this.vox=0;
	}
	
	/**
	 * Instantiates a new voxel short.
	 * 
	 * @param v the v
	 */
	public VoxelShort(Voxel v){set(v);}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Voxel obj) {
		return (int)(this.getShort()-((Voxel)obj).getShort());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getInt()
	 */
	public int getInt() {
		return vox;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#toString()
	 */
	public String toString(){
		return (vox+"");
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.image.Voxel#getType()
	 */
	public VoxelType getType() {
		return VoxelType.SHORT;
	}
}
