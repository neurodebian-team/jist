/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;


import java.awt.geom.Rectangle2D;
import java.security.InvalidParameterException;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipePort.PortListener;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleCell;
import edu.jhu.ece.iacl.jist.pipeline.gui.LayoutPanel;
import edu.jhu.ece.iacl.jist.pipeline.gui.PipeInternalFrame;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPoint;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;


/**
 * A Pipe Module is the generic class for graph objects. All modules can have
 * input and output ports to provide connections to other modules.
 * 
 * @author Blake Lucas
 */
public abstract class PipeModule implements PipePort.PortListener, ParamViewObserver {

	/**
	 * Listener class to monitor for updates to pipe modules.
	 * 
	 * @author Blake Lucas
	 */
	public static interface PipeListener extends PipePort.PortListener {
	}

	/**
	 * Forward information from the source port to destination port. This method
	 * avoids loading external resources to forward information.
	 * 
	 * @param src source port
	 * @param dest destination port
	 */
	public static void forward(PipePort src, PipePort dest) {
		try {
			if (dest instanceof ParamCollection) {
				ParamCollection collect = ((ParamCollection) dest);
				if (dest.getIncomingConnectors().size() == collect.getNumberOfAddedConnections()) {
					collect.clear();
				}
				collect.add(src);
			} else if (dest instanceof ParamPoint) {
				if (src instanceof ParamPoint) {
					((ParamPoint) dest).getParamX().setValue(((ParamPoint) src).getParamX().getValue());
					((ParamPoint) dest).getParamY().setValue(((ParamPoint) src).getParamY().getValue());
					((ParamPoint) dest).getParamZ().setValue(((ParamPoint) src).getParamZ().getValue());
				}
			} else if (dest instanceof ObjectCollection) {
				if (src instanceof ObjectCollection) {
					if(dest.getIncomingPorts().size()<=1)
						//If this only accepts one port and it is a collection, set all the values in the collection
						dest.setValue(src.getValue());
					else {
						//Otherwise, the destination is a collection and source is a collection. 
						ObjectCollection collect = ((ObjectCollection) dest);
						int index=dest.getIncomingPorts().indexOf(src);
						if(index>=0) {
							collect.setCollection(index,(ObjectCollection)src);
						}							


					}
					/*
					System.out.println("**&**");

					ObjectCollection collect = ((ObjectCollection) dest);
					int index=dest.getIncomingPorts().indexOf(src);
					if(index>=0)
						collect.set(index, src.getValue());
					else 
						dest.setValue(src.getValue());
					 */
					/*dest.setValue(src.getValue());
					ObjectCollection destcollect = ((ObjectCollection) dest);
					ObjectCollection srccollect = ((ObjectCollection) src);
					for(int i=0;i<srccollect.size();i++)  {
						System.out.println("ADD");
						destcollect.add(srccollect.getValue());
					}*/
				} else {
					// Add parameter to data collection
					ObjectCollection collect = ((ObjectCollection) dest);
					int index=dest.getIncomingPorts().indexOf(src);					
					collect.set(index, src.getValue());		

				}
			} else if (src instanceof ObjectCollection) {
				PipeConnector connect = src.getOutgoingConnector(dest);
				if (connect != null) {
					dest.setValue(((ObjectCollection) src).getValue(connect.getSourceIndex()));
				} else {
					dest.setValue(src.getValue());
				}
			} else {
				dest.setValue(src.getValue());
			}
		} catch (Exception e) {
			System.err.println("jist.base"+"COULD NOT FORWARD FROM " + src.getDescription() + " TO " + dest.getDescription());
			e.printStackTrace();
		}
		// System.out.println(getClass().getCanonicalName()+"\t"+"FORWARD "+src.getDescription()+" DEST
		// "+dest.getDescription());
	}

	/** Label to display for this parameter. */
	transient protected String layoutUniqueID;
	static transient protected int layoutUniqueIDIndex;
	static public void resetLayoutUniqueIndex() {
		layoutUniqueIDIndex = 0;
	}

	public String getUniqueID() {
		return layoutUniqueID;
	}

	public void setLayoutUniqueID() {
		layoutUniqueIDIndex++;
		layoutUniqueID = "Mod-"+layoutUniqueIDIndex+"-"+name;
	}

	/** Label to display for this parameter. */
	protected String label;

	/** Unique name to identify this parameter. */
	protected String name;

	/** Size of module in graph. */
	protected Rectangle2D bounds;

	/** Input and output ports for module. */
	protected Vector<PipePort> inputPorts, outputPorts;

	/** Input parameters, some of which may not be visible. */
	protected ParamCollection inputParams;

	/** Output parameters, some of which may not be visible. */
	protected ParamCollection outputParams;

	/** Module cell to render on graph. */
	transient protected PipeModuleCell cell;

	/** Listeners that listen for changes to this module, including input/output ports. */
	transient protected Vector<PipeListener> listeners;

	/** Execution context that represents this module. */
	transient protected ExecutionContext currentContext = null;

	/** Input hash representing map from child parameter name to parameter. */
	protected transient Hashtable<String, ParamModel> inputHash = null;

	/** Output hash representing map from child parameter name to parameter. */
	protected transient Hashtable<String, ParamModel> outputHash = null;

	/**
	 * Default constructor.
	 */
	public PipeModule() {
		listeners = new Vector<PipeListener>();
		inputPorts = new Vector<PipePort>();
		outputPorts = new Vector<PipePort>();
		inputParams = null;
		outputParams = null;
	}

	/**
	 * Add listener for this module.
	 * 
	 * @param listener module listener
	 */
	public void addListener(PipeListener listener) {
		if (!getListeners().contains(listener)) {
			getListeners().add(listener);
		}
	}

	/**
	 * Listen for connection event to occur on a port.
	 * 
	 * @param owner the owner
	 */
	public void connectAction(PipePort owner) {
		for (PortListener listener : getListeners()) {
			listener.connectAction(owner);
		}
	}

	/**
	 * Create default graph cell representing this module.
	 * 
	 * @return graph cell
	 */
	public abstract PipeModuleCell createModuleCell();

	/**
	 * Disconnect all incoming and outgoing ports for this module.
	 */
	public void disconnect() {
		for (PipePort p : inputPorts) {
			p.disconnect();
		}
		for (PipePort p : outputPorts) {
			p.disconnect();
		}
	}

	/**
	 * Listen for disconnect event to occur on a port.
	 * 
	 * @param owner the owner
	 * @param child the child
	 * @param wire the wire
	 */
	public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire) {
		for (PortListener listener : getListeners()) {
			listener.disconnectAction(owner, child, wire);
		}
	}

	/**
	 * Get all modules that may effect the input parameters to this module.
	 * 
	 * @return ancestor list
	 */
	public Vector<PipeModule> getAncestors() {
		Vector<PipeModule> list = new Vector<PipeModule>();
		getAncestors(list);
		return list;
	}

	/**
	 * Get all ancestor modules and append them to the list.
	 * 
	 * @param list list to append ancestors
	 */
	protected void getAncestors(Vector<PipeModule> list) {
		for (PipeModule parent : getParents()) {
			if (!list.contains(parent)) {
				list.add(parent);
				parent.getDescendants(list);
			}
		}
	}

	/**
	 * Rectangular bounds for graphical representation. This information is
	 * serialized.
	 * 
	 * @return the bounds
	 */
	public Rectangle2D getBounds() {
		return bounds;
	}

	/**
	 * Get Pipe Modules that receive data from this module.
	 * 
	 * @return the children
	 */
	public Vector<PipeModule> getChildren() {
		Vector<PipeModule> modules = new Vector<PipeModule>();
		for (PipePort port : getOutputPorts()) {
			for (PipePort child : (Vector<PipePort>) port.getOutgoingPorts()) {
				PipeModule mod = child.getOwner();
				if (!modules.contains(mod)) {
					modules.add(mod);
				}
			}
		}
		return modules;
	}

	/**
	 * Get current context used to execute this module.
	 * 
	 * @return current context representing this module
	 */
	public ExecutionContext getCurrentContext() {
		return currentContext;
	}

	/**
	 * Get all modules that are effected by information outputted by this
	 * module.
	 * 
	 * @return the descendants
	 */
	public Vector<PipeModule> getDescendants() {
		Vector<PipeModule> list = new Vector<PipeModule>();
		getDescendants(list);
		return list;
	}

	/**
	 * Get all descendant modules and append them to the list.
	 * 
	 * @param list list to append parameters
	 */
	protected void getDescendants(Vector<PipeModule> list) {
		for (PipeModule child : getChildren()) {
			if (!list.contains(child)) {
				list.add(child);
				child.getDescendants(list);
			}
		}
	}

	/**
	 * Get input hash representing map from child parameter name to parameter.
	 * 
	 * @return input hash
	 */
	public Hashtable<String, ParamModel> getInputHash() {
		if (inputHash == null) {
			inputHash=inputParams.getDescendantChildrenHash();
			/*
			inputHash = new Hashtable<String, PipePort>();
			for (PipePort child : inputPorts) {
				inputHash.put(child.getName(), child);
			}
			 */
		}
		return inputHash;
	}

	/**
	 * Get input parameters for this module.
	 * 
	 * @return module input parameters
	 */
	public ParamCollection getInputParams() {
		return inputParams;
	}

	/**
	 * Get output parameters for this module.
	 * 
	 * @return output module output parameters
	 */
	public Vector<PipePort> getInputPorts() {
		return inputPorts;
	}

	/**
	 * Get display label for this module.
	 * 
	 * @return label name
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Get all listeners for this module.
	 * 
	 * @return module listeners
	 */
	public Vector<PipeListener> getListeners() {
		if (listeners == null) {
			listeners = new Vector<PipeListener>();
		}
		return listeners;
	}

	/**
	 * Get graph cell representing this module.
	 * 
	 * @return graph cell
	 */
	public PipeModuleCell getModuleCell() {
		return cell;
	}

	/**
	 * Get unique name that identifies this parameter.
	 * 
	 * @return unique name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get output hash representing map from child parameter name to parameter.
	 * 
	 * @return input hash
	 */
	public Hashtable<String, ParamModel> getOutputHash() {
		if (outputHash == null) {
			outputHash = outputParams.getDescendantChildrenHash();
		}
		return outputHash;
	}

	/**
	 * Get input ports.
	 * 
	 * @return input ports
	 */
	public ParamCollection getOutputParams() {
		return outputParams;
	}

	/**
	 * Get output ports.
	 * 
	 * @return output ports
	 */
	public Vector<PipePort> getOutputPorts() {
		return outputPorts;
	}

	/**
	 * Get modules that provide information to this module.
	 * 
	 * @return the parents
	 */
	public Vector<PipeModule> getParents() {
		Vector<PipeModule> modules = new Vector<PipeModule>();
		for (PipePort port : getInputPorts()) {
			for (PipePort child : (Vector<PipePort>) port.getIncomingPorts()) {
				PipeModule mod = child.getOwner();
				if (!modules.contains(mod)) {
					modules.add(mod);
				}
			}
		}
		return modules;
	}

	/**
	 * Initialize all transient data that could not be deserialized.
	 * 
	 * @param graph the graph
	 * 
	 * @return the pipe module cell
	 */
	public abstract PipeModuleCell init(PipeJGraph graph);

	/**
	 * Returns true if this is an ancestor of the specified module.
	 * 
	 * @param child the child
	 * 
	 * @return true, if checks if is ancestor of
	 */
	public boolean isAncestorOf(PipeModule child) {
		Vector<PipeModule> children = getChildren();
		if (children.contains(child)) {
			return true;
		} else {
			for (PipeModule mod : children) {
				if (mod.isAncestorOf(child)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns true if this is a descendant of the specified module.
	 * 
	 * @param parent module
	 * 
	 * @return true, if checks if is descendant of
	 */
	public boolean isDescendantOf(PipeModule parent) {
		Vector<PipeModule> parents = getParents();
		if (parents.contains(parent)) {
			return true;
		} else {
			for (PipeModule mod : parents) {
				if (mod.isDescendantOf(parent)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns true if this module has no incoming connections.
	 * 
	 * @return true, if checks if is root
	 */
	public boolean isRoot() {
		for (PipePort port : getInputPorts()) {
			if (((Vector<PipePort>) port.getIncomingConnectors()).size() > 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Forward information from output ports to all child input ports.
	 * 
	 * @return true, if push
	 */
	public boolean push() {
		for (PipePort outport : getOutputPorts()) {
			for (PipePort inport : (Vector<PipePort>) outport.getOutgoingPorts()) {

				forward(outport, inport);
				inport.getOwner().setCurrentContext(null);
				for (PipeModule child : inport.getOwner().getDescendants()) {
					child.setCurrentContext(null);
				}
			}
		}
		return true;
	}

	/**
	 * Remove all listeners for this module.
	 */
	public void removeAllListeners() {
		getListeners().removeAllElements();
	}

	/**
	 * Remove listener for this module.
	 * 
	 * @param listener module listener
	 */
	public void removeListener(PipeListener listener) {
		getListeners().remove(listener);
	}

	/**
	 * Set dimensions for graph cell.
	 * 
	 * @param bounds cell dimensions
	 */
	public void setBounds(Rectangle2D bounds) {
		this.bounds = bounds;
	}

	/**
	 * Set current context used to execute this module.
	 * 
	 * @param context the context
	 */
	public void setCurrentContext(ExecutionContext context) {
		this.currentContext = context;
	}

	/**
	 * Set display label for this module.
	 * 
	 * @param label name
	 */
	public void setLabel(String label) {
		inputParams.setLabel(label);
		if (cell != null) {
			cell.setUserObject(label);
		}
		this.label = label;
	}

	/**
	 * Set graph cell representing this module.
	 * 
	 * @param cell graph cell
	 */
	public void setModuleCell(PipeModuleCell cell) {
		this.cell = cell;
	}

	/**
	 * Set unique name that identifies this parameter.
	 * 
	 * @param name unique name
	 */
	public void setName(String name) {
		inputParams.setName(name);
		this.name = name;
	}

	/**
	 * Refresh the graph frame and indicate that the layout has changed.
	 * 
	 * @param model the model
	 * @param view the view
	 */
	public void update(ParamModel model, ParamInputView view) {
		PipeInternalFrame frame = LayoutPanel.getInstance().getActiveFrame();
		if (frame != null) {
			frame.getGraph().refresh();
			frame.getPipelineLayout().setDirty(true);
		}
	}

	protected boolean xmlEncodeModule(Document document, Element parent) {

		Element em;

		em = document.createElement("layoutUniqueID");
		em.appendChild(document.createTextNode(layoutUniqueID));
		parent.appendChild(em);

		/** Classname to display for this parameter. */
		em = document.createElement("classname");		
		
		em.appendChild(document.createTextNode(this.getClass().getCanonicalName()));
		parent.appendChild(em);

		/** Label to display for this parameter. */
		em = document.createElement("label");		
		em.appendChild(document.createTextNode(label));
		parent.appendChild(em);

		/** Unique name to identify this parameter. */		
		em = document.createElement("name");		
		em.appendChild(document.createTextNode(name));
		parent.appendChild(em);

		/** Size of module in graph. */
		em = document.createElement("bounds");	
		Element subel;
		em.appendChild(subel=document.createElement("x"));
		subel.appendChild(document.createTextNode(bounds.getBounds2D().getX()+""));
		em.appendChild(subel=document.createElement("y"));
		subel.appendChild(document.createTextNode(bounds.getBounds2D().getY()+""));
		em.appendChild(subel=document.createElement("width"));
		subel.appendChild(document.createTextNode(bounds.getBounds2D().getWidth()+""));
		em.appendChild(subel=document.createElement("height"));
		subel.appendChild(document.createTextNode(bounds.getBounds2D().getHeight()+""));
		parent.appendChild(em);		

//		/** Input parameters, some of which may not be visible. */
		em = document.createElement("inputParams");
		if(inputParams.xmlEncodeParam(document,em))
			parent.appendChild(em);
//
//		/** Output parameters, some of which may not be visible. */
		em = document.createElement("outputParams");
		if(outputParams.xmlEncodeParam(document,em))
			parent.appendChild(em);
//		



		return true;

	}



	public boolean xmlEncodeConnections(Document document, Element parent) {
		boolean val = false;
		Element em;
		for(PipePort port : inputPorts) {
			if(port.xmlEncodeConnections(document,parent))
				val=true;
		}
		return val;
		// TODO Auto-generated method stub
		//	/	sfs
		// ?? 
		//TODO
		/** Input and output ports for module. */
		//		protected Vector<PipePort> inputPorts, outputPorts;

	}

	public void xmlDecodeModule(Document document, Element el) {
		
		
		layoutUniqueID = JistXMLUtil.xmlReadTag(el,"layoutUniqueID");
		
		label = JistXMLUtil.xmlReadTag(el,"label");
		
		name = JistXMLUtil.xmlReadTag(el,"name");
				
		/** Size of module in graph. */
		Element em = JistXMLUtil.xmlReadElement(el,"bounds");
		String x = JistXMLUtil.xmlReadTag(em,"x");
		String y = JistXMLUtil.xmlReadTag(em,"y");
		String w = JistXMLUtil.xmlReadTag(em,"width");
		String h = JistXMLUtil.xmlReadTag(em,"height");
		bounds = new Rectangle2D.Float(Float.valueOf(x), Float.valueOf(y),
				Float.valueOf(w), Float.valueOf(h));
				

		inputParams = new ParamCollection();
		inputParams.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"inputParams"));
		outputParams = new ParamCollection();
		outputParams.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"outputParams"));

		listeners = new Vector<PipeListener>();
		inputPorts = new Vector<PipePort>();
		outputPorts = new Vector<PipePort>();
	}

	abstract public String reconcileAndMerge() throws InvalidJistMergeException;
}
