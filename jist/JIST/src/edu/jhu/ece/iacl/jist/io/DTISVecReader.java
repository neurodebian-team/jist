package edu.jhu.ece.iacl.jist.io;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;



// TODO: Auto-generated Javadoc
/**
 * The Class DTISVecReader.
 */
public class DTISVecReader {
	
	/** The out. */
	ImageDataFloat out;
	
	/**
	 * Read float vec img.
	 * 
	 * @param filename the filename
	 * @param xdim the xdim
	 * @param ydim the ydim
	 * @param zdim the zdim
	 * 
	 * @return the image data float
	 */
	public static ImageDataFloat readFloatVecImg(String filename, int xdim, int ydim, int zdim) {
		
		ImageDataFloat out = new ImageDataFloat(3,xdim,ydim,zdim);

		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));

			for(int k=0; k<zdim;k++){
				for(int j=0; j<ydim;j++){
					for(int i=0; i<xdim;i++){
						out.set(0, i, j, k, imin.readFloat());
						out.set(1, i, j, k, imin.readFloat());
						out.set(2, i, j, k, imin.readFloat());
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return out;
	}

}
