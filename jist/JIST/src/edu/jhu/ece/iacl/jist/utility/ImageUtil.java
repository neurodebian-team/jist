package edu.jhu.ece.iacl.jist.utility;

import gov.nih.mipav.model.structures.ModelImage;

import java.util.SortedSet;
import java.util.TreeSet;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageUtil.
 */
public class ImageUtil {
	
	/**
	 * Sets the safe min max.
	 * 
	 * @param img the new safe min max
	 */
	public static void setSafeMinMax(ModelImage img) {
		int []extents  = img.getExtents();
		double max = Double.NEGATIVE_INFINITY;
		double min = Double.POSITIVE_INFINITY;
		if(extents.length==4) {
			int cnt = (int)(extents[0]*extents[1]*extents[2]*extents[3]/20);
			
			double []minbuffer = new double[cnt];
			double []maxbuffer = new double[cnt];
			SortedSet mins = new TreeSet();
			SortedSet maxs = new TreeSet();			
		
			for(int i=0;i<cnt;i++) {
				Double pinf = new Double(i+1e14);
				Double ninf = new Double(-1e14-i);
				maxs.add(ninf);
				mins.add(pinf);				
			}
			for(int i=0;i<extents[0];i++){
				for(int j=0;j<extents[1];j++){
					for(int k=0;k<extents[2];k++){
						for(int l=0;l<extents[3];l++){
							double temp = img.getDouble(i,j,k,l);
							if(!(Double.isInfinite(temp) || Double.isNaN(temp))) {
								if(temp>((Double)maxs.first())) {
									maxs.remove(maxs.first());
									Double d = new Double(temp);
									maxs.add(d);									
								}
								if(temp<((Double)mins.last())) {  
									mins.remove(mins.last());
									Double d = new Double(temp);
									mins.add(d);					
								}
									
//								max = Math.max(temp, max);
//								min = Math.min(temp, min);								
							}

						}
					}
				}
			}
			img.setMax((Double)maxs.first());
			img.setMin((Double)mins.last());
		} else if(extents.length==3) {
			for(int i=0;i<extents[0];i++){
				for(int j=0;j<extents[1];j++){
					for(int k=0;k<extents[2];k++){

						double temp = img.getDouble(i,j,k);
						if(!(Double.isInfinite(temp) || Double.isNaN(temp))) {
							max = Math.max(temp, max);
							min = Math.min(temp, min);								
						}

					}
				}
			}
		} else {
			throw new RuntimeException("setSafeMinMax: Invalid model image type");
		}		
		
	}
}
