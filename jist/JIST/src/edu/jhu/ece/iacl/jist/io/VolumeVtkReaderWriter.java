package edu.jhu.ece.iacl.jist.io;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

// TODO: Auto-generated Javadoc
/**
 * VolumeVtkReaderWriter is a specific filetype for reading/writing volumes
 * Actual formatting is handled here because it is not included in MIPAV.
 */
public class VolumeVtkReaderWriter extends FileReaderWriter<ImageData> {
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final VolumeVtkReaderWriter readerWriter = new VolumeVtkReaderWriter();

	/**
	 * Instantiates a new volume vtk reader writer.
	 */
	public VolumeVtkReaderWriter() {
		super(new FileExtensionFilter(new String[] { "vtk" }));
	}

	/**
	 * Gets the single instance of VolumeVtkReaderWriter.
	 * 
	 * @return single instance of VolumeVtkReaderWriter
	 */
	public static VolumeVtkReaderWriter getInstance() {
		return readerWriter;
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(ImageData img,File dir){
		String name = img.getName();
		File f = new File(dir, name + ".vtk");
		if((f=writeObject(img,f))!=null){
			return f;
		} else {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(ImageData vol, File f) {
		int rows = vol.getRows();
		int cols = vol.getCols();
		int slices = vol.getSlices();
		int components = vol.getComponents();
		try {
			BufferedWriter data = new BufferedWriter(new FileWriter(f));
			String type="";
			switch(vol.getType()){
				case UBYTE:type="unsigned_char";break;
				case DOUBLE:type="double";break;
				case FLOAT:type="float";break;
				case SHORT:type="short";break;
				case USHORT:type="unsigned_short";break;
				case INT:type="int";break;
				case BOOLEAN:type="bit";break;
				case VECTORX:type="float";break;
			}
			if(vol.getType()==VoxelType.COLOR||vol.getType()==VoxelType.COLOR_FLOAT||vol.getType()==VoxelType.COLOR_USHORT){
				data.append("# vtk DataFile Version 2.0\n" + vol.getName() + "\n"
						+ "ASCII\n" + "DATASET STRUCTURED_POINTS\n" + "DIMENSIONS "
						+ rows + " " + cols + " " + slices + " \n"
						+ "ASPECT_RATIO 1 1 1\n" + "ORIGIN 0 0 0\n" + "POINT_DATA "
						+ (rows * cols * slices*components) + "\n" + "COLOR_SCALARS ImageFile 4\n");
			} else {
				data.append("# vtk DataFile Version 2.0\n" + vol.getName() + "\n"
						+ "ASCII\n" + "DATASET STRUCTURED_POINTS\n" + "DIMENSIONS "
						+ rows + " " + cols + " " + slices + " \n"
						+ "ASPECT_RATIO 1 1 1\n" + "ORIGIN 0 0 0\n" + "POINT_DATA "
						+ (rows * cols * slices) + "\n" + "SCALARS volume "+type+" "
						+ components + "\n" + "LOOKUP_TABLE default\n");
			}
			if (components < 2) {
				// This is single component volumetric data
				switch(vol.getType()){
				case UBYTE:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getUByte(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				case DOUBLE:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getDouble(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				case FLOAT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getFloat(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				case COLOR_FLOAT:
				case COLOR_USHORT:
				case COLOR:
					float[] colorArray=new float[4];
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								Color c=vol.getColor(i, j, k);
								c.getRGBComponents(colorArray);
								data.append(colorArray[0]+" "+colorArray[1]+" "+colorArray[2] +" "+ 255*colorArray[3]+" ");
							}
						}
						data.append("\n");
					}
					break;
				case SHORT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getShort(i, j, k) + " ");
							}
						}
						data.append("\n");
					}	
					break;
				case USHORT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getUShort(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				case INT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getInt(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				case BOOLEAN:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getShort(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				case VECTORX:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								data.append(vol.getFloat(i, j, k) + " ");
							}
						}
						data.append("\n");
					}
					break;
				}
				
			} else {
				// This is vector volumetric data
				switch(vol.getType()){
				case UBYTE:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getUByte(i, j, k, l) + " ");
								}
							}
						}
						data.append("\n");
					}
					break;
				case DOUBLE:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getDouble(i, j, k,l) + " ");
								}
							}
						}
						data.append("\n");
					}
					break;
				case FLOAT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getFloat(i, j, k, l) + " ");
								}
							}
						}
						data.append("\n");
					}	
					break;
				case COLOR_FLOAT:
				case COLOR_USHORT:
				case COLOR:
					float[] colorArray=new float[4];
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									Color c=vol.getColor(i, j, k,l);
									c.getRGBComponents(colorArray);
									data.append(colorArray[0]+" "+colorArray[1]+" "+colorArray[2] +" "+ 255*colorArray[3]+" ");
								}
							}
						}
						data.append("\n");
					}
					break;
				case SHORT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getShort(i, j, k, l) + " ");
								}
							}
						}
						data.append("\n");
					}	
					break;
				case USHORT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getUShort(i, j, k, l) + " ");
								}
							}
						}
						data.append("\n");
					}
					break;
				case INT:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getInt(i, j, k, l) + " ");
								}
							}
						}
						data.append("\n");
					}
					break;
				case BOOLEAN:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getShort(i, j, k, l) + " ");
								}
							}
						}
						data.append("\n");
					}
					break;
				case VECTORX:
					for (int k = 0; k < slices; k++) {
						for (int j = 0; j < cols; j++) {
							for (int i = 0; i < rows; i++) {
								for (int l = 0; l < components; l++) {
									data.append(vol.getFloat(i, j, k,l) + " ");
								}
							}
						}
						data.append("\n");
					}
					break;
				}
			}
			data.close();
			return f;
		} catch (IOException e) {
			System.err.println(getClass().getCanonicalName()+e.getMessage());
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected ImageData readObject(File f) {
		BufferedReader in;
		StringBuffer buff = new StringBuffer();
		EmbeddedSurface surf=null;
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));

			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		Pattern header;
		Matcher m;
		header=Pattern.compile("DataFile Version 2.0\\s+\\S+\\s+");
		m=header.matcher(buff);
		String name="unknown";
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\s+");	
			name=vals[3];
		}
		header=Pattern.compile("DIMENSIONS\\s+\\d+\\s+\\d+\\s+\\d+");
		m=header.matcher(buff);
		int rows=0,cols=0,slices=0,components=0;
		//Find dimensions
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>=3){
				rows=Integer.parseInt(vals[1]);
				cols=Integer.parseInt(vals[2]);
				slices=Integer.parseInt(vals[3]);
			}
		}
		header=Pattern.compile("SCALARS volume\\D+\\d+\\s+");
		m=header.matcher(buff);
		String type="float";
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>=2){
				components=Integer.parseInt(vals[1]);
			}
			vals=head.split("\\s+");
			type=vals[2].trim();
		}
		VoxelType vtype=VoxelType.FLOAT;
		if(type.equals("unsigned_char")){
			vtype=VoxelType.UBYTE;
		} else if(type.equals("double")){
			vtype=VoxelType.DOUBLE;
		} else if(type.equals("float")){
			vtype=VoxelType.FLOAT;
		} else if(type.equals("int")){
			vtype=VoxelType.INT;
		} else if(type.equals("short")){
			vtype=VoxelType.SHORT;
		} else if(type.equals("unsigned_short")){
			vtype=VoxelType.USHORT;
		}  else if(type.equals("bit")){
			vtype=VoxelType.BOOLEAN;
		}
		header=Pattern.compile("LOOKUP_TABLE default");
		int stIndex=m.end();
		m=header.matcher(buff);
		if(m.find()){
			stIndex=m.end();
		}
		header=Pattern.compile("\\S+");
		m=header.matcher(buff);
		int count=0;
		int i,j,k,l;
		int rcc=rows*cols*components;
		int rc=rows*components;
		ImageData vol=new ImageDataMipav(name,vtype,rows,cols,slices,components);
		while(m.find(stIndex)){
			String str=buff.substring(m.start(),m.end());
			k=count/(rcc);
			j=(count%(rcc))/(rc);
			i=(count%(rc))/components;
			l=count%components;
			if(components>1){
				vol.set(i, j, k,l,Double.parseDouble(str));	
			} else {
				vol.set(i, j, k,Double.parseDouble(str));
			}
			count++;
			stIndex=m.end();	
		}
		return vol;
	}
}
