/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.jgraph.graph.BasicMarqueeHandler;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.PortView;

import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmCell;
import edu.jhu.ece.iacl.jist.pipeline.gui.ModuleTreePanel;

/**
 * The Class PipeMarqueeHandler.
 */
public class PipeMarqueeHandler extends BasicMarqueeHandler {
	
	/** The Constant labelAngle. */
	private static final double labelAngle = Math.PI / 4;
	
	/** The graph. */
	PipeJGraph graph;
	
	/** The buff. */
	BufferedImage buff;
	// Holds the Start and the Current Point
	/** The current. */
	protected Point2D start, current;
	// Holds the First and the Current Port
	/** The port. */
	protected PortView port;
	
	/** The first port. */
	protected PortView firstPort;
	
	/** The edge selected. */
	protected boolean edgeSelected = false;
	
	/** The current port. */
	protected PortView currentPort = null;
	// Show Special Cursor if Over Port
	/** The last port. */
	private PortView lastPort = null;

	/**
	 * Instantiates a new pipe marquee handler.
	 * 
	 * @param graph
	 *            the graph
	 */
	public PipeMarqueeHandler(PipeJGraph graph) {
		this.graph = graph;
	}

	/**
	 * Creates the popup menu.
	 * 
	 * @param pt
	 *            the pt
	 * @param algo
	 *            the algo
	 * @param cell
	 *            the cell
	 * @return the j popup menu
	 */
	public JPopupMenu createPopupMenu(final Point pt, final PipeAlgorithm algo, final AlgorithmCell cell) {
		JPopupMenu menu = new JPopupMenu();
		if (algo != null) {
			menu.add(new AbstractAction("Save As Module Definition") {
				public void actionPerformed(ActionEvent e) {
					if (algo.saveAs()) {
						ModuleTreePanel.getInstance().forceInit();
					}
				}
			});
		}
		return menu;
	}

	/**
	 * Dispose.
	 */
	public void dispose() {
	}

	/**
	 * Draw port label.
	 * 
	 * @param g
	 *            the g
	 * @param port
	 *            the port
	 */
	protected void drawPortLabel(Graphics2D g, PortView port) {
		if ((port.getCell() != null) && (port.getCell() instanceof DefaultPort)) {
			DefaultPort modcell = (DefaultPort) port.getCell();
			PipePort pport = (PipePort) modcell.getUserObject();
			if (pport.getLabel() == null) {
				return;
			}
			Point2D p = graph.toScreen((Point2D) port.getLocation().clone());
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			Font f = GraphConstants.getFont(port.getAllAttributes());
			g.setFont(f);
			FontMetrics metrics = g.getFontMetrics();
			int sw = metrics.stringWidth(pport.getLabel());
			int sh = metrics.getHeight();
			int voff = metrics.getDescent();
			Point2D offset = GraphConstants.getLabelPosition(port.getAllAttributes());
			if (pport.isInputPort()) {
				g.translate((int) p.getX() + offset.getX(), (int) p.getY() + offset.getY());
				g.rotate(-labelAngle);
			} else {
				g.translate((int) p.getX() + offset.getX(), (int) p.getY() + offset.getY());
				g.rotate(labelAngle);
			}
			g.setColor(GraphConstants.getBackground(port.getAllAttributes()));
			g.fillRoundRect(-2, voff - sh - 2, sw + 4, sh + 4, 3, 3);
			g.setColor(GraphConstants.getBorderColor(port.getAllAttributes()));
			g.drawRoundRect(-2, voff - sh - 2, sw + 4, sh + 4, 3, 3);
			g.setColor(GraphConstants.getForeground(port.getAllAttributes()));
			g.setFont(f);
			g.drawString(pport.getLabel(), 0, 0);
			if (pport.isInputPort()) {
				g.rotate(labelAngle);
				g.translate(-((int) p.getX() + offset.getX()), -((int) p.getY() + offset.getY()));
			} else {
				g.rotate(-labelAngle);
				g.translate(-((int) p.getX() + offset.getX()), -((int) p.getY() + offset.getY()));
			}
		}
	}

	/**
	 * Gets the source port at.
	 * 
	 * @param point
	 *            the point
	 * @return the source port at
	 */
	public PortView getSourcePortAt(Point2D point) {
		// Disable jumping
		graph.setJumpToDefaultPort(false);
		PortView result;
		try {
			// Find a Port View in Model Coordinates and Remember
			result = graph.getPortViewAt(point.getX(), point.getY());
		} finally {
			graph.setJumpToDefaultPort(true);
		}
		return result;
	}

	// Find a Cell at point and Return its first Port as a PortView
	/**
	 * Gets the target port at.
	 * 
	 * @param point
	 *            the point
	 * @return the target port at
	 */
	protected PortView getTargetPortAt(Point2D point) {
		// Find a Port View in Model Coordinates and Remember
		return graph.getPortViewAt(point.getX(), point.getY());
	}

	// Override to Gain Control (for PopupMenu and ConnectMode)
	/* (non-Javadoc)
	 * @see org.jgraph.graph.BasicMarqueeHandler#isForceMarqueeEvent(java.awt.event.MouseEvent)
	 */
	public boolean isForceMarqueeEvent(MouseEvent e) {
		if (e.isShiftDown()) {
			return false;
		}
		// If Right Mouse Button we want to Display the PopupMenu
		if (SwingUtilities.isRightMouseButton(e)) {
			// Return Immediately
			return true;
		}
		// Find and Remember Port
		port = getSourcePortAt(e.getPoint());
		// If Port Found and in ConnectMode (=Ports Visible)
		if ((port != null) && graph.isPortsVisible()) {
			return true;
		}
		// Else Call Superclass
		return super.isForceMarqueeEvent(e);
	}

	// Find Port under Mouse and Repaint Connector
	/* (non-Javadoc)
	 * @see org.jgraph.graph.BasicMarqueeHandler#mouseDragged(java.awt.event.MouseEvent)
	 */
	public void mouseDragged(MouseEvent e) {
		// If remembered Start Point is Valid
		if (start != null) {
			// Fetch Graphics from Graph
			Graphics2D g = (Graphics2D) graph.getGraphics();
			// Reset Remembered Port
			PortView newPort = getTargetPortAt(e.getPoint());
			// Do not flicker (repaint only on real changes)
			if ((newPort == null) || (newPort != port)) {
				graph.paint(g);
				// Xor-Paint the old Connector (Hide old Connector)
				// paintConnector(Color.black, graph.getBackground(), g);
				// If Port was found then Point to Port Location
				port = newPort;
				if (port != null) {
					current = graph.toScreen(port.getLocation());
					// Else If no Port was found then Point to Mouse Location
				} else {
					current = graph.snap(e.getPoint());
					// Xor-Paint the new Connector
				}
				//	
				paintConnector(graph.getBackground(), Color.black, g);
				g.setPaintMode();
				if (newPort != null) {
					drawPortLabel(g, newPort);
				}
			}
		}
		// Call Superclass
		super.mouseDragged(e);
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.BasicMarqueeHandler#mouseMoved(java.awt.event.MouseEvent)
	 */
	public void mouseMoved(MouseEvent e) {
		// Check Mode and Find Port
		if ((e != null) && (getSourcePortAt(e.getPoint()) != null) && graph.isPortsVisible()) {
			// PortView port=getSourcePortAt(e.getPoint());
			Graphics2D g = (Graphics2D) graph.getGraphics();
			currentPort = getSourcePortAt(e.getPoint());
			if (currentPort != null) {
				if ((graph.getSelectionCells().length == 0) || ((lastPort != currentPort) && edgeSelected)) {
					if (edgeSelected) {
						graph.clearSelection();
					}
					PipePort p = ((PipeModulePort) currentPort.getCell()).getPort();
					Vector<PipeConnector> connectors = (p.isOutputPort()) ? p.getOutgoingConnectors() : p
							.getIncomingConnectors();
					for (PipeConnector c : connectors) {
						graph.addSelectionCell(c.getGraphEdge());
					}
					edgeSelected = true;
				}
			}
			lastPort = currentPort;
			// Set Cusor on Graph (Automatically Reset)
			graph.setCursor(new Cursor(Cursor.HAND_CURSOR));
			// Consume Event
			// Note: This is to signal the BasicGraphUI's
			// MouseHandle to stop further event processing.
			// Set Foreground
			e.consume();
		} else {
			currentPort = null;
			if (edgeSelected) {
				graph.clearSelection();
				edgeSelected = false;
				lastPort = null;
			}
			Object cell = graph.getFirstCellForLocation(e.getX(), e.getY());
			// Call Superclass
			super.mouseMoved(e);
		}
	}

	// Display PopupMenu or Remember Start Location and First Port
	/* (non-Javadoc)
	 * @see org.jgraph.graph.BasicMarqueeHandler#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(final MouseEvent e) {
		// If Right Mouse Button
		if (SwingUtilities.isRightMouseButton(e)) {
			// Find Cell in Model Coordinates
			Object cell = graph.getFirstCellForLocation(e.getX(), e.getY());
			// Create PopupMenu for the Cell
			// Display PopupMenu
			if ((cell != null) && (cell instanceof AlgorithmCell)) {
				PipeModule mod = ((AlgorithmCell) cell).getPipeModule();
				if (mod instanceof PipeAlgorithm) {
					JPopupMenu menu = createPopupMenu(e.getPoint(), (PipeAlgorithm) mod, (AlgorithmCell) cell);
					// Display PopupMenu
					menu.show(graph, e.getX(), e.getY());
				}
			}
			// Else if in ConnectMode and Remembered Port is Valid
		} else if ((port != null) && graph.isPortsVisible()) {
			// Remember Start Location
			start = graph.toScreen(port.getLocation());
			// Remember First Port
			firstPort = port;
			graph.setPortToCheck(((PipeModulePort) firstPort.getCell()).getPort());
			graph.setOffscreenValid(false);
			graph.paint(graph.getGraphics());
			drawPortLabel((Graphics2D) graph.getGraphics(), port);
		} else {
			// Call Superclass
			super.mousePressed(e);
		}
	}

	// Connect the First Port and the Current Port in the Graph or Repaint
	/* (non-Javadoc)
	 * @see org.jgraph.graph.BasicMarqueeHandler#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent e) {
		// If Valid Event, Current and First Port
		if ((e != null) && (port != null) && (firstPort != null) && (firstPort != port)) {
			// Then Establish Connection
			PipeConnector connector = PipeConnector.connect(((PipeModulePort) firstPort.getCell()).getPort(),
					((PipeModulePort) port.getCell()).getPort());
			e.consume();
			// Else Repaint the Graph
		}
		graph.setPortToCheck(null);
		graph.repaint();
		// Reset Global Vars
		firstPort = null;
		port = null;
		start = current = null;
		// Call Superclass
		super.mouseReleased(e);
	}

	// Use Xor-Mode on Graphics to Paint Connector
	/**
	 * Paint connector.
	 * 
	 * @param fg
	 *            the fg
	 * @param bg
	 *            the bg
	 * @param g
	 *            the g
	 */
	protected void paintConnector(Color fg, Color bg, Graphics g) {
		// Set Foreground
		g.setColor(fg);
		// Set Xor-Mode Color
		g.setXORMode(bg);
		// Highlight the Current Port
		paintPort(graph.getGraphics());
		// If Valid First Port, Start and Current Point
		if ((firstPort != null) && (start != null) && (current != null)) {
			// Then Draw A Line From Start to Current Point
			g.drawLine((int) start.getX(), (int) start.getY(), (int) current.getX(), (int) current.getY());
		}
	}

	// Use the Preview Flag to Draw a Highlighted Port
	/**
	 * Paint port.
	 * 
	 * @param g
	 *            the g
	 */
	protected void paintPort(Graphics g) {
		// If Current Port is Valid
		if (port != null) {
			// If Not Floating Port...
			boolean o = (GraphConstants.getOffset(port.getAllAttributes()) != null);
			// ...Then use Parent's Bounds
			Rectangle2D r = (o) ? port.getBounds() : port.getParentView().getBounds();
			// Scale from Model to Screen
			r = graph.toScreen((Rectangle2D) r.clone());
			// Add Space For the Highlight Border
			// r.setFrame(r.getX() - 3, r.getY() - 3, r.getWidth() + 6,
			// r.getHeight() + 6);
			// Paint Port in Preview (=Highlight) Mode
			graph.getUI().paintCell(g, port, r, true);
		}
	}
}
