/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.Vector;

import javax.swing.ImageIcon;

import org.jgraph.graph.GraphConstants;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithmGroup;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parser.JISTDynamicPluginLookup;
import edu.jhu.ece.iacl.jist.pipeline.parser.JistLayoutParser;
import edu.jhu.ece.iacl.jist.pipeline.parser.LoniPipeParser;
import edu.jhu.ece.iacl.jist.pipeline.parser.MipavPluginParser;
import edu.jhu.ece.iacl.jist.pipeline.parser.MipavScriptParser;
import edu.jhu.ece.iacl.jist.pipeline.tree.DraggableNode;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmExecutableAdapter;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmJistLayoutAdapter;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmMipavAdapter;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

/**
 * Creates algorithm modules from a module definition.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeAlgorithmFactory extends PipeModuleFactory {
	
	/**
	 * Get module cell that represents algorithm.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class AlgorithmCell extends PipeModuleCell {
		
		/**
		 * Constructor.
		 * 
		 * @param name
		 *            cell name
		 * @param pipe
		 *            module associated with cell
		 */
		public AlgorithmCell(String name, PipeModule pipe) {
			super(name, pipe);
			PipeCellViewFactory.setViewClass(this.getAttributes(), PipeAlgorithmView.class.getCanonicalName());
			ProcessingAlgorithm algo=((PipeAlgorithm)pipe).getAlgorithm();
			if(algo==null){
				GraphConstants.setGradientColor(this.getAttributes(), Color.gray);
			} else if(!algo.isRunningInSeparateProcess())GraphConstants.setGradientColor(this.getAttributes(), Color.yellow.darker());
		
		}

		/* (non-Javadoc)
		 * @see org.jgraph.graph.DefaultGraphCell#clone()
		 */
		public PipeModuleCell clone() {			
			PipeAlgorithmFactory factory = new PipeAlgorithmFactory(pipe.getLabel(),"", ((PipeAlgorithm) pipe)
					.getAlgorithmClass());
			AlgorithmCell cell = factory.createGraphCell();
			PipeModule pipeClone=cell.getPipeModule();
			((PipeAlgorithm)pipe).getInputParams().getInputView().update();
			((PipeAlgorithm)pipeClone).getInputParams().importParameter(pipe.getInputParams());
			((PipeAlgorithm)pipeClone).getInputParams().getInputView().update();
			return cell;
		}
	}

	/**
	 * Get module cell that represents algorithm group.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class AlgorithmGroupCell extends AlgorithmCell {
		
		/** The children. */
		protected Vector<AlgorithmCell> children;

		/**
		 * Constructor.
		 * 
		 * @param name
		 *            cell name
		 * @param pipe
		 *            pipe module assocaited with group cell
		 */
		public AlgorithmGroupCell(String name, PipeModule pipe) {
			super(name, pipe);
			children = new Vector<AlgorithmCell>();
			PipeCellViewFactory.setViewClass(this.getAttributes(), PipeAlgorithmGroupView.class.getCanonicalName());
		}

		/**
		 * Get children cells.
		 * 
		 * @return the children cells
		 */
		public Vector<AlgorithmCell> getChildrenCells() {
			return children;
		}

		/**
		 * Set children cells.
		 * 
		 * @param children
		 *            the children
		 */
		public void setChildrenCells(Vector<AlgorithmCell> children) {
			this.children = children;
		}

		/**
		 * Set location of group cell.
		 * 
		 * @param x
		 *            the x
		 * @param y
		 *            the y
		 */
		public void setLocation(int x, int y) {
			Rectangle2D bounds = new Rectangle2D.Double(x, y, 150, 50);
			GraphConstants.setBounds(this.getAttributes(), bounds);
			setBounds(bounds);
		}
	}

	/**
	 * Tree node to represent algorithm.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class AlgorithmNode extends DraggableNode {
		
		/**
		 * Instantiates a new algorithm node.
		 * 
		 * @param factory
		 *            the factory
		 */
		public AlgorithmNode(PipeModuleFactory factory) {
			super(factory);
			if(MedicAlgorithmExecutableAdapter.class.equals(factory.getModuleClass())){
				 this.icon=new ImageIcon(PlaceHolder.class.getResource("algorithm_loni.png"));
			} else if(MedicAlgorithmMipavAdapter.class.equals(factory.getModuleClass())){
				 this.icon=new ImageIcon(PlaceHolder.class.getResource("algorithm_mipav.png"));
			} else {
				this.icon=new ImageIcon(PlaceHolder.class.getResource("algorithm_jist.png")); // name update
			}
		}
		
		/**
		 * Gets the map file.
		 * 
		 * @return the map file
		 */
		public File getMapFile() {
			return ((PipeAlgorithmFactory) factory).getMapFile();
		}

		/* (non-Javadoc)
		 * @see javax.swing.tree.DefaultMutableTreeNode#setUserObject(java.lang.Object)
		 */
		public void setUserObject(Object userObject) {
			super.setUserObject(userObject);
			((PipeAlgorithmFactory) factory).rename(userObject.toString());
		}
	}
	
	/**
	 * Tree node to represent algorithm.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	public static class AlgorithmGroupNode extends AlgorithmNode {
		
		/**
		 * Instantiates a new algorithm group node.
		 * 
		 * @param factory
		 *            the factory
		 */
		public AlgorithmGroupNode(PipeModuleFactory factory) {
			super(factory);
			this.icon=new ImageIcon(PlaceHolder.class.getResource("algorithm_group.png"));
		}

	}
	
	/** The map file. */
	protected File mapFile = null;

	/**
	 * Constructor.
	 * 
	 * @param mapFile
	 *            file that contains module definition
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	public PipeAlgorithmFactory(File mapFile) throws ClassNotFoundException{
		super();
		this.mapFile = mapFile;
		PipeAlgorithm algo = createPipe();
		if(algo!=null){
			if(!(algo instanceof PipeAlgorithmGroup)){
				this.c = algo.getAlgorithmClass();	
				ProcessingAlgorithm method=algo.getAlgorithm();
				if(method==null){
					throw new ClassNotFoundException(mapFile.toString());				
				}
				if(MipavPluginParser.class.equals(c)) {
					MipavPluginParser a;
					
					try {
						a = (MipavPluginParser )method;
						String classTag = algo.getInputParams().getFirstChildByName(a.getClassTag()).getValue().toString();
						a.init(classTag);											
					} catch (AlgorithmRuntimeException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JistLogger.logError(JistLogger.SEVERE, "Cannot wrap class (skipping): "+c.getCanonicalName());						
					}catch(Exception e) {
						e.printStackTrace();
						JistLogger.logError(JistLogger.SEVERE, "Cannot wrap class (skipping): "+c.getCanonicalName());
					}
					
				}

				
				String status=algo.getAlgorithm().getAlgorithmInformation().getStatusString();
				this.version =method.getAlgorithmInformation().getVersion().trim()+((status.length()>0)?" "+status:"");
				this.description = method.getAlgorithmInformation().getDescription();
			}
			this.name = algo.getLabel();
		} else {
			throw new ClassNotFoundException(mapFile.toString());
		}
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            name of module
	 * @param c
	 *            algorithm class for module
	 */
	public PipeAlgorithmFactory(String name, String description, Class c) {
		super(name, description, c);
	}

	/**
	 * Create graph cell to represent algorithm.
	 * 
	 * @return the algorithm cell
	 */
	public AlgorithmCell createGraphCell() {
		PipeAlgorithm p = createPipe();
		return p.createModuleCell();
	}

	/**
	 * Create module to represent algorithm.
	 * 
	 * @return the pipe algorithm
	 */
	public PipeAlgorithm createPipe() {
		PipeAlgorithm p = null;
		if (mapFile != null) {
			p = PipeAlgorithm.read(mapFile);
		} else {
			try {
				ProcessingAlgorithm algo = (ProcessingAlgorithm) c.newInstance();				
				p = new PipeAlgorithm(algo);
				
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		// There are no input parameters to this plug-in, try to load parameters
		// from script!
		if ((p != null) && (p.getInputParams().size() == 2)) {
			if (MedicAlgorithmMipavAdapter.class.equals(c)) {
				MipavScriptParser parser = new MipavScriptParser();
				p = parser.openPipeAlgorithm();
			} else if (MedicAlgorithmExecutableAdapter.class.equals(c)) {
				LoniPipeParser parser = new LoniPipeParser();
				p = parser.openPipeAlgorithm();
			} else if (MedicAlgorithmJistLayoutAdapter.class.equals(c)){
				JistLayoutParser parser=new JistLayoutParser();
				p = parser.openPipeAlgorithm();
			}
		}
		if(p!=null) {
			if(MipavPluginParser.class.equals(c)) {
				MipavPluginParser a;
				try {
					a = (MipavPluginParser )c.newInstance();
					String classTag = p.getInputParams().getFirstChildByName(a.getClassTag()).getValue().toString();
					a.init(classTag);
					p = new PipeAlgorithm(a);					
					p.setAlgorithm(a);
					p.setMapFile(mapFile );
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (AlgorithmRuntimeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
				
			}
		}
		return p;
	}

	/**
	 * Create tree node to represent algorithm.
	 * 
	 * @return the draggable node
	 */
	public DraggableNode createTreeNode() {
		if(c==null){
			return new AlgorithmGroupNode(this);
		} else {
			return new AlgorithmNode(this);
		}
	}

	/**
	 * Get module definition file.
	 * 
	 * @return module file
	 */
	public File getMapFile() {
		return mapFile;
	}

	/**
	 * Rename module and save the change to the module definition file.
	 * 
	 * @param label
	 *            module name
	 */
	public void rename(String label) {
		if (mapFile != null) {
			PipeAlgorithm algo = createPipe();
			algo.setLabel(label);
			algo.write(mapFile);
		}
	}

	/**
	 * Set module definition file.
	 * 
	 * @param f
	 *            module file
	 */
	public void setMapFile(File f) {
		this.mapFile = f;
	}

	public AlgorithmInformation getAlgInformation() {
		PipeAlgorithm algo = PipeAlgorithm.read(mapFile);
//		PipeAlgorithm algo = createPipe();
		if(algo!=null){
			if(!(algo instanceof PipeAlgorithmGroup)){
				this.c = algo.getAlgorithmClass();	
				ProcessingAlgorithm method=algo.getAlgorithm();
				if(method==null){
				return null;				
				}
				if(MipavPluginParser.class.equals(c)) {
					MipavPluginParser a;
					
					try {
						a = (MipavPluginParser )method;
						String classTag = algo.getInputParams().getFirstChildByName(a.getClassTag()).getValue().toString();
						a.init(classTag);											
					} catch (AlgorithmRuntimeException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JistLogger.logError(JistLogger.SEVERE, "Cannot wrap class (skipping): "+c.getCanonicalName());						
					}catch(Exception e) {
						e.printStackTrace();
						JistLogger.logError(JistLogger.SEVERE, "Cannot wrap class (skipping): "+c.getCanonicalName());
					}
					
				}

				
				return method.getAlgorithmInformation();
			}
		
		}	
		
		return null;
	}
}
