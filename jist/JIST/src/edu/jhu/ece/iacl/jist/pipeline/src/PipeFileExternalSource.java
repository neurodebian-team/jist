package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;

public class PipeFileExternalSource extends PipeExternalSource{
	public PipeFileExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamFile("Default File",new FileExtensionFilter(new String[]{})));
		group.setLabel("External File");
		group.setName("extfile");
		group.setCategory("Externalize.File");
		return group;
	}
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("File");
		group.add(valParam = new ParamFile("File",new FileExtensionFilter(new String[]{})));
		return group;
	}
}
