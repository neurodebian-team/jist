package edu.jhu.ece.iacl.jist.structures.vector;
import java.text.NumberFormat;

/**
 * The Class Vector3.
 */
public class Vector3 implements VectorX{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The z. */
	private Number x,y,z;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#size()
	 */
	public int size(){return 3;}
	
	/**
	 * Round.
	 * 
	 * @param v the v
	 * @param prec the prec
	 * 
	 * @return the vector3
	 */
	public static Vector3 round(Vector3 v,int prec){
		double exp=Math.pow(10,prec);
		v.setX(Math.round(v.getX().doubleValue()*exp)/exp);
		v.setY(Math.round(v.getY().doubleValue()*exp)/exp);
		v.setZ(Math.round(v.getZ().doubleValue()*exp)/exp);
		return v;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#get(int)
	 */
	public Number get(int i){
		switch(i){
			case 0:return x;
			case 1:return y;
			case 2:return z;
			default:return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getX()
	 */
	public Number getX(){return x;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getY()
	 */
	public Number getY(){return y;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getZ()
	 */
	public Number getZ(){return z;}
	
	/**
	 * Instantiates a new vector3.
	 * 
	 * @param m the m
	 */
	public Vector3(Number m[]){
		x=m[0];
		y=m[1];
		z=m[2];
	}
	
	/**
	 * Instantiates a new vector3.
	 * 
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public Vector3(Number x,Number y,Number z){
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	/**
	 * Instantiates a new vector3.
	 */
	public Vector3(){
		this(0,0,0);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setX(java.lang.Number)
	 */
	public void setX(Number x){this.x=x;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setY(java.lang.Number)
	 */
	public void setY(Number y){this.y=y;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setZ(java.lang.Number)
	 */
	public void setZ(Number z){this.z=z;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setW(java.lang.Number)
	 */
	public void setW(Number z){}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#toArray()
	 */
	public Number[] toArray() {
		return new Number[]{x,y,z};
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(double)
	 */
	public Vector3 add(double a) {
		return new Vector3(getX().doubleValue()+a,getY().doubleValue()+a,getZ().doubleValue()+a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(double)
	 */
	public Vector3 sub(double a) {
		return new Vector3(getX().doubleValue()-a,getY().doubleValue()-a,getZ().doubleValue()-a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(double)
	 */
	public Vector3 mul(double a) {
		return new Vector3(getX().doubleValue()*a,getY().doubleValue()*a,getZ().doubleValue()*a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(double)
	 */
	public Vector3 div(double a) {
		return new Vector3(getX().doubleValue()/a,getY().doubleValue()/a,getZ().doubleValue()/a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector3 add(VectorX v) {
		return new Vector3(getX().doubleValue()+v.getX().doubleValue(),getY().doubleValue()+v.getY().doubleValue(),getZ().doubleValue()+v.getZ().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector3 sub(VectorX v) {
		return new Vector3(getX().doubleValue()-v.getX().doubleValue(),getY().doubleValue()-v.getY().doubleValue(),getZ().doubleValue()-v.getZ().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector3 mul(VectorX v) {
		return new Vector3(getX().doubleValue()*v.getX().doubleValue(),getY().doubleValue()*v.getY().doubleValue(),getZ().doubleValue()*v.getZ().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector3 div(VectorX v) {
		return new Vector3(getX().doubleValue()/v.getX().doubleValue(),getY().doubleValue()/v.getY().doubleValue(),getZ().doubleValue()/v.getZ().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getW()
	 */
	public Number getW() {
		return null;
	}
	
	/**
	 * Sets the.
	 * 
	 * @param v the v
	 */
	public void set(Vector3 v){
		x=v.x;
		y=v.y;
		z=v.z;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#normalize()
	 */
	public Vector3 normalize(){
		double mag=mag().doubleValue();
		if(mag>0)return div(mag); else return this;	
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mag()
	 */
	public Number mag() {
		return Math.sqrt(x.doubleValue()*x.doubleValue()+y.doubleValue()*y.doubleValue()+z.doubleValue()*z.doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Vector3 clone(){
		return new Vector3(x,y,z);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		NumberFormat format=NumberFormat.getNumberInstance();
		format.setMaximumFractionDigits(5);
		format.setMinimumFractionDigits(5);
		return ("["+format.format(x)+","+format.format(y)+","+format.format(z)+"]");
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#set(java.lang.Number, int)
	 */
	public void set(Number value, int i) {
		switch(i){
			case 0:x=value;
			case 1:y=value;
			case 2:z=value;
		}
	}
}
