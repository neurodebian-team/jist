package edu.jhu.ece.iacl.jist.structures.vector;

// TODO: Auto-generated Javadoc
/**
 * The Class VectorN.
 */
public class VectorN implements VectorX{
	
	/** The nums. */
	private Number nums[];
	
	/**
	 * Instantiates a new vector n.
	 * 
	 * @param size the size
	 */
	public VectorN(int size){
		nums=new Number[size];
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#toArray()
	 */
	public Number[] toArray() {
		return nums;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#size()
	 */
	public int size() {
		return nums.length;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#get(int)
	 */
	public Number get(int i) {
		return nums[i];
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getX()
	 */
	public Number getX() {
		return nums[0];
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getY()
	 */
	public Number getY() {
		return nums[1];
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getZ()
	 */
	public Number getZ() {
		return nums[2];
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getW()
	 */
	public Number getW() {
		return nums[3];
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mag()
	 */
	public Number mag() {
		double total=0;
		for(int i=0;i<nums.length;i++){
			total+=nums[i].doubleValue()*nums[i].doubleValue();
		}
		return Math.sqrt(total);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setX(java.lang.Number)
	 */
	public void setX(Number x) {
		nums[0]=x;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setY(java.lang.Number)
	 */
	public void setY(Number y) {
		nums[1]=y;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setZ(java.lang.Number)
	 */
	public void setZ(Number z) {
		nums[2]=z;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setW(java.lang.Number)
	 */
	public void setW(Number w) {
		nums[3]=w;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#set(java.lang.Number, int)
	 */
	public void set(Number value, int i) {
		nums[i]=value;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(double)
	 */
	public VectorN add(double a) {
		VectorN v=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v.set(nums[i].doubleValue()+a,i);
		}
		return v;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(double)
	 */
	public VectorN sub(double a) {
		VectorN v=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v.set(nums[i].doubleValue()-a,i);
		}
		return v;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(double)
	 */
	public VectorN mul(double a) {
		VectorN v=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v.set(nums[i].doubleValue()*a,i);
		}
		return v;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(double)
	 */
	public VectorN div(double a) {
		VectorN v=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v.set(nums[i].doubleValue()/a,i);
		}
		return v;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public VectorN add(VectorX v) {
		VectorN v2=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v2.set(nums[i].doubleValue()+v.get(i).doubleValue(),i);
		}
		return v2;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public VectorN sub(VectorX v) {
		VectorN v2=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v2.set(nums[i].doubleValue()-v.get(i).doubleValue(),i);
		}
		return v2;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public VectorN mul(VectorX v) {
		VectorN v2=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v2.set(nums[i].doubleValue()*v.get(i).doubleValue(),i);
		}
		return v2;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public VectorN div(VectorX v) {
		VectorN v2=new VectorN(size());
		for(int i=0;i<nums.length;i++){
			v2.set(nums[i].doubleValue()/v.get(i).doubleValue(),i);
		}
		return v2;
	}
	
	/**
	 * Instantiates a new vector n.
	 * 
	 * @param nums the nums
	 */
	public VectorN(Number nums[]){
		this.nums=nums;

	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#normalize()
	 */
	public VectorN normalize(){
		double mag=mag().doubleValue();
		if(mag>0)return div(mag); else return this;	
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public VectorN clone(){
		return new VectorN(nums.clone());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String text="(";
		for(int i=0;i<size();i++){
			text+=get(i)+((i<size()-1)?",":")");
		}
		return text;
	}
}
