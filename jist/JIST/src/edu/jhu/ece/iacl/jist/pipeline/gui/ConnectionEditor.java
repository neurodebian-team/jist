/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicComboBoxEditor;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;

/**
 * Class for editing connections that have an identifying index.
 * 
 * @author Blake Lucas
 */
public class ConnectionEditor extends BasicComboBoxEditor implements ComboBoxEditor, ChangeListener {
	
	/** The index selection. */
	JSpinner indexSelection;
	
	/** The dest port. */
	PipePort destPort;
	
	/** The src port. */
	PipePort srcPort;
	
	/** The pane. */
	JPanel pane;

	/**
	 * Constructor.
	 * 
	 * @param port
	 *            destination port
	 */
	public ConnectionEditor(PipePort port) {
		this.destPort = port;
		indexSelection = new JSpinner(new SpinnerNumberModel(0, 0, 1000000, 1));
		indexSelection.setPreferredSize(new Dimension(45, 20));
		indexSelection.addChangeListener(this);
		pane = new JPanel(new GridLayout(1, 0));
	}

	/**
	 * Get edit pane.
	 * 
	 * @return the editor component
	 */
	public Component getEditorComponent() {
		return pane;
	}

	/**
	 * Get selected item.
	 * 
	 * @return the item
	 */
	public PipePort getItem() {
		// TODO Auto-generated method stub
		try {
			indexSelection.commitEdit();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return srcPort;
	}

	/**
	 * Set selected item.
	 * 
	 * @param obj
	 *            the obj
	 */
	public void setItem(Object obj) {
		if (obj instanceof PipePort) {
			srcPort = (PipePort) obj;
			updatePanel(srcPort);
		} else {
			srcPort = null;
			updatePanel(null);
		}
	}

	/**
	 * Index selection has changed.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void stateChanged(ChangeEvent evt) {
		if (srcPort instanceof ObjectCollection) {
			PipeConnector wire = srcPort.getOutgoingConnector(destPort);
			if (wire != null) {
				wire.setSourceIndex(Integer.parseInt(indexSelection.getValue().toString()));
			}
		}
	}

	/**
	 * Update panel.
	 * 
	 * @param port
	 *            selected source port
	 */
	protected void updatePanel(PipePort port) {
		// Get the selected index. (The index parameter isn't
		// always valid, so just use the value.)
		JLabel listLabel;
		pane.removeAll();
		if (port != null) {
			listLabel = new JLabel(port.getOwner().getLabel() + "::" + port.getLabel());
			if (port instanceof ObjectCollection) {
				PipeConnector wire = port.getOutgoingConnector(destPort);
				if ((wire != null) && (wire.getSourceIndex() != -1)) {
					JPanel smallPane = new JPanel(new BorderLayout());
					indexSelection.setValue(wire.getSourceIndex());
					smallPane.add(listLabel, BorderLayout.WEST);
					smallPane.add(indexSelection, BorderLayout.EAST);
					pane.add(smallPane);
				} else {
					pane.add(listLabel);
				}
			} else {
				pane.add(listLabel);
			}
		} else {
			listLabel = new JLabel("---NONE---");
			pane.add(listLabel);
		}
		pane.setBorder(BorderFactory.createLineBorder((new JList()).getSelectionBackground().darker()));
	}
}
