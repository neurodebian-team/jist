/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import org.jgraph.graph.CellViewRenderer;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Cell view render for child ports.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PortChildView extends PipePortView {
	
	/**
	 * The Class ActivityRenderer.
	 */
	public class ActivityRenderer extends PipePortView.ActivityRenderer {
		
		/**
		 * Instantiates a new activity renderer.
		 */
		public ActivityRenderer() {
			super();
			setForeground(Color.gray);
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipePortView.ActivityRenderer#createShape()
		 */
		public Shape createShape() {
			Dimension d = this.getSize();
			return new Rectangle2D.Float(1, 2, d.width - 3, d.height - 3);
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipePortView.ActivityRenderer#isCompatible()
		 */
		protected boolean isCompatible() {
			PipePort src = graph.getPortToCheck();
			PipePort dest = ((PipeModulePort) view.getCell()).getPort();
			if (src == null) {
				return false;
			}
			if (src == dest) {
				return false;
			}
			if (src.isOutputPort() && dest.isInputPort()) {
				return PipeConnector.isCompatible(src, dest);
			} else {
				return false;
			}
		}
	}

	/** The renderer. */
	protected transient ActivityRenderer renderer = new ActivityRenderer();

	/**
	 * Instantiates a new port child view.
	 */
	public PortChildView() {
		super();
	}

	/**
	 * Instantiates a new port child view.
	 * 
	 * @param cell
	 *            the cell
	 */
	public PortChildView(Object cell) {
		super(cell);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.graph.PipePortView#getRenderer()
	 */
	public CellViewRenderer getRenderer() {
		return this.renderer;
	}
}
