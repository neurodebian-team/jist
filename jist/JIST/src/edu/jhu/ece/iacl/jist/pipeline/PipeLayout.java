/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.awt.Component;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences.NamingConvention;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeJGraph;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmCell;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory.AlgorithmGroupCell;
import edu.jhu.ece.iacl.jist.pipeline.gui.ParameterPanel;
import edu.jhu.ece.iacl.jist.pipeline.gui.PipeInternalFrame;
import edu.jhu.ece.iacl.jist.pipeline.gui.PipelineLayoutTool;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.parser.LoniPipeParser;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

// TODO: Auto-generated Javadoc
/**
 * Pipe Layout stores all information necessary to construct the pipe graph and
 * layout preferences.
 * 
 * @author Blake Lucas
 */
public class PipeLayout {

	/**
	 * The Class RunParameters.  This is specific to each maps layout
	 */
	public static class RunParameters {

		/** directory to store algorithm output. */
		private File inputDir = null, outputDir = null;
		//		private JistPreferences JistPreferences = JistPreferences.getPreferences();
		/** Maximum heap size. */
		private int maxHeap = JistPreferences.getPreferences().getDefaultMemory();

		/** Maximum number of simultaneous processes. */
		private int maxProcs = JistPreferences.getPreferences().getDefaultMaxProcesses();

		/** The output uri. */
		private URI inputURI=null,outputURI=null;

		/** The use grid engine. */
		private boolean useGridEngine=JistPreferences.getPreferences().isUseGridEngine();

		/** The naming convention. */
		private NamingConvention namingConvention=JistPreferences.getPreferences().getNamingConvention();

		/** The preferredextension. */
		private String preferredextension = JistPreferences.getPreferences().getPreferredExtension();

		/**
		 * Gets the preferred extension.
		 * 
		 * @return the preferred extension
		 */
		public String getPreferredExtension()
		{
			return preferredextension;
		}
		//		public JistPreferences getJistPreferences()
		//		{
		//			return JistPreferences;
		//		}
		/**
		 * Gets the naming convention.
		 * 
		 * @return the naming convention
		 */
		public NamingConvention getNamingConvention() {
			return namingConvention;
		}

		/**
		 * Checks if is use grid engine.
		 * 
		 * @return true, if is use grid engine
		 */
		public boolean isUseGridEngine() {
			return useGridEngine;
		}

		/**
		 * Sets the use grid engine.
		 * 
		 * @param useGridEngine the new use grid engine
		 */
		public void setUseGridEngine(boolean useGridEngine) {
			this.useGridEngine = useGridEngine;
		}

		/**
		 * Gets the input directory.
		 * 
		 * @return the input directory
		 */
		public File getInputDirectory() {
			if(inputURI!=null){
				inputDir=new File(inputURI);
			}
			return inputDir;
		}

		/**
		 * Gets the input directory.
		 * 
		 * @return the input directory
		 */
		public URI getOutputURI() {
			if(outputURI==null){
				if(outputDir!=null){
					return outputDir.toURI();
				} else {
					return null;
				}
			} else {
				return outputURI;
			}
		}

		/**
		 * Gets the input directory.
		 * 
		 * @return the input directory
		 */
		public URI getInputURI() {
			if(inputURI==null){
				if(inputDir!=null){
					return inputDir.toURI();
				} else {
					return null;
				}
			} else {
				return inputURI;
			}
		}

		/**
		 * Gets the max heap.
		 * 
		 * @return the max heap
		 */
		public int getMaxHeap() {
			return maxHeap;
		}

		/**
		 * Gets the max processes.
		 * 
		 * @return the max processes
		 */
		public int getMaxProcs() {
			return maxProcs;
		}

		/**
		 * Gets the output directory.
		 * 
		 * @return the output directory
		 */
		public File getOutputDirectory() {
			if(outputURI!=null){
				outputDir=new File(outputURI);
			}
			return outputDir;
		}

		/**
		 * Sets the input directory.
		 * 
		 * @param inputURI the input uri
		 */
		public void setInputDirectory(URI inputURI) {
			this.inputURI = inputURI;
		}

		/**
		 * Sets the input directory.
		 * 
		 * @param inputDir the new input directory
		 */
		public void setInputDirectory(File inputDir) {
			this.inputURI = (inputDir!=null)?inputDir.toURI():null;
			this.inputDir=inputDir;
		}

		/**
		 * Sets the input directory.
		 * 
		 * @param outputDir the output dir
		 */
		public void setOutputDirectory(File outputDir) {
			this.outputURI = (outputDir!=null)?outputDir.toURI():null;
			this.outputDir=outputDir;
		}

		/**
		 * Sets the max heap.
		 * 
		 * @param maxHeap the new max heap
		 */
		public void setMaxHeap(int maxHeap) {
			this.maxHeap = maxHeap;
		}

		/**
		 * Sets the max processes.
		 * 
		 * @param maxProcs the new max processes
		 */
		public void setMaxProcs(int maxProcs) {
			this.maxProcs = maxProcs;
		}

		/**
		 * Sets the output directory.
		 * 
		 * @param outputURI the output uri
		 */
		public void setOutputDirectory(URI outputURI) {
			this.outputURI = outputURI;
		}


		public void xmlBuildHeader(Document document, Element rootElement) {

			/** directory to store algorithm output. */

			Element em;
			if(inputDir!=null) {
				em = document.createElement("inputDir");
				em.appendChild(document.createTextNode(inputDir.toString()));
				rootElement.appendChild(em);
			}

			/** Maximum heap size. */			
			em = document.createElement("maxHeap");
			em.appendChild(document.createTextNode(""+maxHeap));
			rootElement.appendChild(em);

			/** Maximum number of simultaneous processes. */
			em = document.createElement("maxProcs");
			em.appendChild(document.createTextNode(""+maxProcs));
			rootElement.appendChild(em);

			/** The output uri. */
			if(inputURI!=null){
				em = document.createElement("inputURI");

				em.appendChild(document.createTextNode(inputURI.toString()));		
				rootElement.appendChild(em);
			}

			if(outputURI!=null) {
				em = document.createElement("outputURI");
				em.appendChild(document.createTextNode(outputURI.toString()));
				rootElement.appendChild(em);
			}

			/** The use grid engine. */			
			em = document.createElement("useGridEngine");
			em.appendChild(document.createTextNode(useGridEngine+""));
			rootElement.appendChild(em);

			/** The naming convention. */
			if(namingConvention!=null){
				em = document.createElement("namingConvention");
				em.appendChild(document.createTextNode(namingConvention.toString()));
				rootElement.appendChild(em);
			}


			/** The preferredextension. */			
			em = document.createElement("preferredextension");
			em.appendChild(document.createTextNode(preferredextension));
			rootElement.appendChild(em);

		}




		public void xmlParseHeader(Document doc, Element root) {
			String str = JistXMLUtil.xmlReadTag(root,"inputDir",true);
			if(str!=null) {
				this.inputDir = new File(str);
			}

			str = JistXMLUtil.xmlReadTag(root,"maxHeap",true);
			if(str!=null) {
				this.maxHeap = Integer.valueOf(str);
			}

			str = JistXMLUtil.xmlReadTag(root,"maxProcs",true);
			if(str!=null) {
				this.maxProcs = Integer.valueOf(str);
			}


			str = JistXMLUtil.xmlReadTag(root,"inputURI",true);
			if(str!=null) {
				try {
					this.inputURI = new URI(str);
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}

			str = JistXMLUtil.xmlReadTag(root,"outputURI");
			if(str!=null) {
				try {
					this.outputURI = new URI(str);
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}


			str = JistXMLUtil.xmlReadTag(root,"useGridEngine",true);
			if(str!=null) {
				this.useGridEngine = Boolean.valueOf(str);
			}

			str = JistXMLUtil.xmlReadTag(root,"namingConvention",true);
			if(str!=null) {
				this.namingConvention = NamingConvention.valueOf(str);
			}

			str = JistXMLUtil.xmlReadTag(root,"preferredextension",true);
			if(str!=null) {
				this.preferredextension = str;
			}
		}

	}

	/**
	 * Reconstruct layout from string.
	 * 
	 * @param str XML string
	 * 
	 * @return layout from string
	 * @deprecated
	 */
	public static PipeLayout fromXML(String str) {
		JistLogger.logOutput(JistLogger.INFO, "Using deprecated layout format.");
		XStream stream = new XStream();
		Object o = null;
		try {
			o = stream.fromXML(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (o == null) {
			return null;
		}
		if (o instanceof PipeLayout) {
			return (PipeLayout) o;
		} else {
			return null;
		}
	}

	public static PipeLayout fromSaneXML(String str) {
		PipeLayout pipeLayout = new PipeLayout();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			// ************************************************
			// Read the RunParameters
			// ************************************************			
			Document doc = db.parse(new ByteArrayInputStream(str.getBytes()));
			Element root = doc.getDocumentElement();
			Element rp = JistXMLUtil.xmlReadElement(root, "RunParameters");

			pipeLayout.getRunParameters().xmlParseHeader(doc, rp);	


			// ************************************************
			// Read the Modules
			// ************************************************			

			pipeLayout.xmlParseModules(doc, JistXMLUtil.xmlReadElement(root, "Modules"));	

			// ************************************************
			// Read the Connections
			// ************************************************
			Element e =JistXMLUtil.xmlReadElement(root, "Connections");
			if(e!=null)
				JistLogger.logOutput(JistLogger.INFO,pipeLayout.xmlParseConnections(doc, e));


		} catch (ParserConfigurationException e) {

			e.printStackTrace();
			return null;
		}  catch (SAXException e) {

			e.printStackTrace();
			return null;
		} catch (IOException e) {

			e.printStackTrace();
			return null;
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return pipeLayout;
	}

	private void xmlParseModules(Document document, Element rootMod) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		pipes = new Vector<PipeModule>();
		Vector<Element> nl = JistXMLUtil.xmlReadElementList(rootMod, "Module");
		for(Element el : nl){		
			String classname = JistXMLUtil.xmlReadTag(el,"classname");
			System.out.println("Found Module of Type: "+classname);		
			PipeModule pipe = (PipeModule)Class.forName(classname).newInstance(); 
			pipe.xmlDecodeModule(document, el);
			String reconcileReport;
			try {
				reconcileReport = pipe.reconcileAndMerge();
				if(reconcileReport!=null) {
					System.out.println("************************************************************");
					System.out.println(reconcileReport);
					System.out.println("************************************************************");
				}							
				this.add(pipe);
			} catch (InvalidJistMergeException e) {
				System.out.println("************************************************************");
				System.out.println("Cannot reconcile module. Ignoring.");
				e.printStackTrace();
				System.out.println("************************************************************");

				
			}
		}
		// protected Vector<PipeModule> pipes;
		/*
		boolean ret = false;
		PipeModule.resetLayoutUniqueIndex();
		for(PipeModule pipe : pipes) {
			pipe.setLayoutUniqueID();
			ret=true;
			Element em = document.createElement("Module");
			if(pipe.xmlEncodeModule(document,em)) {
				rootMod.appendChild(em);
				ret = true;
			}
		}
		 */

	}

	private PipeModule findPipeModule(String str) {
		for(PipeModule p : pipes) {
			if(p.getLabel().equalsIgnoreCase(str))
				return p;
		}
		return null;
	}
	
	private PipePort findPipePort(String str, String port, boolean in) {
		for(PipeModule p : pipes) {
			
			if(p.getUniqueID().equalsIgnoreCase(str)) {
				Vector<PipePort> pp = null;
				if(in) 
					pp = p.getInputPorts();
				else 
					pp = p.getOutputPorts();
				for(PipePort ppp : pp) {
					if(ppp.getLabel().equalsIgnoreCase(port))
						return ppp;
				}
				if(p instanceof PipeSource) {
					if(((PipeSource) p).getParentPort().getLabel().equalsIgnoreCase(port))
						return ((PipeSource) p).getParentPort();
					if(((PipeSource) p).getChildPort().getLabel().equalsIgnoreCase(port))
						return ((PipeSource) p).getChildPort();
				}
			
			}
			
		}
		return null;
	}
	private String xmlParseConnections(Document document, Element rootMod) {
		String msg = ""; 
		Vector<Element> nl = JistXMLUtil.xmlReadElementList(rootMod, "conn");
		for(Element el : nl){
			String src = JistXMLUtil.xmlReadTag(el, "src");
			
			String dest =  JistXMLUtil.xmlReadTag(el, "dest");
			
			String srcport =  JistXMLUtil.xmlReadTag(el, "src-port");
			String srcportindex =  JistXMLUtil.xmlReadTag(el, "src-port-index");
			
			String destport =  JistXMLUtil.xmlReadTag(el, "dest-port");
			try {
				PipePort srcP = findPipePort(src,srcport,false);
				PipePort destP = findPipePort(dest,destport,true);
				PipeConnector pc = PipeConnector.connect(srcP, destP);
				if(srcportindex!=null)
					pc.setSourceIndex(Integer.valueOf(srcportindex));
			} catch (Exception e) {
				msg+=("Cannot parse connection: "+src+"("+srcport+")"+" --> "+dest+"("+destport+")")+"\n"+e.toString();
			}


		}
		return msg;
		/*
		boolean ret = false;
		for(PipeModule pipe : pipes) {					
			if(pipe.xmlEncodeConnections(document, rootMod)){							
				ret=true;
			}
		}
		return ret;
		 */
	}


	/**
	 * Create "Open Dialog".
	 * 
	 * @return the pipe layout
	 */
	public static PipeLayout open() {
		return open(PipelineLayoutTool.getInstance());
	}

	/**
	 * Create "Open Dialog".
	 * 
	 * @param parent Parent component
	 * 
	 * @return the pipe layout
	 */
	public static PipeLayout open(Component parent) {
		return PipeLayout.read(selectOpenFile(parent));
	}

	/**
	 * Read layout from file.
	 * 
	 * @param f layout location
	 * 
	 * @return layout
	 */
	public static PipeLayout read(File f) {
		if ((f == null) || !f.exists()) {
			return null;
		}
		BufferedReader in;
		if (FileReaderWriter.getFileExtension(f).equals("pipe")) {
			LoniPipeParser parser = new LoniPipeParser();
			PipeLayout layout = parser.openLayout(f);
			if (layout != null) {
				JistPreferences.getPreferences().addFileToHistory(f);
				JistPreferences.savePreferences();
			}
			return layout;
		} else if (FileReaderWriter.getFileExtension(f).equals("LayoutXML")) {
			try {
				// Create input stream from file
				in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				String text = "";
				StringBuffer buff = new StringBuffer();
				String str;
				// Read file as string
				while ((str = in.readLine()) != null) {
					buff.append(str + "\n");
				}
				text = buff.toString();
				in.close();
				// Reconstruct class from XML
				PipeLayout layout = fromSaneXML(text);

				/*
				for(PipeModule mod:layout.getPipes()){
					mod.getInputParams().clean();
				}
				 */
				if (layout != null) {
					layout.lastModified=f.lastModified();
					layout.setFileLocation(f);
					layout.setDirty(false);
					JistPreferences.getPreferences().addFileToHistory(f);
				} else {
					System.err.println("jist.base"+"Error occured while reading parameter file:\n" + f);
				}
				return layout;
			} catch (Exception e) {
				System.err.println("jist.base"+"Error occured while reading parameter file:\n" + e.getMessage());
				e.printStackTrace();
				return null;
			}
		} else {
			try {
				// Create input stream from file
				in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				String text = "";
				StringBuffer buff = new StringBuffer();
				String str;
				// Read file as string
				while ((str = in.readLine()) != null) {
					buff.append(str + "\n");
				}
				text = buff.toString();
				in.close();
				// Reconstruct class from XML
				PipeLayout layout = fromXML(text);

				/*
				for(PipeModule mod:layout.getPipes()){
					mod.getInputParams().clean();
				}
				 */
				if (layout != null) {
					layout.lastModified=f.lastModified();
					layout.setFileLocation(f);
					layout.setDirty(false);
					JistPreferences.getPreferences().addFileToHistory(f);
				} else {
					System.err.println("jist.base"+"Error occured while reading parameter file:\n" + f);
				}
				return layout;
			} catch (Exception e) {
				System.err.println("jist.base"+"Error occured while reading parameter file:\n" + e.getMessage());
				e.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * Select layout to open.
	 * 
	 * @param parent parent component
	 * 
	 * @return file location
	 */
	private static File selectOpenFile(Component parent) {
		JFileChooser loadDialog = new JFileChooser("Specify Pipeline Profile");
		loadDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		loadDialog.setDialogType(JFileChooser.OPEN_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(JistPreferences.getPreferences().getAllValidLayoutExtensions()));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showOpenDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return loadDialog.getSelectedFile();
		} else {
			return null;
		}
	}

	/** Layout preferences. */
	protected RunParameters params;

	/** List of all modules for this layout. */
	protected Vector<PipeModule> pipes;

	/** file location for this layout. */
	transient protected File file;

	/** internal frame used to display this layout. */
	transient PipeInternalFrame frame;

	/** flag to indicate whether the layout has changed and needs to be saved. */
	transient protected boolean dirty;

	/** scheduler used to schedule execution of this layout. */
	transient protected PipeScheduler scheduler;

	/** flag indicating that layout is initializing and should not be edited. */
	transient protected boolean initializing = false;

	/** Time for when the file was last modified. */
	transient protected long lastModified=-1;

	/**
	 * Detect if current version of layout is most current.
	 * 
	 * @return true, if checks if is current
	 */
	public boolean isCurrent(){
		if(lastModified!=-1&&lastModified<getFileLocation().lastModified()){
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Gets the last modified time.
	 * 
	 * @return the last modified time
	 */
	public long getLastModifiedTime(){
		return lastModified;
	}

	/**
	 * Instantiates a new pipe layout.
	 */
	public PipeLayout() {
		super();
		this.dirty = false;
		params = new RunParameters();
		pipes = new Vector<PipeModule>();
	}

	/**
	 * Instantiates a new pipe layout.
	 * 
	 * @param algo the algorithm
	 */
	public PipeLayout(PipeAlgorithm algo) {
		super();
		this.dirty = false;
		params = new RunParameters();
		pipes = new Vector<PipeModule>();
		pipes.add(algo);
	}

	/**
	 * The Constructor.
	 * 
	 * @param frame Bind layout to specified frame
	 */
	public PipeLayout(PipeInternalFrame frame) {
		this();
		this.dirty = false;
		this.frame = frame;
	}

	/**
	 * Add module to layout.
	 * 
	 * @param mod module
	 */
	public void add(PipeModule mod) {
		if (!initializing) {
			if (!pipes.contains(mod)) {
				if (mod instanceof PipeAlgorithmGroup) {
					Vector<PipeAlgorithm> children = ((PipeAlgorithmGroup) mod).getGroupModules();
					for (PipeAlgorithm child : children) {
						if (pipes.contains(child)) {
							pipes.remove(child);
						}
					}
				}
				pipes.add(mod);
			}
		}
	}

	/**
	 * Ask user if they want to save before closing layout.
	 * 
	 * @return true if window should be disposed.
	 */
	public boolean close() {
		int n = 1;
		if (isDirty() || (file == null)) {
			n = JOptionPane.showOptionDialog(frame, "Would you like to save " + getTitle() + " before closing?", null,
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		}
		switch (n) {
		case 0:
			return (saveAs());
		case 1:
			return true;
		default:
			return false;
		}
	}

	/**
	 * Dispose of all resources associated with this layout.
	 */
	//	public void dispose() {
	//		for (PipeModule mod : pipes) {
	//			for (ParamModel model : mod.getInputParams().getAllVisibleDescendants()) {
	//				if (model instanceof ParamVolume) {
	//					(((ParamVolume) model)).dispose();
	//				}
	//				if (model instanceof ParamVolumeCollection) {
	//					(((ParamVolumeCollection) model)).dispose();
	//				}
	//			}
	//		}
	//	}

	/**
	 * Get all modules including parents and children.
	 * 
	 * @return the all descendant pipes
	 */
	public Vector<PipeModule> getAllDescendantPipes() {
		Vector<PipeModule> ret = new Vector<PipeModule>();
		for (PipeModule pipe : pipes) {
			ret.add(pipe);
			if (pipe instanceof PipeAlgorithmGroup) {
				ret.addAll(((PipeAlgorithmGroup) pipe).getAllDescendantModules());
			}
		}
		return ret;
	}

	/**
	 * Get all leaf modules.
	 * 
	 * @return leaf modules
	 */
	public Vector<PipeModule> getAllLeafPipes() {
		Vector<PipeModule> ret = new Vector<PipeModule>();
		for (PipeModule pipe : pipes) {
			if (pipe instanceof PipeAlgorithmGroup) {
				ret.addAll(((PipeAlgorithmGroup) pipe).getAllLeafModules());
			} else {
				ret.add(pipe);
			}
		}
		return ret;
	}

	/**
	 * Returns file location for this layout.
	 * 
	 * @return layout location
	 */
	public File getFileLocation() {
		return file;
	}

	/**
	 * Get input parameters for particular module by identifying name.
	 * 
	 * @param name identifying name
	 * 
	 * @return input parameter
	 */
	public ParamModel getModuleInputParam(String name) {
		for (PipeModule mod : pipes) {
			ParamModel param = mod.getInputParams().getFirstChildByName(name);
			if (param != null) {
				return param;
			}
		}
		return null;
	}

	/**
	 * Get module output parameters.
	 * 
	 * @param name identifying name
	 * 
	 * @return input parameter
	 */
	public ParamModel getModuleOutputParam(String name) {
		for (PipeModule mod : pipes) {
			ParamModel param = mod.getOutputParams().getFirstChildByName(name);
			if (param != null) {
				return param;
			}
		}
		return null;
	}

	/**
	 * Get all modules for this layout.
	 * 
	 * @return modules
	 */
	public Vector<PipeModule> getPipes() {
		return pipes;
	}

	/**
	 * Runt.
	 * 
	 * @return the run parameters
	 */
	public RunParameters getRunParameters() {
		return params;
	}


	/**
	 * Get scheduler.
	 * 
	 * @return scheduler
	 */
	public PipeScheduler getScheduler() {
		return scheduler;
	}

	/**
	 * Get title of layout. Add asterisk if layout has been modified since last
	 * change
	 * 
	 * @return the title
	 */
	public String getTitle() {
		if (file != null) {
			return file.getName();
		} else {
			if(frame==null){
				return null;
			} else {
				String title = frame.getTitle();
				if (title.charAt(0) == '*') {
					title = title.substring(1, title.length());
				}
				return title;
			}
		}
	}

	/**
	 * Initialize transient fields that could not be deserialized.
	 */
	public void init() {
		init(null, null);
	}

	/**
	 * Initialize transient fields that could not be deserialized.
	 * 
	 * @param graph Layout graph if one is available, null otherwise
	 * @param monitor the monitor
	 */
	public void init(PipeJGraph graph, ProgressMonitor monitor) {
		initializing = true;
		Refresher.getInstance().pauseAll();
		if (graph != null) {
			if(monitor!=null){
				monitor.setMillisToPopup(0);
				monitor.setNote("Loading "+this.getFileLocation().getName()+" ...");
			}
			Vector<PipeModule> allPipes = getAllDescendantPipes();
			for (PipeModule pipe : allPipes) { 
				/* Delay shortened by Bennett - seems to add much unnecessary load time.*/ 
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				//No longer needed since we do not want to load volumes
				//pipe.getInputParams().loadResources(pipe.getInputParams(), monitor);
				pipe.init(null);

			}
		}
		for (PipeModule pipe : getPipes()) {
			Vector<PipePort> ports = pipe.getInputPorts();
			for (PipePort port : ports) {
				port.setPortType(PipePort.type.INPUT);
			}
			ports = pipe.getOutputPorts();
			for (PipePort port : ports) {
				port.setPortType(PipePort.type.OUTPUT);
			}
			if (pipe instanceof PipeSource) {
				((PipeSource) pipe).getChildPort().init();
				((PipeSource) pipe).getParentPort().init();
			}
			pipe.getInputParams().init();
			pipe.getOutputParams().init();
			if (pipe instanceof PipeAlgorithmGroup) {
				Vector<AlgorithmCell> cells = new Vector<AlgorithmCell>();
				for (PipePort port : pipe.getInputPorts()) {
					port.setPortType(PipePort.type.INPUT);
					port.setOwner(pipe);
				}
				for (PipePort port : pipe.getOutputPorts()) {
					port.setPortType(PipePort.type.OUTPUT);
					port.setOwner(pipe);
				}
				for (PipeModule gpipe : ((PipeAlgorithmGroup) pipe).getGroupModules()) {
					Vector<PipePort> gports = pipe.getInputPorts();
					for (PipePort gport : gports) {
						gport.setPortType(PipePort.type.INPUT);
					}
					gports = pipe.getOutputPorts();
					for (PipePort gport : gports) {
						gport.setPortType(PipePort.type.OUTPUT);
					}
					gpipe.getInputParams().init();
					gpipe.getOutputParams().init();
					if (graph != null) {
						gpipe.getInputParams().getInputView().addObserver(gpipe);
						gpipe.addListener(ParameterPanel.getInstance());
					}
					cells.add((AlgorithmCell) gpipe.init(graph));
				}
				if (graph != null) {
					AlgorithmGroupCell cell = ((PipeAlgorithmGroup) pipe).createModuleCell();
					cell.setChildrenCells(cells);
					graph.getGraphLayoutCache().insertGroup(cell, cells.toArray());
					graph.getGraphLayoutCache().collapse(new Object[] { cell });
				}
			} else {
				if (graph != null) {
					pipe.getInputParams().getInputView().addObserver(pipe);
					pipe.init(graph);
				}
			}
		}
		for (PipeModule pipe : getAllLeafPipes()) {
			// Connect Input Ports
			Vector<PipePort> ports = pipe.getInputPorts();
			for (PipePort port : ports) {
				if (port instanceof ParamModel) {
					if (graph != null) {
						((ParamModel) port).getInputView().update();
					}
				}
				port.addListener(pipe);
				if (port.getIncomingConnectors().size() > 0) {
					port.notifyListenersOfConnection();
					port.setUseConnector(true);
				}
			}
			// Connect Output Ports
			ports = pipe.getOutputPorts();
			for (PipePort port : ports) {
				port.addListener(pipe);
				port.notifyListenersOfConnection();
			}
			if (pipe instanceof PipeSource) {
				((PipeSource) pipe).getParentPort().addListener(pipe);
				((PipeSource) pipe).getParentPort().notifyListenersOfConnection();
				((PipeSource) pipe).getChildPort().addListener(pipe);
				((PipeSource) pipe).getChildPort().notifyListenersOfConnection();
			}
		}
		Refresher.getInstance().refreshAll();
		Refresher.getInstance().resumeAll();
		initializing = false;
	}

	/**
	 * Returns true if layout has changed since last save.
	 * 
	 * @return true if changes made
	 */
	public boolean isDirty() {
		return (dirty);
	}

	/**
	 * Returns true if layout is initializing and should not be edited.
	 * 
	 * @return true if initializing
	 */
	public boolean isInitializing() {
		return initializing;
	}

	/**
	 * Remove module from layout.
	 * 
	 * @param mod module
	 */
	public void remove(PipeModule mod) {
		if (mod instanceof PipeAlgorithmGroup) {
			((PipeAlgorithmGroup) mod).removeAllChildren();
			PipeAlgorithmGroup parent = ((PipeAlgorithmGroup) mod).getParentGroup();
			if (parent != null) {
				parent.remove((PipeAlgorithm) mod);
			} else {
				pipes.remove(mod);
			}
		}
		if (mod instanceof PipeAlgorithm) {
			PipeAlgorithmGroup parent = ((PipeAlgorithm) mod).getParentGroup();
			if (parent != null) {
				parent.remove((PipeAlgorithm) mod);
			} else {
				pipes.remove(mod);
			}
		} else {
			pipes.remove(mod);
		}
	}

	/**
	 * Open "Save" dialog.
	 * 
	 * @param commit Force layout to commit input parameters from parameter pane
	 * before saving
	 * 
	 * @return true, if save
	 */
	public boolean save(boolean commit) {
		if (file != null) {
			return write(file, commit);
		} else {
			return saveAs(commit);
		}
	}

	/**
	 * Open "Save As" dialog.
	 * 
	 * @return true, if save as
	 */
	public boolean saveAs() {
		return saveAs(true);
	}

	/**
	 * Open "Save As" dialog.
	 * 
	 * @param commit Force layout to commit input parameters from parameter pane
	 * before saving
	 * 
	 * @return true, if save as
	 */
	public boolean saveAs(boolean commit) {
		File f = selectSaveFile(null);
		if (write(f, commit)) {
			this.params.outputDir = f.getParentFile();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Select location to save layout.
	 * 
	 * @param parent parent component
	 * 
	 * @return file location
	 */
	private File selectSaveFile(Component parent) {
		JFileChooser saveDialog = new JFileChooser("Save Pipeline Layout");
		saveDialog.setCurrentDirectory(MipavController.getDefaultWorkingDirectory());
		if (file != null) {
			saveDialog.setSelectedFile(file);
		} else {
			saveDialog.setSelectedFile(new File(getTitle()));
		}
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		FileExtensionFilter fe =new FileExtensionFilter(JistPreferences.getPreferences().getAllValidLayoutExtensions());
		saveDialog.setFileFilter(fe);
		saveDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = saveDialog.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File f = saveDialog.getSelectedFile();		
			if(!fe.accept(f)) {
				String message = "The selected file invalid. Should I use "+JistPreferences.getPreferences().getDefaultLayoutExtension()+" instead?";
				String title = "Invalid extension";
				int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION);
				if (reply == JOptionPane.CANCEL_OPTION)
				{
					return null;
				}
				f = new File(f.getParent(), FileReaderWriter.getFileName(f) +"."+JistPreferences.getPreferences().getDefaultLayoutExtension());
			} 
			if(f.exists()) {
				String message = "The selected file already exists. Should I overwrite it?";
				String title = "File Exists";
				int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION);
				if (reply == JOptionPane.CANCEL_OPTION)
				{
					return null;
				}
			}
			return f;
		} else {
			return null;
		}
	}

	/**
	 * Indicate that the layout has changed since last save.
	 * 
	 * @param dirty true if layout has changed
	 */
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		if(frame!=null){
			String title = getTitle();
			if (frame != null) {
				if (dirty) {
					frame.setTitle("*" + title);
				} else {
					frame.setTitle(title);
				}
			}
		}
	}

	/**
	 * Set file location for this layout.
	 * 
	 * @param file layout location
	 */
	public void setFileLocation(File file) {
		this.file = file;
		if(getRunParameters().getOutputDirectory()==null){
			getRunParameters().setOutputDirectory(file.getParentFile());
		}
		if (frame != null) {
			frame.setTitle(file.getName());
		}
	}

	/**
	 * Set frame used to render this layout.
	 * 
	 * @param parent internal frame
	 */
	public void setFrame(PipeInternalFrame parent) {
		this.frame = parent;
		if (frame != null) {
			frame.setTitle(file.getName());
		}
	}

	/**
	 * Set root input directory.
	 * 
	 * @param f root input directory
	 */
	public void setInputDir(File f) {
		if ((f == null&&params.inputDir!=null) || (f!=null&&!f.equals(params.inputDir))) {
			setDirty(true);
		}
		if(f==null){
			params.inputURI=null;
		}
		params.inputDir = (f != null) ? f.getAbsoluteFile() : null;
	}

	/**
	 * Set root input directory.
	 * 
	 * @param f root input directory
	 */
	public void setInputDir(URI f) {
		if ((f == null&&params.inputURI!=null) || (f!=null&&!f.equals(params.inputURI))) {
			setDirty(true);
		}
		params.inputURI = (f != null) ? f : null;
	}


	/**
	 * Set maximum allowable heap.
	 * 
	 * @param heap maximum heap size
	 */
	public void setMaxHeap(int heap) {
		if (params.maxHeap != heap) {
			setDirty(true);
			JistPreferences.getPreferences().setDefaultMemory(heap);
		}
		params.maxHeap = heap;
	}

	/**
	 * Set maximum allowable simultaneous processes.
	 * 
	 * @param procs maximum number of processes
	 */
	public void setMaxProcs(int procs) {
		if (params.maxProcs != procs) {
			setDirty(true);
			JistPreferences.getPreferences().setDefaultMaxProcesses(procs);
		}

		params.maxProcs = procs;
	}

	/**
	 * Set naming convention for output directory.
	 * 
	 * @param convention naming convention
	 */
	public void setNamingConvention(NamingConvention convention) {
		if (params.namingConvention != convention) {
			setDirty(true);
			JistPreferences.getPreferences().setNamingConvention(convention);
		}
		params.namingConvention = convention;
	}

	/**
	 * Sets the use grid engine.
	 * 
	 * @param useGrid the new use grid engine
	 */
	public void setUseGridEngine(boolean useGrid) {
		if (params.useGridEngine != useGrid) {
			setDirty(true);
			JistPreferences.getPreferences().setUseGridEngine(useGrid);
		} else {
			JistPreferences.getPreferences().setUseGridEngine(useGrid);
		}

		params.useGridEngine = useGrid;
	}

	/**
	 * Set root output directory.
	 * 
	 * @param f root output directory
	 */
	public void setOutputDir(File f) {
		if ((f == null&&params.outputDir!=null) || (f!=null&&!f.equals(params.outputDir))) {
			setDirty(true);
		}
		if(f!=null){
			params.setOutputDirectory(f);
		}
	}

	/**
	 * Set root output directory.
	 * 
	 * @param f root output directory
	 */
	public void setOutputDir(URI f) {
		if ((f == null&&params.outputURI!=null) || (f!=null&&!f.equals(params.outputURI))) {
			setDirty(true);
		}
		if(f!=null){
			params.setOutputDirectory(f);
		}
	}

	/**
	 * Set runtime parameters.
	 * 
	 * @param runParams runtime parameters
	 */
	public void setRunParams(RunParameters runParams) {
		this.params = runParams;
	}

	/**
	 * Set scheduler.
	 * 
	 * @param scheduler scheduler
	 */
	public void setScheduler(PipeScheduler scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * Serialize class as XML.
	 * 
	 * @return the string
	 * @deprecated 
	 */
	public String toXML() {
		JistLogger.logOutput(JistLogger.INFO, "Using deprecated layout format.");
		XStream stream = new XStream();
		return stream.toXML(this);
	}

	public String toSaneXML() {
		try{
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
			System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");

			DocumentBuilderFactory documentBuilderFactory = 
				DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder;

			documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.newDocument();

			Element root = document.createElement("PipeLayout");

			Element rootParam = document.createElement("RunParameters");


			getRunParameters().xmlBuildHeader(document,rootParam);
			root.appendChild(rootParam);

			Element rootMod = document.createElement("Modules");			
			if(xmlBuildModules(document,rootMod))
				root.appendChild(rootMod);


			Element rootConn = document.createElement("Connections");

			if(xmlBuildConnections(document,rootConn))
				root.appendChild(rootConn);

			document.appendChild(root);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter stringOut = new StringWriter();
			StreamResult result =  new StreamResult(stringOut);
			transformer.transform(source, result);
			System.out.println(stringOut.toString());
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
			System.out.flush();
			return stringOut.toString();

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}






	private boolean xmlBuildModules(Document document, Element rootMod) {
		// protected Vector<PipeModule> pipes;
		boolean ret = false;
		PipeModule.resetLayoutUniqueIndex();
		for(PipeModule pipe : pipes) {
			pipe.setLayoutUniqueID();
			ret=true;
			Element em = document.createElement("Module");
			try { 
			if(pipe.xmlEncodeModule(document,em)) {
				rootMod.appendChild(em);
				ret = true;
			} 
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return ret;

	}

	private boolean xmlBuildConnections(Document document, Element rootMod) {
		// protected Vector<PipeModule> pipes;
		boolean ret = false;
		for(PipeModule pipe : pipes) {					
			if(pipe.xmlEncodeConnections(document, rootMod)){							
				ret=true;
			}
		}
		return ret;
	}



	/**
	 * Ungroup module group.
	 * 
	 * @param mod module group
	 */
	public void ungroup(PipeAlgorithmGroup mod) {
		PipeAlgorithmGroup parent = mod.getParentGroup();
		Vector<PipeAlgorithm> children = (mod).getGroupModules();
		if (parent != null) {
			for (PipeAlgorithm child : children) {
				parent.add(child);
			}
		} else {
			for (PipeAlgorithm child : children) {
				child.setParentGroup(null);
				add(child);
			}
		}
		children.clear();
	}

	/**
	 * Clean.
	 * 
	 * @param layout the layout
	 * 
	 * @return the pipe layout
	 */
	public static PipeLayout clean(PipeLayout layout){
		//			PipeLayout deepClone=fromSaneXML(layout.toSaneXML());
		PipeLayout deepClone=fromXML(layout.toXML());
		Vector<PipeModule> origMods=layout.getAllDescendantPipes();
		Vector<PipeModule> cloneMods=deepClone.getAllDescendantPipes();
		for(int i=0;i<origMods.size();i++){
			PipeModule origMod=origMods.get(i);
			PipeModule cloneMod=cloneMods.get(i);
			Vector<PipePort> origPorts=origMod.getInputPorts();
			Vector<PipePort> clonePorts=cloneMod.getInputPorts();
			//Clean input parameters
			for(int j=0;j<origPorts.size();j++){
				PipePort origPort=origPorts.get(j);
				PipePort clonePort=clonePorts.get(j);
				if(origPort.isConnected()&&clonePort instanceof ParamModel){
					((ParamModel)clonePort).clean();
				}
			}
			//Clean all output ports
			for(PipePort port:cloneMod.getOutputPorts()){
				if(port instanceof ParamModel){
					((ParamModel)port).clean();
				}			
			}
		}
		return deepClone;
	}

	/**
	 * Replace path.
	 * 
	 * @param layout the layout
	 * @param origDir the orig dir
	 * @param replaceDir the replace dir
	 */
	public static void replacePath(PipeLayout layout,File origDir,File replaceDir){
		Vector<PipeModule> origMods=layout.getAllDescendantPipes();
		for(int i=0;i<origMods.size();i++){
			PipeModule origMod=origMods.get(i);
			Vector<PipePort> origPorts=origMod.getInputPorts();
			//Replace file directories for input parameters
			for(PipePort port:origMod.getInputPorts()){
				if(port instanceof ParamModel){
					((ParamModel)port).replacePath(origDir,replaceDir);
				}			
			}
		}
	}

	/**
	 * Write layout to file.
	 * 
	 * @param f output file
	 * @param commit Force layout commit current values in parameter pane before
	 * saving
	 * 
	 * @return true, if write
	 */
	public boolean write(File f, boolean commit) {
		PrintWriter out;
		if (f == null) {
			return false;
		}
		try {
			if (commit) {
				for (PipeModule mod : getPipes()) {
					mod.getInputParams().getInputView().commit();
				}
			}
			this.setFileLocation(f);
			out = new PrintWriter(new BufferedWriter(new FileWriter(f)));
			String text;
			String fname = f.toString(); 
			String ext=fname.lastIndexOf(".")==-1?"":fname.substring(fname.lastIndexOf(".")+1,fname.length());
			if(0==ext.compareToIgnoreCase("LayoutXML")) {
				text = clean(this).toSaneXML();
			} else {
				text = clean(this).toXML();
			}

			out.print(text);
			out.flush();
			out.close();
			this.setDirty(false);
			JistPreferences.getPreferences().addFileToHistory(f);
			JistPreferences.savePreferences();
			lastModified=f.lastModified();
			return true;
		} catch (IOException e) {
			System.err.println("jist.base"+e.getMessage());
			return false;
		}
	}

	public boolean validateAndVerify() {
		for(PipeModule p : pipes) {
			try {
				p.reconcileAndMerge();
			} catch (InvalidJistMergeException e) {
				return false;
			}
		}
		return true;
	}
}