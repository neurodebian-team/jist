/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.view.ViewUserInterface;

import java.util.Vector;

// TODO: Auto-generated Javadoc
/**
 * Overload MIPAV's view user interface.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MipavViewUserInterface extends ViewUserInterface {
	
	/** The my user interface reference. */
	static ViewUserInterface myUserInterfaceReference=null;
	
	/**
	 * Create view user interface.
	 * 
	 * @return the view user interface
	 */
	public static ViewUserInterface create() {
		if(userInterfaceReference!=null) {			
			myUserInterfaceReference = userInterfaceReference;
			JistLogger.logOutput(JistLogger.INFO, "MipavViewUserInterface: Defaulting to base controller."); System.out.flush();
		}
		if (myUserInterfaceReference == null) {
			JistLogger.logOutput(JistLogger.INFO, "MipavViewUserInterface: Starting MIPAV with forceQuiet=true"); 
			System.out.flush();
			myUserInterfaceReference=new MipavViewUserInterface(true);
			userInterfaceReference = myUserInterfaceReference;
		}
		return myUserInterfaceReference;
	}

	/**
	 * Get a reference to the ViewUserInterface object.
	 * 
	 * @return ViewUserInterface
	 */
	public static ViewUserInterface getReference() {
		if(myUserInterfaceReference ==null)
			create();
		return myUserInterfaceReference;
		/*if(ViewUserInterface.userInterfaceReference instanceof MipavViewUserInterface){
			return (MipavViewUserInterface) MipavViewUserInterface.myUserInterfaceReference;
		} else {
			
			return ViewUserInterface.getReference();
		}*/
			
	}

	/**
	 * Instantiates a new mipav view user interface.
	 */
	public MipavViewUserInterface() {
		this(true);
	};
	
	/**
	 * Instantiates a new mipav view user interface.
	 * 
	 * @param flag the flag
	 */
	public MipavViewUserInterface(boolean flag) {		
		//Ignore forceCheck flag and set to ignore
		super(true); // set the "shutup" = "force no checks" flag
		// Set global singleton to use this version of ViewUserInterface
		JistLogger.logOutput(JistLogger.INFO, "MipavViewUserInterface: Created MIPAV with forceQuiet="+flag); 
		System.out.flush();
		ViewUserInterface.userInterfaceReference = this;
		MipavScriptRunner scriptRunner = new MipavScriptRunner();
	}

	/**
	 * Accessor to check whether the application frame visible or not.
	 * 
	 * @return isAppFrameVisible application frame visibility flag
	 */
	public boolean isAppFrameVisible() {
		return false;
	}

	/**
	 * Accessor to see if a stand-alone plugin frame is visible (not app frame).
	 * 
	 * @return true, if checks if is lug in frame visible
	 */
	public boolean isPlugInFrameVisible() {
		return false;
	}

	/**
	 * Run command line from script file.
	 * 
	 * @param scriptFile the script file
	 * @param imageList the image list
	 * @param voiList the voi list
	 */
	public void runCmdLine(String scriptFile, Vector imageList, Vector voiList) {
		super.runCmdLine(scriptFile, imageList, voiList);
	}
}
