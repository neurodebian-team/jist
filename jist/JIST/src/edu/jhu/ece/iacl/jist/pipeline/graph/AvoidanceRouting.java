/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Vector;

import org.jgraph.graph.CellView;
import org.jgraph.graph.EdgeView;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.PortView;
import org.jgraph.graph.VertexView;
import org.jgraph.graph.DefaultEdge.DefaultRouting;
import org.jgraph.graph.Edge.Routing;

/**
 * Algorithm for routing connectors so that they avoid intersecting graph
 * modules.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class AvoidanceRouting extends DefaultRouting implements Routing {
	
	/**
	 * Possible path to traverse that will bring the connector end closer to its
	 * destination.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	private class AvoidancePath implements Comparable<AvoidancePath> {
		
		/** The direction. */
		public Dir direction;
		
		/** The dist to dest. */
		protected double distToDest;
		
		/** The path length. */
		protected double pathLength;
		
		/** The depth. */
		public int depth;
		
		/** The children. */
		public ArrayList<AvoidancePath> children;
		
		/** The parent. */
		public AvoidancePath parent;
		
		/** The path. */
		Line2D path;
		
		/** The obstacle. */
		protected Rectangle2D obstacle = null;

		/**
		 * Constructor.
		 * 
		 * @param from
		 *            source point
		 * @param to
		 *            destination point
		 * @param direction
		 *            initial direction
		 */
		public AvoidancePath(Point2D from, Point2D to, Dir direction) {
			this(from, to, direction, null);
		}

		/**
		 * Constructor.
		 * 
		 * @param from
		 *            source point
		 * @param to
		 *            destination point
		 * @param direction
		 *            initial direction
		 * @param parent
		 *            parent path node
		 */
		public AvoidancePath(Point2D from, Point2D to, Dir direction, AvoidancePath parent) {
			this.direction = direction;
			this.parent = parent;
			path = new Line2D.Double((Point2D) from.clone(), (Point2D) to.clone());
			distToDest = Double.MAX_VALUE;
			children = new ArrayList<AvoidancePath>();
			updatePath(to);
			updateDistToDestination(to);
		}

		/**
		 * Add child path.
		 * 
		 * @param child
		 *            path
		 */
		public void addChild(AvoidancePath child) {
			children.add(child);
			child.depth = this.depth + 1;
		}

		/**
		 * Find path by back-tracking to root from leaf.
		 * 
		 * @return path of control points
		 */
		public Vector<Point2D> backTrack() {
			Vector<Point2D> path = new Vector<Point2D>();
			path.add(backTrack(path));
			return path;
		}

		/**
		 * Find path by back-tracking to root from leaf.
		 * 
		 * @param pointList
		 *            list of point to append to
		 * @return end point
		 */
		protected Point2D backTrack(Vector<Point2D> pointList) {
			if (parent != null) {
				pointList.add(parent.backTrack(pointList));
			} else {
				pointList.add(path.getP1());
			}
			return path.getP2();
		}

		/**
		 * Compare collision paths by distances to destination.
		 * 
		 * @param node
		 *            the node
		 * @return the int
		 */
		public int compareTo(AvoidancePath node) {
			if (distToDest == node.distToDest) {
				if (pathLength == node.pathLength) {
					return (int) Math.signum(depth - node.depth);
				} else {
					return (int) Math.signum(pathLength - node.pathLength);
				}
			} else {
				return (int) Math.signum(distToDest - node.distToDest);
			}
		}

		/**
		 * Create children directions that will bring the connector closer to
		 * the destination.
		 * 
		 * @param to
		 *            the to
		 * @return true, if creates the children
		 */
		public boolean createChildren(Point2D to) {
			if ((direction == Dir.North) || (direction == Dir.South)) {
				AvoidancePath east = new AvoidancePath(path.getP2(), to, Dir.East, this);
				AvoidancePath west = new AvoidancePath(path.getP2(), to, Dir.West, this);
				if ((east.getDistanceToDestination() == 0) || (west.getDistanceToDestination() == 0)) {
					if (east.getDistanceToDestination() == 0) {
						addChild(east);
					}
					if (west.getDistanceToDestination() == 0) {
						addChild(west);
					}
					return true;
				} else {
					addChild(east);
					addChild(west);
					return false;
				}
			} else {
				AvoidancePath south = new AvoidancePath(path.getP2(), to, Dir.South, this);
				AvoidancePath north = new AvoidancePath(path.getP2(), to, Dir.North, this);
				if ((north.getDistanceToDestination() == 0) || (south.getDistanceToDestination() == 0)) {
					if (north.getDistanceToDestination() == 0) {
						addChild(north);
					}
					if (south.getDistanceToDestination() == 0) {
						addChild(south);
					}
					return true;
				} else {
					addChild(north);
					addChild(south);
					return false;
				}
			}
		}

		/**
		 * Create descendants down to a certain depth.
		 * 
		 * @param to
		 *            destination point
		 * @param depth
		 *            maximum depth
		 * @return the array list< avoidance path>
		 */
		public ArrayList<AvoidancePath> createDescendants(Point2D to, int depth) {
			ArrayList<AvoidancePath> ret = new ArrayList<AvoidancePath>();
			if (!createChildren(to)) {
				if (depth != 0) {
					for (AvoidancePath child : children) {
						ret.addAll(child.createDescendants(to, depth - 1));
					}
				}
			} else {
				ret.addAll(children);
			}
			if (depth == 0) {
				ret.addAll(children);
			}
			return ret;
		}

		/**
		 * Two collision paths are equal if their end points are the same.
		 * 
		 * @param obj
		 *            the obj
		 * @return true, if equals
		 */
		public boolean equals(Object obj) {
			if (obj instanceof AvoidancePath) {
				return (path.getP1().equals(((AvoidancePath) obj).path.getP1()) && path.getP2().equals(
						((AvoidancePath) obj).path.getP2()));
			} else {
				return false;
			}
		}

		/**
		 * From a particular start location and a particular direction, find the
		 * next module that intersects the path.
		 * 
		 * @param point
		 *            the point
		 * @return the point2 d
		 */
		protected Point2D findNextBoundary(Point2D point) {
			Point2D p = (Point2D) point.clone();
			Point2D tmp;
			double minDist = Double.MIN_VALUE;
			if ((parent != null) && (parent.obstacle != null)) {
				Rectangle2D rect = parent.obstacle;
				switch (direction) {
				case North:
					p = new Point2D.Double(point.getX(), rect.getMinY() - BORDER_SPACE);
					break;
				case South:
					p = new Point2D.Double(point.getX(), rect.getMaxY() + BORDER_SPACE);
					break;
				case East:
					p = new Point2D.Double(rect.getMaxX() + BORDER_SPACE, point.getY());
					break;
				case West:
					p = new Point2D.Double(rect.getMinX() - BORDER_SPACE, point.getY());
					break;
				}
			} else {
				p = (Point2D) point.clone();
			}
			return p;
		}

		/**
		 * Get all descendants down to a certain depth.
		 * 
		 * @param depth
		 *            maximum depth
		 * @return list of descendants
		 */
		public ArrayList<AvoidancePath> getDescendants(int depth) {
			ArrayList<AvoidancePath> ret = new ArrayList<AvoidancePath>();
			if (depth == 0) {
				return ret;
			}
			ret.addAll(children);
			for (AvoidancePath child : children) {
				ret.addAll(child.getDescendants(depth - 1));
			}
			return ret;
		}

		/**
		 * Get distance to destination.
		 * 
		 * @return distance
		 */
		public double getDistanceToDestination() {
			return distToDest;
		}

		/**
		 * Get path path length.
		 * 
		 * @return the path length
		 */
		public double getPathLength() {
			return pathLength;
		}

		/**
		 * Get length of line segment.
		 * 
		 * @return the double
		 */
		public double lineLength() {
			return path.getP1().distance(path.getP2());
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			if ((parent != null) && (parent.obstacle != null)) {
				Point2D min = new Point2D.Double(parent.obstacle.getMinX(), parent.obstacle.getMinY());
				Point2D max = new Point2D.Double(parent.obstacle.getMaxX(), parent.obstacle.getMaxY());
				return "DIR:" + direction + " [" + path.getP1() + "->" + path.getP2() + "] " + min + ":" + max;
			} else {
				return "DIR:" + direction + " [" + path.getP1() + "->" + path.getP2() + "] ";
			}
		}

		/**
		 * Update distance to destination.
		 * 
		 * @param target
		 *            the target
		 * @return distance
		 */
		public double updateDistToDestination(Point2D target) {
			if (path != null) {
				distToDest = path.getP2().distance(target);
			} else {
				distToDest = Double.MAX_VALUE;
			}
			return distToDest;
		}

		/**
		 * Update path to be as close as it can be to the destination without
		 * intersecting a module.
		 * 
		 * @param to
		 *            destination point
		 * @return line segment
		 */
		public Line2D updatePath(Point2D to) {
			if (direction == null) {
				return path;
			}
			switch (direction) {
			case North:
				if (path.getP2().getY() >= path.getP1().getY()) {
					path = new Line2D.Double(path.getP1(), findNextBoundary(path.getP1()));
				} else {
					path = new Line2D.Double(path.getP1(), new Point2D.Double(path.getP1().getX(), path.getP2().getY()));
				}
				break;
			case South:
				if (path.getP2().getY() <= path.getP1().getY()) {
					path = new Line2D.Double(path.getP1(), findNextBoundary(path.getP1()));
				} else {
					path = new Line2D.Double(path.getP1(), new Point2D.Double(path.getP1().getX(), path.getP2().getY()));
				}
				break;
			case East:
				if (path.getP2().getX() <= path.getP1().getX()) {
					path = new Line2D.Double(path.getP1(), findNextBoundary(path.getP1()));
				} else {
					path = new Line2D.Double(path.getP1(), new Point2D.Double(path.getP2().getX(), path.getP1().getY()));
				}
				break;
			case West:
				if (path.getP2().getX() >= path.getP1().getX()) {
					path = new Line2D.Double(path.getP1(), findNextBoundary(path.getP1()));
				} else {
					path = new Line2D.Double(path.getP1(), new Point2D.Double(path.getP2().getX(), path.getP1().getY()));
				}
				break;
			}
			for (Rectangle2D obstacle : obstacles) {
				if (path.intersects(obstacle)) {
					switch (direction) {
					case South:
						path.setLine(path.getP1(), new Point2D.Double(path.getP1().getX(), obstacle.getMinY()
								- BORDER_SPACE));
						break;
					case North:
						path.setLine(path.getP1(), new Point2D.Double(path.getP1().getX(), obstacle.getMaxY()
								+ BORDER_SPACE));
						break;
					case West:
						path.setLine(path.getP1(), new Point2D.Double(obstacle.getMaxX() + BORDER_SPACE, path.getP1()
								.getY()));
						break;
					case East:
						path.setLine(path.getP1(), new Point2D.Double(obstacle.getMinX() - BORDER_SPACE, path.getP1()
								.getY()));
						break;
					}
					this.obstacle = obstacle;
				}
			}
			updatePathLength();
			return path;
		}

		/**
		 * Update path length based on current line segment and all previous
		 * line segments in path.
		 * 
		 * @return length of path
		 */
		public double updatePathLength() {
			this.pathLength = path.getP1().distance(path.getP2()) + ((parent != null) ? parent.updatePathLength() : 0);
			return pathLength;
		}
	};

	/**
	 * The Enum Dir.
	 */
	protected enum Dir {
		
		/** The North. */
		North, 
 /** The South. */
 South, 
 /** The East. */
 East, 
 /** The West. */
 West
	}

	/** The BORDE r_ space. */
	protected static double BORDER_SPACE = 10;
	
	/** The DEPT h_ limit. */
	protected static int DEPTH_LIMIT = 4;
	
	/** The MA x_ paths. */
	protected static int MAX_PATHS = 128;
	
	/** The obstacles. */
	protected ArrayList<Rectangle2D> obstacles;

	/**
	 * Compute avoidance route.
	 * 
	 * @param cache
	 *            layout cache
	 * @param from
	 *            start point
	 * @param to
	 *            end point
	 * @param direction
	 *            initial direction
	 * @return list of control points
	 */
	protected Vector computeRoute(GraphLayoutCache cache, Point2D from, Point2D to, Dir direction) {
		AvoidancePath root = null;
		obstacles = getObstacles(cache, from, to);
		Point2D origFrom = from;
		Point2D origTo = to;
		switch (direction) {
		case South:
			from = new Point2D.Double(from.getX(), from.getY() + BORDER_SPACE);
			to = new Point2D.Double(to.getX(), to.getY() - BORDER_SPACE);
			break;
		case North:
			from = new Point2D.Double(from.getX(), from.getY() - BORDER_SPACE);
			to = new Point2D.Double(to.getX(), to.getY() + BORDER_SPACE);
			break;
		case East:
			from = new Point2D.Double(from.getX() + BORDER_SPACE, from.getY());
			to = new Point2D.Double(to.getX() - BORDER_SPACE, to.getY());
			break;
		case West:
			from = new Point2D.Double(from.getX() - BORDER_SPACE, from.getY());
			to = new Point2D.Double(to.getX() + BORDER_SPACE, to.getY());
			break;
		}
		Vector path = new Vector();
		PriorityQueue<AvoidancePath> queue = new PriorityQueue<AvoidancePath>();
		root = new AvoidancePath(from, to, direction);
		queue.add(root);
		AvoidancePath optNode = root;
		int count = 0;
		boolean intersect;
		LinkedList<AvoidancePath> history = new LinkedList<AvoidancePath>();
		AvoidancePath head = null;
		while ((queue.size() > 0) && (count < MAX_PATHS)) {
			head = queue.remove();
			intersect = false;
			if (!intersect) {
				if (head.getDistanceToDestination() == 0) {
					optNode = head;
					break;
				}
				count++;
				ArrayList<AvoidancePath> children = head.createDescendants(to, DEPTH_LIMIT);
				boolean allowAdd = false;
				for (AvoidancePath child : children) {
					allowAdd = true;
					for (AvoidancePath node : history) {
						if (node.equals(child)) {
							allowAdd = false;
							break;
						}
					}
					if (allowAdd) {
						queue.add(child);
						history.add(child);
					}
				}
			}
		}
		path = optNode.backTrack();
		path.setElementAt(origFrom, 0);
		if ((direction == Dir.South) || (direction == Dir.North)) {
			if ((optNode.direction == Dir.South) || (optNode.direction == Dir.North)) {
				path.setElementAt(origTo, path.size() - 1);
			} else {
				path.add(origTo);
			}
			simplifyPath(path, 0);
		} else {
			if ((optNode.direction == Dir.East) || (optNode.direction == Dir.West)) {
				path.setElementAt(origTo, path.size() - 1);
			} else {
				path.add(origTo);
			}
			simplifyPath(path, 1);
		}
		if (path.size() == 2) {
			double dx = Math.abs(origFrom.getX() - origTo.getX());
			double dy = Math.abs(origFrom.getY() - origTo.getY());
			double x2 = origFrom.getX() + ((origTo.getX() - origFrom.getX()) / 2);
			double y2 = origFrom.getY() + ((origTo.getY() - origFrom.getY()) / 2);
			path.clear();
			// Add mid point for routing to insert label
			path.add(origFrom);
			if (dx > dy) {
				path.add(new Point2D.Double(x2, origFrom.getY()));
				path.add(new Point2D.Double(x2, origTo.getY()));
			} else {
				path.add(new Point2D.Double(origFrom.getX(), y2));
				path.add(new Point2D.Double(origTo.getX(), y2));
			}
			path.add(origTo);
		}
		return path;
	}

	/**
	 * Get list of rectangular obstacles in graph.
	 * 
	 * @param cache
	 *            layout cache
	 * @param from
	 *            start point
	 * @param to
	 *            end point
	 * @return list of rectangular obstacles
	 */
	protected ArrayList<Rectangle2D> getObstacles(GraphLayoutCache cache, Point2D from, Point2D to) {
		CellView[] views = cache.getAllViews();
		ArrayList<Rectangle2D> obst = new ArrayList<Rectangle2D>();
		LinkedList<CellView> viewList = new LinkedList<CellView>();
		for (CellView view : views) {
			if (view instanceof VertexView) {
				boolean leaf = true;
				for (CellView child : view.getChildViews()) {
					if (child instanceof VertexView) {
						leaf = false;
						break;
					}
				}
				if (leaf) {
					Rectangle2D rect = view.getBounds();
					obst.add(rect);
				}
			}
		}
		return obst;
	}

	/**
	 * Get rectangular bounds around start and end locations.
	 * 
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * @return the path bounds
	 */
	protected Rectangle2D getPathBounds(Point2D from, Point2D to) {
		double minx = Math.min(from.getX(), to.getX());
		double miny = Math.min(from.getY(), to.getY());
		double width = Math.max(from.getX() - minx, to.getX() - minx);
		double height = Math.max(from.getY() - miny, to.getY() - miny);
		Rectangle2D rect = new Rectangle2D.Double(minx, miny, width, height);
		return rect;
	}

	/* (non-Javadoc)
	 * @see org.jgraph.graph.DefaultEdge$LoopRouting#route(org.jgraph.graph.GraphLayoutCache, org.jgraph.graph.EdgeView)
	 */
	public List route(GraphLayoutCache cache, EdgeView edge) {
		if (!edge.isLoop()) {
			return routeEdge(cache, edge);
		} else {
			return null;
		}
	}

	/**
	 * Route edge.
	 * 
	 * @param cache
	 *            the cache
	 * @param edge
	 *            the edge
	 * @return the list
	 */
	protected List routeEdge(GraphLayoutCache cache, EdgeView edge) {
		List newPoints = new ArrayList();
		int n = edge.getPointCount();
		Dir direction = null;
		CellView target = edge.getTarget();
		CellView src = edge.getSource();
		Point2D to = edge.getPoint(n - 1), from = edge.getPoint(0);
		if ((src instanceof PortView)) {
			from = ((PortView) src).getLocation();
		}
		if ((target instanceof PortView)) {
			to = ((PortView) target).getLocation();
		}
		if ((from != null) && (to != null)) {
			if (src != null) {
				if (src instanceof PortParentView) {
					direction = Dir.East;
				} else if (src instanceof PortChildView) {
					direction = Dir.West;
				} else if (src instanceof PortOutputView) {
					direction = Dir.South;
				} else if (src instanceof PortInputView) {
					direction = Dir.North;
				} 
			} else {
				direction = Dir.South;
			}
			Vector path = computeRoute(cache, from, to, direction);
			// sanityCheck(path, from, to);
			// System.out.println(getClass().getCanonicalName()+"\t"+"ROUTE FROM "+from+" TO "+to+" "+path);
			return path;
		} else {
			return super.routeEdge(cache, edge);
		}
	}

	/**
	 * Sanity check.
	 * 
	 * @param path
	 *            the path
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * @return true, if successful
	 */
	private boolean sanityCheck(Vector<Point2D> path, Point2D from, Point2D to) {
		if (path.size() == 0) {
			System.out.println(getClass().getCanonicalName()+"\t"+"ZERO LENGTH PATH");
			return false;
		}
		Point2D p1, p2;
		if (!path.firstElement().equals(from)) {
			System.out.println(getClass().getCanonicalName()+"\t"+"FROM NOT THE SAME " + path.firstElement() + " " + from);
			return false;
		}
		if (!path.lastElement().equals(to)) {
			System.out.println(getClass().getCanonicalName()+"\t"+"TO NOT THE SAME " + path.lastElement() + " " + to);
			return false;
		}
		for (int i = 1; i < path.size(); i++) {
			p1 = path.get(i - 1);
			p2 = path.get(i);
			if ((p1.getX() != p2.getX()) && (p1.getY() != p2.getY())) {
				System.out.println(getClass().getCanonicalName()+"\t"+"NOT BENT CORRECTLY " + p1 + " " + p2 + " " + i + " " + path.size());
				return false;
			}
		}
		return true;
	}

	/**
	 * The avoidance path may have lots of unnecessary bends in it. This method
	 * removes those unnecessary bends
	 * 
	 * @param path
	 *            original path
	 * @param parity
	 *            Zero indicates inital direction is up/down and one indicates
	 *            left/right
	 */
	protected void simplifyPath(Vector<Point2D> path, int parity) {
		Point2D st, end, stNext, endNext;
		boolean reduce = false;
		Line2D.Double l1, l2;
		for (int i = 0; i < path.size() - 4; i++) {
			st = path.get(i);
			end = path.get(i + 3);
			if (i % 2 == parity) {
				stNext = new Point2D.Double(st.getX(), end.getY());
			} else {
				stNext = new Point2D.Double(end.getX(), st.getY());
			}
			l1 = new Line2D.Double(path.get(i + 1), stNext);
			l2 = new Line2D.Double(stNext, end);
			reduce = true;
			for (Rectangle2D obstacle : obstacles) {
				if (l1.intersects(obstacle) || l2.intersects(obstacle)) {
					reduce = false;
					break;
				}
			}
			if (reduce) {
				path.set(i + 1, stNext);
				path.remove(i + 2);
				path.remove(i + 2);
				i = -1;
			}
		}
	}
}