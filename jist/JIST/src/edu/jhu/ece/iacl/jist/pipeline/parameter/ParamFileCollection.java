/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * File collection stores a collection of files. The restrictions on the files
 * are set to be the same as the collection.
 * 
 * @author Blake Lucas
 */
public class ParamFileCollection extends ParamModel<List> implements ObjectCollection<File> {

	/** The file params. */
	protected Vector<ParamFile> fileParams;
	private Vector<Integer> portIndex = null;

	/** The reader writer. */
	protected FileReaderWriter readerWriter;


	/** The extension filter. */
	protected FileExtensionFilter extensionFilter;

	protected DialogType dialogType=DialogType.DIRECTORY;
	
	public boolean xmlEncodeParam(Document document, Element parent) {
		super.xmlEncodeParam(document, parent);		 
		Element em;					

		em = document.createElement("extensionFilter");	
		if(extensionFilter.xmlEncodeParam(document,em))		 
			parent.appendChild(em);

		em = document.createElement("readerWriter");
		if(readerWriter!=null)
			em.appendChild(document.createTextNode(readerWriter.getClass().getCanonicalName()));
		else 
			em.appendChild(document.createTextNode("null"));
		parent.appendChild(em);

		em = document.createElement("fileParams");
		boolean val = false;
		for(ParamFile pm : fileParams) 
		{
			Element em2 = document.createElement("file");
			if(pm.xmlEncodeParam(document, em2)) {
				em.appendChild(em2);
				val = true;
			}
		}
		if(val)
			parent.appendChild(em);
		if(portIndex!=null) {
			em = document.createElement("portIndex");
			val = false;
			for(Integer pi : portIndex) 
			{
				Element em2 = document.createElement("port");
				em2.appendChild(document.createTextNode(pi.toString()));
				em.appendChild(em2);
				val = true;

			}
			if(val)
				parent.appendChild(em);
		}
		return true;
	}

	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		Vector<Element> nv;
		String rw = (JistXMLUtil.xmlReadTag(parent, "readerWriter"));
		try {
			if(rw==null || "null".equalsIgnoreCase(rw))
				readerWriter =null;
			else
				readerWriter = (FileReaderWriter)Class.forName(rw).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		

		extensionFilter = new FileExtensionFilter();
		extensionFilter.xmlDecodeParam(document,
				JistXMLUtil.xmlReadElement(parent, "extensionFilter"));

		fileParams = new Vector<ParamFile>();
		Element eFp = JistXMLUtil.xmlReadElement(parent,"fileParams");
		if(eFp!=null) {
			nv = JistXMLUtil.xmlReadElementList(eFp,"file");;
			for(Element e : nv) {		
				String classname = JistXMLUtil.xmlReadTag(e, "classname");		
				try {
					ParamFile p = (ParamFile)Class.forName(classname).newInstance();
					p.xmlDecodeParam(document, e);
					fileParams.add(p);
				} catch (InstantiationException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				} catch (IllegalAccessException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				} catch (ClassNotFoundException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				}
			}
		}
		portIndex = new Vector<Integer>();
		nv = JistXMLUtil.xmlReadElementList(parent,"portIndex");
		for(Element e : nv) {
			String val = e.getFirstChild().getNodeValue();
			portIndex.add(Integer.valueOf(val));
		}

	}
	/**
	 * Default constructor.
	 */
	public ParamFileCollection() {
		this.setMaxIncoming(-1);
		mandatory = true;
		fileParams = new Vector<ParamFile>();
		this.factory = new ParamFileCollectionFactory(this);
	}

	/**
	 * Default constructor.
	 * 
	 * @param name
	 *            collection name
	 */
	public ParamFileCollection(String name) {
		this();
		this.setName(name);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            name
	 * @param filter
	 *            extension filter
	 */
	public ParamFileCollection(String name, FileExtensionFilter filter) {
		this();
		this.setName(name);
		this.setExtensionFilter(filter);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            name
	 * @param readerWriter
	 *            the reader writer
	 */
	public ParamFileCollection(String name, FileReaderWriter readerWriter) {
		this();
		this.setName(name);
		this.setReaderWriter(readerWriter);
	}

	/**
	 * Add file to collection.
	 * 
	 * @param val
	 *            the val
	 */
	public void add(File val) {
		this.add((Object) val);
	}
	public void set(int i,File val){
		this.set(i,(Object) val);
	}

	@Override
	public void setCollection(int index, ObjectCollection src) {
		if(src.size()<1)
			return;

		// Set the indicated index to the first element of the collection
		Object val = src.getValue(0);		

		this.set(index, val);


		if(portIndex==null)
			portIndex = new Vector<Integer>();

		int lastFixedPosition = this.getIncomingPorts().size();
		for(int i=(portIndex.size()-1);i>=lastFixedPosition;i--) {
			if(i>=portIndex.size())
				continue;
			Integer idx = portIndex.get(i);
			if(idx==index) { 
				portIndex.remove(idx);
				fileParams.remove(idx);
			}
		}

		int offset = lastFixedPosition-1;
		if(offset<portIndex.size())
			offset = portIndex.size()-1;
		for(int i=1;i<src.size();i++) {
			val = src.getValue(i);		

			if(fileParams.size()<=(offset+i))
				fileParams.setSize(offset+i+1);
			this.set(offset+i, val);
			if(portIndex.size()<=(offset+i))
				portIndex.setSize(offset+i+1);
			portIndex.set(offset+i, new Integer(index));
		}			
	}

	public ParamFile set(int i,Object value){
		while(i>=size()){
			this.add((File)null);
		}
		ParamFile param;
		if (value instanceof ParamFile) {
			fileParams.set(i,param = (ParamFile) value);
		} else {
			param = create(value);
			fileParams.set(i,param);
		}
		return param;
	}
	/**
	 * Add a new value to the collection.
	 * 
	 * @param value
	 *            the value
	 * @return the param file
	 */
	public void add(Object value) {
		ParamFile param;
		if (value instanceof ParamFile) {
			fileParams.add(param = (ParamFile) value);
		} else {
			if(fileParams.size()>0){
				param=fileParams.get(fileParams.size()-1);
				File file=param.getValue();
				//Volume is null, so we don't have to create a new parameter, we can use the last one
				if(file==null){
					if (value instanceof String) {
						param.setValue((String) value);
					} else if (value instanceof File) {
						param.setValue((File) value);
					}
				} else {
					param = create(value);
					fileParams.add(param);					
				}
			} else {
				param = create(value);
				fileParams.add(param);
			}
		}
//		return param;
	}

	/**
	 * Remove all files from collection.
	 */
	public void clear() {
		fileParams.clear();
		portIndex=null;
	}

	/**
	 * Clone object.
	 * 
	 * @return the param file collection
	 */
	public ParamFileCollection clone() {
		ParamFileCollection param = new ParamFileCollection();
		param.setName(this.getName());
		param.label=this.label;
		param.fileParams = new Vector<ParamFile>(fileParams.size());
		for (ParamFile p : fileParams) {
			param.fileParams.add(p.clone());
		}

		param.mandatory = mandatory;
		param.readerWriter = readerWriter;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Compare restriction of one file collection to another.
	 * 
	 * @param model
	 *            the model
	 * @return the int
	 */
	public int compareTo(ParamModel model) {
		return (model instanceof ParamFileCollection) ? 0 : 1;
	}

	/**
	 * Create a new ParamFile with the same restrictions as the collection and
	 * the specified value.
	 * 
	 * @param value
	 *            the value
	 * @return the param file
	 */
	protected ParamFile create(Object value) {
		ParamFile param = new ParamFile(getName());
		param.setMandatory(mandatory);
		param.setExtensionFilter(this.extensionFilter);
		if (value instanceof String) {
			param.setValue((String) value);
		} else if (value instanceof File) {
			param.setValue((File) value);
		}
		return param;
	}

	/**
	 * Get extension filter.
	 * 
	 * @return filter
	 */
	public FileExtensionFilter getExtensionFilter() {
		return this.extensionFilter;
	}

	/**
	 * Get list of parameter files.
	 * 
	 * @return the parameters
	 */
	public List<ParamFile> getParameters() {
		return fileParams;
	}

	/**
	 * Get File reader writer.
	 * 
	 * @return the reader writer
	 */
	public FileReaderWriter getReaderWriter() {
		return readerWriter;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#getValue()
	 */
	public List<File> getValue() {
		ArrayList<File> list = new ArrayList<File>();
		for (ParamFile f : fileParams) {
			if(f!=null)
				list.add(f.getValue());
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection#getValue(int)
	 */
	public File getValue(int i) {
		if (i < fileParams.size()) {
			return fileParams.get(i).getValue();
		} else {
			return null;
		}
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(-1);
		connectible = true;
		factory = new ParamFileCollectionFactory(this);
	}

	/**
	 * Returns true if port is compatible with this parameter's extension
	 * filter.
	 * 
	 * @param model
	 *            the model
	 * @return true, if checks if is compatible
	 */
	public boolean isCompatible(PipePort model) {
		if (model instanceof ParamFile) {
			if ((this.extensionFilter != null)
					&& this.extensionFilter.isCompatible(((ParamFile) model).getExtensionFilter())) {
				return true;
			}
		} else if (model instanceof ParamFileCollection) {
			if ((this.extensionFilter != null)
					&& this.extensionFilter.isCompatible(((ParamFileCollection) model).getExtensionFilter())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * File field is mandatory.
	 * 
	 * @return true, if checks if is mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * Set extension filter.
	 * 
	 * @param filter
	 *            filter
	 */
	public void setExtensionFilter(FileExtensionFilter filter) {
		this.extensionFilter = filter;
	}

	/**
	 * Set the mandatory field. The default is true.
	 * 
	 * @param mandatory
	 *            the mandatory
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * Set extension filter. This adds an extra restriction to the file
	 * parameter
	 * 
	 * @param readerWriter
	 *            the reader writer
	 */
	public void setReaderWriter(FileReaderWriter readerWriter) {
		this.readerWriter = readerWriter;
		this.extensionFilter = readerWriter.getExtensionFilter();
	}

	/**
	 * Set the file collection. This method accepts ArrayLists with any of the
	 * valid types of ParamFile
	 * 
	 * @param value
	 *            parameter value
	 */
	public void setValue(List value) {
		List list = value;
		clear();
		for (Object obj : list) {
			this.add(obj);
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection#size()
	 */
	public int size() {
		return fileParams.size();
	}

	/**
	 * Get description of volumes.
	 * 
	 * @return the string
	 */
	public String toString() {
		String text = "<HTML><OL>";
		for (ParamFile file : fileParams) {
			text += "<LI>"+file.toString() + "</LI>";
		}
		text+="</OL></HTML>";
		return text;
	}
	/**
	 * Validate that the files meet all restrictions.
	 * 
	 * @throws InvalidParameterException
	 *             parameter does not meet value restrictions
	 */
	public void validate() throws InvalidParameterException {
		if (mandatory && (fileParams.size() == 0)) {
			throw new InvalidParameterException(this);
		}
		for (ParamFile fparam : fileParams) {
			fparam.validate();
		}
	}
	public void clean(){
		for(ParamFile f:fileParams){
			f.clean();
		}
	}
	public void replacePath(File originalPath,File replacePath){
		for(ParamFile f:fileParams){
			f.replacePath(originalPath,replacePath);
		}
	}

	@Override
	public String getHumanReadableDataType() {
		return "file collection: semi-colon delimited list"; 
	}

	@Override
	public String getXMLValue() {
		StringWriter sw = new StringWriter();
		List<File> val = getValue();

		try {
			for( int i=0;i<val.size();i++){
				File f= val.get(i);
				if(i>0)
					sw.append(";");
				sw.append(f.getCanonicalPath());
			}

			return sw.toString();
		} catch (IOException e) {
			throw new InvalidParameterValueException(this,"unable to realize canonical path");
		}
	}
	@Override
	public void setXMLValue(String arg) {
		String []args = arg.trim().split("[;]+");
		setValue(Arrays.asList(args));
	}

	@Override
	public String probeDefaultValue() {
		if(fileParams==null)
			return null;
		return getXMLValue();
	}


}
