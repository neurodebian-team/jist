package edu.jhu.ece.iacl.jist.structures.image;

import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.structures.ModelImage;

public class ImageDataMipavPointer extends ImageDataMipav {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4829242269116830395L;

	public ImageDataMipavPointer(ModelImage vol) {		
		super();
		JistLogger.logOutput(JistLogger.INFO,"Pointing to a Model Image: " + vol.getImageName());
		ImageDataMipavPrivate(vol);
	}

	protected void finalize() throws Throwable {

		img = null;
		super.finalize();	        	        
		
	}
}
