package edu.jhu.ece.iacl.jist.processcontrol;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessControllerNative.
 */
public class ProcessControllerNative implements ProcessController {

	/** The Job submit time. */
	long JobSubmitTime;
	
	/** The proc. */
	Process proc=null;
	
	/** The builder. */
	ProcessBuilder builder;

	/**
	 * Instantiates a new process controller native.
	 * 
	 * @param Command the command
	 */
	public ProcessControllerNative(List<String> Command)
	{
		System.out.println(getClass().getCanonicalName()+"\t"+Command);
//		String a = null; 
//		ArrayList<String> ExecCommand = new ArrayList<String>();
//		StringTokenizer st = new StringTokenizer(Command);
//		 while (st.hasMoreTokens()) {
//			   a = st.nextToken();
//			   ExecCommand.add(a);
//			}  
		builder  = new ProcessBuilder(Command);
		
		System.out.println(getClass().getCanonicalName()+"\t"+builder.toString());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#Setup(java.util.List)
	 */
	public boolean Setup(List<String> Command) {
		return true;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getStatus()
	 */
	@Override
	public ProcessStatus getStatus() {
		// TODO Auto-generated method stub
		if(proc==null) 
			return null;
		try {
			int e = proc.exitValue();
			return ProcessStatus.DONE;
		} catch(IllegalThreadStateException e) {
			return ProcessStatus.RUNNING;
		}	
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getSubmissionTime()
	 */
	@Override
	public long getSubmissionTime()
	{
		return JobSubmitTime;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getStderrFile()
	 */
	@Override
	public InputStream getStderrFile() {
		// TODO Auto-generated method stub
		return proc.getErrorStream();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getStdoutFile()
	 */
	@Override
	public InputStream getStdoutFile() {
		// TODO Auto-generated method stub
		return proc.getInputStream();		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#submit()
	 */
	@Override
	public boolean submit() {
		// TODO Auto-generated method stub
		try {
			proc = builder.start();	
			JobSubmitTime = System.currentTimeMillis();
		} catch(IOException e) {
			proc=null;
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#destroy()
	 */
	@Override
	public boolean destroy() {		
		proc.destroy();
		return true;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#controlJob(edu.jhu.ece.iacl.jist.processcontrol.ProcessControlAction)
	 */
	@Override
	public void controlJob(ProcessControlAction Action)
	{
		switch(Action) {
		case HOLD: 
		case RELEASE:
		case RESUME:

			//not supported
			break;
		case TERMINATE:
			destroy();
			break;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getProcess()
	 */
	@Override
	public Process getProcess()
	{
		return proc;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#waitFor()
	 */
	@Override
	public int waitFor() throws InterruptedException 
	{
		System.out.println(getClass().getCanonicalName()+"\t"+"WaitFor");
		return proc.waitFor();

	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.processcontrol.ProcessController#getExitCode()
	 */
	@Override
	public Integer getExitCode() {		
		try {
			
			return proc.exitValue();
		} catch(Exception e) {
			return null;
		}		
	}

}
