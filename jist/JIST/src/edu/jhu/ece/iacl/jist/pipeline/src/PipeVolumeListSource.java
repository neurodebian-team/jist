/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through all volumes in file.
 * 
 * @author Blake Lucas
 */
public class PipeVolumeListSource extends PipeListSource {
	
	/** The vol param. */
	protected ParamVolume volParam;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("volParam");
//		volParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
		
					
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		
		volParam = (ParamVolume)outputParams.getFirstChildByName("Volume");;
//		volParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"volParam"));
		getParentPort().setParameter(volParam);
	}
	
	/**
	 * Default constructor.
	 */
	public PipeVolumeListSource() {
		super();
		getParentPort().setParameter(volParam);
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Volulme File List");
		group.setName("vollist");
		group.setCategory("Volume");
		return group;
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume");
		group.add(volParam = new ParamVolume("Volume"));
		return group;
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamVolume getOutputParam() {
		return volParam;
	}

	/**
	 * Get value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected File getValue(int i) {
		if (off < data[i].length) {
			return new File((String) data[i][off]);
		} else {
			return null;
		}
	}

	/**
	 * Set value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected void setValue(Object obj) {
		volParam.setValue((File) obj);
	}
}
