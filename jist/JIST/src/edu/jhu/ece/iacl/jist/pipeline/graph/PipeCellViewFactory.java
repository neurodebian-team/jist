/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.util.Map;

import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.Port;
import org.jgraph.graph.PortView;
import org.jgraph.graph.VertexView;

/**
 * Factory to create graph cells for different module types.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeCellViewFactory extends DefaultCellViewFactory {
	
	/** The Constant VIEW_CLASS_KEY. */
	public static final String VIEW_CLASS_KEY = "viewClassKey";
	
	/** The Constant PORT_CLASS_KEY. */
	public static final String PORT_CLASS_KEY = "portClassKey";

	/**
	 * Sets the port class.
	 * 
	 * @param map
	 *            the map
	 * @param viewClass
	 *            the view class
	 */
	public static final void setPortClass(Map map, String viewClass) {
		map.put(PORT_CLASS_KEY, viewClass);
	}

	/**
	 * Sets the view class.
	 * 
	 * @param map
	 *            the map
	 * @param viewClass
	 *            the view class
	 */
	public static final void setViewClass(Map map, String viewClass) {
		map.put(VIEW_CLASS_KEY, viewClass);
	}

	/** The graph. */
	protected PipeJGraph graph;

	/**
	 * Instantiates a new pipe cell view factory.
	 * 
	 * @param graph
	 *            the graph
	 */
	public PipeCellViewFactory(PipeJGraph graph) {
		this.graph = graph;
	}

	// Override Superclass Method to Return Custom EdgeView
	/* (non-Javadoc)
	 * @see org.jgraph.graph.DefaultCellViewFactory#createEdgeView(java.lang.Object)
	 */
	protected PipeEdgeView createEdgeView(Object cell) {
		// Return Custom EdgeView
		return new PipeEdgeView(cell);
	}

	/**
	 * Create port view.
	 * 
	 * @param p
	 *            the p
	 * @return the port view
	 */
	protected PortView createPortView(Object p) {
		try {
			String viewClass = (String) ((Port) p).getAttributes().get(PORT_CLASS_KEY);
			PortView view = (PortView) Class.forName(viewClass).newInstance();
			view.setCell(p);
			if (view instanceof PipePortView) {
				((PipePortView) view).setGraph(graph);
			}
			return view;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return super.createPortView(p);
	}

	/**
	 * Create cell view.
	 * 
	 * @param v
	 *            the v
	 * @return the vertex view
	 */
	protected VertexView createVertexView(Object v) {
		try {
			DefaultGraphCell cell = (DefaultGraphCell) v;
			String viewClass = (String) cell.getAttributes().get(VIEW_CLASS_KEY);
			VertexView view = (VertexView) Class.forName(viewClass).newInstance();
			view.setCell(v);
			return view;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return super.createVertexView(v);
	}
}
