/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.factory;

import java.io.File;

import javax.swing.ProgressMonitor;

import edu.jhu.ece.iacl.jist.pipeline.parameter.DataLabelCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDataLabel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamDataLabelInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Data Label Factory.
 * 
 * @author Blake Lucas
 */
public class ParamDataLabelFactory extends ParamVolumeFactory {
	
	/** The param. */
	private ParamDataLabel param;
	
	/** The resource data. */
	protected DataLabelCollection resourceData;
	
	/** The resource volume. */
	protected ImageData resourceVolume;

	/**
	 * Instantiates a new param data label factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamDataLabelFactory(ParamDataLabel param) {
		this.param = param;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamDataLabelInputView(param);
		}
		return inputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamOutputView(param);
		}
		return outputView;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamVolumeFactory#getParameter()
	 */
	public ParamDataLabel getParameter() {
		return param;
	}

	/**
	 * Import model image and label file location from foreign parameter.
	 * 
	 * @param foreign
	 *            foreign parameter
	 * @return parameter imported correctly
	 */
	public boolean importParameter(ParamModel foreign) {
		ParamModel param = getParameter();
		// this parameter is less than or as restrictive as model parameter
		if (param.compareTo(foreign) <= 0) {
			param.setValue(foreign.getValue());
			// Also import label file name
			((ParamDataLabel) param).setLabelFile(((ParamDataLabel) foreign).getLabelFile());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Load label file from file specified in foreign parameter.
	 * 
	 * @param foreign
	 *            foreign parameter
	 * @param monitor
	 *            the monitor
	 * @return external resources loaded correctly
	 */
	public boolean loadResources(ParamModel foreign, ProgressMonitor monitor) {
		if (foreign instanceof ParamDataLabel) {
			ParamInputView view = getInputView();
			if (view instanceof Refreshable) {
				Refresher.getInstance().remove((Refreshable) view);
			}
			param.openLabelFile(((ParamDataLabel) foreign).getLabelFile());
			view.update();
			if (view instanceof Refreshable) {
				Refresher.getInstance().add((Refreshable) view);
			}
		}
		return true;
	}

	/**
	 * Save both the Model Image and label file to specified directory The label
	 * file is saved with extension .label
	 * 
	 * @param dir
	 *            save directory
	 * @return resources saved correctly
	 */
	public boolean saveResources(File dir, boolean overRidesubDirectory) {
		super.saveResources(dir,overRidesubDirectory);
		if (resourceVolume == null) {
			resourceVolume = param.getImageData();
		}
		if (resourceData == null) {
			resourceData = param.getDataLabels();
		}
		if (resourceVolume != null) {
			File f = param.getReaderWriter().write(resourceVolume, dir);
			param.setValue(f);
		}
		File f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(getParameter().getName()) + "_label");
		if (resourceData.write(f)) {
			getParameter().setLabelFile(f);
		}
		return true;
	}
}
