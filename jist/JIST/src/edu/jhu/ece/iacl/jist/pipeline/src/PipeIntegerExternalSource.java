package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;

public class PipeIntegerExternalSource extends PipeExternalSource{
	public PipeIntegerExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamInteger("Default"));
		group.setLabel("External Integer");
		group.setName("extint");
		group.setCategory("Externalize.Number.Integer");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Integer");
		group.add(valParam = new ParamInteger("Integer"));
		return group;
	}
}
