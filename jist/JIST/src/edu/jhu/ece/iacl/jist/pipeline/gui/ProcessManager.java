/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PerformanceSummary;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.PipeScheduler;
import edu.jhu.ece.iacl.jist.pipeline.gui.resources.PlaceHolder;
import edu.jhu.ece.iacl.jist.pipeline.tree.ProcessNode;
import edu.jhu.ece.iacl.jist.utility.FileUtil;

/**
 * Process manager GUI to manage the execution of experiments.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ProcessManager extends JFrame implements Runnable, ActionListener, WindowListener,
JistPreferences.PreferenceListener {
	protected String PMTitle = "Title Not Set";
	protected boolean useWorker=true;
	protected boolean managerVisible=true;
	public boolean isUsingWorker(){
		return useWorker;
	}
	public void setShowManager(boolean vis){
		this.managerVisible=vis;
	}
	public void setUseWorker(boolean useWorker){
		this.useWorker=useWorker;
	}
	/**
	 * Worker to load layout.
	 * 
	 * @author Blake Lucas (bclucas@jhu.edu)
	 */
	protected class LoadLayout extends SwingWorker<Void, Void> {

		/* (non-Javadoc)
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		public Void doInBackground() {
			try {
				for (JMenuItem item : toggleItems) {
					item.setEnabled(false);
				}
				scheduler.init();
				for (JMenuItem item : toggleItems) {
					item.setEnabled(true);
				}
				// Do not process destinations on load! It takes too long!
				// scheduler.processDestinations();
				stopAllItem.setEnabled(true);
				pauseAllItem.setEnabled(false);
				runAllItem.setEnabled(true);
				closeItem.setEnabled(true);
				reloadItem.setEnabled(true);
				prefItem.setEnabled(true);
				saveAsItem.setEnabled(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	/** The manager. */
	protected static ProcessManager manager = null;

	/** The streams. */
	protected static PrintStream[] streams = null;

	/**
	 * Get singleton reference.
	 * 
	 * @return process manager
	 */
	public static ProcessManager getInstance() {
		if (manager == null) {
			manager = new ProcessManager();
		}
		return manager;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		//		System.out.println(getClass().getCanonicalName()+"\t"+"main: "+args);System.out.flush();
		//		System.out.println(getClass().getCanonicalName()+"\t"+"CLASSPATH "+System.getProperty("java.class.path"));
		//		System.out.flush();
		MipavController.setQuiet(true);
		MipavController.init();
		//		System.out.println(getClass().getCanonicalName()+"\t"+"Init ");System.out.flush();
		if (args.length > 1) {
			Date today;
			String output;
			SimpleDateFormat formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd-kkmmssz");
			today = new Date();
			output = formatter.format(today);
			File debugOut = new File(args[0]+".debug."+output+".out");
			File debugErr = new File(args[0]+".debug."+output+".err");
			streams = FileUtil.redirect(debugOut, debugErr);
		}
		ProcessManager frame = getInstance();
		frame.setVisible(frame.managerVisible);
		frame.addWindowListener(frame);
		//		frame.library = PipeLibrary.getInstance();
		for (String element : args) {
			File f = new File(element);
			if (f.exists() && f.isDirectory()) {
				PipeLibrary.getInstance().setLibraryPath(f);
			}
		}
		PipeLibrary.getInstance().loadPreferences();
		JistPreferences.getPreferences().addListener(frame);
		frame.historyChange(JistPreferences.getPreferences());
		for (String element : args) {
			File f = new File(element);
			if (f.exists() && !f.isDirectory()) {
				frame.open(f);
			}
		}
	}
	public void run(File f){
		run(PipeLayout.read(f),false);
	}
	public boolean runAndWait(PipeLayout layout, boolean outOfProcessOverride){
		setExitOnClose(false);
		setUseWorker(false);
		if (layout != null) {
			layout.init();
			initScheduler(layout); // Fix to NITRC Bug 3934
		}
		//		if(outOfProcessOverride)
		//			scheduler.setOverrideRunOutOfProcess(true);
		run(layout,outOfProcessOverride);
		try {
			while(!scheduler.isCompleted()){
				Thread.sleep(1000);
			}
		} catch(InterruptedException e){
			System.out.println(getClass().getCanonicalName()+"\t"+"ProcessManager runAndWait interrupted.");
			System.err.println(getClass().getCanonicalName()+e.getMessage());
		}
		return (scheduler.getWaitingCount()==0&&scheduler.getFailedCount()==0);
	}
	public void forceQuit(){
		quit(true);
	}
	public void run(PipeLayout layout, boolean outOfProcessOverride){
		if(this.layout!=null)
			close();
		this.layout=layout;
		layout.init();
		setVisible(managerVisible);
		addWindowListener(this);
		//		library = PipeLibrary.getInstance();
		PipeLibrary.getInstance().loadPreferences();
		JistPreferences.getPreferences().addListener(this);
		historyChange(JistPreferences.getPreferences());
		String title=layout.getTitle();
		if(title!=null){
			this.setTitle(PMTitle+" ["+PipeLibrary.getHostName()+" - "+ layout.getTitle() + "]");
		} else {
			this.setTitle(PMTitle+" ["+PipeLibrary.getHostName()+"]");
		}
		if (scheduler != null) {
			scheduler.dispose();
		}
		scheduler = new PipeScheduler(layout, this);

		scheduler.setOverrideRunOutOfProcess(outOfProcessOverride);
		managerPane.setScheduler(scheduler);
		for (JMenuItem item : toggleItems) {
			item.setEnabled(false);
		}
		scheduler.init();
		for (JMenuItem item : toggleItems) {
			item.setEnabled(true);
		}
		// Do not process destinations on load! It takes too long!
		// scheduler.processDestinations();
		stopAllItem.setEnabled(true);
		pauseAllItem.setEnabled(false);
		runAllItem.setEnabled(true);
		closeItem.setEnabled(true);
		reloadItem.setEnabled(true);
		prefItem.setEnabled(true);
		saveAsItem.setEnabled(true);
		scheduler.enqueue();
		if(!scheduler.isRunning())
			scheduler.start();
	}
	/** The manager pane. */
	public ProcessManagerTable managerPane;

	/** The ancestors pane. */
	public ProcessInfoPanel ancestorsPane;

	/** The descendants pane. */
	public ProcessInfoPanel descendantsPane;

	/** The scheduler. */
	PipeScheduler scheduler = null;

	public PipeScheduler getCurrentScheduler() {
		return scheduler;
	}

	/** The layout. */
	PipeLayout layout;

	/** The status bar. */
	JLabel statusBar;

	/** The updater. */
	Thread updater;

	/** The library. */
	//	public PipeLibrary library;

	/** The update running. */
	boolean updateRunning;

	/** The recent files. */
	private JMenu recentFiles = null;

	/** The recent items. */
	Vector<JMenuItem> recentItems = new Vector<JMenuItem>();

	/** The toggle items. */
	ArrayList<JMenuItem> toggleItems;

	/** The jre item. */
	JMenuItem runAllItem, destItem, stopAllItem, openItem, pauseAllItem, quitItem, prefItem, closeItem, saveAsItem,
	reloadItem, cleanAllItem, globalItem, aboutItem;

	/** The exit on close. */
	protected boolean exitOnClose = true;

	/**
	 * Default constructor.
	 */
	public ProcessManager() {
		super();
		PMTitle = "JIST Process Manager "+
		"(v"+license.JISTLicenseAbout.getReleaseID()+"-"+license.JISTLicenseAbout.getReleaseBuildDate()
		+")";		
		
		managerPane = new ProcessManagerTable();
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		splitPane.setTopComponent(managerPane);
		splitPane.setOneTouchExpandable(true);
		splitPane.setResizeWeight(0.5);
		ancestorsPane = new ProcessInfoPanel(managerPane, ProcessNode.Hierarchy.Ancestors);
		descendantsPane = new ProcessInfoPanel(managerPane, ProcessNode.Hierarchy.Descendants);
		JSplitPane infoSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		infoSplit.setLeftComponent(ancestorsPane);
		infoSplit.setRightComponent(descendantsPane);
		infoSplit.setDividerLocation(1024 / 2 - 10);
		splitPane.setBottomComponent(infoSplit);
		splitPane.setDividerLocation(200);
		splitPane.setResizeWeight(0.5);
		setPreferredSize(new Dimension(1024, 500));
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				quit();
			}
		});
		pack();
		this.setTitle(PMTitle);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(managerPane.createToolBar(), BorderLayout.NORTH);
		getContentPane().add(statusBar = new JLabel(" "), BorderLayout.SOUTH);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		setJMenuBar(createMenuBar());
		statusBar.setHorizontalAlignment(SwingConstants.RIGHT);
		statusBar.setBorder(BorderFactory.createLoweredBevelBorder());
		updater = new Thread(this);
		updater.setName("Process Status Updater");
		updater.start();
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				PlaceHolder.class.getResource("zoom.gif"))); /**SET ICON HERE**/
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == runAllItem) {
			if (scheduler != null) {
				scheduler.enqueue();
				if(!scheduler.isRunning())
					scheduler.start();
			}
			stopAllItem.setEnabled(true);
			pauseAllItem.setEnabled(true);
			cleanAllItem.setEnabled(true);
		} else if (evt.getSource() == stopAllItem) {
			if (scheduler != null) {
				scheduler.killAll();
			}
			pauseAllItem.setEnabled(false);
		} else if (evt.getSource() == pauseAllItem) {
			if (scheduler != null) {
				if(scheduler.isRunning())
					scheduler.stop();
			}
			pauseAllItem.setEnabled(false);
			runAllItem.setEnabled(true);
		} else if (evt.getSource() == quitItem) {
			quit();
		} else if (evt.getSource() == openItem) {
			open();
		} else if (evt.getSource() == closeItem) {
			if (layout != null) {
				close();
			}
		} else if (evt.getSource() == reloadItem) {
			if (layout != null) {
				File f = layout.getFileLocation();
				close();
				open(f);
			}
		} else if (evt.getSource() == saveAsItem) {
			if (layout != null) {
				layout.saveAs(false);
			}
		} else if (evt.getSource() == prefItem) {
			if (layout != null) {
				if (LayoutPreferencePanel.showDialog(this, layout)) {
					lastModifiedLock=true;
					layout.save(false);
					lastModifiedLayoutTime=layout.getFileLocation().lastModified();
					lastModifiedLock=false;
					scheduler.killAll();
				}
			}
		} else if (evt.getSource() == destItem) {
			if (scheduler != null) {
				scheduler.processDestinations();
			}
		} else if (evt.getSource() == cleanAllItem) {
			int n = JOptionPane.showOptionDialog(this, "Are you sure you want to delete ALL existing experiment folders in "+layout.getRunParameters().getOutputDirectory().getAbsolutePath()+" and ALL its sub-directories? \n"+
					"Warning: This may delete data that exists in the output directory that was not created by this JIST layout.", null,
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
			if (n == 0) {
				scheduler.cleanAll();

			}
		} else if (recentItems.contains(evt.getSource())) {
			open(new File(((JMenuItem) evt.getSource()).getText()));
		} else if (evt.getSource() == globalItem) 		{
			if (GlobalPreferencePanel.showDialog(this, JistPreferences.getPreferences())) { 
			}
		} else if(evt.getSource() == aboutItem) {
			license.JISTLicenseAbout.showDialog();
		}
	}

	/**
	 * Close process manager and clean up resources.
	 */
	public void close() {
		this.setTitle(PMTitle);
		closeItem.setEnabled(false);
		runAllItem.setEnabled(false);
		stopAllItem.setEnabled(false);
		pauseAllItem.setEnabled(false);
		prefItem.setEnabled(false);
		saveAsItem.setEnabled(false);
		reloadItem.setEnabled(false);
		cleanAllItem.setEnabled(false);
		if(scheduler!=null) 
			scheduler.killAll();
		if(!hideOnExit) {
			//			layout.dispose();
			scheduler = null;
			layout = null;
		}
		managerPane.removeScheduler();
		ancestorsPane.updateTree(null);
		descendantsPane.updateTree(null);
		managerPane.getOutFrame().setVisible(false);
	}

	/**
	 * Create menubar.
	 * 
	 * @return the j menu bar
	 */
	public JMenuBar createMenuBar() {
		toggleItems = new ArrayList<JMenuItem>();
		JMenuBar menuBar = new JMenuBar();
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuBar.add(menuFile);
		toggleItems.add(openItem = new JMenuItem("Open"));
		openItem.setMnemonic(KeyEvent.VK_O);
		openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		openItem.addActionListener(this);
		menuFile.add(openItem);
		recentFiles = new JMenu("Recent Files");
		menuFile.add(recentFiles);
		toggleItems.add(saveAsItem = new JMenuItem("Save As"));
		saveAsItem.setMnemonic(KeyEvent.VK_A);
		saveAsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		saveAsItem.addActionListener(this);
		menuFile.add(saveAsItem);
		toggleItems.add(reloadItem = new JMenuItem("Reload"));
		reloadItem.setMnemonic(KeyEvent.VK_C);
		reloadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		reloadItem.addActionListener(this);
		menuFile.add(reloadItem);
		toggleItems.add(closeItem = new JMenuItem("Close"));
		closeItem.setMnemonic(KeyEvent.VK_C);
		closeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		closeItem.addActionListener(this);
		menuFile.add(closeItem);
		toggleItems.add(prefItem = new JMenuItem("Layout Preferences"));
		prefItem.setMnemonic(KeyEvent.VK_P);
		prefItem.addActionListener(this);
		menuFile.add(prefItem);
		toggleItems.add(globalItem = new JMenuItem("Global Preferences"));
		globalItem.addActionListener(this);
		menuFile.add(globalItem);
		toggleItems.add(quitItem = new JMenuItem("Quit"));
		quitItem.setMnemonic(KeyEvent.VK_Q);
		quitItem.addActionListener(this);
		quitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		quitItem.setMnemonic(KeyEvent.VK_Q);
		menuFile.add(quitItem);
		JMenu menuAction = new JMenu("Scheduler");
		menuAction.setMnemonic(KeyEvent.VK_S);
		menuBar.add(menuAction);
		toggleItems.add(runAllItem = new JMenuItem("Start Scheduler"));
		runAllItem.addActionListener(this);
		menuAction.add(runAllItem);
		toggleItems.add(pauseAllItem = new JMenuItem("Finish Queued / Stop Enqueue"));
		pauseAllItem.addActionListener(this);
		menuAction.add(pauseAllItem);
		toggleItems.add(destItem = new JMenuItem("Process Destinations"));
		destItem.addActionListener(this);
		menuAction.add(destItem);
		toggleItems.add(stopAllItem = new JMenuItem("Stop All"));
		stopAllItem.addActionListener(this);
		menuAction.add(stopAllItem);
		toggleItems.add(cleanAllItem = new JMenuItem("Clean All"));
		cleanAllItem.addActionListener(this);
		menuAction.add(cleanAllItem);

		JMenu menuActionHelp = new JMenu("Help");
		menuActionHelp.setMnemonic(KeyEvent.VK_H);
		menuBar.add(menuActionHelp);
		toggleItems.add(aboutItem = new JMenuItem("About"));
		aboutItem.addActionListener(this);
		menuActionHelp.add(aboutItem );



		closeItem.setEnabled(false);
		runAllItem.setEnabled(false);
		stopAllItem.setEnabled(false);
		cleanAllItem.setEnabled(true);
		pauseAllItem.setEnabled(false);
		prefItem.setEnabled(false);
		saveAsItem.setEnabled(false);
		reloadItem.setEnabled(false);
		return menuBar;
	}

	/**
	 * Update menus if history changed.
	 * 
	 * @param prefs
	 *            the prefs
	 */
	public void historyChange(JistPreferences prefs) {
		Vector<File> fileHistory = prefs.getFileHistory();
		for (JMenuItem item : recentItems) {
			item.removeActionListener(this);
		}
		recentItems.clear();
		JMenuItem item;
		recentFiles.removeAll();
		int i = 1;
		for (File f : fileHistory) {
			recentItems.add(item = new JMenuItem(f.getAbsolutePath()));
			item.addActionListener(this);
			if(i<10){
				item.setAccelerator(KeyStroke.getKeyStroke('0' + i, ActionEvent.CTRL_MASK));
			}
			recentFiles.add(item);
			i++;
		}
	}

	/**
	 * Initlaize manager.
	 */
	/*
	public void init() {
		setVisible(managerVisible);
		addWindowListener(this);
		library = PipeLibrary.getInstance();
		library.loadPreferences();
		JistPreferences.getPreferences().addListener(this);
		historyChange(JistPreferences.getPreferences());
	}
	 */
	public void initScheduler(PipeLayout layout){
		this.setTitle(PMTitle+" ["+PipeLibrary.getHostName()+" - "+ layout.getTitle() + "]");
		if (scheduler != null) {
			scheduler.dispose();
		}
		lastModifiedLayoutTime=layout.getLastModifiedTime();
		scheduler = new PipeScheduler(layout, this);
		scheduler.setUseWorker(useWorker);
		managerPane.setScheduler(scheduler);
		this.layout = layout;	

	}
	/**
	 * Load layout.
	 * 
	 * @param layout
	 *            layout
	 */
	public void load(PipeLayout layout) {
		if (layout == null) {
			return;
		}
		File outputDir=layout.getRunParameters().getOutputDirectory();
		if(!outputDir.exists()){
			int n = JOptionPane.showOptionDialog(this, "The output directory "+outputDir.getAbsolutePath()+" could not be found. Would you like to change it now?", null,
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
			if(n==0){
				if (LayoutPreferencePanel.showDialog(this, layout)) {
					lastModifiedLock=true;
					layout.save(false);
					lastModifiedLayoutTime=layout.getFileLocation().lastModified();
					lastModifiedLock=false;
				}
			}
		}
		initScheduler(layout);		
		LoadLayout loader = new LoadLayout();
		if(useWorker){
			loader.execute();
		} else {
			loader.doInBackground();
		}
	}

	/**
	 * Open layout from file.
	 */
	public void open() {
		PipeLayout layout = PipeLayout.open(this);
		if (layout != null) {
			layout.init();
		}
		load(layout);
	}
	/**
	 * Open specified layout file.
	 * 
	 * @param f
	 *            file
	 */
	public void open(File f) {
		PipeLayout layout=PipeLayout.read(f);
		if (layout != null) {
			layout.init();
			load(layout);
		}
	}
	public boolean quit(){
		return quit(false);
	}
	/**
	 * Quit process manager.
	 * 
	 * @return true, if quit
	 */
	public boolean quit(boolean forceQuit) {

		int n = (!forceQuit)?JOptionPane.showOptionDialog(this, "Are you sure that you want to close the JIST Process Manager?", null, JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, null, null):0;
		if(hideOnExit) {
			this.setVisible(false);
			// Let the updater live - we can shut down at a later time if it eats too much cpu
			//			updateRunning = false;
			//			try {
			//				//				updater.join();
			//				//				updater.join(100);
			//				if(updater.isAlive()) {
			//					updater.interrupt();
			//					updater.join(200);
			//					if(updater.isAlive()) {
			//						System.err.println(getClass().getCanonicalName()+"ProcessManager join failed.");				
			//						System.err.flush();
			//					}
			//				}
			//			} catch (InterruptedException e) {
			//				// TODO Auto-generated catch block
			//				System.err.println(getClass().getCanonicalName()+"ProcessManager join interrupted.");
			//				//				System.err.println(getClass().getCanonicalName()+e.getMessage());
			//				System.err.flush();
			//			}
			if (scheduler != null) {
				scheduler.killAll();
			}

			if (streams != null) {
				for (PrintStream stream : streams) {
					stream.flush();
					stream.close();
				}
			}
			if(!forceQuit)
				JistPreferences.getPreferences().write(new File(PipeLibrary.getInstance().getLibraryPath(), 
						JistPreferences.getDefaultPreferencesFileName()));
			close();
			return false;
		}
		if (n == 0) {
			managerPane.getOutFrame().dispose();
			this.setVisible(false);
			updateRunning = false;
			try {
				//				updater.join();
				//				updater.join(100);
				if(updater.isAlive()) {
					updater.interrupt();
					updater.join(200);
					if(updater.isAlive()) {
						System.err.println(getClass().getCanonicalName()+"ProcessManager join failed.");				
						System.err.flush();
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.err.println(getClass().getCanonicalName()+"ProcessManager join interrupted.");
				//				System.err.println(getClass().getCanonicalName()+e.getMessage());
				System.err.flush();
			}
			if (scheduler != null) {
				scheduler.killAll();
			}
			this.dispose();
			if (streams != null) {
				for (PrintStream stream : streams) {
					if(stream!=null) {
						stream.flush();
						stream.close();
					}
				}
			}
			if(!forceQuit)
				JistPreferences.getPreferences().write(new File(PipeLibrary.getInstance().getLibraryPath(), 
						JistPreferences.getDefaultPreferencesFileName()));
			return true;
		}
		return false;
	}
	protected long lastModifiedLayoutTime;
	protected boolean lastModifiedLock;
	/**
	 * Main process manager thread.
	 */
	public void run() {
		updateRunning = true;
		while (updateRunning) {
			if (scheduler != null) {
				if (scheduler.isRunning()) {
					if (runAllItem.isEnabled()) {
						runAllItem.setEnabled(false);
					}
					if (!pauseAllItem.isEnabled()) {
						pauseAllItem.setEnabled(true);
					}
				} else {
					if (!runAllItem.isEnabled()) {
						runAllItem.setEnabled(true);
					}
					if (pauseAllItem.isEnabled()) {
						pauseAllItem.setEnabled(false);
					}
				}

				if ((scheduler != null) && (scheduler.hasRunningQueue()/*getRunningQueue() != null*/)) {
					LinkedList<ExecutionContext> rows =scheduler.getRunningQueue();
					for(int i=0;i<rows.size();i++) {
						managerPane.getTableModel().updateRow(rows.get(i));
					}
				}

				updateStatusBar();
				long lastModified=(layout.getFileLocation()!=null)?layout.getFileLocation().lastModified():0;
				if((!lastModifiedLock&&lastModified>lastModifiedLayoutTime) && this.managerVisible){
					int n = JOptionPane.showOptionDialog(this, "The current layout has been modified. Would you like to reload it?", null, JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE, null, null, null);
					if(n==0){
						File f = layout.getFileLocation();
						close();
						open(f);
					} else {
						lastModifiedLayoutTime=lastModified;
					}
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				updateRunning = false;
				System.err.println(getClass().getCanonicalName()+"ProcessManager stopping via interrupt.");
				System.err.flush();
				//				e.printStackTrace();
			}
		}
	}

	/**
	 * Set true if process manager should exit when closed.
	 * 
	 * @param exitOnClose
	 *            true if exit on close
	 */
	public void setExitOnClose(boolean exitOnClose) {
		this.exitOnClose = exitOnClose;
	}

	/**
	 * Update status bar.
	 */
	public void updateStatusBar() {

		if (scheduler.isInitializing()) {
			statusBar.setText("Initializing " + scheduler.getInitializedCount() + " experiments");
		} else {
			int totalCompleted = scheduler.getCompletedCount();
			int failed = scheduler.getFailedCount();
			int waiting = scheduler.getWaitingCount();
			int queued = scheduler.getQueuedCount();
			int running = scheduler.getRunningCount();
			boolean isRunning = scheduler.isRunning();
			int total = totalCompleted + waiting + running;
			// int
			// percent=(int)Math.round(100*totalCompleted/(float)(totalCompleted+waiting+running));
			statusBar.setText("Scheduler " + ((isRunning) ? "Running" : "Stopped") + ": Succeeded ("
					+ (totalCompleted - failed) + ") Failed (" + failed + ") Running (" + running + ") Queued ("
					+ queued + ") Total (" + total + ")  Elapsed Time (Actual: "+PerformanceSummary.formatTime(scheduler.getElapsedActualTime())+" / CPU: "+PerformanceSummary.formatTime(scheduler.getElapsedCpuTime())+")");
		}

		if (scheduler != null) {
			scheduler.updateManagerTable();
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	public void windowClosed(WindowEvent arg0) {
		if (exitOnClose) {
			System.exit(0);
		} else {
			this.setVisible(false);
			this.dispose();
		}
	}

	/**
	 * Create toolbar to manipulate graph.
	 * 
	 * @param arg0
	 *            the arg0
	 */
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	/** Disable system.exit on exit - causes close window and and exit to be the same **/ 
	private boolean hideOnExit = false;

	public void setCloseOnlyOnExit(boolean flag) { hideOnExit = flag;}
	public void cleanAllQuietly(PipeLayout layout) {
		// TODO Auto-generated method stub
		scheduler = new PipeScheduler(layout, this);
		scheduler.cleanAll(false);
	}
	public void processDestinations() {
		// TODO Auto-generated method stub
		scheduler.processDestinations();

	};
}