/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.util.List;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMultiOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Source to select multiple options.
 * 
 * @author Blake Lucas
 */
public class PipeSelectionSource extends PipeSource {
	
	/** The multi option. */
	protected ParamMultiOption multiOption;
	
	/** The option. */
	protected ParamOption option;
	
	/** The selection. */
	transient protected List<String> selection;
	
	/** The index. */
	transient protected int index;


	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("option");
//		option.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("multiOption");
//		multiOption.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		option = (ParamOption)outputParams.getFirstChildByName("Selection");
		multiOption = (ParamMultiOption)outputParams.getFirstChildByName("Multi-Selection");
		
//		option = new ParamOption();
//		option.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"option"));
//		multiOption = new ParamMultiOption();
//		multiOption.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"multiOption"));
		getParentPort().setParameter(option);
	}
	
	/**
	 * Instantiates a new pipe selection source.
	 */
	public PipeSelectionSource() {
		super();
		getParentPort().setParameter(option);
	}

	/**
	 * Update selection options when a new connector is added.
	 * 
	 * @param port
	 *            the port
	 */
	public void connectAction(PipePort port) {
		super.connectAction(port);
		if (port == option) {
			Vector<PipePort> ports = port.getOutgoingPorts();
			for (PipePort child : ports) {
				if (child instanceof ParamOption) {
					List<String> options = ((ParamOption) child).getOptions();
					for (String opt : options) {
						option.add(opt);
						multiOption.add(opt);
					}
				}
			}
		}
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Multi-Selection");
		group.setName("multiselect");
		group.setCategory("Selection");
		group.add(multiOption = new ParamMultiOption("Multi-Selection"));
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection("Multi-Selection");
		group.add(option = new ParamOption("Selection", new String[] {}));
		return group;
	}

	/**
	 * Update selection options if connector is removed.
	 * 
	 * @param owner
	 *            the owner
	 * @param child
	 *            the child
	 * @param wire
	 *            the wire
	 */
	public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire) {
		super.disconnectAction(owner, child, wire);
		if (owner == option) {
			List<String> select = multiOption.getValue();
			option.setOptions(null);
			multiOption.setOptions(null);
			connectAction(owner);
			multiOption.setValue(select);
			multiOption.getInputView().update();
		}
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamOption getOutputParam() {
		return option;
	}

	/**
	 * Returns true if iterator has more elements.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || (index < selection.size()));
	}

	/**
	 * Iterate.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (index < selection.size()) {
					option.setValue(selection.get(index++));
					push();
				} else {
					//
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			// System.out.println(getClass().getCanonicalName()+"\t"+"CURRENT SELECTION 2 "+selection);
			reset();
			isReset = true;
			return false;
		}
	}

	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		selection = multiOption.getValue();
		index = 0;
		if (selection.size() > 0) {
			option.setValue(selection.get(0));
		}
		index++;
		push();
	}
}
