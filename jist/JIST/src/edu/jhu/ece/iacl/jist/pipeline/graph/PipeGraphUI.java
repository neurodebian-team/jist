/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import org.jgraph.plaf.basic.BasicGraphUI;

/**
 * The Class PipeGraphUI.
 */
public class PipeGraphUI extends BasicGraphUI {
	
	/**
	 * Instantiates a new pipe graph ui.
	 */
	public PipeGraphUI() {
		super();
	}
	/*
	 * public CellHandle createHandle(GraphContext context) { if (context !=
	 * null && !context.isEmpty() && graph.isEnabled()) { try { return new
	 * PipeRootHandle(context); } catch (NullPointerException e) { // ignore for
	 * now... } } return null; } public void paint(Graphics g, JComponent c) {
	 * super.paint(g, c); } protected void drawGraph(Graphics g, Rectangle2D
	 * clipBounds) { super.drawGraph(g, clipBounds); } int index=0; protected
	 * void paintCells(Graphics g, Rectangle2D realClipBounds) {
	 * 
	 * super.paintCells(g, realClipBounds); } public class PipeRootHandle
	 * extends RootHandle{ public PipeRootHandle(GraphContext ctx) { super(ctx); }
	 * public void paint(Graphics g) { super.paint(g); } }
	 */
}
