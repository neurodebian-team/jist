package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.jist.structures.fiber.DTIStudioHDR;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.image.USbyte;

// TODO: Auto-generated Javadoc
/**
 * The Class FiberCollectionReaderWriter.
 */
public class FiberCollectionReaderWriter extends FileReaderWriter<FiberCollection>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final FiberCollectionReaderWriter readerWriter=new FiberCollectionReaderWriter();
	
	/**
	 * Gets the single instance of FiberCollectionReaderWriter.
	 * 
	 * @return single instance of FiberCollectionReaderWriter
	 */
	public static FiberCollectionReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new fiber collection reader writer.
	 */
	public FiberCollectionReaderWriter(){
		super(new FileExtensionFilter(new String[]{"dat"}));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	@Override
	protected FiberCollection readObject(File file) {
		FiberCollection fbrcollection=new FiberCollection();
		String filename=file.getAbsolutePath();
	        LEFileReader fp;
			try {
				fp = new LEFileReader(filename);
		        DTIStudioHDR hdr = new DTIStudioHDR();
		        hdr.read(fp);	        
		        for(int j=0;j<hdr.FiberNR;j++) {
		            Fiber f = new Fiber();
		            f.read(fp,hdr.versionNum);
		            fbrcollection.add(f);
		        }
		        fbrcollection.setDimensions(new Point3i(hdr.ImageSize[0],hdr.ImageSize[1],hdr.ImageSize[2]));
		        fbrcollection.setResolutions(new Point3f(hdr.VoxelSize[0],hdr.VoxelSize[1],hdr.VoxelSize[2]));
		        fbrcollection.setName(FileReaderWriter.getFileName(file));
		        return fbrcollection;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch(IOException e){
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;				
			}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(FiberCollection fbr,File dir){
		String name = fbr.getName();
		File f = new File(dir, name + ".dat");
		if((f=writeObject(fbr,f))!=null){
			return f;
		} else {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(FiberCollection allFibers, File file) {
		String filename=file.getAbsolutePath();
		Point3i dimensions=allFibers.getDimensions();
		Point3f resolutions=allFibers.getResolutions();
			try {
	    		LEFileWriter bos = new LEFileWriter(filename);
	    		
	    		//WRITE HEADER
	    		//DTIStudioHDR hdr = new DTIStudioHDR(); 
	        	bos.writeBytes("FiberDat"); 
	        	
	        	bos.writeInt(allFibers.size());	//Number of Fibers
	        	
	        	float[] stats = getStats(allFibers);
	        	
	        	bos.writeInt((int)stats[1]);		//FLM
	        	bos.writeFloat(stats[2]);	//FLMn

	        	//bos.writeFloat(0);
	        	
	        	bos.writeInt(dimensions.x);
	        	bos.writeInt(dimensions.y);
	        	bos.writeInt(dimensions.z);
	        	
	        	bos.writeFloat(resolutions.x);
	        	bos.writeFloat(resolutions.y);
	        	bos.writeFloat(resolutions.z);
	        	
	        	bos.writeInt(1);
	        	bos.writeInt(0);
	        	
	        	bos.seek(128);
	        	
	        	
	        	//WRITE FIBERS
	        	USbyte usb = new USbyte();
	    		for(int i=0;i<allFibers.size();i++){
	    			
	    			Fiber f = allFibers.get(i);		//GET THE iTH FIBER
	    			XYZ fbrs[] = f.getXYZChain();
	    			
	        		bos.writeInt(fbrs.length);	//fiber length
	        		bos.writeByte(0);			//cReserved
	        		
	        		//HOW TO COME UP WITH COLORS?
	        		//everything red
	        		bos.writeByte(usb.unsigned2signed(255));			//Color - Red
	        		bos.writeByte(usb.unsigned2signed(0));				//Color - Green
	        		bos.writeByte(usb.unsigned2signed(0));				//Color - Blue
	        		
	        		bos.writeInt(0);			//nSelectFiberStartPoint
	        		bos.writeInt(fbrs.length);	//nSelec
	        		
	        		//WRITE EVERY POINT IN THE FIBER 
	        		for (int j=0;j<fbrs.length;j++){
	        			XYZ pt = fbrs[j];
	        			bos.writeFloat(pt.x);
	        			bos.writeFloat(pt.y);
	        			bos.writeFloat(pt.z);
	        		}
	        	}
	        	bos.close();
	        	
	        	return file;
	    	}catch (IOException e){
	    		System.out.println("jist.io"+"\t"+"Error Writing File");
	    		e.printStackTrace();
	    	}
	    	return null;
	}
	
	/**
	 * Gets the stats.
	 * 
	 * @param allFibers the all fibers
	 * 
	 * @return the stats
	 */
	private static float[] getStats(Vector<Fiber> allFibers){
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int mean = 0;
	
		for(int i=0; i<allFibers.size(); i++){
	
			int n = allFibers.get(i).getXYZChain().length;
			if(n<min){
				min=n;
			}else if(n>max){
				max = n;
			}
			mean = mean+n;
		}
		if(allFibers.size()>0){
			mean = mean/allFibers.size();
		}
		float[] stats = {min, max, mean};
		return stats;
	}
}
