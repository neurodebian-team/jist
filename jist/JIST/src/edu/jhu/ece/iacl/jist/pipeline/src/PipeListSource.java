/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Iterate through list of parameters.
 * 
 * @author Blake Lucas
 */
public abstract class PipeListSource extends PipeSource {
	
	/** The file param. */
	protected ParamFile fileParam;
	
	/** The offset. */
	protected ParamInteger offset;
	
	/** The start entry. */
	protected ParamInteger startEntry;
	
	/** The end entry. */
	protected ParamInteger endEntry;
	
	/** The data. */
	transient protected Object[][] data;
	
	/** The index. */
	transient protected int index;
	
	/** The off. */
	transient int st, end, off;

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("fileParam");
//		fileParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("offset");
//		offset.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		em = document.createElement("startEntry");
//		startEntry.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		em = document.createElement("endEntry");
//		endEntry.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		fileParam = (ParamFile) inputParams.getFirstChildByName("List File");
		startEntry = (ParamInteger) inputParams.getFirstChildByName("Start Entry");
		endEntry = (ParamInteger) inputParams.getFirstChildByName("End Entry");
		offset = (ParamInteger) inputParams.getFirstChildByName("Offset Index");
//
//		fileParam = new ParamFile();
//		fileParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileParam"));
//		offset = new ParamInteger();
//		offset.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"offset"));
//		startEntry = new ParamInteger();
//		startEntry.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"startEntry"));
//		endEntry = new ParamInteger();
//		endEntry.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"endEntry"));	
	}
	/**
	 * Default constructor.
	 */
	public PipeListSource() {
		super();
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("List");
		group.setName("list");
		group.add(fileParam = new ParamFile("List File"));
		fileParam.setExtensionFilter(ArrayObjectTxtReaderWriter.getInstance().getExtensionFilter());
		group.add(offset = new ParamInteger("Offset Index", 0, 1000000, 0));
		group.add(startEntry = new ParamInteger("Start Entry", 0, 100000, 0));
		group.add(endEntry = new ParamInteger("End Entry", -1, 100000, -1));
		return group;
	}

	/**
	 * Get list value.
	 * 
	 * @param i
	 *            the i
	 * @return the value
	 */
	protected abstract Object getValue(int i);

	/**
	 * Returns true if iterator has more values.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return (super.hasNext() || (((end == -1) || (index < end)) && (index < data.length)));
	}

	/**
	 * Iterate.
	 * 
	 * @return true, if iterate
	 */
	public boolean iterate() {
		if (hasNext()) {
			if (!super.iterate()) {
				if (index < data.length) {
					setValue(getValue(index++));
					push();
				} else {
					reset();
					isReset = true;
					return false;
				}
			}
			return true;
		} else {
			reset();
			isReset = true;
			return false;
		}
	}

	/**
	 * Reset iterator.
	 */
	public void reset() {
		super.reset();
		data = ArrayObjectTxtReaderWriter.getInstance().read(fileParam.getValue());
		if(data == null) {
			JistLogger.logError(JistLogger.INFO, "Cannot Read: "+fileParam.getValue());
			data = new Object[0][0];
		} 
		index = 0;
		st = startEntry.getInt();
		end = endEntry.getInt();
		off = offset.getInt();
		index = st;
		Object obj;
		if (((end == -1) || (index < end)) && (index < data.length)) {
			setValue(obj = getValue(index++));
		} else {
			index++;
		}
		push();
	}

	/**
	 * Set list value.
	 * 
	 * @param obj
	 *            the obj
	 */
	protected abstract void setValue(Object obj);
}
