package edu.jhu.ece.iacl.jist.structures.image;
// TODO: Auto-generated Javadoc

/**
 * Volume kernel interface.
 * 
 * @author Blake Lucas
 */
public interface ConvolutionVolumeKernel {
	
	/**
	 * Gets the volume.
	 * 
	 * @return the volume
	 */
	public double[][][] getVolume();

}
