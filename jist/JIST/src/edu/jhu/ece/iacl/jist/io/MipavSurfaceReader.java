package edu.jhu.ece.iacl.jist.io;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.geom.NormalGenerator;

// TODO: Auto-generated Javadoc
/**
 * The Class MipavSurfaceReader.
 */
public class MipavSurfaceReader {
	
	/** The vertices open tag. */
	static int verticesOpenTag;
	
	/** The vertices close tag. */
	static int verticesCloseTag;
	
	/**
	 * Read surface.
	 * 
	 * @param f the f
	 * 
	 * @return the embedded surface
	 */
	public static EmbeddedSurface readSurface(File f){
		ArrayList<String> str = new ArrayList<String>();
		BufferedReader in;
		try {		
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));
			
			String line;
			// Read file and store the string in a array 
			while ((line = in.readLine()) != null) {
				StringTokenizer tokenizer = new StringTokenizer(line, " ,\t,\n,\r,<,>");
				while(tokenizer.hasMoreTokens()){
					String token = tokenizer.nextToken();
					str.add(token);
				}
				
			}
		} catch (Exception e) {
			System.err.println("jist.base"+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		
		// determine the start and end index of the vertices data
		for (int index = 0; index < str.size(); index++){
			String verticesData = str.get(index);
			if (verticesData.equals("Vertices")){
				verticesOpenTag = index;
			}
			else if (verticesData.equals("/Vertices")){
				verticesCloseTag = index;
			}else return null;
		}
		int verticesStartIndex = (int)verticesOpenTag +1;
		int verticesEndIndex = (int)verticesCloseTag -1;

		//create an array and populate it with vertex coordinates
		ArrayList<String> vertexArray = new ArrayList<String>();
		for (int i = verticesStartIndex; i <= verticesEndIndex; i++ ){
			vertexArray.add(str.get(i));
		}
		
		//format the vertices
		Point3f[] points = new Point3f[((int)(vertexArray.size()))/3];
		Point3f p = new Point3f();
		for (int i = 0; i < vertexArray.size(); i+=3){
			try{
				p.x = Float.parseFloat(vertexArray.get(i));
				p.y = Float.parseFloat(vertexArray.get(i+1));
				p.z = Float.parseFloat(vertexArray.get(i+2));
				points[i/3] = p;
			}catch(NumberFormatException e){
				System.err.println("jist.base"+"CANNOT FORMAT VERTS");
				return null;
			}
		}
		
		// format the indices
		int[] indices = new int[((int)(vertexArray.size()))*3];
		for (int i = 0; i < vertexArray.size(); i+=3){
			try{
				indices[i] = Integer.parseInt(vertexArray.get(i+2));
				indices[i+1]= Integer.parseInt(vertexArray.get(i+1));
				indices[i+2] = Integer.parseInt(vertexArray.get(i));
			}catch(NumberFormatException e){
				System.err.println("jist.base"+"CANNOT FORMAT INDICES");
				return null;
			}
		}

			
		EmbeddedSurface surf=new EmbeddedSurface(points,NormalGenerator.generate(points, indices),indices);
		surf.setName(FileReaderWriter.getFileName(f));

		return surf;
	}
}
	
// SAMPLE MIPAV XML SURFACE FILE
/*
 <?xml version="1.0" encoding="UTF-8"?>
<!-- MIPAV header file -->
<Surface xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<Unique-ID>22</Unique-ID>
	<Material>
		<Ambient>0.0 0.0 0.5</Ambient>
		<Diffuse>0.0 0.0 0.5</Diffuse>
		<Emissive>0.0 0.0 0.0</Emissive>
		<Specular>0.0 0.0 0.0</Specular>
		<Shininess>64.0</Shininess>
	</Material>
	<Type>TMesh</Type>
	<Opacity>0.0</Opacity>
	<LevelDetail>100</LevelDetail>
	<Mesh>
		<Vertices>...-0.011764706 0.29411766 0.0031324679 -0.011764706 0.2980313 -7.8125E-4...</Vertices>	
		<Normals>..0.36700046 0.3670057 0.85476166 -0.29240003 ..</Normals>  
		<Colors> ...0.0 0.0 0.5 1.0 0.0 0.0 0.5 1.0 0.0 0.0 0.5 1.0 0.0 0.0 0.5 1.0 0.0...</Colors>   
		<Connectivity>...300 7366 2014 2085 2015 8279 8347 8346 5606 838172...</Connectivity> (int)
	</Mesh>
</Surface>
 */
		

