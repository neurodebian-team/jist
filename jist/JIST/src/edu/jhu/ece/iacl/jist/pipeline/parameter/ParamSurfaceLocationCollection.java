package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.util.ArrayList;
import java.util.Vector;

import edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFileCollectionFactory;

public class ParamSurfaceLocationCollection extends ParamFileCollection{
	public ParamSurfaceLocationCollection() {
		this.setMaxIncoming(-1);
		mandatory = true;
		this.setReaderWriter(SurfaceReaderWriter.getInstance());
		fileParams = new Vector<ParamFile>();
		this.factory = new ParamFileCollectionFactory(this);
	}
	public ParamSurfaceLocationCollection(String name) {
		this();
		setName(name);
	}
}
