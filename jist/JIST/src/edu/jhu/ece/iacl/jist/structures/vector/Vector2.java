package edu.jhu.ece.iacl.jist.structures.vector;
// TODO: Auto-generated Javadoc

/**
 * The Class Vector2.
 */
public class Vector2 implements VectorX{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The y. */
	public Number x,y;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#size()
	 */
	public int size(){return 2;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#get(int)
	 */
	public Number get(int i){
		switch(i){
			case 0:return x;
			case 1:return y;
			default:return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getX()
	 */
	public Number getX(){return x;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getY()
	 */
	public Number getY(){return y;}
	
	/**
	 * Instantiates a new vector2.
	 * 
	 * @param m the m
	 */
	public Vector2(Number m[]){
		x=m[0];
		y=m[1];
	}
	
	/**
	 * Instantiates a new vector2.
	 * 
	 * @param x the x
	 * @param y the y
	 */
	public Vector2(Number x,Number y){
		this.x=x;
		this.y=y;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setX(java.lang.Number)
	 */
	public void setX(Number x){this.x=x;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setY(java.lang.Number)
	 */
	public void setY(Number y){this.y=y;}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setZ(java.lang.Number)
	 */
	public void setZ(Number z){}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#setW(java.lang.Number)
	 */
	public void setW(Number w){}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#toArray()
	 */
	public Number[] toArray() {
		return new Number[]{x,y};
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(double)
	 */
	public Vector2 add(double a) {
		return new Vector2(getX().doubleValue()+a,getY().doubleValue()+a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(double)
	 */
	public Vector2 sub(double a) {
		return new Vector2(getX().doubleValue()-a,getY().doubleValue()-a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(double)
	 */
	public Vector2 mul(double a) {
		return new Vector2(getX().doubleValue()*a,getY().doubleValue()*a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(double)
	 */
	public Vector2 div(double a) {
		return new Vector2(getX().doubleValue()/a,getY().doubleValue()/a);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#add(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector2 add(VectorX v) {
		return new Vector2(getX().doubleValue()+v.getX().doubleValue(),getY().doubleValue()+v.getY().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#sub(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector2 sub(VectorX v) {
		return new Vector2(getX().doubleValue()-v.getX().doubleValue(),getY().doubleValue()-v.getY().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mul(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector2 mul(VectorX v) {
		return new Vector2(getX().doubleValue()*v.getX().doubleValue(),getY().doubleValue()*v.getY().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#div(edu.jhu.ece.iacl.jist.structures.vector.VectorX)
	 */
	public Vector2 div(VectorX v) {
		return new Vector2(getX().doubleValue()/v.getX().doubleValue(),getY().doubleValue()/v.getY().doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getZ()
	 */
	public Number getZ() {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#getW()
	 */
	public Number getW() {
		return null;
	}
	
	/**
	 * Sets the.
	 * 
	 * @param v the v
	 */
	public void set(Vector2 v){
		x=(Number)v.x;
		y=(Number)v.y;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Vector2 clone(){
		return new Vector2(x,y);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#mag()
	 */
	public Number mag() {
		return Math.sqrt(x.doubleValue()*x.doubleValue()+y.doubleValue()*y.doubleValue());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return ("["+x+","+y+"]");
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#set(java.lang.Number, int)
	 */
	public void set(Number value, int i) {
		switch(i){
			case 0:x=value;
			case 1:y=value;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.structures.vector.VectorX#normalize()
	 */
	public Vector2 normalize(){
		double mag=mag().doubleValue();
		if(mag>0)return div(mag); else return this;	
	}
}
