package edu.jhu.ece.iacl.jist.applicationplugins;

import edu.jhu.ece.iacl.jist.applicationplugins.JISTPluginContext.ContextType;

public interface JISTApplicationPluginInterface {
	public boolean init(); // return false if the plugin should NOT be displayed, called ONCE per session

	public void execute(ContextType calledAs, Object contextData); // run in the current process thread. Context data is a type corresponding to the start method either PipeLayout, A PipeModule, etc. Allow the system to respond to where the user specifies something

	public JISTPluginContext[] getValidContexts(); // where is the method shown and how

	public void forceQuit();  // force any open threads to quit
}
