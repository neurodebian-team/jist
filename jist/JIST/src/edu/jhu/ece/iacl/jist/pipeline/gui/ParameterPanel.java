/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.gui;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithmGroup;
import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamCollectionPipelineInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;

/**
 * Panel to display input parameters.
 * 
 * @author Blake Lucas
 */
public class ParameterPanel extends JTabbedPane implements PipeModule.PipeListener, ChangeListener {
	
	/** The panel. */
	private static ParameterPanel panel = null;

	/**
	 * Get singleton reference to parameter panel.
	 * 
	 * @return parameter panel
	 */
	public static ParameterPanel getInstance() {
		if (panel == null) {
			panel = new ParameterPanel();
		}
		return panel;
	}

	/** The ignore refresh. */
	protected boolean ignoreRefresh = false;
	
	/** The last select. */
	private PipeModule lastSelect = null;
	
	/** The pipes. */
	private Vector<PipeModule> pipes = null;

	/**
	 * Default constructor.
	 */
	protected ParameterPanel() {
		super(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
		pipes = new Vector<PipeModule>();
	}

	/**
	 * Update input views when connection occurs.
	 * 
	 * @param owner
	 *            the owner
	 */
	public void connectAction(PipePort owner) {
		if (!ignoreRefresh) {
			owner.getOwner().getInputParams().getInputView().update();
		}
	}

	/**
	 * Update input view when disconnection occurs.
	 * 
	 * @param owner
	 *            the owner
	 * @param child
	 *            the child
	 * @param wire
	 *            the wire
	 */
	public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire) {
		if (!ignoreRefresh) {
			child.getOwner().getInputParams().getInputView().update();
		}
	}

	/**
	 * Returns true if panel is currently ignoring refreshing.
	 * 
	 * @return true if refresh ignored
	 */
	public boolean isIgnoreRefresh() {
		return ignoreRefresh;
	}

	/**
	 * Refresh view of input parameters.
	 */
	public /*synchronized*/ void refreshView() {
		if (isIgnoreRefresh()) {
			return;
		}
		PipeInternalFrame frame = LayoutPanel.getInstance().getActiveFrame();
		int selection = this.getSelectedIndex();
		pipes.clear();
		ParamInputView view;
		if ((frame != null) && isEnabled()) {
			pipes.addAll(frame.getPipelineLayout().getPipes());
			for (PipeModule pipe : pipes) {
				pipe.getInputParams().setInputView(
						view=new ParamCollectionPipelineInputView(pipe.getInputParams(), (pipe instanceof PipeAlgorithm)));
			}
		}
		this.removeAll();
		this.removeChangeListener(this);
		for (PipeModule pipe : pipes) {
			JPanel vpanel = new JPanel();
			vpanel.setLayout(new BoxLayout(vpanel, BoxLayout.Y_AXIS));
			vpanel.add(pipe.getInputParams().getInputView());
			JPanel panel = new JPanel(new BorderLayout());
			panel.add(vpanel, BorderLayout.NORTH);
			JScrollPane scrollPane = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.add(pipe.getLabel(), scrollPane);
		}
		if ((selection >= 0) && (selection < this.getTabCount())) {
			this.setSelectedIndex(selection);
		} else {
			if (pipes.size() > 0) {
				lastSelect = pipes.firstElement();
			}
		}
		this.addChangeListener(this);
	}

	/**
	 * Focus panel on module properties.
	 * 
	 * @param pipe
	 *            module
	 */
	public void select(PipeModule pipe) {
		lastSelect = pipe;
		int i = 0;
		for (PipeModule p : pipes) {
			if (p == pipe) {		
				if(this.getTabCount()<=i)
					System.err.println(getClass().getCanonicalName()+"Cannot select index "+i+" of "+getTabCount()+" object:"+pipe);
				else
					this.setSelectedIndex(i);
				break;
			} else if (p instanceof PipeAlgorithmGroup) {
				if (((PipeAlgorithmGroup) p).getGroupModules().contains(pipe)) {
					if(this.getTabCount()<=i)
						System.err.println(getClass().getCanonicalName()+"Cannot select index "+i+" of "+getTabCount()+" object:"+pipe);
					else
						this.setSelectedIndex(i);
					break;
				}
			}
			i++;
		}
	}

	/**
	 * Enable parameter panel.
	 * 
	 * @param enabled
	 *            the enabled
	 */
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		refreshView();
	}

	/**
	 * Set true if panel should ignore refreshes.
	 * 
	 * @param ignoreRefresh
	 *            true if regresh ignored
	 */
	public void setIgnoreRefresh(boolean ignoreRefresh) {
		this.ignoreRefresh = ignoreRefresh;
	}

	/**
	 * Remember last panel selection.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void stateChanged(ChangeEvent evt) {
		int index = this.getSelectedIndex();
		if (index > 0) {
			lastSelect = pipes.get(index);
		}
	}
}
