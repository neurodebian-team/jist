/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.dest;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Destination to collect a summary of parameter values for all experiments.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeVolumeExternalDestination extends PipeExternalDestination {
	protected ParamVolumeCollection outputVolumes;
	protected boolean xmlEncodeModule(Document document, Element parent) {
		return super.xmlEncodeModule(document, parent);
//		Element em;	
//		em = document.createElement("outputVolumes");		
//		outputVolumes.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		outputVolumes  = (ParamVolumeCollection)outputParams.getFirstChildByName("Output Volumes");
//		outputVolumes = new ParamVolumeCollection();
//		outputVolumes.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"outputVolumes"));
	}
	/**
	 * Clean up destination after iterating though all experiments.
	 */
	public void complete() {
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createInputParams() {
		selectedParams = new ParamVolume("Parameters");
		ParamCollection input = new ParamCollection();
		input.setLabel("External Volume");
		input.setName("extvol");
		input.setCategory("Externalize");
		return input;
	}
	/**
	 * Get input parameter
	 */
	public ParamVolume getInputParam(){
		return (ParamVolume)selectedParams;
	}
	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createOutputParams() {
		ParamCollection output = new ParamCollection();
		output.add(outputVolumes=new ParamVolumeCollection("Output Volumes"));
		return output;
	}

	/**
	 * Iterate through objects.
	 * 
	 * @param monitor
	 *            the monitor
	 */
	public void iterate(ProgressMonitor monitor) {
		outputVolumes.add(getInputParam().getValue());
	}
	public void reset() {
		outputVolumes.clear();
	}
	
}
