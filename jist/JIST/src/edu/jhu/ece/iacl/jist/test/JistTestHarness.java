package edu.jhu.ece.iacl.jist.test;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeAssertionTestDestination;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;

// TODO: Auto-generated Javadoc
/**
 * The Class JistTestHarness.
 */
public class JistTestHarness{
	
	/** The layout files. */
	protected ArrayList<File> layoutFiles;
	
	/** The test directory. */
	protected File testDirectory;
	
	/** The passed dir. */
	protected File passedDir;
	
	/** The failed dir. */
	protected File failedDir;
	
	/** The manager. */
	protected ProcessManager manager;
	
	/** The failed files. */
	protected ArrayList<File> failedFiles;
	
	/** The passed files. */
	protected ArrayList<File> passedFiles;
	
	/**
	 * Instantiates a new jist test harness.
	 * 
	 * @param testDirectory the test directory
	 */
	public JistTestHarness(File testDirectory){
		this.testDirectory=testDirectory;
	}
	
	/**
	 * Run tests.
	 */
	public void runTests(){
		for(File f:layoutFiles){
			runTest(f);
		}
	}
	
	/**
	 * Clean.
	 */
	public void clean(){	
		for(File layoutFile:layoutFiles){
			File outputDir=new File(layoutFile.getParent(),FileReaderWriter.getFileName(layoutFile));
			ExecutionContext.deleteDir(outputDir,null);
		}		
	}
	
	/**
	 * Inits the.
	 */
	public void init(){
		layoutFiles=discoverLayouts(testDirectory);
		manager=new ProcessManager();
		failedFiles=new ArrayList<File>();
		passedFiles=new ArrayList<File>();
		
	}
	
	/**
	 * Run test.
	 * 
	 * @param layoutFile the layout file
	 */
	public void runTest(File layoutFile){
		PipeLayout layout=PipeLayout.read(layoutFile);
		String testName=FileReaderWriter.getFileName(layoutFile);
		File outputDir=new File(layoutFile.getParent(),testName);
		if(!outputDir.exists())outputDir.mkdir();
		layout.getRunParameters().setMaxProcs(1);
		layout.getRunParameters().setOutputDirectory(outputDir);
		System.out.println(getClass().getCanonicalName()+"\t"+"RUNNING "+layoutFile+" ...");
		manager.runAndWait(layout, false);
		boolean passed=true;
		File testResultFile=null;
		for(PipeModule mod:layout.getAllDescendantPipes()){
			if(mod instanceof PipeAssertionTestDestination){
				PipeAssertionTestDestination dest=((PipeAssertionTestDestination)mod);
				testResultFile=dest.getTestResultsFile();
				passed=dest.didAllTestsPass();
				if(passed){
					passedFiles.add(testResultFile);
				} else {
					failedFiles.add(testResultFile);
					System.out.println(getClass().getCanonicalName()+"\t"+testName+": TEST FAILED, SEE "+testResultFile);
				}
			}
		}
		File[][] files=new File[passedFiles.size()][1];
		for(int i=0;i<files.length;i++){
			files[i][0]=passedFiles.get(i);
		}
		ArrayObjectTxtReaderWriter.getInstance().write(files, new File(testDirectory,"passed.csv"));
		files=new File[failedFiles.size()][1];
		for(int i=0;i<files.length;i++){
			files[i][0]=failedFiles.get(i);
		}
		ArrayObjectTxtReaderWriter.getInstance().write(files, new File(testDirectory,"failed.csv"));

	}
	
	/**
	 * Complete.
	 */
	public void complete(){
		System.out.println(getClass().getCanonicalName()+"\t"+"PASSED: "+passedFiles.size()+" FAILED: "+failedFiles.size()+" TOTAL: "+(passedFiles.size()+failedFiles.size()));
		manager.forceQuit();
	}
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args){
		MipavController.setQuiet(true);
		MipavController.init();
		JistTestHarness harness=new JistTestHarness(new File(args[0]));
		harness.init();
		harness.runTests();
		harness.complete();
		System.exit(0);
	}
	
	/**
	 * Discover layouts.
	 * 
	 * @param startDir the start dir
	 * 
	 * @return the array list< file>
	 */
	protected ArrayList<File> discoverLayouts(File startDir){
		LinkedList<File> dirs = new LinkedList<File>();
		ArrayList<File> layoutFiles=new ArrayList<File>();
		dirs.add(startDir);
		File d;
		while (dirs.size() > 0) {
			d = dirs.remove();
			if (d == null) {
				continue;
			}
			File[] files = d.listFiles();
			if (files == null) {
				continue;
			}
			for (File f : files) {
				if (!f.isHidden()) {
					if (f.isDirectory()) {
						// Add directory to search path
						dirs.add(f);
					} else {
						String ext = FileReaderWriter.getFileExtension(f);
						if (ext.equals(JistPreferences.getPreferences().getDefaultLayoutExtension())) {
							layoutFiles.add(f);
						}
					}
				}
			}
		}
		return layoutFiles;
	}
}
