package edu.jhu.ece.iacl.jist.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurvePath;

// TODO: Auto-generated Javadoc
/**
 * The Class CurveVtkReaderWriter.
 */
public class CurveVtkReaderWriter extends FileReaderWriter<CurveCollection>{
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/** The Constant readerWriter. */
	protected static final CurveVtkReaderWriter readerWriter=new CurveVtkReaderWriter();
	
	/**
	 * Gets the single instance of CurveVtkReaderWriter.
	 * 
	 * @return single instance of CurveVtkReaderWriter
	 */
	public static CurveVtkReaderWriter getInstance(){
		return readerWriter;
	}
	
	/**
	 * Instantiates a new curve vtk reader writer.
	 */
	public CurveVtkReaderWriter(){
		super(new FileExtensionFilter(new String[]{"vtk"}));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected CurveCollection readObject(File f) {
		BufferedReader in;
		StringBuffer buff = new StringBuffer();
		CurveCollection curves=new CurveCollection();
		try {
			// Create input stream from file
			in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));

			String str;
			// Read file as string
			while ((str = in.readLine()) != null) {
				buff.append(str+"\n");
			}
		} catch (Exception e) {
			System.err.println(getClass().getCanonicalName()+"Error occured while reading parameter file:\n"+e.getMessage());
			e.printStackTrace();
			return null;
		}
		Pattern header=Pattern.compile("POINTS\\s\\d+\\sfloat");
		Matcher m=header.matcher(buff);
		int vertexCount=0;
		int lineCount=0;
		Point3f[] points;
		//Find vertex count
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					vertexCount=Integer.parseInt(vals[vals.length-1]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE VERTEX COUNT");
					return null;
				}
			}
			points=new Point3f[vertexCount];
			System.out.println("jist.io"+"\t"+"VERTS "+vertexCount);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",vertexCount*3+2);
			//Read in vertexes
			for(int i=1;i<strs.length-1;i+=3){
				try {
					Point3f p=new Point3f();
					p.x=Float.parseFloat(strs[i]);
					p.y=Float.parseFloat(strs[i+1]);
					p.z=Float.parseFloat(strs[i+2]);
					points[(i-1)/3]=p;
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT VERTS");
					return null;
				}
			}
		} else return null;
		//Find cell count
		header=Pattern.compile("LINES\\s+\\d+\\s+\\d+");
		m=header.matcher(buff);
		if(m.find()){
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>2){
				try {
					lineCount=Integer.parseInt(vals[1]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE LINE COUNT");
					return null;
				}
			}
			System.out.println("jist.io"+"\t"+"LINE COUNT "+lineCount);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",lineCount+vertexCount+2);	
			int count=0;
			int idx=-1;
			int len=0;
			int i=1;
			//Read in indexes
			while(i<strs.length-1){
				try {
					len=Integer.parseInt(strs[i]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT INDICES ["+strs[i]+"]");
					return null;
				}
				CurvePath path=new CurvePath();
				count=0;
				i++;
				for(;i<strs.length-1&&count<len;i++){
					try {
						idx=Integer.parseInt(strs[i]);
					} catch(NumberFormatException e){
						System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT INDICES");
						return null;
					}
					path.add(points[idx]);
					count++;
				}
				curves.add(path);
			}
		} else return null;
		curves.setName(FileReaderWriter.getFileName(f));

		header=Pattern.compile("CELL_DATA\\s+\\d+\\D+float\\s+\\d+\\nLOOKUP_TABLE\\s");
		m=header.matcher(buff);
		double[][] cellData=null;
		int count=0;
		int dim=0;
		//Find cell data count
		if(m.find()){
			
			String head=buff.substring(m.start(),m.end());
			String[] vals=head.split("\\D+");
			if(vals.length>0){
				try {
					count=Integer.parseInt(vals[1]);
					dim=Integer.parseInt(vals[2]);
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT DETERMINE DATA POINTS");
					return null;
				}
			}
			cellData=new double[count][dim];
			System.out.println("jist.io"+"\t"+"CELL DATA "+count+" by "+dim);
			String[] strs=buff.substring(m.end(),buff.length()).split("\\s+",count*dim+2);
			int index=0;
			//Read in cell data
			for(int i=1;i<strs.length&&index<count*dim;i++){
				try {		
					cellData[index/dim][index%dim]=Double.parseDouble(strs[i]);
					index++;
				} catch(NumberFormatException e){
					System.err.println(getClass().getCanonicalName()+"CANNOT FORMAT DATA ["+strs[i]+"]");
					//return null;
				}
			}
			System.out.println("jist.io"+"\t"+index+" "+count);
			curves.setLineData(cellData);
		}
		return curves;

	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(CurveCollection curves,File dir){
		String name = curves.getName();
		System.out.println("jist.io"+"\t"+"WHITE "+name);
		File f = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + ".vtk");
		if((f=writeObject(curves,f))!=null){
			return f;
		} else {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(CurveCollection collection, File f) {
		try {
			System.out.println("jist.io"+"\t"+"GET CURVES");
			Point3f[][] curves=collection.getCurves();
			int[][] indices=new int[curves.length][0];
			int pointCount=0;
			System.out.println("jist.io"+"\t"+"WRITE COLLECTION "+curves.length);
			for(int i=0;i<curves.length;i++){
				indices[i]=new int[curves[i].length];
				pointCount+=curves[i].length;
			}
			BufferedWriter data=new BufferedWriter(new FileWriter(f));
			data.append("# vtk DataFile Version 3.0\n"
					+"Curves\n"
					+"ASCII\n"
					+"DATASET POLYDATA\n"
					+"POINTS "+pointCount+" float\n");
			int i=0,j=0;
			Point3f p;
			int index=0;
			//Write curve points
			if(curves.length>0)
				System.out.println("jist.io"+"\t"+"WRITING "+curves.length+" CURVES WITH LENGTH "+curves[0].length+" POINT COUNT "+pointCount);
			else
				System.out.println("jist.io"+"\t"+"No curves to write.");
			for(i=0;i<curves.length;i++){
				for(j=0;j<curves[i].length;j++){
					p=curves[i][j];
					data.append(p.x+" "+p.y+" "+p.z+"\n");
					indices[i][j]=index++;
				}
			}
			//Write indexes to each curve
			data.append("LINES "+curves.length+" "+(pointCount+curves.length)+"\n");
			for(i=0;i<curves.length;i++){
				data.append(curves[i].length+" ");
				for(j=0;j<indices[i].length;j++){
					data.append(indices[i][j]+" ");
				}
				data.append("\n");
			}
			//Write embedded line data
			//data.append("CELL_TYPES "+curves.length+"\n");
			//for(i=0;i<curves.length;i++)data.append("4 ");
			double[][] lineData=collection.getLineData();
			if(lineData!=null&&lineData.length>0){
				data.append("\nCELL_DATA "+curves.length+"\n"
						+"SCALARS Length float "+lineData[0].length+"\n"
						+"LOOKUP_TABLE default\n");
				for(i=0;i<lineData.length;i++){
					for(j=0;j<lineData[i].length;j++){
						data.append(lineData[i][j]+" ");
					}
					data.append("\n");
				}
			}
			data.close();
			return f;
		} catch (IOException e) {
			System.err.println(getClass().getCanonicalName()+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
