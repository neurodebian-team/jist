/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.dest;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipeDestination;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPerformance;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Destination to collect a summary of parameter values for all experiments.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeParamSummaryDestination extends PipeDestination {
	
	/** The param summary file. */
	protected ParamObject<Object[][]> paramSummaryFile;
	
	/** The file name. */
	protected ParamString fileName;
	
	/** The data objects. */
	transient protected ArrayList<Object[]> dataObjects;
	
	
	protected boolean xmlEncodeModule(Document document, Element parent) {
		return super.xmlEncodeModule(document, parent);
//		Element em;	
//		em = document.createElement("fileName");		
//		fileName.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		em = document.createElement("paramSummaryFile");		
//		paramSummaryFile.xmlEncodeParam(document, em);
//		parent.appendChild(em);
//		
//		return true;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		fileName = (ParamString) inputParams.getFirstChildByName("File Name");
		paramSummaryFile  = (ParamObject) outputParams.getFirstChildByName("Summary File");
//
//		fileName = new ParamString();
//		fileName.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileName"));
//		paramSummaryFile = new ParamObject<Object[][]>();
//		paramSummaryFile.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"paramSummaryFile"));
	}
	
	/**
	 * Get input parameter
	 */
	public ParamCollection getInputParam(){
		return (ParamCollection)selectedParams;
	}
	/**
	 * Clean up destination after iterating though all experiments.
	 */
	public void complete() {
		Object[][] objs = new Object[dataObjects.size()][getInputParam().size()];
		for (int i = 0; i < dataObjects.size(); i++) {
			objs[i] = dataObjects.get(i);
		}
		paramSummaryFile.setObject(objs);
		paramSummaryFile.setFileName(fileName.getValue());
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createInputParams() {
		selectedParams = new ParamCollection("Parameters");
		ParamCollection input = new ParamCollection();
		input.setLabel("Summary");
		input.setName("summary");
		input.add(fileName = new ParamString("File Name", "summary"));
		input.setCategory("Output");
		return input;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	protected ParamCollection createOutputParams() {
		ParamCollection output = new ParamCollection();
		output.add(paramSummaryFile = new ParamObject("Summary File", ParamFile.DialogType.FILE));
		paramSummaryFile.setMandatory(false);
		paramSummaryFile.setReaderWriter(new ArrayObjectTxtReaderWriter());
		return output;
	}

	/**
	 * Iterate through objects.
	 * 
	 * @param monitor
	 *            the monitor
	 */
	public void iterate(ProgressMonitor monitor) {
		Vector<ParamModel> allParams = getInputParam().getAllDescendants();
		Object[] objs = new Object[allParams.size()];
		if (dataObjects.size() == 0) {
			String[] labels = new String[allParams.size()];
			for (int i = 0; i < allParams.size(); i++) {
				ParamModel mod = allParams.get(i);
				labels[i] = mod.getLabel() + " (" + mod.getOwner().getLabel() + ")";
			}
			dataObjects.add(labels);
		}
		int i = 0;
		for (ParamModel model : allParams) {
			if (model instanceof ParamPerformance) {
				objs[i] = ((ParamPerformance) model).getValue().getTotalElapsedCPUTime() / 1000.0;
				i++;
				continue;
			} else if (model instanceof ParamFile) {
				objs[i] = ((ParamFile) model).getValue().getAbsolutePath();
				i++;
				continue;
			} else if (model instanceof ParamVolume) {
				objs[i] = ((ParamVolume) model).getValue().getAbsolutePath();
				i++;
				continue;
			}
			String str = model.toString();
			try {
				objs[i] = Integer.parseInt(str);
				i++;
				continue;
			} catch (NumberFormatException e) {
			}
			try {
				objs[i] = Double.parseDouble(str);
				i++;
				continue;
			} catch (NumberFormatException e) {
			}
			objs[i] = str;
			i++;
		}
		dataObjects.add(objs);
	}
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeDestination#reset()
	 */
	public void reset() {
		dataObjects = new ArrayList<Object[]>();
	}
}
