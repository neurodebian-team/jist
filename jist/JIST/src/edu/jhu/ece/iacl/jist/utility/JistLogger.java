package edu.jhu.ece.iacl.jist.utility;

import java.util.Vector;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import gov.nih.mipav.model.structures.ModelImage;

public class JistLogger {

	public final static int PAD_MESSAGE_LENGTH=100;
	public final static int SEVERE = 0;
	public final static int WARNING = 1;
	public final static int INFO = 2;
	public final static int CONFIG = 3;
	public final static int FINE = 4;
	public final static int FINER = 5;
	public final static int FINEST = 6;
	public final static int INTENSIVE_I = 7;
	public final static int INTENSIVE_II = 8;
	public final static int INTENSIVE_III = 9;

	public static void logError(int debugLevel, String msg) {
		JistPreferences prefs = JistPreferences.getPreferences();
		if(prefs.getDebugLevel()>=debugLevel) {
			System.err.println(augmentMessage(prefs.getDebugLevel(),msg));
			if(prefs.getDebugLevel()>=FINE)
				System.err.flush();
		}
	}
	public static String padRight(String s, int n) {
		while(n>0) {
			s+= " "; n--;
		}
		return s;
//		return String.format("%1$-" + n + "s"+n, s);  
	}

	private static String augmentMessage(int debugLevel, String msg) {
		msg = msg.replaceAll("\t", "     ").trim();
		if(debugLevel>INFO) {
			if(msg.length()<PAD_MESSAGE_LENGTH)
				msg = padRight(msg,PAD_MESSAGE_LENGTH-msg.length());

			final Throwable throwable = new IllegalArgumentException("Blah");
			StackTraceElement[] trace = throwable.getStackTrace();
			if(trace.length>2) {
				msg = msg + "\t("+trace[2].getFileName()+":"+trace[2].getLineNumber()+")";
			}	else {
				msg = msg + "\t("+trace[1].getFileName()+":"+trace[1].getLineNumber()+")";	
			}
		}
		if(debugLevel>=INTENSIVE_I){
			Runtime runtime = Runtime.getRuntime();  
			long maxMemory = runtime.maxMemory();  
			long allocatedMemory = runtime.totalMemory();  
			long freeMemory = runtime.freeMemory();
			
			long totalFreeMemory = (freeMemory + (maxMemory - allocatedMemory));   
long totalUsedMemory = maxMemory -totalFreeMemory; 
			final long MB = (1 << 20);
			String memoryReport = (totalFreeMemory/MB)+"\t"+
			(totalUsedMemory/MB)+"\t"+
			(allocatedMemory/MB)+"\t"+
			(maxMemory/MB); 			
			msg+="\t"+memoryReport;
		}
		return msg;
	}


	public static void logOutput(int debugLevel, String msg) {
		JistPreferences prefs = JistPreferences.getPreferences();
		if(prefs.getDebugLevel()>=debugLevel) {
			System.out.println(augmentMessage(prefs.getDebugLevel(),msg));
			if(prefs.getDebugLevel()>=FINE)
				System.out.flush();
		}
	}

	public static void logFlush() {
		System.out.flush();
		System.err.flush();
	}
	
	public static void logMIPAVRegistry() {
		Vector<ModelImage>imgs = MipavController.getImages();
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("MIPAV REGISTRY:");
		for(ModelImage img: imgs) {
			System.out.println("\t"+img.getImageName()+"\t: Pixels: "+(img.getNumPixels()));
		}
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
		System.out.flush();
		
	}
}
