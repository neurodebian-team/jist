package edu.jhu.ece.iacl.jist.io;

import java.io.File;
import java.io.IOException;

import edu.jhu.ece.iacl.jist.structures.geom.MipavVOI;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.view.MipavUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class VOIReaderWriter.
 */
public class VOIReaderWriter extends FileReaderWriter<MipavVOI> {
	 protected FileExtensionFilter extensionFilter;
	public void setExtensionFilter(FileExtensionFilter extensionFilter) {
		this.extensionFilter = extensionFilter;
	}
	public FileExtensionFilter getExtensionFilter() {
		return extensionFilter;
	}
	/**
	 * Instantiates a new vOI reader writer.
	 */
	public VOIReaderWriter() {
		super(new FileExtensionFilter(new String[] { "xml"}));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObjectToDirectory(java.lang.Object, java.io.File)
	 */
	protected File writeObjectToDirectory(MipavVOI img, File dir) {
		String name = img.getName();
		File f = new File(dir, name + ".xml");
		if ((f=writeObject(img, f))!=null) {
			return f;
		} else {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#readObject(java.io.File)
	 */
	protected MipavVOI readObject(File f) {
		//Cannot read VOI because it requires a volume which we don't have apriori.
		return null;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.io.FileReaderWriter#writeObject(java.lang.Object, java.io.File)
	 */
	protected File writeObject(MipavVOI VOIs, File f) {
		int i;
		MipavFileVOI fileVOI;
		String voiDir = f.getParentFile().getAbsolutePath();
		ModelImage img = VOIs.getImage();
		try {
			if(f.exists())f.delete();
			String name=VOIs.getName() + ".xml";
			fileVOI = new MipavFileVOI(name, voiDir+File.separator,img);
			fileVOI.writeXML(VOIs,true);
			return f;
		} catch (IOException error) {
			MipavUtil.displayError("Error writing all VOIs to " + voiDir + ": "
					+ error);
		}
		return null;
	}
}
