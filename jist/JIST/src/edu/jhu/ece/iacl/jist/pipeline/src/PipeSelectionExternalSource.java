/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.src;

import java.util.List;
import java.util.Vector;

import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;

/**
 * Source to select multiple options.
 * 
 * @author Blake Lucas
 */
public class PipeSelectionExternalSource extends PipeExternalSource {
	
	
	/** The selection. */
	transient protected List<String> selection;
	
	/** The index. */
	transient protected int index;

	/**
	 * Instantiates a new pipe selection source.
	 */
	public PipeSelectionExternalSource() {
		super();
		getParentPort().setParameter(valParam);
	}

	/**
	 * Update selection options when a new connector is added.
	 * 
	 * @param port
	 *            the port
	 */
	public void connectAction(PipePort port) {
		super.connectAction(port);
		if (port == valParam) {
			Vector<PipePort> ports = port.getOutgoingPorts();
			for (PipePort child : ports) {
				if (child instanceof ParamOption) {
					List<String> options = ((ParamOption) child).getOptions();
					for (String opt : options) {
						((ParamOption)valParam).add(opt);
						((ParamOption)defaultValueParam).add(opt);
					}
				}
			}
		}
	}

	/**
	 * Create input parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("External Selection");
		group.setName("extselect");
		group.setCategory("Externalize.Selection");
		group.add(defaultValueParam= new ParamOption("Selection",new String[]{}));
		return group;
	}

	/**
	 * Create output parameters.
	 * 
	 * @return the param collection
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection("Multi-Selection");
		group.add(valParam = new ParamOption("Selection", new String[] {}));
		return group;
	}

	/**
	 * Update selection options if connector is removed.
	 * 
	 * @param owner
	 *            the owner
	 * @param child
	 *            the child
	 * @param wire
	 *            the wire
	 */
	public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire) {
		super.disconnectAction(owner, child, wire);
		if (owner == ((ParamOption)valParam)) {
			String select =((ParamOption)defaultValueParam).getValue();
			((ParamOption)valParam).setOptions(null);
			((ParamOption)defaultValueParam).setOptions(null);
			connectAction(owner);
			if(select!=null){
				((ParamOption)defaultValueParam).setValue(select);
				((ParamOption)defaultValueParam).getInputView().update();
			} 
		}
	}

	/**
	 * Get output parameter.
	 * 
	 * @return the output param
	 */
	public ParamOption getOutputParam() {
		return ((ParamOption)valParam);
	}
}
