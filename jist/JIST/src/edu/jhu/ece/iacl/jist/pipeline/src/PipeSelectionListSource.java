package edu.jhu.ece.iacl.jist.pipeline.src;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipeChildPort;
import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.PipeSource;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;
/**
 * A parameter that allows the user to input a number of options from a text file
 * 
 * @author John Bogovic
 */
public class PipeSelectionListSource extends PipeListSource {
//public class PipeSelectionListSource {
	
	
	/** The option. */
	protected ParamOption option;
	
//	/** The file param. */
//	protected ParamFile fileParam;
	
	/** The Map */
	transient protected HashMap<String,Integer> optionMap;
	

	protected boolean xmlEncodeModule(Document document, Element parent) {
		boolean val = super.xmlEncodeModule(document, parent);		
//		Element em;
//		em = document.createElement("option");
//		option.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//		em = document.createElement("fileParam");
//		fileParam.xmlEncodeParam(document, em);	
//		parent.appendChild(em);
//					
//		return true;
		return val;
	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		option = (ParamOption)outputParams.getFirstChildByName("Option");
//		fileParam = (ParamFile) outputParams.getFirstChildByName("Point");
//		option = new ParamOption();
//		option.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"option"));
//		fileParam = new ParamFile();
//		fileParam.xmlDecodeParam(document,JistXMLUtil.xmlReadElement(el,"fileParam"));
		getParentPort().setParameter(option);
	}
	
	/**
	 * Instantiates a new pipe selection List source.
	 */
	public PipeSelectionListSource() {
		super();
		getParentPort().setParameter(option);
	}

	public ParamCollection createInputParams() {
		ParamCollection group = super.createInputParams();
		group.setLabel("Selection List");
		group.setName("optionlist");
		group.setCategory("Selection");
		return group;
	}
	
	@Override
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Option List");
		group.add(option = new ParamOption("Option", new String[]{}));
		return group;
	}
	
	@Override
	public ParamOption getOutputParam() {
		return option;
	}
	
	@Override
	protected String getValue(int i) {
		if (off < data[i].length) {
			return (String)data[i][0];
		} else {
			return null;
		}
	}

	@Override
	protected void setValue(Object obj) {
		option.setValue((String)obj);
	}
	
	/**
	 * Reset iterator.
	 */
	@Override
	public void reset() {
//		super.reset();
		isReset = false;
		for (PipePort port : getParentPort().getOutgoingPorts()) {
			if (port instanceof PipeChildPort) {
				PipeSource src = ((PipeChildPort) port).getOwner();
				src.reset();
			}
		}
		data = ArrayObjectTxtReaderWriter.getInstance().read(fileParam.getValue());
		
//		System.out.println(getClass().getCanonicalName()+"\t"+"");
//		System.out.println(getClass().getCanonicalName()+"\t"+data.length);
//		System.out.println(getClass().getCanonicalName()+"\t"+data[0].length);
//		System.out.println(getClass().getCanonicalName()+"\t"+data[0][0]);
//		System.out.println(getClass().getCanonicalName()+"\t"+data[1][0]);
//		System.out.println(getClass().getCanonicalName()+"\t"+"");
		
		index = 0;
		st = startEntry.getInt();
		end = endEntry.getInt();
		off = offset.getInt();
		index = st;
		Object obj;
		if (((end == -1) || (index < end)) && (index < data.length)) {
			setValue(obj = getValue(index++));
		} else {
			index++;
		}
		push();
	}
	
	public void connectAction(PipePort port) {
		super.connectAction(port);
		if (port == option) {
			Vector<PipePort> ports = port.getOutgoingPorts();
			for (PipePort child : ports) {
				optionMap = new HashMap<String, Integer>();
				if (child instanceof ParamOption) {
					List<String> options = ((ParamOption) child).getOptions();
					String[] goodoptions = new String[options.size()];
					int i=0;
					for (String opt : options) {
						option.add(opt);
//						multiOption.add(opt);
						optionMap.put(opt, new Integer(i));
						goodoptions[i]=opt;
						i++;
					}
					System.out.println(getClass().getCanonicalName()+"\t"+"Refreshed options!");
					System.out.println(getClass().getCanonicalName()+"\t"+options);
					System.out.println(getClass().getCanonicalName()+"\t"+"");
					option.setOptions(options);
				}
			}
			
		}
	}
	
	public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire) {
		super.disconnectAction(owner, child, wire);
		if (owner == option) {
//			List<String> select = multiOption.getValue();
			option.setOptions(null);
//			multiOption.setOptions(null);
			connectAction(owner);
//			multiOption.setValue(select);
//			multiOption.getInputView().update();
		}
	}
	
}
