package edu.jhu.ece.iacl.jist.pipeline.parameter;

/**
 * Interface used to flag options which are strictly internal to the JIST system.
 *
 * @author bennett
 *
 */
public interface JISTInternalParam {

}
