/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.graph;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.util.EventObject;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.FontUIResource;

import org.jgraph.graph.DefaultGraphCellEditor;
import org.jgraph.graph.GraphCellEditor;

/**
 * Editor for index spinner box.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class PipeEdgeEditor extends DefaultGraphCellEditor {
	
	/**
	 * The Class DefaultSpinner.
	 */
	public class DefaultSpinner extends JSpinner {
		
		/** Border to use. */
		protected Border border;

		/**
		 * Constructs a DefaultTreeCellEditor$DefaultTextField object.
		 * 
		 * @param border
		 *            a Border object
		 */
		public DefaultSpinner(Border border) {
			super(new SpinnerNumberModel(0, 0, 1000000, 1));
			this.setPreferredSize(new Dimension(20, 20));
			this.border = border;
		}

		/**
		 * Overrides <code>JComponent.getBorder</code> to returns the current
		 * border.
		 * 
		 * @return the border
		 */
		public Border getBorder() {
			return border;
		}

		// implements java.awt.MenuContainer
		/* (non-Javadoc)
		 * @see java.awt.Component#getFont()
		 */
		public Font getFont() {
			Font font = super.getFont();
			// Prefer the parent containers font if our font is a
			// FontUIResource
			if (font instanceof FontUIResource) {
				Container parent = getParent();
				if ((parent != null) && (parent.getFont() != null)) {
					font = parent.getFont();
				}
			}
			return font;
		}
	}

	/** The editor inside cell. */
	public static boolean editorInsideCell = true;

	/**
	 * Instantiates a new pipe edge editor.
	 */
	public PipeEdgeEditor() {
		super();
	}

	/**
	 * This is invoked if a TreeCellEditor is not supplied in the constructor.
	 * It returns a TextField editor.
	 * 
	 * @return the graph cell editor
	 */
	protected GraphCellEditor createGraphCellEditor() {
		Border aBorder = UIManager.getBorder("Tree.editorBorder");
		PipeEdgeRealEditor editor = new PipeEdgeRealEditor(new DefaultSpinner(aBorder)) {
			public boolean shouldSelectCell(EventObject event) {
				boolean retValue = super.shouldSelectCell(event);
				getComponent().requestFocus();
				return retValue;
			}
		};
		// One click to edit.
		editor.setClickCountToStart(1);
		return editor;
	}
}
