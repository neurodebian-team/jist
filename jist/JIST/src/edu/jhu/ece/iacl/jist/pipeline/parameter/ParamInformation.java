/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamInformationFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * Stores information about algorithm.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamInformation extends ParamModel<AlgorithmInformation>  implements JISTInternalParam{
	
	/** The info. */
	protected AlgorithmInformation info;

	 public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);		 
		 Element em;				
		 em = document.createElement("info");				 
		 if(info.xmlEncodeParam(document,em))
			 parent.appendChild(em);			
		 else 
			 return false;
		 
			
		 return true;
	 }
	 
	 public void xmlDecodeParam(Document document, Element parent) {
			super.xmlDecodeParam(document, parent);			
			info = new AlgorithmInformation(null, null, null);
			info.xmlDecodeParam(document,
					JistXMLUtil.xmlReadElement(parent, "info"));
		}
	 
	/**
	 * Default constructor.
	 */
	public ParamInformation() {
		init();
	}

	/**
	 * Constructor.
	 * 
	 * @param info
	 *            algorithm information
	 */
	public ParamInformation(AlgorithmInformation info) {
		this();
		setName("Algorithm Information");
		setValue(info);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 */
	public ParamInformation(String name) {
		this();
		setName(name);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            parameter name
	 * @param info
	 *            algorithm information
	 */
	public ParamInformation(String name, AlgorithmInformation info) {
		this();
		setName(name);
		setValue(info);
	}

	/**
	 * Clone object.
	 * 
	 * @return the param information
	 */
	public ParamInformation clone() {
		ParamInformation mod = new ParamInformation();
		mod.setValue(this.getValue().clone());
		mod.label=this.label;
		mod.setName(this.getName());
		return mod;
	}

	/**
	 * Compare information.
	 * 
	 * @param mod
	 *            the mod
	 * @return the int
	 */
	public int compareTo(ParamModel mod) {
		if (!(mod instanceof ParamInformation)) {
			return 1;
		}
		return this.info.compareTo((AlgorithmInformation) mod.getValue());
	}

	/**
	 * Get algorithm information.
	 * 
	 * @return the value
	 */
	public AlgorithmInformation getValue() {
		return info;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		connectible = false;
		this.factory = new ParamInformationFactory(this);
	}

	/**
	 * Set algorithm information.
	 * 
	 * @param value
	 *            the value
	 * @throws InvalidParameterValueException
	 *             the invalid parameter value exception
	 */
	public void setValue(AlgorithmInformation value) throws InvalidParameterValueException {
		this.info = value;
	}

	/**
	 * Get description of information.
	 * 
	 * @return the string
	 */
	public String toString() {
		return info.toString();
	}

	/**
	 * Validate information.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if (info == null) {
			throw new InvalidParameterException(this);
		}
	}
	public boolean equals(ParamModel<AlgorithmInformation> mod){
		return info.equals(mod.getValue());
	}
	
	@Override
	public String getHumanReadableDataType() {
		return "JIST INTERNAL Parameter. Report if this message is seen";
	}
	public String getXMLValue() {throw new RuntimeException("INTERNAL: Not Serializable"); };
	
	@Override
	public void setXMLValue(String arg) {
		throw new RuntimeException("INTERNAL: Not Serializable"); 
		
	};
	
	@Override
	public String probeDefaultValue() {	
		return null;
	}


	
	
}
