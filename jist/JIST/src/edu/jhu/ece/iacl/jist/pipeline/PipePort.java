/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline;

import java.util.Vector;

import javax.swing.JPanel;

import org.jgraph.graph.Port;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModulePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterValueException;

// TODO: Auto-generated Javadoc
/**
 * The Class PipePort.
 */
public abstract class PipePort<T> {
	
	/**
	 * Listener class to monitor for changes to a port.
	 * 
	 * @author Blake Lucas
	 */
	public static interface PortListener {
		
		/**
		 * Connect action.
		 * 
		 * @param owner the owner
		 */
		public void connectAction(PipePort owner);

		/**
		 * Disconnect action.
		 * 
		 * @param owner the owner
		 * @param child the child
		 * @param wire the wire
		 */
		public void disconnectAction(PipePort owner, PipePort child, PipeConnector wire);
	};

	/**
	 * Differentiate between input and output ports.
	 */
	public enum type {
		
		/** The INPUT. */
		INPUT, 
 
 /** The OUTPUT. */
 OUTPUT
	}

	/** Module that this port is attached to. */
	protected PipeModule owner;
	
	/** incoming connectors attached to this port. */
	protected Vector<PipeConnector> incomingConnectors = null;
	
	/** outgoing connectors attached to this port. */
	protected Vector<PipeConnector> outgoingConnectors = null;
	
	/** indicates whether this is an input or output port. */
	transient protected type portType;
	
	/** Maximum number of incoming ports. -1 indicates unlimited. */
	transient protected int maxIncoming = 1;
	
	/** Maximum number of outgoing ports. -1 indicates unlimited. */
	transient protected int maxOutgoing = -1;
	
	/** Flag indicating whether the user prefers to use a connector or not for this port. */
	transient protected boolean useWire = false;
	
	/** Rendering port component. */
	transient protected PipeModulePort gport;
	
	/** Listeners that are called when port is connected or disconnected. */
	transient protected Vector<PortListener> listeners;
	
	/** Indicates if this port can be connected to. */
	transient protected boolean connectible = true;

	/**
	 * Default constructor.
	 */
	public PipePort() {
		listeners = new Vector<PortListener>();
	}

	/**
	 * Default constructor.
	 * 
	 * @param owner module that owns this port
	 * @param type port type
	 */
	public PipePort(PipeModule owner, PipePort.type type) {
		this();
		setPortType(type);
		this.owner = owner;
	}

	/**
	 * Add listener.
	 * 
	 * @param listener port listener
	 */
	public void addListener(PortListener listener) {
		if (!getListeners().contains(listener)) {
			getListeners().add(listener);
		}
	}

	/**
	 * Clone parameter.
	 * 
	 * @return the pipe port
	 */
	public abstract PipePort clone();

	/**
	 * Disconnect all incoming and outgoing connectors.
	 */
	public void disconnect() {
		if (isOutputPort()) {
			while (outgoingConnectors.size() > 0) {
				outgoingConnectors.firstElement().disconnect();
			}
		} else if (isInputPort()) {
			while (incomingConnectors.size() > 0) {
				incomingConnectors.firstElement().disconnect();
			}
		}
	}

	/**
	 * Disconnect a specific connector.
	 * 
	 * @param index connector index
	 */
	public void disconnect(int index) {
		if (index < 0) {
			disconnect();
		}
		if (isOutputPort()) {
			outgoingConnectors.get(index).disconnect();
		} else if (isInputPort()) {
			incomingConnectors.get(index).disconnect();
		}
	}

	/**
	 * Remove connector from this port.
	 * 
	 * @param wire connector
	 */
	public void disconnect(PipeConnector wire) {
		if (isOutputPort()) {
			disconnect(outgoingConnectors.indexOf(wire));
		} else if (isInputPort()) {
			disconnect(incomingConnectors.indexOf(wire));
		}
	}

	/**
	 * Remove all connections between this port and the specified port.
	 * 
	 * @param port the port
	 */
	public void disconnect(PipePort port) {
		if (isOutputPort()) {
			for (PipeConnector c : getOutgoingConnectors()) {
				if (c.getDestination() == port) {
					disconnect(c);
					break;
				}
			}
		} else if (isInputPort()) {
			for (PipeConnector c : getIncomingConnectors()) {
				if (c.getSource() == port) {
					disconnect(c);
					break;
				}
			}
		}
	}

	/**
	 * Get port description.
	 * 
	 * @return port description
	 */
	public String getDescription() {
		return portType + ":" + getLabel() + "::" + getValue();
	}

	/**
	 * Get port to display on graph.
	 * 
	 * @return the graph port
	 */
	public Port getGraphPort() {
		return gport;
	}

	/**
	 * Get incoming connector for specified source.
	 * 
	 * @param src source port
	 * 
	 * @return the incoming connector
	 */
	public PipeConnector getIncomingConnector(PipePort src) {
		for (PipeConnector wire : incomingConnectors) {
			if (wire.src == src) {
				return wire;
			}
		}
		return null;
	}

	/**
	 * Get input connections.
	 * 
	 * @return input connections
	 */
	public Vector<PipeConnector> getIncomingConnectors() {
		return incomingConnectors;
	}

	/**
	 * Get all ports that are connected to this port.
	 * 
	 * @return the incoming ports
	 */
	public Vector<PipePort> getIncomingPorts() {
		Vector<PipePort> ports = new Vector<PipePort>();
		if(incomingConnectors!=null){
			for (PipeConnector wire : incomingConnectors) {
				ports.add(wire.src);
			}
		}
		return ports;
	}

	/**
	 * Get displayed label.
	 * 
	 * @return displayed label
	 */
	public abstract String getLabel();

	/**
	 * Get all listeners.
	 * 
	 * @return the listeners
	 */
	public Vector<PortListener> getListeners() {
		if (listeners == null) {
			listeners = new Vector<PortListener>();
		}
		return listeners;
	}

	/**
	 * Maximum number of input ports.
	 * 
	 * @return the max incoming
	 */
	public int getMaxIncoming() {
		return maxIncoming;
	}

	/**
	 * Get maximum number of outgoing ports.
	 * 
	 * @return the max outgoing
	 */
	public int getMaxOutgoing() {
		return maxOutgoing;
	}

	/**
	 * Get identifying name.
	 * 
	 * @return identifying name
	 */
	public abstract String getName();

	/**
	 * Get outgoing connector for specified destination.
	 * 
	 * @param dest destination port
	 * 
	 * @return the outgoing connector
	 */
	public PipeConnector getOutgoingConnector(PipePort dest) {
		for (PipeConnector wire : outgoingConnectors) {
			if (wire.dest == dest) {
				return wire;
			}
		}
		return null;
	}

	/**
	 * Get output connections.
	 * 
	 * @return output connections
	 */
	public Vector<PipeConnector> getOutgoingConnectors() {
		return outgoingConnectors;
	}

	/**
	 * Get all ports that are connected to this port.
	 * 
	 * @return the outgoing ports
	 */
	public Vector<PipePort> getOutgoingPorts() {
		Vector<PipePort> ports = new Vector<PipePort>();
		for (PipeConnector wire : outgoingConnectors) {
			ports.add(wire.dest);
		}
		return ports;
	}

	/**
	 * Module that owns this port.
	 * 
	 * @return module owner
	 */
	public PipeModule getOwner() {
		return owner;
	}

	/**
	 * Get port type.
	 * 
	 * @return port type
	 */
	public type getPortType() {
		return this.portType;
	}

	/**
	 * Value associated with this port.
	 * 
	 * @return the value
	 */
	public abstract T getValue();

	/**
	 * Get panel to render user input to this port.
	 * 
	 * @return the view
	 */
	public abstract JPanel getView();

	/**
	 * Initialized data that could not be deserialized.
	 */
	public abstract void init();

	/**
	 * Check if port is compatible with other port. This check is not as
	 * rigorous as validate.
	 * 
	 * @param p parameter to check against compatibility
	 * 
	 * @return true is compatible
	 */
	public abstract boolean isCompatible(PipePort p);

	/**
	 * Returns true if this port is connectible.
	 * 
	 * @return true if connectible
	 */
	public boolean isConnectible() {
		return connectible;
	}

	/**
	 * Returns true if this port is connected to another port.
	 * 
	 * @return true if this port is connected
	 */
	public boolean isConnected() {
		return (isInputPort()) ? (incomingConnectors!=null&&incomingConnectors.size() > 0) : (outgoingConnectors!=null&&outgoingConnectors.size() > 0);
	}

	/**
	 * Returns true if this port is connected to the specified port.
	 * 
	 * @param p port
	 * 
	 * @return true, if checks if is connected to
	 */
	public boolean isConnectedTo(PipePort p) {
		return ((isOutputPort() && getOutgoingPorts().contains(p)) || (isInputPort() && getIncomingPorts().contains(p)));
	}

	/**
	 * Returns true if this is an input port.
	 * 
	 * @return true if input port
	 */
	public boolean isInputPort() {
		return (portType == type.INPUT);
	}

	/**
	 * Returns true if this is an output port.
	 * 
	 * @return true if output port
	 */
	public boolean isOutputPort() {
		return (portType == type.OUTPUT);
	}

	/**
	 * Notify port listeners of connections.
	 */
	public void notifyListenersOfConnection() {
		for (PortListener listener : getListeners()) {
			if(listener!=null)
				listener.connectAction(this);
		}
	}

	/**
	 * Notify port listeners of disconnections.
	 * 
	 * @param child disconnected port
	 * @param wire disconnected connector
	 */
	public void notifyListenersOfDisconnection(PipePort child, PipeConnector wire) {
		if (listeners == null) {
			listeners = new Vector<PortListener>();
		}
		for (PortListener listener : listeners) {
			listener.disconnectAction(this, child, wire);
		}
	}

	/**
	 * Remove all listeners.
	 */
	public void removeAllListeners() {
		getListeners().removeAllElements();
	}

	/**
	 * Remove listener.
	 * 
	 * @param listener port listener
	 */
	public void removeListener(PortListener listener) {
		getListeners().remove(listener);
	}

	/**
	 * Set whether this port is connectible.
	 * 
	 * @param connectable true if connectible
	 */
	public void setConnectable(boolean connectable) {
		this.connectible = connectable;
	}

	/**
	 * Set port to display on graph.
	 * 
	 * @param p the port
	 */
	public void setGraphPort(PipeModulePort p) {
		this.gport = p;
	}

	/**
	 * Set label to display next to port.
	 * 
	 * @param label the label
	 */
	public abstract void setLabel(String label);

	/**
	 * Set maximum number of input ports.
	 * 
	 * @param maxIncoming the max incoming
	 */
	public void setMaxIncoming(int maxIncoming) {
		this.maxIncoming = maxIncoming;
	}

	/**
	 * Set maximum number of outgoing ports.
	 * 
	 * @param maxOutgoing the max outgoing
	 */
	public void setMaxOutgoing(int maxOutgoing) {
		this.maxOutgoing = maxOutgoing;
	}

	/**
	 * Set name to identify port.
	 * 
	 * @param name the name
	 */
	public abstract void setName(String name);

	/**
	 * Set module that owns this port.
	 * 
	 * @param owner module owner
	 */
	public void setOwner(PipeModule owner) {
		this.addListener(owner);
		this.owner = owner;
	}

	/**
	 * Set port type as either input or output.
	 * 
	 * @param ty port type
	 */
	public void setPortType(type ty) {
		this.portType = ty;
		if (isOutputPort()) {
			if (outgoingConnectors == null) {
				outgoingConnectors = new Vector<PipeConnector>();
			}
			incomingConnectors = null;
		} else {
			outgoingConnectors = null;
			if (incomingConnectors == null) {
				incomingConnectors = new Vector<PipeConnector>();
			}
		}
	}

	/**
	 * Indicate that this port should use a connector instead of a default
	 * value.
	 * 
	 * @param useWire the use wire
	 */
	public void setUseConnector(boolean useWire) {
		this.useWire = useWire;
		if (!useWire) {
			this.disconnect();
		}
	}

	/**
	 * Set value for this parameter.
	 * 
	 * @param value parameter value
	 * 
	 * @throws InvalidParameterValueException the invalid parameter value exception
	 */
	public abstract void setValue(T value) throws InvalidParameterValueException;

	/**
	 * Get port description.
	 * 
	 * @return the string
	 */
	public String toString() {
		return getDescription();
	}

	/**
	 * Returns true if this port should use a connector instead of a default
	 * value.
	 * 
	 * @return true, if using connector
	 */
	public boolean usingConnector() {
		return useWire;
	}

	
	/**
	 * Validate parameter associated with this port.
	 * 
	 * @throws InvalidParameterException the invalid parameter exception
	 */
	public abstract void validate() throws InvalidParameterException;

	public boolean xmlEncodeConnections(Document document, Element parent) {
		boolean val = false;
		if(incomingConnectors!=null)
			for(PipeConnector in : incomingConnectors) {
				Element em = document.createElement("conn");

				PipePort src = in.getSource();
				PipePort dest = in.getDestination();

				int inIndex = in.getSourceIndex();
				Element subel;
				em.appendChild(subel=document.createElement("src"));
				subel.appendChild(document.createTextNode(src.getOwner().getUniqueID()));
				em.appendChild(subel=document.createElement("src-port"));
				subel.appendChild(document.createTextNode(src.getName()));
				if(inIndex!=-1) {
					em.appendChild(subel=document.createElement("src-port-index"));
					subel.appendChild(document.createTextNode(""+inIndex));
				}
				em.appendChild(subel=document.createElement("dest"));
				subel.appendChild(document.createTextNode(dest.getOwner().getUniqueID()));
				em.appendChild(subel=document.createElement("dest-port"));
				subel.appendChild(document.createTextNode(dest.getName()));			
				parent.appendChild(em);
				val=true;
			}
		return val;
	}
}
