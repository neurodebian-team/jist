package edu.jhu.ece.iacl.jist.pipeline.src;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;


public class PipeVolumeExternalSource extends PipeExternalSource{
	public PipeVolumeExternalSource(){
		super();
	}
	public ParamCollection createInputParams() {
		ParamCollection group = new ParamCollection();
		group.add(defaultValueParam = new ParamVolume("Default Volume"));
		group.setLabel("External Volume");
		group.setName("extvol");
		group.setCategory("Externalize.Volume");
		return group;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipeSource#createOutputParams()
	 */
	public ParamCollection createOutputParams() {
		ParamCollection group = new ParamCollection();
		group.setLabel("Volume");
		group.add(valParam = new ParamVolume("Volume"));
		return group;
	}
}
