/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.ece.iacl.jist.pipeline.parameter;

import java.io.File;
import java.net.URI;

import edu.jhu.ece.iacl.jist.pipeline.PipePort;

/**
 * Executable file parameter.
 * 
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class ParamExecutable extends ParamFile {
	
	/**
	 * Default constructor.
	 * 
	 * @param f
	 *            default constructor
	 */
	public ParamExecutable(File f) {
		super("Executable", f, DialogType.FILE);
	}
	
	public ParamExecutable() {
	this((File)null);
	};
	/**
	 * Default constructor.
	 * 
	 * @param f
	 *            default constructor
	 */
	public ParamExecutable(URI f) {
		super("Executable", f, DialogType.FILE);
	}
	/**
	 * Clone parameter.
	 * 
	 * @return the param executable
	 */
	public ParamExecutable clone() {
		ParamExecutable param = new ParamExecutable(this.getURI());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		super.init();
		connectible = false;
	}

	/**
	 * Always returns false. An executable port cannot be connected to another
	 * port.
	 * 
	 * @param port
	 *            the port
	 * @return true, if checks if is compatible
	 */
	public boolean isCompatible(PipePort port) {
		// Cannot connect output to this type of input
		return false;
	}

	/**
	 * Validate parameter.
	 * 
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		super.validate();
		if (!mandatory) {
			return;
		}
		file=getValue();
		if (((file == null) || ((!file.exists()) && mandatory))
				|| ((extensionFilter != null) && !extensionFilter.accept(file))
				|| ((dialogType == DialogType.DIRECTORY) && file.isFile())
				|| ((dialogType == DialogType.FILE) && file.isDirectory())) {
			throw new InvalidParameterException(this);
		}
		if ((file != null) && file.exists() && !file.canExecute()) {
			throw new InvalidParameterException(this);
		}
	}
}
