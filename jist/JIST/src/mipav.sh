#!/bin/sh
#
# MIPAV Start Up script.
# $Id: mipav,v 1.9 2008/08/27 18:11:42 aaron Exp $
#


COMMAND=`echo $0 | sed -e 's,.*/,,'`
REVISION="$Revision: 1.9 $"
VERSION=`echo "$REVISION" | sed -e 's/:\| \|\$$//g;'`
MIPAV_RELEASE=



VERSION_INFO="   $COMMAND $MIPAV_RELEASE

MIPAV Release         $MIPAV_RELEASE
MIPAV RPM Release     $RPM_RELEASE
MIPAV Startup Script  $VERSION

"


USAGE="   $COMMAND $MIPAV_RELEASE

Usage: $COMMAND [-hlsvV] 

   -a         Specify alternative Java Binary to the system default.
   -c         Append CLASSPATH enviroment variable to Java CLASSPATH.
   -h         This help message.
   -H         Head less mode.
   -l         Write the output to log file /tmp/USERNAME_mipav_PID.log
   -v         Display the output in the terminal.
   -V         Version information.

All other command line arguments are passed along to MIPAV.

This script is not strictly part of MIPAV.

"


# standard parameters
HEADLESS=0

MIPAV_BASE=/usr/share/mipav
SYS_BASE=/usr/share/java
MIPAV_JAR=`find $MIPAV_BASE -name \*.jar | sed "s#${MIPAV_BASE}/*#:#" | tr -d '\n' | sed 's/^://'`
SYS_JAR=`find $SYS_BASE -name \*.jar | sed "s#\.jar*#\.jar:#"  | tr -d '\n' | sed 's/^://'`
MIPAV_JAVA=`which java`
MIPAV_JAVA_FLAGS=""
MIPAV_LOG=/dev/null
MIPAV_PLUGINS=~/mipav/:~/mipav/plugins/:/usr/share/mipav/plugins/
MIPAV_MEMORY=256M
MIPAV_ARG="-pluginDir /usr/share/mipav/plugins/ "

ADD_CLASS=""


if [ -e /etc/mipav/mipav.conf ]; then
   . /etc/mipav/mipav.conf
fi


# options
while [ $# -gt 0 ]; do 
   case "$1" in 
      -a) MIPAV_JAVA=$2; echo "Using Java Binary $java "; shift;;
      -c) ADD_CLASS=":$CLASSPATH"; shift;;
      -h) echo "$USAGE"; exit 0;;
                -H) HEADLESS=1;;
      -l) echo "Log mode"; MIPAV_LOG=/tmp/`whoami`_mipav_$$.log; echo "file: $log";;
      -v) echo "Verbose mode"; MIPAV_LOG=-;;
      -V) echo "$VERSION_INFO"; exit 0;;
      *) MIPAV_ARG=`echo "$MIPAV_ARG" "$1"`;;
   esac
   shift
done


if [ "$MIPAV_LOG" == '-' ]; then
   echo ""
   echo " MIPAV_BASE    = $MIPAV_BASE"
   echo " MIPAV_JAR     = $MIPAV_JAR"
   echo " MIPAV_JAVA    = $MIPAV_JAVA"
   echo " MIPAV_LOG     = $MIPAV_LOG"
   echo " MIPAV_PLUGINS = $MIPAV_PLUGINS"
   echo " MIPAV_MEMORY  = $MIPAV_MEMORY"
   echo " MIPAV_ARG     = $MIPAV_ARG"
   echo ""
fi


# running mipav
cd $MIPAV_BASE


if [ "$HEADLESS" == 1 ]; then
        DISPLAY=:0
        export DISPLAY
        xhost +
        if [ ! -z $MIPAV_JAVA_FLAGS ]; then
                MIPAV_JAVA_FLAGS=" -Djava.awt.headless=true "${MIPAV_JAVA_FLAGS}
        else
                MIPAV_JAVA_FLAGS=" -Djava.awt.headless=true "
        fi
fi

# echo $MIPAV_JAVA $MIPAV_JAVA_FLAGS -classpath ./:/usr/share/java:$SYS_JAR:$MIPAV_JAR:$MIPAV_PLUGINS$ADD_CLASS  -Xmx$MIPAV_MEMORY -Xms$MIPAV_MEMORY StartMipav $MIPAV_ARG 
test "$MIPAV_LOG" != '-' && exec >$MIPAV_LOG
( $MIPAV_JAVA $MIPAV_JAVA_FLAGS -classpath ./:/usr/share/java:$SYS_JAR:$MIPAV_JAR:$MIPAV_PLUGINS$ADD_CLASS  -Xmx$MIPAV_MEMORY -Xms$MIPAV_MEMORY StartMipav $MIPAV_ARG 2>&1 )

RETVAL=$?


if [ "$HEADLESS" == 1 ]; then
        xhost -
fi


exit $RETVAL

