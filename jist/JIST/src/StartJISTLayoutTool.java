import edu.jhu.ece.iacl.jist.pipeline.gui.PipelineLayoutTool;


// TODO: Auto-generated Javadoc
/**
 * The Class StartJISTLayoutTool.
 */
public class StartJISTLayoutTool {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args){
		PipelineLayoutTool.main(args);
	}
}