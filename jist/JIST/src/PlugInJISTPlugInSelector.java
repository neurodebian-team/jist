import edu.jhu.ece.iacl.jist.pipeline.ProcessingApplication;
import edu.jhu.ece.iacl.jist.test.MipavTestHarness;

/**
 * The Class PlugInJISTPlugInSelector.
 */
public class PlugInJISTPlugInSelector extends ProcessingApplication{
	
	/** The Constant CATEGORY. */
	public static final String[] CATEGORY = {"JIST"};
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String args[]){	
		MipavTestHarness harness=new MipavTestHarness(args);
		ProcessingApplication plug=new PlugInJISTPlugInSelector();
		harness.invoke(plug);
	}
}
