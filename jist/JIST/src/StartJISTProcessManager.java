import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;


// TODO: Auto-generated Javadoc
/**
 * The Class StartJISTProcessManager.
 */
public class StartJISTProcessManager {
	
	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args){
//		System.out.println(getClass().getCanonicalName()+"\t"+"StartProcessManager: "+args);System.out.flush();
		ProcessManager.main(args);
	}
}
