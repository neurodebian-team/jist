/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

// TODO: Auto-generated Javadoc
/**
 * The Class TopologyRule2D.
 */
public abstract class TopologyRule2D {
	
	/**
	 * The topology rule.
	 */
	public enum Rule{
CONNECT_4,
CONNECT_8};
	
	/** The rule. */
	protected Rule rule;
	
	/** The grid points. */
	protected GridPoint2D[][] gridPoints;
	
	/** The rows. */
	protected int rows;

	/** The columns. */
	protected int cols;
	
	/**
	 * Instantiates a new topology rule2 d.
	 * 
	 * @param rule the rule
	 */
	public TopologyRule2D(Rule rule){
		this.rule=rule;
	}
	
	/**
	 * Sets the grid points.
	 * 
	 * @param gridPoints the new grid points
	 */
	public void setGridPoints(GridPoint2D[][] gridPoints){
		this.gridPoints=gridPoints;
		this.rows=gridPoints.length;
		this.cols=gridPoints[0].length;
	}
	
	/**
	 * Gets the grid point.
	 * 
	 * @param i the i
	 * @param j the j
	 * 
	 * @return the grid point
	 */
	public GridPoint2D getGridPoint(int i,int j){
		if(i<rows&&j<cols&&i>=0&&j>=0){
			return gridPoints[i][j];
		} else {
			return null;
		}
	}
	
	/**
	 * Apply rule.
	 * 
	 * @param pt the pt
	 * @param value the value
	 * 
	 * @return the double
	 */
	public abstract double applyRule(GridPoint2D pt,double value);
}
