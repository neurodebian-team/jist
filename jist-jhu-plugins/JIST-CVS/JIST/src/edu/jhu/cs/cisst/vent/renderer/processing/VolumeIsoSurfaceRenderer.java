/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.renderer.processing;

import java.awt.Color;

import javax.swing.SwingWorker;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PMatrix3D;
import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoSurfaceGenerator;
import edu.jhu.cs.cisst.jist.parameter.ParamColor;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamDoubleSliderInputView;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamIntegerSliderInputView;
import edu.jhu.cs.cisst.vent.VisualizationProcessing;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeIsoSurfaceRenderer.IsoSurfaceWorker;
import edu.jhu.cs.cisst.vent.structures.processing.PTriangleMesh;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VolumeIsoSurfaceRenderer.
 */
public class VolumeIsoSurfaceRenderer extends VolumeSliceRenderer3D {

	/**
	 * Instantiates a new volume iso surface renderer.
	 *
	 * @param img the img
	 * @param volToImageTransform the vol to image transform
	 * @param applet the applet
	 */
	public VolumeIsoSurfaceRenderer(ImageData img,
			PMatrix3D volToImageTransform, VisualizationProcessing applet) {
		super(img, volToImageTransform, applet);
		// TODO Auto-generated constructor stub
	}

	/** The curves. */
	protected PTriangleMesh[] surfs = null;

	/** The surface worker. */
	protected IsoSurfaceWorker surfaceWorker = null;

	/** The visible surface. */
	protected boolean visibleSurface = true;

	/** The iso level. */
	protected float isoLevel = 0;

	/** The iso-level parameter. */
	protected ParamFloat isolevelParam;

	/** The show surface parameter. */
	protected ParamBoolean showSurfaceParam;

	/** The flip normals. */
	protected boolean flipNormals = false;

	/** The visible param. */
	protected ParamBoolean gouraudParam, flipNormalsParam;

	/** The diffuse color param. */
	protected ParamColor diffuseColorParam;

	/** The diffuse color. */
	protected Color diffuseColor = new Color(Color.white.getRGB());

	/** The gouraud. */
	protected boolean gouraud = false;

	/**
	 * Sets the surface visible.
	 * 
	 * @param visible
	 *            the new surface visible
	 */
	public void setSurfaceVisible(boolean visible) {
		this.visibleSurface = visible;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer#setComponent
	 * (int)
	 */
	public void setComponent(int component) {
		if (this.component != component) {
			this.component = component;
			clearCache();
		}
	}

	/**
	 * Sets the iso level.
	 * 
	 * @param isoLevel
	 *            the new iso level
	 */
	public void setIsoLevel(float isoLevel) {
		if (isoLevel != this.isoLevel) {
			this.isoLevel = isoLevel;
			if (surfaceWorker != null && !surfaceWorker.isDone())
				surfaceWorker.cancel(false);
			surfaceWorker = new IsoSurfaceWorker();
			surfaceWorker.execute();

		}
	}

	/**
	 * Instantiates a new volume iso surface renderer.
	 *
	 * @param img the img
	 * @param volToImageTransform the vol to image transform
	 * @param applet the applet
	 */
	public VolumeIsoSurfaceRenderer(ImageDataFloat img,
			PMatrix3D volToImageTransform, VisualizationProcessing applet) {
		super(img, volToImageTransform, applet);
		surfs = new PTriangleMesh[components];
		surfaceWorker = new IsoSurfaceWorker();
		surfaceWorker.execute();
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D#draw()
	 */
	@Override
	public void draw() {

		applet.pushStyle();
		applet.pushMatrix();
		applet.applyMatrix(volToImageTransform);
		applet.tint(255, 255, 255, transparency * 255);
		applet.fill(255, 255, 255);
		applet.stroke(255, 153, 0, transparency * 255);
		if (showZplane) {
			PImage img = getImageZ(slice);
			float w = img.width;
			float h = img.height;
			PImage pimage = img;
			applet.beginShape(PApplet.QUADS);

			applet.texture(pimage);
			applet.vertex(0, 0, slice + 0.5f, 0, 0);
			applet.vertex(w, 0, slice + 0.5f, 1, 0);
			applet.vertex(w, h, slice + 0.5f, 1, 1);
			applet.vertex(0, h, slice + 0.5f, 0, 1);
			applet.endShape();
		}
		if (showYplane) {
			PImage img = getImageY(col);
			float w = img.width;
			float h = img.height;
			PImage pimage = img;
			applet.beginShape(PApplet.QUADS);

			applet.texture(pimage);
			applet.vertex(0, col + 0.5f, 0, 0, 0);
			applet.vertex(w, col + 0.5f, 0, 1, 0);
			applet.vertex(w, col + 0.5f, h, 1, 1);
			applet.vertex(0, col + 0.5f, h, 0, 1);
			applet.endShape();

		}
		if (showXplane) {
			PImage img = getImageX(row);
			float w = img.width;
			float h = img.height;
			PImage pimage = img;
			applet.beginShape(PApplet.QUADS);

			applet.texture(pimage);
			applet.vertex(row + 0.5f, 0, 0, 0, 0);
			applet.vertex(row + 0.5f, w, 0, 1, 0);
			applet.vertex(row + 0.5f, w, h, 1, 1);
			applet.vertex(row + 0.5f, 0, h, 0, 1);
			applet.endShape();

		}
		PTriangleMesh mesh = surfs[component];
		if (mesh != null && visibleSurface) {
			applet.noStroke();
			applet.smooth();
			applet.fill(diffuseColor.getRed(), diffuseColor.getGreen(),
					diffuseColor.getBlue(), transparency * 255.0f);
			mesh.draw(applet, gouraud, flipNormals);
		}
		applet.popMatrix();
		applet.popStyle();
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters
	 *            the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		visualizationParameters.setName("Volume - " + image.getName());
		visualizationParameters.add(rowParam = new ParamInteger("Row", 1, rows,
				1));
		rowParam.setInputView(new ParamIntegerSliderInputView(rowParam, 4));

		visualizationParameters.add(colParam = new ParamInteger("Column", 1,
				cols, 1));
		colParam.setInputView(new ParamIntegerSliderInputView(colParam, 4));

		visualizationParameters.add(sliceParam = new ParamInteger("Slice", 1,
				slices, 1));
		sliceParam.setInputView(new ParamIntegerSliderInputView(sliceParam, 4));
		visualizationParameters.add(componentParam = new ParamInteger(
				"Component", 1, Math.max(1, components), component));
		componentParam.setInputView(new ParamIntegerSliderInputView(
				componentParam, 4));
		visualizationParameters.add(contrastParam = new ParamFloat("Contrast",
				-5, 5, contrast));
		contrastParam.setInputView(new ParamDoubleSliderInputView(
				contrastParam, 4, false));
		visualizationParameters.add(brightnessParam = new ParamFloat(
				"Brightness", -5, 5, brightness));
		brightnessParam.setInputView(new ParamDoubleSliderInputView(
				brightnessParam, 4, false));
		visualizationParameters.add(transparencyParam = new ParamFloat(
				"Transparency", 0, 1, 1));
		transparencyParam.setInputView(new ParamDoubleSliderInputView(
				transparencyParam, 4, false));
		visualizationParameters.add(diffuseColorParam = new ParamColor(
				"Diffuse Color", diffuseColor));
		visualizationParameters.add(gouraudParam = new ParamBoolean(
				"Gouraud Shading", gouraud));
		visualizationParameters.add(isolevelParam = new ParamFloat("Iso-Level",
				-1E6f, 1E6f, isoLevel));
		visualizationParameters.add(flipNormalsParam = new ParamBoolean(
				"Flip Normals", flipNormals));
		visualizationParameters.add(showXplaneParam = new ParamBoolean(
				"Show X Plane", showXplane));
		visualizationParameters.add(showYplaneParam = new ParamBoolean(
				"Show Y Plane", showYplane));
		visualizationParameters.add(showZplaneParam = new ParamBoolean(
				"Show Z Plane", showZplane));
		visualizationParameters.add(showSurfaceParam = new ParamBoolean(
				"Surface Visible", visibleSurface));
	}

	/**
	 * Update.
	 * 
	 * @param model
	 *            the model
	 * @param view
	 *            the view
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
	 *      edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	@Override
	public void update(ParamModel model, ParamInputView view) {
		super.update(model, view);
		if (model == sliceParam) {
			setSlice(sliceParam.getInt() - 1);
		} else if (model == componentParam) {
			setComponent(componentParam.getInt() - 1);
		} else if (model == contrastParam) {
			setContrast(contrastParam.getFloat());
		} else if (model == brightnessParam) {
			setBrightness(brightnessParam.getFloat());
		} else if (model == transparencyParam) {
			setTransparency(transparencyParam.getFloat());
		} else if (model == showXplaneParam) {
			setShowXplane(showXplaneParam.getValue());
		} else if (model == showYplaneParam) {
			setShowYplane(showYplaneParam.getValue());
		} else if (model == showZplaneParam) {
			setShowZplane(showZplaneParam.getValue());
		} else if (model == rowParam) {
			setRow(rowParam.getInt() - 1);
		} else if (model == colParam) {
			setCol(colParam.getInt() - 1);
		} else if (model == isolevelParam) {
			setIsoLevel(isolevelParam.getFloat());
		} else if (model == showSurfaceParam) {
			setSurfaceVisible(showSurfaceParam.getValue());
		} else if (model == diffuseColorParam) {
			diffuseColor = diffuseColorParam.getValue();
		} else if (model == flipNormalsParam) {
			flipNormals = flipNormalsParam.getValue();
		} else if (model == gouraudParam) {
			gouraud = gouraudParam.getValue();
		}
	}

	/**
	 * Update visualization parameters.
	 * 
	 * @seeedu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer# 
	 *                                                                    updateVisualizationParameters
	 *                                                                    ()
	 */
	public void updateVisualizationParameters() {
		super.updateVisualizationParameters();
		setIsoLevel(isolevelParam.getFloat());
		setSurfaceVisible(showSurfaceParam.getValue());
		diffuseColor = diffuseColorParam.getValue();
		flipNormals = flipNormalsParam.getValue();
		transparency = transparencyParam.getFloat();
		gouraud = gouraudParam.getValue();
	}

	/**
	 * The Class IsoSurfaceWorker creates iso-surfaces for all slices.
	 */
	protected class IsoSurfaceWorker extends SwingWorker<Void, Void> {

		/**
		 * Do in background.
		 * 
		 * @return the void
		 * 
		 * @throws Exception
		 *             the exception
		 * 
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		@Override
		protected Void doInBackground() throws Exception {

			for (int i = 0; i < surfs.length; i++) {
				surfs[i] = null;
				if (this.isCancelled())
					break;
				try {
					IsoSurfaceGenerator isogen = new IsoSurfaceGenerator();
					isogen.setUseResolutions(false);
					System.out.println("CREATE ISO SURFACE " + i);
					surfs[i] = new PTriangleMesh(isogen.solve(
							(ImageDataFloat) image, isoLevel, i));
				} catch (Exception e) {
					System.err.println(e.getMessage());
					e.printStackTrace();
				}
			}

			return null;

		}

	}
}
