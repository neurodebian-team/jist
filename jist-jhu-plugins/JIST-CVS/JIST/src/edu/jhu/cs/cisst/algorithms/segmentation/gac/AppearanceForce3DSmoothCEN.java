/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import javax.vecmath.Vector3d;

import edu.jhu.cs.cisst.algorithms.util.DataOperations;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class AppearanceForce3DSmoothCEN.
 */
public class AppearanceForce3DSmoothCEN extends AppearanceForce3D {

	/** The fg p val. */
	float[][][][] fgPVal;

	/** The bg p val. */
	float[][][][] bgPVal;

	/** The fg v val. */
	float[][][] fgVVal;

	/** The bg v val. */
	float[][][] bgVVal;

	/** The time step. */
	protected double timeStep = 0.01;

	/** The max iterations. */
	protected int maxIterations = 1000;

	/** The foreground smoothness. */
	protected double foregroundSmoothness = 0.1;

	/** The background smoothness. */
	protected double backgroundSmoothness = 0.1;

	/** The convergence threshold. */
	protected double convergenceThreshold = 1E-4;

	/** The lambda. */
	protected double lambda = 0;

	/**
	 * Sets the lambda.
	 * 
	 * @param lambda
	 *            the new lambda
	 */
	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

	/**
	 * Sets the time step.
	 * 
	 * @param timeStep
	 *            the new time step
	 */
	public void setTimeStep(double timeStep) {
		this.timeStep = timeStep;
	}

	/**
	 * Sets the max iterations.
	 * 
	 * @param maxIterations
	 *            the new max iterations
	 */
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	/**
	 * Sets the foreground smoothness.
	 * 
	 * @param foregroundSmoothness
	 *            the new foreground smoothness
	 */
	public void setForegroundSmoothness(double foregroundSmoothness) {
		this.foregroundSmoothness = foregroundSmoothness;
	}

	/**
	 * Sets the background smoothness.
	 * 
	 * @param backgroundSmoothness
	 *            the new background smoothness
	 */
	public void setBackgroundSmoothness(double backgroundSmoothness) {
		this.backgroundSmoothness = backgroundSmoothness;
	}

	/**
	 * Sets the convergence threshold.
	 * 
	 * @param convergenceThreshold
	 *            the new convergence threshold
	 */
	public void setConvergenceThreshold(double convergenceThreshold) {
		this.convergenceThreshold = convergenceThreshold;
	}

	/**
	 * Instantiates a new appearance force2 d smooth cen.
	 * 
	 * @param image
	 *            the image
	 * @param normalizeIntensities
	 *            the normalize intensities
	 */
	public AppearanceForce3DSmoothCEN(ImageDataFloat image,
			boolean normalizeIntensities) {
		super(image, normalizeIntensities);
		fgPVal = new float[rows][cols][slices][3];
		bgPVal = new float[rows][cols][slices][3];
		fgVVal = new float[rows][cols][slices];
		bgVVal = new float[rows][cols][slices];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce3D#update
	 * ()
	 */
	public void update() {
		float[][][] heavisideImage = new float[rows][cols][slices];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					heavisideImage[i][j][k] = (float) getHeavisideValue(i, j, k);
				}
			}
		}
		float[][][] fgPDiv = new float[rows][cols][slices];
		float[][][] bgPDiv = new float[rows][cols][slices];
		float[][][] fgImage = new float[rows][cols][slices];
		float[][][] bgImage = new float[rows][cols][slices];
		// Setup linear solver
		double val, divp, heavy, mag, fg, bg, fgv, bgv;
		Vector3d v;
		float[] pval;
		double maxError = 0;
		for (int n = 0; n < maxIterations; n++) {
			maxError = 0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						val = intensityImage[i][j][k];
						fgv = fgVVal[i][j][k];
						bgv = bgVVal[i][j][k];
						heavy = heavisideImage[i][j][k];
						divp = DataOperations.divergence(fgPVal, i, j, k);
						fgPDiv[i][j][k] = (float) (divp - (val - fgv)
								/ foregroundSmoothness);
						fg = val - fgv - foregroundSmoothness * divp;
						divp = DataOperations.divergence(bgPVal, i, j, k);
						bgPDiv[i][j][k] = (float) (divp - (val - bgv)
								/ backgroundSmoothness);
						bg = val - bgv - backgroundSmoothness * divp;
						maxError = Math.max(maxError, Math.max(Math.abs(fg
								- fgImage[i][j][k]), Math.abs(bg
								- bgImage[i][j][k])));

						if (val - fg >= foregroundSmoothness * lambda) {
							fgVVal[i][j][k] = (float) (val - fg - foregroundSmoothness
									* lambda);
						} else if (val - fg <= -foregroundSmoothness * lambda) {
							fgVVal[i][j][k] = (float) (val - fg + foregroundSmoothness
									* lambda);
						} else {
							fgVVal[i][j][k] = 0;
						}
						if (val - bg >= backgroundSmoothness * lambda) {
							bgVVal[i][j][k] = (float) (val - bg - backgroundSmoothness
									* lambda);
						} else if (val - bg <= -backgroundSmoothness * lambda) {
							bgVVal[i][j][k] = (float) (val - bg + backgroundSmoothness
									* lambda);
						} else {
							bgVVal[i][j][k] = 0;
						}
						fgImage[i][j][k] = (float) fg;
						bgImage[i][j][k] = (float) bg;
					}
				}
			}
			if (n % 10 == 0)
				System.out.println(n + ") " + maxError);
			if (maxError <= convergenceThreshold)
				break;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						heavy = heavisideImage[i][j][k];
						v = DataOperations.gradient(fgPDiv, i, j, k);
						mag = v.length();
						pval = fgPVal[i][j][k];
						fgPVal[i][j][k][0] = (float) ((pval[0] + timeStep * v.x) / (1 + timeStep
								* mag / (1 - heavy)));
						fgPVal[i][j][k][1] = (float) ((pval[1] + timeStep * v.y) / (1 + timeStep
								* mag / (1 - heavy)));
						fgPVal[i][j][k][2] = (float) ((pval[2] + timeStep * v.z) / (1 + timeStep
								* mag / (1 - heavy)));
						v = DataOperations.gradient(bgPDiv, i, j, k);
						mag = v.length();
						pval = bgPVal[i][j][k];
						bgPVal[i][j][k][0] = (float) ((pval[0] + timeStep * v.x) / (1 + timeStep
								* mag / heavy));
						bgPVal[i][j][k][1] = (float) ((pval[1] + timeStep * v.y) / (1 + timeStep
								* mag / heavy));
						bgPVal[i][j][k][2] = (float) ((pval[2] + timeStep * v.z) / (1 + timeStep
								* mag / heavy));

					}
				}
			}
		}

		float[][][] intensityEstimateImage = imageEstimate.toArray3d();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double fgGradMag = DataOperations.gradientMagnitude(
							fgImage, i, j, k);
					double bgGradMag = DataOperations.gradientMagnitude(
							bgImage, i, j, k);

					heavy = heavisideImage[i][j][k];
					val = intensityImage[i][j][k];
					fg = fgImage[i][j][k];
					bg = bgImage[i][j][k];
					intensityEstimateImage[i][j][k] = (float) ((1 - heavy) * fg + bg
							* heavy);
					pressureForce[i][j][k] = (float) (heavy * (-fgWeight
							* ((0.5) * (val - fg) * (val - fg) + foregroundSmoothness
									* fgGradMag) + bgWeight
							* ((0.5) * (val - bg) * (val - bg) + backgroundSmoothness
									* bgGradMag)));
				}
			}
		}
	}

}
