/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.solver;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.FMG;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;

// TODO: Auto-generated Javadoc
/**
 * The interface to objects that exactly solve a linear elliptic partial differential equation (PDE) on a regular cubic
 * grid in 3D.
 * <p>
 * Part of the (Full) Multigrid ((F)MG) algorithm is the exact solving of the PDE at the coarsest grid level. This step
 * must be efficient, because it is performed several times during the course of the MG algorithm and once at the
 * beginning of the FMG algorithm.
 * <p>
 * Not all algorithms suited for exactly solving a PDE work on all grid sizes, so it is important that the coarsest
 * grid of the (F)MG algorithm is of a size that is compatible with the solving algorithm that is implemented by
 * implementing this interface.
 * 
 * @see FMG
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public interface Solver {
     
     /**
      * exactly solve a linear elliptic PDE on a regular cubic grid in 3D given an approximation to the solution and the
      * right hand side (source term) of the PDE
      * 
      * If Ax = f defines the PDE, this method solves for the true solution x given an approximation u of x and the
      * right hand side f.
      * 
      * @param u an approximation to the true solution. Even if the solution algorithm doesn't need to start from an
      * approximation this argument is needed because it serves as a prototype for the construction of the
      * solution grid.
      * @param f the right hand side of the PDE
      * 
      * @return the exact solution x
      * 
      * @see PDE
      */
     public BoundaryGrid solve(ConstBoundaryGrid u, ConstNoBoundaryGrid f);
}
