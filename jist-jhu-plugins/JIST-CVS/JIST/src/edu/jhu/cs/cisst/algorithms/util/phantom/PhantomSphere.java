/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.util.phantom;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

// TODO: Auto-generated Javadoc
/**
 * The Class PhantomSphere.
 */
public class PhantomSphere extends PhantomSimulator3D {
	
	/** The center. */
	protected Point3d center;
	
	/** The radius. */
	protected double radius;

	/**
	 * Sets the center.
	 * 
	 * @param center the new center
	 */
	public void setCenter(Point3d center) {
		this.center = center;
	}

	/**
	 * Sets the radius.
	 * 
	 * @param radius the new radius
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * Instantiates a new phantom sphere.
	 * 
	 * @param dims the dims
	 */
	public PhantomSphere(Point3i dims) {
		super(dims);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.algorithms.util.phantom.PhantomSimulator3D#solve()
	 */
	public void solve() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double x = (2 * i / (double) rows - 1);
					double y = (2 * j / (double) cols - 1);
					double z = (2 * k / (double) slices - 1);
					double r = Math.sqrt((x - center.x) * (x - center.x)
							+ (y - center.y) * (y - center.y) + (z - center.z)
							* (z - center.z));
					levelset.set(i, j, k, r - radius);
				}
			}
		}
		levelset.setName("sphere_level");
		image.setName("sphere");
		finish();
	}
}
