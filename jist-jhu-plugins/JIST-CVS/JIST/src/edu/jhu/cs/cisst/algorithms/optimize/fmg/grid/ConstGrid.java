/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.grid;


// TODO: Auto-generated Javadoc
/**
 * Interface to the constant methods of class Grid.
 * 
 * @see Grid
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public interface ConstGrid extends Cloneable {
     
     /**
      * returns the element at the specified position.
      * 
      * @param x the x index of the element (0 <= x < size())
      * @param y the y index of the element (0 <= y < size())
      * @param z the z index of the element (0 <= z < size())
      * 
      * @return  the element (a double value) at the specified position
      */
     public double get(int x, int y, int z);
     
     /**
      * Set the value for a grid node.
      * 
      * @param sx the dimension in X
      * @param sy the dimension in Y
      * @param sz the dimension in Z  
      * @param level the resolution level 
      * @param value the value
      */
     public void set(int x,int y,int z,double value);
    
     
     /**
      * Gets the rows.
      * 
      * @return the rows
      */
     public int getRows();
     
     /**
      * Gets the cols.
      * 
      * @return the cols
      */
     public int getCols();
     
     /**
      * Gets the slices.
      * 
      * @return the slices
      */
     public int getSlices();
     
     /**
      * determines if the specified element is in the interior of the grid.
      * 
      * @param x the x index of the element
      * @param y the y index of the element
      * @param z the z index of the element
      * 
      * @return  true if the element is in the interior of the grid, false otherwise
      */
     public boolean isInterior(int x, int y, int z);

     /**
      * determines if the specified element is at the boundary of the grid.
      * 
      * @param x the x index of the element
      * @param y the y index of the element
      * @param z the z index of the element
      * 
      * @return  true if the element is at the boundary of the grid, false otherwise
      */
     public boolean isBoundary(int x, int y, int z);

     /**
      * create a new grid with exactly the same handling of the boundary (i.e. of the same tzpe as this object) but of
      * an arbitrary size and set all interior elements to a given value.
      * 
      * @param value the value to which all interior grid elements are to be set
      * @param sx the sx
      * @param sy the sy
      * @param sz the sz
      * @param level the level
      * 
      * @return the newly constructed grid
      */
     public Grid newInstance(int sx,int sy,int sz, int level,double value);
     
     /**
      * Get the resolution level.
      * 
      * @return level
      */
     public int getLevel();
     
     /**
      * Set the resolution level.
      * 
      * @param level the level
      */
     public void setLevel(int level);
     
     /**
      * clone this grid.
      * 
      * @return the const grid
      */
     public ConstGrid clone();

     /**
      * create a new grid that is the sum of this grid and the given grid.
      * 
      * Only the interiors of the two grids are added. The types of the two grids must be identical and the handling of
      * the boundary is copied from this grid.
      * 
      * @param grid the grid to add to this grid
      * 
      * @return the newly constructed grid that is the sum of the grids
      */
     public Grid add(ConstGrid grid);
}
