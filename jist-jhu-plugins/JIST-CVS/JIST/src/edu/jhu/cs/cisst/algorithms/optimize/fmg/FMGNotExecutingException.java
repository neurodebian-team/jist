/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;

// TODO: Auto-generated Javadoc
/**
 * Exception class that is thrown if FMG.waitForResult() is called although the FMG algorithm is currently not executing
 * in a thread.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public class FMGNotExecutingException extends Exception {
     
     /**
      * construct with no particular message.
      */
     public FMGNotExecutingException() {};
     
     /**
      * construct with the given message.
      * 
      * @param msg the message
      */
     public FMGNotExecutingException(String msg) {super(msg);};
}
