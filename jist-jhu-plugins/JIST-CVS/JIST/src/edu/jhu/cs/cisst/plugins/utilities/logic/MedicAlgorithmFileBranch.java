/*
 *
 */
package edu.jhu.cs.cisst.plugins.utilities.logic;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmFileBranch extends ProcessingAlgorithm{
	private ParamFile fileTrue;
	private ParamFile fileFalse;
	private ParamFile outFile;
	private ParamBoolean branchParam;
	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(outFile=new ParamFile("File"));
		inputParams.add(branchParam=new ParamBoolean("Assertion",true));
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("File Branch");
		inputParams.setName("file_branch");
		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription("An if/then/else condition for forwarding a file.");
		info.add(CommonAuthors.blakeLucas);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fileTrue=new ParamFile("File for True"));
		outputParams.add(fileFalse=new ParamFile("File for False"));
		fileTrue.setMandatory(false);
		fileFalse.setMandatory(false);
	}
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		if(branchParam.getValue()){
			fileTrue.setValue(outFile.getValue());
		} else {
			fileFalse.setValue(outFile.getValue());
		}
	}
}
