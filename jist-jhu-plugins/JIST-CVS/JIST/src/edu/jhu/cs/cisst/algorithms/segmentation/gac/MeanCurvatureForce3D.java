/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class MeanCurvatureForce3D moves the level set according to mean
 * curvature. Mean curvature is defined as the divergence of the level set
 * normal. For numerical stability, the magnitude of the Mean curvature is
 * bounded from above.
 */
public class MeanCurvatureForce3D extends ActiveContourForce3D {

	/** The max curvature force. */
	protected double maxCurvatureForce = 10;

	/**
	 * Gets the max curvature force.
	 * 
	 * @return the max curvature force
	 */
	public double getMaxCurvatureForce() {
		return maxCurvatureForce;
	}

	/**
	 * Sets the max curvature force.
	 * 
	 * @param maxCurvatureForce
	 *            the new max curvature force
	 */
	public void setMaxCurvatureForce(double maxCurvatureForce) {
		this.maxCurvatureForce = maxCurvatureForce;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D#evaluate(int,
	 *      int)
	 */
	public double evaluate(int i, int j, int k) {

		GridPoint3D p111 = getGridPoint(i, j, k);
		GridPoint3D p010 = getGridPoint(i - 1, j, k - 1);
		GridPoint3D p120 = getGridPoint(i, j + 1, k - 1);
		GridPoint3D p110 = getGridPoint(i, j, k - 1);
		GridPoint3D p100 = getGridPoint(i, j - 1, k - 1);
		GridPoint3D p210 = getGridPoint(i + 1, j, k - 1);		
		GridPoint3D p001 = getGridPoint(i - 1, j - 1, k);
		GridPoint3D p011 = getGridPoint(i - 1, j, k);
		GridPoint3D p101 = getGridPoint(i, j - 1, k);
		GridPoint3D p211 = getGridPoint(i + 1, j, k);
		GridPoint3D p201 = getGridPoint(i + 1, j - 1, k);
		GridPoint3D p221 = getGridPoint(i + 1, j + 1, k);
		GridPoint3D p021 = getGridPoint(i - 1, j + 1, k);
		GridPoint3D p121 = getGridPoint(i, j + 1, k);
		GridPoint3D p012 = getGridPoint(i - 1, j, k + 1);
		GridPoint3D p122 = getGridPoint(i, j + 1, k + 1);
		GridPoint3D p112 = getGridPoint(i, j, k + 1);
		GridPoint3D p102 = getGridPoint(i, j - 1, k + 1);
		GridPoint3D p212 = getGridPoint(i + 1, j, k + 1);


		double v111 = p111.value;
		double v011 = ((p011 != null) ? p011.value : v111);
		double v121 = ((p121 != null) ? p121.value : v111);
		double v101 = ((p101 != null) ? p101.value : v111);
		double v211 = ((p211 != null) ? p211.value : v111);

		double v012 = ((p012 != null) ? p012.value : v111);
		double v122 = ((p122 != null) ? p122.value : v111);
		double v102 = ((p102 != null) ? p102.value : v111);
		double v212 = ((p212 != null) ? p212.value : v111);

		double v010 = ((p010 != null) ? p010.value : v111);
		double v120 = ((p120 != null) ? p120.value : v111);
		double v100 = ((p100 != null) ? p100.value : v111);
		double v210 = ((p210 != null) ? p210.value : v111);

		double v001 = ((p001 != null) ? p001.value : v111);
		double v021 = ((p021 != null) ? p021.value : v111);
		double v201 = ((p201 != null) ? p201.value : v111);
		double v221 = ((p221 != null) ? p221.value : v111);

		double v110 = ((p110 != null) ? p110.value : v111);
		double v112 = ((p112 != null) ? p112.value : v111);

		double DxCtr = 0.5 * (v211 - v011);
		double DyCtr = 0.5 * (v121 - v101);
		double DzCtr = 0.5 * (v112 - v110);
		double DxxCtr = v211 - v111 - v111 + v011;
		double DyyCtr = v121 - v111 - v111 + v101;
		double DzzCtr = v112 - v111 - v111 + v110;
		double DxyCtr = (v221 - v021 - v201 + v001) * 0.25;
		double DxzCtr = (v212 - v012 - v210 + v010) * 0.25;
		double DyzCtr = (v122 - v102 - v120 + v100) * 0.25;

		double numer = 0.5 * (
				(DyyCtr+DzzCtr)*DxCtr*DxCtr
				+(DxxCtr+DzzCtr)*DyCtr*DyCtr
				+(DxxCtr+DyyCtr)*DzCtr*DzCtr
				-2*DxCtr*DyCtr*DxyCtr
				-2*DxCtr*DzCtr*DxzCtr
				-2*DyCtr*DzCtr*DyzCtr);
		double denom = DxCtr * DxCtr + DyCtr * DyCtr+ DzCtr * DzCtr;
		double kappa;
		if (Math.abs(denom) > 1E-5) {
			kappa = weight * numer / denom;
		} else {
			kappa = weight * numer * Math.signum(denom) * 1E5;
		}
		if (kappa < -maxCurvatureForce) {
			kappa = -maxCurvatureForce;
		} else if (kappa > maxCurvatureForce) {
			kappa = maxCurvatureForce;
		}
		return kappa;
	}
}
