/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.visualization;

import java.util.List;

import processing.core.PMatrix3D;

import Jama.Matrix;

import edu.jhu.cs.cisst.vent.Visualization;
import edu.jhu.cs.cisst.vent.VisualizationApplication;
import edu.jhu.cs.cisst.vent.VisualizationPlugIn;
import edu.jhu.cs.cisst.vent.widgets.VisualizationImage2D;
import edu.jhu.cs.cisst.vent.widgets.VisualizationImage3D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInVisualizeVectorField3D.
 */
public class PlugInVisualizeImage3D extends ProcessingAlgorithm implements
		VisualizationPlugIn {
	
	/** The orig image param. */
	protected ParamVolumeCollection origImageParam;
	
	/** The vol transform param. */
	protected ParamMatrix volTransformParam;

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(origImageParam = new ParamVolumeCollection(
				"Images"));
		inputParams.add(volTransformParam = new ParamMatrix(
				"Volume to Image Transform", Matrix.identity(4, 4)));
		inputParams.setName("vis_image_3d");
		inputParams.setLabel("Visualize Image 3D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Visualization");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setDescription("Visualizes image in 3d.");
		info.setVersion(VisualizationImage2D.getVersion());
		info.setStatus(DevelopmentStatus.RC);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		VisualizationApplication app = new VisualizationApplication(this,
				createVisualization());

		app.runAndWait();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationPlugIn#createVisualization()
	 */
	@Override
	public Visualization createVisualization() {
		List<ImageData> images = origImageParam.getImageDataList();
		int width = 0;
		int height = 0;
		for (ImageData img : images) {
			width = Math.max(width, img.getRows());
			height = Math.max(height, img.getCols());
		}
		double[] mat = volTransformParam.getValue().getRowPackedCopy();
		PMatrix3D m = new PMatrix3D((float) mat[0], (float) mat[1],
				(float) mat[2], (float) mat[3], (float) mat[4], (float) mat[5],
				(float) mat[6], (float) mat[7], (float) mat[8], (float) mat[9],
				(float) mat[10], (float) mat[11], (float) mat[12],
				(float) mat[13], (float) mat[14], (float) mat[15]);
		VisualizationImage3D visual = new VisualizationImage3D(1024, 768, m);
		for (ImageData img : images) {
			visual.addImage(img);
		}
		return visual;
	}

}
