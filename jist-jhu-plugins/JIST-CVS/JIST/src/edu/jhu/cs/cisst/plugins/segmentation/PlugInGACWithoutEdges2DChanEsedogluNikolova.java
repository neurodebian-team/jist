/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.segmentation;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce2DSmoothCEN;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContour2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContourWithoutEdges2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.MeanCurvatureForce2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyPreservationRule2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInGACWithoutEdges2DChanEsedogluNikolova.
 */
public class PlugInGACWithoutEdges2DChanEsedogluNikolova extends
		ProcessingAlgorithm {
	
	/** The reference image param. */
	protected ParamVolume referenceImageParam;
	
	/** The initial image param. */
	protected ParamVolume initialImageParam;
	
	/** The verbose segmented image param. */
	protected ParamVolume verboseSegmentedImageParam;
	
	/** The verbose estimated image param. */
	protected ParamVolume verboseEstimatedImageParam;
	
	/** The segmented image param. */
	protected ParamVolume segmentedImageParam;
	
	/** The fg pressure weight param. */
	protected ParamDouble fgPressureWeightParam;
	
	/** The bg pressure weight param. */
	protected ParamDouble bgPressureWeightParam;
	
	/** The curvature weight param. */
	protected ParamDouble curvatureWeightParam;
	
	/** The dice threshold param. */
	protected ParamDouble diceThresholdParam;
	
	/** The outer iters param. */
	protected ParamInteger outerItersParam;
	
	/** The inner iters param. */
	protected ParamInteger innerItersParam;
	
	/** The verbose param. */
	protected ParamBoolean verboseParam;
	
	/** The normalize param. */
	protected ParamBoolean normalizeParam;
	
	/** The estimated image param. */
	protected ParamVolume estimatedImageParam;
	
	/** The heaviside func param. */
	protected ParamOption heavisideFuncParam;
	
	/** The heaviside fuzz param. */
	protected ParamDouble heavisideFuzzParam;
	
	/** The time step param. */
	protected ParamDouble timeStepParam;
	
	/** The foreground weight param. */
	protected ParamDouble foregroundWeightParam;
	
	/** The background weight param. */
	protected ParamDouble backgroundWeightParam;
	
	/** The max iterations param. */
	protected ParamInteger maxIterationsParam;
	
	/** The epsilon param. */
	protected ParamDouble epsilonParam;
	
	/** The lambda param. */
	protected ParamDouble lambdaParam;
	
	/** The connectivity rule param. */
	protected ParamOption connectivityRuleParam;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		ParamCollection dataFrame = new ParamCollection("Data");
		dataFrame.add(referenceImageParam = new ParamVolume("Reference Image",
				null, -1, -1, 1, 1));
		dataFrame.add(initialImageParam = new ParamVolume(
				"Initial Levelset Image", null, -1, -1, 1, 1));
		String[] ruleStrings=new String[3];
		ruleStrings[0]="NONE";
		ruleStrings[1]="Connected (4,8)";
		ruleStrings[2]="Connected (8,4)";
		dataFrame.add(connectivityRuleParam=new ParamOption("Connectivity Rule",ruleStrings));
		dataFrame.add(normalizeParam = new ParamBoolean(
				"Normalize Intensities", true));
		dataFrame.add(verboseParam = new ParamBoolean("Enable Verbose Output",
				false));

		ParamCollection paramFrame = new ParamCollection("Parameters");
		paramFrame.add(lambdaParam = new ParamDouble("Lambda", 0, 1E6, 1));
		paramFrame.add(foregroundWeightParam = new ParamDouble(
				"Foreground Smoothness", 10));
		paramFrame.add(backgroundWeightParam = new ParamDouble(
				"Background Smoothness", 10));

		paramFrame.add(fgPressureWeightParam = new ParamDouble(
				"Foreground Pressure Weight", 0.3));
		paramFrame.add(bgPressureWeightParam = new ParamDouble(
				"Background Pressure Weight", 0.3));
		paramFrame.add(curvatureWeightParam = new ParamDouble(
				"Curvature Weight", 0.1));
		ParamCollection solverFrame = new ParamCollection("Solver");
		solverFrame.add(heavisideFuncParam = new ParamOption(
				"Heaviside Function", AppearanceForce2D.heavisideNames));
		solverFrame.add(heavisideFuzzParam = new ParamDouble(
				"Heaviside Fuzziness", 0, 1E10, 0.5));
		solverFrame.add(outerItersParam = new ParamInteger(
				"Max Outer Iterations", 0, 1000000, 75));
		solverFrame.add(innerItersParam = new ParamInteger(
				"Max Inner Iterations", 1, 1000000, 100));
		solverFrame.add(diceThresholdParam = new ParamDouble("Dice Threshold",
				0, 1, 0.9999));
		solverFrame.add(maxIterationsParam = new ParamInteger(
				"Maximum Iterations", 100));
		solverFrame.add(timeStepParam = new ParamDouble("Time Step", 0, 1E10,
				1.0 / 8));
		solverFrame.add(epsilonParam = new ParamDouble("Convergence Threshold",
				0, 1E10, 1E-4));
		inputParams.add(dataFrame);
		inputParams.add(paramFrame);
		inputParams.add(solverFrame);
		inputParams.setLabel("Active Contour Without Edges 2D - CEN");
		inputParams.setName("acwe2d_cen");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setDescription("Segments a 2D image using a geodesic active contour without edges. "
						+ "Intensities are modeled with a Chan-Esedolgu-Nikolova (CEN) functional that peanilizes the gradient magnitude of the estimated intensities, also known as Total Variation (TV) minimization. The CEN model is slightly different from the ROF model in that it uses an L1 penalty for the data term instead of L2. The L1 is approximated with a convex regularization so it can be easiliy optimized.");
		info
				.add(new Citation(
						"Chan, T., S. Esedoglu, et al. Algorithms for finding global minimizers of image segmentation and denoising models.ALGORITHMS 66(5): 1632-1648."));
		info
				.add(new Citation(
						"Bresson, X., S. Esedoglu, et al. (2007). Fast global minimization of the active contour/snake model.Journal of Mathematical Imaging and Vision 28(2): 151-167."));
		info.add(new Citation("DOI:10.1023/A:1008036829907"));
		info.add(new Citation("DOI:10.1016/0021-9991(88)90002-2"));
		info.add(new Citation("DOI:10.1109/TPAMI.2003.1201824"));
		info
				.add(new Citation(
						"Sethian, J. (1999). Level set methods and fast marching methods, Cambridge university press Cambridge."));
		info.setVersion(GeodesicActiveContour2D.getVersion());
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setStatus(DevelopmentStatus.Release);
		inputParams.setPackage("CISST");
		inputParams.setCategory("Segmentation");

	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(segmentedImageParam = new ParamVolume(
				"Level Set Image"));
		outputParams.add(estimatedImageParam = new ParamVolume(
				"Estimated Image"));
		outputParams.add(verboseSegmentedImageParam = new ParamVolume(
				"Verbose Level Set Image"));
		outputParams.add(verboseEstimatedImageParam = new ParamVolume(
				"Verbose Estimated Image"));
		verboseEstimatedImageParam.setMandatory(false);
		verboseSegmentedImageParam.setMandatory(false);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ImageDataFloat initIm = new ImageDataFloat(initialImageParam
				.getImageData());
		ImageData refImage = (referenceImageParam.getImageData() != null) ? referenceImageParam
				.getImageData()
				: initIm;
		GeodesicActiveContourWithoutEdges2D activeContour = new GeodesicActiveContourWithoutEdges2D(
				refImage);
		monitor.observe(activeContour);
		activeContour.setDiceThreshold(diceThresholdParam.getDouble());
		MeanCurvatureForce2D curvatureForce = new MeanCurvatureForce2D();
		curvatureForce.setWeight(curvatureWeightParam.getDouble());
		activeContour.add(curvatureForce);
		AppearanceForce2DSmoothCEN af = null;
		af = new AppearanceForce2DSmoothCEN(new ImageDataFloat(
				referenceImageParam.getImageData()), normalizeParam.getValue());

		af.setHeaviside(AppearanceForce2D.Heaviside.values()[heavisideFuncParam
				.getIndex()]);
		af.setLambda(lambdaParam.getDouble());
		af.setConvergenceThreshold(epsilonParam.getDouble());
		af.setMaxIterations(maxIterationsParam.getInt());
		af.setTimeStep(timeStepParam.getDouble());
		af.setFuzziness(heavisideFuzzParam.getDouble());
		af.setBackgroundSmoothness(backgroundWeightParam.getDouble());
		af.setForegroundSmoothness(foregroundWeightParam.getDouble());
		af.setForegroundWeight(fgPressureWeightParam.getDouble());
		af.setBackgroundWeight(bgPressureWeightParam.getDouble());
		activeContour.add(af);
		int innerIters = innerItersParam.getInt();
		int outerIters = outerItersParam.getInt();
		activeContour.setInnerIterations(innerIters);
		activeContour.setOuterIterations(outerIters);
		if(connectivityRuleParam.getIndex()>0){
			activeContour.attachTopologyRule(new TopologyPreservationRule2D(TopologyRule2D.Rule.values()[connectivityRuleParam.getIndex()-1]));
		}
		if (verboseParam.getValue()) {
			segmentedImageParam.setValue(activeContour.solveVerbose(initIm));
			verboseSegmentedImageParam.setValue(activeContour
					.getVerboseLevelSet());
			estimatedImageParam.setValue(activeContour.getEstimatedImage());
			verboseEstimatedImageParam.setValue(activeContour
					.getVerboseEstimatedImage());
		} else {
			segmentedImageParam.setValue(activeContour.solve(initIm));
			estimatedImageParam.setValue(activeContour.getEstimatedImage());
		}
	}

}
