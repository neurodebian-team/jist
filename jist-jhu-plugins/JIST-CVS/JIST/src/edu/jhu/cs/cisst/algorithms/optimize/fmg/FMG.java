/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;

import java.util.*;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.NoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator.Interpolator;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor.Restrictor;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.Smoother;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.solver.Solver;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * A class that implements the Full Multigrid (FMG) algorithm for the solution
 * of a linear elliptic partial differential equation (PDE) on a regular cubic
 * grid in 3D.
 * <p>
 * The FMG algorithm solves a PDE by discretizing it on several grids of
 * different coarseness corresponding to different levels. The grids at
 * different levels are positioned in such a way that the corners of their
 * boundaries are superimposed. This has the effect that every new grid level
 * shares some grid elements with the next coarser grid and introduces new grid
 * elements that lie exactly between the grid elements of the next coarser grid.
 * <p>
 * The FMG algorithm starts by solving the PDE on the coarsest grid and then
 * decends to the finest grid by interpolating stepwise from a coarse grid to
 * the next finer grid. At each grid level (except the coarsest) the FMG
 * algorithm employs the Multigrid (MG) algorithm possibly several times to
 * improve the solution on this level.
 * <p>
 * The MG algorithm improves an approximate solution of the PDE on a certain
 * grid level by pre-smoothing the solution with a conventional relaxation
 * algorithm, restricting the problem (in principle) to the next coarser grid
 * level, obtaining an improved solution to this coarse grid problem by
 * (recursively) calling the MG algorithm possibly several times at that level,
 * interpolating the solution at the coarse grid to the fine grid thereby
 * correcting the fine grid solution, and finally post-smoothing the solution.
 * <P>
 * There have been several modifications made to this solver to make it more
 * flexible:<BR>
 * 1) Grids can now have arbitrary dimensions instead of being a cubic volume of
 * power 2.<BR>
 * 2) The resolution levels can be arbitrary and not necessarily powers of 2.<BR>
 * 3) The coarsest level at which the PDE is solved does not have to be 3x3x3,
 * but the PDE must specify its own direct solver.<BR>
 *- Blake
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public class FMG extends Observable {

	/**
	 * construct an FMG object that will be able to solve a linear elliptic PDE
	 * using the FMG algorithm on a regular cubic grid in 3D.
	 * <p>
	 * The FMG algorithm employs several sub-algorithms that must be specified
	 * as arguments to the constructor and can not be changed afterwards. Thus a
	 * specific FMG object will allways employ these sub-algorithms. However,
	 * the details of the FMG algorithm itself must be specified when calling
	 * the fmg() method and can thus be changed with every invocation of this
	 * method.
	 * 
	 * @param pde
	 *            an object representing the PDE to solve
	 * @param smoother
	 *            an object encapsulating the smoothing algorithm to use
	 * @param restrictor
	 *            an object encapsulating the restriction algorithm to use
	 * @param interpolator
	 *            an object encapsulating the interpolation method to use
	 * @param solver
	 *            an object encapsulating the algorithm to use for solving the
	 *            PDE on the coarsest level
	 * @param solutionPrototype
	 *            a prototype object from which the exact type of the desired
	 *            solution will be deduced. By selecting a proper subclass of
	 *            BoundaryGrid for this prototype object, the boundary
	 *            conditions that the solution must satisfy are selected. The
	 *            state of the prototype object is irrelevant because only its
	 *            type is used.
	 * @param correctionPrototype
	 *            the correction prototype
	 * @param resolutions
	 *            the resolutions
	 */
	public FMG(PDE pde, Smoother smoother, Restrictor restrictor,
			Interpolator interpolator, Solver solver,
			ConstBoundaryGrid solutionPrototype,
			ConstBoundaryGrid correctionPrototype,
			SolverResolutionLevels resolutions) {
		Contract.pre(pde != null && smoother != null && restrictor != null
				&& interpolator != null && solver != null
				&& solutionPrototype != null && correctionPrototype != null,
				"all objects not null-objects");

		this.pde = pde;
		this.smoother = smoother;
		this.restrictor = restrictor;
		this.interpolator = interpolator;
		this.solver = solver;
		this.uPrototype = solutionPrototype;
		this.resolutions = resolutions;
		this.vPrototype = correctionPrototype;
	}

	/**
	 * starts the FMG Algorithm with the given parameters in a separate thread
	 * and returns immediately.
	 * <p>
	 * At any time there can be only one instance of the FMG algorithm
	 * executing. Hence if this method is called while the FMG algorithm is
	 * already running, an exception is thrown.
	 * <p>
	 * There are two independent ways of receiving the result of the FMG
	 * algorithm that this method produces. The first is to call waitForResult()
	 * which blocks the thread that is calling it until the result is available
	 * and then returns it. The second is by registering one or more
	 * java.util.Observer objects with this object (via addObserver()) which
	 * will be notified as soon as the result is available (their update()
	 * method will be called with the result of type BoundaryGrid as the second
	 * (the arg) argument).
	 * 
	 * @param numPresmooth
	 *            the number of pre-smoothing steps to perform ( > 0)
	 * @param numPostsmooth
	 *            the number of post-smoothing steps to perform ( >= 0)
	 * @param cyclingStrategy
	 *            the cycling strategy to use ( > 0). A value of 1 results in
	 *            V-cycles, a value of 2 in W-cycles. This gives the number of
	 *            times the MG algorithm recursively calls itself to solve the
	 *            coarse grid problem.
	 * @param numMultiGrid
	 *            the number of invocations of the MG algorithm per level of the
	 *            FMG algorithm ( > 0)
	 * 
	 * @throws FMGAlreadyExecutingException
	 *             the FMG already executing exception
	 * 
	 * @see java.util.Observer
	 * @exception FMGAlreadyExecutingException
	 *                fmg() has already been called for this object and is still
	 *                executing
	 */
	public synchronized void solveParallel(int numPresmooth, int numPostsmooth,
			int cyclingStrategy, int numMultiGrid)
			throws FMGAlreadyExecutingException {

		if (fmgIsExecuting)
			throw new FMGAlreadyExecutingException();
		fmgIsExecuting = true;

		// let's register the default observer with this observable object so
		// that waitForResult() can get the result
		defaultObserver = new FMGDefaultObserver();
		addObserver(defaultObserver);

		// now start the FMG algorithm (implemented via fmgProper()) in its own
		// thread
		new FMGThread(this, numPresmooth, numPostsmooth, cyclingStrategy,
				numMultiGrid);
	}

	/**
	 * blocks the current thread until the FMG algorithm which is executing in a
	 * different thread has finished and then returns the solution of the PDE.
	 * <p>
	 * If the FMG algorithm is not currently executing or has not just delivered
	 * a result an exception is thrown. In other words, call waitForResult()
	 * only after calling fmg().
	 * <p>
	 * This method can be called only once for every invocation of fmg().
	 * 
	 * @return the solution of the PDE
	 * 
	 * @throws FMGNotExecutingException
	 *             the FMG not executing exception
	 * 
	 * @exception FMGNotExecutingException
	 *                method fmg() has not been called before and hence the FMG
	 *                algorithm is not executing or has not just delivered a
	 *                result
	 */
	public BoundaryGrid waitForResult() throws FMGNotExecutingException {
		if (defaultObserver == null)
			throw new FMGNotExecutingException();
		BoundaryGrid result = defaultObserver.getResult(); // blocks until
		// result is
		// available
		deleteObserver(defaultObserver);
		defaultObserver = null;
		return result;
	}

	/**
	 * the body fo the FMG Algorithm that executes in its own thread.
	 * <p>
	 * This method is called from FMGThread.run() which in turn is indirectly
	 * called from FMG.fmg().
	 * <p>
	 * The parameters of this method are identical to the parameters of method
	 * fmg(). The result of this method (which is the result of the FMG
	 * algorithm and hence a grid of type BoundaryGrid) is returned via the
	 * notification mechanism to all observers that have been registered with
	 * this object.
	 * 
	 * @see FMG#solveParallel
	 */
	public ConstBoundaryGrid solve(int numPresmooth, int numPostsmooth,
			int cyclingStrategy, int numMultiGrid) {
		// don't check preconditions here, because they were already checked in
		ConstBoundaryGrid u = null;
		ConstBoundaryGrid uCoarse = null;
		ConstNoBoundaryGrid f = null;
		// Use all available resolutions
		int coarsestLevel = 0;
		int finestLevel = resolutions.getResolutionCount() - 1;
		for (int level = coarsestLevel; level <= finestLevel; level++) {
			int[] res = resolutions.getResolution(level);
			f = pde.sampleRHS(res[0], res[1], res[2], level);
			if (level == coarsestLevel) {
				// compute the exact solution on coarsest level using direct
				// solver
				u = solver.solve((BoundaryGrid) uPrototype.newInstance(res[0],
						res[1], res[2], level, 0), f);
			} else {
				// Interpolate solution to finer level
				u = interpolator.interpolate(uCoarse, resolutions);
				for (int i = 1; i <= numMultiGrid; i++) {
					u = mg(u, f, coarsestLevel, numPresmooth, numPostsmooth,
							cyclingStrategy);
				}

			}
			uCoarse = u;
		}
		// notify Observers of the availability of the result by sending it to
		// them
		setChanged();
		notifyObservers(u);
		fmgIsExecuting = false;
		return u;
	}

	/**
	 * performs the Multigrid algorithm which is at the heart of the FMG
	 * algorithm.
	 * <p>
	 * Given an approximate solution of the PDE on a grid the Multigrid
	 * algorithm finds a better solution on that grid by pre-smoothing, going to
	 * the next coarser grid and approximately solving there, going back to the
	 * original grid, and finally post-smoothing.
	 * 
	 * @param u
	 *            the approximate solution an a grid at a certain level
	 * @param f
	 *            the right hand side of the PDE at a grid at the same level as
	 *            u
	 * @param coarsestLevel
	 *            the coarsest level to which the Multigrid algorithm should
	 *            recursively go (0 < coarsestLevel <= level of u and f)
	 * @param numPresmooth
	 *            the number of pre-smoothing steps to perform ( > 0)
	 * @param numPostsmooth
	 *            the number of post-smoothing steps to perform ( >= 0)
	 * @param cyclingStrategy
	 *            the cycling strategy to use ( > 0). A value of 1 results in
	 *            V-cycles, a value of 2 in W-cycles. This gives the number of
	 *            times the MG algorithm recursively calls itself to solve the
	 *            coarse grid problem.
	 * 
	 * @return an improved approximate solution on a grid of the same size as u
	 */
	protected BoundaryGrid mg(ConstBoundaryGrid u, ConstNoBoundaryGrid f,
			int coarsestLevel, int numPresmooth, int numPostsmooth,
			int cyclingStrategy) {
		int level = u.getLevel();
		if (level == coarsestLevel) {
			// Solve for residual using the smoother because the direct solve
			// method will
			// likely fail since f will be close to zero.
			int iters=Math.max(numPostsmooth,numPostsmooth);
			for (int i = 1; i <= iters; i++) {
				u = smoother.smooth(u, f);
			}
		} else {
			// find a better solution at this level by using an approx. solution
			// from the next higher level
			
			for (int i = 1; i <= numPresmooth; i++) {
				u = smoother.smooth(u, f);
			}
			
			ConstNoBoundaryGrid rCoarse = (NoBoundaryGrid) restrictor.restrict(
					calcResidual(u, f), resolutions);
			int[] res = resolutions.getResolution(level - 1);
			ConstBoundaryGrid vCoarse = (BoundaryGrid) vPrototype.newInstance(
					res[0], res[1], res[2], level - 1, 0);
			for (int cycle = 1; cycle <= cyclingStrategy; cycle++) {
				vCoarse = mg(vCoarse, rCoarse, coarsestLevel, numPresmooth,
						numPostsmooth, cyclingStrategy);
			}
			rCoarse = null;
			u = (BoundaryGrid) u.add(interpolator.interpolate(vCoarse,
					resolutions));
			vCoarse = null;
		
			for (int i = 1; i <= numPostsmooth; i++) {
				u = smoother.smooth(u, f);
			}
			

		}
		return ((BoundaryGrid) u);
	}

	/**
	 * calculate the residual of an approximate solution to a PDE.
	 * <p>
	 * If Ax = f is the PDE and u is an approximation to the true solution x,
	 * then the residual r is defined as r = f - Au and is hence a 3-dimensional
	 * function sampled on a regular cubic grid without a boundary (because f is
	 * in our scheme without a boundary, too).
	 * 
	 * @param u
	 *            the approximate solution to the PDE whose residual is to be
	 *            calculated
	 * @param f
	 *            the right hand side (source term) of the PDE
	 * 
	 * @return the residual of the given approximate solution on a grid of the
	 *         size in question
	 */
	protected NoBoundaryGrid calcResidual(ConstBoundaryGrid u,
			ConstNoBoundaryGrid f) {
		Contract.pre(u != null && f != null, "all objects not null-objects");

		NoBoundaryGrid r = (NoBoundaryGrid) f.newInstance(u.getRows(), u
				.getCols(), u.getSlices(), u.getLevel(), 0);
		new CalcResidualParallelizer(this, r, u, f);
		return r;
	}

	/**
	 * implements the parallel part of calcResidual().
	 * 
	 * @param r
	 *            the r
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * @param myNum
	 *            the my num
	 * @param totalNum
	 *            the total num
	 */
	protected void calcResidualProper(NoBoundaryGrid r, ConstBoundaryGrid u,
			ConstNoBoundaryGrid f, int myNum, int totalNum) {
		int sx = u.getRows();
		int sy = u.getCols();
		int sz = u.getSlices();
		IntRange1D range = Parallelizer.partition(new IntRange1D(1, sx - 2),
				myNum, totalNum);
		double totalRes=0;
		int count=0;
		for (int x = range.from(); x <= range.to(); x++) {
			for (int y = 1; y <= (sy - 2); y++) {
				for (int z = 1; z <= (sz - 2); z++) {
					double res=f.get(x, y, z)
					- pde.evaluateLHS(u, f, x, y, z);
					r.set(x, y, z, res);
					count++;
					totalRes+=Math.abs(res);
				}
			}
		}
		totalRes/=count;
		System.out.println("RESIDUAL "+totalRes);
	}

	/** The resolutions. */
	private SolverResolutionLevels resolutions;

	/** The smoother. */
	private Smoother smoother;

	/** The restrictor. */
	private Restrictor restrictor;

	/** The interpolator. */
	private Interpolator interpolator;

	/** The solver. */
	private Solver solver;

	/** The pde. */
	private PDE pde;

	/** The u prototype. */
	private ConstBoundaryGrid uPrototype;

	/** The v prototype. */
	private ConstBoundaryGrid vPrototype;

	/** The fmg is executing. */
	private boolean fmgIsExecuting = false;

	/** The default observer. */
	private FMGDefaultObserver defaultObserver = null;
}
