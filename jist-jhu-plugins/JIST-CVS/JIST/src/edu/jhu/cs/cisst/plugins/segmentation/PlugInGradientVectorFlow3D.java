/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.segmentation;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.GradientVectorFlow3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.GradientVectorFlow3D.Method;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

/**
 * The Class PlugInGradientVectorFlow3D extends a gradient field using vector field diffusion.
 */
public class PlugInGradientVectorFlow3D extends ProcessingAlgorithm{
	
	/** The std dev param. */
	ParamDouble stdDevParam;
	
	/** The mu param. */
	ParamDouble muParam;
	
	/** The normalize param. */
	ParamBoolean normalizeParam;
	
	/** The max iterations param. */
	ParamInteger maxIterationsParam;
	
	/** The orig image param. */
	ParamVolume origImageParam;
	
	/** The gvf param. */
	ParamVolume gvfParam;
	
	/** The multigrid down sampling param. */
	ParamInteger multigridDownSamplingParam;
	
	/** The multigrid resolutions param. */
	ParamInteger multigridResolutionsParam;
	
	/** The multigrid smoothing param. */
	ParamInteger multigridSmoothingParam;
	
	/** The method param. */
	ParamOption methodParam;
	
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(origImageParam=new ParamVolume("Intensity Image"));
		inputParams.add(maxIterationsParam=new ParamInteger("Maximum Iterations",1000));
		inputParams.add(stdDevParam=new ParamDouble("Gaussian Std. Dev.",1));
		inputParams.add(muParam=new ParamDouble("Diffusion Weight",0.1));
		inputParams.add(normalizeParam=new ParamBoolean("Normalize"));
		Method[] methods=GradientVectorFlow3D.Method.values();
		String[] methodNames=new String[methods.length];
		for(int i=0;i<methodNames.length;i++)methodNames[i]=methods[i].name();
		inputParams.add(methodParam=new ParamOption("Method",methodNames));
		inputParams.add(multigridDownSamplingParam=new ParamInteger("Down-Sampling Factor",1,1000,8));
		inputParams.add(multigridResolutionsParam=new ParamInteger("Resolutions",1,1000,4));
		inputParams.add(multigridSmoothingParam=new ParamInteger("Smoothing Iterations",0,1000000,50));
		
		inputParams.setName("gvf3d");
		inputParams.setLabel("Gradient Vector Flow 3D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Segmentation");
		AlgorithmInformation info=getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setVersion(GradientVectorFlow3D.getVersion());
		info.setStatus(DevelopmentStatus.Release);
		info.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.add(new Citation("DOI:10.1109/83.661186"));
		info.setDescription("Extends the gradient field through a vector field diffusion process.");
		info.setLongDescription("The diffusion PDEs are solved using the Full Multigrid (FMG) method with a Conjugate Gradient (CG) solver.");
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(gvfParam=new ParamVolume("GVF Vector Field",VoxelType.FLOAT,-1,-1,-1,3));
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		GradientVectorFlow3D gvf=new GradientVectorFlow3D(stdDevParam.getDouble(), muParam.getDouble(),normalizeParam.getValue());
		monitor.observe(gvf);
		gvf.setMaxIterations(maxIterationsParam.getInt());
		gvf.setCoarsestLevelResolution(1.0/multigridDownSamplingParam.getDouble());
		gvf.setMultiGridResolutions(multigridResolutionsParam.getInt());
		gvf.setMultiGridSmoothIterations(multigridSmoothingParam.getInt());
		ImageDataFloat gradImage=gvf.solve(new ImageDataFloat(origImageParam.getImageData()),GradientVectorFlow3D.Method.values()[methodParam.getIndex()]);
		gvfParam.setValue(gradImage);
	}

}
