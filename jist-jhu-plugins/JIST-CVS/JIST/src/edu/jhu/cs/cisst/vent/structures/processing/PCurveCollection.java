/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.structures.processing;

import javax.vecmath.Point3f;
import processing.core.PApplet;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;

// TODO: Auto-generated Javadoc
/**
 * The Class PCurveCollection wraps a curve collection for rendering.
 */
public class PCurveCollection {
	
	/** The points. */
	Point3f[][] points;
	
	/** The wrap. */
	protected boolean wrap=false;
	public Point3f[][] getPoints(){
		return points;
	}
	/**
	 * Instantiates a new p curve collection.
	 * 
	 * @param curves the curves
	 * @param wrap the wrap
	 */
	public PCurveCollection(CurveCollection curves, boolean wrap) {
		this.points = curves.getCurves();
		this.wrap = wrap;
	}

	/**
	 * Draw in 2D
	 * 
	 * @param applet the applet
	 */
	public void draw2D(PApplet applet) {

		for (int i = 0; i < points.length; i++) {
			Point3f[] line = points[i];
			applet.beginShape();
			Point3f pt1;
			for (int j = 0; j < line.length; j++) {
				pt1=line[j];
				applet.vertex(pt1.x, pt1.y);
			}
			if(wrap){
				applet.endShape(PApplet.CLOSE);
			} else {
				applet.endShape();
			}
		}
	}

	/**
	 * Draw in 3D
	 * 
	 * @param applet the applet
	 */
	public void draw3D(PApplet applet) {
		for (int i = 0; i < points.length; i++) {
			Point3f[] line = points[i];
			for (int j = 0; j < line.length; j++) {

				if (wrap) {
					Point3f pt1 = line[j];
					Point3f pt2 = line[(j + 1) % line.length];
					Point3f pt3 = line[(j + 2) % line.length];
					Point3f pt4 = line[(j + 3) % line.length];
					applet.curve(pt1.x, pt1.y, pt1.z, pt2.x, pt2.y, pt2.z,
							pt3.x, pt3.y, pt3.z, pt4.x, pt4.y, pt4.z);
				} else {
					Point3f pt1 = line[j];
					Point3f pt2 = line[Math.min(j + 1, line.length - 1)];
					Point3f pt3 = line[Math.min(j + 2, line.length - 1)];
					Point3f pt4 = line[Math.min(j + 3, line.length - 1)];
					applet.curve(pt1.x, pt1.y, pt1.z, pt2.x, pt2.y, pt2.z,
							pt3.x, pt3.y, pt3.z, pt4.x, pt4.y, pt4.z);
				}
			}
		}
	}
}
