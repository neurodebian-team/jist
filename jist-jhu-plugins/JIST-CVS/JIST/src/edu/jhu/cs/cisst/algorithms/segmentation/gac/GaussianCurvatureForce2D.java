/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class GaussianCurvatureForce2D moves the level set using Gaussian
 * curvature, defined as the determinant of the second fundamental form divided
 * by the determinant of the first fundamental form. For numerical stability,
 * the magnitude of the Gaussian curvature is bounded from above.
 */
public class GaussianCurvatureForce2D extends ActiveContourForce2D {

	/** The max curvature force. */
	protected double maxCurvatureForce = 10;

	/**
	 * Gets the max curvature force.
	 * 
	 * @return the max curvature force
	 */
	public double getMaxCurvatureForce() {
		return maxCurvatureForce;
	}

	/**
	 * Sets the max curvature force.
	 * 
	 * @param maxCurvatureForce
	 *            the new max curvature force
	 */
	public void setMaxCurvatureForce(double maxCurvatureForce) {
		this.maxCurvatureForce = maxCurvatureForce;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D#evaluate(int,
	 *      int)
	 */
	public double evaluate(int i, int j) {
		GridPoint2D p00 = getGridPoint(i - 1, j - 1);
		GridPoint2D p01 = getGridPoint(i - 1, j);
		GridPoint2D p02 = getGridPoint(i - 1, j + 1);
		GridPoint2D p12 = getGridPoint(i, j + 1);
		GridPoint2D p11 = getGridPoint(i, j);
		GridPoint2D p10 = getGridPoint(i, j - 1);
		GridPoint2D p20 = getGridPoint(i + 1, j - 1);
		GridPoint2D p21 = getGridPoint(i + 1, j);
		GridPoint2D p22 = getGridPoint(i + 1, j + 1);
		double v11 = p11.value;
		double v00 = ((p00 != null) ? p00.value : v11);
		double v01 = ((p01 != null) ? p01.value : v11);
		double v02 = ((p02 != null) ? p02.value : v11);
		double v12 = ((p12 != null) ? p12.value : v11);
		double v10 = ((p10 != null) ? p10.value : v11);
		double v20 = ((p20 != null) ? p20.value : v11);
		double v21 = ((p21 != null) ? p21.value : v11);
		double v22 = ((p22 != null) ? p22.value : v11);
		double DxCtr = 0.5 * (v21 - v01);
		double DyCtr = 0.5 * (v12 - v10);
		double DxxCtr = v21 - v11 - v11 + v01;
		double DyyCtr = v12 - v11 - v11 + v10;
		double DxyCtr = (v22 - v02 - v20 + v00) * 0.25;
		double numer = DxxCtr * DyyCtr - DxyCtr * DxyCtr;
		double denom = DxCtr * DxCtr + DyCtr * DyCtr;
		double kappa;
		if(Math.abs(denom)>1E-5){
			kappa=weight*numer/denom;
		} else {
			kappa=weight*numer*Math.signum(denom)*1E5;

		}
		if(kappa<-maxCurvatureForce){
			kappa=-maxCurvatureForce;
		} else if(kappa>maxCurvatureForce){
			kappa=maxCurvatureForce;
		}
		return kappa;
	}

}
