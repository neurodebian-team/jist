package edu.jhu.cs.cisst.plugins.utilities.surface;

import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoSurfaceGenerator;
import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoSurfaceGenerator.Method;
import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoSurfaceGenerator.Winding;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The PlugInIsoSurfaceGenerator generates an iso-surface using either the
 * marching-cubes or marching-tetrahedral algorithm. The patent for
 * marching-cubes has expired as of 2005 and is now available for use in the
 * public domain. This implementation is a reimplementation of a C++ version
 * with enhancements that preserve mesh connectivity.
 * 
 * @author Blake Lucas
 * @author Cory Bloyd
 */
public class PlugInIsoSurfaceGenerator extends ProcessingAlgorithm {
	protected ParamVolume levelSetParam;
	protected ParamDouble isoLevelParam;
	protected ParamOption methodParam;
	protected ParamOption windingParam;
	protected ParamBoolean useResolutionsParam;
	protected ParamSurface surfaceParam;

	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(levelSetParam = new ParamVolume("Level Set Volume"));
		inputParams.add(isoLevelParam = new ParamDouble("Target Iso-Level", 0));
		Method[] methods = IsoSurfaceGenerator.Method.values();
		String[] methodNames = new String[methods.length];
		for (int i = 0; i < methodNames.length; i++)
			methodNames[i] = methods[i].toString().replace('_', ' ');
		inputParams.add(methodParam = new ParamOption("Method", methodNames));
		Winding[] windings = IsoSurfaceGenerator.Winding.values();
		String[] windingNames = new String[windings.length];
		for (int i = 0; i < windingNames.length; i++)
			windingNames[i] = windings[i].toString().replace('_', ' ');

		inputParams
				.add(windingParam = new ParamOption("Winding", windingNames));
		windingParam.setValue(1);
		inputParams.add(useResolutionsParam = new ParamBoolean(
				"Use Image Resolutions", true));
		inputParams.setName("iso-surf");
		inputParams.setLabel("Iso-Surface Generator");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Surface");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.add(new AlgorithmAuthor("Cory Bloyd", "corysama@yahoo.com",
				"http://astronomy.swin.edu.au/pbourke/modelling/polygonise/"));
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info
				.setDescription("Generates an iso-surface using either the marching-cubes or marching-tetrahedral algorithm.");
		info
				.setLongDescription("The patent for marching-cubes expired in 2005 and is now available for use in the public domain. This implementation is a reimplementation of a C++ version  by Cory Boyd with enhancements that preserve mesh connectivity.");
		info.add(new Citation("DOI:10.1145/37401.37422"));
		info.add(new Citation("DOI:10.1109/2945.485620"));
		info.setVersion(IsoSurfaceGenerator.getVersion());
		info.setStatus(DevelopmentStatus.Release);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(surfaceParam = new ParamSurface("Iso-Surface"));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		IsoSurfaceGenerator gen = new IsoSurfaceGenerator();
		monitor.observe(gen);
		gen.setMethod(Method.values()[methodParam.getIndex()]);
		gen.setWinding(Winding.values()[windingParam.getIndex()]);
		gen.setUseResolutions(useResolutionsParam.getValue());
		surfaceParam.setValue(gen.solve(new ImageDataFloat(levelSetParam
				.getImageData()), isoLevelParam.getFloat()));
	}

}
