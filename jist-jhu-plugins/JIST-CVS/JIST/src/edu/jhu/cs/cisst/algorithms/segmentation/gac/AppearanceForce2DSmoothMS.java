/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import no.uib.cipr.matrix.Vector;
import no.uib.cipr.matrix.sparse.AbstractIterativeSolver;
import no.uib.cipr.matrix.sparse.BiCG;
import no.uib.cipr.matrix.sparse.BiCGstab;
import no.uib.cipr.matrix.sparse.CG;
import no.uib.cipr.matrix.sparse.CGS;
import no.uib.cipr.matrix.sparse.DefaultIterationMonitor;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import no.uib.cipr.matrix.sparse.IterationReporter;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.SparseVector;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.DefaultSolverResolutionLevels2D;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.FMG;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.RepeatBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator.Interpolator;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator.InterpolatorByStencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.Parallelizer;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor.Restrictor;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor.RestrictorByStencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.RedBlackGaussSeidel;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.solver.ArithmeticSolver;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.solver.Solver;
import edu.jhu.cs.cisst.algorithms.util.DataOperations;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class AppearanceForce2DSmoothMS models regional appearance with the
 * Mumford-Shah denoising model.
 */
public class AppearanceForce2DSmoothMS extends AppearanceForce2D implements
		IterationReporter {

	/**
	 * The Enum Method.
	 */
	public enum Method {

		/** The Multi-grid solver. */
		MULTIGRID,

		/** The conjugate gradient solver. */
		CG,

		/** The conjugate gradient squared solver. */
		CGS,

		/** The Biconjugate graident solver. */
		BiCG,

		/** The stabilized biconjugate gradient solver. */
		BiCGstab
	}

	/** The A foreground. */
	protected FlexCompRowMatrix AForeground;

	/** The A background. */
	protected FlexCompRowMatrix ABackground;

	/** The b foreground. */
	protected SparseVector bForeground;

	/** The b background. */
	protected SparseVector bBackground;

	/** The x foreground. */
	protected SparseVector xForeground;

	/** The x background. */
	protected SparseVector xBackground;

	/** The fg image. */
	protected float[][] fgImage;

	/** The bg image. */
	protected float[][] bgImage;

	/** The foreground smoothness. */
	protected double foregroundSmoothness = 0.1;

	/** The background smoothness. */
	protected double backgroundSmoothness = 0.1;

	/** The method. */
	protected Method method = Method.BiCG;

	/** The multigrid threads. */
	protected int multiGridThreads = 1;

	/** The smooth iterations. */
	protected int smoothIterations = 10;

	/** The multi-grid cycles. */
	protected int multiGridCycles = 1;

	/** The multi-grid iterations. */
	protected int multiGridIterations = 2;

	/** The multi-grid resolutions. */
	protected int multiGridResolutions = 4;

	/** The coarsest level resolution. */
	protected double coarsestLevelResolution = 0.125;

	/** The max iterations. */
	protected int maxIterations = 10000;

	/** The allow multi grid update. */
	protected boolean allowMultiGridUpdate = false;

	/**
	 * Instantiates a new appearance force2 d smooth ms.
	 * 
	 * @param image
	 *            the image
	 * @param normalizeIntensities
	 *            the normalize intensities
	 */
	public AppearanceForce2DSmoothMS(ImageDataFloat image,
			boolean normalizeIntensities) {
		super(image, normalizeIntensities);
		int N = rows * cols;
		bForeground = new SparseVector(N);
		bBackground = new SparseVector(N);
		xForeground = new SparseVector(N);
		xBackground = new SparseVector(N);
		AForeground = new FlexCompRowMatrix(N, N);
		ABackground = new FlexCompRowMatrix(N, N);
	}

	/**
	 * Checks if is allow multi grid update.
	 * 
	 * @return true, if is allow multi grid update
	 */
	public boolean isAllowMultiGridUpdate() {
		return allowMultiGridUpdate;
	}

	/**
	 * Monitor.
	 * 
	 * @param err
	 *            the err
	 * @param iter
	 *            the iter
	 * 
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double, int)
	 */
	public void monitor(double err, int iter) {
		// TODO Auto-generated method stub

	}

	/**
	 * Monitor.
	 * 
	 * @param err
	 *            the err
	 * @param res
	 *            the res
	 * @param iter
	 *            the iter
	 * 
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double,
	 *      no.uib.cipr.matrix.Vector, int)
	 */
	public void monitor(double err, Vector res, int iter) {
		if (iter % 10 == 0)
			System.out.println(iter + ") " + err);
	}

	/**
	 * Sets the allow multi grid update.
	 * 
	 * @param allowMultiGridUpdate
	 *            the new allow multi grid update
	 */
	public void setAllowMultiGridUpdate(boolean allowMultiGridUpdate) {
		this.allowMultiGridUpdate = allowMultiGridUpdate;
	}

	/**
	 * Sets the background smoothness.
	 * 
	 * @param backgroundSmoothness
	 *            the new background smoothness
	 */
	public void setBackgroundSmoothness(double backgroundSmoothness) {
		this.backgroundSmoothness = backgroundSmoothness;
	}

	/**
	 * Sets the coarsest level resolution.
	 * 
	 * @param coarsestLevelResolution
	 *            the new coarsest level resolution
	 */
	public void setCoarsestLevelResolution(double coarsestLevelResolution) {
		this.coarsestLevelResolution = coarsestLevelResolution;
	}

	/**
	 * Sets the foreground smoothness.
	 * 
	 * @param foregroundSmoothness
	 *            the new foreground smoothness
	 */
	public void setForegroundSmoothness(double foregroundSmoothness) {
		this.foregroundSmoothness = foregroundSmoothness;
	}

	/**
	 * Sets the max iterations.
	 * 
	 * @param maxIterations
	 *            the new max iterations
	 */
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	/**
	 * Sets the method.
	 * 
	 * @param method
	 *            the new method
	 */
	public void setMethod(Method method) {
		this.method = method;
	}

	/**
	 * Sets the multigrid cycles.
	 * 
	 * @param numMultigridCycles
	 *            the new multigrid cycles
	 */
	public void setMultiGridCycles(int numMultigridCycles) {
		this.multiGridCycles = numMultigridCycles;
	}

	/**
	 * Sets the multi grid iterations.
	 * 
	 * @param numMultiGridIterations
	 *            the new multi grid iterations
	 */
	public void setMultiGridIterations(int numMultiGridIterations) {
		this.multiGridIterations = numMultiGridIterations;
	}

	/**
	 * Sets the multi grid resolutions.
	 * 
	 * @param numMultiGridLevels
	 *            the new multi grid resolutions
	 */
	public void setMultiGridResolutions(int numMultiGridLevels) {
		this.multiGridResolutions = numMultiGridLevels;
	}

	/**
	 * Sets the multi grid smooth iterations.
	 * 
	 * @param numMultiGridSmoothIterations
	 *            the new multi grid smooth iterations
	 */
	public void setMultiGridSmoothIterations(int numMultiGridSmoothIterations) {
		this.smoothIterations = numMultiGridSmoothIterations;
	}

	/**
	 * Sets the multi grid threads.
	 * 
	 * @param numMultiGridThreads
	 *            the new multi grid threads
	 */
	public void setMultiGridThreads(int numMultiGridThreads) {
		this.multiGridThreads = numMultiGridThreads;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D#update
	 * ()
	 */
	public void update() {
		if (method == Method.MULTIGRID) {
			updateMultiGrid();
		} else {
			updateLinear();
		}
	}

	/**
	 * Solve using a sparse matrix method.
	 * 
	 * @return the image data float
	 */
	protected void updateLinear() {
		float[][] heavisideImage = new float[rows][cols];
		float[][] intensityEstimateImage = imageEstimate.toArray2d();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				heavisideImage[i][j] = (float) getHeavisideValue(i, j);
			}
		}

		float[][][] gradient = DataOperations.gradient(heavisideImage, false);

		int index = 0;
		System.out.println("SMOOTHNESS " + foregroundSmoothness + " "
				+ backgroundSmoothness);
		// Setup linear solver
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double dx = gradient[i][j][0];
				double dy = gradient[i][j][1];
				double indicator = heavisideImage[i][j];
				double val = intensityImage[i][j];
				double valEst = intensityEstimateImage[i][j];
				int v11 = getIndex(i, j);
				int v01 = getIndex(i - 1, j);
				int v21 = getIndex(i + 1, j);
				int v10 = getIndex(i, j - 1);
				int v12 = getIndex(i, j + 1);

				double weightForeground = foregroundSmoothness
						* (1 - indicator);
				double weightBackground = backgroundSmoothness * (indicator);
				double centerWeight = 0;

				if (i > 0) {
					AForeground.set(index, v01, weightForeground + 0.5 * dx
							* foregroundSmoothness);
					centerWeight++;
				}
				if (i < rows - 1) {
					AForeground.set(index, v21, weightForeground - 0.5 * dx
							* foregroundSmoothness);
					centerWeight++;
				}
				if (j > 0) {
					AForeground.set(index, v10, weightForeground + 0.5 * dy
							* foregroundSmoothness);
					centerWeight++;
				}
				if (j < cols - 1) {
					AForeground.set(index, v12, weightForeground - 0.5 * dy
							* foregroundSmoothness);
					centerWeight++;
				}
				AForeground.set(index, v11, -centerWeight * weightForeground
						- 1);
				centerWeight = 0;

				if (i > 0) {
					ABackground.set(index, v01, weightBackground - 0.5 * dx
							* backgroundSmoothness);
					centerWeight++;
				}
				if (i < rows - 1) {
					ABackground.set(index, v21, weightBackground + 0.5 * dx
							* backgroundSmoothness);
					centerWeight++;
				}
				if (j > 0) {
					ABackground.set(index, v10, weightBackground - 0.5 * dy
							* backgroundSmoothness);
					centerWeight++;
				}
				if (j < cols - 1) {
					ABackground.set(index, v12, weightBackground + 0.5 * dy
							* backgroundSmoothness);
					centerWeight++;
				}
				ABackground.set(index, v11, -centerWeight * weightBackground
						- 1);
				bForeground.set(index, -val);
				bBackground.set(index, -val);
				if (fgImage != null && bgImage != null) {
					xForeground.set(index, fgImage[i][j]);
					xBackground.set(index, bgImage[i][j]);
				} else {
					xForeground.set(index, val);
					xBackground.set(index, val);
				}
				index++;

			}
		}
		AbstractIterativeSolver solver = null;
		// Choose solving method
		switch (method) {
		case CG:
			solver = new CG(bForeground);
			break;
		case CGS:
			solver = new CGS(bForeground);
			break;
		case BiCG:
			solver = new BiCG(bForeground);
			break;
		case BiCGstab:
			solver = new BiCGstab(bForeground);
			break;
		}
		DefaultIterationMonitor im = new DefaultIterationMonitor();
		im.setMaxIterations(maxIterations);
		im.setIterationReporter(this);
		solver.setIterationMonitor(im);

		try {
			solver.solve(AForeground, bForeground, xForeground);
		} catch (IterativeSolverNotConvergedException e) {
			e.printStackTrace();
		}
		try {
			solver.solve(ABackground, bBackground, xBackground);
		} catch (IterativeSolverNotConvergedException e) {
			e.printStackTrace();
		}
		index = 0;
		double maxHeavyVal = heavisideDerivative(0, fuzziness, heaviside);
		double invMaxHeavyVal = 1 / maxHeavyVal;
		if (fgImage == null || bgImage == null) {
			fgImage = new float[rows][cols];
			bgImage = new float[rows][cols];
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double fg = xForeground.get(index);
				double bg = xBackground.get(index);
				fgImage[i][j] = (float) fg;
				bgImage[i][j] = (float) bg;
				index++;
			}
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double fg = fgImage[i][j];
				double bg = bgImage[i][j];
				double fgGradMag = DataOperations.gradientMagnitudeSquared(
						fgImage, i, j);
				double bgGradMag = DataOperations.gradientMagnitudeSquared(
						bgImage, i, j);
				double indicator = heavisideImage[i][j];
				intensityEstimateImage[i][j] = (float) ((1 - indicator) * fg + bg
						* indicator);
				double val = intensityImage[i][j];
				double heavyVal = invMaxHeavyVal
						* heavisideDerivative(levelset[i][j], fuzziness,
								heaviside);
				// Pressure force is only valid in capture range of heaviside
				pressureForce[i][j] = (float) (heavyVal * (-fgWeight
						* ((val - fg) * (val - fg) + foregroundSmoothness
								* fgGradMag) + bgWeight
						* ((val - bg) * (val - bg) + backgroundSmoothness
								* bgGradMag)));

			}
		}
	}

	/**
	 * Update multi grid.
	 */
	protected void updateMultiGrid() {
		Parallelizer.setDefaultNumberOfThreads(multiGridThreads);
		Restrictor restrictor = new RestrictorByStencil(
				RestrictorByStencil.FULL_WEIGHTING_2D);
		Interpolator interpolator = new InterpolatorByStencil(
				InterpolatorByStencil.BILINEAR);
		// Setup prototypes for correction
		ConstBoundaryGrid prototype = new RepeatBoundaryGrid(rows, cols, 3, 0,
				0, 0);
		fgImage = new float[rows][cols];
		bgImage = new float[rows][cols];
		DefaultSolverResolutionLevels2D resolutions = new DefaultSolverResolutionLevels2D(
				prototype, (int) Math.round(Math.max(rows, cols)
						* coarsestLevelResolution), multiGridResolutions);
		for (int i = 0; i < resolutions.getResolutionCount(); i++) {
			int[] res = resolutions.getResolution(i);
			System.out.println("Resolution: (" + res[0] + "," + res[1] + ","
					+ res[2] + ") Grid-spacing: "
					+ resolutions.getGridSpacing(i));
		}
		float[][][] intensityImagePyramid = new float[resolutions
				.getResolutionCount()][rows][cols];
		float[][][] heavisideImagePyramid = new float[resolutions
				.getResolutionCount()][rows][cols];
		float[][] lastHeavisideImage = new float[rows][cols];
		int insideCount = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				lastHeavisideImage[i][j] = (float) getHeavisideValue(i, j);
				if (lastHeavisideImage[i][j] < 0.5)
					insideCount++;
			}
		}
		System.err.println("INSIDE COUNT " + insideCount + " / "
				+ (rows * cols));
		System.err.flush();
		float[][] lastIntensityImage = intensityImage;
		// Create gradient pyramid
		float[][][][] gradientPyramid = new float[resolutions
				.getResolutionCount()][2][rows][cols];
		// Create initial gradient image

		float[][][] lastGradientImage = DataOperations.gradient(
				lastHeavisideImage, true);
		gradientPyramid[gradientPyramid.length - 1] = lastGradientImage;
		intensityImagePyramid[intensityImagePyramid.length - 1] = lastIntensityImage;
		heavisideImagePyramid[heavisideImagePyramid.length - 1] = lastHeavisideImage;
		for (int l = 1; l < gradientPyramid.length; l++) {
			int[] dims = resolutions.getResolution(gradientPyramid.length - 1
					- l);
			// X gradient
			float[][] tmp = heavisideImagePyramid[heavisideImagePyramid.length
					- 1 - l] = DataOperations.restrict(lastHeavisideImage,
					RestrictorByStencil.FULL_WEIGHTING_2D, dims[0], dims[1]);
			gradientPyramid[gradientPyramid.length - 1 - l] = DataOperations
					.gradient(tmp, true);
			intensityImagePyramid[intensityImagePyramid.length - 1 - l] = DataOperations
					.restrict(lastIntensityImage,
							RestrictorByStencil.FULL_WEIGHTING_2D, dims[0],
							dims[1]);

		}
		// Solve PDE in X
		MumfordShah2D fgPDE = new MumfordShah2D(foregroundSmoothness,
				MumfordShah2D.Region.Foreground, gradientPyramid,
				intensityImagePyramid, heavisideImagePyramid, resolutions,
				maxIterations);

		Smoother fgSmoother = new RedBlackGaussSeidel(fgPDE);
		Solver fgSolver = new ArithmeticSolver(fgPDE);
		FMG fmg = new FMG(fgPDE, fgSmoother, restrictor, interpolator,
				fgSolver, prototype, prototype, resolutions);
		ConstBoundaryGrid fgResult = fmg.solve(smoothIterations,
				smoothIterations, multiGridCycles, multiGridIterations);
		MumfordShah2D bgPDE = new MumfordShah2D(backgroundSmoothness,
				MumfordShah2D.Region.Background, gradientPyramid,
				intensityImagePyramid, heavisideImagePyramid, resolutions,
				maxIterations);
		Smoother bgSmoother = new RedBlackGaussSeidel(bgPDE);
		Solver bgSolver = new ArithmeticSolver(bgPDE);
		fmg = new FMG(bgPDE, bgSmoother, restrictor, interpolator, bgSolver,
				prototype, prototype, resolutions);
		ConstBoundaryGrid bgResult = fmg.solve(smoothIterations,
				smoothIterations, multiGridCycles, multiGridIterations);

		float[][] intensityEstimateImage = imageEstimate.toArray2d();
		double maxHeavyVal = heavisideDerivative(0, fuzziness, heaviside);
		double invMaxHeavyVal = 1 / maxHeavyVal;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (i < fgResult.getRows() && j < bgResult.getCols()) {
					double fg = fgResult.get(i, j, 1);
					double bg = bgResult.get(i, j, 1);
					fgImage[i][j] = (float) fg;
					bgImage[i][j] = (float) bg;
					double fgGradMag = DataOperations
							.gradientMagnitudeSquared2D(fgResult, i, j, 1);
					double bgGradMag = DataOperations
							.gradientMagnitudeSquared2D(bgResult, i, j, 1);
					double indicator = lastHeavisideImage[i][j];
					intensityEstimateImage[i][j] = (float) ((1 - indicator)
							* fg + bg * indicator);
					double val = lastIntensityImage[i][j];
					double heavyVal = invMaxHeavyVal
							* heavisideDerivative(levelset[i][j], fuzziness,
									heaviside);
					// Pressure force is only valid in capture range of
					// heaviside
					pressureForce[i][j] = (float) (heavyVal * (-fgWeight
							* ((val - fg) * (val - fg) + foregroundSmoothness
									* fgGradMag) + bgWeight
							* ((val - bg) * (val - bg) + backgroundSmoothness
									* bgGradMag)));
				}
			}
		}
		if (!allowMultiGridUpdate) {
			method = Method.CG;
		}
	}
}
