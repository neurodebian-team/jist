/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * An implementation of the damped Jacobi method of relaxing (thereby smoothing)
 * the solution of a linear elliptic PDE.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class DampedJacobi implements Smoother {

	/**
	 * in theory the ideal weighting factor, i.e. the relative contribution of a
	 * new iteration value at a grid element.
	 */
	public static final double IDEAL_WEIGHTING_FACTOR = 2.0 / 3.0;

	/**
	 * construct from PDE and weighting factor.
	 * 
	 * @param pde
	 *            a representation of the PDE
	 * @param weightingFactor
	 *            the relative contribution of a new iteration value at a grid
	 *            element (0 < weightingFactor <= 1)
	 */
	public DampedJacobi(PDE pde, double weightingFactor) {
		Contract.pre(pde != null, "pde not null-object");
		Contract.pre(weightingFactor > 0 && weightingFactor <= 1,
				"0 < weightingFactor <= 1");

		this.pde = pde;
		this.weightingFactor = weightingFactor;

		oneMinusWF = 1.0 - weightingFactor;
	}

	/**
	 * implements method from Smoother.
	 * 
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * 
	 * @return the boundary grid
	 * 
	 * @see Smoother#smooth
	 */
	public BoundaryGrid smooth(ConstBoundaryGrid u, ConstNoBoundaryGrid f) {
		Contract.pre(u != null && f != null, "all objects not null-objects");

		sx = u.getRows();
		sy = u.getCols();
		sz = u.getSlices();
		totalRange = new IntRange1D(1, sx - 2);
		BoundaryGrid g = (BoundaryGrid) u.newInstance(u.getRows(), u.getCols(),
				u.getSlices(), u.getLevel(), 0);
		new DampedJacobiSmoothParallelizer(this, g, u, f);
		return g;
	}

	/**
	 * implements of the parallel part of smooth().
	 * 
	 * @param g
	 *            the g
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * @param myNum
	 *            the current index
	 * @param totalNum
	 *            the number of parallel jobs
	 */
	void smoothProper(BoundaryGrid g, ConstBoundaryGrid u,
			ConstNoBoundaryGrid f, int myNum, int totalNum) {
		IntRange1D range = Parallelizer.partition(totalRange, myNum, totalNum);
		for (int x = range.from(); x <= range.to(); x++) {
			for (int y = 1; y <= (sy - 2); y++) {
				for (int z = 1; z <= (sz - 2); z++) {
					// calculate new value
					g.set(x, y, z, oneMinusWF * u.get(x, y, z)
							+ weightingFactor * pde.evaluate(u, f, x, y, z));
				}
			}
		}
	}

	/** The pde. */
	private PDE pde;

	/** The weighting factor. */
	private double weightingFactor;

	/** The one minus wf. */
	private double oneMinusWF;

	/** The sx. */
	private int sx;

	/** The sy. */
	private int sy;

	/** The sz. */
	private int sz;

	/** The total range. */
	private IntRange1D totalRange;
}
