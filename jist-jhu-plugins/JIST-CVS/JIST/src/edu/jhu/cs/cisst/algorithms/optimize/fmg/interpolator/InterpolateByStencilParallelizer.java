/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * A helper class of class InterpolatorByStencil that helps parallelizing method InterpolatorByStencil.interpolate().
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
final class InterpolateByStencilParallelizer extends Parallelizer {
     
     /**
      * construct.
      * 
      * @param inter the stencil
      * @param fine the fine
      * @param coarse the coarse
      */
     public InterpolateByStencilParallelizer(InterpolatorByStencil inter, BoundaryGrid fine, ConstBoundaryGrid coarse) {
          this.inter  = inter;
          this.fine   = fine;
          this.coarse = coarse;
          
          start();
     }
     
     /**
      * re-route.
      * 
      * @param myNum the current index
      * @param totalNum the number of parallel jobs
      */
     public void runParallel(int myNum, int totalNum) {
          inter.interpolateProper(fine,coarse,myNum,totalNum);
     }
     
     /** The stencil */
     private InterpolatorByStencil inter;
     
     /** The fine. */
     private BoundaryGrid          fine;
     
     /** The coarse. */
     private ConstBoundaryGrid     coarse;
}
