/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.utilities.surface;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import edu.jhu.cs.cisst.algorithms.geometry.surface.*;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class PlugInConvertSurfaceToLevelSet extends ProcessingAlgorithm {
	protected ParamSurface surfParam;
	protected ParamVolume levelsetParam;
	protected ParamDouble cellsizeParam;
	protected ParamPointFloat minPointParam;
	protected ParamPointFloat maxPointParam;
	protected ParamPointInteger dimsParam;
	protected ParamBoolean useDimsParam;
	protected ParamBoolean autoBoundsParam;

	protected ParamMatrix reshapeMatrixParam;
	protected ParamSurface isoSurfParam;

	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surfParam = new ParamSurface("Surface"));
		inputParams.add(cellsizeParam = new ParamDouble("Cell Size", 1));
		inputParams.add(minPointParam = new ParamPointFloat("Min Point",
				new Point3f(0, 0, 0)));
		inputParams.add(maxPointParam = new ParamPointFloat("Max Point",
				new Point3f(256, 256, 256)));
		inputParams.add(dimsParam = new ParamPointInteger("Dimensions",
				new Point3i(256, 256, 256)));
		inputParams
				.add(useDimsParam = new ParamBoolean("Use Dimensions", true));
		inputParams
				.add(autoBoundsParam = new ParamBoolean("Auto-Bounds", true));

		inputParams.setName("surf_to_level_set");
		inputParams.setLabel("Surface to Level Set");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setPackage("CISST");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setVersion(MeshDistanceHash.getVersion());
		info.setDescription("Converts a surface to level set.");
		info.setStatus(DevelopmentStatus.Release);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(levelsetParam = new ParamVolume("Level Set"));
		outputParams.add(isoSurfParam = new ParamSurface("Iso-Surface"));
		outputParams.add(reshapeMatrixParam = new ParamMatrix(
				"Volume to Surface Transform", 4, 4));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		TriangleMesh mesh = new TriangleMesh(surfParam.getSurface());
		MeshDistanceHash hash;
		if (autoBoundsParam.getValue()) {
			hash = new MeshDistanceHash(mesh, cellsizeParam.getDouble());
		} else {
			if (useDimsParam.getValue()) {
				hash = new MeshDistanceHash(mesh, cellsizeParam.getDouble(),
						minPointParam.getValue(), dimsParam.getValue());
			} else {
				hash = new MeshDistanceHash(mesh, cellsizeParam.getDouble(),
						minPointParam.getValue(), maxPointParam.getValue());
			}
		}
		monitor.observe(hash);
		ImageDataFloat levelset = hash.getDistanceField();
		levelsetParam.setValue(levelset);
		isoSurfParam.setValue(hash.getIsoSurface());
		reshapeMatrixParam.setValue(hash.getTransform());
		levelsetParam.setValue(hash.getDistanceField());
	}

}
