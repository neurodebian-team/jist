package edu.jhu.cs.cisst.plugins.utilities.file;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;

public class PlugInCastFileToVolume extends ProcessingAlgorithm {
	protected ParamFile fileParam;
	protected ParamVolume volumeParam;

	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(fileParam = new ParamFile("File"));
		inputParams.setName("file_to_vol");
		inputParams.setLabel("Cast File to Volume");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.File");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setDescription("Casts a file to volume.");
		info.setStatus(DevelopmentStatus.Release);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(volumeParam = new ParamVolume("Volume"));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		volumeParam.setValue(fileParam.getValue());
	}

}
