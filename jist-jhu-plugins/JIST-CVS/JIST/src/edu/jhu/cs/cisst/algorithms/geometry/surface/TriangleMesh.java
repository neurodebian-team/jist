/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;

// TODO: Auto-generated Javadoc
/**
 * The Class PointMesh stores vertices and triangles that describe a surface.
 * 
 * @author Kel and Blake
 */
public class TriangleMesh {

	/** The surf. */
	EmbeddedSurface surf;
	
	/** The mesh vertices. */
	Point3f[] meshVertices;
	
	/** The triangles which make up the mesh. */
	protected MeshTriangle[] triangles;
	
	/** The bbox. */
	protected MeshBBox bbox;
	
	/**
	 * Gets the surface.
	 *
	 * @return the surface
	 */
	public EmbeddedSurface getSurface(){
		return surf;
	}
	/**
	 * Gets the mesh vertices.
	 * 
	 * @return the mesh vertices
	 */
	public Point3f[] getMeshVertices() {
		return meshVertices;
	}

	/**
	 * Gets the triangles.
	 * 
	 * @return the triangles
	 */
	public MeshTriangle[] getTriangles() {
		return triangles;
	}

	/**
	 * Instantiate a new PointMesh.
	 *
	 * @param surf the surf
	 */
	public TriangleMesh(EmbeddedSurface surf) {
		this.surf = surf;
		init();
	}

	/**
	 * Update vertices coordinates based on new indices for every triangle.
	 */
	public void updateMeshTriangles() {

		for (int i = 0; i < triangles.length; i++) {
			triangles[i].updateVertices(meshVertices[triangles[i].indices[0]],
					meshVertices[triangles[i].indices[1]],
					meshVertices[triangles[i].indices[2]]);
		}

	}
	
	/**
	 * Gets the bounding box.
	 *
	 * @return the bounding box
	 */
	public MeshBBox getBoundingBox(){
		return bbox;
	}
	
	/**
	 * Inits the.
	 */
	protected void init() {
		int numIndexes = surf.getIndexCount();
		this.triangles = new MeshTriangle[numIndexes / 3];
		this.meshVertices = surf.getVertexCopy();
		int currentTri = 0;
		int index = 0;
		MeshTriangle tri;
		LinkedList<MeshBBox> bboxes=new LinkedList<MeshBBox>();
		while (index < numIndexes) {
			int[] indexes=new int[]{surf.getCoordinateIndex(index++),surf.getCoordinateIndex(index++),surf.getCoordinateIndex(index++)};
			Point3f[] vertexes=new Point3f[]{meshVertices[indexes[0]],meshVertices[indexes[1]],meshVertices[indexes[2]]};
			this.triangles[currentTri++] = tri=new MeshTriangle(indexes,vertexes);
			bboxes.add(new MeshBBox(tri));
		}
		bbox=new MeshBBox(bboxes);
	}

}
