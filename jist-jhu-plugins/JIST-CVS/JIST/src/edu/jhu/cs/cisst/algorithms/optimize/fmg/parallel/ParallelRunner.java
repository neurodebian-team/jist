/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * Customized implementation of class Thread that works closely with class Parallelizer and actually calls
 * the parallel algorithm in each thread.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */

public class ParallelRunner extends Thread {
     
     /**
      * create a new thread working with the given Parallelizer and having the given thread number.
      * 
      * @param p the parallelizer to work with
      * @param num the number of this thread (0 <= num < total number of threads)
      */
     public ParallelRunner(Parallelizer p, int num) {
          super(p.threadGroup,"ParallelRunner-" + num);
          
          // if p were a null-object we would have noticed it in the call top super() above - so no need to check this.
          Contract.pre(0 <= num && num < p.numThreads,"0 <= num < total number of threads");
          
          this.p   = p;
          this.num = num;
     }

     /**
      * the Threads main method.
      */
     public void run() {
          p.target.runParallel(num,p.numThreads);
          p.finished();
     }

     /** The p. */
     private Parallelizer p;
     
     /** The num. */
     private int          num;
}
