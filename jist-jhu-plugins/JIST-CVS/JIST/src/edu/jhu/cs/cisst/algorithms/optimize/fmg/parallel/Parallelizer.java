/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * A class that simplifies the task of scheduling one thread per processor of a SMP machine and calling a parallel
 * algorithm in each of these threads, so that together these threads solve a common problem in parallel.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */

public class Parallelizer implements Parallelizable {
     
     /**
      * construct using the default implementation of runParallel() and using the default number of threads.
      * 
      * @see Parallelizer#setDefaultNumberOfThreads
      */
     public Parallelizer() {
          this(null,defaultNumThreads);
     }
     
     /**
      * construct given a certain number of threads and using the default implementation of runParallel().
      * 
      * @param numThreads the number of threads that will be created and put to work on the problem ( > 0)
      */
     public Parallelizer(int numThreads) {
          this(null,numThreads);
     }
     
     /**
      * construct given a target object and using the default number of threads.
      * 
      * @param target interface to the parallel algorithm that will be called in each thread
      * 
      * @see Parallelizer#setDefaultNumberOfThreads
      */
     public Parallelizer(Parallelizable target) {
          this(target,defaultNumThreads);
     }

     /**
      * construct given a target object and a certain number of threads.
      * 
      * @param target     interface to the parallel algorithm that will be called in each thread
      * @param numThreads the number of threads that will be created and put to work on the problem ( > 0)
      */
     public Parallelizer(Parallelizable target, int numThreads) {
          Contract.pre(numThreads > 0,"numThreads > 0");
          
          this.target     = (target == null ? this : target);
          this.numThreads = numThreads;
     }

     /**
      * start the Parallelizer by creating the requested number of threads, calling the runParallel() method of the
      * target object (of type Parallelizable) in each thread and waiting for all threads to finish before returning.
      * <p>
      * All threads will be put into a newly created thread group.
      */
     public synchronized void start() {
          threadGroup = new ThreadGroup("Parallelizer_Group");
          for (int t = 0; t < numThreads; t++) {
               (new ParallelRunner(this,t)).start();
          }
          try {wait();} catch (InterruptedException e) {}
     }
     
     /**
      * the default implementation of the parallel algorithm, which does nothing.
      * <p>
      * If a target was supplied when creating this object, this method is not consulted.
      * 
      * @param myNum the my num
      * @param totalNum the total num
      */
     public void runParallel(int myNum, int totalNum) {}
     
     /**
      * set the default number of threads for all objects of this type.
      * <p>
      * Before calling this method, the default number of threads defaults to 1 (corresponding to serial execution),
      * because there is no system-independent way of querying the number of processors on a machine from Java.
      * 
      * @param defaultNum the default number of threads to create from now on ( > 0)
      */
     public static synchronized void setDefaultNumberOfThreads(int defaultNum) {
          Contract.pre(defaultNum > 0,"defaultNum > 0");
          
          defaultNumThreads = defaultNum;
     }

     /**
      * partition a 1-dimensional range of (consecutive!) numbers (usually values for a loop variable) into
      * a given number of portions and return one of these portions as a range.
      * <p>
      * When the total workload of an algorithm can be evenly mapped to the given range of numbers, and this
      * method is supplied with the total number of threads working on the algorithm, it can determine the
      * percentage of the total workload that should be carried by each processor for optimal load balancing.
      * 
      * @param totalRange the consecutive range of integer values that should be partitioned
      * @param myNum      the number of the thread for which the partition is sought (0 <= myNum < totalNum)
      * @param totalNum   the total number of threads working on this problem ( > 0)
      * 
      * @return the int range1 d
      */
     public static IntRange1D partition(IntRange1D totalRange, int myNum, int totalNum) {
          int size = totalRange.size();
          
          Contract.pre(size > 0,"totalRange a valid range with from-value <= to-value");
          Contract.pre(0 <= myNum && myNum < totalNum,"0 <= myNum < totalNum");
          Contract.pre(totalNum > 0,"totalNum > 0");
          
          int intDiv = size/totalNum;
          int firstIndex, lastIndex;
          if (intDiv == 0) {
               if (myNum < size) {
                    firstIndex = lastIndex = totalRange.from() + myNum;
               } else {
                    // don't assign any portion to this thread
                    firstIndex = totalRange.to();
                    lastIndex  = totalRange.from();
               }
          } else {
               int rest = size - intDiv*totalNum;
               if (myNum < rest) {
                    firstIndex = totalRange.from() + myNum*(intDiv + 1);
                    lastIndex  = totalRange.from() + (myNum + 1)*(intDiv + 1) - 1;
               } else {
                    firstIndex = totalRange.from() + myNum*intDiv + rest;
                    lastIndex  = totalRange.from() + (myNum + 1)*intDiv + rest - 1;
               }
          }
          return new IntRange1D(firstIndex,lastIndex);
     }

     /**
      * called by the run() method of each thread as it finishes.
      */
     public synchronized void finished() {
          if (++numFinished == numThreads) notifyAll();
     }

     /** The thread group. */
     ThreadGroup        threadGroup;
     
     /** The target. */
     Parallelizable     target;
     
     /** The num threads. */
     int                numThreads;
     
     /** The num finished. */
     private int        numFinished = 0;
     
     /** The default num threads. */
     private static int defaultNumThreads = 1;
}
