/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.Stencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.StencilImpl;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor.RestrictorByStencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;
import edu.jhu.cs.cisst.algorithms.util.DataOperations;

/**
 * An interpolator that operates according to an interpolation stencil.
 * <p>
 * A stencil is a means of defining the relationship of a grid element and its 26 nearest neighbors. In this case the
 * interpolation stencil defines how to distribute the values of a coarse grid to the values of a fine grid during the
 * process of interpolation.
 * 
 * @see Stencil
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public class InterpolatorByStencil implements Interpolator {
     
     /** the stencil that characterises trilinear interpolation where every element of the fine grid that does not coincide with an element of the coarse grid is calculated by linear interpolation between its nearest neighbors. */
     public static final Stencil TRILINEAR = new StencilImpl(
          1./8.,1./4.,1./8., 1./4.,1./2.,1./4., 1./8.,1./4.,1./8.,
          1./4.,1./2.,1./4., 1./2.,1./1.,1./2., 1./4.,1./2.,1./4.,
          1./8.,1./4.,1./8., 1./4.,1./2.,1./4., 1./8.,1./4.,1./8.);
     public static final Stencil BILINEAR = new StencilImpl(
             0,3./4.,0,
             0,1./1.,0,
             0,3./4.,0,
             0,1./1.,0,
             0,1./1.,0,
             0,1./1.,0,
             0,3./4.,0,
             0,1./1.,0,
             0,3./4.,0);
     /**
      * construct from interpolation stencil.
      * 
      * @param stencil the stencil that defines interpolation
      */
     public InterpolatorByStencil(Stencil stencil) {
          Contract.pre(stencil != null,"stencil not null-object");
          
          s = stencil;
     }
     
     /**
      * implements method from Interpolator.
      * 
      * @param grid the grid
      * @param levels the levels
      * 
      * @return the boundary grid
      * 
      * @see Interpolator#interpolate
      */
     public BoundaryGrid interpolate(ConstBoundaryGrid grid,SolverResolutionLevels levels) {
          Contract.pre(grid != null,"grid not null-object");
          
          coarseSize = levels.getResolution(grid.getLevel());
          int level=grid.getLevel()+1;
          fineSize = levels.getResolution(level);
          totalRange = new IntRange1D(0,fineSize[0] - 1);
          BoundaryGrid fine = (BoundaryGrid) grid.newInstance(fineSize[0],fineSize[1],fineSize[2],level,0); // create fine grid

          new InterpolateByStencilParallelizer(this,fine,grid);
          return fine;
     }
     
     /**
      * implements the parallel part of interpolate().
      * 
      * @param fine the fine
      * @param coarse the coarse
      * @param myNum the current index
      * @param totalNum the number of parallel jobs
      */
     public void interpolateProper(BoundaryGrid fine, ConstBoundaryGrid coarse, int myNum, int totalNum) {
 		IntRange1D range = Parallelizer.partition(totalRange, myNum, totalNum);
 		int fx = fine.getRows();
 		int fy = fine.getCols();
 		int fz = fine.getSlices();
 		int cx = coarse.getRows();
 		int cy = coarse.getCols();
 		int cz = coarse.getSlices();
 		//Interpolation 
 		double scale = Math.min(cz / (double) fz, Math.min(cx / (double) fx, cy
 				/ (double) fy));
          for (int i = range.from(); i <= range.to(); i++) {
               double iCoarse = scale*i;
               for (int j = 0; j < fineSize[1]; j++) {
                    double jCoarse = scale*j;
                    for (int k = 0; k < fineSize[2]; k++) {
                         double kCoarse = scale*k;
                         // add stencil to fine grid
                         fine.set(i,j,k,DataOperations.interpolate(iCoarse, jCoarse, kCoarse, coarse, cx,cy,cz));
                    }
               }
          }
     }

     /** The stencil. */
     protected Stencil    s;
     
     /** The coarse size. */
     protected int[]        coarseSize;
     
     /** The fine size. */
     protected int[]        fineSize;
     
     /** The total range. */
     protected IntRange1D totalRange;
}
