/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import javax.vecmath.Vector2d;

import edu.jhu.cs.cisst.algorithms.util.DataOperations;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class AppearanceForce2DSmoothROF models regional appearance with the
 * Rudin-Osher-Fatemi denoising model.
 */
public class AppearanceForce2DSmoothROF extends AppearanceForce2D {

	/** The fg p-value. */
	float[][][] fgPVal;

	/** The bg p-value. */
	float[][][] bgPVal;

	/** The time step. */
	protected double timeStep = 0.01;

	/** The max iterations. */
	protected int maxIterations = 1000;

	/** The foreground smoothness. */
	protected double foregroundSmoothness = 0.1;

	/** The background smoothness. */
	protected double backgroundSmoothness = 0.1;

	/** The convergence threshold. */
	protected double convergenceThreshold = 1E-4;

	/** The heavy tolerance. */
	protected static double heavyTolerance = 1E-3;

	/**
	 * Instantiates a new appearance force2 d smooth rof.
	 * 
	 * @param image
	 *            the image
	 * @param normalizeIntensities
	 *            the normalize intensities
	 */
	public AppearanceForce2DSmoothROF(ImageDataFloat image,
			boolean normalizeIntensities) {
		super(image, normalizeIntensities);
		fgPVal = new float[rows][cols][2];
		bgPVal = new float[rows][cols][2];
	}

	/**
	 * Sets the background smoothness.
	 * 
	 * @param backgroundSmoothness
	 *            the new background smoothness
	 */
	public void setBackgroundSmoothness(double backgroundSmoothness) {
		this.backgroundSmoothness = backgroundSmoothness;
	}

	/**
	 * Sets the convergence threshold.
	 * 
	 * @param convergenceThreshold
	 *            the new convergence threshold
	 */
	public void setConvergenceThreshold(double convergenceThreshold) {
		this.convergenceThreshold = convergenceThreshold;
	}

	/**
	 * Sets the foreground smoothness.
	 * 
	 * @param foregroundSmoothness
	 *            the new foreground smoothness
	 */
	public void setForegroundSmoothness(double foregroundSmoothness) {
		this.foregroundSmoothness = foregroundSmoothness;
	}

	/**
	 * Sets the max iterations.
	 * 
	 * @param maxIterations
	 *            the new max iterations
	 */
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	/**
	 * Sets the time step.
	 * 
	 * @param timeStep
	 *            the new time step
	 */
	public void setTimeStep(double timeStep) {
		this.timeStep = timeStep;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D#update()
	 */
	public void update() {
		float[][] heavisideImage = new float[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				heavisideImage[i][j] = (float) getHeavisideValue(i, j);
			}
		}
		float[][] fgPDiv = new float[rows][cols];
		float[][] bgPDiv = new float[rows][cols];
		float[][] fgImage = new float[rows][cols];
		float[][] bgImage = new float[rows][cols];
		double val, divp, mag, fg, bg;
		Vector2d v;
		float[] pval;
		double maxError = 0;
		for (int k = 0; k < maxIterations; k++) {
			maxError = 0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					val = intensityImage[i][j];
					divp = DataOperations.divergence(fgPVal, i, j);
					fgPDiv[i][j] = (float) ((divp - val / foregroundSmoothness));
					fg = val - foregroundSmoothness * divp;
					divp = DataOperations.divergence(bgPVal, i, j);
					bgPDiv[i][j] = (float) ((divp - val / backgroundSmoothness));
					bg = val - backgroundSmoothness * divp;
					maxError = Math.max(maxError, Math.max(Math.abs(fg
							- fgImage[i][j]), Math.abs(bg - bgImage[i][j])));
					fgImage[i][j] = (float) fg;
					bgImage[i][j] = (float) bg;
				}
			}
			if (k % 10 == 0)
				System.out.println(k + ") " + maxError);
			if (maxError <= convergenceThreshold)
				break;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					double heavy = heavisideImage[i][j];

					v = DataOperations.gradient(fgPDiv, i, j);
					mag = v.length();
					pval = fgPVal[i][j];
					if (1 - heavy > heavyTolerance) {
						fgPVal[i][j][0] = (float) ((pval[0] + timeStep * v.x) / (1 + timeStep
								* mag / (1 - heavy)));
						fgPVal[i][j][1] = (float) ((pval[1] + timeStep * v.y) / (1 + timeStep
								* mag / (1 - heavy)));
					} else {
						fgPVal[i][j][0] = 0;
						fgPVal[i][j][1] = 0;
					}
					v = DataOperations.gradient(bgPDiv, i, j);
					mag = v.length();
					pval = bgPVal[i][j];
					if (heavy > heavyTolerance) {
						bgPVal[i][j][0] = (float) ((pval[0] + timeStep * v.x) / (1 + timeStep
								* mag / heavy));
						bgPVal[i][j][1] = (float) ((pval[1] + timeStep * v.y) / (1 + timeStep
								* mag / heavy));
					} else {
						bgPVal[i][j][0] = 0;
						bgPVal[i][j][1] = 0;
					}

				}
			}
		}

		float[][] intensityEstimateImage = imageEstimate.toArray2d();
		double maxHeavyVal = heavisideDerivative(0, fuzziness, heaviside);
		double invMaxHeavyVal = 1 / maxHeavyVal;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double fgGradMag = DataOperations.gradientMagnitude(fgImage, i,
						j);
				double bgGradMag = DataOperations.gradientMagnitude(bgImage, i,
						j);

				double heavy = heavisideImage[i][j];
				val = intensityImage[i][j];
				fg = fgImage[i][j];
				bg = bgImage[i][j];
				intensityEstimateImage[i][j] = (float) ((1 - heavy) * fg + heavy
						* bg);
				double heavyDeriv = invMaxHeavyVal
						* heavisideDerivative(levelset[i][j], fuzziness,
								heaviside);
				pressureForce[i][j] = (float) (heavyDeriv * (-fgWeight
						* ((0.5) * (val - fg) * (val - fg) + foregroundSmoothness
								* fgGradMag) + bgWeight
						* ((0.5) * (val - bg) * (val - bg) + backgroundSmoothness
								* bgGradMag)));
			}
		}
	}

}
