/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.grid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * An implementation of BoundaryGrid where all the boundary elements are set once at construction time to a
 * single, common value and remain fixed thereafter.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class FixedBoundaryGrid extends BoundaryGrid {
     
     /**
      * construct from size, initial value for all elements in the interior and initial value for all elements
      * at the boundary.
      * 
      * @param sx the dimension in X
      * @param sy the dimension in Y
      * @param sz the dimension in Z
      * @param level the resolution level
      * @param interiorValue the value to which all interior grid elements will be set
      * @param boundaryValue the value to which all boundary grid elements will be set
      */
     public FixedBoundaryGrid(int sx,int sy,int sz, int level,double interiorValue, double boundaryValue) {
          super(sx,sy,sz,level,boundaryValue);
          this.boundaryValue = boundaryValue;
          for (int i = 1; i < (sx - 1); i++) {
               for (int j = 1; j < (sy - 1); j++) {
                    for (int k = 1; k < (sz - 1); k++) {
                         g[i][j][k] = interiorValue;
                    }
               }
          }
     }

     /**
      * implements method from ConstGrid.
      * 
      * @param sx the dimension in X
      * @param sy the dimension in Y
      * @param sz the dimension in Z  
      * @param level the resolution level 
      * @param value the value
      * 
      * @return the grid
      * 
      * @see ConstGrid#newInstance
      */
     public Grid newInstance(int sx,int sy,int sz,int level, double value) {
          return new FixedBoundaryGrid(sx,sy,sz,level,value,boundaryValue);
     }

     /**
      * implements method from Grid.
      * 
      * the given grid element must lie at the boundary of the grid
      * 
      * @param x the x
      * @param y the y
      * @param z the z
      * 
      * @return the boundary
      * 
      * @see Grid#getBoundary
      */
     protected double getBoundary(int x, int y, int z) {
          Contract.pre(isBoundary(x,y,z),"grid element must be part of the boundary");
          
          return g[x][y][z];
     }
     
     /** The boundary value. */
     private double boundaryValue;
}
