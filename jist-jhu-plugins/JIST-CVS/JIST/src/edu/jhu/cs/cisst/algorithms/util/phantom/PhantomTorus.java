package edu.jhu.cs.cisst.algorithms.util.phantom;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

public class PhantomTorus extends PhantomSimulator3D {
	/** The center. */
	protected Point3d center;

	/** The inner radius. */
	protected double innerRadius;

	/** The outer radius */
	protected double outerRadius;

	/**
	 * Sets the center.
	 * 
	 * @param center
	 *            the new center
	 */
	public void setCenter(Point3d center) {
		this.center = center;
	}

	/**
	 * Sets the outer radius.
	 * 
	 * @param radius
	 *            the new radius
	 */
	public void setOuterRadius(double outerRadius) {
		this.outerRadius = outerRadius;
	}

	/**
	 * Sets the inner radius.
	 * 
	 * @param radius
	 *            the new radius
	 */
	public void setInnerRadius(double innerRadius) {
		this.innerRadius = innerRadius;
	}

	/**
	 * Instantiates a new phantom torus.
	 * 
	 * @param dims
	 *            the dims
	 */
	public PhantomTorus(Point3i dims) {
		super(dims);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.jhu.cs.cisst.algorithms.util.phantom.PhantomSimulator3D#solve()
	 */
	public void solve() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double x = (2 * i / (double) rows - 1);
					double y = (2 * j / (double) cols - 1);
					double z = (2 * k / (double) slices - 1);
					double xp = (x - center.x);
					double yp = (y - center.y);
					double zp = (z - center.z);
					double tmp = (outerRadius - Math.sqrt(zp * zp + yp * yp));
					levelset.set(i, j, k, tmp * tmp + xp * xp - innerRadius
							* innerRadius);
				}
			}
		}
		levelset.setName("torus_level");
		image.setName("torus");
		finish();
	}
}
