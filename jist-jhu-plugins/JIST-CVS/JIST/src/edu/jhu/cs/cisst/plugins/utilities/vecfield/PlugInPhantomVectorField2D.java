/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.utilities.vecfield;

import javax.vecmath.Vector2f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class PlugInPhantomVectorField2D extends ProcessingAlgorithm{

	protected ParamDouble curlParam;
	protected ParamDouble divergenceParam;
	protected ParamDouble horizontalForceParam;
	protected ParamDouble verticalForceParam;
	protected ParamDouble singularityXParam;
	protected ParamDouble singularityYParam;
	protected ParamVolume referenceImage;
	protected ParamInteger imageWidthParam;
	protected ParamInteger imageHeightParam;
	protected ParamVolume vectorFieldImage;
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(referenceImage=new ParamVolume("Reference Image"));
		referenceImage.setMandatory(false);
		inputParams.add(imageWidthParam=new ParamInteger("Image Width",0,(int)1E6,640));
		inputParams.add(imageHeightParam=new ParamInteger("Image Height",0,(int)1E6,480));
		inputParams.add(curlParam=new ParamDouble("Rotation",0));
		inputParams.add(divergenceParam=new ParamDouble("Divergence",0));
		inputParams.add(horizontalForceParam=new ParamDouble("Horizontal Force",0));
		inputParams.add(verticalForceParam=new ParamDouble("Verticeal Force",0));
		inputParams.add(singularityXParam=new ParamDouble("Singularity X Position",0));
		inputParams.add(singularityYParam=new ParamDouble("Singularity Y Position",0));
		inputParams.setName("phantom_vecfield2D");
		inputParams.setLabel("Phantom Vector Field 2D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Vector Field");
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(vectorFieldImage=new ParamVolume("Vector Field"));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		double curl=curlParam.getDouble();
		double divergence=divergenceParam.getDouble();
		double horizontalForce=horizontalForceParam.getDouble();
		double verticalForce=verticalForceParam.getDouble();
		double sx=singularityXParam.getDouble();
		double sy=singularityYParam.getDouble();
		int width,height;
		ImageData img;
		if((img=referenceImage.getImageData())!=null){
			width=img.getRows();
			height=img.getCols();
		} else {
			width=imageWidthParam.getInt();
			height=imageHeightParam.getInt();
		}
		ImageDataFloat vecfield=new ImageDataFloat(width,height,2);
		vecfield.setName("vecfield");
		float[][][] vecfieldMat=vecfield.toArray3d();
		double x, y;
		Vector2f curlV=new Vector2f();
		Vector2f divV=new Vector2f();
		Vector2f transForce=new Vector2f((float)horizontalForce,(float)verticalForce);
		
		double scale=2.0/Math.max(width, height);
		for(int i=0;i<width;i++){
			for(int j=0;j<height;j++){
				x=i*scale-1-sx;
				y=j*scale-1-sy;
				curlV.x=(float)(-y);
				curlV.y=(float)(x);
				//if(curlV.length()>0)curlV.normalize();
				curlV.scale((float)curl);		
				
				divV.x=(float)(x);
				divV.y=(float)(y);
				if(divV.length()>0)divV.normalize();
				divV.scale((float)divergence);
				
				vecfieldMat[i][j][0]=curlV.x+divV.x+transForce.x;
				vecfieldMat[i][j][1]=curlV.y+divV.y+transForce.y;
			}
		}
		vectorFieldImage.setValue(vecfield);
	}

}
