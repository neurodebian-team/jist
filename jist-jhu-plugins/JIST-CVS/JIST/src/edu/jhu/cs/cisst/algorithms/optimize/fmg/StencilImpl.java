/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * A concrete implementation of interface Stencil.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class StencilImpl implements Stencil {
     
     /**
      * construct from the 27 double values in an array.
      * 
      * @param array a 3x3x3 array holding the 27 double values for the stencil
      */
     public StencilImpl(double[][][] array) {
          Contract.pre(array.length    == 3 &&
                       array[0].length == 3 && array[0][0].length == 3 && array[0][2].length == 3 &&
                       array[2].length == 3 && array[2][0].length == 3 && array[2][2].length == 3,
                       "array must be a 3x3x3 array");
          
          for (int i = 0; i < 3; i++) {
               for (int j = 0; j < 3; j++) {
                    for (int k = 0; k < 3; k++) {
                         g[i][j][k] = array[i][j][k];
                    }
               }
          }
     }

     /**
      * construct from the 27 double values.
      * 
      * @param d01 double value for position (-1,-1,-1) of the stencil
      * @param d02 double value for position (-1,-1,0) of the stencil
      * @param d26 double value for position (1,1,0) of the stencil
      * @param d27 double value for position (1,1,1) of the stencil
      * @param d03 the d03
      * @param d04 the d04
      * @param d05 the d05
      * @param d06 the d06
      * @param d07 the d07
      * @param d08 the d08
      * @param d09 the d09
      * @param d10 the d10
      * @param d11 the d11
      * @param d12 the d12
      * @param d13 the d13
      * @param d14 the d14
      * @param d15 the d15
      * @param d16 the d16
      * @param d17 the d17
      * @param d18 the d18
      * @param d19 the d19
      * @param d20 the d20
      * @param d21 the d21
      * @param d22 the d22
      * @param d23 the d23
      * @param d24 the d24
      * @param d25 the d25
      */
     public StencilImpl(double d01, double d02, double d03, double d04, double d05, double d06,
                        double d07, double d08, double d09, double d10, double d11, double d12,
                        double d13, double d14, double d15, double d16, double d17, double d18,
                        double d19, double d20, double d21, double d22, double d23, double d24,
                        double d25, double d26, double d27) {
     g[0][0][0] = d01; g[0][0][1] = d02; g[0][0][2] = d03;
     g[0][1][0] = d04; g[0][1][1] = d05; g[0][1][2] = d06;
     g[0][2][0] = d07; g[0][2][1] = d08; g[0][2][2] = d09;
     g[1][0][0] = d10; g[1][0][1] = d11; g[1][0][2] = d12;
     g[1][1][0] = d13; g[1][1][1] = d14; g[1][1][2] = d15;
     g[1][2][0] = d16; g[1][2][1] = d17; g[1][2][2] = d18;
     g[2][0][0] = d19; g[2][0][1] = d20; g[2][0][2] = d21;
     g[2][1][0] = d22; g[2][1][1] = d23; g[2][1][2] = d24;
     g[2][2][0] = d25; g[2][2][1] = d26; g[2][2][2] = d27;
     }

     /**
      * implements method from Stencil.
      * 
      * @param x the x
      * @param y the y
      * @param z the z
      * 
      * @return the double
      * 
      * @see Stencil#get
      */
     public double get(int x, int y, int z) {
          return g[x + 1][y + 1][z + 1];
     }

     /** The g. */
     private double[][][] g = new double[3][3][3];
}
