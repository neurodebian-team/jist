/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;
import  java.util.*;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;



/**
 * The default observer for observables of class FMG.
 * <p>
 * One object of this class is by default registered as an observer of class FMG (the observable). This is used to
 * implement method FMG.waitForResult() within the observer/observable approach.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */

public class FMGDefaultObserver implements Observer {
     
     /**
      * will be called if the FMG algorithm (invoced via FMG.fmg()) has produced a result.
      * <p>
      * The result of the FMG algorithm is stored inside this object and can be retrieved (from FMG.waitForResult()) via
      * method getResult().
      * 
      * @param o the o
      * @param arg the arg
      * 
      * @see FMG#waitForResult
      * @arg o   the object of type FMG that we observe
      * @arg arg the result of of the FMG algorithm which is of type BoundaryGrid
      */
     public synchronized void update(Observable o, Object arg) {
     	Contract.pre(o instanceof FMG,"o of type FMG");
     	Contract.pre(arg instanceof BoundaryGrid,"arg of type BoundaryGrid");
     	
     	result = (BoundaryGrid) arg;
     	notifyAll();
     }
     
     /**
      * return the result of the FMG algorithm that was or will be delivered to this object by calling update().
      * <p>
      * If no result is available, this method blocks the current thread until a result becomes available.
      * <p>
      * Each result may be retrieved exactly once.
      * 
      * @return the result of the FMG algorithm
      */
     public synchronized BoundaryGrid getResult() {
          while (result == null) {
               try {wait();} catch (InterruptedException e) {}
          }
     	BoundaryGrid returnValue = result;
     	result = null;
     	return returnValue;
     }
     
     /** The result. */
     private BoundaryGrid result = null;
}
