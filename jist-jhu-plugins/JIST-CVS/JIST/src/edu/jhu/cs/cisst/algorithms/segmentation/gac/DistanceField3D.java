package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import edu.jhu.ece.iacl.algorithms.volume.DistanceField;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;

public class DistanceField3D extends DistanceField {
	public DistanceField3D() {
		super();
	}

	public DistanceField3D(AbstractCalculation parent) {
		super(parent);
	}
}
