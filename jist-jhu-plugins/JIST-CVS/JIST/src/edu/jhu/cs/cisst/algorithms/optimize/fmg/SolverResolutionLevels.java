/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;

// TODO: Auto-generated Javadoc
/**
 * The Interface SolverResolutionLevels.
 */
public interface SolverResolutionLevels {
	
	/**
	 * Gets the resolutions.
	 * 
	 * @return the resolutions
	 */
	public int[][] getResolutions();
	
	/**
	 * Gets the resolution count.
	 * 
	 * @return the resolution count
	 */
	public int getResolutionCount();
	
	/**
	 * Gets the resolution.
	 * 
	 * @param i the i
	 * 
	 * @return the resolution
	 */
	public int[] getResolution(int i);
	
	/**
	 * Gets the grid spacing.
	 * 
	 * @param i the i
	 * 
	 * @return the grid spacing
	 */
	public double getGridSpacing(int i);
}
