/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.FMG;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.Grid;

/**
 * This interface defines the restrict() method which produces a coarse grid from the next finer grid by restriction.
 * <p>
 * The Full Multigrid (FMG) algorithm works with grids of different sizes corresponding to different levels through
 * the relation size = 2^level + 1, where size is the number of grid elements in one dimension. Restriction is the
 * process of taking a function whose value is known at the grid elements of a grid at a certain level (the fine grid)
 * and producing the values of the function at the grid elements of a grid at the next coarser level (the coarse grid).
 * The fine grid and the coarse grid are oriented such that their corner elements coincide.
 * 
 * @see FMG
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public interface Restrictor {
     
     /**
      * restricts a function sampled on a specific grid to the next coarser grid.
      * 
      * @param grid the input function sampled on a grid at a certain level (size of grid must be 2^level + 1 where
      * level > 1)
      * @param levels the levels
      * 
      * @return the function sampled on a grid at the next coarser (lower) level
      */
     public Grid restrict(ConstGrid grid,SolverResolutionLevels levels);
}
