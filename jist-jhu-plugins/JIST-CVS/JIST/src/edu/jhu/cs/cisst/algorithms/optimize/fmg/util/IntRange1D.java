/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.util;

// TODO: Auto-generated Javadoc
/**
 * A class to represent a one-diensional range of integer values.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */

public class IntRange1D {
     
     /**
      * construct from lower and upper boundary.
      * <p>
      * It is perfectly legal for lower boundary value to be greater then the upper boundary value.
      * <p>
      * The upper boundary is considered being part of the range.
      * 
      * @param lower the lower boundary of the range
      * @param upper the upper boundary of the range
      */
     public IntRange1D(int lower, int upper) {
          this.lower = lower;
          this.upper = upper;
     }

     /**
      * return lower boundary.
      * 
      * @return the lower boundary of the range
      */
     public int from() {
          return lower;
     }

     /**
      * return upper boundary.
      * 
      * @return the upper boundary of the range
      */
     public int to() {
          return upper;
     }

     /**
      * return the number of integer values lying in the range.
      * <p>
      * The size is defined as lower - upper + 1, i.e. the upper boundary is part of the range.
      * 
      * @return the size of the range which is upper - lower + 1
      */
     public int size() {
          return (upper - lower + 1);
     }

     /** The lower. */
     private int lower;
     
     /** The upper. */
     private int upper;
}
