/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.FMG;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;

// TODO: Auto-generated Javadoc
/**
 * This interface defines the smooth() method which is used for smoothing the approximate solution of a linear elliptic
 * partial differential equation (PDE) on a regular cubic grid in 3D before and/or after going to the next coarser grid
 * in the Multigrid (MG) algorithm.
 * <p>
 * In the theory of the MG algorithm, smoothing refers to the reduction of the high-frequency components of the error of
 * an approximate solution of a PDE. This is usuallu achieved by applying one or more sweeps of a conventional
 * relaxation algorithm. This algorithms are usually good in reducing the high-frequency components of the error, but
 * bad in reducing the low-frequency components of the error, which is the reason why they are slow if applied in
 * isolation to solve a PDE. However, this property is important in the context of the MG algorithm.
 * <p>
 * The method smooth() is intended to apply one sweep of a smoothing algorithm.
 * 
 * @see FMG
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public interface Smoother {
     
     /**
      * smooth an approximate solution to a linear elliptic PDE on a regular cubic grid in 3D.
      * <p>
      * Let the elliptic PDE be represented by Ax = f, and let u be an approximation to the exact solution x,
      * then this method smoothes (i.e. reduces the high-frequency components of the error of) u.
      * 
      * @param u      the approximate solution of the PDE sampled at a grid of a certain size
      * @param f      the right hand side of the PDE sampled at a grid of the same size
      * 
      * @return the smmothed approximate solution of the PDE
      * 
      * @see PDE
      */
     BoundaryGrid smooth(ConstBoundaryGrid u, ConstNoBoundaryGrid f);
}
