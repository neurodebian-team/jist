/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * A helper class of class FMG that implements the run() method of class thread.
 * <p>
 * Creating an object of this class means creating a new thread and immediately starting it. The run() method of this
 * thread than simply calls method FMG.fmgProper() of the FMG object that it is helping.
 */
public final class FMGThread extends Thread {
     
     /**
      * construct from target FMG object and from the parameters that will be passed on to method FMG.fmgProper() and
      * immediately start the thread, i.e. immediately start executing FMG.fmgProper().
      * 
      * @param fmg             the target FMG object, i.e. the object whose fmgProper() method will be called from run()
      * @param numPresmooth the num presmooth
      * @param numPostsmooth the num postsmooth
      * @param cyclingStrategy the cycling strategy
      * @param numMultiGrid the num multi grid
      */
     public FMGThread(FMG fmg,int numPresmooth, int numPostsmooth, 
                      int cyclingStrategy, int numMultiGrid) {
          this.fmg              = fmg;
          this.numPresmooth     = numPresmooth;
          this.numPostsmooth    = numPostsmooth;
          this.cyclingStrategy  = cyclingStrategy;
          this.numMultiGrid     = numMultiGrid;
          
          start();
     }
     
     /**
      * re-routes to FMG.fmgProper()
      * <p>
      * implements method from Thread.
      * 
      * @see Thread#run
      * @see FMG#fmgProper
      */
     public void run() {
          fmg.solve(numPresmooth,numPostsmooth,cyclingStrategy,numMultiGrid);
     }
     
     /** The fmg. */
     private FMG fmg;
     
     /** The num presmooth. */
     private int numPresmooth;
     
     /** The num postsmooth. */
     private int numPostsmooth;
     
     /** The cycling strategy. */
     private int cyclingStrategy;
     
     /** The num multi grid. */
     private int numMultiGrid;
}
