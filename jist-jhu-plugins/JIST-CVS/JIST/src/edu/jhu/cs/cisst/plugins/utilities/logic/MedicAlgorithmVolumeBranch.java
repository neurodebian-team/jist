/*
 *
 */
package edu.jhu.cs.cisst.plugins.utilities.logic;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmVolumeBranch extends ProcessingAlgorithm{
	private ParamVolume volTrue;
	private ParamVolume volFalse;
	private ParamVolume outVol;
	private ParamBoolean branchParam;
	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(outVol=new ParamVolume("Volume"));
		inputParams.add(branchParam=new ParamBoolean("Assertion",true));
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Volume Branch");
		inputParams.setName("volume_branch");
		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription("An if/then/else condition for forwarding a volume.");
		info.add(CommonAuthors.blakeLucas);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(volTrue=new ParamVolume("Volume for True"));
		outputParams.add(volFalse=new ParamVolume("Volume for False"));
		volTrue.setMandatory(false);
		volFalse.setMandatory(false);
	}
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		if(branchParam.getValue()){
			volTrue.setValue(outVol.getValue());
		} else {
			volFalse.setValue(outVol.getValue());
		}
	}
}
