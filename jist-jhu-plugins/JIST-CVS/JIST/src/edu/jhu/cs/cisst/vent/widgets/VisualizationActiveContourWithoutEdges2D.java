/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.widgets;

import java.util.ArrayList;
import java.util.LinkedList;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.cs.cisst.vent.VisualizationParameters;
import edu.jhu.cs.cisst.vent.VisualizationProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.ImageRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VectorFieldRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeIsoContourRenderer;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationActiveContour2D.
 */
public class VisualizationActiveContourWithoutEdges2D extends VisualizationImage2D{
	
	/** The rule. */
	protected TopologyRule2D.Rule rule;
	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.9 $");
	}
	
	/** The vector field image. */
	ImageDataFloat levelset=null,estimatedImage=null,vectorFieldImage=null;
	
	/** The original image. */
	ImageData originalImage=null;
	
	/**
	 * Instantiates a new visualization active contour.
	 *
	 * @param levelset the levelset
	 * @param rule the rule
	 */
	public VisualizationActiveContourWithoutEdges2D(ImageDataFloat levelset,TopologyRule2D.Rule rule) {
		super(4*levelset.getRows(),4*levelset.getCols(),levelset.getRows(),levelset.getCols());
		this.levelset=levelset;
		this.rule=rule;
		setName("Visualization - Active Contour 2D");
	}	
	
	/**
	 * Instantiates a new visualization active contour.
	 *
	 * @param width the width
	 * @param height the height
	 * @param levelset the levelset
	 */
	public VisualizationActiveContourWithoutEdges2D(int width,int height,ImageDataFloat levelset) {
		super(width,height,levelset.getRows(),levelset.getCols());
		this.levelset=levelset;
		setName("Visualization - Active Contour 2D");
	}
	
	/** The slice renderer. */
	protected VolumeSliceRenderer2D sliceRenderer=null;
	
	/** The iso renderer. */
	protected VolumeSliceRenderer2D isoRenderer=null;
	
	/**
	 * Creates the.
	 *
	 * @return the param collection
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#create()
	 */
	@Override
	public ParamCollection create(){
		renderers=new ArrayList<RendererProcessing2D>();

		if(originalImage!=null){
			renderers.add(new ImageRenderer2D(originalImage,this));
		}
		if(estimatedImage!=null){
			renderers.add(sliceRenderer=new VolumeSliceRenderer2D(estimatedImage,this));
		}
		if(levelset!=null){
			isoRenderer=new VolumeIsoContourRenderer(levelset,rule,this);
			renderers.add(isoRenderer);
			isoRenderer.setVisible(false);
		}
		

		ParamCollection params=super.create();
		if(isoRenderer!=null&&sliceRenderer!=null){
			isoRenderer.getSliceParameter().getInputView().addObserver(new ParamViewObserver(){
				public void update(ParamModel model, ParamInputView view) {
					int isoSlice=isoRenderer.getSliceParameter().getInt();
					int estSlice=sliceRenderer.getSliceParameter().getInt();
					if(isoSlice!=estSlice){
						sliceRenderer.getSliceParameter().setValue(isoSlice);
						sliceRenderer.getSliceParameter().getInputView().update();
					}
				}
			});
			sliceRenderer.getSliceParameter().getInputView().addObserver(new ParamViewObserver(){
				public void update(ParamModel model, ParamInputView view) {
					int isoSlice=isoRenderer.getSliceParameter().getInt();
					int estSlice=sliceRenderer.getSliceParameter().getInt();
					if(isoSlice!=estSlice){
						isoRenderer.getSliceParameter().setValue(estSlice);
						isoRenderer.getSliceParameter().getInputView().update();
					}
				}
			});
		}
		return params;
	}
	
	/**
	 * Sets the pressure force.
	 * 
	 * @param estimatedImage the new pressure force
	 */
	public void setEstimatedImage(ImageDataFloat estimatedImage){
		this.estimatedImage=estimatedImage;
	}
	

	/**
	 * Sets the original image.
	 * 
	 * @param img the new original image
	 */
	public void setOriginalImage(ImageData img){
		this.originalImage=img;
	}
}
