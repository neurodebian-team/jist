/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.imageproc;

import edu.jhu.cs.cisst.algorithms.imageproc.Morphology2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInMorphology2D.
 */
public class PlugInMorphology2D extends ProcessingAlgorithm{
	
	/** The method param. */
	protected ParamOption methodParam;
	
	/** The kernel type. */
	protected ParamOption kernelType;
	
	/** The dilation iterations param. */
	protected ParamInteger dilationIterationsParam;
	
	/** The erosion iterations param. */
	protected ParamInteger erosionIterationsParam;
	
	/** The kernel size. */
	protected ParamFloat kernelSize;
	
	/** The image param. */
	protected ParamVolume imageParam;
	
	/** The result param. */
	protected ParamVolume resultParam;
	
	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(imageParam=new ParamVolume("Image",null,-1,-1,-1,-1));
		inputParams.add(methodParam=new ParamOption("Operation",Morphology2D.operationNames));
		inputParams.add(dilationIterationsParam=new ParamInteger("Dilation Iterations",0,1000,1));
		inputParams.add(erosionIterationsParam=new ParamInteger("Erosion Iterations",0,1000,1));
		inputParams.add(kernelType=new ParamOption("Kernel",Morphology2D.kernelNames));
		inputParams.add(kernelSize=new ParamFloat("Kernel Size",0,1E6f,5));
		inputParams.setName("morphology2d");
		inputParams.setLabel("Morphology 2D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Image Processing");
		AlgorithmInformation info=getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setVersion(Morphology2D.getVersion());
		info.setDescription("A wrapper around MIPAV's gray level morphology operators.");
		info.setStatus(DevelopmentStatus.Release);
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultParam=new ParamVolume("Result",null,-1,-1,-1,0));
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		Morphology2D morph=new Morphology2D();
		morph.setDilationIterations(dilationIterationsParam.getInt());
		morph.setErosionIterations(erosionIterationsParam.getInt());
		morph.setKernelSize(kernelSize.getFloat());
		morph.setKernel(Morphology2D.Kernel.values()[kernelType.getIndex()]);
		resultParam.setValue(morph.solve(imageParam.getImageData(),Morphology2D.Operation.values()[methodParam.getIndex()]));
	}

}
