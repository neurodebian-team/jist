/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent;

import java.awt.Dimension;
import java.awt.Image;
import java.util.ArrayList;
import java.util.LinkedList;

import processing.core.PImage;

import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing3D;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationProcessing3D.
 */
public abstract class VisualizationProcessing3D extends VisualizationProcessing {

	/** The renderers. */
	protected ArrayList<RendererProcessing3D> renderers;

	/**
	 * Adds a new processing renderer to the rendering pipeline.
	 * 
	 * @param renderer
	 *            the renderer
	 */
	public void add(RendererProcessing3D renderer) {
		renderers.add(renderer);
	}

	/**
	 * Instantiates a new visualization processing 3d.
	 */
	public VisualizationProcessing3D() {
		super();
		renderers = new ArrayList<RendererProcessing3D>();
	}

	/**
	 * Instantiates a new visualization processing 3d.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 */
	public VisualizationProcessing3D(int width, int height) {
		super(width, height);
		renderers = new ArrayList<RendererProcessing3D>();
	}

	/**
	 * Setup.
	 * 
	 * @see processing.core.PApplet#setup()
	 */
	@Override
	public void setup() {
		super.setup();
		for (RendererProcessing renderer : renderers) {
			renderer.setVisualization(this);
			renderer.setup();
		}
		loop();
	}

	/**
	 * Draw.
	 * 
	 * @see processing.core.PApplet#draw()
	 */
	@Override
	public void draw() {
		for (RendererProcessing renderer : renderers) {
			renderer.draw();
		}
		if (requestScreenShot) {
			screenshot = get();
			requestScreenShot = false;
		}
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters
	 *            the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationParameters#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		super.createVisualizationParameters(visualizationParameters);
		for (RendererProcessing renderer : renderers) {
			ParamCollection pane = new ParamCollection();
			renderer.createVisualizationParameters(pane);
			visualizationParameters.add(pane);
		}
	}

	/**
	 * Update.
	 * 
	 * @param model
	 *            the model
	 * @param view
	 *            the view
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
	 *      edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	public void update(ParamModel model, ParamInputView view) {
		for (RendererProcessing renderer : renderers) {
			renderer.update(model, view);
		}
	}

	/**
	 * Update visualization parameters.
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationParameters#updateVisualizationParameters()
	 */
	public void updateVisualizationParameters() {
		for (RendererProcessing renderer : renderers) {
			renderer.updateVisualizationParameters();
		}
	}

	/**
	 * Gets the screenshot.
	 * 
	 * @return the screenshot
	 * 
	 * @see edu.jhu.cs.cisst.vent.Visualization#getScreenshot()
	 */
	public Image getScreenshot() {
		screenshot = null;
		requestScreenShot = true;
		while (requestScreenShot) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return screenshot.getImage();
	}

}
