/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.DistanceField3D;

/**
 * The Class GeodesicActiveContourWithoutEdges3D extends GAC by adding support
 * for appearance forces. This subclass is necessary because the appearance
 * force requires the narrow-band to be rebuilt after each outer iteration.
 */
public class GeodesicActiveContourWithoutEdges3D extends
		GeodesicActiveContour3D {

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.3 $");
	}

	/** The verbose estimated image. */
	protected ImageDataFloat verboseEstimatedImage;

	/** The estimated image. */
	protected ImageDataFloat estimatedImage;

	/**
	 * Instantiates a new geodesic active contour without edges2 d.
	 * 
	 * @param referenceImage
	 *            the reference image
	 */
	public GeodesicActiveContourWithoutEdges3D(ImageData referenceImage) {
		super(referenceImage);
	}

	/**
	 * Gets the estimated image.
	 * 
	 * @return the estimated image
	 */
	public ImageDataFloat getEstimatedImage() {
		return estimatedImage;
	}

	/**
	 * Gets the verbose estimated image.
	 * 
	 * @return the verbose estimated image
	 */
	public ImageDataFloat getVerboseEstimatedImage() {
		return verboseEstimatedImage;
	}

	/**
	 * Solve.
	 * 
	 * @param initialLevelset
	 *            the initial level set
	 * 
	 * @return the final level set
	 */
	public ImageDataFloat solve(ImageDataFloat initialLevelset) {
		double dice = 0;
		posToNegCount = negToPosCount = 0;
		estimatedImage = new ImageDataFloat(rows, cols, slices);
		estimatedImage.setName(image.getName() + "_estimated");
		initNarrowBand(initialLevelset);

		DistanceField3D dist = new DistanceField3D();
		for (ActiveContourForce3D force : forces) {
			if (force instanceof AppearanceForce3D) {
				((AppearanceForce3D) force).setLevelSet(distField);
			}
		}
		setTotalUnits(outerIterations);
		for (int outerIter = 0; outerIter < outerIterations; outerIter++) {
			for (GridPoint3D pt : activePoints) {
				distField[pt.x][pt.y][pt.z] = pt.value;
			}
			for (int k = 0; k < maxLayers; k++) {
				for (GridPoint3D pt : insidePoints[k]) {
					distField[pt.x][pt.y][pt.z] = pt.value;
				}
				for (GridPoint3D pt : outsidePoints[k]) {
					distField[pt.x][pt.y][pt.z] = pt.value;
				}
			}
			// Update forces
			for (ActiveContourForce3D force : forces) {
				force.update();
			}

			for (int innerIter = 0; innerIter < innerIterations; innerIter++) {
				updateNarrowBand();
			}
			dice = 2
					* (insideCount - negToPosCount)
					/ (double) (2 * insideCount + posToNegCount - negToPosCount);
			// Update number of inside points
			insideCount = insideCount + posToNegCount - negToPosCount;
			posToNegCount = negToPosCount = 0;
			setLabel(String.format("Dice Coeff: %1.4f", dice));
			System.out.println(outerIter + ") DICE "
					+ String.format("Dice Coeff: %1.4f", dice));
			if (dice >= diceThreshold) {
				break;
			}
			incrementCompletedUnits();
		}
		setLabel("Active Contour");
		// Rebuild signed distance function
		for (GridPoint3D pt : activePoints) {
			distField[pt.x][pt.y][pt.z] = pt.value;
		}
		for (int k = 0; k < maxLayers; k++) {
			for (GridPoint3D pt : insidePoints[k]) {
				distField[pt.x][pt.y][pt.z] = pt.value;
			}
			for (GridPoint3D pt : outsidePoints[k]) {
				distField[pt.x][pt.y][pt.z] = pt.value;
			}
		}

		distFieldImage = dist.solve(distFieldImage, maxLayers + 0.5);
		distFieldImage.setName(image.getName() + "_levelset");
		distFieldImage.setHeader(image.getHeader());
		distField = distFieldImage.toArray3d();
		for (ActiveContourForce3D force : forces) {
			if (force instanceof AppearanceForce3D)
				((AppearanceForce3D) force).setLevelSet(distField);
			force.update();
			if (force instanceof AppearanceForce3D) {
				ImageDataFloat img = ((AppearanceForce3D) force)
						.getImageEstimate();
				float[][][] tmp = estimatedImage.toArray3d();
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						for (int k = 0; k < slices; k++) {
							tmp[i][j][k] = img.getFloat(i, j, k);
						}
					}
				}

			}
		}
		markCompleted();
		return distFieldImage;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContour3D#solveVerbose(edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat)
	 */
	public ImageDataFloat solveVerbose(ImageDataFloat initialLevelSet) {
		double dice = 0;
		posToNegCount = negToPosCount = 0;
		initNarrowBand(initialLevelSet);
		verboseDistFieldImage = new ImageDataFloat(rows, cols, slices,
				outerIterations);
		verboseEstimatedImage = new ImageDataFloat(rows, cols, slices,
				outerIterations);
		verboseDistFieldImage.setName(image.getName() + "_levelset_verbose");
		verboseEstimatedImage.setName(image.getName() + "_estimated_verbose");
		estimatedImage = new ImageDataFloat(rows, cols, slices);
		estimatedImage.setName(image.getName() + "_estimated");
		float[][][][] verboseDistFieldMat = verboseDistFieldImage.toArray4d();
		int outerIter = 0;
		for (ActiveContourForce3D force : forces) {
			if (force instanceof AppearanceForce3D) {
				((AppearanceForce3D) force).setLevelSet(distField);
			}
		}
		setTotalUnits(outerIterations);
		for (outerIter = 0; outerIter < outerIterations; outerIter++) {
			// Update forces
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						distField[i][j][k] = verboseDistFieldMat[i][j][k][outerIter] = maxLayers
								* Math.signum(distField[i][j][k]);
					}
				}
			}
			for (GridPoint3D pt : activePoints) {
				distField[pt.x][pt.y][pt.z] = verboseDistFieldMat[pt.x][pt.y][pt.z][outerIter] = pt.value;
			}
			for (int k = 0; k < maxLayers; k++) {
				for (GridPoint3D pt : insidePoints[k]) {
					distField[pt.x][pt.y][pt.z] = verboseDistFieldMat[pt.x][pt.y][pt.z][outerIter] = pt.value;
				}
				for (GridPoint3D pt : outsidePoints[k]) {
					distField[pt.x][pt.y][pt.z] = verboseDistFieldMat[pt.x][pt.y][pt.z][outerIter] = pt.value;
				}
			}
			for (ActiveContourForce3D force : forces) {
				force.update();
				if (force instanceof AppearanceForce3D) {
					AppearanceForce3D af = (AppearanceForce3D) force;
					ImageDataFloat img = ((AppearanceForce3D) force)
							.getImageEstimate();
					float[][][][] tmp = verboseEstimatedImage.toArray4d();
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							for (int k = 0; k < slices; k++) {
								tmp[i][j][k][outerIter] = img.getFloat(i, j, k);
							}
						}
					}

				}
			}
			if (dice >= diceThreshold) {
				break;
			}
			for (int innerIter = 0; innerIter < innerIterations; innerIter++) {
				updateNarrowBand();
			}
			dice = 2
					* (insideCount - negToPosCount)
					/ (double) (2 * insideCount + posToNegCount - negToPosCount);
			insideCount = insideCount + posToNegCount - negToPosCount;
			posToNegCount = negToPosCount = 0;
			setLabel(String.format("Dice Coeff: %1.4f", dice));
			incrementCompletedUnits();
		}
		// Rebuild narrow-band
		DistanceField3D dist = new DistanceField3D();
		for (GridPoint3D pt : activePoints) {
			distField[pt.x][pt.y][pt.z] = pt.value;
		}
		for (int k = 0; k < maxLayers; k++) {
			for (GridPoint3D pt : insidePoints[k]) {
				distField[pt.x][pt.y][pt.z] = pt.value;
			}
			for (GridPoint3D pt : outsidePoints[k]) {
				distField[pt.x][pt.y][pt.z] = pt.value;
			}
		}

		distFieldImage = dist.solve(distFieldImage, maxLayers + 0.5);
		distFieldImage.setName(image.getName() + "_levelset");
		distFieldImage.setHeader(image.getHeader());
		distField = distFieldImage.toArray3d();
		for (ActiveContourForce3D force : forces) {
			force.update();
			if (force instanceof AppearanceForce3D) {
				((AppearanceForce3D) force).setLevelSet(distField);
				ImageDataFloat img = ((AppearanceForce3D) force)
						.getImageEstimate();
				float[][][] tmp = estimatedImage.toArray3d();
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						for (int k = 0; k < slices; k++) {
							tmp[i][j][k] = img.getFloat(i, j, k);
						}
					}
				}
			}
		}

		markCompleted();
		return distFieldImage;
	}
}
