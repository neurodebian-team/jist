/*
 * 
 */
package edu.jhu.cs.cisst.algorithms.util.phantom;

import java.util.Random;

import javax.vecmath.Point3i;

import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoSurfaceGenerator;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.DistanceField3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce3D.Heaviside;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class PhantomSimulator3D.
 */
public abstract class PhantomSimulator3D {

	/** The rows. */
	protected int rows;

	/** The cols. */
	protected int cols;

	/** The slices. */
	protected int slices;

	/** The invert image. */
	protected boolean invertImage;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.3 $");
	}

	/** The Constant phantomNames. */
	public static final String[] phantomNames = new String[] { "Sphere",
			"Cube", "Bubbles", "Metasphere" };

	/**
	 * The Enum Phantom.
	 */
	public enum Phantom {

		/** The Sphere. */
		Sphere,
		/** The Cube. */
		Cube,
		/** The Bubbles. */
		Bubbles,
		/** The Metasphere. */
		Metasphere
	}

	/**
	 * Creates the.
	 * 
	 * @param phantom
	 *            the phantom
	 * @param dims
	 *            the dims
	 * 
	 * @return the phantom simulator3 d
	 */
	public static PhantomSimulator3D create(Phantom phantom, Point3i dims) {
		switch (phantom) {
		case Sphere:
			return new PhantomSphere(dims);
		case Metasphere:
			return new PhantomMetasphere(dims);
		case Cube:
			return new PhantomCube(dims);
		case Bubbles:
			return new PhantomBubbles(dims);
		}
		return null;
	}

	/**
	 * The Enum NoiseType.
	 */
	public enum NoiseType {

		/** The Uniform. */
		Uniform,
		/** The Gaussian. */
		Gaussian
	};

	/**
	 * Solve.
	 */
	public abstract void solve();

	/** The noise type. */
	protected NoiseType noiseType = NoiseType.Uniform;

	/** The noise level. */
	protected double noiseLevel = 0.1;

	/** The fuzziness. */
	protected double fuzziness;

	/** The heaviside. */
	protected Heaviside heaviside;

	/** The levelset. */
	protected ImageDataFloat levelset;

	/**
	 * Gets the levelset.
	 * 
	 * @return the levelset
	 */
	public ImageDataFloat getLevelset() {
		return levelset;
	}

	/**
	 * Gets the image.
	 * 
	 * @return the image
	 */
	public ImageDataFloat getImage() {
		return image;
	}

	/**
	 * Gets the surface.
	 * 
	 * @return the surface
	 */
	public EmbeddedSurface getSurface() {
		return surf;
	}

	/** The image. */
	protected ImageDataFloat image;

	/** The surf. */
	protected EmbeddedSurface surf;

	/** The randn. */
	Random randn;

	/**
	 * Instantiates a new phantom simulator3 d.
	 * 
	 * @param dims
	 *            the dims
	 */
	public PhantomSimulator3D(Point3i dims) {
		this.rows = dims.x;
		this.cols = dims.y;
		this.slices = dims.z;
		levelset = new ImageDataFloat(rows, cols, slices);
		image = new ImageDataFloat(rows, cols, slices);
		randn = new Random(43897075348790543l);
	}

	/**
	 * Finish.
	 */
	protected void finish() {
		DistanceField3D df = new DistanceField3D();
		levelset = df.solve(levelset, 10);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double l = levelset.getDouble(i, j, k);
					double noise = 0;
					switch (noiseType) {
					case Uniform:
						noise = noiseLevel * (2 * randn.nextDouble() - 1);
						break;
					case Gaussian:
						noise = noiseLevel * randn.nextGaussian();
						break;
					}
					double v = noise
							+ AppearanceForce3D.heaviside(l, fuzziness,
									heaviside);
					if (invertImage) {
						image.set(i, j, k, 1 - v);
					} else {
						image.set(i, j, k, v);
					}
				}
			}
		}
		IsoSurfaceGenerator isosurf = new IsoSurfaceGenerator();
		surf = isosurf.solve(levelset, 0);
		surf.setName(image.getName());
	}

	/**
	 * Sets the noise level.
	 * 
	 * @param noiseLevel
	 *            the new noise level
	 */
	public void setNoiseLevel(double noiseLevel) {
		this.noiseLevel = noiseLevel;
	}

	/**
	 * Sets the noise type.
	 * 
	 * @param noiseType
	 *            the new noise type
	 */
	public void setNoiseType(NoiseType noiseType) {
		this.noiseType = noiseType;
	}

	/**
	 * Sets the fuzziness.
	 * 
	 * @param fuzziness
	 *            the new fuzziness
	 */
	public void setFuzziness(double fuzziness) {
		this.fuzziness = fuzziness;
	}

	/**
	 * Sets the heaviside.
	 * 
	 * @param heaviside
	 *            the new heaviside
	 */
	public void setHeaviside(Heaviside heaviside) {
		this.heaviside = heaviside;
	}

	public void setInvertImage(boolean invertImage) {
		this.invertImage = invertImage;
	}
}
