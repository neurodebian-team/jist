/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import JSci.instruments.Dimensions;

import processing.core.PImage;

import edu.jhu.cs.cisst.video.GenericMovieMaker;
import edu.jhu.cs.cisst.video.MipavMovieMaker;
import edu.jhu.cs.cisst.video.ProcessingMovieMaker;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationApplication creates a window to display the
 * visualization.
 */
public class VisualizationApplication extends JFrame implements ActionListener {

	/** The visual. */
	protected Visualization visual;

	/** The visualization parameters. */
	protected ParamCollection visualizationParameters;

	/** The cache file. */
	protected File cacheFile;

	/** The movie maker. */
	protected GenericMovieMaker movieMaker;

	protected ProcessingAlgorithm algo;

	/**
	 * Sets the movie maker.
	 * 
	 * @param movieMaker
	 *            the new movie maker
	 */
	public void setMovieMaker(GenericMovieMaker movieMaker) {
		this.movieMaker = movieMaker;
	}

	/**
	 * Instantiates a new visualization application.
	 * 
	 * @param algo
	 *            the algorithm
	 * @param visual
	 *            the visual
	 */
	public VisualizationApplication(ProcessingAlgorithm algo,
			Visualization visual) {
		this.visual = visual;
		this.algo = algo;
		File outputDir = algo.getOutputDirectory();
		cacheFile = new File(outputDir.getParent(), outputDir.getName()
				+ ".cache");
		this.movieMaker = new ProcessingMovieMaker();
	}

	/**
	 * Instantiates a new visualization application.
	 * 
	 * @param cacheFile
	 *            the cache file
	 * @param visual
	 *            the visual
	 */
	public VisualizationApplication(File cacheFile, Visualization visual) {
		this.visual = visual;
		this.cacheFile = cacheFile;
		this.movieMaker = new ProcessingMovieMaker();
	}

	public void capture() {
		visualizationParameters = visual.create();
		if (cacheFile != null) {
			// Import cached visualization parameters if they exist
			ParamCollection params = ((ParamCollection) ParamFactory
					.fromXML(cacheFile));
			if (params != null) {
				visualizationParameters.importParameter(params);
			}
		}
		setTitle(visual.getName());
		ParamInputView inputView = visualizationParameters.getInputView();
		visual.updateVisualizationParameters();
		inputView.addObserver(visual);
		inputView.update();
		JPanel smallPane = new JPanel();
		smallPane.add(visual.getComponent());
		getContentPane().add(smallPane);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (cacheFile != null && visualizationParameters != null)
					visualizationParameters.write(cacheFile);
				if (visual != null)
					visual.dispose();
			}
		});

		((VisualizationProcessing) visual).init();
		pack();
		setVisible(true);

		PipeVentilator ventilator = PipeVentilator.getInstance();
		File pipeFile = ventilator.getPipeFile();
		String uuid = FileReaderWriter.getFileName(pipeFile);

		if (ventilator.isCaptureScreenshot()) {
			File screenshotFile = new File(ventilator.getOutputDirectory(),
					"screenshot_" + uuid + ".png");
			(new PImage(visual.getScreenshot())).save(screenshotFile
					.getAbsolutePath());
			ventilator.setScreenshotFile(screenshotFile);
		}
		if (ventilator.isCaptureVideo()) {
			File videoFile = new File(ventilator.getOutputDirectory(), "video_"
					+ uuid + ".mov");
			Dimension d = visual.getMovieDimensions();
			saveVideo(videoFile, visual.getVideoFrames((long) visual
					.getFrameRate(), (long) visual.getDuration()), visual
					.getFrameRate(), d.width, d.height);

			ventilator.setVideoFile(videoFile);
		}
		visual.dispose();
		setVisible(false);
	}

	/**
	 * Execute.
	 */
	public void execute() {
		visualizationParameters = visual.create();
		if (cacheFile != null) {
			// Import cached visualization parameters if they exist
			ParamCollection params = ((ParamCollection) ParamFactory
					.fromXML(cacheFile));
			if (params != null) {
				visualizationParameters.importParameter(params);
			}
		}
		this.init();
		setVisible(true);
	}

	/**
	 * Run and wait.
	 */
	public void runAndWait() {
		if (PipeVentilator.isVenting()) {
			capture();
		} else {
			execute();
			while (this.isVisible()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Inits the.
	 */
	protected void init() {
		setTitle(visual.getName());
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		ParamInputView inputView = visualizationParameters.getInputView();
		visual.updateVisualizationParameters();
		inputView.addObserver(visual);
		inputView.update();
		splitPane.setLeftComponent(inputView);
		splitPane.setDividerLocation(350);
		setPreferredSize(new Dimension(1024, 768));
		JPanel smallPane = new JPanel();
		smallPane.add(visual.getComponent());
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(smallPane);
		splitPane.setRightComponent(scrollPane);
		splitPane.setOneTouchExpandable(true);
		getContentPane().add(splitPane);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setJMenuBar(createMenuBar());
		setVisible(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (cacheFile != null && visualizationParameters != null)
					visualizationParameters.write(cacheFile);
				if (visual != null)
					visual.dispose();
			}
		});
		pack();
	}

	/**
	 * Creates the menu bar.
	 * 
	 * @return the menu bar
	 */
	private JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		JMenuItem menuItem;
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuBar.add(menuFile);
		menuItem = new JMenuItem("Save Screenshot");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_S);
		menuFile.add(menuItem);
		menuItem.addActionListener(this);
		menuItem = new JMenuItem("Save Video");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
				ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_V);
		menuFile.add(menuItem);
		menuItem.addActionListener(this);
		menuItem = new JMenuItem("Exit");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
				ActionEvent.CTRL_MASK));
		menuItem.setMnemonic(KeyEvent.VK_X);
		menuFile.add(menuItem);
		menuItem.addActionListener(this);
		menuFile.add(menuItem);
		menuBar.add(menuFile);
		return menuBar;
	}

	/**
	 * Select screenshot to save.
	 * 
	 * @param parent
	 *            the parent
	 * 
	 * @return the file
	 */

	private static File selectImageToSave(Component parent) {
		JFileChooser loadDialog = new JFileChooser("Save Screenshot");
		loadDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		loadDialog
				.setFileFilter(new FileExtensionFilter(new String[] { "png" }));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File f = loadDialog.getSelectedFile();
			String name = FileReaderWriter.getFileName(f);
			f = new File(f.getParent(), FileReaderWriter.getFileName(f)
					+ ".png");
			return f;
		} else {
			return null;
		}
	}

	/**
	 * Save video.
	 * 
	 * @param f
	 *            the f
	 * @param images
	 *            the images
	 * @param frameRate
	 *            the frame rate
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 */
	protected void saveVideo(File f, Image[] images, int frameRate, int width,
			int height) {
		if (images == null)
			return;
		movieMaker.save(f, images, frameRate, width, height);
	}

	/**
	 * Select video to save.
	 * 
	 * @param parent
	 *            the parent
	 * 
	 * @return the file
	 */

	private static File selectVideoToSave(Component parent) {
		JFileChooser loadDialog = new JFileChooser("Save Video");
		loadDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		loadDialog.setFileFilter(new FileExtensionFilter(new String[] { "mov",
				"avi" }));
		loadDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = loadDialog.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File f = loadDialog.getSelectedFile();
			String name = FileReaderWriter.getFileName(f);
			String ext = FileReaderWriter.getFileExtension(f);
			if (!ext.equalsIgnoreCase("mov") && !ext.equalsIgnoreCase("avi")) {
				f = new File(f.getParent(), FileReaderWriter.getFileName(f)
						+ ".mov");
			}
			return f;
		} else {
			return null;
		}
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Exit")) {
			this.dispose();
		} else if (e.getActionCommand().equals("Save Screenshot")) {
			File f = selectImageToSave(this);
			if (f != null) {
				(new PImage(visual.getScreenshot())).save(f.getAbsolutePath());
			}
		} else if (e.getActionCommand().equals("Save Video")) {
			File f = selectVideoToSave(this);
			if (f != null) {
				Dimension d = visual.getMovieDimensions();
				saveVideo(f, visual.getVideoFrames(
						(long) visual.getFrameRate(), (long) visual
								.getDuration()), visual.getFrameRate(), (int) d
						.getWidth(), (int) d.getHeight());
			}
		}
	}
}
