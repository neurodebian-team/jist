/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.video;

import gov.nih.mipav.view.renderer.flythroughview.MovieMaker;

import java.awt.Image;
import java.io.File;

// TODO: Auto-generated Javadoc
/**
 * The Class MipavMovieMaker.
 */
public class MipavMovieMaker implements GenericMovieMaker {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.jhu.cs.cisst.video.GenericMovieMaker#save(java.io.File,
	 * java.awt.Image[], int, int, int)
	 */
	public void save(File f, Image[] images, int frameRate, int width,
			int height) {
		MovieMaker movieMaker;
		try {
			// Requires bug fix in mipav in order for this to work
			movieMaker = new MovieMaker(width, height, images.length, f, images);
			movieMaker.makeMovie();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
