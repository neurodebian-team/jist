/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.solver;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * Solve a linear elliptic PDE at the coarsest level (level = 1), i.e. at grid size 3 through simple arithmetic.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class ArithmeticSolver implements Solver {
     
     /**
      * construct from the PDE.
      * 
      * @param pde the PDE to solve
      */
     public ArithmeticSolver(PDE pde) {
          Contract.pre(pde != null,"pde not null-object");
          
          this.pde = pde;
     }

     /**
      * Solves pde at coarsest grid level.
      * <p>
      * 
      * @param u the u
      * @param f the f
      * 
      * @return the boundary grid
      * 
      * @see Solver#solve
      */
     public BoundaryGrid solve(ConstBoundaryGrid u, ConstNoBoundaryGrid f) {
    	 return pde.solve(u, f);    	  
     }

     /** The pde. */
     private PDE pde;
}
