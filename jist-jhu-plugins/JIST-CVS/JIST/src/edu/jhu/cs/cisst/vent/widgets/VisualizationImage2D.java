/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.widgets;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.net.URISyntaxException;
import java.util.LinkedList;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PMatrix;
import processing.core.PVector;

import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamDoubleSliderInputView;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamIntegerSliderInputView;
import edu.jhu.cs.cisst.vent.VisualizationParameters;
import edu.jhu.cs.cisst.vent.VisualizationProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.ImageRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VectorFieldSliceRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeIsoContourRenderer;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D;
import edu.jhu.cs.cisst.vent.resources.PlaceHolder;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.cs.cisst.jist.parameter.*;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationImage.
 */
public class VisualizationImage2D extends VisualizationProcessing2D implements
		Refreshable, MouseWheelListener,SliceNumberDisplay {

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.10 $");
	}

	/**
	 * Instantiates a new visualization image2 d.
	 */
	public VisualizationImage2D() {
		super();
	}

	/** The maximize. */
	protected boolean maximize = false;

	/** The maximize param. */
	protected ParamBoolean maximizeParam;

	/** The scale. */
	protected float tx = 0, ty = 0, rotz = 0, scale = 1;

	/** The scale param. */
	protected ParamFloat txParam, tyParam, rotzParam;

	/** The scale param. */
	protected ParamDouble  scaleParam;
	/** The cursor text color param. */
	protected ParamColor cursorTextColorParam;

	/** The show cursor text param. */
	protected ParamBoolean showCursorTextParam;

	/** The refresh lock. */
	protected boolean refreshLock = false;

	/** The request update. */
	protected boolean requestUpdate = false;

	/** The scene params. */
	protected ParamCollection sceneParams;

	/** The rotation rate in radians. */
	protected float rotRate = 0.01f;

	/** The scale rate. */
	protected float scaleRate = 0.01f;

	/** The scale rate. */
	protected float wheelScaleRate = 0.1f;

	/** The show cursor text. */
	protected boolean showCursorText = true;

	/** The cursor text color. */
	protected Color cursorTextColor = new Color(255, 255, 0);

	/**
	 * Instantiates a new visualization image.
	 * 
	 * @param rows
	 *            the rows
	 * @param cols
	 *            the cols
	 */
	public VisualizationImage2D(int rows, int cols) {
		super();
		setName("Visualize Image");
	}

	/**
	 * Instantiates a new visualization image.
	 * 
	 * @param img
	 *            the img
	 */
	public VisualizationImage2D(ImageData img) {
		super();
		addImage(img);
		setName("Visualize Image - " + img.getName());
	}

	/**
	 * Instantiates a new visualization image.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param rows
	 *            the rows
	 * @param cols
	 *            the cols
	 */
	public VisualizationImage2D(int width, int height, int rows, int cols) {
		super(width, height);
		setName("Visualize Image");
	}

	/**
	 * Instantiates a new visualization image.
	 * 
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param img
	 *            the img
	 */
	public VisualizationImage2D(int width, int height, ImageData img) {
		super(width, height);
		addImage(img);
		setName("Visualize Image - " + img.getName());
	}

	/** The refresher. */
	protected Refresher refresher;

	/**
	 * Setup.
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing2D#setup()
	 */
	public void setup() {
		super.setup();
		this.addMouseWheelListener(this);
		refresher = new Refresher();
		refresher.add(this);
		refresher.setRefreshInterval(250);
		refresher.start();
		try {
			String fontFile = (new File(PlaceHolder.class.getResource(
					"./TheSans-Plain-12.vlw").toURI()).getAbsolutePath());
			System.out.println(fontFile);
			textFont(loadFont(fontFile));
		} catch (URISyntaxException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Dispose.
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#dispose()
	 */
	public void dispose() {
		if (refresher != null) {
			refresher.stop();
		}
		super.dispose();
	}

	/** The cursor refresh rate. */
	int cursorRefreshRate = 10;

	/** The refresh count. */
	int refreshCount = 0;

	/** The mouse value. */
	String mouseValue = "";

	/** The mouse position. */
	PVector mousePosition = new PVector();

	/**
	 * Draw.
	 * 
	 * @see processing.core.PApplet#draw()
	 */
	public void draw() {

		background(255, 255, 255);
		int rows = 0;
		int cols = 0;

		int size = renderers.size();
		for (int i = size - 1; i >= 0; i--) {
			RendererProcessing2D renderer = renderers.get(i);

			if (renderer instanceof ImageRenderer2D) {
				if (!((ImageRenderer2D) renderer).isVisible())
					continue;
				PImage img = ((ImageRenderer2D) renderer).getImage();
				rows = Math.max(rows, img.width);
				cols = Math.max(cols, img.height);
			}  else if(renderer instanceof VolumeIsoContourRenderer){
				//PImage img = ((VolumeSliceRenderer2D) renderer).getImage(((VolumeSliceRenderer2D) renderer).getSliceParameter().getInt()-1);
				rows = Math.max(rows, ((VolumeSliceRenderer2D) renderer).rows);
				cols = Math.max(cols, ((VolumeSliceRenderer2D) renderer).cols);				
			} else if (renderer instanceof VolumeSliceRenderer2D) {
				if (!((VolumeSliceRenderer2D) renderer).isVisible())
					continue;
				//PImage img = ((VolumeSliceRenderer2D) renderer).getImage(((VolumeSliceRenderer2D) renderer).getSliceParameter().getInt()-1);
				rows = Math.max(rows, ((VolumeSliceRenderer2D) renderer).rows);
				cols = Math.max(cols, ((VolumeSliceRenderer2D) renderer).cols);
			}
		}
		pushMatrix();
		float effectiveScale = scale;
		if (maximize) {
			effectiveScale = Math.min(width / (float) rows, height
					/ (float) cols);
			scale(effectiveScale, effectiveScale);
		} else {
			translate(rows * 0.5f - tx, cols * 0.5f - ty);
			rotateZ(rotz);
			scale(scale, scale);
			translate(-rows * 0.5f, -cols * 0.5f);
		}
		super.draw();
		stroke(0, 0, 0);
		noSmooth();
		noFill();
		rect(0, 0, rows, cols);

		PMatrix p = getMatrix();
		popMatrix();
		if (showCursorText && !mousePressed && !requestScreenShot) {
			if (refreshCount % cursorRefreshRate == 0) {
				p.invert();
				p.mult(new PVector(mouseX - width * 0.5f, mouseY - height
						* 0.5f, 0), mousePosition);
				refreshCount = 0;
				mouseValue = "";
				// Examine renderers in reverse order
				for (int i = size - 1; i >= 0; i--) {
					RendererProcessing2D renderer = renderers.get(i);
					if (renderer instanceof ImageRenderer2D) {
						if (!((ImageRenderer2D) renderer).isVisible())
							continue;
						mouseValue += ((ImageRenderer2D) renderer)
								.getValueString((int) Math
										.round(mousePosition.x), (int) Math
										.round(mousePosition.y))
								+ " ";
						break;
					} else if (renderer instanceof VolumeSliceRenderer2D) {
						if (!((VolumeSliceRenderer2D) renderer).isVisible())
							continue;
						mouseValue += ((VolumeSliceRenderer2D) renderer)
								.getValueString((int) Math
										.round(mousePosition.x), (int) Math
										.round(mousePosition.y))
								+ " ";
						break;
					}
				}
			} else {
				refreshCount++;
			}

			if (mousePosition.x >= 0 && mousePosition.x <= rows
					&& mousePosition.y >= 0 && mousePosition.y <= cols) {
				float x;
				float y;
				if (mousePosition.x > rows / 2) {
					x = mouseX - 110;
				} else {
					x = mouseX + 5;
				}
				if (mousePosition.y < cols / 2) {
					y = mouseY + 15;
				} else {
					y = mouseY - 5;
				}
				fill(cursorTextColor.getRGB());
				text(String.format("%4.1f,%4.1f: " + mouseValue,
						mousePosition.x, mousePosition.y), x, y);
				cursor(PApplet.CROSS);
			} else {
				cursor(PApplet.ARROW);
			}
		}
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters
	 *            the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing2D#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		super.createVisualizationParameters(visualizationParameters);
		sceneParams = new ParamCollection("Scene Controls");
		sceneParams.add(rotzParam = new ParamFloat("Rotation", 0, 360,
				(float) Math.toDegrees(rotz)));
		rotzParam.setInputView(new ParamIntegerSliderInputView(rotzParam, 4));
		sceneParams.add(scaleParam = new ParamDouble("Scale", 0.1, 100, scale));

		scaleParam.setInputView(new ParamDoubleSliderInputView(scaleParam, 4,true));
		
		sceneParams.add(txParam = new ParamFloat("Translation X", -10*width,10*width, tx));
		txParam.setInputView(new ParamDoubleSliderInputView(txParam, 4,false));
		
		sceneParams.add(tyParam = new ParamFloat("Translation Y", -10*height,10*height, ty));
		tyParam.setInputView(new ParamDoubleSliderInputView(tyParam, 4,false));
		
		sceneParams.add(cursorTextColorParam = new ParamColor(
				"Cursor Text Color", cursorTextColor));
		sceneParams.add(showCursorTextParam = new ParamBoolean(
				"Show Cursor Text", showCursorText));
		sceneParams.add(maximizeParam = new ParamBoolean("Maximize", maximize));
		visualizationParameters.add(sceneParams);
	}

	/**
	 * Update.
	 * 
	 * @param model
	 *            the model
	 * @param view
	 *            the view
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing2D#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
	 *      edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	public void update(ParamModel model, ParamInputView view) {
		if (!refreshLock) {
			super.update(model, view);
			if (model == rotzParam) {
				rotz = (float) Math.toRadians(rotzParam.getFloat());
			} else if (model == txParam) {
				tx = txParam.getFloat();
			} else if (model == tyParam) {
				ty = tyParam.getFloat();
			} else if (model == scaleParam) {
				scale = scaleParam.getFloat();
			} else if (model == maximizeParam) {
				maximize = maximizeParam.getValue();
			} else if (model == showCursorTextParam) {
				showCursorText = showCursorTextParam.getValue();
			} else if (model == cursorTextColorParam) {
				cursorTextColor = cursorTextColorParam.getValue();
			}
		}
	}

	/**
	 * Mouse dragged.
	 * 
	 * @see processing.core.PApplet#mouseDragged()
	 */
	public void mouseDragged() {
		if ((mouseButton == RIGHT || mouseButton == CENTER) && !keyPressed) {
			scale = Math.max(0, scale + scaleRate * (mouseY - pmouseY));
			scaleParam.setValue(scale);
			requestUpdate = true;
		} else if (mouseButton == LEFT && !keyPressed) {
			tx += (pmouseX - mouseX);
			ty += (pmouseY - mouseY);
			txParam.setValue(tx);
			tyParam.setValue(ty);
			requestUpdate = true;
		} else if ((mouseButton == LEFT && keyPressed && keyCode == KeyEvent.VK_CONTROL)) {
			rotz += (mouseY - pmouseY) * rotRate;
			if (rotz > 2 * Math.PI)
				rotz -= 2 * Math.PI;
			if (rotz < 0)
				rotz += 2 * Math.PI;
			rotzParam.setValue(Math.toDegrees(rotz));
			requestUpdate = true;
		}
	}

	/**
	 * Update visualization parameters.
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing2D#updateVisualizationParameters()
	 */
	public void updateVisualizationParameters() {
		super.updateVisualizationParameters();
		rotz = (float) Math.toRadians(rotzParam.getFloat());
		tx = txParam.getFloat();
		ty = tyParam.getFloat();
		scale = scaleParam.getFloat();
		maximize = maximizeParam.getValue();
		cursorTextColor = cursorTextColorParam.getValue();
		showCursorText = showCursorTextParam.getValue();
	}

	/**
	 * Adds the image.
	 * 
	 * @param img
	 *            the img
	 */
	public void addImage(ImageData img) {
		if (img.getSlices() <= 1) {
			renderers.add(new ImageRenderer2D(img, this));
		} else {
			renderers.add(new VolumeSliceRenderer2D(img, this));
		}
	}
	/**
	 * Adds the image.
	 * 
	 * @param img
	 *            the img
	 */
	public void addVectorField(ImageData img) {
		renderers.add(new VectorFieldSliceRenderer2D(img, this));
	}
	/**
	 * Refresh.
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable#refresh()
	 */
	@Override
	public void refresh() {
		if (requestUpdate) {
			requestUpdate = false;
			refreshLock = true;
			sceneParams.getInputView().update();
			refreshLock = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#getVideoFrames(long,
	 * long)
	 */
	public Image[] getVideoFrames(long sampleInterval, long duration) {
		int maxSlice = 0;
		LinkedList<ParamInteger> sliceParams = new LinkedList<ParamInteger>();
		LinkedList<Integer> currentSlice = new LinkedList<Integer>();
		for (RendererProcessing2D renderer : renderers) {
			if (renderer instanceof VolumeSliceRenderer2D) {
				VolumeSliceRenderer2D render = (VolumeSliceRenderer2D) renderer;

				ParamInteger sliceParam = ((VolumeSliceRenderer2D) renderer)
						.getSliceParameter();
				maxSlice = Math.max(maxSlice, sliceParam.getMax().intValue()
						- sliceParam.getMin().intValue() + 1);
				sliceParams.add(sliceParam);
				currentSlice.add(sliceParam.getInt());
				sliceParam.setValue(sliceParam.getMin());
				renderer.updateVisualizationParameters();
			} 
		}
		Image[] images = new Image[maxSlice];
		for (int i = 0; i < maxSlice; i++) {
			// Acquire video frames
			images[i] = getScreenshot();
			for (ParamInteger param : sliceParams) {
				param.setValue(param.getValue().intValue() + 1);
			}
			for (RendererProcessing2D renderer : renderers) {
				if (renderer instanceof VolumeSliceRenderer2D) {
					renderer.updateVisualizationParameters();
				}
			}
		}
		int index = 0;
		for (ParamInteger param : sliceParams) {
			param.setValue(currentSlice.get(index++));
		}
		return images;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejava.awt.event.MouseWheelListener#mouseWheelMoved(java.awt.event.
	 * MouseWheelEvent)
	 */
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		scale = Math.max(0, scale - wheelScaleRate * (e.getWheelRotation()));
		scaleParam.setValue(scale);
		requestUpdate = true;
	}

	/**
	 * Gets the scale.
	 * 
	 * @return the scale
	 */
	public float getScale() {
		if (maximize) {
			int rows = 0;
			int cols = 0;

			int size = renderers.size();
			for (int i = size - 1; i >= 0; i--) {
				RendererProcessing2D renderer = renderers.get(i);
				if (renderer instanceof ImageRenderer2D){
					if (!((ImageRenderer2D) renderer).isVisible())
						continue;
					PImage img = ((ImageRenderer2D) renderer).getImage();
					rows = Math.max(rows, img.width);
					cols = Math.max(cols, img.height);
				} else if (renderer instanceof VolumeSliceRenderer2D) {
					if (!((VolumeSliceRenderer2D) renderer).isVisible())
						continue;
					//PImage img = ((VolumeSliceRenderer2D) renderer).getImage(((VolumeSliceRenderer2D) renderer).getSliceParameter().getInt()-1);
					rows = Math.max(rows, ((VolumeSliceRenderer2D) renderer).rows);
					cols = Math.max(cols, ((VolumeSliceRenderer2D) renderer).cols);
				}
			}

			return Math.min(width / (float) rows, height / (float) cols);
		} else 
			return scale;
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.widgets.SliceNumberDisplay#draw(processing.core.PApplet, int, int, int)
	 */
	public void draw(PApplet applet, int sliceIndex, int totalSlices,int h) {
		applet.noStroke();
		applet.tint(255);
		applet.fill(cursorTextColor.getRGB());
		applet.pushMatrix();
		float textScale=1;
		textScale=getScale();
		applet.scale(1.0f / textScale);
		
		applet.text(String.format("%d/%d", sliceIndex, totalSlices), 5, textScale*h - 15);
		applet.popMatrix();
		applet.noFill();
	}

}
