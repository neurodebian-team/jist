/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.imageproc;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import gov.nih.mipav.model.algorithms.AlgorithmGrayScaleMorphology2D;
import gov.nih.mipav.model.structures.ModelImage;

// TODO: Auto-generated Javadoc
/**
 * The Class Morphology2D.
 */
public class Morphology2D {
	
	/** The dilation iterations. */
	protected int dilationIterations = 1;
	
	/** The erosion iterations. */
	protected int erosionIterations = 1;
	
	/** The kernel size. */
	protected float kernelSize = 1;
	
	/** The Constant kernelNames. */
	public static final String[] kernelNames = new String[] {
			"Circle", "3x3 -  4 connected", "3x3 -  8 connected",
			"5x5 - 12 connected" };

	/**
	 * The Enum Kernel.
	 */
	public enum Kernel {
		
		/** The Circle. */
		Circle, 
 /** The Connected4. */
 Connected4, 
 /** The Connected8. */
 Connected8, 
 /** The Connected12. */
 Connected12
	};

	/** The Constant operationNames. */
	public static final String[] operationNames = new String[] { "Erode",
			"Dilate", "Close", "Open", "Gradient", "Top Hat", "Bottom Hat",
			"Laplacian" };

	/**
	 * The Enum Operation.
	 */
	public enum Operation {
		
		/** The Erode. */
		Erode, 
 /** The Dilate. */
 Dilate, 
 /** The Close. */
 Close, 
 /** The Open. */
 Open, 
 /** The Gradient. */
 Gradient, 
 /** The Top hat. */
 TopHat, 
 /** The Bottom hat. */
 BottomHat, 
 /** The Laplacian. */
 Laplacian
	};

	/** The Constant opCodes. */
	protected static final int opCodes[] = new int[] { 0, 1, 2, 3, 15, 16, 17,
			18 };
	
	/** The kernel. */
	protected Kernel kernel = Kernel.Circle;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.1 $");
	}

	/**
	 * Sets the dilation iterations.
	 * 
	 * @param dilationIterations the new dilation iterations
	 */
	public void setDilationIterations(int dilationIterations) {
		this.dilationIterations = dilationIterations;
	}

	/**
	 * Gets the erosion iterations.
	 * 
	 * @return the erosion iterations
	 */
	public int getErosionIterations() {
		return erosionIterations;
	}

	/**
	 * Sets the erosion iterations.
	 * 
	 * @param erosionIterations the new erosion iterations
	 */
	public void setErosionIterations(int erosionIterations) {
		this.erosionIterations = erosionIterations;
	}

	/**
	 * Gets the kernel size.
	 * 
	 * @return the kernel size
	 */
	public float getKernelSize() {
		return kernelSize;
	}

	/**
	 * Sets the kernel size.
	 * 
	 * @param kernelSize the new kernel size
	 */
	public void setKernelSize(float kernelSize) {
		this.kernelSize = kernelSize;
	}

	/**
	 * Gets the kernel.
	 * 
	 * @return the kernel
	 */
	public Kernel getKernel() {
		return kernel;
	}

	/**
	 * Sets the kernel.
	 * 
	 * @param kernel the new kernel
	 */
	public void setKernel(Kernel kernel) {
		this.kernel = kernel;
	}

	/**
	 * Solve.
	 * 
	 * @param img the img
	 * @param operation the operation
	 * 
	 * @return the image data
	 */
	public ImageData solve(ImageData img, Operation operation) {

		ModelImage inputImage = img.getModelImageCopy();
		ModelImage resultImage = (ModelImage) inputImage.clone();
		AlgorithmGrayScaleMorphology2D algo2d = new AlgorithmGrayScaleMorphology2D(
				resultImage, kernel.ordinal(), kernelSize, opCodes[operation
						.ordinal()], dilationIterations, erosionIterations, 0,
				0, true);
		algo2d.run();
		ImageDataMipav resultImg = new ImageDataMipav(resultImage);
		resultImg.setName(img.getName() + "_" + operation.toString());
		return resultImg;
	}

}
