/*
 *
 */
package edu.jhu.cs.cisst.plugins.utilities.logic;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmSurfaceBranch extends ProcessingAlgorithm{
	private ParamSurface surfTrue;
	private ParamSurface surfFalse;
	private ParamSurface outVol;
	private ParamBoolean branchParam;
	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(outVol=new ParamSurface("Surface"));
		inputParams.add(branchParam=new ParamBoolean("Assertion",true));
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Surface Branch");
		inputParams.setName("surface_branch");
		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription("An if/then/else condition for forwarding a surface.");
		info.add(CommonAuthors.blakeLucas);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(surfTrue=new ParamSurface("Surface for True"));
		outputParams.add(surfFalse=new ParamSurface("Surface for False"));
		surfTrue.setMandatory(false);
		surfFalse.setMandatory(false);
	}
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		if(branchParam.getValue()){
			surfTrue.setValue(outVol.getValue());
		} else {
			surfFalse.setValue(outVol.getValue());
		}
	}
}
