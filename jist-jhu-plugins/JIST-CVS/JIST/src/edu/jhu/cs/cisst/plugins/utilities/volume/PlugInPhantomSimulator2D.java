package edu.jhu.cs.cisst.plugins.utilities.volume;

import javax.vecmath.Point2f;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
public class PlugInPhantomSimulator2D extends ProcessingAlgorithm{
	protected ParamInteger xSeed;
	protected ParamInteger ySeed;
	protected ParamInteger rows;
	protected ParamInteger cols;
	protected ParamDouble radius;
	protected ParamVolume refImageParam;
	protected ParamVolume maskImage;
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(xSeed=new ParamInteger("Center X Position",100));
		inputParams.add(ySeed=new ParamInteger("Center Y Position",100));
		inputParams.add(radius=new ParamDouble("Radius",20));
		inputParams.add(refImageParam=new ParamVolume("Reference Image"));
		inputParams.setName("phantom_sim_2d");
		inputParams.setLabel("Phantom Simulator 2D");
		AlgorithmInformation info=getAlgorithmInformation();

		info.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.add(CommonAuthors.blakeLucas);
		info.setStatus(DevelopmentStatus.Release);
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Volume");
	}

	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(maskImage=new ParamVolume("Level Set Image"));
	}

	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		int xs=xSeed.getInt();
		int ys=ySeed.getInt();
		Point2f pt=new Point2f(ys,xs);
		double rad=radius.getDouble();
		ImageData refImage=refImageParam.getImageData();
		int r=refImage.getRows();
		int c=refImage.getCols();

		ImageDataFloat mask=new ImageDataFloat(r,c);
		for(int i=0;i<r;i++){
			for(int j=0;j<c;j++){
				double d1=((new Point2f((float)j,(float)i)).distance(pt));
				mask.set(i,j,(float)d1-rad);
			}
		}
		mask.setName(refImage.getName()+"_circle_levelset");
		maskImage.setValue(mask);
	}

}
