/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import java.util.LinkedList;
import java.util.PriorityQueue;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Vector3f;

import Jama.Matrix;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.MaskVolume6;

// TODO: Auto-generated Javadoc
/**
 * The Class MeshDistanceHash.
 */
public class MeshDistanceHash extends AbstractMeshDistance {
	
	/** The hash table. */
	protected HashMeshBBox[][][] hashTable;
	
	/** The slices. */
	protected int rows, cols, slices;
	
	/** The distance field image. */
	protected ImageDataFloat distanceFieldImage = null;
	
	/** The cell size. */
	protected double cellSize;
	
	/** The min point. */
	protected Point3f minPoint;
	
	/** The max point. */
	protected Point3f maxPoint;
	
	/** The cell padding. */
	protected double cellPadding=3;
	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.1 $");
	}
	/** The closest triangle. */
	protected MeshTriangle closestTriangle = null;

	/** The intersect count. */
	protected int intersectCount = 0;

	/** The closest point. */
	protected Point3f closestPoint = null;;

	/**
	 * Instantiates a new mesh distance hash.
	 *
	 * @param mesh the mesh
	 * @param cellSize the cell size
	 */
	public MeshDistanceHash(TriangleMesh mesh, double cellSize) {
		super(mesh);

		this.mesh = mesh;
		this.cellSize = cellSize;
		MeshBBox bbox = mesh.getBoundingBox();
		minPoint = bbox.getMinPoint();
		minPoint.x -= cellPadding*cellSize;
		minPoint.y -= cellPadding*cellSize;
		minPoint.z -= cellPadding*cellSize;

		maxPoint = bbox.getMaxPoint();
		maxPoint.x += cellPadding*cellSize;
		maxPoint.y += cellPadding*cellSize;
		maxPoint.z += cellPadding*cellSize;
		this.rows = (int) Math.ceil((maxPoint.x - minPoint.x) / cellSize);
		this.cols = (int) Math.ceil((maxPoint.y - minPoint.y) / cellSize);
		this.slices = (int) Math.ceil((maxPoint.z - minPoint.z) / cellSize);
		init();
	}

	/**
	 * Instantiates a new mesh distance hash.
	 *
	 * @param mesh the mesh
	 * @param cellSize the cell size
	 * @param minPoint the min point
	 * @param maxPoint the max point
	 */
	public MeshDistanceHash(TriangleMesh mesh, double cellSize,
			Point3f minPoint, Point3f maxPoint) {
		super(mesh);

		this.mesh = mesh;
		this.cellSize = cellSize;
		this.minPoint = minPoint;
		this.maxPoint = maxPoint;
		this.rows = (int) Math.ceil((maxPoint.x - minPoint.x) / cellSize);
		this.cols = (int) Math.ceil((maxPoint.y - minPoint.y) / cellSize);
		this.slices = (int) Math.ceil((maxPoint.z - minPoint.z) / cellSize);
		init();
	}

	/**
	 * Instantiates a new mesh distance hash.
	 *
	 * @param mesh the mesh
	 * @param cellSize the cell size
	 * @param minPoint the min point
	 * @param dims the dims
	 */
	public MeshDistanceHash(TriangleMesh mesh, double cellSize,
			Point3f minPoint, Point3i dims) {
		super(mesh);

		this.mesh = mesh;
		this.cellSize = cellSize;
		this.minPoint = minPoint;
		rows = dims.x;
		cols = dims.y;
		slices = dims.z;
		this.maxPoint = new Point3f(minPoint.x + (float) cellSize * dims.x,
				minPoint.y + (float) cellSize * dims.y, minPoint.z
						+ (float) cellSize * dims.z);
		init();
	}

	/**
	 * Inits the.
	 */
	protected void init() {
		MeshBBox bbox = mesh.getBoundingBox();
		System.out.println("Hash Size [" + rows + "," + cols + "," + slices
				+ "]");
		System.out.flush();
		hashTable = new HashMeshBBox[rows][cols][slices];
		setTotalUnits(rows*cols*slices);
		setLabel("Initializing");
		for (MeshBBox box : bbox.getChildren()) {

			if (box.minPoint.x <= maxPoint.x && box.minPoint.y <= maxPoint.y
					&& box.minPoint.z <= maxPoint.z
					&& box.maxPoint.x >= minPoint.x
					&& box.maxPoint.y >= minPoint.y
					&& box.maxPoint.z >= minPoint.z) {

				splatBBox(box);
			}
			incrementCompletedUnits();
		}
		markCompleted();
	}

	/**
	 * The Class IndexedVoxel.
	 */
	protected class IndexedVoxel implements Comparable<IndexedVoxel> {
		
		/** The k. */
		public int i, j, k;
		
		/** The val. */
		float val;

		/**
		 * Instantiates a new indexed voxel.
		 *
		 * @param i the i
		 * @param j the j
		 * @param k the k
		 * @param val the val
		 */
		public IndexedVoxel(int i, int j, int k, float val) {
			this.val = val;
			this.i = i;
			this.j = j;
			this.k = k;
		}

		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(IndexedVoxel voxel) {
			return (int) Math.signum(voxel.val - this.val);
		}
	}

	/**
	 * Gets the transform.
	 *
	 * @return the transform
	 */
	public Matrix getTransform() {
		Matrix T = new Matrix(4, 4);
		T.set(0, 0, cellSize);
		T.set(1, 1, cellSize);
		T.set(2, 2, cellSize);
		T.set(3, 3, 1);
		T.set(0, 3, minPoint.x + 0.5 * cellSize);
		T.set(1, 3, minPoint.y + 0.5 * cellSize);
		T.set(2, 3, minPoint.z + 0.5 * cellSize);
		return T;
	}

	/**
	 * Gets the iso surface.
	 *
	 * @return the iso surface
	 */
	public EmbeddedSurface getIsoSurface() {
		IsoSurfaceGenerator isogen = new IsoSurfaceGenerator();
		EmbeddedSurface surf = isogen.solve(getDistanceField(), 0);
		surf.transform(getTransform());
		return surf;
	}

	/**
	 * Gets the distance field.
	 *
	 * @return the distance field
	 */
	public ImageDataFloat getDistanceField() {
		if (distanceFieldImage == null) {
			distanceFieldImage = new ImageDataFloat(rows, cols, slices);
			distanceFieldImage.setName(mesh.getSurface().getName() + "_dist");
			float[][][] img = distanceFieldImage.toArray3d();
			PriorityQueue<IndexedVoxel> voxels = new PriorityQueue<IndexedVoxel>();
			System.out.println("Computing distance field.");
			setLabel("Computing Distance Field");
			setTotalUnits(rows*cols*slices);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						HashMeshBBox bbox = hashTable[i][j][k];
						float val;
						if (bbox != null && bbox.size() > 0) {
							val = img[i][j][k] = -(float) distance(new Point3f(
									minPoint.x + (i + 0.5f) * (float) cellSize,
									minPoint.y + (j + 0.5f) * (float) cellSize,
									minPoint.z + (k + 0.5f) * (float) cellSize));
						} else {
							val = img[i][j][k] = -5;
						}
						if ((i == 0 && j == 0 && k == 0)
								|| (i == 0 && j == 0 && k == slices - 1)
								|| (i == 0 && j == cols - 1 && k == 0)
								|| (i == 0 && j == cols - 1 && k == slices - 1)
								|| (i == rows - 1 && j == 0 && k == 0)
								|| (i == rows - 1 && j == 0 && k == slices - 1)
								|| (i == rows - 1 && j == cols - 1 && k == 0)
								|| (i == rows - 1 && j == cols - 1 && k == slices - 1)) {
							voxels.add(new IndexedVoxel(i, j, k, -val));
						}

						incrementCompletedUnits();
					}
				}
			}

			MaskVolume6 mask = new MaskVolume6();
			byte[] neighborsX = mask.getNeighborsX();
			byte[] neighborsY = mask.getNeighborsY();
			byte[] neighborsZ = mask.getNeighborsZ();
			System.out.println("Flooding level set.");
			while (voxels.size() > 0) {
				IndexedVoxel voxel = voxels.remove();
				for (int n = 0; n < 6; n++) {
					int ni = voxel.i + neighborsX[n];
					int nj = voxel.j + neighborsY[n];
					int nk = voxel.k + neighborsZ[n];
					if (nj < 0 || nj >= cols || nk < 0 || nk >= slices
							|| ni < 0 || ni >= rows)
						continue;
					float val = img[ni][nj][nk];
					if (val < 0 && -val <= voxel.val) {
						voxels.add(new IndexedVoxel(ni, nj, nk, -val));
						img[ni][nj][nk] *= -1;
					}
				}
			}

			System.out.println("Distance field.");
			markCompleted();
			// DistanceField df = new DistanceField();
			// distanceFieldImage = df.solve(distanceFieldImage, 5);
		}
		return distanceFieldImage;
	}

	/**
	 * Gets the last closest point.
	 * 
	 * @return the last closest point
	 */
	public Point3f getLastClosestPoint() {
		return closestPoint;
	}

	/**
	 * Gets the last closest triangle.
	 * 
	 * @return the last closest triangle
	 */
	public MeshTriangle getLastClosestTriangle() {
		return closestTriangle;
	}

	/**
	 * Gets the last intersection count.
	 * 
	 * @return the last intersection count
	 */
	public int getLastIntersectionCount() {
		return intersectCount;
	}

	/**
	 * The Class HashMeshBBox.
	 */
	public class HashMeshBBox extends LinkedList<MeshBBox> {
		
		/** The k. */
		public int i, j, k;

		/**
		 * Instantiates a new hash mesh b box.
		 *
		 * @param i the i
		 * @param j the j
		 * @param k the k
		 */
		public HashMeshBBox(int i, int j, int k) {
			this.i = i;
			this.j = j;
			this.k = k;
		}

		/* (non-Javadoc)
		 * @see java.util.AbstractCollection#toString()
		 */
		public String toString() {
			return "[" + i + "," + j + "," + k + "] " + this.size();
		}

		/** The cached distance. */
		protected double cachedDistance = 0;

		/** The cached point. */
		protected Point3f cachedPoint = null;

		/**
		 * Gets the cached point.
		 *
		 * @return the cached point
		 */
		public Point3f getCachedPoint() {
			return cachedPoint;
		}

		/**
		 * Checks if is cached.
		 *
		 * @param pt the pt
		 * @return true, if is cached
		 */
		public boolean isCached(Point3f pt) {
			return (pt.equals(cachedPoint));
		}

		/**
		 * Get the distance squared to a point.
		 * 
		 * @param pt
		 *            the point
		 * 
		 * @return the distance squared
		 */
		public double distanceSquared(Point3f pt) {
			if (pt.equals(cachedPoint))
				return cachedDistance;
			Point3f bboxMinPoint = new Point3f(i * (float) cellSize
					+ minPoint.x, j * (float) cellSize + minPoint.y, k
					* (float) cellSize + minPoint.z);
			Point3f bboxMaxPoint = new Point3f((i + 1) * (float) cellSize
					+ minPoint.x, (j + 1) * (float) cellSize + minPoint.y,
					(k + 1) * (float) cellSize + minPoint.z);
			// System.out.println(bboxMinPoint+" "+bboxMaxPoint);
			Vector3f closestPt = new Vector3f();
			if (pt.x < bboxMinPoint.x) {
				closestPt.x = bboxMinPoint.x - pt.x;
			} else if (pt.x > bboxMaxPoint.x) {
				closestPt.x = pt.x - bboxMaxPoint.x;
			} else {
				closestPt.x = 0;
			}
			if (pt.y < bboxMinPoint.y) {
				closestPt.y = bboxMinPoint.y - pt.y;
			} else if (pt.y > bboxMaxPoint.y) {
				closestPt.y = pt.y - bboxMaxPoint.y;
			} else {
				closestPt.y = 0;
			}
			if (pt.z < bboxMinPoint.z) {
				closestPt.z = bboxMinPoint.z - pt.z;
			} else if (pt.z > bboxMaxPoint.z) {
				closestPt.z = pt.z - bboxMaxPoint.z;
			} else {
				closestPt.z = 0;
			}
			cachedDistance = closestPt.lengthSquared();
			cachedPoint = pt;
			return cachedDistance;
		}
	}

	/**
	 * Get the distance from a point to the surface using a KD-tree accelerator.
	 * 
	 * @param p
	 *            the point
	 * 
	 * @return the distance
	 * 
	 * @see cis.registration.point.AbstractMeshDistance#distance(javax.vecmath.Point3f)
	 */
	public double distance(Point3f p) {
		double minDistSquared = MeshBBox.MAX_VALUE;
		MeshTriangle tri = null;
		closestTriangle = null;
		closestPoint = null;
		double d;
		intersectCount = 0;
		int N = mesh.getTriangles().length;
		// Build priority queue sorted by distance to bounding box
		PriorityQueue<MeshBBox> bboxQueue = new PriorityQueue<MeshBBox>(N / 8,
				new MeshBBoxDistanceComparator(p));

		// Build priority queue sorted by distance to bounding box
		PriorityQueue<HashMeshBBox> hashQueue = new PriorityQueue<HashMeshBBox>(100,
				new HashBBoxDistanceComparator3D(p));
		Point3i start = getIndex(p);
		HashMeshBBox hbox = hashTable[start.x][start.y][start.z];
		if (hbox == null) {
			hbox = hashTable[start.x][start.y][start.z] = new HashMeshBBox(start.x,
					start.y, start.z);
		}
		hashQueue.add(hbox);
		while (hashQueue.size() > 0) {
			hbox = hashQueue.remove();
			if (hbox.distanceSquared(p) > minDistSquared)
				break;
			bboxQueue.addAll(hbox);
			while (bboxQueue.size() > 0) {
				// Remove closest bounding box
				MeshBBox bbox = bboxQueue.remove();
				d = bbox.distanceSquared(p);
				if (d < minDistSquared) {
					// Distance to bounding box is smaller than any seen so far
					if ((tri = bbox.getTriangle()) != null) {
						intersectCount++;
						// Calculate distance to triangle if this is a leaf node
						d = tri.distanceSquared(p);
						if (d < minDistSquared) {
							// Update minimum distance
							minDistSquared = d;
							closestTriangle = tri;
							closestPoint = tri.getLastClosestPoint();
						}
					}
				}
			}
			HashMeshBBox tmp;

			if (minDistSquared > hbox.distanceSquared(p)) {
				for (int i = Math.max(0, hbox.i - 1); i < Math.min(hbox.i + 2,
						rows); i++) {
					for (int j = Math.max(0, hbox.j - 1); j < Math.min(
							hbox.j + 2, cols); j++) {
						for (int k = Math.max(0, hbox.k - 1); k < Math.min(
								hbox.k + 2, slices); k++) {
							tmp = hashTable[i][j][k];
							if (tmp == null) {
								tmp = hashTable[i][j][k] = new HashMeshBBox(i, j, k);
							}
							if (!tmp.isCached(p)
									&& tmp.distanceSquared(p) <= minDistSquared) {
								hashQueue.add(tmp);
							}

						}
					}
				}
			}
		}
		if (minDistSquared == MeshBBox.MAX_VALUE) {
			System.err.println("NO DISTANCE " + p);
		}
		return Math.sqrt(minDistSquared);
	}

	/**
	 * Gets the index.
	 *
	 * @param pt the pt
	 * @return the index
	 */
	protected Point3i getIndex(Point3f pt) {
		int i = (int) Math.min(rows - 1, Math.max(0, Math
				.floor((pt.x - minPoint.x) / cellSize)));
		int j = (int) Math.min(cols - 1, Math.max(0, Math
				.floor((pt.y - minPoint.y) / cellSize)));
		int k = (int) Math.min(slices - 1, Math.max(0, Math
				.floor((pt.z - minPoint.z) / cellSize)));
		return new Point3i(i, j, k);
	}

	/**
	 * Splat b box.
	 *
	 * @param bbox the bbox
	 */
	protected void splatBBox(MeshBBox bbox) {
		Point3f lowerPoint = bbox.getMinPoint();
		Point3f upperPoint = bbox.getMaxPoint();
		int lowerRow = (int) Math.max(0, Math.floor((lowerPoint.x - minPoint.x)
				/ cellSize));

		int lowerCol = (int) Math.max(0, Math.floor((lowerPoint.y - minPoint.y)
				/ cellSize));

		int lowerSlice = (int) Math.max(0, Math
				.floor((lowerPoint.z - minPoint.z) / cellSize));

		int upperRow = (int) Math.min(rows, Math
				.ceil((upperPoint.x - minPoint.x) / cellSize) + 1);

		int upperCol = (int) Math.min(cols, Math
				.ceil((upperPoint.y - minPoint.y) / cellSize) + 1);

		int upperSlice = (int) Math.min(slices, Math
				.ceil((upperPoint.z - minPoint.z) / cellSize) + 1);

		for (int i = lowerRow; i < upperRow; i++) {
			for (int j = lowerCol; j < upperCol; j++) {
				for (int k = lowerSlice; k < upperSlice; k++) {
					if (hashTable[i][j][k] == null) {
						hashTable[i][j][k] = new HashMeshBBox(i, j, k);
					}
					hashTable[i][j][k].add(bbox);
				}
			}
		}

	}

}
