/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import javax.vecmath.Point3i;

/**
 * The Class GridPoint2D stores information about a narrow band node.
 */
public class GridPoint3D extends Point3i implements Comparable<GridPoint3D> {

	/** The level set value. */
	public float value;

	/** The layer indicator. */
	public int layer;

	/**
	 * Instantiates a new grid point.
	 * 
	 * @param i
	 *            the row
	 * @param j
	 *            the column
	 * @param k
	 * 			  the slice
	 * @param layer
	 *            the layer
	 * @param value
	 *            the level set value
	 */
	public GridPoint3D(int i, int j,int k, int layer, float value) {
		super(i, j,k);
		this.layer = layer;
		this.value = value;
	}

	/**
	 * Instantiates a new grid point.
	 * 
	 * @param i
	 *            the row
	 * @param j
	 *            the column
	 * @param k
	 * 			  the slice
	 * @param layer
	 *            the layer
	 */
	public GridPoint3D(int i, int j,int k, int layer) {
		super(i, j, k);
		this.layer = layer;
		this.value = Math.signum(layer) * 1E10f;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(GridPoint3D gpt) {
		return (int) Math.signum(Math.abs(layer) - Math.abs(gpt.layer));
	}

	/**
	 * @see javax.vecmath.Tuple2i#toString()
	 */
	public String toString() {
		return "[(" + x + "," + y + ","+z+") : " + layer + " :: " + value + "]";
	}
}