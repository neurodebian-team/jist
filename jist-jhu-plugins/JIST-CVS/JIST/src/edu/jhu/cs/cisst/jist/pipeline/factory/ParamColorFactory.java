package edu.jhu.cs.cisst.jist.pipeline.factory;
/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */

import java.awt.Color;

import javax.vecmath.Point3i;


import edu.jhu.cs.cisst.jist.parameter.ParamColor;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamColorInputView;
import edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamPointSpinnerInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.output.ParamOutputView;
import gov.nih.mipav.model.scripting.ParserException;
import gov.nih.mipav.model.scripting.parameters.ParameterException;
import gov.nih.mipav.model.scripting.parameters.ParameterFactory;
import gov.nih.mipav.model.scripting.parameters.ParameterTable;
import gov.nih.mipav.view.dialogs.AlgorithmParameters;

/**
 * Color Parameter Factory.
 * 
 * @author Blake Lucas
 */
public class ParamColorFactory extends ParamFactory {
	
	/** The param. */
	protected ParamColor param;

	/**
	 * Instantiates a new param color factory.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamColorFactory(ParamColor param) {
		this.param = param;
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#createMipavParameter(gov.nih.mipav.view.dialogs.AlgorithmParameters)
	 */
	public void createMipavParameter(AlgorithmParameters scriptParams) throws ParserException {
		ParamColor param = getParameter();
		Color p = param.getValue();
		scriptParams.getParams().put(
				ParameterFactory.newParameter(encodeName(param.getName()), new int[] { p.getRed(),p.getBlue(),p.getGreen(),p.getAlpha() }));
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getInputView()
	 */
	public ParamInputView getInputView() {
		if (inputView == null) {
			inputView = new ParamColorInputView(param);
		}
		return inputView;
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getOutputView()
	 */
	public ParamOutputView getOutputView() {
		if (outputView == null) {
			outputView = new ParamOutputView(param);
		}
		return outputView;
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#getParameter()
	 */
	public ParamColor getParameter() {
		return param;
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.factory.ParamFactory#importMipavParameter(gov.nih.mipav.model.scripting.parameters.ParameterTable)
	 */
	public void importMipavParameter(ParameterTable paramTable) throws ParameterException {
		int[] pt = (paramTable.getList(encodeName(getParameter().getName()))).getAsIntArray();
		getParameter().setValue(new Color(pt[0], pt[1], pt[2],pt[3]));
	}
}
