/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class PressureForce3D moves the level set outwards in regions of positive
 * pressure and inwards in regions of negative pressure.
 */
public class PressureForce3D extends ActiveContourForce3D {

	/** The pressure force. */
	protected float[][][] pressureForce;

	public PressureForce3D() {
		this.pressureForce = null;
	}

	/**
	 * Instantiates a new pressure force.
	 * 
	 * @param pressureForce
	 *            the pressure force
	 */
	public PressureForce3D(float[][][] pressureForce) {
		this.pressureForce = pressureForce;
	}

	/**
	 * Rescale the pressure field so that the min and max are normalized to one
	 * and zero pressure is at the specified offset.
	 * 
	 * @param offset
	 *            the offset pressure
	 */
	public void rescale(double offset) {
		int index = 0;
		double min = Float.MAX_VALUE;
		double max = Float.MIN_VALUE;
		for (int i = 0; i < pressureForce.length; i++) {
			for (int j = 0; j < pressureForce[0].length; j++) {
				for (int k = 0; k < pressureForce[0][0].length; k++) {
					double val = pressureForce[i][j][k] - offset;
					min = Math.min(val, min);
					max = Math.max(val, max);
					index++;
				}
			}
		}
		double normMin = (Math.abs(min) > 1E-4) ? 1 / Math.abs(min) : 1;
		double normMax = (Math.abs(max) > 1E-4) ? 1 / Math.abs(max) : 1;
		for (int i = 0; i < pressureForce.length; i++) {
			for (int j = 0; j < pressureForce[0].length; j++) {
				for (int k = 0; k < pressureForce[0][0].length; k++) {
					double val = pressureForce[i][j][k] - offset;
					if (val < 0) {
						pressureForce[i][j][k] = (float) (val * normMin);
					} else {
						pressureForce[i][j][k] = (float) (val * normMax);
					}
				}
			}
		}
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D
	 *      #evaluate(int, int)
	 */
	public double evaluate(int i, int j,int k) {
		GridPoint3D p011 = getGridPoint(i - 1, j,k);
		GridPoint3D p121 = getGridPoint(i, j + 1,k);
		GridPoint3D p111 = getGridPoint(i, j,k);
		GridPoint3D p101 = getGridPoint(i, j - 1,k);
		GridPoint3D p211 = getGridPoint(i + 1, j,k);
		GridPoint3D p110 = getGridPoint(i,j,k-1);
		GridPoint3D p112 = getGridPoint(i,j,k+1);
		
		double v111 = p111.value;
		double v011 = ((p011 != null) ? p011.value : v111);
		double v121 = ((p121 != null) ? p121.value : v111);
		double v101 = ((p101 != null) ? p101.value : v111);
		double v211 = ((p211 != null) ? p211.value : v111);
		double v110 = ((p110 != null) ? p110.value : v111);
		double v112 = ((p112 != null) ? p112.value : v111);
		double DxNeg = v111 - v011;
		double DxPos = v211 - v111;
		double DyNeg = v111 - v101;
		double DyPos = v121 - v111;
		double DzNeg = v111 - v110;
		double DzPos = v112 - v111;
		double DxNegMin = Math.min(DxNeg, 0);
		double DxNegMax = Math.max(DxNeg, 0);
		double DxPosMin = Math.min(DxPos, 0);
		double DxPosMax = Math.max(DxPos, 0);
		double DyNegMin = Math.min(DyNeg, 0);
		double DyNegMax = Math.max(DyNeg, 0);
		double DyPosMin = Math.min(DyPos, 0);
		double DyPosMax = Math.max(DyPos, 0);
		double DzNegMin = Math.min(DzNeg, 0);
		double DzNegMax = Math.max(DzNeg, 0);
		double DzPosMin = Math.min(DzPos, 0);
		double DzPosMax = Math.max(DzPos, 0);
		
		double GradientSqrPos = 
				  DxNegMax * DxNegMax + DxPosMin * DxPosMin
				+ DyNegMax * DyNegMax + DyPosMin * DyPosMin
				+ DzNegMax * DzNegMax + DzPosMin * DzPosMin;
		double GradientSqrNeg = 
			      DxPosMax * DxPosMax + DxNegMin * DxNegMin
				+ DyPosMax * DyPosMax + DyNegMin * DyNegMin
				+ DzPosMax * DzPosMax + DzNegMin * DzNegMin;
		// Force should be negative to move level set outwards if pressure is
		// positive
		double force = weight * pressureForce[i][j][k];
		if (force > 0) {
			return -force * Math.sqrt(GradientSqrPos);
		} else if (force < 0) {
			return -force * Math.sqrt(GradientSqrNeg);
		} else
			return 0;
	}

}
