/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import java.util.Comparator;

import javax.vecmath.Point3f;

import edu.jhu.cs.cisst.algorithms.geometry.surface.MeshDistanceHash.HashMeshBBox;

// TODO: Auto-generated Javadoc
/**
 * The Class BoundingBoxDistanceComparator is used for comparing distances to
 * bounding boxes.
 */
public class HashBBoxDistanceComparator3D implements Comparator<HashMeshBBox> {

	/** The point. */
	Point3f pt;

	/**
	 * Instantiates a new bounding box distance comparator.
	 * 
	 * @param pt
	 *            the point
	 */
	public HashBBoxDistanceComparator3D(Point3f pt) {
		this.pt = pt;
	}

	/**
	 * Compare.
	 * 
	 * @param box1
	 *            the bounding box1
	 * @param box2
	 *            the bounding box2
	 * 
	 * @return the comparison indicator
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(HashMeshBBox box1, HashMeshBBox box2) {
		return (int) Math.signum(box1.distanceSquared(pt)
				- box2.distanceSquared(pt));
	}

}