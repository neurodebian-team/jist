/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import java.util.BitSet;
import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoContourGenerator;

// TODO: Auto-generated Javadoc
/**
 * The Class TopologyRule2DLookUpTable.
 */
public class TopologyPreservationRule2D extends TopologyRule2D {
	
	/**
	 * Instantiates a new topology rule2 d look up table.
	 * 
	 * @param rule the rule
	 */
	public TopologyPreservationRule2D(Rule rule) {
		super(rule);
	}

	/** The Constant lut4_8. */
	private static final byte[] lut4_8 = new byte[] { 123, -13, -5, -13, -69,
			51, -69, 51, -128, -13, -128, -13, 0, 51, 0, 51, -128, -13, -128,
			-13, -69, -52, -69, -52, -128, -13, -128, -13, -69, -52, -69, -52,
			-128, 0, -128, 0, -69, 51, -69, 51, 0, 0, 0, 0, 0, 51, 0, 51, -128,
			-13, -128, -13, -69, -52, -69, -52, -128, -13, -128, -13, -69, -52,
			-69, -52, 123, -13, -5, -13, -69, 51, -69, 51, -128, -13, -128,
			-13, 0, 51, 0, 51, -128, -13, -128, -13, -69, -52, -69, -52, -128,
			-13, -128, -13, -69, -52, -69, -52, -128, 0, -128, 0, -69, 51, -69,
			51, 0, 0, 0, 0, 0, 51, 0, 51, -128, -13, -128, -13, -69, -52, -69,
			-52, -128, -13, -128, -13, -69, -52, -69, -52 };

	/**
	 * From byte array.
	 * 
	 * @param bytes the bytes
	 * 
	 * @return the bit set
	 */
	public static BitSet fromByteArray(byte[] bytes) {
		BitSet bits = new BitSet();
		for (int i = 0; i < bytes.length * 8; i++) {
			if ((bytes[bytes.length - i / 8 - 1] & (1 << (i % 8))) > 0) {
				bits.set(i);
			}
		}
		return bits;
	}

	/** The Constant lut. */
	protected static final BitSet lut;
	static {
		lut = fromByteArray(lut4_8);
	}
	
	/** The cube foreground. */
	protected boolean[][] cubeForeground = new boolean[3][3];
	
	/** The cube background. */
	protected boolean[][] cubeBackground = new boolean[3][3];

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D#applyRule(edu.jhu.cs.cisst.algorithms.segmentation.gac.GridPoint2D, double)
	 */
	public double applyRule(GridPoint2D pt, double value) {
		if (value * pt.value > 0) {
			return value;
		}
		int index = 0;
		int mask = 0;
		switch (rule) {
		case CONNECT_4:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					GridPoint2D nbr = getGridPoint(pt.x + i, pt.y + j);
					boolean flag = (nbr != null && nbr.value < 0);
					mask |= (flag) ? (1 << index) : 0;
					index++;
				}
			}
			if (lut.get(mask)) {
				return value;
			} else {
				return (Math.signum(pt.value) * IsoContourGenerator.LEVEL_SET_TOLERANCE);
			}
		case CONNECT_8:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					GridPoint2D nbr = getGridPoint(pt.x + i, pt.y + j);
					boolean flag = (nbr != null && nbr.value < 0);
					mask |= (!flag) ? (1 << index) : 0;
					index++;
				}
			}
			if (lut.get(mask)) {
				return value;
			} else {
				return (Math.signum(pt.value) * IsoContourGenerator.LEVEL_SET_TOLERANCE);
			}
		default:
			return value;
		}

	}
}
