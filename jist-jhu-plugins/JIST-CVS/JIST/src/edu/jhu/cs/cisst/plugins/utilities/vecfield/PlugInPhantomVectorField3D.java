/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.utilities.vecfield;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

public class PlugInPhantomVectorField3D extends ProcessingAlgorithm{

	protected ParamPointDouble curlParam;
	protected ParamPointDouble divergenceParam;
	protected ParamVolume referenceImage;
	protected ParamVolume vectorFieldImage;

	protected ParamPointInteger dimensionsParam;
	protected ParamPointDouble sigularityParam;
	protected ParamPointDouble translationParam;
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(referenceImage=new ParamVolume("Reference Image"));
		referenceImage.setMandatory(false);
		inputParams.add(dimensionsParam=new ParamPointInteger("Dimensions",new Point3i(128,128,128)));
		inputParams.add(curlParam=new ParamPointDouble("Rotation"));
		inputParams.add(divergenceParam=new ParamPointDouble("Divergence"));
		inputParams.add(translationParam=new ParamPointDouble("Translation"));
		inputParams.add(sigularityParam=new ParamPointDouble("Singularity Location"));
		inputParams.setName("phantom_vecfield3D");
		inputParams.setLabel("Phantom Vector Field 3D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Vector Field");
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(vectorFieldImage=new ParamVolume("Vector Field",VoxelType.FLOAT,-1,-1,-1,3));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		Point3d rotation=curlParam.getValue();
		Point3d divergence=divergenceParam.getValue();
		Point3i dimensions=dimensionsParam.getValue();
		Point3d translation=translationParam.getValue();
		Point3d singularity=sigularityParam.getValue();
		int rows,cols,slices;
		ImageData img;
		if((img=referenceImage.getImageData())!=null){
			rows=img.getRows();
			cols=img.getCols();
			slices=img.getSlices();
		} else {
			rows=dimensions.x;
			cols=dimensions.y;
			slices=dimensions.z;
		}
		ImageDataFloat vecfield=new ImageDataFloat(rows,cols,slices,3);
		vecfield.setName("vecfield");
		float[][][][] vecfieldMat=vecfield.toArray4d();
		double x, y,z;
		Vector3f curlV=new Vector3f();
		Vector3f divV=new Vector3f();
		Vector3f transForce=new Vector3f((float)translation.x,(float)translation.y,(float)translation.z);
		
		double scale=2.0/Math.max(rows,Math.max(cols,slices));
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					x=i*scale-1-singularity.x;
					y=j*scale-1-singularity.y;
					z=k*scale-1-singularity.z;
					curlV.x=(float)((-y*rotation.z)+(z*rotation.y));
					curlV.y=(float)((x*rotation.z)+(-z*rotation.x));
					curlV.z=(float)((y*rotation.x)+(-x*rotation.y));
					divV.x=(float)(x*divergence.x);
					divV.y=(float)(y*divergence.y);
					divV.z=(float)(z*divergence.z);
					vecfieldMat[i][j][k][0]=curlV.x+divV.x+transForce.x;
					vecfieldMat[i][j][k][1]=curlV.y+divV.y+transForce.y;
					vecfieldMat[i][j][k][2]=curlV.z+divV.z+transForce.z;
				}
			}
		}
		vectorFieldImage.setValue(vecfield);
	}

}
