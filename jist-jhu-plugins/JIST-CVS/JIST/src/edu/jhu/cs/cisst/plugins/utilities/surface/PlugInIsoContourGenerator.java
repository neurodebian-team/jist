package edu.jhu.cs.cisst.plugins.utilities.surface;
/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoContourGenerator;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
/**
 * The Iso-Contour Generator creates an iso-contour using the marching-squares algorithm.
 *
 */
public class PlugInIsoContourGenerator extends ProcessingAlgorithm{
	protected ParamVolume levelSetParam;
	protected ParamDouble isoLevelParam;
	protected ParamBoolean useResolutionsParam;
	protected ParamOption connectivityRuleParam;
	protected ParamOption windingParam;
	protected ParamObject<CurveCollection> contourParam;

	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(levelSetParam = new ParamVolume("Level Set Image",null,-1,-1,1,1));
		inputParams.add(isoLevelParam = new ParamDouble("Target Iso-Level", 0));
		IsoContourGenerator.Winding[] windings = IsoContourGenerator.Winding.values();
		String[] windingNames = new String[windings.length];
		for (int i = 0; i < windingNames.length; i++)
			windingNames[i] = windings[i].toString().replace('_', ' ');
		String[] ruleStrings=new String[3];
		ruleStrings[0]="NONE";
		ruleStrings[1]="Connected (4,8)";
		ruleStrings[2]="Connected (8,4)";

		inputParams.add(connectivityRuleParam=new ParamOption("Connectivity Rule",ruleStrings));
		inputParams
				.add(windingParam = new ParamOption("Winding", windingNames));
		inputParams.add(useResolutionsParam = new ParamBoolean(
				"Use Image Resolutions", true));
		inputParams.setName("iso-contour");
		inputParams.setLabel("Iso-Contour Generator");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Surface");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info
				.setDescription("Generates an iso-contour using the marching-squares algorithm.");
		info.add(new Citation("Schneider, P. and D. Eberly (2003). Geometric tools for computer graphics, Morgan Kaufmann Pub."));
		info.setVersion(IsoContourGenerator.getVersion());
		info.setStatus(DevelopmentStatus.Release);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(contourParam = new ParamObject<CurveCollection>("Iso-Contour",new CurveVtkReaderWriter()));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		IsoContourGenerator gen=new IsoContourGenerator();
		monitor.observe(gen);
		if(connectivityRuleParam.getIndex()>0){
			gen.setRule(TopologyRule2D.Rule.values()[connectivityRuleParam.getIndex()-1]);
		}
		gen.setUseResolutions(useResolutionsParam.getValue());
		gen.setWinding(IsoContourGenerator.Winding.values()[windingParam.getIndex()]);
		contourParam.setObject(gen.solve(new ImageDataFloat(levelSetParam
				.getImageData()), isoLevelParam.getFloat()));
	}



}
