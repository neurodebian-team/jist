/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.segmentation;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce3DConstant;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContourWithoutEdges3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.MeanCurvatureForce3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyPreservationRule3D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule3D;
// TODO: Auto-generated Javadoc
/**
 * The Class PlugInGACWithoutEdges3DConstant.
 */
public class PlugInGACWithoutEdges3DConstant extends ProcessingAlgorithm {
	/** The connectivity rule param. */
	ParamOption connectivityRuleParam;
	/** The reference image param. */
	ParamVolume referenceImageParam;

	/** The initial image param. */
	ParamVolume initialImageParam;

	/** The verbose segmented image param. */
	ParamVolume verboseSegmentedImageParam;

	/** The verbose estimated image param. */
	ParamVolume verboseEstimatedImageParam;

	/** The segmented image param. */
	ParamVolume segmentedImageParam;

	/** The fg pressure weight param. */
	ParamDouble fgPressureWeightParam;

	/** The bg pressure weight param. */
	ParamDouble bgPressureWeightParam;

	/** The curvature weight param. */
	ParamDouble curvatureWeightParam;

	/** The dice threshold param. */
	ParamDouble diceThresholdParam;

	/** The outer iters param. */
	ParamInteger outerItersParam;

	/** The inner iters param. */
	ParamInteger innerItersParam;

	/** The verbose param. */
	ParamBoolean verboseParam;

	/** The normalize param. */
	ParamBoolean normalizeParam;

	/** The estimated image param. */
	ParamVolume estimatedImageParam;

	/** The metric param. */
	protected ParamOption metricParam;

	/** The heaviside func param. */
	protected ParamOption heavisideFuncParam;

	/** The heaviside fuzz param. */
	protected ParamDouble heavisideFuzzParam;

	/**
	 * Creates the input parameters.
	 * 
	 * @param inputParams the input params
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		ParamCollection dataFrame = new ParamCollection("Data");
		dataFrame.add(referenceImageParam = new ParamVolume("Reference Image",
				null, -1, -1, -1, 1));
		dataFrame.add(initialImageParam = new ParamVolume(
				"Initial Levelset Image", null, -1, -1, -1, 1));
		String[] ruleStrings=new String[2];
		ruleStrings[0]="NONE";
		//ruleStrings[1]="Connected (6,18)";
		//ruleStrings[2]="Connected (18,6)";
		ruleStrings[1]="Connected (6,26)";
		//ruleStrings[4]="Connected (26,6)";
		dataFrame.add(connectivityRuleParam=new ParamOption("Connectivity Rule",ruleStrings));
		dataFrame.add(normalizeParam = new ParamBoolean(
				"Normalize Intensities", true));
		dataFrame.add(verboseParam = new ParamBoolean("Enable Verbose Output",
				false));

		ParamCollection paramFrame = new ParamCollection("Parameters");
		paramFrame.add(fgPressureWeightParam = new ParamDouble(
				"Foreground Pressure Weight", 1));
		paramFrame.add(bgPressureWeightParam = new ParamDouble(
				"Background Pressure Weight", 1));
		paramFrame.add(curvatureWeightParam = new ParamDouble(
				"Curvature Weight", 1.0));
		paramFrame.add(heavisideFuncParam = new ParamOption(
				"Heaviside Function", AppearanceForce3D.heavisideNames));
		paramFrame.add(heavisideFuzzParam = new ParamDouble(
				"Heaviside Fuzziness", 0, 1E10, 1.5));
		paramFrame.add(outerItersParam = new ParamInteger(
				"Max Outer Iterations", 0, 1000000, 50));
		paramFrame.add(innerItersParam = new ParamInteger(
				"Max Inner Iterations", 1, 1000000, 100));
		paramFrame.add(diceThresholdParam = new ParamDouble("Dice Threshold",
				0, 1, 0.9999));
		paramFrame.add(metricParam = new ParamOption("Metric",
				AppearanceForce3DConstant.metricNames));
		inputParams.add(dataFrame);
		inputParams.add(paramFrame);
		inputParams.setLabel("Active Contour Without Edges 3D - Constant");
		inputParams.setName("gacwe_3d");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setDescription("Segments a 3D image using a geodesic active contour without edges. Intensities in the foreground / background are approximated using a constant value. The active contour attempts to minimize an energy functional with an L2 penalty on a region's deviation from the constant approximation.");
		info
				.setLongDescription("Although the mean is an optimal estimator for a region's intensity, robust metrics (i.e. median statistic) achieve better performance.");
		info
				.add(new Citation(
						"Chan, T. and L. Vese (2001). Active contours without edges.IEEE Transactions on Image Processing 10(2): 266-277."));
		info.add(new Citation("DOI:10.1023/A:1008036829907"));
		info.add(new Citation("DOI:10.1016/0021-9991(88)90002-2"));
		info
				.add(new Citation(
						"Sethian, J. (1999). Level set methods and fast marching methods, Cambridge university press Cambridge."));
		info.setVersion(GeodesicActiveContourWithoutEdges3D.getVersion());
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setStatus(DevelopmentStatus.Release);
		inputParams.setPackage("CISST");
		inputParams.setCategory("Segmentation");

	}

	/**
	 * Creates the output parameters.
	 * 
	 * @param outputParams the output params
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(segmentedImageParam = new ParamVolume(
				"Level Set Image"));
		outputParams.add(estimatedImageParam = new ParamVolume(
				"Estimated Image"));
		outputParams.add(verboseSegmentedImageParam = new ParamVolume(
				"Verbose Level Set Image",VoxelType.FLOAT,-1,-1,-1,-1));
		outputParams.add(verboseEstimatedImageParam = new ParamVolume(
				"Verbose Estimated Image",VoxelType.FLOAT,-1,-1,-1,-1));
		verboseEstimatedImageParam.setMandatory(false);
		verboseSegmentedImageParam.setMandatory(false);
	}

	/**
	 * Execute.
	 * 
	 * @param monitor the monitor
	 * 
	 * @throws AlgorithmRuntimeException the algorithm runtime exception
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ImageDataFloat initIm = new ImageDataFloat(initialImageParam
				.getImageData());
		ImageData refImage = (referenceImageParam.getImageData() != null) ? referenceImageParam
				.getImageData()
				: initIm;
		GeodesicActiveContourWithoutEdges3D activeContour = new GeodesicActiveContourWithoutEdges3D(
				refImage);
		monitor.observe(activeContour);
		activeContour.setDiceThreshold(diceThresholdParam.getDouble());
		MeanCurvatureForce3D curvatureForce = new MeanCurvatureForce3D();
		curvatureForce.setWeight(curvatureWeightParam.getDouble());
		activeContour.add(curvatureForce);
		AppearanceForce3DConstant af = new AppearanceForce3DConstant(
				new ImageDataFloat(referenceImageParam.getImageData()),
				normalizeParam.getValue());
		af.setHeaviside(AppearanceForce3D.Heaviside.values()[heavisideFuncParam
				.getIndex()]);
		af.setMetric(AppearanceForce3DConstant.Metric.values()[metricParam
				.getIndex()]);
		af.setFuzziness(heavisideFuzzParam.getDouble());
		af.setForegroundWeight(fgPressureWeightParam.getDouble());
		af.setBackgroundWeight(bgPressureWeightParam.getDouble());
		activeContour.add(af);
		int innerIters = innerItersParam.getInt();
		int outerIters = outerItersParam.getInt();
		activeContour.setInnerIterations(innerIters);
		activeContour.setOuterIterations(outerIters);
		if(connectivityRuleParam.getIndex()>0){
			activeContour.attachTopologyRule(new TopologyPreservationRule3D(TopologyRule3D.Rule.values()[connectivityRuleParam.getIndex()-1]));
		}
		if (verboseParam.getValue()) {
			segmentedImageParam.setValue(activeContour.solveVerbose(initIm));
			verboseSegmentedImageParam.setValue(activeContour
					.getVerboseLevelSet());
			estimatedImageParam.setValue(activeContour.getEstimatedImage());
			verboseEstimatedImageParam.setValue(activeContour
					.getVerboseEstimatedImage());
		} else {
			segmentedImageParam.setValue(activeContour.solve(initIm));
			estimatedImageParam.setValue(activeContour.getEstimatedImage());
		}
	}

}
