/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class ActiveContourForce2D is an abstract class to represent level set forces.
 */
public abstract class ActiveContourForce2D {
	
	/** The grid points. */
	protected GridPoint2D[][] gridPoints;
	
	/** The columns. */
	protected int rows,cols;
	
	/** The weight. */
	protected double weight=1.0;
	
	/**
	 * Instantiates a new active contour force.
	 * 
	 * @param gridPoints the grid points
	 * @param weight the weight
	 */
	public ActiveContourForce2D(GridPoint2D[][] gridPoints,double weight){
		setGridPoints(gridPoints);
		this.weight=weight;
	}
	
	/**
	 * Instantiates a new active contour force.
	 * 
	 * @param gridPoints the grid points
	 */
	public ActiveContourForce2D(GridPoint2D[][] gridPoints){
		setGridPoints(gridPoints);
		this.weight=1.0;
	}
	
	/**
	 * Sets the grid points.
	 * 
	 * @param gridPoints the new grid points
	 */
	public void setGridPoints(GridPoint2D[][] gridPoints){
		this.gridPoints=gridPoints;
		this.rows=gridPoints.length;
		this.cols=gridPoints[0].length;
	}
	
	/**
	 * Instantiates a new active contour force.
	 */
	public ActiveContourForce2D(){
	}
	
	/**
	 * Gets the grid point.
	 * 
	 * @param i the i
	 * @param j the j
	 * 
	 * @return the grid point
	 */
	public GridPoint2D getGridPoint(int i,int j){
		if(i<rows&&j<cols&&i>=0&&j>=0){
			return gridPoints[i][j];
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the weight.
	 * 
	 * @return the weight
	 */
	public double getWeight(){
		return weight;
	}
	
	/**
	 * Sets the weight.
	 * 
	 * @param weight the new weight
	 */
	public void setWeight(double weight){
		this.weight=weight;
	}
	
	/**
	 * Evaluate the force at a particular grid point position.
	 * 
	 * @param x the x
	 * @param y the y
	 * 
	 * @return the double
	 */
	public abstract double evaluate(int x,int y);
	
	/**
	 * Update the force periodically while solving the PDE.
	 */
	public void update(){
		
	}
}
