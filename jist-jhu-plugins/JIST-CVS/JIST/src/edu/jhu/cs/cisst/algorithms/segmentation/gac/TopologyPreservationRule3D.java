/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import edu.jhu.cs.cisst.algorithms.geometry.surface.IsoContourGenerator;

// TODO: Auto-generated Javadoc
/**
 * The Class TopologyRule3DLookUpTable.
 */
public class TopologyPreservationRule3D extends TopologyRule3D {

	/**
	 * Instantiates a new topology rule2 d look up table.
	 * 
	 * @param rule
	 *            the rule
	 */
	public TopologyPreservationRule3D(Rule rule) {
		super(rule);
		//if (rule == Rule.CONNECT_6_18 || rule == Rule.CONNECT_18_6) {
		//	loadLUT618();
		//} else if (rule == Rule.CONNECT_6_26 || rule == Rule.CONNECT_26_6) {
			loadLUT626();
		//}
	}

	/**
	 * From byte array.
	 * 
	 * @param bytes
	 *            the bytes
	 * 
	 * @return the bit set
	 */
	public static BitSet fromByteArray(byte[] bytes) {
		BitSet bits = new BitSet();
		// int onesCount = 0;
		// int[] bitpos=new int[100];

		for (int i = 0; i < bytes.length * 8; i++) {
			if ((bytes[bytes.length - i / 8 - 1] & (1 << (i % 8))) > 0) {
				bits.set(i);
				// if(onesCount<bitpos.length)bitpos[onesCount]=i;
				// onesCount++;

			}
		}
		/*
		 * System.out.println("ONES COUNT " + 100 * (double) onesCount/ (double)
		 * bits.length() + "%" + onesCount+" "+bits.length()); for(int
		 * i=0;i<bitpos.length;i++){
		 * System.out.println("FIRST BIT LOCATIONS: "+bitpos[i]); }
		 */
		return bits;
	}

	/** The lut6_18. */
	protected static BitSet lut6_18 = null;

	/** The lut6_26. */
	protected static BitSet lut6_26 = null;

	/** The cube foreground. */
	protected boolean[][] cubeForeground = new boolean[3][3];

	/**
	 * Load lu t626.
	 * 
	 * @return true, if successful
	 */
	private boolean loadLUT626() {
		if (lut6_26 != null)
			return true;
		try {
			lut6_26 = loadLUT(new File(
					new File(TopologyPreservationRule3D.class.getResource("./")
							.toURI()), "connectivity6_26.zip"));
			return true;
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Load lu t618.
	 * 
	 * @return true, if successful
	 */
	private boolean loadLUT618() {
		if (lut6_18 != null)
			return true;
		try {
			lut6_18 = loadLUT(new File(
					new File(TopologyPreservationRule3D.class.getResource("./")
							.toURI()), "connectivity6_18.zip"));

			return true;
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Load lut.
	 * 
	 * @param f
	 *            the f
	 * @return the bit set
	 */
	private BitSet loadLUT(File f) {
		final int BUFFER = 4096;
		try {
			FileInputStream fis = new FileInputStream(f);
			ZipInputStream zis = new ZipInputStream(
					new BufferedInputStream(fis));
			ZipEntry entry;
			if ((entry = zis.getNextEntry()) != null) {
				System.out.println("Extracting: " + entry + " from "
						+ f.getAbsolutePath());
				int index = 0;
				int count = 0;
				byte[] buff = new byte[(2 << 24)];
				while ((count = zis.read(buff, index,
						Math.min(BUFFER, buff.length - index))) > 0) {
					index += count;
				}
				return fromByteArray(buff);
			}
			zis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isSimplePoint(int x, int y, int z) {
		int index = 0;
		int mask = 0;
		switch (rule) {
		/*
		case CONNECT_6_18:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(x + i, y + j, z + k);
						boolean flag = (nbr != null && nbr.value < 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_18.get(mask)) {
				return true;
			} else {
				return false;
			}
		case CONNECT_18_6:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(x + i, y + j, z + k);
						boolean flag = (nbr != null && nbr.value >= 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_18.get(mask)) {
				return true;
			} else {
				return false;
			}
			*/
		case CONNECT_6_26:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(x + i, y + j, z + k);
						boolean flag = (nbr != null && nbr.value < 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_26.get(mask)) {
				return true;
			} else {
				return false;
			}
			/*
		case CONNECT_26_6:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(x + i, y + j, z + k);
						if(nbr==null)return false;
						boolean flag = (nbr != null && nbr.value >= 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_26.get(mask)) {
				return true;
			} else {
				return false;
			}
			*/
		default:
			return false;
		}
	}

	/**
	 * Apply rule.
	 * 
	 * @param pt
	 *            the pt
	 * @param value
	 *            the value
	 * @return the double
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule3D#applyRule
	 *      (edu.jhu.cs.cisst.algorithms.segmentation.gac.GridPoint3D, double)
	 */
	public double applyRule(GridPoint3D pt, double value) {
		if (value * pt.value > 0) {
			return value;
		}
		int index = 0;
		int mask = 0;
		switch (rule) {
		/*
		case CONNECT_6_18:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(pt.x + i, pt.y + j, pt.z
								+ k);
						boolean flag = (nbr != null && nbr.value < 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_18.get(mask)) {
				return value;
			} else {
				return (Math.signum(pt.value) * IsoContourGenerator.LEVEL_SET_TOLERANCE);
			}
		case CONNECT_18_6:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(pt.x + i, pt.y + j, pt.z
								+ k);
						boolean flag = (nbr != null && nbr.value < 0);
						mask |= (!flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_18.get(mask)) {
				return value;
			} else {
				return (Math.signum(pt.value) * IsoContourGenerator.LEVEL_SET_TOLERANCE);
			}
			*/
		case CONNECT_6_26:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(pt.x + i, pt.y + j, pt.z
								+ k);
						boolean flag = (nbr != null && nbr.value < 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_26.get(mask)) {
				return value;
			} else {
				return (Math.signum(pt.value) * IsoContourGenerator.LEVEL_SET_TOLERANCE);
			}
			/*
		case CONNECT_26_6:
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					for (int k = -1; k <= 1; k++) {
						GridPoint3D nbr = getGridPoint(pt.x + i, pt.y + j, pt.z
								+ k);
						boolean flag = (nbr == null || nbr.value >= 0);
						mask |= (flag) ? (1 << index) : 0;
						index++;
					}
				}
			}
			if (lut6_26.get(mask)) {
				return value;
			} else {
				return (Math.signum(pt.value) * IsoContourGenerator.LEVEL_SET_TOLERANCE);
			}
			*/
		default:
			return value;
		}

	}
}
