/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.renderer.processing;

import javax.media.j3d.BoundingBox;

import edu.jhu.cs.cisst.vent.*;

// TODO: Auto-generated Javadoc
/**
 * JIST Extensions for Computer-Integrated Surgery
 * 
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 * 
 * @author Blake Lucas
 */
public abstract class RendererProcessing3D extends RendererProcessing{
	
	/** The visualization. */
	protected VisualizationProcessing3D visualization;
	
	/** The bbox. */
	protected BoundingBox bbox=new BoundingBox();
	
	/**
	 * Gets the bounding box.
	 * 
	 * @return the bounding box
	 */
	public BoundingBox getBoundingBox(){
		return bbox;
	}
	
	/**
	 * Sets the bounding box.
	 * 
	 * @param bbox the new bounding box
	 */
	public void setBoundingBox(BoundingBox bbox){
		this.bbox=bbox;
	}
	
	/**
	 * Sets the visualization.
	 * 
	 * @param vis the new visualization
	 */
	public void setVisualization(VisualizationProcessing vis) {
		this.visualization = (VisualizationProcessing3D)vis;
	}
}
