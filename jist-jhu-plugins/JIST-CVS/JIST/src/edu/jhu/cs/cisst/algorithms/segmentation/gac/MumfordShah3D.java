package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import no.uib.cipr.matrix.Vector;
import no.uib.cipr.matrix.sparse.AbstractIterativeSolver;
import no.uib.cipr.matrix.sparse.BiCG;
import no.uib.cipr.matrix.sparse.CG;
import no.uib.cipr.matrix.sparse.DefaultIterationMonitor;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import no.uib.cipr.matrix.sparse.IterationReporter;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.SparseVector;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.NoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.CachingPDE;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;

public class MumfordShah3D extends CachingPDE implements IterationReporter {
	/** The gradient pyramid. */
	protected float[][][][][] gradientPyramid;

	/** The heaviside pyramid. */
	protected float[][][][] heavisidePyramid;

	/** The image pyramid. */
	protected float[][][][] imagePyramid;

	/** The resolution levels. */
	protected SolverResolutionLevels levels;

	/** The smoothing weight. */
	protected double smoothWeight;

	/** The maximum iterations. */
	protected int maxIterations = 1000;

	public enum Region {
		Foreground, Background
	};

	protected Region region;

	/**
	 * Instantiates a new PDE for vector field diffusion.
	 * 
	 * @param smoothWeight
	 *            the diffusion weight
	 * @param dim
	 *            the dimension
	 * @param gradientPyramid
	 *            the gradient pyramid
	 * @param levels
	 *            the resolution levels
	 * @param maxIterations
	 *            the max iterations
	 */
	public MumfordShah3D(double smoothWeight, Region region,
			float[][][][][] gradientPyramid, float[][][][] imagePyramid,
			float[][][][] heavisidePyramid, SolverResolutionLevels levels,
			int maxIterations) {
		this.gradientPyramid = gradientPyramid;
		this.imagePyramid = imagePyramid;
		this.heavisidePyramid = heavisidePyramid;
		this.region = region;
		this.levels = levels;
		this.maxIterations = maxIterations;
		this.smoothWeight = smoothWeight;
	}

	/**
	 * implements method from PDE.
	 * 
	 * @param level
	 *            the level
	 * 
	 * @return the grid spacing
	 * 
	 * @see PDE#getGridSpacing
	 */
	public double getGridSpacing(int level) {
		return levels.getGridSpacing(level);
	}

	/**
	 * implements method from PDE.
	 * 
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param z
	 *            the z
	 * 
	 * @return the double
	 * 
	 * @see PDE#evaluateLHS
	 */
	public double evaluateLHS(ConstBoundaryGrid u, ConstNoBoundaryGrid f,
			int x, int y, int z) {
		int level = u.getLevel();
		double h = getGridSpacing(level);
		double lhs = 0;
		double ind = 0;
		double dx = gradientPyramid[level][0][x][y][z];
		double dy = gradientPyramid[level][1][x][y][z];
		double dz = gradientPyramid[level][2][x][y][z];
		double w = smoothWeight / (h * h);
		if (region == Region.Foreground) {
			double regionWeight = w * (1 - heavisidePyramid[level][x][y][z]);
			lhs = (u.get(x + 1, y, z)
					* (regionWeight - 0.5 * dx * smoothWeight)
					+ u.get(x - 1, y, z)
					* (regionWeight + 0.5 * dx * smoothWeight)
					+ u.get(x, y + 1, z)
					* (regionWeight - 0.5 * dy * smoothWeight)
					+ u.get(x, y - 1, z)
					* (regionWeight + 0.5 * dy * smoothWeight)
					+ u.get(x, y, z + 1)
					* (regionWeight - 0.5 * dz * smoothWeight)
					+ u.get(x, y, z - 1)
					* (regionWeight + 0.5 * dz * smoothWeight) - 6
					* regionWeight * u.get(x, y, z) - u.get(x, y, z));
		} else if (region == Region.Background) {
			double regionWeight = w * (heavisidePyramid[level][x][y][z]);
			lhs = (u.get(x + 1, y, z)
					* (regionWeight + 0.5 * dx * smoothWeight)
					+ u.get(x - 1, y, z)
					* (regionWeight - 0.5 * dx * smoothWeight)
					+ u.get(x, y + 1, z)
					* (regionWeight + 0.5 * dy * smoothWeight)
					+ u.get(x, y - 1, z)
					* (regionWeight - 0.5 * dy * smoothWeight)
					+ u.get(x, y, z + 1)
					* (regionWeight + 0.5 * dz * smoothWeight)
					+ u.get(x, y, z - 1)
					* (regionWeight - 0.5 * dz * smoothWeight) - 6
					* regionWeight * u.get(x, y, z) - u.get(x, y, z));
		}
		return lhs;
	}

	/**
	 * Gets the index.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * @param k
	 *            the k
	 * @param cols
	 *            the cols
	 * @param slices
	 *            the slices
	 * 
	 * @return the index
	 */
	protected static final int getIndex(int i, int j, int k, int cols,
			int slices) {
		return (i * cols * slices) + (j * slices) + k;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE#solve(edu.jhu.cs.cisst
	 *      .algorithms.optimize.fmg.grid.ConstBoundaryGrid,
	 *      edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid)
	 */
	public BoundaryGrid solve(ConstBoundaryGrid u, ConstNoBoundaryGrid f) {
		int rows = u.getRows();
		int cols = u.getCols();
		int slices = u.getSlices();
		int N = rows * cols * slices;
		int level = u.getLevel();
		double h = getGridSpacing(level);
		// System.out.println("GRID SPACING "+h+" "+rows+" "+cols);
		FlexCompRowMatrix A = new FlexCompRowMatrix(N, N);
		SparseVector b = new SparseVector(N);
		SparseVector x = new SparseVector(N);
		int index = 0;
		double w = smoothWeight / (h * h);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double dx = gradientPyramid[level][0][i][j][k];
					double dy = gradientPyramid[level][1][i][j][k];
					double dz = gradientPyramid[level][2][i][j][k];
					double indicator = heavisidePyramid[level][i][j][k];
					int v111 = getIndex(i, j, k, cols, slices);
					int v011 = getIndex(i - 1, j, k, cols, slices);
					int v211 = getIndex(i + 1, j, k, cols, slices);
					int v101 = getIndex(i, j - 1, k, cols, slices);
					int v121 = getIndex(i, j + 1, k, cols, slices);
					int v110 = getIndex(i, j, k - 1, cols, slices);
					int v112 = getIndex(i, j, k + 1, cols, slices);

					double centerWeight = 0;
					if (region == Region.Foreground) {
						double regionWeight = w * (1 - indicator);

						if (i > 0) {
							A.set(index, v011, regionWeight + 0.5 * dx
									* smoothWeight);
							centerWeight++;
						}
						if (i < rows - 1) {
							A.set(index, v211, regionWeight - 0.5 * dx
									* smoothWeight);
							centerWeight++;
						}
						if (j > 0) {
							A.set(index, v101, regionWeight + 0.5 * dy
									* smoothWeight);
							centerWeight++;
						}
						if (j < cols - 1) {
							A.set(index, v121, regionWeight - 0.5 * dy
									* smoothWeight);
							centerWeight++;
						}
						if (k > 0) {
							A.set(index, v110, regionWeight + 0.5 * dz
									* smoothWeight);
							centerWeight++;
						}
						if (k < slices - 1) {
							A.set(index, v112, regionWeight - 0.5 * dz
									* smoothWeight);
							centerWeight++;
						}
						A.set(index, v111, -centerWeight * regionWeight - 1);
					} else if (region == Region.Background) {
						double regionWeight = w * indicator;
						if (i > 0) {
							A.set(index, v011, regionWeight - 0.5 * dx
									* smoothWeight);
							centerWeight++;
						}
						if (i < rows - 1) {
							A.set(index, v211, regionWeight + 0.5 * dx
									* smoothWeight);
							centerWeight++;
						}
						if (j > 0) {
							A.set(index, v101, regionWeight - 0.5 * dy
									* smoothWeight);
							centerWeight++;
						}
						if (j < cols - 1) {
							A.set(index, v121, regionWeight + 0.5 * dy
									* smoothWeight);
							centerWeight++;
						}
						if (k > 0) {
							A.set(index, v110, regionWeight - 0.5 * dz
									* smoothWeight);
							centerWeight++;
						}
						if (k < slices - 1) {
							A.set(index, v112, regionWeight + 0.5 * dz
									* smoothWeight);
							centerWeight++;
						}

						A.set(index, v111, -centerWeight * regionWeight - 1);
					}
					b.set(index, f.get(i, j, k));
					// Use current RHS as solution
					x.set(index, 0);
					index++;
				}
			}
		}
		// Use conjugate gradient solver
		AbstractIterativeSolver solver = new BiCG(b);
		solver.getIterationMonitor().setIterationReporter(this);
		((DefaultIterationMonitor) solver.getIterationMonitor())
				.setMaxIterations(maxIterations);
		try {
			solver.solve(A, b, x);
		} catch (IterativeSolverNotConvergedException e) {
			System.err
					.println("Solver did not converge in "
							+ solver.getIterationMonitor().iterations()
							+ " iterations");
		}

		BoundaryGrid solution = (BoundaryGrid) u.newInstance(u.getRows(), u
				.getCols(), u.getSlices(), u.getLevel(), 0);
		index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					solution.set(i, j, k, x.get(index));
					index++;
				}
			}
		}
		return solution;

	}

	/**
	 * implements method from PDE.
	 * 
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param z
	 *            the z
	 * 
	 * @return the double
	 * 
	 * @see PDE#evaluate
	 */
	public double evaluate(ConstBoundaryGrid u, ConstNoBoundaryGrid f, int x,
			int y, int z) {

		int level = u.getLevel();
		double h = getGridSpacing(level);
		double val = u.get(x, y, z);
		double dx = gradientPyramid[level][0][x][y][z];
		double dy = gradientPyramid[level][1][x][y][z];
		double dz = gradientPyramid[level][2][x][y][z];
		double ind = 0;
		if (region == Region.Foreground) {
			ind = 1 - heavisidePyramid[level][x][y][z];
			double denom = (4 * smoothWeight * ind + h * h);
			val = (smoothWeight
					* (u.get(x + 1, y, z) * (ind - 0.5 * dx)
							+ u.get(x - 1, y, z) * (ind + 0.5 * dx)
							+ u.get(x, y + 1, z) * (ind - 0.5 * dy)
							+ u.get(x, y - 1, z) * (ind + 0.5 * dy)
							+ u.get(x, y, z + 1) * (ind - 0.5 * dz) + u.get(x,
							y, z - 1)
							* (ind + 0.5 * dz)

					) - f.get(x, y, z) * h * h)
					/ denom;
		} else if (region == Region.Background) {
			ind = heavisidePyramid[level][x][y][z];
			double denom = (4 * smoothWeight * ind + h * h);
			val = (smoothWeight
					* (u.get(x + 1, y, z) * (ind + 0.5 * dx)
							+ u.get(x - 1, y, z) * (ind - 0.5 * dx)
							+ u.get(x, y + 1, z) * (ind + 0.5 * dy)
							+ u.get(x, y - 1, z) * (ind - 0.5 * dy)
							+ u.get(x, y, z - 1) * (ind + 0.5 * dz) + u.get(x,
							y, z + 1)
							* (ind - 0.5 * dz)

					) - f.get(x, y, z) * h * h)
					/ denom;
		}
		return 0;
	}

	/**
	 * implements method from CachingPDE.
	 * 
	 * @param sx
	 *            the sx
	 * @param sy
	 *            the sy
	 * @param sz
	 *            the sz
	 * @param level
	 *            the level
	 * 
	 * @return the no boundary grid
	 * 
	 * @see CachingPDE#actuallySampleRHS
	 */
	@Override
	protected NoBoundaryGrid actuallySampleRHS(int sx, int sy, int sz, int level) {
		NoBoundaryGrid p = new NoBoundaryGrid(sx, sy, sz, level, 0);
		for (int i = 0; i < sx; i++) {
			for (int j = 0; j < sy; j++) {
				for (int k = 0; k < sz; k++) {
					double val = imagePyramid[level][i][j][k];
					p.set(i, j, k, -val);
				}
			}
		}
		return p;
	}

	/**
	 * implements method from CachingPDE.
	 * 
	 * @param sx
	 *            the sx
	 * @param sy
	 *            the sy
	 * @param sz
	 *            the sz
	 * @param level
	 *            the level
	 * 
	 * @return the object
	 * 
	 * @see CachingPDE#sampleMatrix
	 */
	@Override
	protected Object actuallySampleMatrix(int sx, int sy, int sz, int level) {
		return (new Double(getGridSpacing(level)));
	}

	/**
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double, int)
	 */
	public void monitor(double error, int iter) {

	}

	/**
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double,
	 *      no.uib.cipr.matrix.Vector, int)
	 */
	public void monitor(double error, Vector res, int iter) {
		if (iter % 10 == 0)
			System.out.println(iter + ") Residual:" + error);
	}
}
