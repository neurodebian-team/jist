/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.FMG;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;

/**
 * This interface defines the interpolate() method which produces a fine grid
 * from the next coarser grid by interpolation.
 * <p>
 * The Full Multigrid (FMG) algorithm works with grids of different sizes
 * corresponding to different levels. Interpolation is the process of taking a
 * function whose value is known at the grid elements of a grid at a certain
 * level (the coarse grid) and producing the values of the function at the grid
 * elements of a grid at the next finer level (the fine grid). The fine grid and
 * the coarse grid are oriented such that their corner elements coincide.
 * 
 * @see FMG
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public interface Interpolator {

	/**
	 * interpolates a function sampled on a specific grid to the next finer grid
	 * <p>
	 * .
	 * 
	 * @param grid
	 *            the input function sampled on a grid at a certain level (size
	 *            of grid must be 2^level + 1)
	 * @param levels
	 *            the levels
	 * 
	 * @return the function sampled on a grid at the next finer (higher) level
	 */
	public BoundaryGrid interpolate(ConstBoundaryGrid grid,
			SolverResolutionLevels levels);
}
