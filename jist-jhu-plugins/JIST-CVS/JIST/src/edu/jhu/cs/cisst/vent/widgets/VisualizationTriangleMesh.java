/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.widgets;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.LinkedList;

import javax.media.j3d.BoundingBox;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;

import edu.jhu.cs.cisst.jist.parameter.ParamColor;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamIntegerSliderInputView;
import edu.jhu.cs.cisst.vent.VisualizationProcessing2D;
import edu.jhu.cs.cisst.vent.VisualizationProcessing3D;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing3D;
import edu.jhu.cs.cisst.vent.renderer.processing.SurfaceRenderer;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable;
import edu.jhu.ece.iacl.jist.pipeline.view.input.Refresher;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationTriangleMesh.
 */
public class VisualizationTriangleMesh extends VisualizationProcessing3D
		implements Refreshable,MouseWheelListener {
	
	/** The bounding box. */
	protected BoundingBox bbox;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.12 $");
	}

	/** The rotation rate in radians. */
	protected float rotRate = 0.01f;

	/** The scale rate. */
	protected float wheelScaleRate = 0.1f;
	
	/** The scale rate. */
	protected float scaleRate = 0.25f;

	/** The rotation in x. */
	protected float rotx = 0;

	/** The rotation in y. */
	protected float roty = 0;

	/** The rotation in z. */
	protected float rotz = 0;

	/** The bg color. */
	protected Color bgColor = new Color(Color.black.getRGB());
	
	/** The translation in y. */
	float tx = 0, ty = 0;
	
	/** The scale. */
	float scale = 1;
	
	/** The axis length. */
	protected float axisLength = 100;
	
	/** The show axes. */
	protected boolean showAxes = true;
	
	/** The scale object. */
	protected float scaleObject=1;
	
	/** The show axes param. */
	protected ParamBoolean showAxesParam;
	
	/** The request update. */
	protected boolean requestUpdate = false;
	
	/** The scene params. */
	protected ParamCollection sceneParams;
	
	/** The scale param. */
	protected ParamDouble txParam, tyParam, rotxParam, rotyParam, rotzParam,
			scaleParam;
	
	/** The center param. */
	protected ParamPointFloat centerParam;
	
	/** The bg color param. */
	protected ParamColor bgColorParam;
	


	/**
	 * Draw axes.
	 */
	protected void drawAxes() {
		pushStyle();
		strokeWeight(2);
		stroke(color(255, 0, 0));
		float axisScale = scaleObject*0.5f;
		line(0, 0, 0, axisScale, 0, 0);
		stroke(color(0, 255, 0));
		line(0, 0, 0, 0, axisScale, 0);
		stroke(color(0, 0, 255));
		line(0, 0, 0, 0, 0, axisScale);
		popStyle();
	}

	/** The refresher. */
	protected Refresher refresher;

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing3D#setup()
	 */
	public void setup() {
		super.setup();

		this.addMouseWheelListener(this);
		float fov = PI/3.0f;
		float cameraZ = (height/2.0f) / tan(fov/2.0f);
		perspective(fov, (float)(width)/(float)(height), 
		            cameraZ/10.0f, cameraZ*10.0f);
		refresher = new Refresher();
		refresher.add(this);
		refresher.setRefreshInterval(250);
		refresher.start();
	}

	/** The center. */
	protected Point3f center = new Point3f();

	/**
	 * Reset scale.
	 */
	protected void resetScale() {
		bbox = new BoundingBox();
		for (RendererProcessing3D renderer : renderers) {
			bbox.combine(renderer.getBoundingBox());
		}
		bbox.combine(new Point3d());
		Point3d lower = new Point3d();
		Point3d upper = new Point3d();
		bbox.getLower(lower);
		bbox.getUpper(upper);
		center = new Point3f((float) (0.5 * (lower.x + upper.x)),
				(float) (0.5 * (lower.y + upper.y)),
				(float) (0.5 * (lower.z + upper.z)));
		scaleObject = 100/(float)Math.max(
				Math.max(upper.x - lower.x, upper.y - lower.y), upper.z
						- lower.z);
	}

	/**
	 * Instantiates a new visualization triangle mesh.
	 * 
	 * @param width the width
	 * @param height the height
	 */
	public VisualizationTriangleMesh(int width, int height) {
		super(width, height);
		setName("Visualize - Triangle Mesh");
	}
	
	/**
	 * Instantiates a new visualization triangle mesh.
	 *
	 */
	public VisualizationTriangleMesh() {
		super();
		setName("Visualize - Triangle Mesh");
	}
	/**
	 * Adds the.
	 * 
	 * @param surf the surf
	 */
	public void add(EmbeddedSurface surf) {
		renderers.add(new SurfaceRenderer(this, surf));
	}
	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing3D#draw()
	 */
	public void draw() {
		background(bgColor.getRed(), bgColor.getGreen(), bgColor.getBlue(),
				bgColor.getAlpha());
		lights();
		translate(width / 2 - tx, height / 2 - ty);
		rotateX(rotx);
		rotateY(roty);
		rotateZ(rotz);
		scale(scaleObject*scale);
		translate(-center.x, -center.y, -center.z);
		if (showAxes)
			drawAxes();
		super.draw();
	}

	/**
	 * Key pressed.
	 * 
	 * @see processing.core.PApplet#mouseDragged()
	 */
	public void keyPressed() {
		if (keyCode == KeyEvent.VK_LEFT) {
			rotz -= 5 * rotRate;
			if (rotz < 0)
				rotz += 2 * Math.PI;
			rotzParam.setValue(Math.toDegrees(rotz));
			requestUpdate = true;
		} else if (keyCode == KeyEvent.VK_RIGHT) {
			rotz += 5 * rotRate;
			if (rotz > 2 * Math.PI)
				rotz -= 2 * Math.PI;
			rotzParam.setValue(Math.toDegrees(rotz));
			requestUpdate = true;
		} else if (keyCode == KeyEvent.VK_UP) {
			rotx += 5 * rotRate;
			if (rotx > 2 * Math.PI)
				rotx -= 2 * Math.PI;
			rotxParam.setValue(Math.toDegrees(rotx));
			requestUpdate = true;
		} else if (keyCode == KeyEvent.VK_DOWN) {
			rotx -= 5 * rotRate;
			if (rotx < 0)
				rotx += 2 * Math.PI;
			rotxParam.setValue(Math.toDegrees(rotx));
			requestUpdate = true;
		}
	}

	/* (non-Javadoc)
	 * @see processing.core.PApplet#mouseDragged()
	 */
	public void mouseDragged() {
		if (mouseButton == RIGHT) {
			scale = Math.max(1E-4f,scale + scaleRate* (mouseY - pmouseY));
			scaleParam.setValue(scale);
			requestUpdate = true;
		} else if (mouseButton == LEFT && !keyPressed) {
			rotx += (pmouseY - mouseY) * rotRate;
			roty += (mouseX - pmouseX) * rotRate;
			if (rotx > 2 * Math.PI)
				rotx -= 2 * Math.PI;
			if (rotx < 0)
				rotx += 2 * Math.PI;
			if (roty > 2 * Math.PI)
				roty -= 2 * Math.PI;
			if (roty < 0)
				roty += 2 * Math.PI;
			rotxParam.setValue(Math.toDegrees(rotx));
			rotyParam.setValue(Math.toDegrees(roty));
			requestUpdate = true;
		} else if (mouseButton == CENTER
				|| (mouseButton == LEFT && keyPressed && keyCode == KeyEvent.VK_CONTROL)) {
			tx += (pmouseX - mouseX);
			ty += (pmouseY - mouseY);
			txParam.setValue(tx);
			tyParam.setValue(ty);
			requestUpdate = true;
		}
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationParameters#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		super.createVisualizationParameters(visualizationParameters);
		resetScale();
		sceneParams = new ParamCollection("Scene Controls");
		sceneParams.add(bgColorParam = new ParamColor("Background", bgColor));
		sceneParams.add(rotyParam = new ParamDouble("Roll", 0, 360, Math
				.toDegrees(roty)));
		rotyParam.setInputView(new ParamIntegerSliderInputView(rotyParam, 4));
		sceneParams.add(rotxParam = new ParamDouble("Pitch", 0, 360, Math
				.toDegrees(rotx)));
		rotxParam.setInputView(new ParamIntegerSliderInputView(rotxParam, 4));

		sceneParams.add(rotzParam = new ParamDouble("Yaw", 0, 360, Math
				.toDegrees(rotz)));
		rotzParam.setInputView(new ParamIntegerSliderInputView(rotzParam, 4));
		sceneParams.add(scaleParam = new ParamDouble("Scale", 0, 1E10, scale));
		sceneParams.add(centerParam = new ParamPointFloat("Center", center));
		sceneParams.add(txParam = new ParamDouble("Translation X", -1E10, 1E10,
				ty));
		sceneParams.add(tyParam = new ParamDouble("Translation Y", -1E10, 1E10,
				tx));

		sceneParams
				.add(showAxesParam = new ParamBoolean("Show Axes", showAxes));
		visualizationParameters.add(sceneParams);

	}

	/** The refresh lock. */
	protected boolean refreshLock = false;

	/**
	 * Update.
	 * 
	 * @param model the model
	 * @param view the view
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
	 * edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	public void update(ParamModel model, ParamInputView view) {
		if (!refreshLock) {
			super.update(model, view);
			if (model == showAxesParam) {
				setAxesVisible(showAxesParam.getValue());
			} else if (model == rotxParam) {
				rotx = (float) Math.toRadians(rotxParam.getFloat());
			} else if (model == rotyParam) {
				roty = (float) Math.toRadians(rotyParam.getFloat());
			} else if (model == rotzParam) {
				rotz = (float) Math.toRadians(rotzParam.getFloat());
			} else if (model == txParam) {
				tx = txParam.getFloat();
			} else if (model == tyParam) {
				ty = tyParam.getFloat();
			} else if (model == scaleParam) {
				scale = scaleParam.getFloat();
			} else if (model == centerParam) {
				center = centerParam.getValue();
			} else if (model == bgColorParam) {
				bgColor = bgColorParam.getValue();
			}
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing3D#updateVisualizationParameters()
	 */
	public void updateVisualizationParameters() {
		super.updateVisualizationParameters();
		setAxesVisible(showAxesParam.getValue());
		rotx = (float) Math.toRadians(rotxParam.getFloat());
		roty = (float) Math.toRadians(rotyParam.getFloat());
		rotz = (float) Math.toRadians(rotzParam.getFloat());
		tx = txParam.getFloat();
		ty = tyParam.getFloat();
		scale = scaleParam.getFloat();
		center = centerParam.getValue();
		bgColor = bgColorParam.getValue();

	}

	/**
	 * Sets the axes visible.
	 * 
	 * @param visible the new axes visible
	 */
	public void setAxesVisible(boolean visible) {
		this.showAxes = visible;
		rotx = (float) Math.toRadians(rotxParam.getFloat());
		roty = (float) Math.toRadians(rotyParam.getFloat());
		rotz = (float) Math.toRadians(rotzParam.getFloat());
		tx = txParam.getFloat();
		ty = tyParam.getFloat();
		scale = scaleParam.getFloat();
		center = centerParam.getValue();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#dispose()
	 */
	public void dispose() {
		refresher.stop();
		super.dispose();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.Refreshable#refresh()
	 */
	@Override
	public void refresh() {
		if (requestUpdate) {
			requestUpdate = false;
			refreshLock = true;
			sceneParams.getInputView().update();
			refreshLock = false;
		}
	}

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#getVideoFrames(long, long)
	 */
	public Image[] getVideoFrames(long frameRate, long duration) {
		//Need to create new axis to rotate around that is in plane with the view plane.
		float currentRotZ=roty;
		int frames=(int)(duration*frameRate);
		float rotRate=360.0f/frames;
		Image[] images=new Image[frames];
		for(int i=0;i<frames;i++){
			roty=(float)Math.toRadians(rotRate*i);
			images[i]=getScreenshot();
		}
		roty=currentRotZ;
		return images;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseWheelListener#mouseWheelMoved(java.awt.event.MouseWheelEvent)
	 */
	public void mouseWheelMoved(MouseWheelEvent e) {
		scale = Math.max(0, scale - wheelScaleRate * (e.getWheelRotation()));
		scaleParam.setValue(scale);
		requestUpdate = true;
	}
}
