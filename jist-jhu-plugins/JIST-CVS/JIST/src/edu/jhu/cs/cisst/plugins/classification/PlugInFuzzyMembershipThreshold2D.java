/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.classification;


import edu.jhu.cs.cisst.algorithms.util.DataOperations;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
// TODO: Auto-generated Javadoc

/**
 * The Class PlugInFuzzyMembershipThreshold2D.
 */
public class PlugInFuzzyMembershipThreshold2D extends ProcessingAlgorithm{
	
	/** The orig img. */
	ParamVolume origImg;
	
	/** The classified img. */
	ParamVolume classifiedImg;
	
	/** The low threshold. */
	ParamDouble lowThreshold;
	
	/** The high threshold. */
	ParamDouble highThreshold;
	
	/** The fuzziness. */
	ParamDouble fuzziness;
	
	/** The threshold type. */
	ParamOption thresholdType;
	
	/** The gradient mag param. */
	ParamBoolean gradientMagParam;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(origImg=new ParamVolume("Intensity Image",null,-1,-1,1,1));
		inputParams.add(lowThreshold=new ParamDouble("Lower Threshold",0.5));
		inputParams.add(highThreshold=new ParamDouble("Upper Threshold",1));
		inputParams.add(thresholdType=new ParamOption("Threshold Type",new String[]{"Step","Pulse"}));
		inputParams.add(gradientMagParam=new ParamBoolean("Threshold Gradient",false));
		inputParams.add(fuzziness=new ParamDouble("Fuzziness",4));
		AlgorithmInformation info=getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setStatus(DevelopmentStatus.Release);
		inputParams.setLabel("Fuzzy Image Intensity Threshold 2D");
		inputParams.setName("fuzzy_thresh2d");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Classification");
		
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(classifiedImg=new ParamVolume("Membership Image"));
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ImageDataFloat origIm=new ImageDataFloat(origImg.getImageData());
		float[][] origImMat=origIm.toArray2d();
		int rows=origIm.getRows();
		int cols=origIm.getCols();
		ImageDataFloat im=new ImageDataFloat(rows, cols);
		float[] hsv=new float[3];
		double tlow=lowThreshold.getDouble();
		double thigh=highThreshold.getDouble();
		
		double fuzz=fuzziness.getDouble();
		int type=thresholdType.getIndex();
		if(gradientMagParam.getValue()){
			float[][][] grad=DataOperations.gradient(origImMat, false);
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					double dx=grad[i][j][0];
					double dy=grad[i][j][1];
					double v=Math.sqrt(dx*dx+dy*dy);
					if(type==0){
						im.set(i,j,1.0/(1+Math.exp(-(v-tlow)*fuzz)));	
					} else {
						im.set(i,j,1.0/(1+Math.exp(-(v-tlow)*fuzz))-1.0/(1+Math.exp(-(v-thigh)*fuzz)));
					}
				}
			}
		} else {
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					double v=origImMat[i][j];
					if(type==0){
						im.set(i,j,1.0/(1+Math.exp(-(v-tlow)*fuzz)));	
					} else {
						im.set(i,j,1.0/(1+Math.exp(-(v-tlow)*fuzz))-1.0/(1+Math.exp(-(v-thigh)*fuzz)));
					}
				}
			}
		}
		im.setName(origIm.getName()+"_fthresh");
		classifiedImg.setValue(im);
	}

}
