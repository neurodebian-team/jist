/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.visualization;

import java.awt.Dimension;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.cs.cisst.vent.Visualization;
import edu.jhu.cs.cisst.vent.VisualizationApplication;
import edu.jhu.cs.cisst.vent.VisualizationPlugIn;
import edu.jhu.cs.cisst.vent.widgets.VisualizationActiveContour2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInVisualizeActiveContour2D.
 */
public class PlugInVisualizeActiveContour2D extends ProcessingAlgorithm
		implements VisualizationPlugIn {
	
	/** The level set param. */
	protected ParamVolume levelSetParam;
	
	/** The gvf field param. */
	protected ParamVolume gvfFieldParam;
	
	/** The pressure force param. */
	protected ParamVolume pressureForceParam;
	
	/** The orig image param. */
	protected ParamVolume origImageParam;
	
	/** The connectivity rule param. */
	protected ParamOption connectivityRuleParam;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(origImageParam = new ParamVolume("Original Image"));
		inputParams.add(levelSetParam = new ParamVolume("Level Set Image"));
		inputParams.add(gvfFieldParam = new ParamVolume("GVF Field"));
		inputParams.add(pressureForceParam = new ParamVolume("Pressure Force"));
		String[] ruleStrings=new String[3];
		ruleStrings[0]="NONE";
		ruleStrings[1]="Connected (4,8)";
		ruleStrings[2]="Connected (8,4)";

		inputParams.add(connectivityRuleParam=new ParamOption("Connectivity Rule",ruleStrings));
		pressureForceParam.setMandatory(false);
		gvfFieldParam.setMandatory(false);
		inputParams.setName("vis_active_contour2d");
		inputParams.setLabel("Visualize Active Contour 2D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Visualization");

		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info
				.setDescription("Visualizes a 2D active contour with associated forces.");
		info.setVersion(VisualizationActiveContour2D.getVersion());
		info.setStatus(DevelopmentStatus.RC);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
	}

	/** The visual. */
	protected VisualizationActiveContour2D visual;

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {

		VisualizationApplication app = new VisualizationApplication(this,
				createVisualization());
		app.runAndWait();
	}

	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.vent.VisualizationPlugIn#createVisualization()
	 */
	@Override
	public Visualization createVisualization() {
		TopologyRule2D.Rule rule=null;
		if(connectivityRuleParam.getIndex()>0){
			rule=TopologyRule2D.Rule.values()[connectivityRuleParam.getIndex()-1];
		}
		VisualizationActiveContour2D visual = new VisualizationActiveContour2D(
				new ImageDataFloat(levelSetParam.getImageData()),rule);
		ImageData origImage = origImageParam.getImageData();
		if (origImage != null) {
			visual.setOriginalImage(new ImageDataFloat(origImage));
		}
		ImageData pressure = pressureForceParam.getImageData();
		if (pressure != null) {
			visual.setPressureForce(new ImageDataFloat(pressure));
		}
		ImageData gvfField = gvfFieldParam.getImageData();
		if (gvfField != null) {
			visual.setVectorFieldForce(new ImageDataFloat(gvfField));
		}
		return visual;
	}

}
