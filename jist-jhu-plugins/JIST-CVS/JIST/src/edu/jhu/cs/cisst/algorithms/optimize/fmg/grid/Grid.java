/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.grid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * A grid in 3D whose elements are alterable doubles.
 *
 * Each element of the grid is a double value and is uniquely identified by three non-negative integer indices.
 * The grid knows about the concept of a boundary, but the actual handling of the boundary elements is
 * deferred to subclasses. Dependent on the boundary strategy of the subclass, accessing the
 * boundary elements may or may not be allowed.
 *
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public abstract class Grid implements ConstGrid {
     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#get
      */
     public final double get(int x, int y, int z) {
          Contract.pre(x >= 0 && x < sx,"0 <= x < size");
          Contract.pre(y >= 0 && y < sy,"0 <= y < size");
          Contract.pre(z >= 0 && z < sz,"0 <= z < size");
          if (isInterior(x,y,z)) {
               return g[x][y][z];
          } else {
               return getBoundary(x,y,z);
          }
     }

     /**
      * set the element at the specified position, which must be in the interior of the grid.
      *
      * @param x the x index of the element (1 <= x < (size() - 1))
      * @param y the y index of the element (1 <= y < (size() - 1))
      * @param z the z index of the element (1 <= z < (size() - 1))
      * @param v the value to which the element is to be set
      */
     public void set(int x, int y, int z, double v) {
          Contract.pre(x >= 1 && x < sx - 1,"1 <= x < size - 1");
          Contract.pre(y >= 1 && y < sy - 1,"1 <= y < size - 1");
          Contract.pre(z >= 1 && z < sz - 1,"1 <= z < size - 1");
          
          g[x][y][z] = v;
     }
     
     /**
      * add to the element at the specified position, which must be in the interior of the grid.
      *
      * @param x the x index of the element (1 <= x < (size() - 1))
      * @param y the y index of the element (1 <= y < (size() - 1))
      * @param z the z index of the element (1 <= z < (size() - 1))
      * @param v the value which is to be added to the element
      */
     public final void add(int x, int y, int z, double v) {
          Contract.pre(x >= 1 && x < sx - 1,"1 <= x < size - 1");
          Contract.pre(y >= 1 && y < sy - 1,"1 <= y < size - 1");
          Contract.pre(z >= 1 && z < sz - 1,"1 <= z < size - 1");
          
          // we can't aquire a lock for a single grid element so we do it for a whole row
          synchronized (g[x][y]) { g[x][y][z] += v; }
     }

     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#size
      */
     public final int getRows() {
          return sx;
     }
     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#size
      */
     public final int getCols() {
          return sy;
     }
     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#size
      */
     public final int getSlices() {
          return sz;
     }
     public final int getLevel() {
         return level;
    }
     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#isInterior
      */
     public final boolean isInterior(int x, int y, int z) {
          return (x > 0 && x < (sx - 1) && y > 0 && y < (sy - 1) && z > 0 && z < (sz - 1));
     }

     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#isBoundary
      *
      * @param x the x index of the element
      * @param y the y index of the element
      * @param z the z index of the element
      * @return  true if the element is at the boundary of the grid, false otherwise
      */
     public final boolean isBoundary(int x, int y, int z) {
          return ((x >= 0 && x < sx && y >= 0 && y < sy && z >= 0 && z < sz) &&
                  (x == 0 || x == (sx - 1) || y == 0 || y == (sy - 1) || z == 0 || z == (sz - 1)));
     }
     
     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#clone
      */
     public synchronized ConstGrid clone() {
          try {
               return (ConstGrid)super.clone();
          } catch (CloneNotSupportedException e) {
               throw new RuntimeException("Someting's very weird here.");
          }
     }
     
     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#add
      */
     public synchronized final Grid add(ConstGrid grid) {
          Contract.pre(this.getClass() == grid.getClass(),"grid of same type as this object");
          Grid sum = this.newInstance(sx,sy,sz,level,0);
          for (int i = 1; i < (sx - 1); i++) {
               for (int j = 1; j < (sy - 1); j++) {
                    for (int k = 1; k < (sz - 1); k++) {
                         sum.g[i][j][k] = this.g[i][j][k] + grid.get(i,j,k);
                    }
               }
          }
          return sum;
     }

     /**
      * construct from size and initial value for all elements.
      * <p>
      * The constructor allocates memory for the interior and the boundary of the grid, riscing that the
      * space for the boundary is wasted if a subclass decides to handle boundary elements differently.
      * 
      * @param size the size of the grid ( > 0)
      * @param value the initial value to which all grid element will be set
      */
     protected Grid(int sx,int sy,int sz,int level, double value) {
    	 this.sx=sx;
    	 this.sy=sy;
    	 this.sz=sz;
    	 this.level=level;
          g = new double[sx][sy][sz];
          for (int i = 0; i < sx; i++) {
               for (int j = 0; j < sy; j++) {
                    for (int k = 0; k < sz; k++) {
                         g[i][j][k] = value;
                    }
               }
          }
     }

     /**
      * get an element from the boundary of the grid.
      *
      * @param x the x index of the boundary element
      * @param y the y index of the boundary element
      * @param z the z index of the boundary element
      * @return  the element (a double value) at the specified position
      * @exception IndexOutOfBoundsException if read access to the boundary is not allowed
      */
     protected abstract double getBoundary(int x, int y, int z);
     public void setLevel(int level){
    	 this.level=level;
     }
     protected int          sx,sy,sz,level=-1;
     protected double[][][] g;
}
