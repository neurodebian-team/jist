package edu.jhu.cs.cisst.algorithms.optimize.fmg.grid;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.Contract;

/**
 * Uses a repeating value boundary condition, which is equivalent to having
 * Neumann boundary conditions with the derivative at the boundary being zero.
 * 
 * @author Blake
 * 
 */
public class RepeatBoundaryGrid extends BoundaryGrid {
	/**
	 * construct from size, initial value for all elements in the interior and
	 * initial value for all elements at the boundary.
	 * 
	 * @param sx
	 *            the dimension in X
	 * @param sy
	 *            the dimension in Y
	 * @param sz
	 *            the dimension in Z
	 * @param level
	 *            the resolution level
	 * @param interiorValue
	 *            the value to which all interior grid elements will be set
	 * @param boundaryValue
	 *            the value to which all boundary grid elements will be set
	 */
	public RepeatBoundaryGrid(int sx, int sy, int sz, int level,
			double interiorValue, double boundaryValue) {
		super(sx, sy, sz, level, boundaryValue);
		this.boundaryValue = boundaryValue;
		for (int i = 1; i < (sx - 1); i++) {
			for (int j = 1; j < (sy - 1); j++) {
				for (int k = 1; k < (sz - 1); k++) {
					g[i][j][k] = interiorValue;
				}
			}
		}
	}

	/**
	 * implements method from ConstGrid.
	 * 
	 * @param sx
	 *            the dimension in X
	 * @param sy
	 *            the dimension in Y
	 * @param sz
	 *            the dimension in Z
	 * @param level
	 *            the resolution level
	 * @param value
	 *            the value
	 * 
	 * @return the grid
	 * 
	 * @see ConstGrid#newInstance
	 */
	public Grid newInstance(int sx, int sy, int sz, int level, double value) {
		return new RepeatBoundaryGrid(sx, sy, sz, level, value, boundaryValue);
	}

	/**
	 * implements method from Grid.
	 * 
	 * the given grid element must lie at the boundary of the grid
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param z
	 *            the z
	 * 
	 * @return the boundary
	 * 
	 * @see Grid#getBoundary
	 */
	protected double getBoundary(int x, int y, int z) {
		Contract.pre(isBoundary(x, y, z),
				"grid element must be part of the boundary");
		x = (int) Math.max(1, Math.min(x, sx - 2));
		y = (int) Math.max(1, Math.min(y, sy - 2));
		z = (int) Math.max(1, Math.min(z, sz - 2));
		return g[x][y][z];
	}

	/** The boundary value. */
	private double boundaryValue;
}
