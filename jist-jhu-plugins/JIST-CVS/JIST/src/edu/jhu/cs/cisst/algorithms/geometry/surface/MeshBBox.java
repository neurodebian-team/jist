/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

// TODO: Auto-generated Javadoc
/**
 * The Class PointMeshBBox encapsulates triangles or other bounding boxes. 
 * 
 * @author Kel and Blake
 */
public class MeshBBox {

	/**
	 * The split Axis.
	 */
	public enum Axis {

		/** The X. */
		X,

		/** The Y. */
		Y,

		/** The Z. */
		Z
	}

	/** The Constant MAX_VALUE. */
	public static final float MAX_VALUE = 1E30f;

	/** The Constant MIN_VALUE. */
	public static final float MIN_VALUE = -1E30f;
	/** The min point. */
	protected Point3f minPoint;

	/** The max point. */
	protected Point3f maxPoint;

	/** The triangle. */
	protected MeshTriangle triangle;

	/** The children. */
	protected LinkedList<MeshBBox> children;

	/** The tree depth. */
	protected int depth = 1;

	/** The split axis. */
	protected Axis splitAxis = null;

	/**
	 * Gets the split axis.
	 * 
	 * @return the split axis
	 */
	public Axis getSplitAxis() {
		return splitAxis;
	}

	/**
	 * Sets the split axis.
	 * 
	 * @param splitAxis
	 *            the new split axis
	 */
	public void setSplitAxis(Axis splitAxis) {
		this.splitAxis = splitAxis;
	}

	/**
	 * Gets the depth.
	 * 
	 * @return the depth
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * Sets the depth.
	 * 
	 * @param depth
	 *            the new depth
	 */
	public void setDepth(int depth) {
		this.depth = depth;
	}

	/**
	 * Gets the centroid.
	 * 
	 * @return the centroid
	 */
	public Point3f getCentroid() {
		Point3f pt = new Point3f();
		pt.add(minPoint, maxPoint);
		pt.scale(0.5f);
		return pt;
	}

	/**
	 * Gets the dimensions.
	 * 
	 * @return the dimensions
	 */
	public Point3f getDimensions() {
		Point3f d = new Point3f();
		d.sub(maxPoint, minPoint);
		return d;
	}

	/**
	 * Gets the min point.
	 * 
	 * @return the min point
	 */
	public Point3f getMinPoint() {
		return minPoint;
	}

	/**
	 * Gets the max point.
	 * 
	 * @return the max point
	 */
	public Point3f getMaxPoint() {
		return maxPoint;
	}

	/**
	 * Gets the triangle.
	 * 
	 * @return the triangle
	 */
	public MeshTriangle getTriangle() {
		return triangle;
	}

	/**
	 * Gets the children.
	 * 
	 * @return the children
	 */
	public List<MeshBBox> getChildren() {
		return children;
	}

	/**
	 * Instantiates a new point mesh bounding box.
	 */
	public MeshBBox() {
		children = new LinkedList<MeshBBox>();
		minPoint = new Point3f();
		maxPoint = new Point3f();
		minPoint.x = minPoint.y = minPoint.z = MAX_VALUE;
		maxPoint.x = maxPoint.y = maxPoint.z = MIN_VALUE;
	}
	
	/**
	 * Update.
	 *
	 * @param box the box
	 */
	public void update(MeshBBox box){
		minPoint.x = Math.min(box.minPoint.x, minPoint.x);
		minPoint.y = Math.min(box.minPoint.y, minPoint.y);
		minPoint.z = Math.min(box.minPoint.z, minPoint.z);

		maxPoint.x = Math.max(box.maxPoint.x, maxPoint.x);
		maxPoint.y = Math.max(box.maxPoint.y, maxPoint.y);
		maxPoint.z = Math.max(box.maxPoint.z, maxPoint.z);		
	}
	/**
	 * Instantiates a new point mesh bounding box.
	 * 
	 * @param children
	 *            the children bounding boxes
	 */
	public MeshBBox(LinkedList<MeshBBox> children) {
		this.children = children;
		minPoint = new Point3f();
		maxPoint = new Point3f();
		minPoint.x = minPoint.y = minPoint.z = MAX_VALUE;
		maxPoint.x = maxPoint.y = maxPoint.z = MIN_VALUE;
		for (MeshBBox box : children) {
			minPoint.x = Math.min(box.minPoint.x, minPoint.x);
			minPoint.y = Math.min(box.minPoint.y, minPoint.y);
			minPoint.z = Math.min(box.minPoint.z, minPoint.z);

			maxPoint.x = Math.max(box.maxPoint.x, maxPoint.x);
			maxPoint.y = Math.max(box.maxPoint.y, maxPoint.y);
			maxPoint.z = Math.max(box.maxPoint.z, maxPoint.z);
		}
	}

	/**
	 * Instantiates a new point mesh bounding box.
	 * 
	 * @param tri
	 *            the triangle
	 */
	public MeshBBox(MeshTriangle tri) {
		this();

		for (Point3f vert : tri.getVertices()) {
			minPoint.x = Math.min(vert.x, minPoint.x);
			minPoint.y = Math.min(vert.y, minPoint.y);
			minPoint.z = Math.min(vert.z, minPoint.z);

			maxPoint.x = Math.max(vert.x, maxPoint.x);
			maxPoint.y = Math.max(vert.y, maxPoint.y);
			maxPoint.z = Math.max(vert.z, maxPoint.z);
		}
		this.triangle = tri;
	}

	/** The cached distance. */
	protected double cachedDistance = 0;

	/** The cached point. */
	protected Point3f cachedPoint = null;

	/**
	 * Clear cached point.
	 */
	public void clearCache() {
		cachedPoint = null;
		cachedDistance = 0;
	}

	/**
	 * Get the distance squared to a point.
	 * 
	 * @param pt
	 *            the point
	 * 
	 * @return the distance squared
	 */
	public double distanceSquared(Point3f pt) {
		if (pt.equals(cachedPoint))
			return cachedDistance;
		Vector3f closestPt = new Vector3f();
		if (pt.x < minPoint.x) {
			closestPt.x = minPoint.x - pt.x;
		} else if (pt.x > maxPoint.x) {
			closestPt.x = pt.x - maxPoint.x;
		} else {
			closestPt.x = 0;
		}
		if (pt.y < minPoint.y) {
			closestPt.y = minPoint.y - pt.y;
		} else if (pt.y > maxPoint.y) {
			closestPt.y = pt.y - maxPoint.y;
		} else {
			closestPt.y = 0;
		}
		if (pt.z < minPoint.z) {
			closestPt.z = minPoint.z - pt.z;
		} else if (pt.z > maxPoint.z) {
			closestPt.z = pt.z - maxPoint.z;
		} else {
			closestPt.z = 0;
		}
		cachedDistance = closestPt.lengthSquared();
		cachedPoint = pt;
		return cachedDistance;
	}

	/**
	 * Get the distance to a point.
	 * 
	 * @param pt
	 *            the point
	 * 
	 * @return the distance
	 */
	public double distance(Point3f pt) {
		return Math.sqrt(distanceSquared(pt));
	}
}
