/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.widgets;

import java.awt.Image;
import java.util.ArrayList;
import java.util.LinkedList;

import processing.core.PMatrix3D;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule3D;
import edu.jhu.cs.cisst.vent.*;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing3D;
import edu.jhu.cs.cisst.vent.renderer.processing.VectorFieldSliceRenderer3D;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeIsoSurfaceRenderer;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer3D;
import edu.jhu.cs.cisst.vent.widgets.VisualizationCRUISE2D.SliceViewObserver;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationActiveContour3D.
 */
public class VisualizationActiveContour3D extends VisualizationImage3D {
	/** The rule. */
	protected TopologyRule3D.Rule rule;

	protected VolumeIsoSurfaceRenderer isoSurfRenderer;

	/** The visualization. */
	protected VisualizationProcessing3D visualization;

	/** The slice renderers. */
	protected LinkedList<VolumeSliceRenderer3D> sliceRenderers;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.5 $");
	}

	/** The vector field image. */
	ImageDataFloat levelset = null, pressureImage = null,
			vectorFieldImage = null;

	/** The original image. */
	ImageData originalImage = null;

	/**
	 * Instantiates a new visualization active contour.
	 * 
	 * @param levelset
	 *            the levelset
	 * @param rule
	 *            the rule
	 */
	public VisualizationActiveContour3D(int width, int height,
			PMatrix3D volToImageTransform) {
		super(width, height, volToImageTransform);
		setName("Visualization - Active Contour 3D");
	}

	/**
	 * Creates the.
	 * 
	 * @return the param collection
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#create()
	 */
	@Override
	public ParamCollection create() {
		renderers = new ArrayList<RendererProcessing3D>();

		sliceRenderers = new LinkedList<VolumeSliceRenderer3D>();
		if (vectorFieldImage != null) {
			VectorFieldSliceRenderer3D tmp = new VectorFieldSliceRenderer3D(
					vectorFieldImage, volToImageTransform, this);
			renderers.add(tmp);
			sliceRenderers.add(tmp);
			tmp.setVisible(false);
		}
		if (levelset != null) {
			isoSurfRenderer = new VolumeIsoSurfaceRenderer(levelset,
					volToImageTransform, this);
			renderers.add(isoSurfRenderer);
			sliceRenderers.add(isoSurfRenderer);
			isoSurfRenderer.setShowZplane(false);
		}
		if (originalImage != null) {
			VolumeSliceRenderer3D tmp = new VolumeSliceRenderer3D(
					originalImage, volToImageTransform, this);
			renderers.add(tmp);
			sliceRenderers.add(tmp);
		}
		if (pressureImage != null) {
			VolumeSliceRenderer3D tmp = new VolumeSliceRenderer3D(
					pressureImage, volToImageTransform, this);
			renderers.add(tmp);
			sliceRenderers.add(tmp);
		}

		ParamCollection params = super.create();
		for (VolumeSliceRenderer3D sliceRenderer : sliceRenderers) {
			sliceRenderer.getSliceParameter().getInputView()
					.addObserver(new SliceViewObserver(sliceRenderer));
		}
		return params;
	}

	/**
	 * An asynchronous update interface for receiving notifications about
	 * SliceView information as the SliceView is constructed.
	 */
	protected class SliceViewObserver implements ParamViewObserver {

		/** The renderer. */
		protected VolumeSliceRenderer3D renderer;

		/**
		 * This method is called when information about an SliceView which was
		 * previously requested using an asynchronous interface becomes
		 * available.
		 * 
		 * @param renderer
		 *            the renderer
		 */
		public SliceViewObserver(VolumeSliceRenderer3D renderer) {
			this.renderer = renderer;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update
		 * (edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
		 * edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
		 */
		public void update(ParamModel model, ParamInputView view) {
			int row = renderer.getRowParameter().getInt();
			int col = renderer.getColParameter().getInt();
			int slice = renderer.getSliceParameter().getInt();
			for (VolumeSliceRenderer3D sliceRenderer2 : sliceRenderers) {
				if (sliceRenderer2 != renderer) {
					if (sliceRenderer2.getRowParameter().getInt() != row) {
						sliceRenderer2.getRowParameter().setValue(row);
						sliceRenderer2.getRowParameter().getInputView()
								.update();
					} else if (sliceRenderer2.getColParameter().getInt() != col) {
						sliceRenderer2.getColParameter().setValue(col);
						sliceRenderer2.getColParameter().getInputView()
								.update();
					} else if (sliceRenderer2.getSliceParameter().getInt() != slice) {
						sliceRenderer2.getSliceParameter().setValue(slice);
						sliceRenderer2.getSliceParameter().getInputView()
								.update();
					}
				}
			}
		}
	}

	/**
	 * Sets the pressure force.
	 * 
	 * @param pressureImage
	 *            the new pressure force
	 */
	public void setPressureForce(ImageDataFloat pressureImage) {
		this.pressureImage = pressureImage;
	}

	/**
	 * Sets the vector field force.
	 * 
	 * @param vectorFieldImage
	 *            the new vector field force
	 */
	public void setVectorFieldForce(ImageDataFloat vectorFieldImage) {
		this.vectorFieldImage = vectorFieldImage;
	}

	/**
	 * Sets the original image.
	 * 
	 * @param img
	 *            the new original image
	 */
	public void setOriginalImage(ImageData img) {
		this.originalImage = img;
	}

	/**
	 * Sets the visualization.
	 * 
	 * @param vis
	 *            the new visualization
	 */
	public void setVisualization(VisualizationProcessing vis) {
		this.visualization = (VisualizationProcessing3D) vis;
	}

	public void setLevelSet(ImageDataFloat levelset) {
		this.levelset = levelset;
	}

	public Image[] getVideoFrames(long frameRate, long duration) {
		if (isoSurfRenderer != null) {
			int frames = isoSurfRenderer.getComponents();
			if(frames<2){
				return super.getVideoFrames(frameRate, duration);
			}
			Image[] images = new Image[frames];
			for (int i = 0; i < frames; i++) {
				if (isoSurfRenderer != null) {
					if (i < isoSurfRenderer.getComponents()) {
						isoSurfRenderer.setComponent(i);
					}
				}
				images[i] = getScreenshot();
			}
			return images;
		} else {
			return super.getVideoFrames(frameRate, duration);
		}
	}
}
