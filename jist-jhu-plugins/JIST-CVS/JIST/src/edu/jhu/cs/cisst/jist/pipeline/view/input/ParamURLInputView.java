/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.cs.cisst.jist.pipeline.view.input;

import java.net.URL;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import edu.jhu.cs.cisst.jist.parameter.ParamURL;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;

/**
 * URL Parameter Input View creates a text field to enter a file and a browse
 * button to select a file. The file name can be mandatory or not mandatory. The
 * file extension filter can be set to only permit files with specific
 * extensions.
 * 
 * @author Blake Lucas
 */
public class ParamURLInputView extends ParamInputView implements CaretListener {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8584156964249536461L;
	/** The field. */
	private JTextField field;

	/**
	 * Construct text field to enter file name and browse button to select file.
	 * 
	 * @param param
	 *            the param
	 */
	public ParamURLInputView(ParamURL param) {
		super(param);
		field = new JTextField("http://");
		field.setPreferredSize(defaultTextFieldDimension);
		field.addCaretListener(this);
		buildLabelAndParam(field);
		update();
	}

	/**
	 * Update parameter when text field is changed.
	 * 
	 * @param event
	 *            field text changed
	 */
	public void caretUpdate(CaretEvent event) {
		if (event.getSource().equals(field)) {
			getParameter().setValue(field.getText());
		}
	}

	/**
	 * Commit changes to this parameter.
	 */
	public void commit() {
		getParameter().setValue(field.getText());
		notifyObservers(param, this);
	}

	/**
	 * Get file parameter.
	 * 
	 * @return the parameter
	 */
	public ParamURL getParameter() {
		return (ParamURL) param;
	}

	/**
	 * Update the textfield with the current parameter file name.
	 */
	public void update() {
		URL url = getParameter().getValue();
		if (url != null) {
			field.setText(url.toString());
		}
	}

	/**
	 * Get field used to enter this value
	 */
	public JComponent getField() {
		return field;
	}
}
