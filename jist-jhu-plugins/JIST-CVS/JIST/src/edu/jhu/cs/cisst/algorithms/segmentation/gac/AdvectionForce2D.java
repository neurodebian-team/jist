/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class AdvectionForce2D moves the zero level set in the direction of the
 * force field.
 */
public class AdvectionForce2D extends ActiveContourForce2D {

	/** The advection force. */
	protected float[][][] advectionForce;

	/**
	 * Instantiates a new advection force.
	 * 
	 * @param advectionForce
	 *            the advection force
	 */
	public AdvectionForce2D(float[][][] advectionForce) {
		this.advectionForce = advectionForce;
	}
	public void setAdvectionForce(float[][][] advectionForce){
		this.advectionForce=advectionForce;
	}
	public float[][][] getAdvectionForce(){
		return advectionForce;
	}
	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D#evaluate(int,
	 *      int)
	 */
	public double evaluate(int i, int j) {
		GridPoint2D p01 = getGridPoint(i - 1, j);
		GridPoint2D p12 = getGridPoint(i, j + 1);
		GridPoint2D p11 = getGridPoint(i, j);
		GridPoint2D p10 = getGridPoint(i, j - 1);
		GridPoint2D p21 = getGridPoint(i + 1, j);
		double v11 = p11.value;
		double v01 = ((p01 != null) ? p01.value : v11);
		double v12 = ((p12 != null) ? p12.value : v11);
		double v10 = ((p10 != null) ? p10.value : v11);
		double v21 = ((p21 != null) ? p21.value : v11);
		double DxNeg = v11 - v01;
		double DxPos = v21 - v11;
		double DyNeg = v11 - v10;
		double DyPos = v12 - v11;
		// Level set force should be the opposite sign of advection force so it
		// moves in the direction of the force.
		double forceX = weight * advectionForce[i][j][0];
		double forceY = weight * advectionForce[i][j][1];
		double advection = 0;
		//Dot product force with upwind gradient
		if (forceX > 0) {
			advection = forceX * DxNeg;
		} else if (forceX < 0) {
			advection = forceX * DxPos;
		}
		if (forceY > 0) {
			advection += forceY * DyNeg;
		} else if (forceY < 0) {
			advection += forceY * DyPos;
		}
		return -advection;
	}

}
