/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import no.uib.cipr.matrix.Vector;
import no.uib.cipr.matrix.sparse.AbstractIterativeSolver;
import no.uib.cipr.matrix.sparse.BiCG;
import no.uib.cipr.matrix.sparse.BiCGstab;
import no.uib.cipr.matrix.sparse.CG;
import no.uib.cipr.matrix.sparse.CGS;
import no.uib.cipr.matrix.sparse.DefaultIterationMonitor;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import no.uib.cipr.matrix.sparse.IterationReporter;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.SparseVector;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.DefaultSolverResolutionLevels2D;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.FMG;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.FixedBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.RepeatBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator.Interpolator;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.interpolator.InterpolatorByStencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.Parallelizer;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor.Restrictor;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor.RestrictorByStencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.DampedJacobi;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.RedBlackGaussSeidel;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.Smoother;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.solver.ArithmeticSolver;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.solver.Solver;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.VectorFieldDiffusion2D.Dimension;
import edu.jhu.cs.cisst.algorithms.util.DataOperations;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class GradientVectorFlowField2D extends a gradient field using vector
 * field diffusion.
 */
public class GradientVectorFlow2D extends AbstractCalculation implements
		IterationReporter {

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.5 $");
	}

	/**
	 * The Enum Method.
	 */
	public enum Method {

		/** The Multi-grid solver. */
		MULTIGRID,
		/** The conjugate gradient solver. */
		CG,
		/** The conjugate gradient squared solver. */
		CGS,
		/** The Biconjugate graident solver. */
		BiCG,
		/** The stabilized biconjugate gradient solver. */
		BiCGstab
	}

	/** The diffusion weight. */
	protected double diffusionWeight = 0.5;

	/** The rows. */
	protected int rows;

	/** The cols. */
	protected int cols;

	/** The std. deviation. */
	protected double stdDeviation = 1;

	/** The multigrid threads. */
	protected int multiGridThreads = 1;

	/** The smooth iterations. */
	protected int smoothIterations = 10;

	/** The multi-grid cycles. */
	protected int multiGridCycles = 1;

	/** The multi-grid iterations. */
	protected int multiGridIterations = 2;

	/** The multi-grid resolutions. */
	protected int multiGridResolutions = 4;

	/** The coarsest level resolution. */
	protected double coarsestLevelResolution = 0.125;

	/** The normalize. */
	protected boolean normalize = false;

	/** The max iterations. */
	protected int maxIterations = 10000;

	/**
	 * Instantiates a new gradient vector flow field2 d.
	 */
	public GradientVectorFlow2D() {
		super();
		setLabel("GVF");
	}

	/**
	 * Instantiates a new gradient vector flow field2 d.
	 * 
	 * @param stdDeviation
	 *            the std deviation
	 * @param diffusionWeight
	 *            the diffusion weight
	 * @param normalize
	 *            the normalize
	 */
	public GradientVectorFlow2D(double stdDeviation, double diffusionWeight,
			boolean normalize) {
		super();
		setLabel("GVF");
		this.diffusionWeight = diffusionWeight;
		this.stdDeviation = stdDeviation;
		this.normalize = normalize;
	}

	/**
	 * Gets the diffusion weight.
	 * 
	 * @return the diffusion weight
	 */
	public double getDiffusionWeight() {
		return diffusionWeight;
	}

	/**
	 * Gets the index.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * 
	 * @return the index
	 */
	protected int getIndex(int i, int j) {
		return i * cols + j;
	}


	/**
	 * Gets the max iterations.
	 * 
	 * @return the max iterations
	 */
	public int getMaxIterations() {
		return maxIterations;
	}

	/**
	 * Gets the standard deviation.
	 * 
	 * @return the standard deviation
	 */
	public double getStandardDeviation() {
		return stdDeviation;
	};

	/**
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double, int)
	 */
	public void monitor(double err, int iter) {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double,
	 *      no.uib.cipr.matrix.Vector, int)
	 */
	public void monitor(double err, Vector res, int iter) {
		setCompletedUnits(iter);
		if (iter % 100 == 0)
			System.out.println(iter + ") " + err);
	}

	/**
	 * Sets the coarsest level resolution.
	 * 
	 * @param coarsestLevelResolution
	 *            the new coarsest level resolution
	 */
	public void setCoarsestLevelResolution(double coarsestLevelResolution) {
		this.coarsestLevelResolution = coarsestLevelResolution;
	}

	/**
	 * Sets the diffusion weight.
	 * 
	 * @param diffusionWeight
	 *            the new diffusion weight
	 */
	public void setDiffusionWeight(double diffusionWeight) {
		this.diffusionWeight = diffusionWeight;
	}

	/**
	 * Sets the max iterations.
	 * 
	 * @param maxIterations
	 *            the new max iterations
	 */
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	/**
	 * Sets the normalize.
	 * 
	 * @param normalize
	 *            the new normalize
	 */
	public void setNormalize(boolean normalize) {
		this.normalize = normalize;
	}

	/**
	 * Sets the multigrid cycles.
	 * 
	 * @param numMultigridCycles
	 *            the new multigrid cycles
	 */
	public void setMultiGridCycles(int numMultigridCycles) {
		this.multiGridCycles = numMultigridCycles;
	}

	/**
	 * Sets the multi grid iterations.
	 * 
	 * @param numMultiGridIterations
	 *            the new multi grid iterations
	 */
	public void setMultiGridIterations(int numMultiGridIterations) {
		this.multiGridIterations = numMultiGridIterations;
	}

	/**
	 * Sets the multi grid resolutions.
	 * 
	 * @param numMultiGridLevels
	 *            the new multi grid resolutions
	 */
	public void setMultiGridResolutions(int numMultiGridLevels) {
		this.multiGridResolutions = numMultiGridLevels;
	}

	/**
	 * Sets the multi grid smooth iterations.
	 * 
	 * @param numMultiGridSmoothIterations
	 *            the new multi grid smooth iterations
	 */
	public void setMultiGridSmoothIterations(int numMultiGridSmoothIterations) {
		this.smoothIterations = numMultiGridSmoothIterations;
	}

	/**
	 * Sets the multi grid threads.
	 * 
	 * @param numMultiGridThreads
	 *            the new multi grid threads
	 */
	public void setMultiGridThreads(int numMultiGridThreads) {
		this.multiGridThreads = numMultiGridThreads;
	}

	/**
	 * Sets the standard deviation.
	 * 
	 * @param stdDeviation
	 *            the new standard deviation
	 */
	public void setStandardDeviation(double stdDeviation) {
		this.stdDeviation = stdDeviation;
	}

	/**
	 * Solve using either multigrid or a sparse matrix method.
	 * 
	 * @param image
	 *            the image
	 * 
	 * @return the image data float
	 */
	public ImageDataFloat solve(ImageDataFloat image, Method method) {
		rows = image.getRows();
		cols = image.getCols();
		if (method == Method.MULTIGRID) {
			return solveMultiGrid(image);
		} else {
			return solveLinear(image, method);
		}
	}

	/**
	 * Solve using a sparse matrix method.
	 * 
	 * @param image
	 *            the image
	 * 
	 * @return the image data float
	 */
	protected ImageDataFloat solveLinear(ImageDataFloat image, Method method) {
		float[][][] gradient = DataOperations.gradient(DataOperations.blur(
				image.toArray2d(), stdDeviation), false);
		int N = rows * cols;
		FlexCompRowMatrix A = new FlexCompRowMatrix(N, N);
		SparseVector bX = new SparseVector(N);
		SparseVector bY = new SparseVector(N);
		SparseVector dX = new SparseVector(N);
		SparseVector dY = new SparseVector(N);
		int index = 0;
		// Setup linear solver
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double dx = gradient[i][j][0];
				double dy = gradient[i][j][1];
				double magSqr = dx * dx + dy * dy;
				double centerWeight = 0;
				int v11 = getIndex(i, j);
				int v01 = getIndex(i - 1, j);
				int v21 = getIndex(i + 1, j);
				int v10 = getIndex(i, j - 1);
				int v12 = getIndex(i, j + 1);

				if (i > 0) {
					A.set(index, v01, diffusionWeight);
					centerWeight++;
				}
				if (i < rows - 1) {
					A.set(index, v21, diffusionWeight);
					centerWeight++;
				}
				if (j > 0) {
					A.set(index, v10, diffusionWeight);
					centerWeight++;
				}
				if (j < cols - 1) {
					A.set(index, v12, diffusionWeight);
					centerWeight++;
				}
				A.set(index, v11, -centerWeight * diffusionWeight - magSqr);
				bX.set(index, -dx * magSqr);
				dX.set(index, dx);
				bY.set(index, -dy * magSqr);
				dY.set(index, dy);
				index++;

			}
		}
		AbstractIterativeSolver solver = null;
		// Choose solving method
		switch (method) {
		case CG:
			solver = new CG(bX);
			break;
		case CGS:
			solver = new CGS(bX);
			break;
		case BiCG:
			solver = new BiCG(bX);
			break;
		case BiCGstab:
			solver = new BiCGstab(bX);
			break;
		}
		DefaultIterationMonitor im = new DefaultIterationMonitor();
		im.setMaxIterations(maxIterations);
		im.setIterationReporter(this);
		solver.setIterationMonitor(im);
		setTotalUnits(maxIterations);
		try {
			solver.solve(A, bX, dX);
		} catch (IterativeSolverNotConvergedException e) {
			e.printStackTrace();
		}
		setTotalUnits(maxIterations);
		try {
			solver.solve(A, bY, dY);
		} catch (IterativeSolverNotConvergedException e) {
			e.printStackTrace();
		}
		index = 0;
		if (normalize) {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					double dx = dX.get(index);
					double dy = dY.get(index);

					double mag = dx * dx + dy * dy;
					mag = (mag > 1E-5) ? 1.0 / Math.sqrt(mag) : 1.0;
					gradient[i][j][0] = (float) (dx * mag);
					gradient[i][j][1] = (float) (dy * mag);
					index++;
				}
			}
		} else {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					double dx = dX.get(index);
					double dy = dY.get(index);
					gradient[i][j][0] = (float) dx;
					gradient[i][j][1] = (float) dy;
					index++;
				}
			}
		}

		ImageDataFloat gvfField = new ImageDataFloat(gradient);
		gvfField.setName(image.getName() + "_gvf");
		gvfField.setHeader(image.getHeader());
		setLabel("GVF");
		markCompleted();
		return gvfField;

	}

	/**
	 * Solve using multi-grid.
	 * 
	 * @param image
	 *            the image
	 * 
	 * @return the image data float
	 */
	protected ImageDataFloat solveMultiGrid(ImageDataFloat image) {
		setTotalUnits(4);
		Parallelizer.setDefaultNumberOfThreads(multiGridThreads);
		Restrictor restrictor = new RestrictorByStencil(
				RestrictorByStencil.FULL_WEIGHTING_2D);
		Interpolator interpolator = new InterpolatorByStencil(
				InterpolatorByStencil.BILINEAR);
		// Setup prototypes for correction
		ConstBoundaryGrid prototype = new FixedBoundaryGrid(rows, cols, 3, 0,
				0, 0);
		DefaultSolverResolutionLevels2D resolutions = new DefaultSolverResolutionLevels2D(
				prototype, (int) Math.round(Math.max(rows, cols)
						* coarsestLevelResolution), multiGridResolutions);
		for (int i = 0; i < resolutions.getResolutionCount(); i++) {
			int[] res = resolutions.getResolution(i);
			System.out.println("Resolution: (" + res[0] + "," + res[1] + ","
					+ res[2] + ") Grid-spacing: "
					+ resolutions.getGridSpacing(i));
		}
		// Create gradient pyramid
		float[][][][] gradientPyramid = new float[resolutions
				.getResolutionCount()][2][rows][cols];
		// Create initial gradient image
		float[][] lastBlurImage;
		float[][][] lastGradientImage = DataOperations.gradient(
				lastBlurImage = DataOperations.blur(image.toArray2d(),
						stdDeviation), true);
		gradientPyramid[gradientPyramid.length - 1] = lastGradientImage;
		for (int l = 1; l < gradientPyramid.length; l++) {
			int[] dims = resolutions.getResolution(gradientPyramid.length - 1
					- l);
			// X gradient
			gradientPyramid[gradientPyramid.length - 1 - l] = DataOperations
					.gradient(DataOperations.restrict(lastBlurImage,
							RestrictorByStencil.FULL_WEIGHTING_2D, dims[0],
							dims[1]), true);
		}
		incrementCompletedUnits();
		// Solve PDE in X
		VectorFieldDiffusion2D pdeX = new VectorFieldDiffusion2D(
				diffusionWeight, Dimension.X, gradientPyramid, resolutions,
				maxIterations);
		Smoother smX = new RedBlackGaussSeidel(pdeX);
		// Smoother smX =new
		// DampedJacobi(pdeX,DampedJacobi.IDEAL_WEIGHTING_FACTOR);
		Solver sX = new ArithmeticSolver(pdeX);
		FMG fmg = new FMG(pdeX, smX, restrictor, interpolator, sX, prototype,
				prototype, resolutions);
		ConstBoundaryGrid resultX = fmg.solve(smoothIterations,
				smoothIterations, multiGridCycles, multiGridIterations);
		incrementCompletedUnits();
		// Solve PDE in Y
		VectorFieldDiffusion2D pdeY = new VectorFieldDiffusion2D(
				diffusionWeight, Dimension.Y, gradientPyramid, resolutions,
				maxIterations);
		Smoother smY = new RedBlackGaussSeidel(pdeY);
		// Smoother smY =new
		// DampedJacobi(pdeY,DampedJacobi.IDEAL_WEIGHTING_FACTOR);
		Solver sY = new ArithmeticSolver(pdeY);

		fmg = new FMG(pdeY, smY, restrictor, interpolator, sY, prototype,
				prototype, resolutions);
		ConstBoundaryGrid resultY = fmg.solve(smoothIterations,
				smoothIterations, multiGridCycles, multiGridIterations);
		incrementCompletedUnits();
		// Create gvf field
		float[][][] gvf = new float[rows][cols][2];
		if (normalize) {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					double dx = resultX.get(i, j, 1);
					double dy = resultY.get(i, j, 1);
					double mag = dx * dx + dy * dy;
					mag = (mag > 1E-5) ? 1.0 / Math.sqrt(mag) : 1.0;
					gvf[i][j][0] = (float) (dx * mag);
					gvf[i][j][1] = (float) (dy * mag);
				}
			}
		} else {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					double dx = resultX.get(i, j, 1);
					double dy = resultY.get(i, j, 1);
					gvf[i][j][0] = (float) dx;
					gvf[i][j][1] = (float) dy;
				}
			}
		}

		ImageDataFloat gvfImage = new ImageDataFloat(gvf);
		gvfImage.setName(image.getName() + "_gvf");
		gvfImage.setHeader(image.getHeader());
		setLabel("GVF");
		markCompleted();
		return gvfImage;
	}
}
