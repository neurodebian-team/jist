/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.grid;

/**
 * An implementation of class Grid where no boundary is allowed.
 *
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class NoBoundaryGrid extends Grid implements ConstNoBoundaryGrid {
     /**
      * construct from size and initial value for all elements in the interior.
      *
      * @param sx the dimension in X
      * @param sy the dimension in Y
      * @param sz the dimension in Z
      * @param level the resolution level
      * @param interiorValue the initial value to which all grid elements will be set
      */
     public NoBoundaryGrid(int sx,int sy,int sz, int level,double interiorValue) {
          super(sx,sy,sz,level,interiorValue);
     }

     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#newInstance
      */
     public Grid newInstance(int sx,int sy,int sz,int level, double value) {
          return new NoBoundaryGrid(sx,sy,sz,level,value);
     }

     /**
      * implements method from Grid.
      * <p>
      * Since objects of class NoBoundaryGrid don't have a boundary, this method simply throws an exception.
      *
      * @see Grid#getBoundary
      */
     protected double getBoundary(int x, int y, int z) {
         return g[x][y][z]; 
    	 //throw new IndexOutOfBoundsException("This grid does not have a boundary.");
     }
}
