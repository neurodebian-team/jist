package edu.jhu.cs.cisst.plugins.utilities.file;

import java.io.File;
import edu.jhu.cs.cisst.algorithms.util.DownloadFile;
import edu.jhu.cs.cisst.algorithms.util.DownloadFile.DataType;
import edu.jhu.cs.cisst.jist.parameter.ParamURL;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;

public class PlugInDownload extends ProcessingAlgorithm {
	protected ParamURL urlFile;
	protected ParamOption type;
	protected ParamFile downloadFile;

	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(urlFile = new ParamURL("URL"));

		inputParams.add(type = new ParamOption("Data Type", new String[] {
				"BINARY", "ASCII" }));
		inputParams.setName("download");
		inputParams.setLabel("Download File");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.File");
		
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setDescription("Downloads a file from a specified URL");
		info.setStatus(DevelopmentStatus.Release);
		info.setVersion(DownloadFile.getVersion());
	}

	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(downloadFile = new ParamFile("File"));
	}

	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		File dir = new File(this.getOutputDirectory(), File.separatorChar
				+ edu.jhu.ece.iacl.jist.utility.FileUtil
						.forceSafeFilename(getOutput().getName()));
		DownloadFile df = new DownloadFile();
		monitor.observe(df);
		downloadFile.setValue(df.download(urlFile.getValue(), dir, DataType
				.values()[type.getIndex()]));

	}

}
