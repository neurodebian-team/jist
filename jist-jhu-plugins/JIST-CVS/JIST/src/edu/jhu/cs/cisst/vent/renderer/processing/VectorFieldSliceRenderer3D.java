/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.renderer.processing;

import java.awt.Color;

import javax.media.j3d.BoundingBox;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import processing.core.PApplet;
import processing.core.PMatrix3D;
import processing.opengl.PGraphicsOpenGL;
import edu.jhu.cs.cisst.jist.parameter.ParamColor;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamDoubleSliderInputView;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamIntegerSliderInputView;
import edu.jhu.cs.cisst.vent.VisualizationProcessing;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VectorFieldSliceRenderer3D.
 */
public class VectorFieldSliceRenderer3D extends VolumeSliceRenderer3D {
	
	/**
	 * Instantiates a new vector field slice renderer3 d.
	 *
	 * @param img the img
	 * @param volToImageTransform the vol to image transform
	 * @param applet the applet
	 */
	public VectorFieldSliceRenderer3D(ImageData img,
			PMatrix3D volToImageTransform, VisualizationProcessing applet) {
		super(img, volToImageTransform, applet);
		// TODO Auto-generated constructor stub
	}

	/** The vector field. */
	protected Vector3f[][][] vectorField = null;

	/** The magnitude image. */
	protected float[][][] magImage = null;

	/** The sample rate. */
	protected int sampleRate = 8;

	/** The arrow head width. */
	protected float arrowHeadWidth = 0.35f;
	
	/** The scale. */
	protected float scale = 1;
	
	/** The x offset. */
	protected float xOffset = 0;
	
	/** The y offset. */
	protected float yOffset = 0;
	
	/** The z offset. */
	protected float zOffset = 0;
	
	/** The visible. */
	protected boolean visible = true;

	/**
	 * Sets the arrow head width.
	 * 
	 * @param arrowHeadWidth
	 *            the new arrow head width
	 */
	public void setArrowHeadWidth(float arrowHeadWidth) {
		this.arrowHeadWidth = arrowHeadWidth;
	}

	/**
	 * Sets the arrow head height.
	 * 
	 * @param arrowHeadHeight
	 *            the new arrow head height
	 */
	public void setArrowHeadHeight(float arrowHeadHeight) {
		this.arrowHeadHeight = arrowHeadHeight;
	}

	/** The arrow head height. */
	protected float arrowHeadHeight = 0.5f;

	/** The arrow width. */
	protected float arrowWidth = 0.2f;

	/** The arrow width param. */
	protected ParamFloat arrowWidthParam;

	/** The visible param. */
	protected ParamBoolean visibleParam;
	/** The arrow color. */
	protected Color arrowColor = new Color(255,0,255);

	/**
	 * Sets the arrow color.
	 * 
	 * @param arrowColor
	 *            the new arrow color
	 */
	public void setArrowColor(Color arrowColor) {
		this.arrowColor = arrowColor;
	}

	/** The arrow color param. */
	protected ParamColor arrowColorParam;

	/** The max magnitude. */
	protected float maxMagnitude;

	/** The sample rate param. */
	protected ParamInteger sampleRateParam;

	/** The transparency param. */
	protected ParamFloat transparencyParam;

	/** The arrow head width param. */
	protected ParamFloat arrowHeadWidthParam;

	/** The arrow head height param. */
	protected ParamFloat arrowHeadHeightParam;

	/**
	 * Clear cache.
	 */
	protected void clearCache() {
		vectorField = null;
	}

	/**
	 * Sets the transparency.
	 * 
	 * @param transparency
	 *            the new transparency
	 */
	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}

	/**
	 * Sets the sample rate.
	 * 
	 * @param sampleRate
	 *            the new sample rate
	 */
	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
	}

	/**
	 * Gets the vector field.
	 * 
	 * @return the vector field
	 */
	public Vector3f[][][] getVectorField() {

		if (vectorField == null) {
			maxMagnitude = 0;
			Vector3f v;
			vectorField = new Vector3f[rows][cols][slices];
			magImage = new float[rows][cols][slices];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						vectorField[i][j][k] = v = new Vector3f(image.getFloat(
								i, j, k, 0), image.getFloat(i, j, k, 1),
								image.getFloat(i, j, k, 2));
						float len = v.length();
						magImage[i][j][k] = len;
						maxMagnitude = Math.max(maxMagnitude, len);
					}
				}
			}
		}
		return vectorField;
	}

	/**
	 * Sets the offset.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void setOffset(double x, double y, double z) {
		this.xOffset = (float) x;
		this.yOffset = (float) y;
		this.zOffset = (float) z;
	}

	/**
	 * Setup.
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderers.RendererProcessing#setup()
	 */
	@Override
	public void setup() {
	}

	/**
	 * Draw arrow.
	 *
	 * @param applet the applet
	 * @param pt the pt
	 * @param v the v
	 * @param alpha the alpha
	 */
	protected void drawArrow(PApplet applet, Point3f pt, Vector3f v,float alpha) {
		applet.pushMatrix();
		applet.translate(pt.x,pt.y,pt.z);
		GLU glu = ((PGraphicsOpenGL) applet.g).glu;
		GL gl = ((PGraphicsOpenGL) applet.g).beginGL();
		GLUquadric quadric = glu.gluNewQuadric();
		gl.glColor4f(arrowColor.getRed()*0.0039215f,arrowColor.getGreen()*0.0039215f,arrowColor.getBlue()*0.0039215f,alpha);
	    glu.gluQuadricNormals(quadric, GLU.GLU_FLAT);
	    glu.gluQuadricOrientation(quadric, GLU.GLU_OUTSIDE);
	    gl.glRotatef((float)(180*Math.atan2(v.y,v.x)/Math.PI), 0, 0, 1);
	    gl.glRotatef((float)(180*Math.acos(v.z)/Math.PI), 0, 1, 0);
	    gl.glTranslatef(0,0,-0.5f);
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
		glu.gluCylinder(quadric, arrowWidth*0.5, arrowWidth*0.5, 1, 5, 5);
		gl.glTranslatef(0,0,1f);
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
		glu.gluCylinder(quadric, arrowHeadWidth*0.5, 0,arrowHeadHeight, 10,10);
		((PGraphicsOpenGL) applet.g).endGL();
		applet.popMatrix();
	}

	/**
	 * Draw.
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderers.RendererProcessing#draw()
	 */
	@Override
	public void draw() {
		applet.pushStyle();
		Vector3f[][][] vectorField = getVectorField();
		int r = vectorField.length;
		int c = vectorField[0].length;
		int s = vectorField[0][0].length;
		int sample = sampleRate;
		applet.lights();
		applet.noStroke();
		applet.pushMatrix();
		applet.applyMatrix(volToImageTransform);
		applet.scale(scale);
		applet.translate(xOffset, yOffset, zOffset);
		Point3f pt = new Point3f();
		if (visible) {

			for (int i = (int) Math.max(-xOffset * sample, 0); i < r
					- Math.max(0, xOffset * sample); i += sample) {
				for (int j = (int) Math.max(-yOffset * sample, 0); j < c
						- Math.max(0, yOffset * sample); j += sample) {
					for (int k = (int) Math.max(-zOffset * sample, 0); k < s
							- Math.max(0, zOffset * sample); k += sample) {
						int is = Math.min(Math.round(i + sample * 0.5f), r - 1);
						int js = Math.min(Math.round(j + sample * 0.5f), c - 1);
						int ks = Math.min(Math.round(k + sample * 0.5f), s - 1);
						Vector3f v = new Vector3f(vectorField[is][js][ks]);
						float len = magImage[is][js][ks];
						v.scale(1.0f / len);
						pt.x = (0.5f * sample + i);
						pt.y = (0.5f * sample + j);
						pt.z = (0.5f * sample + k);
						float alpha=((float)Math.max( 0, Math.min(1, transparency + len /maxMagnitude)));
						drawArrow(applet, pt, v,alpha);
					}
				}
			}
		} else {
			if (showXplane) {
				int i = row;
				for (int j = (int) Math.max(-yOffset * sample, 0); j < c
						- Math.max(0, yOffset * sample); j += sample) {
					for (int k = (int) Math.max(-zOffset * sample, 0); k < s
							- Math.max(0, zOffset * sample); k += sample) {
						int js = Math.min(Math.round(j + sample * 0.5f), c - 1);
						int ks = Math.min(Math.round(k + sample * 0.5f), s - 1);
						Vector3f v = new Vector3f(vectorField[i][js][ks]);
						float len = magImage[i][js][ks];
						v.scale(1.0f / len);
						pt.x = (0.5f + i);
						pt.y = (0.5f * sample + j);
						pt.z = (0.5f * sample + k);
						float alpha=((float)Math.max( 0, Math.min(1, transparency + len /maxMagnitude)));
						drawArrow(applet, pt, v,alpha);

					}
				}
				applet.tint(255, 255, 255, transparency * 255);
				applet.noFill();
				applet.strokeWeight(1.0f);
				applet.stroke(255, 153, 0);

				applet.beginShape(PApplet.QUADS);
				applet.vertex(row + 0.5f, 0, 0);
				applet.vertex(row + 0.5f, r, 0);
				applet.vertex(row + 0.5f, r, c);
				applet.vertex(row + 0.5f, 0, c);
				applet.endShape();
			}
			if (showYplane) {
				int j = col;
				for (int i = (int) Math.max(-xOffset * sample, 0); i < r
						- Math.max(0, xOffset * sample); i += sample) {
					for (int k = (int) Math.max(-zOffset * sample, 0); k < s
							- Math.max(0, zOffset * sample); k += sample) {
						int is = Math.min(Math.round(i + sample * 0.5f), r - 1);
						int ks = Math.min(Math.round(k + sample * 0.5f), s - 1);
						Vector3f v = new Vector3f(vectorField[is][j][ks]);
						float len = magImage[is][j][ks];
						v.scale(1.0f / len);
						pt.x = (0.5f * sample + i);
						pt.y = (0.5f + j);
						pt.z = (0.5f * sample + k);
						float alpha=((float)Math.max( 0, Math.min(1, transparency + len /maxMagnitude)));
						drawArrow(applet, pt, v,alpha);

					}
				}
				applet.tint(255, 255, 255, transparency * 255);
				applet.noFill();
				applet.strokeWeight(1.0f);
				applet.stroke(255, 153, 0);

				applet.beginShape(PApplet.QUADS);
				applet.vertex(0, col + 0.5f, 0);
				applet.vertex(r, col + 0.5f, 0);
				applet.vertex(r, col + 0.5f, s);
				applet.vertex(0, col + 0.5f, s);
				applet.endShape();
			}
			if (showZplane) {
				int k = slice;
				for (int i = (int) Math.max(-xOffset * sample, 0); i < r
						- Math.max(0, xOffset * sample); i += sample) {
					for (int j = (int) Math.max(-yOffset * sample, 0); j < c
							- Math.max(0, yOffset * sample); j += sample) {
						int is = Math.min(Math.round(i + sample * 0.5f), r - 1);
						int js = Math.min(Math.round(j + sample * 0.5f), c - 1);
						Vector3f v = new Vector3f(vectorField[is][js][k]);
						float len = magImage[is][js][k];
						v.scale(1.0f / len);
						pt.x = (0.5f * sample + i);
						pt.y = (0.5f * sample + j);
						pt.z = (0.5f + k);
						float alpha=((float)Math.max( 0, Math.min(1, transparency + len /maxMagnitude)));
						drawArrow(applet, pt, v,alpha);

					}
				}
				applet.tint(255, 255, 255, transparency * 255);
				applet.noFill();
				applet.strokeWeight(1.0f);
				applet.stroke(255, 153, 0);

				applet.beginShape(PApplet.QUADS);
				applet.vertex(0, 0, slice + 0.5f);
				applet.vertex(r, 0, slice + 0.5f);
				applet.vertex(r, c, slice + 0.5f);
				applet.vertex(0, c, slice + 0.5f);
				applet.endShape();

			}
		}

		applet.popMatrix();
		applet.popStyle();

	}

	/**
	 * Draw.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param v
	 *            the v
	 */
	protected void draw(int x, int y, Vector3f v) {

	}

	/**
	 * Sets the contrast.
	 * 
	 * @param contrast
	 *            the new contrast
	 */
	public void setContrast(float contrast) {
		if (contrast != this.contrast)
			clearCache();
		this.contrast = contrast;

	}

	/**
	 * Sets the brightness.
	 * 
	 * @param brightness
	 *            the new brightness
	 */
	public void setBrightness(float brightness) {
		if (brightness != this.brightness)
			clearCache();
		this.brightness = brightness;
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters
	 *            the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationParameters#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		visualizationParameters.setName("Vector Field - " + image.getName());
		visualizationParameters.add(rowParam = new ParamInteger("Row", 1, rows,
				1));
		rowParam.setInputView(new ParamIntegerSliderInputView(rowParam, 4));

		visualizationParameters.add(colParam = new ParamInteger("Column", 1,
				cols, 1));
		colParam.setInputView(new ParamIntegerSliderInputView(colParam, 4));

		visualizationParameters.add(sliceParam = new ParamInteger("Slice", 1,
				slices, 1));
		sliceParam.setInputView(new ParamIntegerSliderInputView(sliceParam, 4));

		visualizationParameters.add(sampleRateParam = new ParamInteger(
				"Sample Rate", 1, 50, sampleRate));
		sampleRateParam.setInputView(new ParamIntegerSliderInputView(
				sampleRateParam, 4));
		visualizationParameters.add(transparencyParam = new ParamFloat(
				"Transparency", -1, 1, transparency));
		transparencyParam.setInputView(new ParamDoubleSliderInputView(
				transparencyParam, 4, false));
		visualizationParameters.add(arrowColorParam = new ParamColor(
				"Arrow Color", arrowColor));
		visualizationParameters.add(arrowWidthParam = new ParamFloat(
				"Arrow Width", 0, 1, arrowWidth));
		arrowWidthParam.setInputView(new ParamDoubleSliderInputView(
				arrowWidthParam, 4, false));
		visualizationParameters.add(arrowHeadWidthParam = new ParamFloat(
				"Arrow Head Width", 0, 1, arrowHeadWidth));
		arrowHeadWidthParam.setInputView(new ParamDoubleSliderInputView(
				arrowHeadWidthParam, 4, false));

		visualizationParameters.add(arrowHeadHeightParam = new ParamFloat(
				"Arrow Head Height", 0, 1, arrowHeadHeight));
		arrowHeadHeightParam.setInputView(new ParamDoubleSliderInputView(
				arrowHeadHeightParam, 4, false));
		visualizationParameters.add(visibleParam = new ParamBoolean("Volumetric Field",
				visible));
		visualizationParameters.add(showXplaneParam = new ParamBoolean(
				"Show X Plane", showXplane));
		visualizationParameters.add(showYplaneParam = new ParamBoolean(
				"Show Y Plane", showYplane));
		visualizationParameters.add(showZplaneParam = new ParamBoolean(
				"Show Z Plane", showZplane));

	}

	/**
	 * Update.
	 * 
	 * @param model
	 *            the model
	 * @param view
	 *            the view
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
	 *      edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	public void update(ParamModel model, ParamInputView view) {
		if (model == rowParam) {
			setRow(rowParam.getInt() - 1);
		} else if (model == colParam) {
			setCol(colParam.getInt() - 1);
		} else if (model == sliceParam) {
			setSlice(sliceParam.getInt() - 1);
		} else if (model == sampleRateParam) {
			setSampleRate(sampleRateParam.getInt());
		} else if (model == transparencyParam) {
			setTransparency(transparencyParam.getFloat());
		} else if (model == visibleParam) {
			setVisible(visibleParam.getValue());
		} else if (model == arrowHeadWidthParam) {
			setArrowHeadWidth(arrowHeadWidthParam.getFloat());
		} else if (model == arrowHeadHeightParam) {
			setArrowHeadHeight(arrowHeadHeightParam.getFloat());
		} else if (model == arrowColorParam) {
			setArrowColor(arrowColorParam.getValue());
		} else if (model == arrowWidthParam) {
			setArrowWidth(arrowWidthParam.getFloat());
		} else if (model == showXplaneParam) {
			setShowXplane(showXplaneParam.getValue());
		} else if (model == showYplaneParam) {
			setShowYplane(showYplaneParam.getValue());
		} else if (model == showZplaneParam) {
			setShowZplane(showZplaneParam.getValue());
		}
	}

	/**
	 * Sets the visible.
	 *
	 * @param visible the new visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Checks if is visible.
	 *
	 * @return true, if is visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Sets the arrow width.
	 * 
	 * @param arrowWidth
	 *            the new arrow width
	 */
	private void setArrowWidth(float arrowWidth) {
		this.arrowWidth = arrowWidth;
	}

	/**
	 * Sets the vector field.
	 *
	 * @param image the new vector field
	 */
	public void setVectorField(ImageDataFloat image) {
		this.image = image;
		this.rows = image.getRows();
		this.cols = image.getCols();
		this.slices = image.getSlices();
		vectorField = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.cs.cisst.vent.VisualizationParameters#updateVisualizationParameters
	 * ()
	 */
	@Override
	public void updateVisualizationParameters() {
		setSampleRate(sampleRateParam.getInt());
		setTransparency(transparencyParam.getFloat());
		setVisible(visibleParam.getValue());
		setArrowHeadWidth(arrowHeadWidthParam.getFloat());
		setArrowHeadHeight(arrowHeadHeightParam.getFloat());
		setArrowColor(arrowColorParam.getValue());
		setArrowWidth(arrowWidthParam.getFloat());
		setRow(rowParam.getInt() - 1);
		setCol(colParam.getInt() - 1);
		setSlice(sliceParam.getInt() - 1);
		setShowXplane(showXplaneParam.getValue());
		setShowYplane(showYplaneParam.getValue());
		setShowZplane(showZplaneParam.getValue());
	}

	/**
	 * Gets the scale.
	 *
	 * @return the scale
	 */
	public float getScale() {
		return scale;
	}

	/**
	 * Sets the scale.
	 *
	 * @param scale the new scale
	 */
	public void setScale(float scale) {
		this.scale = scale;
	}
}
