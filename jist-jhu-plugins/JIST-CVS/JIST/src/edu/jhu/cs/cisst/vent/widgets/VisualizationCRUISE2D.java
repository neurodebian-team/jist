/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.widgets;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.cs.cisst.vent.*;
import edu.jhu.cs.cisst.vent.renderer.processing.ImageRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VectorFieldRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VectorFieldSliceRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeIsoContourRenderer;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeSliceRenderer2D;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationActiveContour2D.
 */
public class VisualizationCRUISE2D extends VisualizationImage2D {
	
	/** The rule. */
	protected TopologyRule2D.Rule rule;

	/** The visualization. */
	protected VisualizationProcessing2D visualization;
	
	/** The slice renderers. */
	protected LinkedList<VolumeSliceRenderer2D> sliceRenderers;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.6 $");
	}

	/** The vector field image. */
	ImageDataFloat innerLevelSet = null, centralLevelSet = null,
			outerLevelSet = null, wmImage = null, gmImage = null,
			vectorFieldImage = null;

	/** The original image. */
	ImageData originalImage = null;

	/**
	 * Instantiates a new visualization active contour.
	 *
	 * @param innerLevelSet the inner level set
	 * @param centralLevelSet the central level set
	 * @param outerLevelSet the outer level set
	 * @param rule the rule
	 */
	public VisualizationCRUISE2D(ImageDataFloat innerLevelSet,
			ImageDataFloat centralLevelSet, ImageDataFloat outerLevelSet,
			TopologyRule2D.Rule rule) {
		super(4*innerLevelSet.getRows(),4*innerLevelSet.getCols(),innerLevelSet.getRows(),innerLevelSet.getCols());
		this.innerLevelSet = innerLevelSet;
		this.centralLevelSet = centralLevelSet;
		this.outerLevelSet = outerLevelSet;
		this.rule = rule;
		setName("Visualization - CRUISE Result 2D");
	}

	/**
	 * Instantiates a new visualization active contour.
	 *
	 * @param width the width
	 * @param height the height
	 * @param levelset the levelset
	 */
	public VisualizationCRUISE2D(int width, int height, ImageDataFloat levelset) {
		super(width, height, levelset.getRows(), levelset.getCols());
		this.innerLevelSet = levelset;
		setName("Visualization - Active Contour 2D");
	}

	/**
	 * Creates the.
	 * 
	 * @return the param collection
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#create()
	 */
	@Override
	public ParamCollection create() {
		renderers = new ArrayList<RendererProcessing2D>();
		sliceRenderers = new LinkedList<VolumeSliceRenderer2D>();
		VolumeSliceRenderer2D tmp;
		if (originalImage != null) {
			renderers.add(tmp = new VolumeSliceRenderer2D(originalImage, this));
			sliceRenderers.add(tmp);
		}
		if (wmImage != null) {
			renderers.add(tmp = new VolumeSliceRenderer2D(wmImage, this));
			tmp.setVisible(false);
			sliceRenderers.add(tmp);
		}
		if (gmImage != null) {
			renderers.add(tmp = new VolumeSliceRenderer2D(gmImage, this));
			tmp.setVisible(false);
			sliceRenderers.add(tmp);
		}
		if (vectorFieldImage != null) {
			renderers.add(tmp = new VectorFieldSliceRenderer2D(
					vectorFieldImage, this));
			tmp.setVisible(true);
			((VectorFieldSliceRenderer2D)tmp).setSampleRate(3);
			((VectorFieldSliceRenderer2D)tmp).setArrowColor(Color.orange.darker());
			sliceRenderers.add(tmp);
		}
		if (innerLevelSet != null) {
			VolumeIsoContourRenderer renderer = new VolumeIsoContourRenderer(
					innerLevelSet, rule, this);
			renderers.add(renderer);
			renderer.setContourColor(Color.red.darker());
			renderer.setStrokeWeight(3);
			renderer.setVisible(false);
			sliceRenderers.add(renderer);
		}
		if (centralLevelSet != null) {
			VolumeIsoContourRenderer renderer = new VolumeIsoContourRenderer(
					centralLevelSet, rule, this);
			renderers.add(renderer);

			renderer.setContourColor(Color.green.darker());
			renderer.setStrokeWeight(3);
			sliceRenderers.add(renderer);
			renderer.setVisible(false);
		}
		if (outerLevelSet != null) {
			VolumeIsoContourRenderer renderer = new VolumeIsoContourRenderer(
					outerLevelSet, rule, this);
			renderers.add(renderer);

			renderer.setContourColor(Color.blue.darker());
			renderer.setStrokeWeight(3);
			sliceRenderers.add(renderer);
			renderer.setVisible(false);
		}
		ParamCollection params = super.create();
		for (VolumeSliceRenderer2D sliceRenderer : sliceRenderers) {
			sliceRenderer.getSliceParameter().getInputView()
					.addObserver(new SliceViewObserver(sliceRenderer));
		}
		return params;
	}

	/**
	 * An asynchronous update interface for receiving notifications
	 * about SliceView information as the SliceView is constructed.
	 */
	protected class SliceViewObserver implements ParamViewObserver {
		
		/** The renderer. */
		protected VolumeSliceRenderer2D renderer;

		/**
		 * This method is called when information about an SliceView
		 * which was previously requested using an asynchronous
		 * interface becomes available.
		 *
		 * @param renderer the renderer
		 */
		public SliceViewObserver(VolumeSliceRenderer2D renderer) {
			this.renderer = renderer;
		}

		/* (non-Javadoc)
		 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel, edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
		 */
		public void update(ParamModel model, ParamInputView view) {
			int slice = renderer.getSliceParameter().getInt();
			for (VolumeSliceRenderer2D sliceRenderer2 : sliceRenderers) {
				if (sliceRenderer2!=renderer&&sliceRenderer2.getSliceParameter().getInt() != slice) {
					sliceRenderer2.getSliceParameter().setValue(slice);
					sliceRenderer2.getSliceParameter().getInputView().update();
				}
			}
		}
	}

	/**
	 * Sets the pressure force.
	 *
	 * @param gmImage the new gM
	 */
	public void setGM(ImageDataFloat gmImage) {
		this.gmImage = gmImage;
	}

	/**
	 * Sets the pressure force.
	 *
	 * @param wmImage the new wM
	 */
	public void setWM(ImageDataFloat wmImage) {
		this.wmImage = wmImage;
	}

	/**
	 * Sets the vector field force.
	 * 
	 * @param vectorFieldImage
	 *            the new vector field force
	 */
	public void setVectorFieldForce(ImageDataFloat vectorFieldImage) {
		this.vectorFieldImage = vectorFieldImage;
	}

	/**
	 * Sets the original image.
	 * 
	 * @param img
	 *            the new original image
	 */
	public void setOriginalImage(ImageData img) {
		this.originalImage = img;
	}

	/**
	 * Sets the visualization.
	 * 
	 * @param vis
	 *            the new visualization
	 */
	public void setVisualization(VisualizationProcessing vis) {
		this.visualization = (VisualizationProcessing2D) vis;
	}
}
