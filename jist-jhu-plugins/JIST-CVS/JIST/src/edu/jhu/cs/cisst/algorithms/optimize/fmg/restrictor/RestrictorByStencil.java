/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.Stencil;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.StencilImpl;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.Grid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.Parallelizer;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.Contract;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.IntRange1D;
import edu.jhu.cs.cisst.algorithms.util.DataOperations;

// TODO: Auto-generated Javadoc
/**
 * The Class RestrictorByStencil.
 * 
 */
public class RestrictorByStencil implements Restrictor {
	
	/** The stencil. */
	protected  Stencil s;
	
	/** The coarse size. */
	private int[] coarseSize;
	
	/** The sz. */
	private int sx;
	
	/** The sy. */
	private int sy;
	
	/** The sz. */
	private int sz;
	
	/** The total range. */
	protected IntRange1D totalRange;
	
	/** the stencil that characterises straight injection where elements of those elements of the fine grid that are also present in the coarse grid are simply copied from the fine grid to the coarse grid. This method is not always stable. */
	public static final Stencil STRAIGHT_INJECTION = new StencilImpl(0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0);

	/** the stencil that characterises full weighting. The stencil for full weighting is the the transpose of the stencil for trilinear interpolation, which is of theoretical importance. */
	public static final Stencil FULL_WEIGHTING = new StencilImpl(1. / 64.,
			1. / 32., 1. / 64., 1. / 32., 1. / 16., 1. / 32., 1. / 64.,
			1. / 32., 1. / 64., 1. / 32., 1. / 16., 1. / 32., 1. / 16.,
			1. / 8., 1. / 16., 1. / 32., 1. / 16., 1. / 32., 1. / 64.,
			1. / 32., 1. / 64., 1. / 32., 1. / 16., 1. / 32., 1. / 64.,
			1. / 32., 1. / 64.);
	
	/** The Constant FULL_WEIGHTING_2D. */
	public static final Stencil FULL_WEIGHTING_2D = new StencilImpl(
			0, 1. / 16., 0,
			0, 1. / 8.,  0,
			0, 1. / 16., 0,
			0, 1. / 8.,  0,
			0, 1. / 4.,  0,
			0, 1. / 8.,  0,
			0, 1. / 16., 0,
			0, 1. / 8.,  0,
			0, 1. / 16., 0);

	/** the stencil that characterises half weighting which is halfway between full weighting and straigh injection. */
	public static final Stencil HALF_WEIGHTING = new StencilImpl(
			0, 1. / 32., 0, 
			1. / 32., 1. / 16., 1. / 32., 
			0, 1. / 32., 0, 
			1. / 32.,1. / 16., 1. / 32., 1. / 16., 
			1. / 4., 1. / 16., 1. / 32.,
			1. / 16., 1. / 32., 0,
			1. / 32., 0, 1. / 32., 
			1. / 16.,1. / 32.,0, 1. / 32., 0);

	/**
	 * construct from restriction stencil.
	 * 
	 * @param stencil the stencil that defines restriction
	 */
	public RestrictorByStencil(Stencil stencil) {
		Contract.pre(stencil != null, "stencil not null-object");

		s = stencil;
	}

	/**
	 * implements method from Restrictor.
	 * 
	 * @param grid the grid
	 * @param levels the levels
	 * 
	 * @return the grid
	 * 
	 * @see Restrictor#restrict
	 */
	public Grid restrict(ConstGrid grid, SolverResolutionLevels levels) {
		Contract.pre(grid != null, "grid not null-object");
		this.sx = grid.getRows();
		this.sy = grid.getCols();
		this.sz = grid.getSlices();
		int fineLevel = grid.getLevel();

		coarseSize = levels.getResolution(fineLevel - 1);
		totalRange = new IntRange1D(1, coarseSize[0] - 2);
		Grid coarse = grid.newInstance(coarseSize[0], coarseSize[1],
				coarseSize[2], fineLevel - 1, 0); // create coarse grid
		new RestrictByStencilParallelizer(this, coarse, grid);

		return coarse;
	}




	/**
	 * Restrict proper.
	 * 
	 * @param coarse the coarse
	 * @param fine the fine
	 * @param myNum the my num
	 * @param totalNum the total num
	 */
	public void restrictProper(Grid coarse, ConstGrid fine, int myNum, int totalNum) {

		IntRange1D range = Parallelizer.partition(totalRange, myNum, totalNum);
		int fx = fine.getRows();
		int fy = fine.getCols();
		int fz = fine.getSlices();
		int cx = coarse.getRows();
		int cy = coarse.getCols();
		int cz = coarse.getSlices();
		double scale = Math.max(fz / (double) cz, Math.max(fx / (double) cx, fy
				/ (double) cy));
		double iFine, jFine, kFine;
		for (int x = range.from(); x <= range.to(); x++) {
			iFine = scale * x;
			for (int y = 0; y < cy; y++) {
				jFine = scale * y;
				for (int z = 0; z < cz; z++) {
					kFine = scale * z;
					// calc coarse grid element from stencil and fine grid
					double v = 0;
					for (int l = -1; l <= +1; l++) {
						double lFine = iFine + l;
						for (int m = -1; m <= +1; m++) {
							double mFine = jFine + m;
							for (int n = -1; n <= +1; n++) {
								double nFine = kFine + n;
								v += DataOperations.interpolate(lFine, mFine, nFine, fine, fx,
										fy, fz)
										* s.get(l, m, n);
							}
						}
					}
					coarse.set(x, y, z, v);
				}
			}
		}
	}


}
