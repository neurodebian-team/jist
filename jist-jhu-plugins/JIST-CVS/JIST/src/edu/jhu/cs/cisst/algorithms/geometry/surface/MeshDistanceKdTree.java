/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.geometry.surface;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import javax.vecmath.Point3f;

import edu.jhu.cs.cisst.algorithms.geometry.surface.MeshBBox.Axis;
// TODO: Auto-generated Javadoc
/**
 * The Class MeshDistanceKdTree builds a KD-Tree for efficient ray-surface and
 * point-surface-distance tests.
 * 
 * This algorithm is an implementation of the method in: Pharr, M. and G.
 * Humphreys (2004). Physically Based Rendering: From Theory to Implementation,
 * Morgan Kaufmann.
 * 
 * @author Kel and Blake
 */
public class MeshDistanceKdTree extends AbstractMeshDistance {





	/**
	 * The Class BoundingBoxEdge is used for sorting bounding boxes along an
	 * axis.
	 */
	protected class BoundingBoxEdge implements Comparable<BoundingBoxEdge> {

		/** The value. */
		protected float value;

		/** The index. */
		protected int index;

		/** The lower. */
		protected boolean lower;

		/**
		 * Instantiates a new bounding box edge.
		 * 
		 * @param bbox the bounding box
		 * @param index the index
		 * @param axis the axis
		 * @param lower the flag to indicate this is the lower bound
		 */
		public BoundingBoxEdge(MeshBBox bbox, int index, Axis axis,
				boolean lower) {
			Point3f p = (lower) ? bbox.getMinPoint() : bbox.getMaxPoint();
			this.index = index;
			this.lower = lower;
			switch (axis) {
			case X:
				value = p.x;
				break;
			case Y:
				value = p.y;
				break;
			case Z:
				value = p.z;
				break;
			}
		}

		/**
		 * Compare to another bounding box edge.
		 * 
		 * @param edge the edge
		 * 
		 * @return the comparison indicator
		 * 
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(BoundingBoxEdge edge) {
			if (value == edge.value) {
				return ((lower) ? 0 : 1) - ((edge.lower) ? 0 : 1);
			} else {
				return (int) Math.signum(value - edge.value);
			}
		}
	}

	/** The max depth. */
	protected int maxDepth = 0;

	/** The root bounding box. */
	protected MeshBBox rootBBox;

	/** The empty bonus probability. */
	protected float emptyBonus = 0.5f;

	/** The traversal cost. */
	protected float traversalCost = 1;

	/** The intersection cost. */
	protected float intersectionCost = 80;

	/** The closest triangle. */
	protected MeshTriangle closestTriangle = null;

	/** The intersect count. */
	protected int intersectCount = 0;

	/** The closest point. */
	protected Point3f closestPoint = null;;

	/** The all bounding boxes list. */
	protected LinkedList<MeshBBox> allBBoxes;

	/**
	 * Instantiates a new mesh distance KD-tree.
	 * 
	 * @param mesh the mesh
	 */
	public MeshDistanceKdTree(TriangleMesh mesh) {
		super(mesh);
		init();
	}
	
	/**
	 * Builds the KD-tree.
	 * 
	 * @param parentNode the parent node
	 * @param badRefines the bad refines
	 */
	protected void buildTree(MeshBBox parentNode,int badRefines) {
		int depth=parentNode.getDepth();
		// Depth is the same as max depth
		if (depth == maxDepth){
			return;
		}
		List<MeshBBox> bboxes = parentNode.getChildren();
		int N = bboxes.size();

		float[] minPoint = new float[3];
		float[] maxPoint = new float[3];
		float[] dPoint = new float[3];

		float bestCost = MeshBBox.MAX_VALUE;
		float oldCost = intersectionCost * N;

		BoundingBoxEdge[][] edges = new BoundingBoxEdge[3][2 * bboxes.size()];
		int nBelow = 0, nAbove = N;
		Axis axis = null;
		int bestAxis = -1;
		int bestOffset = -1;
		// Choose the longest axis of the bounding box as the split axis
		Point3f d = new Point3f();
		d.sub(parentNode.maxPoint, parentNode.minPoint);
		if (d.x > d.y && d.x > d.z)
			axis = Axis.X;
		else
			axis = ((d.y > d.z) ? Axis.Y : Axis.Z);
		// Calculate total bounding box surface area and inverse surface area
		float totalSA = 2.0f * (d.x * d.y + d.x * d.z + d.y * d.z);
		float invTotalSA = 1.0f / totalSA;

		// extract min, max point, and distance into array
		parentNode.minPoint.get(minPoint);
		parentNode.maxPoint.get(maxPoint);
		d.get(dPoint);
		int[][] otherAxis = new int[][] { { 1, 2 }, { 0, 2 }, { 0, 1 } };
		int axisNum = axis.ordinal();
		int retries = 0;
		// Repeat for each axis if necessary
		while (bestAxis == -1 && retries < 3) {
			int index = 0;
			// Initialize bounding box edges
			for (MeshBBox bbox : bboxes) {
				edges[axisNum][2 * index] = new BoundingBoxEdge(bbox, index,
						Axis.values()[axisNum], true);
				edges[axisNum][2 * index + 1] = new BoundingBoxEdge(bbox,
						index, Axis.values()[axisNum], false);
				index++;
			}
			// Sort edges
			Arrays.sort(edges[axisNum]);
			for (int i = 0; i < 2 * N; i++) {
				// Update number of elements above split
				if (!edges[axisNum][i].lower)
					nAbove--;
				// Distance along axis
				float edget = edges[axisNum][i].value;
				// Check if distance along axis is in bounding box
				if (edget > minPoint[axisNum] && edget < maxPoint[axisNum]) {
					int otherAxis0 = otherAxis[axisNum][0];
					int otherAxis1 = otherAxis[axisNum][1];
					// Estimate probability of being below and above split
					float belowSA = 2 * (dPoint[otherAxis0]
							* dPoint[otherAxis1] + (edget - minPoint[axisNum])
							* (dPoint[otherAxis0] + dPoint[otherAxis1]));
					float aboveSA = 2 * (dPoint[otherAxis0]
							* dPoint[otherAxis1] + (maxPoint[axisNum] - edget)
							* (dPoint[otherAxis0] + dPoint[otherAxis1]));
					float pBelow = belowSA * invTotalSA;
					float pAbove = aboveSA * invTotalSA;
					// Add bonus cost for an empty box
					float eb = (nAbove == 0 || nBelow == 0) ? emptyBonus : 0.0f;
					// Estimate traversal cost
					float cost = traversalCost + intersectionCost * (1.0f - eb)
							* (pBelow * nBelow + pAbove * nAbove);
					// Estimate best cost
					if (cost < bestCost) {
						bestCost = cost;
						bestAxis = axisNum;
						bestOffset = i;
					}
				}
				// Update number of elements below split
				if (edges[axisNum][i].lower)
					nBelow++;
			}
			// Choose a different axis to split along
			axisNum = (axisNum + 1) % 3;
			retries++;
		}
		// best cost is greater than old cost
		if (bestCost > oldCost) {
			++badRefines;
		}
		if ((bestCost > 4 * oldCost && N < 16) || bestAxis == -1
				|| badRefines >= 3) {
			// Could not determine good split position, so don't split node
			return;
		} else {
			LinkedList<MeshBBox> leftBranch = new LinkedList<MeshBBox>();
			LinkedList<MeshBBox> rightBranch = new LinkedList<MeshBBox>();
			int i = 0;
			// Create list of bounding boxes to left and right of split
			for (i = 0; i < bestOffset; i++) {
				BoundingBoxEdge edge = edges[bestAxis][i];
				if (edge.lower) {
					leftBranch.add(bboxes.get(edge.index));
				}
			}
			for (i = bestOffset + 1; i < 2 * N; i++) {
				BoundingBoxEdge edge = edges[bestAxis][i];
				if (!edge.lower) {
					rightBranch.add(bboxes.get(edge.index));
				}
			}

			// Left and right branches both contain nodes, use computed split
			// location.
			if (leftBranch.size() > 1 && rightBranch.size() > 1) {
				parentNode.getChildren().clear();
				MeshBBox leftNode = new MeshBBox(leftBranch);
				leftNode.setDepth(depth+1);
				MeshBBox rightNode = new MeshBBox(rightBranch);
				rightNode.setDepth(depth+1);
				allBBoxes.add(leftNode);
				allBBoxes.add(rightNode);
				parentNode.setSplitAxis(Axis.values()[bestAxis]);
				parentNode.getChildren().add(leftNode);
				parentNode.getChildren().add(rightNode);
				buildTree(leftNode, badRefines);
				buildTree(rightNode, badRefines);
			}
		}

	}

	/**
	 * Get the distance from a point to the surface using a KD-tree accelerator.
	 * 
	 * @param p the point
	 * 
	 * @return the distance
	 * 
	 * @see cis.registration.point.AbstractMeshDistance#distance(javax.vecmath.Point3f)
	 */
	public double distance(Point3f p) {
		double minDistSquared = MeshBBox.MAX_VALUE;
		MeshTriangle tri = null;
		closestTriangle = null;
		closestPoint = null;
		double d;
		intersectCount = 0;

		int N = mesh.getTriangles().length;
		// Build priority queue sorted by distance to bounding box
		PriorityQueue<MeshBBox> bboxQueue = new PriorityQueue<MeshBBox>(
				N / 8, new MeshBBoxDistanceComparator(p));
		bboxQueue.add(rootBBox);
		while (bboxQueue.size() > 0) {
			// Remove closest bounding box
			MeshBBox bbox = bboxQueue.remove();
			d = bbox.distanceSquared(p);
			if (d < minDistSquared) {
				// Distance to bounding box is smaller than any seen so far
				if ((tri = bbox.getTriangle()) != null) {
					intersectCount++;
					// Calculate distance to triangle if this is a leaf node
					d = tri.distanceSquared(p);
					if (d < minDistSquared) {
						// Update minimum distance
						minDistSquared = d;
						closestTriangle = tri;
						closestPoint = tri.getLastClosestPoint();
					}
				}
				// Add children bounding boxes
				bboxQueue.addAll(bbox.getChildren());
			}
		}
		return Math.sqrt(minDistSquared);
	}

	/**
	 * Gets the all bounding boxes.
	 * 
	 * @return the all bounding boxes
	 */
	public LinkedList<MeshBBox> getAllBBoxes() {
		return allBBoxes;
	}

	/**
	 * Gets the last closest point.
	 * 
	 * @return the last closest point
	 */
	public Point3f getLastClosestPoint() {
		return closestPoint;
	}

	/**
	 * Gets the last closest triangle.
	 * 
	 * @return the last closest triangle
	 */
	public MeshTriangle getLastClosestTriangle() {
		return closestTriangle;
	}

	/**
	 * Gets the last intersection count.
	 * 
	 * @return the last intersection count
	 */
	public int getLastIntersectionCount() {
		return intersectCount;
	}

	/**
	 * Gets the max depth.
	 * 
	 * @return the max depth
	 */
	public int getMaxDepth() {
		return maxDepth;
	}

	/**
	 * Gets the root bounding box.
	 * 
	 * @return the root bounding box
	 */
	public MeshBBox getRootBBox() {
		return rootBBox;
	}

	/**
	 * Initializes the KD-tree.
	 */
	protected void init() {
		// Refresh triangle list
		mesh.updateMeshTriangles();
		int N = mesh.getTriangles().length;
		// Estimate best max depth
		maxDepth = (int) Math.round(8 + 1.3 * Math.log(N) / Math.log(8.0));
		// Create list of bounding boxes
		LinkedList<MeshBBox> bboxes = new LinkedList<MeshBBox>();
		for (MeshTriangle tri : mesh.getTriangles()) {
			MeshBBox bbox = new MeshBBox(tri);
			bboxes.add(bbox);
		}
		allBBoxes=new LinkedList<MeshBBox>();
		// Create root bounding box
		rootBBox = new MeshBBox(bboxes);
		allBBoxes.add(rootBBox);
		// Build tree
		buildTree(rootBBox, 0);
		

	}

}
