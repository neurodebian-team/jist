/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.renderer.processing;

import java.awt.Color;
import java.io.File;
import java.net.URISyntaxException;

import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamDoubleSliderInputView;
import edu.jhu.cs.cisst.jist.pipeline.view.input.ParamIntegerSliderInputView;
import edu.jhu.cs.cisst.vent.VisualizationProcessing;
import edu.jhu.cs.cisst.vent.widgets.*;
import edu.jhu.cs.cisst.vent.converter.processing.ConvertImageDataToPImage;
import edu.jhu.cs.cisst.vent.resources.PlaceHolder;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import processing.core.*;

// TODO: Auto-generated Javadoc
/**
 * The Class VolumeSliceRenderer2D renders volume by slice in 2D.
 */
public class VolumeSliceRenderer2D extends RendererProcessing2D {

	/** The image. */
	protected ImageData image;

	/** The images. */
	protected PImage[] images = null;

	/** The contrast. */
	protected float contrast = 1;

	/** The brightness. */
	protected float brightness = 0;

	/** The transparency. */
	protected float transparency = 1;

	/** The show slice number. */
	protected boolean showSliceNumber = true;

	/** The show slice number param. */
	protected ParamBoolean showSliceNumberParam;

	/**
	 * Sets the transparency.
	 * 
	 * @param transparency the new transparency
	 */
	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}

	/** The max. */
	protected double min, max;

	/**
	 * Sets the visible.
	 * 
	 * @param visible the new visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/** The visible. */
	protected boolean visible = true;

	/** The slice. */
	protected int slice = 0;

	/** The component. */
	protected int component = 0;

	/**
	 * Gets the rows.
	 *
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Gets the cols.
	 *
	 * @return the cols
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Gets the slices.
	 *
	 * @return the slices
	 */
	public int getSlices() {
		return slices;
	}

	/** The slices. */
	public int rows, cols, slices, components;

	/** The applet. */
	protected VisualizationProcessing applet;

	/** The slice param. */
	protected ParamInteger sliceParam;

	/** The slice param. */
	protected ParamInteger componentParam;

	/** The contrast param. */
	protected ParamFloat contrastParam;

	/** The brightness param. */
	protected ParamFloat brightnessParam;

	/** The visible param. */
	protected ParamBoolean visibleParam;

	/** The transparency param. */
	protected ParamFloat transparencyParam;

	/**
	 * Gets the slice parameter.
	 * 
	 * @return the slice parameter
	 */
	public ParamInteger getSliceParameter() {
		return sliceParam;
	}

	/**
	 * Gets the value string.
	 * 
	 * @param x the x
	 * @param y the y
	 * 
	 * @return the value string
	 */
	public String getValueString(int x, int y) {
		if (x < 0 || x >= rows || y < 0 || y >= cols)
			return "";
		VoxelType type = image.getType();
		switch (type) {
		case COLOR:
		case COLOR_FLOAT:
		case COLOR_USHORT:
			Color c = image.getColor(x, y, slice, component);
			return String.format("(%d,%d,%d,%d)", c.getRed(), c.getGreen(), c
					.getBlue(), c.getAlpha());
		default:
			return String.format("%4.3f", image
					.getFloat(x, y, slice, component));
		}
	}

	/**
	 * Instantiates a new volume slice renderer.
	 * 
	 * @param img the img
	 * @param applet the applet
	 */
	public VolumeSliceRenderer2D(ImageData img, VisualizationProcessing applet) {
		this.image = img;
		rows = image.getRows();
		cols = image.getCols();
		slices = image.getSlices();
		components = image.getComponents();
		min = 1E10f;
		max = -1E10f;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						double val = img.getDouble(i, j, k, l);
						min = Math.min(min, val);
						max = Math.max(max, val);
					}
				}
			}
		}
		images = new PImage[slices];
		this.applet = applet;
	}

	/**
	 * Sets the component.
	 * 
	 * @param component the new component
	 */
	public void setComponent(int component) {
		this.component = component;
		clearCache();
	}

	/**
	 * Clear cache.
	 */
	protected void clearCache() {
		for (int i = 0; i < images.length; i++) {
			images[i] = null;
		}
	}

	/**
	 * Gets the image.
	 * 
	 * @param index the index
	 * 
	 * @return the image
	 */
	public PImage getImage(int index) {
		if (images[index] == null) {
			ConvertImageDataToPImage converter = new ConvertImageDataToPImage();
			images[index] = converter.convert(image, index, component,
					contrast, brightness, min, max);
		}
		return images[index];
	}

	/**
	 * Setup.
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing#setup()
	 */
	@Override
	public void setup() {

		try {
			String fontFile = (new File(PlaceHolder.class.getResource(
					"./TheSans-Plain-12.vlw").toURI()).getAbsolutePath());
			System.out.println(fontFile);
			applet.textFont(applet.loadFont(fontFile));
		} catch (URISyntaxException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Draw.
	 * 
	 * @see edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing#draw()
	 */
	@Override
	public void draw() {
		applet.pushStyle();
		if (visible) {
			PImage img = getImage(slice);

			applet.tint(255, 255, 255, transparency * 255);
			applet.image(img, 0, 0, rows,cols);
			if (slices>1&&showSliceNumber) {
				if(visualization instanceof SliceNumberDisplay){
					((SliceNumberDisplay)visualization).draw(applet, slice, slices+1,cols);
				}
			}
		}
		applet.popStyle();

	}

	/**
	 * Sets the images.
	 * 
	 * @param images the new images
	 */
	public void setImages(PImage[] images) {
		this.images = images;
	}

	/**
	 * Sets the contrast.
	 * 
	 * @param contrast the new contrast
	 */
	public void setContrast(float contrast) {
		if (contrast != this.contrast)
			clearCache();
		this.contrast = contrast;

	}

	/**
	 * Sets the brightness.
	 * 
	 * @param brightness the new brightness
	 */
	public void setBrightness(float brightness) {
		if (brightness != this.brightness)
			clearCache();
		this.brightness = brightness;
	}

	/**
	 * Sets the slice.
	 * 
	 * @param slice the new slice
	 */
	public void setSlice(int slice) {
		this.slice = slice;
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationParameters#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		visualizationParameters.setName(image.getName());
		visualizationParameters.add(sliceParam = new ParamInteger("Slice", 1,
				slices, 1));
		sliceParam.setInputView(new ParamIntegerSliderInputView(sliceParam, 4));
		visualizationParameters.add(componentParam = new ParamInteger(
				"Component", 1, Math.max(1, components), component));
		componentParam.setInputView(new ParamIntegerSliderInputView(
				componentParam, 4));

		visualizationParameters.add(contrastParam = new ParamFloat("Contrast",
				-5, 5, contrast));
		contrastParam.setInputView(new ParamDoubleSliderInputView(
				contrastParam, 4, false));
		visualizationParameters.add(brightnessParam = new ParamFloat(
				"Brightness", -5, 5, brightness));
		brightnessParam.setInputView(new ParamDoubleSliderInputView(
				brightnessParam, 4, false));
		visualizationParameters.add(transparencyParam = new ParamFloat(
				"Transparency", 0, 1, 1));
		transparencyParam.setInputView(new ParamDoubleSliderInputView(
				transparencyParam, 4, false));
		visualizationParameters.add(showSliceNumberParam = new ParamBoolean(
				"Show Slice Number", showSliceNumber));
		visualizationParameters.add(visibleParam = new ParamBoolean("Visible",
				visible));
	}

	/**
	 * Update.
	 * 
	 * @param model the model
	 * @param view the view
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.view.input.ParamViewObserver#update(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel,
	 * edu.jhu.ece.iacl.jist.pipeline.view.input.ParamInputView)
	 */
	@Override
	public void update(ParamModel model, ParamInputView view) {
		if (model == sliceParam) {
			setSlice(sliceParam.getInt() - 1);
		} else if (model == componentParam) {
			setComponent(componentParam.getInt() - 1);
		} else if (model == contrastParam) {
			setContrast(contrastParam.getFloat());
		} else if (model == brightnessParam) {
			setBrightness(brightnessParam.getFloat());
		} else if (model == transparencyParam) {
			setTransparency(transparencyParam.getFloat());
		} else if (model == visibleParam) {
			setVisible(visibleParam.getValue());
		} else if (model == showSliceNumberParam) {
			setSliceNumberVisible(showSliceNumberParam.getValue());
		}
	}

	/**
	 * Sets the slice number visible.
	 * 
	 * @param showSliceNumber the new slice number visible
	 */
	private void setSliceNumberVisible(boolean showSliceNumber) {
		this.showSliceNumber = showSliceNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.cs.cisst.vent.VisualizationParameters#updateVisualizationParameters
	 * ()
	 */
	@Override
	public void updateVisualizationParameters() {
		setSlice(sliceParam.getInt() - 1);
		setContrast(contrastParam.getFloat());
		setBrightness(brightnessParam.getFloat());
		setTransparency(transparencyParam.getFloat());
		setVisible(visibleParam.getValue());
		setSliceNumberVisible(showSliceNumberParam.getValue());
		setComponent(componentParam.getInt() - 1);
	}

	/**
	 * Checks if is visible.
	 * 
	 * @return true, if is visible
	 */
	public boolean isVisible() {
		return visible;
	}
}
