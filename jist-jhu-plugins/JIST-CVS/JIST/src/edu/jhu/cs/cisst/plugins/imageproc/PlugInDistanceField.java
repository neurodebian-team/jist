/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.imageproc;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.DistanceField2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.volume.DistanceField;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class PlugInDistanceField.
 */
public class PlugInDistanceField extends ProcessingAlgorithm {

	/** The image param. */
	protected ParamVolume imageParam;

	/** The dist field param. */
	protected ParamVolume distFieldParam;

	/** The iso level param. */
	protected ParamFloat isoLevelParam;

	/** The max distance param. */
	protected ParamFloat maxDistanceParam;

	/** The sign param. */
	protected ParamBoolean signParam;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters
	 * (edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(imageParam = new ParamVolume("Image"));
		inputParams.add(isoLevelParam = new ParamFloat("Iso-Level"));
		inputParams.add(maxDistanceParam = new ParamFloat("Max Distance", 15));
		inputParams.add(signParam = new ParamBoolean("Flip Sign", false));
		inputParams.setName("dist_field");
		inputParams.setLabel("Distance Field");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Image Processing");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setDescription("Calculates the 2D or 3D distance field from a specified iso-level using the fast-marching method.");
		info
				.add(new Citation(
						"Sethian, J. (1999). Level set methods and fast marching methods, Cambridge university press Cambridge."));
		info.setVersion(DistanceField.getVersion());
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setStatus(DevelopmentStatus.Release);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters
	 * (edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(distFieldParam = new ParamVolume("Distance Field"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece
	 * .iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ImageDataFloat img = new ImageDataFloat(imageParam.getImageData());
		float isoLevel = isoLevelParam.getFloat();
		float sign = (signParam.getValue()) ? -1 : 1;
		if (img.toArray2d() != null) {
			DistanceField2D df = new DistanceField2D();
			monitor.observe(df);
			float[][] imgData = img.toArray2d();
			for (int i = 0; i < imgData.length; i++) {
				for (int j = 0; j < imgData[0].length; j++) {
					imgData[i][j] = sign * (imgData[i][j] - isoLevel);
				}
			}
			distFieldParam.setValue(df.solve(img, maxDistanceParam.getFloat()));
		} else {
			DistanceField df = new DistanceField();
			monitor.observe(df);
			float[][][] imgData = img.toArray3d();
			for (int i = 0; i < imgData.length; i++) {
				for (int j = 0; j < imgData[0].length; j++) {
					for (int k = 0; k < imgData[0][0].length; k++) {
						imgData[i][j][k] = sign * (imgData[i][j][k] - isoLevel);
					}
				}
			}
			ImageDataFloat result=df.solve(img, maxDistanceParam
					.getFloat());
			distFieldParam.setValue(result);
		}
	}
}
