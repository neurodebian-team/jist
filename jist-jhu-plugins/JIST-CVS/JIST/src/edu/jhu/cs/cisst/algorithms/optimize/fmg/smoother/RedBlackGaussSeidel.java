/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * An implementation of the Red-Black Gauss-Seidel method of relaxing (thereby
 * smoothing) the solution of a linear elliptic PDE.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class RedBlackGaussSeidel implements Smoother {
	
	/**
	 * construct from PDE.
	 * 
	 * @param pde a representation of the PDE
	 */
	public RedBlackGaussSeidel(PDE pde) {
		Contract.pre(pde != null, "pde not null-object");

		this.pde = pde;
	}

	/**
	 * implements method from Smoother.
	 * 
	 * @param u the u
	 * @param f the f
	 * 
	 * @return the boundary grid
	 * 
	 * @see Smoother#smooth
	 */
	public BoundaryGrid smooth(ConstBoundaryGrid u, ConstNoBoundaryGrid f) {	
		int sx = u.getRows();
		int sy = u.getCols();
		int sz = u.getSlices();

		BoundaryGrid g=(BoundaryGrid)u.clone();
		//Method has been changed so it can handle data that is not a power of 2 in all 3 dimensions - bcl
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				for (int k = 0; k < 2; k++) {
					for (int x = 1+i; x < (sx - 1); x += 2) {
						for (int y = 1+j; y < (sy - 1); y += 2) {
							for (int z = 1+k; z < (sz - 1); z += 2) {
								g.set(x, y, z, pde.evaluate(g, f, x, y, z));
							}
						}
					}

				}
			}
		}
		
		return g;
		
	}

	/** The pde. */
	private PDE pde;
}
