/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.util;

// TODO: Auto-generated Javadoc
/**
 * Some minimal support for "design by contract".
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class Contract {
     
     /** flag that says if the checks are really performed. */
     private static final boolean DO_IT = false;
     
     /**
      * check if precondition is met and throw exception if not.
      * 
      * @param condition the precondition that must be met
      * @param msg       if the condition is not met an object of class PreconditionNotMetException is thrown with the
      * message "A precondition was not met: " + msg + ".".
      * 
      * @exception PreconditionNotMetException if condition is not true
      */
     public static void pre(boolean condition, String msg) {
          if (DO_IT && !condition) throw new PreconditionNotMetException("A precondition was not met: " + msg + ".");
     }
     
     /**
      * check if postcondition is met and throw exception if not.
      * 
      * @param condition the postcondition that must be met
      * @param msg       if the condition is not met an object of class PostconditionNotMetException is thrown with the
      * message "A postcondition was not met: " + msg + ".".
      * 
      * @exception PostconditionNotMetException if condition is not true
      */
     public static void post(boolean condition, String msg) {
          if (DO_IT && !condition) throw new PostconditionNotMetException("A postcondition was not met: " + msg + ".");
     }
     
}
