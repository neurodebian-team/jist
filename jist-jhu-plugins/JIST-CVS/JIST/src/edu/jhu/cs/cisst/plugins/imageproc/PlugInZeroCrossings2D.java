/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.imageproc;

import edu.jhu.cs.cisst.algorithms.imageproc.Morphology2D;
import edu.jhu.cs.cisst.algorithms.imageproc.ZeroCrossings2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInMorphology2D.
 */
public class PlugInZeroCrossings2D extends ProcessingAlgorithm{
	
	
	/** The sigma x. */
	protected ParamFloat sigmaXParam;

	/** The sigma y. */
	protected ParamFloat sigmaYParam;
	
	/** The image param. */
	protected ParamVolume imageParam;
	
	/** The result param. */
	protected ParamVolume resultParam;
	
	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(imageParam=new ParamVolume("Image"));
		inputParams.add(sigmaXParam=new ParamFloat("Sigma X",1));
		inputParams.add(sigmaYParam=new ParamFloat("Sigma Y",1));
		inputParams.setName("zero_crossings2d");
		inputParams.setLabel("Zero Crossings 2D");
		inputParams.setPackage("CISST");
		inputParams.setCategory("Image Processing");
		AlgorithmInformation info=getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info.setVersion(Morphology2D.getVersion());
		info.setDescription("A wrapper around MIPAV's zero-crossings of Laplacian plug-in with non-maximal supression.");
		info.setStatus(DevelopmentStatus.Release);
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultParam=new ParamVolume("Result",null,-1,-1,-1,0));
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ZeroCrossings2D zerox=new ZeroCrossings2D(sigmaXParam.getFloat(),sigmaYParam.getFloat());
		resultParam.setValue(zerox.solve(imageParam.getImageData()));
	}

}
