/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent.widgets;

import java.util.ArrayList;
import java.util.LinkedList;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.cs.cisst.vent.*;
import edu.jhu.cs.cisst.vent.renderer.processing.ImageRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.RendererProcessing2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VectorFieldRenderer2D;
import edu.jhu.cs.cisst.vent.renderer.processing.VolumeIsoContourRenderer;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationActiveContour2D.
 */
public class VisualizationActiveContour2D extends VisualizationImage2D {
	
	/** The rule. */
	protected TopologyRule2D.Rule rule;
	
	/** The visualization. */
	protected VisualizationProcessing2D visualization;

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.11 $");
	}

	/** The vector field image. */
	ImageDataFloat levelset = null, pressureImage = null,
			vectorFieldImage = null;

	/** The original image. */
	ImageData originalImage = null;

	/**
	 * Instantiates a new visualization active contour.
	 *
	 * @param levelset the levelset
	 * @param rule the rule
	 */
	public VisualizationActiveContour2D(ImageDataFloat levelset,TopologyRule2D.Rule rule) {
		super(4*levelset.getRows(),4*levelset.getCols(),levelset.getRows(),levelset.getCols());
		this.levelset = levelset;
		this.rule=rule;
		setName("Visualization - Active Contour 2D");
	}

	/**
	 * Instantiates a new visualization active contour.
	 *
	 * @param width the width
	 * @param height the height
	 * @param levelset the levelset
	 */
	public VisualizationActiveContour2D(int width, int height,
			ImageDataFloat levelset) {
		super(width, height, levelset.getRows(), levelset.getCols());
		this.levelset = levelset;
		setName("Visualization - Active Contour 2D");
	}

	/**
	 * Creates the.
	 * 
	 * @return the param collection
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationProcessing#create()
	 */
	@Override
	public ParamCollection create() {
		renderers = new ArrayList<RendererProcessing2D>();
		if (originalImage != null) {
			renderers.add(new ImageRenderer2D(originalImage, this));
		}
		if (pressureImage != null) {
			renderers.add(new ImageRenderer2D(pressureImage, this));
		}
		if (levelset != null) {
			VolumeIsoContourRenderer renderer = new VolumeIsoContourRenderer(
					levelset,rule, this);
			renderers.add(renderer);
			renderer.setVisible(false);
		}
		if (vectorFieldImage != null)
			renderers.add(new VectorFieldRenderer2D(vectorFieldImage, this));

		ParamCollection params = super.create();

		return params;
	}

	/**
	 * Sets the pressure force.
	 * 
	 * @param pressureImage the new pressure force
	 */
	public void setPressureForce(ImageDataFloat pressureImage) {
		this.pressureImage = pressureImage;
	}

	/**
	 * Sets the vector field force.
	 * 
	 * @param vectorFieldImage the new vector field force
	 */
	public void setVectorFieldForce(ImageDataFloat vectorFieldImage) {
		this.vectorFieldImage = vectorFieldImage;
	}

	/**
	 * Sets the original image.
	 * 
	 * @param img the new original image
	 */
	public void setOriginalImage(ImageData img) {
		this.originalImage = img;
	}

	/**
	 * Sets the visualization.
	 * 
	 * @param vis the new visualization
	 */
	public void setVisualization(VisualizationProcessing vis) {
		this.visualization = (VisualizationProcessing2D)vis;
	}
}
