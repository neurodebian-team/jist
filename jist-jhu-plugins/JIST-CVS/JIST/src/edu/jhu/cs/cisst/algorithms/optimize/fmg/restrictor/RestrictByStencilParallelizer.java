/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.restrictor;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.Grid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * A helper class of class RestrictorByStencil that helps parallelizing method RestrictorByStencil.restrict().
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class RestrictByStencilParallelizer extends Parallelizer {
     
     /**
      * construct.
      * 
      * @param res the stencil
      * @param coarse the coarse grid
      * @param fine the fine grid
      */
     public RestrictByStencilParallelizer(RestrictorByStencil res, Grid coarse, ConstGrid fine) {
          this.res    = res;
          this.coarse = coarse;
          this.fine   = fine;
          
          start();
     }
     
     /**
      * re-route.
      * 
      * @param myNum the current index
      * @param totalNum the number of parallel jobs
      */
     public void runParallel(int myNum, int totalNum) {
          res.restrictProper(coarse,fine,myNum,totalNum);
     }
     
     /** The stencil. */
     private RestrictorByStencil res;
     
     /** The coarse. */
     private Grid                coarse;
     
     /** The fine. */
     private ConstGrid           fine;
}
