/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

// TODO: Auto-generated Javadoc
/**
 * The Class PointMeshTriangle stores triangle indices and vertex locations.
 * 
 * The point-triangle distance is an implementation of the method in: Schneider,
 * P. and D. Eberly (2002). Geometric tools for computer graphics.
 * 
 * @author Kel and Blake
 */
public class MeshTriangle {

	/** Indices to vertex values from associated triangle set. */
	protected int[] indices;

	/** Current Vertices Values (Changed by Update Method). */
	protected Point3f[] vertices;

	/** The cached closest point. */
	protected Point3f cachedClosestPoint = null;

	/**
	 * Gets the vertices.
	 * 
	 * @return the vertices
	 */
	public Point3f[] getVertices() {
		return vertices;
	}

	/**
	 * Sets the vertices.
	 * 
	 * @param vertices
	 *            the new vertices
	 */
	public void setVertices(Point3f[] vertices) {
		this.vertices = vertices;
	}


	/**
	 * Instantiate a PointMeshTriangle from an array of indices.
	 * 
	 * @param indices
	 *            the indices
	 */
	public MeshTriangle(int[] indices) {
		// Set Indices
		this.indices = indices;
	}

	/**
	 * Instantiate a PointMeshTriangle from an array of 3D Points.
	 * 
	 * @param vertices
	 *            the vertices
	 */
	public MeshTriangle(Point3f[] vertices) {
		// Set Points
		this.vertices = vertices;
	}

	/**
	 * Instantiate a PointMeshTriangle from an array of 3D Points.
	 *
	 * @param indices the indices
	 * @param vertices the vertices
	 */
	public MeshTriangle(int[] indices, Point3f[] vertices) {

		// Set Points
		this.indices = indices;
		this.vertices = vertices;
	}

	/**
	 * Instantiate a PointMeshTriangle from 3 indices.
	 * 
	 * @param i1
	 *            int index 1
	 * @param i2
	 *            int index 2
	 * @param i3
	 *            int index 3
	 */
	public MeshTriangle(int i1, int i2, int i3) {
		// Set Indices
		this.indices[0] = i1;
		this.indices[1] = i2;
		this.indices[2] = i3;
	}

	/**
	 * Instantiate a PointMeshTriangle from 3 3D Points.
	 * 
	 * @param v1
	 *            Point3f
	 * @param v2
	 *            Point3f
	 * @param v3
	 *            Point3f
	 */
	public MeshTriangle(Point3f v1, Point3f v2, Point3f v3) {
		// Set Points
		this.vertices[0] = v1;
		this.vertices[1] = v2;
		this.vertices[2] = v3;
	}

	/**
	 * Update the values of the vertices 3D locations from 3 points.
	 * 
	 * @param newV1
	 *            the new v1
	 * @param newV2
	 *            the new v2
	 * @param newV3
	 *            the new v3
	 */
	public void updateVertices(Point3f newV1, Point3f newV2, Point3f newV3) {
		this.vertices[0] = newV1;
		this.vertices[1] = newV2;
		this.vertices[2] = newV3;
	}

	/**
	 * Update the values of the vertices 3D locations from an array of points.
	 * 
	 * @param newVertices
	 *            the new vertices
	 */
	public void updateVertices(Point3f[] newVertices) {
		this.vertices = newVertices;
	}

	/**
	 * Distance to a point using parametric calculations.
	 *
	 * @param p the p
	 * @return the double
	 */
	public double distanceSquared(Point3f p) {
		double distanceSquared = 0;

		Vector3f v0 = new Vector3f(vertices[0]);
		Vector3f v1 = new Vector3f(vertices[1]);
		Vector3f v2 = new Vector3f(vertices[2]);

		Point3f P = p;
		Vector3f B = v0;

		Vector3f e0 = new Vector3f();
		e0.sub(v1, v0);

		Vector3f e1 = new Vector3f();
		e1.sub(v2, v0);

		float a = e0.dot(e0);
		float b = e0.dot(e1);
		float c = e1.dot(e1);

		Vector3f dv = new Vector3f();
		dv.sub(B, P);
		float d = e0.dot(dv);
		float e = e1.dot(dv);
		float f = dv.dot(dv);

		// Determine which region contains s, t
		int region = 0;

		float det = a * c - b * b;
		float s = b * e - c * d;
		float t = b * d - a * e;

		if (s + t <= det) {
			if (s < 0) {
				if (t < 0) {
					region = 4;
				} else {
					region = 3;
				}
			} else if (t < 0) {
				region = 5;
			} else {
				region = 0;
			}
		} else {
			if (s < 0) {
				region = 2;
			} else if (t < 0) {
				region = 6;
			} else {
				region = 1;
			}
		}

		/* Region Cases ------------------------------------------------------ */
		// System.out.println("\nThe Point is in Region: " + region + "\n");
		// Parametric Triangle Point
		Point3f T = new Point3f();

		if (region == 0) {// Region 0
			float invDet = (float) 1 / (float) det;
			s *= invDet;
			t *= invDet;

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		} else if (region == 1) {// Region 1
			float numer = c + e - b - d;

			if (numer < +0) {
				s = 0;
			} else {
				float denom = a - 2 * b + c;
				s = (numer >= denom ? 1 : numer / denom);
			}
			t = 1 - s;

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		} else if (region == 2) {// Region 2
			float tmp0 = b + d;
			float tmp1 = c + e;

			if (tmp1 > tmp0) {
				float numer = tmp1 - tmp0;
				float denom = a - 2 * b + c;
				s = (numer >= denom ? 1 : numer / denom);
				t = 1 - s;
			} else {
				s = 0;
				t = (tmp1 <= 0 ? 1 : (e >= 0 ? 0 : -e / c));
			}

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		} else if (region == 3) {// Region 3
			s = 0;
			t = (e >= 0 ? 0 : (-e >= c ? 1 : -e / c));

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		} else if (region == 4) {// Region 4
			float tmp0 = c + e;
			float tmp1 = a + d;

			if (tmp0 > tmp1) {
				s = 0;
				t = (tmp1 <= 0 ? 1 : (e >= 0 ? 0 : -e / c));
			} else {
				t = 0;
				s = (tmp1 <= 0 ? 1 : (d >= 0 ? 0 : -d / a));
			}

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		} else if (region == 5) {// Region 5
			t = 0;
			s = (d >= 0 ? 0 : (-d >= a ? 1 : -d / a));

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		} else {// Region 6
			float tmp0 = b + e;
			float tmp1 = a + d;

			if (tmp1 > tmp0) {
				float numer = tmp1 - tmp0;
				float denom = c - 2 * b + a;
				t = (numer >= denom ? 1 : numer / denom);
				s = 1 - t;
			} else {
				t = 0;
				s = (tmp1 <= 0 ? 1 : (d >= 0 ? 0 : -d / a));
			}

			// Find point on parametric triangle based on s and t
			T = parametricTriangle(e0, e1, s, t, B);
			// Find distance from P to T
			distanceSquared = P.distanceSquared(T);

		}

		cachedClosestPoint = T;
		return distanceSquared;
	}

	/**
	 * Create a point on a parametric triangle T(s,t).
	 *
	 * @param e0 vector e0
	 * @param e1 vector e1
	 * @param s the s parameter
	 * @param t the t parameter
	 * @param B the base vector
	 * @return T The point on the triangle
	 */
	public Point3f parametricTriangle(Vector3f e0, Vector3f e1, float s,
			float t, Vector3f B) {
		Vector3f Bsum = new Vector3f();

		Vector3f se0 = new Vector3f();
		se0.scale(s, e0);

		Vector3f te1 = new Vector3f();
		te1.scale(t, e1);

		Bsum.add(B, se0);
		Bsum.add(te1);

		Point3f T = new Point3f(Bsum);

		return T;
	}

	/**
	 * Distance to triangle.
	 * 
	 * @param p
	 *            the point
	 * 
	 * @return distance from point to triangle
	 */
	/*
	 * public double distanceSimple(Point3f p){ return
	 * Math.sqrt(distanceSquared(p)); }
	 */
	/**
	 * Distance to triangle.
	 * 
	 * @param p
	 *            the point
	 * 
	 * @return distance from point to triangle
	 */
	public double distance(Point3f p) {
		return Math.sqrt(distanceSquared(p));
	}

	/**
	 * Gets the last closest point.
	 * 
	 * @return the last closest point
	 */
	public Point3f getLastClosestPoint() {
		return cachedClosestPoint;
	}



	/**
	 * Get description of triangle.
	 *
	 * @return the string
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[" + indices[0] + "," + indices[1] + "," + indices[2] + "] ["
				+ vertices[0] + "," + vertices[1] + "," + vertices[2] + "]";
	}
}
