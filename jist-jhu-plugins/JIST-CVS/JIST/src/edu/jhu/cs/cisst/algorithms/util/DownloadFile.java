package edu.jhu.cs.cisst.algorithms.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;

public class DownloadFile extends AbstractCalculation {
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.1 $");
	}
	public enum DataType {
		BINARY, ASCII
	};

	public DownloadFile() {
		super();
		setLabel("Download");
	}

	public DownloadFile(AbstractCalculation parent) {
		super(parent);
		setLabel("Download");
	}

	/**
	 * Get file name from url.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return image name
	 */
	public static String getFileNameFromUrl(URL url) {

		String fileName = url.getFile();
		int index = fileName.lastIndexOf("/");
		if (index >= 0) {
			return fileName.substring(index, fileName.length());
		} else {
			return fileName;
		}
	}

	public File download(URL url, File dir, DataType type) {
		URLConnection conn;
		try {
			conn = url.openConnection();
			if (!dir.exists()) {
				dir.mkdirs();
			}
			File newFile = new File(dir, getFileNameFromUrl(url));
			if (type == DataType.BINARY) {

				DataInputStream in = new DataInputStream(conn.getInputStream());
				BufferedOutputStream out = new BufferedOutputStream(
						new FileOutputStream(newFile));
				// Transfer bytes from the ZIP file to the output file
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				// Close this output stream
				out.close();
				in.close();
			} else if (type == DataType.ASCII) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				BufferedWriter out = new BufferedWriter(new FileWriter(newFile));
				String inputLine = null;
				while ((inputLine = in.readLine()) != null) {
					out.write(in.readLine() + "\n");
				}
				out.close();
				in.close();

			}
			return newFile;
		} catch (IOException e) {
			e.printStackTrace();

		}
		return null;
	}
}
