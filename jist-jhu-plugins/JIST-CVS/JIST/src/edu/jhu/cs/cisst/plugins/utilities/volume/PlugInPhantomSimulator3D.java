/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.utilities.volume;

import javax.vecmath.*;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce3D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContour2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.AppearanceForce3D.Heaviside;
import edu.jhu.cs.cisst.algorithms.util.phantom.*;
import edu.jhu.cs.cisst.algorithms.util.phantom.PhantomSimulator3D.NoiseType;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInPhantomSimulator3D.
 */
public class PlugInPhantomSimulator3D extends ProcessingAlgorithm {

	/** The levelset param. */
	protected ParamVolume levelsetParam;

	/** The image param. */
	protected ParamVolume imageParam;

	/** The phantom type param. */
	protected ParamOption phantomTypeParam;

	/** The dim param. */
	protected ParamPointInteger dimParam;

	/** The heaviside func param. */
	protected ParamOption heavisideFuncParam;

	/** The heaviside fuzz param. */
	protected ParamDouble heavisideFuzzParam;

	/** The noise level param. */
	protected ParamDouble noiseLevelParam;

	/** The noise type param. */
	protected ParamOption noiseTypeParam;

	/** The sphere radius param. */
	protected ParamDouble sphereRadiusParam;

	/** The cube width param. */
	protected ParamDouble cubeWidthParam;
	/** The cube center param. */
	protected ParamPointDouble cubeCenterParam;

	/** The cube width param. */
	protected ParamDouble checkerboardWidthParam;

	/** The cube width param. */
	protected ParamPointDouble checkerboardFrequencyParam;

	/** The cube center param. */
	protected ParamPointDouble checkerboardCenterParam;

	/** The sphere center param. */
	protected ParamPointDouble sphereCenterParam;

	/** The metasphere center param. */
	protected ParamPointDouble metasphereCenterParam;

	/** The metasphere center param. */
	protected ParamPointDouble torusCenterParam;

	/** The torus inner radius param. */
	protected ParamDouble torusInnerRadiusParam;

	/** The torus outer radius param. */
	protected ParamDouble torusOuterRadiusParam;

	/** The num bubbles param. */
	protected ParamInteger numBubblesParam;

	/** The min radius param. */
	protected ParamDouble minRadiusParam;

	/** The max radius param. */
	protected ParamDouble maxRadiusParam;

	/** The freq param. */
	protected ParamDouble freqParam;

	/** The min amp param. */
	protected ParamDouble minAmpParam;

	/** The max amp param. */
	protected ParamDouble maxAmpParam;

	/** The surf param. */
	protected ParamSurface surfParam;

	/** The invert image param. */
	protected ParamBoolean invertImageParam;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters
	 * (edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {

		ParamCollection mainParams = new ParamCollection("Main");
		mainParams.add(phantomTypeParam = new ParamOption("Phantom Type",
				new String[] { "Sphere", "Cube", "Bubbles", "Metasphere",
						"Torus", "Checkerboard" }));
		mainParams.add(dimParam = new ParamPointInteger("Dimensoins",
				new Point3i(100, 100, 100)));
		mainParams.add(noiseLevelParam = new ParamDouble("Noise Level", 0, 1,
				0.01));
		mainParams.add(noiseTypeParam = new ParamOption("Noise Type",
				new String[] { "Uniform", "Gaussian" }));
		noiseTypeParam.setValue(1);
		mainParams.add(heavisideFuncParam = new ParamOption(
				"Heaviside Function", AppearanceForce3D.heavisideNames));
		mainParams.add(heavisideFuzzParam = new ParamDouble(
				"Heaviside Fuzziness", 0, 1E10, 1.5));
		mainParams.add(invertImageParam = new ParamBoolean("Invert", true));
		ParamCollection sphereParams = new ParamCollection("Sphere");
		sphereParams.add(sphereRadiusParam = new ParamDouble("Radius", 0, 1,
				0.5));
		sphereParams.add(sphereCenterParam = new ParamPointDouble("Center",
				new Point3d(0, 0, 0)));

		ParamCollection cubeParams = new ParamCollection("Cube");
		cubeParams.add(cubeWidthParam = new ParamDouble("Width", 0, 2, 0.5));
		cubeParams.add(cubeCenterParam = new ParamPointDouble("Center",
				new Point3d(0, 0, 0)));

		ParamCollection bubblesParams = new ParamCollection("Bubbles");
		bubblesParams.add(numBubblesParam = new ParamInteger(
				"Number of Bubbles", 1, 1000000, 8));
		bubblesParams.add(minRadiusParam = new ParamDouble("Min Radius", 0, 1,
				0.05));
		bubblesParams.add(maxRadiusParam = new ParamDouble("Max Radius", 0, 1,
				0.15));

		ParamCollection metasphereParams = new ParamCollection("Metasphere");
		metasphereParams.add(freqParam = new ParamDouble("Frequency", 0,
				1000000, 6));
		metasphereParams.add(minAmpParam = new ParamDouble("Min Amplitude", 0,
				1, 0.7));
		metasphereParams.add(maxAmpParam = new ParamDouble("Max Amplitude", 0,
				1, 0.9));
		metasphereParams.add(metasphereCenterParam = new ParamPointDouble(
				"Center", new Point3d()));

		ParamCollection torusParams = new ParamCollection("Torus");
		torusParams.add(torusCenterParam = new ParamPointDouble("Center",
				new Point3d(0, 0, 0)));
		torusParams.add(torusInnerRadiusParam = new ParamDouble("Inner Radius",
				0, 1, 0.2));
		torusParams.add(torusOuterRadiusParam = new ParamDouble("Outer Radius",
				0, 1, 0.6));

		ParamCollection checkerboardParams = new ParamCollection("Checkerboard");
		checkerboardParams.add(checkerboardWidthParam = new ParamDouble(
				"Width", 0, 2, 0.5));
		checkerboardParams.add(checkerboardCenterParam = new ParamPointDouble(
				"Center", new Point3d()));
		checkerboardParams
				.add(checkerboardFrequencyParam = new ParamPointDouble(
						"Frequencies", new Point3d(1, 1, 1)));

		inputParams.add(mainParams);
		inputParams.add(sphereParams);
		inputParams.add(cubeParams);
		inputParams.add(bubblesParams);
		inputParams.add(metasphereParams);
		inputParams.add(torusParams);
		inputParams.add(checkerboardParams);
		inputParams.setName("phantom_sim_3d");
		inputParams.setLabel("Phantom Simulator 3D");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setDescription("Generates a 3D phantom image, it's level set and iso-surface representation.");
		info
				.setLongDescription("This method is particularly useful for testing 3D segmentation methods.");
		info.setVersion(PhantomSimulator3D.getVersion());
		info
				.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setStatus(DevelopmentStatus.Release);
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Volume");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters
	 * (edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(levelsetParam = new ParamVolume("Level Set"));
		outputParams.add(imageParam = new ParamVolume("Image"));
		outputParams.add(surfParam = new ParamSurface("Surface"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece
	 * .iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		PhantomSimulator3D phantom = null;

		switch (phantomTypeParam.getIndex()) {
		case 0:
			phantom = new PhantomSphere(dimParam.getValue());
			((PhantomSphere) phantom).setCenter(sphereCenterParam.getValue());
			((PhantomSphere) phantom).setRadius(sphereRadiusParam.getDouble());
			break;
		case 1:
			phantom = new PhantomCube(dimParam.getValue());
			((PhantomCube) phantom).setCenter(cubeCenterParam.getValue());
			((PhantomCube) phantom).setWidth(cubeWidthParam.getDouble());
			break;
		case 2:
			phantom = new PhantomBubbles(dimParam.getValue());
			((PhantomBubbles) phantom).setMinRadius(minRadiusParam.getDouble());
			((PhantomBubbles) phantom).setMaxRadius(maxRadiusParam.getDouble());
			((PhantomBubbles) phantom).setNumberOfBubbles(numBubblesParam
					.getInt());
			break;
		case 3:
			phantom = new PhantomMetasphere(dimParam.getValue());
			((PhantomMetasphere) phantom).setFrequency(freqParam.getDouble());
			((PhantomMetasphere) phantom).setMinAmplitude(minAmpParam
					.getDouble());
			((PhantomMetasphere) phantom).setMaxAmplitude(maxAmpParam
					.getDouble());
			((PhantomMetasphere) phantom).setCenter(metasphereCenterParam
					.getValue());
			break;
		case 4:
			phantom = new PhantomTorus(dimParam.getValue());
			((PhantomTorus) phantom).setCenter(torusCenterParam.getValue());
			((PhantomTorus) phantom).setInnerRadius(torusInnerRadiusParam
					.getDouble());
			((PhantomTorus) phantom).setOuterRadius(torusOuterRadiusParam
					.getDouble());
			break;
		case 5:
			phantom = new PhantomCheckerBoard(dimParam.getValue());
			((PhantomCheckerBoard) phantom).setCenter(checkerboardCenterParam.getValue());
			((PhantomCheckerBoard) phantom).setFrequency(checkerboardFrequencyParam.getValue());
			((PhantomCheckerBoard) phantom).setWidth(checkerboardWidthParam.getDouble());
			break;
		}
		phantom.setFuzziness(heavisideFuzzParam.getDouble());
		phantom.setHeaviside(Heaviside.values()[heavisideFuncParam.getIndex()]);
		phantom.setNoiseLevel(noiseLevelParam.getDouble());
		phantom.setNoiseType(NoiseType.values()[noiseTypeParam.getIndex()]);
		phantom.setInvertImage(invertImageParam.getValue());
		phantom.solve();
		levelsetParam.setValue(phantom.getLevelset());
		imageParam.setValue(phantom.getImage());
		surfParam.setValue(phantom.getSurface());
	}

}
