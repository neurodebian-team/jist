/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.NoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * A helper class of class FMG that helps parallelizing method FMG.calcResidual().
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class CalcResidualParallelizer extends Parallelizer {
     
     /**
      * construct.
      * 
      * @param fmg the FMG object with which this object co-operates
      * @param r the r
      * @param u the u
      * @param f the f
      */
     public CalcResidualParallelizer(FMG fmg, NoBoundaryGrid r, ConstBoundaryGrid u, ConstNoBoundaryGrid f) {
          this.fmg = fmg;
          this.r   = r;
          this.u   = u;
          this.f   = f;
          
          start();
     }
     
     /**
      * re-route.
      * 
      * @param myNum the my num
      * @param totalNum the total num
      */
     public void runParallel(int myNum, int totalNum) {
          fmg.calcResidualProper(r,u,f,myNum,totalNum);
     }
     
     /** The fmg. */
     private FMG                 fmg;
     
     /** The residual grid. */
     private NoBoundaryGrid      r;
     
     /** The u. */
     private ConstBoundaryGrid   u;
     
     /** The f. */
     private ConstNoBoundaryGrid f;
}
