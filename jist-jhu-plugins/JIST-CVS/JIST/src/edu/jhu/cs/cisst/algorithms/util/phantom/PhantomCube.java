/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.util.phantom;

import javax.vecmath.Point3i;
import javax.vecmath.Point3d;
// TODO: Auto-generated Javadoc

/**
 * The Class PhantomCube.
 */
public class PhantomCube extends PhantomSimulator3D{
	
	/** The width. */
	protected double width;
	
	/** The center. */
	protected Point3d center;
	
	/**
	 * Sets the width.
	 * 
	 * @param width the new width
	 */
	public void setWidth(double width) {
		this.width = width;
	}
	
	/**
	 * Sets the center.
	 * 
	 * @param center the new center
	 */
	public void setCenter(Point3d center) {
		this.center = center;
	}
	
	/**
	 * Instantiates a new phantom cube.
	 * 
	 * @param dims the dims
	 */
	public PhantomCube(Point3i dims) {
		super(dims);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.algorithms.util.phantom.PhantomSimulator3D#solve()
	 */
	public void solve(){
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double x = (2 * i / (double) rows - 1);
					double y = (2 * j / (double) cols - 1);
					double z = (2 * k / (double) slices - 1);
					levelset.set(i,j,k,
							(		(x-center.x)>-0.5*width&&
									(x-center.x)<0.5*width&&
									(y-center.y)>-0.5*width&&
									(y-center.y)<0.5*width&&
									(z-center.z)>-0.5*width&&
									(z-center.z)<0.5*width)?-1:1);
				}
			}
		}
		levelset.setName("cube_level");
		image.setName("cube");
		finish();
	}
}
