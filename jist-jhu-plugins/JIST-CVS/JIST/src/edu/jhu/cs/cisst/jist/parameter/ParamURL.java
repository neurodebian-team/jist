/**
 * Java Image Science Toolkit (JIST)
 *
 * Image Analysis and Communications Laboratory &
 * Laboratory for Medical Image Computing &
 * The Johns Hopkins University
 * 
 * http://www.nitrc.org/projects/jist/
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 */
package edu.jhu.cs.cisst.jist.parameter;

import java.net.MalformedURLException;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.cs.cisst.jist.pipeline.factory.ParamURLFactory;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

/**
 * The Class ParamURL.
 */
public class ParamURL extends ParamModel<URL> {

	/** The url. */
	protected URL url;

	/**
	 * Instantiates a new param url.
	 */
	public ParamURL() {
		init();
	}

	/**
	 * Instantiates a new param url.
	 * 
	 * @param name the name
	 */
	public ParamURL(String name) {
		this();
		setName(name);
	}

	/**
	 * Instantiates a new param url.
	 * 
	 * @param name the name
	 * @param url the url
	 */
	public ParamURL(String name, URL url) {
		this(name);
		setName(name);
		setValue(url);
	}

	/**
	 * Clone object.
	 * 
	 * @return the param url
	 */
	public ParamURL clone() {
		ParamURL param = new ParamURL(this.getName());
		param.setValue(url);
		param.setName(this.getName());
		param.label = this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel = shortLabel;
		param.cliTag = cliTag;
		return param;
	}

	/**
	 * Compare two parameters based on their mandatory fields.
	 * 
	 * @param model the model
	 * 
	 * @return the int
	 */
	public int compareTo(ParamModel model) {
		if (model instanceof ParamURL) {
			ParamURL f = (ParamURL) model;
			if (this.mandatory && !f.isMandatory()) {
				return 1;
			} else {
				return 0;
			}
		}
		return 1;
	}

	/**
	 * Get url location.
	 * 
	 * @return url location
	 */
	public URL getValue() {
		return url;
	}

	/**
	 * Inits the.
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.PipePort#init()
	 */
	public void init() {
		this.setMaxIncoming(1);
		connectible = true;
		factory = new ParamURLFactory(this);
	}

	/**
	 * Returns true if port is compatible with this parameter's extension
	 * filter.
	 * 
	 * @param model the model
	 * 
	 * @return true, if checks if is compatible
	 */
	public boolean isCompatible(PipePort model) {
		if (model instanceof ParamURL) {
			return true;
		}
		return false;
	}

	/**
	 * Set the url location for this parameter.
	 * 
	 * @param value parameter value
	 */
	public void setValue(URL value) {
		this.url = value;
	}

	/**
	 * Set url location.
	 * 
	 * @param value url location as string
	 */
	public void setValue(String value) {
		this.url=null;
		try {
			this.url = new URL(value);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	/**
	 * Get description of parameter.
	 * 
	 * @return the string
	 */
	public String toString() {
		return (url != null) ? url.toString() : "None";
	}

	/**
	 * Validate that the specified url exists and meets all restrictions.
	 * 
	 * @throws InvalidParameterException parameter value does not meet value restrictions
	 */
	public void validate() throws InvalidParameterException {
		if (!mandatory) {
			return;
		}
		URL url = getValue();
		if (url == null) {
			throw new InvalidParameterException(this);
		}
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#clean()
	 */
	public void clean() {
		url = null;
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#getHumanReadableDataType()
	 */
	public String getHumanReadableDataType() {
		return "url";
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#getXMLValue()
	 */
	@Override
	public String getXMLValue() {
		if (getValue() == null)
			return null;
		return getValue().toString();
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#setXMLValue(java.lang.String)
	 */
	@Override
	public void setXMLValue(String arg) {
		setValue(arg);
	}

	/**
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#probeDefaultValue()
	 */
	public String probeDefaultValue() {
		if (url == null)
			return null;
		return getXMLValue();
	}

	public boolean xmlEncodeParam(Document document, Element parent) {
		 super.xmlEncodeParam(document, parent);		 
		 Element em;				
		 em = document.createElement("url");				 
		 em.appendChild(document.createTextNode(url+""));
		 parent.appendChild(em);
		 return true;
		 		
	 }
	
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		try {
			url = new URL(JistXMLUtil.xmlReadTag(parent, "url"));
		} catch (MalformedURLException e) {
url=null;
			e.printStackTrace();
		}
			
	}
}
