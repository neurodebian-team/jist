/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent;

import java.awt.Dimension;
import java.awt.Image;

import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import processing.core.PApplet;
import processing.core.PImage;

// TODO: Auto-generated Javadoc
/**
 * The Class VisualizationProcessing.
 */
public abstract class VisualizationProcessing extends PApplet implements
		Visualization {

	/** The request screen shot. */
	protected boolean requestScreenShot = false;

	/** The screenshot. */
	protected PImage screenshot = null;

	protected int preferredWidth;
	protected int preferredHeight;
	/** The name. */
	protected String name;

	public VisualizationProcessing(int width, int height) {
		this.preferredWidth = width;
		this.preferredHeight = height;
		setPreferredSize(new Dimension(width, height));
		setSize(new Dimension(width, height));
	}

	/** The visualization parameters. */
	protected ParamCollection visualizationParameters;

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * 
	 * @see java.awt.Component#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @see java.awt.Component#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Instantiates a new visualization processing.
	 */
	public VisualizationProcessing() {
		this(VentPreferences.getInstance().getDefaultCanvasWidth(),
				VentPreferences.getInstance().getDefaultCanvasHeight());

	}

	/**
	 * Gets the component.
	 * 
	 * @return the component
	 * 
	 * @see edu.jhu.cs.cisst.vent.Visualization#getComponent()
	 */
	public PApplet getComponent() {
		return this;
	}

	/**
	 * Dispose.
	 * 
	 * @see edu.jhu.cs.cisst.vent.Visualization#dispose()
	 */
	public void dispose() {
		super.destroy();
	}

	protected ParamDouble durationParam;
	protected ParamInteger movieWidthParam;
	protected ParamInteger movieHeightParam;
	protected ParamInteger frameRateParam;

	public double getDuration() {
		return durationParam.getDouble();
	}

	public Dimension getMovieDimensions() {
		return new Dimension(movieWidthParam.getInt(), movieHeightParam
				.getInt());
	}


	public int getFrameRate() {
		return frameRateParam.getInt();
	}

	/**
	 * Creates the visualization parameters.
	 * 
	 * @param visualizationParameters
	 *            the visualization parameters
	 * 
	 * @see edu.jhu.cs.cisst.vent.VisualizationParameters#createVisualizationParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	public void createVisualizationParameters(
			ParamCollection visualizationParameters) {
		ParamCollection prefsParam = new ParamCollection("Preferences");
		prefsParam.add(durationParam = new ParamDouble("Duration", 0, 1E6, 1));
		prefsParam.add(frameRateParam = new ParamInteger("Frame Rate", 1,
				10000000, 15));
		prefsParam.add(movieWidthParam = new ParamInteger("Movie Width", 0,
				1000000, 640));
		prefsParam.add(movieHeightParam = new ParamInteger("Movie Height", 0,
				1000000, 480));
		visualizationParameters.add(prefsParam);
	}

	/**
	 * Creates the.
	 * 
	 * @return the param collection
	 * 
	 * @see edu.jhu.cs.cisst.vent.Visualization#create()
	 */
	public ParamCollection create() {
		init();
		visualizationParameters = new ParamCollection(name);
		createVisualizationParameters(visualizationParameters);
		return visualizationParameters;
	}
	public void setup(){
		size(preferredWidth, preferredHeight, OPENGL);
	}
	/**
	 * @see edu.jhu.cs.cisst.vent.Visualization#getVideoFrames(long, long)
	 */
	public abstract Image[] getVideoFrames(long frameRate, long duration);
}