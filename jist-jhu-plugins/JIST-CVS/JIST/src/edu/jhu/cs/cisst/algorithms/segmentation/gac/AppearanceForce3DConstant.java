/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import java.util.ArrayList;
import java.util.Collections;

import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class AppearanceForce2DConstant approximates the foreground and
 * background of a region using a constant.
 */
public class AppearanceForce3DConstant extends AppearanceForce3D {

	/**
	 * The Enum Metric.
	 */
	public enum Metric {
		/** The MEAN. */
		MEAN, /** The MEDIAN. */
		MEDIAN
	}

	/** The fg color. */
	protected double fgColor = 0;

	/** The bg color. */
	protected double bgColor = 0;;

	/** The metric. */
	protected Metric metric = Metric.MEDIAN;

	/** The Constant metricNames. */
	public static final String[] metricNames = new String[] { "Mean", "Median" };

	/**
	 * Instantiates a new appearance force2 d constant.
	 * 
	 * @param image
	 *            the image
	 * @param normalizeIntensities
	 *            the normalize intensities
	 */
	public AppearanceForce3DConstant(ImageDataFloat image,
			boolean normalizeIntensities) {
		super(image, normalizeIntensities);
	}

	/**
	 * Gets the metric.
	 * 
	 * @return the metric
	 */
	public Metric getMetric() {
		return metric;
	}

	/**
	 * Sets the metric.
	 * 
	 * @param metric
	 *            the new metric
	 */
	public void setMetric(Metric metric) {
		this.metric = metric;
	}

	/**
	 * Update the foreground / background intensity estimates.
	 * 
	 * @return the image data float
	 */
	public void update() {
		float[][][] heavisideImage = new float[rows][cols][slices];
		float[][][] intensityEstimateImage = imageEstimate.toArray3d();
		switch (metric) {
		case MEAN:
			fgColor = 0;
			bgColor = 0;
			double fgArea = 0;
			double bgArea = 0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						double heavy = heavisideImage[i][j][k] = (float) getHeavisideValue(
								i, j, k);
						double val = intensityImage[i][j][k];
						fgColor += (1 - heavy) * val;
						fgArea += 1 - heavy;
						bgColor += heavy * val;
						bgArea += heavy;
					}
				}
			}
			if (bgArea > 0) {
				bgColor /= bgArea;
			}
			if (fgArea > 0) {
				fgColor /= fgArea;
			}
			break;
		case MEDIAN:
			ArrayList<Float> fgPixels = new ArrayList<Float>();
			ArrayList<Float> bgPixels = new ArrayList<Float>();
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						heavisideImage[i][j][k] = (float) getHeavisideValue(i,
								j, k);
						float val = intensityImage[i][j][k];
						if (getLevelsetValue(i, j,k) < 0) {
							fgPixels.add(val);
						} else {
							bgPixels.add(val);
						}
					}
				}
			}
			Collections.sort(fgPixels);
			Collections.sort(bgPixels);
			if (fgPixels.size() != 0) {
				if (fgPixels.size() % 2 == 0) {
					fgColor = 0.5 * (fgPixels.get(fgPixels.size() / 2) + fgPixels
							.get(Math.max(0, fgPixels.size() / 2 - 1)));
				} else {
					fgColor = fgPixels.get(fgPixels.size() / 2);
				}
			}
			if (bgPixels.size() != 0) {
				if (bgPixels.size() % 2 == 0) {
					bgColor = 0.5 * (bgPixels.get(bgPixels.size() / 2) + bgPixels
							.get(Math.max(0, bgPixels.size() / 2 - 1)));
				} else {
					bgColor = bgPixels.get(bgPixels.size() / 2);
				}
			}
			break;
		}
		System.out.println("FG Color: " + fgColor + " BG Color: " + bgColor);
		double maxHeavyVal = heavisideDerivative(0, fuzziness, heaviside);
		double invMaxHeavyVal = 1 / maxHeavyVal;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double indicator = heavisideImage[i][j][k];
					intensityEstimateImage[i][j][k] = (float) ((((1 - indicator)
							* fgColor + bgColor * indicator)));
					double val = intensityImage[i][j][k];
					double heavyVal = invMaxHeavyVal
							* heavisideDerivative(levelset[i][j][k], fuzziness,
									heaviside);
					// Pressure force is only valid in capture range of
					// heaviside
					pressureForce[i][j][k] = (float) (heavyVal * (-fgWeight
							* (val - fgColor) * (val - fgColor) + bgWeight
							* (val - bgColor) * (val - bgColor)));
				}
			}
		}
	}

}
