/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class PressureForce2D moves the level set outwards in regions of positive
 * pressure and inwards in regions of negative pressure.
 */
public class PressureForce2D extends ActiveContourForce2D {

	/** The pressure force. */
	protected float[][] pressureForce;

	public PressureForce2D() {
		this.pressureForce = null;
	}

	/**
	 * Instantiates a new pressure force.
	 * 
	 * @param pressureForce
	 *            the pressure force
	 */
	public PressureForce2D(float[][] pressureForce) {
		this.pressureForce = pressureForce;
	}

	/**
	 * Rescale the pressure field so that the min and max are normalized to one
	 * and zero pressure is at the specified offset.
	 * 
	 * @param offset
	 *            the offset pressure
	 */
	public void rescale(double offset) {
		int index = 0;
		double min = Float.MAX_VALUE;
		double max = Float.MIN_VALUE;
		for (int i = 0; i < pressureForce.length; i++) {
			for (int j = 0; j < pressureForce[0].length; j++) {

				double val = pressureForce[i][j] - offset;
				min = Math.min(val, min);
				max = Math.max(val, max);
				index++;
			}
		}
		double normMin = (Math.abs(min) > 1E-4) ? 1 / Math.abs(min) : 1;
		double normMax = (Math.abs(max) > 1E-4) ? 1 / Math.abs(max) : 1;
		for (int i = 0; i < pressureForce.length; i++) {
			for (int j = 0; j < pressureForce[0].length; j++) {
				double val = pressureForce[i][j] - offset;
				if (val < 0) {
					pressureForce[i][j] = (float) (val * normMin);
				} else {
					pressureForce[i][j] = (float) (val * normMax);
				}
			}
		}
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce2D
	 *      #evaluate(int, int)
	 */
	public double evaluate(int i, int j) {
		GridPoint2D p01 = getGridPoint(i - 1, j);
		GridPoint2D p12 = getGridPoint(i, j + 1);
		GridPoint2D p11 = getGridPoint(i, j);
		GridPoint2D p10 = getGridPoint(i, j - 1);
		GridPoint2D p21 = getGridPoint(i + 1, j);
		double v11 = p11.value;
		double v01 = ((p01 != null) ? p01.value : v11);
		double v12 = ((p12 != null) ? p12.value : v11);
		double v10 = ((p10 != null) ? p10.value : v11);
		double v21 = ((p21 != null) ? p21.value : v11);
		double DxNeg = v11 - v01;
		double DxPos = v21 - v11;
		double DyNeg = v11 - v10;
		double DyPos = v12 - v11;
		double DxNegMin = Math.min(DxNeg, 0);
		double DxNegMax = Math.max(DxNeg, 0);
		double DxPosMin = Math.min(DxPos, 0);
		double DxPosMax = Math.max(DxPos, 0);
		double DyNegMin = Math.min(DyNeg, 0);
		double DyNegMax = Math.max(DyNeg, 0);
		double DyPosMin = Math.min(DyPos, 0);
		double DyPosMax = Math.max(DyPos, 0);
		double GradientSqrPos = DxNegMax * DxNegMax + DxPosMin * DxPosMin
				+ DyNegMax * DyNegMax + DyPosMin * DyPosMin;
		double GradientSqrNeg = DxPosMax * DxPosMax + DxNegMin * DxNegMin
				+ DyPosMax * DyPosMax + DyNegMin * DyNegMin;
		// Force should be negative to move level set outwards if pressure is
		// positive
		double force = weight * pressureForce[i][j];
		if (force > 0) {
			return -force * Math.sqrt(GradientSqrPos);
		} else if (force < 0) {
			return -force * Math.sqrt(GradientSqrNeg);
		} else
			return 0;
	}

}
