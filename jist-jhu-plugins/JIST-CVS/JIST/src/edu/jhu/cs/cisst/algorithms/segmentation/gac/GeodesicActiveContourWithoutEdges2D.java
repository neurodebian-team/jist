/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class GeodesicActiveContourWithoutEdges2D extends GAC by adding support
 * for appearance forces. This subclass is necessary because the appearance
 * force requires the narrow-band to be rebuilt after each outer iteration.
 */
public class GeodesicActiveContourWithoutEdges2D extends
		GeodesicActiveContour2D {

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.2 $");
	}

	/** The verbose estimated image. */
	protected ImageDataFloat verboseEstimatedImage;

	/** The estimated image. */
	protected ImageDataFloat estimatedImage;

	/**
	 * Instantiates a new geodesic active contour without edges2 d.
	 * 
	 * @param referenceImage
	 *            the reference image
	 */
	public GeodesicActiveContourWithoutEdges2D(ImageData referenceImage) {
		super(referenceImage);
	}

	/**
	 * Gets the estimated image.
	 * 
	 * @return the estimated image
	 */
	public ImageDataFloat getEstimatedImage() {
		return estimatedImage;
	}

	/**
	 * Gets the verbose estimated image.
	 * 
	 * @return the verbose estimated image
	 */
	public ImageDataFloat getVerboseEstimatedImage() {
		return verboseEstimatedImage;
	}

	/**
	 * Solve.
	 * 
	 * @param initialLevelset
	 *            the initial level set
	 * 
	 * @return the final level set
	 */
	public ImageDataFloat solve(ImageDataFloat initialLevelset) {
		double dice = 0;
		posToNegCount = negToPosCount = 0;
		estimatedImage = new ImageDataFloat(rows, cols);
		estimatedImage.setName(image.getName() + "_estimated");
		initNarrowBand(initialLevelset);

		DistanceField2D dist = new DistanceField2D();
		for (ActiveContourForce2D force : forces) {
			if (force instanceof AppearanceForce2D) {
				((AppearanceForce2D) force).setLevelSet(distField);
			}
		}
		setTotalUnits(outerIterations);
		for (int outerIter = 0; outerIter < outerIterations; outerIter++) {
			for (GridPoint2D pt : activePoints) {
				distField[pt.x][pt.y] = pt.value;
			}
			for (int k = 0; k < maxLayers; k++) {
				for (GridPoint2D pt : insidePoints[k]) {
					distField[pt.x][pt.y] = pt.value;
				}
				for (GridPoint2D pt : outsidePoints[k]) {
					distField[pt.x][pt.y] = pt.value;
				}
			}
			// Update forces
			for (ActiveContourForce2D force : forces) {
				force.update();
			}

			for (int innerIter = 0; innerIter < innerIterations; innerIter++) {
				updateNarrowBand();
			}
			dice = 2
					* (insideCount - negToPosCount)
					/ (double) (2 * insideCount + posToNegCount - negToPosCount);
			// Update number of inside points
			insideCount = insideCount + posToNegCount - negToPosCount;
			posToNegCount = negToPosCount = 0;
			setLabel(String.format("Dice Coeff: %1.4f", dice));
			if (dice >= diceThreshold) {
				break;
			}
			incrementCompletedUnits();
		}
		setLabel("Active Contour");
		// Rebuild signed distance function
		for (GridPoint2D pt : activePoints) {
			distField[pt.x][pt.y] = pt.value;
		}
		for (int k = 0; k < maxLayers; k++) {
			for (GridPoint2D pt : insidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
			for (GridPoint2D pt : outsidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
		}
		for (ActiveContourForce2D force : forces) {
			force.update();
			if (force instanceof AppearanceForce2D) {
				ImageDataFloat img = ((AppearanceForce2D) force)
						.getImageEstimate();
				float[][] tmp = estimatedImage.toArray2d();
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						tmp[i][j] = img.getFloat(i, j);
					}
				}

			}
		}
		distFieldImage = dist.solve(distFieldImage, maxLayers + 0.5);
		distFieldImage.setName(image.getName() + "_levelset");
		distFieldImage.setHeader(image.getHeader());
		markCompleted();
		return distFieldImage;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContour2D#solveVerbose(edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat)
	 */
	public ImageDataFloat solveVerbose(ImageDataFloat initialLevelSet) {
		double dice = 0;
		posToNegCount = negToPosCount = 0;
		initNarrowBand(initialLevelSet);
		verboseDistFieldImage = new ImageDataFloat(rows, cols, outerIterations);
		verboseEstimatedImage = new ImageDataFloat(rows, cols, outerIterations);
		verboseDistFieldImage.setName(image.getName() + "_levelset_verbose");
		verboseEstimatedImage.setName(image.getName() + "_estimated_verbose");
		estimatedImage = new ImageDataFloat(rows, cols);
		estimatedImage.setName(image.getName() + "_estimated");
		float[][][] verboseDistFieldMat = verboseDistFieldImage.toArray3d();
		int outerIter = 0;
		for (ActiveContourForce2D force : forces) {
			if (force instanceof AppearanceForce2D) {
				((AppearanceForce2D) force).setLevelSet(distField);
			}
		}
		setTotalUnits(outerIterations);
		for (outerIter = 0; outerIter < outerIterations; outerIter++) {
			// Update forces
			System.out.println("DICE " + dice);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					distField[i][j] = verboseDistFieldMat[i][j][outerIter] = maxLayers
							* Math.signum(distField[i][j]);
				}
			}
			for (GridPoint2D pt : activePoints) {
				distField[pt.x][pt.y] = verboseDistFieldMat[pt.x][pt.y][outerIter] = pt.value;
			}
			for (int k = 0; k < maxLayers; k++) {
				for (GridPoint2D pt : insidePoints[k]) {
					distField[pt.x][pt.y] = verboseDistFieldMat[pt.x][pt.y][outerIter] = pt.value;
				}
				for (GridPoint2D pt : outsidePoints[k]) {
					distField[pt.x][pt.y] = verboseDistFieldMat[pt.x][pt.y][outerIter] = pt.value;
				}
			}
			for (ActiveContourForce2D force : forces) {
				force.update();
				if (force instanceof AppearanceForce2D) {
					ImageDataFloat img = ((AppearanceForce2D) force)
							.getImageEstimate();
					float[][][] tmp = verboseEstimatedImage.toArray3d();
					for (int i = 0; i < rows; i++) {
						for (int j = 0; j < cols; j++) {
							tmp[i][j][outerIter] = img.getFloat(i, j);
						}
					}

				}
			}
			if (dice >= diceThreshold) {
				break;
			}
			for (int innerIter = 0; innerIter < innerIterations; innerIter++) {
				updateNarrowBand();
			}
			dice = 2
					* (insideCount - negToPosCount)
					/ (double) (2 * insideCount + posToNegCount - negToPosCount);
			insideCount = insideCount + posToNegCount - negToPosCount;
			posToNegCount = negToPosCount = 0;
			setLabel(String.format("Dice Coeff: %1.4f", dice));
			incrementCompletedUnits();
		}
		// Rebuild narrow-band
		DistanceField2D dist = new DistanceField2D();
		for (GridPoint2D pt : activePoints) {
			distField[pt.x][pt.y] = pt.value;
		}
		for (int k = 0; k < maxLayers; k++) {
			for (GridPoint2D pt : insidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
			for (GridPoint2D pt : outsidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
		}
		for (ActiveContourForce2D force : forces) {
			force.update();
			if (force instanceof AppearanceForce2D) {
				ImageDataFloat img = ((AppearanceForce2D) force)
						.getImageEstimate();
				float[][] tmp = estimatedImage.toArray2d();
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						tmp[i][j] = img.getFloat(i, j);
					}
				}

			}
		}
		distFieldImage = dist.solve(distFieldImage, maxLayers + 0.5);
		distFieldImage.setName(image.getName() + "_levelset");
		distFieldImage.setHeader(image.getHeader());
		distField = distFieldImage.toArray2d();
		markCompleted();
		return distFieldImage;
	}
}
