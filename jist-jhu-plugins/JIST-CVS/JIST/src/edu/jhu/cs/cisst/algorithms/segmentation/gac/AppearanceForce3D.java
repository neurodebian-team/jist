/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * The Class AppearanceForce2D uses a model to estimate the regional appearance
 * of the foreground and background.
 */
public abstract class AppearanceForce3D extends PressureForce3D {

	/**
	 * The Enum Heaviside.
	 */
	public enum Heaviside {

		/** The ARCTAN. */
		ARCTAN,
		/** The BINARY. */
		BINARY,
		/** The SIN. */
		SIN,
		/** The SIGMOID. */
		SIGMOID
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.4 $");
	}

	/**
	 * Heaviside.
	 * 
	 * @param val
	 *            the val
	 * @param fuzziness
	 *            the fuzziness
	 * @param heavy
	 *            the heavy
	 * 
	 * @return the double
	 */
	public static double heaviside(double val, double fuzziness, Heaviside heavy) {
		double invPI = 1.0 / Math.PI;
		double invFuzziness = 1.0 / fuzziness;
		switch (heavy) {
		case ARCTAN:
			return (0.5 + invPI * Math.atan(invFuzziness * val));
		case BINARY:
			return (val > 0) ? 1 : 0;
		case SIGMOID:
			double exp = Math.exp(-val / fuzziness);
			return ((1 - exp) / (1 + exp));
		case SIN:
			if (val > fuzziness) {
				return 1;
			} else if (val < -fuzziness) {
				return 0;
			} else {
				return (0.5 * (1 + val * invFuzziness + invPI
						* Math.sin(invFuzziness * val * Math.PI)));
			}
		default:
			return (val > 0) ? 1 : 0;

		}
	}

	/**
	 * Heaviside derivative.
	 * 
	 * @param val
	 *            the val
	 * @param fuzziness
	 *            the fuzziness
	 * @param heavy
	 *            the heavy
	 * 
	 * @return the double
	 */
	public static double heavisideDerivative(double val, double fuzziness,
			Heaviside heavy) {
		double invPI = 1.0 / Math.PI;
		double invFuzziness = 1.0 / fuzziness;
		switch (heavy) {
		case ARCTAN:
			return (invPI * fuzziness / (fuzziness * fuzziness + val * val));
		case BINARY:
			return (val == 0) ? 1 : 0;
		case SIGMOID:
			double exp = Math.exp(-val / fuzziness);
			return 2 * invFuzziness * exp / ((1 + exp) * (1 + exp));
		case SIN:
			if (val > fuzziness) {
				return 0;
			} else if (val < -fuzziness) {
				return 0;
			} else {
				return (0.5 * invFuzziness * (1 + Math.cos(invFuzziness * val
						* Math.PI)));
			}
		default:
			return (val == 0) ? 1 : 0;

		}
	}

	/** The heaviside fuzziness. */
	protected double fuzziness = 1;

	/** The bg weight. */
	protected double bgWeight = 1;

	/** The fg weight. */
	protected double fgWeight = 1;

	/** The max. */
	protected double min = 0, max = 0;

	/** The normalize scale. */
	protected double normalizeScale = 1;

	/** The intensity image. */
	protected float[][][] intensityImage;

	/** The normalize intensities. */
	protected boolean normalizeIntensities = false;

	/** The rows. */
	protected int rows;

	/** The cols. */
	protected int cols;

	/** The image. */
	protected ImageDataFloat image;

	/** The image estimate. */
	protected ImageDataFloat imageEstimate;

	/** The levelset. */
	protected float[][][] levelset;

	/** The Constant heavisideNames. */
	public static final String[] heavisideNames = new String[] { "Arc Tan",
			"Binary", "Sin", "Sigmoid" };;

	/** The heaviside. */
	protected Heaviside heaviside = Heaviside.ARCTAN;

	public AppearanceForce3D() {
		super();
	}

	/**
	 * Instantiates a new appearance force.
	 * 
	 * @param image
	 *            the image
	 */
	public AppearanceForce3D(ImageDataFloat image, boolean normalizeIntensities) {
		super();
		double val;
		min = 1E10;
		max = -1E10;
		this.image = image;
		this.rows = image.getRows();
		this.cols = image.getCols();
		this.slices = image.getSlices();
		imageEstimate = new ImageDataFloat(rows, cols, slices);
		imageEstimate.setName(image.getName() + "_estimate");
		pressureForce = new float[rows][cols][slices];
		this.normalizeIntensities = normalizeIntensities;
		if (normalizeIntensities) {
			intensityImage = new float[rows][cols][slices];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						val = intensityImage[i][j][k] = image.getFloat(i, j, k);
						min = Math.min(val, min);
						max = Math.max(val, max);
					}
				}
			}
			normalizeScale = (Math.abs(max - min) > 1E-6) ? 1.0 / (max - min)
					: 1;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						intensityImage[i][j][k] = (float) ((intensityImage[i][j][k] - min) * normalizeScale);
					}
				}
			}
		} else {
			intensityImage = image.toArray3d();
		}
	}

	/**
	 * Gets the heaviside value.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * @param k
	 *            the k
	 * 
	 * 
	 * @return the heaviside value
	 */
	protected double getHeavisideValue(int i, int j, int k) {
		return heaviside(getLevelsetValue(i, j, k), fuzziness, heaviside);
	}

	/**
	 * Gets the image estimate.
	 * 
	 * @return the image estimate
	 */
	public ImageDataFloat getImageEstimate() {
		return imageEstimate;
	}

	/**
	 * Gets the index.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * @param k
	 *            the k
	 * 
	 * @return the index
	 */
	protected int getIndex(int i, int j, int k) {
		return (i * cols * slices) + (j * slices) + k;
	}

	/**
	 * Gets the levelset value.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * @param k
	 *            the k
	 * 
	 * 
	 * @return the levelset value
	 */
	protected double getLevelsetValue(int i, int j, int k) {
		return levelset[i][j][k];
	}

	/**
	 * Sets the weight.
	 * 
	 * @param bgweight
	 *            the bgweight
	 */
	public void setBackgroundWeight(double bgweight) {
		this.bgWeight = bgweight;
	}

	/**
	 * Sets the weight.
	 * 
	 * @param fgweight
	 *            the fgweight
	 */
	public void setForegroundWeight(double fgweight) {
		this.fgWeight = fgweight;
	}

	/**
	 * Sets the standard deviation.
	 * 
	 * @param fuzziness
	 *            the fuzziness
	 */
	public void setFuzziness(double fuzziness) {
		this.fuzziness = fuzziness;
	}

	/**
	 * Sets the heaviside.
	 * 
	 * @param heavy
	 *            the new heaviside
	 */
	public void setHeaviside(Heaviside heavy) {
		this.heaviside = heavy;
	}

	/**
	 * Sets the level set.
	 * 
	 * @param levelset
	 *            the new level set
	 */
	public void setLevelSet(float[][][] levelset) {
		this.levelset = levelset;
	}

}
