/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.solver;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * A helper class of class SolverThroughSmoother that helps parallelizing method SolverThroughSmoother.solve().
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class SolveThroughSmootherParallelizer extends Parallelizer {
     
     /**
      * construct.
      * 
      * @param solver the solver
      * @param gOld the g old
      * @param gNew the g new
      */
     public SolveThroughSmootherParallelizer(SolverThroughSmoother solver, ConstBoundaryGrid gOld, 
                                             ConstBoundaryGrid gNew) {
          
          this.solver = solver;
          this.gOld   = gOld;
          this.gNew   = gNew;
          
          start();
     }
     
     /**
      * re-route.
      * 
      * @param myNum the my num
      * @param totalNum the total num
      */
     public void runParallel(int myNum, int totalNum) {
          solver.solveProper(gOld,gNew,myNum,totalNum);
     }
     
     /** The solver. */
     private SolverThroughSmoother solver;
     
     /** The g old. */
     private ConstBoundaryGrid     gOld;
     
     /** The g new. */
     private ConstBoundaryGrid     gNew;
}
