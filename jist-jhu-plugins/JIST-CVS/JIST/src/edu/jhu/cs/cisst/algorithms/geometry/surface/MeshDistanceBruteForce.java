/*
 * 
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import javax.vecmath.Point3f;

// TODO: Auto-generated Javadoc
/**
 * The Class MeshDistanceBruteForce computes the distance between a point and
 * surface using a linear search.
 * 
 * @author Kel and Blake
 */
public class MeshDistanceBruteForce extends AbstractMeshDistance {
	
	/** The last closest point. */
	protected Point3f lastClosestPoint = null;
	
	/** The last closest triangle. */
	protected MeshTriangle lastClosestTriangle = null;

	/**
	 * Instantiates a new mesh distance brute force.
	 * 
	 * @param mesh the mesh
	 */
	public MeshDistanceBruteForce(TriangleMesh mesh) {
		super(mesh);
		mesh.updateMeshTriangles();
	}

	/**
	 * Get distance from point to surface.
	 * 
	 * @param p the point
	 * 
	 * @return the distance
	 * 
	 * @see cis.registration.point.AbstractMeshDistance#distance(javax.vecmath.Point3f)
	 */
	public double distance(Point3f p) {

		double minDist = MeshBBox.MAX_VALUE;
		double d = 0;

		for (MeshTriangle tri : mesh.getTriangles()) {
			if ((d = tri.distance(p)) < minDist) {
				lastClosestTriangle = tri;
				lastClosestPoint = tri.getLastClosestPoint();
				minDist = d;
			}
		}
		return minDist;
	}

	/**
	 * Gets the last closest point.
	 * 
	 * @return the last closest point
	 * 
	 * @see cis.registration.point.AbstractMeshDistance#getLastClosestPoint()
	 */
	public Point3f getLastClosestPoint() {
		return lastClosestPoint;
	}

	/**
	 * Gets the last closest triangle.
	 * 
	 * @return the last closest triangle
	 * 
	 * @see cis.registration.point.AbstractMeshDistance#getLastClosestTriangle()
	 */
	public MeshTriangle getLastClosestTriangle() {
		return lastClosestTriangle;
	}

}
