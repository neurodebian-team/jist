/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.pde;
import  java.util.*;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.NoBoundaryGrid;
/**
 * An abstract class that has the same public interface as class PDE but adds caching for improved performance of
 * classes that dervie from it.
 * <p>
 * Caching is supported for the right hand side (source term) f and for a representation of the matrix A which is
 * allmost always needed by the methods evaluateLHS() and evaluate().
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public abstract class CachingPDE implements PDE {
     
     /**
      * implements method from PDE.
      * <p>
      * Sampling the right hand side f of a PDE for a given grid size (as this method does) is usually a time-consuming
      * task. This implementation improves performance for those cases where sampleRHS() is called more than once for a
      * specific grid size because it caches the right hand side (source term) f for every grid size. This may lead to
      * space inefficiencies in general but not in the context of the (Full) Multigrid algorithm, where the right hand
      * side is needed at all grid levels over and over again.
      * <p>
      * The actual calculation of the right hand side f is re-routed to method actuallySampleRHS().
      * 
      * @param sx the sx
      * @param sy the sy
      * @param sz the sz
      * @param level the level
      * 
      * @return the no boundary grid
      * 
      * @see CachingPDE#actuallySampleRHS
      * @see PDE#sampleRHS
      */
     public NoBoundaryGrid sampleRHS(int sx,int sy,int sz,int level) {
          
          Integer        s = new Integer(level);
          NoBoundaryGrid f = (NoBoundaryGrid) rhsCache.get(s);
          if (f == null) {
               // actually sample the right hand side for this grid size and store it in the cache
               f        = actuallySampleRHS(sx,sy,sz,level);
               Object r = rhsCache.put(s,f);
               //Contract.assert(r == null,"don't know if rhsCache has a cahed value for size " + size + " or not");
          }
          return f;
     }
     
     /**
      * sample the right hand side f on a cubic grid of the given size.
      * <p>
      * It is not necessary to know the value of f at the boundary of the grid, so the return type of this method is
      * of type NoBoundaryGrid. But note that the size of the grid passed to this method includes (as always) the
      * boundary. E.g., a 65x65x65 grid would be described by a size of 65 and its interior values (the one to be set by
      * this method) would comprise the grid elements (1,1,1) to (63,63,63). The boundary elements (where at least one
      * index is either 0 or 64) are non-existent in a NoBoundaryGrid.
      * 
      * @param sx the sx
      * @param sy the sy
      * @param sz the sz
      * @param level the level
      * 
      * @return the right hand side (source term) f sampled on a cubic grid of the given size
      */
     protected abstract NoBoundaryGrid actuallySampleRHS(int sx,int sy,int sz,int level);
     
     /**
      * a caching frontend to calculating a representation of the matrix A of the PDE for a specific grid size.
      * <p>
      * Methods evaluateLHS() and evaluate() are supposed to call this method if they need a representation of the
      * matrix A for doing their job for a grid of a certain size.
      * <p>
      * The actual calculation of the representation of the matrix A is re-routed to method actuallySampleMatrix().
      * 
      * @param sx the sx
      * @param sy the sy
      * @param sz the sz
      * @param level the level
      * 
      * @return a representation of matrix A sampled on a grid of the given size
      * 
      * @see CachingPDE#actuallySampleMatrix
      */
     protected Object sampleMatrix(int sx,int sy,int sz,int level) {
          Integer s      = new Integer(level);
          Object  matrix = matrixCache.get(s);
          if (matrix == null) {
               // actually sample the matrix for this grid size and store it in the cache
               matrix   = actuallySampleMatrix(sx,sy,sz,level);
               Object r = matrixCache.put(s,matrix);
               //Contract.assert(r == null,"don't know if matrixCache has a cahed value for size " + size + " or not");
          }
          return matrix;
     }
     
     /**
      * sample the matrix A in a way that it can later be used by methods evaluate() and evaluateLHS() on a grid of
      * the given size.
      * <p>
      * The implementer of this method is entirely free in choosing any suitable representation for the matrix A as long
      * as the methods evaluate() and evaluateLHS() are able to fulfill their job when passed the output of this method
      * later in the course of the execution of the FMG algorithm.
      * 
      * @param sx the sx
      * @param sy the sy
      * @param sz the sz
      * @param level the level
      * 
      * @return a representation of matrix A sampled on a grid of the given size
      */
     protected abstract Object actuallySampleMatrix(int sx,int sy,int sz,int level);
     
     /** The rhs cache. */
     private Dictionary rhsCache    = new Hashtable();
     
     /** The matrix cache. */
     private Dictionary matrixCache = new Hashtable();
}
