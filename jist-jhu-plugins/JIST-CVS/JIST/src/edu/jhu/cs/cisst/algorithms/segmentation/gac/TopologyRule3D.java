/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

// TODO: Auto-generated Javadoc
/**
 * The Class TopologyRule3D.
 */
public abstract class TopologyRule3D {

	/**
	 * The topology rule.
	 */
	public enum Rule {

		// CONNECT_6_18,
		// CONNECT_18_6,
		CONNECT_6_26,
		// CONNECT_26_6
	};

	/** The rule. */
	protected Rule rule;

	/** The grid points. */
	protected GridPoint3D[][][] gridPoints;

	/** The rows. */
	protected int rows;

	/** The columns. */
	protected int cols;

	/** The slices. */
	protected int slices;

	/**
	 * Instantiates a new topology rule 3d.
	 * 
	 * @param rule
	 *            the rule
	 */
	public TopologyRule3D(Rule rule) {
		this.rule = rule;
	}

	/**
	 * Sets the grid points.
	 * 
	 * @param gridPoints
	 *            the new grid points
	 */
	public void setGridPoints(GridPoint3D[][][] gridPoints) {
		this.gridPoints = gridPoints;
		this.rows = gridPoints.length;
		this.cols = gridPoints[0].length;
		this.slices = gridPoints[0][0].length;
	}

	/**
	 * Gets the grid point.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * @param k
	 *            the k
	 * @return the grid point
	 */
	public GridPoint3D getGridPoint(int i, int j, int k) {
		if (i < rows && j < cols && k < slices && i >= 0 && j >= 0 && k >= 0) {
			return gridPoints[i][j][k];
		} else {
			return null;
		}
	}

	/**
	 * Apply rule.
	 * 
	 * @param pt
	 *            the pt
	 * @param value
	 *            the value
	 * 
	 * @return the double
	 */
	public abstract double applyRule(GridPoint3D pt, double value);
}
