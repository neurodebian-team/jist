package edu.jhu.cs.cisst.algorithms.imageproc;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import gov.nih.mipav.model.algorithms.AlgorithmBase;
import gov.nih.mipav.model.algorithms.AlgorithmEdgeNMSuppression;
import gov.nih.mipav.model.algorithms.AlgorithmInterface;
import gov.nih.mipav.model.algorithms.filters.AlgorithmNMSuppression;
import gov.nih.mipav.model.structures.ModelImage;

/**
 * The Class ZeroCrossings2D computes Zero-crossings of the Laplacian with non-maximal suppression.
 */
public class ZeroCrossings2D extends AbstractCalculation {
	
	/** The sigma x. */
	protected float sigmaX;
	/** The sigma y. */
	protected float sigmaY;

	/**
	 * Instantiates a new zero crossings2 d.
	 * 
	 * @param sigmaX the sigma x
	 * @param sigmaY the sigma y
	 */
	public ZeroCrossings2D(float sigmaX, float sigmaY) {
		this.sigmaX = sigmaX;
		this.sigmaY = sigmaY;
		setLabel("Zero-Crossings");
	}

	/**
	 * Solve.
	 * 
	 * @param img the img
	 * 
	 * @return the image data
	 */
	public ImageData solve(ImageData img) {
		ModelImage inputImage = img.getModelImageCopy();
		inputImage.calcMinMax();
		ModelImage resultImage = new ModelImage(7, new int[] { img.getRows(),
				img.getCols() }, " EdgeNMSup");
		AlgorithmEdgeNMSuppression nmSupAlgo = new AlgorithmEdgeNMSuppression(
				resultImage, inputImage, new float[] { sigmaX, sigmaY }, true,
				false);
		nmSupAlgo.run();
		ImageDataMipav resultImg = new ImageDataMipav(nmSupAlgo.getZeroXMask());
		resultImg.setName(img.getName() + "_zerox");
		return resultImg;
	}
}
