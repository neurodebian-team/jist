/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */

package edu.jhu.cs.cisst.algorithms.util.phantom;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

// TODO: Auto-generated Javadoc
/**
 * The Class PhantomMetasphere.
 */
public class PhantomMetasphere extends PhantomSimulator3D{
	
	/** The frequency. */
	protected double frequency;
	
	/** The min amplitude. */
	protected double minAmplitude;
	
	/** The max amplitude. */
	protected double maxAmplitude;
	
	/** The center. */
	protected Point3d center;
	
	/**
	 * Sets the center.
	 * 
	 * @param center the new center
	 */
	public void setCenter(Point3d center) {
		this.center = center;
	}
	
	/**
	 * Sets the frequency.
	 * 
	 * @param frequency the new frequency
	 */
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}
	
	/**
	 * Sets the min amplitude.
	 * 
	 * @param minAmplitude the new min amplitude
	 */
	public void setMinAmplitude(double minAmplitude) {
		this.minAmplitude = minAmplitude;
	}
	
	/**
	 * Sets the max amplitude.
	 * 
	 * @param maxAmplitude the new max amplitude
	 */
	public void setMaxAmplitude(double maxAmplitude) {
		this.maxAmplitude = maxAmplitude;
	}
	
	/**
	 * Instantiates a new phantom metasphere.
	 * 
	 * @param dims the dims
	 */
	public PhantomMetasphere(Point3i dims) {
		super(dims);
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.cs.cisst.algorithms.util.phantom.PhantomSimulator3D#solve()
	 */
	public void solve(){
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double x = (2 * i / (double) rows - 1);
					double y = (2 * j / (double) cols - 1);
					double z = (2 * k / (double) slices - 1);
										 
					double rXY = Math.sqrt((x - center.x) * (x - center.x)
							+ (y - center.y) * (y - center.y));
					

					double rXYZ = Math.sqrt((x - center.x) * (x - center.x)
							+ (y - center.y) * (y - center.y)
							+(z - center.z) * (z - center.z));
					double alpha=Math.atan2(y, x);
					double r1 = Math.sqrt(maxAmplitude*maxAmplitude-((z - center.z) * (z - center.z)))/maxAmplitude;
					double beta=Math.atan2(z, rXY);
					double d=(minAmplitude+(maxAmplitude-minAmplitude)*(Math.cos(alpha*frequency)));
					double r2=(minAmplitude+(maxAmplitude-minAmplitude)*(Math.cos(2*beta*frequency)));
					levelset.set(i,j,k,0.5*((rXY-r1*d)+(rXYZ-r2)));
				}
			}
		}
		levelset.setName("metasphere_level");
		image.setName("metasphere");
		finish();
	}
}
