/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.grid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * An implementation of BoundaryGrid where the boundary elements are periodic images of interior grid elements.
 *
 * The exact definition of periodic boundary conditions in this context is that any index equal to 0 will
 * be translated to (size() - 2) and any index equal to (size() - 1) will be translated to 1.
 * This translation is done independently for each index and the resulting interior element is returned as
 * the requested boundary element.
 *
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class PeriodicBoundaryGrid extends BoundaryGrid {
     /**
      * construct from size and initial value for all elements in the interior.
      *
      * @param sx the dimension in X
      * @param sy the dimension in Y
      * @param sz the dimension in Z
      * @param level the resolution level
      * @param interiorValue the value to which all interior grid elements will be set
      */
     public PeriodicBoundaryGrid(int sx,int sy,int sz, int level,double interiorValue) {
          super(sx,sy,sz,level,interiorValue);
     }

     /**
      * implements method from ConstGrid.
      *
      * @see ConstGrid#newInstance
      */
     public Grid newInstance(int sx,int sy,int sz,  int level,double value) {
          return new PeriodicBoundaryGrid(sx,sy,sz,level,value);
     }

     /**
      * implements method from Grid.
      *
      * @see Grid#getBoundary
      */
     protected double getBoundary(int x, int y, int z) {
          Contract.pre(isBoundary(x,y,z),"grid element must be part of the boundary");

          if      (x == 0)       x = sx - 2;
          else if (x == (sx - 1)) x = 1;
          if      (y == 0)       y = sy - 2;
          else if (y == (sy - 1)) y = 1;
          if      (z == 0)       z = sz - 2;
          else if (z == (sz - 1)) z = 1;
          
          return g[x][y][z];
     }
}
