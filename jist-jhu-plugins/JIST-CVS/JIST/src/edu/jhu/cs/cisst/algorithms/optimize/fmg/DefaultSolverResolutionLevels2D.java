/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstGrid;

/**
 * The Class DefaultSolverResolutionLevels2D describes a set of solver resolutions.
 */
public class DefaultSolverResolutionLevels2D implements SolverResolutionLevels{
	
	/** The resolutions. */
	protected int[][] resolutions;
	
	/** The grid sizes. */
	protected double[] gridSizes;
	
	/**
	 * Instantiates a new solver resolutions.
	 * 
	 * @param grid the grid
	 * @param minSize the min grid size
	 * @param numResolutions the number of resolutions
	 */
	public DefaultSolverResolutionLevels2D(ConstGrid grid,int minSize,int numResolutions){
		int sx=grid.getRows();
		int sy=grid.getCols();
		int sz=grid.getSlices();
		int maxSize=Math.max(Math.max(sx,sy),sz);
		double logMin = Math.log10(minSize);
		double logMax = Math.log10(maxSize);
		resolutions=new int[numResolutions][3];
		gridSizes=new double[numResolutions];
		for(int level=0;level<numResolutions;level++){
			//Sample in log scale
			int size=Math.min(maxSize,Math.max(minSize,(int)Math.pow(10, logMin * (1 - level/(double)(numResolutions-1))+ logMax * level/(double)(numResolutions-1))));
			if(level==numResolutions-1){
				int[] res=new int[]{sx,sy,sz};
				gridSizes[level]=maxSize/(double)Math.max(Math.max(res[0], res[1]),res[2]);
				resolutions[level]=res;
			} else {
				int[] res=new int[]{Math.min(sx,size),Math.min(sy,size),sz};
				gridSizes[level]=maxSize/(double)Math.max(Math.max(res[0], res[1]),res[2]);
				resolutions[level]=res;
			}
		}
	}
	
	/**
	 * @see edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels#getResolution(int)
	 */
	public int[] getResolution(int i) {
		return resolutions[i];
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels#getResolutionCount()
	 */
	public int getResolutionCount() {
		return resolutions.length;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels#getResolutions()
	 */
	public int[][] getResolutions() {
		return resolutions;
	}
	
	/**
	 * @see edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels#getGridSpacing(int)
	 */
	public double getGridSpacing(int i) {
		return gridSizes[i];
	}

}
