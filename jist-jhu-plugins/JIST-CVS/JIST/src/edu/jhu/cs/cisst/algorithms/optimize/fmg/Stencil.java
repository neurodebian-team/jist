/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg;

// TODO: Auto-generated Javadoc
/**
 * A Stencil contains 27 doubles and can be used to represent the relationship between a grid element and its
 * 26 nearest neighbours in 3D.
 * 
 * Positions are identified by integer indices relative to the central element, which consequently has the
 * position (0,0,0).
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public interface Stencil {
     
     /**
      * gets the element at the specified position.
      * 
      * @param x the index in the x-direction of the desired element (-1 <= x <= 1)
      * @param y the index in the y-direction of the desired element (-1 <= y <= 1)
      * @param z the index in the z-direction of the desired element (-1 <= z <= 1)
      * 
      * @return the element at the specified position
      */
     double get(int x, int y, int z);
}
