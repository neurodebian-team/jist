/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.geometry.surface;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
// TODO: Auto-generated Javadoc

/**
 * The class AbstractMeshDistance computes the distance between a point and surface.
 * 
 * @author Kel and Blake
 */
public abstract class AbstractMeshDistance extends AbstractCalculation{
	
	/** The mesh. */
	protected TriangleMesh mesh;
	
	/**
	 * Instantiates a new AbstractMeshDistance.
	 * 
	 * @param mesh the surface
	 */
	public AbstractMeshDistance(TriangleMesh mesh){
		this.mesh=mesh;
		setLabel("Mesh Distance");
	}
	
	/**
	 * Get distance from point to surface.
	 * 
	 * @param p the point
	 * 
	 * @return the distance
	 */
	public abstract double distance(Point3f p);
	
	/**
	 * Gets the last closest point.
	 * 
	 * @return the last closest point
	 */
	public abstract Point3f getLastClosestPoint();

	/**
	 * Gets the last closest triangle.
	 * 
	 * @return the last closest triangle
	 */
	public abstract MeshTriangle getLastClosestTriangle();

	
}
