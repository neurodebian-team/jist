/*
 *
 */
package edu.jhu.cs.cisst.plugins.utilities.logic;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmFileJoin extends ProcessingAlgorithm{
	private ParamFile fileTrue;
	private ParamFile fileFalse;
	private ParamFile outFile;
	private ParamBoolean branchParam;
	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(fileTrue=new ParamFile("File for True"));
		inputParams.add(fileFalse=new ParamFile("File for False"));
		fileTrue.setMandatory(false);
		fileFalse.setMandatory(false);
		inputParams.add(branchParam=new ParamBoolean("Assertion",true));
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("File Join");
		inputParams.setName("file_join");
		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription("Joins two pipelines and chooses which input to use for subsequent modules. This module can only join two pipelines if BOTH have sucessfully executed.");
		info.add(CommonAuthors.blakeLucas);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outFile=new ParamFile("File"));
	}
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		if(branchParam.getValue()){
			outFile.setValue(fileTrue.getValue());
		} else {
			outFile.setValue(fileFalse.getValue());
		}
	}
}
