/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel;

/**
 * An interface to those classes that supply a method for parallel multithreaded execution.
 * <p>
 * When a compute-intensive sequential algorithm is to be parallelized for execution on a SMP, it often results in a
 * very similar looking parallel algorithm that should be executed in one thread for each processor and will work on a
 * different portion of the total problem in different threads.
 * This interface is intended for this kind of parallel algorithms.
 * <p>
 * The total number of threads put to work on this problem and the number of the current thread is supplied to the
 * method of this interface, so that the parallel algorithm can easily determine on which portion of the total problem
 * it should work.
 * 
 * @see Parallelizer
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */

public interface Parallelizable {
     
     /**
      * the method that will be run by several threads in parallel.
      * <p>
      * The total number of threads that work on the problem by calling this method is supplied, as well as the number
      * of the current thread calling the method.
      * 
      * @param myNum    supplies the number of the thread that called the method (0 <= myNum < totalNum)
      * @param totalNum supplies the total number of threads that call this method
      */
     public void runParallel(int myNum, int totalNum);
}
