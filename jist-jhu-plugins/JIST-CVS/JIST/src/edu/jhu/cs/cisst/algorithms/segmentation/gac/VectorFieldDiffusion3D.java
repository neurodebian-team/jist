/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import edu.jhu.cs.cisst.algorithms.optimize.fmg.SolverResolutionLevels;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.NoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.CachingPDE;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE;
import no.uib.cipr.matrix.Vector;
import no.uib.cipr.matrix.sparse.AbstractIterativeSolver;
import no.uib.cipr.matrix.sparse.BiCG;
import no.uib.cipr.matrix.sparse.CG;
import no.uib.cipr.matrix.sparse.DefaultIterationMonitor;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import no.uib.cipr.matrix.sparse.IterationReporter;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.SparseVector;

// TODO: Auto-generated Javadoc
/**
 * The Class VectorFieldDiffusion3D describes the vector field diffusion PDE for
 * use by the multi-grid solver.
 */
public class VectorFieldDiffusion3D extends CachingPDE implements
		IterationReporter {

	/** The gradient pyramid. */
	protected float[][][][][] gradientPyramid;

	/**
	 * The Enum Dimension.
	 */
	public enum Dimension {

		/** The X dimension. */
		X,
		/** The Y dimension. */
		Y,
		/** The Z dimension. */
		Z
	};

	/** The dimension. */
	protected Dimension dim;

	/** The resolution levels. */
	protected SolverResolutionLevels levels;

	/** The diffusion weight. */
	protected double diffusionWeight;

	/** The maximum iterations. */
	protected int maxIterations = 1000;

	/**
	 * Instantiates a new PDE for vector field diffusion.
	 * 
	 * @param diffusionWeight
	 *            the diffusion weight
	 * @param dim
	 *            the dimension
	 * @param gradientPyramid
	 *            the gradient pyramid
	 * @param levels
	 *            the resolution levels
	 * @param maxIterations
	 *            the max iterations
	 */
	public VectorFieldDiffusion3D(double diffusionWeight, Dimension dim,
			float[][][][][] gradientPyramid, SolverResolutionLevels levels,
			int maxIterations) {
		this.gradientPyramid = gradientPyramid;
		this.dim = dim;
		this.levels = levels;
		this.maxIterations = maxIterations;
		this.diffusionWeight = diffusionWeight;
	}

	/**
	 * implements method from PDE.
	 * 
	 * @param level
	 *            the level
	 * 
	 * @return the grid spacing
	 * 
	 * @see PDE#getGridSpacing
	 */
	public double getGridSpacing(int level) {
		return levels.getGridSpacing(level);
	}

	/**
	 * implements method from PDE.
	 * 
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param z
	 *            the z
	 * 
	 * @return the double
	 * 
	 * @see PDE#evaluateLHS
	 */
	public double evaluateLHS(ConstBoundaryGrid u, ConstNoBoundaryGrid f,
			int x, int y, int z) {
		int level = u.getLevel();
		double h = getGridSpacing(level);

		double dx = gradientPyramid[level][0][x][y][z] * h;
		double dy = gradientPyramid[level][1][x][y][z] * h;
		double dz = gradientPyramid[level][2][x][y][z] * h;
		double magSqr = dx * dx + dy * dy + dz * dz;
		return ((diffusionWeight / (h * h)) * (u.get(x + 1, y, z)
				+ u.get(x - 1, y, z) + u.get(x, y + 1, z) + u.get(x, y - 1, z)
				+ u.get(x, y, z + 1) + u.get(x, y, z - 1) - 6 * u.get(x, y, z)))
				- magSqr * u.get(x, y, z);

	}

	/**
	 * Gets the index.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 * @param k
	 *            the k
	 * @param cols
	 *            the cols
	 * @param slices
	 *            the slices
	 * 
	 * @return the index
	 */
	protected static final int getIndex(int i, int j, int k, int cols,
			int slices) {
		return (i * cols * slices) + (j * slices) + k;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.optimize.fmg.pde.PDE#solve(edu.jhu.cs.cisst
	 *      .algorithms.optimize.fmg.grid.ConstBoundaryGrid,
	 *      edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid)
	 */
	public BoundaryGrid solve(ConstBoundaryGrid u, ConstNoBoundaryGrid f) {
		int rows = u.getRows();
		int cols = u.getCols();
		int slices = u.getSlices();
		int N = rows * cols * slices;
		int level = u.getLevel();
		double h = getGridSpacing(level);
		FlexCompRowMatrix A = new FlexCompRowMatrix(N, N);
		SparseVector b = new SparseVector(N);
		SparseVector x = new SparseVector(N);
		int index = 0;
		double w = diffusionWeight / (h * h);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					double dx = gradientPyramid[level][0][i][j][k] * h;
					double dy = gradientPyramid[level][1][i][j][k] * h;
					double dz = gradientPyramid[level][2][i][j][k] * h;
					double magSqr = dx * dx + dy * dy + dz * dz;

					double centerWeight = 0;
					int v111 = getIndex(i, j, k, cols, slices);
					int v011 = getIndex(i - 1, j, k, cols, slices);
					int v211 = getIndex(i + 1, j, k, cols, slices);
					int v101 = getIndex(i, j - 1, k, cols, slices);
					int v121 = getIndex(i, j + 1, k, cols, slices);
					int v110 = getIndex(i, j, k - 1, cols, slices);
					int v112 = getIndex(i, j, k + 1, cols, slices);
					if (i > 0) {
						A.set(index, v011, diffusionWeight);
						centerWeight++;
					}
					if (i < rows - 1) {
						A.set(index, v211, diffusionWeight);
						centerWeight++;
					}
					if (j > 0) {
						A.set(index, v101, diffusionWeight);
						centerWeight++;
					}
					if (j < cols - 1) {
						A.set(index, v121, diffusionWeight);
						centerWeight++;
					}
					if (k > 0) {
						A.set(index, v110, diffusionWeight);
						centerWeight++;
					}
					if (k < slices - 1) {
						A.set(index, v112, diffusionWeight);
						centerWeight++;
					}
					A
							.set(index, v111, -centerWeight * diffusionWeight
									- magSqr);
					b.set(index, f.get(i, j, k));
					switch (dim) {
					case X:
						x.set(index, dx);
						break;
					case Y:
						x.set(index, dy);
						break;
					case Z:
						x.set(index, dz);
						break;
					}
					index++;
				}
			}
		}
		// Use conjugate gradient solver
		AbstractIterativeSolver solver = new BiCG(b);
		solver.getIterationMonitor().setIterationReporter(this);
		((DefaultIterationMonitor) solver.getIterationMonitor())
				.setMaxIterations(maxIterations);
		try {
			solver.solve(A, b, x);
		} catch (IterativeSolverNotConvergedException e) {
			System.err
					.println("Solver did not converge in "
							+ solver.getIterationMonitor().iterations()
							+ " iterations");
		}

		BoundaryGrid solution = (BoundaryGrid) u.newInstance(u.getRows(), u
				.getCols(), u.getSlices(), u.getLevel(), 0);
		index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					solution.set(i, j, k, x.get(index));
					index++;
				}
			}
		}
		return solution;

	}

	/**
	 * implements method from PDE.
	 * 
	 * @param u
	 *            the u
	 * @param f
	 *            the f
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param z
	 *            the z
	 * 
	 * @return the double
	 * 
	 * @see PDE#evaluate
	 */
	public double evaluate(ConstBoundaryGrid u, ConstNoBoundaryGrid f, int x,
			int y, int z) {

		int level = u.getLevel();
		double h = getGridSpacing(level);
		double val = u.get(x, y, z);
		double dx = gradientPyramid[level][0][x][y][z] * h;
		double dy = gradientPyramid[level][1][x][y][z] * h;
		double dz = gradientPyramid[level][2][x][y][z] * h;
		double magSqr = dx * dx + dy * dy + dz * dz;
		double denom = (6 * diffusionWeight + h * h * magSqr);
		val = (diffusionWeight
				* (u.get(x + 1, y, z) + u.get(x - 1, y, z) + u.get(x, y + 1, z)
						+ u.get(x, y - 1, z) + u.get(x, y, z - 1) + u.get(x, y,
						z + 1)) - f.get(x, y, z) * h * h)
				/ denom;
		return val;
	}

	/**
	 * implements method from CachingPDE.
	 * 
	 * @param sx
	 *            the sx
	 * @param sy
	 *            the sy
	 * @param sz
	 *            the sz
	 * @param level
	 *            the level
	 * 
	 * @return the no boundary grid
	 * 
	 * @see CachingPDE#actuallySampleRHS
	 */
	@Override
	protected NoBoundaryGrid actuallySampleRHS(int sx, int sy, int sz, int level) {
		NoBoundaryGrid p = new NoBoundaryGrid(sx, sy, sz, level, 0);
		double h = getGridSpacing(level);
		for (int i = 0; i < sx; i++) {
			for (int j = 0; j < sy; j++) {
				for (int k = 0; k < sz; k++) {
					double dx = gradientPyramid[level][0][i][j][k] * h;
					double dy = gradientPyramid[level][1][i][j][k] * h;
					double dz = gradientPyramid[level][2][i][j][k] * h;

					double magSqr = dx * dx + dy * dy + dz * dz;
					switch (dim) {
					case X:
						p.set(i, j, k, -dx * magSqr);
						break;
					case Y:
						p.set(i, j, k, -dy * magSqr);
						break;
					case Z:
						p.set(i, j, k, -dz * magSqr);
						break;
					}
				}
			}
		}
		return p;
	}

	/**
	 * implements method from CachingPDE.
	 * 
	 * @param sx
	 *            the sx
	 * @param sy
	 *            the sy
	 * @param sz
	 *            the sz
	 * @param level
	 *            the level
	 * 
	 * @return the object
	 * 
	 * @see CachingPDE#sampleMatrix
	 */
	@Override
	protected Object actuallySampleMatrix(int sx, int sy, int sz, int level) {
		return (new Double(getGridSpacing(level)));
	}

	/**
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double, int)
	 */
	public void monitor(double error, int iter) {

	}

	/**
	 * @see no.uib.cipr.matrix.sparse.IterationReporter#monitor(double,
	 *      no.uib.cipr.matrix.Vector, int)
	 */
	public void monitor(double error, Vector res, int iter) {
		if (iter % 100 == 0)
			System.out.println(iter + ") Residual:" + error);
	}

}
