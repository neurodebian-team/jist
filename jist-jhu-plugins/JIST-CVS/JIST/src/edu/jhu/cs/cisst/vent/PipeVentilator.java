/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.vent;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.PipeRunner;
import edu.jhu.ece.iacl.jist.utility.FileUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class PipeVentilator.
 */
public class PipeVentilator extends PipeRunner {

	/** The capture screenshot. */
	protected boolean captureScreenshot = false;

	/** The capture video. */
	protected boolean captureVideo = false;

	/** The pipe file. */
	protected File pipeFile = null;

	/** The ventilator. */
	protected static PipeVentilator ventilator = null;

	/** The is venting. */
	protected static boolean isVenting = false;

	protected File outputDirectory = null;

	public File getPipeFile() {
		return pipeFile;
	}

	protected File screenshotFile;
	protected File videoFile;

	public File getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * Instantiates a new pipe ventilator.
	 */
	public PipeVentilator() {
	}

	/**
	 * Gets the single instance of PipeVentilator.
	 * 
	 * @return single instance of PipeVentilator
	 */
	public static PipeVentilator getInstance() {
		if (ventilator == null) {
			ventilator = new PipeVentilator();
		}
		return ventilator;
	}

	/**
	 * Checks if is venting.
	 * 
	 * @return true, if is venting
	 */
	public static boolean isVenting() {
		return (ventilator != null);
	}

	/**
	 * Main method to execute algorithm specified by input parameters.
	 * 
	 * @param args
	 *            The first argument should be the input parameter file
	 */
	public static void main(String[] args) {
		File pipeFile = null;
		boolean captureScreenshot = false;
		boolean captureVideo = false;
		boolean showOutput = false;
		// Set relative or absolute path if one is specified
		for (String arg : args) {
			if (arg.equalsIgnoreCase("-screenshot")) {
				captureScreenshot = true;
			} else if (arg.equalsIgnoreCase("-video")) {
				captureVideo = true;
			} else if (arg.equalsIgnoreCase("-show")) {
				showOutput = true;
			} else {
				pipeFile = new File(args[0]);
			}
			// Run pipe file
			run(pipeFile, pipeFile.getParentFile(), showOutput,
					captureScreenshot, captureVideo);
			System.exit(0);
		}
		// Accept multiple parameters
	}

	/**
	 * Run.
	 * 
	 * @param pipeFile
	 *            the pipe file
	 * @param showOutput
	 *            the show output
	 * @param captureScreenshot
	 *            the capture screenshot
	 * @param captureVideo
	 *            the capture video
	 */
	public static void run(File pipeFile, File outputDirectory,
			boolean showOutput, boolean captureScreenshot, boolean captureVideo) {

		PipeVentilator harness = PipeVentilator.getInstance();
		harness.setCaptureScreenshot(captureScreenshot);
		harness.setCaptureVideo(captureVideo);
		harness.setPipeFile(pipeFile);
		harness.setOutputDirectory(outputDirectory);
		// Set relative or absolute path if one is specified
		// Redirect stdout and stderr to debug.out and debug.err
		String pipeName = FileReaderWriter.getFileName(pipeFile);
		File pipeDir = new File(pipeFile.getParentFile(), pipeName
				+ File.separator);
		if (!pipeDir.exists()) {
			pipeDir.mkdir();
		}
		File debugOut = new File(pipeDir, "debug.out");
		File debugErr = new File(pipeDir, "debug.err");
		// outWriter = new BufferedWriter(new OutputStreamWriter(
		// new BufferedOutputStream(System.out)));
		PrintStream[] streams = null;
		if (!showOutput) { // Bug fix to NITRC bug #3929
//			outWriter = new BufferedWriter(new OutputStreamWriter( //BROKEN
//					new BufferedOutputStream(System.out)));
			streams = FileUtil.redirect(debugOut, debugErr);
		}
		try {
			// Invoke plug-in
			harness.invoke(pipeFile, pipeDir);
			// Close true stdout stream
			// try {
			// outWriter.flush();
			// outWriter.close();
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
		} catch (Exception e) {
			e.printStackTrace();
			System.err.flush();
			System.out.flush();
			// if (!showOutput) {
			// for (PrintStream stream : streams) {
			// stream.flush();
			// stream.close();
			// }
			// }
		} finally {
			// Close redirected streams
			if (!showOutput) {
				for (PrintStream stream : streams) {
					stream.flush();
					stream.close();
				}
				FileUtil.restoreRedirect();
			}
		}
	}

	/**
	 * Checks if is capture screenshot.
	 * 
	 * @return true, if is capture screenshot
	 */
	public boolean isCaptureScreenshot() {
		return captureScreenshot;
	}

	/**
	 * Checks if is capture video.
	 * 
	 * @return true, if is capture video
	 */
	public boolean isCaptureVideo() {
		return captureVideo;
	}

	public void setCaptureScreenshot(boolean captureScreenshot) {
		this.captureScreenshot = captureScreenshot;
	}

	public void setCaptureVideo(boolean captureVideo) {
		this.captureVideo = captureVideo;
	}

	public void setPipeFile(File pipeFile) {
		this.pipeFile = pipeFile;
	}

	public File getScreenshotFile() {
		return screenshotFile;
	}

	public void setScreenshotFile(File screenshotFile) {
		this.screenshotFile = screenshotFile;
	}

	public File getVideoFile() {
		return videoFile;
	}

	public void setVideoFile(File videoFile) {
		this.videoFile = videoFile;
	}

	public void setOutputDirectory(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

}
