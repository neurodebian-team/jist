/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

/**
 * A helper class of class DampedJacobi that implements method DampedJacobi.smooth() in a parallel manner.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class DampedJacobiSmoothParallelizer extends Parallelizer {
     
     /**
      * construct.
      * 
      * @param dj the dj
      * @param g the g
      * @param u the u
      * @param f the f
      */
     public DampedJacobiSmoothParallelizer(DampedJacobi dj, BoundaryGrid g, ConstBoundaryGrid u,
                                           ConstNoBoundaryGrid f) {
          this.dj = dj;
          this.g  = g;
          this.u  = u;
          this.f  = f;
          
          start();
     }
     
     /**
      * re-route.
      * 
      * @param myNum the my num
      * @param totalNum the total num
      */
     public void runParallel(int myNum, int totalNum) {
          dj.smoothProper(g,u,f,myNum,totalNum);
     }
     
     /** The damped jacobi. */
     private DampedJacobi        dj;
     
     /** The grid. */
     private BoundaryGrid        g;
     
     /** The u. */
     private ConstBoundaryGrid   u;
     
     /** The f. */
     private ConstNoBoundaryGrid f;
}
