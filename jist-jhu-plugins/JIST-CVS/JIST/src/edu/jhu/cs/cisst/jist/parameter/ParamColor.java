/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.jist.parameter;

import java.awt.Color;
import java.net.MalformedURLException;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


import edu.jhu.cs.cisst.jist.pipeline.factory.ParamColorFactory;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterValueException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.utility.JistXMLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ParamColor.
 */
public class ParamColor extends ParamModel<Color> {
	
	/** The color. */
	protected Color color;

	public boolean xmlEncodeParam(Document document, Element parent) {
		super.xmlEncodeParam(document, parent);		 
		Element em;				
		em = document.createElement("color");		
		 em.appendChild(document.createTextNode(color.toString()+""));
		 parent.appendChild(em);
		return true;
	}
	
	public void xmlDecodeParam(Document document, Element parent) {
		super.xmlDecodeParam(document, parent);
		color = Color.decode(JistXMLUtil.xmlReadTag(parent, "color"));		
	}
	/**
	 * Default constructor.
	 */
	public ParamColor() {
		super();
		color=new Color(0,0,0);
		this.factory = new ParamColorFactory(this);
	}

	/**
	 * Instantiates a new param color.
	 * 
	 * @param p the p
	 */
	public ParamColor(Color p) {
		this();
		color=new Color(p.getRGB());
	}

	/**
	 * Constructor.
	 * 
	 * @param name parameter name
	 */
	public ParamColor(String name) {
		this();
		this.setName(name);
	}


	/**
	 * Instantiates a new parameter color.
	 * 
	 * @param name the name
	 * @param p the p
	 */
	public ParamColor(String name, Color p) {
		this();
		this.setName(name);
		color=new Color(p.getRGB());
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#clone()
	 */
	public ParamColor clone() {
		ParamColor param = new ParamColor();
		param.color=new Color(color.getRGB());
		param.setName(this.getName());
		param.label=this.label;
		param.setHidden(this.isHidden());
		param.setMandatory(this.isMandatory());
		param.shortLabel=shortLabel;
		param.cliTag=cliTag;
		return param;
	}

	/**
	 * Compare to.
	 * 
	 * @param model the model
	 * 
	 * @return the int
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#compareTo(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel)
	 */
	public int compareTo(ParamModel model) {
		return (model instanceof ParamColor) ? 0: 1;
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#getValue()
	 */
	public Color getValue() {
		return color;
	}

	/**
	 * Initialize parameter.
	 */
	public void init() {
		this.setMaxIncoming(1);
		connectible = true;
		factory = new ParamColorFactory(this);
	}

	/**
	 * Checks if is compatible.
	 * 
	 * @param model the model
	 * 
	 * @return true, if checks if is compatible
	 * 
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#isCompatible(edu.jhu.ece.iacl.jist.pipeline.PipePort)
	 */
	public boolean isCompatible(PipePort model) {
		return (model instanceof ParamColor);
	}

	/**
	 * Set color value.
	 * 
	 * @param value the color
	 * 
	 * @throws InvalidParameterValueException the invalid parameter value exception
	 */
	public void setValue(Color value) throws InvalidParameterValueException {
		if (value != null) {
			color=new Color(value.getRGB());
		} else {
			throw new InvalidParameterValueException(this, value);
		}
	}


	/**
	 * Get description of color.
	 * 
	 * @return the string
	 */
	public String toString() {
		return "(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ","+color.getAlpha()+")";
	}

	/**
	 * Validate color.
	 * 
	 * @throws InvalidParameterException the invalid parameter exception
	 */
	public void validate() throws InvalidParameterException {
		if(color==null)throw new InvalidParameterException(this);
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#getHumanReadableDataType()
	 */
	@Override
	public String getHumanReadableDataType() {
		return "color: semi-colon separated list of 4 color components (red,green,blue,alpha) which are integers in the range [0,255]";
	}
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#getXMLValue()
	 */
	public String getXMLValue() {
		Color c =getValue();
		return c.getRed()+";"+c.getGreen()+";"+c.getBlue()+";"+c.getAlpha();
	};
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#setXMLValue(java.lang.String)
	 */
	@Override
	public void setXMLValue(String arg) {
		String []args=arg.trim().split("[;]+");
		if(args.length!=4)
			throw new InvalidParameterValueException(this,"Cannot find four entries:"+arg);
		setValue(new Color(Integer.valueOf(args[0]),Integer.valueOf(args[1]),Integer.valueOf(args[2]),Integer.valueOf(args[3])));
	};
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel#probeDefaultValue()
	 */
	public String probeDefaultValue() {	
		return getXMLValue();
	}
}
