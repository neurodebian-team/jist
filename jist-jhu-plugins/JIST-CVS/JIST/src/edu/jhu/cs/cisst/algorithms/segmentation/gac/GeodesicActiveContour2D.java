/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

// TODO: Auto-generated Javadoc
/**
 * The Class GeodesicActiveContour2D segments an image using the sparse matrix
 * method. Regions where the level set positive are outside the segmented
 * region, and regions where the level set is negative are inside the segmented
 * region.
 * 
 */
public class GeodesicActiveContour2D extends AbstractCalculation {

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.6 $");
	}

	/**
	 * The maximum number of layers. Do not use less than 3 layers because then
	 * inside/outside become ambiguous
	 */
	protected int maxLayers = 3;

	/** The inside points. */
	protected LinkedList<GridPoint2D>[] insidePoints;

	/** The outside points. */
	protected LinkedList<GridPoint2D>[] outsidePoints;

	/** The active points. */
	protected LinkedList<GridPoint2D> activePoints;

	/** The reference image. */
	protected ImageData image = null;

	/** The grid points. */
	protected GridPoint2D[][] gridPoints;

	/** The distance field. */
	protected float[][] distField;

	/** The distance field image. */
	protected ImageDataFloat distFieldImage;

	/** The verbose distance field image. */
	protected ImageDataFloat verboseDistFieldImage;
	
	/** The topology rule. */
	protected TopologyRule2D topologyRule=null;
	
	/** The rows. */
	protected int rows;

	/** The columns. */
	protected int cols;

	/** The inside point count. */
	protected int insideCount = 0;

	/** The dice threshold. */
	protected double diceThreshold = 0.995;

	/** The max speed. */
	protected double maxSpeed = 0.999;

	/** The inner iterations. */
	protected int innerIterations = 100;

	/** The outer iterations. */
	protected int outerIterations = 100;

	/** The positive to negative transition count. */
	protected int posToNegCount = 0;

	/** The negative to positive transition count. */
	protected int negToPosCount = 0;

	/** The 6-connected neighbors x. */
	protected static int[] neighborsX = new int[] { 1, 0, -1, 0 };

	/** The 6-connected neighbors y. */
	protected static int[] neighborsY = new int[] { 0, 1, 0, -1 };

	/** The forces. */
	protected LinkedList<ActiveContourForce2D> forces;

	/**
	 * Sets the dice threshold.
	 * 
	 * @param diceThreshold
	 *            the new dice threshold
	 */
	public void setDiceThreshold(double diceThreshold) {
		this.diceThreshold = diceThreshold;
	}

	/**
	 * Sets the maximum number of layers.
	 * 
	 * @param maxLayers
	 *            the new max layers
	 */
	public void setMaxLayers(int maxLayers) {
		this.maxLayers = maxLayers;
	}

	/**
	 * Sets the inner iterations.
	 * 
	 * @param innerIterations
	 *            the new inner iterations
	 */
	public void setInnerIterations(int innerIterations) {
		this.innerIterations = innerIterations;
	}

	/**
	 * Sets the outer iterations.
	 * 
	 * @param outerIterations
	 *            the new outer iterations
	 */
	public void setOuterIterations(int outerIterations) {
		this.outerIterations = outerIterations;
	}
	/**
	 * Gets the verbose level set.
	 * 
	 * @return the level set
	 */
	public ImageDataFloat getVerboseLevelSet() {
		return verboseDistFieldImage;
	}
	/**
	 * Gets the level set.
	 * 
	 * @return the level set
	 */
	public ImageDataFloat getLevelSet() {
		return distFieldImage;
	}

	/**
	 * Instantiates a new geodesic active contour.
	 * 
	 * @param referenceImage
	 *            the reference image
	 */
	public GeodesicActiveContour2D(ImageData referenceImage) {
		super();
		init(referenceImage);
		setLabel("Active Contour");
	}
	
	/**
	 * Instantiates a new geodesic active contour.
	 *
	 * @param referenceImage the reference image
	 * @param layers the layers
	 */
	public GeodesicActiveContour2D(ImageData referenceImage,int layers) {
		super();
		this.maxLayers=layers;
		init(referenceImage);
		
		setLabel("Active Contour");
	}
	/**
	 * Adds the force.
	 * 
	 * @param force
	 *            the force
	 */
	public void add(ActiveContourForce2D force) {
		forces.add(force);
		force.setGridPoints(gridPoints);
	}

	/**
	 * Gets the forces.
	 * 
	 * @return the forces
	 */
	public LinkedList<ActiveContourForce2D> getForces() {
		return forces;
	}

	/**
	 * Initializes the solver.
	 * 
	 * @param referenceImage
	 *            the reference image
	 */
	protected void init(ImageData referenceImage) {
		this.image = referenceImage;
		forces = new LinkedList<ActiveContourForce2D>();
		insidePoints = new LinkedList[maxLayers];
		outsidePoints = new LinkedList[maxLayers];

		activePoints = new LinkedList<GridPoint2D>();
		for (int i = 0; i < maxLayers; i++) {
			insidePoints[i] = new LinkedList<GridPoint2D>();
			outsidePoints[i] = new LinkedList<GridPoint2D>();
		}
		this.rows = referenceImage.getRows();
		this.cols = referenceImage.getCols();
		this.gridPoints = new GridPoint2D[rows][cols];
		

	}

	/**
	 * Solve.
	 * 
	 * @param initialLevelset
	 *            the initial level set
	 * 
	 * @return the final level set
	 */
	public ImageDataFloat solve(ImageDataFloat initialLevelset) {
		double dice = 0;
		posToNegCount = negToPosCount = 0;
		initNarrowBand(initialLevelset);
		setTotalUnits(outerIterations);
		for (int outerIter = 0; outerIter < outerIterations; outerIter++) {
			// Update forces
			for (ActiveContourForce2D force : forces) {
				force.update();
			}
			for (int innerIter = 0; innerIter < innerIterations; innerIter++) {
				updateNarrowBand();
			}
			dice = 2
					* (insideCount - negToPosCount)
					/ (double) (2 * insideCount + posToNegCount - negToPosCount);
			// Update number of inside points
			insideCount = insideCount + posToNegCount - negToPosCount;
			posToNegCount = negToPosCount = 0;
			setLabel(String.format("Dice Coeff: %1.4f", dice));
			if (dice >= diceThreshold) {
				break;
			}
			incrementCompletedUnits();
		}
		setLabel("Active Contour");
		// Rebuild signed distance function
		DistanceField2D dist = new DistanceField2D();
		for (GridPoint2D pt : activePoints) {
			distField[pt.x][pt.y] = pt.value;
		}
		for (int k = 0; k < maxLayers; k++) {
			for (GridPoint2D pt : insidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
			for (GridPoint2D pt : outsidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
		}
		distFieldImage = dist.solve(distFieldImage, maxLayers + 0.5);
		distFieldImage.setName(image.getName() + "_levelset");
		distFieldImage.setHeader(image.getHeader());
		markCompleted();
		return distFieldImage;
	}

	/**
	 * Solve and generate verbose output.
	 * 
	 * @param initialLevelSet
	 *            the initial level set
	 * 
	 * @return all intermediate level sets
	 */
	public ImageDataFloat solveVerbose(ImageDataFloat initialLevelSet) {
		double dice = 0;
		posToNegCount = negToPosCount = 0;
		initNarrowBand(initialLevelSet);
		verboseDistFieldImage = new ImageDataFloat(rows, cols,
				outerIterations);
		verboseDistFieldImage.setName(image.getName() + "_verbose");
		float[][][] verboseMat = verboseDistFieldImage.toArray3d();
		int outerIter = 0;
		setTotalUnits(outerIterations);
		for (outerIter = 0; outerIter < outerIterations; outerIter++) {
			// Update forces
			for (ActiveContourForce2D force : forces) {
				force.update();
			}
			System.out.println("DICE " + dice);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					verboseMat[i][j][outerIter] = maxLayers
							* Math.signum(distField[i][j]);
				}
			}
			for (GridPoint2D pt : activePoints) {
				verboseMat[pt.x][pt.y][outerIter] = pt.value;
			}
			for (int k = 0; k < maxLayers; k++) {
				for (GridPoint2D pt : insidePoints[k]) {
					verboseMat[pt.x][pt.y][outerIter] = pt.value;
				}
				for (GridPoint2D pt : outsidePoints[k]) {
					verboseMat[pt.x][pt.y][outerIter] = pt.value;
				}
			}
			if (dice >= diceThreshold) {
				break;
			}
			for (int innerIter = 0; innerIter < innerIterations; innerIter++) {
				updateNarrowBand();
			}
			dice = 2
					* (insideCount - negToPosCount)
					/ (double) (2 * insideCount + posToNegCount - negToPosCount);
			insideCount = insideCount + posToNegCount - negToPosCount;
			posToNegCount = negToPosCount = 0;
			setLabel(String.format("Dice Coeff: %1.4f", dice));
			incrementCompletedUnits();
		}
		// Rebuild narrow-band
		DistanceField2D dist = new DistanceField2D();
		for (GridPoint2D pt : activePoints) {
			distField[pt.x][pt.y] = pt.value;
		}
		for (int k = 0; k < maxLayers; k++) {
			for (GridPoint2D pt : insidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
			for (GridPoint2D pt : outsidePoints[k]) {
				distField[pt.x][pt.y] = pt.value;
			}
		}
		distFieldImage = dist.solve(distFieldImage, maxLayers + 0.5);
		distFieldImage.setName(image.getName() + "_levelset");
		distFieldImage.setHeader(image.getHeader());
		distField = distFieldImage.toArray2d();
		markCompleted();
		return distFieldImage;
	}

	/**
	 * Update narrow band.
	 */
	protected void updateNarrowBand() {
		double value = 0;
		double[] delta = new double[activePoints.size()];
		double maxDelta = 0;
		int index = 0;
		// Compute force updates
		for (GridPoint2D pt : activePoints) {
			value = 0;
			for (ActiveContourForce2D force : forces) {
				double forceUpdate = force.evaluate(pt.x, pt.y);
				value += forceUpdate;
			}
			maxDelta = Math.max(Math.abs(value), maxDelta);
			// Keep track of level set changes
			delta[index++] = value;
		}
		// Rescale max speed to enforce CFL condition
		double timeStep = 0.5 * ((maxDelta > maxSpeed) ? (maxSpeed / maxDelta)
				: maxSpeed);
		LinkedList<GridPoint2D> updateList = new LinkedList<GridPoint2D>();
		LinkedList<GridPoint2D> promoteList = new LinkedList<GridPoint2D>();
		LinkedList<GridPoint2D> demoteList = new LinkedList<GridPoint2D>();
		ListIterator<GridPoint2D> iterator = activePoints.listIterator();
		index = 0;
		// Update the level set value for all active grid points
		while (iterator.hasNext()) {
			GridPoint2D pt = iterator.next();
			value = pt.value + timeStep * delta[index++];
			if(topologyRule!=null){
				value=topologyRule.applyRule(pt, value);
			}
			// Keep track of sign changes
			if (pt.value >= 0 && value < 0) {
				posToNegCount++;
			} else if (pt.value < 0 && value >= 0) {
				negToPosCount++;
			}
			pt.value = (float) value;
			if (value < -0.5) {
				// Point is outside of active list, demote to lower layer
				pt.layer = -1;
				iterator.remove();
				demoteList.add(pt);
			} else if (value > 0.5) {
				// Point is outside of active list, promote to higher layer
				pt.layer = 1;
				iterator.remove();
				promoteList.add(pt);
			}
		}
		int x, y;
		// Update inside/outside layers
		for (int layer = 1; layer <= maxLayers; layer++) {
			iterator = outsidePoints[layer - 1].listIterator();
			// Update outer layers
			while (iterator.hasNext()) {
				GridPoint2D pt = iterator.next();
				float oldValue = pt.value;
				pt.value = 1E10f;
				pt.layer = maxLayers + 1;
				// Compute level set update
				for (int i = 0; i < neighborsX.length; i++) {
					x = pt.x + neighborsX[i];
					y = pt.y + neighborsY[i];
					if (x >= 0 && y >= 0 && x < rows && y < cols) {
						GridPoint2D npt = gridPoints[x][y];
						// The layer cannot change sign
						if (npt != null && npt.layer > -2) {
							pt.value = Math.min(npt.value, pt.value);
							pt.layer = Math.min(npt.layer, pt.layer);
						}
					}
				}
				pt.value++;
				pt.layer++;
				// Demote to layer
				if (pt.layer < layer) {
					if (pt.layer > 0) {
						// Point should be moved into lower outside layer
						outsidePoints[pt.layer - 1].add(pt);
						// Demoting a grid point at boundary could indicate the
						// need to create a new boundary grid point
						if (pt.layer == maxLayers - 2) {
							for (int k = 0; k < neighborsX.length; k++) {
								x = pt.x + neighborsX[k];
								y = pt.y + neighborsY[k];
								if (x >= 0 && y >= 0 && x < rows && y < cols
										&& gridPoints[x][y] == null) {
									GridPoint2D npt = new GridPoint2D(x, y,
											maxLayers - 1);
									gridPoints[x][y] = npt;
									updateList.add(npt);
								}
							}
						}
					} else {
						// Sign could have changed, tally sign changes
						if (pt.value >= 0 && oldValue < 0) {
							negToPosCount++;
						} else if (pt.value < 0 && oldValue >= 0) {
							posToNegCount++;
						}
						// Point should be moved into active list
						activePoints.add(pt);
					}
					iterator.remove();
					// Promote to layer
				} else if (pt.layer > layer) {
					if (pt.layer < maxLayers) {
						// Point should be moved into higher outside later
						outsidePoints[pt.layer - 1].add(pt);

					} else {
						// Remove point from narrow band
						gridPoints[pt.x][pt.y] = null;
						distField[pt.x][pt.y] = pt.layer;
					}

					iterator.remove();
				}
			}
			iterator = insidePoints[layer - 1].listIterator();
			// Update inside layers
			while (iterator.hasNext()) {
				GridPoint2D pt = iterator.next();
				float oldValue = pt.value;
				pt.value = -1E10f;
				pt.layer = -maxLayers - 1;
				// Compute level set update
				for (int i = 0; i < neighborsX.length; i++) {
					x = pt.x + neighborsX[i];
					y = pt.y + neighborsY[i];
					if (x >= 0 && y >= 0 && x < rows && y < cols) {
						GridPoint2D npt = gridPoints[x][y];
						// The layer cannot change sign
						if (npt != null && npt.layer < 2) {
							pt.value = Math.max(npt.value, pt.value);
							pt.layer = Math.max(npt.layer, pt.layer);
						}
					}
				}

				pt.value--;
				pt.layer--;
				// Demote to layer
				if (-pt.layer < layer) {
					if (-pt.layer > 0) {
						insidePoints[-pt.layer - 1].add(pt);
						if (-pt.layer == maxLayers - 2) {
							for (int k = 0; k < neighborsX.length; k++) {
								x = pt.x + neighborsX[k];
								y = pt.y + neighborsY[k];
								if (x >= 0 && y >= 0 && x < rows && y < cols
										&& gridPoints[x][y] == null) {
									GridPoint2D npt = new GridPoint2D(x, y,
											-maxLayers + 1);
									gridPoints[x][y] = npt;
									updateList.add(npt);
								}
							}
						}
					} else {
						// Sign could have changed, tally sign changes
						if (pt.value >= 0 && oldValue < 0) {
							negToPosCount++;
						} else if (pt.value < 0 && oldValue >= 0) {
							posToNegCount++;
						}
						// Point should be moved into active list
						activePoints.add(pt);
					}
					iterator.remove();
					// Promote to layer
				} else if (-pt.layer > layer) {
					if (-pt.layer < maxLayers) {
						insidePoints[-pt.layer - 1].add(pt);
					} else {
						// Remove point from narrow band
						gridPoints[pt.x][pt.y] = null;
						distField[pt.x][pt.y] = pt.layer;
					}
					iterator.remove();
				}
			}
		}
		// Add all active points that changed layer
		insidePoints[0].addAll(demoteList);
		outsidePoints[0].addAll(promoteList);
		// Update points that were added to the narrow band
		for (GridPoint2D pt : updateList) {
			// Update inside point
			if (pt.layer < 0) {
				pt.value = -1E10f;
				for (int i = 0; i < neighborsX.length; i++) {
					x = pt.x + neighborsX[i];
					y = pt.y + neighborsY[i];
					if (x >= 0 && y >= 0 && x < rows && y < cols) {
						GridPoint2D npt = gridPoints[x][y];
						if (npt != null && npt.layer > pt.layer) {
							pt.value = Math.max(npt.value, pt.value);
						}
					}
				}
				pt.value--;
				insidePoints[-pt.layer - 1].add(pt);
			} else {
				// Update outside point
				pt.value = 1E10f;
				for (int i = 0; i < neighborsX.length; i++) {
					x = pt.x + neighborsX[i];
					y = pt.y + neighborsY[i];
					if (x >= 0 && y >= 0 && x < rows && y < cols) {
						GridPoint2D npt = gridPoints[x][y];
						if (npt != null && npt.layer < pt.layer) {
							pt.value = Math.min(npt.value, pt.value);
						}
					}
				}
				pt.value++;
				outsidePoints[pt.layer - 1].add(pt);
			}
		}

	}

	/**
	 * Initializes the narrow band.
	 * 
	 * @param initialLevelSet
	 *            the initial level set
	 */
	public void initNarrowBand(ImageDataFloat initialLevelSet) {
		DistanceField2D dist = new DistanceField2D();
		distFieldImage = dist.solve(initialLevelSet, 1.5);
		distField = distFieldImage.toArray2d();
		GridPoint2D gpt;
		insideCount = 0;
		PriorityQueue<GridPoint2D> queue = new PriorityQueue<GridPoint2D>();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				float val = distField[i][j];
				//Create grid points for active set
				if (val <= 0.5f && val >= -0.5f) {
					activePoints.add(gpt = new GridPoint2D(i, j, 0, val));
					gridPoints[i][j] = gpt;
					queue.add(gpt);
				}
				//Track number of points inside boundary
				if (val < 0) {
					insideCount++;
				}
				distField[i][j] = maxLayers * Math.signum(val);
			}
		}
		//Create inside/outside grid points
		while (queue.size() > 0) {
			GridPoint2D apt = queue.remove();
			if (Math.abs(apt.layer) >= maxLayers - 1)
				continue;
			for (int i = 0; i < neighborsX.length; i++) {
				int x = apt.x + neighborsX[i];
				int y = apt.y + neighborsY[i];
				if (x >= 0 && y >= 0 && x < rows && y < cols
						&& gridPoints[x][y] == null) {
					GridPoint2D lpt;
					if (distField[x][y] > 0) {
						outsidePoints[apt.layer].add(lpt = new GridPoint2D(x,
								y, apt.layer + 1));
					} else {
						insidePoints[-apt.layer].add(lpt = new GridPoint2D(x,
								y, apt.layer - 1));
					}
					gridPoints[x][y] = lpt;
					queue.add(lpt);
				}
			}
		}
		//Compute signed distance for inside/outisde points
		for (int l = 0; l < maxLayers; l++) {
			for (GridPoint2D pt : outsidePoints[l]) {
				pt.value = 1E10f;
				for (int i = 0; i < neighborsX.length; i++) {
					int x = pt.x + neighborsX[i];
					int y = pt.y + neighborsY[i];
					if (x >= 0 && y >= 0 && x < rows && y < cols) {
						GridPoint2D npt = gridPoints[x][y];
						if (npt != null && npt.layer < pt.layer) {
							pt.value = Math.min(npt.value, pt.value);
						}
					}
				}
				pt.value++;
			}
			for (GridPoint2D pt : insidePoints[l]) {
				pt.value = -1E10f;
				for (int i = 0; i < neighborsX.length; i++) {
					int x = pt.x + neighborsX[i];
					int y = pt.y + neighborsY[i];
					if (x >= 0 && y >= 0 && x < rows && y < cols) {
						GridPoint2D npt = gridPoints[x][y];
						if (npt != null && npt.layer > pt.layer) {
							pt.value = Math.max(npt.value, pt.value);
						}
					}
				}
				pt.value--;
			}
		}

	}
	/**
	 * Attach topology rule to active contour.
	 * @param rule rule
	 */
	public void attachTopologyRule(TopologyRule2D rule){
		this.topologyRule=rule;
		if(topologyRule!=null)topologyRule.setGridPoints(gridPoints);
	}
	
	/**
	 * Gets the grid points.
	 *
	 * @return the grid points
	 */
	public GridPoint2D[][] getGridPoints() {
		return gridPoints;
	}
	/*
	 * private boolean sanityCheck() { boolean ok = true;
	 * 
	 * System.err.println("SANITY CHECK ... "); int insideCount = 0; int
	 * outsideCount = 0;
	 * 
	 * for (GridPoint2D pt : activePoints) { if (Math.abs(pt.value) > 0.5) {
	 * System.err.println("Error: Active point not in narrow band " + pt); ok =
	 * false; } if (pt.layer != 0) {
	 * System.err.println("Error: Active point has wrong layer id " + pt); ok =
	 * false; } } for (int l = 0; l < maxLayers; l++) { insideCount +=
	 * insidePoints[l].size(); outsideCount += outsidePoints[l].size(); for
	 * (GridPoint2D pt : outsidePoints[l]) { if (Math.abs(pt.value - pt.layer) -
	 * 0.5 > 1E-3) { System.err
	 * .println("Error: Outside point not in proper distance range " + pt + " "
	 * + Math .abs(Math.abs(pt.value - (l + 1)) - 0.5)); ok = false; } if
	 * (pt.layer != l + 1) { System.err
	 * .println("Error: Outside point has wrong layer id " + pt); ok = false; }
	 * int minLayer = maxLayers + 1; for (int i = 0; i < neighborsX.length; i++)
	 * { int x = pt.x + neighborsX[i]; int y = pt.y + neighborsY[i]; if (x >= 0
	 * && y >= 0 && x < rows && y < cols) { GridPoint2D npt = gridPoints[x][y];
	 * if (npt != null && npt.layer >= 0) { minLayer = (int) Math.min(npt.layer,
	 * minLayer); } } } if (minLayer > pt.layer) { System.err
	 * .println("Error: Outside point does not have a neighbor in a lower layer ("
	 * + minLayer + ") " + pt); for (int i = 0; i < neighborsX.length; i++) {
	 * int x = pt.x + neighborsX[i]; int y = pt.y + neighborsY[i]; if (x >= 0 &&
	 * y >= 0 && x < rows && y < cols) { GridPoint2D npt = gridPoints[x][y]; if
	 * (npt != null) { System.err.println("NEIGHBOR " + npt); } } }
	 * 
	 * ok = false; } } for (GridPoint2D pt : insidePoints[l]) {
	 * 
	 * if (Math.abs(pt.value - pt.layer) - 0.5 > 1E-3) { System.err
	 * .println("Error: Inside point not in proper distance range " + pt); ok =
	 * false; } if (pt.layer != -l - 1) { System.err
	 * .println("Error: Inside point has wrong layer id " + pt); ok = false; }
	 * int maxLayer = -maxLayers - 1; for (int i = 0; i < neighborsX.length;
	 * i++) { int x = pt.x + neighborsX[i]; int y = pt.y + neighborsY[i]; if (x
	 * >= 0 && y >= 0 && x < rows && y < cols) { GridPoint2D npt =
	 * gridPoints[x][y]; if (npt != null && npt.layer <= 0) { maxLayer = (int)
	 * Math.max(npt.layer, maxLayer); } } } if (maxLayer < pt.layer) {
	 * System.err
	 * .println("Error: Inside point does not have a neighbor in a higher layer ("
	 * + maxLayer + ") " + pt); for (int i = 0; i < neighborsX.length; i++) {
	 * int x = pt.x + neighborsX[i]; int y = pt.y + neighborsY[i]; if (x >= 0 &&
	 * y >= 0 && x < rows && y < cols) { GridPoint2D npt = gridPoints[x][y]; if
	 * (npt != null) { System.err.println("NEIGHBOR " + npt); } } } ok = false;
	 * } } } int checkInsideCount = 0; int checkOutsideCount = 0; int
	 * checkActiveCount = 0; for (int i = 0; i < rows; i++) { for (int j = 0; j
	 * < cols; j++) { GridPoint2D pt = gridPoints[i][j]; if (pt != null) { if
	 * (pt.x != i || pt.y != j) { System.err
	 * .println("Error: Point location is not correct (" + i + "," + j + ") " +
	 * pt); ok = false; } if (pt.layer < 0) { checkInsideCount++; } else if
	 * (pt.layer > 0) { checkOutsideCount++; } else { checkActiveCount++; } }
	 * else { for (int k = 0; k < neighborsX.length; k++) { int x = i; int y =
	 * j; if (x >= 0 && y >= 0 && x < rows && y < cols) { GridPoint2D npt =
	 * gridPoints[x][y]; if (npt != null) { if (Math.abs(npt.layer) != maxLayers
	 * - 1) { System.err .println("Error: Grid node is not boundary " + pt); ok
	 * = false; } } } } } } } if (checkInsideCount != insideCount) {
	 * System.err.println("Number of inside points do not match: " + insideCount
	 * + "/" + checkInsideCount); ok = false; } if (checkOutsideCount !=
	 * outsideCount) {
	 * System.err.println("Number of outside points do not match: " +
	 * outsideCount + "/" + checkOutsideCount); ok = false; } if
	 * (checkActiveCount != activePoints.size()) {
	 * System.err.println("Number of inside points do not match: " +
	 * activePoints.size() + "/" + checkActiveCount); ok = false; } if (ok) {
	 * System.err.println("... OK"); } else {
	 * System.err.println("... SANITY CHECK FAILED"); } return ok; }
	 */


}
