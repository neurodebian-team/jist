/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.segmentation.gac;

/**
 * The Class AdvectionForce3D moves the zero level set in the direction of the
 * force field.
 */
public class AdvectionForce3D extends ActiveContourForce3D {

	/** The advection force. */
	float[][][][] advectionForce;

	/**
	 * Instantiates a new advection force.
	 * 
	 * @param advectionForce
	 *            the advection force
	 */
	public AdvectionForce3D(float[][][][] advectionForce) {
		this.advectionForce = advectionForce;
	}

	/** Get the advection force. */
	public float[][][][] getAdvectionForce() {
		return advectionForce;
	}

	public void setAdvectionForce(float[][][][] advectionForce) {
		this.advectionForce = advectionForce;
	}

	/**
	 * @see edu.jhu.cs.cisst.algorithms.segmentation.gac.ActiveContourForce3D#evaluate(int,
	 *      int)
	 */
	public double evaluate(int i, int j, int k) {
		GridPoint3D p011 = getGridPoint(i - 1, j, k);
		GridPoint3D p121 = getGridPoint(i, j + 1, k);
		GridPoint3D p111 = getGridPoint(i, j, k);
		GridPoint3D p101 = getGridPoint(i, j - 1, k);
		GridPoint3D p211 = getGridPoint(i + 1, j, k);
		GridPoint3D p110 = getGridPoint(i, j, k - 1);
		GridPoint3D p112 = getGridPoint(i, j, k + 1);

		double v111 = p111.value;
		double v011 = ((p011 != null) ? p011.value : v111);
		double v121 = ((p121 != null) ? p121.value : v111);
		double v101 = ((p101 != null) ? p101.value : v111);
		double v211 = ((p211 != null) ? p211.value : v111);
		double v110 = ((p110 != null) ? p110.value : v111);
		double v112 = ((p112 != null) ? p112.value : v111);
		double DxNeg = v111 - v011;
		double DxPos = v211 - v111;
		double DyNeg = v111 - v101;
		double DyPos = v121 - v111;
		double DzNeg = v111 - v110;
		double DzPos = v112 - v111;
		// Level set force should be the opposite sign of advection force so it
		// moves in the direction of the force.
		double forceX = weight * advectionForce[i][j][k][0];
		double forceY = weight * advectionForce[i][j][k][1];
		double forceZ = weight * advectionForce[i][j][k][2];
		double advection = 0;
		// Dot product force with upwind gradient
		if (forceX > 0) {
			advection = forceX * DxNeg;
		} else if (forceX < 0) {
			advection = forceX * DxPos;
		}
		if (forceY > 0) {
			advection += forceY * DyNeg;
		} else if (forceY < 0) {
			advection += forceY * DyPos;
		}
		if (forceZ > 0) {
			advection += forceZ * DzNeg;
		} else if (forceZ < 0) {
			advection += forceZ * DzPos;
		}
		return -advection;
	}
}
