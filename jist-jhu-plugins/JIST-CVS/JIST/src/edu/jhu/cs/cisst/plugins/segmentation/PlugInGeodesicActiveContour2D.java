/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.plugins.segmentation;

import edu.jhu.cs.cisst.algorithms.segmentation.gac.AdvectionForce2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.GeodesicActiveContour2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.MeanCurvatureForce2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.PressureForce2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyRule2D;
import edu.jhu.cs.cisst.algorithms.segmentation.gac.TopologyPreservationRule2D;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

// TODO: Auto-generated Javadoc
/**
 * The Class PlugInGeodesicActiveContour2D.
 */
public class PlugInGeodesicActiveContour2D extends ProcessingAlgorithm {
	
	/** The reference image param. */
	ParamVolume referenceImageParam;
	
	/** The pressure image param. */
	ParamVolume pressureImageParam;
	
	/** The initial image param. */
	ParamVolume initialImageParam;
	
	/** The vecfield param. */
	ParamVolume vecfieldParam;

	/** The verbose segmented image param. */
	ParamVolume verboseSegmentedImageParam;
	
	/** The segmented image param. */
	ParamVolume segmentedImageParam;
	
	/** The connectivity rule param. */
	ParamOption connectivityRuleParam;
	
	/** The advection weight param. */
	ParamDouble advectionWeightParam;
	
	/** The pressure weight param. */
	ParamDouble pressureWeightParam;
	
	/** The curvature weight param. */
	ParamDouble curvatureWeightParam;
	
	/** The dice threshold param. */
	ParamDouble diceThresholdParam;
	
	/** The target pressure param. */
	ParamDouble targetPressureParam;
	
	/** The outer iters param. */
	ParamInteger outerItersParam;
	
	/** The inner iters param. */
	ParamInteger innerItersParam;
	
	/** The verbose param. */
	ParamBoolean verboseParam;
	
	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		ParamCollection dataFrame = new ParamCollection("Data");
		dataFrame.add(referenceImageParam = new ParamVolume("Reference Image",
				null, -1, -1, 1, 1));
		referenceImageParam.setMandatory(false);
		dataFrame.add(pressureImageParam = new ParamVolume("Pressure Image",
				VoxelType.FLOAT, -1, -1, 1, 1));
		pressureImageParam.setMandatory(false);
		dataFrame.add(vecfieldParam = new ParamVolume("Advection Vector Field",
				VoxelType.FLOAT, -1, -1, 2, 1));
		vecfieldParam.setMandatory(false);
		dataFrame.add(initialImageParam = new ParamVolume(
				"Initial Levelset Image", null, -1, -1, 1, 1));
		String[] ruleStrings=new String[3];
		ruleStrings[0]="NONE";
		ruleStrings[1]="Connected (4,8)";
		ruleStrings[2]="Connected (8,4)";
		dataFrame.add(connectivityRuleParam=new ParamOption("Connectivity Rule",ruleStrings));
		dataFrame.add(verboseParam = new ParamBoolean("Enable Verbose Output",
				false));
		ParamCollection paramFrame = new ParamCollection("Parameters");
		paramFrame.add(targetPressureParam = new ParamDouble("Target Pressure",
				-1000000, 1000000, 0.5));
		paramFrame.add(pressureWeightParam = new ParamDouble("Pressure Weight",
				0.3));
		paramFrame.add(curvatureWeightParam = new ParamDouble(
				"Curvature Weight", 1.0));
		paramFrame.add(advectionWeightParam = new ParamDouble(
				"Advection Weight", 1.0));
		paramFrame.add(outerItersParam = new ParamInteger(
				"Max Outer Iterations", 0, 1000000, 50));
		paramFrame.add(innerItersParam = new ParamInteger(
				"Max Inner Iterations", 1, 1000000, 100));
		paramFrame.add(diceThresholdParam = new ParamDouble("Dice Threshold",
				0, 1, 0.995));

		inputParams.add(dataFrame);
		inputParams.add(paramFrame);
		inputParams.setLabel("Geodesic Active Contour 2D");
		inputParams.setName("geodesic_active_contour2d");
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.blakeLucas);
		info
				.setDescription("Segments a 2D image using a geodesic active contour. " +
						"The active contour is represented by a levelset where positive values are outside the segmented region and negative values are inside the region." +
						" This implementation uses the sparse matrix method to solve PDEs approximated with first-order finite differences.");
		info
				.setLongDescription("The sparse matrix method is roughly a factor of 10x faster than the narrow-band method " +
						"because it solves PDEs on the minimum narrow-band required for finite difference approximations. " +
						"It also maintains a valid signed distance function at each time step, so there is no need to periodically rebuild the level set.");
		info.add(new Citation("DOI:10.1023/A:1008036829907"));
		info.add(new Citation("DOI:10.1016/0021-9991(88)90002-2"));
		info.add(new Citation("DOI:10.1109/TPAMI.2003.1201824"));
		info
				.add(new Citation(
						"Sethian, J. (1999). Level set methods and fast marching methods, Cambridge university press Cambridge."));
		info.setVersion(GeodesicActiveContour2D.getVersion());
		info.setAffiliation("Johns Hopkins University, Department of Computer Science");
		info.setStatus(DevelopmentStatus.Release);
		inputParams.setPackage("CISST");
		inputParams.setCategory("Segmentation");

	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection)
	 */
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(segmentedImageParam = new ParamVolume(
				"Level Set Image"));
		outputParams.add(verboseSegmentedImageParam = new ParamVolume(
				"Verbose Level Set Image"));
		verboseSegmentedImageParam.setMandatory(false);
	}

	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor)
	 */
	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ImageDataFloat initIm = new ImageDataFloat(initialImageParam
				.getImageData());
		ImageData refImage = (referenceImageParam.getImageData() != null) ? referenceImageParam
				.getImageData()
				: initIm;
		ImageDataFloat pressureImage = (pressureImageParam.getImageData() != null) ? new ImageDataFloat(
				pressureImageParam.getImageData())
				: null;
		ImageDataFloat vecfieldImage = (vecfieldParam.getImageData() != null) ? new ImageDataFloat(
				vecfieldParam.getImageData())
				: null;
		GeodesicActiveContour2D activeContour = new GeodesicActiveContour2D(
				refImage);
		monitor.observe(activeContour);
		activeContour.setDiceThreshold(diceThresholdParam.getDouble());
		MeanCurvatureForce2D curvatureForce = new MeanCurvatureForce2D();
		curvatureForce.setWeight(curvatureWeightParam.getDouble());
		activeContour.add(curvatureForce);
		if (pressureImage != null) {
			PressureForce2D pressureForce = new PressureForce2D((pressureImage
					.toArray2d()));
			pressureForce.rescale(targetPressureParam.getDouble());
			pressureForce.setWeight(pressureWeightParam.getDouble());
			activeContour.add(pressureForce);
		}
		if (vecfieldImage != null) {
			AdvectionForce2D advectionForce = new AdvectionForce2D(
					vecfieldImage.toArray3d());
			advectionForce.setWeight(advectionWeightParam.getDouble());
			activeContour.add(advectionForce);
		}
		int innerIters = innerItersParam.getInt();
		int outerIters = outerItersParam.getInt();
		activeContour.setInnerIterations(innerIters);
		activeContour.setOuterIterations(outerIters);
		if(connectivityRuleParam.getIndex()>0){
			activeContour.attachTopologyRule(new TopologyPreservationRule2D(TopologyRule2D.Rule.values()[connectivityRuleParam.getIndex()-1]));
		}
		if (verboseParam.getValue()) {
			segmentedImageParam.setValue(activeContour.solveVerbose(initIm));
			verboseSegmentedImageParam.setValue(activeContour.getVerboseLevelSet());
		} else {
			segmentedImageParam.setValue(activeContour.solve(initIm));
		}
	}

}
