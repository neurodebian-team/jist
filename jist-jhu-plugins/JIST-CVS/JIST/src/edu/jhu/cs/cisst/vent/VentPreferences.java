package edu.jhu.cs.cisst.vent;

import java.awt.Dimension;

public class VentPreferences {
	protected static Dimension defaultCanvasSize = new Dimension(600, 600);

	public Dimension getDefaultCanvasSize() {
		return defaultCanvasSize;
	}

	public int getDefaultCanvasWidth() {
		return defaultCanvasSize.width;
	}

	public int getDefaultCanvasHeight() {
		return defaultCanvasSize.height;
	}

	public void setDefaultCanvasSize(Dimension defaultCanvasSize) {
		VentPreferences.defaultCanvasSize = defaultCanvasSize;
	}

	protected static VentPreferences preferences;

	public static VentPreferences getInstance() {
		if (preferences == null) {
			preferences = new VentPreferences();
		}
		return preferences;
	}
}
