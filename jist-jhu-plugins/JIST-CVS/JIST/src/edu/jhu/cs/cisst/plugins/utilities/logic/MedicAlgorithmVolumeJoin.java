/*
 *
 */
package edu.jhu.cs.cisst.plugins.utilities.logic;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmVolumeJoin extends ProcessingAlgorithm{
	private ParamVolume volTrue;
	private ParamVolume volFalse;
	private ParamVolume outVol;
	private ParamBoolean branchParam;
	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(volTrue=new ParamVolume("Volume for True"));
		inputParams.add(volFalse=new ParamVolume("Volume for False"));

		volTrue.setMandatory(false);
		volFalse.setMandatory(false);
		inputParams.add(branchParam=new ParamBoolean("Assertion",true));
		inputParams.setPackage("CISST");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Volume Join");
		inputParams.setName("volume_join");
		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription("Joins two pipelines and chooses which input to use for subsequent modules. This module can only join two pipelines if BOTH have sucessfully executed.");
		info.add(CommonAuthors.blakeLucas);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Volume"));
	}
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		if(branchParam.getValue()){
			outVol.setValue(volTrue.getValue());
		} else {
			outVol.setValue(volFalse.getValue());
		}
	}
}
