/**
 * JIST Extensions for Computer-Integrated Surgery
 *
 * Center for Computer-Integrated Surgical Systems and Technology &
 * Johns Hopkins Applied Physics Laboratory &
 * The Johns Hopkins University
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.  The license is available for reading at:
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * @author Blake Lucas
 */
package edu.jhu.cs.cisst.algorithms.optimize.fmg.solver;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.BoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.grid.ConstNoBoundaryGrid;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.parallel.*;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.smoother.Smoother;
import edu.jhu.cs.cisst.algorithms.optimize.fmg.util.*;

// TODO: Auto-generated Javadoc
/**
 * Solve a PDE by iteratively applying a smoothing (i.e. relaxation) algorithm.
 * 
 * @author Gerald Loeffler (Gerald.Loeffler@univie.ac.at)
 * @link http://www.gerald-loeffler.net
 */
public final class SolverThroughSmoother implements Solver {
     
     /**
      * construct from smoother and allowed error.
      * 
      * @param smoother the object supplying the smoothing algorithm
      * @param error    the allowed error for the solution as a fraction of the true solution (0 < error < 1)
      */
     public SolverThroughSmoother(Smoother smoother, double error) {
          Contract.pre(smoother != null,"smoother not null-object");
          Contract.pre(error > 0 && error < 1,"0 < error < 1");
          
          this.smoother = smoother;
          this.error    = error;
     }

     /**
      * implements method from Solver.
      * 
      * @param u the u
      * @param f the f
      * 
      * @return the boundary grid
      * 
      * @see Solver#solve
      */
     public BoundaryGrid solve(ConstBoundaryGrid u, ConstNoBoundaryGrid f) {
          Contract.pre(u != null && f != null,"all objects not null-objects");
          
          totalRange             = new IntRange1D(1,sx - 2);
          ConstBoundaryGrid gOld = u;
          do {
               ConstBoundaryGrid gNew = smoother.smooth(gOld,f);
               goOn = false;
               new SolveThroughSmootherParallelizer(this,gOld,gNew);
               gOld = gNew;
          } while (goOn);

          return ((BoundaryGrid) gOld);
     }
     
     /**
      * implements the parallel part of solve().
      * 
      * @param gOld the g old
      * @param gNew the g new
      * @param myNum the my num
      * @param totalNum the total num
      */
     public void solveProper(ConstBoundaryGrid gOld, ConstBoundaryGrid gNew, int myNum, int totalNum) {
          IntRange1D range = Parallelizer.partition(totalRange,myNum,totalNum);
          for (int x = range.from(); (x <= range.to()) && !goOn; x++) {
                    for (int y = 1; (y <= (sy - 2)) && !goOn; y++) {
                         for (int z = 1; (z <= (sz - 2)) && !goOn; z++) {
                              double n = gNew.get(x,y,z);
                              double o = gOld.get(x,y,z);
                              if ((n != 0) && (Math.abs((o - n)/n) > error)) goOn = true;
                         }
                    }
               }
     }

     /** The smoother. */
     private Smoother   smoother;
     
     /** The error. */
     private double     error;
     
     /** The sz. */
     private int        sx,sy,sz;
     
     /** The total range. */
     private IntRange1D totalRange;
     
     /** The go on. */
     private boolean    goOn;
}
