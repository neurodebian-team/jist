package edu.jhu.bme.smile.commons.optimize;

/**
 * Projects a Differentiable ND function to 1D so that LineSearchNDDifferentiable can optimize in one dimension at a time.
 * @author Hanlin Wan
 */
public class ProjectNDto1DOptimizableDifferentiable extends
		ProjectNDto1DOptimizable implements
		Optimizable1DContinuousDifferentiable {
	
	OptimizableNDContinuousDifferentiable myDiffFunc;
	
	/**
	 * Constructor to create the projected differentiable function.
	 * @param func differentiable function
	 */
	public ProjectNDto1DOptimizableDifferentiable(OptimizableNDContinuousDifferentiable func) {
		super(func);
		myDiffFunc = func;		
	}

	/**
	 * Gets the second derivative at location x in the current dimension.
	 * @param x location to evaluate derivative
	 * @return second derivative at x in the current dimension
	 */
	public double get2ndDerivative(double x) {
		currentLocation[currentDim]=x;
		return myDiffFunc.get2ndDerivative(currentLocation)[currentDim][currentDim];
	}

	/**
	 * Gets the derivative at location x in the current dimension.
	 * @param x location to evaluate derivative
	 * @return derivative at x in the current dimension
	 */
	public double getDerivative(double x) {
		currentLocation[currentDim]=x;
		return myDiffFunc.getDerivative(currentLocation)[currentDim];
	}

}
