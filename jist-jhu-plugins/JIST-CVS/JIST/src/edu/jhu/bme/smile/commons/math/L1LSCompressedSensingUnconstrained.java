package edu.jhu.bme.smile.commons.math;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.sparse.BiCGstab;
import no.uib.cipr.matrix.sparse.CompRowMatrix;
import no.uib.cipr.matrix.sparse.ILU;
import no.uib.cipr.matrix.sparse.IterativeSolver;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.Preconditioner;
import Jama.Matrix;

public class L1LSCompressedSensingUnconstrained extends L1LSCompressedSensing {

	public L1LSCompressedSensingUnconstrained(double[][] A) {
		this(new Matrix(A));
	}
	
	public L1LSCompressedSensingUnconstrained(Matrix A) {
		super(A);
		Matrix bigA = new Matrix(2*n,2*n);
		bigA.setMatrix(0,n-1,0,n-1,AtA2);
		cgA = new DenseMatrix(bigA.getArray());
		int[][] nz = new int[2*n][2];
		for (int i=0; i<2*n; i++) {
			nz[i][0]=i;
			nz[i][1]=(i+n)%(2*n);
		}
		pre = new CompRowMatrix(2*n,2*n,nz);
	}

	public boolean solve(Matrix y, double lambda, double reltol, double maxError, boolean allowRestart) {

		x = new Matrix(n,1);
		Matrix u = new Matrix(n,1,1);
		Matrix xu = MatrixMath.concatinateRows(x, u);
		Matrix f = MatrixMath.concatinateRows(x.minus(u),x.times(-1).minus(u)); //f = [x-u;-x-u];
		double t0=Math.min(Math.max(1,1/lambda), n/1e-3);
		double t = t0;

		if(verbose) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Solving a problem of size (m="+m+", n="+n+"), with lambda="+lambda);
			System.out.println(getClass().getCanonicalName()+"\t"+"-----------------------------------------------------------------------------");
			System.out.println(getClass().getCanonicalName()+"\t"+"iter\tgap\tprimobj\tdualobj\tstep\tlen\tpcg\titers");
		}
		double pobj  = Double.POSITIVE_INFINITY; 
		double dobj  = Double.NEGATIVE_INFINITY; 
		double s     = Double.POSITIVE_INFINITY;
		double pitr  = 0 ; 
		int ntiter =0;
		int lsiter  = 0; 		
		Matrix dxu =  new Matrix(2*n,1);
		Matrix dx = (Matrix)x.clone();
		Matrix du = (Matrix)x.clone();

		for(ntiter = 0;ntiter<=MAX_NT_ITER;ntiter++) {
			//		    %------------------------------------------------------------
			//		    %       CALCULATE DUALITY GAP
			//		    %------------------------------------------------------------

			Matrix z = A.times(x).minus(y); //z = A*x-y;
			Matrix nu = z.times(2); //nu = 2*z;

			double maxAnu = At.times(nu).normInf();
			if (maxAnu > lambda)
				nu = nu.times(lambda/(maxAnu));

			pobj  =  MatrixMath.dotProduct(z,z)+lambda*MatrixMath.colSumNorm(x);
			dobj  =  Math.max(MatrixMath.dotProduct(nu,nu)*(-.25)-MatrixMath.dotProduct(nu,y),dobj); //dobj  =  max(-0.25*nu'*nu-nu'*y,dobj);
			double gap   =  pobj - dobj;
			if(verbose) {
				System.out.println(getClass().getCanonicalName()+"\t"+ntiter+"\t"+gap+"\t"+pobj+"\t"+dobj+"\t"+s+"\t"+pitr);
			}

			//		    %------------------------------------------------------------
			//		    %   STOPPING CRITERION
			//		    %------------------------------------------------------------

			if (gap/Math.abs(dobj) < reltol) {
				if(verbose)  
					System.out.println(getClass().getCanonicalName()+"\t"+"Converged.");
				statusConverged=true;	
				return statusConverged;
			}

			//		    %------------------------------------------------------------
			//		    %       UPDATE t
			//		    %------------------------------------------------------------
			if (s >= 0.5) 
				t = Math.max(Math.min(2*n*MU/gap, MU*t), t); //        t = max(min(n*MU/gap, MU*t), t);

			//		    %------------------------------------------------------------
			//		    %       CALCULATE NEWTON STEP
			//		    %------------------------------------------------------------

			Matrix q1 = MatrixMath.elementInverse(u.plus(x));
			Matrix q2 = MatrixMath.elementInverse(u.minus(x));
			Matrix d1 = MatrixMath.elementProduct(q1, q1).plus(MatrixMath.elementProduct(q2, q2)).times(1./t); 
			Matrix d2 = MatrixMath.elementProduct(q1, q1).minus(MatrixMath.elementProduct(q2, q2)).times(1./t);

			//		    % calculate gradient
			//		    gradphi = [At*(z*2)-(q1-q2)/t; lambda*ones(n,1)-(q1+q2)/t];
			Matrix gradphi = MatrixMath.concatinateRows(At.times(z.times(2)).minus(q1.minus(q2).times(1./t)), 
					MatrixMath.add(q1.plus(q2).times(-1./t),lambda));

			// [2*A'*A+diag(d1) diag(d2); diag(d2) diag(d1)]
			for (int i=0; i<n; i++) {
				cgA.set(i, i, AtA2.get(i, i)+d1.get(i, 0));
				cgA.set(i, i+n, d2.get(i, 0));
				cgA.set(i+n, i, d2.get(i, 0));
				cgA.set(i+n, i+n, d1.get(i, 0));
				
				pre.set(i, i, d1.get(i, 0));
				pre.set(i, i+n, d2.get(i, 0));
				pre.set(i+n, i, d2.get(i, 0));
				pre.set(i+n, i+n, d1.get(i, 0));
			}

			DenseVector cgB = new DenseVector(gradphi.times(-1).getColumnPackedCopy());
			DenseVector cgX = new DenseVector(xu.getColumnPackedCopy());
	
			IterativeSolver solver = new BiCGstab(cgB);
			Preconditioner M = new ILU(pre);
			M.setMatrix(pre);			
			solver.setPreconditioner(M);

			try {
				solver.solve(cgA, cgB, cgX);
			}
			catch (IterativeSolverNotConvergedException e) {
				System.err.println(getClass().getCanonicalName()+"Iterative solver failed to converge");
			}

			// Writes the data back into dx and du
			for (int i=0; i<n; i++) {
				dx.set(i,0,cgX.get(i));
				du.set(i,0,cgX.get(i+n));
			}
			
			//		    %------------------------------------------------------------
			//		    %   BACKTRACKING LINE SEARCH
			//		    %------------------------------------------------------------

			double phi = MatrixMath.dotProduct(z,z)+lambda*MatrixMath.sum(u)-MatrixMath.sum(MatrixMath.log(f.times(-1)))/t;
			s = 1.0;
			double gdx = MatrixMath.dotProduct(gradphi,dxu); 
			Matrix newx=null;
			Matrix newf=null;
			Matrix newu = null;
			for(lsiter = 1;lsiter<=MAX_LS_ITER;lsiter++) {
				newx = x.plus(dx.times(s));// newx = x+s*dx;
				newu = u.plus(du.times(s));//newu = u+s*du;
				newf = MatrixMath.concatinateRows(newx.minus(newu),newx.times(-1).minus(newu)); //f = [x-u;-x-u];
				if (MatrixMath.max(newf) < 0) {
					Matrix newz   =  A.times(newx).minus(y);
					double newphi =  MatrixMath.dotProduct(newz,newz)+lambda*MatrixMath.sum(newu)-MatrixMath.sum(MatrixMath.log(newf.times(-1)))/t;		            
					if (newphi-phi <= ALPHA*s*gdx)
						break;
				}
				s = BETA*s;
			}

			if (lsiter > MAX_LS_ITER) 
				break; // end % exit by BLS

			x = newx; f = newf;
			u = newu;
		}

		statusConverged=false;
		if(verbose) {
			if (lsiter > MAX_LS_ITER) {		    
				System.out.println(getClass().getCanonicalName()+"\t"+"MAX_LS_ITER exceeded in BLS"); 
			} else if (ntiter > MAX_NT_ITER) {
				System.out.println(getClass().getCanonicalName()+"\t"+"MAX_NT_ITER exceeded.");

			} else {
				System.out.println(getClass().getCanonicalName()+"\t"+"Unknown failure result.");
			}
		}
		return false;
	}
}
