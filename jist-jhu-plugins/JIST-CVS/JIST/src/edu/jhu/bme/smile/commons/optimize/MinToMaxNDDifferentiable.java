package edu.jhu.bme.smile.commons.optimize;

/**
 * Finds the maximum of Differentiable ND functions by negating the function and finding the minimum.
 * @author Hanlin Wam
 */
public class MinToMaxNDDifferentiable implements OptimizableNDContinuousDifferentiable {

	private OptimizableNDContinuousDifferentiable myFunction;
	
	/**
	 * Constructor to create the negated function
	 * @param func function to negate
	 */
	public MinToMaxNDDifferentiable(OptimizableNDContinuousDifferentiable func) {
		myFunction = func;
	}

	/**
	 * Gets the negated hessian
	 * @param x location to calculate derivative
	 * @return the negated hessian
	 */
	public double[][] get2ndDerivative(double []x) {
		double der[][] = myFunction.get2ndDerivative(x);
		for (int i = 0; i < x.length; i++)
			for (int j = 0; j < x.length; j++)
				der[i][j] *= -1;
		return der;
	}
	
	/**
	 * Gets the negated second derivative in a specific direction
	 * @param x location to calculate derivative
	 * @param dim1 dimension to take the first derivative
	 * @param dim2 dimension to take the second derivative
	 * @return the negated second derivative in the specified direction
	 */
	public double get2ndDerivative(double[] x, int dim1, int dim2) {
		return -1 * myFunction.get2ndDerivative(x, dim1, dim2);
	}

	/**
	 * Gets the negated gradient
	 * @param x location to calculate derivative
	 * @return the negated gradient
	 */
	public double[] getDerivative(double []x) {
		double der[] = myFunction.getDerivative(x);
		for (int i = 0; i < x.length; i++)
			der[i] *= -1;
		return der;
	}

	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double[] getDomainMax() {	
		return myFunction.getDomainMax();
	}

	/**
	 * Gets the domain minimum
	 * @return domain min
	 */
	public double[] getDomainMin() {
		return myFunction.getDomainMin();
	}

	/**
	 * Gets the tolerance
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunction.getDomainTolerance();
	}

	/**
	 * Gets the negated value
	 * @param x location to get value at
	 * @return negated value at location x
	 */
	public double getValue(double []x) {
		return -1 * myFunction.getValue(x);
	}

	/**
	 * Gets the number of dimensions
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return myFunction.getNumberOfDimensions();
	}
}