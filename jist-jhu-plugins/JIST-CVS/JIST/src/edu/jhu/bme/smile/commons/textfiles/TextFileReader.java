package edu.jhu.bme.smile.commons.textfiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TextFileReader {
	private File myFile=null;
	private String myDelim =null;
	private int maxLines = 0;
	private int maxColsPerLine =0;

	public TextFileReader(File fp) {
		myFile = fp;
		myDelim = ":;,";
	}

	public TextFileReader(File fp, String delim) {
		myFile = fp;
		myDelim = delim;
	}

	public int countLines() throws IOException {
		BufferedReader bufReader = new BufferedReader(new FileReader(myFile));
		int count =0;
		String line=null;
		maxColsPerLine=0;
		while(null!=(line=bufReader.readLine())) {
			if(!line.isEmpty()) {

				count++;			
				String []num = line.split("[\\s"+myDelim +"]+");
				int idx=0;
				for(int i=0;i<num.length;i++) {
					if(!num[i].isEmpty()) {				
						idx++;
					}
				}
				maxColsPerLine=Math.max(idx,maxColsPerLine);
			}
		}
		bufReader.close();
		maxLines =count;
		return count;
	}

	public float[][]parseFloatFile() throws IOException {
		float [][]value = null;		
		countLines();
		value = new float[maxLines][maxColsPerLine];

		BufferedReader bufReader = new BufferedReader(new FileReader(myFile));
		int count =0;
		String line=null;
		while(null!=(line=bufReader.readLine())) {
			if(!line.isEmpty()) {
				String []num = line.split("["+myDelim+"\\s]+");
				int idx=0;
				for(int i=0;i<num.length;i++) {
					if(!num[i].isEmpty()) {
						try {
						value[count][idx]=Float.valueOf(num[i]); }
					catch (Exception e) {
						System.out.println(getClass().getCanonicalName()+"\t"+e);
					}
						idx++;
					}
				}
				count++;
			}
		}
		bufReader.close();

		return value;
	}
	
	public int[][]parseIntFile() throws IOException {
		int [][]value = null;		
		countLines();
		value = new int[maxLines][maxColsPerLine];

		BufferedReader bufReader = new BufferedReader(new FileReader(myFile));
		int count =0;
		String line=null;
		while(null!=(line=bufReader.readLine())) {
			if(!line.isEmpty()) {
				String []num = line.split("["+myDelim+"\\s]+");
				int idx=0;
				for(int i=0;i<num.length;i++) {
					if(!num[i].isEmpty()) {
						try {
						value[count][idx]=Integer.valueOf(num[i]); }
					catch (Exception e) {
						System.out.println(getClass().getCanonicalName()+"\t"+e);
					}
						idx++;
					}
				}
				count++;
			}
		}
		bufReader.close();

		return value;
	}

	public double[][]parseDoubleFile() throws IOException {
		double [][]value = null;		
		countLines();
		value = new double[maxLines][maxColsPerLine];

		BufferedReader bufReader = new BufferedReader(new FileReader(myFile));
		int count =0;
		String line=null;
		while(null!=(line=bufReader.readLine())) {
			if(!line.isEmpty()) {
				String []num = line.split("["+myDelim+"\\s]+");
				int idx=0;
				for(int i=0;i<num.length;i++) {
					if(!num[i].isEmpty()) {
						try {
						value[count][idx]=Float.valueOf(num[i]); }
					catch (Exception e) {
						System.out.println(getClass().getCanonicalName()+"\t"+e);
					}
						idx++;
					}
				}
				count++;
			}
		}
		bufReader.close();

		return value;
	}

}

