package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;
import edu.jhu.bme.smile.commons.math.*;


public class ImageArithmetic extends ProcessingAlgorithm{
	//Input Field
	private ParamVolumeCollection in1;
	private ParamVolumeCollection in2;
	private ParamFloat implicitThreshold; 
	private ParamOption func;
	//Output Field
	private ParamVolumeCollection result;
	//CVS
	private static final String rcsid =
		"$Id: ImageArithmetic.java,v 1.8 2009/12/15 19:52:19 navid1362 Exp $";
	private static final String cvsversion = "$Revision: 1.8 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Perform voxelwise algebraic operations between two volumes to produce another volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		//Plug-in Information
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Image Arithmetic");
		inputParams.setName("Image_Arithmetic");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Robert Kim","rkim35@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(in1=new ParamVolumeCollection("First Image"));
		in1.setLoadAndSaveOnValidate(false);
		inputParams.add(in2=new ParamVolumeCollection("Second Image"));
		in2.setLoadAndSaveOnValidate(false);
		inputParams.add(func=new ParamOption("Function",new String[]{"Add","Subtract","Divide","Multiply","Average","Min","Max","Modulo"}));
		inputParams.add(implicitThreshold =new ParamFloat("Implicit Threshold",Float.NEGATIVE_INFINITY,Float.POSITIVE_INFINITY,Float.NEGATIVE_INFINITY));
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(result=new ParamVolumeCollection("Result Image Volume",null,-1,-1,-1,-1));
		result.setLoadAndSaveOnValidate(false);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		for(int ii=0;ii<in1.size();ii++ ){
			float thres = implicitThreshold.getFloat();
			ParamVolume In1 = in1.getParamVolume(ii);
			ParamVolume In2 = in2.getParamVolume(ii);
			ImageData input1 = In1.getImageData();			
			ImageData input2 = In2.getImageData();			
			ImageData out = new ImageDataFloat(input1);

			int rows = input1.getRows(), cols = input1.getCols(),slices = input1.getSlices(),components = input1.getComponents();		
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					for(int k=0;k<slices;k++){
						for(int l=0;l<components;l++){
							float a= input1.getFloat(i, j, k, l), b=input2.getFloat(i, j, k, l);
							if(a<=thres && b<=thres) {
								out.set(i,j,k,l,Float.NaN);
							} else {
								switch(func.getIndex()){
								case 0: //Add
									out.set(i, j, k, l,a+b);
									break;
								case 1: //Subtract
									out.set(i, j, k, l, a-b);
									break;
								case 2: // Divide
									if ((a==0) && (b==0))
										out.set(i, j, k, l, 0);
									else
										out.set(i, j, k, l, a/b);
									break;
								case 3: // Multiply
									out.set(i, j, k, l, a*b);
									break;
								case 4: // Average
									out.set(i, j, k, l, (a+b)/2);
									break;
								case 5: // Min
									out.set(i, j, k, l, (a<b?a:b));
									break;
								case 6: // Max
									out.set(i, j, k, l, (a>b?a:b));
									break;	
								case 7: // Modulo
									out.set(i, j, k, l, a%b);
									break;	


								}
							}
						}
					}
				}
			}
			In1.dispose();
			In2.dispose();
			out.setName(input1.getName()+"_calc");
			input1.dispose();
			input2.dispose();
			result.add(out);
			result.writeAndFreeNow(this);
		}
	}
}
