package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;


public class RandomVol extends ProcessingAlgorithm{
	/*********************************************
	 * Input Parameters (Volume size in X/Y/Z)
	 *********************************************/
	private ParamInteger x;
	private ParamInteger y;
	private ParamInteger z;
	private ParamInteger t;
	private ParamInteger MAX;
	private ParamInteger MIN;
	private ParamFloat l;
	private ParamInteger SD;
	private ParamOption field;
	/*********************************************
	 * Output Parameters (Random Scalar Volumes)
	 *********************************************/
	private ParamVolume RandomVolume_1;
	
	private static final String rcsid =
		"$Id: RandomVol.java,v 1.11 2009/12/15 20:19:07 navid1362 Exp $";
	private static final String cvsversion = "$Revision: 1.11 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Generate a random scalar volume.";
	private static final String longDescription = "";


	/**
	 * Creates the input parameters.
	 * 
	 * @param inputParams the input params
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Random Volume Generator");
		inputParams.setLabel("Random_Volume_Generator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Robert Kim","rkim35@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);

		
		inputParams.add(x=new ParamInteger("Size of Volume in X direction"));
		x.setValue(new Integer(100));
		inputParams.add(y=new ParamInteger("Size of Volume in Y direction"));
		y.setValue(new Integer(100));
		inputParams.add(z=new ParamInteger("Size of Volume in Z direction"));
		z.setValue(new Integer(20));
		inputParams.add(t=new ParamInteger("Size of Volume in t direction"));
		t.setValue(new Integer(1));
		inputParams.add(SD=new ParamInteger("Standard Deviation for Normal Distribution"));
		SD.setValue(new Integer(1));
		inputParams.add(l=new ParamFloat("Lambda Value for Exponential Distribution"));
		l.setValue(new Double(5));
		inputParams.add(MAX=new ParamInteger("Maximum Value"));
		MAX.setValue(new Integer(1));
		inputParams.add(MIN=new ParamInteger("Minimum Value"));
		MIN.setValue(new Integer(0));
		inputParams.add(field = new ParamOption("Field", new String[]{"Uniform", "Normal", "Exponential"}));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************
		 * Creates random volume 1
		 ****************************************/
		RandomVolume_1 = new ParamVolume("The First Random Volume",null,-1,-1,-1,-1);
		RandomVolume_1.setName("Rand1");
//		RandomVolume_1.setMandatory(false);
		outputParams.add(RandomVolume_1);
	}


	protected void execute(CalculationMonitor monitor) {
		int SZ_X = x.getInt();
		int SZ_Y = y.getInt();
		int SZ_Z = z.getInt();
		int SZ_T = t.getInt();
		int sd = SD.getInt();
		int max = MAX.getInt();
		int min = MIN.getInt();
		float lambda = l.getFloat();
		float [][][][]rand1 = new float[SZ_X][SZ_Y][SZ_Z][SZ_T];
		//Generates random data 1
		java.util.Random rand = new java.util.Random();

		switch(field.getIndex()){
		//Uniform Field
		case 0:
			for(int i=0;i<SZ_X;i++)
			{
				for(int j=0;j<SZ_Y;j++)
				{
					for(int k=0;k<SZ_Z;k++)
					{		
						for(int l=0;l<SZ_T;l++)
						{
							rand1[i][j][k][l] = (float) (rand.nextFloat()*(max-min)+min);	
						}																
					}
				}
			}
			break;
		//Normal Field
		case 1:
			for(int i=0;i<SZ_X;i++)
			{
				for(int j=0;j<SZ_Y;j++)
				{
					for(int k=0;k<SZ_Z;k++)
					{
						for(int l=0;l<SZ_T;l++)
						{
							/*
							double r = 2;
							double u_1=0;double u_2=0;
							while(r>1){
								u_1 = (Math.random()*2-1);
								u_2 = (Math.random()*2-1);
								r = u_1*u_1 + u_2*u_2;
							}						
							float z_1 = (float) ((u_1*Math.sqrt((-2*Math.log(r)/r))));*/
							rand1[i][j][k][l] = (float)rand.nextGaussian();
						}												
					}
				}
			}
			break;
		// Exponential Field
		case 2:
			for(int i=0;i<SZ_X;i++)
			{
				for(int j=0;j<SZ_Y;j++)
				{
					for(int k=0;k<SZ_Z;k++)
					{
						for(int l=0;l<SZ_T;l++)
						{
							rand1[i][j][k][l] = (float)(-Math.log(Math.random())/lambda);
						}												
					}
				}
			}
			break;
		}
		ImageData out1 = (new ImageDataFloat(rand1));
		out1.setName(field.getValue()+" Noise");
		RandomVolume_1.setValue(out1);		
		System.out.println(getClass().getCanonicalName()+"\t"+"FINISHED");
	}
}
