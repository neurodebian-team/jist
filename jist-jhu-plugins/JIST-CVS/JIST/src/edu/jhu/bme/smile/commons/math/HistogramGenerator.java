package edu.jhu.bme.smile.commons.math;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import javax.swing.JFrame;


import ptolemy.plot.Plot;

public class HistogramGenerator extends AbstractCalculation{
	public boolean isAutoMinMax() {
		return autoMinMax;
	}
	public void setAutoMinMax(boolean autoMinMax) {
		this.autoMinMax = autoMinMax;
	}
	public boolean isRobustMinMax() {
		return robustMinMax;
	}
	public void setRobustMinMax(boolean robustMinMax) {
		this.robustMinMax = robustMinMax;
	}
	public boolean isLogScale() {
		return logScale;
	}
	public boolean isProbability(){
		return probability;
	}
	public void setProbability(boolean prob){
		probability = prob;
	}
	public HistogramGenerator(){
		super();
		this.setLabel("Surface Histogram");
	}
	public void setLogScale(boolean logScale) {
		this.logScale = logScale;
	}
	public int getBins() {
		return bins;
	}
	public void setBins(int bins) {
		this.bins = bins;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public double getMin() {
		return min;
	}
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
	protected boolean autoMinMax,robustMinMax,logScale,probability;
	protected int bins,index;
	protected double min,max;
	protected double maxY;
	protected double logScalingFactor=100;
	public double getLogScalingFactor() {
		return logScalingFactor;
	}
	public void setLogScalingFactor(double logScalingFactor) {
		this.logScalingFactor = logScalingFactor;
	}
	protected double[][] hist;

	protected void computeMinMax(float[][] data){
		min=1E30;
		max=-1E30;
		for(int i=0;i<data.length;i++){
			min=Math.min(min,data[i][index]);
			max=Math.max(max,data[i][index]);
		}
	}

	protected void computeMinMax(double[][] data){
		min=1E30;
		max=-1E30;
		for(int i=0;i<data.length;i++){
			min=Math.min(min,data[i][index]);
			max=Math.max(max,data[i][index]);
		}
	}
	protected void computeRobustMinMax(float[][] data){
		min=robustMinimum(data, 0.01f, 4);
		max=robustMaximum(data, 0.01f, 4);
	}
	protected void computeRobustMinMax(double[][] data){
		min=robustMinimum(data, 0.01, 4);
		max=robustMaximum(data, 0.01, 4);
	}
    protected float robustMinimum(float[][] data, float ratio, int scales ) {
		float Imin,Imax,Rmin;
		int Nbins = 10;
		float[] bins = new float[Nbins];
		float count;
		int n;
		
		// ratio: global value
		ratio = ratio*data.length;
		
		// find first min, max
		Imin = 1E30f;
		Imax = -1E30f;
		for (int x=0;x<data.length;x++) {
			if (data[x][index]>Imax) Imax = data[x][index];
			if (data[x][index]<Imin) Imin = data[x][index];
		}
		Rmin = Imin;
		
		for (int t=0;t<scales;t++) {
			
			Rmin = Imin;
		
			// compute coarse histogram
			for (n=0;n<Nbins;n++) bins[n] = 0;
			
			for (int x=0;x<data.length;x++) {
				// first one include both boundaries 
				if (  (data[x][index] >= Imin )
					&&(data[x][index] <= Imin + 1.0f/(float)Nbins*(Imax-Imin) ) ) bins[0]++;
				for (n=1;n<Nbins;n++) {
					if (  (data[x][index] >  Imin + (float)n/(float)Nbins*(Imax-Imin) )
						&&(data[x][index] <= Imin + (float)(n+1)/(float)Nbins*(Imax-Imin) ) ) bins[n]++;	
				}
			}
			/* debug
			System.out.print("histogram: \n");
			for (n=0;n<Nbins;n++) System.out.print(" | "+bins[n]);
			System.out.print("\n");
			*/
			
			// find the value corresponding to the ratio
			count = 0;
			n=0;
			while ( (count < ratio) && (n<Nbins) ) {
				count +=bins[n];
				n=n+1;
			}
			Rmin = Imin + (float)(n-0.5f)/(float)Nbins*(Imax-Imin);
			
			//System.out.print("robust minimum: "+Rmin+" ("+n+", "+ratio+", "+count+")\n");
		
			// new boundaries
			float I0 = Imin + (float)(n-1)/(float)Nbins*(Imax-Imin);
			float I1 = Imin + (float)(n)/(float)Nbins*(Imax-Imin);
			
			Imin = I0;
			Imax = I1;
			
			// new ratio
			ratio = ratio - (count-bins[n-1]);		
		}
		
		return Rmin;
	}

    protected float robustMaximum(float[][] data, float ratio, int scales) {
		float Imin,Imax,Rmax;
		int Nbins = 10;
		float[] bins = new float[Nbins];
		float count;
		int n;
		// ratio: global value
		ratio = ratio*data.length;
		// find first min, max
		Imin = 1E30f;
		Imax = -1E30f;
		for (int x=0;x<data.length;x++) {
			if (data[x][index]>Imax) Imax = data[x][index];
			if (data[x][index]<Imin) Imin = data[x][index];
		}
		Rmax = Imax;
		
		for (int t=0;t<scales;t++) {
			
			Rmax = Imax;
			
			// compute coarse histogram
			for (n=0;n<Nbins;n++) bins[n] = 0;			
			for (int x=0;x<data.length;x++) {
				// first one include both boundaries 
				if (  (data[x][index]>= Imin )
					&&(data[x][index] <= Imin + 1.0f/(float)Nbins*(Imax-Imin) ) ) bins[0]++;
				for (n=1;n<Nbins;n++) {
					if (  (data[x][index] >  Imin + (float)n/(float)Nbins*(Imax-Imin) )
						&&(data[x][index] <= Imin + (float)(n+1)/(float)Nbins*(Imax-Imin) ) ) bins[n]++;	
				}
			}
			count = 0;
			n=Nbins;
			while ( (count < ratio) && (n>0) ) {
				n=n-1;
				count +=bins[n];
			}
			Rmax = Imin + (float)(n+0.5f)/(float)Nbins*(Imax-Imin);
			
			//System.out.print("robust maximum: "+Rmax+" ("+n+", "+ratio+", "+count+")\n");		

			// new boundaries
			float I0 = Imin + (float)n/(float)Nbins*(Imax-Imin);
			float I1 = Imin + (float)(n+1)/(float)Nbins*(Imax-Imin);
			
			Imin = I0;
			Imax = I1;
			
			// new ratio
			ratio = ratio - (count-bins[n]);			
		}
		
		return Rmax;
	}
	public double[][] solve(float[][] data){
		setTotalUnits(data.length);
		hist=new double[bins][2];
		if(autoMinMax){
			if(robustMinMax){
				computeRobustMinMax(data);
			} else {
				computeMinMax(data);
			}
		}
		double logFactor=1.0/Math.log(logScalingFactor);
		if(logScale){
			for(int i=0;i<bins;i++){
				hist[i][0]=min+(max-min)*Math.log(1+(logScalingFactor-1)*i/(bins-1))*logFactor;
				
			}
		} else {
			for(int i=0;i<bins;i++){
				hist[i][0]=min+i*(max-min)/(bins-1);
			}
		}
		int count=0;
		for(int i=0;i<data.length;i++){
			incrementCompletedUnits();
			float val=data[i][index];
			for(int b=1;b<bins;b++){
				if(val<=hist[b][0]&&val>hist[b-1][0]){
					hist[b][1]++;
					count++;
					break;
				}
			}
		}
		maxY=0;
		if(count>0){
			for(int b=0;b<bins;b++){
				hist[b][1]/=(float)count;
				maxY=Math.max(maxY, hist[b][1]);
				System.out.println(getClass().getCanonicalName()+"\t"+"BIN "+b+") "+hist[b][0]+": "+hist[b][1]);
			}
		}
		markCompleted();
		return hist;
	}

    protected double robustMinimum(double[][] data, double ratio, int scales ) {
		double Imin,Imax,Rmin;
		int Nbins = 10;
		double[] bins = new double[Nbins];
		double count;
		int n;
		
		// ratio: global value
		ratio = ratio*data.length;
		
		// find first min, max
		Imin = 1E30;
		Imax = -1E30;
		for (int x=0;x<data.length;x++) {
			if (data[x][index]>Imax) Imax = data[x][index];
			if (data[x][index]<Imin) Imin = data[x][index];
		}
		Rmin = Imin;
		
		for (int t=0;t<scales;t++) {
			
			Rmin = Imin;
		
			// compute coarse histogram
			for (n=0;n<Nbins;n++) bins[n] = 0;
			
			for (int x=0;x<data.length;x++) {
				// first one include both boundaries 
				if (  (data[x][index] >= Imin )
					&&(data[x][index] <= Imin + 1.0f/(double)Nbins*(Imax-Imin) ) ) bins[0]++;
				for (n=1;n<Nbins;n++) {
					if (  (data[x][index] >  Imin + (double)n/(double)Nbins*(Imax-Imin) )
						&&(data[x][index] <= Imin + (double)(n+1)/(double)Nbins*(Imax-Imin) ) ) bins[n]++;	
				}
			}
			/* debug
			System.out.print("histogram: \n");
			for (n=0;n<Nbins;n++) System.out.print(" | "+bins[n]);
			System.out.print("\n");
			*/
			
			// find the value corresponding to the ratio
			count = 0;
			n=0;
			while ( (count < ratio) && (n<Nbins) ) {
				count +=bins[n];
				n=n+1;
			}
			Rmin = Imin + (double)(n-0.5f)/(double)Nbins*(Imax-Imin);
			
			//System.out.print("robust minimum: "+Rmin+" ("+n+", "+ratio+", "+count+")\n");
		
			// new boundaries
			double I0 = Imin + (double)(n-1)/(double)Nbins*(Imax-Imin);
			double I1 = Imin + (double)(n)/(double)Nbins*(Imax-Imin);
			
			Imin = I0;
			Imax = I1;
			
			// new ratio
			ratio = ratio - (count-bins[n-1]);		
		}
		
		return Rmin;
	}

    protected double robustMaximum(double[][] data, double ratio, int scales) {
		double Imin,Imax,Rmax;
		int Nbins = 10;
		double[] bins = new double[Nbins];
		double count;
		int n;
		// ratio: global value
		ratio = ratio*data.length;
		// find first min, max
		Imin = 1E30;
		Imax = -1E30;
		for (int x=0;x<data.length;x++) {
			if (data[x][index]>Imax) Imax = data[x][index];
			if (data[x][index]<Imin) Imin = data[x][index];
		}
		Rmax = Imax;
		
		for (int t=0;t<scales;t++) {
			
			Rmax = Imax;
			
			// compute coarse histogram
			for (n=0;n<Nbins;n++) bins[n] = 0;			
			for (int x=0;x<data.length;x++) {
				// first one include both boundaries 
				if (  (data[x][index]>= Imin )
					&&(data[x][index] <= Imin + 1.0f/(double)Nbins*(Imax-Imin) ) ) bins[0]++;
				for (n=1;n<Nbins;n++) {
					if (  (data[x][index] >  Imin + (double)n/(double)Nbins*(Imax-Imin) )
						&&(data[x][index] <= Imin + (double)(n+1)/(double)Nbins*(Imax-Imin) ) ) bins[n]++;	
				}
			}
			count = 0;
			n=Nbins;
			while ( (count < ratio) && (n>0) ) {
				n=n-1;
				count +=bins[n];
			}
			Rmax = Imin + (double)(n+0.5f)/(double)Nbins*(Imax-Imin);
			
			//System.out.print("robust maximum: "+Rmax+" ("+n+", "+ratio+", "+count+")\n");		

			// new boundaries
			double I0 = Imin + (double)n/(double)Nbins*(Imax-Imin);
			double I1 = Imin + (double)(n+1)/(double)Nbins*(Imax-Imin);
			
			Imin = I0;
			Imax = I1;
			
			// new ratio
			ratio = ratio - (count-bins[n]);			
		}
		
		return Rmax;
	}
	public double[][] solve(double[][] data){
		setTotalUnits(data.length);
		hist=new double[bins][2];
		if(autoMinMax){
			if(robustMinMax){
				computeRobustMinMax(data);
			} else {
				computeMinMax(data);
			}
		}
		double logFactor=1.0/Math.log(100);
		double bin;
		System.out.println(getClass().getCanonicalName()+"\t"+"LOG SCALE "+logScale);
		if(logScale){
			for(int i=0;i<bins;i++){
				hist[i][0]=bin=min+(max-min)*Math.log(1+99*i/(double)(bins-1))*logFactor;
				
			}
		} else {
			for(int i=0;i<bins;i++){
				hist[i][0]=min+i*(max-min)/(bins-1);
			}
		}
		int count=0;
		for(int i=0;i<data.length;i++){
			incrementCompletedUnits();
			double val=data[i][index];
			for(int b=1;b<bins;b++){
				if(val<=hist[b][0]&&val>hist[b-1][0]){
					hist[b][1]++;
					count++;
					break;
				}
			}
		}
		maxY=0;
		if(count>0){
			for(int b=0;b<bins;b++){
				if(probability){
					hist[b][1]/=(double)count;
				}
				maxY=Math.max(maxY, hist[b][1]);
				System.out.println(getClass().getCanonicalName()+"\t"+"BIN "+b+") "+hist[b][0]+": "+hist[b][1]);
			}
		}
		markCompleted();
		return hist;
	}
	public double[][] solve(ImageData data){
		int rows=data.getRows();
		int cols=data.getCols();
		int slices=data.getSlices();
		int count=0;
		double[][] dataArray=new double[rows*cols*slices][1];
		for(int i=0;i<rows;i++)for(int j=0;j<cols;j++)for(int k=0;k<slices;k++){
			dataArray[count++][0]=data.getDouble(i, j,k);
		}
		index=0;
		return solve(dataArray);
	}
	public ImageDataColor getImage(String plotName,String xaxisName,boolean ylogscale,int rows,int cols){
        Plot plot = new Plot();
        plot.setSize(rows,cols);
        plot.setTitle(plotName);
        plot.setXRange(min,max);
        plot.setXLabel(xaxisName);
        plot.setYLabel("Probability (%)");
        plot.setColor(false);
        plot.setMarksStyle("none");
        plot.setYRange(0, 100*maxY);
        plot.setYLog(ylogscale);
        plot.setXLog(false);
        for (int i = 1; i <bins; i++) {
        	plot.addPoint(0, 0.5*(hist[i][0]+hist[i-1][0]),Math.max(1E-6,100*hist[i][1]),true);
        }
        JFrame frame=new JFrame(plotName);
        frame.add(plot);
        frame.pack();
        frame.setVisible(true);
        BufferedImage img=plot.exportImage();
        frame.setVisible(false);
        frame.dispose();
        //plot.setVisible(false);
        Color[][] imgMat=new Color[rows][cols];
        for(int i=0;i<rows;i++){
        	for(int j=0;j<cols;j++){
        		imgMat[i][j]=new Color(img.getRGB(i, j));
        	}
        }
        ImageDataColor imgData=new ImageDataColor(imgMat);
        return imgData;
	}
}
