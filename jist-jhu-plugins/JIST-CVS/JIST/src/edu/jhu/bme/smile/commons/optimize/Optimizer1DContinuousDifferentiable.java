package edu.jhu.bme.smile.commons.optimize;

/**
 * Interface for Differentiable 1D functions.
 * @author Yufeng Guo, Hanlin Wan
 */
public interface Optimizer1DContinuousDifferentiable  {
	
	/**
	 * Initializes an Optimizable1DContinuousDifferentiable function.
	 * @param function 1-dimensional differentiable function to optimize
	 */
	public void initialize(Optimizable1DContinuousDifferentiable function);

	/**
	 * Optimizes a funtion.
	 * @param findMinima true if find min, false if find max
	 * @return true if optimization successful, false if failed
	 */
	public boolean optimize(boolean findMinima);
	
	/**
	 * Gets the optimized value.
	 * @return the extrema value
	 */
	public double getExtrema();
	
	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations();
	
	/**
	 * Gets any status message.
	 * @return status message
	 */
	public String statusMessage(); 
}
