package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.bme.smile.commons.math.*;
import java.util.ArrayList;
import java.util.List;


public class ImageCollectionAlgebra extends ProcessingAlgorithm{	
	/*******************************************
	 * Input Parameters
	 ******************************************/
	private ParamVolumeCollection ParamVols1;
	private ParamOption operation;	

	/*******************************************
	 * Output Parameters
	 ******************************************/
	private ParamVolume ParamResult;

	private static final String rcsid =
		"$Id: ImageCollectionAlgebra.java,v 1.3 2009/12/15 19:14:06 navid1362 Exp $";
	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");

	private static final String shortDescription = "Basic arithemtic functions (Min, Max, sum, product, mean, median, standard deviation) across a collection of volumes";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams){
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Image Collection Algebra");
		inputParams.setName("Image_Collection_Algebra");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Robert Kim","rkim35@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/***************************************
		 * Creating Inputs
		 ***************************************/
		inputParams.add(ParamVols1=new ParamVolumeCollection("Input Volumes"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Mean","Median","Max","Min","Standard Deviation","Qn","Product","Sum"}));

	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(ParamResult=new ParamVolume("Result Volumes",VoxelType.FLOAT,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		List<ImageData> vol1List = ParamVols1.getImageDataList();

		int N = vol1List.size();

		ImageDataFloat resultVol=new ImageDataFloat(vol1List.get(0));
		resultVol.setName(vol1List.get(0).getName()+"_calc");


		ImageData vol1=vol1List.get(0);
		int rows=vol1.getRows();
		int cols=vol1.getCols();
		int slices=vol1.getSlices();
		int components = vol1.getComponents();
		if(slices<1) slices=1;
		if(components<1) components=1;
		System.out.println("good");
		System.out.flush();



		int i,j,k,m;
		double []tmp = new double[N];
		for (i = 0; i < rows; i++) {
			for (j = 0; j < cols; j++) {
				for (k = 0; k < slices; k++) {
					for(m = 0; m < components; m++) {
						for(int n=0;n<N;n++)
							tmp[n] = vol1List.get(n).getDouble(i, j,k,m);

						switch(operation.getIndex()){
						case 0: //Mean
							resultVol.set(i,j,k,m,StatisticsDouble.mean(tmp));
							break;
						case 1: //Median
							resultVol.set(i,j,k,m,StatisticsDouble.median(tmp));
							break;
						case 2: //max
							resultVol.set(i,j,k,m, StatisticsDouble.max(tmp));
							break;
						case 3://min
							resultVol.set(i,j,k,m,StatisticsDouble.min(tmp));
							break;
						case 4://std
							resultVol.set(i,j,k,m, StatisticsDouble.std(tmp));
							break;
						case 5://Qn
							resultVol.set(i,j,k,m, StatisticsDouble.Qn(tmp));
							break;
						case 6: //Product
							resultVol.set(i,j,k,m,StatisticsDouble.product(tmp));
							break;
						case 7: //Sum
							resultVol.set(i,j,k,m,StatisticsDouble.sum(tmp));
							break;
						default:
						}
					}
				}
			}		
		}

		ParamResult.setValue(resultVol);
	}
}
