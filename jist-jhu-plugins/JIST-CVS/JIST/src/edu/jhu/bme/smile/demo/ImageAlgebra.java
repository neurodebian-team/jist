package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.bme.smile.commons.math.*;
import java.util.ArrayList;
import java.util.List;


public class ImageAlgebra extends ProcessingAlgorithm{	
	/*******************************************
	 * Input Parameters
	 ******************************************/
	private ParamVolumeCollection ParamVols1;
	private ParamOption operation;	
	private ParamInteger dim;
	/*******************************************
	 * Output Parameters
	 ******************************************/
	private ParamVolumeCollection ParamResult;

	private static final String rcsid =
		"$Id: ImageAlgebra.java,v 1.5 2009/12/15 20:10:57 navid1362 Exp $";
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	
	private static final String shortDescription = "Basic arithemtic functions (Min, Max, mean, median, standard deviation) performed along the specified dimension";
	private static final String longDescription = "The given function is only applied along the given dimension. Each of the input images are processed separately.  ";


	protected void createInputParameters(ParamCollection inputParams){
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Image Algebra");
		inputParams.setName("Image_Algebra");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Robert Kim","rkim35@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/***************************************
		 * Creating Inputs
		 ***************************************/
		inputParams.add(ParamVols1=new ParamVolumeCollection("Input Volumes"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Mean","Median","Max","Min","Standard Deviation","Qn"}));
		inputParams.add(dim = new ParamInteger("Dimension",0,3));
		dim.setValue(new Integer(3));
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(ParamResult=new ParamVolumeCollection("Result Volumes"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		List<ImageData> vol1List = ParamVols1.getImageDataList();
		ArrayList<ImageData> listout = new ArrayList<ImageData>();
		int N = vol1List.size();
		for(int l=0;l<N;l++){
			ImageData vol1=vol1List.get(l);
			int rows=vol1.getRows();
			int cols=vol1.getCols();
			int slices=vol1.getSlices();
			int components = vol1.getComponents();
			if(slices<1) slices=1;
			if(components<1) components=1;

			int a=0;int b=0;int c=0;int d=0;
			int i=0;int j=0;int k=0;int m=0;			
			int dim1=1,dim2=2,dim3=3,dim4=4;
			switch(dim.getValue().intValue()) {
			case 0: 
				dim1=1;dim2=2;dim3=3;dim4=0;
				d=rows;a=cols;b=slices;c=components;
				break;
			case 1:
				dim1=0;dim2=3;dim3=1;dim4=2;
				d=cols;a=rows;b=slices;c=components;				
				break;
			case 2:
				dim1=0;dim2=1;dim3=3;dim4=2;
				d=slices;a=rows;b=cols;c=components;
				break;
			case 3: 
				dim1=0;dim2=1;dim3=2;dim4=3;
				d=components;a=rows;b=cols;c=slices;
				break;
			default: 
				throw new RuntimeException("Invalid argument - should NEVER be here due to input validation.");
			}		
			ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",a,b,c);
			resultVol.setHeader(vol1.getHeader());
			double []tmp = new double[d];
			for (i = 0; i < a; i++) {
				for (j = 0; j < b; j++) {
					for (k = 0; k < c; k++) {
						for(m = 0; m < d; m++) {
							int []cur={i,j,k,m};
							tmp[m]=vol1.get(cur[dim1], cur[dim2], cur[dim3], cur[dim4]).doubleValue();															
						}
						switch(operation.getIndex()){
						case 0: //Mean
							resultVol.set(i,j,k,StatisticsDouble.mean(tmp));
							break;
						case 1: //Median
							resultVol.set(i,j,k,StatisticsDouble.median(tmp));
							break;
						case 2: //max
							resultVol.set(i,j,k, StatisticsDouble.max(tmp));
							break;
						case 3://min
							resultVol.set(i,j,k, StatisticsDouble.min(tmp));
							break;
						case 4://std
							resultVol.set(i,j,k, StatisticsDouble.std(tmp));
							break;
						case 5://Qn
							resultVol.set(i,j,k, StatisticsDouble.Qn(tmp));
							break;
						default:
						}
					}
				}
			}		
			listout.add(resultVol);		
		}		
		ParamResult.setValue(listout);
	}
}
