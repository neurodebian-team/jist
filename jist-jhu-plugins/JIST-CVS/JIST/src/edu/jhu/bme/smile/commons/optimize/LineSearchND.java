package edu.jhu.bme.smile.commons.optimize;

import java.util.ArrayList;

/**
 * Uses 1D optimizers to optimize an ND function. It optimizes in one dimension at a time.
 * Once the extrema has been found in one dimension, it goes onto the next dimension and
 * optimizes that one. It keeps changing dimensions until all of them converge to one extrema.
 * @author Hanlin Wan
 */
public class LineSearchND implements OptimizerNDContinuous {

	private double extrema[], higher[], lower[], tol;
	private int dim;
	private String statusMessage;
	private OptimizableNDContinuous func;
	private ProjectNDto1DOptimizable myFunction;
	private Optimizer1DContinuous my1DAlg;
	private int maxIterations, iterations;
	private int maxSteps;
	private boolean logOn = false;
	private ArrayList<double[]> logData;

	/**
	 * Default constructor. Uses BrentMethod1D as the default method
	 */
	public LineSearchND() {
		maxIterations = 1000;
		myFunction = null;
		func = null;
		my1DAlg = new BrentMethod1D();
	}
	
	/**
	 * Constructor to specify what 1D optimizer to use.
	 * @param alg your Optimizer1DContinuous algorithm
	 */
	public LineSearchND(Optimizer1DContinuous alg) {
		maxIterations = 1000;
		myFunction = null;
		func = null;
		my1DAlg = alg;
	}

	/**
	 * Constructor to specify optimizer and maximum iterations
	 * @param alg your Optimizer1DContinuous algorithm
	 * @param maxIter maximum number of iterations
	 */
	public LineSearchND(Optimizer1DContinuous alg, int maxIter) {
		maxIterations = maxIter;
		myFunction = null;
		func = null;
		my1DAlg = alg;
	}

	/**
	 * Sets the tolerance.
	 * @param t tolerance
	 */
	public void setTolerance(double t) {
		tol = t;
	}
	
	/**
	 * Turns on logging for each iteration of the optimization.
	 * @param turnOn true if logging, false if not
	 * @param maxSteps maximum number of steps to log
	 */
	public void logData(boolean turnOn, int maxSteps) {
		logOn = turnOn;
		this.maxSteps = maxSteps;
		logData = new ArrayList<double[]>();
	}
	
	/**
	 * Gets the logged data
	 * @return array of logged data
	 */
	public ArrayList<double[]> getLog() {
		return logData;
	}
	
	/**
	 * Gets the extrema that was found
	 * @return the extrema array of where the extrema is
	 */
	public double[] getExtrema() {
		return extrema;
	}

	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations-1;
	}
	
	/**
	 * Initialize the optimizer with the ND function.
	 * @param function your OptimizableNDContinuous function
	 */
	public void initialize(OptimizableNDContinuous function) {
		dim = function.getNumberOfDimensions();
		extrema = new double[dim];
		func = function;
		myFunction = new ProjectNDto1DOptimizable(function);
		higher = function.getDomainMax();
		lower = function.getDomainMin();
		for (int i = 0; i < dim; i++) extrema[i] = (higher[i]+lower[i])/2;
		tol = function.getDomainTolerance();
	}

	/**
	 * Initialize the optimizer with the ND function. Sets your initial starting point.
	 * @param function your OptimizableNDContinuous function
	 * @param init starting point
	 */
	public void initialize(OptimizableNDContinuous function, double[] init) {
		dim = function.getNumberOfDimensions();
		extrema = new double[dim];
		func = function;
		myFunction = new ProjectNDto1DOptimizable(function);
		higher = function.getDomainMax();
		lower = function.getDomainMin();
		extrema = init;
		tol = function.getDomainTolerance();
	}
	
	/**
	 * Optimizes the function
	 * @param findMinima true if you want to find the minimum, false if you want to find the maximum
	 * @return true if extrema was found successfully, false otherwise
	 */
	public boolean optimize(boolean findMinima) {
		if (myFunction == null)  {
			statusMessage = "Not initialized.";
			return false;			
		}
		
		iterations = 0;
		int curDim = 0;
		double extremaCompare[] = new double[dim];
		
		while (iterations < maxIterations) {
			System.arraycopy(extrema, 0, extremaCompare, 0, extrema.length);
			for(curDim=0; curDim<dim; curDim++) {
				if (logOn && iterations<maxSteps) {
					double[] log = new double[dim+1];
					log[0] = curDim;
					System.arraycopy(extrema, 0, log, 1, dim);
					logData.add(log);
				}
				myFunction.setCurrentDimension(curDim);
				my1DAlg.initialize(myFunction);
				my1DAlg.optimize(findMinima);
				extrema[curDim] = my1DAlg.getExtrema();
				myFunction.setCurrentLocation(extrema[curDim]);
				iterations++;
			}

			double ftol = Math.abs(func.getValue(extremaCompare) - func.getValue(extrema));
			if (ftol <= tol) 
				break;
		}

		if (iterations == maxIterations){
			statusMessage = "Maximum iterations reached.";
			return false;
		}
			
		statusMessage = "Convergence reached";
		return true;
	}

	/**
	 * Gets the status message from the optimizing process
	 * @return string of the status message
	 */
	public String statusMessage() {
		return statusMessage;
	}
}
