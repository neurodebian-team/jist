package edu.jhu.bme.smile.commons.math.specialFunctions;

import java.io.StringWriter;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

public class ComplexMatrix extends Matrix {

	int rowOffset;
	int colOffset;
	public ComplexMatrix(int m, int n) {
		super(2*m, 2*n);
		rowOffset=m;
		colOffset=n;	
	}


	public void setComplex(int m, int n, double real, double imag) {
		set(m,n,real);
		set(m+rowOffset,n+colOffset,real);
		set(m,n+colOffset,imag);
		set(m+rowOffset,n,-imag);
	}

	public double getReal(int m, int n) {
		return get(m,n);
	}

	public double getImag(int m, int n) {
		return get(m,n+colOffset);
	}
	
	public Matrix safeInverse() {
		
			SingularValueDecomposition svd = svd();

			Matrix v = svd.getV();
			Matrix sinv = svd.getS();
			double s1 = sinv.get(1,1);
			double eps = s1*1e-10;
			
			for(int j=0;j<sinv.getColumnDimension();j++) {
				double vv=sinv.get(j,j);
				if(Math.abs(vv)>eps)
					sinv.set(j, j, 1./vv);
			}
			Matrix ut = svd.getU().transpose();
			svd=null;
			return v.times(sinv.times(ut));

		
	}

	public ComplexMatrix inverse() {
		Matrix inv = safeInverse();
		System.out.println(getClass().getCanonicalName()+"\t"+this.getRowDimension()+","+getColumnDimension());
		System.out.println(getClass().getCanonicalName()+"\t"+inv.getRowDimension()+","+inv.getColumnDimension());
		System.out.println(getClass().getCanonicalName()+"\t"+colOffset+","+rowOffset);
		ComplexMatrix cinv = new ComplexMatrix(colOffset,rowOffset);
		for(int i=0;i<colOffset;i++)
			for(int j=0;j<rowOffset;j++) 
				cinv.setComplex(i,j,inv.get(i,j), inv.get(i,j+rowOffset));
		return cinv;
	}
	
	public double[][] getReal() {
		return this.getMatrix(0, rowOffset-1, 0, colOffset-1).getArray();
	}
	
	public double[][] getImag() {
		return this.getMatrix(0,rowOffset-1,  colOffset, 2*colOffset-1).getArray();
	}
	
	public ComplexMatrix times(ComplexMatrix B) {
//		System.out.println(getClass().getCanonicalName()+"\t"+this.getRowDimension()+","+getColumnDimension());
//		System.out.println(getClass().getCanonicalName()+"\t"+B.getRowDimension()+","+B.getColumnDimension());
		Matrix m = super.times(B);
		ComplexMatrix cm = new ComplexMatrix(m.getRowDimension()/2,m.getColumnDimension()/2);
		
		for(int i=0;i<m.getRowDimension()/2;i++)
			for(int j=0;j<m.getColumnDimension()/2;j++) 
				cm.setComplex(i,j,m.get(i,j), m.get(i,j+m.getColumnDimension()/2));
		return cm;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
public String toString() {
	StringWriter out = new StringWriter();
	for(int i=0;i<getRowDimension()/2;i++) {
		for(int j=0;j<getColumnDimension()/2;j++) {
			out.append(getReal(i, j)+"+i*"+getImag(i, j)+" ");
		}
		out.append('\n');
	}	
	return out.toString();
}

}
