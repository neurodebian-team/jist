package edu.jhu.bme.smile.commons.math;

import java.awt.Color;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import ptolemy.plot.Plot;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;

public class KLDivergence {
	double[] histKernelX;
	double[] histKernelY;
	double[] bins;
	double min,max;
	double kld;
	public KLDivergence(){
		
	}
	public double KLdivergence(double[][] histX, double[][] histY,double kernelSize){
		histKernelX=new double[histX.length-1];
		histKernelY=new double[histX.length-1];
		bins=new double[histX.length-1];
		double sumX=0,sumY=0;
		double bval,center;
		min=histX[0][0];
		max=histX[histX.length-1][0];
		for(int b=1;b<histX.length;b++){
			bval=0;
			center=0.5*(histX[b-1][0]+histX[b][0]);
			bins[b-1]=center;
			for(int i=1;i<histX.length;i++){
				bval+=histX[i][1]*Math.exp(-0.5*((0.5*(histX[i-1][0]+histX[i][0])-center)/kernelSize)*((0.5*(histX[i-1][0]+histX[i][0])-center)/kernelSize));
			}
			sumX+=bval;
			histKernelX[b-1]=bval;
			bval =0;
			for(int i=1;i<histY.length;i++){
				bval+=histY[i][1]*Math.exp(-0.5*((0.5*(histY[i-1][0]+histY[i][0])-center)/kernelSize) * ((0.5*(histY[i-1][0]+histY[i][0])-center)/kernelSize));
			}
			sumY+=bval;
			histKernelY[b-1]=bval;
		}
		for(int b=0;b<histX.length-1;b++){
			histKernelX[b]/=sumX;
			histKernelY[b]/=sumY;
		}
		kld=0;
		for(int b=0;b<histX.length-1;b++){
			kld+=(histKernelX[b])*(Math.log((histKernelX[b])/(histKernelY[b])));
		}
		return kld;
	}
	public ImageDataColor getImage(String plotNameX,String plotNameY,String xaxisName,boolean ylogscale,int rows,int cols){
        Plot plot = new Plot();
        plot.setSize(rows,cols);
        plot.setTitle("KL Divergence "+kld);
        plot.setXRange(min,max);
        plot.setXLabel(xaxisName);
        plot.setYLabel("Probability (%)");
        plot.setColor(true);
        plot.setColors(new Color[]{Color.red,Color.blue});
        plot.setMarksStyle("none");
        plot.setYLog(ylogscale);
        plot.addLegend(0, plotNameX);
        plot.addLegend(1, plotNameY);
        for (int i = 0; i <bins.length; i++) {
        	if(histKernelX[i]>1E-6)plot.addPoint(0, bins[i],histKernelX[i],true);
        	if(histKernelY[i]>1E-6)plot.addPoint(1, bins[i],histKernelY[i],true);
        }
        JFrame frame=new JFrame("Histogram Plot");
        frame.add(plot);
        frame.pack();
        frame.setVisible(true);
        BufferedImage img=plot.exportImage();
        frame.setVisible(false);
        frame.dispose();
        Color[][] imgMat=new Color[rows][cols];
        for(int i=0;i<rows;i++){
        	for(int j=0;j<cols;j++){
        		imgMat[i][j]=new Color(img.getRGB(i, j));
        	}
        }
        ImageDataColor imgData=new ImageDataColor(imgMat);
        return imgData;
	}
}
