package edu.jhu.bme.smile.commons.optimize;

/**
 * A wrapper class used to facilitate getting derivatives of ND functions along each dimension individually
 * @author Yufeng Guo, Hanlin Wan
 */
public class FunctionNumericND1DDifferentiation implements OptimizableNDContinuous  {

	public static final int DIFF_FWD_EULER = 0;
	public static final int DIFF_BAC_EULER = 1;
	public static final int DIFF_3_POINT = 3;
	public static final int DIFF_5_POINT = 5;
	public static final int DIFF_7_POINT = 7;
	public static final int DIFF_9_POINT = 9;
	public static final double DEFAULT_STEP = 1e-5;

	private OptimizableNDContinuous myFunction;
	private double myStep;
	private int myMethod;
	private int myDim;

	/**
	 * Creates object to be used for getting a derivative of an ND function along 1-dimension, 
	 * but contains the information to take more derivatives, along another other dimension as well.
	 * Uses the default step of 1e-5 adn default method of 3 point.
	 * @param function function to be differentiated
	 * @param dim the dimension to take the derivative in
	 */
	public FunctionNumericND1DDifferentiation(OptimizableNDContinuous function, int dim) {	
		this(function, dim, DEFAULT_STEP, DIFF_3_POINT);		
	}

	/**
	 * Creates object to be used for getting a derivative of an ND function along 1-dimension, 
	 * but contains the information to take more derivatives, along another other dimension as well.
	 * @param function function to be differentiated
	 * @param dim the dimension to take the derivative in
	 * @param method the differentiating method requested: 0,1,3,5,7, or 9. Default is 3.
	 */
	public FunctionNumericND1DDifferentiation(OptimizableNDContinuous function, int dim, double step, int method) {
		myMethod = method;
		myFunction=function;
		myStep = step;
		myDim = dim;
	}

	private double[] xplus(double []x,double y) {
		double []z = x.clone();
		z[myDim]=z[myDim]+y;
		return z;
	}

	private double[] xminus(double []x,double y) {
		double []z = x.clone();
		z[myDim]=z[myDim]-y;
		return z;
	}
	
	/**
	 * Gets the first derivative. Dimension to take the derivative should have been set.
	 * @param x location to evaluate derivative at
	 * @return derivative at location x in the set dimension
	 */
	public double getDerivative(double []x) {
		switch(myMethod) {
		case DIFF_FWD_EULER: 
			return (myFunction.getValue(xplus(x,myStep)) - myFunction.getValue(x))/myStep;
		case DIFF_BAC_EULER:
			return (myFunction.getValue(x) - myFunction.getValue(xminus(x,myStep)))/myStep;
		case DIFF_3_POINT:
			return (myFunction.getValue(xplus(x,myStep)) - myFunction.getValue(xminus(x,myStep)))/(2*myStep);
		case DIFF_5_POINT:
			return (-myFunction.getValue(xplus(x,2*myStep)) + 8*myFunction.getValue(xplus(x,myStep)) - 8*myFunction.getValue(xplus(x,-myStep)) + myFunction.getValue(xplus(x,-2*myStep))) / (12*myStep);
		case DIFF_7_POINT:
			return (myFunction.getValue(xplus(x,3*myStep)) - 9*myFunction.getValue(xplus(x,2*myStep)) + 45*myFunction.getValue(xplus(x,+myStep)) 
					- 45*myFunction.getValue(xplus(x,-myStep)) + 9*myFunction.getValue(xplus(x,-2*myStep)) - myFunction.getValue(xplus(x,-3*myStep)))/(60*myStep);
		case DIFF_9_POINT:
			return (-3*myFunction.getValue(xplus(x,4*myStep)) + 32*myFunction.getValue(xplus(x,+3*myStep)) - 168*myFunction.getValue(xplus(x,+2*myStep)) 
					+ 672*myFunction.getValue(xplus(x,+myStep)) - 672*myFunction.getValue(xplus(x,-myStep)) + 168*myFunction.getValue(xplus(x,-2*myStep)) - 32*myFunction.getValue(xplus(x,-3*myStep)) 
					+ 3*myFunction.getValue(xplus(x,-4*myStep)))/(840*myStep);
		default: 
			break;
		}
		return Double.NaN;
	}
	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double[] getDomainMax() {	
		return myFunction.getDomainMax();
	}

	/**
	 * Gets the domain minimum
	 * @return domain min
	 */
	public double[] getDomainMin() {
		return myFunction.getDomainMin();
	}

	/**
	 * Gets the tolerance
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunction.getDomainTolerance();
	}

	/**
	 * Gets the number of dimensions
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return myFunction.getNumberOfDimensions();
	}

	/**
	 * Same as getDerivative.
	 * @param x the location to get the derivative.
	 * @return the derivative at location x
	 */
	public double getValue(double[] x) {
		return getDerivative(x);
	}

}
