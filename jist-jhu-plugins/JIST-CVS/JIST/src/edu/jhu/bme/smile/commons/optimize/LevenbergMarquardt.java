package edu.jhu.bme.smile.commons.optimize;

import java.util.ArrayList;

import Jama.Matrix;

/**
 * This optimizer uses the Levenberg Marquardt method in order to find the minimum of a function.
 * You can choose an initial starting point to start the optimization from.
 * Code adapted from http://www.mathworks.com/matlabcentral/fx_files/22778/1/content/optimization/seeMe.htm
 * @author Hanlin Wan, Yufeng Guo
 *
 */
public class LevenbergMarquardt implements OptimizerNDContinuousDifferentiable {

	private double extrema[], tol;
	private OptimizableNDContinuousDifferentiable myFunction;
	private String statusMessage;
	private int dim, maxIterations, iterations;
	private double lambda = 0.001, maxLambda = 1000;
	private int maxSteps;
	private boolean logOn = false;
	private ArrayList<double[][]> logData;
	
	/**
	 * Default constructor for LM. Maximum iterations set to 1000.
	 */
	public LevenbergMarquardt() {
		maxIterations = 1000;
		myFunction = null;
	}
	
	/**
	 * Constructor to let you set the maximum number of iterations.
	 * @param maxIter maximum number of iterations
	 */
	public LevenbergMarquardt(int maxIter) {
		maxIterations = maxIter;
		myFunction = null;
	}
	
	/**
	 * Turns on logging for each iteration of the optimization.
	 * @param turnOn true if logging, false if not
	 * @param maxSteps maximum number of steps to log
	 */
	public void logData(boolean turnOn, int maxSteps) {
		logOn = turnOn;
		this.maxSteps = maxSteps;
		logData = new ArrayList<double[][]>();
	}
	
	/**
	 * Gets the logged data
	 * @return array of logged data
	 */
	public ArrayList<double[][]> getLog() {
		return logData;
	}
	
	/**
	 * Gets the extrema that was found.
	 * @return the extrema array of where the extrema is
	 */
	public double[] getExtrema() {
		return extrema;
	}

	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations;
	}

	/**
	 * Sets the initial and maximum lambda values.
	 * Default values are: initial lambda=0.001; maximum lambda=1000.
	 * @param lamInit initial lambda value
	 * @param lamMax maximum lambda value
	 */
	public void setGamma(double lamInit, double lamMax) {
		lambda = lamInit;
		maxLambda = lamMax;
	}
	
	/**
	 * Initializes the optimizer with a differentiable ND function. You can specify the starting point.
	 * @param function function to minimize
	 * @param init starting point
	 */
	public void initialize(OptimizableNDContinuousDifferentiable function, double[] init) {
		dim = function.getNumberOfDimensions();
		extrema = init;
		tol = function.getDomainTolerance();
		myFunction = function;
	}
	
	/**
	 * Initializes the optimizer with a differentiable ND function. The starting point is set to halfway
	 * between the minimum and maximum domain points in each direction.
	 * @param function function to optimize
	 */
	public void initialize(OptimizableNDContinuousDifferentiable function) {
		dim = function.getNumberOfDimensions();
		extrema = new double[dim];
		for (int i=0; i<dim; i++) 
			extrema[i] = 0.5*(function.getDomainMax()[i]+function.getDomainMin()[i]);
		tol = function.getDomainTolerance();
		myFunction = function;
	}

	/**
	 * Optimizes the function.
	 * @param findMinima true if minimizing, false if maximizing
	 * @return whether the function was successfully optimized
	 */
	public boolean optimize(boolean findMinima) {
		double evalOld, evalNew, extOld[], extNew[] = new double[dim], lam = lambda;
		Matrix eye = Matrix.identity(dim,dim);
		Matrix hessian, gradient, temp;
		Matrix ext;
		
		if (myFunction == null) {
			statusMessage = "Not initialized.";
			return false;			
		}
		
		if (!findMinima) myFunction = new MinToMaxNDDifferentiable(myFunction);
		iterations = 0;
		
		while(iterations<maxIterations) {
			// calculate gradient and hessian
			gradient = new Matrix(myFunction.getDerivative(extrema), dim);
			hessian = new Matrix(myFunction.get2ndDerivative(extrema));
			// calculate new location
			ext = new Matrix(extrema, dim);	
			temp = ext.minus(hessian.plus(eye.times(lam)).inverse().times(gradient));
			extOld = extrema;
			evalOld = myFunction.getValue(extOld);
			extNew = temp.getColumnPackedCopy();
			evalNew = myFunction.getValue(extNew);
			// logging
			if (logOn && iterations<maxSteps) {
				double log[][] = new double[4][dim];
				log[0][0] = lam;
				System.arraycopy(extOld,0,log[1],0,dim);
				System.arraycopy(extNew,0,log[2],0,dim);
				log[3][0] = (evalNew<evalOld) ? 1 : 0;
				logData.add(log);
			}
			// check to see if tolerance reached
			if(lam>maxLambda || Math.abs(Math.pow(evalNew-evalOld,2)) < tol) {
				extrema = extNew;
				break;
			}
			if (evalNew<evalOld) {
				lam*=.1;
				extrema = extNew;
			}
			else
				lam*=10;
			iterations++;
		}
		
		if (iterations >= maxIterations) {
			statusMessage = "Maximum iterations reached.";
			return false;
		}
		
		statusMessage = "Convergence reached";		
		return true;
	}

	/**
	 * Gets the status message.
	 * @return status message
	 */
	public String statusMessage() {
		return statusMessage;
	}

}
