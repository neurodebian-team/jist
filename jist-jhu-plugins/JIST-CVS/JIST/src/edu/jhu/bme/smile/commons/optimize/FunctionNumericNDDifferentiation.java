package edu.jhu.bme.smile.commons.optimize;

/**
 * This class does numeric differentiation in ND.
 * There are lots of methods of doing so, and depending on how many points are used, enter that value in for the method variable.
 * Also computes 2nd derivatives and hessians
 * Defaults to 3-point method.
 * @author Yufeng Guo, Hanlin Wan
 */
public class FunctionNumericNDDifferentiation implements OptimizableNDContinuousDifferentiable {

	private OptimizableNDContinuous myFunction;
	private int myMethod;
	private int dim;
	private double myStep;

	/**
	 * Creates an object which can differentiate an ND function with default values.
	 * Step is 1e-5 and method is 3-point.
	 * @param function the function to be differentiated
	 */
	public FunctionNumericNDDifferentiation(OptimizableNDContinuous function) {
		myMethod = 3;
		myFunction = function;
		dim = function.getNumberOfDimensions();
		myStep = 1e-5;
	}
	
	/**
	 * Creates an object which can differentiate an ND function in various ways.
	 * @param function the function to be differentiated
	 * @param step the step size
	 * @param method the differentiating method requested: 0,1,3,5,7, or 9. Default is 3.
	 */
	public FunctionNumericNDDifferentiation(OptimizableNDContinuous function, double step, int method) {
		myMethod = method;
		myFunction = function;
		dim = function.getNumberOfDimensions();
		myStep = step;
	}

	/**
	 * Gets 2nd partial derivative for the dimensions given
	 * @param x a double array that contains the location of the derivative required
	 * @param dim1 the first dimension of the derivative
	 * @param dim2 the second dimension of the derivative
	 * @return the 2nd derivative
	 */
	public double get2ndDerivative(double[] x,int dim1, int dim2) {
		OptimizableNDContinuous d = 
			new FunctionNumericND1DDifferentiation(new FunctionNumericND1DDifferentiation(myFunction,dim1,myStep,myMethod),dim2,myStep,myMethod);
		return d.getValue(x);		
	}

	/**
	 * Creates the hessian matrix
	 * @param x location to evaluate derivative at
	 * @return hessian matrix at location x
	 */
	public double[][] get2ndDerivative(double[] x) {
		double [][]diff2 = new double[dim][dim]; 
		for(int i=0;i<dim;i++)
			for(int j=0;j<dim;j++) {
				if(j<i)
					diff2[i][j]=diff2[j][i];
				else
					diff2[i][j] = get2ndDerivative(x,i,j);
			} 
		return diff2;
	}

	/**
	 * Gets the gradient
	 * @param x location to evaluate derivative at
	 * @return gradient at location x
	 */
	public double[] getDerivative(double[] x) {
		double diff[] = new double[dim];		
		for(int i=0; i<dim; i++) {
			OptimizableNDContinuous d = new FunctionNumericND1DDifferentiation(myFunction,i,myStep,myMethod);
			diff[i] = d.getValue(x);
		}
		return diff;
	}

	/**
	 * Gets the method used
	 * @return method
	 */
	public int getMethod() {
		return this.myMethod;
	}

	/**
	 * Gets the step size used
	 * @return step size
	 */
	public double getStep() {
		return this.myStep;
	}

	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double[] getDomainMax() {	
		return myFunction.getDomainMax();
	}

	/**
	 * Gets the domain minimum
	 * @return domain min
	 */
	public double[] getDomainMin() {
		return myFunction.getDomainMin();
	}

	/**
	 * Gets the tolerance
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunction.getDomainTolerance();
	}
	
	/**
	 * Gets the value
	 * @param x location to get value at
	 * @return value at location x
	 */
	public double getValue(double[] x) {
		return myFunction.getValue(x);
	}

	/**
	 * Gets the number of dimensions
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return dim;
	}
}
