package edu.jhu.bme.smile.commons.optimize.tensor;

import Jama.Matrix;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;
import edu.jhu.bme.smile.commons.optimize.DownhillSimplexND;
import edu.jhu.bme.smile.commons.optimize.FunctionNumericNDDifferentiation;
import edu.jhu.bme.smile.commons.optimize.GoldenSectionSearch1D;
import edu.jhu.bme.smile.commons.optimize.LevenbergMarquardt;
import edu.jhu.bme.smile.commons.optimize.LineSearchND;
import edu.jhu.bme.smile.commons.optimize.LineSearchNDDifferentiable;
import edu.jhu.bme.smile.commons.optimize.NewtonMethod1D;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuous;

/**
 * This class is used to minimize the least likelihood estimator from data from diffusion weighted imaging.
 * It appears that all the algorithms but Line Search using Newton's method are very stable. The Line Searches
 * are fairly slow since they have to iterate through the optimizer so many times. Both Downhill Simplex
 * and LM are very fast, with Downhill Simplex only slightly faster with this function. With nicer functions
 * LM can be significantly faster than Downhill Simplex as demonstrated by the optimization of simpler functions
 * in TestNDOptimizations. 
 * 
 * @author Hanlin Wan
 *
 */
public class MLTensor  {

	Matrix imgMatrix;
	private double[] mat, obsDW;
	private double[] truth, sig, obs;
	private double[] dMin, dMax;
	private double obsB0, sigmaFactor, dFactor = 1e5, tol = 1e-5, myStep = 1;
	private int iterations, myMethod = 3;
	private Matrix myD;
	public static double PLUS_EPSILON = 1e-6;

	/**
	 * Initializes MLTensor with these parameters
	 * @param imgMatrix image matrix
	 * @param b0 b0
	 * @param sigma standard deviation
	 * @param obsB0 observed b0
	 * @param obsDW observed diffusion weighted data
	 * @param dInit initial tensor
	 */
	public void init(Matrix imgMatrix, double b0, double sigma, double obsB0, double []obsDW, double []dInit) {
		this.imgMatrix = imgMatrix;
		sigmaFactor = b0 / sigma;
		mat = new double[8];
		myD = new Matrix(6,1);
		mat[0] = b0;
		mat[1] = sigma * sigmaFactor;
		for (int i=2; i<8; i++) 
			mat[i] = 0;
		this.obsB0 = obsB0;
		this.obsDW = obsDW.clone();
		for (int i=0; i<dInit.length; i++) mat[i+2] = dInit[i] * dFactor;
		this.iterations = 0;
		// set default domain minimum and maximum
		dMax = new double[8];
		dMax[0] = mat[0] * 2;
		dMax[1] = mat[1] * 2;
		for (int i=0; i<6; i++) {
			if (dInit[i]*2 < 1e-3) dMax[i+2] = 1e-3 * dFactor;
			else dMax[i+2] = dInit[i] * 2 * dFactor;
		}
		dMin = new double[8];
		dMin[0] = mat[0] * 0.5;;
		for (int i=1; i<8; i++) dMin[i] = 0;
	}

	/**
	 * Lets you specify which algorithm to use to optimize all 8 parameters at once. 
	 * @param method 0 - Downhill Simplex; 1 - LM; 2 - Line Search using Golden Section Search 
	 * 3 - Line Search using Brent's Method; 4 - Line Search using Newton
	 * @return whether the optimization was successful
	 */
	public boolean optimize8D(int method) {		
		boolean status = false;
		int size = obsDW.length + 1;
		truth = new double[size];
		sig = new double[size];
		obs = new double[size];
		obs[0] = obsB0;
		System.arraycopy(obsDW, 0, obs, 1, obsDW.length);

		if (method == 0) {						// Downhill Simplex
			DownhillSimplexND alg = new DownhillSimplexND();
			opt8D opt = new opt8D(this);
			alg.initialize(opt, mat, 20);
			status = alg.optimize(true);
			iterations = alg.getIterations();
		} 
		else if (method == 1) {					// LM
			LevenbergMarquardt alg = new LevenbergMarquardt();
			opt8D opt = new opt8D(this);
			alg.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), mat);
			status = alg.optimize(true);
			iterations = alg.getIterations();
		}
		else if (method == 2 || method == 3) {	// Line Search - Golden Section Search and Brent's Method
			LineSearchND alg;
			if (method == 2) alg = new LineSearchND(new GoldenSectionSearch1D());
			else alg = new LineSearchND(new BrentMethod1D());
			opt8D opt = new opt8D(this);
			alg.initialize(opt);
			status = alg.optimize(true);
			iterations = alg.getIterations();
		}
		else if (method == 4) {					// Line Search - Newton
			LineSearchNDDifferentiable alg = new LineSearchNDDifferentiable(new NewtonMethod1D());
			opt8D opt = new opt8D(this);
			alg.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod));
			status = alg.optimize(true);
			iterations = alg.getIterations();
		}
		return status;
	}

	/**
	 * Lets you specify which algorithm to use and the maximum number of iterations.
	 * It will optimize alternate between optimizing D and optimizing b0 and sigma.
	 * @param method 0 - Downhill Simplex; 1 - LM; 2 - Line Search using Golden Section Search
	 * 3 - Line Search using Brent's; 4 - Line Search using Newton
	 * @param maxIter maximum number of iterations
	 * @return whether the optimization was successful
	 */
	public boolean optimize6D2D(int method, int maxIter) {
		boolean status = false;
		int size = obsDW.length + 1;
		truth = new double[size];
		sig = new double[size];
		obs = new double[size];
		obs[0] = obsB0;
		System.arraycopy(obsDW, 0, obs, 1, obsDW.length);
		double extremaCompare[] = new double[8];
		opt6D2D opt = new opt6D2D(this);
		double[] D6 = new double[6];
		double[] D2 = new double[2];
		
		if (method == 0) {		// Downhill Simplex
			DownhillSimplexND alg = new DownhillSimplexND();
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);
				
				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg.initialize(opt, D6, 20);
				alg.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg.initialize(opt, D2, 20);
				alg.optimize(true);
				
				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}
		}
		else if (method == 1){		// LM
			LevenbergMarquardt alg = new LevenbergMarquardt();
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);
				
				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, 6);
				alg.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D6);
				alg.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, 2);
				alg.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D2);
				alg.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}	
		}
		else if (method == 2 || method == 3) {		// Line Search - Golden Section Search and Brent's Method
			LineSearchND alg;
			if (method == 2) alg = new LineSearchND(new GoldenSectionSearch1D());
			else alg = new LineSearchND(new BrentMethod1D());
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg.initialize(opt, D6);
				alg.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg.initialize(opt, D2);
				alg.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}
		}
		else if (method == 4) {		// Line Search - Newton
			LineSearchNDDifferentiable alg = new LineSearchNDDifferentiable(new NewtonMethod1D());
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D6);
				alg.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D2);
				alg.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
				
			}		
		}
		return status;
	}	

	/**
	 * Calculates the least likelihood value
	 * @return LLvalue
	 */
	public double LLvalue() {
		truth[0] = getB0();
		Matrix modelDW = (imgMatrix.times(getD()));		
		for (int i=0; i<modelDW.getRowDimension(); i++) 
			truth[i+1] = getB0() * Math.exp(modelDW.get(i,0));
		for (int i=0; i<=obsDW.length; i++) 
			sig[i] = getSigma();
		double val=-1*(StatisticsDouble.logRicianPDF(truth, sig, obs));
		if(val==Double.NaN)
			return Double.MAX_VALUE;
		return val;

	}
	
	/**
	 * Gets the value of b0
	 * @return b0
	 */
	public double getB0() {
		return Math.abs(mat[0]);
	}

	/**
	 * Gets the value of sigma. If sigma is less than 1e-6, 1e-6 is returned.
	 * @return sigma
	 */
	public double getSigma() {
		double s = Math.abs(mat[1] / sigmaFactor);
		return (s<PLUS_EPSILON?PLUS_EPSILON:s); // sigma MUST be finite and positive
	}

	/**
	 * Gets the D matrix
	 * @return D
	 */
	public Matrix getD() {
		for (int i=2; i<8; i++) 
			myD.set(i-2,0, mat[i] / dFactor);
		return myD;
	}
	
	/**
	 * Sets the scaling factor for sigma. You want a value that when multiplied by
	 * sigma, you get a number in the same order of magnitude as b0. This is to 
	 * ensure that when optimizing, the parameters are not too far apart. The default 
	 * sigmaFactor is set to be b0/(10*sigma).
	 * @param s number to scale sigma by
	 */
	public void setSigmaFactor(double s) {
		mat[1] = (mat[1] / sigmaFactor) * s;
		sigmaFactor = s;
	}
	
	/**
	 * Sets the scaling factor for D. The default is set to 1e6.
	 * @param d number to scale D by
	 */
	public void setDFactor(double d) {
		for (int i=2; i<8; i++) 
			myD.set(i-2,0, mat[i] / dFactor * d);
		dFactor = d;
	}
	
	/**
	 * Sets the tolerance. Default value is 0.001. 
	 * @param t tolerance
	 */
	public void setTolerance(double t) {
		tol = t;
	}

	/**
	 * Sets the step size used in calculating derivatives. The default is set to 0.1.
	 * @param s step size
	 */
	public void setStepSize(double s) {
		myStep = s;
	}
	
	/**
	 * Sets the method used to calculate the derivatives. The default is set to 3-point.
	 * @param m method
	 */
	public void setMethod(int m) {
		myMethod = m;
	}
	
	/**
	 * Sets the domain minimum and maximum values. The default values are as follows:
	 * b0 and sigma - 0.5 to 2 times the initial value
	 * D - minimum is 0. Maximum is 2 times the initial value but is at least 1e-3.
	 * @param min domain minimum array of size 8
	 * @param max domain maximum array of size 8
	 */
	public void setDomain(double []min, double []max) {
		dMin[0] = min[0];
		dMax[0] = max[0];
		dMin[1] = min[1] * sigmaFactor;
		dMax[1] = max[1] * sigmaFactor;
		for (int i=2; i<8; i++) {
			dMin[i] = min[i] * dFactor;
			dMax[i] = max[i] * dFactor;
		}
	}
	
	/**
	 * Gets the number of iterations required to calculate the result.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations + 1;
	}

	/**
	 * Optimizes using all 8 parameters at the same time.
	 */
	private class opt8D implements OptimizableNDContinuous {
		MLTensor parent;	

		/**
		 * Constructor to create the object to optimize 8 dimensions at once.
		 * @param par parent MLTensor object
		 */
		public opt8D(MLTensor par) {
			parent = par;
		}
		
		/**
		 * Gets the tolerance.
		 * @return tolerance
		 */
		public double getDomainTolerance() {
			return tol;
		}

		/**
		 * Gets the domain maximum.
		 * @return domain max
		 */
		public double[] getDomainMax() {
			return dMax;
		}

		/**
		 * Gets the domain minimum
		 * @return domain min
		 */
		public double[] getDomainMin() {
			return dMin;
		}

		/**
		 * Gets the number of dimensions.
		 * @return number of dimensions
		 */
		public int getNumberOfDimensions() {
			return 8;
		}

		/**
		 * Gets the value of the function at location x.
		 * @param x location to evaluate function
		 * @return value at x
		 */
		public double getValue(double[] x) {
			System.arraycopy(x, 0, mat, 0, x.length);
			return LLvalue();
		}
	}

	/**
	 * Pptimizes using D then b0 and sigma.
	 */
	private class opt6D2D implements OptimizableNDContinuous {
		MLTensor parent;	
		boolean sixD;

		/**
		 * Constructor to create the object to optimize 6 dimensions and then 2 dimensions.
		 * @param par parent MLTensor object
		 */
		public opt6D2D(MLTensor par) {
			this.parent = par;
		}
		
		/**
		 * Gets the tolerance.
		 * @return tolerance
		 */
		public double getDomainTolerance() {
			return tol;
		}

		/**
		 * Sets which set of variables to optimize.
		 * @param sixD true if optimizing D, false if optimizing b0 and sigma
		 */
		public void setDim(boolean sixD) {
			this.sixD = sixD;
		}
		
		/**
		 * Gets the domain maximum.
		 * @return domain max
		 */
		public double[] getDomainMax() {
			if (sixD) {
				double[] max = new double[6];
				System.arraycopy(dMax, 2, max, 0, 6);
				return max;
			}
			else {
				double[] max = new double[2];
				System.arraycopy(dMax, 0, max, 0, 2);
				return max;
			}
		}

		/**
		 * Gets the domain minimum.
		 * @return domain min
		 */
		public double[] getDomainMin() {
			if (sixD) {
				double[] min = new double[6];
				System.arraycopy(dMin, 2, min, 0, 6);
				return min;
			}
			else {
				double[] min = new double[2];
				System.arraycopy(dMin, 0, min, 0, 2);
				return min;
			}
		}

		/**
		 * Gets the number of dimensions.
		 * @return number of dimensions
		 */
		public int getNumberOfDimensions() {
			if (sixD) return 6;
			else return 2;
		}

		/**
		 * Gets the value of the function at location x.
		 * @param x location to evaluate function
		 * @return value at x
		 */
		public double getValue(double[] x) {
			if (sixD) 
				System.arraycopy(x, 0, mat, 2, x.length);
			else 
				System.arraycopy(x, 0, mat, 0, x.length);
			return LLvalue();
		}
	}
}