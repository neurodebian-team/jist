package edu.jhu.bme.smile.commons.optimize.tensor;
import java.util.Random;

import Jama.Matrix;

/**
 * This class is to test the different optimizers in a simple real-world case.
 * All ND optimizers are used to optimize tensors acquired from diffusion weighted imaging.
 * @author Hanlin Wan
 *
 */
public class OptimizeTensor {
	private static double b0 = 1000, bvalue = 700, sigma = 50;
	private static double g[][] = new double[12][3];
	private static double[] obsDW;
	private static double obsB0;
	private static Matrix imgMatrix;
	
	public static void main(String[] args){
		MLTensor tensor = new MLTensor();
	
		String meth[] = new String[]{"opt6D2D ", "opt8D "};
		String alg[] = new String[]{"Downhill Simplex", "LM", "Line Search - Golden Section Search", "Line Search - Brent's Method"};
		//, "Line Search - Newton"};

		int nMax=50;
		double[] initD = new double[]{0.8e-3,0.2e-3,0.2e-3,1.8e-3,0.2e-3,2.8e-3};
		double[][] totalLL = new double[alg.length][meth.length];
		double[][] totalIter = new double[alg.length][meth.length];
		long[][] totalTime = new long[alg.length][meth.length];
		
		for (int n=1; n<=nMax; n++) {
			randTensor();
			System.out.println("jist.plugins"+"\t"+"Running iteration " + n + " of " + nMax + ".");
			for (int i=0; i<alg.length; i++) {
				for (int j=0; j<meth.length; j++) {
					boolean status = false;
					long time = System.currentTimeMillis();
					tensor.init(imgMatrix, b0, sigma, obsB0, obsDW, initD);
					if (j==0) {
						status = tensor.optimize6D2D(i,2);
						totalTime[i][j] += (System.currentTimeMillis() - time);
						totalLL[i][j] += tensor.LLvalue();
						totalIter[i][j] += tensor.getIterations();
					}
					else {
						status = tensor.optimize8D(i);
						totalTime[i][j] += (System.currentTimeMillis() - time);
						totalLL[i][j] += tensor.LLvalue();
						totalIter[i][j] += tensor.getIterations();
					}
					if (n==nMax) {
						System.out.println("jist.plugins"+"\t"+"\n" + meth[j] + alg[i]);
						System.out.println("jist.plugins"+"\t"+"Converged:  " + status);
						System.out.println("jist.plugins"+"\t"+"Avg Iters:  " + totalIter[i][j]/nMax);
						System.out.println("jist.plugins"+"\t"+"Avg Time:   " + totalTime[i][j]/(float)nMax);
						System.out.println("jist.plugins"+"\t"+"Avg LL:     " + totalLL[i][j]/nMax);
						System.out.println("jist.plugins"+"\t"+"b0:         " + tensor.getB0());
						System.out.println("jist.plugins"+"\t"+"sigma:      " + tensor.getSigma());
						double[] D = tensor.getD().getColumnPackedCopy();
						System.out.print  ("D:          [");
						for (int k=0; k<D.length; k++) System.out.print(D[k] + " ");
						System.out.println("jist.plugins"+"\t"+"]");
					}
				}
			}
		}
	}
	
	/**
	 * Creates a random tensor.
	 */
	public static void randTensor() {
		for (int i=0; i<12; i++)
			for (int j=0; j<3; j++)
				g[i][j] = Math.random() - 0.5;
		Matrix G = normalizeGrad(g);
		imgMatrix = makeImgMatrix(G, bvalue);
		Matrix GG = makeImgMatrix(G, -1);
		Matrix d = new Matrix(new double[]{1e-3,0,0,2e-3,0,3e-3},6);
		Random r = new Random();
		obsB0 = Math.round(Math.sqrt(Math.pow(b0+sigma*r.nextGaussian(), 2) + Math.pow(r.nextGaussian(), 2)));
		obsDW = GG.times(d).times(-1*bvalue).getColumnPackedCopy();
		for (int i=0; i<obsDW.length; i++) {
			obsDW[i] = b0 * Math.exp(obsDW[i]) + sigma * r.nextGaussian();
			obsDW[i] = Math.round(Math.sqrt(Math.pow(obsDW[i], 2) + Math.pow(sigma * r.nextGaussian(), 2)));
		}
	}
	
	/**
	 * normalizes the gradient matrix
	 * @param g gradient matrix to normalize
	 * @return normalized gradient matrix
	 */
	public static Matrix normalizeGrad(double[][] g) {
		double r[] = new double[g.length];
		for (int i=0; i<g.length; i++) {
			double sum = 0;
			for (int j=0; j<g[0].length; j++)
				sum += Math.pow(g[i][j],2);
			sum = Math.sqrt(sum);
			for (int j=0; j<g[0].length; j++)
				g[i][j] /= sum;
			r[i] = sum;
		}
		return new Matrix(g);
	}
	
	/**
	 * makes the imgMatrix
	 * @param m normalized gradient matrix
	 * @param bvalue the bvalue
	 * @return imgMatrix
	 */
	public static Matrix makeImgMatrix(Matrix m, double bvalue) {
		int rows = m.getRowDimension();
		Matrix imagMatrix = new Matrix(rows,6);
		rows--;
		Matrix x = m.getMatrix(0,rows,0,0);
		Matrix y = m.getMatrix(0,rows,1,1);
		Matrix z = m.getMatrix(0,rows,2,2);
        imagMatrix.setMatrix(0,rows,0,0,x.arrayTimes(x).times(bvalue*-1));		//xx
        imagMatrix.setMatrix(0,rows,1,1,x.arrayTimes(y).times(2*bvalue*-1));	//2xy
        imagMatrix.setMatrix(0,rows,2,2,x.arrayTimes(z).times(2*bvalue*-1));	//2xz
        imagMatrix.setMatrix(0,rows,3,3,y.arrayTimes(y).times(bvalue*-1));		//yy
        imagMatrix.setMatrix(0,rows,4,4,y.arrayTimes(z).times(2*bvalue*-1));	//2yz
        imagMatrix.setMatrix(0,rows,5,5,z.arrayTimes(z).times(bvalue*-1));		//zz
		return imagMatrix;
	}
}
