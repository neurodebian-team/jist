package edu.jhu.bme.smile.commons.optimize;

import java.util.ArrayList;

/**
 * Class GoldenSectionSearch1D
 * Uses the gold section search algorithm to find the minima of a 1D function
 * @author Yufeng Guo
 * 
 * */
public class GoldenSectionSearch1D implements Optimizer1DContinuous {
	
	private double extrema, tol, x0, x1, x2, x3;
	private String statusMessage;
	private Optimizable1DContinuous myFunction;
	private int maxIterations, iterations;
	private int maxSteps;
	private boolean logOn = false;
	private ArrayList<double[]> logData;
	
	/**
	 * Default constructor
	 * Sets iterations to 1000*/
	public GoldenSectionSearch1D() {
		maxIterations = 1000;
		extrema = Double.NaN;
		myFunction = null;
	}
	
	/**
	 * Constructor that allows setting the maximum number of iterations.
	 * @param max iterations desired*/
	public GoldenSectionSearch1D(int maxIter) {
		maxIterations = maxIter;
		extrema = Double.NaN;
		myFunction = null;
	}
	
	/**
	 * Turns on logging for each iteration of the optimization.
	 * @param turnOn true if logging, false if not
	 * @param maxSteps maximum number of steps to log
	 */
	public void logData(boolean turnOn, int maxSteps) {
		logOn = turnOn;
		this.maxSteps = maxSteps;
		logData = new ArrayList<double[]>();
	}
	
	/**
	 * Gets the logged data
	 * @return array of logged data
	 */
	public ArrayList<double[]> getLog() {
		return logData;
	}
	
	/**
	 * Gets the extrema that was found
	 * @return the extrema array of where the extrema is
	 */
	public double getExtrema() {
		return extrema;
	}

	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations;
	}
	
	/**
	 * Sets up the optimization.
	 * @param function to be optimized*/
	public void initialize(Optimizable1DContinuous function) {		
		myFunction = function;
		tol = myFunction.getDomainTolerance();
		x0=myFunction.getDomainMin();
		x3=myFunction.getDomainMax();
		extrema = Double.NaN;
	}
	
	/**Optimizes the function
	 * @param findMinima true if looking for minimum, false for maximum
	 * @return true only if minimum is found
	 * */
	public boolean optimize(boolean findMinima) {
		if(myFunction==null)  {
			statusMessage = "Not initialized. ";
			return false;
		}
		if (!findMinima) myFunction = new MinToMax1D(myFunction);
		double P = (3-Math.sqrt(5))/2.0;
		x1 = x0 + P*(x3-x0);
		x2 = x1 + P*(x3-x1);
		iterations = 0;
		boolean df;
		while ((Math.abs(x3-x0) > tol) && (iterations < maxIterations)) {
			if (logOn && iterations<maxSteps) logData.add(new double[]{x0,x1,x2,x3});
			df = myFunction.getValue(x2) <= myFunction.getValue(x1);
			if(df) {
				x0=x1; x1=x2;
				x2 = x1 + P*(x3-x1);
			}
			else {
				x3=x2; x2=x1;
				x1=x2 - P*(x2-x0);
			}
			iterations++;
		}
		if (iterations==maxIterations) {
			statusMessage = "Max iterations reached. ";
			return false;
		}
		
		extrema = myFunction.getValue(x1)<myFunction.getValue(x2) ? x1 : x2;
		statusMessage = "Convergence reached";
		return true;
	}

	/**
	 * Gets the status message from the optimizing process
	 * @return string of the status message
	 */
	public String statusMessage() {
		return statusMessage;
	}

}
