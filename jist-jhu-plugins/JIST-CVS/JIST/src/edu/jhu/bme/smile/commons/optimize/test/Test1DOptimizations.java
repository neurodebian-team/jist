package edu.jhu.bme.smile.commons.optimize.test;

import edu.jhu.bme.smile.commons.optimize.NewtonMethod1D;
import edu.jhu.bme.smile.commons.optimize.GoldenSectionSearch1D;
import edu.jhu.bme.smile.commons.optimize.Optimizable1DContinuousDifferentiable;
import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;
import java.util.*;

/**
 * Test class to test all 1D optimizers.
 * @author Yufeng Guo, Hanlin Wan
 *
 */
public class Test1DOptimizations {

	public static int iterations = 1000, pickedFunc = 1;
	static long time = 0;
	static double trueExtrema = Math.random() * 100, error = 0;
	//default function
	static boolean min = true;
	static Optimizable1DContinuousDifferentiable func = new FunctionXSquared(trueExtrema,-500,500);
	static String function = "y = x^2 + 10 ";
	
	public static void main(String[] args) {
		
		int t = -1;
		while(t != 0) {
			System.out.println("jist.plugins"+"\t"+"\n***************************************");
			System.out.println("jist.plugins"+"\t"+"Method for testing Optimizers.");
			if (min) System.out.print("Minimizing: ");
			else System.out.print("Maximizing: ");
			System.out.println("jist.plugins"+"\t"+function + "\n");
			System.out.println("jist.plugins"+"\t"+"1. Change function");
			System.out.println("jist.plugins"+"\t"+"2. Set Min Max");
			System.out.println("jist.plugins"+"\t"+"3. Test All 1D Methods ");
			System.out.println("jist.plugins"+"\t"+"\t4. NewtonMethod1D ");
			System.out.println("jist.plugins"+"\t"+"\t5. GoldenSectionSearch1D ");
			System.out.println("jist.plugins"+"\t"+"\t6. BrentMethod1D ");
			System.out.println("jist.plugins"+"\t"+"0. Quit ");
			System.out.println("jist.plugins"+"\t"+"***************************************\n");
			Scanner kb = new Scanner(System.in);
			t = kb.nextInt();
			
			switch(t) {
			case 1:
				changeFunc();
				break;
			case 2:
				changeMinMax();
				break;
			case 3:
				NewtonMethod1D();
				GoldenSectionSearch1D();
				BrentMethod1D();
				break;
			case 4: 
				NewtonMethod1D();
				break;
			case 5:
				GoldenSectionSearch1D();
				break;
			case 6:
				BrentMethod1D();
				break;
			default: break;
			}
		}
	}
	
	/**
	 * Randomizes the extrema.
	 */
	private static void randomizeExtrema() {
		trueExtrema = Math.random() * 100;
	}

	/**
	 * Brent's Method
	 */
	public static void BrentMethod1D() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nBrentMethod1D: Starting");
		BrentMethod1D opt = null;
		for(int j = 0; j < iterations; j++) {
			opt = new BrentMethod1D(1000);
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(min);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			error += Math.pow((trueExtrema-opt.getExtrema()),2);

		}		
		printResults("BrentMethod1D", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}
	
	/**
	 * Golden Section Search
	 */
	public static void GoldenSectionSearch1D() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nGoldenSectionSearch: Starting");
		GoldenSectionSearch1D opt = null;
		for(int j = 0; j < iterations; j++) {
			opt = new GoldenSectionSearch1D(1000);
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(min);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			error += Math.pow((trueExtrema-opt.getExtrema()),2);

		}		
		printResults("GoldenSectionSearch", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}
	
	/**
	 * Newton
	 */
	public static void NewtonMethod1D() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nNewtonMethod1D: Starting");
		NewtonMethod1D opt=null;
		for(int j = 0; j < iterations; j++) {
			opt = new NewtonMethod1D();
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(min);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			error += Math.pow((trueExtrema-opt.getExtrema()),2);
		}	
		printResults("NewtonMethod1D", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}
	
	/**
	 * Changes the function to test.
	 */
	public static void changeFunc() {
		System.out.println("jist.plugins"+"\t"+"Pick the function you want to optimize: ");
		System.out.println("jist.plugins"+"\t"+"\t1. y = x^2 + 10 ");
		System.out.println("jist.plugins"+"\t"+"\t2. y = 10 - x^2 ");
		System.out.println("jist.plugins"+"\t"+"\t3. y = x^2 + 4*sin(x^2) + 10 ");		
		Scanner kb = new Scanner(System.in);
		pickedFunc = kb.nextInt();
		funcInit();
		System.out.println("jist.plugins"+"\t"+"Function Changed.");
	}
	
	/**
	 * Changes between finding minimum and maximum.
	 */
	public static void changeMinMax() {
		System.out.println("jist.plugins"+"\t"+"Choose minimize or maximize: ");
		System.out.println("jist.plugins"+"\t"+"\t1. Minimize ");
		System.out.println("jist.plugins"+"\t"+"\t2. Maximize ");
		Scanner kb = new Scanner(System.in);
		if (kb.nextInt() == 1) min = true;
		else min = false;
	}
	
	/**
	 * Initializes the function.
	 */
	public static void funcInit() {
		randomizeExtrema() ;
		switch(pickedFunc) {
		case 1:
			func = new FunctionXSquared(trueExtrema, -500, 500);
			function = "y = x^2 + 10 ";
			break;
		case 2: 
			func = new FunctionNegativeXSquared(trueExtrema,-500, 500);
			function = "y = -x^2 + 10 ";
			break;
		case 3:
			func = new FunctionXXplusSinXX(trueExtrema, -500, 500);
			function = "y = x^2 + 4*sin(x^2) + 10 ";
			break;
		default: 
			System.out.println("jist.plugins"+"\t"+"Please enter valid number for function selection");
			changeFunc();
			break;
		}
	}
	
	/**
	 * Resets all variables.
	 */
	public static void resetVariables() {
		trueExtrema = Math.random() * 100;
		error = 0;	
		time = 0;
		func = null;
	}
	
	/**
	 * Prints the results.
	 * @param alg algorithm used
	 * @param status status message
	 * @param time time spent optimizing
	 * @param tEx true extrema
	 * @param nEx calculated extrema
	 * @param error error
	 */
	public static void printResults(String alg, String status, long time, double tEx, double nEx, double error) {
		System.out.println("jist.plugins"+"\t"+alg + ": Completed in " + ((double)time) / iterations + " ms");
		System.out.println("jist.plugins"+"\t"+"\tLast Status:          " + status);
		System.out.println("jist.plugins"+"\t"+"\tLast True Extrema:    " + tEx);
		System.out.println("jist.plugins"+"\t"+"\tLast Numeric Extrema: " + nEx);
		System.out.println("jist.plugins"+"\t"+"\tMean Squared Error:   " + error / iterations);
		System.out.println("jist.plugins"+"\t"+alg + ": Finished");
	}

}
