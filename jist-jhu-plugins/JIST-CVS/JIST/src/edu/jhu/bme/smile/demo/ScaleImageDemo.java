package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;


public class ScaleImageDemo extends ProcessingAlgorithm{	
	/*******************************************
	 * Input Parameters
	 ******************************************/
	private ParamVolume ParamVol;
	private ParamFloat ParamScale;

	/*******************************************
	 * Output Parameters
	 ******************************************/
	private ParamVolume ParamResult;

	private static final String rcsid =
		"$Id: ScaleImageDemo.java,v 1.4 2009/12/15 20:22:28 navid1362 Exp $";
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Scale the image by the given facctor";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams){
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Scale Image");
		inputParams.setName("Scale_Image");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/***************************************
		 * Creating Inputs
		 ***************************************/
		inputParams.add(ParamVol=new ParamVolume("Input Volume",null,-1,-1,-1,-1));
		inputParams.add(ParamScale=new ParamFloat("Scale"));		
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(ParamResult=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		ImageData img = ParamVol.getImageData();
		ImageDataMath.scaleFloatValue(img, ParamScale.getFloat());
		img.setName(img.getName()+"ScaledBy"+ParamScale.getFloat());
		ParamResult.setValue(img);
	}
}
