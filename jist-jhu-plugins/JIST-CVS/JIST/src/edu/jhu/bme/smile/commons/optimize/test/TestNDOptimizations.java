package edu.jhu.bme.smile.commons.optimize.test;

import java.util.*;
import edu.jhu.bme.smile.commons.optimize.DownhillSimplexND;
import edu.jhu.bme.smile.commons.optimize.GoldenSectionSearch1D;
import edu.jhu.bme.smile.commons.optimize.LevenbergMarquardt;
import edu.jhu.bme.smile.commons.optimize.LineSearchND;
import edu.jhu.bme.smile.commons.optimize.LineSearchNDDifferentiable;
import edu.jhu.bme.smile.commons.optimize.NewtonMethod1D;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuousDifferentiable;
import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;

/**
 * Test class to test the ND optimizers.
 * @author Yufeng Guo, Hanlin Wan
 *
 */
public class TestNDOptimizations {

	public static int iterations = 1000, pickedFunc = 1;
	static long time = 0;
	static double error = 0;
	//default function
	static boolean min = true;
	static int dim;
	static double low[] = {-500, -500, -500};
	static double high[] = {500, 500, 500};
	static double trueExtrema[] = {Math.random() * 100, Math.random() * 100, Math.random() * 100};
	static OptimizableNDContinuousDifferentiable func = new FunctionXSquaredND(dim, trueExtrema, low, high);
	static String function = "f(x1, x2, x3) = x1^2 + x2^2 + x3^2 + 10 ";
	
	public static void main(String[] args) {
	
		int t = -1;
		while(t != 0) {
			System.out.println("jist.plugins"+"\t"+"\n***************************************");
			System.out.println("jist.plugins"+"\t"+"Method for testing Optimizers.");
			if (min) System.out.print("Minimizing: ");
			else System.out.print("Maximizing: ");
			System.out.println("jist.plugins"+"\t"+function + "\n");
			System.out.println("jist.plugins"+"\t"+"1. Change function");
			System.out.println("jist.plugins"+"\t"+"2. Change Min Max");
			System.out.println("jist.plugins"+"\t"+"3. Test All Methods ");
			System.out.println("jist.plugins"+"\t"+"\t4. NewtonMethod1D ");
			System.out.println("jist.plugins"+"\t"+"\t5. GoldenSectionSearch1D ");
			System.out.println("jist.plugins"+"\t"+"\t6. BrentMethod1D ");
			System.out.println("jist.plugins"+"\t"+"\t7. Downhill Simplex Method ");
			System.out.println("jist.plugins"+"\t"+"\t8. LM Method ");
			System.out.println("jist.plugins"+"\t"+"0. Quit ");
			System.out.println("jist.plugins"+"\t"+"***************************************\n");
			Scanner kb = new Scanner(System.in);
			t = kb.nextInt();
			
			switch(t) {
			case 1: 
				changeFunc();
				break;
			case 2:
				changeMinMax();
				break;
			case 3:
				NewtonMethod();
				GoldenSectionSearch();
				BrentMethod1D();
				DownhillSimplex();
				LM();
				break;
			case 4: 
				NewtonMethod();
				break;
			case 5:
				GoldenSectionSearch();
				break;
			case 6:
				BrentMethod1D();
				break;
			case 7:
				DownhillSimplex();
				break;
			case 8:
				LM();
			default: break;
			}
		}
	}
	
	/**
	 * Randomizes the extrema.
	 */
	private static void randomizeExtrema() {
		for (int i=0; i<dim; i++) trueExtrema[i] = Math.random() * 100;
	}

	/**
	 * LM
	 */
	public static void LM() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nLM: Starting");
		LevenbergMarquardt opt = null;
		for(int j = 0; j < 1; j++) {
			opt = new LevenbergMarquardt();//can add parameters here
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(true);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			for (int i=0; i<dim; i++)
				error += Math.pow((trueExtrema[i]-opt.getExtrema()[i]),2);

		}		
		printResults("LM", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}
	
	/**
	 * Line Search using Brent's Method
	 */
	public static void BrentMethod1D() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nND Line Search using BrentMethod1D: Starting");
		LineSearchND opt = null;
		for(int j = 0; j < iterations; j++) {
			opt = new LineSearchND(new BrentMethod1D(1000));
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(true);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			for (int i=0; i<dim; i++)
				error += Math.pow((trueExtrema[i]-opt.getExtrema()[i]),2);

		}		
		printResults("ND Line Search using BrentMethod1D", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}

	/**
	 * Line Search using Golden Section Search
	 */
	public static void GoldenSectionSearch() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nND Line Search using GoldenSectionSearch: Starting");
		LineSearchND opt = null;
		for(int j = 0; j < iterations; j++) {
			opt = new LineSearchND(new GoldenSectionSearch1D(1000));
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(min);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			for (int i=0; i<dim; i++)
				error += Math.pow((trueExtrema[i]-opt.getExtrema()[i]),2);

		}		
		printResults("ND Line Search using GoldenSectionSearch", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}
	
	/**
	 * Line Search using Newton
	 */
	public static void NewtonMethod() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nND Line Search using NewtonMethod1D: Starting");
		LineSearchNDDifferentiable opt=null;
		for(int j = 0; j < iterations; j++) {
			opt = new LineSearchNDDifferentiable(new NewtonMethod1D());
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(min);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			for (int i=0; i<dim; i++)
				error += Math.pow((trueExtrema[i]-opt.getExtrema()[i]),2);
		}	
		printResults("ND Line Search using NewtonMethod1D", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}
	
	/**
	 * Downhill Simplex
	 */
	public static void DownhillSimplex() {
		resetVariables();
		System.out.println("jist.plugins"+"\t"+"\nDownhill Simplex: Starting");
		DownhillSimplexND opt = null;
		for(int j = 0; j < iterations; j++) {
			opt = new DownhillSimplexND();
			funcInit();
			opt.initialize(func);
			long t0 = System.currentTimeMillis();
			opt.optimize(min);
			long t1 = System.currentTimeMillis();
			time += t1-t0;
			for (int i=0; i<dim; i++)
				error += Math.pow((trueExtrema[i]-opt.getExtrema()[i]),2);

		}		
		printResults("Downhill Simplex", opt.statusMessage(), time, trueExtrema, opt.getExtrema(), error);
	}

	/**
	 * Changes the function to optimize.
	 */
	public static void changeFunc()
	{
		System.out.println("jist.plugins"+"\t"+"Pick the function you want to optimize: ");
		System.out.println("jist.plugins"+"\t"+"\t1. f(x1, x2, x3) = x1^2 + x2^2 + x3^2 ");
		System.out.println("jist.plugins"+"\t"+"\t2. f(x1, x2, x3) = -x1^2 - x2^2 - x3^2 ");
		System.out.println("jist.plugins"+"\t"+"\t3. f(x1, x2, x3) = x1^2 + 4*sin(x1^2) + x2^2 + 4*sin(x2^2) + x3^2 + 4*sin(x3^2) + 10 ");
		System.out.println("jist.plugins"+"\t"+"\t4. f(x, y) = x^2 + y^2 + 0.4*x^2*y^2 ");
		Scanner kb = new Scanner(System.in);
		pickedFunc = kb.nextInt();
		funcInit();
		System.out.println("jist.plugins"+"\t"+"Function Changed.");
	}
	
	/**
	 * Changes between finding the minimum and maximum.
	 */
	public static void changeMinMax() {
		System.out.println("jist.plugins"+"\t"+"Choose minimize or maximize: ");
		System.out.println("jist.plugins"+"\t"+"\t1. Minimize ");
		System.out.println("jist.plugins"+"\t"+"\t2. Maximize ");
		Scanner kb = new Scanner(System.in);
		if (kb.nextInt() == 1) min = true;
		else min = false;
	}
	
	/**
	 * Initializes the function.
	 */
	public static void funcInit()
	{
		randomizeExtrema() ;
		switch(pickedFunc) {
		case 1:
			func = new FunctionXSquaredND(3, trueExtrema, low, high);
			dim = 3;
			function = "f(x1, x2, x3) = x1^2 + x2^2 + x3^2 + 10 ";
			break;
		case 2:
			func = new FunctionNegativeXSquaredND(3, trueExtrema, low, high);
			dim = 3;
			function = "f(x1, x2, x3) = -x1^2 - x2^2 - x3^2 + 10 ";
			break;
		case 3:
			func = new FunctionXXplusSinXXND(3, trueExtrema, low, high);
			dim = 3;
			function = "f(x1, x2, x3) = x1^2 + 4*sin(x1^2) + x2^2 + 4*sin(x2^2) + x3^2 + 4*sin(x3^2) + 10 ";
			break;
		case 4:
			func = new FunctionXXYYND(2, trueExtrema, low, high);
			dim = 2;
			function = "f(x, y) = x^2 + y^2 + 0.4*x^2*y^2 ";
			break;
		default: 
			System.out.println("jist.plugins"+"\t"+"Please enter valid number for function selection");
			changeFunc();
			break;
		}
	}
	
	/**
	 * Resets the variables.
	 */
	public static void resetVariables() {
		randomizeExtrema();
		error = 0;	
		time = 0;
		func = null;
	}
	
	/**
	 * Prints the results.
	 * @param alg algorithm used
	 * @param status status message
	 * @param time time spent optimizing
	 * @param tEx true extrema
	 * @param nEx calculated extrema
	 * @param error error
	 */
	public static void printResults(String alg, String status, long time, double tEx[], double nEx[], double error) {
		System.out.println("jist.plugins"+"\t"+alg + ": Completed in " + ((double)time) / iterations + " ms");
		System.out.println("jist.plugins"+"\t"+"\tLast Status:          " + status);
		System.out.print("\tLast True Extrema:    (");
		for (int i=0; i<dim; i++) System.out.print(tEx[i] + ",");
		System.out.println("jist.plugins"+"\t"+")");
		System.out.print("\tLast Numeric Extrema: (");
		for (int i=0; i<nEx.length; i++) System.out.print(nEx[i] + ",");
		System.out.println("jist.plugins"+"\t"+")");
		System.out.println("jist.plugins"+"\t"+"\tMean Squared Error:   " + error / iterations);
		System.out.println("jist.plugins"+"\t"+alg + ": Finished");
	}

}
