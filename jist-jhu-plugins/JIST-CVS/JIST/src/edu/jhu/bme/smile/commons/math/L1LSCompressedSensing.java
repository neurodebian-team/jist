package edu.jhu.bme.smile.commons.math;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.sparse.CompRowMatrix;
import Jama.Matrix;

/* Derived from the ls_l1 Matlab package.
 * This port is made available under GPL (as opposed to LGPL (for the rest of the package).
 *   
 * Adapted by Bennett Landman <landman@jhu.edu> and Hanlin Wan <hwan1@jhu.edu>
 * 2009.07.09
 * 
	% AUTHOR    Kwangmoo Koh <deneb1@stanford.edu>
	% UPDATE    Apr 8 2007
	%
	% COPYRIGHT 2008 Kwangmoo Koh, Seung-Jean Kim, and Stephen Boyd
	%
	% l1-Regularized Least Squares Problem Solver
	%
	%   l1_ls solves problems of the following form:
	%
	%       minimize ||A*x-y||^2 + lambda*sum|x_i|,
	%
	%   where A and y are problem data and x is variable (described below).


 * % AUTHOR    Kwangmoo Koh <deneb1@stanford.edu>
	% UPDATE    Apr 10 2008
	%
	% COPYRIGHT 2008 Kwangmoo Koh, Seung-Jean Kim, and Stephen Boyd
	%
	% l1-Regularized Least Squares Problem Solver
	%
	%   l1_ls solves problems of the following form:
	%
	%       minimize   ||A*x-y||^2 + lambda*sum(x_i),
	%       subject to x_i >= 0, i=1,...,n
	%
	%   where A and y are problem data and x is variable (described below).
	%   x       : n vector; classifier
 */

public class L1LSCompressedSensing {

	//	% IPM PARAMETERS
	protected double MU              = 2;        // updating parameter of t
	protected double MAX_NT_ITER     = 100;      // maximum IPM (Newton) iteration

	//	% LINE SEARCH PARAMETERS
	protected double ALPHA           = 0.01;     // minimum fraction of decrease in the objective
	protected double BETA            = 0.5;      // stepsize decrease factor
	protected double MAX_LS_ITER     = 100;      // maximum backtracking line search iteration

	protected Matrix A;
	protected Matrix At;
	protected Matrix AtA2;
	protected Matrix x; 				// the result
	protected int m; //	%   m       : number of examples (rows) of A
	protected int n; //	%   n       : number of features (column)s of A
	protected DenseMatrix cgA;
	protected CompRowMatrix pre;
	
	protected boolean statusConverged = false;
	protected boolean verbose = false;

	public L1LSCompressedSensing(double [][]A) {
		this(new Matrix(A));
	}
	
	public double findLambdaMax(double []y) {
		double [][]y1 = new double[1][];
		y1[0]=y;
		Matrix m = At.times(new Matrix(y1).transpose());
		return m.normInf()*2;		
	}

	public L1LSCompressedSensing(Matrix A) {
		this.A=A;
		At = this.A.transpose();
		AtA2 = At.times(this.A).times(2);
		n = this.A.getColumnDimension();
		m = this.A.getRowDimension();
		statusConverged=false;
		verbose = false;
	}

	public boolean solve(double []y, double lambda) {
		double [][]y1 = new double[1][];
		y1[0]=y;
		return solve(new Matrix(y1).transpose(), lambda);
	}

	public boolean solve(double []y, double lambda, double reltol) {
		return solve(y, lambda, reltol, -1);
	}

	public boolean solve(double []y, double lambda, double reltol, double maxError) {
		return solve(y, lambda, reltol, maxError, false);
	}

	public boolean solve(double []y, double lambda, double reltol, double maxError, boolean allowRestart) {
		double [][]y1 = new double[1][];
		y1[0]=y;
		return solve(new Matrix(y1).transpose(), lambda, reltol, maxError, allowRestart);
	}

	public boolean solve(Matrix y, double lambda) {
		return solve(y,lambda,1e-3, 10, false);
	}

	public boolean solve(Matrix y, double lambda, double reltol, double maxError, boolean allowRestart) {
		return false;
	}
		
	public boolean isConvereged() {
		return statusConverged;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose; 
	}

	public Matrix getMatrixResult() {
		if(x==null)
			return null;
		return (Matrix)x.clone();
	}

	public double[] getResult() {
		double []res = new double[x.getRowDimension()];
		for(int i=0;i<x.getRowDimension();i++)
			res[i]=x.get(i,0);
		return res;
	}
}
