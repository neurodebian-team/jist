package edu.jhu.bme.smile.commons.math;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.sparse.CG;
import no.uib.cipr.matrix.sparse.CompRowMatrix;
import no.uib.cipr.matrix.sparse.DefaultIterationMonitor;
import no.uib.cipr.matrix.sparse.DiagonalPreconditioner;
import no.uib.cipr.matrix.sparse.IterativeSolver;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.Preconditioner;
import Jama.Matrix;

/**
 * Positively constrained least squares solver
 * @author Hanlin Wan <hanlinwan@gmail.com>
 *
 */
public class L1LSCompressedSensingNonNeg extends L1LSCompressedSensing {

	public L1LSCompressedSensingNonNeg(double[][] A) {
		this(new Matrix(A));
	}
	
	public double findLambdaMax(double []y) {
		double [][]y1 = new double[1][];
		y1[0]=y;
		Matrix m = At.times(new Matrix(y1).transpose());
		double mx = Double.NEGATIVE_INFINITY;
		for(int i=0;i<m.getRowDimension();i++) {
			if(mx<m.get(i,0))
				mx = m.get(i,0);			
		}
		return 2*mx;		
	}
	
	public L1LSCompressedSensingNonNeg(Matrix A) {
		super(A);
		cgA = new DenseMatrix(AtA2.getArray());
		int[][] nz = new int[n][1];
		for (int i=0; i<n; i++) nz[i][0]=i;
		pre = new CompRowMatrix(n,n,nz);
	}
	
	public boolean solve(Matrix y, double lambda, double reltol, double maxError, boolean allowRestart) {

		x = new Matrix(n,1,0.01);
		if (allowRestart) 
			MAX_LS_ITER = 20;
		double t0 = Math.min(Math.max(1,1/lambda), n/1e-3);
		double t = t0;

		if(verbose) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Solving a problem of size (m="+m+", n="+n+"), with lambda="+lambda);
			System.out.println(getClass().getCanonicalName()+"\t"+"-----------------------------------------------------------------------------");
			System.out.println(getClass().getCanonicalName()+"\t"+"iter\tgap\tprimobj\tdualobj\tstep\tlen\tpcg\titers");
		}
		double pobj = Double.POSITIVE_INFINITY; 
		double dobj = Double.NEGATIVE_INFINITY; 
		double s = Double.POSITIVE_INFINITY;
		double pitr = 0 ; 
		int ntiter = 0;
		int lsiter = 0; 			
		int errorCount = 0;
		double prevError = -1;
		double minAnu, gap, relError, phi, newphi;
		
		Matrix dx = new Matrix(n,1);
		Matrix z = A.times(x).minus(y); //z = A*x-y;
		Matrix nu, newx=null, newz=null;
		DenseVector cgX = new DenseVector(n);
		DenseVector cgB = new DenseVector(n);
		IterativeSolver solver = new CG(cgX);
		if (allowRestart)
			solver.setIterationMonitor(new DefaultIterationMonitor(1000,0.1,1e-5,1e3));
		Preconditioner M = new DiagonalPreconditioner(n);
		solver.setPreconditioner(M);

		for(ntiter = 0;ntiter<=MAX_NT_ITER;ntiter++) {

			nu = z.times(2); //nu = 2*z;
			Matrix Atz2 = At.times(nu);
			minAnu = MatrixMath.min(Atz2);
			if (minAnu < -lambda)
				nu = nu.times(lambda/(-minAnu));

			pobj = MatrixMath.dotProduct(z,z)+lambda*MatrixMath.sum(x); //pobj  =  z'*z+lambda*sum(x,1);
			dobj = Math.max(MatrixMath.dotProduct(nu,nu)*(-.25)-MatrixMath.dotProduct(nu,y),dobj); //dobj  =  max(-0.25*nu'*nu-nu'*y,dobj);
			gap  = pobj - dobj;
			if(verbose) {
				System.out.println(getClass().getCanonicalName()+"\t"+ntiter+"\t"+gap+"\t"+pobj+"\t"+dobj+"\t"+s+"\t"+pitr);
			}

			//		    %------------------------------------------------------------
			//		    %   STOPPING CRITERION
			//		    %------------------------------------------------------------
			relError = gap/Math.abs(dobj);
			if (relError < reltol) { 
				if(verbose)  
					System.out.println(getClass().getCanonicalName()+"\t"+"Converged.");
				statusConverged=true;
				return statusConverged;
			}
			else if (maxError > 0 && relError > maxError) {
				if (verbose)
					System.out.println(getClass().getCanonicalName()+"\t"+"Error too high.");
				if (prevError==-1) { errorCount++; prevError = relError; }
				else {
					if (relError > prevError) { errorCount++; prevError=relError; }
					else { errorCount=0; prevError=-1; }
				}
				if (errorCount==3) {
					if(verbose)  
						System.out.print("FAILED1\t");
					statusConverged=false;
					return statusConverged;
				}
			}
			else { errorCount=0; prevError=-1; }			
	
			//		    %------------------------------------------------------------
			//		    %       UPDATE t
			//		    %------------------------------------------------------------
			if (s >= 0.5) 
				t = Math.max(Math.min(n*MU/gap, MU*t), t); //        t = max(min(n*MU/gap, MU*t), t);

			//		    %------------------------------------------------------------
			//		    %       CALCULATE NEWTON STEP
			//		    %------------------------------------------------------------
			for (int i=0; i<n; i++) {
				pre.set(i, i, 1/(x.get(i,0)*x.get(i,0)*t));		// d1 = (1/t)./(x.^2);
				cgA.set(i, i, AtA2.get(i,i)+pre.get(i,i));		// 2*A'*A+diag(d1)
				cgB.set(i, -Atz2.get(i,0)-lambda+1/(t*x.get(i,0)));	// -gradphi = -[At*(z*2)+lambda-(1/t)./x]; 
			}
			M.setMatrix(pre);

			try {
				solver.solve(cgA, cgB, cgX);
			}
			catch (IterativeSolverNotConvergedException e) {
				System.err.println(getClass().getCanonicalName()+"Iterative solver failed to converge");
			}

			// Writes the data back into dx
			for (int i=0; i<n; i++) {
				dx.set(i,0,cgX.get(i));
			}

			//		    %------------------------------------------------------------
			//		    %   BACKTRACKING LINE SEARCH
			//		    %------------------------------------------------------------
			phi = MatrixMath.dotProduct(z,z)+lambda*MatrixMath.sum(x)-MatrixMath.sum(MatrixMath.log(x))/t;
			s = 1.0;
			double gdx = 0;
			for (int i=0; i<n; i++) gdx += x.get(i,0)*cgB.get(i)*-1;
			for (lsiter=1; lsiter<=MAX_LS_ITER; lsiter++) {
				newx = x.plus(dx.times(s));		// newx = x+s*dx;
				if (MatrixMath.min(newx) > 0) {
					newz = A.times(newx).minus(y);
					newphi = MatrixMath.dotProduct(newz,newz)+lambda*MatrixMath.sum(newx)-MatrixMath.sum(MatrixMath.log(newx))/t;		            
					if (newphi-phi <= ALPHA*s*gdx)
						break;
				}
				s = BETA*s;
			}
			
			if (lsiter > MAX_LS_ITER) {	
				if (allowRestart && ntiter<5) { // restart
					if (verbose)
						System.out.print("RESTART\t");
					solver.setIterationMonitor(new DefaultIterationMonitor());
					x = new Matrix(n,1,0.01);
					z = A.times(x).minus(y);
					s = Double.POSITIVE_INFINITY;
					MAX_LS_ITER = 100;
				}
				else
					break;
			}
			else {
				x = newx; z = newz;
				if (allowRestart && ntiter>5)
					MAX_LS_ITER = 100;
			}
		}
		

		statusConverged=false;
		if(verbose) {
			if (lsiter > MAX_LS_ITER) {		    
				System.out.println(getClass().getCanonicalName()+"\t"+"MAX_LS_ITER exceeded in BLS"); 
			} else if (ntiter > MAX_NT_ITER) {
				System.out.println(getClass().getCanonicalName()+"\t"+"MAX_NT_ITER exceeded.");

			} else {
				System.out.println(getClass().getCanonicalName()+"\t"+"Unknown failure result.");
			}
		}
		if(verbose)  
			System.out.print("FAILED2\t");
		return true;
	}
}
