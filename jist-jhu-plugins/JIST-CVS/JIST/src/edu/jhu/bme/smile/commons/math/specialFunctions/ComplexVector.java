package edu.jhu.bme.smile.commons.math.specialFunctions;

import Jama.Matrix;

public class ComplexVector {
	ComplexMatrix result; 
	int rowOffset;
	public ComplexVector(ComplexMatrix r) {
		result=r;		
	}

	public ComplexMatrix getMatrix() {return result;};

	public double getReal(int i) {
		return result.getReal(i, 0);
	}
	public double getImag(int i) {
		return result.getImag(i, 0);
	}
	public int length() {
		return result.rowOffset;
	}
	public double[] getRealResult() {
		double [][]r=result.getReal();

		double []real = new double[r.length];
		for(int i=0;i<real.length;i++)			
			real[i]=r[i][0];
		return real;
	}

	public double[] getImagResult() {
		double [][]r=result.getImag();

		double []imag = new double[r.length];
		for(int i=0;i<imag.length;i++)			
			imag[i]=r[i][0];
		return imag;
	}
	
	public String toString() {
		return result.toString();
	}
}
