package edu.jhu.bme.smile.commons.math;

import Jama.Matrix;

/**
 *
 *  This class handles 3x3 rotation matrices with
 *	inverse Rodrigues parametrization.
 *	
 * If you have any questions, please drop me a line.
 * =====
 * Pierre-Louis Bazin
 * MEDIC, JHU
 * pbazin1@jhmi.edu
 * 
 *	@version    November 4, 2004
 *	@author     Pierre-Louis Bazin
 *		
 * Modified (slightly) by Bennett Landman, landman@jhu.edu.
 *
 */

public class RotationMatrix {

	/*	private double[][]	matrix;
	private double[]		parameters;

	public RotationMatrix() {
		matrix = new double[3][3];
		parameters = new double[3];

        for (int i=0;i<3;i++) {
            for (int j=0;j<3;j++) matrix[i][j] = 0.0f;
            matrix[i][i] = 1.0f;
            parameters[i] = 0.0f;
        }       
	}

    public double getParameter(int i) {
        return parameters[i];
    }

    public double []getParameters() {
        return parameters;
    }

    public double getMatrix(int i,int j) {
        return matrix[i][j];
    }



    public void setParameters(double a, double b, double c) {
    	double x, a2, b2, c2;
    	double an, bn, cn, n, m;

        // set the parameters
        parameters[0] = a; parameters[1] = b; parameters[2] = c;

        // compute the new matrix
        n = (double)Math.sqrt( (a*a + b*b + c*c) );
        if (n >= 1.0) {
            // reproject onto 0,2 values
            m = n % 2;
            an = a * m / n;
            bn = b * m / n;
            cn = c * m / n;
        } else {
            an = a;
            bn = b;
            cn = c;
        }

        a2 = an*an; b2 = bn*bn; c2 = cn*cn;
        x = (double)Math.sqrt( (1.0f - a2 - b2 - c2) );

        matrix[0][0] = 1.0f - 2.0f * b2 - 2.0f * c2;
        matrix[1][0] = 2.0f * an * bn + 2.0f * x * cn;
        matrix[2][0] = 2.0f * an * cn - 2.0f * x * bn;
        matrix[0][1] = 2.0f * an * bn - 2.0f * x * cn;
        matrix[1][1] = 1.0f - 2.0f * a2 - 2.0f * c2;
        matrix[2][1] = 2.0f * bn * cn + 2.0f * x * an;
        matrix[0][2] = 2.0f * an * cn + 2.0f * x * bn;
        matrix[1][2] = 2.0f * bn * cn - 2.0f * x * an;
        matrix[2][2] = 1.0f - 2.0f * a2 - 2.0f * b2;

        return;
    }
    public void setParameters(double[] param) {
        setParameters(param[0],param[1],param[2]);
    }



    public void derivatives(double[][] dR, double da, double db, double dc) {
    	double x, x2, a2, b2, c2, ab, ac, bc, ax, bx, cx;
        double an, bn, cn, n, m;
        double ZERO = 1e-6f;

        n = (double)Math.sqrt(parameters[0]*parameters[0] + parameters[1]*parameters[1] + parameters[2]*parameters[2]);
        if (n >= 1.0) {
            // reproject onto 0,2 values
            m = n % 2;
            an = parameters[0] * m / n;
            bn = parameters[1] * m / n;
            cn = parameters[2] * m / n;
        } else {
            an = parameters[0];
            bn = parameters[1];
            cn = parameters[2];
        }

        a2 = an*an; b2 = bn*bn; c2 = cn*cn;
        ab = an*bn; ac = an*cn; bc = bn*cn;
        x2 = 1.0f - a2 - b2 - c2;
        x = (double)Math.sqrt(x2);
        ax = x*an; bx = x*bn; cx = x*cn;

        //* should not occur: if x=0 then a=b=c=0 then x=1... *
        if (x < ZERO) System.out.println("jist.plugins"+"\t"+"x<0 in rotation");

        dR[0][0] = -4.0f * (bn * db + cn * dc);
        dR[1][0] = 2.0f / x * ((bx - ac) * da + (ax - bc) * db + (x2 - c2) * dc);
        dR[2][0] = 2.0f / x * ((cx + ab) * da + (b2 - x2) * db + (ax + bc) * dc);
        dR[0][1] = 2.0f / x * ((bx + ac) * da + (ax + bc) * db + (c2 - x2) * dc);
        dR[1][1] = -4.0f * (an * da + cn * dc);
        dR[2][1] = 2.0f / x * ((x2 - a2) * da + (cx - ab) * db + (bx - ac) * dc);
        dR[0][2] = 2.0f / x * ((cx - ab) * da + (x2 - b2) * db + (ax - bc) * dc);
        dR[1][2] = 2.0f / x * ((a2 - x2) * da + (cx + ab) * db + (bx + ac) * dc);
        dR[2][2] = -4.0f * (an * da + bn * db);

        return;
    }
    public double[][] derivatives(double da, double db, double dc) {
    	double[][] dR = new double[3][3];
        derivatives(dR, da, db, dc);
        return dR;
    }


	public double[] computeEulerAngles() {
		double[] euler = new double[3];
		double cphi,sphi,ctheta,stheta,cpsi,spsi;

		ctheta = matrix[2][2];
		stheta = Math.sqrt(1.0f-ctheta*ctheta);

		if (stheta>0) {
			cphi = -matrix[2][1]/stheta;
			sphi =  matrix[2][0]/stheta;

			cpsi =  matrix[1][2]/stheta;
			spsi =  matrix[0][2]/stheta;

			euler[0] = (double)Math.atan2(sphi,cphi);
			euler[1] = (double)Math.atan2(stheta,ctheta);
			euler[2] = (double)Math.atan2(spsi,cpsi);

		} else {
			cphi = matrix[0][0];
			sphi = matrix[0][1];

			euler[0] = (double)Math.atan2(sphi,cphi);
			euler[1] = 0.0f;
			euler[2] = 0.0f;
		}

        return euler;
    }

    public double twoNorm() {
        return (double)Math.sqrt(parameters[0]*parameters[0]+parameters[1]*parameters[1]+parameters[2]*parameters[2]);
    }


    public void setParameters(double a, double b, double c) {
        double x, a2, b2, c2;
        double an, bn, cn, n, m;

		// reproject in appropriate space
        n = (double)Math.sqrt( (double)(a*a + b*b + c*c) );
        if (n > 1.0f) {
			// reproject onto [-1,1] instead
            m = n % 2;
			if (m>1.0) {
				an = m * a / n - 2 * a / n;
				bn = m * b / n - 2 * b / n;
				cn = m * c / n - 2 * c / n;
			} else {
				an = a * m / n;
				bn = b * m / n;
				cn = c * m / n;
			}
        } else {
            an = a;
            bn = b;
            cn = c;
        }

        // set the parameters
        parameters[0] = an; parameters[1] = bn; parameters[2] = cn;

        // compute the new matrix
        a2 = an*an; b2 = bn*bn; c2 = cn*cn;
        x = (double)Math.sqrt( (double)(1.0f - a2 - b2 - c2) );

		if (double.isNaN(x)) System.out.print("! x^2 < 0: "+a2+", "+b2+", "+c2+"\n");

        matrix[0][0] = 1.0f - 2.0f * b2 - 2.0f * c2;
        matrix[1][0] = 2.0f * an * bn + 2.0f * x * cn;
        matrix[2][0] = 2.0f * an * cn - 2.0f * x * bn;
        matrix[0][1] = 2.0f * an * bn - 2.0f * x * cn;
        matrix[1][1] = 1.0f - 2.0f * a2 - 2.0f * c2;
        matrix[2][1] = 2.0f * bn * cn + 2.0f * x * an;
        matrix[0][2] = 2.0f * an * cn + 2.0f * x * bn;
        matrix[1][2] = 2.0f * bn * cn - 2.0f * x * an;
        matrix[2][2] = 1.0f - 2.0f * a2 - 2.0f * b2;

        return;
    }
    public void setParameters(double[] param) {
        setParameters(param[0],param[1],param[2]);
    }
    public void setParameters(double[] param, int offset) {
        setParameters(param[offset+0],param[offset+1],param[offset+2]);
    }

    public void setMatrix(double[][] mat) {
        double x;//, a2, b2, c2;

        // set the matrix
        for (int i=0;i<3;i++) for (int j=0;j<3;j++)
            matrix[i][j] = mat[i][j];

        // compute the new parameters
        x = (double)Math.sqrt(mat[0][0] + mat[1][1] + mat[2][2] + 1.0f)/ 2.0f;

        if (x > 0.0) {
            parameters[0] = (mat[2][1] / x - mat[1][2] / x) / 4.0f;
            parameters[1] = (mat[0][2] / x - mat[2][0] / x) / 4.0f;
            parameters[2] = (mat[1][0] / x - mat[0][1] / x) / 4.0f;
        } else {
            if ((mat[0][0] < 1.0) && (mat[1][1] < 1.0) && (mat[2][2] < 1.0)) {
                parameters[0] = (double)Math.sqrt( (mat[0][1]*mat[0][1] + mat[0][2]*mat[0][2]) 
                                                    / (2.0f*(1.0 - mat[0][0])) );
                parameters[1] = (double)Math.sqrt( (mat[1][0]*mat[1][0] + mat[1][2]*mat[1][2]) 
                                                    / (2.0f*(1.0 - mat[1][1])) );
                parameters[2] = (double)Math.sqrt( (mat[2][0]*mat[2][0] + mat[2][1]*mat[2][1]) 
                                                    / (2.0f*(1.0 - mat[2][2])) );
                if (mat[0][1] < 0.0) parameters[1] = -parameters[1];
                if (mat[0][2] < 0.0) parameters[2] = -parameters[2];
            } else if (mat[0][0] == 1.0f) {
                parameters[0] = 1.0f;
                parameters[1] = 0.0f;
                parameters[2] = 0.0f;
            } else if (mat[1][1] == 1.0f) {
                parameters[0] = 0.0f;
                parameters[1] = 1.0f;
                parameters[2] = 0.0f;
            } else if (mat[2][2] == 1.0f) {
                parameters[0] = 0.0f;
                parameters[1] = 0.0f;
                parameters[2] = 1.0f;
            } 
        }
    }

    public void setMatrix(double[][] mat) {
        double x;//, a2, b2, c2;

        // set the matrix
        for (int i=0;i<3;i++) for (int j=0;j<3;j++)
            matrix[i][j] = (double)mat[i][j];

        // compute the new parameters
        x = (double)Math.sqrt(mat[0][0] + mat[1][1] + mat[2][2] + 1.0f)/ 2.0f;

        if (x > 0.0) {
            parameters[0] = (double) ( (mat[2][1] / x - mat[1][2] / x) / 4.0 );
            parameters[1] = (double) ( (mat[0][2] / x - mat[2][0] / x) / 4.0 );
            parameters[2] = (double) ( (mat[1][0] / x - mat[0][1] / x) / 4.0 );
        } else {
            if ((mat[0][0] < 1.0) && (mat[1][1] < 1.0) && (mat[2][2] < 1.0)) {
                parameters[0] = (double)Math.sqrt( (mat[0][1]*mat[0][1] + mat[0][2]*mat[0][2]) 
                                                    / (2.0f*(1.0 - mat[0][0])) );
                parameters[1] = (double)Math.sqrt( (mat[1][0]*mat[1][0] + mat[1][2]*mat[1][2]) 
                                                    / (2.0f*(1.0 - mat[1][1])) );
                parameters[2] = (double)Math.sqrt( (mat[2][0]*mat[2][0] + mat[2][1]*mat[2][1]) 
                                                    / (2.0f*(1.0 - mat[2][2])) );
                if (mat[0][1] < 0.0) parameters[1] = -parameters[1];
                if (mat[0][2] < 0.0) parameters[2] = -parameters[2];
            } else if (mat[0][0] == 1.0f) {
                parameters[0] = 1.0f;
                parameters[1] = 0.0f;
                parameters[2] = 0.0f;
            } else if (mat[1][1] == 1.0f) {
                parameters[0] = 0.0f;
                parameters[1] = 1.0f;
                parameters[2] = 0.0f;
            } else if (mat[2][2] == 1.0f) {
                parameters[0] = 0.0f;
                parameters[1] = 0.0f;
                parameters[2] = 1.0f;
            } 
        }
    }

    public void derivatives(double [][] dR, double da, double db, double dc) {
        double x, x2, a2, b2, c2, ab, ac, bc, ax, bx, cx;
        double an, bn, cn, n, m;
        double ZERO = 1e-6f;

        n = (double)Math.sqrt(parameters[0]*parameters[0] + parameters[1]*parameters[1] + parameters[2]*parameters[2]);
        if (n > 1.0) {
			// reproject onto [-1,1] instead ?
			m = n % 2;
			if (m>1.0) {
				an = m * parameters[0] / n - 2 * parameters[0] / n;
				bn = m * parameters[1] / n - 2 * parameters[1] / n;
				cn = m * parameters[2] / n - 2 * parameters[2] / n;
			} else {
				an = parameters[0] * m / n;
				bn = parameters[1] * m / n;
				cn = parameters[2] * m / n;
			}
         } else {
            an = parameters[0];
            bn = parameters[1];
            cn = parameters[2];
        }

        a2 = an*an; b2 = bn*bn; c2 = cn*cn;
        ab = an*bn; ac = an*cn; bc = bn*cn;
        x2 = 1.0f - a2 - b2 - c2;
        x = (double)Math.sqrt(x2);
        ax = x*an; bx = x*bn; cx = x*cn;

        // should not occur: if x=0 then a=b=c=0 then x=1... 
        if (x < ZERO) System.out.println("jist.plugins"+"\t"+"x<0 in rotation");

        dR[0][0] = -4.0f * (bn * db + cn * dc);
        dR[1][0] = 2.0f / x * ((bx - ac) * da + (ax - bc) * db + (x2 - c2) * dc);
        dR[2][0] = 2.0f / x * ((cx + ab) * da + (b2 - x2) * db + (ax + bc) * dc);
        dR[0][1] = 2.0f / x * ((bx + ac) * da + (ax + bc) * db + (c2 - x2) * dc);
        dR[1][1] = -4.0f * (an * da + cn * dc);
        dR[2][1] = 2.0f / x * ((x2 - a2) * da + (cx - ab) * db + (bx - ac) * dc);
        dR[0][2] = 2.0f / x * ((cx - ab) * da + (x2 - b2) * db + (ax - bc) * dc);
        dR[1][2] = 2.0f / x * ((a2 - x2) * da + (cx + ab) * db + (bx + ac) * dc);
        dR[2][2] = -4.0f * (an * da + bn * db);

        return;
    }
	 */
	private double[][]	matrix;
	private double[]		parameters;

	public RotationMatrix() {
		matrix = new double[3][3];
		parameters = new double[3];

		for (int i=0;i<3;i++) {
			for (int j=0;j<3;j++) matrix[i][j] = 0.0f;
			matrix[i][i] = 1.0f;
			parameters[i] = 0.0f;
		}       
	}

	public double getParameter(int i) {
		return parameters[i];
	}

	public double[] getParameters() {
		return parameters;
	}

	public double getMatrix(int i,int j) {
		return matrix[i][j];
	}

	public double[][] getMatrixArray() {
		return matrix;
	}

	public Matrix getMatrix() {
		return new Matrix(matrix);
	}

	public void setParameters(double a, double b, double c) {
		double x, a2, b2, c2;
		double an, bn, cn, n, m;

		// reproject in appropriate space
		n = Math.sqrt( a*a + b*b + c*c );
		if (n > 1.0) {
			// reproject onto [-1,1] instead
			m = n % 2;
			if (m>1.0) {
				an = m * a / n - 2 * a / n;
				bn = m * b / n - 2 * b / n;
				cn = m * c / n - 2 * c / n;
			} else {
				an = a * m / n;
				bn = b * m / n;
				cn = c * m / n;
			}
		} else {
			an = a;
			bn = b;
			cn = c;
		}

		// set the parameters
		parameters[0] = (double)an; parameters[1] = (double)bn; parameters[2] = (double)cn;

		// compute the new matrix
		a2 = an*an; b2 = bn*bn; c2 = cn*cn;

		x = 1.0 - a2 - b2 - c2;
		// for roundoff errors ?
		if ( x<0.0 && x>-1e-10) x = 0.0;
		x = Math.sqrt(x);

		if (Double.isNaN(x)) System.out.print("! x^2 < 0: "+a2+", "+b2+", "+c2+"\n");

		matrix[0][0] = (double)(1.0 - 2.0 * b2 - 2.0 * c2);
		matrix[1][0] = (double)(2.0 * an * bn + 2.0 * x * cn);
		matrix[2][0] = (double)(2.0 * an * cn - 2.0 * x * bn);
		matrix[0][1] = (double)(2.0 * an * bn - 2.0 * x * cn);
		matrix[1][1] = (double)(1.0 - 2.0 * a2 - 2.0 * c2);
		matrix[2][1] = (double)(2.0 * bn * cn + 2.0 * x * an);
		matrix[0][2] = (double)(2.0 * an * cn + 2.0 * x * bn);
		matrix[1][2] = (double)(2.0 * bn * cn - 2.0 * x * an);
		matrix[2][2] = (double)(1.0 - 2.0 * a2 - 2.0 * b2);

		return;
	}
	public void setParameters(double[] param) {
		setParameters(param[0],param[1],param[2]);
	}
	public void setParameters(double[] param, int offset) {
		setParameters(param[offset+0],param[offset+1],param[offset+2]);
	}

	public void setMatrix(float[][] mat) {
		double x, a2, b2, c2;

		// set the matrix
		for (int i=0;i<3;i++) for (int j=0;j<3;j++)
			matrix[i][j] = mat[i][j];

		// compute the new parameters
		x = (double)Math.sqrt(mat[0][0] + mat[1][1] + mat[2][2] + 1.0f)/ 2.0f;

		if (x > 0.0) {
			parameters[0] = (mat[2][1] / x - mat[1][2] / x) / 4.0f;
			parameters[1] = (mat[0][2] / x - mat[2][0] / x) / 4.0f;
			parameters[2] = (mat[1][0] / x - mat[0][1] / x) / 4.0f;
		} else {
			if ((mat[0][0] < 1.0) && (mat[1][1] < 1.0) && (mat[2][2] < 1.0)) {
				parameters[0] = (double)Math.sqrt( (mat[0][1]*mat[0][1] + mat[0][2]*mat[0][2]) 
						/ (2.0f*(1.0 - mat[0][0])) );
				parameters[1] = (double)Math.sqrt( (mat[1][0]*mat[1][0] + mat[1][2]*mat[1][2]) 
						/ (2.0f*(1.0 - mat[1][1])) );
				parameters[2] = (double)Math.sqrt( (mat[2][0]*mat[2][0] + mat[2][1]*mat[2][1]) 
						/ (2.0f*(1.0 - mat[2][2])) );
				if (mat[0][1] < 0.0) parameters[1] = -parameters[1];
				if (mat[0][2] < 0.0) parameters[2] = -parameters[2];
			} else if (mat[0][0] == 1.0f) {
				parameters[0] = 1.0f;
				parameters[1] = 0.0f;
				parameters[2] = 0.0f;
			} else if (mat[1][1] == 1.0f) {
				parameters[0] = 0.0f;
				parameters[1] = 1.0f;
				parameters[2] = 0.0f;
			} else if (mat[2][2] == 1.0f) {
				parameters[0] = 0.0f;
				parameters[1] = 0.0f;
				parameters[2] = 1.0f;
			} 
		}
	}

	public void setMatrix(double[][] mat) {
		double x, a2, b2, c2;

		// set the matrix
		for (int i=0;i<3;i++) for (int j=0;j<3;j++)
			matrix[i][j] = (double)mat[i][j];

		// compute the new parameters
		x = (double)Math.sqrt(mat[0][0] + mat[1][1] + mat[2][2] + 1.0f)/ 2.0f;

		if (x > 0.0) {
			parameters[0] = (double) ( (mat[2][1] / x - mat[1][2] / x) / 4.0 );
			parameters[1] = (double) ( (mat[0][2] / x - mat[2][0] / x) / 4.0 );
			parameters[2] = (double) ( (mat[1][0] / x - mat[0][1] / x) / 4.0 );
		} else {
			if ((mat[0][0] < 1.0) && (mat[1][1] < 1.0) && (mat[2][2] < 1.0)) {
				parameters[0] = (double)Math.sqrt( (mat[0][1]*mat[0][1] + mat[0][2]*mat[0][2]) 
						/ (2.0f*(1.0 - mat[0][0])) );
				parameters[1] = (double)Math.sqrt( (mat[1][0]*mat[1][0] + mat[1][2]*mat[1][2]) 
						/ (2.0f*(1.0 - mat[1][1])) );
				parameters[2] = (double)Math.sqrt( (mat[2][0]*mat[2][0] + mat[2][1]*mat[2][1]) 
						/ (2.0f*(1.0 - mat[2][2])) );
				if (mat[0][1] < 0.0) parameters[1] = -parameters[1];
				if (mat[0][2] < 0.0) parameters[2] = -parameters[2];
			} else if (mat[0][0] == 1.0f) {
				parameters[0] = 1.0f;
				parameters[1] = 0.0f;
				parameters[2] = 0.0f;
			} else if (mat[1][1] == 1.0f) {
				parameters[0] = 0.0f;
				parameters[1] = 1.0f;
				parameters[2] = 0.0f;
			} else if (mat[2][2] == 1.0f) {
				parameters[0] = 0.0f;
				parameters[1] = 0.0f;
				parameters[2] = 1.0f;
			} 
		}
	}

	public void derivatives(double[][] dR, double da, double db, double dc) {
		double x, x2, a2, b2, c2, ab, ac, bc, ax, bx, cx;
		double an, bn, cn, n, m;
		double ZERO = 1e-6f;

		n = (double)Math.sqrt(parameters[0]*parameters[0] + parameters[1]*parameters[1] + parameters[2]*parameters[2]);
		if (n > 1.0) {
			// reproject onto [-1,1] instead ?
			m = n % 2;
			if (m>1.0) {
				an = m * parameters[0] / n - 2 * parameters[0] / n;
				bn = m * parameters[1] / n - 2 * parameters[1] / n;
				cn = m * parameters[2] / n - 2 * parameters[2] / n;
			} else {
				an = parameters[0] * m / n;
				bn = parameters[1] * m / n;
				cn = parameters[2] * m / n;
			}
		} else {
			an = parameters[0];
			bn = parameters[1];
			cn = parameters[2];
		}

		a2 = an*an; b2 = bn*bn; c2 = cn*cn;
		ab = an*bn; ac = an*cn; bc = bn*cn;
		x2 = 1.0f - a2 - b2 - c2;
		x = (double)Math.sqrt(x2);
		ax = x*an; bx = x*bn; cx = x*cn;

		/* should not occur: if x=0 then a=b=c=0 then x=1... */
		if (x < ZERO) System.out.println("jist.plugins"+"\t"+"x<0 in rotation");

		dR[0][0] = -4.0f * (bn * db + cn * dc);
		dR[1][0] = 2.0f / x * ((bx - ac) * da + (ax - bc) * db + (x2 - c2) * dc);
		dR[2][0] = 2.0f / x * ((cx + ab) * da + (b2 - x2) * db + (ax + bc) * dc);
		dR[0][1] = 2.0f / x * ((bx + ac) * da + (ax + bc) * db + (c2 - x2) * dc);
		dR[1][1] = -4.0f * (an * da + cn * dc);
		dR[2][1] = 2.0f / x * ((x2 - a2) * da + (cx - ab) * db + (bx - ac) * dc);
		dR[0][2] = 2.0f / x * ((cx - ab) * da + (x2 - b2) * db + (ax - bc) * dc);
		dR[1][2] = 2.0f / x * ((a2 - x2) * da + (cx + ab) * db + (bx + ac) * dc);
		dR[2][2] = -4.0f * (an * da + bn * db);

		return;
	}
	public double[][] derivatives(double da, double db, double dc) {
		double[][] dR = new double[3][3];
		derivatives(dR, da, db, dc);
		return dR;
	}


	public double[] computeEulerAngles() {
		double[] euler = new double[3];
		double cphi,sphi,ctheta,stheta,cpsi,spsi;

		ctheta = matrix[2][2];
		stheta = Math.sqrt(1.0f-ctheta*ctheta);

		if (stheta>0) {
			cphi = -matrix[2][1]/stheta;
			sphi =  matrix[2][0]/stheta;

			cpsi =  matrix[1][2]/stheta;
			spsi =  matrix[0][2]/stheta;

			euler[0] = (double)Math.atan2(sphi,cphi);
			euler[1] = (double)Math.atan2(stheta,ctheta);
			euler[2] = (double)Math.atan2(spsi,cpsi);

		} else {
			cphi = matrix[0][0];
			sphi = matrix[0][1];

			euler[0] = (double)Math.atan2(sphi,cphi);
			euler[1] = 0.0f;
			euler[2] = 0.0f;
		}

		return euler;
	}



	public static void main(String []arg) {
		RotationMatrix rm = new RotationMatrix(); 
		Matrix m = new Matrix(new double[][]{{0,0,1},{0,-1,0},{1,0,0}});
		System.out.println("jist.plugins"+"\t"+"In matrix:");
		m.print(3, 2);
		rm.setMatrix(m.getArrayCopy());
		System.out.println("jist.plugins"+"\t"+"Parameters");
		double []p=rm.getParameters();
		for(int i=0;i<3;i++)
			System.out.println("jist.plugins"+"\t"+p[i]);
		//    	rm.setParameters(p);
		rm.setParameters(p[0],p[1],p[2]);
		Matrix m2 = rm.getMatrix();
		System.out.println("jist.plugins"+"\t"+"Resulting matrix:");
		m2.print(3, 2);    	
	}
}
