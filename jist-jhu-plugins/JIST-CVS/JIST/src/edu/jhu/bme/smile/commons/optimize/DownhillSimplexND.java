package edu.jhu.bme.smile.commons.optimize;

import java.util.ArrayList;

/**
 * This optimizer uses the Nelder-Mead downhill simplex method to optimize a function.
 * You can input an initial guess which may make optimizations faster. Otherwise, a 
 * default initial guess is supplied.
 * Adapted from Numerical Recipes (Press), 3rd Edition.
 * @author Hanlin Wan
 */
public class DownhillSimplexND implements OptimizerNDContinuous {

	private double extrema[], tol, radius[];
	private int dim, mpts;
	private OptimizableNDContinuous myFunction;
	private String statusMessage;
	private int maxIterations, iterations;
	private double value[], psum[];
	private double simplex[][];
	private int maxSteps=100;
	private boolean logOn = true;
	private ArrayList<double[][]> logData = new ArrayList<double[][]>();

	/**
	 * Default constructor
	*/
	public DownhillSimplexND() {
		maxIterations = 1000;
		myFunction = null;
	}

	/**
	 * Constructor to set maximum number of iterations to run
	 * @param maxIter maximum number of iterations
	 */
	public DownhillSimplexND(int maxIter) {
		maxIterations = maxIter;
		myFunction = null;
	}
	
	/**
	 * Turns on logging for each iteration of the optimization.
	 * @param turnOn true if logging, false if not
	 * @param maxSteps maximum number of steps to log
	 */
	public void logData(boolean turnOn, int maxSteps) {
		logOn = turnOn;
		this.maxSteps = maxSteps;
		logData = new ArrayList<double[][]>();
	}
	
	/**
	 * Gets the logged data
	 * @return array of logged data
	 */
	public ArrayList<double[][]> getLog() {
		return logData;
	}

	/**
	 * Gets the extrema that was found
	 * @return the extrema array of where the extrema is
	 */
	public double[] getExtrema() {
		return extrema;
	}

	/**
	 * Initializes the optimizer with a function and an initial guess. Guessing incorrectly
	 * may affect the accuracy and/or speed of the calculated extrema. 
	 * @param function your OptimizableNDContinuous function that is being optimized
	 * @param center the approximate location of the extrema
	 * @param rad approximate radius from center where the extrema lies
	 */
	public void initialize(OptimizableNDContinuous function, double center[], double rad) {
		dim = function.getNumberOfDimensions();
		extrema = center;
		radius = new double[dim];
		for (int i = 0; i < dim; i++) radius[i]=rad;
		tol = function.getDomainTolerance();
		myFunction = function;
	}

	/**
	 * Initializes the optimizer with a function and an initial guess. Guessing incorrectly
	 * may affect the accuracy and/or speed of the calculated extrema. 
	 * @param function your OptimizableNDContinuous function that is being optimized
	 * @param center the approximate location of the extrema
	 * @param rad approximate radius from center where the extrema lies
	 */
	public void initialize(OptimizableNDContinuous function, double center[], double[] rad) {
		dim = function.getNumberOfDimensions();
		extrema = center;
		radius = rad;
		tol = function.getDomainTolerance();
		myFunction = function;
	}
	
	/**
	 * Default initialize method. It will supply an initial guess depending on the domain given.
	 * @param function your OptimizableNDContinuous function that is being optimized
	 */
	public void initialize(OptimizableNDContinuous function) {
		dim = function.getNumberOfDimensions();
		extrema = new double[dim];
		radius = new double[dim];
		tol = function.getDomainTolerance();
		for (int i = 0; i < dim; i++) {
			extrema[i] = (function.getDomainMin()[i] + function.getDomainMin()[i]) / 2;
			radius[i] = function.getDomainMax()[i] - extrema[i];
		}
		myFunction = function;
	}

	/**
	 * Optimizes the function
	 * @param findMinima true if you want to find the minimum, false if you want to find the maximum
	 * @return true if extrema was found successfully, false otherwise
	 */
	public boolean optimize(boolean findMinima) {
		if (myFunction == null)  {
			statusMessage = "Not initialized.";
			return false;			
		}
		
		if (!findMinima) myFunction = new MinToMaxND(myFunction);
		amoeba();
		
		if (iterations >= maxIterations) {
			statusMessage = "Maximum iterations reached.";
			return false;
		}
		
		statusMessage = "Convergence reached";		
		return true;
	}

	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations;
	}
	
	/**
	 * Gets the status message from the optimizing process
	 * @return string of the status message
	 */
	public String statusMessage() {
		return statusMessage;
	}

	/**
	 * This method uses simplexes to move the enclosed area so that it converges to the extrema.
	 */
	private void amoeba() {
		simplex = new double[dim+1][dim];
		for (int i=0; i<dim+1; i++) {
			for (int j=0; j<dim; j++) simplex[i][j] = extrema[j] - radius[j];
			if (i!=0) simplex[i][i-1] += 2 * radius[i-1];
		}
		
		int ihi, ilo, inhi;
		mpts = simplex.length;
		psum = new double[dim];
		double []x = new double[dim];
		value = new double[mpts];
		for (int i=0; i<mpts; i++) {
			for (int j=0; j<dim; j++) x[j] = simplex[i][j];
			value[i] = myFunction.getValue(x);
		}
		getPsum();
		iterations = 0;
		while (iterations < maxIterations) {
			//rank from worst to best points in simplex
			ilo = 0;
			if (value[0] > value[1]) {
				inhi = 1;
				ihi = 0;
			}
			else {
				inhi = 0;
				ihi = 1;
			}
			for (int i=0; i<mpts; i++) {
				if (value[i] <= value[ilo]) ilo = i;
				if (value[i] > value[ihi]) {
					inhi = ihi;
					ihi = i;
				}
				else if (value[i] > value[inhi] && i != ihi) inhi = i;
			}
			
			//logging
			if (logOn && iterations<maxSteps) {
				double log[][] = new double[dim+2][dim];
				for (int i=0; i<dim+1; i++)
					for (int j=0; j<dim; j++)
						log[i][j] = simplex[i][j];
				log[dim+1][0]=ilo;
				logData.add(log);
			}
			
			//compute the fractional range from highest to lowest point and test if within tolerance
			double rtol = 2.0 * Math.abs(value[ihi] - value[ilo]) / (Math.abs(value[ihi]) + Math.abs(value[ilo]) + tol);
			if (rtol < tol) {
				double temp = value[0];
				value[0] = value[ilo];
				value[ilo] = temp;
				for (int i=0; i<dim; i++) {
					double temp2 = simplex[0][i];
					simplex[0][i] = simplex[ilo][i];
					simplex[ilo][i] = temp2;
					extrema[i] = simplex[0][i];
				}
				return;
			}
		
			//reflect simplex from highest point
			double ytry = amotry(ihi, -1.0);
			if (ytry <= value[ilo]) 
				//gives a better result so expand simplex
				ytry = amotry(ihi, 2.0);
			else if (ytry >= value[inhi]) {
				//gives a worse result so contract simplex
				double ysave = value[ihi];
				ytry = amotry(ihi, 0.5);
				if (ytry >= ysave) {
					//contract around the best point
					for (int i=0; i<mpts; i++) {
						if (i != ilo) {
							for (int j=0; j<dim; j++) 
								simplex[i][j] = 0.5 * (simplex[i][j] + simplex[ilo][j]);
							value[i] = myFunction.getValue(simplex[i]);
						}
					}
					getPsum();
				}
			}
			iterations++;
		}
	}
	
	/**
	 * Calculates the psum of the simplex.
	 */
	private void getPsum() {
		for (int j=0; j<dim; j++) {
			double sum = 0.0;
			for (int i=0; i<mpts; i++) 
				sum += simplex[i][j];
			psum[j] = sum;
		}
	}

	/**
	 * Extrapolate by a factor of fac through the face of the simplex across from the high point, 
	 * tries it, and replaces the high point if the new point is better
	 * @param ihi initial high point
	 * @param fac factor used to move vertex
	 * @return the better value after trying the different extrapolations
	 */
	private double amotry(int ihi, double fac) {
		double ptry[] = new double[dim];
		double fac1 = (1.0 - fac) / dim;
		double fac2 = fac1 - fac;
		for (int j=0; j<dim; j++) 
			ptry[j] = psum[j] * fac1 - simplex[ihi][j] * fac2;
		double ytry = myFunction.getValue(ptry);	//evaluate function at trial point
		if (ytry < value[ihi]) {
			//replace high point if better
			value[ihi] = ytry;
			for (int j=0; j<dim; j++) {
				psum[j] += ptry[j] - simplex[ihi][j];
				simplex[ihi][j] = ptry[j];
			}
		}
		return ytry;
	}
}
