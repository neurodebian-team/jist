package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;


public class MemoryDemo extends ProcessingAlgorithm{	
	/*******************************************
	 * Input Parameters
	 ******************************************/
	private ParamVolume ParamVol;
	private ParamVolumeCollection ParamVolCol;
	

	/*******************************************
	 * Output Parameters
	 ******************************************/
	private ParamVolume ParamResult;
	private ParamVolumeCollection ParamResultCol;

	private static final String rcsid =
		"$Id: MemoryDemo.java,v 1.1 2009/11/20 02:27:57 bennett Exp $";
	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams){
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Memory Demo");
		inputParams.setName("Memory_Demo");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);

		/***************************************
		 * Creating Inputs
		 ***************************************/
		inputParams.add(ParamVol=new ParamVolume("Input Volume",null,-1,-1,-1,-1));
		ParamVol.setLoadAndSaveOnValidate(false);
		inputParams.add(ParamVolCol=new ParamVolumeCollection("Input Volumes"));
		ParamVolCol.setLoadAndSaveOnValidate(false);
		ParamVolCol.setMandatory(false);
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(ParamResult=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
		ParamResult.setLoadAndSaveOnValidate(false);
		
		outputParams.add(ParamResultCol=new ParamVolumeCollection("Result Volume"));
		ParamResultCol.setLoadAndSaveOnValidate(false);
		ParamResultCol.setMandatory(false);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		JistLogger.logOutput(JistLogger.INFO, "Memory test starting...");
		JistLogger.logMIPAVRegistry();
		ImageData img = ParamVol.getImageData();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Clone an image 25x...");		
		ImageData clone=null;
		for(int i=0;i<25;i++) {
			clone = new ImageDataFloat(img);
			
		}
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Now only 1+1 copy in memory...");
		JistLogger.logOutput(JistLogger.INFO, "Disposing original...");
		img=null; ParamVol.dispose();
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Now only 1 copy in memory...");
		JistLogger.logOutput(JistLogger.INFO, "Getting original...");
		img = ParamVol.getImageData();
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Now only 1+1 copy in memory...");
		clone.dispose(); clone = null;
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Now back to the basics copy in memory...");		
		JistLogger.logOutput(JistLogger.INFO, "Setup output...");
		ImageData dat2 = new ImageDataFloat(new float[1000][200][200]);
		dat2.setHeader(img.getHeader());
		ParamResult.setValue(dat2);dat2=null;
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Now base+80MB...");
		ParamResult.writeAndFreeNow(this);		
		JistLogger.logOutput(JistLogger.INFO, "Back down to base...");
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");		
		JistLogger.logOutput(JistLogger.INFO, "Write out 25x...");
		clone=null;
		for(int i=0;i<25;i++) {
			clone = new ImageDataFloat(img);
			clone.setName("clone"+i);
			ParamResultCol.add(clone);
			ParamResultCol.writeAndFreeNow(this);
		}
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Now only 1+1 copy in memory...");
		JistLogger.logMIPAVRegistry();
		
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Reading inputs and disposing...");
		for(ParamVolume vol : ParamVolCol.getParamVolumeList()) {
			ImageData foo = vol.getImageData();
			JistLogger.logOutput(JistLogger.INFO, "Volume: "+foo.getName());
			foo=null;
			vol.dispose();			
		}
		
		System.gc();
		JistLogger.logMIPAVRegistry();
		JistLogger.logOutput(JistLogger.INFO, "***************************************************************************************");
		JistLogger.logOutput(JistLogger.INFO, "Done...");
		JistLogger.logMIPAVRegistry();
		
	}
}
