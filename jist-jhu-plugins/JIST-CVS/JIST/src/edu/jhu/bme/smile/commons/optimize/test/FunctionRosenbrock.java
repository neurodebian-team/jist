package edu.jhu.bme.smile.commons.optimize.test;

import edu.jhu.bme.smile.commons.optimize.FunctionNumericNDDifferentiation;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuousDifferentiable;

/**
 * Test case to test ND optimizers.
 * Function is 5*(x-y^2)^2 + (1-y)^2
 * @author Yufeng Guo, Hanlin Wan
 *
 */
public class FunctionRosenbrock implements OptimizableNDContinuousDifferentiable {
	
	private double dMin[], dMax[], tol=1e-5;
	private int numDim = 2;
	
	/**
	 * Default constructor.
	 */
	public FunctionRosenbrock() {
		dMin = new double[numDim];
		dMax = new double[numDim];
		for (int i=0; i<numDim; i++) {
			dMin[i] = -2;
			dMax[i] = 2;
		}
	}
	
	/**
	 * Constructor to set the domain.
	 * @param domMin domain minimum
	 * @param domMax domain maximum
	 */
	public FunctionRosenbrock(double domMin[], double domMax[]) {
		dMin = domMin;
		dMax = domMax;
	}

	/**
	 * Gets the domain maximum.
	 * @return domain max
	 */
	public double[] getDomainMax() {
		return dMax;
	}

	/**
	 * Gets the domain minimum.
	 * @return domain min
	 */
	public double[] getDomainMin() {
		return dMin;
	}

	/**
	 * Gets the tolerance.
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return tol;
	}

	/**
	 * Gets the number of dimensions.
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return numDim;
	}

	/**
	 * Evaluates the function at location x.
	 * @param x location to evaluate function
	 * @return value of the function at x
	 */
	public double getValue(double[] x) {
		return 5*Math.pow(x[0]-x[1]*x[1],2) + Math.pow(1-x[1],2);
	}
	
	/**
	 * Gets the hessian of the function.
	 * Uses the default step size and method
	 * @param x location to calculate hessian
	 * @return hessian at location x
	 */
	public double[][] get2ndDerivative(double[] x) {
		return (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(x);  
	}

	/**
	 * Gets the hessian of the function.
	 * Uses the specified step size and method.
	 * @param x location to calculate hessian
	 * @param step step size
	 * @param method method
	 * @return hessian at location x
	 */
	public double[][] get2ndDerivative(double[] x, double step, int method) {
		return (new FunctionNumericNDDifferentiation(this, step, method)).get2ndDerivative(x);  
	}
	
	/**
	 * Gets the 2nd derivative of the function in a specific direction.
	 * Uses the default step size and method.
	 * @param x location to calculate derivative
	 * @param dim1 dimension to take the first derivative
	 * @param dim2 dimension to take the second derivative
	 * @return 2nd derivative at location x in the specified direction
	 */
	public double get2ndDerivative(double[] x, int dim1, int dim2) {
		return (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(x, dim1, dim2);  
	}

	/**
	 * Gets the gradient of the function.
	 * Uses the default step size and method
	 * @param x location to calculate gradient
	 * @return gradient at location x
	 */
	public double[] getDerivative(double[] x) {
		return (new FunctionNumericNDDifferentiation(this)).getDerivative(x);  
	}
	
	/**
	 * Gets the gradient of the function.
	 * Uses the specified step size and method.
	 * @param x location to calculate gradient
	 * @param step step size
	 * @param method method
	 * @return gradient at location x
	 */
	public double[] getDerivative(double[] x, double step, int method) {
		return (new FunctionNumericNDDifferentiation(this, step, method)).getDerivative(x);  
	}	
}
