package edu.jhu.bme.smile.commons.math.specialFunctions;

public class SFunc {

	static public int factorial(int d) {
		if(d==0)
			return 1;
		if(d<0) return 0;		
		int result = d;
		d--;
		while(d>1) {
			result*=d;
			d--;
		}
		return result;
	}

	/**
	 * DoubleFactorial !!
	 * See: http://en.wikipedia.org/wiki/Factorial#Double_factorial
	 * @param d 
	 * @return  d!!
	 */
	static public double doubleFactorial(int d) {	
		if(d==0)
			return 1;
		if(d<0) return 0;
		int result = d;
		d-=2;
		while(d>1) {
			result*=d;
			d-=2;
		}
		return result;
	}

	public static double factorialratio(int num, int denom) {
		double ret = 1;
		if(denom>num) {
			for(int k=denom;k>num;k--)
				ret*=(double)k;
			return 1./ret; 
	
		}else {
			for(int k=num;k>denom;k--)
				ret*=(double)k;
			return ret; 
		}
	
	}

	public static double rootfactorialratio(int num, int denom) {
		double ret = 1;
		if(denom>num) {
			for(int k=denom;k>num;k--)
				ret*=Math.sqrt((double)k);
			return 1./ret; 
	
		}else {
			for(int k=num;k>denom;k--)
				ret*=Math.sqrt((double)k);
			return ret; 
		}
	
	}

}
