package edu.jhu.bme.smile.commons.optimize;
/**
 * This class does numeric differentiation in 1D.
 * There are lots of methods of doing so, and depending on how many points are used, enter that value in for the method variable.
 * Also computes 2nd derivatives. 
 * Defaults to 3-point method.
 * @author Yufeng Guo, Hanlin Wan
 */
public class FunctionNumeric1DDifferentiation implements Optimizable1DContinuousDifferentiable {

	public static final int DIFF_FWD_EULER = 0;
	public static final int DIFF_BAC_EULER = 1;
	public static final int DIFF_3_POINT = 3;
	public static final int DIFF_5_POINT = 5;
	public static final int DIFF_7_POINT = 7;
	public static final int DIFF_9_POINT = 9;
	public static final double DEFAULT_STEP = 1e-5;
	
	private Optimizable1DContinuous myFunction;
	private double myStep;
	private int myMethod;
	
	/**
	 * Constructor to create a 1D differentiable object.
	 * Default step size of 1e-5 and default method of 3 point are used.
	 * @param function function to differentiate
	 */
	public FunctionNumeric1DDifferentiation(Optimizable1DContinuous function) {	
		this(function, DEFAULT_STEP, DIFF_3_POINT);		
	}
	
	/**
	 * Constructor to create a 1D differentiable object.
	 * @param function function to differentiate
	 * @param step step size to use
	 * @param method the differentiating method to use: 0,1,3,5,7, or 9. Default is 3.
	 */
	public FunctionNumeric1DDifferentiation(Optimizable1DContinuous function, double step, int method) {
		if (method<0|| method==2 || method==6 || method==8 || method>9)
				myMethod = 3;
		else myMethod=method;
		myFunction=function;
		myStep = step;
	}

	/**
	 * Gets the second derivative
	 * @param x location to evaluate derivative at
	 * @return second derivative at location x
	 */
	public double get2ndDerivative(double x) {
		switch(myMethod) {
		case DIFF_3_POINT: case DIFF_BAC_EULER: case DIFF_FWD_EULER:
			return (myFunction.getValue(x+myStep) - 2*myFunction.getValue(x) + myFunction.getValue(x-myStep))/(myStep*myStep);
		case DIFF_5_POINT:
			return (-1*myFunction.getValue(x+2*myStep) + 16*myFunction.getValue(x+myStep) - 30*myFunction.getValue(x) + 16*myFunction.getValue(x-myStep) - myFunction.getValue(x-2*myStep)) / (12*myStep*myStep);
		case DIFF_7_POINT:
			return (2*myFunction.getValue(x+3*myStep) - 27*myFunction.getValue(x+2*myStep) + 270*myFunction.getValue(x+myStep) - 490*myFunction.getValue(x) + 270*myFunction.getValue(x-myStep) - 27*myFunction.getValue(x-2*myStep) + 2*myFunction.getValue(x-3*myStep))/(180*myStep*myStep);
		case DIFF_9_POINT:
			return (-9*myFunction.getValue(x+4*myStep) + 128*myFunction.getValue(x+3*myStep) - 1008*myFunction.getValue(x+2*myStep) + 8064*myFunction.getValue(x+myStep) - 14350*myFunction.getValue(x) + 8064*myFunction.getValue(x-myStep) - 1008*myFunction.getValue(x-2*myStep) + 128*myFunction.getValue(x-3*myStep) - 9*myFunction.getValue(x-4*myStep))/(5040*myStep*myStep);
		default: 
			break;
		}
		return Double.NaN;
	}

	/**
	 * Gets the first derivative
	 * @param x location to evaluate derivative at
	 * @return derivative at location x
	 */
	public double getDerivative(double x) {
		switch(myMethod) {
		case DIFF_FWD_EULER: 
			return (myFunction.getValue(x+myStep) - myFunction.getValue(x))/myStep;
		case DIFF_BAC_EULER:
			return (myFunction.getValue(x) - myFunction.getValue(x-myStep))/myStep;
		case DIFF_3_POINT:
			return (myFunction.getValue(x+myStep) - myFunction.getValue(x-myStep))/(2*myStep);
		case DIFF_5_POINT:
			return (-myFunction.getValue(x+2*myStep) + 8*myFunction.getValue(x+myStep) - 8*myFunction.getValue(x-myStep) + myFunction.getValue(x-2*myStep)) / (12*myStep);
		case DIFF_7_POINT:
			return (myFunction.getValue(x+3*myStep) - 9*myFunction.getValue(x+2*myStep) + 45*myFunction.getValue(x+myStep) - 45*myFunction.getValue(x-myStep) + 9*myFunction.getValue(x-2*myStep) - myFunction.getValue(x-3*myStep))/(60*myStep);
		case DIFF_9_POINT:
			return (-3*myFunction.getValue(x+4*myStep) + 32*myFunction.getValue(x+3*myStep) - 168*myFunction.getValue(x+2*myStep) + 672*myFunction.getValue(x+myStep) - 672*myFunction.getValue(x-myStep) + 168*myFunction.getValue(x-2*myStep) - 32*myFunction.getValue(x-3*myStep) + 3*myFunction.getValue(x-4*myStep))/(840*myStep);
		default: 
			break;
		}
		return Double.NaN;
	}
	
	/**
	 * Gets the method
	 * @return method
	 */
	public int getMethod() {
		return this.myMethod;
	}

	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double getDomainMax() {	
		return myFunction.getDomainMax();
	}

	/**
	 * Gets the domain minimum
	 * @return domain min
	 */
	public double getDomainMin() {
		return myFunction.getDomainMin();
	}

	/**
	 * Gets the tolerance
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunction.getDomainTolerance();
	}

	/**
	 * Gets the value
	 * @param x location to get value at
	 * @return value at location x
	 */
	public double getValue(double x) {
		return myFunction.getValue(x);
	}
}
