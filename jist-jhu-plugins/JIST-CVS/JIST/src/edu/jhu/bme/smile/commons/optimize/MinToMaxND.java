package edu.jhu.bme.smile.commons.optimize;

/**
 * Finds the maximum of ND functions by negating the function and finding the minimum.
 * @author Hanlin Wan
 */
public class MinToMaxND implements OptimizableNDContinuous {

	private OptimizableNDContinuous myFunction;
	
	/**
	 * Constructor to create the negated function
	 * @param func function to negate
	 */
	public MinToMaxND(OptimizableNDContinuous func) {
		myFunction = func;
	}
	
	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double[] getDomainMax() {	
		return myFunction.getDomainMax();
	}

	/**
	 * Gets the domain minimum
	 * @return domain min
	 */
	public double[] getDomainMin() {
		return myFunction.getDomainMin();
	}

	/**
	 * Gets the tolerance
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunction.getDomainTolerance();
	}

	/**
	 * Gets the negated value
	 * @param x location to get value at
	 * @return negated value at location x
	 */
	public double getValue(double []x) {
		return -1 * myFunction.getValue(x);
	}

	/**
	 * Gets the number of dimensions
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return myFunction.getNumberOfDimensions();
	}
}
