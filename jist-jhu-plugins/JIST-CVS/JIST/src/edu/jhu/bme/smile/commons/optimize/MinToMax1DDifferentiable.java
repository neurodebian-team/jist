package edu.jhu.bme.smile.commons.optimize;

/**
 * Finds the maximum of Diffentiable 1D functions by negating the function and finding the minimum.
 * @author Hanlin Wan
 */
public class MinToMax1DDifferentiable implements Optimizable1DContinuousDifferentiable {

	private Optimizable1DContinuousDifferentiable myFunction;
	
	/**
	 * Constructor to create the negated function
	 * @param func function to negate
	 */
	public MinToMax1DDifferentiable(Optimizable1DContinuousDifferentiable func) {
		myFunction = func;
	}

	/**
	 * Gets the negated second derivative
	 * @param x location to calculate derivative
	 * @return the negated second derivative
	 */
	public double get2ndDerivative(double x) {
		return -1 * myFunction.get2ndDerivative(x);
	}

	/**
	 * Gets the negated derivative
	 * @param x location to calculate derivative
	 * @return the negated derivative
	 */
	public double getDerivative(double x) {
		return -1 * myFunction.getDerivative(x);
	}

	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double getDomainMax() {	
		return myFunction.getDomainMax();
	}

	/**
	 * Gets the domain minimum
	 * @return domain min
	 */
	public double getDomainMin() {
		return myFunction.getDomainMin();
	}

	/**
	 * Gets the tolerance
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunction.getDomainTolerance();
	}

	/**
	 * Gets the negated value
	 * @param x location to get value at
	 * @return negated value at location x
	 */
	public double getValue(double x) {
		return -1 * myFunction.getValue(x);
	}
}