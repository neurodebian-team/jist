package edu.jhu.bme.smile.commons.optimize.test;

import edu.jhu.bme.smile.commons.optimize.FunctionNumeric1DDifferentiation;
import edu.jhu.bme.smile.commons.optimize.FunctionNumericNDDifferentiation;
import edu.jhu.bme.smile.commons.optimize.Optimizable1DContinuousDifferentiable;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuousDifferentiable;

/**
 * Test class to test all derivative methods.
 * @author Hanlin Wan
 *
 */
public class TestDerivativeFunction {

	static Optimizable1DContinuousDifferentiable my1DFunc = new FunctionXXplusSinXX(0, -100, 100);
	static OptimizableNDContinuousDifferentiable myNDFunc = new FunctionXXYYND(2, new double[]{-30,20}, new double[]{-100,-100}, new double[]{100,100});

	public static void main(String[] args) {
		
		//1D derivatives
		for (int i = -1; i <= 9; i += 2) {
			if (i == 2) i = 1;
			if (i == -1) i = 0;
			System.out.println("jist.plugins"+"\t"+"Derivative Method: " + i);
			for (int j = -100; j <= 100; j += 50) {
				System.out.println("jist.plugins"+"\t"+"at " + j + ": 1st: " + 
						(new FunctionNumeric1DDifferentiation(my1DFunc, 1e-5, i)).getDerivative(j));
				System.out.println("jist.plugins"+"\t"+"        2nd: " + 
						(new FunctionNumeric1DDifferentiation(my1DFunc, 1e-5, i)).get2ndDerivative(j));
			}
			System.out.println("jist.plugins"+"\t");
		}
	
		//ND derivatives
		for (int i = -1; i <= 9; i += 2) {
			if (i == 2) i = 1;
			if (i == -1) i = 0;
			System.out.println("jist.plugins"+"\t"+"Derivative Method: " + i);
			for (int j = -100; j <= 100; j += 100) {
				for (int k = -100; k <= 100; k += 100) {
					FunctionNumericNDDifferentiation ndFunc = new FunctionNumericNDDifferentiation(myNDFunc, 1e-5, i);
					double[] deriv = ndFunc.getDerivative(new double[]{j,k});
					double[][] hess = ndFunc.get2ndDerivative(new double[]{j,k});
					System.out.println("jist.plugins"+"\t"+"Location: (" + j + ", " + k + ")");
					System.out.println("jist.plugins"+"\t"+"Gradient: [" + deriv[0] + ", " + deriv[1] + "]");
					System.out.println("jist.plugins"+"\t"+"Hessian: [" + hess[0][0] + ", " + hess[0][1]);
					System.out.println("jist.plugins"+"\t"+"          " + hess[1][0] + ", " + hess[1][1] + "]");
				}

			}
			System.out.println("jist.plugins"+"\t");
		}
	}
}