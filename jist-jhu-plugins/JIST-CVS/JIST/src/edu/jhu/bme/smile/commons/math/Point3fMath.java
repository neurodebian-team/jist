package edu.jhu.bme.smile.commons.math;

import javax.vecmath.Point3f;

public class Point3fMath {

	public static Point3f crossProductPoint(Point3f a, Point3f b) {
		return new Point3f(a.y*b.z-a.z*b.y,a.z*b.x-a.x*b.z,a.x*b.y-a.y*b.x);
	}

	public static Point3f normalizePoint(Point3f a) {
		// TODO Auto-generated method stub
		float mag = (float)Math.sqrt(a.x*a.x+a.y*a.y+a.z*a.z);
		return new Point3f(a.x/mag,a.y/mag,a.z/mag);
	}

}
