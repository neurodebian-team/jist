package edu.jhu.bme.smile.commons.math.specialFunctions;

/**
 * @author bennett
 *

 */
public class Legendre 
{
	/**
	 * *	Legendre polynomials can be defined by the following recurrence relations:
	 *
	 *        P0(X)   = 1
	 *        P1(X)   = X
	 *  (k+1) Pk+1(X) = (2k+1) X Pk(X) - k Pk-1(X) 
	 * @param L - degree
	 * @param x - location value
	 * @return P_L(x)
	 */
	public static double val(int L, double x) {
		if(L<=0)
			return 1;
		if(L==1)
			return x;
		double prev2=1;
		double prev=x;
		double current=prev;
		for(int k=2;k<=L;k++) {			
			current = ((2*k)*x*prev-(k-1)*prev2)/k;
			prev2=prev;
			prev=current;
		}
		return current;
	}

	/**
	 * Associated Legendre Polynomials
	 * 
	 * See: http://en.wikipedia.org/wiki/Legendre_function
	 * 
	 * @param L - degree
	 * @param M - order
	 * @param x - location value
	 * @return P^M_L(x)
	 */
//	Work in progress. Current version not stable for order>10
//	static public double val_mod(int L, int M, double x){
//		if(Math.abs(L)<Math.abs(M))
//			return 0;
//		if(L<0) {
//			return val(-L,M,x);			
//		}		
//		if(M<0) {
//			double v = val(L,-M,x);
//			return Math.pow(-1, M)*SFunc.factorialratio(L-M,L+M)*v;
//		}
//		if(L==0) {
//			return 1; 
//		}
//
//		double []rootn = new double[2*L+1];
//		for(int i=0;i<=2*L;i++)
//			rootn[i] = Math.sqrt((double)i);
//		double s = Math.sqrt(1.-x*x);
//		double []P = new double[L+3];
//		double twocot = x;
//
//		if(x==-1) {
//			if(M==0)
//				if(L%2==1)
//					return -1;
//				else 
//					return 1;
//			else 
//				return 0;
//		}
//		if(x==1)
//		{ 	
//			if(M==0)				
//				return 1;
//			else 
//				return 0;
//		}
//		if(s>0)
//			twocot = -2*x/s;
//		double sn = Math.pow(-s,L);
//		double tol = Math.sqrt(Float.MIN_VALUE);
//		boolean ind = (s>0) && (Math.abs(sn)<=tol);
//
//		if(ind)
//			throw new RuntimeException("unhandled case"); 
//		//			% Find the values of x,s for which there is no underflow, and for
//		//			% which twocot is not infinite (x~=1).
//		if(!Double.isInfinite(twocot)) {
//
//			//			 *  % Produce normalization constant for the m = n function
//
//			double c = 1;
//			for(int i=2;i<=2*L;i+=2)
//				c*=(1-1./i); 
//			//			d = 2:2:2*n;
//			//			c = prod(1-1./d);
//
//			//    % Use sn = (-s).^n (written above) to write the m = n function
//			P[L+1] = Math.sqrt(c)*sn;
//			P[L] = P[L+1]*twocot*L/rootn[rootn.length-1];
//
//			//			% Recur downwards to m = 0
//			for(int m = L-2;m>=0;m--) {
//				P[m+1] = (P[m+2]*twocot*(m+1)
//						-P[m+3]*rootn[L+m+3]*rootn[L-m])/ 
//						(rootn[L+m+2]*rootn[L-m+1]);
//			}
//			//			*/
//		}
//
//
//		double []y = new double[M+1];
//		for(int i=0;i<M+1;i++)
//			y[i]=P[i];
//
//		//	% Polar argument   (x = +-1)
//		//s0 = find(s == 0);
//		//y(1,s0) = x(s0).^n;
//
//
//		//% Calculate the standard A&S functions (i.e., unnormalized) by
//		//% multiplying each row by: sqrt((n+m)!/(n-m)!) = sqrt(prod(n-m+1:n+m))
//		for(int m=1;m<=L-1;m++) {
//			y[m]=SFunc.rootfactorialratio(L-M,L+M)*y[m];
//		}
//		//		y[m+1] = prod(rootn(n-m+2:n+m+1))*y(m+1,:);
//
//		//% Last row (m = n) must be done separately to handle 0!.
//		//% NOTE: the coefficient for (m = n) overflows for n>150.
//		double prodrootn = 1;
//		for(int i=1;i<rootn.length;i++)
//			prodrootn*=rootn[i];
//		y[L+1] = prodrootn*y[L+1];
//		return y[M];
//
//	}

	static public double val(int L, int M, double x){
		if(Math.abs(L)<Math.abs(M))
			return 0;
		if(L<0) {
			return val(-L,M,x);			
		}		
		if(M<0) {
			double v = val(L,-M,x);
			return Math.pow(-1, M)*SFunc.factorialratio(L-M,L+M)*v;
		}

		// Find P_M^M
		double current; 
		if(M==0)
			current =1;
		else 
			current = Math.pow(-1, M)*SFunc.doubleFactorial(2*M-1)*Math.pow(1-x*x, M/2.);
		//Current is now at L=M, M=M

		double last = 0; // P_(M-1)^M = 0; 
		//Now advance the order until L is reached

		double next;
		for(int k=M;k<L;k++) {
			next = ((2*k+1)*x*current-(k+M)*last)/(k-M+1); 
			//Next is now P_(k+1)^M
			last = current; 
			current = next; 
		}
		return current;
	}
}