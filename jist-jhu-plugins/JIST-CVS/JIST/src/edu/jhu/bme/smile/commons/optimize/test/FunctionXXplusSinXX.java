package edu.jhu.bme.smile.commons.optimize.test;

import edu.jhu.bme.smile.commons.optimize.FunctionNumeric1DDifferentiation;
import edu.jhu.bme.smile.commons.optimize.Optimizable1DContinuousDifferentiable;

/**
 * Test case to test 1D optimizers.
 * Function is x^2 + 4*sin(x^2).
 * @author Yufeng Guo, Hanlin Wan
 *
 */
public class FunctionXXplusSinXX implements Optimizable1DContinuousDifferentiable {

	private double dMin, dMax, extrema = 0, delta = 1e-5;

	/**
	 * Default constuctor. Sets the extrema at the origin.
	 * Sets domain to maximum values.
	 */
	public FunctionXXplusSinXX() {
		extrema = 0;
		dMin = Double.MIN_VALUE;
		dMax = Double.MAX_VALUE;
	}

	/**
	 * Constructor to specify extrema and domain.
	 * @param extrema where the extrema is
	 * @param domMin domain minimum
	 * @param domMax domain maximum
	 */
	public FunctionXXplusSinXX(double extrema, double domMin, double domMax) {
		this.extrema = extrema;
		dMin = domMin;
		dMax = domMax;
		
	}

	/**
	 * Gets the first derivative of the function.
	 * Uses the default step size and method
	 * @param x location to calculate derivative
	 * @return derivative at location x
	 */
	public double getDerivative(double x) {
		return (new FunctionNumeric1DDifferentiation(this)).getDerivative(x);  
	}
	
	/**
	 * Gets the first derivative of the function.
	 * Uses the specified step size and method.
	 * @param x location to calculate derivative
	 * @param step step size
	 * @param method method
	 * @return derivative at location x
	 */
	public double getDerivative(double x, double step, int method) {
		return (new FunctionNumeric1DDifferentiation(this, step, method)).getDerivative(x);  
	}

	/**
	 * Gets the second derivative of the function.
	 * Uses the default step size and method
	 * @param x location to calculate derivative
	 * @return 2nd derivative at location x
	 */
	public double get2ndDerivative(double x) {
		return (new FunctionNumeric1DDifferentiation(this)).get2ndDerivative(x);  
	}
	
	/**
	 * Gets the second derivative of the function.
	 * Uses the specified step size and method.
	 * @param x location to calculate derivative
	 * @param step step size
	 * @param method method
	 * @return 2ndderivative at location x
	 */
	public double get2ndDerivative(double x, double step, int method) {
		return (new FunctionNumeric1DDifferentiation(this, step, method)).get2ndDerivative(x);  
	}

	/**
	 * Gets the domain maximum
	 * @return domain max
	 */
	public double getDomainMax() {
		return dMax;
	}

	/**
	 * Gets the domain minimum.
	 * @return domain min
	 */
	public double getDomainMin() {
		return dMin;
	}

	/**
	 * Evaluates the function.
	 * @param x the location to evaluate the function at
	 * @return value of the function at x
	 */
	public double getValue(double x) {
		return (((x-extrema) * (x-extrema)) + 4*Math.sin((x-extrema)*(x-extrema)) + 10);
	}

	/**
	 * Gets the tolerance.
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return delta;
	}
}
