package edu.jhu.bme.smile.commons.optimize;

/**
 * Interface for Differentiable ND functions.
 * @author Yufeng Guo, Hanlin Wan
 */
public interface OptimizerNDContinuousDifferentiable {
	
	/**
	 * Initializes an OptimizableNDContinuousDifferentiable function.
	 * @param function N-dimensional differentiable function to optimize
	 */
	public void initialize(OptimizableNDContinuousDifferentiable function);
	
	/**
	 * Optimizes a funtion.
	 * @param findMinima true if find min, false if find max
	 * @return true if optimization successful, false if failed
	 */
	public boolean optimize(boolean findMinima);
	
	/**
	 * Gets the optimized value.
	 * @return the array of extrema values
	 */
	public double[] getExtrema();
	
	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations();
	
	/**
	 * Gets any status message.
	 * @return status message
	 */
	public String statusMessage(); 
}
