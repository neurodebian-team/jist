package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;


public class FirstPluginTest extends ProcessingAlgorithm{
	private ParamInteger x;
	private ParamInteger y;
	private static final String rcsid =
		"$Id: FirstPluginTest.java,v 1.5 2009/12/15 19:20:01 bennett Exp $";
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "This is a Demo Plugin";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Demo1");
		inputParams.setName("Demo1");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(x=new ParamInteger("Input Variable"));
		x.setValue(new Integer(10));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(y=new ParamInteger("Output Variable"));	
	}


	protected void execute(CalculationMonitor monitor) {
		y.setValue(new Integer(x.getInt()));
		System.out.println(getClass().getCanonicalName()+"\t"+y.getInt());
		System.out.println(getClass().getCanonicalName()+"\t"+"FINISHED");
	}
}
