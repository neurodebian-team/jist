package edu.jhu.bme.smile.commons.optimize;

/**
 * Interface for ND functions.
 * @author Yufeng Guo, Hanlin Wan
 */
public interface OptimizableNDContinuous {

	/**
	 * Gets the number of dimensions of the function.
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions();
	
	/**
	 * Gets the value of a function.
	 * @param x array of inputs
	 * @return the value of the function
	 */
	public double getValue(double x[]); 
	
	/**
	 * Gets the minimum input of the function.
	 * @return array of minimum domains
	 */
	public double[] getDomainMin(); 
	
	/**
	 * Gets the maximum input of the function.
	 * @return array of maximum domains
	 */
	public double[] getDomainMax(); 
	
	/**
	 * Gets the tolerance
	 * @return domain tolerance
	 */
	public double getDomainTolerance();
}
