package edu.jhu.bme.smile.commons.math.specialFunctions;

import Jama.Matrix;

public class SphericalHarmonicRepresentation extends ComplexVector {

	SphericalHarmonics xfm;

	public SphericalHarmonicRepresentation(
			SphericalHarmonics sphericalHarmonics, ComplexMatrix data) {
		super(sphericalHarmonics.matSHProj.times(data));
		xfm = sphericalHarmonics;

	}



}
