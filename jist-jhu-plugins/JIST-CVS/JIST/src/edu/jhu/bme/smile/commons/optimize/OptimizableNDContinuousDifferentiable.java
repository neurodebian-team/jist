package edu.jhu.bme.smile.commons.optimize;

/**
 * Interface for Differentiable ND functions. It extends the OptimizableNDContinuous interface.
 * @author Yufeng Guo, Hanlin Wan
 */
public interface OptimizableNDContinuousDifferentiable extends OptimizableNDContinuous {
	
	/**
	 * Gets the gradient of the function.
	 * @param x location to calculate derivatives
	 * @return gradient evaluated at x
	 */
	public double[] getDerivative(double []x);
	
	/**
	 * Gets the hessian of the function.
	 * @param x location to calculate derivatives
	 * @return hessian evaluated at x
	 */
	public double[][] get2ndDerivative(double []x);
	
	/**
	 * Gets the second derivative of the function in a specific direction
	 * @param x location to calculate derivative
	 * @param dim1 dimension to take the first derivative in
	 * @param dim2 dimension to take the second derivative in
	 * @return 2nd derivative evaluated at x in the specified direction
	 */
	public double get2ndDerivative(double[] x,int dim1, int dim2);
}
