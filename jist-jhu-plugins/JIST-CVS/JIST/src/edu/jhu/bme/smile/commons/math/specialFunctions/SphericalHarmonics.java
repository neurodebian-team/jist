package edu.jhu.bme.smile.commons.math.specialFunctions;

import java.io.StringWriter;

import JSci.maths.Complex;
import Jama.Matrix;



public class SphericalHarmonics {

	// to do: build SH matrix for even valued coefficients
	/*
	 * function [SH,SHorder] = SHmatEven(order,TH,PH)

%lazy
SHorder =[];
SH=[];
idx=1;
for l=0:2:order
    for m=-l:1:l
        SH(:,idx) = spharm(l,m,TH(:),PH(:));
        SHorder(idx) = l;
        idx=idx+1;
    end
end

Use over-sized matrices for complex calculations
I learned a trick a long time ago which I think will work. You can represent a complex number ar + i * ai as a matrix:

[ ar  ai ]
[-ai  ar ]

If you double the size of all your matrices and insert these elements in

	 */

	/*
	 *      


        if(~exist('SHfunc'))
            disp('Generating Func SH Matrix')

            tic;
            [SHfunc,SHorderfunc] = SHmatEven(order,t(:),p(:));
            disp(sprintf('Complete in: %.2f s', toc));
        end

        disp('Performing iSH Transform');
        tic;
        func= real(SHfunc*shCoeff);
        disp(sprintf('Complete in: %.2f s', toc));
        funcF = double(squeeze(sum(reshape(func,[size(XX) length(mask)]),3)));

	 */

	public SphericalHarmonics() {};

	protected ComplexMatrix matSHRecon;
	protected ComplexMatrix matSHProj;
	protected int[] L;
	protected int[] M;
	
	public void recordLM(int [][]LM) {
		for(int i=0;i<L.length;i++) {
//			System.out.println(getClass().getCanonicalName()+"\t"+"LM:"+L[i]+" "+M[i]);
			LM[i][0]=L[i];
			LM[i][1]=M[i];
		}
		
	}

	public int getL(int r) {
		return L[r];
	}
	
	public int getM(int r) {
		return M[r];
	}
	
	public ComplexMatrix getRecon() {
		return (ComplexMatrix)matSHRecon;
	}
	
	public ComplexMatrix matSHProj() {
		return (ComplexMatrix)matSHProj;
	}
	
	public static int numberOfCoefficientsInEvenOrder(int order) {
		return (1+order/2)*(order+1);
	}
	
	public static void createLM(int order, int[][] LM) {
		int idx=0;
		for(int l=0;l<=order;l+=2){
			for(int m=-l;m<=l;m++) {
				LM[idx][0]=l;
				LM[idx][1]=m;
				idx++;				
			}
		}
	}
	
	public static SphericalHarmonics evenOrderTransform(int order, double []TH, double []PH) {
		SphericalHarmonics xfm = new SphericalHarmonics();
		// Number of terms: (1+n/2).*(n+1)
		int nterms = numberOfCoefficientsInEvenOrder(order);
		xfm.matSHRecon = new ComplexMatrix(TH.length,nterms);
		xfm.L = new int[nterms];
		xfm.M = new int[nterms];

		int idx=0;
		for(int l=0;l<=order;l+=2){
			for(int m=-l;m<=l;m++) {
				xfm.L[idx]=l;
				xfm.M[idx]=m;
				for(int jx=0;jx<TH.length;jx++) {
					Complex val = getCoeff(l, m, TH[jx], PH[jx]);
					xfm.matSHRecon.setComplex(jx,idx,val.real(), val.imag());	// real
					
				}
				idx++;				
			}
		}
		xfm.matSHProj = xfm.matSHRecon.inverse();
		return xfm;
	}

	public SphericalHarmonicRepresentation transform(SphericalHarmonicRepresentation complexData) {		
		return new SphericalHarmonicRepresentation(this,complexData.result);
	}
	
	public SphericalHarmonicRepresentation transform(double []realData) {
		ComplexMatrix data2 = new ComplexMatrix(realData.length,1);	
		for(int i=0;i<realData.length;i++) {
			data2.setComplex(i,0,realData[i],0);		
		}
		return new SphericalHarmonicRepresentation(this,data2);
	}
	
	public SphericalHarmonicRepresentation transform(double []realData, double []imagData) {
		ComplexMatrix data2 = new ComplexMatrix(realData.length,1);		
		for(int i=0;i<realData.length;i++) {
			data2.setComplex(i,0,realData[i],imagData[i]);		
		}		
		return new SphericalHarmonicRepresentation(this,data2);
	}

	public ComplexVector inverseTransform(SphericalHarmonicRepresentation complexData) {		
		return new ComplexVector(matSHRecon.times(complexData.result));
	}
	
	public ComplexVector inverseTransform(double []realData) {
		ComplexMatrix data2 = new ComplexMatrix(realData.length,1);	
		for(int i=0;i<realData.length;i++) {
			data2.setComplex(i,0,realData[i],0);		
		}
		return new ComplexVector(this.matSHRecon.times(data2));
	}
	
	public ComplexVector inverseTransform(double []realData, double []imagData) {
		ComplexMatrix data2 = new ComplexMatrix(realData.length,1);		
		for(int i=0;i<realData.length;i++) {
			data2.setComplex(i,0,realData[i],imagData[i]);		
		}		
		return new ComplexVector(this.matSHRecon.times(data2));
	}

	/**
	 * 
	 % NOTE: It is very important to keep the various usages of THETA and PHI
	% straight.  For this function THETA is the Azimuthal/Longitude/Circumferential 
	% coordinate and is defined on the interval [0,2*pi], whereas PHI is the 
	% Altitude/Latitude/Elevation and is defined on the interval [0,pi].  Also note that 
	% the conversion to cartesian coordinates requires that PHI be offset by pi/2 so 
	% that the conversion is on the interval [-pi/2,pi/2]. 
	 * @param L - Spherical harmonic degree, [1x1]
	 * @param M - Spherical harmonic order,  [1x1]
	 * @param Theta - Circumferential coordinates
	 * @param Phi - Latitudinal coordinates
	 * @return
	 */
	public static Complex getCoeff(int L, int M, double Theta, double Phi) {
		//		% Define constants (REQUIRED THAT L(DEGREE)>=M(ORDER))
		double shift = 0;
		if(M<0) shift = (Math.PI/2)/M;

		if(L<M)
			throw new RuntimeException("The ORDER (M) must be less than or eqaul to the DEGREE(L)."); 

		Phi  = Phi-Math.PI/2; // % Altitude /Latitude /Elevation

		double Lmn=Legendre.val(L,Math.abs(M),Math.cos(Phi));
		double a1=((2*L+1)/(4*Math.PI));

		double a2=SFunc.factorialratio(L-M,L+M);
		double C=Math.sqrt(a1*a2);

		JSci.maths.Complex Ymn = new JSci.maths.Complex(
				C*Lmn*Math.cos((M*Theta+shift)),
				C*Lmn*Math.sin((M*Theta+shift)));
		return Ymn;
		//		double Ymn=C*Lmn*Math.exp(i*(M*Theta+shift));

	}
	
	public static double getFRTCoeff(int L, int M, double Theta, double Phi) {
		JSci.maths.Complex Ymn = getCoeff(L,M,Theta,Phi);
		double Y;
		if (M<0) Y=Math.sqrt(2)*Ymn.real();
		else if (M>0) Y=Math.sqrt(2)*Ymn.imag();
		else Y=Ymn.real();
		return Y;
	}

	public static SphericalHarmonics evenOrderTransform(int SHorder,
			float[][] grads) {
		double []TH=new double[grads.length];
		double []PH=new double[grads.length];
		for(int i=0;i<TH.length;i++) {
			double hypxy = Math.sqrt(grads[i][0]*grads[i][0]+grads[i][1]*grads[i][1]);
			TH[i]=Math.atan2(grads[i][1],grads[i][0]);
			PH[i]=Math.atan2(grads[i][2],hypxy);
		}
		return evenOrderTransform(SHorder, TH, PH);
	}
	
	public static double[][] cart2sph(int SHorder, float[][] grads) {
		double sph[][] = new double[grads.length][2]; 
		for(int i=0;i<sph.length;i++) {
			double hypxy = Math.sqrt(grads[i][0]*grads[i][0]+grads[i][1]*grads[i][1]);
			sph[i][0]=Math.atan2(grads[i][1],grads[i][0]);
			sph[i][1]=Math.atan2(grads[i][2],hypxy);
		}
		return sph;
	}
	
	public String toString() {
		return matSHProj.toString();
		
	}

}
