package edu.jhu.bme.smile.commons.optimize;

/**
 * Interface for Differentiable 1D functions. It extends the Optimizable1DContinuous interface.
 * @author Yufeng Guo, Hanlin Wan
 */
public interface Optimizable1DContinuousDifferentiable extends Optimizable1DContinuous {
	
	/**
	 * Gets the value of the derivative of the function.
	 * @param x location to evaluate the derivative
	 * @return the value of the derivative at x
	 */
	public double getDerivative(double x);

	/**
	 * Gets the value of the 2nd derivative of the function.
	 * @param x location to evaluate the derivative
	 * @return the value of the 2nd derivative at x
	 */
	public double get2ndDerivative(double x);

	
}
