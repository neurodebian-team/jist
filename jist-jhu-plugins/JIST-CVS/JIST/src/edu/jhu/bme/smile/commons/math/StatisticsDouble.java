package edu.jhu.bme.smile.commons.math;

import java.util.Arrays;

public class StatisticsDouble {
	private static final double log2pi = Math.log(Math.PI*2);

	
	public static double sum(double []x) {
		double y=0;
		int cnt=0;
		for(int i=0;i<x.length;i++) {
			if(!Double.isNaN(x[i])) {
				y+=x[i];
				cnt++;
			}
		}
		return y;			
	}
	
	public static double product(double []x) {
		double y=1;
		int cnt=0;
		for(int i=0;i<x.length;i++) {
			if(!Double.isNaN(x[i])) {
				y*=x[i];
				cnt++;
			}
		}
		return y;			
	}
	
	public static double mean(double []x) {
		double y=0;
		int cnt=0;
		for(int i=0;i<x.length;i++) {
			if(!Double.isNaN(x[i])) {
				y+=x[i];
				cnt++;
			}
		}
		return y/cnt;			
	}
	public static double square(double x) {return x*x;};

	public static double std(double []x) {
		double y=0;
		double mn=mean(x);
		int cnt=0;
		for(int i=0;i<x.length;i++) {
			if(!Double.isNaN(x[i])) {
				y+=square(x[i]-mn);
				cnt++;
			}
		}
		return Math.sqrt(y/(cnt-1));			
	}

	public static double median(double []x) {
		if(x.length<1)
			return Double.NaN;
		if(x.length==1)
			return x[0];
		java.util.Arrays.sort(x);
		double L=((double)x.length)/2;
		return (x[(int)java.lang.Math.floor(L)]+x[(int)java.lang.Math.ceil(L)])/2;				
	}

	public static double medianRobust(double []x) {
		int cnt=0;
		for(int i=0;i<x.length;i++) {
			cnt+=(Double.isNaN(x[i])?1:0);
		}
		if(cnt==0)
			return median(x);
		double []xx=new double[x.length-cnt];
		cnt=0;
		for(int i=0;i<x.length;i++) {
			if(!Double.isNaN(x[i])) {
				xx[cnt]=x[i];
				cnt++;
			}
		}
		return median(xx);						
	}
	
	/**
	 * Computes the histogram using equally spaced bins.  
	 * The first column of the output array is the lower bounds for each bin
	 * The second column of the output array is the count in that bin.
	 * @param x - the input data
	 * @param bins - the number of bins to use
	 * @return the output
	 */
	public static double[][] histogram(double[] x, int bins){
		double[][] hist = new double[bins][2];
		double min = min(x);
		double max = max(x);
		// determine the bin sizes
		double k = (max-min)/(bins);
		System.out.println("jist.plugins"+"\t"+"Min: " + min);
		System.out.println("jist.plugins"+"\t"+"Max: " + max);
		System.out.println("jist.plugins"+"\t"+"Bin size: " + k);
		int i=0;
		hist[0][0]=min;
		for(i=1; i<bins; i++){  hist[i][0]=min+(k*i); }
		int j=1;
		boolean a = true;
		for(i=0; i<x.length; i++){
			j=1;
			a=true;
//			System.out.println("jist.plugins"+"\t"+x[i]);
			while(j<bins && a){
				if(x[i]>= hist[j-1][0] && x[i]<hist[j][0]){
					a=false;
					hist[j-1][1]++;
//					System.out.println("jist.plugins"+"\t"+"Incremented " +j);
				}
				j++;
			}
			if(a){
//				System.out.println("jist.plugins"+"\t"+"in here");
//				System.out.println("jist.plugins"+"\t"+"Incremented " +(j-1));
				hist[j-1][1]++;
			}
//			System.out.println("jist.plugins"+"\t"+);
		}
		return hist;
	}
	
	/**
	 * Computes the joint histogram using equally spaced bins.  
	 * The first row of the output array is the lower bounds for each bin
	 * The second row of the output array is the count in that bin.
	 * @param x - the input data
	 * @param bins - the number of bins to use
	 * @return the output
	 */
	public static double[][] jointhistogram(double[] x, double[] y, int bins){
		
		if(x.length != y.length){
			System.out.println("jist.plugins"+"\t"+"Inputs to joint histogram must have like dimension!");
			return null;
		}
		
		double[][] hist = new double[bins+2][bins];
		
		//define bounds for x 
		double min = min(x);
		double max = max(x);
		// determine the bin sizes
		double width = (max-min)/(bins);
		System.out.println("jist.plugins"+"\t"+"Min x: " + min);
		System.out.println("jist.plugins"+"\t"+"Max x: " + max);
		System.out.println("jist.plugins"+"\t"+"Bin size x: " + width);
		int i=0;
		hist[0][0]=min;
		for(i=1; i<bins; i++){  hist[0][i]=min+(width*i); }
		
		//define bounds for y
		min = min(y);
		max = max(y);
		// determine the bin sizes
		width = (max-min)/(bins);
		System.out.println("jist.plugins"+"\t"+"Min y: " + min);
		System.out.println("jist.plugins"+"\t"+"Max y: " + max);
		System.out.println("jist.plugins"+"\t"+"Bin size y: " + width);
		
		hist[1][0]=min;
		for(i=1; i<bins; i++){ hist[1][i]=min+(width*i); }
		
		int j=1;
		int k=1;
		boolean a = true;
		boolean b = true;
		for(i=0; i<x.length; i++){
			j=1;
			k=1;
			a=true;
			b=true;
//			System.out.println("jist.plugins"+"\t"+x[i]);
			while(a && b && j<bins && k<bins){
				if(a && x[i]>= hist[0][j-1] && x[i]<hist[0][j]){
					a=false;
//					System.out.println("jist.plugins"+"\t"+"Incremented " +j);
				}else{
					j++;
				}
				if(b && y[i]>= hist[1][k-1] && y[i]<hist[1][k]){
					b=false;
//					System.out.println("jist.plugins"+"\t"+"Incremented " +k);
				}else{
					k++;
				}
			}
//			System.out.println("jist.plugins"+"\t"+"(" +x[i]+","+y[i]+") goes in: " + "(" +(j-1)+","+(k-1)+")");
			
			hist[j+1][k-1]++;
		}
		return hist;
	}
	
	/**
	 * Computes the histogram using the specified bins
	 * The first column of the output array is the lower bounds for each bin
	 * The second column of the output array is the count in that bin.
	 * @param x - the input data
	 * @param bins - the array of bins
	 * @return the output
	 */
	public static double[][] histogram(double[] x, double[] bins){
		double[][] hist = new double[bins.length][2];
		int i=0;
		for(i=1; i<bins.length; i++){  hist[i][0]=bins[i]; }
		int j=1;
		boolean a = true;
		for(i=0; i<x.length; i++){
			j=1;
			a=true;
//			System.out.println("jist.plugins"+"\t"+x[i]);
			while(j<bins.length && a){
				if(x[i]>= hist[j-1][0] && x[i]<hist[j][0]){
					a=false;
					hist[j-1][1]++;
//					System.out.println("jist.plugins"+"\t"+"Incremented " +j);
				}
				j++;
			}
			if(a){
//				System.out.println("jist.plugins"+"\t"+"in here");
//				System.out.println("jist.plugins"+"\t"+"Incremented " +(j-1));
				hist[j-1][1]++;
			}
//			System.out.println("jist.plugins"+"\t"+);
		}
		return hist;
	}
	
	public static double[][] normalizeHistogram(double[][] hist){
		double[][] histout = new double[hist.length][hist[0].length];
		double sum = 0;
		//compute normalization constant
		for(int i=0; i<hist.length; i++){
			sum+=hist[i][1];
		}
		
		//normalize
		for(int i=0; i<hist.length; i++){
			histout[i][0]=hist[i][0];
			histout[i][1]=(hist[i][1]/sum);
		}
		return histout;
	}
	
	public static double[][] normalizeJointHistogram(double[][] hist){
		double[][] histout = new double[hist.length][hist[0].length];
		double sum = 0;
		//compute normalization constant
		for(int i=2; i<hist.length; i++){
			for(int j=0; j<hist[0].length; j++){
				sum+=hist[i][j];
			}
		}
		
		//transfer bin locations
		for(int i=0; i<2; i++){
			for(int j=0; j<hist[0].length; j++){
				histout[i][j]=hist[i][j];
			}
		}
		
		//normalize
		for(int i=2; i<hist.length; i++){
			for(int j=0; j<hist[0].length; j++){
				histout[i][j]=hist[i][j]/sum;
			}
		}
		return histout;
	}
	
	/**
	 * Computes the Kullbach-Liebler divergence between two data sets x and y
	 * @param x - input data 
	 * @param y - input data 2
	 * @return the KL divergence
	 */
	public static double KLdivergence(double[] x, double[] y, int binnum){
		double eps = 0.00000000001;
		
		//determine the bins.
		double maxx = max(x);
		double maxy = max(y);
		double max;
		if(maxx>maxy){max=maxx;}
		else{ max=maxy; }
	
		double minx = min(x);
		double miny = min(y);
		double min;
		if(minx<miny){min=minx;}
		else{ min=miny; }
		
		double k = (max-min)/(binnum);
		double[] bins = new double[binnum];
		int i=0;
		bins[0]=min;
		for(i=1; i<binnum; i++){  bins[i]=min+(k*i); }
		
		double[][] histx = normalizeHistogram(histogram(x,bins));
		double[][] histy = normalizeHistogram(histogram(y,bins));
		
		double kld = 0;
		for(i=0; i<histx.length; i++){
			kld+=(histx[i][1]+eps)*(Math.log((histx[i][1]+eps)/(histy[i][1]+eps)));
		}
		return kld;
	}
	
	public static String hist2string(double[][] hist){
		String s = "";
		for(int i=0; i<hist.length; i++){
			s+=hist[i][0]+"\t"+hist[i][1];
			if(i==hist.length-1){
				s+="\n";
			}
		}
		return s;
	}

	public static double QnRobust(double []x) {
		int cnt=0;
		for(int i=0;i<x.length;i++) {
			cnt+=(Double.isNaN(x[i])?1:0);
		}
		if(cnt==0)
			return Qn(x);
		double []xx=new double[x.length-cnt];
		cnt=0;
		for(int i=0;i<x.length;i++) {
			if(!Double.isNaN(x[i])) {
				xx[cnt]=x[i];
				cnt++;
			}
		}
		return Qn(xx);						
	}

	public static double max(double []x) {		
		return kthOrderStat(x,x.length);				
	}

	public static double min(double []x) {		
		return kthOrderStat(x, 1);
	}
	/* Qn code adapted from 
Time-efficient algorithms for two highly robust estimators of scale
C Croux, PJ Rousseeuw - Computational Statistics, 1992 - econ.kuleuven.be
http://www.econ.kuleuven.be/public/ndbae06/PDF-FILES/snqn.pdf
	 */ 
	public static double Qn(double []x) {
		if(x.length<=1)
			return Double.NaN;
		// Qn = dn * 2.2219 * {|x_i-x_j|; i<j}_(k)

		int n=x.length;
		Arrays.sort(x); 
		double []y = x;
		double []work = new double[n];
		int []left = new int[n], right=new int[n],weight=new int[n],
		Q = new int[n], P = new int [n];
		double trial,Qn=0,dn;
		int i,j,jj,h,k,knew,jhelp,nL,nR,sumQ,sumP;
		boolean found; 



		h=n/2+1;
		k=h*(h-1)/2;


		for(i=1;i<=n;i++) {
			left[i-1]=n-i+2;
			right[i-1]=n;
		}
		jhelp = n*(n+1)/2;
		knew =k+jhelp;
		nL=jhelp;
		nR=n*n;
		found=false;

		while(((nR-nL)>n) && !found) {
			j=1;
			for(i=2;i<=n;i++) {
				if(left[i-1]<=right[i-1]) {
					weight[j-1]=right[i-1]-left[i-1]+1;
					jhelp = left[i-1]+weight[j-1]/2;
					work[j-1]=y[i-1]-y[n+1-jhelp-1];
					j++;					
				}
			}
			trial=whimed(work,weight,j-1);
			j=0;
			for(i=n;i>=1;i--) {
				while((j<n) && ((y[i-1]-y[n-j-1])<trial)) {
					j=j+1;				
				} 
				P[i-1]=j;

			}
			j=n+1;
			for(i=1;i<=n;i++){
				while((y[i-1]-y[n-j+2-1])>trial) {
					j--;
				} 
				Q[i-1]=j;
			}
			sumP=0;
			sumQ=0;
			for(i=1;i<=n;i++) {
				sumP+=P[i-1];
				sumQ+=Q[i-1]-1;				
			}
			if(knew<sumP) {
				for(i=1;i<=n;i++) {
					right[i-1]=P[i-1];
				}
				nR=sumP;
			} else 
				if(knew>sumQ) {
					for(i=1;i<=n;i++) {
						left[i-1]=Q[i-1];
					}
					nL=sumQ;
				} else {
					Qn=trial;
					found = true;
				}
		}
		if(!found) {
			j=1;
			for(i=2;i<=n;i++) {
				if(left[i-1]<=right[i-1]) {
					for(jj=left[i-1];jj<=right[i-1];jj++) {
						work[j-1]=y[i-1]-y[n-jj+1-1]; 
						j++;
					}
				}
			}
			Qn=pull(work,j-1,knew-nL);			
		}
		switch(n) {
		case 1: dn=1.000; break;
		case 2: dn=0.399; break;
		case 3: dn=0.994; break;
		case 4: dn=0.512; break;
		case 5: dn=0.844; break;
		case 6: dn=0.611; break;
		case 7: dn=0.857; break;
		case 8: dn=0.669; break;
		case 9: dn=0.872; break;		
		default:
			if(n%2==1)
				dn=n/(n+1.4);
			else 
				dn=n/(n+3.8);
		break;
		}
		return dn*2.2219*Qn;

	}
	private static double pull(double[] work, int n, int j) {
		double []subWork = new double[n];
		for(int i=0;i<n;i++)
			subWork[i]=work[i];
		return kthOrderStat(subWork,Math.min(n, j));
	}
	private static double whimed(double[] a, int[] iw, int n) {
		double []acand=new double[n]; 
		int []iwcand=new int[n];

		double trial,whimed;
		int wtotal, wrest, wleft, wmid, wright,kcand;
		int nn=n,i;
		wtotal=0;
		for(i=1;i<=nn;i++)
			wtotal+=iw[i-1];
		wrest=0;
		while(true) {
			trial=pull(a,nn,nn/2+1);
			wleft=0;
			wmid=0;
			wright=0;
			for(i=1;i<=nn;i++) {
				if(a[i-1]<trial) {
					wleft+=iw[i-1];
				}else {
					if(a[i-1]>trial) {
						wright+=iw[i-1];

					}else {
						wmid+=iw[i-1];
					}
				}
			}
			if((2*wrest+2*wleft)>wtotal) {
				kcand=0;
				for(i=1;i<=nn;i++) {
					if(a[i-1]<trial) {
						kcand+=1;
						acand[kcand-1]=a[i-1];
						iwcand[kcand-1]=iw[i-1];

					}
				}
				nn=kcand;			
			} else {
				if((2*wrest+2*wleft+2*wmid)>wtotal) {
					whimed=trial;
					return whimed; 
				} else {
					kcand=0;
					for(i=1;i<=nn;i++) {
						if(a[i-1]>trial) {
							kcand=kcand+1;
							acand[kcand-1]=a[i-1];
							iwcand[kcand-1]=iw[i-1];
						}
					}
					nn=kcand;
					wrest=wrest+wleft+wmid;
				}
			}
			for(i=1;i<=nn;i++){
				a[i-1]=acand[i-1];
				iw[i-1]=iwcand[i-1];
			}
		}
	}

	private static double kthOrderStat(double []x, int i) {
		if(x.length<i){
			System.out.println("jist.plugins"+"\t"+"Warning: order statistic > data length");
			return Double.NaN;
		}
		if(i<1){
			System.out.println("jist.plugins"+"\t"+"Warning: order statistic < 1?");
			return Double.NaN;
		}
		java.util.Arrays.sort(x);
		
		return x[i-1];
	}

	public static double logRicianPDF(double []mu, double []sigma, double []x) {
		double val=0;
		for(int i=0;i<mu.length;i++) 
			val+=(double)logRicianPDF((float)mu[i],(float)sigma[i],(float)x[i]);
		return val;
	}

	public static double[] robustLogRicianPDF(double []mu, double []sigma, double []x, double []result) {
		if(result==null) 
			result = new double[mu.length];
		for(int i=0;i<mu.length;i++) 
			result[i]=(double)logRicianPDF((float)mu[i],(float)sigma[i],(float)x[i]);
		return result;
	}

	private static float logRicianPDF(float mu, float sigma, float x) {

		// log likelihood =
		// log(x/sigma^2) - x^2/sigma^2 - mu^2/sigma^2
		float LL;
		if (x < 0)
			LL = 100 * x - 10;
		// float LL =
		// -(float)(Math.log(x/sigma/sigma)-(x*x+mu*mu)/sigma/sigma+Math.log(JSci.maths.SpecialMath.modBesselFirstZero(mu*x/sigma/sigma)));
		else {
			double arg=mu * x / sigma / sigma;
			if(arg<=120) {
//				LL = 1f * (fastLog((x / sigma / sigma)) 
//				+ (-(x * x + mu * mu) / (2 * sigma * sigma))
//				+ fastLogBessel( x / sigma
//				/ sigma));
				LL = 1f * (float) Math.log((x / sigma / sigma)
						* Math.exp(-(x * x + mu * mu) / (2 * sigma * sigma))
						* JSci.maths.SpecialMath.modBesselFirstZero(arg));

			} else {
				LL = 1f * (float)( Math.log((x / sigma / sigma))+
						-(x * x + mu * mu) / (2 * sigma * sigma) +
						arg-0.5*(log2pi+Math.log(arg)));

			}
		}
		/*
		 * if(LL>20) LL = -20;
		 */
//		if (Float.isInfinite(LL) || Float.isNaN(LL))
//		LL = -20;
		// System.out.println("jist.plugins"+"\t"+x+" "+mu+" "+sigma+" "+LL);
		return LL;
	}
	public static void main(String args[]) {
		double []test = {10,9,8,7,6,5,2,3,4,1};

//		for(int i=1;i<=test.length;i++)
//			System.out.println("jist.plugins"+"\t"+kthOrderStat(test, i));
//		System.out.println("jist.plugins"+"\t"+Qn(test));
		
		double[][] hist = histogram(test,9);
		double[][] nhist = normalizeHistogram(hist);
		for(int i=0; i<hist.length; i++){
			System.out.println("jist.plugins"+"\t"+hist[i][0]+ "\t" + hist[i][1]);
		}
		System.out.println("jist.plugins"+"\t");
		System.out.println("jist.plugins"+"\t");
		
		for(int i=0; i<nhist.length; i++){
			System.out.println("jist.plugins"+"\t"+nhist[i][0]+ "\t" + nhist[i][1]);
		}
		
//		double[][] hist = jointhistogram(test, test, 10);
//		double[][] nhist = normalizeJointHistogram(hist);
//		for(int i=0; i<hist.length; i++){
//			for(int j=0; j<hist[0].length; j++){
//				System.out.print(hist[i][j]+"\t");
//			}
//			System.out.print("\n");
//		}
//		System.out.println("jist.plugins"+"\t"+);
//		System.out.println("jist.plugins"+"\t"+);
//		for(int i=0; i<nhist.length; i++){
//			for(int j=0; j<nhist[0].length; j++){
//				System.out.print(nhist[i][j]+"\t");
//			}
//			System.out.print("\n");
//		}
	}
}
