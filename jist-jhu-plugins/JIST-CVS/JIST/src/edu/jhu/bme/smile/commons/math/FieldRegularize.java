package edu.jhu.bme.smile.commons.math;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

/**
 * 
 * public class FieldRegularize 
 * 
 * This class contains static methods to regularize (smooth) a 2d
 * field using polynomial basis sets. Float.NaN values indicate points
 * at which the data in the field should be ignored.
 * 
 * @author bennett
 * 
 */
public class FieldRegularize {


	/**
	 * @param f - a floating point number
	 * @return boolean - true if f is not NaN and is finite 
	 */
	public static final boolean isFinite(float f) {
		return !(Float.isNaN(f) || Float.isInfinite(f));
	}


	/**
	 * @param degX - the number of degrees of freedom in the polynomial for the the x (1st) dimension 
	 * @param degY - the number of degrees of freedom in the polynomial for the the y (2nd) dimension
	 * @param field - the data to be regularized
	 * @return - polynomial fit to the value with Chebyshev polynomials 
	 */
	public static final float[][] regularize2DChebyshev(int degX, int degY,
			float[][] field) {

		// First, count the number of non-NaN elements
		int sizeOrig = field.length * field[0].length;
		int size = sizeOrig;
		for (int i = 0; i < field.length; i++)
			for (int j = 0; j < field[i].length; j++)
				if (!isFinite(field[i][j]))
					size--;
		
		if(size<((1 + degX) * (1 + degY))) {
			// System is under-determined! Return all NaN.
			float [][]ret =new float[field.length][field[0].length];
			for(int i=0;i<ret.length;i++)
				for(int j=0;j<ret[0].length;j++)
					ret[i][j]=Float.NaN;
			return ret;
			
		}
		Jama.Matrix data = new Jama.Matrix(size, (1 + degX) * (1 + degY));
		Jama.Matrix dataOrig = new Jama.Matrix(sizeOrig, (1 + degX) * (1 + degY));
		Jama.Matrix values = new Jama.Matrix(size, 1);
		Matrix ChebyCoeff = new Matrix(1, (1 + degX) * (1 + degY));		
		float maxX = field.length - 1;
		float maxY = field[0].length - 1;

		int index = 0,ii=0;
		// int []cols = new int[field[0].length];
		for (int i = 0; i < field.length; i++)
			for (int j = 0; j < field[i].length; j++) {
				fill2DChebyCoefficients(ChebyCoeff, degX, degY,
						2 * ((float) i / maxX - 0.5f), 
						2 * ((float) j / maxY - 0.5f));
				dataOrig.setMatrix(ii, ii, 0,
						(1 + degX) * (1 + degY) - 1, ChebyCoeff);
				ii++;
				if (isFinite(field[i][j])) {
					values.set(index, 0, field[i][j]);

					data.setMatrix(index, index, 0,
							(1 + degX) * (1 + degY) - 1, ChebyCoeff);
					index++;
				}
			}



		Matrix result = pseudoInverse(data).times(values);
		Matrix regularized = dataOrig.times(result);

		float[][] ret = new float[field.length][field[0].length];
		index = 0;
		for (int i = 0; i < field.length; i++)
			for (int j = 0; j < field[i].length; j++) {
				//				if (isFinite(field[i][j])) {
				ret[i][j] = (float) regularized.get(index, 0);
				index++;
			}
		return ret;
	}

	/**
	 * @param degX - the number of degrees of freedom in the polynomial for the the x (1st) dimension 
	 * @param degY - the number of degrees of freedom in the polynomial for the the y (2nd) dimension
	 * @param field - the data to be regularized
	 * @param weight - pixel-by-pixel weightings for polynomial weighting
	 * @return - polynomial fit to the value with Chebyshev polynomials using weighted least squares 
	 */
	public static final float[][] regularize2DChebyshevWLS(int degX, int degY,
			float[][] field, float [][]weight) {
		return regularize2DChebyshevWLS(degX,degY,field,weight,new Matrix(1,(1 + degX) * (1 + degY)));
	}

	/**
	 * @param degX - the number of degrees of freedom in the polynomial for the the x (1st) dimension 
	 * @param degY - the number of degrees of freedom in the polynomial for the the y (2nd) dimension
	 * @param field - the data to be regularized
	 * @param weight - pixel-by-pixel weightings for polynomial weighting
	 * @param result - Matrix to be used for storage of the resulting polynomial fit 
	 * @return - polynomial fit to the value with Chebyshev polynomials using weighted least squares 
	 */
	public static final float[][] regularize2DChebyshevWLS(int degX, int degY,
			float[][] field, float [][]weight, Matrix result) {
		// First, count the number of non-NaN elements
		int sizeOrig = field.length * field[0].length;
		int size = sizeOrig;
		for (int i = 0; i < field.length; i++)
			for (int j = 0; j < field[i].length; j++)
				if (!isFinite(field[i][j]))
					size--;
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:size "+size); System.out.flush();
		if(size<((1 + degX) * (1 + degY))) {
			// System is under-determined! Return all NaN.
			float [][]ret =new float[field.length][field[0].length];
			for(int i=0;i<ret.length;i++)
				for(int j=0;j<ret[0].length;j++)
					ret[i][j]=Float.NaN;
			return ret;
			
		}
		Jama.Matrix data = new Jama.Matrix(size, (1 + degX) * (1 + degY));
		Jama.Matrix dataOrig = new Jama.Matrix(size, (1 + degX) * (1 + degY));

		Jama.Matrix values = new Jama.Matrix(size, 1);
		Matrix ChebyCoeff = new Matrix(1, (1 + degX) * (1 + degY));
		float maxX = field.length - 1;
		float maxY = field[0].length - 1;
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:FillCheby "); System.out.flush();
		
		int index = 0,ii=0;
		// int []cols = new int[field[0].length];
		for (int i = 0; i < field.length; i++)
			for (int j = 0; j < field[i].length; j++) {
				fill2DChebyCoefficients(ChebyCoeff, degX, degY,
						2 * ((float) i / maxX - 0.5f), 
						2 * ((float) j / maxY - 0.5f));
				
				
				ii++;
				if (isFinite(field[i][j])) {
					dataOrig.setMatrix(index, index, 0,
							(1 + degX) * (1 + degY) - 1, ChebyCoeff);
					
					ChebyCoeff=ChebyCoeff.times(Math.sqrt(weight[i][j]));
					
					values.set(index, 0, field[i][j]*Math.sqrt(weight[i][j]));

					data.setMatrix(index, index, 0,
							(1 + degX) * (1 + degY) - 1, ChebyCoeff);
					index++;
				}
			}
	
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:B "+data.getRowDimension()+" "+data.getColumnDimension()); System.out.flush();
		
		Matrix result2 = pseudoInverse(data).times(values);
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:B2 "); System.out.flush();
		result.setMatrix(0,0,0,(1 + degX) * (1 + degY) - 1,result2.transpose());
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:C "); System.out.flush();
		Matrix regularized = dataOrig.times(result2);				
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:D "+field.length); System.out.flush();
		float[][] ret = new float[field.length][field[0].length];
		index = 0;
		for (int i = 0; i < field.length; i++)
			for (int j = 0; j < field[i].length; j++){
				if (isFinite(field[i][j])) {
					ret[i][j] = (float) regularized.get(index, 0);
					index++;
				} else 
					ret[i][j]=Float.NaN;
			}
//		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:E "); System.out.flush();
		return ret;

	}



	/**
	 * @param deg - degree of 1D Chebyshev polynomial coefficient
	 * @param x - input value
	 * @return - value of degree deg Chebyshev polynomial evaluated at x
	 */
	private static float[] computeChebyshevCoefficient(int deg, float x) {
		float[] ret = new float[deg + 1];
		ret[0] = 1;
		if (deg > 0)
			ret[1] = x;
		if (deg >= 2)
			for (int i = 2; i <= deg; i++)
				ret[i] = 2 * x * ret[i - 1] - ret[i - 2];
		return ret;
	}

	/**
	 * @param ChebyCoeff - destination Matrix
	 * @param degX - the number of degrees of freedom in the polynomial for the the x (1st) dimension 
	 * @param degY - the number of degrees of freedom in the polynomial for the the y (2nd) dimension
	 * @param x - x location in [0,1]
	 * @param y - y location in [0,1]
	 */
	private static void fill2DChebyCoefficients(Matrix ChebyCoeff, int degX,
			int degY, float x, float y) { 
		float[] chebyX = computeChebyshevCoefficient(degX, x);
		float[] chebyY = computeChebyshevCoefficient(degY, y);
		for (int i = 0; i <= degX; i++)
			for (int j = 0; j <= degY; j++)
				ChebyCoeff.set(0, i * (degY + 1) + j, chebyX[i] * chebyY[j]);
	}

	/**
	 * @param data - input matrix
	 * @return the pseudo-inverse of the input matrix
	 */
	public static Matrix pseudoInverse(Matrix data) {

		SingularValueDecomposition svd = data.svd();
		
		Matrix v = svd.getV();
		
		Matrix sinv = svd.getS();
		
		for(int j=0;j<sinv.getColumnDimension();j++)
			sinv.set(j, j, 1./sinv.get(j,j));
		
		Matrix ut = svd.getU().transpose();
		svd=null;
		
		return v.times(sinv.times(ut));

	}

}
