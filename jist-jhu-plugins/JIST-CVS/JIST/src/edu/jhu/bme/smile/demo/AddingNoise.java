package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


public class AddingNoise extends ProcessingAlgorithm{
	/*******************************************************
	 * Input Parameters
	 *******************************************************/
	private ParamVolume noise;
	private ParamVolume img;

	/*******************************************************
	 * Output Parameters
	 *******************************************************/
	private ParamVolume img_with_noise;

	/*******************************************************
	 * CVS
	 *******************************************************/
	private static final String rcsid =
		"$Id: AddingNoise.java,v 1.4 2009/12/15 17:29:08 navid1362 Exp $";
	private static final String cvsversion =
		"$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Add noise to an image.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Adding Noise");
		inputParams.setName("Adding_Noise");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Robert Kim","rkim35@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/**************************************
		 * Input Initialization
		 **************************************/
		inputParams.add(noise=new ParamVolume("Noise Volume"));
		inputParams.add(img=new ParamVolume("Image Volume"));
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(img_with_noise=new ParamVolume("Image Volume Corrupted with Noise",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		ImageData IMG = img.getImageData();
		ImageData NOISE = noise.getImageData();
		//ImageDataMath.addFloatImage(IMG, NOISE);
		//Below code is from edu.jhu.ece.iacl.structures.image.ImageDataMath with some changes.
		int rows=IMG.getRows(), cols=IMG.getCols(), slices=IMG.getSlices(),
		components = IMG.getComponents();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						double val = IMG.getDouble(i, j, k, l)+NOISE.getDouble(i, j, k, l);				
						if(val<0){
							val=0;
						}
						IMG.set(i,j,k,l,val);						
					}
				}
			}
		}
		img_with_noise.setValue(IMG);
		img_with_noise.getImageData().setHeader(IMG.getHeader());
	}
}
