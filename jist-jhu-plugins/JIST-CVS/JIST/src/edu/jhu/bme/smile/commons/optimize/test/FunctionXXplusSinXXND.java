package edu.jhu.bme.smile.commons.optimize.test;

import edu.jhu.bme.smile.commons.optimize.FunctionNumericNDDifferentiation;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuousDifferentiable;

/**
 * Test case to test ND optimizers.
 * Function is x1^2 + 4*sin(x1^2) + x2^2 + 4*sin(x2^2) + ... + xn^2 + 4*sin(xn^2).
 * @author Yufeng Guo, Hanlin Wan
 *
 */
public class FunctionXXplusSinXXND implements OptimizableNDContinuousDifferentiable {
	
	private double dMin[], dMax[], extrema[], tol;
	private int numDim;
	
	/**
	 * Initializes the arrays.
	 */
	private void init() {
		dMin = new double[numDim];
		dMax = new double[numDim];
		extrema = new double[numDim];
		tol = 1e-5; 
	}
	
	/**
	 * Default constuctor. Sets the extrema at the origin.
	 * Sets domain to maximum values.
	 * @param dim number of dimensions
	 */
	public FunctionXXplusSinXXND(int dim) {
		numDim = dim;
		init();
		for (int i=0; i<dim; i++) {
			extrema[i] = 0;
			dMin[i] = Double.MIN_VALUE;
			dMax[i] = Double.MAX_VALUE;
		}
	}

	/**
	 * Constructor to specify extrema and domain.
	 * @param dim number of dimensions
	 * @param extrema where the extrema is
	 * @param domMin domain minimum
	 * @param domMax domain maximum
	 */
	public FunctionXXplusSinXXND(int dim, double extrema[], double domMin[], double domMax[]) {
		numDim = dim;
		init();
		for (int i=0; i<dim; i++) {
			this.extrema[i] = extrema[i];
			dMin[i] = domMin[i];
			dMax[i] = domMax[i];
		}		
	}

	/**
	 * Gets the domain maximum.
	 * @return domain max
	 */
	public double[] getDomainMax() {
		return dMax;
	}

	/**
	 * Gets the domain minimum.
	 * @return domain min
	 */
	public double[] getDomainMin() {
		return dMin;
	}

	/**
	 * Gets the tolerance.
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return tol;
	}

	/**
	 * Gets the number of dimensions.
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return numDim;
	}

	/**
	 * Evaluates the function at location x.
	 * @param x location to evaluate function
	 * @return value of the function at x
	 */
	public double getValue(double[] x) {
		double val = 0;
		for (int i=0; i<numDim; i++) { 
			val += ((x[i]-extrema[i]) * (x[i]-extrema[i]) + 4*Math.sin((x[i]-extrema[i])*(x[i]-extrema[i])));
		}
		return val + 10;
	}
	
	/**
	 * Gets the hessian of the function.
	 * Uses the default step size and method
	 * @param x location to calculate hessian
	 * @return hessian at location x
	 */
	public double[][] get2ndDerivative(double[] x) {
		return (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(x);  
	}

	/**
	 * Gets the hessian of the function.
	 * Uses the specified step size and method.
	 * @param x location to calculate hessian
	 * @param step step size
	 * @param method method
	 * @return hessian at location x
	 */
	public double[][] get2ndDerivative(double[] x, double step, int method) {
		return (new FunctionNumericNDDifferentiation(this, step, method)).get2ndDerivative(x);  
	}
	
	/**
	 * Gets the 2nd derivative of the function in a specific direction.
	 * Uses the default step size and method.
	 * @param x location to calculate derivative
	 * @param dim1 dimension to take the first derivative
	 * @param dim2 dimension to take the second derivative
	 * @return 2nd derivative at location x in the specified direction
	 */
	public double get2ndDerivative(double[] x, int dim1, int dim2) {
		return (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(x, dim1, dim2);  
	}

	/**
	 * Gets the gradient of the function.
	 * Uses the default step size and method
	 * @param x location to calculate gradient
	 * @return gradient at location x
	 */
	public double[] getDerivative(double[] x) {
		return (new FunctionNumericNDDifferentiation(this)).getDerivative(x);  
	}
	
	/**
	 * Gets the gradient of the function.
	 * Uses the specified step size and method.
	 * @param x location to calculate gradient
	 * @param step step size
	 * @param method method
	 * @return gradient at location x
	 */
	public double[] getDerivative(double[] x, double step, int method) {
		return (new FunctionNumericNDDifferentiation(this, step, method)).getDerivative(x);  
	}	
}
