package edu.jhu.bme.smile.commons.optimize;

import java.util.ArrayList;

/**
 * This optimizer uses Newton's Method of finding roots to find extrema. The extrema occur where 
 * the first derivative is 0. Newton's method takes the 2nd derivative of the original function 
 * to find the roots of the first derivative, which in turn gives you the extrema. This method 
 * depends heavily on what the derivative looks like. You must start close to the extrema. Otherwise,
 * it might go in the right direction. But for nice functions, it is very fast.
 * @author Hanlin Wan
 */
public class NewtonMethod1D implements Optimizer1DContinuousDifferentiable {

	private double extrema, lower, higher, tol, gamma = 1;
	private String statusMessage;
	private Optimizable1DContinuousDifferentiable myFunction;
	private int maxIterations, iterations;
	private int maxSteps;
	private boolean logOn = false;
	private ArrayList<double[]> logData;
	
	/**
	 * Default constructor.
	 */
	public NewtonMethod1D() {
		maxIterations = 1000;
		extrema = Double.NaN;
		myFunction = null;
	}
	
	/**
	 * Constructor to specify optimizer and maximum iterations
	 * @param maxIter maximum number of iterations
	 */
	public NewtonMethod1D(int maxIter) {
		maxIterations = maxIter;
		extrema = Double.NaN;
		myFunction = null;
	}

	/**
	 * Turns on logging for each iteration of the optimization.
	 * @param turnOn true if logging, false if not
	 * @param maxSteps maximum number of steps to log
	 */
	public void logData(boolean turnOn, int maxSteps) {
		logOn = turnOn;
		this.maxSteps = maxSteps;
		logData = new ArrayList<double[]>();
	}
	
	/**
	 * Gets the logged data
	 * @return array of logged data
	 */
	public ArrayList<double[]> getLog() {
		return logData;
	}
	
	/**
	 * Gets the extrema that was found
	 * @return the extrema array of where the extrema is
	 */
	public double getExtrema() {
		return extrema;
	}

	/**
	 * Gets the number of iterations done.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations;
	}
	
	/**
	 * Sets gamma, the coefficient to multiply the step by. The flatter the function, the larger
	 * gamma could be. It should never exceed 1. The default is 1.
	 * @param g gamma
	 */
	public void setGamma(double g) {
		gamma = g;
	}
	
	/**
	 * Initialize the optimizer with the Differentiable 1D function.
	 * @param function your Optimizable1DContinuousDifferentiable function
	 */
	public void initialize(Optimizable1DContinuousDifferentiable function) {
		myFunction = function; 
		lower = function.getDomainMin();
		higher = function.getDomainMax();
		tol = function.getDomainTolerance();
		extrema = (lower+higher)/2;
	}
	
	/**
	 * Initialize the optimizer with the Differentiable 1D function. You can set your initial point.
	 * @param function your Optimizable1DContinuousDifferentiable function
	 * @param init initial point
	 */
	public void initialize(Optimizable1DContinuousDifferentiable function, double init) {
		myFunction = function; 
		lower = function.getDomainMin();
		higher = function.getDomainMax();
		tol = function.getDomainTolerance();
		extrema = init;
	}
	
	/**
	 * Optimizes the function
	 * @param findMinima true if you want to find the minimum, false if you want to find the maximum
	 * @return true if extrema was found successfully, false otherwise
	 */
	public boolean optimize(boolean findMinima) {
		if (myFunction == null)  {
			statusMessage = "Not initialized.";
			return false;			
		}

		if (!findMinima) myFunction = new MinToMax1DDifferentiable(myFunction);
		
		double dx = higher - lower, x = extrema, denom, num;
		iterations = 0;
		int timesMax = 0;
		while (Math.abs(dx) > tol && iterations < maxIterations) {
			denom = myFunction.get2ndDerivative(x);
			num = myFunction.getDerivative(x);
			dx = num / denom;			
			if (logOn && iterations<maxSteps) logData.add(new double[]{x, num, denom, dx});
			if (Math.abs(dx) < tol)
				break;
			x -= gamma*dx;
			if (x > higher) {
				x = higher;
				timesMax++;
			}
			else if (x < lower) {
				x = lower;
				timesMax++;
			}
			if (timesMax == 2)
				break;
			iterations++;
		}
		extrema = x;
		if (iterations == maxIterations){
			statusMessage = "Maximum iterations reached.";
			return false;
		}
			
		statusMessage = "Convergence reached";
		return true;
	}

	/**
	 * Gets the status message from the optimizing process
	 * @return string of the status message
	 */
	public String statusMessage() {
		return statusMessage ;
	}
}
