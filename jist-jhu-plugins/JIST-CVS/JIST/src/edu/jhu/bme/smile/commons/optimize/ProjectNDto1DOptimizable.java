package edu.jhu.bme.smile.commons.optimize;

/**
 * Projects an ND function to 1D so that LineSearchND can optimize in one dimension at a time.
 * @author Hanlin Wan
 */
public class ProjectNDto1DOptimizable implements Optimizable1DContinuous {

	OptimizableNDContinuous myFunc;
	int currentDim;
	double []currentLocation;
	
	/**
	 * Constructor to create the projected function
	 * @param func function to project
	 */
	public ProjectNDto1DOptimizable(OptimizableNDContinuous func) {
		myFunc = func;
		currentDim=0;
		currentLocation = new double[myFunc.getNumberOfDimensions()];
	}
	
	/**
	 * Gets the domain maximum in the current dimension.
	 * @return domain max
	 */
	public double getDomainMax() {
		return myFunc.getDomainMax()[currentDim];
	}

	/**
	 * Gets the domain minimum in the current dimension.
	 * @return domain min
	 */
	public double getDomainMin() {
		return myFunc.getDomainMin()[currentDim];		
	}

	/**
	 * Gets the tolerance.
	 * @return tolerance
	 */
	public double getDomainTolerance() {
		return myFunc.getDomainTolerance();
	}

	/**
	 * Gets the value of the function evaluated at the current dimension of x
	 * @param x value in the current dimension to evaluate at
	 * @return the value
	 */
	public double getValue(double x) {
		currentLocation[currentDim]=x;
		return myFunc.getValue(currentLocation);
	}
	
	/**
	 * Sets the location of the current dimension 
	 * @param location new location to be set
	 */
	public void setCurrentLocation(double location) {
		currentLocation[currentDim]=location;
	}
	
	/**
	 * Sets the current dimension.
	 * @param curDim current dimension
	 */
	public void setCurrentDimension(int curDim) {
		currentDim = curDim;
	}
	
	/**
	 * Gets the number of dimensions.
	 * @return number of dimensions
	 */
	public int getNumberOfDimensions() {
		return currentLocation.length;
	}
}
