package edu.jhu.bme.smile.demo;


import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;
import edu.jhu.bme.smile.commons.math.*;


/*
 * public class ChebyShevFitting
 * 
 * This plugin performs ChebyShev polynomial fitting on an input data set.
 * 
 * @author Robert Kim
 *
 */
public class ChebyShevFitting extends ProcessingAlgorithm{
/*
 * Input Parameters
 * ParamVolume img - Image volume upon which Chebyshev polynomial fitting will be applied.
 * ParamInteger degX - the number of degrees of freedom in the polynomial for the x (1st) dimension
 * ParamInteger degY - the number of degrees of freedom in the polynomial for the y (2nd) dimension
 */	
	private ParamVolume img;
	private ParamInteger degX;
	private ParamInteger degY;

/*
 * Output Parameters
 * ParamVolume result - Result image volume after Chebyshev polynomial fitting
 */
	private ParamVolume result;

/*
 * CVS
 */
	private static final String rcsid =
		"$Id: ChebyShevFitting.java,v 1.4 2009/10/09 16:28:04 aaron_carass Exp $";
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Chebyshev polynomial fitting on an image volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/**
		 * Plugin Information
		 */
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Chebyshev Fitting");
		inputParams.setName("Chebyshev_Fitting");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Robert Kim","rkim35@jhu.edu","http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/**
		 * Input Initialization
		 */
		inputParams.add(img=new ParamVolume("Image for Chebyshev Fitting"));
		inputParams.add(degX=new ParamInteger("Degrees of Freedom for x"));
		degX.setValue(new Integer(3));
		inputParams.add(degY=new ParamInteger("Degrees of Freedom for y"));
		degY.setValue(new Integer(3));
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(result=new ParamVolume("Image Volume After Chebyshev Fitting",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		ImageData IMG = img.getImageData();
		int DEG_X = degX.getInt();
		int DEG_Y = degY.getInt();
		int rows=IMG.getRows(), cols=IMG.getCols(), slices=IMG.getSlices(),
		components = IMG.getComponents();
		if(slices<1) slices=1;
		if(components<1) components=1;
		ImageData out = new ImageDataFloat(IMG);
		out.setName("After Fitting");
		float[][]tmp = new float[rows][cols];
		float[][]rst = new float[rows][cols];
	
		for(int p=0;p<components;p++){
			for(int q=0;q<slices;q++)
			{
				for(int i=0;i<rows;i++)
				{
					for(int j=0;j<cols;j++)
					{
						tmp[i][j] = IMG.getFloat(i, j, q,p);
					}
				}
				rst = FieldRegularize.regularize2DChebyshev(DEG_X, DEG_Y, tmp);
				for(int i=0;i<rows;i++)
				{
					for(int j=0;j<cols;j++)
					{
						out.set(i,j,q,p,rst[i][j]);		
					}
				}	
			}	
		}				
		result.setValue(out);

		/**Debugging
		//for(int i=0;i<components;i++)
		//{
			for(int j=0;j<slices;j++)
			{
				for(int k=0;k<rows;k++)
				{
					for(int l=0;l>cols;l++)
					{
						tmp[k][l] = IMG.getFloat(k, l, j);						
					}
				}
				rst = FieldRegularize.regularize2DChebyshev(DEG_X, DEG_Y, tmp);
				for(int m=0;m<rows;m++)
				{
					for(int n=0;n<cols;n++)
					{
						out.set(m, n, j, rst[m][n]);	
					}
				}				
			}
		//}
		result.setValue(out);
		*/
	}
}
