package edu.jhu.bme.smile.commons.math;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

public class MatrixMath {

	public static double svdNorm(Matrix x) {
		SingularValueDecomposition svd = new SingularValueDecomposition(x);
		return max(svd.getS());		
	}

	public static Matrix log(Matrix x1) {
		Matrix y = new Matrix(x1.getRowDimension(),x1.getColumnDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++)
				y.set(i,j,Math.log(x1.get(i,j)));					
		return y;	
	}

	public static Matrix diag(Matrix x1) {
		Matrix y = new Matrix(x1.getRowDimension(),x1.getRowDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			y.set(i,i,x1.get(i,0));
		return y;
	}

	public static Matrix add(Matrix x1, double s) {
		Matrix y = new Matrix(x1.getRowDimension(),x1.getColumnDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++)
				y.set(i,j,x1.get(i,j)+s);					
		return y;	
	}

	public static Matrix elementInverse(Matrix x1) {
		Matrix y = new Matrix(x1.getRowDimension(),x1.getColumnDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++)
				y.set(i,j,1/x1.get(i,j));					
		return y;	
	}

	public static double dotProduct(Matrix x1, Matrix x2) {
		double result=0;
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++)
				result+=x1.get(i,j)*x2.get(i,j);					
		return result;	
	}

	public static Matrix elementProduct(Matrix x1, Matrix x2) {
		Matrix y = new Matrix(x1.getRowDimension(),x1.getColumnDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++)
				y.set(i,j,x1.get(i,j)*x2.get(i,j));					
		return y;	
	}

	public static double scalar(Matrix x) {
		return x.get(0,0);		
	}

	public static double sum(Matrix x) {
		double sum =0;
		for(int i=0;i<x.getRowDimension();i++)
			for(int j=0;j<x.getColumnDimension();j++)
				sum+=x.get(i,j);					
		return sum;	
	}

	public static double min(Matrix x) {
		double min = Double.MAX_VALUE;
		for(int i=0;i<x.getRowDimension();i++)
			for(int j=0;j<x.getColumnDimension();j++)
				if(min>x.get(i,j))
					min = x.get(i,j);
		return min;
	}

	public static double max(Matrix x) {
		double max= -Double.MAX_VALUE;
		for(int i=0;i<x.getRowDimension();i++)
			for(int j=0;j<x.getColumnDimension();j++)
				if(max<x.get(i,j))
					max = x.get(i,j);
		return max;
	}

	public static Matrix concatinateRows(Matrix x1, Matrix x2) {
		int r=x1.getRowDimension();
		Matrix y = new Matrix(x1.getRowDimension()+x2.getRowDimension(),x1.getColumnDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++) {
				y.set(i,j,x1.get(i,j));
				y.set(r+i,j, x2.get(i,j));
			}
		return y;
	}
	
	public static Matrix concatinateColumns(Matrix x1, Matrix x2) {
		int c=x1.getColumnDimension();
		Matrix y = new Matrix(x1.getRowDimension(),x1.getColumnDimension()+x2.getColumnDimension());
		for(int i=0;i<x1.getRowDimension();i++)
			for(int j=0;j<x1.getColumnDimension();j++) {
				y.set(i,j,x1.get(i,j));
				y.set(i,c+j, x2.get(i,j));
			}
		return y;
	}

	public static double colSumNorm(Matrix x) {
		double norm=Double.NEGATIVE_INFINITY;
		for(int j=0;j<x.getColumnDimension();j++) {
			double colNorm = 0;
			for(int i=0;i<x.getRowDimension();i++) {
				colNorm+=Math.abs(x.get(i,j));
			}
			if(colNorm>norm)
				norm=colNorm;
		}
		return norm;			
	}

}
