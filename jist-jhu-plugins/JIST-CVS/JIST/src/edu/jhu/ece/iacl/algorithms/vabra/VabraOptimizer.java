package edu.jhu.ece.iacl.algorithms.vabra;

import java.util.ArrayList;
import java.util.List;

import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;
import edu.jhu.bme.smile.commons.optimize.Optimizable1DContinuous;
import edu.jhu.bme.smile.commons.optimize.Optimizer1DContinuous;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities.InterpolationType;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class VabraOptimizer extends AbstractCalculation{

	VabraOptimizer globalPtr;
	VabraSubjectTargetPairs imgSubTarPairs;
	VabraRBF rbf;

	static double lambda = .350;;//jacobian threshold
	int[] localROI;
	double[] localCoarseGradient; // 3 = gradient dimensions
	int[] coarseLocalRegionCenter;
	int[][] fineLocalRegionCenters;
	boolean[] directionsToOptmize;//Directional Constraints
	
	//Determines the way the deformation field is updated. 
	//Mode 0 is the original ABA mode where the fields are updated through summation. 
	//Mode 1 reapply the deformation field with each update and registers using the new deformed image;
	//See https://putter.ece.jhu.edu/IACL:MinProjectLogs_VABRA_DefFieldUpdate for details.
	int defFieldUpdateMode;

	
	public VabraOptimizer(VabraSubjectTargetPairs imgSubTarPairs, AbstractCalculation parent, boolean[] directionsToOptmize, int defFieldUpdateMode) {
		super(parent);
		this.directionsToOptmize = directionsToOptmize;
		this.imgSubTarPairs = imgSubTarPairs;
		this.defFieldUpdateMode = defFieldUpdateMode;

		globalPtr = this;
		localROI = new int[6];
		rbf = new VabraRBF(parent);
		localCoarseGradient = new double[3];
		coarseLocalRegionCenter = new int[3];


	}

	public void dispose() {

		rbf = null;
		globalPtr = null;
		imgSubTarPairs = null;
		localROI = null;
		localCoarseGradient = null;
		coarseLocalRegionCenter = null;

	}

	public class CoarseOptimizer implements Optimizable1DContinuous
	{

		double domainMax,domainMin,tolerance;

		public CoarseOptimizer(double max, double min) {

			domainMax = max;
			domainMin = min;
			tolerance = 5.0e-5;
		}

		@Override
		public double getDomainMax() {
			return domainMax;
		}

		@Override
		public double getDomainMin() {
			return domainMin;
		}

		@Override
		public double getDomainTolerance() {
			return tolerance;
		}

		//@Override
		public double getValue(double c) {
			return globalPtr.coarseCostFunction(c,false);
		}		
	}

	public class FineOptimizer implements Optimizable1DContinuous
	{

		double domainMax,domainMin,tolerance;
		int numOfParam;
		double[] normalizedParams;

		public FineOptimizer(double max, double min, double[] inParams) {
			normalizedParams = inParams;
			numOfParam = normalizedParams.length;
			domainMax = max;
			domainMin = min;
			tolerance = 5.0e-5;
		}

		@Override
		public double getDomainMax() {
			return domainMax;
		}

		@Override
		public double getDomainMin() {
			return domainMin;
		}

		@Override
		public double getDomainTolerance() {
			return tolerance;
		}

		//@Override
		public double getValue(double c) {

			double[] coeffArray = new double[numOfParam];
			for (int j = 0; j < numOfParam; j++) {
				coeffArray[j] = c * normalizedParams[j];
			}
			return globalPtr.fineCostFunction(coeffArray);
		}		
	}


	public void coarseGradient(int[] regionCenter, double[] results) {
		double x, y, z;
		VabraVolumeCollection referenceSubject;
		
		int tx_valRBFoff, ty_valRBFoff, tz_valRBFoff;
		int supportRBF[] = new int[6];

		double nmiOrig;
		double gradx[] = new double[imgSubTarPairs.numOfCh];
		double grady[] = new double[imgSubTarPairs.numOfCh];
		double gradz[] = new double[imgSubTarPairs.numOfCh];

		int targetBins[] = new int[imgSubTarPairs.numOfCh];
		int subjectBins[] = new int[imgSubTarPairs.numOfCh];

		//Step Size
		double defX, defY, defZ;
		double deltaC[] = new double[3];
		deltaC[0] = 0.1;
		deltaC[1] = 0.1;
		deltaC[2] = 0.1;

		int TimeCounter = 0;

		long start, stop;

		//int counter memflag;
		imgSubTarPairs.hist.copyOrigHistograms();

		nmiOrig = 0;
		for (int ch = 0; ch < imgSubTarPairs.numOfCh; ch++)
			nmiOrig += imgSubTarPairs.chWeights[ch]* RegistrationUtilities.NMI(imgSubTarPairs.hist.origDeformedSubject, imgSubTarPairs.hist.origTarget,
					imgSubTarPairs.hist.origJointST, ch, imgSubTarPairs.numOfBins);

		supportRBF[0] = Math.max(imgSubTarPairs.boundingBox[0], regionCenter[0] - rbf.getScale());
		supportRBF[1] = Math.min(imgSubTarPairs.boundingBox[1], regionCenter[0] + rbf.getScale());
		supportRBF[2] = Math.max(imgSubTarPairs.boundingBox[2], regionCenter[1] - rbf.getScale());
		supportRBF[3] = Math.min(imgSubTarPairs.boundingBox[3], regionCenter[1] + rbf.getScale());
		supportRBF[4] = Math.max(imgSubTarPairs.boundingBox[4], regionCenter[2] - rbf.getScale());
		supportRBF[5] = Math.min(imgSubTarPairs.boundingBox[5], regionCenter[2] + rbf.getScale());


		
		

		start = System.currentTimeMillis();
		for (int i = supportRBF[0]; i <= supportRBF[1]; i++) 
			for (int j = supportRBF[2]; j <= supportRBF[3]; j++) 
				for (int k = supportRBF[4]; k <= supportRBF[5]; k++) {
					/* coordinates relative to region center */
					tx_valRBFoff = i - regionCenter[0] + rbf.getOffset();
					ty_valRBFoff = j - regionCenter[1] + rbf.getOffset();
					tz_valRBFoff = k - regionCenter[2] + rbf.getOffset();
					if (rbf.values[tx_valRBFoff][ty_valRBFoff][tz_valRBFoff] != 0) {

						//Calculate rbf at point multiplied by step size
						 
						defX = deltaC[0]*rbf.values[tx_valRBFoff][ty_valRBFoff][tz_valRBFoff];
						defY = deltaC[1]*rbf.values[tx_valRBFoff][ty_valRBFoff][tz_valRBFoff];
						defZ = deltaC[2]*rbf.values[tx_valRBFoff][ty_valRBFoff][tz_valRBFoff];

						//Find values(normalized) of subject and target at the point 
						for (int ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
							targetBins[ch] = imgSubTarPairs.normedTarget.data[ch].getUByte(i, j, k);
							subjectBins[ch] = imgSubTarPairs.normedDeformedSubject.data[ch].getUByte(i,j, k);
						}
						
						//Check which type of deformation update method to use(see "defFieldUPdateMode" variable declaration for details)
						if (defFieldUpdateMode == 0){
							x = i + imgSubTarPairs.totalDeformFieldM[i][j][k][0];
							y = j + imgSubTarPairs.totalDeformFieldM[i][j][k][1];
							z = k + imgSubTarPairs.totalDeformFieldM[i][j][k][2];
							referenceSubject = imgSubTarPairs.subject;	
						}else{
							x = i;
							y = j;
							z = k;
							referenceSubject = imgSubTarPairs.deformedSubject;
						}

						//Adjusts histograms bins in plus/minus x, y and z directions to find NMI gradient
						imgSubTarPairs.adjustHistBinsTo( referenceSubject, x + defX, y, z, imgSubTarPairs.hist.defSxPlus, imgSubTarPairs.hist.defSTxPlus, targetBins, subjectBins);
						imgSubTarPairs.adjustHistBinsTo( referenceSubject, x - defX, y, z, imgSubTarPairs.hist.defSxMinus, imgSubTarPairs.hist.defSTxMinus, targetBins, subjectBins);
						imgSubTarPairs.adjustHistBinsTo( referenceSubject, x, y + defY, z, imgSubTarPairs.hist.defSyPlus, imgSubTarPairs.hist.defSTyPlus, targetBins, subjectBins);
						imgSubTarPairs.adjustHistBinsTo( referenceSubject, x, y - defY, z, imgSubTarPairs.hist.defSyMinus, imgSubTarPairs.hist.defSTyMinus, targetBins, subjectBins);
						imgSubTarPairs.adjustHistBinsTo( referenceSubject, x, y, z + defZ, imgSubTarPairs.hist.defSzPlus, imgSubTarPairs.hist.defSTzPlus, targetBins, subjectBins);
						imgSubTarPairs.adjustHistBinsTo( referenceSubject, x, y, z - defZ, imgSubTarPairs.hist.defSzMinus, imgSubTarPairs.hist.defSTzMinus, targetBins, subjectBins);
					}
				}


		//Find Gradients

		for (int ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
			gradx[ch] = (RegistrationUtilities.NMI(imgSubTarPairs.hist.defSxPlus, imgSubTarPairs.hist.origTarget,imgSubTarPairs.hist.defSTxPlus, ch, imgSubTarPairs.numOfBins)
					- RegistrationUtilities.NMI(imgSubTarPairs.hist.defSxMinus,imgSubTarPairs.hist.origTarget, imgSubTarPairs.hist.defSTxMinus, ch, imgSubTarPairs.numOfBins))
					/ (2.0f * deltaC[0]);
			grady[ch] = (RegistrationUtilities.NMI(imgSubTarPairs.hist.defSyPlus, imgSubTarPairs.hist.origTarget, imgSubTarPairs.hist.defSTyPlus, ch, imgSubTarPairs.numOfBins)
					- RegistrationUtilities.NMI(imgSubTarPairs.hist.defSyMinus, imgSubTarPairs.hist.origTarget, imgSubTarPairs.hist.defSTyMinus, ch, imgSubTarPairs.numOfBins))
					/ (2.0f * deltaC[1]);
			gradz[ch] = (RegistrationUtilities.NMI(imgSubTarPairs.hist.defSzPlus, imgSubTarPairs.hist.origTarget, imgSubTarPairs.hist.defSTzPlus, ch, imgSubTarPairs.numOfBins)
					- RegistrationUtilities.NMI(imgSubTarPairs.hist.defSzMinus, imgSubTarPairs.hist.origTarget, imgSubTarPairs.hist.defSTzMinus, ch, imgSubTarPairs.numOfBins))
					/ (2.0f * deltaC[2]);
		}

		results[0] = 0.0;
		results[1] = 0.0;
		results[2] = 0.0;

		for (int ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
			results[0] += gradx[ch] * imgSubTarPairs.chWeights[ch];
			results[1] += grady[ch] * imgSubTarPairs.chWeights[ch];
			results[2] += gradz[ch] * imgSubTarPairs.chWeights[ch];
		}

		stop = System.currentTimeMillis();
		if ((float) (stop - start) > 1000) {
			System.out.format("Coarse Gradient time:%f, %d\n", (float) (stop - start), TimeCounter);
			System.out.format("%f/%f\n", (float) Runtime.getRuntime().freeMemory(), (float) Runtime.getRuntime().totalMemory());
			TimeCounter = 0;
		} else TimeCounter++;

	}


	void updateFromCoarseOptimization(double coeff) {
		//int channels = tarSubImagePairs.numOfCh
		/*double val;
		float x, y, z;
		int i, j, k, ch;
		int ox_valRBFoff, oy_valRBFoff, oz_valRBFoff;*/
		if (Math.abs(coeff) < 0.00001) return;


		double maxJacob = Math.abs(coeff)* rbf.getMaxValuesChange()
		* (Math.max(Math.abs(localCoarseGradient[0]), 
				Math.max(Math.abs(localCoarseGradient[1]), Math.abs(localCoarseGradient[2]))));
		if (maxJacob > lambda) return;

		coarseCostFunction(coeff, true);

	}



	ImageData[]  totalDeformFieldSplit = new ImageData[3];
//	should return change in cost function due to optimization
	public double coarseOptimize(int[] regionCenter, double[] gradient) {
		int gradParams = imgSubTarPairs.coarseGradientParameters();

		localROI[0] = Math.max(imgSubTarPairs.boundingBox[0], regionCenter[0] - rbf.getScale());
		localROI[1] = Math.min(imgSubTarPairs.boundingBox[1], regionCenter[0] + rbf.getScale());
		localROI[2] = Math.max(imgSubTarPairs.boundingBox[2], regionCenter[1] - rbf.getScale());
		localROI[3] = Math.min(imgSubTarPairs.boundingBox[3], regionCenter[1] + rbf.getScale());
		localROI[4] = Math.max(imgSubTarPairs.boundingBox[4], regionCenter[2] - rbf.getScale());
		localROI[5] = Math.min(imgSubTarPairs.boundingBox[5], regionCenter[2] + rbf.getScale());

		// do I use the old gradient or calculate a new in since things may have
		// changed
		coarseGradient(regionCenter, localCoarseGradient);
		RegistrationUtilities.VectorNormalization(localCoarseGradient, gradParams);

		coarseLocalRegionCenter[0] = regionCenter[0];
		coarseLocalRegionCenter[1] = regionCenter[1];
		coarseLocalRegionCenter[2] = regionCenter[2];

		double originalNMI = -coarseCostFunction(0,false);
		double delta = 0;
		double optimizedCoeff = 0;
		double optimizedNMI = 0;


		if(originalNMI != 2){ //skip if already perfectly registered

			double maxCoeff = lambda/(rbf.getMaxValuesChange() * (Math.max(Math.abs(localCoarseGradient[0]), 
					Math.max(Math.abs(localCoarseGradient[1]), Math.abs(localCoarseGradient[2])))));
			CoarseOptimizer fun = new CoarseOptimizer(maxCoeff,-maxCoeff);
			//CoarseOptimizer fun = new CoarseOptimizer(imgSubTarPairs.maxDimensions,-imgSubTarPairs.maxDimensions);
			Optimizer1DContinuous opt = new BrentMethod1D();
			opt.initialize(fun);		
			opt.optimize(true);
			optimizedCoeff = opt.getExtrema();
			optimizedNMI = coarseCostFunction(optimizedCoeff,false);
			delta = (optimizedNMI) + originalNMI;
		}
		//System.out.format("**************************Coeff: "+optimizedCoeff[0]+"***************\n");
		if (delta < 0 && Math.abs(optimizedCoeff) >= 0.005) {
			//System.out.println(getClass().getCanonicalName()+"\t"+"UPDATE FROM COARSE OPTIMIZATION "+Math.abs(optimizedCoeff[0]));
			//System.out.format("At ("+coarseLocalRegionCenter[0]+","+coarseLocalRegionCenter[1]+","+coarseLocalRegionCenter[2]+") Coeff:"+optimizedCoeff+" NMI:"+ optimizedNMI +"\n");
			updateFromCoarseOptimization(optimizedCoeff);
			// System.out.format("accepted\n");
		} else {
			//System.out.format("SHOULD NOT OPTIMIZE %f<0 && %f>=0.005\n",delta,Math.abs(optimizedCoeff[0]));
			delta = 0;
		}
		return delta;
	}
	


	//public float coarseCostFunction(float[] lambda) {
	public double coarseCostFunction(double coeff, boolean commitUpdate) {
		double rbfVal;
		double coeff_x, coeff_y, coeff_z;
		
		int ox_valRBFoff, oy_valRBFoff, oz_valRBFoff;
		double nmiValD;
		double defX, defY, defZ;
		double tlambda;
		double interpValsD;
		int targetBins;
		int subjectBins;
		int testBin;
		//int i, j, k, ch;
		double x, y, z;
		VabraVolumeCollection referenceSubject;

		
		//temp start
		int XN =  imgSubTarPairs.totalDeformField.getRows();
		int YN =  imgSubTarPairs.totalDeformField.getCols();
		int ZN =  imgSubTarPairs.totalDeformField.getSlices();
		//temp end
		imgSubTarPairs.hist.currentDeformedSubject = ImageData.clone(imgSubTarPairs.hist.origDeformedSubject);
		imgSubTarPairs.hist.currentJointST = ImageData.clone(imgSubTarPairs.hist.origJointST);

		tlambda = (Math.round((double) coeff * 10000.0)) / 10000.0;

		coeff_x = ((double) tlambda) * localCoarseGradient[0];
		coeff_y = ((double) tlambda) * localCoarseGradient[1];
		coeff_z = ((double) tlambda) * localCoarseGradient[2];

		//optimized by putting outside and incrementing.
		for (int i = localROI[0]; i <= localROI[1]; i++) {
			for (int j = localROI[2]; j <= localROI[3]; j++) {
				for (int k = localROI[4]; k <= localROI[5]; k++) {

					//(ox, oy, oz) are coordinates of (i, j) relative to region center				
					ox_valRBFoff = i - coarseLocalRegionCenter[0] + rbf.getOffset();
					oy_valRBFoff = j - coarseLocalRegionCenter[1] + rbf.getOffset();
					oz_valRBFoff = k - coarseLocalRegionCenter[2] + rbf.getOffset();

					rbfVal = rbf.values[ox_valRBFoff][oy_valRBFoff][oz_valRBFoff];

					if (rbfVal != 0) {
						// steepest descent direction: negative of gradient of NMI wrt c.

						//Amount to adjust with RBF if not constrained
						if(directionsToOptmize[0]) defX = coeff_x*rbfVal;
						else defX = 0;
						if(directionsToOptmize[1]) defY = coeff_y*rbfVal;
						else defY = 0;
						if(directionsToOptmize[2]) defZ = coeff_z*rbfVal;
						else defZ = 0;

						
						//Check which type of deformation update method to use(see "defFieldUPdateMode" variable declaration for details)
						if (defFieldUpdateMode == 0){
							x = i + imgSubTarPairs.totalDeformFieldM[i][j][k][0];
							y = j + imgSubTarPairs.totalDeformFieldM[i][j][k][1];
							z = k + imgSubTarPairs.totalDeformFieldM[i][j][k][2];
							referenceSubject = imgSubTarPairs.subject;	
						}else{
							x = i;
							y = j;
							z = k;
							referenceSubject = imgSubTarPairs.deformedSubjectCopy;
						}

						//set as current deformation field if actually updating
						if(commitUpdate){			
							//Check which type of deformation update method to use(see "defFieldUPdateMode" variable declaration for details)
							if (defFieldUpdateMode == 0){
								imgSubTarPairs.totalDeformFieldM[i][j][k][0] += (float)defX;
								imgSubTarPairs.totalDeformFieldM[i][j][k][1] += (float)defY;
								imgSubTarPairs.totalDeformFieldM[i][j][k][2] += (float)defZ;
							}else{
								imgSubTarPairs.currentDeformFieldM[i][j][k][0] = (float)defX;
								imgSubTarPairs.currentDeformFieldM[i][j][k][1] = (float)defY;
								imgSubTarPairs.currentDeformFieldM[i][j][k][2] = (float)defZ;
							}
						}

						for (int ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
							targetBins = imgSubTarPairs.normedTarget.data[ch].getUByte(i, j, k);
							subjectBins = imgSubTarPairs.normedDeformedSubject.data[ch].getUByte(i, j, k);
							
							interpValsD = RegistrationUtilities.Interpolation(referenceSubject.data[ch], XN, YN, ZN, x + defX, y + defY,z + defZ, imgSubTarPairs.chInterpType[ch]);
							testBin = imgSubTarPairs.calculateBin(interpValsD, ch);
							imgSubTarPairs.hist.adjustBins(imgSubTarPairs.hist.currentDeformedSubject, imgSubTarPairs.hist.currentJointST, subjectBins, targetBins,testBin,ch);

							//change images and real histograms if actually updating
							if(commitUpdate){
								imgSubTarPairs.hist.adjustBins(imgSubTarPairs.hist.origDeformedSubject, imgSubTarPairs.hist.origJointST, subjectBins,targetBins,testBin,ch);
								imgSubTarPairs.deformedSubject.data[ch].set(i, j, k, interpValsD);
								imgSubTarPairs.normedDeformedSubject.data[ch].set(i, j, k, testBin);
							}
						}

					}
				}
			}
		}

		//Update copies and final deformation field if actually updating
		if(commitUpdate){
			//Check which type of deformation update method to use(see "defFieldUPdateMode" variable declaration for details)
			if (defFieldUpdateMode == 1){
				for(int ch = 0; ch < imgSubTarPairs.numOfCh; ch++){
					for (int i = localROI[0]; i <= localROI[1]; i++) 
						for (int j = localROI[2]; j <= localROI[3]; j++) 
							for (int k = localROI[4]; k <= localROI[5]; k++) 
								imgSubTarPairs.deformedSubjectCopy.data[ch].set(i, j, k, imgSubTarPairs.deformedSubject.data[ch].getDouble(i, j, k));
				}
	
				imgSubTarPairs.updateDefField(localROI);
			}
				
		}



		nmiValD = 0;

		for (int ch = 0; ch < imgSubTarPairs.numOfCh; ch++){
			nmiValD -= imgSubTarPairs.chWeights[ch] * RegistrationUtilities.NMI(imgSubTarPairs.hist.currentDeformedSubject, imgSubTarPairs.hist.origTarget,
					imgSubTarPairs.hist.currentJointST, ch, imgSubTarPairs.numOfBins);
			//if(commitUpdate)System.out.format("Committed" + nmiValD + "\n");
		}
		//System.out.format("COARSE COST FUNC %f %f\n",lambda[0],nmiVal);
		return nmiValD;
	}

	public VabraRBF getRBF(){
		return rbf;
	}


	void updateFromFineOptimization(double[] coef) {

		//int channels = tarSubImagePairs.numOfCh
		int[] targetBins = new int[imgSubTarPairs.numOfCh];
		int[] subjectBins = new int[imgSubTarPairs.numOfCh];
		double[] interpValsD = new double[imgSubTarPairs.numOfCh];
		float x, y, z;
		int i, j, k, ch, q;
		float dfx, dfy, dfz;
		int newSubjectBin;
		int ox_valRBFoff, oy_valRBFoff, oz_valRBFoff;

		// check topology constraints
		for (k = localROI[4]; k <= localROI[5]; k++) {
			for (j = localROI[2]; j <= localROI[3]; j++) {
				for (i = localROI[0]; i <= localROI[1]; i++) {
					dfx = 0;
					dfy = 0;
					dfz = 0;
					for (q = 0; q < 8; q++) {

						ox_valRBFoff = i - fineLocalRegionCenters[q][0] + rbf.getOffset();
						oy_valRBFoff = j - fineLocalRegionCenters[q][1] + rbf.getOffset();
						oz_valRBFoff = k - fineLocalRegionCenters[q][2] + rbf.getOffset();

						dfx += coef[q * 3 + 0]* rbf.values[ox_valRBFoff][oy_valRBFoff][oz_valRBFoff];
						dfy += coef[q * 3 + 1]* rbf.values[ox_valRBFoff][oy_valRBFoff][oz_valRBFoff];
						dfz += coef[q * 3 + 2]* rbf.values[ox_valRBFoff][oy_valRBFoff][oz_valRBFoff];
					}

					//Adjust Deformation Fields if not constrained
					if(directionsToOptmize[0]) imgSubTarPairs.currentDeformFieldM[i][j][k][0] += dfx;
					if(directionsToOptmize[1]) imgSubTarPairs.currentDeformFieldM[i][j][k][1] += dfy;
					if(directionsToOptmize[2]) imgSubTarPairs.currentDeformFieldM[i][j][k][2] += dfz;
				}
			}
		}

		for (k = localROI[4]; k <= localROI[5]; k++)
			for (j = localROI[2]; j <= localROI[3]; j++)
				for (i = localROI[0]; i <= localROI[1]; i++)

				{

					// calculate point deformation in 3D
					x = i + imgSubTarPairs.currentDeformFieldM[i][j][k][0];
					y = j + imgSubTarPairs.currentDeformFieldM[i][j][k][1];
					z = k + imgSubTarPairs.currentDeformFieldM[i][j][k][2];

					// we interpolate from the original subject image
					imgSubTarPairs.deformedSubject.interpolate(x, y, z, interpValsD);
					for (ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
						targetBins[ch] = imgSubTarPairs.normedTarget.data[ch].getUByte(i, j, k);
						subjectBins[ch] = imgSubTarPairs.normedDeformedSubject.data[ch].getUByte(i, j, k);

						newSubjectBin = imgSubTarPairs.calculateBin(interpValsD[ch],ch);

						imgSubTarPairs.deformedSubject.data[ch].set(i, j, k, interpValsD[ch]);
						imgSubTarPairs.normedDeformedSubject.data[ch].set(i, j, k, newSubjectBin);

						imgSubTarPairs.hist.adjustBins(imgSubTarPairs.hist.origDeformedSubject, imgSubTarPairs.hist.origJointST, subjectBins[ch],targetBins[ch],newSubjectBin,ch);

					}

				}

	}

	public void fineGradient(int[][] regionCenters, int n, double[] results) {
		int i;
		double grad[] = new double[3];
		for (i = 0; i < n; i++) {
			localROI[0] = Math.max(imgSubTarPairs.boundingBox[0], regionCenters[i][0] - rbf.getScale());
			localROI[1] = Math.min(imgSubTarPairs.boundingBox[1], regionCenters[i][0] + rbf.getScale());
			localROI[2] = Math.max(imgSubTarPairs.boundingBox[2], regionCenters[i][1] - rbf.getScale());
			localROI[3] = Math.min(imgSubTarPairs.boundingBox[3], regionCenters[i][1] + rbf.getScale());
			localROI[4] = Math.max(imgSubTarPairs.boundingBox[4], regionCenters[i][2] - rbf.getScale());
			localROI[5] = Math.min(imgSubTarPairs.boundingBox[5], regionCenters[i][2] + rbf.getScale());

			coarseGradient(regionCenters[i], grad);
			results[3 * i] = grad[0];
			results[3 * i + 1] = grad[1];
			results[3 * i + 2] = grad[2];
			// System.out.format("fine grad (%d %d %d) %f %f
			// %f\n",regionCenters[i][0],regionCenters[i][1],regionCenters[i][2],(float)grad[0],(float)grad[1],(float)grad[2]);
		}
	}

	public void fineOptimize(int[][] regionCenters, double[] results) {
		//Changed into buffers to lower memory usage
		int gradParams = 24;
		int i,j;
		double[] grad = new double[gradParams];
		//float[] gradf = new float[gradParams];
		double[] Ci = new double[gradParams];
		float val2;
		int[] regionCentersMax = new int[3];
		int[] regionCentersMin = new int[3];


		fineLocalRegionCenters = regionCenters;
		fineGradient(regionCenters, gradParams / 3, grad);
		RegistrationUtilities.VectorNormalization(grad, gradParams);

		for (j = 0; j < 3; j++) {
			regionCentersMax[j] = regionCenters[0][j];
			regionCentersMin[j] = regionCenters[0][j];
			for (i = 1; i < 8; i++) {
				regionCentersMax[j] = (regionCentersMax[j] > regionCenters[i][j] ? regionCentersMax[j] : regionCenters[i][j]);
				regionCentersMin[j] = (regionCentersMin[j] < regionCenters[i][j] ? regionCentersMin[j] : regionCenters[i][j]);
			}
		}

		localROI[0] = Math.max(imgSubTarPairs.boundingBox[0], Math.min(imgSubTarPairs.boundingBox[1], regionCentersMin[0] - rbf.getScale()));
		localROI[1] = Math.min(imgSubTarPairs.boundingBox[1], Math.max(imgSubTarPairs.boundingBox[0], regionCentersMax[0] + rbf.getScale()));
		localROI[2] = Math.max(imgSubTarPairs.boundingBox[2], Math.min(imgSubTarPairs.boundingBox[3], regionCentersMin[1] - rbf.getScale()));
		localROI[3] = Math.min(imgSubTarPairs.boundingBox[3], Math.max(imgSubTarPairs.boundingBox[2], regionCentersMax[1] + rbf.getScale()));
		localROI[4] = Math.max(imgSubTarPairs.boundingBox[4], Math.min(imgSubTarPairs.boundingBox[5], regionCentersMin[2] - rbf.getScale()));
		localROI[5] = Math.min(imgSubTarPairs.boundingBox[5], Math.max(imgSubTarPairs.boundingBox[4], regionCentersMax[2] + rbf.getScale()));

		val2 = fineCostFunction(Ci);

		FineOptimizer fun = new FineOptimizer(imgSubTarPairs.maxDimensions,-imgSubTarPairs.maxDimensions, grad);
		Optimizer1DContinuous opt = new BrentMethod1D();
		opt.initialize(fun);		
		opt.optimize(true);

		double minCoeff = opt.getExtrema();
		double[] optimizedCoeffs = new double[grad.length];

		for (int w = 0; w < grad.length; w++ ) optimizedCoeffs[w] = grad[w]*minCoeff; 


		// System.out.format("old:%f new:%f\n",val2,fnew);
		float delta = fineCostFunction(optimizedCoeffs) - val2;

		//System.out.format("**************************Coeff: "+Ci[0]+"***************\n");

		if (delta < 0) {
			//System.out.format("UPDATE FROM FINE OPTIMIZE %f\n",Ci[0]);
			updateFromFineOptimization(optimizedCoeffs);
		}

		//for (i = 0; i < gradParams; i++)
		//	Ci[i] = 0.0f;
		//val2 = fineCostFunction(Ci);
		//System.out.format("%f %f\n",val1,val2);
	}

	float fineCostFunction(double[] coeffs) {
		float nmiVal;
		int testBin;
		int i, j, k, ch, q;
		//int ox, oy, oz;
		double x, y, z;
		int ox_valRBFoff, oy_valRBFoff, oz_valRBFoff;
		double rbfVal;
//		int channels = tarSubImagePairs.numOfCh
		double[] testValsD = new double[imgSubTarPairs.numOfCh];
		int[] targetBins = new int[imgSubTarPairs.numOfCh];
		int[] subjectBins = new int[imgSubTarPairs.numOfCh];

		for (ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
			RegistrationUtilities.Histogram3D(imgSubTarPairs.normedDeformedSubject.data, ch, imgSubTarPairs.numOfBins,localROI, imgSubTarPairs.hist.currentDeformedSubject);
			RegistrationUtilities.Histogram3D(imgSubTarPairs.normedTarget.data, ch, imgSubTarPairs.numOfBins, localROI, imgSubTarPairs.hist.currentTarget);
			RegistrationUtilities.JointHistogram3D(imgSubTarPairs.normedDeformedSubject.data, imgSubTarPairs.normedTarget.data, ch, imgSubTarPairs.numOfBins, localROI, imgSubTarPairs.hist.currentJointST);
		}

		int maxBin = imgSubTarPairs.hist.currentDeformedSubject[0].length - 1;
		for (i = localROI[0]; i <= localROI[1]; i++) {
			for (j = localROI[2]; j <= localROI[3]; j++) {
				for (k = localROI[4]; k <= localROI[5]; k++) {
					/*
					 * (ox, oy, oz) are coordinates of (i, j,k) relative to
					 * region center
					 */

					x = (double) i;
					y = (double) j; 
					z = (double) k;

					for (q = 0; q < 8; q++) {
						ox_valRBFoff = i - fineLocalRegionCenters[q][0] + rbf.getOffset();
						oy_valRBFoff = j - fineLocalRegionCenters[q][1] + rbf.getOffset();
						oz_valRBFoff = k - fineLocalRegionCenters[q][2] + rbf.getOffset();
						rbfVal = rbf.values[ox_valRBFoff][oy_valRBFoff][oz_valRBFoff];

						//Adjust with RBF if not constrained
						if(directionsToOptmize[0]) x += coeffs[q * 3 + 0] * rbfVal;
						if(directionsToOptmize[1]) y += coeffs[q * 3 + 1] * rbfVal;
						if(directionsToOptmize[2]) z += coeffs[q * 3 + 2] * rbfVal;

					}


					imgSubTarPairs.deformedSubject.interpolate(x, y, z, testValsD);

					for (ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
						targetBins[ch] = (int) Math.max(0, Math.min(maxBin,imgSubTarPairs.normedTarget.data[ch].getUByte(i, j, k)));
						subjectBins[ch] = (int) Math.max(0, Math.min(maxBin,imgSubTarPairs.normedDeformedSubject.data[ch].getUByte(i, j, k)));
					}
					for (ch = 0; ch < imgSubTarPairs.numOfCh; ch++) {
						testBin = imgSubTarPairs.calculateBin(testValsD[ch], ch);
						imgSubTarPairs.hist.adjustBins(imgSubTarPairs.hist.currentDeformedSubject, imgSubTarPairs.hist.currentJointST, subjectBins[ch],targetBins[ch],testBin,ch);
					}

				}
			}
		}
		nmiVal = 0;
		for (ch = 0; ch < imgSubTarPairs.numOfCh; ch++)
			nmiVal -= imgSubTarPairs.chWeights[ch]
			                                   * RegistrationUtilities.NMI(imgSubTarPairs.hist.currentDeformedSubject, imgSubTarPairs.hist.currentTarget,
			                                		   imgSubTarPairs.hist.currentJointST, ch, imgSubTarPairs.numOfBins);
		return nmiVal;

	}
	
}
