package edu.jhu.ece.iacl.algorithms.hardi;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import gov.nih.mipav.view.renderer.J3D.model.structures.ModelTriangleMesh;

import java.util.Arrays;
import java.util.Vector;

import javax.vecmath.Point3f;

public class SurfaceTools {

	public static ModelTriangleMesh triangulate(Point3f []pts, int [][]connect) {
		Point3f []pts2=new Point3f[pts.length+connect.length];
		for(int i=0;i<pts.length;i++)
			pts2[i]=pts[i];
		int voffset = pts.length;
		int coffset = 0;
		int []connect2=new int[(connect.length*connect[0].length)*3];
	
		for(int j=0;j<connect.length;j++) {
			Point3f pt = new Point3f(0,0,0);
			for(int k=0;k<connect[j].length;k++) { // for each node
				//				System.out.println(getClass().getCanonicalName()+"\t"+j+" "+k);
				pt.add(pts[connect[j][k]]);
				int next = k-1;
				if(next<0)
					next = connect[j].length-1;
				connect2[coffset] = connect[j][k];
				connect2[coffset+1] = connect[j][next];
				connect2[coffset+2] = voffset;
				coffset+=3;
			}
			pt.scale(1.f/connect[j].length);
			pts2[voffset]=pt;
			voffset++;				
		}
	
		return new ModelTriangleMesh (pts2,connect2); 
	}

	//	%% TETRAHEDRON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	public static ModelTriangleMesh tetrahedron() {
		Point3f[] pts = new Point3f[4];
	
	
		float phiaa  = -19.471220333f; // the phi angle needed for generation *
	
	
		float phia = (float)Math.PI*phiaa/180.0f;  // 1 set of three points *
		float the120 = (float)Math.PI*120.0f/180.0f;
		pts[0] = new Point3f(0,0,1);//	vertices(1,:) =  [0 0 r];
		float the = 0.0f;
		for(int i=1;i<=3;i++) {
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phia)),(float)(Math.sin(the)*Math.cos(phia)),(float)Math.sin(phia));
			the = the+the120;
		}
		//	%* map vertices to 4 faces *
		int []connect = {0, 1 ,2,0, 2, 3,0, 3, 1,1, 2, 3};
		return new ModelTriangleMesh(pts,connect);
	
	}

	public static ModelTriangleMesh tesselate(ModelTriangleMesh input, int order) {
			if(order<2)	//nothing to do
				return input;
			Vector<Point3f> pts = new Vector<Point3f>();
			Point3f[] pp=input.getVertexCopy();
			for(int i=0;i<pp.length;i++)
				pts.add(i,pp[i]);
			Vector<Integer> connect = new Vector<Integer>();
			int []idx = input.getIndexCopy();
			for(int i=0;i<idx.length;i++) {
	//			System.out.println(getClass().getCanonicalName()+"\t"+idx[i]);
				connect.add(i, new Integer(idx[i]));
			}
				
	
			int lastTriangle = connect.size();
			for(int i=0;i<lastTriangle;i+=3) {
				tesselateTriangle(pts,connect,i, order);
			}
			//		Vector<Integer> connect = new Vector<>();
			//		int []cnt = input.getIndexCopy();
			//		input.getIndex
	
			int[] cnt = new int[connect.size()]; // and assigned as req'd
			int i = 0;
			for(Integer val : connect) cnt[i++] = (val).intValue();
	
			Point3f[]ptsa = new Point3f[pts.size()];
			for(i=0;i<pts.size();i++)
				ptsa[i]=pts.get(i);
			return new ModelTriangleMesh(ptsa,cnt);
		}

	public static void tesselateTriangle(Vector<Point3f> pts, Vector<Integer>connect, int tri, int order) {
			Point3f a,b,c;
			
			int ia=connect.get(tri).intValue();a=pts.get(ia);
			int ib=connect.get(tri+1).intValue();b=pts.get(ib);
			int ic=connect.get(tri+2).intValue();c=pts.get(ic);
			int npts = pts.size();
	
			if(order==2) {
	
	
				Point3f Vab = new Point3f((a.x+b.x)/2,(a.y+b.y)/2,(a.z+b.z)/2);
				Point3f Vbc = new Point3f((c.x+b.x)/2,(c.y+b.y)/2,(c.z+b.z)/2);
				Point3f Vac = new Point3f((a.x+c.x)/2,(a.y+c.y)/2,(a.z+c.z)/2);
	//			System.out.println(getClass().getCanonicalName()+"\t"+ia+","+ib+","+ic);
	//			System.out.println(getClass().getCanonicalName()+"\t"+tri+":"+Vab+" "+Vbc+" "+Vac);
	
	
				pts.add(Vab); int iVab = npts; npts++;
				pts.add(Vbc); int iVbc = npts; npts++;
				pts.add(Vac); int iVac = npts; npts++;
				// Replace the current triangle
				connect.setElementAt(ia,tri);connect.setElementAt(iVab,tri+1);connect.setElementAt(iVac,tri+2);			//outFace{end+1} = [face(1) iVab iVac];
				// Add 3 more triangles
				connect.add(ib);connect.add(iVab);connect.add(iVbc);			//outFace{end+1} = [face(2) iVab iVbc];
				connect.add(ic);connect.add(iVac);connect.add(iVbc);			//outFace{end+1} = [face(3) iVac iVbc];
	
				connect.add(iVab);connect.add(iVbc);connect.add(iVac);			//outFace{end+1} = [iVab iVbc iVac];
			} else	{
	
				// Step 1: Prune off 1/order-th of a strip for new triangles and recurse
				Point3f Vab_n = new Point3f(0,0,0);
				Vab_n.x = 1.f/(float)order*a.x+(float)(order-1)/(float)order*b.x;
				Vab_n.y = 1.f/(float)order*a.y+(float)(order-1)/(float)order*b.y;
				Vab_n.z = 1.f/(float)order*a.z+(float)(order-1)/(float)order*b.z;
				//			Vab_n = 1/order*vert(face(1),:)+(order-1)/order*vert(face(2),:);
				pts.add(Vab_n); int iVab_n = npts; npts++;
				//			[vert, iVab_n] = addVertex(vert,Vab_n);
				Point3f Vac_n = new Point3f(0,0,0);
				Vac_n.x = 1.f/(float)order*a.x+(float)(order-1)/(float)order*c.x;
				Vac_n.y = 1.f/(float)order*a.y+(float)(order-1)/(float)order*c.y;
				Vac_n.z = 1.f/(float)order*a.z+(float)(order-1)/(float)order*c.z;
				//			Vac_n = 1/order*vert(face(1),:)+(order-1)/order*vert(face(3),:);
				pts.add(Vac_n); int iVac_n = npts; npts++;
				//			[vert, iVac_n] = addVertex(vert,Vac_n);
				// Replace the current triangle with a smaller one
				connect.setElementAt(ia,tri);connect.setElementAt(iVab_n,tri+1);connect.setElementAt(iVac_n,tri+2);		
				//			% Tesselate the sub problem
				tesselateTriangle(pts, connect, tri, order-1);
				npts = pts.size();
				//			[vert,outFace] = tesselateTri(vert,outFace, [face(1) iVab_n iVac_n], order-1);
	
				//			% Add the triangle at the top of the ladder
				Point3f dBC = new Point3f(0,0,0);
				dBC.x = (c.x-b.x)/(float)order; 
				dBC.y = (c.y-b.y)/(float)order;
				dBC.z = (c.z-b.z)/(float)order;
				//			dBC = (vert(face(3),:)-vert(face(2),:))/order;
				Point3f Vbc_n = ((Point3f)b.clone()); Vbc_n.add(dBC); //vert(face(2),:)+dBC;
				pts.add(Vbc_n); int iVbc_n = npts; npts++;
				//			[vert, iVbc_n] = addVertex(vert,Vbc_n);
				connect.add(ib);connect.add(iVab_n);connect.add(iVbc_n);			//			outFace{end+1} = [face(2) iVab_n iVbc_n];
	
				Point3f Vbc0_n = (Point3f)Vab_n.clone();
				int iVbc0_n = iVab_n;
	
				//			% Walk down the ladder adding two triangles at once
				for(int j=1;j<=(order-1);j++) {
					Point3f Vbc_n2 = (Point3f)Vbc_n.clone(); Vbc_n2.add(dBC);
					pts.add(Vbc_n2); int iVbc_n2 = npts; npts++; //				[vert, iVbc_n2] = addVertex(vert,Vbc_n2);
					Point3f Vbc0_n2 = (Point3f)Vbc0_n.clone(); Vbc0_n2.add(dBC);//				Vbc0_n2 = Vbc0_n+dBC;
					pts.add(Vbc0_n2); int iVbc0_n2 = npts; npts++; //				[vert, iVbc0_n2] = addVertex(vert,Vbc0_n2);
					
					connect.add(iVbc_n);connect.add(iVbc_n2);connect.add(iVbc0_n2);			//			outFace{end+1} = [iVbc_n iVbc_n2 iVbc0_n2];
					connect.add(iVbc_n);connect.add(iVbc0_n);connect.add(iVbc0_n2);			// 			outFace{end+1} = [iVbc_n iVbc0_n iVbc0_n2];
	
					iVbc_n = iVbc_n2; Vbc_n=Vbc_n2;
					iVbc0_n = iVbc0_n2; Vbc0_n = Vbc0_n2;
				}
			}
	
		}

	//	%% CUBE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	public static ModelTriangleMesh cube() {
	
		double phiaa = 35.264391; //% * the phi needed for generation *
	
	
		double phia = Math.PI*phiaa/180.0; //%/ 2 sets of four points *
		double phib = -phia;
		double the90 = Math.PI*90.0/180.0;
		double the = 0.0;
		Point3f[] pts = new Point3f[8];
	
		for(int i=0;i<=3;i++) {
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phia)),
					(float)(Math.sin(the)*Math.cos(phia)),(float)Math.sin(phia));;
					the = the+the90;
		}
		the=0.0;
		for(int i=4;i<=7;i++) {
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phib)),
					(float)(Math.sin(the)*Math.cos(phib)),(float)Math.sin(phib));;
					//	    vertices(i+1,:)=[r*cos(the)*cos(phib) r*sin(the)*cos(phib) r*sin(phib)];
					the = the+the90;
		}
	
		//	% / map vertices to 6 faces *
		int [][]connect = {{0,1,2,3},{4,5,6,7},{0,1,5,4},{1,2,6,5},{2,3,7,6},{3,0,4,7}};
	
		return triangulate(pts,connect);
	}

	//	%% DODECAHEDRON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	public static ModelTriangleMesh dodecahedron() {
	
		double phiaa = 52.62263590; // the two phi angles needed for generation *
		double phibb = 10.81231754;
		Point3f[] pts = new Point3f[20];
	
	
		double phia = Math.PI*phiaa/180.0; // 4 sets of five points each *
		double phib = Math.PI*phibb/180.0;
		double phic = Math.PI*(-phibb)/180.0;
		double phid = Math.PI*(-phiaa)/180.0;
		double the72 = Math.PI*72.0/180;
		double theb = the72/2.0; // pairs of layers offset 36 degrees *
		double the = 0.0;
		for(int i=0;i<=4;i++) { 
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phia)), (float)(Math.sin(the)*Math.cos(phia)), (float)Math.sin(phia));
			the = the+the72;
		}
		the=0.0;
		for(int i=5;i<=9;i++) {
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phib)),(float)(Math.sin(the)*Math.cos(phib)), (float)Math.sin(phib));
			the = the+the72;
		}
		the = theb;
		for(int i=10;i<=14;i++) { 
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phic)),(float)(Math.sin(the)*Math.cos(phic)),(float)Math.sin(phic));
			the = the+the72;
		}
		the=theb;
		for(int i=15;i<=19;i++) {
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phid)),(float)(Math.sin(the)*Math.cos(phid)),(float)Math.sin(phid));
			the = the+the72;
		}
	
		//	  %/ map vertices to 12 faces *
		int [][]connect = {{0,1,2,3,4},{0,1,6,10,5},{1,2,7,11,6},{2,3,8,12,7},{3,4,9,13,8},
				{4,0,5,14,9},{15,16,11,6,10},{16,17,12,7,11},{17,18,13,8,12},{18,19,14,9,13},
				{19,15,10,5,14},{15,16,17,18,19}};
		return triangulate(pts,connect);
	}

	public static ModelTriangleMesh normalizeToSphere(ModelTriangleMesh input, float r) {
		Vector<Point3f> pts = new Vector<Point3f>(Arrays.asList(input.getVertexCopy()));
		
		for(Point3f pt : pts) {
			pt.scale((float)(r/Math.sqrt(pt.x*pt.x+pt.y*pt.y+pt.z*pt.z)));			
		}			
		Point3f[]ptsa = new Point3f[pts.size()];
		for(int i=0;i<pts.size();i++)
			ptsa[i]=pts.get(i);
		return new ModelTriangleMesh(ptsa,input.getIndexCopy());
	}

	//	%% OCTAHEDRON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	public static ModelTriangleMesh octahedron() {
	
		double phiaa  = 0.0; //% / the phi needed for generation *
	
		double phia = Math.PI*phiaa/180.0; //% /* 1 set of four points *
		double the90 = Math.PI*90.0/180;	  
		double the = 0.0;
	
		Point3f[] pts = new Point3f[6];
		pts[0] = new Point3f(0,0,1);
		pts[5] = new Point3f(0,0,-1);
		//	  vertices(1,:) = [0 0 1];
		//	  vertices(6,:) = [0 0 -1];
		for(int i=1;i<=4;i++) {
			pts[i] = new Point3f((float)(Math.cos(the)*Math.cos(phia)), (float)(Math.sin(the)*Math.cos(phia)), (float)Math.sin(phia));	    
			the = the+the90;
		}
	
		int []connect = {0,1,2,0,2,3,0,3,4,0,4,1,5,1,2,5,2,3,5,3,4,5,4,1};
		return new ModelTriangleMesh(pts,connect);
	}

	public static EmbeddedSurface selectUniqueDirectionsAndReflections(
			EmbeddedSurface surface, double tolerance) {
		Point3f[] pts = surface.getVertexCopy();
		Vector<Point3f> v = new Vector<Point3f>();
		for(int i=0;i<pts.length;i++) {
			float norm = (float)Math.sqrt(
					pts[i].x*pts[i].x+
					pts[i].y*pts[i].y+
					pts[i].z*pts[i].z
					);
			pts[i].x/=norm;
			pts[i].y/=norm;
			pts[i].z/=norm;
		}
		for(int i=0;i<pts.length;i++) {
			float dot = 0;
			for(int j=i+1;j<pts.length;j++) {
				float dotproduct = (float)Math.abs(
						pts[i].x*pts[j].x+
						pts[i].y*pts[j].y+
						pts[i].z*pts[j].z
						);
				dot = (dotproduct>dot)?dotproduct:dot;		
			}			
			if(dot<1-tolerance) {
				v.add(pts[i]);
			}
		}
		Point3f []pts2 = new Point3f[v.size()];
		
		int i=0;	
//		quickhull3d.Point3d []ptd=new quickhull3d.Point3d[pts2.length];
		for(Point3f p : v) {
			pts2[i]=p;
//			ptd[i]=new quickhull3d.Point3d(p.x,p.y,p.z);
			i++;
		}
//		qhull.build(ptd);
//		qhull.triangulate(); 		
//		int [][]faces=qhull.getFaces(qhull.POINT_RELATIVE);
//		int [] connect = new int[3*faces.length];
//		for( i=0;i<faces.length;i++)
//			for(int j=0;j<3;j++) {
//				connect[3*i+j]=faces[i][j];				
//			}
		return quickHull3D(pts2); 
	}
	
	public static EmbeddedSurface quickHull3D(Point3f []pts) {
		quickhull3d.Point3d []ptd=new quickhull3d.Point3d[pts.length];
		int i=0;
		for(Point3f p : pts) {			
			ptd[i]=new quickhull3d.Point3d(p.x,p.y,p.z);
			i++;
		}
		quickhull3d.QuickHull3D qhull = new quickhull3d.QuickHull3D(); 
		qhull.build(ptd);
		qhull.triangulate(); 		
		int [][]faces=qhull.getFaces(qhull.POINT_RELATIVE);
		int [] connect = new int[3*faces.length];
		for( i=0;i<faces.length;i++)
			for(int j=0;j<3;j++) {
				connect[3*i+j]=faces[i][j];				
			}
		return new EmbeddedSurface(pts,connect);
	}

}
