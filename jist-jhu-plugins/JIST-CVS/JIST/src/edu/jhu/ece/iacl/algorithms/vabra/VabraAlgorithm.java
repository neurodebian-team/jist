package edu.jhu.ece.iacl.algorithms.vabra;

import java.io.File;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class VabraAlgorithm extends AbstractCalculation {
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.6 $");
	}

	protected ImageDataFloat deformationField;
	protected List<ImageData> registeredResults;
	
	public VabraAlgorithm() {

	}

	public List<ImageData> solve(List<ImageData> subjectVols, List<Number> subjectWeights, List<ImageData> targetVols,
			List<Number> targetWeights, File config) {

		int[] InterpType = new int[subjectVols.size()]; 
		boolean[] directionsToOptmize= {true, true, true};
		return solve(subjectVols, targetVols, config, 0.000f, 0.000f, VabraHistograms.defaultBins, InterpType, false, null, false, 
				directionsToOptmize, 0);
	}

	public List<ImageData> solve(List<ImageData> subjectVols, List<ImageData> targetVols, File config, float robustMaxT,
			float robustMinT, int numBins, int[] InterpType, boolean fineOptimize, File outputDir, boolean saveIntermResults,
			boolean[] directionsToOptmize, int defFieldUpdateMode) {
		setTotalUnits(2);

		setLabel("VABRA - Deformable Registration");
		
		System.out.println(getClass().getCanonicalName()+"\t"+"VABRA-ALG: Before RBFPair");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

		
		//1.)Construct Target and Subject Pairs 
		VabraSubjectTargetPairs imgSubTarPairs = new VabraSubjectTargetPairs(subjectVols, targetVols, this, 
				robustMaxT, robustMinT, numBins, InterpType);
		incrementCompletedUnits();
		
		//2.)Construct Vabra Solver
		VabraSolver solver = new VabraSolver(imgSubTarPairs, config, fineOptimize, this, outputDir, saveIntermResults, directionsToOptmize, defFieldUpdateMode);

		System.out.println(getClass().getCanonicalName()+"\t"+"VABRA-ALG: Before Register");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

		//3.)Register Images
		solver.registerImages();
		incrementCompletedUnits();
		
		//4.)Set outputs
		registeredResults = solver.getDeformedSubject();
		deformationField = solver.getDeformationField();
		
		System.out.println(getClass().getCanonicalName()+"\t"+"VABRA-ALG: Before Cleanup");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

		solver.dispose();
		System.gc();

		markCompleted();
		return registeredResults;
	}
	
	public ImageDataFloat getDeformationField() {
		return deformationField;
	}
	
	public List<ImageData> getRegisteredResults() {
		return registeredResults;
	}
}
