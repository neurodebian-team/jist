package edu.jhu.ece.iacl.plugins;

public class JistPluginUtil {
	public static String parseCVSRevision(String cvsversion){
		return cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	}
}
