package edu.jhu.ece.iacl.plugins.dti;

import java.io.IOException;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;
import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.algorithms.registration.AlgorithmRegOAR3D;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;


public class ComputeMeanDW extends ProcessingAlgorithm {
	//output params
	private ParamVolume meanDW;

	//input params
	private ParamVolumeCollection inputFiles;		// Slabs
	private ParamFile bvaluesTable;		// .b file with a list of b-values
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions
	private ParamBoolean register;		// register the inputs before averaging?


	// Controls the threaded nature of the OAR Registration.
	private ParamBoolean OARThreadedBool;


	//Variables

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Averages all diffusion weighted (DW, not b0). Registration (12 dof) to the first b0 is optional.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Compute Mean DW");
		inputParams.setName("Compute_Mean_DW");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		// Input Parameters
		inputParams.add(inputFiles= new ParamVolumeCollection("Input Slab Collection"));
		inputFiles.setLoadAndSaveOnValidate(false);
		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(register = new ParamBoolean("Register to first b0?",false));

		OARThreadedBool = new ParamBoolean("Multithreading for Registration", false);
		OARThreadedBool.setDescription("Set to false by default, this parameter controls the multithreaded behavior of the linear registration.");
		inputParams.add(OARThreadedBool);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(meanDW = new ParamVolume("Mean DW volume"));		
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute() {

			float [][]bs=null;		
			TextFileReader text = new TextFileReader(bvaluesTable.getValue());
			try {
				bs = text.parseFloatFile();
			} catch (IOException e) 
			{
				throw new RuntimeException("Unable to parse b-file");
			}

			float [][]grads=null;
			text = new TextFileReader(gradsTable.getValue());
			try {
				grads  = text.parseFloatFile();
			} catch (IOException e) { 

				throw new RuntimeException("Unable to parse grad-file");
			}

			// If there are 4 columns in the gradient table, remove the 1st column (indecies)
			if(grads[0].length==4) {
				float [][]g2 = new float[grads.length][3];
				for(int i=0;i<grads.length;i++) 
					for(int j=0;j<3;j++)
						g2[i][j]=grads[i][j+1];
				grads=g2;
			}

			if(grads[0].length!=3)
				throw new RuntimeException("ComputeMeanDW: Invalid gradient table. Must have 3 or 4 columns.");
			if(bs[0].length!=1)
				throw new RuntimeException("ComputeMeanDW: Invalid b-value table. Must have 1 column.");
			float []bval = new float[bs.length];
			for(int i=0;i<bval.length;i++)
				bval[i]=bs[i][0];

			this.setTotalUnits(bval.length);

			String baseName = null;
			ImageData mean = null;
			float count =0;
			int firstb0index = -1;
			//		File dir = new File("/home/john/Desktop/regresults");

			ImageData firstb0=null;
			ModelImage tarImg =null;
			if(register.getValue()) {
				for(int i=0;i<inputFiles.size();i++){
					if(bval[i]==0 && !(grads[i][0]==100 && grads[i][1]==100 && grads[i][2]==100)) {
						count ++;
						firstb0 = inputFiles.getParamVolume(i).getImageData();
						break;
					}
				}
tarImg = firstb0.getModelImageCopy();
			} else {
				
			}

			
			ImageData vol=null;
			for(int i=0;i<inputFiles.size();i++){
				if(!(bval[i]==0 && !(grads[i][0]==100 && grads[i][1]==100 && grads[i][2]==100))) {
					count ++;
					vol = inputFiles.getParamVolume(i).getImageData();
					ImageHeader hdr = vol.getHeader();
					if(mean==null){
						mean = new ImageDataFloat(vol);					
						firstb0index = i;
					} else {
						// run the registration
						if(register.getValue()){
							if(firstb0!=null) {
								System.out.println(getClass().getCanonicalName()+"\t"+"Registering volume: " + count);
								ModelImage volA = vol.getModelImageCopy();
								vol = register(tarImg,volA);
								volA.disposeLocal();
								vol.setHeader(hdr);
							} else {
								System.out.println(getClass().getCanonicalName()+"\t"+"WARNING: No first b0 found. Not registering.");
							}
						}
						ImageDataMath.addFloatImage(mean, vol);
					}
					vol.dispose();
					inputFiles.getParamVolume(i).dispose();			
				}
				this.setCompletedUnits(i+1);
			}
			if(tarImg!=null){
				tarImg.disposeLocal();
			}
			ImageDataMath.scaleFloatValue(mean, (float)(1.0f/count));
			mean.setName(mean.getName()+"_meanDW");	
			meanDW.setValue(mean);
		}


		private ImageDataMipav register(ModelImage target, ModelImage src){
			float xresA = src.getResolutions(0)[0];
			float yresA = src.getResolutions(0)[1];
			float zresA = src.getResolutions(0)[2];
			int xdimA = src.getExtents()[0];
			int ydimA = src.getExtents()[1];
			int zdimA = src.getExtents()[2];
			AlgorithmRegOAR3D reg = new AlgorithmRegOAR3D(target,src,
					AlgorithmCostFunctions.NORMALIZED_XCORRELATION_SMOOTHED,12,AlgorithmTransform.TRILINEAR,
					-20,20,10,5,-20,20,10,5,-20,20,10,5,true,true,false,10,2,3);
			reg.setMultiThreadingEnabled(OARThreadedBool.getValue());
			reg.runAlgorithm();
			TransMatrix finalMatrix = reg.getTransform();
			AlgorithmTransform transform = new AlgorithmTransform(src, finalMatrix, AlgorithmTransform.TRILINEAR,
					xresA, yresA, zresA,xdimA, ydimA, zdimA, true, false, false);
			transform.run();
			ImageDataMipav out = new ImageDataMipav(transform.getTransformedImage());

			reg.finalize(); reg=null;
			transform.finalize(); transform = null;

			return out;
		}
	}
}
