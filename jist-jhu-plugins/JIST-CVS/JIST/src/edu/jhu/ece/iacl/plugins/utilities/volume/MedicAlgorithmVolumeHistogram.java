package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.bme.smile.commons.math.HistogramGenerator;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.plugins.JistPluginUtil;


public class MedicAlgorithmVolumeHistogram extends ProcessingAlgorithm{
	protected ParamVolume vol;
	protected ParamInteger bins;
	protected ParamDouble min,max;
	protected ParamBoolean logScale,autoMinMax,robustMinMax,yLogScale,probability;
	protected ParamInteger rows,cols;
	protected ParamObject<double[][]> histogram;
	protected ParamVolume histImg;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Computes a histogram from an input volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vol=new ParamVolume("Volume"));
		inputParams.add(bins=new ParamInteger("Number of bins",1,10000000,256));
		inputParams.add(logScale=new ParamBoolean("Log Scale"));
		inputParams.add(min=new ParamDouble("Min"));
		inputParams.add(max=new ParamDouble("Max"));
		inputParams.add(autoMinMax=new ParamBoolean("Auto Min/Max",true));
		inputParams.add(robustMinMax=new ParamBoolean("Robust Min/Max"));
		inputParams.add(rows=new ParamInteger("Image Width",0,1600,800));
		inputParams.add(cols=new ParamInteger("Image Height",0,1600,600));
		inputParams.add(yLogScale=new ParamBoolean("Probability in Log Scale",true));
		inputParams.add(probability=new ParamBoolean("Normalize histogram",false));

		inputParams.setPackage("IACL");
		inputParams.setCategory("Measurement.Statistics");
		inputParams.setLabel("Volume Histogram");
		inputParams.setName("Volume_Histogram");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("IACL","",""));
		info.setWebsite("iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.NotFunctional);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(histogram=new ParamObject<double[][]>("Histogram",new ArrayDoubleTxtReaderWriter()));
		outputParams.add(histImg=new ParamVolume("Histogram Image",VoxelType.COLOR_FLOAT));
		histImg.getExtensionFilter().setPreferredExtension("tiff");
	}


	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		HistogramGenerator hist=new HistogramGenerator();
		monitor.observe(hist);
		hist.setBins(bins.getInt());
		hist.setLogScale(logScale.getValue());
		hist.setMin(min.getDouble());
		hist.setMax(max.getDouble());
		hist.setAutoMinMax(autoMinMax.getValue());
		hist.setRobustMinMax(robustMinMax.getValue());
		hist.setProbability(probability.getValue());
		ImageData img=vol.getImageData();
		double[][] histData=hist.solve(img);
		histogram.setFileName(img.getName()+"_hist");
		histogram.setObject(histData);
		ImageDataColor imgData=hist.getImage("Histogram for "+img.getName(),"Intensity",yLogScale.getValue(),rows.getInt(),cols.getInt());
		imgData.setName(img.getName()+"_plot");
		histImg.setValue(imgData);
	}
}
