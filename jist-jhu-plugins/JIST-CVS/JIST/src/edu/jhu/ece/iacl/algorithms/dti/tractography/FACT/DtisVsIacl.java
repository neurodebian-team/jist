package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;

public class DtisVsIacl {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String dir = "/home/john/Research/20070727/corrected_dti/processed/jb_8/";
		String iaclname = dir+"jb_8_iacl_7.dat";
		String dtisname = dir+"jb_8_dtis_7.dat";
		
		System.out.println("jist.plugins"+"\t"+dtisname);
		System.out.println("jist.plugins"+"\t"+iaclname);
		
		DTIStudioReader reader1 = new DTIStudioReader();
		DTIStudioReader reader2 = new DTIStudioReader();
		
		try{
			
			reader1.read(dtisname);
			Fiber[] dtis = reader1.getFibers();
			System.out.println("jist.plugins"+"\t"+"*********************************");
			reader2.read(iaclname);
			Fiber[] iacl = reader2.getFibers();
			
			int howmany = 500;
			
			boolean[][][] missimg = new boolean[256][256][173];
			double[] alldistances= new double[howmany];
			int matches = 0;
			int misses = 0;
//			for(int i=0; i<dtis.length; i++){
			for(int i=0; i<howmany; i++){
//				System.out.println("jist.plugins"+"\t"+"Working on fiber " + i);
				
				for(int j=0; j<dtis[i].getXYZChain().length; j++){	
					XYZ pt = dtis[i].getChain()[j];
					
					if((pt.x % 1f)==0.5 && (pt.y % 1f)==0.5 && (pt.z % 1f)==0.5){
//						System.out.println("jist.plugins"+"\t"+pt.x+" " + pt.y + " " +pt.z);
//						System.out.println("jist.plugins"+"\t"+"Match Found!");
						
						int x=Math.round(pt.x);
						int y=Math.round(pt.y);
						int z=Math.round(pt.z);
//						System.out.println("jist.plugins"+"\t"+x+" " + y + " " +z);
						//WE HAVE THE SEED VOXEL!
						//FIND OUR FIBER WITH THE SAME SEED...
						boolean foundmatch = false;
						for(int k=0; k<iacl.length; k++){
							for(int l=0; l<iacl[k].getXYZChain().length; l++){
								XYZ ourpt = (iacl[k].getXYZChain())[l];
								if(ourpt.x==x && ourpt.y==y && ourpt.z==z){
									alldistances[i] = getdist(dtis[i].getXYZChain(), iacl[k].getXYZChain());
									matches++;
									foundmatch = true;
									break;
//									System.out.println("jist.plugins"+"\t"+"We've found a matching seed in iacl!");
								}
							}
							if(foundmatch){
								break;
							}
						}
						if(!foundmatch){
							misses++;
							missimg[x][y][z]=true;
							alldistances[i]=-1;
						}
						
					}
//					System.out.println("jist.plugins"+"\t"+"**********************");
				}
//				System.out.println("jist.plugins"+"\t"+"We had "+ matches + " matches");
//				System.out.println("jist.plugins"+"\t"+"We had "+ misses + " misses");
//				System.out.println("jist.plugins"+"\t"+"*********************************");
				
			}
			System.out.println("jist.plugins"+"\t"+"We had "+ matches + " matches");
			System.out.println("jist.plugins"+"\t"+"We had "+ misses + " misses");
			
//			DataOutputStream imgout = new DataOutputStream(new FileOutputStream(dir+"_missimg.raw"));
//			for(int k=0; k<missimg[0][0].length;k++){
//				for(int j=1; j<=missimg[0].length;j++){
//					for(int i=0; i<missimg.length;i++){
//						imgout.writeBoolean(missimg[i][missimg[0].length-j][k]);
//					}
//				}
//			}
			
			DataOutputStream distout = new DataOutputStream(new FileOutputStream(dir+"dists.raw"));
			distout.writeInt(howmany);
			for(int g=0; g<howmany;g++){
						distout.writeDouble(alldistances[g]);
			}
			
			
//			for(int i=0; i<10; i++){
//				System.out.println("jist.plugins"+"\t"+dtis[i]);
//			}
//			System.out.println("jist.plugins"+"\t"+"*********************************");
//			for(int i=0; i<10; i++){
//				System.out.println("jist.plugins"+"\t"+iacl[i]);
//			}
			
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	
	public static double getdist(XYZ[] in1, XYZ[] in2){
		double dist=0;
		for(int i=0; i<in1.length; i++){
			
			double cdist = Double.MAX_VALUE;
			
			for(int j=0; j<in2.length; j++){
				double l = (in1[i].minus(in2[j])).length();
				if(l<cdist){  cdist=l; }
			}
			if(cdist>dist){dist=cdist;}
		}
		return dist;
	}

}
