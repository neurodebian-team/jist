package edu.jhu.ece.iacl.algorithms.manual_label;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class LabelImage {
	private static final String cvsversion = "$Revision: 1.11 $";
	public static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");

	public static String get_version() {
		return revnum;
	}


	protected ImageData img;
	protected ArrayList<Integer> allhits;
	protected ArrayList<Integer> allComparisonHits;
	protected TreeSet<GridPt> searched;
	protected String name;
	protected float resX = -1f;
	protected float resY = -1f;
	protected float resZ = -1f;
	protected int rows, cols, slcs;
	public LabelImage(){
		
	}
	public LabelImage(ImageData img){
		setimg(img);
	}
	public LabelImage(List<ImageData> imgs){
		setimg(LabelImage.discreteLabelsFromMemberships(imgs, 0));
	}
	public void setimg(ImageData img){
		this.img = img;
		this.name=img.getName();
		rows = img.getRows();
		cols = img.getCols();
		slcs = img.getSlices();
		resX = img.getHeader().getDimResolutions()[0];
		resY = img.getHeader().getDimResolutions()[1];
		if(slcs>1){
			resZ = img.getHeader().getDimResolutions()[2];
		}
		
	}
	public ImageData getImg(){
		return img;
	}
	public String getName(){
		return name;
	}
	public int getPt(GridPt pt){
		if(isinBounds(pt)){
			return img.get(pt.x, pt.y, pt.z).intValue();
		}else{
			return -1;
		}
	}
	public int getVal(int x, int y, int z){
		return img.getInt(x, y, z);
	}
	public int getVal(GridPt p){
		return getVal(p.x,p.y,p.z);
	}
	public void setVal(int val, int x, int y, int z){
		img.set(x, y, z, val);
	}
	public void setVal(int val, GridPt p){
		setVal(val,p.x,p.y,p.z);
	}
	public void setRes(float resX, float resY){
		this.resX=resX;
		this.resY=resY;
	}
	public void setRes(float resX, float resY, float resZ){
		this.resX=resX;
		this.resY=resY;
		this.resZ=resZ;
	}
	public int[] getDims(){
		int[] dim = {img.getRows(), img.getCols(), img.getSlices()};
		return dim;
	}
	public ArrayList<Integer> getLastComparisonHits(){
		return allComparisonHits;
	}
	public boolean isPtinBounds(GridPt pt){
		if(pt.x<0 || pt.y<0 || pt.z<0){
			return false;
		}
		if(pt.x>img.getRows()){
			return false;
		}
		if(pt.y>img.getCols()){
			return false;
		}
		if(pt.z>img.getSlices()){
			return false;
		}
		return true;
	}
	public boolean sameDims(LabelImage a){
		if(img.getRows()==a.img.getRows() && img.getCols()==a.img.getCols() && img.getSlices()==a.img.getSlices()){
			return true;
		}else{
			return false;
		}
	}
	public static boolean isPtinBounds(int[][][] vol, GridPt pt){
		if(pt.x<0 || pt.y<0 || pt.z<0){
			return false;
		}
		if(pt.x>vol.length){
			return false;
		}
		if(pt.y>vol[0].length){
			return false;
		}
		if(pt.z>vol[0][0].length){
			return false;
		}
		return true;
	}
	public GridPt[] getlabsetArray(int lab){
		ArrayList<GridPt> l = new ArrayList<GridPt>();
		for(int i=0; i<img.getRows();i++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){
					if(img.get(i,j,k).intValue()==lab){
						l.add(new GridPt(i,j,k));
					}
				}
			}
		}
		
		GridPt[] list = new GridPt[l.size()];
		for(int z=0; z<l.size(); z++){
			list[z]=(GridPt)l.get(z);
		}
		return list;
	}

	public ArrayList<GridPt> getlabset(int lab){
		ArrayList<GridPt> l = new ArrayList<GridPt>();
		for(int i=0; i<img.getRows();i++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){
					if(img.getInt(i, j, k)==lab){
						l.add(new GridPt(i,j,k));
					}
				}
			}
		}
		return l;
	}
	
	public double getLabelVolume(int lab){
		ArrayList<GridPt> list = getlabset(lab);
		double volume = resX*resY*resZ*list.size();
		return volume;
	}
	
	/*returns center of mass for a particular label*/
	public XYZ getlabCM(int lab){
		float x = 0;
		float y = 0;
		float z = 0;
		int N = 0;
		for(int i=0; i<img.getRows();i++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){
					if(img.get(i, j, k).intValue()==lab){
						N++;
						x = x+i;
						y = y+j;
						z = z+k;
					}
				}
			}
		}
		if(N>0){
			return new XYZ(x/N,y/N,z/N);	
		}else{
			return null;
		}
	}
	
	public static LabelImage unifyMasks(LabelImage[] imgs){
		LabelImage out = new LabelImage();
		
		/* IF THERE IS OVERLAP:
		 * PRIORITIZE BY ORDER - START OF ARRAY GETS HIGHEST PRIORITY */
		
		ImageData outimg = imgs[0].getImg();
		for(int i=2; i<imgs.length; i++){
			for(int j=0; j<outimg.getRows(); j++){
				for(int k=0; k<outimg.getCols(); k++){
					for(int l=0; l<outimg.getSlices(); l++){
						if(imgs[i].getVal(j, k, l)!=0 && outimg.get(j, k, l).intValue()==0){
							outimg.set(j, k, l, i+1);
						}
					}
				}
			}
		}
		out.setimg(outimg);
		return out;
	}
	
	public HashMap<Integer, ImageData> separateLabels(){
		
		HashMap<Integer, ImageData> allimages = new HashMap<Integer, ImageData>();

		for(int i=0; i<img.getRows();i++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){
					int thislab = img.get(i,j,k).intValue();
					if(allimages.containsKey(new Integer(thislab))){
						allimages.get(thislab).set(i,j,k,1);
					}else{
						System.out.println("Found new label: " + thislab);
						ImageData newimg = img.mimic();
						newimg.set(i,j,k,1);
						allimages.put(thislab, newimg);
					}
				}
			}
		}
		return allimages;
	}
	
//	public boolean writeToMultipleFiles(String out){
//		HashMap<Integer, int[][][]> allimages = separateLabels();
//		Iterator<Integer> it = allimages.keySet().iterator();
//		while(it.hasNext()){
//			Integer a = it.next();
//			RawWriter.writeImgByteF(allimages.get(a), out+"_"+a+".raw");
//		}
//		return true;
//				
//	}
//	
//	public boolean writeToFile(String out){
//		RawWriter.writeImgInt(img, out);
//		return true;	
//	}
//	
//	public void readFromFile(String in, int[] dim){
//		this.img=RawReader.readUSByteImgF(in, dim);
//	}
	
	public boolean isinBounds(GridPt pt){
		if(pt.x<0 || pt.x >=img.getRows() || pt.y<0 || pt.y>=img.getCols() || pt.z<0 || pt.z>=img.getSlices()){
			return false;
		}
		return true;
	}
	
	public ArrayList<Integer> getLabelList(){
		
//		System.out.println("****************");
//		System.out.println(getVal(133,126,67));
//		System.out.println(getVal(119,135,73));
//		System.out.println("****************");
		
		allhits = new ArrayList<Integer>();
		for(int i=0; i<img.getRows();i++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){

					if(getHitIndex(new Integer(getVal(i,j,k)))<0){
						allhits.add(new Integer(getVal(i,j,k)));
//						System.out.println("Added " + getVal(i,j,k) + " at (" +
//								i+","+j+","+k+")");
					}

				}
			}
		}
		allhits.trimToSize();
		return allhits;
	}
	
	/**
	 * Computes a number of statistics between this LabelImage and another LabelImage {Dice Coefficient, Jaccard Coefficient,
	 * the containment of this object in A, the containment of A in this object, the Mountford Index, True Positive, True Negative,
	 * False Positive, and False Negative} for each label in this image.
	 * 
	 *  Note for True Positive(TP), TN, FP and FN: this object is considered the truth.
	 * 
	 * @param a a LabelImage to compare to this LabelImage
	 * @returns the array containing all statistics
	 */
	public double[][] computeStatistics(LabelImage a){
		allhits = getLabelList();
		allhits.trimToSize();
		Collections.sort(allhits);
		
		allComparisonHits = a.getLabelList();
		
		for(Integer num : allhits){
			if (!allComparisonHits.contains(num)){
				allComparisonHits.add(num);
			}
		}
		Collections.sort(allComparisonHits);
		
//		System.out.println("allvals: " + allvals);
		
		double[] nt = new double[allComparisonHits.size()];
		double[] na = new double[allComparisonHits.size()];
		double[] ntia =new double[allComparisonHits.size()];	//intersection or "true positive"
		double[] ntua =new double[allComparisonHits.size()];	//union
		double[] trueneg = new double[allComparisonHits.size()];
		double[] falsepos = new double[allComparisonHits.size()];
		double[] falseneg = new double[allComparisonHits.size()];
		
		int valt = -1;
		int vala = -1;
		
		System.out.println(allComparisonHits);
		
		for(int i=0; i<img.getRows();i++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){
					
//					System.out.println("Working on point" + i+","+j+","+k);

					valt = img.get(i,j,k).intValue();
					nt[allComparisonHits.indexOf(valt)]++;
					
					vala = a.img.get(i,j,k).intValue();
					na[allComparisonHits.indexOf(vala)]++;
					
					if(valt==vala){
						ntua[allComparisonHits.indexOf(vala)]++;
						ntia[allComparisonHits.indexOf(vala)]++;
					}else{
						ntua[allComparisonHits.indexOf(vala)]++;
						ntua[allComparisonHits.indexOf(valt)]++;
					}
					
				}
			}
		}

		System.out.println("nt: " + nt[1]);
		System.out.println("na: " +na[1]);
		System.out.println("ntia: " +ntia[1]);
		System.out.println("ntua: " +ntua[1]);
		
		double[] dice = new double[allComparisonHits.size()];
		double[] jaccard = new double[allComparisonHits.size()];
		double[] TcontA =new double[allComparisonHits.size()];	//intersection
		double[] AcontT = new double[allComparisonHits.size()];	//union
		double[] mountford = new double[allComparisonHits.size()];	//mountford index
		double[] cohenkappa = new double[allComparisonHits.size()];	//cohen's kappa statistic
		double[] pca = new double[allComparisonHits.size()]; //prob of chance agreement (pca) - helps for cohen kappa stat
		       
		int N = img.getRows()*img.getCols()*img.getSlices();
		System.out.println("N: " +N);
		for(int n=0; n<allComparisonHits.size(); n++){
			dice[n] = (2*(ntia[n]))/(nt[n]+na[n]);
			jaccard[n] = ntia[n]/ntua[n];
			TcontA[n] = ntia[n]/na[n];
			AcontT[n] = ntia[n]/nt[n];
			mountford[n] = (2 * (ntia[n]))/((2 * nt[n] * na[n]) - ((na[n] + nt[n]) * ntia[n]));
			trueneg[n] = N - ntua[n];
			falsepos[n] = na[n]-ntia[n];
			falseneg[n] = nt[n]-ntia[n];
			pca[n] = (na[n]*nt[n] + (N-na[n])*(N-nt[n]))/(N*N);
			cohenkappa[n]= ((ntia[n]+trueneg[n])/N - pca[n]) / (1 - pca[n]);
		}
		
//		double[] meanDists = meanDists(a);
//		double[][] stats = {dice, jaccard, TcontA, AcontT, meanDists};
		
		double[][] stats = {dice, jaccard, TcontA, AcontT, mountford, ntia, trueneg, falsepos, falseneg, cohenkappa};
		return stats;
	}
	
	public double[] oneWayHausdorffDistance(LabelImage b, int lab){
		double[] dist = new double[3];	// {max, meantot, meanedge}
		ArrayList<GridPt> thishitlist = getlabset(lab);
		int numedges = 0;
		for(GridPt pt : thishitlist){
//			System.out.println("Working on Point: " + pt);
			if(b.getVal(pt)!=lab){
				System.out.println("Images don't overlap -- Looking for closest point!");
				double thisdist = pt.distance(findClosestBLabelTo(pt,b,lab,2000));
				System.out.println("Distance: " + thisdist);
				if(thisdist>dist[0]){
					dist[0]=thisdist;
				}
				dist[1]=dist[1]+thisdist;
				dist[2]=dist[2]+thisdist;
				numedges++;
			}
		}
		System.out.println("Num Edges was: " + numedges);
		
		dist[1]=dist[1]/thishitlist.size();
		if(numedges==0){
			dist[2] = -1;
		}else{
			dist[2] = dist[2]/numedges;
		}
		System.out.println("HD : " + dist[0]);
		System.out.println("MM : " + dist[1]);
		System.out.println("MME: " + dist[2]);
		return dist;
	}
	
	/*
	 * Helper method for one-way Hausdorff distance
	 * Note: This method assumes that b(pt)!= lab
	 */
	private GridPt findClosestBLabelTo(GridPt pt, LabelImage b, int lab, int maxNbhdSize){
		
		boolean nohit = true;
		searched = new TreeSet<GridPt>();
		TreeSet<GridPt> bound = new TreeSet<GridPt>();
		TreeSet<GridPt> hits = new TreeSet<GridPt>();
		searched.add(pt);
		
		GridPt[] nbhd = pt.neighborhood6C();
		System.out.println(nbhd.length);
	
		for(int i=0; i<nbhd.length; i++){
			
			GridPt addme = new GridPt(nbhd[i].x,nbhd[i].y,nbhd[i].z,pt.distance(nbhd[i]));
			if(searched.add(addme)){
//				System.out.println("Seached: " + searched);
//				System.out.println("Adding to Bound " + nbhd[i]);
				bound.add(nbhd[i]);
				//note because of the conditions of this method being called, the if statement below
				//is not necessary.  However, it is included here for generality and completeness.
				if(b.getVal(nbhd[i])==lab){  
					System.out.println("Done");
					nohit=false;
					return nbhd[i];
				}
			}
		}
		
		while(nohit && searched.size()<maxNbhdSize){
			
			// Get new boundary Set;
			bound = getBoundarySet(bound);
			for(GridPt bpt : bound){
				if(b.getVal(bpt)==lab){
					nohit=false;
					hits.add(bpt);
				}
			}	
//			System.out.println("Seached: " + searched);
//			System.out.println("Bound: " + bound);
		}
		
		//Determine which of the ''hit'' points are closest to the seed point
		GridPt minDistPt = new GridPt(-1,-1,-1);
		if(hits.size()>0){
			double mindist = Double.MAX_VALUE;
			for(GridPt hpt : hits){
				if(minDistPt.x==-1){
					minDistPt = hpt;
					mindist = hpt.distance(pt);
				}else if (hpt.distance(pt)<mindist){
					minDistPt = hpt;
					mindist = hpt.distance(pt);
				}
			}
		}
		System.out.println("Closest Point is: " + minDistPt);
		return minDistPt;
	}
	
	private TreeSet<GridPt> getBoundarySet(TreeSet<GridPt> prevBoundSet){
		TreeSet<GridPt> boundSet = new TreeSet<GridPt>();
		
		for(GridPt pt : prevBoundSet){
			GridPt[] nbhd = pt.neighborhood6C();
			for(int i=0; i<nbhd.length; i++){
				if(searched.add(nbhd[i])){
					boundSet.add(nbhd[i]);
				}
			}
		}
		return boundSet;
	}
	
	public LabelImage organizeLabels(){
		LabelImage out = new LabelImage(img.mimic());
		if(allhits==null){
			getLabelList();
		}
		
		System.out.println("out: "+out);
		System.out.println("allhits: " +allhits);
		System.out.println("allhits size: " +allhits.size());
		System.out.println("img: "+img);
		for(int l=0; l<img.getRows();l++){
			for(int j=0; j<img.getCols();j++){
				for(int k=0; k<img.getSlices();k++){
					if(img.get(l, j, k).intValue()!=0){
//						System.out.println(img.get(l, j, k));
						out.setVal(getHitIndex(img.getInt(l, j, k))+1, l, j, k);
					}
				}
			}
		}
		return out;
	}
	
	public int getHitIndex(int hit){
		int i = 0;
		for(Integer n : allhits){
			if(n.intValue()==hit){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	private double[] meanDistsUniDir(LabelImage a, LabelImage b){
		ArrayList<Integer> allvals = getLabelList();
		allvals.trimToSize();
		
		double[] meandists = new double[allvals.size()];
	
		Iterator<Integer> it = allvals.iterator();
		int k=0;
		while(it.hasNext()){
			int val = it.next().intValue();
			GridPt[] myvalset = a.getlabsetArray(val);
			GridPt[] avalset = b.getlabsetArray(val);
			
			if(myvalset.length==0 || avalset.length==0){
				meandists[k]=Double.MAX_VALUE;
			}else{
				double sum = 0;
				for(int i=0; i<myvalset.length; i++){
					double mindist = Double.MAX_VALUE;
					for(int j=0; j<avalset.length; j++){
						if(myvalset[i].distance(avalset[j])<mindist){
							mindist=myvalset[i].distance(avalset[j]);
						}
					}
					sum=sum+mindist;
				}
				meandists[k]=sum/myvalset.length;
				k++;
			}
		}
		return meandists;
	}
	
	private double meanDistsUniDir(LabelImage a, LabelImage b, int val){
		ArrayList<Integer> allvals = getLabelList();
		allvals.trimToSize();

		double meandists = 0;

		GridPt[] myvalset = a.getlabsetArray(val);
		GridPt[] avalset = b.getlabsetArray(val);
		
		//catch the case where one label image does not contain the label of interest
		if(myvalset.length==0 || avalset.length==0){
			return Double.MAX_VALUE;
		}else{
			System.out.println("resx: " +resX);
			double sum = 0;
			for(int i=0; i<myvalset.length; i++){
				double mindist = Double.MAX_VALUE;
				for(int j=0; j<avalset.length; j++){
//					if(myvalset[i].distance(avalset[j])<mindist){
					if(myvalset[i].distance(avalset[j],resX,resY,resZ)<mindist){
//						mindist=myvalset[i].distance(avalset[j]);
						
						mindist=myvalset[i].distance(avalset[j],resX,resY,resZ);
					}
				}
				sum=sum+mindist;
			}
			meandists=sum/myvalset.length;
			return meandists;
		}
	}
	
	public double[] meanDists(LabelImage a){
		double[] fwddists = meanDistsUniDir(this,a);
		double[] revdists = meanDistsUniDir(a,this);
		double[] out = new double[fwddists.length];
		for(int i=0; i<out.length; i++){
			out[i]=(fwddists[i]+revdists[i])/2;
		}
		return out;
	}
	
	public double meanDists(LabelImage a, int label){
		double fwddists = meanDistsUniDir(this,a,label);
		double revdists = meanDistsUniDir(a,this,label);
		double out = (fwddists+revdists)/2;
		return out;
	}
	
	public static String runAllComparisons(List<ImageData> list, int numRaters){
		int numVols = list.size();
		int numImages = numVols/numRaters;
		String out = " Label Number \t Dice \t Jaccard \t Fwd Containment \t Rev Containment \n \n";
		for(int l=0; l<numRaters; l++){
			for(int j=0; j<numImages; j++){
				for(int k=1; k<numRaters-l; k++){
					
					System.out.println("Index 1: " + (j+l*numImages));
					System.out.println("Index 2: " + (j+l*numImages+k*numImages));
					
					LabelImage a = new LabelImage(list.get(j+l*numImages));
					LabelImage b = new LabelImage(list.get(j+l*numImages+k*numImages));
					if(!a.sameDims(b)){
						System.err.println("Corresponding images must have like dimensions");
					}
					double[][] metrics = a.computeStatistics(b);
					String ithstep = "";
					ithstep=ithstep+"Comparing: " + a.img.getName() + " and " + b.img.getName() + "\n";
					for(int i=0; i<a.allhits.size(); i++){
//						out=out+"Level "+i+" Results: \t";
//						System.out.println("This hit is: " + a.allhits.get(i));
						ithstep=ithstep+"" + a.allhits.get(i)+"\t";
						ithstep=ithstep+"" + metrics[0][i]+"\t";
						ithstep=ithstep+"" + metrics[1][i]+"\t";
						ithstep=ithstep+"" + metrics[2][i]+"\t";
						ithstep=ithstep+"" + metrics[3][i]+"\n";
//						ithstep=ithstep+"Mean Distance:" + metrics[4][i]+"\n";
					}
					System.out.println(ithstep);
					out=out+ithstep+"\n";

				}
				out=out+"\n";
			}
		}
		return out;
	}
	
	public static double fleissReliability(List<LabelImage> list){
		ArrayList<Integer> alllabels = getCombinedLabelList(list);
		Collections.sort(alllabels);
		int rows = list.get(0).getImg().getRows();
		int cols = list.get(0).getImg().getCols();
		int slices = list.get(0).getImg().getSlices();
		int raters = list.size();
		
		int N = rows*cols*slices;	//number of targets (ie voxels)
		int R = list.size();		//number of raters 
		float[] labelfreq = new float[alllabels.size()];
		float agreement = 0;
		for(int i=0; i<rows;i++){
			for(int j=0; j<cols;j++){
				for(int k=0; k<slices;k++){
					for(int l=0; k<raters;l++){
						//increment labelcount
						labelfreq[alllabels.indexOf(list.get(l).getImg().getInt(i, j, k))]++;
						
						agreement+=list.get(l).getImg().getInt(i, j, k)*list.get(l).getImg().getInt(i, j, k);
					}
					
				}
			}
		}
		agreement = (agreement -(N*R))/(N*R*(R-1));
		float chanceagreement = 0;
		for(int l=0; l<alllabels.size();l++){
			chanceagreement+=(labelfreq[l]*labelfreq[l])/(N*N);
		}
		return (agreement-chanceagreement)/(1-chanceagreement);
	}
	
	public static ArrayList<Integer> getCombinedLabelList(List<LabelImage> list){
		ArrayList<Integer> listout, templist;
		listout = list.get(0).getLabelList();
		for(int i=1; i<list.size(); i++){
			templist = list.get(i).getLabelList();
			for(int j=0; j<templist.size(); j++){
				if(!listout.contains(templist.get(j))){
					listout.add(templist.get(j));
				}
			}
		}
		return listout;
	}
	
	public static ArrayList<LabelImage> getLabelImageList(List<ImageData> imglist){
		ArrayList<LabelImage> out = new ArrayList<LabelImage>(imglist.size());
		for(int i=1; i<imglist.size(); i++){
			out.add(new LabelImage(imglist.get(i)));
		}
		return out;
	}
	
	
	public static String runMeanDistance(List<ImageData> list, int numRaters, int label){
		int numVols = list.size();
		int numImages = numVols/numRaters;

		String out = "Mean Distance\n";
		for(int l=0; l<numRaters; l++){
			for(int j=0; j<numImages; j++){
				for(int k=1; k<numRaters-l; k++){
					
					LabelImage a = new LabelImage(list.get(j+l*numImages));
					LabelImage b = new LabelImage(list.get(j+l*numImages+k*numImages));
					if(!a.sameDims(b)){
						System.err.println("Corresponding images must have like dimensions");
					}
					String ithstep = "";
					
					double meanDists = a.meanDists(b,label);
					ithstep=ithstep+"" + meanDists+"\n";
					out=out+ithstep;
					System.out.println(ithstep);
				}
			}
		}
		return out;
	}
	
	public static String runCMDistance(List<ImageData> list, int numRaters, int label){
		int numVols = list.size();
		int numImages = numVols/numRaters;

		String out = "Center of Mass Distance\n";
		out = out+"for label "+label+"\n";
		for(int l=0; l<numRaters; l++){
			for(int j=0; j<numImages; j++){
				for(int k=1; k<numRaters-l; k++){
					
					LabelImage a = new LabelImage(list.get(j+l*numImages));
					LabelImage b = new LabelImage(list.get(j+l*numImages+k*numImages));
					if(!a.sameDims(b)){
						System.err.println("Corresponding images must have like dimensions");
					}
					String ithstep = "";
					
					XYZ apt = a.getlabCM(label);
					XYZ bpt = b.getlabCM(label);
					
					
					
					double cmDist = 0;
					if(apt!=null && bpt!=null){
						apt.scale(a.resX,a.resY,a.resZ);
						bpt.scale(b.resX,b.resY,b.resZ);
						
						System.out.println("CM1: " +apt);
						System.out.println("CM2: " +bpt);
						
						cmDist = apt.distance(bpt);
					}else{
						cmDist=Double.MAX_VALUE;
					}
					
					ithstep=ithstep+"" + cmDist+"\n";
					out=out+ithstep;
					System.out.println(ithstep);
				}
			}
		}
		return out;
	}
	
	public static String runCenterOfMasses(List<ImageData> list, int label){
		
		String out = "Center of masses for label: " + label+"\n";
			
		for(int i=0; i<list.size(); i++){
			LabelImage a = new LabelImage(list.get(i));
			XYZ cm = a.getlabCM(label);
			if(cm!=null){
				out = out+a.img.getName()+"\t";
				out = out+"("+cm.x+","+cm.y+","+cm.z+")\n";
				
			}else{
				out = out+"(NaN,NaN,NaN)\n";
			}
		}
		return out;
		
	}

	public static String runVolumes(List<ImageData> list, int label){
		String out = "Volumes for label: " + label+"\n";
			
		for(int i=0; i<list.size(); i++){
			LabelImage a = new LabelImage(list.get(i));
			
			ImageHeader hdr = list.get(i).getHeader();
			float []res=hdr.getDimResolutions();
			a.setRes(res[0],res[1],res[2]);
			
			out = out+a.img.getName()+"\t";
			out = out+a.getLabelVolume(label)+"\n";
			
		}
		return out;
		
	}
	
	public static String[] runHausdorfDistance(List<LabelImage> list, int label){
		String[] out = new String[3];
		String hdout = "Hausdorff Distances for label: " + label+"\n";
		String mmout ="Mean-Min Distances for label: "+ label+"\n";
		String mmeout = "Edge-Weighted Mean-Min Distances for label: "+ label+"\n";
		
		for(int i=0; i<list.size(); i++){
			for(int j=i+1; j<list.size(); j++){
				double[] thesestatsfwd = list.get(i).oneWayHausdorffDistance(list.get(j), label);
				double[] thesestatsbck = list.get(j).oneWayHausdorffDistance(list.get(i), label);
				
				hdout = hdout + list.get(i).getName() + "\t" + list.get(j).getName() + "\t" + Math.max(thesestatsfwd[0],thesestatsbck[0]) + "\n";
				mmout = mmout + list.get(i).getName() + "\t" + list.get(j).getName() + "\t" + (thesestatsfwd[1]+thesestatsbck[1])/2 + "\n";
				if(thesestatsfwd[2]<0){
					mmeout = mmeout + list.get(i).getName() + "\t" + list.get(j).getName() + "\t" + thesestatsbck[2] + "\n";
				}else if(thesestatsbck[2]<0){
					mmeout = mmeout + list.get(i).getName() + "\t" + list.get(j).getName() + "\t" + thesestatsfwd[2] + "\n";
				}else{
					mmeout = mmeout + list.get(i).getName() + "\t" + list.get(j).getName() + "\t" + (thesestatsfwd[2]+thesestatsbck[2])/2 + "\n";
				}
				
			}
		}
		
		out[0]=hdout;
		out[1]=mmout;
		out[2]=mmeout;
		return out;
	}
	public static ImageData discreteLabelsFromMemberships(List<ImageData> imgs, float thresh){
//		System.out.println();
		JistLogger.logOutput(JistLogger.FINEST, "\nInput images are" + imgs);
		ImageDataInt seg = new ImageDataInt(imgs.get(0).getRows(),imgs.get(0).getCols(),
				imgs.get(0).getSlices(),imgs.get(0).getComponents());
		seg.setName(imgs.get(0).getName()+"_hardSeg");
		seg.setHeader(imgs.get(0).getHeader());
		for(int i=0;i<seg.getRows();i++){
			for(int j=0;j<seg.getCols();j++){
				for(int k=0;k<seg.getSlices();k++){
					Float max = thresh;
					for(int l=0; l<imgs.size(); l++){
						if(imgs.get(l).getFloat(i, j, k)> max){
							max=imgs.get(l).getFloat(i, j, k);
							seg.set(i, j, k, l+1);
						}
					}
				}
			}
		}
		return seg;
	}
	public static ImageData discreteLabelsFromMemberships(List<ImageData> imgs, int[] labels, float thresh){
//		System.out.println();
		JistLogger.logOutput(JistLogger.FINEST, "\nInput images are" + imgs);
		ImageDataInt seg = new ImageDataInt(imgs.get(0).getRows(),imgs.get(0).getCols(),
				imgs.get(0).getSlices(),imgs.get(0).getComponents());
		seg.setName(imgs.get(0).getName()+"_hardSeg");
		seg.setHeader(imgs.get(0).getHeader());
		for(int i=0;i<seg.getRows();i++){
			for(int j=0;j<seg.getCols();j++){
				for(int k=0;k<seg.getSlices();k++){
					Float max = thresh;
					for(int l=0; l<imgs.size(); l++){
						if(imgs.get(l).getFloat(i, j, k)> max){
							max=imgs.get(l).getFloat(i, j, k);
							seg.set(i, j, k, labels[l]);
						}
					}
				}
			}
		}
		return seg;
	}
	public static ImageData discreteLabelsFromMemberships(List<ImageData> imgs, List<Integer> labellist, float thresh){
		
		ImageDataInt seg = new ImageDataInt(imgs.get(0).getRows(),imgs.get(0).getCols(),
				imgs.get(0).getSlices(),imgs.get(0).getComponents());
		seg.setName(imgs.get(0).getName()+"_hardSeg");
		seg.setHeader(imgs.get(0).getHeader());
		for(int i=0;i<seg.getRows();i++){
			for(int j=0;j<seg.getCols();j++){
				for(int k=0;k<seg.getSlices();k++){
					Float max = thresh;
					for(int l=0; l<imgs.size(); l++){
						if(imgs.get(l).getFloat(i, j, k)> max){
							max=imgs.get(l).getFloat(i, j, k);
							seg.set(i, j, k, labellist.get(l));
						}
					}
				}
			}
		}
		return seg;
	}
}
