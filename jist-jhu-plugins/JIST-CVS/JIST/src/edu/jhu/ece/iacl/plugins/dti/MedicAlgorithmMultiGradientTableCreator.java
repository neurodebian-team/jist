package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import Jama.Matrix;
import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.dti.DTIGradientTableCreator;
import edu.jhu.ece.iacl.algorithms.dti.GTCparams;
import edu.jhu.ece.iacl.algorithms.dti.GradientTable;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringArrayXMLReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;


/*
 * The MedicAlgorithmMultiGradientTableCreator class is the MAPS module wrapper
 * for DTIGradientTableCreator.  It's inputs are a collection of parameters (written to text files) obtained 
 * from ParInfo modules ({@link MedicAlgorithmMultiParV3Info}, and {@link MedicAlgorithmMultiParV4Info})
 * It concatenates the resulting Gradient tables and writes them to a file in the desired format.
 * 
 * @author John Bogovic
 *
 * @see MedicAlgorithmMultiParV3Info
 * @see MedicAlgorithmMultiParV4Info
 */
public class MedicAlgorithmMultiGradientTableCreator extends ProcessingAlgorithm{
	//output params
	private ParamFile grad;	// gradient directions 
	private ParamFile bout;		// b values

	//input params
	private ParamOption Center;
	private ParamOption Scanner;
	private ParamOption FatShift;
	private ParamOption Foldover;
	private ParamOption PatientOrientation;
	private ParamOption PatientPosition;
	private ParamOption GradChoice;
	private ParamOption release;
	private ParamFile inTable;

	private ParamObject<String> sliceAngulationst;
	private ParamObject<String> sliceOrientst;
	private ParamObject<String> Foldoverst;
	private ParamObject<String> PatOrientst;
	private ParamObject<String> PatPosst;

	private ParamMatrix sliceAngulation;
	private ParamOption SliceOrient;
	private ParamObject<String> date;
	private ParamObject<String> parversion;

	private ParamFileCollection bvalsin;

	private ParamBoolean sortImgs;
	private ParamOption tabletype;

	private HashMap<String,String> paramMap; 
	int[] numbvals;
	ArrayList<String> allbs;

	private final StringArrayXMLReaderWriter saxml = new StringArrayXMLReaderWriter();

	GTCparams params;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.11 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "The inputs to this module are a collection of parameters (written to text files) obtained" +
    "from ParInfo modules {ParV3Info, ParV4Info}.  It concatenates the resulting Gradient tables and writes them to a file in the desired format.";
  		
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(date = new ParamObject<String>("Date", new StringReaderWriter()));

		inputParams.add(parversion = new ParamObject<String>("Par Version", new StringReaderWriter()));

		inputParams.add(sliceAngulationst= new ParamObject<String>("Slice Angulation (from Par)", new StringReaderWriter()));
		sliceAngulationst.setMandatory(false);

		//Slice angulation can always be obtained from a par file
		inputParams.add(sliceAngulation = new ParamMatrix("Slice Angulation",3,1));
		sliceAngulation.setValue(new Matrix(new double[][]{{-1},{-1},{-1}}));

		inputParams.add(sliceOrientst = new ParamObject<String>("Slice Orientation (from Par)", new StringReaderWriter()));
		sliceOrientst.setMandatory(false);

		String[] slcorient = {"<read from Par>","Transverse","Sagittal","Coronal"};
		inputParams.add(SliceOrient = new ParamOption("Slice Orientation",slcorient));

		inputParams.add(Foldoverst = new ParamObject<String>("Foldover (from Par)", new StringReaderWriter()));
		Foldoverst.setMandatory(false);

		String[] fo = {"<read from Par>","Anterior-Posterior","Right-Left","Superior-Inferior"};
		inputParams.add(Foldover = new ParamOption("Foldover",fo));

		inputParams.add(PatOrientst = new ParamObject<String>("Patient Orientation (from Par)", new StringReaderWriter()));
		PatOrientst.setMandatory(false);

		String[] po = {"<read from Par>","Supine","Prone","Right Decubitus","Left Decubitus"};
		inputParams.add(PatientOrientation = new ParamOption("Patient Orientation",po));

		inputParams.add(PatPosst = new ParamObject<String>("Patient Postition (from Par)", new StringReaderWriter()));
		PatPosst.setMandatory(false);

		String[] ppos = {"<read from Par>","Head-First","Feet-First"};
		inputParams.add(PatientPosition = new ParamOption("Patient Position",ppos));

		String[] cntr = {"KIRBY","Other"};
		inputParams.add(Center = new ParamOption("Center",cntr));

		String[] scn = {"1.5","3.0"}; 
		inputParams.add(Scanner = new ParamOption("Scanner",scn));
		Scanner.setValue(1);
		
		String[] fs = {"Anterior","Posterior","Right","Left","Superior","Inferior"};
		inputParams.add(FatShift = new ParamOption("Fat Shift",fs));

//		inputParams.add(numVols = new ParamInteger("Number of Volumes"));
//		numVols.setValue(new Integer(34));

		String[] gt = {"Jones30 (KKI Only)","Inverted Jones30 (KKI Only)","Philips No Overplus High","Philips No Overplus Medium","Philips No Overplus Low",
				"Philips Yes Overplus High","Philips Yes Overplus Medium","Philips Yes Overplus Low", "User-defined"};
		inputParams.add(GradChoice = new ParamOption("Gradient Table",gt));
		GradChoice.setValue(0);

		inputParams.add(inTable = new ParamFile("User-defined Gradient Table"));
		inTable.setMandatory(false);

		String[] rel = {"-KIRBY (auto)-","Rel_11.x","Rel_11.x","Rel_2.0","Rel_2.1","Rel_2.5"};
		inputParams.add(release = new ParamOption("Philips Release",rel));

		inputParams.add(sortImgs = new ParamBoolean("\"Sort Images\" checked on scanner"));
		sortImgs.setValue(true);

		StringReaderWriter bvalrw = new StringReaderWriter();
		bvalrw.setExtensionFilter(new FileExtensionFilter(new String[]{"b"}));
//		inputParams.add(bvalsin = new ParamObject<String>("b Values", bvalrw));
		inputParams.add(bvalsin = new ParamFileCollection("b Values", new FileExtensionFilter(new String[]{"b"})));

		String[] tt = {"DTIStudio","IACL","FSL","MedINRIA"};
		inputParams.add(tabletype = new ParamOption("Output Table Type",tt));
		tabletype.setValue(1);


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("DTI Gradient Table Creator");
		inputParams.setName("DTI_Gradient_Table_Creator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.add(new AlgorithmAuthor("Jon Farrell", "", ""));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(bout = new ParamFile("b values",new FileExtensionFilter(new String[]{"b"})));
		outputParams.add(grad = new ParamFile("Gradient Table",new FileExtensionFilter(new String[]{"grad"})));
	}


	/**
	 * The execute algorithm that implements ProcessingAlgorithms's abstract method definition. 
	 */
	protected void execute(CalculationMonitor monitor) {
		numbvals = getNumbvals();
		GradientTable[] gradtables = new GradientTable[numbvals.length];
		GradientTable gt = new GradientTable();

		for(int i=0; i<numbvals.length; i++){
			setParams(i);
			System.out.println(getClass().getCanonicalName()+"\t"+"**************");
			System.out.println(getClass().getCanonicalName()+"\t"+"******INPUT PARAMS TO MODULE********");
			System.out.println(getClass().getCanonicalName()+"\t"+"**************");
			printParams();

			//Do some work...
			if(params.verifyParams()){
				System.out.println(getClass().getCanonicalName()+"\t"+"Parameters are Legit!");

				DTIGradientTableCreator gtc = new DTIGradientTableCreator(params);
				gtc.run();
				GradientTable gti = new GradientTable(gtc.getCorrectedTable());
				System.out.println(getClass().getCanonicalName()+"\t"+gti.toString());
				if(i==0){
					gt = new GradientTable(gti.getTable());
				}else{
					gt = gt.append(gti);
//					System.out.println(getClass().getCanonicalName()+"\t"+gt.toString());
				}
				
				gradtables[i]=gti; 

			}else{
				//Error handling on incompatible parameters
				System.err.println("jist.plugins"+"Incompatible Parameters");
			}
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"************");
		System.out.println(getClass().getCanonicalName()+"\t"+"FINAL TABLE");
		System.out.println(getClass().getCanonicalName()+"\t"+gt.toString());
		System.out.println(getClass().getCanonicalName()+"\t"+"************");

		String gradobj;
		if(tabletype.getIndex()==0){
			gradobj = gt.toDtisTable();
		}else if(tabletype.getIndex()==1){
			gradobj = gt.toArrayTxt();
		}else if(tabletype.getIndex()==2){
			gradobj = gt.toFslTable();
		}else if(tabletype.getIndex()==3){
			gradobj = gt.toMedINRIATable();
		}else{
			gradobj = "Problem!";
		}
//		grad.setFileName("GradientTable.grad");

		StringReaderWriter rw = new StringReaderWriter();

		// Need to correct bvalues for when the PAR files are incorrect
		// as in the example files provided with DTIStudio.


		// append all bvalues
		System.out.println(getClass().getCanonicalName()+"\t"+"IN NEW CODE");
		String bstringout = "";
		double [][]table = gt.getTable();
		int j=0;
		String lastB0 = "0";
		for(String s : allbs){
			String []bs = s.split("[ \r\n\t]+");
			for (String jS : bs) {
				jS=jS.trim();
				if(jS.length()==0)
					continue;
				if((table[j][0]!=0)||(table[j][0]!=0)||(table[j][0]!=0)) {
					if(Double.valueOf(jS)==0) {
						bstringout = bstringout + lastB0 + "\n";
						System.out.println(getClass().getCanonicalName()+"\t"+"WARNING: Correcting for zero-b-value when gradient table is known.");
					} else {
						bstringout = bstringout + jS + "\n";
						lastB0 = jS;
					}
				} else {
					lastB0 = jS;
					bstringout = bstringout + jS + "\n";
				}
				j++;
			}
//			bstringout = bstringout+s;
		}

		//write Gradient Table and bValues to file
		File outputdir = this.getOutputDirectory();
		File gradout = null;
		File bvalout = null;
		try{
			File destdir = new File(outputdir.getCanonicalFile()+File.separator+this.getAlgorithmName());
			if(!destdir.isDirectory()){
				(new File(destdir.getCanonicalPath())).mkdir();
			}
			String f = destdir.getCanonicalPath()+File.separator+bvalsin.getValue().get(0).getName();
			f = f.substring(0, f.lastIndexOf('.'));
			bvalout = new File(f+"_concat.b");
			System.out.println(getClass().getCanonicalName()+"\t"+"Writing bvalues to : "+bvalout);
			
			String g = destdir.getCanonicalPath()+File.separator+bvalsin.getValue().get(0).getName();
			g = g.substring(0, g.lastIndexOf('.'));
			gradout = new File(f+"_concat.grad");
			System.out.println(getClass().getCanonicalName()+"\t"+"Writing bvalues to : "+gradout);
			
			rw.write(gradobj, gradout);
			rw.write(bstringout, bvalout);

		}catch(IOException e){
			e.printStackTrace();
		}

		grad.setValue(gradout);
		bout.setValue(bvalout);
	}


	/**
	 * This method sets the Parameters for the ith set of inputs.
	 * @param  i  The index of the input set under consideration.
	 */
	private void setParams(int i){
		setMap();
		params=new GTCparams();
		
		params.flag = Center.getValue();
		if(Foldover.getIndex()==0){
			try {
			params.fo = paramMap.get(saxml.readStrings(Foldoverst.getObject()).get(i));
		} catch (RuntimeException e) {
			System.out.println(getClass().getCanonicalName()+"\t"+"ERROR: Unable to extract foldover from par file. -- Is this a version 3.X?");
			System.err.println("jist.plugins"+"ERROR: Unable to extract foldover from par file. -- Is this a version 3.X?");
		  throw e;			   
		}
		}else{
			params.fo = paramMap.get(Foldover.getValue());
		}
		params.fs = paramMap.get(FatShift.getValue());
		params.grad_choice = GradChoice.getValue();
		if(PatientOrientation.getIndex()==0){
			try {
//			System.out.println(getClass().getCanonicalName()+"\t"+"** "+saxml.readStrings(PatOrientst.getObject()).get(i));
			params.patient_orientation = paramMap.get(saxml.readStrings(PatOrientst.getObject()).get(i));
			} catch (RuntimeException e) {
				System.out.println(getClass().getCanonicalName()+"\t"+"ERROR: Unable to extract patient orientation from par file. -- Is this a version 3.X?");
				System.err.println("jist.plugins"+"ERROR: Unable to extract patient orientation from par file. -- Is this a version 3.X?");
			  throw e;			   
			}
		}else{
			params.patient_orientation = paramMap.get(PatientOrientation.getValue());
		}

		if(PatientPosition.getIndex()==0){
//			System.out.println(getClass().getCanonicalName()+"\t"+"In here");
			try {
			System.out.println(getClass().getCanonicalName()+"\t"+"** "+saxml.readStrings(PatPosst.getObject()).get(i));
			params.patient_position = paramMap.get(saxml.readStrings(PatPosst.getObject()).get(i));
			} catch (RuntimeException e) {
				System.out.println(getClass().getCanonicalName()+"\t"+"ERROR: Unable to extract patient position from par file. -- Is this a version 3.X?");
				System.err.println("jist.plugins"+"ERROR: Unable to extract patient position from par file. -- Is this a version 3.X?");
			  throw e;			   
			}
		}else{
			params.patient_position = paramMap.get(PatientPosition.getValue());
		}

		params.scanner = Scanner.getValue();
		if(sliceAngulation.getValue().getArray()[0][0]==-1){
			double[] v = saxml.parseVector(saxml.readStrings(sliceAngulationst.getObject()).get(i));
			System.out.println(getClass().getCanonicalName()+"\t"+v.length);
			System.out.println(getClass().getCanonicalName()+"\t"+saxml.readStrings(sliceAngulationst.getObject()).get(i));
			System.out.println(getClass().getCanonicalName()+"\t"+v[0]+ " " + v[1] + " " + v[2]);
			params.sliceAng = new double[][]{{v[0]},{v[1]},{v[2]}};
		}else{
			params.sliceAng = sliceAngulation.getValue().getArray();
		}
		
		if(SliceOrient.getIndex()==0){
			params.slice_orient = paramMap.get(saxml.readStrings(sliceOrientst.getObject()).get(i));
		}else{
			params.slice_orient = paramMap.get(SliceOrient.getValue());
		}

		params.date = saxml.readStrings(date.getObject()).get(i);
//		params.numvolumes=numVols.getInt();
		params.numvolumes=numbvals[i];
		params.release=release.getValue();
		params.sort_image=sortImgs.getValue();
		params.parversion=saxml.readStrings(parversion.getObject().trim()).get(i);

		System.out.println(getClass().getCanonicalName()+"\t"+"");
		params.printParams();
		System.out.println(getClass().getCanonicalName()+"\t"+"");
	}


	private void printParams(){
		System.out.println(getClass().getCanonicalName()+"\t"+Center);
		System.out.println(getClass().getCanonicalName()+"\t"+Scanner);
		System.out.println(getClass().getCanonicalName()+"\t"+FatShift);
		System.out.println(getClass().getCanonicalName()+"\t"+Foldover);
		System.out.println(getClass().getCanonicalName()+"\t"+PatientOrientation);
		System.out.println(getClass().getCanonicalName()+"\t"+PatientPosition);
		System.out.println(getClass().getCanonicalName()+"\t"+sliceAngulation.getValue().get(0, 0) + 
				"," + sliceAngulation.getValue().get(1, 0) +
				"," + sliceAngulation.getValue().get(2, 0));
		System.out.println(getClass().getCanonicalName()+"\t"+date.getObject());
		System.out.println(getClass().getCanonicalName()+"\t"+sortImgs.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+parversion.getObject());
	}


	/**
	 * This creates a HashMap that converts informative parameter names to shorthands
	 * used in DTIGradientTable Creator.
	 * 
	 * @see DTIGradientTableCreator
	 */
	private void setMap(){
		paramMap = new HashMap<String,String>();

		//Foldover
		paramMap.put("Anterior-Posterior", "AP");
		paramMap.put("Right-Left", "RL");
		paramMap.put("Head-Foot", "HF");
		paramMap.put("Superior-Inferior", "HF");

		//Fat shift
		paramMap.put("Right", "R");
		paramMap.put("Left", "L");
		paramMap.put("Anterior", "A");
		paramMap.put("Posterior", "P");
		paramMap.put("Head", "H");
		paramMap.put("Superior", "H");
		paramMap.put("Foot", "F");
		paramMap.put("Feet", "F");
		paramMap.put("Inferior", "F");

		//slice orienation
		paramMap.put("Axial", "tra");
		paramMap.put("Transverse", "tra");
		paramMap.put("Sagittal", "sag");
		paramMap.put("Coronal", "cor");

		//Patient Position and Orientation
		paramMap.put("Supine", "sp");
		paramMap.put("Left Decubitus", "ld");
		paramMap.put("Right Decubitus", "rd");
		paramMap.put("Prone", "pr");
		paramMap.put("Head First", "hf");
		paramMap.put("Head-First", "hf");
		paramMap.put("Foot First", "ff");
		paramMap.put("Feet First", "ff");
		paramMap.put("Foot-First", "ff");
		paramMap.put("Feet-First", "ff");
	}



	/**
	 * Looks at all of the input par files and determines the 
	 * number of DW volumes (including b0's)
	 * 
	 * @returns An int array, the ith element of which is the number
	 * of DW volumes in the ith Par file.
	 */
	private int[] getNumbvals(){
		allbs = new ArrayList<String>();
		StringReaderWriter rw = new StringReaderWriter();
		for(File f : bvalsin.getValue()){
			allbs.add(rw.read(f));
		}
		int[] Nb0 = new int[allbs.size()];
		int j = 0;
		for(int k=0; k<allbs.size(); k++){
			j=0;
			/* Treat each list of bvalues seperately   */
			for(int i=0; i<allbs.get(k).length(); i++){
				if(allbs.get(k).charAt(i)=='\n'){
					j++;
				}
			}
			Nb0[k]=j;
			System.out.println(getClass().getCanonicalName()+"\t"+"Found " +Nb0[k] + " volumes in the "+k+"th dwi");
		}
		return Nb0;
	}
}
