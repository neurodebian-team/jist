package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

public class Volume4DToSlabCollection extends ProcessingAlgorithm {
	/****************************************************
	 * Input Parameters
	 ****************************************************/

	private ParamVolume inputvol;		// Files to be split
	private ParamInteger numslabs;
	private ParamOption dimOpt;
	

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolumeCollection outputFiles;	// Slabs



	//Variables
	ArrayList<File> subVols;
	File outDir;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts a 4D Volume to a CombinedSlabCollection.";
	private static final String longDescription = "Splits the input volume into a number of smaller volumes 'slabs', along the dimension specified \n" +
												  "(rows, collumns, slices or components). The user can specify the number of slabs with the 'Number of Slabs' input.\n" +
												  "The output collection will have 'numSlabs' volumes, with the same dimensions as the input volume, except for the splitted dimension.";

	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("UMCU");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Volume4DToSlabCollection");
		inputParams.setName("Volume4D_To_SlabCollection");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.add(new AlgorithmAuthor("Daniel Polders", "daniel.polders@gmail.com", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription +"\n"+ longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(inputvol= new ParamVolume("Input Volume"));
		inputvol.setLoadAndSaveOnValidate(false);
		inputParams.add(numslabs= new ParamInteger("Number of Slabs",1,100,4));
		String[] dimlist = {"Rows", "Collumns", "Slices", "Components"}; 
		inputParams.add(dimOpt = new ParamOption("Dimension to be splitted",dimlist));
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputParams.add(outputFiles = new ParamVolumeCollection("Output Combined Slabs"));
		outputFiles.setLoadAndSaveOnValidate(false);
	}


	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}
	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {
			this.setLabel("Initializing");

			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 ****************************************************/
			int nSlabs = numslabs.getInt();
			ImageData vol = inputvol.getImageData();
			String dim = dimOpt.getValue();
			ImageData outputSlab;
			VoxelType type=null;
			int lastSlabEndRow=-1;
			int lastSlabEndCollumn=-1;
			int lastSlabEndSlice=-1;
			int lastSlabEndComponent=-1;
	
			this.setTotalUnits(nSlabs);
			for(int jSlab=0;jSlab<nSlabs;jSlab++) {
				outputSlab = null;
				this.setLabel("Splitting of Slab "+(jSlab+1)+" of "+ (nSlabs+1));
				if (dim.contentEquals("Rows")){
					int startRow=0, endRow=0, nRows;
					type=vol.getType();
					if(outputSlab==null) {
						int totalRows = vol.getRows();
						int RowsPerSlab = (int)Math.ceil(totalRows/(float)nSlabs);
						startRow = lastSlabEndRow+1;
						endRow = startRow+RowsPerSlab-1;
						endRow = (endRow>=totalRows?totalRows-1:endRow);
						lastSlabEndRow=endRow;
						nRows = endRow-startRow+1;
						String jSlabNum = String.format("%02d", jSlab);
						if (type.name().compareTo("UBYTE")==0)
							outputSlab=new ImageDataUByte(nRows, vol.getCols(),vol.getSlices(),vol.getComponents());
						else if (type.name().compareTo("FLOAT")==0)
							outputSlab=new ImageDataFloat(nRows, vol.getCols(),vol.getSlices(),vol.getComponents());
						else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
							outputSlab=new ImageDataInt(nRows, vol.getCols(),vol.getSlices(),vol.getComponents());
						else
							outputSlab=new ImageDataDouble(nRows, vol.getCols(),vol.getSlices(),vol.getComponents());
						outputSlab.setName(vol.getName()+"_"+dim +"_Slab"+jSlabNum);
						outputSlab.setHeader(vol.getHeader());
						JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Image Type: "+outputSlab.getType().name());
					}
					JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Rows: "+startRow+"-"+endRow);
					for(int j=0;j<vol.getCols();j++) {
						for (int k=0; k< vol.getSlices(); k++){
							for(int l=0; l<vol.getComponents(); l++){
								int jRow=0;
								for(int i=startRow;i<=endRow;i++) {
									if (type.name().compareTo("UBYTE")==0)
										outputSlab.set(jRow,j,k,l,vol.getUByte(i, j, k,l));
									else if (type.name().compareTo("FLOAT")==0)
										outputSlab.set(jRow,j,k,l,vol.getFloat(i, j, k,l));
									else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
										outputSlab.set(jRow,j,k,l,vol.getInt(i, j, k,l));
									else
										outputSlab.set(jRow,j,k,l,vol.getDouble(i, j, k,l));
									jRow++;
								}
							}
						}
					}
				} else if (dim.contentEquals("Collumns")){
					int startCollumn=0, endCollumn=0, nCollumns;
					type=vol.getType();
					if(outputSlab==null) {
						int totalCollumns = vol.getCols();
						int CollumnsPerSlab = (int)Math.ceil(totalCollumns/(float)nSlabs);
						startCollumn = lastSlabEndCollumn+1;
						endCollumn = startCollumn+CollumnsPerSlab-1;
						endCollumn = (endCollumn>=totalCollumns?totalCollumns-1:endCollumn);
						lastSlabEndSlice=endCollumn;
						nCollumns = endCollumn-startCollumn+1;
						String jSlabNum = String.format("%02d", jSlab);
						if (type.name().compareTo("UBYTE")==0)
							outputSlab=new ImageDataUByte(vol.getRows(), nCollumns,vol.getSlices(),vol.getComponents());
						else if (type.name().compareTo("FLOAT")==0)
							outputSlab=new ImageDataFloat(vol.getRows(), nCollumns,vol.getSlices(),vol.getComponents());
						else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
							outputSlab=new ImageDataInt(vol.getRows(), nCollumns,vol.getSlices(),vol.getComponents());
						else
							outputSlab=new ImageDataDouble(vol.getRows(), nCollumns,vol.getSlices(),vol.getComponents());
						outputSlab.setName(vol.getName()+"_"+dim +"_Slab"+jSlabNum);
						outputSlab.setHeader(vol.getHeader());
						JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Image Type: "+outputSlab.getType().name());
					}
					JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Collumns: "+startCollumn+"-"+endCollumn);
					for(int i=0;i<vol.getRows();i++) {
						for (int k=0; k< vol.getSlices(); k++){
							for(int l=0; l<vol.getComponents(); l++){
								int jCollumn=0;
								for(int j=startCollumn;j<=endCollumn;j++) {
									if (type.name().compareTo("UBYTE")==0)
										outputSlab.set(i,jCollumn,k,l,vol.getUByte(i, j, k,l));
									else if (type.name().compareTo("FLOAT")==0)
										outputSlab.set(i,jCollumn,k,l,vol.getFloat(i, j, k,l));
									else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
										outputSlab.set(i,jCollumn,k,l,vol.getInt(i, j, k,l));
									else
										outputSlab.set(i,jCollumn,k,l,vol.getDouble(i, j, k,l));
									jCollumn++;
								}
							}
						}
					}
				
				} else if (dim.contentEquals("Slices")){
					int startSlice=0, endSlice=0, nSlices;
					type=vol.getType();
					if(outputSlab==null) {
						int totalSlices = vol.getSlices();
						int slicesPerSlab = (int)Math.ceil(totalSlices/(float)nSlabs);
						startSlice = lastSlabEndSlice+1;
						endSlice = startSlice+slicesPerSlab-1;
						endSlice = (endSlice>=vol.getSlices()?vol.getSlices()-1:endSlice);
						lastSlabEndSlice=endSlice;
						nSlices = endSlice-startSlice+1;
						String jSlabNum = String.format("%02d", jSlab);
						if (type.name().compareTo("UBYTE")==0)
							outputSlab=new ImageDataUByte(vol.getRows(), vol.getCols(),nSlices,vol.getComponents());
						else if (type.name().compareTo("FLOAT")==0)
							outputSlab=new ImageDataFloat(vol.getRows(), vol.getCols(),nSlices,vol.getComponents());
						else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
							outputSlab=new ImageDataInt(vol.getRows(), vol.getCols(),nSlices,vol.getComponents());
						else
							outputSlab=new ImageDataDouble(vol.getRows(), vol.getCols(),nSlices,vol.getComponents());
						outputSlab.setName(vol.getName()+"_"+dim +"_Slab"+jSlabNum);
						outputSlab.setHeader(vol.getHeader());
						JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Image Type: "+outputSlab.getType().name());
					}
					JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Slices: "+startSlice+"-"+endSlice);
					for(int i=0;i<vol.getRows();i++) {
						for(int j=0;j<vol.getCols();j++) {
							for(int l=0; l<vol.getComponents(); l++){
								int jSlice=0;
								for(int k=startSlice;k<=endSlice;k++) {
									if (type.name().compareTo("UBYTE")==0)
										outputSlab.set(i,j,jSlice,l,vol.getUByte(i, j, k,l));
									else if (type.name().compareTo("FLOAT")==0)
										outputSlab.set(i,j,jSlice,l,vol.getFloat(i, j, k,l));
									else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
										outputSlab.set(i,j,jSlice,l,vol.getInt(i, j, k,l));
									else
										outputSlab.set(i,j,jSlice,l,vol.getDouble(i, j, k,l));
									jSlice++;
								}
							}
						}
					}
				} else if (dim.contentEquals("Components")){
					int startComponent=0, endComponent=0, nComponents;
					type=vol.getType();
					if(outputSlab==null) {
						int totalComponents = vol.getComponents();
						int ComponentsPerSlab = (int)Math.ceil(totalComponents/(float)nSlabs);
						startComponent = lastSlabEndComponent+1;
						endComponent = startComponent+ComponentsPerSlab-1;
						endComponent = (endComponent>=totalComponents?totalComponents-1:endComponent);
						lastSlabEndComponent=endComponent;
						nComponents = endComponent-startComponent+1;
						String jSlabNum = String.format("%02d", jSlab);
						if (type.name().compareTo("UBYTE")==0)
							outputSlab=new ImageDataUByte(vol.getRows(), vol.getCols(),vol.getSlices(),nComponents);
						else if (type.name().compareTo("FLOAT")==0)
							outputSlab=new ImageDataFloat(vol.getRows(), vol.getCols(),vol.getSlices(),nComponents);
						else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
							outputSlab=new ImageDataInt(vol.getRows(), vol.getCols(),vol.getSlices(),nComponents);
						else
							outputSlab=new ImageDataDouble(vol.getRows(), vol.getCols(),vol.getSlices(),nComponents);
						outputSlab.setName(vol.getName()+"_"+dim +"_Slab"+jSlabNum);
						outputSlab.setHeader(vol.getHeader());
						JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Image Type: "+outputSlab.getType().name());
					}
					JistLogger.logOutput(2, "VolumeComponentsToSlabCollection: Components: "+startComponent+"-"+endComponent);
					for(int i=0;i<vol.getRows();i++) {
						for(int j=0;j<vol.getCols();j++) {
							for(int k=0; k<vol.getSlices(); k++){
								int jComponent=0;
								for(int l=startComponent;l<=endComponent;l++) {
									if (type.name().compareTo("UBYTE")==0)
										outputSlab.set(i,j,k,jComponent,vol.getUByte(i, j, k,l));
									else if (type.name().compareTo("FLOAT")==0)
										outputSlab.set(i,j,k,jComponent,vol.getFloat(i, j, k,l));
									else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
										outputSlab.set(i,j,k,jComponent,vol.getInt(i, j, k,l));
									else
										outputSlab.set(i,j,k,jComponent,vol.getDouble(i, j, k,l));
									jComponent++;
								}
							}
						}
					}
				}
				this.setLabel("Saving Slab " + (jSlab+1));
				outputFiles.add(outputSlab);
				outputFiles.writeAndFreeNow(alg);
				this.setCompletedUnits(jSlab);
			}
		}
	}
}
	
