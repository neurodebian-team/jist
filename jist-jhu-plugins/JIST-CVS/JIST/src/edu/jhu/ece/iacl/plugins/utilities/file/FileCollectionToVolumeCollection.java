package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.util.List;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;


public class FileCollectionToVolumeCollection extends ProcessingAlgorithm {
	ParamFileCollection in;
	ParamVolumeCollection out;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "") .replace(" ", "");
	private static final String shortDescription = "Convert a FileCollection into a VolumeCollection.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		FileExtensionFilter fef = new FileExtensionFilter();
		inputParams.add(in = new ParamFileCollection("File Collection", fef));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("FileCollection To VolumeCollection");
		inputParams.setName("FileCollection_To_VolumeCollection");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		FileExtensionFilter fef = new FileExtensionFilter();
		outputParams.add(out = new ParamVolumeCollection("Output File Collection"));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {

		List<File> alist = in.getValue();

		for(File in : alist) {
			out.add(in);
		}
	}
}
