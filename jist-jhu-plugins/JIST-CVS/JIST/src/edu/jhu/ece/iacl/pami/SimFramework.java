package edu.jhu.ece.iacl.pami;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import Jama.Matrix;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;


public class SimFramework extends ProcessingAlgorithm{
	//Input Variables
	private ParamMatrix param_D;
	private ParamObject<double[][]> param_Ds;
	private ParamObject<double[][]> param_g;
	private ParamFloat param_b;
	private ParamInteger param_dimX;
	private ParamInteger param_dimY;
	private ParamInteger param_dimZ;
	private ParamFloat param_sigma;
	private ParamFloat param_Sref;
	//Output Variables
	protected ParamVolume param_Sdw, param_SdwNF;
	protected ParamFile bout;
	protected ParamFile gout;
	protected ParamMatrix param_imgMatrix;
	protected ParamFloat param_sigma_out;
	protected ParamFloat param_Sref_out;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Simulation Framework. Takes parameters for one voxel and returns the diffusion weighted signal based on a diffusion tensor model.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams){
		//Plugin Information
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Simulation");
		inputParams.setLabel("SimFramework");
		inputParams.setName("SimFramework");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Yu Ouyang", "couyang@jhu.edu", "http://sites.google.com/a/jhu.edu/pami/"));
		info.add(new AlgorithmAuthor("Hanlin Wan", "hwan1@jhu.edu", "http://sites.google.com/a/jhu.edu/pami/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		//Inputs
		inputParams.add(param_D = new ParamMatrix("Diffusion Tensor",3,3));
		inputParams.add(param_Ds = new ParamObject<double[][]>("Diffusion Tensor File", new ArrayDoubleTxtReaderWriter()));
		param_Ds.setMandatory(false);	// directions will be generated randomly if empty
		inputParams.add(param_g = new ParamObject<double[][]>("Gradient Directions", new ArrayDoubleTxtReaderWriter()));
		inputParams.add(param_b = new ParamFloat("b",700));
		inputParams.add(param_dimX = new ParamInteger("dimX",10));
		inputParams.add(param_dimY = new ParamInteger("dimY",10));
		inputParams.add(param_dimZ = new ParamInteger("dimZ",10));
		inputParams.add(param_sigma = new ParamFloat("sigma",0));
		inputParams.add(param_Sref = new ParamFloat("S_ref",1));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(param_Sdw = new ParamVolume("Output Volume",null,-1,-1,-1,-1));
		outputParams.add(param_SdwNF = new ParamVolume("Noise Free DW Volume",null,-1,-1,-1,-1));
		outputParams.add(gout = new ParamFile("Gradient Tables",new FileExtensionFilter(new String[]{"grad"})));
		outputParams.add(bout = new ParamFile("b-Values",new FileExtensionFilter(new String[]{"b"})));
		outputParams.add(param_imgMatrix = new ParamMatrix("imgMatrix",1,1)); //size of matrix changed later
		outputParams.add(param_sigma_out = new ParamFloat("sigma"));
		outputParams.add(param_Sref_out = new ParamFloat("Sref"));
	}


	protected void execute(CalculationMonitor monitor) {
		//pass through sigma and Sref
		param_sigma_out.setValue(param_sigma.getFloat());
		param_Sref_out.setValue(param_Sref.getFloat());

		//Initialize
		double[][] Ds = (double[][])param_Ds.getObject();
		boolean useDFile = (Ds != null);
		int dimX, dimY, dimZ;
		if(useDFile)
			if(Ds[0].length!=6)
				useDFile =false;
		if(useDFile) {
			dimX = (int) Ds[0][0];
			dimY = (int) Ds[0][1];
			dimZ = (int) Ds[0][2];
		}
		else {
			dimX = param_dimX.getInt();
			dimY = param_dimY.getInt();
			dimZ = param_dimZ.getInt();
		}
		int count = 1;
		Matrix tmpProduct = null;
		Matrix D = param_D.getValue();
		Matrix D2 = new Matrix(6,1);
		Matrix g = new Matrix((double[][])param_g.getObject());
		float b = param_b.getFloat();
		float sigma = param_sigma.getFloat();
		float Sref = param_Sref.getFloat();
		int row = g.getRowDimension();
		float [][][][]out = new float[dimX][dimY][dimZ][row];
		float [][][][]outNF = new float[dimX][dimY][dimZ][row];
		ArrayList<double[]> dirD = new ArrayList<double[]>();

		//rebuild g as a Nx6 matrix
		Matrix G = new Matrix(row, 6);
		for (int i=0; i<row; i++) {
			double x = g.get(i,0);
			double y = g.get(i,1);
			double z = g.get(i,2);
			double norm = Math.sqrt(x*x+y*y+z*z);
			if(norm>0) {
				x/=norm;y/=norm;z/=norm;
				g.set(i,0,x);
				g.set(i,1,y);
				g.set(i,2,z);
			}

			G.set(i, 0, x*x);
			G.set(i, 1, 2*x*y);
			G.set(i, 2, 2*x*z);
			G.set(i, 3, y*y);
			G.set(i, 4, 2*y*z);
			G.set(i, 5, z*z);
		}

		if (!useDFile) {
			//rebuild D as a 6x1 column vector
			D2.set(0, 0, D.get(0,0));
			D2.set(1, 0, D.get(0,1));
			D2.set(2, 0, D.get(0,2));
			D2.set(3, 0, D.get(1,1));
			D2.set(4, 0, D.get(1,2));
			D2.set(5, 0, D.get(2,2));
			D2.print(8, 4);
			tmpProduct = G.times(D2);
		}


		//Loop over all directions, including ref
		for (int i=0; i < dimX; i++) {
			for (int j=0; j < dimY; j++) {
				for (int k=0; k < dimZ; k++) {
					if (useDFile) {
						D2 = new Matrix(Ds[count++], 6);
						tmpProduct = G.times(D2);
						dirD.add(D2.getColumnPackedCopy());
					}
					for (int l=0; l < row; l++) {
						if ((g.getMatrix(l, l, 0, 2)).norm1()==0) {
							outNF[i][j][k][l] = Sref;
							out[i][j][k][l] = addRicianNoise(outNF[i][j][k][l], sigma);
						}
						else {
							outNF[i][j][k][l] = Sref*(float)(Math.exp((double) -b*tmpProduct.get(l,0)));
							out[i][j][k][l] = addRicianNoise(outNF[i][j][k][l], sigma);
						}
					}
				}
			}
		}

		ImageDataFloat tmp = (new ImageDataFloat(out));
		tmp.getHeader().setSliceThickness(0);		//used to suppress printing warning message
		tmp.setName("Output");
		param_Sdw.setValue(tmp);

		tmp = (new ImageDataFloat(outNF));
		tmp.getHeader().setSliceThickness(0);		//used to suppress printing warning message
		tmp.setName("OutputNF");
		param_SdwNF.setValue(tmp);

		param_imgMatrix = new ParamMatrix("imgMatrix",row,6);
		Matrix imgMat = G.times(b);
		param_imgMatrix.setValue(imgMat);

		// Output files
		String bfilename = getOutputDirectory().toString() + File.separatorChar +  "bvals.b";
		String gradfilename = getOutputDirectory().toString() + File.separatorChar +  "grads.grad";
		try {
			// b
			File bfile = new File(bfilename);
			FileWriter rw;
			rw = new FileWriter(bfile,false);

			for (int i=0; i<row; i++) {
				if ((g.getMatrix(i, i, 0, 2)).norm1()==0)
					rw.write(0+"\n");
				else
					rw.write(b+"\n");
			}
			rw.close();
			bout.setValue(bfile);

			// gradient
			File gfile = new File(gradfilename);
			rw = new FileWriter(gfile,false);

			for(int i=0; i<row; i++) {
				rw.write(g.get(i,0)+" \t"+g.get(i,1)+" \t"+g.get(i,2)+"\n");
			}
			rw.close();
			gout.setValue(gfile);
		}
		catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("I/O Error.");
		}
	}


	public float addRicianNoise(float Sdw, float sigma) {
		Random rnd = new Random();
		double r1 = rnd.nextGaussian()*sigma;
		double r2 = rnd.nextGaussian()*sigma;
		float out = (float) Math.sqrt((Sdw + r1)*(Sdw + r1) + r2*r2);
		return out;
	}
}
