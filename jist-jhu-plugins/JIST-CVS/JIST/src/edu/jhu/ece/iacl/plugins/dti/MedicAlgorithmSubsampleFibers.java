package edu.jhu.ece.iacl.plugins.dti;

import java.util.*;

import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.Curve;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;


public class MedicAlgorithmSubsampleFibers extends ProcessingAlgorithm {
	//input params
	private ParamObject<FiberCollection> fibers;

	//ouputparams
	private ParamObject<CurveCollection> fiberLines;
	private ParamInteger maxFibers;
	private ParamOption samplingMethod;

	private static final String cvsversion = "$Revision: 1.11 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fibers=new ParamObject<FiberCollection>("DTI Studio Fibers",new FiberCollectionReaderWriter()));
		inputParams.add(samplingMethod=new ParamOption("Sampling Method",new String[]{"Length","Random"}));
		inputParams.add(maxFibers=new ParamInteger("Maximum number of fibers",-1,(int)1E7,10000));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Sub-sample Fibers");
		inputParams.setName("Sub-sample_Fibers");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("Blake Lucas","blake.c.lucas@gmail.com",""));
		info.setDescription("Sub-samples collection of fibers and converts them to VTK format");
		info.setLongDescription("");
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");	
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fiberLines=new ParamObject<CurveCollection>("VTK Fibers", new CurveVtkReaderWriter()));
	}


	protected static class StaticCurve implements Comparable<StaticCurve>{
		public Curve curve;
		protected double len;
		public StaticCurve(Curve c){
			this.curve=c;
			len=c.getValue();
		}
		public int compareTo(StaticCurve c2) {
			return (int)Math.signum(c2.len-len);
		}
	}


	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		CurveCollection curves=fibers.getObject().toCurveCollection();
		int max=maxFibers.getInt();
		if(max>0){
			if(samplingMethod.getIndex()==0){
				ArrayList<StaticCurve> subSampleCurves=new ArrayList<StaticCurve>(curves.size()); 
				for(Curve c:curves){
					subSampleCurves.add(new StaticCurve(c));
				}
				Collections.sort(subSampleCurves);
				CurveCollection newCollection=new CurveCollection();
				for(int i=0;i<max&&i<subSampleCurves.size();i++){
					newCollection.add(subSampleCurves.get(i).curve);
				}
				newCollection.setName(curves.getName()+"_subsample");
				fiberLines.setObject(newCollection);
			} else {
				ArrayList<StaticCurve> subSampleCurves=new ArrayList<StaticCurve>(curves.size()); 
				for(Curve c:curves){
					subSampleCurves.add(new StaticCurve(c));
				}
				Collections.shuffle(subSampleCurves);
				CurveCollection newCollection=new CurveCollection();
				for(int i=0;i<max&&i<subSampleCurves.size();i++){
					newCollection.add(subSampleCurves.get(i).curve);
				}
				newCollection.setName(curves.getName()+"_subsample");
				fiberLines.setObject(newCollection);
			}
		} else {
			fiberLines.setObject(curves);
		}
	}
}
