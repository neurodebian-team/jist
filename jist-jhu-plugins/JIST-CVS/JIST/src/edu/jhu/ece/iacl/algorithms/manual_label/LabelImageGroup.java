package edu.jhu.ece.iacl.algorithms.manual_label;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import edu.jhu.ece.iacl.jist.structures.geom.GridPt;


public class LabelImageGroup {
	
	protected HashMap<Integer,LabelImage> groups;
	protected int[] dim;
	
	public LabelImageGroup(){
		groups = new HashMap<Integer,LabelImage>();
	}
	
	public void add(LabelImage img, Integer key){
		if(groups.isEmpty()){
			groups.put(key, img);
//			int[] thisdim=img.getDims();
		}else{
			int[] thisdim=img.getDims();
			if(this.dim[0]==thisdim[0] && this.dim[1]==thisdim[1] && this.dim[2]==thisdim[2]){
				groups.put(key, img);
			}else{
				System.out.println(getClass().getCanonicalName()+"\t"+"ERROR: DIMENSIONS NOT CONSISTENT");
			}
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"Dim:" + dim[0] + " " + dim[1] + " " + dim[2]);
	}
	public void add(LabelImage img, int key){
		groups.put(new Integer(key), img);
	}
	public Set<Integer> keySet(){
		return groups.keySet();
	}
	
	public int[] getValuesForAllImages(GridPt pt){
		Set<Integer> keys = keySet();
		int[] out = new int[keys.size()];
		Iterator<Integer> it = keys.iterator();

		int j=0;
		while(it.hasNext()){
			Integer i = it.next();
			out[j]=groups.get(i).getVal(pt);
			System.out.println(getClass().getCanonicalName()+"\t"+"Value for label:" + i);
			System.out.println(getClass().getCanonicalName()+"\t"+out[j]);
		}
		return out;
	}
	
//	public synchronized Labelimg genMaxKeyImage(){
//		Set<Integer> keyset = keySet();
//		
//		Labelimg high = new Labelimg(dim);
//		Labelimg out = new Labelimg(dim);
//		for(int i=0; i<dim[0]; i++){
//			System.out.println(getClass().getCanonicalName()+"\t"+"On row:" + i);
//			for(int j=0; j<dim[1]; j++){
//				for(int k=0; k<dim[2]; k++){
//					Iterator<Integer> it = keyset.iterator();
//					while(it.hasNext()){
//						Integer thiskey = it.next();
//						Labelimg thisimg = groups.get(thiskey);
//						if(thisimg.getPt(i, j, k)>high.getPt(i, j, k)){
//							high.setVal(thisimg.getPt(i,j,k), i, j, k);
//							out.setVal(thiskey, i, j, k);
//						}
//						
//					}
//				}
//			}
//		}
//		return out;
//	}
	
	public ArrayList<int[]> getDefaultGroupMerges(){
		int[] mergePrefronalR = {139,137,138,129,120};
		int[] mergePrefronalL = {39,37,38,29,20};
		int[] mergeParietalR = {109,126,130,132};
		int[] mergeParietalL = {9,26,30,32};
		int[] mergeTemporalR = {108,110,116,131};
		int[] mergeTemporalL = {8,10,16,31};
		int[] mergeOccipitalR = {106,112,114};
		int[] mergeOccipitalL = {6,12,14};
		int[] PrePostMergeR = {123,125};
		int[] PrePostMergeL = {23,25};
		
		ArrayList<int[]> mergeus = new ArrayList<int[]>(10);
		mergeus.add(mergePrefronalR);
		mergeus.add(mergePrefronalL);
		mergeus.add(mergeParietalR);
		mergeus.add(mergeParietalL);
		mergeus.add(mergeTemporalR);
		mergeus.add(mergeTemporalL);
		mergeus.add(mergeOccipitalR);
		mergeus.add(mergeOccipitalL);
		mergeus.add(PrePostMergeR);
		mergeus.add(PrePostMergeL);
		
		return mergeus;
	}
	
	public void mergeGroups(ArrayList<int[]> mergeus){
		Iterator<int[]> it = mergeus.iterator();
		HashMap<Integer,LabelImage> newgroups = new HashMap<Integer,LabelImage>();
		int num=0;
		while(it.hasNext()){
			int[] these = it.next();
			LabelImage[] tomerge = new LabelImage[these.length];
			for(int i=0; i<these.length; i++){
				tomerge[i]=groups.get(these[i]);
			}
			LabelImage merged = LabelImage.unifyMasks(tomerge);
			newgroups.put(new Integer(num), merged);
			num++;
		}
		groups = newgroups;
	}
	
	// Creates LabelImageGroup using all files that fit the template string
	// i.e. files of the form template*.ext are added to their own group
	// in the returned FiberGroups
//	public void readFiberGroups(String directory, String template, String ext){
//		System.out.println(getClass().getCanonicalName()+"\t"+"Reading Label Image groups:");
//		System.out.println(getClass().getCanonicalName()+"\t"+"From directory: " + directory);
//		System.out.println(getClass().getCanonicalName()+"\t"+"with template: " + template);
//		
//		File dir = new File(directory);
//		
//		/* Get files fitting template */
//		LabelGroupFileFilter filter = new LabelGroupFileFilter(template,ext);
//		File[] readUs = dir.listFiles(filter);
////		System.out.println(getClass().getCanonicalName()+"\t"+readUs);
//		/* Read those files and drop into 'groups' */
//		int[] dim = {256,256,173};
//		this.dim=dim;
//		int skip = 352;
//		int numgroups = 0;
//		for(int i=0; i<readUs.length; i++){
////		for(int i=0; i<4; i++){
//			String ithfile = readUs[i].getPath();
//			int key = Integer.parseInt(ithfile.substring(ithfile.lastIndexOf("_")+1, ithfile.lastIndexOf(".")));
//			System.out.println(getClass().getCanonicalName()+"\t"+"Found file: " + ithfile);
//			System.out.println(getClass().getCanonicalName()+"\t"+"Adding fibers w/ key " +  key);
//			numgroups++;
//			
////			READ
//			int[][][] ithimg = RawReader.readIntImgFLE(ithfile, dim, skip);
//			
//			add(new Labelimg(ithimg),key);
//			
//		}
//		System.out.println(getClass().getCanonicalName()+"\t"+"Created " + numgroups + " groups");
//
//	}

}
