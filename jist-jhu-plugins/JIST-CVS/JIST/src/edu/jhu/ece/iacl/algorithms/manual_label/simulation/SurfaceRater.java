package edu.jhu.ece.iacl.algorithms.manual_label.simulation;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import javax.vecmath.Point2i;

import edu.jhu.ece.iacl.jist.io.SurfaceVtkReaderWriter;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;

/**
 * Simulates a random surface rater characterized by their
 * 1) Probability of error
 * 2) Probability of boundary error (vs global error)
 * 3) Confusion Matrix (for global error)
 * 4) Probability that boundary errors will occur on particular boundaries
 * 5) Probability that the boundary will "shrink" or "grow".
 * 
 * Note: A boundary between label i and j (i!=j, i<j) is represented as 
 * a Point2i with p(1)=i, p(2)=j.  
 * Note: We adopt the convention that p(1)<p(2).
 * 
 * If this boundary 'shrinks' it means that label j replaces label i.
 * If this boundary 'grows' it means that label i replaces label j.
 * 
 * @author John Bogovic 
 *
 */
public class SurfaceRater {
	
	EmbeddedSurface truthsurf;
	EmbeddedSurface ratersurf;
	int[][] nbrtable;
	int dataoffset;
	
	ConfusionMatrix confusion;
	float[] perError;			
	
	private float totReliability;		//global probability of error
	private float[] growPerProb;		//probability that a certain boundary will grow or shrink.
	private float[] boundErrProb;		//probability that, given an error, it will occur on a certain boundary 
	private float[] boundErrProbCum;	//cumulative probability of the above - useful for calculations      
	private String outputname;
	
	private int[] labels;
	private ArrayList<Point2i> boundaries;						//input boundaries corresponding to growPerProb
	private HashMap<Point2i, ArrayList<Integer>> boundverts;	//stores the vertex indices beloning to each boundary
	private HashSet<Integer> changedverts;
	private int numboundverts;
	private Random rand = new Random();
	
	public SurfaceRater(EmbeddedSurface truthsurf){
		this.truthsurf=truthsurf;
	}
	
	public SurfaceRater(EmbeddedSurface truthsurf, float totReliability, int[] labels, float[][] confusion, int offset){
		this.truthsurf=truthsurf;
		this.totReliability=totReliability;
		this.confusion=new ConfusionMatrix(confusion,labels);
	}
	
	public SurfaceRater(EmbeddedSurface truthsurf, float[] perError, float totReliability, int offset){
		this.truthsurf=truthsurf;
		this.perError=perError;
		this.totReliability=totReliability;
		labels = getLabelList(truthsurf, offset);
	}
	
	public SurfaceRater(EmbeddedSurface truthsurf, float[][] confusion, float[] perError, float totReliability, int offset){
		dataoffset=offset;
		labels = getLabelList(truthsurf, offset);
		this.confusion=new ConfusionMatrix(confusion,labels);
		this.truthsurf=truthsurf;
		this.perError=perError;
		this.totReliability=totReliability;
	}
	
	public SurfaceRater(EmbeddedSurface truthsurf, float totReliability, ArrayList<Point2i> boundaries, float[] boundErrProb, float[] growPerProb,  int offset){
		this.truthsurf=truthsurf;
		this.totReliability=totReliability;
		
		this.boundaries=boundaries;
		this.boundErrProb=boundErrProb;
		this.growPerProb=growPerProb;
		
		dataoffset=offset;
		
		labels = getLabelList(truthsurf, offset);
	}
	
	public SurfaceRater(EmbeddedSurface truthsurf, float totReliability, float[][] confusion, ArrayList<Point2i> boundaries, float[] boundErrProb, float[] growPerProb,  int offset){
		this.truthsurf=truthsurf;
		this.totReliability=totReliability;
		
		this.boundaries=boundaries;
		this.boundErrProb=boundErrProb;
		this.growPerProb=growPerProb;
		
		dataoffset=offset;
		
		labels = getLabelList(truthsurf, offset);
		this.confusion=new ConfusionMatrix(confusion,labels);
	}
	
	public void setoffset(int offset){
		dataoffset = offset;
	}
	public void setName(String name){
		outputname = name;
	}
	public void setNeighborTable(int[][] table){
		nbrtable = table;
	}
	/**
	 * clean up everything related to the rater data to 
	 * prepare to create a new rater
	 */
	public void reset(){
		ratersurf = null;
		boundverts = null;
		boundErrProbCum=null;
		System.gc();
		try{ Thread.sleep(1, 0); }catch(Exception e){}
	}
	public void setSeed(long seed){
		rand = new Random(seed);
	}
	
	/**
	 * Computes boundaries, and boundary vertices
	 */
	public void computeBoundaryInformation(){
		if(nbrtable==null){
			nbrtable = EmbeddedSurface.buildNeighborVertexVertexTable(truthsurf, EmbeddedSurface.Direction.COUNTER_CLOCKWISE);
		}
		
		numboundverts = 0;
		int vertcount = truthsurf.getVertexCount();
		double[][] dat = truthsurf.getVertexData();
		
		int lab=-1;
		int j=-1;
		
		boundverts = new HashMap<Point2i,ArrayList<Integer>>();
		
		boolean foundboundvert = false;
		for(int i=0; i<vertcount; i++){
			lab = (int)dat[i][dataoffset];

			/* is it a boundary voxel? */
			foundboundvert = false;
			j = 0;
			while(!foundboundvert && j<nbrtable[i].length){
				int nbrlab = (int)dat[nbrtable[i][j]][dataoffset];
				if(nbrlab!=lab){ //we've found a boundary vertex
					foundboundvert = true;
					
					Point2i bound;
					if(nbrlab<lab){
						bound = new Point2i(nbrlab,lab);
					}else{
						bound = new Point2i(lab,nbrlab);
					}
					
					if(boundaries.contains(bound)){
						if(!boundverts.containsKey(bound)){
							boundverts.put(bound, new ArrayList<Integer>());
							boundverts.get(bound).add(i);
						}else{
							boundverts.get(bound).add(i);
						}
					}else{
						System.out.println("jist.plugins"+"\t"+"Warning: Found boundary with no error probability data!");
					}
					numboundverts++;
				}else{
					j++;
				}
			}
		}
		
	}
	
	/**
	 * Calls genConfusionRater using this class's data
	 * @return the simulated rater surface
	 */
	public EmbeddedSurface genConfRaterSurface(){
		EmbeddedSurface out = genConfusionRater(truthsurf,confusion,dataoffset,labels);
		out.setName(outputname);
		ratersurf=out;
		return out;
	}
	
	/**
	 * Returns a simulated rater surface
	 * @return the simulated rater surface
	 */
	public EmbeddedSurface genPerimRaterSurface(){
		if(boundverts==null){
			computeBoundaryInformation();
		}
		//set boundErrProbCum
		if(boundErrProbCum==null){
			boundErrProbCum=new float[boundErrProb.length];
			boundErrProbCum[0]=boundErrProb[0];
			for(int i=1; i<boundErrProb.length; i++){
				boundErrProbCum[i]=boundErrProbCum[i-1]+boundErrProb[i];
			}
		}
		
		// init changed verts set
		changedverts = new HashSet<Integer>();
		
		//compute number of vertices that will need their labels changed
		int V = truthsurf.getVertexCount();
		
		double[][] rdat = deepClone(truthsurf.getVertexData());
		
		ratersurf = truthsurf.clone();
		
//		System.out.println("jist.plugins"+"\t"+"Vertex data: " + rdat);
		ratersurf.setVertexData(rdat);
		ratersurf.setName(outputname);
		
		//for debugging purposes
//		writeRaterSurface(new File("/home/john/Desktop/testSurfs"));
		
		
		
		int tochange = Math.round((1-totReliability)*V);
		int changed = 0 ;
		int changedprev = 0;
		int bound2perturb=-1;
		int vert2perturb=-1;
		
		int maxiters = 20*tochange;		//just to be safe
		int iters = 0;
		
		while(changed<tochange && iters<maxiters){
			
			/* DEBUG */
//			if(iters<10){
//				if(changedprev!=changed){
//					writeBoundaryVertexSurface(iters);
//					writeIntermediateRaterSurface(rdat,iters);
//				}
//			}
//			changedprev = changed;
//			
//			System.out.println("jist.plugins"+"\t"+"BOUNDARY");
//			System.out.println("jist.plugins"+"\t"+boundverts.get(boundaries.get(0)));
//			System.out.println("jist.plugins"+"\t"+"");
			
			/* END DEBUG */
			
			//choose which boundary will be perturbed
			int tries = 0;
			boolean found = false;
			while(tries<10 && !found){
				bound2perturb = drawFromCumulativeProbabilities(boundErrProbCum);

				//make sure the boundary has some vertices
				if(boundverts.get(boundaries.get(bound2perturb))!=null && boundverts.get(boundaries.get(bound2perturb)).size()>0){ 
					//pick which vertex on the boundary to perturb
					vert2perturb = boundverts.get(boundaries.get(bound2perturb)).get(drawUniform(boundverts.get(boundaries.get(bound2perturb)).size()));
					found = true;
				}
				tries++;
			}
			
//			System.out.println("jist.plugins"+"\t"+"Perturbing vertex: " + vert2perturb);
//			System.out.println("jist.plugins"+"\t"+"Is it on the boundary? " + boundverts.get(boundaries.get(bound2perturb)).contains(vert2perturb));
			
			//grow or shrink?
			
//			double grow = Math.random();
			double grow = rand.nextDouble();
			
			if(grow>growPerProb[bound2perturb]){ //GROW
				//"Grow" means we replace a vertex with the 1st component of the Point2i boundary
				if((int)rdat[vert2perturb][dataoffset]==boundaries.get(bound2perturb).x){
					changed+=perturbLabel(rdat, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).x);
				}else if((int)rdat[vert2perturb][dataoffset]==boundaries.get(bound2perturb).y){
					changed+=perturbLabel(rdat, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).x);
				}else{
					System.err.println("jist.plugins"+"Warning: vertex classified as wrong boundary");
				}
				
				
			}else{	//SHRINK
				//"Shrink" means we replace a vertex with the 2nd component of the Point2i boundary
				if((int)rdat[vert2perturb][dataoffset]==boundaries.get(bound2perturb).x){
					changed+=perturbLabel(rdat, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).y);
				}else if((int)rdat[vert2perturb][dataoffset]==boundaries.get(bound2perturb).y){
					changed+=perturbLabel(rdat, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).y);
				}else{
					System.err.println("jist.plugins"+"Warning: vertex classified as wrong boundary");
				}
			}
//			System.out.println("jist.plugins"+"\t"+"Changed " + changed + " of " + tochange);
			iters++;
		}

		
		return ratersurf;
	}
	

	/**
	 * 
	 * @param vertex - index of the vertex to perturb.
	 * @param newlabel - the label that needs to take over a vertex
	 * @return 0 if nothing was changed, 1 if a vertex had its labeld changed
	 */
	private int perturbLabel(double[][] rdat, int vertex, Point2i boundary, int newlabel){
		if((int)rdat[vertex][dataoffset]==newlabel){  //we have to choose a neighbor
			
			int labeltochange = Integer.MIN_VALUE;
			if(boundary.x==newlabel){
				labeltochange=boundary.y;
			}else{
				labeltochange=boundary.x;
			}
			//a vertex that we change has to have the label 'labeltochange'
			
			//make sure that a vertex with the other label exists...
			boolean doesexist = false;
			for(int i=0; i<nbrtable[vertex].length; i++){
				if((int)rdat[nbrtable[vertex][i]][dataoffset]==labeltochange){
					doesexist=true;
					break;
				}
			}
			
			if(doesexist){
				int nbr = -1;
				boolean found = false;
				//choose a random neighbor until we get one of the 'other' label
				while(!found){
					nbr = drawUniform(nbrtable[vertex].length);
					if((int)rdat[nbrtable[vertex][nbr]][dataoffset]==labeltochange){
						found=true;
						break;
					}
				}
				
				//have we changed this vertex label before?
				if(!changedverts.contains(nbrtable[vertex][nbr])){ 
					//update the vertex data
					//System.out.println("jist.plugins"+"\t"+"Changing label at: " + nbrtable[vertex][nbr] + " from " + rdat[nbrtable[vertex][nbr]][dataoffset]);
					rdat[nbrtable[vertex][nbr]][dataoffset]=newlabel;
					changedverts.add(nbrtable[vertex][nbr]);
					updateBoundaryList(rdat, nbrtable[vertex][nbr], boundary);
					return 1;
				}
				
			}
			
		}else{	//we can change this vertex's label
			// have we changed this vertex before?
			// if not, change it
			if(!changedverts.contains(vertex)){ 
//				System.out.println("jist.plugins"+"\t"+"Changing label at: " + vertex);
				rdat[vertex][dataoffset]= newlabel;
				changedverts.add(vertex);
				
				updateBoundaryList(rdat, vertex, boundary);
				return 1;
			}
		}
		
		return 0;
	}
	

	
	/**
	 * 
	 * @param rdat - data
	 * @param changedvert - index of the vertex that changed
	 * @param boundary - boundary that was affected
	 */
	private void updateBoundaryList(double[][] rdat, int changedvert, Point2i boundary){
		//is the changed point part of the boundary?
		//check to see if all of its neighbors are still part of it.
		ArrayList<Point2i> vertbounds = boundariesPresent(rdat,changedvert);
//		System.out.println("jist.plugins"+"\t"+"boundaries at :" +changedvert);
//		System.out.println("jist.plugins"+"\t"+vertbounds);
		if(vertbounds==null){ 
			//this point is not a boundary at all anymore  - remove it 
			if(boundverts.get(boundary).contains(changedvert)){
//				System.out.println("jist.plugins"+"\t"+"Removed " + changedvert + " from boundary");
				boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(changedvert));
			}
		}else if(!vertbounds.contains(boundary)){ 
			//this point is not a part of this boundary - remove it 
			if(boundverts.get(boundary).contains(changedvert)){
//				System.out.println("jist.plugins"+"\t"+"Removed " + changedvert + " from boundary");
				boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(changedvert));
			}
		}
		
		//check to see if all its neighbors are boundaries
		for(int i=0; i<nbrtable[changedvert].length; i++){
			vertbounds = boundariesPresent(rdat,nbrtable[changedvert][i]);
//			System.out.println("jist.plugins"+"\t"+"boundaries at :" +nbrtable[changedvert][i]);
//			System.out.println("jist.plugins"+"\t"+vertbounds);
			if(vertbounds==null){ 
				//this nbr is not part of this boundary - remove it if it's there
				if(boundverts.get(boundary).contains(nbrtable[changedvert][i])){
//					System.out.println("jist.plugins"+"\t"+"Removed " + nbrtable[changedvert][i] + " from boundary");
					boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(nbrtable[changedvert][i]));
				}
			}else{
				if(!vertbounds.contains(boundary)){ 
					//this nbr is not a part of this boundary - remove it if it's there
					if(boundverts.get(boundary).contains(nbrtable[changedvert][i])){
//						System.out.println("jist.plugins"+"\t"+"Removed " + nbrtable[changedvert][i] + " from boundary");
						boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(nbrtable[changedvert][i]));
					}
				}else{
					//this point IS part of the boundary - add it if not already present
					if(!boundverts.get(boundary).contains(nbrtable[changedvert][i])){
//						System.out.println("jist.plugins"+"\t"+"Added " + nbrtable[changedvert][i] + " to boundary");
						boundverts.get(boundary).add(nbrtable[changedvert][i]);
					}
				}
			}
		}
		boundverts.get(boundary).trimToSize();
	} //Whew!
	
	/**
	 * Computes the boundaries present at the vertex with index vert
	 * @param vert The vertex's index
	 * @return A list of all the boundaries present at that vertex.  Null if the point is
	 * not a boundary point
	 */
	private ArrayList<Point2i> boundariesPresent(double[][] dat, int vert){
		/* is it a boundary voxel? */
		boolean foundboundvert = false;
		int j = 0;
		int lab = (int)dat[vert][dataoffset];
		ArrayList<Point2i> out = null;
		while(!foundboundvert && j<nbrtable[vert].length){
			int nbrlab = (int)dat[nbrtable[vert][j]][dataoffset];
			if(nbrlab!=lab){ //we've found a boundary vertex
				foundboundvert = true;
				out = new ArrayList<Point2i>();
				Point2i bound;
				if(nbrlab<lab){
					bound = new Point2i(nbrlab,lab);
				}else{
					bound = new Point2i(lab,nbrlab);
				}
				
				if(!out.contains(bound)){
					out.add(bound);
				}
			}else{
				j++;
			}
		}
		return out;
	}
	
	/**
	 * 
	 * @param probs - Cumulative probabilites for drawing a particular index
	 * @return the index that was chosen
	 */
	private int drawFromCumulativeProbabilities(float[] probs){
//		double r = Math.random();
		double r = rand.nextDouble();
		int i=0;
		if(r<probs[0]){
			return i;
		}
		for(i=1; i<probs.length; i++){
			if(r>=probs[i-1] && r<probs[i]){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Choose a random integer from 0 to N-1, inclusive
	 * @param probs - number of
	 * @return the index that was chosen
	 */
	private int drawUniform(int N){
//		double r = Math.random();
		double r = rand.nextDouble();
		int i=0;
		float step = 1f/(float)N;
		if(r<step){
			return i;
		}
		for(i=1; i<N; i++){
			if(r>=step*(i) && r<step*(i+1)){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Find all boundaries that exist on a surface.  A boundary is represented by an integer pair. {i,j}=={j,i} 
	 * @param surf The labeled surface
	 * @param offset The data offset
	 * @return List of boundaries
	 */
	public static ArrayList<Point2i> findBoundaries(EmbeddedSurface surf, int offset){
		int[][] nbrTable = EmbeddedSurface.buildNeighborVertexVertexTable(surf, EmbeddedSurface.Direction.COUNTER_CLOCKWISE);
		
		int numboundverts = 0;
		int vertcount = surf.getVertexCount();
		int lab=-1;
		int j=-1;
		
		double[][] dat = surf.getVertexData();
		ArrayList<Point2i> boundaries = new ArrayList<Point2i>();
		
		boolean foundboundvert = false;
		for(int i=0; i<vertcount; i++){
			lab = (int)dat[i][offset];

			/* is it a boundary voxel? */
			foundboundvert = false;
			j = 0;
			while(!foundboundvert && j<nbrTable[i].length){
				int nbrlab = (int)dat[nbrTable[i][j]][offset];
				if(nbrlab!=lab){
					foundboundvert = true;
					
					Point2i bound;
					if(nbrlab<lab){
						bound = new Point2i(nbrlab,lab);
					}else{
						bound = new Point2i(lab,nbrlab);
					}
					if(!boundaries.contains(bound)){
						boundaries.add(bound);
					}
					numboundverts++;
				}else{
					j++;
				}
			}
		}
		return boundaries;
	}
	
	
	/**
	 * Returns a rater surface by modifying boundary vertex labels
	 * until 
	 * @param surf 'true' labeled surface
	 * @param perimparam parameter controlling 
	 * @param offset data offset
	 * @return the rater surface
	 */
	public static EmbeddedSurface genPerimRaterOld(EmbeddedSurface surf,float[] perimparam, int[] labels,  float totreliability, int offset){
		System.out.println("jist.plugins"+"\t"+"Generating rater with perim errors.");
		System.out.println("jist.plugins"+"\t"+"Data offset: " + offset);
		EmbeddedSurface ratersurf = surf.clone();
		
		double[][] rdata = ratersurf.getVertexData();
		
		int vertcount = ratersurf.getVertexCount();
		
		int[][] nbrTable = EmbeddedSurface.buildNeighborVertexVertexTable(surf, EmbeddedSurface.Direction.COUNTER_CLOCKWISE);
		System.out.println("jist.plugins"+"\t"+"nbr table: " + nbrTable.length + " " + nbrTable[0].length);
		
		int N = labels.length;
		int tochange = Math.round((1-totreliability)*surf.getVertexCount());
		int changed = 0 ;
		//generate map
		HashMap<Integer,Integer> labeltoind = new HashMap<Integer,Integer>();
		for(int i=0; i<N; i++){
			labeltoind.put(labels[i], i);
		}
		
		ArrayList<Integer> labelstochange = new ArrayList<Integer>();
		for(int i=0; i<N; i++){
			System.out.println("jist.plugins"+"\t"+"Perimparam for label: " + labels[i] + " is " + perimparam[i]);
			if(perimparam[i]!=0){
				labelstochange.add(labels[i]);
			}
		}
		System.out.println("jist.plugins"+"\t"+labeltoind);
		
		/*variable  for the loops to come */
		int maxiters = 50*tochange;
		int iters = 0;
		
		int maxiniters = 500;
		int initers = 0;
		
		int lab = -1;
		double label = -1;
		int vert = -1;
		int j = 0;
		int numboundverts = 0;
		int percentdone = 0;
		
		/*
		 * METHODOLOGY:
		 * FIRST FIND ALL BOUNDARY VOXELS ON THE SURFACE
		 * 
		 * INNER LOOP:
		 * RANDOMLY CHOOSE AMONG THESE AND CHANGE THOSE THAT SHOULD
		 * BE CHANGED ACCORDING TO THE PERIMETER ERROR PARAMETER
		 * 
		 * SINCE THIS PROCESS CHANGES BOUNDARY VOXELS WE ITERATE THE 
		 * ABOVE TWO STEPS IN AN OUTER LOOP
		 */
		
		
		/* OUTER MOST LOOP */
		while(changed < tochange && iters<maxiters){
//		while(changed < tochange ){
			/* FIND BOUNDARY VERTICES FOR LABELS WE ARE ALLOWED TO CHANGE*/
			numboundverts = 0;
			ArrayList<Integer> boundverts = new ArrayList<Integer>();
			boolean foundboundvert = false;
			for(int i=0; i<vertcount; i++){
				Double labd = new Double(rdata[i][offset]);
				lab = labd.intValue();
				
				/* Are we allowed to change this label boundary? */
				if(labelstochange.contains(lab)){
					
					/* is it a boundary voxel? */
					foundboundvert = false;
					j = 0;
					while(!foundboundvert && j<nbrTable[i].length){
						if(rdata[nbrTable[i][j]][offset]!=lab){
							foundboundvert = true;
							boundverts.add(i);
							numboundverts++;
						}else{
							j++;
						}
					}
				}
			}

			
			System.out.println("jist.plugins"+"\t"+"Boundary vertices : " + numboundverts);
			/* INNER LOOP - PICK A RANDOM BOUNDARY VERTEX */

			maxiniters = numboundverts;
			initers =0;

			/* INNER LOOP SEARCHES BOUNDARY VOXELS FOUND HERE */
			while(changed<tochange && initers<maxiniters){
				
				//get the vertex index and its label
				vert = boundverts.get((int)Math.round(Math.random()*(numboundverts-1)));
				label = rdata[vert][offset];

//				System.out.println("jist.plugins"+"\t"+"vert: "+vert);
				
				/* FIND A NEIGHBOR LABEL FOR THE CURRENT VERTEX 
				 * Loop over vertex neighbors to find 
				 * which neighbor has another label*/
				j = 0;
				foundboundvert = false;
				while(!foundboundvert && j<nbrTable[vert].length){
					if(rdata[nbrTable[vert][j]][offset]!=label){
//						System.out.println("jist.plugins"+"\t"+"This label is: " + label);
//						System.out.println("jist.plugins"+"\t"+"Nbr  label is: " + data[nbrTable[vert][j]][offset]);
						foundboundvert = true;
					}else{
						j++;
					}
				}

				/* CHANGE IT IF WE ARE ALLOWED */
				if(foundboundvert && labeltoind.containsKey((int)label)){
					float perimerr = perimparam[labeltoind.get((int)label)];
					if(perimerr<0){  //shrink the current label
						rdata[vert][offset]=rdata[nbrTable[vert][j]][offset];
						changed++;
//						System.out.println("jist.plugins"+"\t"+"Changed another boundary point: " + changed + " of " + tochange);
					}else if(perimerr>0){ //expand the current label
						rdata[nbrTable[vert][j]][offset]=label;
						changed++;
//						System.out.println("jist.plugins"+"\t"+"Changed another boundary point: " + changed + " of " + tochange);
					}//else do nothing
				}else{
					System.out.println("jist.plugins"+"\t"+"UH-OH! No key: " + (int)label);
				}
				initers++;
				
				
			}
			iters++;
			percentdone = (int)(100*((float)changed)/tochange);
			System.out.println("jist.plugins"+"\t"+"Finished " + percentdone + "%");
		}
		
		
		ratersurf.setVertexData(rdata);
		return ratersurf;
	}
	
	/**
	 * Returns a rater surface by passing the label at each 
	 * vertex through a confusion matrix.
	 * 
	 * @param surf 'true' labeled surface
	 * @param confusion confusion matrix
	 * @param offset offset of label data
	 * @return the same surface with the rater data
	 */
	public static EmbeddedSurface genConfusionRater(EmbeddedSurface surf, ConfusionMatrix confusion, int offset, int[] labels){
		System.out.println("jist.plugins"+"\t"+"Generating rater with confusion matrix:");
		
		/* Organize Data */
		double[][] dat = surf.getVertexData();
		EmbeddedSurface ratersurf = surf.clone();
		double[][] rdat = ratersurf.getVertexData();
		
		/* Generate rater data based on confusion matrix */
		confusion.printConfusion();
		for(int i=0; i<surf.getVertexCount(); i++){
			rdat[i][offset] = confusion.genNextLabel((int)dat[i][offset]);
		}
		ratersurf.setVertexData(rdat);
		System.out.println("jist.plugins"+"\t"+"Finished");
		return ratersurf;
	}
	
	/**
	 * Gets a list of all of the labels (stored as an int array)
	 * that are present on an input surface
	 * 
	 * @param surf labeled surface
	 * @param offset offset of label data
	 * @return
	 */
	public static int[] getLabelList(EmbeddedSurface surf, int offset){
		ArrayList<Double> list = new ArrayList<Double>();
		double[][] dat = surf.getVertexData();
		for(int i=0; i<surf.getVertexCount(); i++){
			if(!list.contains(dat[i][offset])){
				list.add(dat[i][offset]);
			}
		}
		list.trimToSize();
		int[] out = new int[list.size()];
		Iterator<Double> it = list.iterator();
		int j=0;
		while(it.hasNext()){
			out[j]=it.next().intValue();
			j++;
		}
		return out;
	}
	
	/**
	 * for debuggins purposes only
	 */
	private void writeBoundaryVertexSurface(int iteration){
		EmbeddedSurface boundarysurf = truthsurf.clone();
		double[][] bounddat = new double[truthsurf.getVertexCount()][1];
		Iterator<Integer> it = boundverts.get(boundaries.get(0)).iterator();
		while(it.hasNext()){
			int bvert = it.next();
			bounddat[bvert][0]=1;
		}
		boundarysurf.setVertexData(bounddat);
		String name = "BoundarySurface"+Integer.toString(iteration)+".vtk";
		SurfaceVtkReaderWriter.getInstance().write(boundarysurf, new File("/home/john/Desktop/testSurfs/"+name));
	}
	private File writeRaterSurface(double[][] data, File f){
		EmbeddedSurface intrater = truthsurf.clone();
		intrater.setVertexData(data);
		return SurfaceVtkReaderWriter.getInstance().write(intrater, f);
	}
	public File writeRaterSurface(File f){
		return SurfaceVtkReaderWriter.getInstance().write(ratersurf, f);
	}
	private void printNeighbors(int vertex){
		System.out.println("jist.plugins"+"\t"+"Neighbors of : " + vertex);
		for(int i=0; i<nbrtable[vertex].length; i++){
			System.out.print(nbrtable[vertex][i] + " ");
		}
		System.out.print("\n");
	}
	private void printNeighborLabels(int vertex, double[][] dat){
		System.out.println("jist.plugins"+"\t"+"Neighbors of : " + vertex);
		for(int i=0; i<nbrtable[vertex].length; i++){
			System.out.print(dat[nbrtable[vertex][i]][dataoffset] + " ");
		}
		System.out.print("\n");
	}
	
	private double[][] deepClone(double[][] a){
		double[][] b = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++){
			for(int j=0; j<a[0].length; j++){
				b[i][j]=a[i][j];
			}
		}
		return b;
	}

	
}
