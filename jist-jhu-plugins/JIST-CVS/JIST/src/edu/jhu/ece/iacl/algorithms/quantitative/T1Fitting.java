package edu.jhu.ece.iacl.algorithms.quantitative;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.bme.smile.commons.optimize.FunctionNumericNDDifferentiation;
import edu.jhu.bme.smile.commons.optimize.LevenbergMarquardt;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuousDifferentiable;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

public class T1Fitting {
	public double[] T1Fit(double []x, double []y, double TR, String nrparams){

		//initial guesses
		double T1 = 1200;
		double I_0 = StatisticsDouble.max(y);
		double k = -1;

		double [] FitResult = new double[4];

		if(nrparams.contentEquals("Two Parameters")){
			T1Fit_2par fun2 = new T1Fit_2par(x, y, TR, I_0, T1);
			LevenbergMarquardt opt2 = new LevenbergMarquardt();
			opt2.setGamma(1e-3, 1e7);
			opt2.initialize(fun2);
			opt2.optimize(true);
			JistLogger.logOutput(2, "T1Fitting - 2 Param fit: Fit finished after "+ opt2.getIterations()+" iterations.");
			for (int i=0; i<opt2.getExtrema().length; i++){
				JistLogger.logOutput(2, "T1Fitting - 2 Param fit: Extrema["+i+"]=" + opt2.getExtrema()[i]);
				// Check if results are sensible, otherwise force to nearest boundary.
				if ((opt2.getExtrema()[i]> fun2.getDomainMin()[i])&& (opt2.getExtrema()[i]<fun2.getDomainMax()[i])){
					FitResult[i] =  opt2.getExtrema()[i];
				}
				else if ((opt2.getExtrema()[i] <= fun2.getDomainMin()[i])){ // too small
					FitResult[i] =  fun2.getDomainMin()[i];
				}
				else if ((opt2.getExtrema()[i] >= fun2.getDomainMax()[i])){ // too large
					FitResult[i] =  fun2.getDomainMax()[i];
				}
			}
			JistLogger.logOutput(2, "T1Fitting - 2 Param fit: sum of squares: "+ fun2.getValue(opt2.getExtrema()));
			
			FitResult[2] = -1; //k is always -1 for 2 param fit
			FitResult[3] = fun2.getValue(opt2.getExtrema());
		//get fitted curve
//				double[] y_fit;
//				y_fit = fun2.getFittedCurve(opt2.getExtrema());
		}


		if(nrparams.contentEquals("Three Parameters")){
			//initial guesses: take results from 2 param fit
//			I_0 = FitResult[0];
//			T1 = FitResult[1];

			T1Fit_3par fun3 = new T1Fit_3par(x, y, TR, I_0, T1, k);
			//OptimizerNDContinuousDifferentiable opt = new LevenbergMarquardt();
			LevenbergMarquardt opt3 = new LevenbergMarquardt();
			opt3.setGamma(1e-3, 1e7);
			opt3.initialize(fun3);		
			opt3.optimize(true);


			//log extrema found in 3par fit and save results in FitResult[]
			//Save results; returns an array, with fields: [I_0, T1, ssqErr]
			// in case of 3 parameter fit, those results are also added, giving:
			// FitResult[I_0,T1,ssqErr,I_0,T1,k,ssqErr]
			JistLogger.logOutput(2, "T1Fitting - 3 Param fit: Fit finished after "+ opt3.getIterations()+" iterations.");
			for (int i=0; i<opt3.getExtrema().length; i++){
				JistLogger.logOutput(4, "T1Fitting - 3 Param fit: Extrema["+i+"]=" + opt3.getExtrema()[i]);
				// Check if results are sensible, otherwise force to nearest boundary.
				if ((opt3.getExtrema()[i]> fun3.getDomainMin()[i])&& (opt3.getExtrema()[i]<fun3.getDomainMax()[i])){
					FitResult[i] =  opt3.getExtrema()[i];
				}
				else if ((opt3.getExtrema()[i] <= fun3.getDomainMin()[i])){ // too small
					FitResult[i] =  fun3.getDomainMin()[i];
				}
				else if ((opt3.getExtrema()[i] >= fun3.getDomainMax()[i])){ // too large
					FitResult[i] =  fun3.getDomainMax()[i];
				}
			}
			JistLogger.logOutput(2, "T1Fitting - 3 Param fit: Chi-squared: "+ fun3.getValue(opt3.getExtrema()));
			FitResult[3] = fun3.getValue(opt3.getExtrema());

		}
		
		return FitResult;
		
	}

		public class T1Fit_2par implements OptimizableNDContinuousDifferentiable
		{
			//Function to be fitted:
			//I=I0*(1.+(k-1.)*EXP(-TI/T1)-k*EXP(-TR/T1))
			//y=I0*(1.+(-2.)*EXP(-x/T1)+1*EXP(-TR/T1))

			//with I is the []Y of intensities as a function of TI ([]X)
			//I0 is the amplitude of the fitted signal
			// k is the inversion rate, assumed -1 (cos(180)) for a complete inversion in the 2 param fit
			// k is fitted in the three param fit
			// TR is the repetition time

			double []x; 
			double []y;
			//		double []max;
			//		double []min;
			double []max = new double [2];
			double []min = new double [2];
			double TR, T1, I_0,tol, k;
			int dim = 2;

			public T1Fit_2par(double []x, double []y, double TR, double I_0, double T1) {

				this.x = x;
				this.y = y;
				this.TR = TR;

				min[0] = 0.0;
				max[0] = StatisticsDouble.max(y)*2; //assuming that I_0 will be near to the maximum y value
				//max[0] = Double.MAX_VALUE;
				min[1] = 0.0;
				max[1] = StatisticsDouble.max(x); // do not allow T1 longer than the latest timepoint.

				tol = 1e-4;
			}

			@Override
			public double[] getDomainMax() {
				return max;
			}

			@Override
			public double[] getDomainMin() {
				return min;
			}

			@Override
			public double getDomainTolerance() {
				return tol;
			}

			@Override
			public double getValue(double []c) {
				double err = 0, e;

				//update parameters from optimizer
				I_0 = c[0];
				T1 = c[1];

				int cnt =0;
				for(int i=0;i<x.length;i++) {
					//y=I0*(1.+(-2.)*EXP(-x/T1)+1*EXP(-TR/T1))
					double v=I_0*(1 -2*Math.exp(-x[i]/T1)+ Math.exp(-TR/T1));
					e = y[i]-v;
					err+=e*e;
					cnt++;
				}
				//			if(cnt<x.length/2) {
				//				return Double.MAX_VALUE;
				//			}
				err= err/cnt;
				//			System.out.println(err+" "+c);
				if(Double.isNaN(err))
					return Double.MAX_VALUE;
				else 
					return err;
			}
			public double[] getFittedCurve(double []c){
				double[] v = new double [x.length];
				I_0 = c[0];
				T1 = c[1];
				for (int i=0; i<x.length; i++){
					v[i]=I_0*(1.+(-2.)*Math.exp(-x[i]/T1)+1*Math.exp(-TR/T1));
				}
				return v;
			}
			/**
			 * Gets the hessian of the function.
			 * Uses the default step size and method
			 * @param x location to calculate hessian
			 * @return hessian at location x
			 */
			public double[][] get2ndDerivative(double[] c) {
				/* Using Mathematica to calculate the derivatives analytically using:
				 *  y = data
				 *  v = S[t_, I0_, TR_, T1_, k_] :=  I0 (1 + (k - 1) \[ExponentialE]^(-t/T1) - k \[ExponentialE]^(-TR/T1))
				 *  f = (y - v)^2
				 *  
				 *  gives 2nd derivative or Hessian :
				 *  [][] = D[f[T1, I0, k], {{T1, I0}, 2}] = 
				 *  {{2 I0^2 ((exp(-(t/T1)) (-1 + k) t)/T1^2 - (exp(-(TR/T1)) k TR)/T1^2)^2 - 2 I0 ((exp(-(t/T1)) (-1 + k) t^2)/T1^4 - (2 exp(-(t/T1)) (-1 + k) t)/T1^3 + (2 exp(-(TR/T1)) k TR)/T1^3 - (exp(-(TR/T1)) k TR^2)/T1^4) (-I0 (1 + exp(-(t/T1)) (-1 + k) - exp(-(TR/T1)) k) + y),
				 *    2 I0 (1 + exp(-(t/T1)) (-1 + k) - exp(-( TR/T1)) k) ((exp(-(t/T1)) (-1 + k) t)/ T1^2 - (exp(-(TR/T1)) k TR)/T1^2) - 2 ((exp(-(t/T1)) (-1 + k) t)/ T1^2 - (exp(-(TR/T1)) k TR)/ T1^2) (-I0 (1 + exp(-(t/ T1)) (-1 + k) - exp(-(TR/T1)) k) + y)},
				 *   {2 I0 (1 + exp(-(t/ T1)) (-1 + k) - exp(-(TR/T1)) k) ((exp(-(t/T1)) (-1 + k) t)/ T1^2 - (exp(-(TR/T1)) k TR)/T1^2) - 2 ((exp(-(t/T1)) (-1 + k) t)/ T1^2 - (exp(-(TR/T1)) k TR)/ T1^2) (-I0 (1 + exp(-(t/ T1)) (-1 + k) - exp(-(TR/T1)) k) + y),
				 *    2 (1 + exp(-(t/T1)) (-1 + k) - exp(-(TR/ T1)) k)^2}}
				 *  */
				//double [][] secderv_orig =(new FunctionNumericNDDifferentiation(this)).get2ndDerivative(c);
				double [][] secderv = new double[2][2];
	
				I_0 = c[0];
				T1 = c[1];
				k = -1.0;
				
				for(int i=0;i<x.length;i++) {
					secderv[0][0] += 2*Math.pow((1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/ T1))*k),2);
					double v1 = 2*I_0*(1+Math.exp(-(x[i]/T1))*(-1 + k)-Math.exp(-(TR/T1))*k)
								*((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/ Math.pow(T1, 2) - (Math.exp(-(TR/T1))*k*TR)/Math.pow(T1,2)) 
								-2*((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/ Math.pow(T1,2) - (Math.exp(-(TR/T1))*k*TR)/ Math.pow(T1,2))*
								(-I_0*(1 +Math.exp(-(x[i]/ T1))*(-1 + k) -Math.exp(-(TR/T1))*k) + y[i]);
					secderv[0][1] += v1;
					secderv[1][0] += v1;
					secderv[1][1] += 2*Math.pow(I_0,2)*Math.pow((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/Math.pow(T1,2) - (Math.exp(-(TR/T1))*k*TR)/Math.pow(T1,2),2) 
										-2*I_0*((Math.exp(-(x[i]/T1))*(-1 + k)*Math.pow(x[i],2))/Math.pow(T1,4) - (2*Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/Math.pow(T1, 3)+ (2*Math.exp(-(TR/T1))*k*TR)/Math.pow(T1, 3) - (Math.exp(-(TR/T1))*k*Math.pow(TR,2))/Math.pow(T1, 4)) 
										*(-I_0*(1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/T1))*k) + y[i]);
				}
				secderv [0][0] /= x.length;
				secderv [0][1] /= x.length;
				secderv [1][0] /= x.length;
				secderv [1][1] /= x.length;
								
				return secderv;
				//return secderv_orig;   
			}

			/**
			 * Gets the hessian of the function.
			 * Uses the specified step size and method.
			 * @param x location to calculate hessian
			 * @param step step size
			 * @param method method
			 * @return hessian at location x
			 */
			public double[][] get2ndDerivative(double[] x, double step, int method) {
				return (new FunctionNumericNDDifferentiation(this, step, method)).get2ndDerivative(x);  
			}

			/**
			 * Gets the 2nd derivative of the function in a specific direction.
			 * Uses the default step size and method.
			 * @param x location to calculate derivative
			 * @param dim1 dimension to take the first derivative
			 * @param dim2 dimension to take the second derivative
			 * @return 2nd derivative at location x in the specified direction
			 */
			public double get2ndDerivative(double[] x, int dim1, int dim2) {
				//f'T = -((2*I0*TI^2-4*I0*T*TI)*exp(TR/T)-I0*exp(TI/T)*TR^2+2*I0*T*exp(TI/T)*TR)*exp-((TR+TI)/T)/T^4
				return (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(x, dim1, dim2);  
			}

			/**
			 * Gets the gradient of the function.
			 * Uses the default step size and method
			 * @param x location to calculate gradient
			 * @return gradient at location x
			 */
			public double[] getDerivative(double[] c) {
				/* Using Mathematica to calculate the derivatives analytically using:
				 *  y = data
				 *  v = S[t_, I0_, TR_, T1_, k_] :=  I0 (1 + (k - 1) exp(-t/T1) - k exp(-TR/T1))
				 *  f = (y - v)^2
				 *  
				 *  gives 1st derivative or gradient :
				 *  [df/dI0,df/dT1] = D[f[T1, I0, k], {{T1, I0}}]
				 *  {-2 I0 ((exp(-(t/T1)) (-1 + k) t)/T1^2	- (exp(-(TR/T1)) k TR)/T1^2) (-I0 (1 +exp(-(t/T1)) (-1 + k)- exp(-(TR/T1)) k) + y),
				 *   -2 (1 + exp(-(t/T1)) (-1 + k) - exp(-(TR/T1)) k) (-I0 (1 + exp(-(t/T1)) (-1 + k) - exp(-(TR/T1)) k) + y)}
				 *  
				 */
				//	
				//double [] derv_orig =(new FunctionNumericNDDifferentiation(this)).getDerivative(c); 
				double [] derv = new double[2];

				I_0 = c[0];
				T1 = c[1];
				int k = -1;
				for(int i=0;i<x.length;i++) {
					derv[1] += -2*I_0*((Math.exp(-(x[i]/T1))*(-1 + k)* x[i])/Math.pow(T1,2)	- (Math.exp(-(TR/T1))*k*TR)/Math.pow(T1,2))
								*(-I_0*(1 +Math.exp(-(x[i]/T1))*(-1 + k)- Math.exp(-(TR/T1))*k) + y[i]);
					derv[0] += -2*(1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/T1))*k)
								*(-I_0*(1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/T1))*k) + y[i]);
				}

				derv [0] = derv [0]/x.length;
				derv [1] = derv [1]/x.length;
				return derv;
				//return derv_orig;
			}

			/**
			 * Gets the gradient of the function.
			 * Uses the specified step size and method.
			 * @param x location to calculate gradient
			 * @param step step size
			 * @param method method
			 * @return gradient at location x
			 */
			public double[] getDerivative(double[] x, double step, int method) {
				return (new FunctionNumericNDDifferentiation(this, step, method)).getDerivative(x);  
			}

			@Override
			public int getNumberOfDimensions() {
				// TODO Auto-generated method stub
				return dim;
			}			
		}


		public class T1Fit_3par implements OptimizableNDContinuousDifferentiable
		{
			//Function to be fitted:
			//I=I0*(1.+(k-1.)*EXP(-TI/T1)-k*EXP(-TR/T1))
			//y=I0*(1.+(-2.)*EXP(-x/T1)+1*EXP(-TR/T1))

			//with I is the []Y of intensities as a function of TI ([]X)
			//I0 is the amplitude of the fitted signal
			// k is the inversion rate, assumed -1 (cos(180)) for a complete inversion in the 2 param fit
			// k is fitted in the three param fit
			// TR is the repetition time

			double []x; 
			double []y;
			double []max = new double [3];
			double []min = new double [3];
			double TR, tol;
			double T1, I_0, k;
			int dim = 3;

			public T1Fit_3par(double []x, double []y, double TR, double I_0, double T1, double k) {
				this.x = x;
				this.y = y;
				this.TR = TR;
				
				min[0] = 0.0;
				max[0] = StatisticsDouble.max(y)*2; //assuming that I_0 will be near to the maximum y value
				min[1] = 0.0;
				max[1] = StatisticsDouble.max(x); // do not allow T1 longer than the latest timepoint.
				min[2] = -1.0;
				max[2] = 1.0;

				tol = 1e-4;
			}

			@Override
			public double[] getDomainMax() {
				return max;
			}

			@Override
			public double[] getDomainMin() {
				return min;
			}

			@Override
			public double getDomainTolerance() {
				return tol;
			}

			@Override
			public double getValue(double []c) {
				double err = 0, e;
				int cnt =0;
				for(int i=0;i<x.length;i++) {
					/* Get function value from I=I0*(1.+(k-1.)*EXP(-TI/T1)-k*EXP(-TR/T1))
					 * update parameters from optimizer
					 * c[0] = I_0;
					 * c[1] = T1;
					 * c[2] = k;*/

					double v=c[0]*(1.+(c[2]-1.)*Math.exp(-x[i]/c[1])-c[2]*Math.exp(-TR/c[1]));
					e = y[i]-v;
					err+=e*e;
					cnt++;
				}

				err= err/cnt;
				if(Double.isNaN(err))
					return Double.MAX_VALUE;
				else 
					return err;
			}

			public double[] getFittedCurve(double []c){
				double[] v = new double [x.length];
				/* Get function value from I=I0*(1.+(k-1.)*EXP(-TI/T1)-k*EXP(-TR/T1))
				 * update parameters from optimizer
				 * c[0] = I_0;
				 * c[1] = T1;
				 * c[2] = k;*/
				for (int i=0; i<x.length; i++){
					v[i]=c[0]*(1.+(c[2]-1.)*Math.exp(-x[i]/c[1])-c[2]*Math.exp(-TR/c[1]));
				}
				return v;
			}
			/**
			 * Gets the hessian of the function.
			 * Uses the default step size and method
			 * @param x location to calculate hessian
			 * @return hessian at location x
			 */
			public double[][] get2ndDerivative(double[] c) {
				/* Using Mathematica to calculate the derivatives analytically using:
				 *  y = data
				 *  v = S[t_, I0_, TR_, T1_, k_] :=  I0 (1 + (k - 1) \[ExponentialE]^(-t/T1) - k \[ExponentialE]^(-TR/T1))
				 *  f = (y - v)^2
				 *  
				 *  gives 2nd derivative or Hessian :
				 *  D[f[T1, I0, k], {{I0, T1, k}, 2}]:
				 *  { II TI kI;
				 *    IT TT kT;		with IT=TI, Ik=kI and Tk=kT
				 *    Ik Tk kk }
				 *    
			II	 *[0][0] {{	2 (1 + \[ExponentialE]^(-(t/T1)) (-1 + k) - \[ExponentialE]^(-(TR/T1)) k)^2,
			a	 *[0][1]	2 I0 (1 + Math.exp(-(t/T1)) (-1 + k) - Math.exp(-(TR/T1)) k) ((Math.exp(-(t/T1)) (-1 + k) t)/T1^2 - (Math.exp(-(TR/T1)) k TR)/T1^2) - 2 ((Math.exp(-(t/T1)) (-1 + k) t)/  T1^2 - (Math.exp(-(TR/T1)) k TR)/ T1^2) (-I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y),
			b	 *[0][2]	2 (Math.exp(-(t/T1)) - Math.exp(-(TR/ T1))) I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) - 2 (Math.exp(-(t/T1)) - Math.exp(-(TR/ T1))) (-I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y)},
			a	 *[1][0]	2 I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) ((Math.exp(-(t/T1)) (-1 + k) t)/ T1^2 - (Math.exp(-(TR/T1)) k TR)/T1^2) - 2 ((Math.exp(-(t/T1)) (-1 + k) t)/ T1^2 - (Math.exp(-(TR/T1)) k TR)/ T1^2) (-I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y),
			TT	 *[1][1]	2 I0^2 ((Math.exp(-(t/T1)) (-1 + k) t)/ T1^2 - (Math.exp(-(TR/T1)) k TR)/T1^2)^2 - 2 I0 ((Math.exp(-(t/T1)) (-1 + k) t^2)/T1^4 - ( 2 Math.exp(-(t/T1)) (-1 + k) t)/T1^3 + ( 2 Math.exp(-(TR/T1)) k TR)/T1^3 - (Math.exp(-(TR/T1)) k TR^2)/ T1^4) (-I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y),
			c	 *[1][2]	2 (Math.exp(-(t/T1)) - Math.exp(-(TR/T1))) I0^2 ((Math.exp(-(t/T1)) (-1 + k) t)/T1^2 - (Math.exp(-(TR/T1)) k TR)/T1^2) - 2 I0 ((Math.exp(-(t/T1)) t)/ T1^2 - (Math.exp(-(TR/T1)) TR)/T1^2) (-I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y)},
			b	 *[2][0]	2 (Math.exp(-(t/T1)) - Math.exp(-(TR/  T1))) I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) - 2 (Math.exp(-(t/T1)) - Math.exp(-(TR/ T1))) (-I0 (1 + Math.exp(-(t/T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y),
			c	 *[2][1]	2 (Math.exp(-(t/T1)) - Math.exp(-(TR/ T1))) I0^2 ((Math.exp(-(t/T1)) (-1 + k) t)/ T1^2 - (Math.exp(-(TR/T1)) k TR)/T1^2) -2 I0 ((Math.exp(-(t/T1)) t)/T1^2 - (Math.exp(-(TR/T1)) TR)/ T1^2) (-I0 (1 + Math.exp(-(t/ T1)) (-1 + k) - Math.exp(-(TR/T1)) k) + y),
			kk	 *[2][2]	2 (Math.exp(-(t/T1)) - Math.exp(-(TR/T1)))^2 I0^2}}
				 *    *  */

				//double [][] secderv_orig = (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(c);
				double [][] secderv = new double[3][3];

				//c[2] = -1;
				I_0 = c[0];
				T1 = c[1];
				k = c[2];
				
				
				for(int i=0;i<x.length;i++) {
					
					double v1 = 2*I_0*(1+Math.exp(-(x[i]/T1))*(-1 + k)-Math.exp(-(TR/T1))*k)
								*((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/ Math.pow(T1, 2) - (Math.exp(-(TR/T1))*k*TR)/Math.pow(T1,2)) 
								-2*((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/ Math.pow(T1,2) - (Math.exp(-(TR/T1))*k*TR)/ Math.pow(T1,2))*
								(-I_0*(1 +Math.exp(-(x[i]/ T1))*(-1 + k) -Math.exp(-(TR/T1))*k) + y[i]);
					
					double v2 = 2*(Math.exp(-(x[i]/T1)) - Math.exp(-(TR/ T1)))
								*I_0*(1 + Math.exp(-(x[i]/ T1))*(-1 + k) - Math.exp(-(TR/T1))*k) 
								-2*(Math.exp(-(x[i]/T1)) - Math.exp(-(TR/ T1)))
								*(-I_0*(1 + Math.exp(-(x[i]/ T1))*(-1 + k) - Math.exp(-(TR/T1))*k) + y[i]);
					
					double v3 = 2*(Math.exp(-(x[i]/T1)) - Math.exp(-(TR/T1))) *Math.pow(I_0,2) 
								* ((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/Math.pow(T1,2) - (Math.exp(-(TR/T1))*k*TR)/Math.pow(T1,2)) 
								- 2*I_0*((Math.exp(-(x[i]/T1))*x[i])/ Math.pow(T1,2) - (Math.exp(-(TR/T1))*TR)/Math.pow(T1,2))
								*(-I_0*(1 + Math.exp(-(x[i]/ T1))*(-1 + k) - Math.exp(-(TR/T1))* k) + y[i]); 
					
					secderv[0][0] += 2*Math.pow((1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/ T1))*k),2);
					secderv[0][1] += v1;
					secderv[0][2] += v2;
					secderv[1][0] += v1;
					secderv[1][1] += 2*Math.pow(I_0,2)*Math.pow((Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/Math.pow(T1,2) - (Math.exp(-(TR/T1))*k*TR)/Math.pow(T1,2),2) 
										-2*I_0*((Math.exp(-(x[i]/T1))*(-1 + k)*Math.pow(x[i],2))/Math.pow(T1,4) - (2*Math.exp(-(x[i]/T1))*(-1 + k)*x[i])/Math.pow(T1, 3)+ (2*Math.exp(-(TR/T1))*k*TR)/Math.pow(T1, 3) - (Math.exp(-(TR/T1))*k*Math.pow(TR,2))/Math.pow(T1, 4)) 
										*(-I_0*(1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/T1))*k) + y[i]);
					secderv[1][2] += v3;//
					secderv[2][0] += v2;
					secderv[2][1] += v3;//
					secderv[2][2] += 2*Math.pow((Math.exp(-(x[i]/T1)) - Math.exp(-(TR/T1))),2)*Math.pow(I_0,2);//
				}
				secderv [0][0] /= x.length;
				secderv [0][1] /= x.length;
				secderv [0][2] /= x.length;
				secderv [1][0] /= x.length;
				secderv [1][1] /= x.length;
				secderv [1][2] /= x.length;
				secderv [2][0] /= x.length;
				secderv [2][1] /= x.length;
				secderv [2][2] /= x.length;
				
				return secderv;
				//return secderv_orig;
				
			}

			/**
			 * Gets the hessian of the function.
			 * Uses the specified step size and method.
			 * @param x location to calculate hessian
			 * @param step step size
			 * @param method method
			 * @return hessian at location x
			 */
			public double[][] get2ndDerivative(double[] c, double step, int method) {
				return (new FunctionNumericNDDifferentiation(this, step, method)).get2ndDerivative(x);  
			}

			/**
			 * Gets the 2nd derivative of the function in a specific direction.
			 * Uses the default step size and method.
			 * @param x location to calculate derivative
			 * @param dim1 dimension to take the first derivative
			 * @param dim2 dimension to take the second derivative
			 * @return 2nd derivative at location x in the specified direction
			 */
			public double get2ndDerivative(double[] x, int dim1, int dim2) {
				return (new FunctionNumericNDDifferentiation(this)).get2ndDerivative(x, dim1, dim2);  
			}

			/**
			 * Gets the gradient of the function.
			 * Uses the default step size and method
			 * @param x location to calculate gradient
			 * @return gradient at location x
			 */
			public double[] getDerivative(double[] c) {
				/* Using Mathematica to calculate the derivatives analytically using:
				 *  y = data
				 *  v = S[t_, I0_, TR_, T1_, k_] :=  I0 (1 + (k - 1) exp(-t/T1) - k exp(-TR/T1))
				 *  f = (y - v)^2
				 *  
				 *  gives 1st derivative or gradient :
				 *  D[f[T1, I0, k], {{I0, T1, k}}]
				 *  {-2 (1 + Math.exp(-(x[i]/T1)) *(-1 + k) - Math.exp(-(TR/ T1))* k) (-I_0* (1 + Math.exp(-(x[i]/     T1))* (-1 + k) - Math.exp(-(TR/T1))* k) + y),
				 *   -2* I_0*((Math.exp(-(x[i]/T1))* (-1 + k)* x[i])/  T1^2 - (Math.exp(-(TR/T1))* k *TR)/ T1^2)* (-I_0 *(1 + Math.exp(-(x[i]/ T1))* (-1 + k) - Math.exp(-(TR/T1))* k) + y),
				 *   -2 *(Math.exp(-(x[i]/T1)) - Math.exp(-(TR/ T1)))* I_0 *(-I_0* (1 + Math.exp(-(x[i]/  T1)) *(-1 + k) - Math.exp(-(TR/T1))* k) + y)}
				 *   	 */

				//double [] derv_orig =new FunctionNumericNDDifferentiation(this).getDerivative(c);
				double[] derv = new double[3];
				
				I_0 = c[0];
				T1 = c[1];
				k = c[2];
				
				for(int i=0;i<x.length;i++) {
					derv[0] += -2*(1 + Math.exp(-(x[i]/T1))*(-1 + k) - Math.exp(-(TR/ T1))* k)
								*(-I_0*(1 + Math.exp(-(x[i]/T1))* (-1 + k) - Math.exp(-(TR/T1))* k) + y[i]);
					derv[1] += -2* I_0*((Math.exp(-(x[i]/T1))*(-1 + k)* x[i])/ Math.pow(T1,2) - (Math.exp(-(TR/T1))* k *TR)/ Math.pow(T1,2))
								* (-I_0 *(1 + Math.exp(-(x[i]/ T1))* (-1 + k) - Math.exp(-(TR/T1))* k) + y[i]);
					derv[2] += -2 *(Math.exp(-(x[i]/T1)) - Math.exp(-(TR/ T1)))
								* I_0 *(-I_0* (1 + Math.exp(-(x[i]/ T1)) *(-1 + k) - Math.exp(-(TR/T1))* k) + y[i]);
				}
				derv[0] = derv[0]/x.length;
				derv[1] = derv[1]/x.length;
				derv[2] = derv[2]/x.length;
				return derv;
				//return derv_orig;
			
			}

			/**
			 * Gets the gradient of the function.
			 * Uses the specified step size and method.
			 * @param x location to calculate gradient
			 * @param step step size
			 * @param method method
			 * @return gradient at location x
			 */
			public double[] getDerivative(double[] x, double step, int method) {
				return (new FunctionNumericNDDifferentiation(this, step, method)).getDerivative(x);  
			}

			@Override
			public int getNumberOfDimensions() {
				// TODO Auto-generated method stub
				return dim;
			}			
		}
	}