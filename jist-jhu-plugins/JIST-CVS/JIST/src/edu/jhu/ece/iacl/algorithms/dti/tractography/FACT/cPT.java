package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import edu.jhu.ece.iacl.jist.structures.geom.*;




/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 16, 2005
 * Time: 3:36:44 PM
 * To change this template use Options | File Templates.
 */
public class cPT  extends PT implements ActionListener{
    public cPT(  float nx,float  ny, float  nz) {
    	super(nx,ny,nz);
     
    }
    public int cor2ind(int sx,int  sy) {
        return Math.round(x)+sx*(Math.round(y)+Math.round(z)*sy);
    }
    public String toString() {
        return "("+Math.round(x)+","+Math.round(y)+","+Math.round(z)+")";
    }

    public boolean equals(cPT pt) {
        return (pt.x==x)&&(pt.y==y)&&(pt.z==z);
    }
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
