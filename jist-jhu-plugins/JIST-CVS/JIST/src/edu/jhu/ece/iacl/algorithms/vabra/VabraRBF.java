package edu.jhu.ece.iacl.algorithms.vabra;

import java.util.ArrayList;
import java.util.List;


import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class VabraRBF extends AbstractCalculation{

	private double maxValuesChange; //max change between adjacent values
	private int scale;//scale of the RBF
	public float[][][] values;//values of the RBF
	private int offset;//offset to the center of the RBF
	

	
	public VabraRBF(AbstractCalculation parent) {
		super(parent);
		offset = 0;
		scale = 5;
		setScale(5, 5, 5);
	}
	
	public void setScale(float dx, float dy, float dz) {

		int mult = 1;
		scale = (int) Math.ceil(1.7 * Math.max(dx, Math.max(dy, dz)));
		//scale = (int) Math.ceil(Math.min(dx, Math.min(dy, dz)));
		int i, j, k;
		float temp;
		offset = (int)Math.floor(mult * scale);
		
		values = (new ImageDataFloat(2 * offset + 1, 2 * offset + 1, 2 * offset + 1)).toArray3d();
		
		System.out.println(getClass().getCanonicalName()+"\t"+"SCALE X");
		for (i = -scale * mult; i <= scale * mult; i++)
			for (j = -scale * mult; j <= scale * mult; j++)
				for (k = -scale * mult; k <= scale * mult; k++) {
					values[i + offset][j + offset][k + offset] = 0;
				}

		System.out.println(getClass().getCanonicalName()+"\t"+"SCALE Y");
		for (i = -scale; i <= scale; i++)
			for (j = -scale; j <= scale; j++)
				for (k = -scale; k <= scale; k++) {
					values[i + offset][j + offset][k + offset] = RegistrationUtilities.RBF3D(0, 0, 0, i, j, k, scale);
				}
		//System.out.format(valRBFM[-3+ valRBFoff][12+ valRBFoff][12+ valRBFoff] + "\n");

		maxValuesChange = 0.0;
		System.out.println(getClass().getCanonicalName()+"\t"+"SCALE Z");
		for (i = 0; i <= scale; i++)
			for (j = 0; j <= scale; j++)
				for (k = 0; k <= scale; k++) {
					temp = Math.abs(values[i + scale * mult][j + scale* mult][k + scale * mult]
					                       - values[i - 1 + scale * mult][j + scale* mult][k + scale * mult]);
					if (temp > maxValuesChange) maxValuesChange = temp;
				}

		//System.out.format("Scale=%d\n", rbfScale);
	}
	

	public int getScale(){
		return scale;
	}
	
	public int getOffset(){
		return offset;
	}
	
	public double getMaxValuesChange(){
		return maxValuesChange;
	}
	
}
