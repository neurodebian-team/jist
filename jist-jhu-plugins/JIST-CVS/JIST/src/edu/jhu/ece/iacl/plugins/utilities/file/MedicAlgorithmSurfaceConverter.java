package edu.jhu.ece.iacl.plugins.utilities.file;

import javax.vecmath.Matrix3f;
import javax.vecmath.Point3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.jist.io.SurfaceBrainSuiteReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceDxReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceFreeSurferReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceMipavReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceVrmlReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceVtkReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class MedicAlgorithmSurfaceConverter extends ProcessingAlgorithm{
	private ParamSurface surfParam;
	private ParamSurface newSurf;
	private ParamOption outputType;

	private ParamBoolean		useMipavCoordinates;
	private ParamPointInteger 	imageDim;
	private	ParamPointDouble	imageRes;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Convert between surface types.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surfParam=new ParamSurface("Surface"));
		inputParams.add(outputType=new ParamOption("File Type",new String[]{"VTK","VRML","DX","FreeSurfer","Mipav","BrainSuite"}));

		inputParams.add(useMipavCoordinates=new ParamBoolean("map to MIPAV coordinates"));
		inputParams.add(imageDim=new ParamPointInteger("Image Dimensions", new Point3i(256, 256, 256)));
		inputParams.add(imageRes=new ParamPointDouble("Image Resolutions", new Point3d(1.0, 1.0, 1.0)));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Surface Conversion");
		inputParams.setName("Surface_Conversion");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(newSurf=new ParamSurface("Surface"));
	}


	protected void execute(CalculationMonitor monitor) {
		switch(outputType.getIndex()){
			case 0:newSurf.setReaderWriter(new SurfaceVtkReaderWriter());break;
			case 1:newSurf.setReaderWriter(new SurfaceVrmlReaderWriter());break;
			case 2:newSurf.setReaderWriter(new SurfaceDxReaderWriter());break;
			case 3:newSurf.setReaderWriter(new SurfaceFreeSurferReaderWriter());break;
			case 4:newSurf.setReaderWriter(new SurfaceMipavReaderWriter());break;
			case 5:newSurf.setReaderWriter(new SurfaceBrainSuiteReaderWriter());break;
		}
		EmbeddedSurface surf=surfParam.getSurface();

		if (useMipavCoordinates.getValue()) {
			float[] box = new float[3];

			box[0] = (float)((0.5f*imageDim.getValue().x-0) * imageRes.getValue().x);
			box[1] = (float)((0.5f*imageDim.getValue().y-0) * imageRes.getValue().y);
			box[2] = (float)((0.5f*imageDim.getValue().z-0) * imageRes.getValue().z);

			surf.scaleVertices(new float[]{-(float)imageRes.getValue().x, -(float)imageRes.getValue().y, -(float)imageRes.getValue().z});
			Point3f pt = new Point3f(box[0],box[1],box[2]);
			Matrix3f rt = new Matrix3f(1,0,0,0,0,-1,0,1,0);
			surf.affineTransformVertices(rt,pt);
			surf.computeNormals();
		}
		newSurf.setValue(surf);
	}
}
