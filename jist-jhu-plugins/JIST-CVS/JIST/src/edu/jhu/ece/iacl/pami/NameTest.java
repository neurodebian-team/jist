package edu.jhu.ece.iacl.pami;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.MipavViewUserInterface;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;

public class NameTest extends ProcessingAlgorithm {
	private ParamVolume refParam;

	
	protected ParamVolume resultVolParam;
	
	private static final String rcsid =
		"$Id: NameTest.java,v 1.5 2010/06/15 20:25:26 bennett Exp $";
	private static final String cvsversion =
		"$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	String shortDescription = "This demo plug copies an input volume to an output volume with a different name.";
	String longDescription;
	
	protected void createInputParameters(ParamCollection inputParams) {
		
		inputParams.setCategory("Demos");
		inputParams.setPackage("PAMI");
		inputParams.setName("Name Test");
		inputParams.setLabel("Name Test");
		
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);

		
		inputParams.add(refParam=new ParamVolume("Reference Volume",null,-1,-1,-1,-1));
		
	}

	protected void createOutputParameters(ParamCollection outputParams) {		
		outputParams.add(resultVolParam=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
	}

	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		System.out.println(getClass().getCanonicalName()+"\t"+"Getting Volume:"+refParam.getValue());
		ImageData p = new ImageDataFloat(refParam.getImageData());
		System.out.println(getClass().getCanonicalName()+"\t"+"Got Volume:"+refParam.getValue());
		p.setName("NameTest");		
		resultVolParam.setValue(p);
		p.dispose();
	}
}
