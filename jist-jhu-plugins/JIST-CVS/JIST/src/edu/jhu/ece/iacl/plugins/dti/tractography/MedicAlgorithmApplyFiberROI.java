package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import edu.jhu.ece.iacl.algorithms.manual_label.LabelImage;
import edu.jhu.ece.iacl.algorithms.manual_label.ROI;
import edu.jhu.ece.iacl.jist.io.*;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class MedicAlgorithmApplyFiberROI extends ProcessingAlgorithm {
	//input param
	private ParamObject<FiberCollection> fiberset;
	private ParamVolume roi;
	private ParamOption operation;
	private ParamInteger label;
	private ParamOption outputtype;
	private ParamOption mode;
	
	//output param
	private ParamFileCollection fibersubset;
	private ParamObject<double[][]> labelList;

	private FiberCollectionReaderWriter fcrw = FiberCollectionReaderWriter.getInstance();
	private CurveVtkReaderWriter cvrw = CurveVtkReaderWriter.getInstance();

	private static final String cvsversion = "$Revision: 1.9 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Gets a subset of the input fibers based on the input volumetric ROI.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fiberset = new ParamObject<FiberCollection>("Fiber Set", new FiberCollectionReaderWriter()));
		inputParams.add(roi = new ParamVolume("ROI"));
		inputParams.add(operation = new ParamOption("ROI Operation", new String[]{"Or", "Not"}));
		inputParams.add(label = new ParamInteger("Label",-1));
		label.setDescription("Integer Label defining the ROI.  A value of negative one indicates that all values greater than zero will be included in the region of interest");

		inputParams.add(outputtype=new ParamOption("Output Type",new String[]{"DtiStudio dat","Vtk"}));
		inputParams.add(mode=new ParamOption("Mode",new String[]{"Union","Single","Pair"}));
		mode.setDescription("This option changes the behavior of the default \"-1\" label.\n" +
		"In the default, \"Union\" mode, a single fiber collection returned, containing all fibers\n" +
		"in the union of all labels >0.\n" +
		"In the \"Single\" mode, a separate fiber collection is returned for each label >0 in the roi volume.\n" +
		"Finally, in the \"Pair\" mode, a fiber collection is returned which contains the fibers connecting each pair\n" +
		"of labels in the roi volume\n");


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Apply Fiber ROI");
		inputParams.setName("Apply_Fiber_ROI");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "http://putter.ece.jhu.edu/John"));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fibersubset = new ParamFileCollection("Fiber Sub-Sets",new FileExtensionFilter(new String[]{"dat","vtk"})));
		outputParams.add(labelList = new ParamObject<double[][]>("Labels found"));
		labelList.setMandatory(false);
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){
			e.printStackTrace();
		}

		if(label.getInt()==-1){
			if(mode.getIndex()==0){
				ROI mask = new  ROI(roi.getImageData());
				File f = writeFibers(mask.fiberroi(fiberset.getObject(), operation.getIndex(),label.getInt()),dir);
				fibersubset.add(f);
			}else if(mode.getIndex()==1){
				System.out.println(getClass().getCanonicalName()+"\t"+"Finding all sigle subsets");
				runSingleMode(dir);
			}else{
				System.out.println(getClass().getCanonicalName()+"\t"+"Finding all paired subsets");
				runDoubleMode(dir);
			}
		}else{
			ROI mask = new  ROI(roi.getImageData());
			File f = writeFibers(mask.fiberroi(fiberset.getObject(), operation.getIndex(),label.getInt()),dir);
			fibersubset.add(f);
		}
	}


	private void runSingleMode(File dir){
		ROI mask = new ROI(roi.getImageData());
		ArrayList<Integer> labels = mask.getLabelList();
		labelList.setObject(toArray(labels));
		for(Integer i: labels){
			System.out.println(getClass().getCanonicalName()+"\t"+"Working on label: " + i);
			File f = writeFibers(mask.fiberroi(fiberset.getObject(), operation.getIndex(),i),dir);
			fibersubset.add(f);
		}
	}


	private void runDoubleMode(File dir){
		ROI mask = new ROI(roi.getImageData());
		ArrayList<Integer> labels = mask.getLabelList();
		labelList.setObject(toArray(labels));
		for(Integer i: labels){
			FiberCollection col = mask.fiberroi(fiberset.getObject(), operation.getIndex(),i);
			for(Integer j: labels){
				System.out.println(getClass().getCanonicalName()+"\t"+"Working on label pair: " + i + " - " +j);
				if(i.intValue()<j.intValue()){
					File f = writeFibers(mask.fiberroi(col, operation.getIndex(),j),dir);
					fibersubset.add(f);
				}
			}
		}
	}


	private File writeFibers(FiberCollection fibers, File dir){
		File out = null;
		if(outputtype.getIndex()==0){
			out = fcrw.write(fibers, dir);
		}else{
			out = cvrw.write(fibers.toCurveCollection(), dir);
		}
		return out;
	}


	private double[][] toArray(ArrayList<Integer> in ){
		double[][] out = new double[in.size()][1];
		for(int i=0; i<in.size(); i++){
			out[i][0]=in.get(i);
		}
		return out;
	}
}
