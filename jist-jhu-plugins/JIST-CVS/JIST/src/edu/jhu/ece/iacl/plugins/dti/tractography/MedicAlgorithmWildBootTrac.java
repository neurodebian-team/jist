package edu.jhu.ece.iacl.plugins.dti.tractography;

import imaging.SchemeV1;
import inverters.AlgebraicDT_Inversion;
import inverters.DT_Inversion;
import inverters.LinearDT_Inversion;
import inverters.NonLinearDT_Inversion;
import inverters.RestoreDT_Inversion;
import inverters.WeightedLinearDT_Inversion;

import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import javax.vecmath.Point3f;

import com.thoughtworks.xstream.XStream;


import edu.jhu.ece.iacl.algorithms.dti.tractography.WildBootFiberTracker;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import edu.jhu.ece.iacl.jist.utility.JistLogger;



public class MedicAlgorithmWildBootTrac  extends ProcessingAlgorithm{
	/******************** TENSOR ESTIMATION *******************************/
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolumeCollection DWdata4D; 		// Imaging Data
	private ParamVolume Mask3D;			// Binary mask to indicate computation volume
	private ParamOption estOptions;		// Option to attempt to estimate with missing data
	private ParamFloat noiseLevel;		// Used for restore
	private ParamFile SchemeFile;
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume tensorVolume;	// A 4D volume with one tensor estimated per pixel
	private ParamVolume exitCodeVolume;	// A 3D volume 
	private ParamVolume intensityVolume;// A 3D volume
	private ParamVolume angUncertaintyVolume;// A 3D volume

	/******************** FIBER TRACKING ***********************************/
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamInteger seedsPerVoxel; 
	private ParamFloat probabilityOfSeedStart;
	private ParamDouble startFA;
	private ParamDouble stopFA;
	private ParamInteger turningAngle;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamObject<FiberCollection> fibers;
	private ParamObject<CurveCollection> fiberLines;		
	private ParamBoolean writeVtk;

	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "") .replace(" ", "");
	private static final String shortDescription = "Wild Bootstrap Verison of FACT - Probabilistic, Streamline Tractography Algorithm.";
	private static final String longDescription = "An implementation of the WildBootFACT algorithm. By default, begins tracking at all voxels, but can optionally track from a single seed point.  Returns a fiber collection in DTIStudio and in VTK format.";

	protected void createInputParameters(ParamCollection inputParams) {
		
		/****************************************************
		 * Input Parameters 
		 ****************************************************/		
		inputParams.add(DWdata4D=new ParamVolumeCollection("DWI and Reference Image(s) Data (4D)",null,-1,-1,-1,-1));
		DWdata4D.setLoadAndSaveOnValidate(false);

		
		/******************** TENSOR ESTIMATION *******************************/
		ParamCollection tensorEst = new ParamCollection("Tensor Estimation");
		tensorEst.add(SchemeFile=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"scheme","schemev1"})));
		tensorEst.add(Mask3D=new ParamVolume("Mask Volume to Determine Region of Tensor Estimation (3D)",null,-1,-1,-1,1));
		Mask3D.setLoadAndSaveOnValidate(false);
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.	
		tensorEst.add(estOptions=new ParamOption("Select CAMINO Tensor Estimator?",new String[]{"Algebraic","Linear","NonLinear","RESTORE","WeightedLinear"}));
		estOptions.setValue("Linear");
		tensorEst.add(noiseLevel=new ParamFloat("Noise Level (RESTORE only)"));
		inputParams.add(tensorEst);


		/******************** FIBER TRACKING ***********************************/
		ParamCollection tracking = new ParamCollection("Fiber Tracking");
		tracking.add(startFA=new ParamDouble("Start FA",0,1,0.5));
		tracking.add(stopFA=new ParamDouble("Stop FA",0,1,0.5));
		tracking.add(turningAngle=new ParamInteger("Max Turn Angle",0,180,40));
		startFA.setDescription("The FA at which to begin tracking");
		stopFA.setDescription("The FA at which to terminate tracking");
		turningAngle.setDescription("The angle (in degrees) which terminates tracking");
		tracking.add(seedsPerVoxel = new ParamInteger("Seeds Per Voxels",1,1000,1));
		tracking.add(probabilityOfSeedStart = new ParamFloat("Probability of starting each seed", 0, 1, 1));
		probabilityOfSeedStart.setDescription("Used for downsampling fiber tracking field.");
		inputParams.add(tracking);

		/******************** Output ***********************************/
		ParamCollection outputOptions  = new ParamCollection("Output Options");
		outputOptions.add(writeVtk = new ParamBoolean("Write VTK fibers (slow)?",false));
		inputParams.add(outputOptions);

		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Tractography");
		inputParams.setLabel("Wild Bootstrap Tractography (FACT)");
		inputParams.setName("WildBoot_FACT");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.add(new Citation("Mori S, Crain BJ, Chacko VP, van Zijl PCM, \"Three dimensional tracking of axonal projections in the brain by magnetic resonance imaging\" Ann Neurol. 1999. 45:(265-9)"));
		info.add(new Citation("Basser PJ, Mattielo J, and Lebihan D, Estimation of the effective self-diffusion tensor from the NMR spin echo, Journal of Magnetic Resonance, 103, 247-54, 1994."));
		info.add(new Citation("Jones DK and Basser PJ, Squashing peanuts and smashing pumpkins: How noise distorts diffusion-weighted MR data, Magnetic Resonance in Medicine, 52(5), 979-993, 2004."));
		info.add(new Citation("Alexander DC and Barker GJ, Optimal imaging parameters for fibre-orientation estimation in diffusion MRI, NeuroImage, 27, 357-367, 2005"));
		info.add(new Citation("Chang L-C, Jones DK and Pierpaoli C, RESTORE: Robust estimation of tensors by outlier rejection, Magnetic Resonance in Medicine, 53(5), 1088-1095, 2005."));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.NotFunctional);
	}


	protected void createOutputParameters(ParamCollection outputParams) {

		/****************************************************
		 * Tensor Estimation Outputs 
		 ****************************************************/
		tensorVolume = new ParamVolume("Tensor Estimate",null,-1,-1,-1,6);
		tensorVolume.setName("Tensor (xx,xy,xz,yy,yz,zz)");
		tensorVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(tensorVolume);
		exitCodeVolume = new ParamVolume("Estimation Exit Code",null,-1,-1,-1,1);
		exitCodeVolume.setName("Exit Code");
		exitCodeVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(exitCodeVolume);	
		intensityVolume = new ParamVolume("Intensity Estimate",null,-1,-1,-1,1);
		intensityVolume.setName("Intensity");
		intensityVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(intensityVolume);
		angUncertaintyVolume = new ParamVolume("Angular Uncertainty",null,-1,-1,-1,1);
		angUncertaintyVolume.setName("Cone of Uncertainty (deg)");
		angUncertaintyVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(angUncertaintyVolume);
		
		

		/****************************************************
		 * Fiber Tracking Outputs
		 ****************************************************/
		outputParams.add(fibers=new ParamObject<FiberCollection>("Fibers (DTI Studio)",new FiberCollectionReaderWriter()));	
		outputParams.add(fiberLines=new ParamObject<CurveCollection>("Fibers (VTK)",new CurveVtkReaderWriter()));		
		fiberLines.setMandatory(false);

	}
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ExecuteWrapper wrapper=new ExecuteWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}


	protected class ExecuteWrapper extends AbstractCalculation{
		public void execute(ProcessingAlgorithm alg){
			 
			/************************ SETUP FIBER TRACKING INPUTS *********************************************/
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Start");
			this.setLabel("Start");
			/****************************************************
			 * Step 1. Load the DTI Scheme
			 ****************************************************/
			this.setLabel("Load Scheme");
			JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Load Scheme");
			SchemeV1 DTIscheme = null;
			XStream xstream = new XStream();
			xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
			try {
				ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
				DTIscheme=(SchemeV1)in.readObject();
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}

			/****************************************************
			 * Step 2. Choose a tensor estimator
			 ****************************************************/
			this.setLabel("Choose Estimator");
			DT_Inversion dtiFit=null;
			String code = estOptions.getValue();
			if(estOptions.getValue().compareToIgnoreCase("Algebraic")==0) {
				dtiFit=new AlgebraicDT_Inversion(DTIscheme);			
				JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Using Tensor Estimator: Algebraic ");
			}
			if(estOptions.getValue().compareToIgnoreCase("Linear")==0) {
				dtiFit=new LinearDT_Inversion(DTIscheme);
				JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Using Tensor Estimator: Linear ");
			}
			if(estOptions.getValue().compareToIgnoreCase("NonLinear")==0) {
				dtiFit=new NonLinearDT_Inversion(DTIscheme);			
				JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Using Tensor Estimator: NonLinear");
			}
			if(estOptions.getValue().compareToIgnoreCase("RESTORE")==0) {
				System.out.println(getClass().getCanonicalName()+"\t"+"Using RESTORE Noise Level: "+noiseLevel.getValue().floatValue());
				dtiFit=new RestoreDT_Inversion(DTIscheme,noiseLevel.getValue().floatValue());		
				JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Using Tensor Estimator: RESTORE, sigma="+noiseLevel.getValue().floatValue()+" (intensity units)");
			}
			if(estOptions.getValue().compareToIgnoreCase("WeightedLinear")==0) {
				dtiFit=new WeightedLinearDT_Inversion(DTIscheme);			
				JistLogger.logOutput(JistLogger.INFO,"WildBootFACT: Using Tensor Estimator: WeightedLinear");
			}


			/****************************************************
			 * Step 3. Check out the data 
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,"Loading DW data.");
			this.setLabel("Load Data");
			List<ImageData> dwds=DWdata4D.getImageDataList();
			ImageHeader dataHeader = dwds.get(0).getHeader();
			String dataName = dwds.get(0).getName();
			int r=dwds.get(0).getRows();
			int c=dwds.get(0).getCols();
			int s=dwds.get(0).getSlices();
			int d=0;
			ImageDataFloat DWFloat;
			JistLogger.logOutput(JistLogger.INFO,"Counting dims...");
			for(int i=0;i<dwds.size();i++) {
				JistLogger.logOutput(JistLogger.INFO,"Dims: "+d);

				d+=dwds.get(i).getComponents();
			}
			JistLogger.logOutput(JistLogger.INFO,"Total Dims: "+d);
			if(dwds.size()>1) {
				DWFloat=new ImageDataFloat(r,c,s,d);
				int jD=0;
				for(int j=0;j<dwds.size();j++) {
					ImageData img = dwds.get(j);
					for(int jjD=0;jjD<img.getComponents();jjD++) {
						for(int jr=0;jr<r;jr++) 
							for(int jc=0;jc<c;jc++)
								for(int js=0;js<s;js++){
									DWFloat.set(jr,jc,js,jD,
											img.getFloat(jr,jc,js,jjD));
								}
						jD++;
					}
				} 
			} else {
				DWFloat=new ImageDataFloat(dwds.get(0));
			}
			dwds=null;
			DWdata4D.dispose();

			/****************************************************
			 * Step 4. Check out the mask
			 ****************************************************/
			this.setLabel("Load Mask");
			ImageData maskVol=Mask3D.getImageData();
			byte [][][]mask=null;
			if(maskVol!=null) {
				mask = new byte[maskVol.getRows()][maskVol.getCols()][maskVol.getSlices()];
				for(int i=0;i<mask.length;i++)
					for(int j=0;j<mask[0].length;j++)
						for(int k=0;k<mask[0][0].length;k++)
							mask[i][j][k]=(byte)maskVol.getShort(i,j,k);					
				maskVol.dispose();
			}





			/****************************************************
			 * Step 5. Allocate memory 
			 ****************************************************/
			this.setLabel("Allocate");
			JistLogger.logOutput(JistLogger.INFO,"Allocate memory."); 
			float [][][][]data=DWFloat.toArray4d();
			int rows = data.length;
			int cols= data[0].length;
			int slices= data[0][0].length;
			int components= data[0][0][0].length;
			float [][][][]tensors = new float[rows][cols][slices][6];
			float [][][][]exitCode= new float[rows][cols][slices][1];
			float [][][][]intensity= new float[rows][cols][slices][1];
			float [][][]angUncertainty= new float[rows][cols][slices];

			// EstimateTensorLLMSE.estimateCamino(data,mask,dtiFit,tensors,exitCode,intensity);

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			this.setLabel("Initialize tracker");
			WildBootFiberTracker ftrack=new WildBootFiberTracker(
					// Tensors 
					DTIscheme,
					dtiFit,
					DWFloat,
					tensors,
					exitCode,
					intensity,
					angUncertainty,
					mask,
					// Fiber tracking
					startFA.getDouble(),
					stopFA.getDouble(),
					seedsPerVoxel.getInt(), 
					probabilityOfSeedStart.getFloat(),
					turningAngle.getInt()*Math.PI/180.0,
					10
			);

			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct	
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,"Data export.");
			this.setLabel("Write Contrasts");
			ImageDataFloat out=new ImageDataFloat(tensors);
			out.setHeader(dataHeader);
			out.setName(dataName+"_Tensor"+code);
			tensorVolume.setValue(out);
			tensorVolume.writeAndFreeNow(alg);
			//tensors still used

			out=new ImageDataFloat(exitCode);
			out.setHeader(dataHeader);
			out.setName(dataName+"_ExitCode"+code);
			exitCodeVolume.setValue(out);
			exitCodeVolume.writeAndFreeNow(alg);
			exitCode=null; 

			out=new ImageDataFloat(intensity);
			out.setHeader(dataHeader);
			out.setName(dataName+"_Intensity"+code);
			intensityVolume.setValue(out);
			intensityVolume.writeAndFreeNow(alg);	
			//intensity still used
			
			out=new ImageDataFloat(angUncertainty);
			out.setHeader(dataHeader);
			out.setName(dataName+"_ConeOfUncertainty"+code);
			angUncertaintyVolume.setValue(out);
			angUncertaintyVolume.writeAndFreeNow(alg);
			angUncertainty=null;
			
			
			////////////// Perform tracking
			monitor.observe(ftrack);

			FiberCollection fibercollection=null;

			this.setLabel("Wild Boot FACT");
			fibercollection=ftrack.solve();

			this.setLabel("Write Fibers");
			fibercollection.setName(dataName+"_fbr");
			fibers.setObject(fibercollection);
			if(writeVtk.getValue()){
				fiberLines.setObject(fibercollection.toCurveCollection());
			}

			/////////////////////////////////////////////////////////////////////////
		}

	}
}