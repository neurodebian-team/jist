package edu.jhu.ece.iacl.plugins.utilities.math;

import edu.jhu.bme.smile.commons.math.KLDivergence;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.plugins.JistPluginUtil;


public class MedicAlgorithmKLDivergence extends ProcessingAlgorithm{
	protected ParamDouble kld;
	protected ParamDouble kernelSize;
	protected ParamBoolean yLogScale;
	protected ParamInteger rows,cols;
	protected ParamObject<double[][]> histX,histY;
	protected ParamVolume histImg;
	protected ParamString valueName;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Computes KL divergence between two histograms using kernel density estimation.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(histX=new ParamObject<double[][]>("Histogram X",new ArrayDoubleTxtReaderWriter()));
		inputParams.add(histY=new ParamObject<double[][]>("Histogram Y",new ArrayDoubleTxtReaderWriter()));
		inputParams.add(rows=new ParamInteger("Image Width",0,1600,800));
		inputParams.add(cols=new ParamInteger("Image Height",0,1600,600));
		inputParams.add(kernelSize=new ParamDouble("Kernel Size ",0,1E30,1));
		inputParams.add(valueName=new ParamString("Measurement Name","value"));
		inputParams.add(yLogScale=new ParamBoolean("Probability in Log Scale",true));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Measurement.Statistics");
		inputParams.setLabel("KL Divergence");
		inputParams.setName("KL_Divergence");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(histImg=new ParamVolume("Histogram Image",VoxelType.COLOR_FLOAT));
		histImg.getExtensionFilter().setPreferredExtension("tiff");
		outputParams.add(kld=new ParamDouble("KL Divergence"));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		KLDivergence kldiv=new KLDivergence();
		kld.setValue(kldiv.KLdivergence(histX.getObject(), histY.getObject(), kernelSize.getDouble()));
		String nameX=histX.getValue().getName();
		String nameY=histY.getValue().getName();
		ImageDataColor img=kldiv.getImage(nameX,nameY,valueName.getValue(),yLogScale.getValue(),rows.getInt(),cols.getInt());
		img.setName(nameX+"_kld");
		histImg.setValue(img);
	}
}
