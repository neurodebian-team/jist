package edu.jhu.ece.iacl.plugins.dti.tractography;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.algorithms.dti.tractography.INFACT.INFACTapi;
import edu.jhu.ece.iacl.algorithms.dti.tractography.INFACT.INFACTparameters;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

/**
 * 
 * @author John Bogovic
 *
 */

public class MedicAlgorithmINFACT  extends ProcessingAlgorithm{
	//private ParamSurface cruiseSurf;
	private ParamDouble startFA;
	private ParamDouble stopFA;
	private ParamDouble startFraction;
	private ParamDouble stopFraction;
	
	private ParamInteger turningAngle;
	private ParamOption seedingOption;
	private ParamOption solver;
//	private ParamPointDouble resolution;
	private ParamVolume mixturefraction;
	private ParamVolume faVol;
	private ParamVolume vecIndexVol;
	private ParamSurface basisVectors;
	private ParamObject<FiberCollection> fibers;
	private ParamObject<CurveCollection> fiberLines;
	private ParamBoolean specseed;
	private ParamPointInteger seed;
	private ParamBoolean writeVtk;
	
	private int rows, cols, slices, components;

	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "") .replace(" ", "");
	private static final String shortDescription = "FACT - Deteriministic, Streamline Tractography Algorithm.";
	private static final String longDescription = "An implementation of the FACT algorithm. By default, begins tracking at all voxels, but can optionally track from a single seed point.  Returns a fiber collection in DTIStudio and in VTK format.";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vecIndexVol=new ParamVolume("Basis Indecies",VoxelType.INT,-1,-1,-1,-1));
		inputParams.add(mixturefraction=new ParamVolume("Mixtures Fraction",VoxelType.FLOAT,-1,-1,-1,-1));
		inputParams.add(basisVectors = new ParamSurface("Basis Directions"));
		
		inputParams.add(faVol=new ParamVolume("Fractional Anisotropy",VoxelType.FLOAT,-1,-1,-1,1));
		faVol.setMandatory(false);
		
		inputParams.add(solver=new ParamOption("Solver", new String[]{"FACT", "Runge-Kutta"}));
		
		inputParams.add(startFA=new ParamDouble("Start FA",0,1,0.3));
		inputParams.add(stopFA=new ParamDouble("Stop FA",0,1,0.3));
		inputParams.add(startFraction=new ParamDouble("Start Fraction",0,1,0.3));
		inputParams.add(stopFraction=new ParamDouble("Stop Fraction",0,1,0.3));
		
		inputParams.add(turningAngle=new ParamInteger("Max Turn Angle",0,180,40));

//		inputParams.add(specseed=new ParamBoolean("Specify Seed"));
//		inputParams.add(seed=new ParamPointInteger("Seed Point"));
//		seed.setValue(new Point3i(13,14,6));
		
		inputParams.add(writeVtk = new ParamBoolean("Write VTK fibers?",false));


		startFA.setDescription("The FA at which to begin tracking");
		stopFA.setDescription("The FA at which to terminate tracking");
		startFA.setDescription("The CFARI direction fraction at which to begin tracking");
		stopFA.setDescription("The CFARI direction fraction at which to terminate tracking");
		turningAngle.setDescription("The angle (in degrees) which terminates tracking"); 
//		specseed.setDescription("Begin tracking from a single voxel only");
//		seed.setDescription("When \"Specify Seed\" is selected, specifies the seed location as a voxel coordinate.");

		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Tractography");
		inputParams.setLabel("Fiber Tracker (INFACT)");
		inputParams.setName("Fiber_Tracker_INFACT");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "iacl.ece.jhu.edu/John"));
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.add(new Citation("Mori S, Crain BJ, Chacko VP, van Zijl PCM, \"Three dimensional tracking of axonal projections in the brain by magnetic resonance imaging\" Ann Neurol. 1999. 45:(265-9)"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.NotFunctional);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fibers=new ParamObject<FiberCollection>("Fibers (DTI Studio)",new FiberCollectionReaderWriter()));	
		outputParams.add(fiberLines=new ParamObject<CurveCollection>("Fibers (VTK)",new CurveVtkReaderWriter()));		
		fiberLines.setMandatory(false);
	}


	protected void execute(CalculationMonitor monitor) {
		System.out.println(getClass().getCanonicalName()+"\t"+"TRACKING FIBERS");
		ImageData vecInds = vecIndexVol.getImageData();
		String name = vecInds.getName();
		rows = vecInds.getRows();
		cols = vecInds.getCols();
		slices = vecInds.getSlices();
		components = vecInds.getComponents();
		
		ImageHeader hdr  = vecIndexVol.getImageData().getHeader();
		Point3f resolution = new Point3f();
		resolution.x = hdr.getDimResolutions()[0];
		resolution.y = hdr.getDimResolutions()[1];
		resolution.z = hdr.getDimResolutions()[2];
		System.out.println(getClass().getCanonicalName()+"\t"+"Resolution:" + resolution.x + "," + resolution.y +"," + resolution.z);
		
		//set up parameters
		INFACTparameters INFACTparams = new INFACTparameters();
		INFACTparams.startFA = startFA.getFloat();
		INFACTparams.stopFA = stopFA.getFloat();
		INFACTparams.maxTurnAngle = (float)(turningAngle.getInt()*Math.PI/180.0);
		INFACTparams.resX = resolution.x;
		INFACTparams.resY = resolution.y;
		INFACTparams.resZ = resolution.z;
		INFACTparams.Nx = rows;
		INFACTparams.Ny = cols;
		INFACTparams.Nz = slices;
		INFACTparams.Nt = components;
		if(faVol.getValue()!=null){
			INFACTparams.useFA=true;
		}
		
		INFACTparams.basisDirs = normalizeBasisDirections(basisVectors.getObject());
		INFACTparams.basisInds = getIntArray(vecIndexVol.getImageData());
		INFACTparams.mixFrac = getFloatArray(mixturefraction.getImageData());
		
		//clean up
		vecIndexVol.dispose();
		mixturefraction.dispose();
		basisVectors.dispose();
		
		INFACTapi ftrack=new INFACTapi(INFACTparams);
		
		monitor.observe(ftrack);
		FiberCollection fibercollection=ftrack.track();
	
//		if(specseed.getValue()){
//			fibercollection=ftrack.trackFromSeed(seed.getValue().x,seed.getValue().y,seed.getValue().z);
//		}else{
//			fibercollection=ftrack.track();
//		}
		
		fibercollection.setName(name+"_fibers");
		fibercollection.setResolutions(new Point3f(INFACTparams.resX,INFACTparams.resY,INFACTparams.resZ));
		fibercollection.setDimensions(new Point3i(INFACTparams.Nx,INFACTparams.Ny,INFACTparams.Nz));
		fibers.setObject(fibercollection);
		if(writeVtk.getValue()){
			fiberLines.setObject(fibercollection.toCurveCollection());
		}
	}
	
	private Point3f[] normalizeBasisDirections(EmbeddedSurface basisdir){
		Point3f[] dirs = basisdir.getVertexCopy();
		int N = dirs.length;
		float nrm = 0;
		for(int i=0; i<N; i++){
			nrm = 0;
			nrm += dirs[i].x*dirs[i].x;
			nrm += dirs[i].y*dirs[i].y;
			nrm += dirs[i].z*dirs[i].z;
			
			dirs[i].x = (dirs[i].x)/nrm;
			dirs[i].y = (dirs[i].y)/nrm;
			dirs[i].z = (dirs[i].z)/nrm;
		}
		return dirs;
	}
	
	private int[][][][] getIntArray(ImageData img){
		int[][][][] vol4d = new int[rows][cols][slices][components];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						vol4d[i][j][k][l] = img.get(i, j, k, l).intValue();
					}
				}
			}
		}
		return vol4d;
	}
	
	private float[][][][] getFloatArray(ImageData img){
		float[][][][] vol4d = new float[rows][cols][slices][components];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						vol4d[i][j][k][l] = img.get(i, j, k, l).floatValue();
					}
				}
			}
		}
		return vol4d;
	}
}
