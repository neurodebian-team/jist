package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.util.ArrayList;
import java.awt.Color;
import java.io.File;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;

/**
 * @author John Bogovic
 * Structure for storing, reading and writing 
 * Fiber Color Data in DX
 */
public class DXFiberData {
	
	protected ArrayList<Integer>[] data;

	//Generate Empty Data based on Size of Fibers
	public DXFiberData(Fiber[] fibers){
		genEmptyData(fibers);
	}
	public DXFiberData(Fiber[] fibers,Integer c){
		genEmptyData(fibers,c);
	}
	public ArrayList<Integer>[] getData(){
		return data;
	}
	public void readData(String s){
		
	}
	public void writeData(String s){
		DxDataWriter.write(data, new File(s));
	}
	private void genEmptyData(Fiber[] fibers){
		genEmptyData(fibers, new Integer(0));
	}
	private void genEmptyData(Fiber[] fibers, Integer c){
		data = new ArrayList[fibers.length];
		for(int i=0; i<fibers.length; i++){
			ArrayList<Integer> ithlist = new ArrayList(fibers[i].getLength());
			for(int j=0; j<fibers[i].getLength(); j++){
				ithlist.add(c);
			}
			data[i]=ithlist;
		}
		
		
	}
	
}
