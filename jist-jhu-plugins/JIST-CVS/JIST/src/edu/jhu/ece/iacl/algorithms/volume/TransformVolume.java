package edu.jhu.ece.iacl.algorithms.volume;

import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import Jama.Matrix;
import WildMagic.LibFoundation.Mathematics.Vector3f;

/**
 * Apply linear transformation matrix to volume.
 * 
 * @author Blake Lucas
 * 
 */
public class TransformVolume {
	public enum Interpolation {
		Trilinear, Bspline_3rd_order, Bspline_4th_order, Cubic_Lagrangian, Quintic_Lagrangian, Heptic_Lagrangian, Windowed_sinc, Nearest_Neighbor
	};


	public static ImageDataMipav transform(ImageDataMipav vol,
			Interpolation interpolation, Matrix m, Point3f resolution,
			Point3i dimensions) {
		return transform(vol.getModelImageCopy(),
				 interpolation,  m,  resolution,
				 dimensions, true);
	}


	public static ImageDataMipav transform(ModelImage img,
			Interpolation interpolation, Matrix m, Point3f resolution,
			Point3i dimensions) {
		return transform(img,
				 interpolation,  m,  resolution,
				 dimensions, false);
	}
		
	public static ImageDataMipav transform(ModelImage img,
			Interpolation interpolation, Matrix m, Point3f resolution,
			Point3i dimensions, boolean imgDestroy) {
		int interp = 0;

		switch (interpolation.ordinal()) {
		case 0:
			interp = AlgorithmTransform.TRILINEAR;
			break;
		case 1:
			interp = AlgorithmTransform.BSPLINE3;
			break;
		case 2:
			interp = AlgorithmTransform.BSPLINE4;
			break;
		case 3:
			interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
			break;
		case 4:
			interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
			break;
		case 5:
			interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
			break;
		case 6:
			interp = AlgorithmTransform.WSINC;
			break;
		case 7:
			interp = AlgorithmTransform.NEAREST_NEIGHBOR;
			break;
		default:
			interp = AlgorithmTransform.TRILINEAR;
			break;
		}


		TransMatrix mat = new TransMatrix(4, 4);
		double[][] ar = m.getArray();

		// First Set mat as the Identity
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				mat.set(i, j, 1);
			}
		}


		for (int i = 0; i < ar.length; i++) {
			for (int j = 0; j < ar[0].length; j++) {
				mat.set(i, j, ar[i][j]);
			}
		}


		System.out.println("jist.plugins"+"\t"+"Transform Volume Method "
				+ mat.matrixToString(4, 4) + " (" + resolution.x + ","
				+ resolution.y + "," + resolution.z + ") (" + dimensions.x
				+ "," + dimensions.y + "," + dimensions.z + ")");
		AlgorithmTransform transform = new AlgorithmTransform(img, mat,
				interp, resolution.x, resolution.y, resolution.z, dimensions.x,
				dimensions.y, dimensions.z, true, false, false);
		transform.setUpdateOriginFlag(true);
		transform.run();

		
		/*
		 * Depending on how this method is called the input
		 * image needs to be destroyed.
		 */
		if (imgDestroy){
			img.disposeLocal();
		}


		ModelImage resultImage = transform.getTransformedImage();
		return new ImageDataMipavWrapper(resultImage);
	}
}
