package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Jama.Matrix;

import edu.jhu.bme.smile.commons.math.MatrixMath;
import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.hardi.QBall;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;


public class QBallProjection extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamFileCollection sphericalHarmonicCoeffsReal;	// SLAB-enabled A 4D volume with one tensor estimated per pixel
	private ParamFileCollection sphericalHarmonicCoeffsImag;	// SLAB-enabled A 4D volume with one tensor estimated per pixel
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions
	private ParamBoolean outputImag;
	private ParamBoolean extractMax;	// whether or not to extract maximas
	private ParamFloat extractThresh;	// threshold for extraction of maximas

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFileCollection reconReal;	// SLAB-enabled A 4D volume with one tensor estimated per pixel
	private ParamFileCollection reconImag;	// SLAB-enabled A 4D volume with one tensor estimated per pixel
	private ParamFileCollection basisIndecies;	// basis indecies of extracted maximas
	private ParamFileCollection basisMixtures;	// basis mixtures of extracted maximas

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Log-linear minium mean squared error tensor estimation.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion.QBall");
		inputParams.setLabel("Q-Ball: SH Projection");
		inputParams.setName("Q-Ball:_SH_Projection");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(sphericalHarmonicCoeffsReal = new ParamFileCollection("Spherical Harmonic Coefficients: Real",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(sphericalHarmonicCoeffsImag = new ParamFileCollection("Spherical Harmonic Coefficients: Imaginary",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(gradsTable=new ParamFile("Directions Along Which to Project the SH Model",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(outputImag=new ParamBoolean("Output Imaginary Part of Reconstuction?",false));
		inputParams.add(extractMax=new ParamBoolean("Extract Maximas?",false));
		inputParams.add(extractThresh=new ParamFloat("Minimum Value to be Considered at Maxima",(float).1));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/

		outputParams.add(reconReal = new ParamFileCollection("QBall Projection Real Part",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		outputParams.add(reconImag = new ParamFileCollection("QBall Projection Imaginary Part",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		reconImag.setMandatory(false);
		outputParams.add(basisIndecies=new ParamFileCollection("Basis Indecies",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		basisIndecies.setMandatory(false);
		outputParams.add(basisMixtures=new ParamFileCollection("Mixture Fraction",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		basisMixtures.setMandatory(false);
	}


	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to
		 * 			know when the bug happened.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Q-Ball Proj: Start");

		/****************************************************
		 * Step 2. Loop over input slabs
		 ****************************************************/
		List<File> shReal = sphericalHarmonicCoeffsReal.getValue();
		List<File> shImag = sphericalHarmonicCoeffsImag.getValue();

		ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
		ArrayList<File> realOutVols = new ArrayList<File>();
		ArrayList<File> imagOutVols = new ArrayList<File>();
		ArrayList<File> maxDirIndecies = new ArrayList<File>();
		ArrayList<File> maxDirMixtures = new ArrayList<File>();
		for(int jSlab=0;jSlab<shReal.size();jSlab++) {
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			ImageData shRealData=rw.read(shReal.get(jSlab));
			String sourceName = shRealData.getName();
			ImageDataFloat shRealFloat=new ImageDataFloat(shRealData);
			shRealData.dispose();
			shRealData=null;
			ImageData shImagData=rw.read(shImag.get(jSlab));
			ImageDataFloat shImagFloat=new ImageDataFloat(shImagData);
			shImagData.dispose();
			shImagData=null;


			float [][]grads=null;
			TextFileReader text = new TextFileReader(gradsTable.getValue());
			try {
				grads  = text.parseFloatFile();
			} catch (IOException e) {

				throw new RuntimeException("QBall: Unable to parse grad-file");
			}

			/****************************************************
			 * Step 3. Perform limited error checking
			 ****************************************************/
			// If there are 4 columns in the gradient table, remove the 1st column (indecies)
			if(grads[0].length==4) {
				float [][]g2 = new float[grads.length][3];
				for(int i=0;i<grads.length;i++)
					for(int j=0;j<3;j++)
						g2[i][j]=grads[i][j+1];
				grads=g2;
			}

			if(grads[0].length!=3)
				throw new RuntimeException("QBall: Invalid gradient table. Must have 3 or 4 columns.");

			int SHorder = QBall.getOrderFromNumberOfCoeff(shRealFloat.getComponents());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			int rows = shRealFloat.getRows();
			int cols = shRealFloat.getCols();
			int slices = shRealFloat.getSlices();
			int dirs = grads.length;

			float [][][][]reconReal = new float[rows][cols][slices][dirs];
			float [][][][]reconImag;
			if(outputImag.getValue())
				reconImag = new float[rows][cols][slices][dirs];
			else
				reconImag=null;

			QBall.projectSphericalHarmonics(reconReal,reconImag,
					shRealFloat.toArray4d(),shImagFloat.toArray4d(),
					grads,SHorder);

			/****************************************************
			 * Step 5.	Extract the maximas.
			 ****************************************************/
			int maxDirInd[][][][] = null;
			float maxDirMix[][][][] = null;

			if(extractMax.getValue()) {
				int nOut=10;
				maxDirInd = new int[rows][cols][slices][nOut];
				maxDirMix = new float[rows][cols][slices][nOut];
				float thresh = extractThresh.getFloat();
				for (int i=0; i<rows; i++) {
					for (int j=0; j<cols; j++) {
						for (int k=0; k<slices; k++) {
							int count=0;
							int maximas[] = new int[nOut];
							Arrays.fill(maximas, -1);
							ArrayList<float[]> directions = new ArrayList<float[]>(dirs);
							for (int d=0; d<dirs; d++)
								directions.add(new float[]{reconReal[i][j][k][d],d});
							Collections.sort(directions, new dCompare());
							ArrayList<Boolean> dirEval = new ArrayList<Boolean>(dirs);
							for (int d=0; d<directions.size(); d++)
								dirEval.add(false);
							for (int g=0; g<directions.size(); g++) {
								int loc=(int)directions.get(g)[1];
								if (!dirEval.get(loc)) {
									dirEval.set(loc,true);
									Matrix v1=new Matrix(new double[]{grads[loc][0],grads[loc][1],grads[loc][2]},3);
									Matrix v2=new Matrix(3,1);
									ArrayList<float[]> angles=new ArrayList<float[]>(dirs);	// move this outside of the loop to run faster
									for (int v=0; v<dirs; v++) {
										v2.set(0,0,grads[v][0]);
										v2.set(1,0,grads[v][1]);
										v2.set(2,0,grads[v][2]);
										float angle=(float) (Math.min(Math.acos(MatrixMath.dotProduct(v1,v2)), Math.acos(MatrixMath.dotProduct(v1,v2.times(-1))))*180/Math.PI);
										angles.add(v,new float[]{90-angle,v});
									}
									Collections.sort(angles,new dCompare());
									boolean max=true;
									for (int v=0; v<20; v++) {
										if (reconReal[i][j][k][loc]-reconReal[i][j][k][(int)angles.get(v)[1]]<0)
											max=false;
										else
											dirEval.set((int)angles.get(v)[1],true);
									}
									if (max) {
										maximas[count++]=loc;
										if (count==nOut)
											break;
									}
								}
							}
							for (int v=0; v<nOut; v++) {
								maxDirInd[i][j][k][v]=maximas[v];
								if (maximas[v]==-1)
									maxDirMix[i][j][k][v]=Float.NaN;
								else
									maxDirMix[i][j][k][v]=reconReal[i][j][k][maximas[v]];
							}
						}
					}
				}
			}

			/****************************************************
			 * Step 6. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/

			ImageData  out= (new ImageDataFloat(reconReal)); reconReal=null;
			out.setHeader(shRealFloat.getHeader());
			shRealFloat.dispose();
			shRealFloat=null;
			out.setName(sourceName+"_QBallProjReal");
			File outputSlab = rw.write(out, getOutputDirectory());
			realOutVols.add(outputSlab);
			out.dispose();

			if(outputImag.getValue()) {
				out= (new ImageDataFloat(reconImag)); reconImag=null;
				out.setHeader(shImagFloat.getHeader());
				shImagFloat.dispose();
				shImagFloat=null;
				out.setName(sourceName+"_QBallProjImag");
				outputSlab = rw.write(out, getOutputDirectory());
				imagOutVols.add(outputSlab);
				out.dispose();
				out=null;
			}

			if(extractMax.getValue()) {
				out= (new ImageDataInt(maxDirInd));
				out.setName(sourceName+"_maxDirIndecies");
				outputSlab = rw.write(out, getOutputDirectory());
				maxDirIndecies.add(outputSlab);
				out.dispose();
				out=null;

				out= (new ImageDataFloat(maxDirMix));
				out.setName(sourceName+"_maxDirMixtures");
				outputSlab = rw.write(out, getOutputDirectory());
				maxDirMixtures.add(outputSlab);
				out.dispose();
				out=null;
			}

		}

		reconReal.setValue(realOutVols);
		if(outputImag.getValue())
			reconImag.setValue(imagOutVols);
		/****************************************************
		 * Step 7. Let the user know that your code is finished.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"QBall: FINISHED");
	}


	private class dCompare implements Comparator<float[]> {
		public int compare(float[] o1, float[] o2) {
			if (o1[0]<o2[0])
				return 1;
			else if (o1[0]>o2[0])
				return -1;
			else
				return 0;
		}
	}
}
