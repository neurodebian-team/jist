/**
 * 
 */
package edu.jhu.ece.iacl.plugins.registration;

import java.awt.Dimension;

import Jama.Matrix;

import edu.jhu.ece.iacl.algorithms.volume.CompareVolumes;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingApplication;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;

import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.algorithms.registration.AlgorithmRegOAR3D;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;
import gov.nih.mipav.view.dialogs.JDialogBase;

/**
 * @author Blake Lucas (bclucas@jhu.edu)
 * 
 */
public class MedicAlgorithmFLIRT extends ProcessingAlgorithm {
	ParamOption dof;
	ParamOption inputInterpolation;
	ParamOption outputInterpolation;
	ParamOption costFunction;
	ParamBoolean useMaxOfMinRes;
	ParamOption rotDim;
	ParamDouble minAngle;
	ParamDouble maxAngle;
	ParamDouble coarseAngleIncrement;
	ParamDouble fineAngleIncrement;
	ParamInteger bracketMinimum;
	ParamInteger iters;
	ParamInteger level8to4;
	ParamBoolean subsample;
	ParamBoolean skipMultilevelSearch;
	public ParamVolume source, target, registered, refWeight, inputWeight;
	public ParamMatrix trans;
	// ParamObject<String> mipavMatrix;
	
	// Controls the threaded nature of the OAR Registration.
	private ParamBoolean OARThreadedBool;

	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");


	protected void createInputParameters(ParamCollection inputParams) {
		ParamCollection mainParams = new ParamCollection("Main");
		mainParams.add(source = new ParamVolume("Source volume"));
		mainParams.add(target = new ParamVolume("Target volume"));
		mainParams
		.add(refWeight = new ParamVolume("Reference Weighted volume"));
		refWeight.setMandatory(false);
		mainParams.add(inputWeight = new ParamVolume("Input Weighted volume"));
		inputWeight.setMandatory(false);
		mainParams.add(dof = new ParamOption("Degrees of freedom",
				new String[] { "Rigid - 6", "Global rescale - 7",
				"Specific rescale - 9", "Affine - 12" }));
		dof.setValue(3);
		mainParams.add(costFunction = new ParamOption("Cost function",
				new String[] { "Correlation ratio", "Least squares",
				"Normalized cross correlation",
		"Normalized mutual information" }));
		mainParams.add(inputInterpolation = new ParamOption(
				"Registration interpolation", new String[] { "Trilinear",
						"Bspline 3rd order", "Bspline 4th order",
						"Cubic Lagrangian", "Quintic Lagrangian",
						"Heptic Lagrangian", "Windowed sinc" }));
		mainParams.add(outputInterpolation = new ParamOption(
				"Output interpolation", new String[] { "Trilinear",
						"Bspline 3rd order", "Bspline 4th order",
						"Cubic Lagrangian", "Quintic Lagrangian",
						"Heptic Lagrangian", "Windowed sinc",
				"Nearest Neighbor" }));
		mainParams.add(rotDim = new ParamOption("Apply rotation", new String[] {
				"All", "X", "Y", "Z" }));
		mainParams.add(minAngle = new ParamDouble("Minimum angle", -360, 360,
				-30));
		mainParams.add(maxAngle = new ParamDouble("Maximum angle", -360, 360,
				30));
		mainParams.add(coarseAngleIncrement = new ParamDouble(
				"Coarse angle increment", 0, 360, 15));
		mainParams.add(fineAngleIncrement = new ParamDouble(
				"Fine angle increment", 0, 360, 6));

		ParamCollection advParams = new ParamCollection("Advanced");

		advParams.add(bracketMinimum = new ParamInteger(
				"Multiple of tolerance to bracket the minimum", 10));
		advParams.add(iters = new ParamInteger("Number of iterations", 2));
		advParams.add(level8to4 = new ParamInteger(
				"Number of minima from Level 8 to test at Level 4", 3));
		advParams
		.add(useMaxOfMinRes = new ParamBoolean(
				"Use the max of the min resolutions of the two datasets when resampling",
				true));
		advParams.add(subsample = new ParamBoolean("Subsample image for speed",
				true));
		advParams
		.add(skipMultilevelSearch = new ParamBoolean(
				"Skip multilevel search (Assume images are close to alignment)"));
		advParams.setLabel("Advanced");

		OARThreadedBool = new ParamBoolean("Multithreading for Registration", false);
		OARThreadedBool.setDescription("Set to false by default, this parameter controls the multithreaded behavior of the linear registration.");
		advParams.add(OARThreadedBool);


		// setPreferredSize(new Dimension(500,500));
		inputParams.add(mainParams);
		inputParams.add(advParams);
		inputParams.setName("Optimized Automated Registration");
		inputParams.setLabel("Optimized_Automated_Registration");

		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("Neva Cherniavsky", " ", "http://mipav.cit.nih.gov/"));
		info.add(new AlgorithmAuthor("Matthew McAuliffe", " ", "http://mipav.cit.nih.gov/"));
		info.setDescription("Global registration algorithm based on FLIRT. ");
		info.setLongDescription("Finds a linear transformation bringing the source volume into the space of the target volume. Degrees of freedom can be specified. This is a wrapper around MIPAV's implementation of FLIRT.");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.Release);
		info.add(new Citation("Jenkinson, M. and Smith, S. \"A global optimisation method for robust affine registration of brain images.\" Medical Image Analysis, 5(2):143-156, 2001."));
 		// irrelevant? 		info.add(new Citation("Bjórn Hamre \"Three-dimensional image registration of magnetic resonance (MRI) head volumes\" Section for Medical Image Analysis and Informatics Department of Physiology & Department of Informatics University of Bergen, Norway."));
		info.add(new Citation("FLIRT, at http://www.fmrib.ox.ac.uk/fsl/flirt/ "));
		info.add(new Citation("Powell method to find the global minimum: http://math.fullerton.edu/mathews/n2003/PowellMethodMod.html"));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createOutputParameters(
	 * edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams
		.add(trans = new ParamMatrix("Transformation Matrix", 4, 4));
		outputParams.add(registered = new ParamVolume("Registered Volume"));
		// outputParams.add(mipavMatrix = new
		// ParamObject<String>("Mipav Matrix", new StringReaderWriter()));
		// mipavMatrix.setExtensionFilter(filter);
		// mipavMatrix.setName("mipavMatrix");
	}

	protected void execute(CalculationMonitor monitor)
	throws AlgorithmRuntimeException {

		System.out.println("FLIRT - execute");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


		ImageData src=source.getImageData();
		ImageData dest=target.getImageData();

		System.out.println("FLIRT - got data");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


		CompareVolumes vc=new CompareVolumes(src,dest,-1E30,1);
		vc.compare();
		if (vc.isComparable()&&vc.getMinError()==0&&vc.getMaxError()==0) {
			System.out.println("Same Image, no registration needed");
			//If images are the same, do not perform registration
			Matrix tmp = new Matrix(4, 4);
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					tmp.set(i, j, (i==j)?1:0);
				}
			}
			registered.setValue(src);
			trans.setValue(tmp);
			return;
		} 

		FlirtWrapper flirt = new FlirtWrapper();
		monitor.observe(flirt);
		flirt.execute();
	}

	protected class FlirtWrapper extends AbstractCalculation {
		public FlirtWrapper() {
			setLabel("FLIRT");
		}

		public void execute() {
			
			// TODO: make more efficient if inputs are of mipav type!
			System.out.println("FLIRT - wrapper");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


			ModelImage matchImage = source.getImageData().getModelImageCopy();
			ModelImage refImage = target.getImageData().getModelImageCopy();

			System.out.println("FLIRT - srctar");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			ModelImage refWeightImage = null;
			if(refWeight.getImageData()!=null)
				refWeightImage =refWeight.getImageData().getModelImageCopy();
			ModelImage inputWeightImage = null;
			if(inputWeight.getImageData()!=null)
				inputWeightImage =inputWeight.getImageData().getModelImageCopy();

			System.out.println("FLIRT - got data");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


			boolean maxOfMinResol = useMaxOfMinRes.getValue();
			int cost = 0;
			switch (costFunction.getIndex()) {
			case 0:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
				// case 0: cost = AlgorithmCostFunctions.CORRELATION_RATIO;
				// break;
			case 1:
				cost = AlgorithmCostFunctions.LEAST_SQUARES_SMOOTHED;
				// cost = AlgorithmCostFunctions.LEAST_SQUARES;
				// costName = "LEAST_SQUARES_SMOOTHED";
				break;
				// case 2: cost =
				// AlgorithmCostFunctions.MUTUAL_INFORMATION_SMOOTHED;
				// break;
			case 2:
				cost = AlgorithmCostFunctions.NORMALIZED_XCORRELATION_SMOOTHED;
				break;
				// case 3: cost =
				// AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION;
				// break;
			case 3:
				cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION_SMOOTHED;
				break;
			default:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
			}
			int DOF = 0;
			switch (dof.getIndex()) {
			case 0:
				DOF = 6;
				break;
			case 1:
				DOF = 7;
				break;
			case 2:
				DOF = 9;
				break;
			case 3:
				DOF = 12;
				break;
			default:
				DOF = 12;
				break;
			}
			int interp = 0;
			switch (inputInterpolation.getIndex()) {
			case 0:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp = AlgorithmTransform.WSINC;
				break;
			default:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			}
			int interp2 = 0;
			switch (outputInterpolation.getIndex()) {
			case 0:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp2 = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp2 = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp2 = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp2 = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp2 = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp2 = AlgorithmTransform.WSINC;
				break;
			case 7:
				interp2 = AlgorithmTransform.NEAREST_NEIGHBOR;
				break;
			default:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			}
			boolean fastMode = skipMultilevelSearch.getValue();
			float rotateBeginX = minAngle.getFloat();
			float rotateEndX = maxAngle.getFloat();
			float coarseRateX = coarseAngleIncrement.getFloat();
			float fineRateX = fineAngleIncrement.getFloat();
			boolean doSubsample = subsample.getValue();
			float rotateBeginY = 0;
			float rotateEndY = 0;
			float coarseRateY = 0;
			float fineRateY = 0;
			float rotateBeginZ = 0;
			float rotateEndZ = 0;
			float coarseRateZ = 0;
			float fineRateZ = 0;

			switch (rotDim.getIndex()) {
			case 0:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = rotateBeginX;
				rotateEndY = rotateEndX;
				rotateEndZ = rotateEndX;
				coarseRateY = coarseRateX;
				coarseRateZ = coarseRateX;
				fineRateY = fineRateX;
				fineRateZ = fineRateX;
				break;
			case 1:
				rotateBeginY = 0;
				rotateBeginZ = 0;
				rotateEndY = 0;
				rotateEndZ = 0;
				coarseRateY = 0;
				coarseRateZ = 0;
				fineRateY = 0;
				fineRateZ = 0;
				break;
			case 2:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = 0;
				rotateEndY = rotateEndX;
				rotateEndZ = 0;
				coarseRateY = coarseRateX;
				coarseRateZ = 0;
				fineRateY = fineRateX;
				fineRateZ = 0;
				rotateBeginX = 0;
				rotateEndX = 0;
				coarseRateX = 0;
				fineRateX = 0;
				break;
			case 3:
				rotateBeginZ = rotateBeginX;
				rotateBeginY = 0;
				rotateEndZ = rotateEndX;
				rotateEndY = 0;
				coarseRateZ = coarseRateX;
				coarseRateY = 0;
				fineRateZ = fineRateX;
				fineRateY = 0;
				rotateBeginX = 0;
				rotateEndX = 0;
				coarseRateX = 0;
				fineRateX = 0;
				break;
			}
			int bracketBound = bracketMinimum.getInt();
			int maxIterations = iters.getInt();
			int numMinima = level8to4.getInt();
			TransMatrix finalMatrix;
			AlgorithmRegOAR3DWrapper reg3;

			boolean weighted = (refWeightImage != null || inputWeightImage != null);

			System.out.println("FLIRT - before new");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


			if (weighted) {
				reg3 = new AlgorithmRegOAR3DWrapper(refImage, matchImage,
						refWeightImage, inputWeightImage, cost, DOF,
						interp, rotateBeginX, rotateEndX, coarseRateX,
						fineRateX, rotateBeginY, rotateEndY, coarseRateY,
						fineRateY, rotateBeginZ, rotateEndZ, coarseRateZ,
						fineRateZ, maxOfMinResol, doSubsample, fastMode,
						bracketBound, maxIterations, numMinima);
			} else {
				reg3 = new AlgorithmRegOAR3DWrapper(refImage, matchImage,
						cost, DOF, interp, rotateBeginX, rotateEndX,
						coarseRateX, fineRateX, rotateBeginY, rotateEndY,
						coarseRateY, fineRateY, rotateBeginZ, rotateEndZ,
						coarseRateZ, fineRateZ, maxOfMinResol, doSubsample,
						fastMode, bracketBound, maxIterations, numMinima);

			}
			System.out.println("FLIRT - before call");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			reg3.setObserver(this);
			reg3.setMultiThreadingEnabled(OARThreadedBool.getValue());
			reg3.run();
			reg3.setObserver(null);
			System.out.println("FLIRT - after call");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			finalMatrix = reg3.getTransform();

			reg3.disposeLocal();
			reg3=null;
			System.out.println("FLIRT - after dispose reg3");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			int xdimA = refImage.getExtents()[0];
			int ydimA = refImage.getExtents()[1];
			int zdimA = refImage.getExtents()[2];
			float xresA = refImage.getFileInfo(0).getResolutions()[0];
			float yresA = refImage.getFileInfo(0).getResolutions()[1];
			float zresA = refImage.getFileInfo(0).getResolutions()[2];
			String name = matchImage
					.getImageName()+ "_reg";
			Matrix tmp = new Matrix(4, 4);
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					tmp.set(i, j, finalMatrix.get(i, j));
				}
			}
			System.out.println("Flirt Method "
					+ finalMatrix.matrixToString(4, 4) + " (" + xresA + ","
					+ yresA + "," + zresA + ") (" + xdimA + "," + ydimA
					+ "," + zdimA + ")");

			System.out.println("FLIRT - before new trans");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


			AlgorithmTransform transform = new AlgorithmTransform(
					matchImage, finalMatrix, interp2, xresA, yresA, zresA,
					xdimA, ydimA, zdimA, true, false, false);
			transform.setUpdateOriginFlag(true);

			System.out.println("FLIRT - before call trans");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			transform.run();

			System.out.println("FLIRT - after call trans");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			ModelImage resultImage = transform.getTransformedImage();
			transform.finalize();
			resultImage.getFileInfo(0).setAxisOrientation(
					refImage.getFileInfo(0).getAxisOrientation());
			resultImage.getFileInfo(0).setImageOrientation(
					refImage.getFileInfo(0).getImageOrientation());
			resultImage.calcMinMax();			
			MipavController.setModelImageName(resultImage,name);

			System.out.println("FLIRT - before set value");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();


			registered.setValue(new ImageDataMipavWrapper(resultImage));

			System.out.println("FLIRT - after set value");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			transform.disposeLocal();

			System.out.println("FLIRT - after dispose trans");
			edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

			trans.setValue(tmp);
			matchImage.disposeLocal();
			refImage.disposeLocal();
			if(inputWeightImage!=null)
				inputWeightImage.disposeLocal();
			if(refWeightImage!=null)
				refWeightImage.disposeLocal();
		}
	}

	protected static class AlgorithmRegOAR3DWrapper extends AlgorithmRegOAR3D {
		public AlgorithmRegOAR3DWrapper(ModelImage _imagea, ModelImage _imageb,
				int choice, int _dof, int _interp, float beginX, float endX,
				float rateX, float rateX2, float beginY, float endY,
				float rateY, float rateY2, float beginZ, float endZ,
				float rateZ, float rateZ2, boolean resol, boolean subsample,
				boolean mode, int bound, int numIter, int minima) {
			super(_imagea, _imageb, choice, _dof, _interp, beginX, endX, rateX,
					rateX2, beginY, endY, rateY, rateY2, beginZ, endZ, rateZ,
					rateZ2, resol, subsample, mode, bound, numIter, minima);
		}

		/**
		 * @param arg0
		 * @param arg1
		 * @param arg2
		 * @param arg3
		 * @param arg4
		 * @param arg5
		 * @param arg6
		 * @param arg7
		 * @param arg8
		 * @param arg9
		 * @param arg10
		 * @param arg11
		 * @param arg12
		 * @param arg13
		 * @param arg14
		 * @param arg15
		 * @param arg16
		 * @param arg17
		 * @param arg18
		 * @param arg19
		 * @param arg20
		 * @param arg21
		 * @param arg22
		 * @param arg23
		 * @param arg24
		 */
		public AlgorithmRegOAR3DWrapper(ModelImage arg0, ModelImage arg1,
				ModelImage arg2, ModelImage arg3, int arg4, int arg5, int arg6,
				float arg7, float arg8, float arg9, float arg10, float arg11,
				float arg12, float arg13, float arg14, float arg15,
				float arg16, float arg17, float arg18, boolean arg19,
				boolean arg20, boolean arg21, int arg22, int arg23, int arg24) {
			super(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9,
					arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17,
					arg18, arg19, arg20, arg21, arg22, arg23, arg24);
		}

		/**
		 * @param _imagea
		 * @param _imageb
		 * @param weight
		 * @param weight2
		 * @param choice
		 * @param _dof
		 * @param _interp
		 * @param beginX
		 * @param endX
		 * @param rateX
		 * @param rateX2
		 * @param beginY
		 * @param endY
		 * @param rateY
		 * @param rateY2
		 * @param beginZ
		 * @param endZ
		 * @param rateZ
		 * @param rateZ2
		 * @param resol
		 * @param subsample
		 * @param mode
		 * @param bound
		 * @param numIter
		 * @param minima
		 */
		protected AbstractCalculation observer=null;

		public void setObserver(AbstractCalculation observer) {
			this.observer = observer;
		}

		public void runAlgorithm() {
			if(observer!=null)
				observer.setTotalUnits(100);
			super.runAlgorithm();
			if(observer!=null)
				observer.markCompleted();
		}

		/**
		 * Notifies all listeners that have registered interest for notification
		 * on this event type.
		 * 
		 * @param value
		 *            the value of the progress bar.
		 */
		protected void fireProgressStateChanged(int value) {
			super.fireProgressStateChanged(value);
			if(observer!=null)
				observer.setCompletedUnits(value);
		}

		/**
		 * Updates listeners of progress status. Without actually changing the
		 * numerical value
		 * 
		 * @param imageName
		 *            the name of the image
		 * @param message
		 *            the new message to display
		 */
		protected void fireProgressStateChanged(String imageName, String message) {
			super.fireProgressStateChanged(imageName, message);
			if(observer!=null)
				observer.setLabel(message);
		}
	}
}
