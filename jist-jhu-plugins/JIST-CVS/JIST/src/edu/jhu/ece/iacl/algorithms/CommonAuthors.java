package edu.jhu.ece.iacl.algorithms;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;

public class CommonAuthors {
	public static AlgorithmAuthor blakeLucas=new AlgorithmAuthor("Blake Lucas","blake@cs.jhu.edu","");
	public static AlgorithmAuthor chenyangXu=new AlgorithmAuthor("Chenyang Xu","chenyang.xu@siemens.com","http://www.iacl.ece.jhu.edu/~chenyang/");
	public static AlgorithmAuthor xiaoHan=new AlgorithmAuthor("Xiao Han","xhan@nmr.mgh.harvard.edu","http://www.iacl.ece.jhu.edu/~xhan/");
	public static AlgorithmAuthor jerryPrince=new AlgorithmAuthor("Jerry L. Prince","prince@jhu.edu","http://iacl.ece.jhu.edu/");
	public static AlgorithmAuthor duyguTosun=new AlgorithmAuthor("Duygu Tosun","duygu.tosun@loni.ucla.edu","");
	public static AlgorithmAuthor baiYing=new AlgorithmAuthor("Bai Ying","yingbai@jhu.edu","");
	public static AlgorithmAuthor bennettLandman=new AlgorithmAuthor("Bennett Landman","bennett.landman@vanderbilt.edu","");
	public static AlgorithmAuthor xiaofengLiu=new AlgorithmAuthor("Xiaofend Liu","xiaofeng@jhu.edu","");
	public static AlgorithmAuthor johnBogovic=new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu","http://putter.ece.jhu.edu/John");
	public static AlgorithmAuthor xianFan=new AlgorithmAuthor("Xian Fan","xfan7@jhu.edu","");
	public static AlgorithmAuthor snehashisRoy=new AlgorithmAuthor("Snehashis Roy","snehashisr@jhu.edu","");
	public static AlgorithmAuthor navidShiee=new AlgorithmAuthor("Navid Shiee","navid@jhu.edu","http://medic.rad.jhmi.edu/nshiee/");
	public static AlgorithmAuthor jingWan=new AlgorithmAuthor("Jing Wan","jingwan@jhu.edu","");
	public static AlgorithmAuthor maryamRettmann=new AlgorithmAuthor("Maryam Rettmann","Rettmann.Maryam@mayo.edu","");
	public static AlgorithmAuthor lottaEllingsen=new AlgorithmAuthor("Lotta Ellingsen","lelling1@jhu.edu",""); 
	public static AlgorithmAuthor dzungPham=new AlgorithmAuthor("Dzung Pham","pham@jhu.edu","http://medic.rad.jhmi.edu/dpham");
	public static AlgorithmAuthor pierreLouisBazin=new AlgorithmAuthor("Pierre-Louis Bazin","pbazin1@jhmi.edu","http://medic.rad.jhmi.edu/pbazin/");
	public static AlgorithmAuthor minChen=new AlgorithmAuthor("Min Chen","dontminchenit@gmail.com","");
	public static AlgorithmAuthor bryanWheeler=new AlgorithmAuthor("Bryan Wheeler","","");
}
