package edu.jhu.ece.iacl.algorithms.dti;

public class GTCparams {
	


	public String flag;
	public String scanner;
	public String grad_choice;
	public String fs;
	public String fo;
	public String slice_orient;
	public double[][] sliceAng;
	public String patient_orientation;
	public String patient_position;
	public String xfmpath;
	public boolean sort_image;
	public String supplied_grad_file;
	public String parfile;
	public String xfmname;
	public String date;  //YYYYMMDD
	public String parversion;
	public String bvals;
	public boolean DTIstudio = true;
	public int numvolumes;
	public String release;
	public String userTable;
	
	public GTCparams(){
	}
	
	public void printParams(){
		System.out.println(getClass().getCanonicalName()+"\t"+flag);
		System.out.println(getClass().getCanonicalName()+"\t"+scanner);
		System.out.println(getClass().getCanonicalName()+"\t"+grad_choice);
		System.out.println(getClass().getCanonicalName()+"\t"+fs);
		System.out.println(getClass().getCanonicalName()+"\t"+fo);
		System.out.println(getClass().getCanonicalName()+"\t"+slice_orient);
		System.out.println(getClass().getCanonicalName()+"\t"+patient_orientation);
		System.out.println(getClass().getCanonicalName()+"\t"+patient_position);
		System.out.println(getClass().getCanonicalName()+"\t"+sort_image);
		System.out.println(getClass().getCanonicalName()+"\t"+numvolumes);
		System.out.println(getClass().getCanonicalName()+"\t"+release);
		System.out.println(getClass().getCanonicalName()+"\t"+sliceAng[0][0]+","+sliceAng[1][0]+","+sliceAng[2][0]);
		System.out.println(getClass().getCanonicalName()+"\t"+parversion);
	}
	
	public void setFlag(String flag){
		if(flag=="KIRBY"){
			this.flag=flag;
		}else if(flag=="Other"){
			this.flag=flag;
		}else{
			System.err.println(getClass().getCanonicalName()+"Invalid flag - must be {'KIRBY','Other'}");
		}
	}
	public void setScanner(String scanner){
		if(scanner=="1.5"){
			this.scanner=scanner;
		}else if(scanner=="3.0"){
			this.scanner=scanner;
		}else{
			System.err.println(getClass().getCanonicalName()+"Invalid scaner - must be {'1.5','3.0'}");
		}
	}
	public void setgrad_choice(String grad){
		if(grad=="Jones 30"){
			if(flag=="KIRBY"){
				grad_choice=grad;
			}else{
				System.err.println(getClass().getCanonicalName()+"Invalid Gradient - Jones30 valid for KKI Only");
			}
		}
		else if(grad=="Phillips No Overplus High"){
			grad_choice=grad;
		}
		else if(grad=="Phillips No Overplus Medium"){
			grad_choice=grad;
		}
		else if(grad=="Phillips No Overplus Low"){
			grad_choice=grad;
		}
		else if(grad=="Phillips Yes Overplus High"){
			grad_choice=grad;
		}
		else if(grad=="Phillips Yes Overplus Medium"){
			grad_choice=grad;
		}
		else if(grad=="Phillips Yes Overplus Low"){
			grad_choice=grad;
		}
		else if(grad=="Custom"){
			grad_choice=grad;
		}
		else{
			System.err.println(getClass().getCanonicalName()+"Invalid Gradient!");
		}
	}

	

	public boolean verifyParams(){
		//Check if Foldover and Fatshift agree
		if(fo=="Anterior-Posterior"){
			if(!(fs=="Anterior" || fs=="Posterior")){
				System.err.println(getClass().getCanonicalName()+"Provided values for foldover and fatshift are incompatible");
				return false;
			}
		}else if(fo=="Right-Left"){
			if(!(fs=="Right" || fs=="Left")){
				System.err.println(getClass().getCanonicalName()+"Provided values for foldover and fatshift are incompatible");
				return false;
			}
		}else if(fo=="Superior-Inferior"){
			if(!(fs=="Superior" || fs=="Inferior")){
				System.err.println(getClass().getCanonicalName()+"Provided values for foldover and fatshift are incompatible");
				return false;
			}
		}
		
		//Check if SliceAngulation and Foldover agree
		if(slice_orient=="Axial"){
			if(fo=="Superior-Inferior"){
				System.err.println(getClass().getCanonicalName()+"Superior-Inferior foldover and Axial Slice Angulation are incompatible");
				return false;
			}
		}else if(slice_orient=="Sagittal"){
			if(fo=="Right-Left"){
				System.err.println(getClass().getCanonicalName()+"Right-Left foldover and Sagittal Slice Angulation are incompatible");
				return false;
			}
		}else if(slice_orient=="Coronal"){
			if(fo=="Anterior-Posterior"){
				System.err.println(getClass().getCanonicalName()+"Anterior-Posterior foldover and Coronal Slice Angulation are incompatible");
				return false;
			}
		}
		
//		Check if Foldover and Fatshift agree
		if(fo=="AP"){
			if(!(fs=="A" || fs=="P")){
				System.err.println(getClass().getCanonicalName()+"Provided values for foldover and fatshift are incompatible");
				return false;
			}
		}else if(fo=="RL"){
			if(!(fs=="R" || fs=="L")){
				System.err.println(getClass().getCanonicalName()+"Provided values for foldover and fatshift are incompatible");
				return false;
			}
		}else if(fo=="HF"){
			if(!(fs=="H" || fs=="F")){
				System.err.println(getClass().getCanonicalName()+"Provided values for foldover and fatshift are incompatible");
				return false;
			}
		}
		
		//Check if SliceAngulation and Foldover agree
		if(slice_orient=="tra"){
			if(fo=="HF"){
				System.err.println(getClass().getCanonicalName()+"Superior-Inferior foldover and Axial Slice Angulation are incompatible");
				return false;
			}
		}else if(slice_orient=="sag"){
			if(fo=="RL"){
				System.err.println(getClass().getCanonicalName()+"Right-Left foldover and Sagittal Slice Angulation are incompatible");
				return false;
			}
		}else if(slice_orient=="cor"){
			if(fo=="AP"){
				System.err.println(getClass().getCanonicalName()+"Anterior-Posterior foldover and Coronal Slice Angulation are incompatible");
				return false;
			}
		}
		
		return true;
	}
	
	public static final double[][] getLowOP(){
		return new double[][]{{-0.9428,-0.4714, 0.9428},
				 {0.9428,-0.9428, 0.4714},
					{-1.0000,-1.0000, 0.0000},
					 {0.0000,-1.0000, 1.0000},
					 {1.0000, 0.0000, 1.0000}};
		
	}
	public static final double[][] getMediumOP(){
		return new double[][]{{-0.7071,-0.7071,-1.0000},
		{-0.7071,-0.7071, 1.0000},
		 {1.0000,-1.0000, 0.0000},
		{-0.1561,-0.9999,-0.9879},
		 {0.4091,-0.9894,-0.9240},
		 {0.8874,-0.4674,-0.9970},
		 {0.9297,-0.3866,-0.9930},
		{-0.9511,-0.7667,-0.7124},
		 {0.9954,-0.6945, 0.7259},
		{-0.9800,-0.3580, 0.9547},
		{-0.9992,-1.0000, 0.0392},
		{-0.3989,-0.9999, 0.9171},
		 {0.4082,-0.9923, 0.9213},
		 {0.9982,-0.9989, 0.0759},
		 {0.9919,-0.2899, 0.9655}};
	}
	

	
	public static final double[][] getLowOP2(){
		return new double[][]{{0.9428,-0.4714,0.9428},
	     {0.4714,-0.9428,-0.9428},
	     {0.9428,0.9428,-0.4714},
	     {1.0,-1.0,0.0},
	     {1.0,0.0,-1.0},
	     {0.0,1.0,-1.0}};
	}
	
	public static final double[][] getMediumOP2(){
		return new double[][]{{0.7071,-0.7071,1.0000},
    	{0.7071,-0.7071,-1.0000},
    	{1.0000,1.0000,0.0000},
    	{0.9999,-0.1561,0.9879},
    	{0.9894,0.4091,0.9240},
    	{0.4674,0.8874,0.9970},
    	{0.3866,0.9297,0.9930},
    	{0.7667,-0.9511,0.7124},
    	{0.6945,0.9954,-0.7259},
    	{0.3580,-0.9800,-0.9547},
    	{1.0000,-0.9992,-0.0392},
    	{0.9999,-0.3989,-0.9171},
    	{0.9923,0.4082,-0.9213},
    	{0.9989,0.9982,-0.0759},
    	{0.2899,0.9919,-0.9655}};
		
	}
	
	public static final double[][] getHighOP_24prev(){
		return new double[][]{{-0.70710,-0.70710,-1.00000},
		{-0.70710,-0.70710, 1.00000},
		{ 1.00000,-1.00000, 0.00000},
		{-0.92390,-0.38270,-1.00000},
		{-0.29510,-0.95550,-1.00000},
		{ 0.02780,-0.99960,-1.00000},
		 { 0.59570,-0.80320,-1.00000},
		 { 0.97570,-0.21910,-1.00000},
		{-0.92420,-0.38280,-0.99970},
		{-0.41420,-1.00000,-0.91020},
		{ 0.41650,-0.99900,-0.91020},
		 { 0.72830,-0.68740,-0.99850},
		 { 1.00000,-0.41420,-0.91020},
		{-1.00000,-0.66820,-0.74400},
		{-0.66820,-1.00000,-0.74400},
		{ 0.78560,-0.91070,-0.74400},
		 { 1.00000,-0.66820,-0.74400},
		{-1.00000,-1.00000,-0.00030},
		{-1.00000,-0.66820, 0.74400},
		{ 1.00000,-0.66820, 0.74400},
		 { 0.66820,-1.00000, 0.74400},
		 { 1.00000,-0.66820, 0.74400},
		{-0.90000,-0.60130, 0.91020},
		{-0.99850,-0.99850, 0.07740},
		{-0.41420,-1.00000, 0.91020},
		{ 0.41420,-1.00000, 0.91020},
		 { 1.00000,-1.00000, 0.01110},
		 { 1.00000,-0.41420, 0.91020},
		{-0.99880,-0.99880, 0.06920},
		{ 0.04910,-0.99880, 1.00000},
		 { 0.99990,-0.99990, 0.01630},
		 {1.00000, 0.00000, 1.00000}};
	}
	
	public static final double[][] getHighOP_rel25(){
		return new double[][]{ 
				 {-0.70710,-0.70710,-1.00000},
					{-0.70710,-0.70710, 1.00000},
					 {1.00000,-1.00000, 0.00000},
					{-0.92390,-0.38270,-1.00000},
					{-0.29510,-0.95550,-1.00000},
					 {0.02780,-0.99960,-1.00000},
					 {0.59570,-0.80320,-1.00000},
					 {0.97570,-0.21910,-1.00000},
					{-0.92420,-0.38280,-0.99970},
					{-0.41420,-1.00000,-0.91020},
					 {0.41650,-0.99900,-0.91020},
					 {0.72830,-0.68740,-0.99850},
					 {1.00000,-0.41420,-0.91020},
					{-1.00000,-0.66820,-0.74400},
					{-0.66820,-1.00000,-0.74400},
					 {0.78560,-0.91070,-0.74400},
					 {1.00000,-0.66820,-0.74400},
					{-1.00000,-1.00000,-0.00030},
					{-1.00000,-0.66820, 0.74400},
					 {1.00000,-0.66820, 0.74400},
					 {0.66820,-1.00000, 0.74400},
					{-1.00000,-1.00000, 0.01110},
					{-0.90000,-0.60130, 0.91020},
					{-0.99850,-0.99850, 0.07740},
					{-0.41420,-1.00000, 0.91020},
					 {0.41420,-1.00000, 0.91020},
					 {1.00000,-1.00000, 0.01110},
					 {1.00000,-0.41420, 0.91020},
					{-0.99880,-0.99880, 0.06920},
					 {0.04910,-0.99880, 1.00000},
					 {0.99990,-0.99990, 0.01630},
					 {1.00000, 0.00000, 1.00000}};
	}
	
	public static final double[][] getHighOP_25post(){
		return new double[][]{{0.3827,-0.9239,1.0000},
	      {0.9555,-0.2951,1.0000},
	      {0.9996,0.0278,1.0000},
	      {0.8032,0.5957,1.0000},
	      {0.2191,0.9757,1.0000},
	      {0.3828,-0.9242,0.9997},
	      {0.7071,-0.7071,1.0000},
	      {1.0000,-0.4142,0.9102},
	      {0.9990,0.4165,0.9102},
	      {0.6874,0.7283,0.9985},
	      {0.4142,1.0000,0.9102},
	      {0.6682,-1.0000,0.7440},
	      {1.0000,-0.6682,0.7440},
	      {0.9107,0.7856,0.7440},
	      {0.6682,1.0000,0.7440},
	      {1.0000,-1.0000,0.0003},
	      {1.0000,1.0000,0.0000},
	      {0.6682,-1.0000,-0.7440},
	      { 0.6682,1.0000,-0.7440},
	      { 1.0000,0.6682,-0.7440},
	      { 0.6682,1.0000,-0.7440},
	      { 0.6013,-0.9000,-0.9102},
	      { 0.9985,-0.9985,-0.0774},
	      {1.0000,-0.4142,-0.9102},
	      {  1.0000,0.4142,-0.9102},
	      { 1.0000,1.0000,-0.0111},
	      { 0.4142,1.0000,-0.9102},
	      { 0.5624,-0.8269,-1.0000},
	      { 0.9988,-0.9988,-0.0692},
	      { 0.9988,0.0491,-1.0000},
	      { 0.9999,0.9999,-0.0163},
	      {0.0000,1.0000,-1.0000}	
		 };
	}
	
	public static final double[][] getJones30(){
		return new double[][]{{1,0,0},
                {0.166,0.986,0},             
                {-0.110,0.664,0.740},   
                {0.901,-0.419,-0.110},      
                {-0.169,-0.601, 0.781},    
                {-0.815, -0.386, 0.433},
                {0.656, 0.366, 0.660},
                {0.582, 0.800, 0.143},
                {0.900, 0.259, 0.350},
                {0.693, -0.698, 0.178},
                {0.357, -0.924, -0.140},
                {0.543, -0.488, -0.683},
                {-0.525, -0.396, 0.753},
                {-0.639, 0.689, 0.341},
                {-0.330, -0.013, -0.944},
                {-0.524, -0.783, 0.335},
                {0.609, -0.065, -0.791},
                {0.220, -0.233, -0.947},
                {-0.004, -0.910, -0.415},
                {-0.511, 0.627, -0.589},
                {0.414, 0.737, 0.535},
                {-0.679, 0.139, -0.721},
                {0.884, -0.296, 0.362},
                {0.262, 0.432, 0.863},
                {0.088, 0.185, -0.979},
                {0.294, -0.907, 0.302},
                {0.887, -0.089, -0.453},
                {0.257, -0.443, 0.859},
                {0.086, 0.867, -0.491},
                {0.863, 0.504, -0.025}};
	}
	
	public static final double[][] getJones30VMS(){
		return new double[][]{{1,0,0},
                {0.166,0.986,0},             
                {-0.110,0.664,0.740},   
                {0.901,-0.419,-0.110},      
                {-0.169,-0.601, 0.781},    
                {-0.815, -0.386, 0.433},
                {0.656, 0.366, 0.660},
                {0.582, 0.800, 0.143},
                {0.900, 0.259, 0.350},
                {0.693, -0.698, 0.178},
                {0.357, -0.924, -0.140},
                {0.543, -0.488, -0.683},
                {-0.525, -0.396, 0.753},
                {-0.639, 0.689, 0.341},
                {-0.330, -0.013, -0.944},
                {-0.524, -0.783, 0.335},
                {-0.609, -0.065, -0.791},
                {0.220, -0.233, -0.947},
                {-0.004, -0.910, -0.415},
                {-0.511, 0.627, -0.589},
                {0.414, 0.737, 0.535},
                {-0.679, 0.139, -0.721},
                {0.884, -0.296, 0.362},
                {0.262, 0.432, 0.863},
                {0.088, 0.185, -0.979},
                {0.294, -0.907, 0.302},
                {0.887, -0.089, -0.453},
                {0.257, -0.443, 0.859},
                {0.086, 0.867, -0.491},
                {0.863, 0.504, -0.025}};
	}
	
	public static final double[][] getLow(){
		return new double[][]{
				 {1.0,0.0,0.0},
				 { 0.0,1.0,0.0},
                 { 0.0,0.0,1.0},
                {-0.7044,-0.0881,-0.7044},
                 {0.7044,0.7044,0.0881},
                 {0.0881,0.7044,0.7044}};
	}
	
	public static final double[][] getMedium(){
		return new double[][]{
				{1.0,0.0,0.0},
				{ 0.0,1.0,0.0},
                { 0.0,0.0,1.0},
                { -0.1789,-0.1113,-0.9776},
               { -0.0635,0.3767,-0.9242},
               {  0.710,0.0516,-0.7015},
                {  0.6191,-0.4385,-0.6515},  
                {  0.2424,0.7843,-0.5710},
                { -0.2589,-0.6180,-0.7423},
               { -0.8169,0.1697,-0.5513},
               { -0.8438,0.5261,-0.1060},
               { -0.2626,0.9548,-0.1389},
               { 0.0001,0.9689,0.2476},
                { 0.7453,0.6663,0.0242},
                {0.9726,0.2317,0.0209}};
	}
	
	public static final double[][] getHigh(){
		return new double[][]{
				{1.0,0.0,0.0},
				{ 0.0,1.0,0.0},
                { 0.0,0.0,1.0},
                { -0.0424,-0.1146,-0.9925},
               {  0.1749,-0.2005,-0.9639},
                { 0.2323,-0.1626,-0.9590},
                { 0.3675,0.0261,-0.9296},
                { 0.1902,0.3744,-0.9076},
                {  -0.1168,0.8334,-0.5402},
               {  -0.2005,0.2527,-0.9466},
               {  -0.4958,0.1345,-0.8580},
               { -0.0141,-0.6281,-0.7780},
               { -0.7445,-0.1477,-0.6511},
               { -0.7609,0.3204,-0.5643},
               { -0.1809,0.9247,-0.3351},
               {-0.6796,-0.4224,-0.5997},
               { 0.7771,0.4707,-0.4178},
                { 0.9242,-0.1036,-0.3677},
                { 0.4685,-0.7674,-0.4378},
                {  0.8817,-0.1893,-0.4322},
                {  0.6904,0.7062,-0.1569},
                {  0.2391,0.7571,-0.6080},
                { -0.0578,0.9837,0.1703},
               { -0.5368,0.8361,-0.1135},
               { -0.9918,-0.1207,-0.0423},
               { -0.9968,0.0709,-0.0379},
               { -0.8724,0.4781,-0.1014},
               { -0.2487,0.9335,0.2581},
               { 0.1183,0.9919,-0.0471},
                { 0.3376,0.8415,0.4218},
                { 0.5286,0.8409,0.1163},
                { 0.9969,0.0550,-0.0571} };
	}
	
	public static double[][] invertTable(double[][] table){
		double[][] inverted = new double[table.length][table[0].length];
		for(int i=0; i<table.length; i++){
			for(int j=0; j<table[0].length; j++){
				inverted[i][j]=-table[i][j];
			}
		}
		return inverted;
	}
	
	public static double[][] flipComponents(double[][] table, boolean invertx, boolean inverty, boolean invertz){
		double[][] inverted = new double[table.length][table[0].length];
		for(int i=0; i<table.length; i++){
			if(invertx){
				inverted[i][0]=-table[i][0];
			}else{
				inverted[i][0]=table[i][0];
			}
			if(inverty){
				inverted[i][1]=-table[i][1];
			}else{
				inverted[i][1]=table[i][1];
			}
			if(invertz){
				inverted[i][2]=-table[i][2];
			}else{
				inverted[i][2]=table[i][2];
			}
		}
		return inverted;
	}
	
}
