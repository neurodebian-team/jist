package edu.jhu.ece.iacl.algorithms.dti;

import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

public class DtiVecMask extends AbstractCalculation{
	
	ArrayList<Number> labels;
	
	public DtiVecMask(AbstractCalculation parent){
		super(parent);
		setLabel("Vector Mask");
	}
	public DtiVecMask(){
		super();
		setLabel("Vector Mask");
	}
	
	public ImageData getSubsetDTISformat(ImageData vec, ImageData mask, Number target){
		
		int xn = vec.getRows();
		int yn = vec.getCols();
		int zn = vec.getSlices();
		int cn = vec.getComponents();
		ImageData f=vec.mimic();
//		System.out.println(getClass().getCanonicalName()+"\t"+"Original");
		for(int i=0;i<cn;i++){
			for(int j=0;j<zn;j++){
				for(int k=0;k<yn;k++){
					if(mask.get(i,j,k).byteValue()==target.byteValue()){
						for(int l=0;l<xn;l++){
							f.set(i, j, k, l, vec.getDouble(i, j, k, l));
						}
					}
				}
			}
		}
		return f;
	}
	
	public ImageData getSubsetMIPAVformat(ImageData vec, ImageData mask, Number target){
		labels = new ArrayList<Number>();
		System.out.println(getClass().getCanonicalName()+"\t"+"Applying Mask...");
		int xn = vec.getRows();
		int yn = vec.getCols();
		int zn = vec.getSlices();
		int cn = vec.getComponents();
		System.out.println(getClass().getCanonicalName()+"\t"+"Vec Size: " + xn + "," + yn + "," + zn + "," + cn);
		ImageData f=vec.mimic();
//		System.out.println(getClass().getCanonicalName()+"\t"+"Original");
		for(int i=0;i<xn;i++){
			for(int j=0;j<yn;j++){
				for(int k=0;k<zn;k++){
//					if(!labels.contains(mask.get(i,j,k))){
//						labels.add(mask.get(i,j,k));
//						System.out.println(getClass().getCanonicalName()+"\t"+mask.get(i,j,k));
//					}
//					if(mask.get(i, j, k).byteValue()!=0 && mask.get(i, j, k).byteValue()!=1){
//						System.out.println(getClass().getCanonicalName()+"\t"+mask.get(i, j, k).byteValue());
//					}
					if(mask.get(i,j,k).byteValue()==target.byteValue()){
//						System.out.println(getClass().getCanonicalName()+"\t"+"Copying at " + i + "," + j + "," + k);
						for(int l=0;l<cn;l++){
							f.set(i, j, k, l, vec.getDouble(i, j, k, l));
						}
					}
				}
			}
		}
		return f;
	}
	
	public float[][][][] getSubsetArray(float[][][][] vec, int[][][] mask, int target){
		float[][][][] vecout = new float[vec.length][vec[0].length][vec[0][0].length][vec[0][0][0].length];
		int xn = vec.length;
		int yn = vec[0].length;
		int zn = vec[0][0].length;
		int cn = vec[0][0][0].length;
		for(int i=0;i<cn;i++){
			for(int j=0;j<zn;j++){
				for(int k=0;k<yn;k++){
					if (mask[k][j][i] == target){
						for(int l=0;l<xn;l++){
							
							vecout[l][k][j][i]=vec[l][k][j][i];
//							System.out.println(getClass().getCanonicalName()+"\t"+vecout[l][k][j][i]);
						}
					}
				}
			}
		}
		return vecout;
	}

	
}
