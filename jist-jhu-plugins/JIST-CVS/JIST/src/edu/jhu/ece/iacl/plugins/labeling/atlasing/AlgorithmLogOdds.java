package edu.jhu.ece.iacl.plugins.labeling.atlasing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.manual_label.atlasing.LogOdds;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;


public class AlgorithmLogOdds extends ProcessingAlgorithm {
	private ParamVolumeCollection input;

	private ParamInteger maxiters;
	private ParamInteger Inorm;
	private ParamFloat epsilon;
	private ParamOption inverse;

	private ParamVolume output;

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Computes the Log Odds and inverse Log Odds transforms of probabilistic atlaes.";
	private static final String longDescription = "It depends on the selection of the 'inverse' input.\nDesigned for both 4D probabilistic atlas images (the fourth dimension encodes the label) or collections of 3D probability volumes.";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(input=new ParamVolumeCollection("Input (ProbAtlas or LogOdds)"));
		inputParams.add(inverse=new ParamOption("Initialization Type", new String[]{"Logit","Inverse Logit"}));

		inputParams.add(Inorm=new ParamInteger("Index of Normalizing Label",0));
		
		float eps = Float.MIN_VALUE;
		inputParams.add(epsilon=new ParamFloat("Epsilon",eps));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.Atlasing");
		inputParams.setName("logodds");
		inputParams.setLabel("Log of Odds");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setAffiliation("Johns Hopkins University, Department of Electrical Engineering");
		info.add(new Citation("Pohl KM, Fisher J, Bouix S, Shenton M, McCarley RW, Grimson, WEL, Kikinis R, Wells WM, \"Using the logarithm of odds" +
				"to define a vector space on probabilistic atlases\" MIA 2007; 11(5):465-77 "));	
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {	
		outputParams.add(output=new ParamVolume("Output (LogOdds or ProbAtlas)", null, -1, -1, -1, -1));
		
	}


	protected void execute(CalculationMonitor monitor) {
		//set the output directory and create it if it doesn't exist
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){ e.printStackTrace(); }


		// LOG ODDS PARAMETERS 
		int indNorm = Inorm.getInt();
		boolean fourthDimLab = true;		//always true for now

		//do some fancy stuff with epsilon
		//IF EPSILON IS < 0 TAKE IT TO BE EQUAL TO THAT (POSITIVE) MULTIPLE
		//OF FLOAT.MINVALUE
		float epsparam = epsilon.getFloat();
		float eps = Float.NaN; //this is the epsilon to be used in the LogOdds computation
		if(epsparam<0){
			eps = epsparam*Float.MIN_VALUE;
		}else if(epsparam==0){
			// having a zero value of epsilon is not good.
			//spit out a warning and do nothing...
			System.err.println(getClass().getCanonicalName()+"WARNING, EPSILON IS ZERO!!!");
			
			//OR SET EQUAL TO A DEFAULT VALUE
//			eps = Float.MIN_VALUE*100;
		}else{
			eps = epsparam;
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"Epsilon: " + eps);

		// ORGANIZE INPUT DATA
		//if we get a volume collection, turn it into a 4D volume
		//(labels vary across the fourth dim)
		ImageData alginput;
		if(input.getImageDataList().size()>1){
			alginput = volCollectToVol4d(input.getImageDataList());
		}else{
			if(inverse.getIndex()==0 && input.getImageDataList().get(0).getComponents()<=1){
				System.err.println(getClass().getCanonicalName()+"Requires a multidimensional input volume for LogOdds transform!");
				return;
			}else{
				alginput = input.getImageDataList().get(0);
			}
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"Number of Labels: " + alginput.getComponents());
		System.out.println(getClass().getCanonicalName()+"\t"+"Index of normalizing label: " + indNorm);
		// RUN THE ALGORITHM
		ImageDataFloat out;
		if(inverse.getIndex()==0){
			out = LogOdds.Logit(alginput, indNorm, eps, fourthDimLab,alginput.getName()+"_LogOdds",dir);
		}else{
			out = LogOdds.InvLogit(alginput, indNorm, fourthDimLab,alginput.getName()+"_invLogOdds");
		}
		output.setValue(out);
	}


	private ImageDataFloat volCollectToVol4d(List<ImageData> a){
		if(a.get(0).getComponents()>1){
			System.err.println(getClass().getCanonicalName()+"Volumes in volume collection must be 3d!");
			return null;
		}
		ImageDataFloat out = new ImageDataFloat(a.get(0).getName()+"_cat",a.get(0).getRows(),a.get(0).getCols(),a.get(0).getSlices(),a.size());
		ImageHeader copyme = a.get(0).getHeader();
		
		out.getHeader().setAxisOrientation(copyme.getAxisOrientation());
		out.getHeader().setDimResolutions(copyme.getDimResolutions());
		
		for(int l=0; l<a.size(); l++){
			System.out.println(getClass().getCanonicalName()+"\t"+"Including volume: " + a.get(l).getName());
			for(int k=0; k<out.getSlices(); k++){
				for(int j=0; j<out.getCols(); j++){
					for(int i=0; i<out.getRows(); i++){
						out.set(i,j,k,l,a.get(l).getFloat(i, j, k));
					}
				}
			}
		}
		return out;
	}
}
