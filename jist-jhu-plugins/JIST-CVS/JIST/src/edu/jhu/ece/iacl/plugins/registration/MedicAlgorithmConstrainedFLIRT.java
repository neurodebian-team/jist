/*
 *
 */
package edu.jhu.ece.iacl.plugins.registration;

import java.awt.Dimension;

import Jama.Matrix;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.algorithms.registration.AlgorithmConstrainedOAR3D;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;
import gov.nih.mipav.view.dialogs.JDialogBase;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class MedicAlgorithmConstrainedFLIRT extends ProcessingAlgorithm {
	ParamOption dof;
	ParamOption inputInterpolation;
	ParamOption outputInterpolation;
	ParamOption costFunction;
	ParamOption rotDim;
	ParamDouble minAngle;
	ParamDouble maxAngle;
	ParamInteger bracketMinimum;
	ParamInteger iters;
	ParamInteger numCoarse;
	ParamInteger level8to4;
	ParamBoolean subsample;
	ParamBoolean translateRange;
	ParamBoolean sameTranslation;
	ParamFloat minX,maxX,minY,maxY,minZ,maxZ;
	ParamBoolean skipMultilevelSearch;
	ParamBoolean useMaxOfMinRes;
	ParamBoolean initCOG;
	ParamVolume source, target, registered,refWeight,inputWeight;
	ParamMatrix trans;
//	ParamObject<String> mipavMatrix;

	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Constrained Linear Registration algorithm based on FLIRT. ";
	private static final String longDescription = "Finds a linear transformation under specified constraints that brings the source volume into the space of the target volume. Degrees of freedom can be specified.";


	protected void createInputParameters(ParamCollection inputParams) {
		ParamCollection mainParams=new ParamCollection("Main");
		mainParams.add(source = new ParamVolume("Source volume"));
		mainParams.add(target = new ParamVolume("Target volume"));
		mainParams.add(refWeight = new ParamVolume("Reference Weighted volume"));
		refWeight.setMandatory(false);
		mainParams.add(inputWeight = new ParamVolume("Input Weighted volume"));
		inputWeight.setMandatory(false);
		mainParams.add(dof = new ParamOption("Degrees of freedom", new String[] { "Rigid - 6", "Global rescale - 7",
				"Specific rescale - 9", "Affine - 12" }));
		dof.setValue(3);
		mainParams.add(costFunction = new ParamOption("Cost function", new String[] { "Correlation ratio",
				"Least squares", "Normalized cross correlation", "Normalized mutual information" }));
		mainParams.add(inputInterpolation = new ParamOption("Registration interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc"}));
		mainParams.add(outputInterpolation = new ParamOption("Output interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc","Nearest Neighbor" }));
		mainParams.add(rotDim = new ParamOption("Apply rotation", new String[] { "All", "X", "Y", "Z" }));
		mainParams.add(minAngle = new ParamDouble("Minimum angle", -360, 360, -10));
		mainParams.add(maxAngle = new ParamDouble("Maximum angle", -360, 360, 10));
		mainParams.add(numCoarse = new ParamInteger("Number of angles to sample in coarse grid:", 0, 360, 6));

		ParamCollection advParams=new ParamCollection("Advanced");
		advParams.add(bracketMinimum = new ParamInteger("Multiple of tolerance to bracket the minimum", 10));
		advParams.add(iters = new ParamInteger("Number of iterations", 10));
		advParams.add(level8to4 = new ParamInteger("Number of minima from Level 8 to test at Level 4", 5));

		advParams.add(useMaxOfMinRes = new ParamBoolean(
				"Use the max of the min resolutions of the two datasets when resampling", true));

		advParams.add(translateRange=new ParamBoolean("Limit translation range",true));
		advParams.add(sameTranslation=new ParamBoolean("Apply same translation limits to all dimensions",true));
		advParams.add(subsample = new ParamBoolean("Subsample image for speed", true));
		advParams.add(skipMultilevelSearch = new ParamBoolean("Skip multilevel search (Assume images are close to alignment)"));
		advParams.add(initCOG=new ParamBoolean("Initialize registration by aligning the COG's"));
		advParams.add(minX=new ParamFloat("Min X (mm)",-200,200,-128));
		advParams.add(maxX=new ParamFloat("Max X (mm)",-200,200,128));
		advParams.add(minY=new ParamFloat("Min Y (mm)",-200,200,-128));
		advParams.add(maxY=new ParamFloat("Max Y (mm)",-200,200,128));
		advParams.add(minZ=new ParamFloat("Min Z (mm)",-200,200,-128));
		advParams.add(maxZ=new ParamFloat("Max Z (mm)",-200,200,128));
		setPreferredSize(new Dimension(500,500));
		inputParams.add(mainParams);
		inputParams.add(advParams);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");
		inputParams.setLabel("Constrained Affine/Rigid Registration");
		inputParams.setName("Constrained_Affine-Rigid_Registration");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("Neva Cherniavsky", " ", "http://mipav.cit.nih.gov/"));
		info.add(new AlgorithmAuthor("Matthew McAuliffe", " ", "http://mipav.cit.nih.gov/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(trans = new ParamMatrix("Transformation Matrix", 4, 4));
		outputParams.add(registered = new ParamVolume("Registered Volume"));
//		outputParams.add(mipavMatrix = new ParamObject<String>("Mipav Matrix", new StringReaderWriter()));
//		mipavMatrix.setExtensionFilter(filter);
//		mipavMatrix.setName("mipavMatrix");
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		FlirtWrapper flirt=new FlirtWrapper();
		monitor.observe(flirt);
		flirt.execute();
	}


	protected class FlirtWrapper extends AbstractCalculation{
		public FlirtWrapper(){
		}


		public void execute(){
			ModelImage matchImage = source.getImageData().getModelImageCopy();
			ModelImage refImage = target.getImageData().getModelImageCopy();
			boolean maxOfMinResol=useMaxOfMinRes.getValue();
			int cost = 0;
			switch (costFunction.getIndex()) {
			case 0:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
			// case 0: cost = AlgorithmCostFunctions.CORRELATION_RATIO; break;
			case 1:
				cost = AlgorithmCostFunctions.LEAST_SQUARES_SMOOTHED;
				// cost = AlgorithmCostFunctions.LEAST_SQUARES;
				// costName = "LEAST_SQUARES_SMOOTHED";
				break;
			// case 2: cost = AlgorithmCostFunctions.MUTUAL_INFORMATION_SMOOTHED;
			// break;
			case 2:
				cost = AlgorithmCostFunctions.NORMALIZED_XCORRELATION_SMOOTHED;
				break;
			// case 3: cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION;
			// break;
			case 3:
				cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION_SMOOTHED;
				break;
			default:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
			}
			int DOF = 0;
			switch (dof.getIndex()) {
			case 0:
				DOF = 6;
				break;
			case 1:
				DOF = 7;
				break;
			case 2:
				DOF = 9;
				break;
			case 3:
				DOF = 12;
				break;
			default:
				DOF = 12;
				break;
			}
			int interp = 0;
			switch (inputInterpolation.getIndex()) {
			case 0:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp = AlgorithmTransform.WSINC;
				break;
			default:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			}
			int interp2 = 0;
			switch (outputInterpolation.getIndex()) {
			case 0:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp2 = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp2 = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp2 = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp2 = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp2 = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp2 = AlgorithmTransform.WSINC;
				break;
			case 7:
				interp2 = AlgorithmTransform.NEAREST_NEIGHBOR;
				break;
			default:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			}
			boolean fastMode = skipMultilevelSearch.getValue();
			float rotateBeginX = minAngle.getFloat();
			boolean doSubsample = subsample.getValue();
			float rotateBeginY = 0;
			float rotateBeginZ = 0;


			int numCoarseX=numCoarse.getInt();
			int numCoarseY=0;
			int numCoarseZ=0;

			float rotateRangeX=0;
			float rotateRangeY=0;
			float rotateRangeZ=0;

			ModelImage refWeightImage;
			ModelImage inputWeightImage;
			float[][] transLimits=new float[2][3];
			boolean calcCOG=initCOG.getValue();
	        float[][] maxLimits = new float[2][3];
	        int[] extents = matchImage.getExtents();
	        float[] resolution = matchImage.getFileInfo(0).getResolutions();
	        float[] halfFov = { 0, 0, 0 };
	        for (int i = 0; i < 3; i++) {
	            halfFov[i] = extents[i] * resolution[i] / 2.0f;
	            maxLimits[0][i] = -Math.round(halfFov[i]);
	            maxLimits[1][i] = Math.round(halfFov[i]);
	        }
			 if (translateRange.getValue()) {
	                transLimits[0][0] = minX.getFloat();
	                transLimits[1][0] = maxX.getFloat();
	                if (sameTranslation.getValue()) {
	                    transLimits[0][1] = transLimits[0][2] = transLimits[0][0];
	                    transLimits[1][1] = transLimits[1][2] = transLimits[1][0];
	                } else {
	                	transLimits[0][1] = minY.getFloat();
	                    transLimits[1][1] = maxY.getFloat();
	                    transLimits[0][2] = minZ.getFloat();
	                    transLimits[1][2] = maxZ.getFloat();
	                }
	            } else {
	                for (int i = 0; i < 3; i++) {
	                    transLimits[0][i] = maxLimits[0][i];
	                    transLimits[1][i] = maxLimits[1][i];
	                }
	            }

			switch (rotDim.getIndex()) {
			case 0:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = rotateBeginX;
				numCoarseY=numCoarseX;
				numCoarseZ=numCoarseX;
				break;
			case 1:
				rotateBeginY = 0;
				rotateBeginZ = 0;
				numCoarseY=0;
				numCoarseZ=0;
				break;
			case 2:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = 0;
				numCoarseY=numCoarseX;
				rotateBeginX = 0;
				numCoarseX=0;
				numCoarseZ=0;
				break;
			case 3:
				rotateBeginZ = rotateBeginX;
				rotateBeginY = 0;
				numCoarseZ=numCoarseX;
				numCoarseY=0;
				rotateBeginX = 0;
				numCoarseX=0;
				break;
			}
			int bracketBound = bracketMinimum.getInt();
			int maxIterations = iters.getInt();
			int numMinima = level8to4.getInt();
			if(refWeight.getImageData()!=null) {
			refWeightImage=refWeight.getImageData().getModelImageCopy();
			inputWeightImage=inputWeight.getImageData().getModelImageCopy();
			}  else {
				refWeightImage=null;
				inputWeightImage=null;
			}
			boolean weighted=(refWeightImage!=null);
			TransMatrix finalMatrix;
			AlgorithmConstrainedOAR3DWrapper reg3;
	        if (weighted) {
	            reg3 = new AlgorithmConstrainedOAR3DWrapper(refImage, matchImage, refWeightImage,inputWeightImage, cost, DOF,
	                                                 interp, rotateBeginX, rotateRangeX, rotateBeginY, rotateRangeY,
	                                                 rotateBeginZ, rotateRangeZ, numCoarseX, numCoarseY, numCoarseZ,
	                                                 transLimits, maxOfMinResol, doSubsample, fastMode, calcCOG,
	                                                 bracketBound, maxIterations, numMinima);
	        } else {
	            System.out.println(getClass().getCanonicalName()+"\t"+"Reference image name is " + refImage.getImageName());
	            System.out.println(getClass().getCanonicalName()+"\t"+"Moving image name is " + matchImage.getImageName());

	            reg3 = new AlgorithmConstrainedOAR3DWrapper(refImage, matchImage, cost, DOF, interp, rotateBeginX, rotateRangeX,
	                                                 rotateBeginY, rotateRangeY, rotateBeginZ, rotateRangeZ, numCoarseX,
	                                                 numCoarseY, numCoarseZ, transLimits, maxOfMinResol, doSubsample,
	                                                 fastMode, calcCOG, bracketBound, maxIterations, numMinima);
	        }

			reg3.setObserver(this);
			reg3.run();
			
			int xdimA = refImage.getExtents()[0];
			int ydimA = refImage.getExtents()[1];
			int zdimA = refImage.getExtents()[2];
			float xresA = refImage.getFileInfo(0).getResolutions()[0];
			float yresA = refImage.getFileInfo(0).getResolutions()[1];
			float zresA = refImage.getFileInfo(0).getResolutions()[2];
			String name = matchImage.getImageName()+ "_reg";
			if(refWeightImage!=null)
				refWeightImage.disposeLocal(); refWeightImage=null;
			finalMatrix = reg3.getTransform();
			AlgorithmTransform transform = new AlgorithmTransform(matchImage, finalMatrix, interp2, xresA, yresA, zresA,
					xdimA, ydimA, zdimA, true, false, false);
			transform.setUpdateOriginFlag(true);
			transform.run();
			matchImage.disposeLocal();matchImage=null;
			
			ModelImage resultImage = transform.getTransformedImage();
			transform.finalize();
			resultImage.getFileInfo(0).setAxisOrientation(refImage.getFileInfo(0).getAxisOrientation());
			resultImage.getFileInfo(0).setImageOrientation(refImage.getFileInfo(0).getImageOrientation());
			refImage.disposeLocal();refImage=null;

			resultImage.calcMinMax();			
			MipavController.setModelImageName(resultImage,name);
			registered.setValue(new ImageDataMipavWrapper(resultImage));
			resultImage=null;
			Matrix tmp=new Matrix(4,4);
			for(int i=0;i<4;i++){
				for(int j=0;j<4;j++){
					tmp.set(i, j, finalMatrix.get(i, j));
				}
			}
			trans.setValue(tmp);
		}
	}


	protected static class AlgorithmConstrainedOAR3DWrapper extends AlgorithmConstrainedOAR3D{
		/**
		 * @param arg0
		 * @param arg1
		 * @param arg2
		 * @param arg3
		 * @param arg4
		 * @param arg5
		 * @param arg6
		 * @param arg7
		 * @param arg8
		 * @param arg9
		 * @param arg10
		 * @param arg11
		 * @param arg12
		 * @param arg13
		 * @param arg14
		 * @param arg15
		 * @param arg16
		 * @param arg17
		 * @param arg18
		 * @param arg19
		 * @param arg20
		 * @param arg21
		 */
		public AlgorithmConstrainedOAR3DWrapper(ModelImage arg0, ModelImage arg1, int arg2, int arg3, int arg4,
				float arg5, float arg6, float arg7, float arg8, float arg9, float arg10, int arg11, int arg12,
				int arg13, float[][] arg14, boolean arg15, boolean arg16, boolean arg17, boolean arg18, int arg19,
				int arg20, int arg21) {
			super(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16,
					arg17, arg18, arg19, arg20, arg21);
			// TODO Auto-generated constructor stub
		}


		public AlgorithmConstrainedOAR3DWrapper(ModelImage arg0, ModelImage arg1, ModelImage arg2, ModelImage arg3,
				int arg4, int arg5, int arg6, float arg7, float arg8, float arg9, float arg10, float arg11,
				float arg12, int arg13, int arg14, int arg15, float[][] arg16, boolean arg17, boolean arg18,
				boolean arg19, boolean arg20, int arg21, int arg22, int arg23) {
			super(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16,
					arg17, arg18, arg19, arg20, arg21, arg22, arg23);

		}
		/**
		 * @param _imagea
		 * @param _imageb
		 * @param weight
		 * @param weight2
		 * @param choice
		 * @param _dof
		 * @param _interp
		 * @param beginX
		 * @param endX
		 * @param rateX
		 * @param rateX2
		 * @param beginY
		 * @param endY
		 * @param rateY
		 * @param rateY2
		 * @param beginZ
		 * @param endZ
		 * @param rateZ
		 * @param rateZ2
		 * @param resol
		 * @param subsample
		 * @param mode
		 * @param bound
		 * @param numIter
		 * @param minima
		 */
		protected AbstractCalculation observer;

		public void setObserver(AbstractCalculation observer){
			this.observer=observer;
		}
		public void runAlgorithm(){
			observer.setTotalUnits(100);
			super.runAlgorithm();
			observer.markCompleted();
		}


		/**
	     * Notifies all listeners that have registered interest for notification on this event type.
	     *
	     * @param  value  the value of the progress bar.
	     */
	    protected void fireProgressStateChanged(int value) {
	        super.fireProgressStateChanged(value);
	        observer.setCompletedUnits(value);
	    }


	    /**
	     * Updates listeners of progress status. Without actually changing the numerical value
	     *
	     * @param  imageName  the name of the image
	     * @param  message    the new message to display
	     */
	    protected void fireProgressStateChanged(String imageName, String message) {
	    	super.fireProgressStateChanged(imageName, message);
	    	observer.setLabel(message);
	    }
	}
}
