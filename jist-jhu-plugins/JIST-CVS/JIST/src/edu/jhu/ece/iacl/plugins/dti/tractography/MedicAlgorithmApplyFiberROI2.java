package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import javax.vecmath.Point2i;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberStatistics;
import edu.jhu.ece.iacl.algorithms.manual_label.LabelImage;
import edu.jhu.ece.iacl.algorithms.manual_label.ROI;
import edu.jhu.ece.iacl.jist.io.*;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class MedicAlgorithmApplyFiberROI2 extends ProcessingAlgorithm {
	//input param
	private ParamObject<FiberCollection> fiberset;
	private ParamVolume roi;
	private ParamVolume statvolume;
	private ParamOption outputtype;
	private ParamOption statistic;
	private ParamBoolean outputFiberSets;
	private ParamBoolean outputConnectionStatTables;
	private ParamBoolean splitFibers;
	
	//output param
	private ParamFileCollection fibersubset;
	private ParamFileCollection connStatTables;
	private ParamObject<double[][]> labelList;
	private ParamFileCollection connMatrices;
	
	private FiberCollectionReaderWriter fcrw = FiberCollectionReaderWriter.getInstance();
	private CurveVtkReaderWriter cvrw = CurveVtkReaderWriter.getInstance();
	private ArrayDoubleTxtReaderWriter arw = ArrayDoubleTxtReaderWriter.getInstance();
	
	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Gets a subset of the input fibers based on the input volumetric ROI.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fiberset = new ParamObject<FiberCollection>("Fiber Set", new FiberCollectionReaderWriter()));
		inputParams.add(roi = new ParamVolume("ROI"));
		inputParams.add(statvolume = new ParamVolume("Volume"));
		statvolume.setMandatory(false);
		inputParams.add(outputtype=new ParamOption("Output Type",new String[]{"DtiStudio dat","Vtk"}));
//		inputParams.add(statistic=new ParamOption("Statistic",new String[]{"Fiber Length","Volume Mean","Volume Median","Volume Min","Volume Max"}));
		inputParams.add(outputFiberSets = new ParamBoolean("Output Fiber SubSets",false));
		inputParams.add(outputConnectionStatTables = new ParamBoolean("Output Fiber Data Tables",false));
		inputParams.add(splitFibers = new ParamBoolean("Split Fibers",false));
		
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Apply Fiber ROI2");
		inputParams.setName("Apply_Fiber_ROI2");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "http://putter.ece.jhu.edu/John"));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.UNKNOWN);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fibersubset = new ParamFileCollection("Fiber Sub-Sets",new FileExtensionFilter(new String[]{"dat","vtk"})));
		fibersubset.setMandatory(false);
		outputParams.add(labelList = new ParamObject<double[][]>("Labels found"));
		labelList.setMandatory(false);
		outputParams.add(connStatTables = new ParamFileCollection("Connection Statistics",new FileExtensionFilter(new String[]{"txt","csv"})));
		connStatTables.setMandatory(false);
		outputParams.add(connMatrices = new ParamFileCollection("Connectivity Matrices",new FileExtensionFilter(new String[]{"txt","csv"})));
		connMatrices.setMandatory(false);
//		connStatTables
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		System.out.println("Ready to go");
		ROI mask = new  ROI(roi.getImageData());
		ArrayList<Integer> labellist = mask.getLabelList();
		Collections.sort(labellist);
		System.out.println("Found Labels");
		
		//write labels
		try{
			arw.write(toArrayInt(labellist), new File(dir.getCanonicalPath()+File.separator+"Labels.csv"));
		}catch(IOException e){
			e.printStackTrace();
		}
		
		HashMap<Point2i, FiberCollection> connsToFibs;
		HashMap<Point2i, HashSet<GridPt>> connsToPts;
		if(splitFibers.getValue()){
//			connsToFibs = null;
//			connsToPts = null;
			connsToFibs = mask.pairFiberRoiSplit(fiberset.getObject());
			connsToPts = mask.getConnectionPoints();
		}else{
			connsToFibs = mask.pairFiberRoi(fiberset.getObject());
			connsToPts = mask.getConnectionPoints();
		}
		
		
//		System.out.println()
		//write output
		if(outputFiberSets.getValue()){
			System.out.println("\nKey Set: "+connsToFibs.keySet());
			System.out.println("Writing Fiber subsets");
			ArrayList<File> fiberfilelist = new ArrayList<File>();
			for(Point2i key : connsToFibs.keySet()){
				System.out.println("This key: " +key);
				System.out.println("This fiberColl: " +connsToFibs.get(key));
				File f = writeFibers(connsToFibs.get(key),dir);
				fiberfilelist.add(f);
			}
			fibersubset.setValue(fiberfilelist);
		}
		
		FiberStatistics fstat = new FiberStatistics();
		double[][] arraylens;
		double[][] count;

		double[][] meanconnmtx = new double[labellist.size()][labellist.size()];
		double[][] medconnmtx = new double[labellist.size()][labellist.size()];
		double[][] maxconnmtx = new double[labellist.size()][labellist.size()];
		double[][] minconnmtx = new double[labellist.size()][labellist.size()];
		double[][] stdconnmtx = new double[labellist.size()][labellist.size()];
		
		for(Point2i key : connsToFibs.keySet()){
			System.out.println("\nWorking on connection: " + key);
			fstat.setFibers(connsToFibs.get(key));
			fstat.computeLengths();
			if(fstat.getRes()==null && statvolume.getValue()!=null){
				fstat.setRes(statvolume.getImageData().getHeader().getDimResolutions()[0],
					statvolume.getImageData().getHeader().getDimResolutions()[1],
					statvolume.getImageData().getHeader().getDimResolutions()[2]);
			}
			ArrayList<Double> lens = fstat.getFiberLengths();
			arraylens = toArray(lens);
			lens=null;
			count = new double[][]{{connsToFibs.get(key).size()}};
			
			//write fiber lengths
			try{
			connStatTables.add(
					arw.write(arraylens, new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_lengths.csv")));

			connStatTables.add(
                    arw.write(count, new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_count.csv")));
			}catch(IOException e){
				e.printStackTrace();
			}
			arraylens = null;

			if(statvolume.getValue()!=null){
				
				double[] connvalues = ROI.getValuesFromGridPts(statvolume.getImageData(), connsToPts.get(key));
				System.out.println("Adding matrix stat at position: " + 
						labellist.indexOf(key.x) + "," + labellist.indexOf(key.y));
				meanconnmtx[labellist.indexOf(key.x)][labellist.indexOf(key.y)] = StatisticsDouble.mean(connvalues);
				medconnmtx[labellist.indexOf(key.x)][labellist.indexOf(key.y)] = StatisticsDouble.median(connvalues);
				maxconnmtx[labellist.indexOf(key.x)][labellist.indexOf(key.y)] = StatisticsDouble.max(connvalues);
				minconnmtx[labellist.indexOf(key.x)][labellist.indexOf(key.y)] = StatisticsDouble.min(connvalues);
				stdconnmtx[labellist.indexOf(key.x)][labellist.indexOf(key.y)] = StatisticsDouble.std(connvalues);


				fstat.findTractVolumeStats(statvolume.getImageData());

				ArrayList<Double> avg = fstat.getMeanFromVolume();
				ArrayList<Double> sum = fstat.getSumFromVolume();
				ArrayList<Double> max = fstat.getMaxFromVolume();
				ArrayList<Double> min = fstat.getMaxFromVolume();
				ArrayList<Double> med = fstat.getMedianFromVolume();
				double[][] avgout = toArray(avg);
				avg=null;
				double[][] sumout = toArray(sum);
				sum=null;
				double[][] maxout = toArray(max);
				max=null;
				double[][] minout = toArray(min);
				min=null;
				double[][] medout = toArray(med);
				med=null;

				//write all others stats
				try{
					connStatTables.add(arw.write(avgout, 
							new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_avgdata_"+statvolume.getImageData().getName()+".csv")));

					connStatTables.add(arw.write(sumout, 
							new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_sumdata_"+statvolume.getImageData().getName()+".csv")));

					connStatTables.add(arw.write(maxout, 
							new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_maxdata_"+statvolume.getImageData().getName()+".csv")));

					connStatTables.add(arw.write(minout, 
							new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_mindata_"+statvolume.getImageData().getName()+".csv")));

					connStatTables.add(arw.write(medout, 
							new File(dir.getCanonicalFile()+File.separator+connsToFibs.get(key).getName()+"_mediandata_"+statvolume.getImageData().getName()+".csv")));

				}catch(IOException e){
					e.printStackTrace();
				}
				avgout = null;
				sumout = null;
				maxout = null;
				minout = null;
				medout = null;
				connsToFibs.get(key).clear();
//				connsToFibs.remove(key);
				System.gc();
				
			}
		}
		try{
			connStatTables.add(arw.write(meanconnmtx, 
					new File(dir.getCanonicalFile()+File.separator+"Avgmtx_"+statvolume.getImageData().getName()+".csv")));

			connStatTables.add(arw.write(medconnmtx, 
					new File(dir.getCanonicalFile()+File.separator+"Medmtx_"+statvolume.getImageData().getName()+".csv")));

			connStatTables.add(arw.write(maxconnmtx, 
					new File(dir.getCanonicalFile()+File.separator+"Maxmtx_"+statvolume.getImageData().getName()+".csv")));

			connStatTables.add(arw.write(minconnmtx, 
					new File(dir.getCanonicalFile()+File.separator+"Minmtx_"+statvolume.getImageData().getName()+".csv")));

			connStatTables.add(arw.write(stdconnmtx, 
					new File(dir.getCanonicalFile()+File.separator+"Stdmtx_"+statvolume.getImageData().getName()+".csv")));

		}catch(IOException e){
			e.printStackTrace();
		}
		
		
	}

	private File writeFibers(FiberCollection fibers, File dir){
		File out = null;
		if(outputtype.getIndex()==0){
			out = fcrw.write(fibers, dir);
		}else{
			out = cvrw.write(fibers.toCurveCollection(), dir);
		}
		return out;
	}


	private double[][] toArray(ArrayList<Double> in){
		double[][] out = new double[in.size()][1];
		for(int i=0; i<in.size(); i++){
			out[i][0]=in.get(i);
		}
		return out;
	}
	private double[][] toArrayInt(ArrayList<Integer> in){
		double[][] out = new double[in.size()][1];
		for(int i=0; i<in.size(); i++){
			out[i][0]=in.get(i);
		}
		return out;
	}
	
}
