package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.algorithms.utilities.AlgorithmMatchImages;
import gov.nih.mipav.model.structures.ModelImage;

import java.util.ArrayList;
import java.util.List;


public class MedicAlgorithmMatchAllImages extends ProcessingAlgorithm{
	private ParamVolumeCollection matchus;
	private ParamVolume target;
	private ParamVolumeCollection matchedVols;
	private ParamBoolean doOrigins, doDims, resByRef;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.8 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Use the MIPAV 'Match Image' algorithm to match the sizes/resolutions/origins for all volumes in a Volue Collection to a Target Volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(matchus=new ParamVolumeCollection("Images to Match"));
		inputParams.add(target=new ParamVolume("Target Image"));
		inputParams.add(doOrigins=new ParamBoolean("Match Origings"));
		inputParams.add(doDims=new ParamBoolean("Match Dimensions"));
		inputParams.add(resByRef=new ParamBoolean("res By Ref"));
		doOrigins.setValue(true);
		doDims.setValue(true);
		resByRef.setValue(true);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Match All Images");
		inputParams.setName("Match_All_Images");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		matchedVols = new ParamVolumeCollection("Matched Images");
		outputParams.add(matchedVols);

	}


	protected void execute(CalculationMonitor monitor) {
		List<ImageData> imgs = matchus.getImageDataList();
		ArrayList<ImageData> out = new ArrayList<ImageData>(imgs.size());

		resetHeaders(imgs);

		int l=0;
		for(ImageData vol : imgs){
			ModelImage T =target.getImageData().getModelImageCopy();
			ModelImage S = vol.getModelImageCopy();
			AlgorithmMatchImages alg = new AlgorithmMatchImages(T, S,
					doOrigins.getValue(),doDims.getValue(),resByRef.getValue());
			alg.runAlgorithm();
			T.disposeLocal();
			S.disposeLocal();
			out.add(new ImageDataMipavWrapper(alg.getImageB()));
			System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
			l++;
		}
		matchedVols.setValue(out);
	}


	private void resetHeaders(List<ImageData> imgs){
		for(ImageData vol: imgs){
			float[] res = {.828125f, .828125f, 2.2f};
			for(int i=0; i<vol.getSlices(); i++){
				vol.getModelImageCopy().setResolutions(i, res);
			}
		}
	}
}
