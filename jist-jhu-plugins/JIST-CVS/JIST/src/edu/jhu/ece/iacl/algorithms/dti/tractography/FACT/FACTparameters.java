package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.*;
import javax.vecmath.*;


/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 16, 2005
 * Time: 7:35:38 AM
 * To change this template use Options | File Templates.
 */
public class FACTparameters {
	public float faData[][][];
    public float vecData[][][][];
    //Parameters:
    public float stopFA=0.4f;
    public float maxTurnAngle=(float)(70*Math.PI/180);
    public int minLength = 20;
    public float startFA=0.4f;
    public int Nx,Ny,Nz;
    public double resX,resY,resZ;
    
    
    public void readParameters(String file){
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			for(int i=0; i<9; i++){
				String s = in.readLine();
				String val = s.substring(s.indexOf(":")+1);
				if(i==0){
					startFA = Float.valueOf(val);
				}else if(i==1){
					stopFA = Float.valueOf(val);
				}
				else if(i==2){
					maxTurnAngle = (float)(Float.valueOf(val)*Math.PI/180);
				}
				else if(i==3){
					Nx = Integer.valueOf(val);
				}
				else if(i==4){
					Ny = Integer.valueOf(val);
				}
				else if(i==5){
					Nz = Integer.valueOf(val);
				}
				else if(i==6){
					resX = Float.valueOf(val);
				}
				else if(i==7){
					resY = Float.valueOf(val);
				}
				else if(i==8){
					resZ = Float.valueOf(val);
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	    System.out.println("jist.plugins"+"\t"+startFA);
	    System.out.println("jist.plugins"+"\t"+stopFA);
	    System.out.println("jist.plugins"+"\t"+maxTurnAngle);
	    System.out.println("jist.plugins"+"\t"+Nx);
	    System.out.println("jist.plugins"+"\t"+Ny);
	    System.out.println("jist.plugins"+"\t"+Nz);
	    System.out.println("jist.plugins"+"\t"+resX);
	    System.out.println("jist.plugins"+"\t"+resY);
	    System.out.println("jist.plugins"+"\t"+resZ);
	    
    }
    
    public void setDefaultParameters(){
    	startFA = 0.2f;
    	stopFA = 0.2f;
    	maxTurnAngle = (float)(30*Math.PI/180);
    	Nx = 256;
    	Ny = 256;
    	Nz = 173;
    	resX = 0.828125;
    	resY = 0.828125;
    	resZ = 0.828125;

    }
    
//  Interpolates Vector
    public Vector3f interpVec(Point3f p){

        //Neighbors
        Point3i uuu = new Point3i((int)Math.ceil(p.x),(int)Math.ceil(p.y),(int)Math.ceil(p.z));
        Point3i uud = new Point3i((int)Math.ceil(p.x),(int)Math.ceil(p.y),(int)Math.floor(p.z));
        Point3i udu = new Point3i((int)Math.ceil(p.x),(int)Math.floor(p.y),(int)Math.ceil(p.z));
        Point3i udd = new Point3i((int)Math.ceil(p.x),(int)Math.floor(p.y),(int)Math.floor(p.z));

        Point3i duu = new Point3i((int)Math.floor(p.x),(int)Math.ceil(p.y),(int)Math.ceil(p.z));
        Point3i dud = new Point3i((int)Math.floor(p.x),(int)Math.ceil(p.y),(int)Math.floor(p.z));
        Point3i ddu = new Point3i((int)Math.floor(p.x),(int)Math.floor(p.y),(int)Math.ceil(p.z));
        Point3i ddd = new Point3i((int)Math.floor(p.x),(int)Math.floor(p.y),(int)Math.floor(p.z));

        //Init vector to (0,0,0)
        Vector3f out = new Vector3f();

        //Interpolate in x
        /*
         * Initialize at 2 because we will never see vecData[*][*][*][*]>1
         * since vectors must be normalized.
         * Therefore, x**>2 is an input that we ignore
         *
         * Note also that if x**>2 => y**>2 and z**>2
         */
            float xuu=2;
            float yuu=2;
            float zuu=2;
            Point3f uu = new Point3f(p.x,uuu.y,uuu.z);
            if(faData[uuu.z][uuu.y][uuu.x]>stopFA && faData[duu.z][duu.y][duu.x]<stopFA){ //if one input is bad, return the good one
                xuu = vecData[uuu.z][uuu.y][uuu.x][0];
                yuu = vecData[uuu.z][uuu.y][uuu.x][1];
                zuu = vecData[uuu.z][uuu.y][uuu.x][2];
            }else if(faData[uuu.z][uuu.y][uuu.x]<stopFA && faData[duu.z][duu.y][duu.x]>stopFA){ //if one input is bad, return the good one
                xuu = vecData[duu.z][duu.y][duu.x][0];
                yuu = vecData[duu.z][duu.y][duu.x][1];
                zuu = vecData[duu.z][duu.y][duu.x][2];
            }else if(faData[uuu.z][uuu.y][uuu.x]>stopFA && faData[duu.z][duu.y][duu.x]>stopFA){ //both inputs good => lin interpolate
                xuu = linInterp(p.x,uuu.x,vecData[uuu.z][uuu.y][uuu.x][0],duu.x,vecData[duu.z][duu.y][duu.x][0]);
                yuu = linInterp(p.x,uuu.x,vecData[uuu.z][uuu.y][uuu.x][1],duu.x,vecData[duu.z][duu.y][duu.x][1]);
                zuu = linInterp(p.x,uuu.x,vecData[uuu.z][uuu.y][uuu.x][2],duu.x,vecData[duu.z][duu.y][duu.x][2]);
            }
           
            float xud=2;
            float yud=2;
            float zud=2;
            Point3f ud = new Point3f(p.x,uud.y,uud.z);
            if(faData[uud.z][uud.y][uud.x]>stopFA && faData[dud.z][dud.y][dud.x]<stopFA){
                xud = vecData[uud.z][uud.y][uud.x][0];
                yud = vecData[uud.z][uud.y][uud.x][1];
                zud = vecData[uud.z][uud.y][uud.x][2];
            }else if(faData[uud.z][uud.y][uud.x]<stopFA && faData[dud.z][dud.y][dud.x]>stopFA){
                xud = vecData[dud.z][dud.y][dud.x][0];
                yud = vecData[dud.z][dud.y][dud.x][1];
                zud = vecData[dud.z][dud.y][dud.x][2];
            }else if(faData[uud.z][uud.y][uud.x]>stopFA && faData[dud.z][dud.y][dud.x]>stopFA){
                xud = linInterp(p.x,uud.x,vecData[uud.z][uud.y][uud.x][0],dud.x,vecData[dud.z][dud.y][dud.x][0]);
                yud = linInterp(p.x,uud.x,vecData[uud.z][uud.y][uud.x][1],dud.x,vecData[dud.z][dud.y][dud.x][1]);
                zud = linInterp(p.x,uud.x,vecData[uud.z][uud.y][uud.x][2],dud.x,vecData[dud.z][dud.y][dud.x][2]);
            }
           
           
            float xdu=2;
            float ydu=2;
            float zdu=2;
            Point3f du = new Point3f(p.x,udu.y,udu.z);
            if(faData[udu.z][udu.y][udu.x]>stopFA && faData[ddu.z][ddu.y][ddu.x]<stopFA){
                xdu = vecData[udu.z][udu.y][udu.x][0];
                ydu = vecData[udu.z][udu.y][udu.x][1];
                zdu = vecData[udu.z][udu.y][udu.x][2];
            }else if(faData[udu.z][udu.y][udu.x]<stopFA && faData[ddu.z][ddu.y][ddu.x]>stopFA){
                xdu = vecData[ddu.z][ddu.y][ddu.x][0];
                ydu = vecData[ddu.z][ddu.y][ddu.x][1];
                zdu = vecData[ddu.z][ddu.y][ddu.x][2];
            }else if(faData[udu.z][udu.y][udu.x]>stopFA && faData[ddu.z][ddu.y][ddu.x]>stopFA){
                xdu = linInterp(p.x,udu.x,vecData[udu.z][udu.y][udu.x][0],ddu.x,vecData[ddu.z][ddu.y][ddu.x][0]);
                ydu = linInterp(p.x,udu.x,vecData[udu.z][udu.y][udu.x][1],ddu.x,vecData[ddu.z][ddu.y][ddu.x][1]);
                zdu = linInterp(p.x,udu.x,vecData[udu.z][udu.y][udu.x][2],ddu.x,vecData[ddu.z][ddu.y][ddu.x][2]);
            }
           
            float xdd=2;
            float ydd=2;
            float zdd=2;
            Point3f dd = new Point3f(p.x,udd.y,udd.z);
            if(faData[udd.z][udd.y][udd.x]>stopFA && faData[ddd.z][ddd.y][ddd.x]<stopFA){
                xdd = vecData[udd.z][udd.y][udd.x][0];
                ydd = vecData[udd.z][udd.y][udd.x][1];
                zdd = vecData[udd.z][udd.y][udd.x][2];
            }else if(faData[udd.z][udd.y][udd.x]<stopFA && faData[ddd.z][ddd.y][ddd.x]>stopFA){
                xdd = vecData[ddd.z][ddd.y][ddd.x][0];
                ydd = vecData[ddd.z][ddd.y][ddd.x][1];
                zdd = vecData[ddd.z][ddd.y][ddd.x][2];
            }else if(faData[udd.z][udd.y][udd.x]>stopFA && faData[ddd.z][ddd.y][ddd.x]>stopFA){
                xdd = linInterp(p.x,udd.x,vecData[udd.z][udd.y][udd.x][0],ddd.x,vecData[ddd.z][ddd.y][ddd.x][0]);
                ydd = linInterp(p.x,udd.x,vecData[udd.z][udd.y][udd.x][1],ddd.x,vecData[ddd.z][ddd.y][ddd.x][1]);
                zdd = linInterp(p.x,udd.x,vecData[udd.z][udd.y][udd.x][2],ddd.x,vecData[ddd.z][ddd.y][ddd.x][2]);
            }
           
            //Interpolate in y
            float xu = 2;
            float yu = 2;
            float zu = 2;
            Point3f u = new Point3f(p.x,p.y,uu.z);
            if(xuu<2 && xdu>2){
                xu = xuu;
                yu = yuu;
                zu = zuu;
            }else if(xuu>2 && xdu<2){
                xu = xdu;
                yu = ydu;
                zu = zdu;
            }else if(xuu<2 && xdu<2){
                xu = linInterp(p.y,uu.y,xuu,du.y,xdu);
                yu = linInterp(p.y,uu.y,yuu,du.y,ydu);
                zu = linInterp(p.y,uu.y,zuu,du.y,zdu);
            }
           
            float xd = 2;
            float yd = 2;
            float zd = 2;
            Point3f d = new Point3f(p.x,p.y,ud.z);
            if(xud<2 && xdd>2){
                xu = xud;
                yu = yud;
                zu = zud;
            }else if(xud>2 && xdd<2){
                xu = xdd;
                yu = ydd;
                zu = zdd;
            }else if(xud<2 && xdd<2){
                xd = linInterp(p.y,ud.y,xud,dd.y,xdd);
                yd = linInterp(p.y,uu.y,yud,du.y,ydd);
                zd = linInterp(p.y,uu.y,zud,du.y,zdd);
            }
           
            //Interpolate in z
            float x = 2;
            float y = 2;
            float z = 2;
            if(xu<2 && xd>2){
                x = xu;
                y = yu;
                z = zu;
            }else if(xu>2 && xd<2){
                x = xd;
                y = yd;
                z = zd;
            }else if(xu<2 && xd<2){
                x = linInterp(p.z,u.z,xu,d.z,xd);
                y = linInterp(p.z,u.z,yu,d.z,yd);
                z = linInterp(p.z,u.z,zu,d.z,zd);
            }
           
            //only update Vector if x<2 => we had at least 1 neighboring point with
            //sufficiently high FA
            if(x<2){
                out = new Vector3f(x,y,z);
            }
            //if this is not the case, the vector returned will be (0,0,0)
       
        return out;
    }
   
    // Finds the value of y at x given y1=y(x1) and y2=y(x2) using linear interpolation
    public static float linInterp(float x, float x1, float x2, float y1, float y2){
        return y1+(x-x1)*(y2-y1)/(x2-x1);
    }

}
