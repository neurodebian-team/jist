package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;

public class ArrayPrinter {
	
	public static void printArray(int[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}
	
	public static void printArray(float[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}
	
	public static void printArrray(int[][] a){
		for(int i=0; i<a[0].length; i++){
			printArray(a[i]);
		}
	}
	
	public static void printArrray(float[][] a){
		for(int i=0; i<a[0].length; i++){
			printArray(a[i]);
		}
	}
	
	public static void printArray(Object[] a){
//		String ans = " ";
		for(int i=0; i<a.length; i++){
//			ans = ans + a[i]+" ";
			System.out.println("jist.plugins"+"\t"+a[i]);
		}
//		System.out.println("jist.plugins"+"\t"+ans);
	}
	public static void printFiberArray(Fiber[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
//		for(int i=0; i<50; i++){
//			if(a[i]!=null){
				System.out.println("jist.plugins"+"\t"+"Fiber: " + i + " is: " +a[i]);
//			}
		}
	}
	
	public static void printArrray(Object[][] a){
		for(int i=0; i<a.length; i++){
			printArray(a[i]);
		}
	}
	
	public static void printArrray(int[][][] a){
		for(int k=0; k<a[0][0].length; k++){
			String[] sa = new String[a[0].length];
			for(int i=0; i<a.length; i++){
				for(int j=0; j<a[0].length; j++){
					sa[j] = sa[j] + a[i][j][k] + " ";
				}
			}
			for(int m=0; m<sa.length; m++){
				System.out.println("jist.plugins"+"\t"+sa[m]);
			}
			System.out.println("jist.plugins"+"\t");
		}
	}
	
	public static void printArray(double[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}
	
//	public static void printArrray(int[][][] a){
//		for(int i=0; i<a[0][0].length; i++){
//			printArray(a[i]);
//		}
//	}

}
