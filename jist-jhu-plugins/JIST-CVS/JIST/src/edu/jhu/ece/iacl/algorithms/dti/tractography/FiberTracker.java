package edu.jhu.ece.iacl.algorithms.dti.tractography;

/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 15, 2005
 * Time: 4:56:47 PM
 * To change this template use Options | File Templates.
 */
// Goal:
// Implement FACT in JAVA (faster and able to memory manage)
// Provide fiber lookup table in Java (hash indecies by point locations)

import java.util.LinkedList;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.algorithms.dti.tractography.FACT.cPT;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
public class FiberTracker extends AbstractCalculation{
    protected static final boolean FORWARD = true;
    protected static final boolean BACKWARD = false;
    protected static final double pi2 = Math.PI/2;
    protected static float stepSize = 0.1f;
    protected static float DTIStudioStepSize = 0.1f;
    protected int minLen, maxlen, cnt, len;
    protected float mnLen;
    protected double startFA,stopFA,maxTurnAngle;
    protected float[][][] faData;
    protected float[][][][] vecData;
    protected int Nx,Ny,Nz;
    Point3f resolution;
    protected static int MAX_LENGTH=2000;
    public FiberTracker(AbstractCalculation parent,double startFA,double stopFA,double maxTurnAngle,Point3f resolution,ImageData faVol,ImageData vecVol) {
    	super(parent);
       	this.startFA=startFA;
    	this.stopFA=stopFA;
        Nx=faVol.getRows();
        Ny=faVol.getCols();
        Nz=faVol.getSlices();
        faData=(new ImageDataFloat(faVol)).toArray3d();
        vecData=(new ImageDataFloat(vecVol)).toArray4d();
        this.maxTurnAngle=maxTurnAngle;
        this.resolution=resolution;
        this.setLabel("Fiber Tracking");
    }
    public FiberTracker(double startFA,double stopFA,double maxTurnAngle,Point3f resolution,ImageData faVol,ImageData vecVol) {
    	this.startFA=startFA;
    	this.stopFA=stopFA;
        Nx=faVol.getRows();
        Ny=faVol.getCols();
        Nz=faVol.getSlices();
        faData=(new ImageDataFloat(faVol)).toArray3d();
        vecData=(new ImageDataFloat(vecVol)).toArray4d();
        this.maxTurnAngle=maxTurnAngle;
        this.resolution=resolution;
        this.setLabel("Fiber Tracking");
    }
    public FiberCollection solve() {
        int numFibers =0;

        FiberCollection fibers=new FiberCollection();
        fibers.setResolutions(resolution);
        fibers.setDimensions(new Point3i(Nx,Ny,Nz));
        fibers.setName("fibers");
        System.out.println("jist.plugins"+"\t"+"Max turn angle is " +maxTurnAngle);
        System.out.println("jist.plugins"+"\t"+"stopFA is " +stopFA);
         for(int i=0;i<Nz;i++) {
                for(int j=0;j<Ny;j++) {
                    for(int k=0;k<Nx;k++)  {
                        if(faData[k][j][i]>=startFA) {
                            numFibers++;
                        }
                        
                    }
                }
            }
        System.out.println("jist.plugins"+"\t"+"Tracking "+numFibers+" fibers...");
        numFibers=0;
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;
        int fiberCount=0;
        for(short i=0;i<Nz;i++) {
            for(short j=0;j<Ny;j++) {
                for(short k=0;k<Nx;k++)  {
                    if(faData[k][j][i]>=startFA) {
                    	fiberCount++;
                    }
                }
            }
        }
        setTotalUnits(fiberCount);
         for(short i=0;i<Nz;i++) {
                for(short j=0;j<Ny;j++) {
                    for(short k=0;k<Nx;k++)  {
                        if(faData[k][j][i]>=startFA) {
                        	incrementCompletedUnits();
                        	//System.out.println("jist.plugins"+"\t"+"Tracking another fiber from "+k+" " +j+" "+i);
                        	FACTfiber newFiber = new FACTfiber();
                        	newFiber.setRes(resolution.x, resolution.y, resolution.z);
                        	//track fiber here
                        	long mem1 = Runtime.getRuntime().freeMemory();
                        	
                        	int myLen=newFiber.track((float)k+0.5f,(float)j+0.5f,(float)i+0.5f);
                        	
                        	long mem2 = Runtime.getRuntime().freeMemory();
                        	
//                        	IGNORE FIBERS OF LENGTH < 7
                        	//System.out.println("jist.plugins"+"\t"+"Length of this fiber is: " + myLen);
                        	if (myLen>6){
                        		leng7++;

                        		diff += (mem2-mem1);

                        		if(myLen<minLen) {
                        			minLen=myLen;
                        		} else{
                        			len2+=myLen;
                        			cnt++;
                        		}
                        		if (myLen>maxlen) { maxlen=myLen; }
                        		len+=myLen;
                        		
                        		fibers.add(numFibers,createFiber(newFiber));
                        		numFibers++;
                        		
                        	}
                        }
                    }
                }
            }
        mnLen = (float)len/(float)numFibers;
        System.out.println("jist.plugins"+"\t"+"Finished, and tracked: " + numFibers + " fibers");
        System.out.println("jist.plugins"+"\t"+"Mean length: "+mnLen);
        System.out.println("jist.plugins"+"\t"+"Min length: "+minLen);
        System.out.println("jist.plugins"+"\t"+"Max length: "+maxlen);
        return fibers;
    }
    public FiberCollection solve(Point3i seed) {
        int numFibers =0;

        FiberCollection fibers=new FiberCollection();
        fibers.setResolutions(resolution);
        fibers.setDimensions(new Point3i(Nx,Ny,Nz));
        fibers.setName("fibers");
        System.out.println("jist.plugins"+"\t"+"Max turn angle is " +maxTurnAngle);
        System.out.println("jist.plugins"+"\t"+"stopFA is " +stopFA);
        System.out.println("jist.plugins"+"\t"+"Tracking "+numFibers+" fibers...");
        numFibers=0;
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;
        int fiberCount=0;
        int i=seed.z;
        int j=seed.y;
        int k=seed.x;
        setTotalUnits(fiberCount);
        if(faData[k][j][i]>=startFA) {
        	incrementCompletedUnits();
        	//System.out.println("jist.plugins"+"\t"+"Tracking another fiber from "+k+" " +j+" "+i);
        	FACTfiber newFiber = new FACTfiber();
        	newFiber.setRes(resolution.x, resolution.y, resolution.z);
        	//track fiber here
        	long mem1 = Runtime.getRuntime().freeMemory();

        	int myLen=newFiber.track((float)k+0.5f,(float)j+0.5f,(float)i+0.5f);

        	long mem2 = Runtime.getRuntime().freeMemory();

        	//IGNORE FIBERS OF LENGTH < 7
        	if (myLen>6){
        		leng7++;

        		diff += (mem2-mem1);

        		if(myLen<minLen) {
        			minLen=myLen;
        		} else{
        			len2+=myLen;
        			cnt++;
        		}
        		if (myLen>maxlen) { maxlen=myLen; }
        		len+=myLen;

        		fibers.add(numFibers,createFiber(newFiber));
        		numFibers++;

        	}
        }


        mnLen = (float)len/(float)numFibers;
        System.out.println("jist.plugins"+"\t"+"Finished, and tracked: " + numFibers + " fibers");
        System.out.println("jist.plugins"+"\t"+"Mean length: "+mnLen);
        System.out.println("jist.plugins"+"\t"+"Min length: "+minLen);
        System.out.println("jist.plugins"+"\t"+"Max length: "+maxlen);
        
        System.out.println("jist.plugins"+"\t"+fibers.get(0));
        
        return fibers;
    }
    protected Fiber createFiber(FACTfiber factfbr){
    	XYZ[] pts=new XYZ[factfbr.fiberPoints.size()];
    	Point3f pt;
    	for(int i=0;i<pts.length;i++){
    		pt=factfbr.fiberPoints.get(i);
    		pts[i]=new XYZ(pt.x,pt.y,pt.z);
    	}
    	Fiber fbr=new Fiber(pts);
    	return fbr;
    }
    protected class FACTfiber {

    public int numPoints;
    public LinkedList<Point3f> fiberPoints;
    public float resX, resY, resZ;


    private class RAY{
        cPT pt;
        float dist;
        public RAY(float x, float y, float z,float d) {
            pt = new cPT(x,y,z);
            dist = d;
        }
    }
    
    public void setRes(float resX, float resY, float resZ){
    	this.resX=resX;
    	this.resY=resY;
    	this.resZ=resZ;
    	
    }

    public int track(float  startX, float  startY, float  startZ) {
        fiberPoints = new LinkedList<Point3f>();
        fiberPoints.add(new cPT(startX,startY,startZ)); numPoints=1;
        
        //System.out.println("jist.plugins"+"\t"+);System.out.println("jist.plugins"+"\t"+"READY TO START TRACKING...."); System.out.println("jist.plugins"+"\t"+);
        
        int szx = floorinbound(startX,Nx-1);
        int szy = floorinbound(startY,Ny-1);
        int szz = floorinbound(startZ,Nz-1);
        
        track(FORWARD,startX,startY,startZ,vecData[szx][szy][szz][0],
                vecData[szx][szy][szz][1],vecData[szx][szy][szz][2],
                startX,startY,startZ);
        track(BACKWARD,startX,startY,startZ,-vecData[szx][szy][szz][0],
                -vecData[szx][szy][szz][1],-vecData[szx][szy][szz][2],
                startX,startY,startZ);
        return numPoints;
    }

    private int floorinbound(float f, int max){
    	int ff = (int)f;
    	if(ff<0){ ff=0; }
    	else if(ff>max){ ff=max; }
    	return ff;
    }

    private void track(boolean addToEnd, float  x, float  y, float  z, float last_vx, float last_vy, float last_vz,
                       float x_c,float y_c,float z_c) {
        float vx,vy,vz;
        
        Point3i last = null;
        Point3i last2 = null;
        Point3i last3 = null;
        
        int xr = floorinbound(x,Nx-1);
        int yr = floorinbound(y,Ny-1);
        int zr = floorinbound(z,Nz-1);
        
        int pts =0;
        
        while(pts<MAX_LENGTH) { //loop until hitting a stop condition - prevent super long fibers
            pts++;
            
            if (pts>MAX_LENGTH-1){System.out.println("jist.plugins"+"\t"+"Fiber has crossed " + pts + " points");}
            vx = vecData[xr][yr][zr][0];
            vy = vecData[xr][yr][zr][1];
            vz = vecData[xr][yr][zr][2];
            
            double angle = vectAng(vx,vy,vz,last_vx,last_vy,last_vz);


            if(angle>pi2) {
                angle = Math.PI-angle;
                vx=-vx;vy=-vy;vz=-vz;
            }
            if(angle<0){
//            	System.out.println("jist.plugins"+"\t"+"******* End of Fiber - No Dir *******");
                return; //No direction
            }
            if(angle>maxTurnAngle) {
//            	System.out.println("jist.plugins"+"\t"+"******* End of Fiber - Turn Ang *******");
                return; //too high turn angle
            }
            if(faData[xr][yr][zr]<stopFA) {
//            	System.out.println("jist.plugins"+"\t"+"******* End of Fiber - Low FA *******");
                return; //too low fa
            }
            if(Float.isNaN(faData[xr][yr][zr])){
            	return; // FA is NaN
            }
            if(Float.isNaN(vecData[xr][yr][zr][0])||Float.isNaN(vecData[xr][yr][zr][1])||Float.isNaN(vecData[xr][yr][zr][2])){
            	return; // VEC is NaN
            }
            
            /* Original method why stepping through the data*/
            /**********************************************
             // Scale v, vectors
             vx = (float)(vx/resX)*stepSize;
             vy = (float)(vy/resY)*stepSize;
             vz = (float)(vz/resZ)*stepSize;
             while((x==Math.round(x_c))&&(y==Math.round(y_c))&&(z==Math.round(z_c))){
             x_c+=vx;
             y_c+=vy;
             z_c+=vz;
             }
             x = (char )Math.round(x_c);
             y = (char )Math.round(y_c);
             z = (char )Math.round(z_c);
             **********************************************/
            RAY DX, DY, DZ; // Distance to nearest X Y and Z planes

            if((float)(vx/resX)>0){
            	DX = distInt(x_c,y_c,z_c,(float)(vx/resX),(float)(vy/resY),(float)(vz/resZ),1,0,0,(float)Math.ceil(x_c+0.0001f));
            }else{
            	DX = distInt(x_c,y_c,z_c,(float)(vx/resX),(float)(vy/resY),(float)(vz/resZ),1,0,0,(float)Math.floor(x_c-0.0001f));
            }

            if((float)(vy/resY)>0){
            	DY = distInt(x_c,y_c,z_c,(float)(vx/resX),(float)(vy/resY),(float)(vz/resZ),0,1,0,(float)Math.ceil(y_c+0.0001f));
            }else{
            	DY = distInt(x_c,y_c,z_c,(float)(vx/resX),(float)(vy/resY),(float)(vz/resZ),0,1,0,(float)Math.floor(y_c-0.0001f));
            }

            if((float)(vz/resZ)>0){
            	DZ = distInt(x_c,y_c,z_c,(float)(vx/resX),(float)(vy/resY),(float)(vz/resZ),0,0,1,(float)Math.ceil(z_c+0.0001f));
            }else{
            	DZ = distInt(x_c,y_c,z_c,(float)(vx/resX),(float)(vy/resY),(float)(vz/resZ),0,0,1,(float)Math.floor(z_c-0.0001f));
            }
            
            if((DX.dist<DY.dist)&&(DX.dist<DZ.dist)) {
                //X is minimum
                x_c=DX.pt.x;
                y_c=DX.pt.y;
                z_c=DX.pt.z;
                if(vx>0){
                	xr++;
                }else{
                	xr--;
                }
              //check and update the discrete coordinates
                if(yr!=(int)Math.floor(y_c)){
//                	System.out.println("changed x, fixing y");
                	yr=(int)Math.floor(y_c);
                }
                if(zr!=Math.floor(z_c)){
//                	System.out.println("changed x, fixing z");
                	zr=(int)Math.floor(z_c);
                }
                
            }  else {
                if((DY.dist<DZ.dist)) {
                    // Y is minimum
                    x_c=DY.pt.x;
                    y_c=DY.pt.y;
                    z_c=DY.pt.z;
                    if(vy>0){
                    	yr++;
                    }else{
                    	yr--;
                    }
                  //check and update the discrete coordinates
                    if(xr!=(int)Math.floor(x_c)){
//                    	System.out.println("changed y, fixing x");
                    	yr=(int)Math.floor(y_c);
                    }
                    if(zr!=Math.floor(z_c)){
//                    	System.out.println("changed y, fixing z");
                    	zr=(int)Math.floor(z_c);
                    }
                    
                } else {
                    //Z is minimum
                    x_c=DZ.pt.x;
                    y_c=DZ.pt.y;
                    z_c=DZ.pt.z;
                    
                    if(vz>0){
                    	zr++;
                    }else{
                    	zr--;
                    }
                  //check and update the discrete coordinates
                    if(yr!=(int)Math.floor(y_c)){
//                    	System.out.println("changed z, fixing y");
                    	yr=(int)Math.floor(y_c);
                    }
                    if(xr!=Math.floor(x_c)){
//                    	System.out.println("changed z, fixing x");
                    	xr=(int)Math.floor(x_c);
                    }
                }
            }
            
            
            if(Float.isNaN(x_c)||Float.isNaN(y_c)||Float.isNaN(z_c)){
            	System.out.println("jist.plugins"+"\t"+"********* IS NAN! *********");
            	System.out.println("jist.plugins"+"\t"+"Vec: " + vx +","+vy+","+vz);
            }
            
            Point3i thiscoord = new Point3i(xr,yr,zr);
            Point3f thispt = new Point3f(x_c,y_c,z_c);
            
            if(thiscoord.equals(last)){
            	return;
            }
            if(thiscoord.equals(last2)){
            	return;
            }
            if(thiscoord.equals(last3)){
            	return;
            }
            
            if((x_c<0)||(y_c<0)||(z_c<0)||(x_c>=Nx)||(y_c>=Ny)||(z_c>=Nz)) {
            	return; //out of bounds
            }
            
            if((xr>=0)&&(yr>=0)&&(zr>=0)&&(xr<Nx)&&(yr<Ny)&&(zr<Nz)) {
            	numPoints++;
                if(addToEnd){ fiberPoints.addFirst(thispt); }
                else{ fiberPoints.addLast(thispt); }
            } else {
                return; //out of bounds
            }
            

            

            last_vx = vx;
            last_vy = vy;
            last_vz = vz;
            
            last3 = last2;
            last2 = last;
            last = thiscoord;
        }
//        System.out.println("jist.plugins"+"\t"+"****** End of Fiber ******");
        //System.out.println("jist.plugins"+"\t"+fiberPoints);
        //System.out.println("jist.plugins"+"\t"+"Found REALLY long fiber");

    }

    private RAY distInt(float x, float y, float z, float vx, float vy, float vz, float nx, float ny, float nz, float v) {
        float nDotv =vx*nx + vy*ny + vz*nz;
        if(nDotv==0) {
            //Parallel
            return new RAY(x,y,z,Float.MAX_VALUE);
        }
        float s =( nx*(v*nx-x) + ny*(v*ny-y) + nz*(v*nz-z)) / nDotv;
        
        return new RAY((x+vx*s),(y+vy*s),(z+vz*s),s);
    }

    private double vectAng(double vx1, double vy1, double vz1, double vx2, double vy2, double vz2) {
        double dv1 = Math.sqrt(vx1*vx1 + vy1*vy1 + vz1*vz1);
        double dv2 = Math.sqrt(vx2*vx2 + vy2*vy2 + vz2*vz2);
        if((dv1==0)||(dv2==0))
            return -1;
        //normalize
        vx1=vx1/dv1;
        vy1=vy1/dv1;
        vz1=vz1/dv1;
        vx2=vx2/dv2;
        vy2=vy2/dv2;
        vz2=vz2/dv2;
        //finite precision can result in normalized sums greater than +-1. catch these with the min
        return Math.acos(Math.max(-1.0f,Math.min(vx1*vx2 + vy1*vy2 + vz1*vz2,1.0f)));
    }

    }

    
}