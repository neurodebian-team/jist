package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class StapleReader {

	public static int[][][][] readImgs(String[] names, int[] dim){
		int[][][][] imgs = new int[dim[0]][dim[1]][dim[2]][names.length];
		try{
			for(int l=0; l<names.length;l++){
				DataInputStream imin = new DataInputStream(new FileInputStream(names[l]));
				for(int k=0; k<dim[2];k++){
					for(int j=0; j<dim[1];j++){
						for(int i=0; i<dim[0];i++){
							imgs[i][j][k][l]=imin.readByte();
						}
					}
				}
			}

		}catch(IOException e){
			e.printStackTrace();
		}
		
		return imgs;
	}
	
}
