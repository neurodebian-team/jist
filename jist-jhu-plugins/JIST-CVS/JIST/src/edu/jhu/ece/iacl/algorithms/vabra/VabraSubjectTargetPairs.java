package edu.jhu.ece.iacl.algorithms.vabra;

import java.util.ArrayList;
import java.util.List;

import edu.jhmi.rad.medic.libraries.ImageFunctionsPublic;
import edu.jhu.bme.smile.commons.math.Spline;
import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;
import edu.jhu.bme.smile.commons.optimize.Optimizable1DContinuous;
import edu.jhu.bme.smile.commons.optimize.Optimizer1DContinuous;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities.InterpolationType;
import edu.jhu.ece.iacl.algorithms.spectro.MSWASSR.Symscore;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;

public class VabraSubjectTargetPairs extends AbstractCalculation{

	protected VabraHistograms hist;

	protected int maxDimensions;
	protected double[] maxValsD;
	protected double[] minValsD;
	protected double[] intervalsD;

	protected VabraVolumeCollection subject;
	protected VabraVolumeCollection deformedSubject;
	protected VabraVolumeCollection target;

	protected VabraVolumeCollection normedTarget;
	protected VabraVolumeCollection normedDeformedSubject;

	protected double currentDownSampleFactor;

	protected int boundingBox[] = new int[6];

	protected ImageDataFloat currentDeformField; //deformation field 
	protected float[][][][] currentDeformFieldM; //deformation field as an array for easy access
	protected ImageDataFloat totalDeformField; //deformation field 
	protected float[][][][] totalDeformFieldM; //deformation field as an array for easy access

	protected List<ImageData> origSubjectList;
	protected List<ImageData> origTargetList;
	protected double[] chWeights;
	protected int[] chInterpType;
	protected int numOfBins;
	protected int numOfCh;
	protected float RobustMinThresh, RobustMaxThresh;

	public void dispose() {

		currentDeformField = null;
		currentDeformFieldM = null;
		totalDeformField = null;
		//totalDeformFieldM = null;
		subject=null;
		deformedSubject=null;
		origSubjectList=null;
		origTargetList=null;
		normedTarget = null;
		normedDeformedSubject = null;
		hist.dispose();
	}

	public VabraSubjectTargetPairs(List<ImageData> subjectVols, List<ImageData> targetVols,
			AbstractCalculation parent, int[] InterpType) {
		this(subjectVols, targetVols, parent, 0.000f, 0.000f, VabraHistograms.defaultBins, InterpType);
	}

	public VabraSubjectTargetPairs(List<ImageData> subjectVols, List<ImageData> targetVols, AbstractCalculation parent,
			float robustMaxT, float robustMinT, int numBins, int[] interpType) {
		super(parent);

		this.origSubjectList=subjectVols;
		this.origTargetList=targetVols;
		this.chInterpType = interpType;
		this.subject=new VabraVolumeCollection(subjectVols, chInterpType);
		deformedSubject=this.subject.clone();
		this.target=new VabraVolumeCollection(targetVols, chInterpType);

		numOfCh = subjectVols.size();
		chWeights = new double[numOfCh];

		for (int i = 0; i < numOfCh; i++ ){
			if (numOfCh == 1) chWeights[0] = 1; 
			else chWeights[i] = 1/((double)numOfCh);
		}

		calculateBoundingBox();

		RobustMinThresh = robustMinT;
		RobustMaxThresh = robustMaxT;

		numOfBins = numBins;

		minValsD = new double[numOfCh];
		maxValsD = new double[numOfCh];
		intervalsD = new double[numOfCh];

		setAlltoRobustHist();
		calculateMaxAndMinVals();
		normalizeImagesToMax();

		incrementCompletedUnits();
		normedTarget = target.clone();
		normedTarget.normalize(minValsD, maxValsD, numOfBins);
		hist = new VabraHistograms(numOfCh, numOfBins, parent);
		incrementCompletedUnits();

		totalDeformFieldSplit = new ImageData[3];


		return;
	}


	public int calculateBin(double val, int ch) {
		return (int) Math.floor((val - minValsD[ch]) / intervalsD[ch]);
	}

	public void threshAtRobustMaxAndMin(VabraVolumeCollection imgVec) {

		float robustMax, robustMin;
		int XN, YN, ZN, i, j, k;

		for (int ch = 0; ch < imgVec.getNumOfCh(); ch++) {
			XN = imgVec.data[ch].getRows();
			YN = imgVec.data[ch].getCols();
			ZN = imgVec.data[ch].getSlices();

			if (RobustMaxThresh > 0.00001f) {
				robustMax = ImageFunctionsPublic.robustMaximum(imgVec.data[ch], RobustMaxThresh, 2, XN, YN, ZN);
				for (i = 0; i < XN; i++) for (j = 0; j < YN; j++) for (k = 0; k < ZN; k++) {
					if (imgVec.data[ch].getFloat(i, j, k) > robustMax) {
						imgVec.data[ch].set(i, j, k, robustMax);
					}
				}
			}

			if (RobustMinThresh > 0.00001f) {
				robustMin = ImageFunctionsPublic.robustMinimum(imgVec.data[ch], RobustMinThresh, 2, XN, YN, ZN);
				for (i = 0; i < XN; i++) for (j = 0; j < YN; j++) for (k = 0; k < ZN; k++) {
					if (imgVec.data[ch].getFloat(i, j, k) < robustMin) {
						imgVec.data[ch].set(i, j, k, robustMin);
					}
				}
			}


		}
	}

	public void setAlltoRobustHist() {
		threshAtRobustMaxAndMin(subject);
		threshAtRobustMaxAndMin(target);
		threshAtRobustMaxAndMin(deformedSubject);
	}

	public void normalizeImagesToMax() {
		for (int ch = 0; ch < numOfCh; ch++) {
			ImageDataMath.normalizeToUnitIntensity(subject.data[ch]);
			ImageDataMath.scaleFloatValue(subject.data[ch],(float) maxValsD[ch]);

			ImageDataMath.normalizeToUnitIntensity(deformedSubject.data[ch]);
			ImageDataMath.scaleFloatValue(deformedSubject.data[ch], (float) maxValsD[ch]);

			ImageDataMath.normalizeToUnitIntensity(target.data[ch]);
			ImageDataMath.scaleFloatValue(target.data[ch], (float) maxValsD[ch]);
		}
	}


	public void prepareForNextLevel() {
		// System.out.println(getClass().getCanonicalName()+"\t"+"START PREPARE FOR NEXT LEVEL");
		double nmiVal = -RegistrationUtilities.NMI(hist.origDeformedSubject, hist.origTarget,
				hist.origJointST, 0, numOfBins);
		System.out.println(getClass().getCanonicalName()+"\t"+"NMI before prepForNextLevelParent=" + nmiVal);

		deformedSubject = subject.returnDeformedCopy(totalDeformField);
		deformedSubjectCopy = deformedSubject.clone();


		setAlltoRobustHist();
		calculateMaxAndMinVals();
		normalizeImagesToMax();
		// renormalize the target based on new min and max values
		normedTarget = target.clone();
		normedTarget.normalize(minValsD, maxValsD, numOfBins);

		normedDeformedSubject = deformedSubject.clone();
		normedDeformedSubject.normalize(minValsD, maxValsD, numOfBins);
		calculateBoundingBox();
		hist.updateHistograms(normedTarget,normedDeformedSubject,boundingBox);
		nmiVal = -RegistrationUtilities.NMI(hist.origDeformedSubject, hist.origTarget,
				hist.origJointST, 0, numOfBins);
		System.out.format("Current NMI=%f\n", nmiVal);
		// System.out.format("I'm bothered that NMI goes down a little bit in
		// between levels... it has something to do with hist.origJointogram
		// not being consitent\n");

	}

	public void calculateMaxAndMinVals() {

		int ch;
		int CH;
		int i, j, k;

		CH = numOfCh;
		int XN, YN, ZN;
		XN = subject.getXN();
		YN = subject.getYN();
		ZN = subject.getZN();



		for (ch = 0; ch < CH; ch++) {
			double max = Double.NEGATIVE_INFINITY;
			double min = Double.POSITIVE_INFINITY;
			ImageData defsub = deformedSubject.data[ch];
			ImageData sub = subject.data[ch];
			ImageData tar = target.data[ch];
			for (i = 0; i < XN; i++) for (j = 0; j < YN; j++) for (k = 0; k < ZN; k++) {
				if (defsub.getDouble(i, j, k) > max) max = defsub.getDouble(i, j, k);
				if (defsub.getDouble(i, j, k) < min) min = defsub.getDouble(i, j, k);
				if (sub.getDouble(i, j, k) > max) max = sub.getDouble(i, j, k);
				if (sub.getDouble(i, j, k) < min) min = sub.getDouble(i, j, k);
				if (tar.getDouble(i, j, k) > max) max = tar.getDouble(i, j, k);
				if (tar.getDouble(i, j, k) < min) min = tar.getDouble(i, j, k);
			}

			minValsD[ch] = min;
			maxValsD[ch] = max;
			intervalsD[ch] = (maxValsD[ch] - minValsD[ch] + 1) / ((double)numOfBins);
		}

	}

	public void setResolution(float downSampleFactor) {

		int newX, newY, newZ;
		int oldX, oldY, oldZ;
		double sigma = 1.0;

		// System.out.format("imagepairChild down sample factor %f\n",
		// downSampleFactor);
		if (currentDownSampleFactor == downSampleFactor)
			return;

		oldX = subject.getXN();
		oldY = subject.getYN();
		oldZ = subject.getZN();

		currentDownSampleFactor = downSampleFactor;
		reintializeFromFile();

		if (currentDownSampleFactor != 1.0) {
			newX = (int) (subject.getXN() / currentDownSampleFactor);
			newY = (int) (subject.getYN() / currentDownSampleFactor);
			newZ = (int) (subject.getZN() / currentDownSampleFactor);
		} else {
			newX = (int) (subject.getXN());
			newY = (int) (subject.getYN());
			newZ = (int) (subject.getZN());
		}
		subject.downSample(newX, newY, newZ, sigma);
		target.downSample(newX, newY, newZ, sigma);
		if (totalDeformField == null) {
			// setup deformation fields for first time
			totalDeformField = new ImageDataFloat(subject.getXN(), subject.getYN(), subject.getZN(), 3);
		} else {
			// resample deformation fields
			totalDeformField = new ImageDataFloat(subject.getXN(), subject.getYN(), subject.getZN(), 3);
			RegistrationUtilities.DeformationFieldResample3DM(totalDeformFieldM, totalDeformField, oldX, oldY, oldZ, newX, newY, newZ);
		}

		totalDeformFieldM = totalDeformField.toArray4d();
		
		currentDeformField = new ImageDataFloat(subject.getXN(), subject.getYN(), subject.getZN(), 3);
		currentDeformFieldM = currentDeformField.toArray4d();		

		for(int c = 0; c < totalDeformField.getComponents(); c++){
			totalDeformFieldSplit[c] = new ImageDataFloat(subject.getXN(), subject.getYN(), subject.getZN());
			for(int i = 0; i < subject.getXN(); i++)
				for(int j = 0; j < subject.getYN(); j++)
					for(int k = 0; k < subject.getZN(); k++){
						totalDeformFieldSplit[c].set(i, j, k, totalDeformField.getDouble(i, j, k, c));
					}
		}
	}

	VabraVolumeCollection deformedSubjectCopy;
	ImageData[]  totalDeformFieldSplit;
	
	public void updateDefField(){
		int[] regionToUpdate = new int[6];
		regionToUpdate[0] = 0;
		regionToUpdate[1] = subject.getXN() -1;
		regionToUpdate[2] = 0;
		regionToUpdate[3] = subject.getYN() -1;
		regionToUpdate[4] = 0;
		regionToUpdate[5] = subject.getZN() -1;
		updateDefField(regionToUpdate);
	}
	
	public void updateDefField(int[] regionToUpdate){

		double newVec, oldVec, x, y, z;
		int XN = totalDeformField.getRows();
		int YN = totalDeformField.getCols();
		int ZN = totalDeformField.getSlices();

		for(int i = regionToUpdate[0]; i <= regionToUpdate[1]; i++)
			for(int j = regionToUpdate[2]; j <= regionToUpdate[3]; j++)
				for(int k = regionToUpdate[4]; k <= regionToUpdate[5]; k++){
					if(currentDeformFieldM[i][j][k][0] != 0 || currentDeformFieldM[i][j][k][1] != 0 || currentDeformFieldM[i][j][k][2] != 0 ){
						x = i + currentDeformFieldM[i][j][k][0];
						y = j + currentDeformFieldM[i][j][k][1];
						z = k + currentDeformFieldM[i][j][k][2];

						for(int c = 0; c < 3; c++){
							oldVec = RegistrationUtilities.Interpolation(totalDeformFieldSplit[c], XN, YN, ZN, x, y, z, 0);
							newVec = currentDeformFieldM[i][j][k][c] + oldVec;
							totalDeformField.set(i, j, k, c, newVec);
							currentDeformFieldM[i][j][k][c] = 0;
						}
					}
				}

		for(int c = 0; c < totalDeformField.getComponents(); c++)
		{
			for(int i = regionToUpdate[0]; i <= regionToUpdate[1]; i++)
				for(int j = regionToUpdate[2]; j <= regionToUpdate[3]; j++)
					for(int k = regionToUpdate[4]; k <= regionToUpdate[5]; k++){
						totalDeformFieldSplit[c].set(i, j, k, totalDeformField.getDouble(i, j, k, c));
					}
		}

	}
	
	void adjustHistBinsTo(VabraVolumeCollection subject, double x, double y, double z,  int[][] subjectHist, int[][][] jointHist, int[] targetBins,int[] subjectBins){
		
		double[] testValsD = new double[numOfCh]; 
		int[] testBins = new int[numOfCh];
		
		if (x < subject.getXN() && x >= 0 && y < subject.getYN() && y >= 0 && z < subject.getZN() && z >= 0) {
			subject.interpolate(x, y, z, testValsD);
		} else {
			for (int ch = 0; ch < numOfCh; ch++) testValsD[ch] = minValsD[ch];
		}
		
		for (int ch = 0; ch < numOfCh; ch++) {
			testBins[ch] = calculateBin(testValsD[ch], ch);
			hist.adjustBins(subjectHist, jointHist, subjectBins[ch],targetBins[ch],testBins[ch],ch);

		}
	}


	public double totalDeformFieldInterpolate(double x, double y, double z, int dim){
		int XN = totalDeformField.getRows();
		int YN = totalDeformField.getCols();
		int ZN = totalDeformField.getSlices();
		int i0, j0, k0, i1, j1, k1;
		double dx, dy, dz, hx, hy, hz;
		if (x < 0 || x > (XN - 1) || y < 0 || y > (YN - 1) || z < 0
				|| z > (ZN - 1)) {
			return 0;
		} else {
			j1 = (int) Math.ceil(x);
			i1 = (int) Math.ceil(y);
			k1 = (int) Math.ceil(z);
			j0 = (int) Math.floor(x);
			i0 = (int) Math.floor(y);
			k0 = (int) Math.floor(z);
			dx = x - j0;
			dy = y - i0;
			dz = z - k0;

			// Introduce more variables to reduce computation
			hx = 1.0f - dx;
			hy = 1.0f - dy;
			hz = 1.0f - dz;
			// Optimized below
			return   (((totalDeformField.getDouble(j0, i0, k0, dim) * hx + totalDeformField.getDouble(j1, i0, k0, dim) * dx) * hy 
					+ (totalDeformField.getDouble(j0, i1, k0, dim) * hx + totalDeformField.getDouble(j1, i1, k0, dim) * dx) * dy) * hz 
					+ ((totalDeformField.getDouble(j0, i0, k1, dim) * hx + totalDeformField.getDouble(j1, i0, k1, dim) * dx) * hy 
							+ (totalDeformField.getDouble(j0, i1, k1, dim) * hx + totalDeformField.getDouble(j1, i1, k1, dim) * dx) * dy)* dz);

		}
	}


	public int coarseGradientParameters() {
		return 3; 
	}

	void reintializeFromFile() {
		this.subject=new VabraVolumeCollection(origSubjectList, chInterpType);
		this.target=new VabraVolumeCollection(origTargetList, chInterpType);
		System.gc();
		//calculateBoundingBox();
	}

	void calculateBoundingBox() {
		int k, j, i, ch;
		int XN, YN, ZN, CH, ext;
		XN = subject.getXN();
		YN = subject.getYN();
		ZN = subject.getZN();
		CH = numOfCh;
		ext = 5;

		boundingBox[1] = 0;
		boundingBox[0] = subject.getXN();
		boundingBox[3] = 0;
		boundingBox[2] = subject.getYN();
		boundingBox[5] = 0;
		boundingBox[4] = subject.getZN();

		int count=0;
		for (ch = 0; ch < CH; ch++) {
			ImageData sub=deformedSubject.data[ch];
			ImageData tar=target.data[ch];
			for (i = 0; i < XN; i++) for (j = 0; j < YN; j++) for (k = 0; k < ZN; k++){
				if ((Math.abs(sub.getDouble(i, j, k)) > 0.0000001) || (Math.abs(tar.getDouble(i, j, k)) > 0.0000001))
				{
					count++;
					if (i < boundingBox[0]) boundingBox[0] = i;
					if (i > boundingBox[1]) boundingBox[1] = i;
					if (j < boundingBox[2]) boundingBox[2] = j;
					if (j > boundingBox[3]) boundingBox[3] = j;
					if (k < boundingBox[4]) boundingBox[4] = k;
					if (k > boundingBox[5]) boundingBox[5] = k;
				}
			}
		}

		boundingBox[0]=Math.max(0, boundingBox[0]-ext); 			
		boundingBox[1]=Math.min(XN-1, boundingBox[1]+ext);
		boundingBox[2]=Math.max(0, boundingBox[2]-ext); 			
		boundingBox[3]=Math.min(YN-1, boundingBox[3]+ext);
		boundingBox[4]=Math.max(0, boundingBox[4]-ext); 			
		boundingBox[5]=Math.min(ZN-1, boundingBox[5]+ext);

		maxDimensions = Math.max(Math.max(boundingBox[1]-boundingBox[0], boundingBox[3]-boundingBox[2]), boundingBox[5]-boundingBox[4]);
	}


	List<ImageData> getDeformedSubject(){
		ArrayList<ImageData> out = new ArrayList<ImageData>();
		ImageData defSub, origSub;
		for (int i = 0; i < numOfCh; i++){
			origSub = origSubjectList.get(i);
			defSub = origSub.clone();
			RegistrationUtilities.DeformImage3D(origSub, defSub, totalDeformField, origSub.getRows(),
					origSub.getCols(), origSub.getSlices(), chInterpType[i]); 
			defSub.setHeader(origSubjectList.get(i).getHeader());
			defSub.setName(origSubjectList.get(i).getName() + "_reg");
			out.add(defSub);
		}
		return out;
	}

	ImageDataFloat getDeformationField(){
		totalDeformField.setHeader(origSubjectList.get(0).getHeader());
		totalDeformField.setName(origSubjectList.get(0).getName()+"_def_field");
		return totalDeformField;
	}

	public int[] getBoundingBox() {
		calculateBoundingBox();
		return boundingBox;
	}

}
