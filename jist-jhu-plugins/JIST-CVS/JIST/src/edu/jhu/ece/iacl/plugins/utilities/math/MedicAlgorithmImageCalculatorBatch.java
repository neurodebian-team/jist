package edu.jhu.ece.iacl.plugins.utilities.math;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;


/*
 * @author John Bogovic (bogovic@jhu.edu)
 *
 */

public class MedicAlgorithmImageCalculatorBatch extends ProcessingAlgorithm {
	ParamVolumeCollection volParam;

	ParamVolume resultVolParam;
	ParamOption operation;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Batch process image calculations. The operations include 'Add', 'Subtract', 'Multiply', 'Average', 'Min, 'Max''";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(volParam=new ParamVolumeCollection("Volumes"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Add","Subtract","Multiply","Average","Min","Max"}));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Math");
		inputParams.setLabel("Image Calculator Batch");
		inputParams.setLabel("Image Calculator Batch");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVolParam=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		int rows=volParam.getImageDataList().get(0).getRows();
		int cols=volParam.getImageDataList().get(0).getCols();
		int slices=volParam.getImageDataList().get(0).getSlices();
		int comps=volParam.getImageDataList().get(0).getComponents();
		ImageData resultVol = new ImageDataFloat(volParam.getImageDataList().get(0));

		int N = volParam.getImageDataList().size();
		for(int list=1; list<N; list++){
			ImageData img = volParam.getImageDataList().get(list);
			System.out.println(getClass().getCanonicalName()+"\t"+"Processing volume: "+list+" : "+img.getName());
			double tmp1;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						for (int l = 0; l < comps; l++) {
							tmp1=img.getDouble(i, j, k, l);

							switch(operation.getIndex()){
							case 0:resultVol.set(i, j, k, l, resultVol.get(i, j, k, l).doubleValue()+tmp1);break;
							case 1:resultVol.set(i, j, k, l, resultVol.get(i, j, k, l).doubleValue()-tmp1);break;
							case 2:resultVol.set(i, j, k, l, resultVol.get(i, j, k, l).doubleValue()*tmp1);break;
							case 3:resultVol.set(i, j, k, l, (resultVol.get(i, j, k, l).doubleValue()+tmp1));break;
							case 4:if(tmp1<resultVol.get(i,j,k,l).doubleValue()){resultVol.set(i, j, k, l, tmp1);};break;
							case 5:if(tmp1>resultVol.get(i,j,k,l).doubleValue()){resultVol.set(i, j, k, l, tmp1);};break;
							default:
								throw new RuntimeException("Invalid Operation Index");
							}
						}
					}
				}
			}
		}
		if(operation.getIndex()==3) {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						for (int l = 0; l < comps; l++) {
							resultVol.set(i, j, k, l, resultVol.get(i, j, k, l).doubleValue()/N);
						}
					}
				}
			}
		}
		resultVol.setName(resultVol.getName()+"_calc");
		resultVol.setHeader(volParam.getImageDataList().get(0).getHeader());
		resultVolParam.setValue(resultVol);
	}
}
