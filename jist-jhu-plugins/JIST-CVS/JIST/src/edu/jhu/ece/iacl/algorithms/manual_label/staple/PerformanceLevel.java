package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author John Bogovic
 * @date 5/31/2008
 * 
 * Structure for storing and managing performance levels for STAPLE
 */


public class PerformanceLevel {
	
	protected float[][][] pl;
	protected double[][][] dpl;
	protected int numlabels;
	protected int numraters;
	/*
	 * First dimension stores information for each rater.
	 * The remaining is an NxN matrix where N is the number of labels
	 * and the i-j-k^th element characterizes the probability that the 
	 * ith rater will decide label k is present when label j is the truth. 
	 */
	
	public PerformanceLevel(){	
	}
	
	public PerformanceLevel(int numlabels, int numraters){
		pl=new float[numraters][numlabels][numlabels];
		this.numlabels = numlabels;
		this.numraters = numraters;
	}
	public PerformanceLevel(int numlabels,int numraters,boolean isdouble){
		if(isdouble){
			dpl=new double[numraters][numlabels][numlabels];
		}else{
			pl=new float[numraters][numlabels][numlabels];
		}
		this.numlabels = numlabels;
		this.numraters = numraters;
	}
	public PerformanceLevel(List<double[][]> perfin){
		setFromDoubleArrayList(perfin);
	}
	public PerformanceLevel(List<double[][]> perfin, boolean isdouble){
		setFromDoubleArrayList(perfin, isdouble);
	}
	public float[][][] getPerformance(){
		return pl;
	}
	public double[][][] getPerformanceD(){
		return dpl;
	}
	
	public float get(int i, int j, int k){
		return pl[i][j][k];
	}
	public double getD(int i, int j, int k){
		return dpl[i][j][k];
	}
	
	public void set(int i, int j, int k, float f){
		pl[i][j][k]=f;
	}
	public void set(int i, int j, int k, double f){
		dpl[i][j][k]=f;
	}
	
	/**
	 * Initialize all performance levels the the same value.
	 * @param The value with which to initialize all peformance level values.  A number ~=1 (e.g. 0.99999) is suggested. 
	 */
	/*
	 * DEPRICATED METHODS
	 * 
	public void initialize2(float init){
		for(int i=0; i<pl.length; i++){
			for(int j=0; j<pl[0].length; j++){
				for(int k=0; k<pl[0][0].length; k++){
					pl[i][j][k]=init;
				}
			}
		}
	}
	public void initialize2(double init){
		for(int i=0; i<dpl.length; i++){
			for(int j=0; j<dpl[0].length; j++){
				for(int k=0; k<dpl[0][0].length; k++){
					dpl[i][j][k]=init;
				}
			}
		}
	}
	*/
	public void initialize(float init){
		float initc = (1-init)/(numlabels-1);
		for(int i=0; i<numraters; i++){
			for(int j=0; j<numlabels; j++){
				for(int k=0; k<numlabels; k++){
					if(j==k){
						pl[i][j][k]=init;
					}else{
						pl[i][j][k]=initc;
					}
				}
			}
		}
	}
	public void initialize(double init){
		double initc = (1-init)/(numlabels-1);
		for(int i=0; i<numraters; i++){
			for(int j=0; j<numlabels; j++){
				for(int k=0; k<numlabels; k++){
					if(j==k){
						dpl[i][j][k]=init;
					}else{
						dpl[i][j][k]=initc;
					}
				}
			}
		}
	}
	public void initializeDiag(List<float[][]> init){
		if(init.size()!=numraters){
			System.err.println("Invalid init array size: "+ init.size() + " but numraters: " + numraters);
			return;
		}
		if(init.get(0).length!=numlabels && init.get(0)[0].length!=numlabels){
			System.err.println("Invalid init array size: " + init.get(0).length + "x" + init.get(0)[0].length + " but numlabels: " + numlabels);
			return;
		}
		boolean rows = (init.get(0).length==1);
		for(int i=0; i<numraters; i++){
			for(int j=0; j<numlabels; j++){
				float initc = -1;
				if(rows){
					initc = (1-init.get(i)[0][j])/(numlabels-1);
				}else{
					initc = (1-init.get(i)[j][0])/(numlabels-1);
				}
				for(int k=0; k<numlabels; k++){
					if(rows){
						if(j==k){
							pl[i][j][k]=init.get(i)[0][j];
						}else{
							pl[i][j][k]=initc;
						}
					}else{
						if(j==k){
							pl[i][j][k]=init.get(i)[j][0];
						}else{
							pl[i][j][k]=initc;
						}
					}
					
				}
			}
		}
	}
	
	public void initializeDiagFromD(List<double[][]> init){
		if(init.size()!=numraters){
			System.err.println("Invalid init array size: "+ init.size() + " but numraters: " + numraters);
			return;
		}
		if(init.get(0).length!=numlabels && init.get(0)[0].length!=numlabels){
			System.err.println("Invalid init array size: " + init.get(0).length + "x" + init.get(0)[0].length + " but numlabels: " + numlabels);
			return;
		}
		boolean rows = (init.get(0).length==1);
		for(int i=0; i<numraters; i++){
			for(int j=0; j<numlabels; j++){
				float initc = -1;
				if(rows){
					initc = (float)(1-init.get(i)[0][j])/(numlabels-1);
				}else{
					initc = (float)(1-init.get(i)[j][0])/(numlabels-1);
				}
				for(int k=0; k<numlabels; k++){
					if(rows){
						if(j==k){
							pl[i][j][k]=(float)init.get(i)[0][j];
						}else{
							pl[i][j][k]=initc;
						}
					}else{
						if(j==k){
							pl[i][j][k]=(float)init.get(i)[j][0];
						}else{
							pl[i][j][k]=initc;
						}
					}
					
				}
			}
		}
	}
	
	public void initializeDiagD(List<float[][]> init){
		if(init.size()!=numraters){
			System.err.println("Invalid init array size: "+ init.size() + " but numraters: " + numraters);
			return;
		}
		if(init.get(0).length!=numlabels && init.get(0)[0].length!=numlabels){
			System.err.println("Invalid init array size: " + init.get(0).length + "x" + init.get(0)[0].length + " but numlabels: " + numlabels);
			return;
		}
		boolean rows = (init.get(0).length==1);
		for(int i=0; i<numraters; i++){
			for(int j=0; j<numlabels; j++){
				float initc = -1;
				if(rows){
					initc = (1-init.get(i)[0][j])/(numlabels-1);
				}else{
					initc = (1-init.get(i)[j][0])/(numlabels-1);
				}
				for(int k=0; k<numlabels; k++){
					if(rows){
						if(j==k){
							dpl[i][j][k]=init.get(i)[0][j];
						}else{
							dpl[i][j][k]=initc;
						}
					}else{
						if(j==k){
							dpl[i][j][k]=init.get(i)[j][0];
						}else{
							dpl[i][j][k]=initc;
						}
					}
					
				}
			}
		}
	}
	
	public void initializeDiagFromDD(List<double[][]> init){
		if(init.size()!=numraters){
			System.err.println("Invalid init array size: "+ init.size() + " but numraters: " + numraters);
			return;
		}
		if(init.get(0).length!=numlabels && init.get(0)[0].length!=numlabels){
			System.err.println("Invalid init array size: " + init.get(0).length + "x" + init.get(0)[0].length + " but numlabels: " + numlabels);
			return;
		}
		boolean rows = (init.get(0).length==1);
		for(int i=0; i<numraters; i++){
			for(int j=0; j<numlabels; j++){
				float initc = -1;
				if(rows){
					initc = (float)(1-init.get(i)[0][j])/(numlabels-1);
				}else{
					initc = (float)(1-init.get(i)[j][0])/(numlabels-1);
				}
				for(int k=0; k<numlabels; k++){
					if(rows){
						if(j==k){
							dpl[i][j][k]=(float)init.get(i)[0][j];
						}else{
							dpl[i][j][k]=initc;
						}
					}else{
						if(j==k){
							dpl[i][j][k]=(float)init.get(i)[j][0];
						}else{
							dpl[i][j][k]=initc;
						}
					}
					
				}
			}
		}
	}
	
	public void initializeGlobal(double[][] in){
		if(in[0].length==numraters && in.length==1){
			
			for(int i=0; i<numraters; i++){
				float initc = (float)(1-in[0][i])/(numlabels-1);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						if(j==k){
							pl[i][j][k]=(float)in[0][i];
						}else{
							pl[i][j][k]=initc;
						}
					}
				}
			}
		}else if(in.length==numraters && in[0].length==1){
			for(int i=0; i<numraters; i++){
				float initc = (float)(1-in[i][0])/(numlabels-1);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						if(j==k){
							pl[i][j][k]=(float)in[i][0];
						}else{
							pl[i][j][k]=initc;
						}
					}
				}
			}
		}else{
			System.err.println("Invalid init array size: "+ in.length + "x" + in[0].length + " but numraters: " + numraters);
			return;
		}
	}
	
	public void initializeGlobal(float[][] in){
		if(in[0].length==numraters && in.length==1){
			
			for(int i=0; i<numraters; i++){
				float initc = (float)(1-in[0][i])/(numlabels-1);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						if(j==k){
							pl[i][j][k]=in[0][i];
						}else{
							pl[i][j][k]=initc;
						}
					}
				}
			}
		}else if(in.length==numraters && in[0].length==1){
			for(int i=0; i<numraters; i++){
				float initc = (float)(1-in[i][0])/(numlabels-1);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						if(j==k){
							pl[i][j][k]=in[i][0];
						}else{
							pl[i][j][k]=initc;
						}
					}
				}
			}
		}else{
			System.err.println("Invalid init array size: "+ in.length + "x" + in[0].length + " but numraters: " + numraters);
			return;
		}
	}
	
	public void initializeGlobalD(double[][] in){
		if(in[0].length==numraters && in.length==1){
			
			for(int i=0; i<numraters; i++){
				float initc = (float)(1-in[0][i])/(numlabels-1);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						if(j==k){
							dpl[i][j][k]=(float)in[0][i];
						}else{
							dpl[i][j][k]=initc;
						}
					}
				}
			}
		}else if(in.length==numraters && in[0].length==1){
			for(int i=0; i<numraters; i++){
				float initc = (float)(1-in[i][0])/(numlabels-1);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						if(j==k){
							dpl[i][j][k]=(float)in[i][0];
						}else{
							dpl[i][j][k]=initc;
						}
					}
				}
			}
		}else{
			System.err.println("Invalid init array size: "+ in.length + "x" + in[0].length + " but numraters: " + numraters);
			return;
		}
	}
	
	public void clear(){
		if(pl!=null){
			clearF();
		}else if(dpl!=null){
			clearD();
		}
	}
	public void clearF(){
		for(int i=0; i<pl.length; i++){
			for(int j=0; j<pl[0].length; j++){
				for(int k=0; k<pl[0][0].length; k++){
					pl[i][j][k]=0;
				}
			}
		}
	}
	
	public void clearD(){
		for(int i=0; i<dpl.length; i++){
			for(int j=0; j<dpl[0].length; j++){
				for(int k=0; k<dpl[0][0].length; k++){
					dpl[i][j][k]=0;
				}
			}
		}
	}
	
	public void divideByTots(int col, float tot){
		for(int i=0; i<pl.length; i++){
			for(int j=0; j<pl[0][0].length; j++){
				pl[i][j][col]=pl[i][j][col]/tot;
				if(pl[i][j][col]>1){
					reinitColumn(col,0.9999f);
				}
			}
		}
	}
	public void divideByTots(int col, double tot){
		for(int i=0; i<dpl.length; i++){
			for(int j=0; j<dpl[0][0].length; j++){
				
//				if(dpl[i][j][col]/tot > 1){
//					System.out.println(getClass().getCanonicalName()+"\t"+"Prob.  Divided: "+
//							dpl[i][j][col] +" by " + tot);
//				}
				
				dpl[i][j][col]=dpl[i][j][col]/tot;
			}
		}
	}
	
	/**
	 * Reinitializes column if something messed up happens
	 */
	private void reinitColumn(int col, double init){
		double initc = (1-init)/dpl[0].length;
		for(int i=0; i<dpl.length; i++){
			for(int j=0; j<dpl[0].length; j++){
				if(j==col){
					dpl[i][j][col]=init;
				}else{
					dpl[i][j][col]=initc;
				}
			}
		}
	}
	
	/**
	 * Reinitializes column if something messed up happens
	 */
	private void reinitColumn(int col, float init){
		float initc = (1-init)/pl[0].length;
		for(int i=0; i<pl.length; i++){
			for(int j=0; j<pl[0].length; j++){
				if(j==col){
					pl[i][j][col]=init;
				}else{
					pl[i][j][col]=initc;
				}
			}
		}
	}
	
	/**
	 * Compute normalized trace- 
	 * The sum of the traces of each rater's confusion matrix normalized
	 * by the number of labels and the number of raters
	 * @return Normalized trace
	 */
	public float normalizedTrace(){
		if(pl!=null){
			float out=0;
			for(int i=0; i<pl.length; i++){
				for(int j=0; j<pl[0].length; j++){
					out+=pl[i][j][j];
				}
			}
			out=out/((pl.length)*(pl[0].length));
			return out;
		}else if(dpl!=null){
			float out=0;
			for(int i=0; i<dpl.length; i++){
				for(int j=0; j<dpl[0].length; j++){
					out+=dpl[i][j][j];
				}
			}
			out=out/((dpl.length)*(dpl[0].length));
			return out;
		}else{
			return -1;
		}
	}
	
	public PerformanceLevel clonePL() {
		float[][][] p = getPerformance();
		PerformanceLevel pl2 = new PerformanceLevel(p[0].length,p.length);
		for (int i=0; i<p.length; i++) {
			for (int j=0; j<p[0].length; j++) {
				for (int k=0; k<p[0][0].length; k++) {
					pl2.set(i,j,k,p[i][j][k]);
				}
			}
		}
		return pl2;
	}
	
	public PerformanceLevel clonePLD() {
		double[][][] p = getPerformanceD();
		PerformanceLevel pl2 = new PerformanceLevel(p[0].length,p.length,true);
		for (int i=0; i<p.length; i++) {
			for (int j=0; j<p[0].length; j++) {
				for (int k=0; k<p[0][0].length; k++) {
					pl2.set(i,j,k,p[i][j][k]);
				}
			}
		}
		return pl2;
	}
	
	public void setFromDoubleArrayList(List<double[][]> in){
		setFromDoubleArrayList(in, false);
	}
	
	public void setFromDoubleArrayList(List<double[][]> in, boolean isdouble){
		numraters = in.size();
		int dim1 = in.get(0).length;
		int dim2 = in.get(0)[0].length;
		
		//IN THIS CASE, THE INPUT IS THE "FULL" PERFORMANCE LEVEL
		if(dim1>1 && dim1==dim2){
			setFromFullDoubleArrayList(in, isdouble);
		}else if(dim1==1 && dim2>1){
			numlabels = dim2;
			pl = new float[numraters][numlabels][numlabels];
			initializeDiagFromD(in);
		}else if(dim2==1 && dim1>1){
			numlabels = dim1;
			pl = new float[numraters][numlabels][numlabels];
			initializeDiagFromD(in);
		}else if(dim1==1 && dim2==1){
			
		}else{
			System.err.println("INVALID ARRAY DIMENSIONS");
		}

	}
	
	public void setFromFullDoubleArrayList(List<double[][]> in){
		setFromFullDoubleArrayList(in,false);	
	}
	
	public void setFromFullDoubleArrayList(List<double[][]> in, boolean isDouble){
		
		int numraters = in.size();
		int numlabels = in.get(0).length;
		if(in.get(0).length != in.get(0).length){
			System.err.println("Input arrays must be NxN!");
		}
		
		if(isDouble){
			dpl = new double[numraters][numlabels][numlabels];
			for(int i=0; i<numraters; i++){
				double[][] thisrater = in.get(i);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						dpl[i][j][k] = thisrater[j][k];
					}
				}
			}
		}else{
			pl = new float[numraters][numlabels][numlabels];
			for(int i=0; i<numraters; i++){
				double[][] thisrater = in.get(i);
				for(int j=0; j<numlabels; j++){
					for(int k=0; k<numlabels; k++){
						pl[i][j][k] = (float)thisrater[j][k];
					}
				}
			}
		}
	}
	
	public ArrayList<double[][]> toDoubleArrayList(){
		ArrayList<double[][]> out = null;
		if(pl!=null){
			out = new ArrayList<double[][]>(pl.length);
			for(int i=0; i<pl.length; i++){
				double[][] thisrater = new double[pl[0].length][pl[0][0].length];
				for(int j=0; j<pl[0].length; j++){
					for(int k=0; k<pl[0][0].length; k++){
						thisrater[j][k] = pl[i][j][k];
					}
				}
				out.add(thisrater);
			}
		}else if(dpl!=null){
			out = new ArrayList<double[][]>(dpl.length);
			for(int i=0; i<dpl.length; i++){
				double[][] thisrater = new double[dpl[0].length][dpl[0][0].length];
				for(int j=0; j<dpl[0].length; j++){
					for(int k=0; k<dpl[0][0].length; k++){
						thisrater[j][k] = dpl[i][j][k];
					}
				}
				out.add(thisrater);
			}
		}
		return out;
	}
	
	public String toString(){
		if(pl!=null){
			String sep = "********************";
			String out = ""+sep+"\n";
			for(int i=0; i<pl.length; i++){
				out = out+ "Rater " + (i+1) + "\n";	
				for(int j=0; j<pl[0].length; j++){
					for(int k=0; k<pl[0][0].length; k++){
						out = out+ "\t" + pl[i][j][k];
					}
					out = out+"\n";
				}
				out = out+sep+"\n";
			}
			return out;
		}else if(dpl!=null){
			String sep = "********************";
			String out = ""+sep+"\n";
			for(int i=0; i<dpl.length; i++){
				out = out+ "Rater " + (i+1) + "\n";	
				for(int j=0; j<dpl[0].length; j++){
					for(int k=0; k<dpl[0][0].length; k++){
						out = out+ "\t" + dpl[i][j][k];
					}
					out = out+"\n";
				}
				out = out+sep+"\n";
			}
			return out;
		}else{
			return null;
		}
		
		
	}
	

}
