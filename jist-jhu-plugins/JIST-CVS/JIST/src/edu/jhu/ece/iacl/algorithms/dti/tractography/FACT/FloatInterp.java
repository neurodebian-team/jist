package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Jul 26, 2006
 * Time: 10:35:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class FloatInterp {
    int maxx, maxy, maxz;
    float image[][][];

    public void init(double data[],double dims[]) {
        maxx = (int)dims[0];
        maxy = (int)dims[1];
        maxz = (int)dims[2];
        image = new float[maxx][maxy][maxz];
        for(int i=0;i<maxx;i++)
            for(int j=0;j<maxy;j++)
                for(int k=0;k<maxz;k++)
                    image[i][j][k] = (float)data[i+j*maxx+k*maxx*maxy];
    }

    public void freeMem() {
        image = null;
        maxx = 0; maxy=0; maxz=0;
    }

    private float safeGet(int x, int y, int z) {
        if((x<1)||(x>maxx)||(y<1)||(y>maxy)||(z<1)||(z>maxz))
            return 0;
        else {
           // System.out.println("jist.plugins"+"\t"+"("+x+","+y+","+z+")="+image[x-1][y-1][z-1]);
            return image[x-1][y-1][z-1];
        }
    }

    public double[] interpolateTriliear(double x[], double y[], double z[]) {

        double retval[] = new double[x.length];
        double x1, y1, z1;
        int xf,xc,yf,yc,zf,zc;
        double u,v,w;
        for(int i=0;i<x.length;i++) {
            x1 = x[i]; y1=y[i];z1=z[i];

            xf =(int) Math.floor(x1);
            xc =(int) Math.ceil(x1);

            yf =(int)Math.floor(y1);
            yc =(int)Math.ceil(y1);

            zf =(int)Math.floor(z1);
            zc =(int)Math.ceil(z1);

            w = x1-xf;
            v = y1-yf;
            u = z1-zf;
            //System.out.println("jist.plugins"+"\t"+u+" "+v+" "+w);
            retval[i] = (
                    safeGet(xf,yf,zf)*(1.0-u)*(1.0-v)*(1.0-w) +
                    safeGet(xf,yf,zc)*u*(1.0-v)*(1.0-w) +
                    safeGet(xf,yc,zf)*(1.0-u)*v*(1.0-w) +
                    safeGet(xc,yf,zf)*(1.0-u)*(1.0-v)*w +
                    safeGet(xc,yf,zc)*u*(1.0-v)*w +
                    safeGet(xc,yc,zf)*(1.0-u)*v*w +
                    safeGet(xf,yc,zc)*u*v*(1.0-w) +
                    safeGet(xc,yc,zc)*u*v*w);
        }
        return retval;
    }



}
