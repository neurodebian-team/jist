package edu.jhu.ece.iacl.algorithms.dti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The ParV3Info class parses par version 4 files for information relevant to CATNAP.
 * 
 * 
 * @author John Bogovic
 *
 */
public class ParV4Info {
	
	String filename;
	String date;
	String parversion;
	String bvals;
	String foldover;
	String patposition;
	String patorient;
	String sliceOrientation;
	int slcor;
	
	double[] sliceAng;
	double[] offCentre;
	int numvolumes;
	int numvolumes2;
	int bvalCol;
	int SlcOrCol;
	
	public ParV4Info(String filename){
		this.filename=toPAR(filename);
		System.out.println("jist.plugins"+"\t"+"Found file: "+filename);
	}
	
	/**
	 * 
	 * @param filename - The path of the file under consideration
	 * @return A String representing the path of the corresponding Par file if the input
	 * string was a .Rec file.  The ouput equals the input param if the filename is a par file.
	 */
	public static String toPAR(String filename){
		String[] parts = filename.split(File.separatorChar+".");
		System.out.println("jist.plugins"+"\t"+parts.length);
		for(String s : parts){
			System.out.println("jist.plugins"+"\t"+s);
		}
		
		String out = filename;
		if(parts[parts.length-1].equals("REC") || parts[parts.length-1].equals("rec")){
			out = combineFirst(parts)+"PAR";
			File f = new File(out);
			if(f.exists()){
				return out;
			}else{
				out = combineFirst(parts)+"par";
				f = new File(out);
				if(f.exists()){
					return out;
				}else{
					out = combineFirst(parts)+"parv2";
					f = new File(out);
					if(f.exists()){
						return out;
					}else{
						out = null;
					}
				}
			}
		}
		return out;
	}
	private static String combineFirst(String[] parts){
		String out = "";
		for(int i=0; i<parts.length-1; i++){
			out=out+parts[i]+".";
		}
		return out;
	}
	
	//"Get" methods below...
	public String getDate(){
		if(date==null)
			return "1900.01.01 / 00:00:01";
		return date;
	}
	public String getParVersion(){
		return parversion;
	}
	public String getbVals(){
		return bvals;
	}
	public double[] getSliceAngulation(){
		return sliceAng;
	}
	public double[] getOffCentre(){
		return offCentre;
	}
	public int getNumVolumes(){
		return numvolumes;
	}
	public String getFoldover(){
		return foldover;
	}
	public String getPatientPosition(){
		return patposition;
	}
	public String getPatientOrientation(){
		return patorient;
	}
	public String getSliceOrientation(){
		return sliceOrientation;
	}
	/**
	 * The getInfo method parses the par file and sets the global variables.
	 */
	public void getInfo(){
		TreeSet<Integer> set = new TreeSet<Integer>(); 
		try{
			BufferedReader rdr = new BufferedReader(new FileReader(new File(filename)));
			bvals = "";
			String thisline = "";
			thisline = rdr.readLine();
			int num = 0;
			
			
			boolean hitGenInfo=false;
			boolean hitImageInformation = false;
			boolean ImageInfoDef = false;
			boolean readImageInfo = false;
			boolean end = false;
			boolean incrementbCol = true;
			boolean incrementSlcCol = true;
			bvalCol = 0;
			
			while(!thisline.contains("END OF DATA DESCRIPTION FILE")){
				thisline = rdr.readLine();
//				System.out.println("jist.plugins"+"\t"+thisline);
				//Extract Date
				if(thisline.contains("Examination date/time")){
					int ind = thisline.indexOf(":");
					date = thisline.substring(ind+1,thisline.indexOf("/",ind));
					date=date.trim();
					date=date.substring(0, 4)+date.substring(5, 7)+date.substring(8, 10);
				}
				
				if(!hitGenInfo){
					Pattern pt = Pattern.compile("V[1-9]");
					Matcher mat = pt.matcher(thisline);
					if(mat.find()){
//						mat.toMatchResult();
						parversion=thisline.substring(mat.start(),thisline.length());
					}
				}
				
				if(thisline.contains("Angulation midslice")){
					String info = thisline.substring(thisline.indexOf(":")+1);
					info=info.trim();
					sliceAng = new double[3];
					sliceAng[0] = Double.parseDouble(info.substring(0, info.indexOf(' ')));
					info = info.substring(info.indexOf(' ')+1, info.length()).trim();
					sliceAng[1] = Double.parseDouble(info.substring(0, info.indexOf(' ')));
					sliceAng[2] = Double.parseDouble(info.substring(info.indexOf(' ')+1, info.length()));

				}
				
				if(thisline.contains("Patient position")){
					String line = thisline.substring(thisline.indexOf(":")+1).trim();
					patposition = line.substring(0 , line.lastIndexOf(' ')).trim();
					patposition = patposition.replace(' ', '-');
					patorient = line.substring(line.lastIndexOf(' ')).trim();
				}
				if(thisline.contains("Preparation direction")){
					foldover = thisline.substring(thisline.indexOf(":")+1).trim();
				}
				
				if(thisline.contains("Off Centre midslice")){
					String info = thisline.substring(thisline.indexOf(":")+1);
					info=info.trim();
					offCentre = new double[3];
					offCentre[0] = Double.parseDouble(info.substring(0, info.indexOf(' ')));
					info = info.substring(info.indexOf(' ')+1, info.length()).trim();
					
					offCentre[1] = Double.parseDouble(info.substring(0, info.indexOf(' ')));
					offCentre[2] = Double.parseDouble(info.substring(info.indexOf(' ')+1, info.length()));
					
					
					
				}
				
				if(thisline.contains("GENERAL INFORMATION")){
					hitGenInfo=true;
					if(parversion==null){
						System.err.println(getClass().getCanonicalName()+"Can't read par version");
						return;
					}
				}
				
				
				if(thisline.contains("END OF DATA DESCRIPTION FILE")){
					end=true;
				}
				if(thisline.contains("IMAGE INFORMATION") && ImageInfoDef){
					hitImageInformation=true;
				}
				if(thisline.contains("IMAGE INFORMATION DEFINITION")){
					ImageInfoDef=true;
				}
				
//				System.out.println("jist.plugins"+"\t"+"readImageInfo " +readImageInfo);
//				System.out.println("jist.plugins"+"\t"+"End " + end);
				
				if(readImageInfo && !end){
					thisline=thisline.trim();
					int ind = thisline.indexOf(' ');
					if(ind >0){
						//counts number of volumes:
						String dyn = thisline.substring(ind+1).trim();
						dyn = dyn.substring(dyn.indexOf(' ')).trim();
						dyn = dyn.substring(0,dyn.indexOf(' ')+1).trim();
//						Integer.parseInt(dyn);
//						System.out.println("jist.plugins"+"\t"+Integer.parseInt(dyn));
						set.add(Integer.parseInt(dyn));
						
						//find the slice
						String slc = thisline.trim();
						slc = slc.substring(0, slc.indexOf(' ')).trim();
//						System.out.println("jist.plugins"+"\t"+slc);
						int slice = Integer.parseInt(slc);
						//tabulates b-values
						//Only find the b-value once per volume(check on the first slice)
						
						
//						if(slice==1){
//							numvolumes2++;
//							
//							int lastind = thisline.trim().lastIndexOf(' ');
//							int seclastind = thisline.substring(0, lastind-1).lastIndexOf(' ');
//							String bval = thisline.substring(seclastind, lastind).trim();
////							System.out.println("jist.plugins"+"\t"+bval);
//							
//							bvals = bvals+bval+"\n";
//							
//						}
//						System.out.println("jist.plugins"+"\t"+"Slice = " + slice);
						
						if(slice==1){
							System.out.println("jist.plugins"+"\t"+"Slice = 1");
							numvolumes2++;
							
							String diffcol = thisline.substring(thisline.indexOf(' ')).trim();
							for(int i=0; i<bvalCol-1; i++){
								diffcol = diffcol.substring(diffcol.indexOf(' ')).trim();
//								System.out.println("jist.plugins"+"\t"+diffcol);
							}
							diffcol = diffcol.substring(0,diffcol.indexOf(' '));
							System.out.println("jist.plugins"+"\t"+"Diffusion col:" + diffcol);
							bvals = bvals+diffcol+"\n";
							
							String orcol = thisline.substring(thisline.indexOf(' ')).trim();
							for(int i=0; i<SlcOrCol-1; i++){
								orcol = orcol.substring(orcol.indexOf(' ')).trim();
							
							}
							orcol = orcol.substring(0,orcol.indexOf(' '));
							System.out.println("jist.plugins"+"\t"+"Orientation col:" + orcol);
							int thisvolor = Integer.parseInt(orcol);
							if(slcor==0){
								slcor=thisvolor;
								System.out.println("jist.plugins"+"\t"+"Orientation ind set: " + slcor);
							}else{
								if(thisvolor!=slcor){
									System.out.println("jist.plugins"+"\t"+"WARNING: error in reading patient orientation");
								}
							}
						}
					}
				}
				
				
				if(thisline.contains("sl ec") && thisline.contains("dyn")  && hitImageInformation){
					readImageInfo=true;				
//					int column = 0;
//					String prevline = "";
//					String endline = thisline.substring(thisline.indexOf(' ')).trim();
//					while(endline.contains("diff")){
//						endline = endline.substring(endline.indexOf(' ')).trim();
////						System.out.println("jist.plugins"+"\t"+endline);
//						column++;
//					}
//					System.out.println("jist.plugins"+"\t"+column);
//					bvalCol=column;
				}
				
				//Determine bvalue column from Image Information definition
				if(ImageInfoDef && !hitImageInformation){
					if(thisline.contains("integer") || thisline.contains("float")){
						if(thisline.contains("diffusion_b_factor")){
							incrementbCol=false;
						}
						if(thisline.contains("slice orientation")){
							incrementSlcCol=false;
						}
						

						if(thisline.indexOf("integer")>0){
//							System.out.println("jist.plugins"+"\t"+thisline.substring(thisline.indexOf("integer")-1,thisline.indexOf("integer")));
							if(thisline.substring(thisline.indexOf("integer")-1,thisline.indexOf("integer")).equalsIgnoreCase("*")){
								System.out.println("jist.plugins"+"\t"+"To int: " + thisline.substring(thisline.indexOf("integer")-2, thisline.indexOf("integer")-1));
								if(incrementbCol){
									bvalCol=bvalCol+Integer.parseInt(thisline.substring(thisline.indexOf("integer")-2, thisline.indexOf("integer")-1));
//									System.out.println("jist.plugins"+"\t"+"Added more than 1!: " + bvalCol);
								}
								if(incrementSlcCol){
									SlcOrCol=SlcOrCol+Integer.parseInt(thisline.substring(thisline.indexOf("integer")-2, thisline.indexOf("integer")-1));
								}
							}else{
								if(incrementbCol){
									bvalCol++;
								}
								if(incrementSlcCol){
									SlcOrCol++;
								}
							}

						}else{
//							System.out.println("jist.plugins"+"\t"+thisline.substring(thisline.indexOf("float")-1,thisline.indexOf("float")));
							if(thisline.substring(thisline.indexOf("float")-1,thisline.indexOf("float")).equalsIgnoreCase("*")){
								if(incrementbCol){
									bvalCol=bvalCol+Integer.parseInt(thisline.substring(thisline.indexOf("float")-2, thisline.indexOf("float")-1));
								}
								if(incrementSlcCol){
									SlcOrCol=SlcOrCol+Integer.parseInt(thisline.substring(thisline.indexOf("float")-2, thisline.indexOf("float")-1));
								}
							}else{
								if(incrementbCol){
									bvalCol++;
								}
								if(incrementSlcCol){
									SlcOrCol++;
								}
							}
						}
					}

				}
				
//				System.out.println("jist.plugins"+"\t"+"Slice or col: " + SlcOrCol);
				
				num++;
			}
		
		}catch(IOException e){
			e.printStackTrace();
		}
		numvolumes = set.size();
		System.out.println("jist.plugins"+"\t"+"NumVolumes2: " + numvolumes2);
		if(numvolumes!=numvolumes2){
			System.err.println(getClass().getCanonicalName()+"WARNING: number of dyn's does not match number of volumes!");
			if(numvolumes2>numvolumes){
				numvolumes=numvolumes2;
			}
		}
		String[] orientations = {"Transverse","Sagittal","Coronal"};
		sliceOrientation = orientations[slcor-1];
	}
	

}
