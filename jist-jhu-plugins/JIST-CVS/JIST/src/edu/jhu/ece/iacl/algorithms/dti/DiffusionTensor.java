package edu.jhu.ece.iacl.algorithms.dti;

import imaging.SchemeV1;

import java.util.Arrays;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;

public class DiffusionTensor {
	private Matrix tensor; 
	private  double[] eigenvalues;
	private Matrix eigenvectors;
	private float []fa = null;
	private float []md = null;
	private boolean missing;
	public DiffusionTensor(float []linearTensor) {
		if(Float.isNaN(linearTensor[0]) || Float.isInfinite(linearTensor[0])) {
			missing = true;

		} else {
			missing = false;
			tensor = new Matrix(3,3);
			tensor.set(0,0,linearTensor[0]);
			tensor.set(0,1,linearTensor[1]);
			tensor.set(1,0,linearTensor[1]);
			tensor.set(0,2,linearTensor[2]);
			tensor.set(2,0,linearTensor[2]);
			tensor.set(1,1,linearTensor[3]);
			tensor.set(1,2,linearTensor[4]);
			tensor.set(2,1,linearTensor[4]);
			tensor.set(2,2,linearTensor[5]);
			EigenvalueDecomposition ed = new EigenvalueDecomposition(tensor);
			double []evals = ed.getRealEigenvalues();
			Matrix V = ed.getV();
			Arrays.sort(eigenvalues=evals.clone());		
			eigenvectors = new Matrix(3,3);
			boolean []matched = new boolean[3]; 
			for(int i=0;i<3;i++)
				matched[i]=false;
			for(int i=0;i<3;i++) {
				for(int j=0;j<3;j++) {
					if(!matched[j]) { //must handle duplicate eigenvalues
						if(evals[j]==eigenvalues[i]) {
							matched[j]=true;
							for(int k=0;k<3;k++) {
								eigenvectors.set(i, k, V.get(k,j));
							}
						}
					}				
				}
			}
		}
	}

	public DiffusionTensor(double []linearTensor) {
		if(Double.isNaN(linearTensor[0]) || Double.isInfinite(linearTensor[0])) {
			missing = true;

		} else {
			missing = false;
			tensor = new Matrix(3,3);
			tensor.set(0,0,linearTensor[0]);
			tensor.set(0,1,linearTensor[1]);
			tensor.set(1,0,linearTensor[1]);
			tensor.set(0,2,linearTensor[2]);
			tensor.set(2,0,linearTensor[2]);
			tensor.set(1,1,linearTensor[3]);
			tensor.set(1,2,linearTensor[4]);
			tensor.set(2,1,linearTensor[4]);
			tensor.set(2,2,linearTensor[5]);
			EigenvalueDecomposition ed = new EigenvalueDecomposition(tensor);
			double []evals = ed.getRealEigenvalues();
			Matrix V = ed.getV();
			Arrays.sort(eigenvalues=evals.clone());		
			eigenvectors = new Matrix(3,3);
			boolean []matched = new boolean[3]; 
			for(int i=0;i<3;i++)
				matched[i]=false;
			for(int i=0;i<3;i++) {
				for(int j=0;j<3;j++) {
					if(!matched[j]) { //must handle duplicate eigenvalues
						if(evals[j]==eigenvalues[i]) {
							matched[j]=true;
							for(int k=0;k<3;k++) {
								eigenvectors.set(i, k, V.get(k,j));
							}
						}
					}				
				}
			}
		}
	}
	
	public float MD() {
		if(missing)
			return Float.NaN;
		if(md==null) {
			md = new float[1];
			md[0] = (float)(eigenvalues[0]+eigenvalues[1]+eigenvalues[2])/3;
		};
		return md[0];
	}

	private static double square(double x) { return x*x;}

	public float FA() {
		if(missing)
			return Float.NaN;
		if(fa==null) {
			double md = MD();
			double num = square(eigenvalues[0]-md)+square(eigenvalues[1]-md)+square(eigenvalues[2]-md);
			double denom = square(eigenvalues[0])+square(eigenvalues[1])+square(eigenvalues[2]);
			fa = new float[1];
			fa[0]=(float) Math.sqrt(3./2.*num/denom);
		} 
		return fa[0];
	}
	
	public float[] pev() { return vec1();};

	public float[] vec1() {
		if(missing)
			return new float[]{Float.NaN,Float.NaN,Float.NaN};
		float []v=new float[3];
		for(int i=0;i<3;i++)
			v[i]=(float)eigenvectors.get(2,i);
		return v;
	}

	public float[] vec2() {
		if(missing)
			return new float[]{Float.NaN,Float.NaN,Float.NaN};
		float []v=new float[3];
		for(int i=0;i<3;i++)
			v[i]=(float)eigenvectors.get(1,i);
		return v;
	}

	public float[] vec3() {
		if(missing)
			return new float[]{Float.NaN,Float.NaN,Float.NaN};
		float []v=new float[3];
		for(int i=0;i<3;i++)
			v[i]=(float)eigenvectors.get(0,i);
		return v;
	}

	public byte[] cmap() {
		if(missing)
			return new byte[]{0,0,0};
		float fa = FA();
		float []vec = vec1();
		byte []dec = new byte[3];
		for(int i=0;i<3;i++)
			dec[i]=(byte)(Math.max(0,Math.min(255.f, fa*vec[i])));
		return dec;		
	}

	public float RA() {
		if(missing) 
			return Float.NaN;
		double num = (eigenvalues[0]*eigenvalues[1]+
				eigenvalues[1]*eigenvalues[2]+
				eigenvalues[0]*eigenvalues[2])/3.;
		double denom = square(eigenvalues[0]+
				eigenvalues[1]+
				eigenvalues[2]/3.);
		return (float)Math.sqrt(2.*(1.-num/denom));
	}

	public float VR() {
		if(missing)
			return Float.NaN;
		double num = eigenvalues[0]*eigenvalues[1]*eigenvalues[2];
		double denom = Math.pow(MD(),3);
		return (float)(num/denom);
	}

	public float []evals() {
		if(missing)
			return new float[]{Float.NaN,Float.NaN,Float.NaN};
		float []e = new float[3];
		for(int i=0;i<3;i++)
			e[i]=(float)eigenvalues[2-i];
		return e;
	}

	static public double[] projectTensor(float ref, float[] outTensors, SchemeV1 dtiScheme) {
		int N = dtiScheme.numMeasurements();
		double []proj = new double[N];
		double Dxx = outTensors[0];
		double Dxy = outTensors[1];
		double Dxz = outTensors[2];
		double Dyy = outTensors[3];
		double Dyz = outTensors[4];
		double Dzz = outTensors[5];
		        
		for(int i=0;i<N;i++) {
			
			double []q = dtiScheme.getQ(i);
			double Gx = q[0];
			double Gy = q[1];
			double Gz = q[2];
			double DELTA = dtiScheme.getDELTA(i);
		
			proj[i] = ref * Math.exp(-1e-6 * DELTA *
					(Gx*Gx*Dxx + 
							2*Gx*Gy*Dxy+
							2*Gx*Gz*Dxz+
							Gy*Gy*Dyy+
							2*Gy*Gz*Dyz+
							Gz*Gz*Dzz));
		}
		
		return proj;
	}

}
