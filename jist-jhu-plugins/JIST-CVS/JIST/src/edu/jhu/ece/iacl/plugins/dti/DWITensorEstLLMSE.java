package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLLMSE;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.structures.ModelImage;


public class DWITensorEstLLMSE extends ProcessingAlgorithm { 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolumeCollection DWdata4D; 		// SLAB-enabled Imaging Data
	private ParamVolumeCollection Mask3D;			// SLAB-enabled Binary mask to indicate computation volume
	private ParamOption estOptions;		// Option to attempt to estimate with missing data
	private ParamFile bvaluesTable;		// .b file with a list of b-values
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolumeCollection tensorVolume;	// SLAB-enabled A 4D volume with one tensor estimated per pixel

	private static final String rcsid =
		"$Id: DWITensorEstLLMSE.java,v 1.8 2010/02/01 20:45:47 bennett Exp $";
	private static final String cvsversion = "$Revision: 1.8 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Log-linear minimum mean squared error tensor estimation.";
	private static final String longDescription = 	"Inputs:\n" +
													"\t-DWI and Reference Image(s) Data (4D file collection)\n" +
													"\t-Table of diffusion weighting directions (file)\n" +
													"\t-Table of b-values (file)\n" +
													"\t-Mask Volume (3D file, optional)\n" +
													"Outputs:\n" +
													"\t-Tensor Estimate (4D file: rows, collums, slices, (xx,xy,xz,yy,yz,zz))\n" ;


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Tensor Estimation LLMSE");	
		inputParams.setName("Tensor_Estimation_LLMSE");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new Citation("Landman BA, Farrell JA, Jones CK, Smith SA, Prince JL, Mori S. Effects of diffusion weighting schemes on the reproducibility of DTI-derived fractional anisotropy, mean diffusivity, and principal eigenvector measurements at 1.5T. Neuroimage 2007;36(4):1123-1138."));
		info.add(new Citation("Basser, PJ, Jones, DK. \"Diffusion-tensor MRI: Theory, experimental design and data analysis - a technical review.\" NMR Biomed 2002; 15(7-8):456-67"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription +"\n" +longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);

		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamVolumeCollection("DWI and Reference Image(s) Data (4D)"));
		DWdata4D.setLoadAndSaveOnValidate(false);
		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(Mask3D=new ParamVolumeCollection("Mask Volume to Determine Region of Tensor Estimation (3D)"));
		Mask3D.setLoadAndSaveOnValidate(false);
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.	
		inputParams.add(estOptions=new ParamOption("Attempt to estimate tensors for voxels with missing data?",new String[]{"Yes","No"}));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		tensorVolume = new ParamVolumeCollection("Tensor Estimate");
		tensorVolume .setLoadAndSaveOnValidate(false);
		tensorVolume.setName("Tensor (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensorVolume);			
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm thisParent) {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			JistLogger.logError(JistLogger.INFO, "DWITensorEstLLMSE: Start");
//			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: Start");
//			MedicUtil.setQuiet(false);

			/****************************************************
			 * Step 2. Loop over input slabs
			 ****************************************************/
			List<ParamVolume> dwList = DWdata4D.getParamVolumeList();
			List<ParamVolume> maskList = Mask3D.getParamVolumeList();

			
			this.addTotalUnits(dwList.size());
			for(int jSlab=0;jSlab<dwList.size();jSlab++) {
				this.setLabel("Load");
				/****************************************************
				 * Step 2. Parse the input data 
				 ****************************************************/
				ImageData dwd=dwList.get(jSlab).getImageData();
ImageHeader hdr = dwd.getHeader();
				String sourceName = dwd.getName();
				ImageDataFloat DWFloat=new ImageDataFloat(dwd);
				dwList.get(jSlab).dispose();
				dwd.dispose();
				dwd=null;

				/* Read the brain mask */
				ImageData maskVol=null;
				if(maskList!=null)
					if(maskList.size()>jSlab)
						if(maskList.get(jSlab)!=null)
							maskVol=maskList.get(jSlab).getImageData();


				byte [][][]mask=null;
				if(maskVol!=null) {
					ImageDataUByte maskByte = new ImageDataUByte (maskVol);
					mask = maskByte.toArray3d();
					maskByte.dispose();
					maskByte=null;
					maskVol.dispose();
					maskVol=null;
					maskList.get(jSlab).dispose();
				}

				/* Read the b values */
				float [][]bs=null;		
				TextFileReader text = new TextFileReader(bvaluesTable.getValue());
				try {
					bs = text.parseFloatFile();
				} catch (IOException e) 
				{
					JistLogger.logError(JistLogger.WARNING, "LLMSE: Unable to parse b-file.");
					throw new RuntimeException("LLMSE: Unable to parse b-file");
				}

				/* Read the gradient table  */
				float [][]grads=null;
				text = new TextFileReader(gradsTable.getValue());
				try {
					grads  = text.parseFloatFile();
				} catch (IOException e) { 
					JistLogger.logError(JistLogger.WARNING, "LLMSE: Unable to parse grad-file.");
					throw new RuntimeException("LLMSE: Unable to parse grad-file");
				}

				/****************************************************
				 * Step 3. Perform limited error checking 
				 ****************************************************/
				// If there are 4 columns in the gradient table, remove the 1st column (indecies)
				if(grads[0].length==4) {
					float [][]g2 = new float[grads.length][3];
					for(int i=0;i<grads.length;i++) 
						for(int j=0;j<3;j++)
							g2[i][j]=grads[i][j+1];
					grads=g2;
				}

				if(grads[0].length!=3){
					JistLogger.logError(JistLogger.WARNING, "LLMSE: Invalid gradient table. Must have 3 or 4 columns.");
					throw new RuntimeException("LLMSE: Invalid gradient table. Must have 3 or 4 columns.");
				}
				if(bs[0].length!=1){
					JistLogger.logError(JistLogger.WARNING, "LLMSE: Invalid b-value table. Must have 1 column.");
					throw new RuntimeException("LLMSE: Invalid b-value table. Must have 1 column.");
				}
				float []bval = new float[bs.length];
				for(int i=0;i<bval.length;i++)
					bval[i]=bs[i][0];

				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program 
				 * 		   has NO knowledge of the MIPAV data structure and 
				 * 		   uses NO MIPAV specific components. This dramatic 
				 * 		   separation is a bit inefficient, but it dramatically 
				 * 		   lower the barriers to code re-use in other applications.  		  
				 ****************************************************/
				this.setLabel("Estimate");
				float[][][][] tensors = EstimateTensorLLMSE.estimate(DWFloat.toArray4d(),bval,grads,mask,estOptions.getValue().compareToIgnoreCase("Yes")==0);
				DWFloat.dispose();
				DWFloat=null;

				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.  
				 ****************************************************/
				this.setLabel("Save");
//				int []ext=dwd.getModelImage().getExtents();
				//DWFloat=null;DWdata4D.dispose();

				
				
				ImageData  out= (new ImageDataFloat(tensors));
				out.setHeader(hdr);
				
				tensors = null; //no longer needed
				out.setName(sourceName+"_TensorLLMSE");		
				//			FileUtilInfo info = FileUtil.getFileInfo(dwd.getModelImage());
				tensorVolume.add(out);
				tensorVolume.writeAndFreeNow(thisParent);
				
				out.dispose();
				out=null;			
				//tensorVolume.setName(DWdata4D.getName()+"_TensorLLMSE"+".xml");
				this.incrementCompletedUnits();
			}
//			tensorVolume.setValue(outVols);

			/****************************************************
			 * Step 6. Let the user know that your code is finished.  
			 ****************************************************/
			JistLogger.logError(JistLogger.INFO, "DWITensorEstLLMSE: FINISHED");
//			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: FINISHED");
		}
	}
}
