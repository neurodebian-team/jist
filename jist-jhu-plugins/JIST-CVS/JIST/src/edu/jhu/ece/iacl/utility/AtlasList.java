package edu.jhu.ece.iacl.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class AtlasList {

	static final public String nextLine(BufferedReader br) throws IOException {
		String line = br.readLine();
		while(line.startsWith("#")) // eat comments 
			line = br.readLine();
		return line;
	}

	static final public List<File[]> loadFileSetAtlas(String filename) {
		return loadFileSetAtlas(new File(filename));
	}
	
	static final public List<File[]> loadFileSetAtlas(File f) {
		String[][] atlasFilenames = null;
		int Natlas= 0;
		try {			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();		
			// Exact corresponding template
			if (!line.equals("File Set Atlas: 1.0")) {
				System.err.println("jist.plugins"+"not a proper atlas file: "+f);
				br.close();
				fr.close();
				return null;
			}

			line = nextLine(br).toLowerCase();
			if (line.startsWith("lines: "))
				Natlas = Integer.valueOf(line.substring(line.indexOf(":") + 2))
				.intValue();
			System.out.println("jist.plugins"+"\t"+"Searching for "+Natlas+" atlases");
			atlasFilenames = new String[Natlas][];
			for (int n = 0; n < Natlas; n++) {
				line = nextLine(br);

				String []files = line.trim().split("\\s+");
				atlasFilenames[n] = new String[files.length];
				for(int i=0;i<files.length;i++) {
					atlasFilenames[n][i] = f.getParent() + File.separator
					+ files[i];
					System.out.print(atlasFilenames[n][i]+"\t");
				}
				System.out.print("\n");
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			System.err.println("jist.plugins"+e.getMessage());
			return null;
		} catch (IOException e) {
			System.err.println("jist.plugins"+e.getMessage());
			return null;
		} catch (OutOfMemoryError e) {
			System.err.println("jist.plugins"+e.getMessage());
			return null;
		} catch (Exception e) {
			System.err.println("jist.plugins"+e.getMessage());
			return null;
		}

		List<File[]> atlas = new LinkedList<File[]>();


		for (int n = 0; n < Natlas; n++) {
			File []list = new File[atlasFilenames[n].length];
			for(int i=0;i<list.length;i++)
				list[i] = new File(atlasFilenames[n][i]);
			atlas.add(list);			
		}
		return atlas;
	}

}
