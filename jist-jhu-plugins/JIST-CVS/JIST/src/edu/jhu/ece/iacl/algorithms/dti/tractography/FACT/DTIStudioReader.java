package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;


import java.io.*;
import java.util.Vector;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

import edu.jhu.ece.iacl.jist.io.LEFileReader;
import edu.jhu.ece.iacl.jist.structures.fiber.DTIStudioHDR;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.geom.IntersectResult;
import edu.jhu.ece.iacl.jist.structures.geom.Polygon;



/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Dec 2, 2005
 * Time: 1:00:14 PM
 * To change this template use Options | File Templates.
 */
public class DTIStudioReader {
	
	boolean verbose = false;
	int numread = 0;
	
    public static void main(String [] args)
    {
        /* Debug LE file reader
        try {
        LEFileReader r = new LEFileReader("c:\\test.le");
        for(float j=1;j<1e11;j+=100000) {
        float f = r.readFloat();
        if(j!=f) {
        System.out.println("jist.plugins"+"\t"+j+" "+f);
        }
        }

        } catch (Exception e) {

        }
        */
        DTIStudioReader read = new DTIStudioReader();
        try {
        	
    		String sub = "jb";
    		String[] nums = {"jb_7","jb_8","jb_9","jb_12"};
    		
//        	String dat = "jb_12";
//        	String dir = "/home/john/Research/20070727/corrected_dti/processed/"+dat+"/";
    		String dir = "/home/john/Research/20070727/corrected_dti/processed/jb_7/ManualTracts/";
    		String autof=dir+"jb_7_Fminor.dat";
    		
//        	read.read("/home/john/Research/dti_cruise/FiberCompare/ToyExample2.dat");
//        	read.read("/home/john/Research/dti_cruise/FiberCompare/at1000/at1000_IACL.dat");
//        	read.read("/home/john/Research/dti_cruise/FiberCompare/at1000/at1000_dtis202.dat");
//        	read.read("/home/john/Research/dti_cruise/FiberCompare/at1000/at1000_dtis24.dat");
//        	read.read("/home/john/Research/20070727/corrected_dti/processed/pa_8_1/pa_8_1_ALLFIBERS.dat");
//        	read.getallFiberLengths(dir+dat+"_ALLFIBERS.dat");
    		
        	read.getallFiberLengths(autof);
        	
        	
        } catch (IOException  e) {
            System.out.println("jist.plugins"+"\t"+e);
            e.printStackTrace();
        }

        double faces[][] = new double[1][3];
        faces[0][0]=0;  faces[0][1]=1;  faces[0][2]=2;

        double pts[][] = new double[3][3];
        // X = []; Y = []; Z = [];
        /*// xy-test
        pts[0][0] = 121; pts[0][1] = 139; pts[0][2] = 18;
        pts[1][0] = 123; pts[1][1] = 148; pts[1][2] = 18;
        pts[2][0] = 113; pts[2][1] = 149; pts[2][2] = 18;
        */
        /*// yz-test
        pts[0][0] = 128; pts[0][1] = 155; pts[0][2] = 65-25;
        pts[1][0] = 128; pts[1][1] = 149; pts[1][2] = 65-31;
        pts[2][0] = 128; pts[2][1] = 164; pts[2][2] = 65-32;
        */
        /*// xz-test
        pts[0][0] = 103; pts[0][1] = 155; pts[0][2] = 65-47;
        pts[1][0] = 113; pts[1][1] = 155; pts[1][2] = 65-44;
        pts[2][0] = 113; pts[2][1] = 155; pts[2][2] = 65-51;
        */
        //ROI TEST
        pts[0][0] = 143; pts[0][1] = 152; pts[0][2] = 48;
        pts[1][0] = 129; pts[1][1] = 105; pts[1][2] = 48;
        pts[2][0] = 117; pts[2][1] = 162; pts[2][2] = 48;
          
         
        long time = System.currentTimeMillis();
//        double []intersect = read.findIntersectPolygon(pts,faces,1);
//        if (intersect.length==0){
//        	System.out.println("jist.plugins"+"\t"+"NO FIBERS INTERSECT ROI");
//        }else{
//        	double [][]res = read.getFiber(intersect[0]);
//        	for(int j=0;j<res.length;j++)
//                System.out.println("jist.plugins"+"\t"+res[j][0]+","+res[j][1]+","+res[j][2]+" = "+res[j][3]);
//        }
        long out = System.currentTimeMillis();
//        System.out.println("jist.plugins"+"\t"+"Fibers Selected: "+intersect.length);
        System.out.println("jist.plugins"+"\t"+(out-time)/1000.f+ " for one triangle");
    }

    public double[] findIntersectPolygon(double[][] pts, double[][] faces, double marker) {
        Polygon p = new Polygon(pts,faces);
        Vector v = new Vector();
        for (int j=0;j<this.fibers.length;j++) {
            //fibers[j].intersects(p);
            IntersectResult r = fibers[j].reportiIntersect(p);
            if(r!=null) {
                v.add(new Integer(j));
                // need to track front/back with ab
                fibers[j].setMarkerPoint(marker,(int)r.fractionalDistance);
                fibers[j].setMarkerPoint(marker,(int)r.fractionalDistance-1);
                //System.out.println("jist.plugins"+"\t"+"hit");
            }
        }
        double res[] = new double[v.size()];
        Iterator it = v.iterator();
        int i=0;
        while(it.hasNext()) {
            res[i] = ((Integer)it.next()).doubleValue();
            i++;
        }
        return res;
    }

    public double[] findIntersectPolygonDTIStudioCoordinates(double[][] pts, double[][] faces, double marker) {
        // Switch from left-handed to right handed coordinate systems
        for(int i=0;i<pts.length;i++) {
                pts[i][2] = this.hdr.ImageSize[2]-pts[i][2];
            }
        return findIntersectPolygon(pts,faces,marker);
    }

    public double[][] getFiber(double index) {
        int idx = (int) index;
        if(idx<0 || idx>=fibers.length)
            return null;
        double [][]result = new double[fibers[idx].getChain().length][4];
        for(int j=0;j<fibers[idx].getChain().length;j++) {
            result[j][0]=fibers[idx].getChain()[j].x;
            result[j][1]=fibers[idx].getChain()[j].y;
            result[j][2]=fibers[idx].getChain()[j].z;
            result[j][3]=fibers[idx].getMarker(j);
        }
        return result;
    }

    public double[] getFiberColor(double index) {
            int idx = (int) index;
            if(idx<0 || idx>=fibers.length)
                return null;
            double []result = new double[3];
        result[0]=fibers[idx].getColor().r&0xff;
        result[1]=fibers[idx].getColor().g&0xff;
        result[2]=fibers[idx].getColor().b&0xff;
        return result;
        }

    public double [][][]computeConnectivityContrast() {
        double [][][]result = new double[hdr.ImageSize[0]][hdr.ImageSize[1]][hdr.ImageSize[2]];
        for(int j=0;j<fibers.length;j++) {
            for(int k=0;k<fibers[j].getChain().length;k++) {
                try{
                result[Math.round(fibers[j].getChain()[k].x)][Math.round(fibers[j].getChain()[k].y)][Math.round(fibers[j].getChain()[k].z)]++;
                } catch(Exception e) {
                    // ignore possible out of bounds exceptions.
                }
            }
        }
        return result;
    }
    
    public DTIStudioHDR getHDR(){ return hdr; }
    
    public Fiber[] getFibers(){ return fibers; }
    
    public int getHDRFiberNR(){ return hdr.FiberNR; }
    
    public XYZ[] getFiberXYZchain(int n){ return fibers[n].getChain(); }

    DTIStudioHDR hdr;
    Fiber fibers[];
    public double read(String filename) throws IOException {
        //RandomAccessFile fp = new RandomAccessFile(filename,"r");
        LEFileReader fp = new LEFileReader(filename);
        hdr = new DTIStudioHDR();
        hdr.read(fp);
//        System.out.println("jist.plugins"+"\t"+"Number of Fibers: " + hdr.FiberNR);
//        System.out.println("jist.plugins"+"\t"+"Image Size is: " + hdr.ImageSize[0]+" "+hdr.ImageSize[1]+" "+hdr.ImageSize[2]);
  
//        int[] fiblens = new int[hdr.FiberNR];
        
        fibers = new Fiber[hdr.FiberNR];
        long time = System.currentTimeMillis();
        for(int j=0;j<hdr.FiberNR;j++) {
//        for(int j=0;j<500000;j++) {
        	
//        	 System.out.println("jist.plugins"+"\t"+"Reading fiber number " + j);
            Fiber f = new Fiber();
            if(f.getLength()<0){
            	break;
            }
            numread=j;
            f.read(fp,hdr.versionNum);
            fibers[j]=f;
            
//            fiblens[j]=f.length;
            
            /*if(0==(j%10000)) {
            System.out.println("jist.plugins"+"\t"+j+" "+fp.offset);
            }    */
        }
        long out = System.currentTimeMillis();

//        System.out.println("jist.plugins"+"\t"+"read: "+ (out-time)/1000.f+ " sec for "+hdr.FiberNR+" fibers");
        
//        RawWriter.writeVectorInt(fiblens,"/home/john/Research/20070727/corrected_dti/processed/jb_12/FiberLenghs.dat");
        
        return 0f;
    }
    
    public int[] getallFiberLengths(String filein) throws IOException {
        //RandomAccessFile fp = new RandomAccessFile(filename,"r");
        LEFileReader fp = new LEFileReader(filein);
        hdr = new DTIStudioHDR();
        hdr.read(fp);
        System.out.println("jist.plugins"+"\t"+"Number of Fibers: " + hdr.FiberNR);
        System.out.println("jist.plugins"+"\t"+"Image Size is: " + hdr.ImageSize[0]+" "+hdr.ImageSize[1]+" "+hdr.ImageSize[2]);
  
        int[] fiblens = new int[hdr.FiberNR];
        
        for(int j=0;j<hdr.FiberNR;j++) {
        	Fiber f = new Fiber();
            f.read(fp,hdr.versionNum);
            fiblens[j]=f.getLength();;
        }
        return fiblens;
    }

    public double[][][] readCC(String filename) throws IOException {
        //RandomAccessFile fp = new RandomAccessFile(filename,"r");
        LEFileReader fp = new LEFileReader(filename);
        hdr = new DTIStudioHDR();
        hdr.read(fp);

        double [][][]result = new double[hdr.ImageSize[0]][hdr.ImageSize[1]][hdr.ImageSize[2]];

        long time = System.currentTimeMillis();
        for(int j=0;j<hdr.FiberNR;j++) {
            Fiber f = new Fiber();
            f.read(fp,hdr.versionNum);
            for(int k=0;k<f.getChain().length;k++) {
                try{
                result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)]++;
                } catch(Exception e) {
                    // ignore possible out of bounds exceptions.
                }
            }
        }
        long out = System.currentTimeMillis();

        System.out.println("jist.plugins"+"\t"+"readCC: "+(out-time)/1000.f+ " sec for "+hdr.FiberNR+" fibers");
        return result;
    }

        public double[][][][] readColorVolume(String filename) throws IOException {
        //RandomAccessFile fp = new RandomAccessFile(filename,"r");
        LEFileReader fp = new LEFileReader(filename);
        hdr = new DTIStudioHDR();
        hdr.read(fp);

        double [][][][]result = new double[hdr.ImageSize[0]][hdr.ImageSize[1]][hdr.ImageSize[2]][3];

        long time = System.currentTimeMillis();
        for(int j=0;j<hdr.FiberNR;j++) {
            Fiber f = new Fiber();
            f.read(fp,hdr.versionNum);
            for(int k=0;k<f.getChain().length;k++) {
                try{
                result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][0]=
                        (f.getColor().r & 0xFF);
                result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][1]=
                        (f.getColor().g & 0xFF);
                result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][2]=
                        (f.getColor().b & 0xFF);
                } catch(Exception e) {
                    // ignroe possible out of bounds exceptions.
                }
            }
        }
        long out = System.currentTimeMillis();

        System.out.println("jist.plugins"+"\t"+"readColorVolume: " + (out-time)/1000.f+ " sec for "+hdr.FiberNR+" fibers");
        return result;
    }

    public double[][][][] readColorVolumeSoft(String filename) throws IOException {
    //RandomAccessFile fp = new RandomAccessFile(filename,"r");
    LEFileReader fp = new LEFileReader(filename);
    hdr = new DTIStudioHDR();
    hdr.read(fp);

    double [][][][]result = new double[hdr.ImageSize[0]][hdr.ImageSize[1]][hdr.ImageSize[2]][4];

    long time = System.currentTimeMillis();
        System.out.println("jist.plugins"+"\t"+hdr.FiberNR);
    for(int j=0;j<hdr.FiberNR;j++) {
        Fiber f = new Fiber();
        f.read(fp,hdr.versionNum);
        for(int k=0;k<f.getChain().length;k++) {

            try{
            result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][0]+=
                    (f.getColor().r & 0xFF);;
            result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][1]+=
                    (f.getColor().g & 0xFF);
            result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][2]+=
                    (f.getColor().b & 0xFF);;
            result[Math.round(f.getChain()[k].x)][Math.round(f.getChain()[k].y)][Math.round(f.getChain()[k].z)][3]+=
                    1;
            } catch(Exception e) {
                // ignroe possible out of bounds exceptions.
              //  System.out.println("jist.plugins"+"\t"+"out of bounds");
            }
        }
    }
    long out = System.currentTimeMillis();

    System.out.println("jist.plugins"+"\t"+"readColorVolumeSoft: " +(out-time)/1000.f+ " sec for "+hdr.FiberNR+" fibers");
    return result;
}


}
