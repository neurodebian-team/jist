package edu.jhu.ece.iacl.plugins.hardi;

import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.algorithms.hardi.CFARIBasisSet;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIEstimator;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import gov.nih.mipav.model.structures.ModelImage;


public class CFARIEstimation extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolume DWdata4D; 		// Imaging Data
	private ParamVolume Mask3D;			// Binary mask to indicate computation volume
	private ParamFile SchemeFile;
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamBoolean optionPD;
	private ParamInteger optionNumBasis;
	private ParamVolume basisIndecies;
	private ParamVolume basisMixture;
	private ParamFloat optionLambda;

	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Original CFARI estimation of tensors.";
	private static final String longDescription = "";

		
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("CFARI Estimation");
		inputParams.setName("CFARI_Estimation");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.add(new Citation("B. A. Landman, J. Bogovic, and J. L. Prince. \"Compressed Sensing of Multiple Intra-Voxel Orientations with Traditional DTI\", In Proceedings of the Workshop on Computational Diffusion MRI at the 11th International Conference on Medical Image Computing and Computer Assisted Intervention, New York, NY, September 2008."));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamVolume("DWI and Reference Image(s) Data (4D)",null,-1,-1,-1,-1));
		inputParams.add(SchemeFile=new ParamFile("CFARI Scheme",new FileExtensionFilter(new String[]{"CFARI"})));
		inputParams.add(Mask3D=new ParamVolume("Mask Volume to Determine Region of Tensor Estimation (3D)",null,-1,-1,-1,1));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.
		inputParams.add(optionPD=new ParamBoolean("Constrain to Positive Mixture?",true));
		inputParams.add(optionLambda=new ParamFloat("Select Lambda",1));
		inputParams.add(optionNumBasis=new ParamInteger("Number of Basis Functions to Export",1,Integer.MAX_VALUE,5));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		basisIndecies = new ParamVolume("Basis Function Indices Estimate",null,-1,-1,-1,-1);
		basisIndecies.setName("Basis Indices");
		outputParams.add(basisIndecies);
		basisMixture = new ParamVolume("Basis Mixture Fraction",null,-1,-1,-1,-1);
		basisMixture .setName("Mixture Fraction");
		outputParams.add(basisMixture );
	}


	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to
		 * 			know when the bug happened.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"CFARIEstimation: Start");

		/****************************************************
		 * Step 2. Parse the input data
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Load data.");System.out.flush();
		ImageData dwd=DWdata4D.getImageData();
		dwd.getHeader().setSliceThickness(0);		//used to suppress printing warning message
		ImageDataFloat DWFloat=new ImageDataFloat(dwd);

		ImageData maskVol=Mask3D.getImageData();
		byte [][][]mask=new byte[dwd.getRows()][dwd.getCols()][dwd.getSlices()];
		if(maskVol!=null) {
			maskVol.getHeader().setSliceThickness(0);		//used to suppress printing warning message
			for (int i=0; i<dwd.getRows(); i++) {
				for (int j=0; j<dwd.getCols(); j++) {
					for (int k=0; k<dwd.getSlices(); k++) {
						mask[i][j][k]=maskVol.get(i,j,k).byteValue();
					}
				}
			}
		}
		else {
			for (int i=0; i<dwd.getRows(); i++) {
				for (int j=0; j<dwd.getCols(); j++) {
					for (int k=0; k<dwd.getSlices(); k++) {
						mask[i][j][k]=1;
					}
				}
			}
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
		CFARIBasisSet basisSet = null;

		XStream xstream = new XStream();		
		try {
			ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
			basisSet=(CFARIBasisSet)in.readObject();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}


		/****************************************************
		 * Step 3. Perform limited error checking
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Error checking."); System.out.flush();
		int nBasisReturn = optionNumBasis.getValue().intValue();
		if(nBasisReturn<0 || nBasisReturn>basisSet.getNumberOfBasisFunctions()) {
			throw new RuntimeException("Invalid number of return basis functions requested");
		}

		boolean requirePos = optionPD.getValue();

		float lambda= optionLambda.getFloat();
		/****************************************************
		 * Step 4. Run the core algorithm. Note that this program
		 * 		   has NO knowledge of the MIPAV data structure and
		 * 		   uses NO MIPAV specific components. This dramatic
		 * 		   separation is a bit inefficient, but it dramatically
		 * 		   lower the barriers to code re-use in other applications.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Allocate memory."); System.out.flush();
		float [][][][]data=DWFloat.toArray4d();
		int rows = data.length;
		int cols= data[0].length;
		int slices= data[0][0].length;
		float [][][][]basisIndecies = new float[rows][cols][slices][nBasisReturn];
		float [][][][]basisMixtures= new float[rows][cols][slices][nBasisReturn];

		System.out.println(getClass().getCanonicalName()+"\t"+"Run CFARI estimate."); System.out.flush();
		CFARIEstimator.estimateCFARI(data, mask, basisSet, basisIndecies, basisMixtures,
				requirePos,lambda);

		/****************************************************
		 * Step 5. Retrieve the image data and put it into a new
		 * 			data structure. Be sure to update the file information
		 * 			so that the resulting image has the correct
		 * 		 	field of view, resolution, etc.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
		int []ext=DWFloat.getModelImageCopy().getExtents();
//		ModelImage img=null;
		ImageDataFloat out=new ImageDataFloat(basisIndecies);
		out.setHeader(DWdata4D.getImageData().getHeader());		
		out.setName(DWdata4D.getImageData().getName()+"_basisIndecies");
		this.basisIndecies.setValue(out);

		ImageDataFloat out2=new ImageDataFloat(basisMixtures);
		out.setHeader(DWdata4D.getImageData().getHeader());		
		out.setName(DWdata4D.getImageData().getName()+"_basisMixtures");		
		this.basisMixture.setValue(out2);


		/****************************************************
		 * Step 6. Let the user know that your code is finished.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"CFARIEstimation: FINISHED");
	}
}
