package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class RawWriter {
	
	public static void writeImgByteB(int[][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

			for(int k=0; k<image[0][0].length;k++){
//				for(int k=1; k<=image[0][0].length;k++){
				for(int j=1; j<=image[0].length;j++){
//					for(int j=0; j<image[0].length;j++){
					for(int i=0; i<image.length;i++){
//						for(int i=1; i<=image.length;i++){
						out.writeByte(image[i][image[0].length-j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeImgByteF(int[][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

			for(int k=0; k<image[0][0].length;k++){
//				for(int k=1; k<=image[0][0].length;k++){
				for(int j=0; j<image[0].length;j++){
//					for(int j=0; j<image[0].length;j++){
					for(int i=0; i<image.length;i++){
//						for(int i=1; i<=image.length;i++){
						out.writeByte(image[i][j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeImgInt(int[][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

			for(int k=0; k<image[0][0].length;k++){
//				for(int k=1; k<=image[0][0].length;k++){
				for(int j=1; j<=image[0].length;j++){
//					for(int j=0; j<image[0].length;j++){
					for(int i=0; i<image.length;i++){
//						for(int i=1; i<=image.length;i++){
						out.writeInt(image[i][image[0].length-j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeImgInt(int[][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

//			for(int k=1; k<=image[0][0].length;k++){
			for(int j=1; j<=image[0].length;j++){
//				for(int j=0; j<image[0].length;j++){
				for(int i=0; i<image.length;i++){
//					for(int i=1; i<=image.length;i++){
					out.writeInt(image[i][image[0].length-j]);
					////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
				}
			}

		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeImgFloat(float[][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

//			for(int k=1; k<=image[0][0].length;k++){
			for(int j=1; j<=image[0].length;j++){
//				for(int j=0; j<image[0].length;j++){
				for(int i=0; i<image.length;i++){
//					for(int i=1; i<=image.length;i++){
					out.writeFloat(image[i][image[0].length-j]);
					////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
				}
			}

		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeImgDouble(double[][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

			for(int k=0; k<image[0][0].length;k++){
				for(int j=1; j<=image[0].length;j++){
					for(int i=0; i<image.length;i++){
						out.writeDouble(image[i][image[0].length-j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeVectorDouble(double[] vec, String filename){
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
			for(int i=0; i<vec.length;i++){
				out.writeDouble(vec[i]);
				////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeVectorInt(int[] vec, String filename){
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
			for(int i=0; i<vec.length;i++){
				out.writeInt(vec[i]);
				////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeVectorByte(DataOutputStream out, byte[] vec) throws IOException{

		for(int i=0; i<vec.length;i++){
			out.writeByte(vec[i]);
		}

	}
	
	public static void writeVectorInt(DataOutputStream out, int[] vec) throws IOException{

		for(int i=0; i<vec.length;i++){
			out.writeInt(vec[i]);
			////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
		}

	}
	
	public static void writeASCII(DataOutputStream out, String towrite) throws IOException{
		out.writeChars(towrite);
	}

	public static void writeImgFloat(float[][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));

			for(int k=0; k<image[0][0].length;k++){
				for(int j=1; j<=image[0].length;j++){
					for(int i=0; i<image.length;i++){
						out.writeFloat(image[i][image[0].length-j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeVecImgFloat(float[][][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
			
			for(int k=0; k<image[0][0][0].length;k++){
				for(int j=0; j<image[0][0].length;j++){
					for(int i=0; i<image[0].length;i++){
						out.writeFloat(image[0][i][j][k]);
						out.writeFloat(image[1][i][j][k]);
						out.writeFloat(image[2][i][j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeVecImgFloatFlipDim1(float[][][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
			
			for(int l=0; l<image.length;l++){
				for(int k=0; k<image[0][0][0].length;k++){
					for(int j=1; j<=image[0][0].length;j++){
						for(int i=0; i<image[0].length;i++){
							out.writeFloat(image[l][i][image[0][0].length-j][k]);
							////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
						}
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void writeVecImgFloatDTIS(float[][][][] image, String filename) {
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
			
			for(int k=0; k<image[0][0][0].length;k++){
				for(int j=1; j<=image[0][0].length;j++){
					for(int i=0; i<image[0].length;i++){
						out.writeFloat(image[0][i][image[0][0].length-j][k]);
						out.writeFloat(image[1][i][image[0][0].length-j][k]);
						out.writeFloat(image[2][i][image[0][0].length-j][k]);
						////System.out.println(getClass().getCanonicalName()+"\t"+"At (" +j+","+i+","+k+") is: " + img[j][i][k]);
					}
				}
			}
			
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	

}
