package edu.jhu.ece.iacl.algorithms.manual_label.atlasing;

import java.io.File;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;

/**
 * 
 * @author John Bogovic
 * @date 07/22/2009
 *
 * Computes the LogOdds of probabilistic atlases as described in
 * Pohl et al. "Using the logarithm of odds to define a vector space on probabilistic atlases"
 * MIA 11(5) 465-477 2007
 */

public class LogOdds {
	
	private static ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();

	/**
	 * 	Computes the Log-Odds of probability maps of labels.
	 * The probabilities for each label should be stored in the 4th dimension 
	 * of the input array.
	 * 
	 * @param probAtlas The input Probability map
	 * @param indNorm Normalizing Index
	 * @param fourthDimLab Are the labels stored in the fourth dimension?  If this is false, 
	 * the method assumes that labels are stored across the first dimension.
	 * @return The point in Log-Odds space.
	 */
	public static float[][][][] Logit(float[][][][] probAtlas, int indNorm, float eps, boolean fourthDimLab){
		float[][][][] lo;
		int i=0;
		int j=0;
		int k=0;
		int l=0;
		if(fourthDimLab){
			// GET SIZES 
			int Nx = probAtlas.length;
			int Ny = probAtlas[0].length;
			int Nz = probAtlas[0][0].length;
			int Nl = probAtlas[0][0][0].length;

			
			//is choice of Normalizing index valid?
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Setting Normalizing Index = Nl");
				indNorm = Nl-1;
			}
			
			//which label indices will not be normalized?
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			//INITIALIZE OUTPUT ARRAY
			lo = new float[Nx][Ny][Nz][Nl-1];
			
			//compute Log-Odds
			for(l=0; l<(Nl-1); l++){
				for(k=0; k<Nz; k++){
					for(j=0; j<Ny; j++){
						for(i=0; i<Nx; i++){
							lo[i][j][k][l]= (float)Math.log(probAtlas[i][j][k][lolabels[l]]/probAtlas[i][j][k][indNorm]);
						}
					}
				}
			}
			
		}else{
			// GET SIZES AND INITIALIZE OUTPUT ARRAY
			int Nl = probAtlas.length;
			int Nx = probAtlas[0].length;
			int Ny = probAtlas[0][0].length;
			int Nz = probAtlas[0][0][0].length;
			
			//is choice of Normalizing index valid?
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Setting Normalizing Index = Nl");
				indNorm = Nl-1;
			}
			
			//which label indices will not be normalized?
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			lo = new float[Nl-1][Nx][Ny][Nz];
			
			//compute Log-Odds
			for(l=0; l<(Nl-1); l++){
				for(k=0; k<Nz; k++){
					for(j=0; j<Ny; j++){
						for(i=0; i<Nx; i++){
							lo[l][i][j][k]= (float)Math.log(probAtlas[lolabels[l]][i][j][k]/probAtlas[indNorm][i][j][k]);
						}
					}
				}
			}
			
		}
		
		return lo;
	}

	/**
	 * 	Computes the Log-Odds of probability maps of labels.
	 * The probabilities for each label should be stored in the 4th dimension 
	 * of the input array.
	 * 
	 * @param probAtlas The input Probability map
	 * @param indNorm Normalizing Index
	 * @param fourthDimLab Are the labels stored in the fourth dimension?  If this is false, 
	 * the method assumes that labels are stored across the first dimension.
	 * @return The point in Log-Odds space.
	 */
	public static ImageDataFloat Logit(ImageData probAtlas, int indNorm, float eps, boolean fourthDimLab, String name, File dir){
		ImageDataFloat lo;
		int i=0;
		int j=0;
		int k=0;
		int l=0;
		if(fourthDimLab){
			// GET SIZES 
			int Nx = probAtlas.getRows();
			int Ny = probAtlas.getCols();
			int Nz = probAtlas.getSlices();
			int Nl = probAtlas.getComponents();
			
			//is choice of Normalizing index valid?
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Setting Normalizing Index = Nl");
				indNorm = Nl-1;
			}
			
			// change the zeros to epsilon
			// important - otherwise the LogOdds space is messy (with +/- infinities)
			zeroToEpsilon(probAtlas,eps,fourthDimLab);
			normalize(probAtlas,fourthDimLab);
			
			System.out.println("jist.plugins"+"\t"+"Writing intermediate probabilities to file:");
			rw.write(probAtlas, dir);
			
			//get a list of all the label indices that were not normalizing
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			//INITIALIZE OUTPUT ARRAY
			lo = new ImageDataFloat(name,Nx,Ny,Nz,Nl-1);
			ImageHeader copyme = probAtlas.getHeader();
			lo.getHeader().setAxisOrientation(copyme.getAxisOrientation());
			lo.getHeader().setDimResolutions(copyme.getDimResolutions());
			
			//compute Log-Odds
			for(l=0; l<(Nl-1); l++){
				for(k=0; k<Nz; k++){
					for(j=0; j<Ny; j++){
						for(i=0; i<Nx; i++){
							lo.set(i,j,k,l,(Math.log(probAtlas.getFloat(i,j,k,lolabels[l])/probAtlas.getFloat(i,j,k,indNorm))));
						}
					}
				}
			}
			
		}else{
			// GET SIZES AND INITIALIZE OUTPUT ARRAY
			int Nl = probAtlas.getRows();
			int Nx = probAtlas.getCols();
			int Ny = probAtlas.getSlices();
			int Nz = probAtlas.getComponents();
			
			//is choice of Normalizing index valid?
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Setting Normalizing Index = Nl");
				indNorm = Nl-1;
			}
			
			//get a list of all the label indices that were not normalizing
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			lo = new ImageDataFloat("name",Nl,Nx,Ny,Nz);
			//compute Log-Odds
			for(l=0; l<(Nl-1); l++){
				for(k=0; k<Nz; k++){
					for(j=0; j<Ny; j++){
						for(i=0; i<Nx; i++){
							lo.set(l,i,j,k,(float)(Math.log(probAtlas.getFloat(lolabels[l],i,j,k)/probAtlas.getFloat(indNorm,i,j,k))));
						}
					}
				}
			}
		}
		return lo;
	}
	

	/**
	 * Computes the probability map corresponding to a point in Log-Odds space
	 * The probabilities for each label should be stored in the 4th dimension 
	 * of the input array.
	 * 
	 * @param logodds The input Log-Odds
	 * @param indNorm Normalizing Index
	 * @param fourthDimLab Are the labels stored in the fourth dimension?  If this is false, 
	 * the method assumes that labels are stored across the first dimension.
	 * @return The corresponding probability map 
	 */
	public static float[][][][] InvLogit(float[][][][] logodds, int indNorm, boolean fourthDimLab){
		float[][][][] pa;
		int i=0;
		int j=0;
		int k=0;
		int l=0;
		if(fourthDimLab){
			// GET SIZES 
			int Nx = logodds.length;
			int Ny = logodds[0].length;
			int Nz = logodds[0][0].length;
			int Nl = logodds[0][0][0].length+1;
			
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Exiting...");
				return null;
			}
			pa = new float[Nx][Ny][Nz][Nl];
			//get a list of all the label indices that were not normalizing
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			float sum = 1f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
						
						// FIRST compute the probabilities for the normalizing 
						// since we need these values for the next step anyway.
						sum=1f;
						for(l=0; l<(Nl-1); l++){
							sum+=Math.exp(logodds[i][j][k][l]);
						}
						pa[i][j][k][indNorm]=1/sum;
						
						// NEXT, compute all of the other probabilities
						for(l=0; l<(Nl-1); l++){
							pa[i][j][k][lolabels[l]]=(float)(pa[i][j][k][indNorm]*Math.exp(logodds[i][j][k][l]));
						}
					}
				}
			}
			
			
		}else{
			// GET SIZES 
			int Nl = logodds.length+1;
			int Nx = logodds[0].length;
			int Ny = logodds[0][0].length;
			int Nz = logodds[0][0][0].length;
			
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Exiting...");
				return null;
			}
			
			pa = new float[Nl][Nx][Ny][Nz];
			
			//get a list of all the label indices that were not normalizing
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			float sum = 1f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
						
						// FIRST compute the probabilities for the normalizing 
						// since we need these values for the next step anyway.
						sum=1f;
						for(l=0; l<(Nl-1); l++){
							sum+=Math.exp(logodds[l][i][j][k]);
						}
						pa[i][j][k][indNorm]=1/sum;
						
						// NEXT, compute all of the other probabilities
						for(l=0; l<(Nl-1); l++){
							pa[lolabels[l]][i][j][k]=(float)(pa[indNorm][i][j][k]*Math.exp(logodds[l][i][j][k]));
						}
					}
				}
			}
		}
		return pa;
	}

	/**
	 * Computes the probability map corresponding to a point in Log-Odds space
	 * The probabilities for each label should be stored in the 4th dimension 
	 * of the input array.
	 * 
	 * @param logodds The input Log-Odds
	 * @param indNorm Normalizing Index
	 * @param fourthDimLab Are the labels stored in the fourth dimension?  If this is false, 
	 * the method assumes that labels are stored across the first dimension.
	 * @return The corresponding probability map 
	 */
	public static ImageDataFloat InvLogit(ImageData logodds, int indNorm, boolean fourthDimLab, String name){
		
		ImageDataFloat pa;
		int i=0;
		int j=0;
		int k=0;
		int l=0;
		
		if(fourthDimLab){
			// GET SIZES 
			int Nx = logodds.getRows();
			int Ny = logodds.getCols();
			int Nz = logodds.getSlices();
			int Nl = logodds.getComponents()+1;
			
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Exiting...");
				return null;
			}
			pa = new ImageDataFloat(name,Nx,Ny,Nz,Nl);
			ImageHeader copyme = logodds.getHeader();
			pa.getHeader().setAxisOrientation(copyme.getAxisOrientation());
			pa.getHeader().setDimResolutions(copyme.getDimResolutions());
			
			//which label indices were not normalized?
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			float sum = 1f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
						
						// FIRST compute the probabilities for the normalizing 
						// since we need these values for the next step anyway.
						sum=1f;
						for(l=0; l<(Nl-1); l++){
							sum+=Math.exp(logodds.getFloat(i,j,k,l));
						}
						
						pa.set(i,j,k,indNorm,1/sum);
						
						// NEXT, compute all of the other probabilities
						for(l=0; l<(Nl-1); l++){
							pa.set(i,j,k,lolabels[l],(float)(pa.getFloat(i,j,k,indNorm)*Math.exp(logodds.getFloat(i,j,k,l))));
						}
					}
				}
			}
			
			
		}else{
			// GET SIZES 
			int Nl = logodds.getRows();
			int Nx = logodds.getCols();
			int Ny = logodds.getSlices();
			int Nz = logodds.getComponents()+1;
			
			if(indNorm>=Nl){
				System.err.println("jist.plugins"+"Warning, choice of Normalizing Index invalid.  Must be less than the number of lables\n" +
						"Exiting...");
				return null;
			}
			
			pa = new ImageDataFloat("name",Nl,Nx,Ny,Nz);
			
			//which label indices were not normalized?
			int[] lolabels = getNonNormalizedVector(Nl,indNorm);
			
			float sum = 1f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
						
						// FIRST compute the probabilities for the normalizing 
						// since we need these values for the next step anyway.
						sum=1f;
						for(l=0; l<(Nl-1); l++){
							sum+=logodds.getFloat(l,i,j,k);
						}
						
						pa.set(i,j,k,indNorm,1/sum);
						
						// NEXT, compute all of the other probabilities
						for(l=0; l<(Nl-1); l++){
							pa.set(lolabels[l],i,j,k,(float)(pa.getFloat(indNorm,i,j,k)*Math.exp(logodds.getFloat(l,i,j,k))));
						}
					}
				}
			}
		}
		
		return pa;
		
	}
	
	/**
	 * Helper method used in Logit and iLogit methods
	 * @param numlabs total number of labels
	 * @param indNorm index of label used as normalizer
	 * @return ordered vector of label indices that were normalized by the indNorm label
	 */
	public static int[] getNonNormalizedVector(int numlabs, int indNorm){
		int[] lolabels = new int[numlabs-1];
		int j=0;
		System.out.println("jist.plugins"+"\t"+"indices:");
		for(int i=0; i<numlabs; i++){
			if(i!=indNorm){
				lolabels[j]=i;  j++;
				System.out.println("jist.plugins"+"\t"+i);
			}
		}
		return lolabels;
	}
	
	/**
	 * Converts all zeros in an image data to epsilon.
	 * Ensures that the 'labe-wise' probability maps are 
	 * properly normalized.
	 * @param a the input ImageData
	 */
	public static void zeroToEpsilon(ImageData a, float epsilon, boolean fourthDim){
		if(fourthDim){
			// GET SIZES 
			int Nx = a.getRows();
			int Ny = a.getCols();
			int Nz = a.getSlices();
			int Nl = a.getComponents();
			int i=0; 
			int j=0;
			int k=0;
			int l=0;
			
//			int zerocount = 0;
//			float sum = 0f;
//			float delta = 0f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
//						zerocount = 0;
//						sum = 0f;
//						delta = 0f;
						for(l=0; l<Nl; l++){
							//look for zero values at this point and keep track of the sum
//							sum+=a.getFloat(i, j, k, l);
							if(a.getFloat(i, j, k, l)<epsilon){
								//whats the difference?
								//use this to update the normalizing sum
//								delta=epsilon-a.getFloat(i, j, k, l);
								
								//change the value
								a.set(i,j,k,l,epsilon);
//								sum+=delta;
//								zerocount++;
							}
						}
//						if(zerocount>0){
							//Renormalize
//							for(l=0; l<Nl; l++){
//								a.set(i,j,k,l,a.getFloat(i, j, k, l)/sum);
//							}
//						}
					}
				}
			}
			
		}else{
			// GET SIZES 
			int Nl = a.getRows();
			int Nx = a.getCols();
			int Ny = a.getSlices();
			int Nz = a.getComponents();
			int i=0; 
			int j=0;
			int k=0;
			int l=0;
			
//			int zerocount = 0;
			float sum = 0f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
//						zerocount = 0;
//						sum = 0f;
						for(l=0; l<Nl; l++){
							//look for zero values at this point and keep track of the sum
//							sum+=a.getFloat(l, i, j, k);
							if(a.getFloat(l, i, j, k)<epsilon){
								//change the value
								a.set(l,i,j,k,epsilon);
//								sum+=epsilon;
//								zerocount++;
							}
						}
//						if(zerocount>0){
//							//Renormalize
//							for(l=0; l<Nl; l++){
//								a.set(l,i,j,k,a.getFloat(l, i, j, k)/sum);
//							}
//						}
					}
				}
			}
			
		}
	}
	
	public static void normalize(ImageData a, boolean fourthDim){
		if(fourthDim){
			// GET SIZES 
			int Nx = a.getRows();
			int Ny = a.getCols();
			int Nz = a.getSlices();
			int Nl = a.getComponents();
			int i=0; 
			int j=0;
			int k=0;
			int l=0;
			
			float sum = 0f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
						sum=0f;
						for(l=0; l<Nl; l++){
							sum+=a.getFloat(i, j, k, l);
						}
						for(l=0; l<Nl; l++){
							a.set(i,j,k,l,a.getFloat(i,j,k,l)/sum);
						}
					}
				}
			}
			
		}else{
			// GET SIZES 
			int Nl = a.getRows();
			int Nx = a.getCols();
			int Ny = a.getSlices();
			int Nz = a.getComponents();
			int i=0; 
			int j=0;
			int k=0;
			int l=0;
			
			float sum = 0f;
			for(k=0; k<Nz; k++){
				for(j=0; j<Ny; j++){
					for(i=0; i<Nx; i++){
						sum=0f;
						for(l=0; l<Nl; l++){
							sum+=a.getFloat(i, j, k, l);
						}
						for(l=0; l<Nl; l++){
							a.set(l,i,j,k,a.getFloat(l,i,j,k)/sum);
						}
					}
				}
			}
		}
	}
	
}
