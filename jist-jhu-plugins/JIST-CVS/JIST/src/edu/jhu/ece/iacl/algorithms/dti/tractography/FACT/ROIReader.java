package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.File;
import java.io.IOException;
import java.awt.Image;

import javax.imageio.ImageIO;

public class ROIReader {

	double[][] pts;
	double[][] faces;
	int xs;
	int ys; 
	int zs;
	
	/* filetype: 	1 is Graphics object
	 * 				2 is Binary image
	 * 			
	 * 
	 */
	public ROIReader(String filename, int filetype){
		
		if (filetype==1){
			try{
				readGraphics();
			}catch (IOException  e) {
	            System.out.println("jist.plugins"+"\t"+e);
	            e.printStackTrace();
	        }
		}
		if (filetype==2){
			try{
				readBinImage();
			}catch (IOException  e) {
	            System.out.println("jist.plugins"+"\t"+e);
	            e.printStackTrace();
	        }
		}
	}
	
	public int[][][] readBinImage()  throws IOException {
		int[][][] mimg = new int[zs][ys][xs];
		
		
		
		return mimg;
	}
	
	public void readGraphics() throws IOException {
		
	}
	
}
