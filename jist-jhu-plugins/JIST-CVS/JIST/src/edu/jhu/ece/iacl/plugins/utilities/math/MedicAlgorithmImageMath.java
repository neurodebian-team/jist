/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.math;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmImageMath extends ProcessingAlgorithm{
	ParamVolume volParam;
	ParamVolume resultVolParam;
	ParamDouble valParam;
	ParamOption operation;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Perform simple image math on an input volume. ";
	private static final String longDescription = "Operations include 'Absolute Value', 'Add', 'Subtract', 'Multiply', 'Divide', 'Power', 'Log', and 'Exp'";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(volParam=new ParamVolume("Volume"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Absolute Value","Add","Subtract","Multiply","Divide","Power","Log","Exp"}));
		inputParams.add(valParam=new ParamDouble("Value",-1E30,1E30,1));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Math");
		inputParams.setLabel("Image Math");
		inputParams.setName("Image_Math");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVolParam=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ImageData vol=volParam.getImageData();
		int rows=vol.getRows();
		int cols=vol.getCols();
		int slices=vol.getSlices();
		int comps=vol.getComponents();
		ImageDataFloat resultVol=new ImageDataFloat(vol.getName()+"_math",rows,cols,slices,comps);
		double val=valParam.getDouble();
		double tmp;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < comps; l++) {
						tmp=vol.getDouble(i, j, k, l);
						switch(operation.getIndex()){
						case 0:
							tmp=Math.abs(tmp);break;
						case 1:
							tmp+=val;break;
						case 2:
							tmp-=val;break;
						case 3:
							tmp*=val;break;
						case 4:
							tmp=(tmp!=0)?tmp/val:0;break;
						case 5:
							tmp=Math.pow(tmp, val);break;
						case 6:
							tmp=Math.log(tmp);break;
						case 7:
							tmp=Math.exp(tmp);break;
						default:
						}
						resultVol.set(i, j, k,l, tmp);
					}
				}
			}
		}
		resultVol.setHeader(vol.getHeader());
		resultVolParam.setValue(resultVol);
	}
}
