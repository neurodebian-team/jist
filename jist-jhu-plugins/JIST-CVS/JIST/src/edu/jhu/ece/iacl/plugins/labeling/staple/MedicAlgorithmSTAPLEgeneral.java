package edu.jhu.ece.iacl.plugins.labeling.staple;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEgeneral;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleDxReaderWriter;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;


public class MedicAlgorithmSTAPLEgeneral extends ProcessingAlgorithm{
	private ParamFileCollection raterData;

	private ParamDouble eps;
	private ParamInteger maxiters;
	private ParamOption init;
	private ParamBoolean flaglabelprobs;

	//output Parameters
	ParamObject<String> pl;
	private ParamFileCollection probsOut;
	private ParamObject<double[][]> segOut;

	private ArrayList<Number> labels;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "STAPLE - Simultaneous Truth and Performance Level Estimation applied to a general array.";
	private static final String longDescription = "Given a number of labelings, STAPLE returns membership functions of the Truth.  This implementation requires only that the input objects be a collection of double arrays.  The ith element of all input arrays should be in correspondence.  Note that, because there is no implied relationship between points in these arrays, no regularization can be applied";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(raterData=new ParamFileCollection("Rater Data"));

		String[] initops = {"Performance"};
		inputParams.add(init=new ParamOption("Initialization Type", initops));
		
		inputParams.add(eps = new ParamDouble("Max Delta for Convergence"));
		eps.setValue(new Double(0.00001));

		inputParams.add(maxiters=new ParamInteger("Max Iterations"));
		maxiters.setValue(new Integer(25));
		
		inputParams.add(flaglabelprobs = new ParamBoolean("Output Label Probabilities?"));
		flaglabelprobs.setValue(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.STAPLE");
		inputParams.setLabel("STAPLE General Array");
		inputParams.setLabel("STAPLE_General_Array");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "http://putter.ece.jhu.edu/John"));
		info.setAffiliation("Johns Hopkins University, Department of Electrical Engineering");
		info.add(new Citation("Warfield SK, Zou KH, Wells WM, \"Simultaneous Truth and Performance Level Estimation (STAPLE): An Algorithm for the Validation of Image Segmentation\" IEEE TMI 2004; 23(7):903-21  "));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {		
		probsOut = new ParamFileCollection("Truth Estimates");
		outputParams.add(probsOut);
		probsOut.setMandatory(false);
		
		outputParams.add(segOut = new ParamObject<double[][]>("Label Array", new ArrayDoubleReaderWriter()));
		
		pl = new ParamObject<String>("PerformanceLevels",new StringReaderWriter());
		outputParams.add(pl);
	}


	protected void execute(CalculationMonitor monitor) {
		STAPLEgeneral staple = new STAPLEgeneral(getData());
		staple.setmaxIters(maxiters.getInt());
		staple.setEps(eps.getDouble());
		staple.setInit(init.getValue());
		staple.iterate();

		if(flaglabelprobs.getValue()){
			probsOut.setValue(writeTruth(staple.getTruth(),labels));
		}

		segOut.setObject(staple.getHardSeg());
		segOut.setFileName("EstimatedTrueLabelArray");

		pl.setObject(staple.getPeformanceLevel().toString());
		pl.setFileName("Performance");

		System.out.println(getClass().getCanonicalName()+"\t"+"FINISHED");
	}

	private ArrayList<double[][]> getData(){
		ArrayList<double[][]> dataout = new ArrayList<double[][]>(raterData.getValue().size());
		for(int i=0; i<raterData.size(); i++){
			dataout.add(ArrayDoubleReaderWriter.getInstance().read(raterData.getValue(i)));
		}
		return dataout;
	}

	private List<File> writeTruth(ArrayList<double[][]> truthdata,ArrayList<Number> labels){
		int i=0;
		ArrayList<File> filelist = new ArrayList<File>();
		ArrayDoubleDxReaderWriter writer = new ArrayDoubleDxReaderWriter();
		writer.setNumberFormat(new DecimalFormat("0.00000"));
		for(double[][] ithtruth : truthdata){
//			ArrayDoubleReaderWriter.getInstance().write(ithtruth, new File("/home/john/Desktop/StapleTest/general_dx/out/Truth"+labels.get(i)+".dx"));
			File f = new File(this.getOutputDirectory()+String.valueOf(File.separatorChar)+"Truth"+labels.get(i)+".dx");
			writer.write(ithtruth, f);
			i++;
		}
		return filelist;
	}
}
