package edu.jhu.ece.iacl.plugins.quantitative;

import java.awt.Color;
import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader.MeasurementUnit;

public class EstimateMTRseries extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume MTSeries;	// 4-D Volume containing MT data series
	private ParamBoolean clean;		// clean up the MTC map aferwards?
	//
	//private ParamVolume	MTWVolume;	// 3-D Volume containing MT-weighted data
	//private ParamFloat firstTE;
	//private ParamFloat secondTE;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume MTRMapSeries;	// Estimated MT maps

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Estimate MTR image(s) from one 4-D volume";
	private static final String longDescription = "Current implementation assumes 4-D Volume to be organized as follows:\n " +
			"First volume V(0)= no MT, other volumes V(1-end)= MT Weighted Images\n" +
			"Also assumes all volumes to be in the same registered space. Calculates MTR=1-(Vn/V0)\n" +
			"Output is a series of volumes sized (x,y,z,MTW-1)";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Quantitative");
		inputParams.setLabel("MTR: 4-D Series");
		inputParams.setName("MTR: 4-D Series");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com",""));
		info.setAffiliation("Utrecht Medical Center, department of Radiology");
		info.setDescription(shortDescription +"\n"+ longDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(MTSeries=new ParamVolume("MT Volume series (4D)",VoxelType.FLOAT,-1,-1,-1,-1));
		inputParams.add(clean = new ParamBoolean("Clean up MTR map?",true));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		MTRMapSeries = new ParamVolume("MTR series Estimate",VoxelType.FLOAT,-1,-1,-1,-1);
		MTRMapSeries.setName("MTRMapSeries");
		outputParams.add(MTRMapSeries);
		
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			this.setLabel("Calculating MTR series");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateMTRseries: Start");

			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageDataFloat scalarvol=new ImageDataFloat(MTSeries.getImageData());
			Boolean cleanb= new Boolean (clean.getValue());
			
			int r = scalarvol.getRows(), c = scalarvol.getCols(), s = scalarvol.getSlices(), t = scalarvol.getComponents();
			
			if (t < 2)
			{
			//	System.err.println(getClass().getCanonicalName()+"\t"+"Only one (or less) volumes found. Input volumes was sized (x,y,z,c): (" +
			//		r + "," + c + "," + s + "," + t + ")."	);
				System.err.println(getClass().getCanonicalName()+"\t"+"Only one (or less) volumes found. Aborting");
				return;
			}
			
			this.setTotalUnits(r*(t-1));
			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateMTRseries: Allocating memory.");

			ImageData mtrseries = new ImageDataFloat(r,c,s,t-1);
			mtrseries.setName(scalarvol.getName()+"_MTRSeries");
			mtrseries.setHeader(scalarvol.getHeader());
			
			
			//Try and figure out 4rth dimension, and set it in Hz.
			// 4rth Dimension will be the offset of the MT pulse
			// use .setUnitsOfMeasure and .setDimResolutions
			/*
			MeasurementUnit[] units = mtrseries.getHeader().getUnitsOfMeasure();
			mtrseries.getHeader().
			float[] reso = mtrseries.getHeader().getDimResolutions();
			reso[3] = mtoffset/(2*(t-1))
			
			//Assuming 4th dimension should be in Hz (ugly coding!!)
			units[3] = HZ; //fix it at HZ, but how?
			
			mtrseries.getHeader().setUnitsOfMeasure(units);
			
		*/
			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			
//			for (int v=0; v<t-1; v++){ //start with second volume
//				for(int i=0;i<r;i++) {
//					for(int j=0;j<c;j++)
//						for(int k=0;k<s;k++) {
//							float v0 = scalarvol.getFloat(i,j,k, 0); //get voxel value of vol(0)
//							float vn = scalarvol.getFloat(i,j,k, v+1);    // and of vol (n)
//							if (v0 < 1e-6) //catch (near) infinities to be zero, are NaN�s supported in JIST volume handling?
//								mtrseries.set(i,j,k,v,(float)(0.0));
//							else
//								mtrseries.set(i,j,k,v,(float)(1-(vn/v0)));
//						}
//				this.setCompletedUnits(i+ ((v)*r));
//				}
//			}
			
			
			for (int v=0; v<t-1; v++){ //start with second volume
				for(int i=0;i<r;i++) {
					for(int j=0;j<c;j++)
						for(int k=0;k<s;k++) {
							float v0 = scalarvol.getFloat(i,j,k,0);
							float v1 = scalarvol.getFloat(i,j,k,v+1);
							float mtr = (float) 0.0;
							if (v0 < 1e-6) //catch (near) infinities to be zero, are NaN�s supported in JIST volume handling?
								mtr = (float) 0.0;
							else{
								mtr = 1- (v1/v0);
								if (cleanb){  //clean up data some more
									if (mtr < 0.0)
										mtr = (float) 0.0;
									if (mtr > 1.0)
										mtr = (float) 1.0;
								}
							}
							mtrseries.set(i,j,k,mtr);
							/*float v0 = scalarvol.getFloat(i,j,k, 0); //get voxel value of vol(0)
							float vn = scalarvol.getFloat(i,j,k, v+1);    // and of vol (n)
							if (v0 < 1e-6) //catch (near) infinities to be zero, are NaNs supported in JIST volume handling?
								mtrseries.set(i,j,k,v,(float)(0.0));
							else
								mtrseries.set(i,j,k,v,(float)(1-(vn/v0)));*/
						}
					
				this.setCompletedUnits(i+ ((v)*r));
				}
			}
			


			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateMTRseries: Setting up exports.");
			MTRMapSeries.setValue(mtrseries);
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateMTRseries: FINISHED");
		}
	}
}
