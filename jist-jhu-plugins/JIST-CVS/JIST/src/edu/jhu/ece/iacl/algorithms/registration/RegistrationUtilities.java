package edu.jhu.ece.iacl.algorithms.registration;

import java.util.ArrayList;
import java.util.Collections;

import edu.jhmi.rad.medic.utilities.NumericsPublic;
import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class RegistrationUtilities {
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.5 $");
	}

	// Buffers
	//public static int i0, j0, k0, i1, j1, k1;
	//public static float dx, dy, dz, hx, hy, hz;
	//public static int ch;
	//public static float xx, yy, zz;

	public static int calculateBin(double interval, double minVal, double val) {
		return (int) Math.floor((val - minVal) / interval);
	}

	/* imA is assumed to be from 0 and numBin-1 */
	/* imB is assumed to be from 0 and numBin-1 */
	public static void JointHistogram3D(ImageData imA, ImageData imB,
			int numBin, int[] roi, int[][] jointHist) {
		int i, j, k, binA, binB;
		for (j = 0; j < numBin; j++) {
			for (i = 0; i < numBin; i++) {
				jointHist[j][i] = 0;
			}
		}
		for (k = roi[4]; k <= roi[5]; k++) {
			for (j = roi[2]; j <= roi[3]; j++) {
				for (i = roi[0]; i <= roi[1]; i++) {
					binA = imA.getUByte(i, j, k);
					if (binA >= numBin)
						binA = numBin - 1;
					if (binA < 0)
						binA = 0;
					binB = imB.getUByte(i, j, k);
					if (binB >= numBin)
						binB = numBin - 1;
					if (binB < 0)
						binB = 0;
					jointHist[binA][binB] += 1;
				}
			}
		}
	}

	public static void JointHistogram3D(ImageData imA[], ImageData imB[],
			int channel, int numBin, int[] roi, int[][][] jointHist) {
		JointHistogram3D(imA[channel], imB[channel], numBin, roi,
				jointHist[channel]);
	}

	/* the max value is assumed to be numBin-1, and min value 0 */
	public static void Histogram3D(ImageData im, int numBin, int[] roi,
			int[] hist) {
		int i, j, k;
		int bin;
		for (i = 0; i < numBin; i++)
			hist[i] = 0;
		for (k = roi[4]; k <= roi[5]; k++) {
			for (j = roi[2]; j <= roi[3]; j++) {
				for (i = roi[0]; i <= roi[1]; i++) {
					bin = im.getUByte(i, j, k);
					if (bin >= numBin)
						bin = numBin - 1;
					if (bin < 0)
						bin = 0;
					hist[bin] += 1;					
				}
			}
		}
	}

	public static void Histogram3D(ImageData im[], int channel, int numBin,
			int[] roi, int[][] hist) {
		Histogram3D(im[channel], numBin, roi, hist[channel]);

	}

	public static ImageDataFloat DeformImageFloat3D(ImageData im,
			ImageDataFloat DF) {
		ImageDataFloat vol = new ImageDataFloat(im.getName() + "_def", im
				.getRows(), im.getRows(), im.getSlices());
		vol.setHeader(im.getHeader());
		DeformImage3D(im, vol, DF, im.getRows(), im.getRows(), im.getSlices(), RegistrationUtilities.InterpolationType.TRILINEAR);
		return vol;
	}

	public static ImageData DeformImage3D(ImageData im, ImageDataFloat DF, int type) {
		ImageData vol = im.mimic();
		vol.setName(im.getName() + "_def");
		DeformImage3D(im, vol, DF, im.getRows(), im.getCols(), im.getSlices(), type);
		return vol;
	}

	public static void DeformImage3D(ImageData im, ImageData deformedIm,
			ImageDataFloat DF, int sizeX, int sizeY, int sizeZ, int type) {
		int i, j, k;
		float[][][][] DFM = DF.toArray4d();
		for (i = 0; i < sizeX; i++) for (j = 0; j < sizeY; j++) for (k = 0; k < sizeZ; k++) {
			if (DFM[i][j][k][0] != 0 || DFM[i][j][k][1] != 0
					|| DFM[i][j][k][2] != 0) {
				deformedIm.set(i, j, k, Interpolation(im, sizeX,
						sizeY, sizeZ, i + DFM[i][j][k][0], j
						+ DFM[i][j][k][1], k
						+ DFM[i][j][k][2], type));
			} else {
				deformedIm.set(i, j, k, im.getDouble(i, j, k));
			}
		}
	}


	public static void DeformationFieldResample3DM(float[][][][] oldDF,
			ImageDataFloat newDF, int oldSizeX, int oldSizeY, int oldSizeZ,
			int newSizeX, int newSizeY, int newSizeZ) {
		int i, j, k, dim;
		float x, y, z;
		float ratio[] = new float[3];
		ratio[0] = (float) Math.floor((float) newSizeX / oldSizeX + 0.5);
		ratio[1] = (float) Math.floor((float) newSizeY / oldSizeY + 0.5);
		ratio[2] = (float) Math.floor((float) newSizeZ / oldSizeZ + 0.5);
		float[][][][] newDFM = newDF.toArray4d();
		
		
		float[][][] tmpX = new float[oldSizeX][oldSizeY][oldSizeZ];
		float[][][] tmpY = new float[oldSizeX][oldSizeY][oldSizeZ];
		float[][][] tmpZ = new float[oldSizeX][oldSizeY][oldSizeZ];
		for(i=0; i < oldSizeX; i++) for(j=0; j < oldSizeY; j++) for(k=0; k < oldSizeZ; k++){
			tmpX[i][j][k] = oldDF[i][j][k][0];
			tmpY[i][j][k] = oldDF[i][j][k][1];
			tmpZ[i][j][k] = oldDF[i][j][k][2];
		}
		ImageDataFloat oldDFX = new ImageDataFloat(tmpX);
		ImageDataFloat oldDFY = new ImageDataFloat(tmpY);
		ImageDataFloat oldDFZ = new ImageDataFloat(tmpZ);
		
		for (i = 0; i < newSizeX; i++) {
			x = i / ratio[0];
			for (j = 0; j < newSizeY; j++) {
				y = j / ratio[1];
				for (k = 0; k < newSizeZ; k++) {
					z = k / ratio[2];
					newDFM[i][j][k][0] = (float) (Interpolation(
							oldDFX, oldSizeX, oldSizeY, oldSizeZ, x, y, z, InterpolationType.TRILINEAR) * ratio[0]);
					newDFM[i][j][k][1] = (float) (Interpolation(
							oldDFY, oldSizeX, oldSizeY, oldSizeZ, x, y, z, InterpolationType.TRILINEAR) * ratio[1]);
					newDFM[i][j][k][2] = (float) (Interpolation(
							oldDFZ, oldSizeX, oldSizeY, oldSizeZ, x, y, z,InterpolationType.TRILINEAR) * ratio[2]);
				}
			}
		}
	}
	
	public static float[][][] computeJacobianDet(float[][][][] def){
		int nx = def.length;
		int ny = def[0].length;
		int nz = def[0][0].length;
		float[][][] jacmap = new float[nx][ny][nz];
		float[][] jac = new float[3][3];
		float Dmx, Dmy, Dmz, Dpx, Dpy, Dpz, D0x, D0y, D0z;
		for(int x=0; x<nx; x++) for(int y=0; y<ny; y++) for(int z=0; z<nz; z++){
			// derivatives of the x comp wrt x, y, and z
			int LY = (y == 0) ? 1 : 0;
			int HY = (y == (ny - 1)) ? 1 : 0;
			int LZ = (z == 0) ? 1 : 0;
			int HZ = (z == (nz - 1)) ? 1 : 0;
			int LX = (x == 0) ? 1 : 0;
			int HX = (x == (nx - 1)) ? 1 : 0;

			// DERIVATIVES OF THE X COMPONENT 
			Dmx = def[x][y][z][0] - def[x - 1 + LX][y][z][0];
			Dpx = def[x + 1 - HX][y][z][0] - def[x][y][z][0];
			Dmy = def[x][y][z][0] - def[x][y - 1 + LY][z][0];
			Dpy = def[x][y + 1 - HY][z][0] - def[x][y][z][0];
			Dmz = def[x][y][z][0] - def[x][y][z - 1 + LZ][0];
			Dpz = def[x][y][z + 1 - HZ][0] - def[x][y][z][0];
			// central differences
			D0x = (def[x + 1 - HX][y][z][0] - def[x - 1 + LX][y][z][0]) / 2;
			D0y = (def[x][y + 1 - HY][z][0] - def[x][y - 1 + LY][z][0]) / 2;
			D0z = (def[x][y][z + 1 - HZ][0] - def[x][y][z - 1 + LZ][0]) / 2;
			
			//set the appropriate values in the jacobian matrix
			jac[0][0]=D0x;
			jac[0][1]=D0y;
			jac[0][2]=D0z;
			
			// DERIVATIVES OF THE Y COMPONENT 
			Dmx = def[x][y][z][1] - def[x - 1 + LX][y][z][1];
			Dpx = def[x + 1 - HX][y][z][1] - def[x][y][z][1];
			Dmy = def[x][y][z][1] - def[x][y - 1 + LY][z][1];
			Dpy = def[x][y + 1 - HY][z][1] - def[x][y][z][1];
			Dmz = def[x][y][z][1] - def[x][y][z - 1 + LZ][1];
			Dpz = def[x][y][z + 1 - HZ][1] - def[x][y][z][1];
			// central differences
			D0x = (def[x + 1 - HX][y][z][1] - def[x - 1 + LX][y][z][1]) / 2;
			D0y = (def[x][y + 1 - HY][z][1] - def[x][y - 1 + LY][z][1]) / 2;
			D0z = (def[x][y][z + 1 - HZ][1] - def[x][y][z - 1 + LZ][1]) / 2;
			//set the appropriate values in the jacobian matrix
			jac[1][0]=D0x;
			jac[1][1]=D0y;
			jac[1][2]=D0z;
			
			// DERIVATIVES OF THE Z COMPONENT 
			Dmx = def[x][y][z][2] - def[x - 1 + LX][y][z][2];
			Dpx = def[x + 1 - HX][y][z][2] - def[x][y][z][2];
			Dmy = def[x][y][z][2] - def[x][y - 1 + LY][z][2];
			Dpy = def[x][y + 1 - HY][z][2] - def[x][y][z][2];
			Dmz = def[x][y][z][2] - def[x][y][z - 1 + LZ][2];
			Dpz = def[x][y][z + 1 - HZ][2] - def[x][y][z][2];
			// central differences
			D0x = (def[x + 1 - HX][y][z][2] - def[x - 1 + LX][y][z][2]) / 2;
			D0y = (def[x][y + 1 - HY][z][2] - def[x][y - 1 + LY][z][2]) / 2;
			D0z = (def[x][y][z + 1 - HZ][2] - def[x][y][z - 1 + LZ][2]) / 2;
			
			//set the appropriate values in the jacobian matrix
			jac[2][0]=D0x;
			jac[2][1]=D0y;
			jac[2][2]=D0z;
			
			//set the value equal to the determinant
			jacmap[x][y][z]= 1f+ NumericsPublic.determinant3D(jac);
			
		}
		return jacmap;
	}




	public static double DoubleDistance(double z0, double y0, double x0,
			double z1, double y1, double x1) {
		double tmp = 0.0;

		tmp += (x0 - x1) * (x0 - x1);
		tmp += (y0 - y1) * (y0 - y1);
		tmp += (z0 - z1) * (z0 - z1);

		return Math.sqrt(tmp);
	}



	public static float RBF(float r) {
		if (r > 1 || r < 0)
			return 0;
		return (float) (Math.pow(1 - r, 4) * (3 * Math.pow(r, 3) + 12 * r * r
				+ 16 * r + 4));
	}

	public static float RBF3D(int cx, int cy, int cz, int x, int y, int z,
			float scale) {

		return RBF((float) (Math.sqrt((cx - x) * (cx - x) + (cy - y) * (cy - y)
				+ (cz - z) * (cz - z)) / scale));
	}


/*
	public static double Interpolation(float[][][][] oldVM,	int XN, int YN, int ZN, 
			int dim, double x, double y, double z, int type) {

		return Interpolation(oldV, XN, YN, ZN, x, y,z, type);
	}
*/

	public static double Interpolation(ImageData oldV, int XN, int YN,
			int ZN, double x, double y, double z, int type) {
		switch(type){

		case InterpolationType.TRILINEAR: //Trilinear
			return TrilinearInterpolation(oldV, XN, YN,	ZN, x, y, z);
		case InterpolationType.NEAREST_NEIGHTBOR: //Nearest Neighbor
			return NNInterpolation(oldV, XN, YN, ZN, x, y, z);
		default:
			return 0;
		}

	}

	public static class InterpolationType{
		public static final int TRILINEAR = 0;
		public static final int NEAREST_NEIGHTBOR = 1;
	}

	// Nearest Neighbor interpolation
	public static double NNInterpolation(ImageData oldV, int XN, int YN, int ZN,
			double x, double y, double z) {
		double d000 = 0.0, d001 = 0.0, d010 = 0.0, d011 = 0.0;
		double d100 = 0.0, d101 = 0.0, d110 = 0.0, d111 = 0.0;
		double value = 0.0, dist = 0.0;
		int x0 = (int) x, y0 = (int) y, z0 = (int) z;
		int x1 = x0 + 1, y1 = y0 + 1, z1 = z0 + 1;

		/*
		if (x == (double) (XN - 1))
			x1 = XN - 1;
		if (y == (double) (YN - 1))
			y1 = YN - 1;
		if (z == (double) (ZN - 1))
			z1 = ZN - 1;
		 */

		if (!((x0 < 0) || (x1 > (XN - 1)) || (y0 < 0) || (y1 > (YN - 1))
				|| (z0 < 0) || (z1 > (ZN - 1)))) {
			d000 = DoubleDistance(z, y, x, (double) z0, (double) y0,
					(double) x0);
			d100 = DoubleDistance(z, y, x, (double) z0, (double) y0,
					(double) x1);
			d010 = DoubleDistance(z, y, x, (double) z0, (double) y0,
					(double) x0);
			d110 = DoubleDistance(z, y, x, (double) z0, (double) y1,
					(double) x1);

			d001 = DoubleDistance(z, y, x, (double) z1, (double) y0,
					(double) x0);
			d101 = DoubleDistance(z, y, x, (double) z1, (double) y0,
					(double) x1);
			d011 = DoubleDistance(z, y, x, (double) z1, (double) y0,
					(double) x0);
			d111 = DoubleDistance(z, y, x, (double) z1, (double) y1,
					(double) x1);

			dist = d000;
			value = oldV.getDouble(x0, y0, z0);

			if (dist > d001) {
				dist = d001;
				value = oldV.getDouble(x0, y0, z1);
			}

			if (dist > d010) {
				dist = d010;
				value = oldV.getDouble(x0, y1, z0);
			}

			if (dist > d011) {
				dist = d011;
				value = oldV.getDouble(x0, y1, z1);
			}

			if (dist > d100) {
				dist = d100;
				value = oldV.getDouble(x1, y0, z0);
			}

			if (dist > d101) {
				dist = d101;
				value = oldV.getDouble(x1, y0, z1);
			}

			if (dist > d110) {
				dist = d110;
				value = oldV.getDouble(x1, y1, z0);
			}

			if (dist > d111) {
				dist = d111;
				value = oldV.getDouble(x1, y1, z1);
			}

			return value;
		} else {
			return 0;
		}
	}	

	public static double TrilinearInterpolation(ImageData oldV, int XN, int YN,
			int ZN, double x, double y, double z) {
		int i0, j0, k0, i1, j1, k1;
		double dx, dy, dz, hx, hy, hz;
		if (x < 0 || x > (XN - 1) || y < 0 || y > (YN - 1) || z < 0
				|| z > (ZN - 1)) {
			return 0;
		} else {
			j1 = (int) Math.ceil(x);
			i1 = (int) Math.ceil(y);
			k1 = (int) Math.ceil(z);
			j0 = (int) Math.floor(x);
			i0 = (int) Math.floor(y);
			k0 = (int) Math.floor(z);
			dx = x - j0;
			dy = y - i0;
			dz = z - k0;

			// Introduce more variables to reduce computation
			hx = 1.0f - dx;
			hy = 1.0f - dy;
			hz = 1.0f - dz;
			// Optimized below
			return   (((oldV.getDouble(j0, i0, k0) * hx + oldV.getDouble(j1, i0, k0) * dx) * hy 
					 + (oldV.getDouble(j0, i1, k0) * hx + oldV.getDouble(j1, i1, k0) * dx) * dy) * hz 
					+ ((oldV.getDouble(j0, i0, k1) * hx + oldV.getDouble(j1, i0, k1) * dx) * hy 
					 + (oldV.getDouble(j0, i1, k1) * hx + oldV.getDouble(j1, i1, k1) * dx) * dy)* dz);

		}
	}


	public static class IndexedFloat implements Comparable<IndexedFloat> {
		public int index;
		public float val;

		public IndexedFloat(float val, int index) {
			this.index = index;
			this.val = val;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(IndexedFloat o) {
			return (int) Math.signum(val - o.val);
		}
	}

	public static int[] QKSort2(float[] arr) {
		ArrayList<IndexedFloat> list = new ArrayList<IndexedFloat>(arr.length);
		int index = 0;
		for (int i = 0; i < arr.length; i++) {
			list.add(new RegistrationUtilities.IndexedFloat(arr[i], i));
		}
		Collections.sort(list);
		int[] brr = new int[arr.length];
		for (IndexedFloat in : list) {
			arr[index] = in.val;
			brr[index++] = in.index;
		}
		return brr;
	}

	public static double VectorNormalization(double[] v, int n) {
		int i;
		double ret = 0;
		double thre = 0.0000001;
		for (i = 0; i < n; i++) {
			ret += v[i] * v[i];
		}
		ret = Math.sqrt(ret);
		if (ret < thre) {
			for (i = 0; i < n; i++) {
				v[i] = 0;
				ret = 0;
			}
		} else {
			for (i = 0; i < n; i++) {
				v[i] /= ret;
			}
		}
		return ret;
	}

	public static double NMI(int[][] histA, int[][] histB, int[][][] histAB,
			int channel, int numBin) {

		return NMI(histA[channel], histB[channel], histAB[channel], numBin);
	}

	public static double NMI(int[] histA, int[] histB, int[][] histAB,
			int numBin) {
		int i = 0, j = 0;
		double HA = 0, HB = 0, HAB = 0;
		int numVoxelA = 0, numVoxelB = 0, numVoxelAB = 0;
		double tmp = 0;
		for (i = 0; i < numBin; i++) {
			numVoxelA += histA[i];
			numVoxelB += histB[i];
			for (j = 0; j < numBin; j++) {
				numVoxelAB += histAB[j][i];
			}
		}
		for (i = 0; i < numBin; i++) {
			if (histA[i] > 0) {
				tmp = ((double) histA[i]) / numVoxelA;
				HA -= tmp * Math.log(tmp);
			}
			if (histB[i] > 0) {
				tmp = ((double) histB[i]) / numVoxelB;
				HB -= tmp * Math.log(tmp);
			}
			for (j = 0; j < numBin; j++) {
				if (histAB[j][i] > 0) {
					tmp = ((double) histAB[j][i]) / numVoxelAB;
					HAB -= tmp * Math.log(tmp);
				}
			}
		}

		if (HA == 0 && HB == 0 && HAB == 0)
			return 2;
		else
			return (HA + HB) / HAB;
	}

	
	
	
	/*

	public static ImageData DeformImage3DNN(ImageData im, ImageDataFloat DF) {
		ImageData vol = im.mimic();
		vol.setName(im.getName() + "_def");
		DeformImage3DNN(im, vol, DF, im.getRows(), im.getCols(), im.getSlices());
		return vol;
	}

	public static void DeformImage3DNN(ImageData im, ImageData deformedIm,
			ImageDataFloat DF, int sizeX, int sizeY, int sizeZ) {
		int i, j, k;
		float[][][][] DFM = DF.toArray4d();
		for (i = 0; i < sizeX; i++) for (j = 0; j < sizeY; j++) for (k = 0; k < sizeZ; k++) {
					if (DFM[i][j][k][0] != 0 || DFM[i][j][k][1] != 0
							|| DFM[i][j][k][2] != 0) {
						deformedIm.set(i, j, k, Interpolation(im, sizeX, sizeY,	sizeZ, i + DFM[i][j][k][0],
								j + DFM[i][j][k][1], k + DFM[i][j][k][2], interpolationType.NearestNeighbor));
					} else {
						deformedIm.set(i, j, k, im.getDouble(i, j, k));
					}
				}
	}
	 */
	
	/*
	public static double Interpolation(ImageData oldV, int XN, int YN,
			int ZN, float x, float y, float z, int type) {

		return Interpolation(oldV, XN, YN, ZN, (double)x, (double)y, (double)z, type);
	}
*/
	
	/*
	public static double TrilinearInterpolationFloatM(float[][][][] oldVM,
			int XO, int YO, int ZO, int dim, double x, double y, double z) {
		int i0, j0, k0, i1, j1, k1;
		float dx, dy, dz, hx, hy, hz;
		float xx, yy, zz;
		xx = (float) x;
		yy = (float) y;
		zz = (float) z;
		if (xx < 0 || xx > (XO - 1) || yy < 0 || yy > (YO - 1) || zz < 0
				|| zz > (ZO - 1)) {
			return 0;
		} else {

			j1 = (int) Math.ceil(xx);
			i1 = (int) Math.ceil(yy);
			k1 = (int) Math.ceil(zz);
			j0 = (int) Math.floor(xx);
			i0 = (int) Math.floor(yy);
			k0 = (int) Math.floor(zz);
			dx = xx - j0;
			dy = yy - i0;
			dz = zz - k0;

			// Introduce more variables to reduce computation
			hx = 1.0f - dx;
			hy = 1.0f - dy;
			hz = 1.0f - dz;
			return ((oldVM[j0][i0][k0][dim] * hx + oldVM[j1][i0][k0][dim] * dx)
	 * hy + (oldVM[j0][i1][k0][dim] * hx + oldVM[j1][i1][k0][dim]
	 * dx)
	 * dy)
	 * hz
					+ ((oldVM[j0][i0][k1][dim] * hx + oldVM[j1][i0][k1][dim]
	 * dx)
	 * hy + (oldVM[j0][i1][k1][dim] * hx + oldVM[j1][i1][k1][dim]
	 * dx)
	 * dy) * dz;
		}
	}
	 */
	/*
	public static double TrilinearInterpolation(ImageData oldV, int XN, int YN,
			int ZN, float x, float y, float z) {

		return TrilinearInterpolation(oldV, XN, YN,	ZN, (double)x, (double)y, (double)z);
	}
	 */

	/*
	// buffers to reduce memory allocation
	public static void TrilinearInterpolation(ImageData oldV[], int XO, int YO,
			int ZO, int channels, double x, double y, double z, double[] res) {
		//int i0, j0, k0, i1, j1, k1;
		//float dx, dy, dz, hx, hy, hz;
		float xx, yy, zz;

		int ch;
		xx = (float) x;
		yy = (float) y;
		zz = (float) z;

	//	if (xx < 0 || xx > (XO - 1) || yy < 0 || yy > (YO - 1) || zz < 0
		//		|| zz > (ZO - 1)) {
		//	for (ch = 0; ch < channels; ch++) {
		//		res[ch] = 0;
		//	}
	//	} else {
	//		j1 = (int) Math.ceil(xx);
		//	i1 = (int) Math.ceil(yy);
	//	k1 = (int) Math.ceil(zz);
		//	j0 = (int) Math.floor(xx);
		//	i0 = (int) Math.floor(yy);
		//	k0 = (int) Math.floor(zz);
	//		dx = xx - j0;
		//	dy = yy - i0;
		//	dz = zz - k0;

			// Introduce more variables to reduce computation
		//	hx = 1.0f - dx;
		//	hy = 1.0f - dy;
		//	hz = 1.0f - dz;
			for (ch = 0; ch < channels; ch++) {

				res[ch] = Interpolation(oldV[ch], XO, YO, ZO, xx, yy, zz, interpolationType.Trilinear);
					//(int) Math
					//	.floor(((oldV[ch].getDouble(j0, i0, k0) * hx + oldV[ch]
					//			.getDouble(j1, i0, k0)
					//			* dx)
					//			* hy + (oldV[ch].getDouble(j0, i1, k0) * hx + oldV[ch]
					//			.getDouble(j1, i1, k0)
					//			* dx)
					//			* dy)
					//			* hz
					//			+ ((oldV[ch].getDouble(j0, i0, k1) * hx + oldV[ch]
					//					.getDouble(j1, i0, k1)
					//					* dx)
					//					* hy + (oldV[ch].getDouble(j0, i1, k1)
					//					* hx + oldV[ch].getDouble(j1, i1, k1)
					//					* dx)
					//					* dy) * dz + 0.5);
		//	}
		}
	}
	 */
}
