package edu.jhu.ece.iacl.plugins.quantitative;

import java.awt.Color;

import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class EstimateMTR extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume NoMTVolume;	// 3-D Volume containing non-MT data
	private ParamVolume	MTWVolume;	// 3-D Volume containing MT-weighted data
	
	private ParamBoolean clean;		// clean up the MTC map afterwards?
	
	//TODO allow for brainmask as optional input
	
	//private ParamFloat firstTE;
	//private ParamFloat secondTE;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume MTRMapVolume;	// Estimated MT map

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Estimate MTR image from two (3D) volumes.";
	private static final String longDescription = "Assumes volumes V0 (no MT) and V1 (MTweighted) to be in the same registered space. Calculates MTR=1-(V1/V0)\n" +
			"					Cleaning up the data will force any value <0 or >1 to be 0 or 1.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Quantitative");
		inputParams.setLabel("MTR:TwoVolumes");
		inputParams.setName("MTR:TwoVolumes");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com",""));
		info.setAffiliation("Utrecht Medical Center, department of Radiology");
		info.setDescription(shortDescription +"\n"+ longDescription);
		info.setLongDescription(shortDescription +"\n"+ longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(NoMTVolume=new ParamVolume("non-MT Volume (3D)",null,-1,-1,-1,1));
		inputParams.add(MTWVolume=new ParamVolume("MT-weighted Volume (3D)",null,-1,-1,-1,1));
		inputParams.add(clean = new ParamBoolean("Clean up MTR map?",true));

	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		MTRMapVolume = new ParamVolume("MTR Estimate (3D)",VoxelType.FLOAT,-1,-1,-1,1);
		MTRMapVolume.setName("MTRmap");
		outputParams.add(MTRMapVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			this.setLabel("Calculating MTR");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/
			JistLogger.logError(JistLogger.INFO, "EstimateMTR: Start");
			JistLogger.logOutput(4, "EstimateMTR: Start");

			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageDataFloat scalarVol0=new ImageDataFloat(NoMTVolume.getImageData());
			ImageDataFloat scalarVol1=new ImageDataFloat(MTWVolume.getImageData());
			Boolean cleanb= new Boolean (clean.getValue());
			
			int r0=scalarVol0.getRows(), c0=scalarVol0.getCols(), s0=scalarVol0.getSlices(), t0 = scalarVol0.getComponents();
			int r1=scalarVol1.getRows(), c1=scalarVol1.getCols(), s1=scalarVol1.getSlices(), t1 = scalarVol1.getComponents();


			if ((r0 != r1) || (c0 != c1) || (s0 != s1) || (t0 != t1)){
				System.err.println(getClass().getCanonicalName()+"\t"+"Image dimensions do not match, aborting");
				return;
			}
			
			this.setTotalUnits(r0);
			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/
			this.setLabel("Allocating Memory");
			JistLogger.logError(JistLogger.INFO, "EstimateMTR: Allocating Memory");

			ImageData MTR_Est = new ImageDataFloat(r0,c0,s0,1);
			MTR_Est.setName(scalarVol0.getName()+"_MTR");
			MTR_Est.setHeader(scalarVol0.getHeader());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			
			this.setLabel("Calculating MTR");
			JistLogger.logError(JistLogger.INFO, "EstimateMTR: Calculating MTR");
			for(int i=0;i<r0;i++) {
				this.setCompletedUnits(i);
				for(int j=0;j<c0;j++)
					for(int k=0;k<s0;k++) {
						float v0 = scalarVol0.getFloat(i,j,k);
						float v1 = scalarVol1.getFloat(i,j,k);
						float mtr = (float) 0.0;
						if (v0 < 1e-6) //catch (near) infinities to be zero, are NaN�s supported in JIST volume handling?
							mtr = (float) 0.0;
						else{
							mtr = 1- (v1/v0);
							if (cleanb){  //clean up data some more
								if (mtr < 0.0)
									mtr = (float) 0.0;
								if (mtr > 1.0)
									mtr = (float) 1.0;
							}
						}
						MTR_Est.set(i,j,k,mtr);
						if (s0 < 1e-6) //catch (near) infinities to be zero, are NaNs supported in JIST volume handling?
							MTR_Est.set(i,j,k,(float)(0.0));
						else
							MTR_Est.set(i,j,k,(float)(1-(v1/v0)));
					}
			}

			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/

			this.setLabel("Finishing");
			JistLogger.logError(JistLogger.INFO, "EstimateMTR: Setting up exports.");
			MTRMapVolume.setValue(MTR_Est);
			
			JistLogger.logError(JistLogger.INFO, "EstimateMTR: Finished.");
		}
	}
}
