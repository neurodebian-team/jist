package edu.jhu.ece.iacl.plugins.labeling;

import java.io.IOException;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.utility.ArrayUtil;

public class MedicAlgorithmLabelCombine extends ProcessingAlgorithm {

	//input params
	private ParamVolume 	labelVolume;
	private ParamFile		combineParam;
	private ParamBoolean	replace;

	//output params
	private ParamVolume combinedLabelVolume;
	
	//helper objects
	TextFileReader tfr;
	
	private static final String cvsversion = "$Revision: 1.2 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Given a volume with N labels, creates a new volume with M labels (M<=N) by combining" +
			"the labels as specified.";
	private static final String longDescription = "The Label combine/replacement specification is a text file that describes how labels" +
			"will be combined or replaced.  If the \"Replacement?\" option is unchecked, the specification can have multiple label values per row." +
			"Any label value in the ith row will be replaced by label 'i'.  IT IS OFTEN USEFUL TO HAVE A '0' (ZERO) IN THE FIRST ROW.  If a label" +
			"value appears in multiple lines, it will be replaced by the index of the first line in which it appears.\n\n" +
			"If the \"Replacement?\" option is checked, this module results in label values being replaced rather than combined. " +
			"The text file must have at least two columns, values in the third column or later will be ignored.  The method will locate" +
			"any label in the first column of the specification and replace it with the label in the second column.";
	
	
	protected void createInputParameters(ParamCollection inputParams) {
		
		inputParams.add(labelVolume = new ParamVolume("Label Volume"));
		labelVolume.setDescription("Volume of labels whose labels will be combined.");

		inputParams.add(combineParam = new ParamFile("Label combination/replacement specification",new FileExtensionFilter(new String[]{"txt","csv"})));
		inputParams.add(replace = new ParamBoolean("Replacement?", false));
		
		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling");
		inputParams.setLabel("Label Combine/Replace");
		inputParams.setName("Combine_Replace_Labels");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "http://www.iacl.ece.jhu.edu/John"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);

	}

	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(combinedLabelVolume = new ParamVolume("Combined Label Volume"));
		combinedLabelVolume.setDescription("Volume of labels after combination.");

	}

	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
	
		ImageData labelvol = labelVolume.getImageData();
		ImageHeader hdr = labelvol.getHeader();
		ImageData outvol = labelvol.mimic();
		outvol.setHeader(hdr);
		outvol.setName(labelvol.getName()+"_labelCombine");
		
		int[][] combs = null;
		tfr = new TextFileReader(combineParam.getValue());
		try {
			combs = tfr.parseIntFile();
		} catch (IOException e) { 
			JistLogger.logError(JistLogger.WARNING, "LabelCombine: Unable to parse combination-file.");
			throw new RuntimeException("LabelCombine: Unable to parse combination-file.");
		}
		
		System.out.println(ArrayUtil.printArray(combs));
		
		if(replace.getValue()){
			for(int i=0; i<labelvol.getRows(); i++) for(int j=0; j<labelvol.getCols(); j++) for(int k=0; k<labelvol.getSlices(); k++){
				outvol.set(i,j,k,replace(combs,labelvol.getInt(i,j,k)));
			}
		}else{
			for(int i=0; i<labelvol.getRows(); i++) for(int j=0; j<labelvol.getCols(); j++) for(int k=0; k<labelvol.getSlices(); k++){
				outvol.set(i,j,k,search(combs,labelvol.getInt(i,j,k)));
			}
		}
		
		combinedLabelVolume.setValue(outvol);

	}
	
	/**
	 * returns the (first) row in array that contains val
	 * @param array
	 * @param val
	 * @return
	 */
	private int search(int[][] array, int val){
		for(int i=0; i<array.length; i++){
			for(int j=0; j<array[0].length; j++){
				if(array[i][j]==val){
					return i;
				}
			}
		}
		return -1;
	}
	
	/**
	 * returns the (first) row in array that contains val
	 * @param array
	 * @param val
	 * @return
	 */
	private int replace(int[][] array, int val){
		for(int i=0; i<array.length; i++){
			if(array[i][0]==val){
				return array[i][1];
			}
		}
		return val;
	}

}
