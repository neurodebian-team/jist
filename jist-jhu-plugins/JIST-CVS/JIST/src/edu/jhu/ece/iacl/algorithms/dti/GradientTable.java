package edu.jhu.ece.iacl.algorithms.dti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The GradientTable class represents a GradientTable used for DWMRI as a 2D double array.
 * It provides functionality for reading and writing String representations of gradient tables
 * in various common formats.
 * 
 * @author John Bogovic
 *
 */
public class GradientTable {
	
	double[][] table;
	String tableType;
	
	/**
	 * Default constructor.
	 */
	public GradientTable(){	
	}
	/**
	 * Constructor that accepts a double array representation.
	 */
	public GradientTable(double[][] table){
		this.table=table;
	}
	/**
	 * This get method returns this object's gradient table.
	 * @return This objects double array
	 */
	public double[][] getTable(){
		return table;
	}
	/**
	 * This get method returns this object's table type.
	 * @return This objects double array
	 */
	public String getTableType(){
		return tableType;
	}
	/**
	 * Writes the Gradient Table simply to a text file.
	 * @return A String representation of the Gradient Table.
	 */
	public String toArrayTxt(){
		String stable = "";
		for(int i=0;i<table.length;i++){
			for(int j=0;j<table[i].length;j++){
				stable=stable+table[i][j]+"\t";
			} 
			stable=stable+"\n";
		}
		return stable;
	}
	/**
	 * Writes the Gradient Table to a text file in DTIStudio's expected format.
	 * @return A String representation of the Gradient Table.
	 */
	public String toDtisTable(){
		String stable = "";
		for(int i=0;i<table.length;i++){
			stable=stable+i+": ";
			for(int j=0;j<table[i].length;j++){
				stable=stable+table[i][j]+", ";
			} 
			stable=stable+"\n";
		}
		return stable;
	}
	/**
	 * Writes the Gradient Table to a text file in FSL's expected format.
	 * @return A String representation of the Gradient Table.
	 */
	public String toFslTable(){
		String stable = "";
		for(int i=0;i<table[0].length;i++){
			for(int j=0;j<table.length;j++){
				stable=stable+table[j][i]+" ";
			} 
			stable=stable+"\n";
		}
		return stable;
	}
	/**
	 * Writes the Gradient Table to a text file in Camino's expected format.
	 * @return A String representation of the Gradient Table.
	 */
	public String toCaminoTable(String bval){
		String stable = bval+"\n"+table.length + "\n";
		for(int i=0;i<table[0].length;i++){
			for(int j=0;j<table.length;j++){
				stable=stable+table[j][i]+"\n";
			} 
		}
		return stable;
	}
	/**
	 * Writes the Gradient Table to a text file in MedINRIA's expected format.
	 * @return A String representation of the Gradient Table.
	 */
	public String toMedINRIATable(){
		String stable = ""+table.length + "\n";
		stable=stable+toArrayTxt();
		return stable;
	}
	/**
	 * Reads a Gradient table from a text file.  Detects the format on the fly.
	 * @param The file (ASCII) to be read.
	 * @return A double array representation of the Gradient Table.
	 */
	public double[][] readTable(File f){
		ArrayList<String> lines = new ArrayList<String>();
		try{
			BufferedReader rdr = new BufferedReader(new FileReader(f));
			String thisline = " ";
			thisline = rdr.readLine();
			while(thisline!=null && !thisline.isEmpty()){
				
				String cleanline = thisline;
				if(thisline.indexOf(':')>0){
					cleanline = cleanline.substring(cleanline.indexOf(':')+1);
				}
				cleanline = cleanline.replaceAll(",", "").trim();
				
				lines.add(cleanline);
				thisline = rdr.readLine();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
		//Convert to a List of Lists (so the 
		ArrayList<ArrayList<Double>> listtable = new ArrayList<ArrayList<Double>>();
		//For each line
		for(String s : lines){
			ArrayList<Double> linenums = new ArrayList<Double>();
			String endline = s;
			while(endline.contains(" ") || endline.contains("\t")){
				if(endline.contains(" ")){
					linenums.add(Double.parseDouble(endline.substring(0, endline.indexOf(' ')).trim()));
					endline = endline.substring(endline.indexOf(' '), endline.length()).trim();
				}else if(endline.contains("\t")){
					linenums.add(Double.parseDouble(endline.substring(0, endline.indexOf('\t')).trim()));
					endline = endline.substring(endline.indexOf('\t'), endline.length()).trim();
				}else{
					System.err.println(getClass().getCanonicalName()+"Improperly Formatted Gradient Table");
					return null;
				}
				
			}
			linenums.add(Double.parseDouble(endline));
			listtable.add(linenums);
		}
		
		//Convert ArrayList to double[][]
		double[][] out;
		listtable.trimToSize();
		if(listtable.size()>0){
			
			if(listtable.size()==3){	//FSL format table
				out = new double[listtable.get(0).size()][3];
				for(int i=0; i<listtable.get(0).size(); i++){
					out[i][0]=listtable.get(0).get(i);
					out[i][1]=listtable.get(1).get(i);
					out[i][2]=listtable.get(2).get(i);
				}
			}else if(listtable.get(0).size()==3){ //DTIS or IACL format table
				out = new double[listtable.size()][3];
				for(int i=0; i<listtable.size(); i++){
					out[i][0]=listtable.get(i).get(0);
					out[i][1]=listtable.get(i).get(1);
					out[i][2]=listtable.get(i).get(2);
				}
			}else{
				System.err.println(getClass().getCanonicalName()+"Improperly Formatted Gradient Table");
				return null;
			}
			
		}else{
			System.err.println(getClass().getCanonicalName()+"Improperly Formatted Gradient Table");
			return null;
		}
		table=out;
		return out;
	}
	/**
	 * Appends an input  Gradient Table to this object.
	 * @param The  Gradient Table to be appended.
	 * @return The result of appending the input Gradient Table to this Gradient Table.
	 */
	public GradientTable append(GradientTable appendme){
		int N = this.table.length;
		double[][] combined = new double[N+appendme.table.length][3];
		//set values from this table
		for(int i=0; i<N; i++){
			for(int j=0;j<this.table[0].length;j++){
				combined[i][j]=this.table[i][j];
			}
		}
		//append the new table
		for(int i=0; i<appendme.table.length; i++){
			for(int j=0;j<appendme.table[0].length;j++){
				combined[N+i][j]=appendme.table[i][j];
			}
		}
		return new GradientTable(combined);
	}
	
//	public double[][] readDtisTable(File f){
//		
//	}
	
	public String toString(){
		String table_str="";
		for(int i=0; i<table.length; i++){
			for(int j=0; j<table[0].length; j++){
				table_str=table_str+table[i][j]+"\t";
			}
			table_str=table_str+"\n";
		}
		
		return table_str;
	}
}
