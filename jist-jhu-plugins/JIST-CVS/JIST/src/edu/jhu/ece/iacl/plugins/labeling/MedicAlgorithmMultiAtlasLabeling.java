package edu.jhu.ece.iacl.plugins.labeling;

import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.vabra.VabraAlgorithm;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmMultiAtlasReg;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmMultiDeformVolume;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmTransformVolume;
import edu.jhu.ece.iacl.plugins.labeling.staple.MedicAlgorithmSTAPLE;

public class MedicAlgorithmMultiAtlasLabeling extends ProcessingAlgorithm {

	//input
	public ParamVolumeCollection inParamAtlases;
	public ParamVolumeCollection inParamAtlasLabels;
	public ParamVolume inParamTarget;
	public ParamBoolean inParamSaveDefAtlases;

	//output
	public ParamVolumeCollection outParamDeformedAtlasesLabels;
	public ParamVolumeCollection outParamDeformedAtlases;
	public ParamVolumeCollection outParamDefFields;
	public ParamFileCollection outTransMatrices;
	public ParamVolume outParamLabeledTarget;


	public MedicAlgorithmMultiAtlasReg multivabra;
	public MedicAlgorithmSTAPLE staple;
	public MedicAlgorithmMultiDeformVolume multideform;
	public MedicAlgorithmTransformVolume multitransform;
	ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();

	private static final String revnum = "$Revision: 1.5 $".replace(
			"Revision: ", "").replace("$", "").replace(" ", "");
	public String[] getDefaultJVMArgs() {
		return new String[] { "-XX:MinHeapFreeRatio=70",
				"-XX:MaxHeapFreeRatio=95",
				"-XX:YoungGenerationSizeIncrement=150",
				"-XX:TenuredGenerationSizeIncrement=150" };
	}

	protected void createInputParameters(ParamCollection inputParams) {
		//Create Dependant Algorithms
		multivabra = new MedicAlgorithmMultiAtlasReg();
		staple = new MedicAlgorithmSTAPLE();
		multideform = new MedicAlgorithmMultiDeformVolume();
		multitransform = new MedicAlgorithmTransformVolume();

		//set intput as
		inParamAtlases = multivabra.inParamAtlases;
		inParamTarget = multivabra.inParamTarget;
		inParamSaveDefAtlases = multivabra.inParamSaveDefAtlases;
		inParamSaveDefAtlases.setValue(false);		

		//Manage my own memory for these
		inParamAtlases.setLoadAndSaveOnValidate(false);

		ParamCollection mainParams = new ParamCollection("Main");
		mainParams.add(inParamTarget);
		mainParams.add(inParamAtlases);
		mainParams.add(inParamAtlasLabels = new ParamVolumeCollection("Atlases Labels"));
		inParamAtlasLabels.setLoadAndSaveOnValidate(false);

		ParamCollection intermParams = new ParamCollection("Intermediate Results");
		intermParams.add(inParamSaveDefAtlases);

		staple.ratervols.setHidden(true);
		staple.probFlag.setHidden(true);
		staple.eps.setValue(.0000001);
		staple.maxiters.setValue(35);
		
		inputParams.add(mainParams);
		inputParams.add(intermParams);
		inputParams.add(multivabra.vabra.getInput());
		inputParams.add(staple.getInput());

		inputParams.setLabel("Labeling via Multiple Atlas Registrations");
		inputParams.setName("MultiAtlasLabel");

		setPreferredSize(new Dimension(300, 600));

		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(CommonAuthors.minChen);
		info.setDescription("Uses N Atlases to label a target image with the same labels");
		info.setLongDescription("This algorithm is used to label a volume by registering multiple atlases to"
						+ " the same target via multiple calls of single channel VABRA.");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}

	protected void createOutputParameters(ParamCollection outputParams) {

		
		outputParams.add(outParamLabeledTarget = new ParamVolume("Label Results for Target"));
		outputParams.add(outParamDeformedAtlasesLabels = new ParamVolumeCollection("Deformed Atlas Labels (Intermediate Result)"));
		outputParams.add(outParamDeformedAtlases = new ParamVolumeCollection("Registered Atlases (Intermediate Result)"));
		outputParams.add(outParamDefFields = new ParamVolumeCollection("Deformation Fields (Intermediate Result)"));
		FileExtensionFilter xfmfilter = new FileExtensionFilter(new String[] {"mtx", "xfm" });
		outputParams.add(outTransMatrices = new ParamFileCollection("Transformation Matrices (Intermediate Result)", xfmfilter));

		outParamDeformedAtlasesLabels.setMandatory(false);
		outParamDeformedAtlases.setMandatory(false);
		outParamDefFields.setMandatory(false);
		outTransMatrices.setMandatory(false);

		outputParams.setName("MultiAtlasLabel");
	}

	protected void execute(CalculationMonitor monitor) {
		File intermOutputDir = new File(this.getOutputDirectory() + File.separator
				+ "Intermediat Results");
		intermOutputDir.mkdir();

		ImageData tarVol = inParamTarget.getImageData();
		ImageData labelsOut;

		System.out.format("*********************Registering All Atlases to Target*********************\n");

		multivabra.inParamSaveDefFields.setValue(true);
		multivabra.setOutputDirectory(intermOutputDir);
		multivabra.run();

		//link intermediate results as outputs
		if(inParamSaveDefAtlases.getValue()){
			outParamDeformedAtlases.setValue(multivabra.outParamDeformedAtlases.getImageDataList());
			outParamDeformedAtlases.setLoadAndSaveOnValidate(false);
			outParamDeformedAtlases.writeAndFreeNow(multivabra);
		}
		
		//link intermediate results as outputs
		outParamDefFields.setValue(multivabra.outParamDefFields.getImageDataList());
		outParamDefFields.setLoadAndSaveOnValidate(false);	
		outParamDefFields.writeAndFreeNow(multivabra);
		
		//check if affine transformation is needed
		outTransMatrices.setValue(multivabra.outParamTransformMatrices.getValue());

		System.out.format("*********************Transforming Labels*********************\n");
		multitransform.vol=inParamAtlasLabels;
		multitransform.interpolation.setValue(7);
		multitransform.matchToMe=inParamTarget;
		multitransform.transformations=multivabra.outParamTransformMatrices;
		multitransform.rotAroundCenter.setValue(false);
		multitransform.setOutputDirectory(intermOutputDir);
		multitransform.run();

		System.out.format("*********************Deforming Labels*********************\n");
		multideform.inParamVolsIn=multitransform.result;		
		inParamAtlasLabels.dispose();
			
		multideform.inParamDefFields.setValue(multivabra.outParamDefFields.getImageDataList());
		multideform.inParamNNFlag.setValue(true);
		multideform.setOutputDirectory(intermOutputDir);
		multideform.run();

		//link intermediate results as outputs
		outParamDeformedAtlasesLabels.setValue(multideform.outParamDeformedVols.getImageDataList());
		outParamDeformedAtlasesLabels.setLoadAndSaveOnValidate(false);
		outParamDeformedAtlasesLabels.writeAndFreeNow(multideform);
		
		System.out.format("*********************Running STAPLE*********************\n");
		
		staple.ratervols=multideform.outParamDeformedVols;
		staple.run();
		labelsOut = staple.labelvol.getImageData();

		labelsOut.setHeader(tarVol.getHeader());
		labelsOut.setName(tarVol.getName() + "_labels");

		outParamLabeledTarget.setValue(labelsOut);
	}

}
