package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

public class MedicAlgorithmTensorFormatConvert extends ProcessingAlgorithm{
	private ParamVolume in;
	private ParamOption formatin, formatout;
	private ParamVolume out;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts a tensor volume in the format described by \"Input format\"" +
			"to a tensor volume in the format of \"Output format\"";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(in=new ParamVolume("Tensor Volume",null,-1,-1,-1,6));
		inputParams.add(formatin=new ParamOption("Input format",
				new String[]{"xx,xy,xz,yy,yz,zz","xx,yy,zz,xy,yz,xz","xx,yy,zz,xy,xz,yz"}));
		formatin.setValue(1);
		inputParams.add(formatout=new ParamOption("Output format",
				new String[]{"xx,xy,xz,yy,yz,zz","xx,yy,zz,xy,yz,xz","xx,yy,zz,xy,xz,yz"}));
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Contrasts");
		inputParams.setLabel("Tensor Format Converter");
		inputParams.setName("Tensor_Format_Converter");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {	
		outputParams.add(out=new ParamVolume("Volume Out",null,-1,-1,-1,6));
	}


	protected void execute(CalculationMonitor monitor) {
        ImageData vol = in.getImageData();
   
        if(formatin.getValue().equals(formatout.getValue())){
        	out.setValue(vol);
        	return;
        }else{
            ImageData volout = vol.mimic();
            volout.setHeader(vol.getHeader());
            int[] map = null;
            
            //1 to 2
        	if(formatin.getValue().equals("xx,yy,zz,xy,yz,xz") && formatout.getValue().equals("xx,xy,xz,yy,yz,zz")){
            	map = new int[]{0,3,4,1,5,2};
            }else if(formatin.getValue().equals("xx,xy,xz,yy,yz,zz") && formatout.getValue().equals("xx,yy,zz,xy,yz,xz")){
            	map = new int[]{0,3,5,1,4,2};
            }
            
        	//1 to 3
            else if(formatin.getValue().equals("xx,yy,zz,xy,xz,yz") && formatout.getValue().equals("xx,xy,xz,yy,yz,zz")){
            	map = new int[]{0,3,4,1,5,2};
            }else if(formatin.getValue().equals("xx,xy,xz,yy,yz,zz") && formatout.getValue().equals("xx,yy,zz,xy,xz,yz")){
            	map = new int[]{0,3,5,1,2,4};
            }
        	
        	//2 to 3
            else if(formatin.getValue().equals("xx,yy,zz,xy,yz,xz") && formatout.getValue().equals("xx,yy,zz,xy,xz,yz")){
            	map = new int[]{0,1,2,3,5,4};
            }else if(formatin.getValue().equals("xx,yy,zz,xy,xz,yz") && formatout.getValue().equals("xx,yy,zz,xy,yz,xz")){
            	map = new int[]{0,1,2,3,5,4};
            }
            
            else{
            	System.err.println("INVALID INPUT/OUTPUT FORMAT!");
            	return;
            }
        	
            for(int l=0;l<vol.getComponents();l++){
            	for(int i=0; i<vol.getRows(); i++){
            		for(int j=0;j<vol.getCols();j++){
            			for(int k=0;k<vol.getSlices();k++){
            				volout.set(i, j, k,l, vol.getDouble(i, j,k, map[l]));
            			}
            		}
            	}   
            }
            out.setValue(volout);
            
        }

	}
}
