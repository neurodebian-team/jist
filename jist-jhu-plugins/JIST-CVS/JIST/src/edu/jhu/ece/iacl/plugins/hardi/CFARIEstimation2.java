package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.vecmath.Point3f;

import Jama.Matrix;

import com.thoughtworks.xstream.XStream;

import edu.jhu.bme.smile.commons.math.MatrixMath;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIBasisFunction;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIBasisSet;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIEstimator2;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import gov.nih.mipav.model.structures.ModelImage;


public class CFARIEstimation2 extends ProcessingAlgorithm {
	// Input Parameters
	private ParamSurface surface1;			// 1st tesselation
	private ParamSurface surface2;			// 2nd tesselation
	private ParamFloat angleSep;			// Maximum angle of separation
	private ParamFloat mixtureThreshold;	// Threshold to reprocess
	private ParamFile SchemeFile;			// CFARI Scheme file
	private ParamFileCollection DWdata4D; 	// DWI data
	private ParamFileCollection Mask3D;		// Binary mask to indicate computation volume
	private ParamInteger optionNumBasis;	// Number of basis functions to export
	private ParamFloat optionLambda;		// Lambda value
	private ParamInteger numProcess;		// Number of concurrent threads to run

	// Output Parameters
	private ParamFileCollection basisIndecies;
	private ParamFileCollection basisMixture;
	private ParamSurface basisDirections;

	// Private variables
	private static TreeMap<int[], ArrayList<int[]>> processedLoc;
	private static int[][][][] bIndecies;
	private static float[][][][] bMixtures;
	private static CFARIBasisSet largeBasisSet;
	private static int[] origLoc;
	private static Point3f[] largeVert;
	private static CFARIWrapper wrapper;

	private static int counter;
	private static int iter;
	private static int[][] sliceLoc;
	private static byte[][][] mask;
	private static float[][][][] data;
	private static int nBasisReturn2;
	private static float lambda;


	// CVS Information
	private static final String cvsversion = "$Revision: 1.15 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";
	

	protected void createInputParameters(ParamCollection inputParams) {
		// Plugin Information
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("CFARIEstimation v2 - Developmental");
		inputParams.setName("CFARI_Estimation_v2 - Developmental");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Hanlin Wan","hanlinwan@gmail.com",""));
		info.setDescription("CFARI Estimator used to resolve multiple tissue orientations in voxels.");
		info.setLongDescription("");
		info.setAffiliation("Johns Hopkins University");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		// Input Parameters
		inputParams.add(surface1=new ParamSurface("1st Orientation Basis"));
		inputParams.add(surface2=new ParamSurface("2nd Orientation Basis"));
		inputParams.add(angleSep=new ParamFloat("Max Separation Angle",5,85,15));
		inputParams.add(mixtureThreshold=new ParamFloat("Mixture Threshold",(float)0.1));
		inputParams.add(SchemeFile=new ParamFile("CFARI Scheme",new FileExtensionFilter(new String[]{"CFARI"})));
		inputParams.add(DWdata4D=new ParamFileCollection("DWI Data (4D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(Mask3D=new ParamFileCollection("Mask Volume to Determine Region of Tensor Estimation (3D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.	
		inputParams.add(optionLambda=new ParamFloat("Select Lambda",0,1,0.1f));
		optionLambda.setLabel("Select Lambda Relative to Lambda-Star");
		inputParams.add(optionNumBasis=new ParamInteger("Number of Basis Functions to Export",1,Integer.MAX_VALUE,5));				
		inputParams.add(numProcess=new ParamInteger("Number of Simultaneous Threads to Use",Runtime.getRuntime().availableProcessors()));				
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		// Output Parameters
		outputParams.add(basisIndecies=new ParamFileCollection("Basis Indecies",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		outputParams.add(basisMixture=new ParamFileCollection("Mixture Fraction",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));			
		outputParams.add(basisDirections=new ParamSurface("Basis Directions"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		wrapper=new CFARIWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	public class CFARIWrapper extends AbstractCalculation implements Runnable {
		private long t1, t2;
		private EmbeddedSurface tes1, tes2;
		private ImageDataMipav dwd, maskVol;
		private ArrayList<File> indList=new ArrayList<File>(), mixList=new ArrayList<File>();
		private ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();

		public void run() {
			int s=getCount();
			int[] slices=sliceLoc[s];
			if (iter==1) {		// 1st iteration
				System.out.println(getClass().getCanonicalName()+"\t"+"Thread "+s+": Slices "+slices[0]+" - "+(slices[1]-1)+"; "+slices[2]+" voxels");
				System.out.flush();
				int[] loc=origLoc;
				CFARIBasisSet basisSet=extractBasisSet(loc);
				CFARIEstimator2.estimateCFARI(data, mask, basisSet, .02, bIndecies, bMixtures, 
						lambda, loc, slices, wrapper);
			}
			else {				// 2nd iteration
				int count;
				int[] loc;
				CFARIBasisSet basisSet;
				Map.Entry<int[],ArrayList<int[]>> pLoc=processedLoc.firstEntry();
				System.out.println(getClass().getCanonicalName()+"\t"+"Thread "+s+": Subsets "+slices[0]+" - "+(slices[1]-1)+"; "+slices[2]+" voxels");
				System.out.flush();
				for (count=0; count<slices[0]; count++)
					pLoc=processedLoc.higherEntry(pLoc.getKey());
				while (count<slices[1]) {
					if (pLoc.getKey()[0]==-2) {
						loc=new int[largeVert.length];
						for (int c=0; c<loc.length; c++)
							loc[c]=c;
					}
					else {
						ArrayList<Integer> pts = new ArrayList<Integer>(origLoc.length*2);
						for (int i=0; i<origLoc.length; i++)
							pts.add(origLoc[i]);
						for (int i=0; i<pLoc.getKey().length; i++) {
							if (pLoc.getKey()[i]!=-1) {
								ArrayList<Integer> pt = getDirections(pLoc.getKey()[i]);
								for (int j=0; j<pt.size(); j++)
									pts.add(pt.get(j));
							}
						}
						Collections.sort(pts);
						int c=0;
						while (c<pts.size()) {		// Remove duplicate directions
							if (c!=0) {
								if (pts.get(c)==pts.get(c-1))
									pts.remove(c);
								else
									c++;
							}
							else 
								c++;
						}
						Point3f[] vert = new Point3f[pts.size()];
						for (c=0; c<pts.size(); c++) 
							vert[c] = largeVert[pts.get(c)];
						loc=setLoc(vert);
					}
					basisSet=extractBasisSet(loc);
					CFARIEstimator2.estimateCFARIList(data, mask, basisSet, .02, bIndecies, bMixtures, 
							lambda, loc, pLoc.getValue(), wrapper);
					pLoc = processedLoc.higherEntry(pLoc.getKey());
					count++;
				}
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Thread "+s+" Done");
			System.out.flush();
		}


		private synchronized int getCount() {
			counter++;
			return counter;
		}


		public void execute() {
			long startTime=System.currentTimeMillis();
			this.setLabel("Starting");
			System.out.println(getClass().getCanonicalName()+"\t"+"Initializing...");
			System.out.flush();
			t1=System.currentTimeMillis();

			// Create tesselated surfaces
			tes1 = surface1.getObject();
			tes2 = surface2.getObject();
			Point3f[] vert = tes1.getVertexCopy();
			largeVert = tes2.getVertexCopy();
			int[] loc=setLoc(vert);
			origLoc = loc.clone();
			loc = null; vert = null;

			// Load scheme
			XStream xstream = new XStream();
			try {
				ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
				largeBasisSet=(CFARIBasisSet)in.readObject();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}

			// Error checking
			nBasisReturn2 = optionNumBasis.getInt()+1;
			if(nBasisReturn2<1 || nBasisReturn2>largeBasisSet.getNumberOfBasisFunctions())
				throw new RuntimeException("Invalid number of return basis functions requested");
			if(Mask3D.getValue().size()!=0 && DWdata4D.getValue().size()!=Mask3D.getValue().size())
				throw new RuntimeException("Number of image slabs do not match number of mask slabs");
			lambda = optionLambda.getFloat();
			t2=System.currentTimeMillis();
			System.out.println(getClass().getCanonicalName()+"\t"+"Done - "+((t2-t1)/(float)1000)+" seconds\n");

			// Parse input
			int slabNum=DWdata4D.getValue().size();
			for (int jSlab=0; jSlab<slabNum; jSlab++) {
				if (slabNum!=1)
					System.out.println(getClass().getCanonicalName()+"\t"+"*****SLAB "+(jSlab+1)+"*****");
				this.setLabel("Slab "+(jSlab+1)+" of "+DWdata4D.getValue().size());
				this.setTotalUnits(1);
				this.setCompletedUnits(0);
				System.out.println(getClass().getCanonicalName()+"\t"+"Loading...");
				System.out.flush();
				t1=System.currentTimeMillis();
				dwd=(ImageDataMipav)rw.read(DWdata4D.getValue(jSlab));
				dwd.setName(DWdata4D.getValue(jSlab).getName());
				maskVol=(ImageDataMipav)rw.read(Mask3D.getValue(jSlab));
				runCFARI();
			}
			basisIndecies.setValue(indList);
			basisMixture.setValue(mixList);
			long endTime=System.currentTimeMillis();
			System.out.println(getClass().getCanonicalName()+"\t"+"CFARI Estimation Finished\nTotal Time: "+(endTime-startTime)/(float)1000);
		}

		/*
		 * Run CFARI on each slab
		 */
		public void runCFARI() {
			// Allocate memory
			ModelImage dwImage=dwd.getModelImageCopy();
			ModelImage maskImage=null;
			if (maskVol!=null)
				maskImage=maskVol.getModelImageCopy();
			int rows=dwd.getRows();
			int cols=dwd.getCols();
			int slices=dwd.getSlices();
			int comps=dwd.getComponents();
			int threads=numProcess.getInt();
			data=new float[rows][cols][slices][comps];
			mask=new byte[rows][cols][slices];
			Matrix numVoxels=new Matrix(slices,1);
			for (int i=0; i<rows; i++) {
				for (int j=0; j<cols; j++) {
					for (int k=0; k<slices; k++) {
						if (maskImage==null)
							mask[i][j][k]=1;
						else
							mask[i][j][k]=maskImage.get(i,j,k).byteValue();
						if (mask[i][j][k]==1) {
							numVoxels.set(k,0,numVoxels.get(k,0)+1);
							for (int l=0; l<comps; l++)
								data[i][j][k][l]=dwImage.get(i,j,k,l).floatValue();
						}
					}
				}
			}

			if (maskVol!=null) {
				maskImage.disposeLocal();
				maskVol.dispose();
			}

			bIndecies = new int[rows][cols][slices][nBasisReturn2];
			bMixtures = new float[rows][cols][slices][nBasisReturn2];
			t2=System.currentTimeMillis();
			System.out.println(getClass().getCanonicalName()+"\t"+"Done - "+((t2-t1)/(float)1000)+" seconds\n");

			// Run 1st iteration of CFARI
			System.out.println(getClass().getCanonicalName()+"\t"+"Starting 1st iteration of CFARI estimation...");
			System.out.flush();
			t1=System.currentTimeMillis();
			int sum=(int) MatrixMath.sum(numVoxels);
			sliceLoc=new int[threads][3];
			int sNum=0;
			for (int i=0; i<threads; i++) {
				sliceLoc[i][0]=sNum;
				while (sNum<slices && sliceLoc[i][2]<=(sum/threads))
					sliceLoc[i][2]+=numVoxels.get(sNum++,0);
				sliceLoc[i][1]=sNum;
			}
			this.setTotalUnits((long)(sum/0.45));
			this.setCompletedUnits(.05);
			counter=-1;
			iter=1;	
			Thread[] th = new Thread[threads];
			for (int s=0; s<threads; s++) {
				th[s]=new Thread(new CFARIWrapper());
				th[s].start();
			}
			for (int i=0; i<th.length; i++) {
				try { th[i].join(); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
			sliceLoc=null;
			t2=System.currentTimeMillis();
			double t=0;
			if (sum!=0) t=(t2-t1+0.0)/sum;
			System.out.println(getClass().getCanonicalName()+"\t"+"Number of voxels: " + sum);
			System.out.println(getClass().getCanonicalName()+"\t"+"Avg time per voxel: " + t);
			System.out.println(getClass().getCanonicalName()+"\t"+"Done - "+((t2-t1)/(float)1000)+" seconds\n");

			// Process 1st iteration results
			System.out.println(getClass().getCanonicalName()+"\t"+"Processing results...");
			System.out.flush();
			t1=System.currentTimeMillis();
			int iso=processResults();
			t2=System.currentTimeMillis();
			System.out.println(getClass().getCanonicalName()+"\t"+"Done - "+((t2-t1)/(float)1000)+" seconds\n");

			// Run 2nd iteration of CFARI
			System.out.println(getClass().getCanonicalName()+"\t"+"Starting 2nd iteration of CFARI estimation...");
			System.out.flush();
			t1=System.currentTimeMillis();
			this.setTotalUnits((long)((sum-iso)/0.5));
			this.setCompletedUnits(.5);
			int totalNum=sum-iso;
			counter=-1;
			iter=0;
			sliceLoc=new int[threads][3];
			sNum=0;			
			Map.Entry<int[],ArrayList<int[]>> pLoc=processedLoc.firstEntry();
			for (int i=0; i<threads; i++) {
				sliceLoc[i][0]=sNum;
				while (sNum<processedLoc.size() && sliceLoc[i][2]<=(totalNum/threads)) {
					sliceLoc[i][2]+=pLoc.getValue().size();
					pLoc=processedLoc.higherEntry(pLoc.getKey());
					sNum++;
				}
				sliceLoc[i][1]=sNum;
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Selective directions:");
			for (int s=0; s<threads; s++) {
				th[s]=new Thread(new CFARIWrapper());
				th[s].start();
			}
			for (int i=0; i<th.length; i++) {
				try { th[i].join(); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
			t2=System.currentTimeMillis();
			t=0;
			if (sum!=0) t=(t2-t1+0.0)/sum;
			System.out.println(getClass().getCanonicalName()+"\t"+"Avg time per voxel: " + t);
			System.out.println(getClass().getCanonicalName()+"\t"+"Done - "+((t2-t1)/(float)1000)+" seconds\n");

			// Output data		
			System.out.println(getClass().getCanonicalName()+"\t"+"Data export...");
			System.out.flush();
			t1=System.currentTimeMillis();
			int[][][][] bInd = new int[rows][cols][slices][nBasisReturn2-1];
			float[][][][] bMix = new float[rows][cols][slices][nBasisReturn2-1];
			for (int i=0; i<rows; i++) {
				for (int j=0; j<cols; j++) {
					for (int k=0; k<slices; k++) {
						for (int l=0; l<nBasisReturn2-1; l++) {
							bInd[i][j][k][l] = bIndecies[i][j][k][l];
							bMix[i][j][k][l] = bMixtures[i][j][k][l];
						}
					}
				}
			}
			ImageData out=new ImageDataInt(bInd);
			out.setHeader(dwd.getHeader());
			ModelImage img=out.getModelImageCopy();
			int ext[] = new int[]{rows,cols,slices,optionLambda.getInt()};
			img.setExtents(ext);
			ext[3]=nBasisReturn2-1;
			FileUtil.updateFileInfo(dwImage,img);
			img.calcMinMax();
			out.setName(dwImage.getImageName()+"_basisIndecies");
			File targetInd=rw.write(out,getOutputDirectory());
			indList.add(targetInd);
			img.disposeLocal();
			out.dispose();

			out=new ImageDataFloat(bMix);
			out.setHeader(dwd.getHeader());
			img=out.getModelImageCopy();
			img.setExtents(ext);
			FileUtil.updateFileInfo(dwImage,img);
			img.calcMinMax();
			out.setName(dwImage.getImageName()+"_basisMixtures");
			File targetMix=rw.write(out,getOutputDirectory());
			mixList.add(targetMix);
			img.disposeLocal();
			out.dispose();
			dwImage.disposeLocal();
			dwd.dispose();
			processedLoc=null; bIndecies=null; bMixtures=null; mask=null;
			dwd=null; maskVol=null; sliceLoc=null; data=null;

			tes2.setName("basisDirections"+tes2.getVertexCount());
			basisDirections.setValue(tes2);
			t2=System.currentTimeMillis();
			System.out.println(getClass().getCanonicalName()+"\t"+"Done - "+((t2-t1)/(float)1000)+" seconds\n");
		}


		/*
		 * Extracts the basis set depending on the directions used.
		 * @param loc indecies to extract
		 * @return basis set
		 */
		private synchronized CFARIBasisSet extractBasisSet(int[] loc) {
			CFARIBasisSet basisSet = new CFARIBasisSet();
			int numDWMeasurements = largeBasisSet.getDWScheme().numDWMeasurements();
			Matrix SensingMatrix = new Matrix(numDWMeasurements,loc.length);
			Vector<CFARIBasisFunction> basisTensors = new Vector<CFARIBasisFunction>();
			Matrix LargeSensingMatrix = largeBasisSet.getSensingMatrix();
			Vector<CFARIBasisFunction> LargeBasisTensors = largeBasisSet.getBasisSet();

			for (int i=0; i<loc.length; i++) {
				basisTensors.add(LargeBasisTensors.get(loc[i]));
				SensingMatrix.setMatrix(0,numDWMeasurements-1,i,i,
						LargeSensingMatrix.getMatrix(0,numDWMeasurements-1,loc[i],loc[i]));
			}
			basisSet.setDWScheme(largeBasisSet.getDWScheme());
			basisSet.setSensingMatrix(SensingMatrix);
			basisSet.setBasisSet(basisTensors);
			return basisSet;
		}


		/*
		 * Sets the location of the smaller tesselated surface to match the same indecies
		 * as the larger tesselated surface
		 * @param vert smaller tesselated surface
		 * @return integer array of the corresponding indecies
		 */
		private synchronized int[] setLoc(Point3f[] vert) {
			int[] loc = new int[vert.length];
			int vertCount = -1;
			for (Point3f v1 : vert) {
				int totalCount = 0;
				loc[++vertCount] = -1;
				for (Point3f v2 : largeVert) {
					if (v1.distance(v2) <= 1e-3) {
						loc[vertCount] = totalCount;
						break;
					}
					totalCount++;
				}
				if (loc[vertCount]==-1)
					throw new RuntimeException("ERROR: Try different combination of tesselations.");
			}
			return loc;
		}


		/*
		 * Processes the results after the first iteration to groups results into subsets 
		 * with the same new directions
		 * @return number of isotropic voxels
		 */
		private int processResults() {
			int xDim=bMixtures.length;
			int yDim=bMixtures[0].length;
			int zDim=bMixtures[0][0].length;
			int[] s = new int[nBasisReturn2+1];
			processedLoc = new TreeMap<int[], ArrayList<int[]>>(new IndexCompare());
			for (int i=0; i<xDim; i++) {
				for (int j=0; j<yDim; j++) {
					for (int k=0; k<zDim; k++) {
						if (bIndecies[i][j][k][0]!=-1) {
							int n=0;
							int[] index= new int[nBasisReturn2];
							Arrays.fill(index, -1);
							for (int p=0; p<bMixtures[i][j][k].length; p++) {
								if (bMixtures[i][j][k][p]>mixtureThreshold.getFloat()) {
									index[p]=bIndecies[i][j][k][p];
									n++;
								}
							}
							if (n==nBasisReturn2)
								Arrays.fill(index, -2);
							Arrays.sort(index);
							if (processedLoc.containsKey(index))
								processedLoc.get(index).add(new int[]{i,j,k});
							else {
								ArrayList<int[]> al = new ArrayList<int[]>();
								al.add(new int[]{i,j,k});
								processedLoc.put(index, al);				
							}
							s[n]++;
						}
					}
				}
			}
			int[] index=new int[nBasisReturn2];
			Arrays.fill(index, -1);
			processedLoc.remove(index);
			for (int i=0; i<s.length; i++)
				System.out.println(getClass().getCanonicalName()+"\t"+"Size "+i+" -- "+s[i]);
			System.out.println(getClass().getCanonicalName()+"\t"+"Number of subsets -- "+processedLoc.size());
			return s[0];
		}


	 	/*
	 	 * Gets the indecies that are within the specified angle to the given index
	 	 * @param pt index to find directions around
	 	 * @return list of all indecies within angle
	 	 */
	 	private synchronized ArrayList<Integer> getDirections(int pt) {
	 		ArrayList<Integer> dir = new ArrayList<Integer>();
			Matrix v1=new Matrix(new double[]{largeVert[pt].x,largeVert[pt].y,largeVert[pt].z},3);;
			Matrix v2=new Matrix(3,1);
			for (int i=0; i<largeVert.length; i++) {
				v2.set(0,0,largeVert[i].x);
				v2.set(1,0,largeVert[i].y);
				v2.set(2,0,largeVert[i].z);
				double angle=Math.min(Math.acos(MatrixMath.dotProduct(v1,v2)), Math.acos(MatrixMath.dotProduct(v1,v2.times(-1))))*180/Math.PI;
				if (angle<angleSep.getFloat())
					dir.add(i);
			}
	 		return dir;
	 	}
	}


	/*
	 * Comparator used to sort processedLoc
	 */
	private class IndexCompare implements Comparator<int[]> {
		public int compare(int[] o1, int[] o2) {
			for (int i=0; i< o1.length; i++) {
				if (o1[i]<o2[i])
					return 1;
				else if (o1[i]>o2[i])
					return -1;
			}
			return 0;
		}
	}
 }
