package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.ArrayIntegerTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class Process3DMRIImages extends ProcessingAlgorithm {
	// Input params
	private ParamFileCollection inputFiles;		// 3D MRI images
	private ParamObject<int[][]> refIndex;		// Indecies for reference images
	private ParamFloat maskThresh;				// Threshold for mask

	// Output params
	private ParamFileCollection outputFiles;	// Cropped 3D volumes
	private ParamFile outputMask;				// Cropped mask

	// CVS info
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Process 3D MRI Images");
		inputParams.setName("Process_3D_MRI_Images");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.setDescription("Takes in 3D MRI images, creates a mask for it, divides the images by sref, and crops the mask and images.");
		info.setLongDescription("");
		info.setAffiliation("Johns Hopkins University");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(inputFiles=new ParamFileCollection("Input Volume Files",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(refIndex=new ParamObject<int[][]>("Indecies of Reference Volumes", new ArrayIntegerTxtReaderWriter()));
		inputParams.add(maskThresh=new ParamFloat("Mask Threshold",(float)70));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outputFiles = new ParamFileCollection("Output Volumes",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		outputParams.add(outputMask = new ParamFile("Output Mask",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ArrayList<File> outVolFiles = new ArrayList<File>();
		ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();
		ImageDataFloat croppedVol=null;
		ImageDataFloat refVol=null;
		ImageDataUByte mask=null;
		ImageDataUByte croppedMask=null;
		int rows=-1, cols=-1, slices=-1;
		int xmin=-1, xmax=-1, ymin=-1, ymax=-1, zmin=-1, zmax=-1;
		int[] index=refIndex.getObject()[0];
		int numRef=index.length;

		// Create and crop the mask and get sref
		int numVols=inputFiles.getValue().size();
		float thresh=maskThresh.getFloat();
		for (int n=0; n<numVols; n++) {
			ImageData vol=rw.read(inputFiles.getValue(n));
			System.out.flush();
			if (n==0) {
				rows=vol.getRows();
				cols=vol.getCols();
				slices=vol.getSlices();
				mask=new ImageDataUByte(rows,cols,slices);
				refVol=new ImageDataFloat(rows,cols,slices);
			}
			for (int i=0; i<rows; i++) {
				for (int j=0; j<cols; j++) {
					for (int k=0; k<slices; k++) {
						for (int l=0; l<numRef; l++)
							if (n==index[l])
								refVol.set(i,j,k,refVol.getFloat(i,j,k)+vol.getFloat(i,j,k)/numRef);
						if (vol.getFloat(i,j,k)>=thresh) {
							mask.set(i,j,k,1);
							if (xmin==-1) xmin=xmax=i;
							else if (i<xmin) xmin=i;
							else if (i>xmax) xmax=i;
							if (ymin==-1) ymin=ymax=j;
							else if (j<ymin) ymin=j;
							else if (j>ymax) ymax=j;
							if (zmin==-1) zmin=zmax=k;
							else if (k<zmin) zmin=k;
							else if (k>zmax) zmax=k;
						}
					}
				}
			}
			vol.dispose();
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"xmin: "+xmin+" xmax: "+xmax);
		System.out.println(getClass().getCanonicalName()+"\t"+"ymin: "+ymin+" ymax: "+ymax);
		System.out.println(getClass().getCanonicalName()+"\t"+"zmin: "+zmin+" zmax: "+zmax);
		System.out.flush();

		croppedMask=new ImageDataUByte((xmax-xmin+1),(ymax-ymin+1),(zmax-zmin+1));
		croppedMask.setName("mask_volume");
		croppedMask.getHeader().setSliceThickness(0);
		for (int i=xmin; i<xmax; i++)
			for (int j=ymin; j<ymax; j++)
				for (int k=zmin; k<zmax; k++)
					croppedMask.set((i-xmin),(j-ymin),(k-zmin),mask.getUByte(i,j,k));
		mask.dispose();
		File fMask=rw.write(croppedMask,this.getOutputDirectory());
		outputMask.setValue(fMask);

		// Crop each volume and divide by sref
		for (int n=0; n<numVols; n++) {
			ImageData vol=rw.read(inputFiles.getValue(n));
			croppedVol=new ImageDataFloat(vol.getName(),(xmax-xmin+1),(ymax-ymin+1),(zmax-zmin+1));
			croppedVol.setName(vol.getName()+"_att_cropped");
			croppedVol.setHeader(vol.getHeader());
			System.out.flush();
			for (int i=xmin; i<xmax; i++)
				for (int j=ymin; j<ymax; j++)
					for (int k=zmin; k<zmax; k++)
						if (croppedMask.getUByte((i-xmin),(j-ymin),(k-zmin))>0) {
							float s=0;
							if (refVol.getFloat(i,j,k)!=0)
								s=vol.getFloat(i,j,k)/refVol.getFloat(i,j,k);
							croppedVol.set((i-xmin),(j-ymin),(k-zmin),s);
						}

			File fVol=rw.write(croppedVol,this.getOutputDirectory());
			outVolFiles.add(fVol);
			vol.dispose();
			croppedVol.dispose();
		}
		refVol.dispose();
		croppedMask.dispose();
		outputFiles.setValue(outVolFiles);
	}
}
