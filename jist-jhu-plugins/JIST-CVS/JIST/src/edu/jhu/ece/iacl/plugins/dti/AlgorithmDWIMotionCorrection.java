package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.dti.DTIGradientTableCreator;
import edu.jhu.ece.iacl.algorithms.dti.DWICoReg;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleListTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringArrayXMLReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import gov.nih.mipav.model.file.FileInfoBase;


public class AlgorithmDWIMotionCorrection {
		
	//output params
	private ParamVolume dwiMotionCorr;
	private ParamVolume structOut;
	private ParamVolume meanb0;
	private ParamObject<ArrayList<double[][]>> DWItransformationMatrices;
	private ParamObject<double[][]> StrucTransformation;
	
	//input params
	private ParamVolumeCollection dwis;
	private ParamVolume struc;
	
//	private ParamBoolean geomCorr;
//	private ParamBoolean resampDwi;
//	private ParamFile vabraconfig;
	private ParamOption RegMeth;
	private ParamOption RegTgt;
	
	private ParamObject<String> b0vals;
	
	private final StringArrayXMLReaderWriter saxml = new StringArrayXMLReaderWriter();
	
	private static final String rcsid =
		"$Id: AlgorithmDWIMotionCorrection.java,v 1.4 2009/10/29 17:28:46 bennett Exp $";
	private static final String cvsversion =
		"$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");

	
	protected void createInputParameters(ParamCollection inputParams) {

		inputParams.add(dwis=new ParamVolumeCollection("DWI Files",null,-1,-1,-1,-1));
		inputParams.add(struc=new ParamVolume("Structural File",null,-1,-1,-1,1));
		struc.setMandatory(false);
		
		String[] regmeth = {"FLIRT","RADAR","No Motion Correction"};
		inputParams.add(RegMeth = new ParamOption("Registration Method",regmeth));
		RegMeth.setValue(1);
		RegMeth.setDescription("");
		
		String[] regtgt = {"First B0", "Structural Volume"};
		inputParams.add(RegTgt = new ParamOption("Registration Target",regtgt));
		
		inputParams.add(b0vals = new ParamObject<String>("b0 Values", new StringReaderWriter()));
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setName("dwiMotionCorrect");
		inputParams.setLabel("DWI Motion Correction v"+revnum);
			
//		AlgorithmInformation info=getAlgorithmInformation();
//		info.setWebsite("");
//		info.add(new AlgorithmAuthor("John Bogovic & Bennett Landman","landman@jhu.edu","http://sites.google.com/site/bennettlandman/"));
//		info.setDescription("Motion Correction of Diffusion-Weighted Images");
//		info.setAffiliation("Johns Hopkins University, Departments of Electrical aFnd Biomedical Engineering");	
//		info.setVersion(revnum);	
//		info.setStatus(DevelopmentStatus.BETA);
	}
	protected void createOutputParameters(ParamCollection outputParams) {		
//		dwiMotionCorr = new ParamVolumeCollection("Output Diffusion-weighted Volumes");
		dwiMotionCorr = new ParamVolume("Output Diffusion-weighted Volumes",null,-1,-1,-1,-1);
		outputParams.add(dwiMotionCorr);
		structOut = new ParamVolume("Output Structural Volume");
		outputParams.add(structOut);
		structOut.setMandatory(false);
		meanb0 = new ParamVolume("Mean b0");
		outputParams.add(meanb0);
		meanb0.setMandatory(false);
		DWItransformationMatrices = new ParamObject<ArrayList<double[][]>>("DWI Transformation Matrices", new ArrayDoubleListTxtReaderWriter());
		StrucTransformation = new ParamObject<double[][]>("Structural Transformation Matrix", new ArrayDoubleTxtReaderWriter());
		outputParams.add(DWItransformationMatrices);
		outputParams.add(StrucTransformation);
	}
	
	protected void execute(CalculationMonitor monitor) {
		
		
		
//		ArrayList<ImageData> dwvols = extractVols();
//		System.out.println(getClass().getCanonicalName()+"\t"+"Num extracted volumes: "+ dwvols.size());
		
		ImageData newstruc=null;
		ArrayList<ArrayList<ImageData>> dwout;
		
		//Initialize transformation matrices
		ArrayList<double[][]> translist = new ArrayList<double[][]>();
		double[][] structran = new double[][]{{1,0,0},{0,1,0},{0,0,1}};
		for(int i=0; i<dwis.getImageDataList().get(0).getComponents(); i++){
			translist.add(new double[][]{{1,0,0},{0,1,0},{0,0,1}});
		}
		
		ImageData b0;
		
		ArrayList<String> strb0 = saxml.readStrings(b0vals.getObject());
		String[] sab0= new String[strb0.size()];
		for(int i=0; i<strb0.size(); i++){
			sab0[i]=strb0.get(i);
		}
//		System.out.println(getClass().getCanonicalName()+"\t"+sab0[0]);
		int[] indexb0 = getFirstB0index(sab0);
		
		if(RegMeth.getValue().equals("FLIRT") && dwis.getImageDataList().size()==1){
			dwout = new ArrayList<ArrayList<ImageData>>(1);
			dwout.add(extractVols(dwis.getImageDataList().get(0)));
			System.out.println(getClass().getCanonicalName()+"\t"+"Num extracted volumes: "+ dwout.size());
//			dwis.dispose(); //jb 2008.10.27
			if(struc.getValue()!=null){
				newstruc = struc.getImageData();
			}
			
			DWICoReg coreg = new DWICoReg(dwout.get(0),struc.getImageData(), RegTgt.getValue(), indexb0);
			coreg.setRegMeth(RegMeth.getValue());
			coreg.setNDWb0(getNumVolsfromb0(sab0));
			dwout=coreg.run();
			if(struc.getValue()!=null){
				coreg.regStrucToB0(indexb0[0]);
				newstruc=coreg.getStruc();
			}else{
				newstruc = null;
//				newstruc = new ImageDataMipav("nodata",VoxelType.BOOLEAN, 1,1,1,1);
			}
			
			translist=coreg.getDWITransform();
			structran=coreg.getStrucTransform();
			b0 = dwout.get(0).get(indexb0[0]);
			
		}else if(RegMeth.getValue().equals("RADAR")){
			ArrayList<ArrayList<ImageData>> dwvols = extractAllVols(dwis.getImageDataList());
			System.out.println(getClass().getCanonicalName()+"\t"+"Num extracted volumes: "+ dwvols.size());
//			dwis.dispose(); //jb 2008.10.27
			if(struc.getValue()!=null){
				newstruc = struc.getImageData();
			}
			
			DWICoReg coreg = new DWICoReg(dwvols,struc.getImageData(), RegTgt.getValue(), indexb0,true);
			coreg.setRegMeth(RegMeth.getValue());
			coreg.setNDWb0(getNumVolsfromb0(sab0));
			
			dwout=coreg.run();
			if(struc.getValue()!=null){
				coreg.regStrucToB0_rdr(indexb0[0]);
				newstruc=coreg.getStruc();
			}else{
//				newstruc = null;
				newstruc = new ImageDataMipav("nodata",VoxelType.BOOLEAN, 1,1,1,1);
			}
			translist=coreg.getDWITransform();
			structran=coreg.getStrucTransform();
			b0=coreg.getb0();
		}else if(RegMeth.equals("No Motion Correction")){
			System.err.println("jist.plugins"+"No motion correction selected.");
			dwout=null;
			b0=null;
			newstruc = new ImageDataMipav("noStrucHere",VoxelType.BOOLEAN,1,1,1,1);
		}else{
			System.err.println("jist.plugins"+"Invalid Registration Method.  No motion correction applied.");
			dwout=null;
			b0=null;
			newstruc = new ImageDataMipav("noStrucHere",VoxelType.BOOLEAN,1,1,1,1);
		}
		

		
		System.out.println(getClass().getCanonicalName()+"\t"+translist);
		System.out.println(getClass().getCanonicalName()+"\t"+structran);
		
		dwiMotionCorr.setValue(concatAllVols(dwout,dwout.get(0).get(0).getModelImageCopy().getFileInfo()));
		structOut.setValue(newstruc);
		
		
//		ArrayList<File> filesout = writeTransformations(translist,"/iacl/cruise08/maps_test/test_cases/CATNAP_java/transformations");
		
		ArrayList<ParamObject<String>> filesout = new ArrayList<ParamObject<String>>();
		int i=0;
		for(double[][] a : translist){
			ParamObject<String> aob = new ParamObject<String>(dwis.getImageDataList().get(0).getName()+"_Transformation_"+i, new StringReaderWriter());
			aob.setObject(DTIGradientTableCreator.tableToString(a));
		}
		StrucTransformation.setObject(structran);
		StrucTransformation.setFileName("Transformation_Stuctural");
		
		meanb0.setObject(b0);
		meanb0.setFileName("Meanb0");
		
		DWItransformationMatrices.setObject(translist);
		DWItransformationMatrices.setFileName("Transformations_DWIs");
		
	}
	
	/*
	 * BENNETT LOOK HERE
	 */
	private ArrayList<ArrayList<ImageData>> extractAllVols(List<ImageData> allvols){
		ArrayList<ArrayList<ImageData>> dataout = new ArrayList<ArrayList<ImageData>>(allvols.size());
		for(ImageData img : allvols){
			dataout.add(extractVols(img));
		}
		
		//Never use the input volumes again after this conversion.
		allvols=null;
		
		System.out.println(getClass().getCanonicalName()+"\t"+"in extract all");
		System.out.println(getClass().getCanonicalName()+"\t"+dataout.size());
		System.out.println(getClass().getCanonicalName()+"\t"+dataout.get(0).size());
		return dataout;
	}
	
	/*
	 * BENNETT LOOK HERE
	 */
	private ArrayList<ImageData> extractVols(ImageData dwi){
		
		// get necessary orientation/resolution information 
		
//		float[] res = dwi.getModelImage().getResolutions(0);		//resolution
//		int	imgOrient = dwi.getModelImage().getImageOrientation();	//Image Orientation (tra,sag,cor)
//		int[] axOrient = dwi.getModelImage().getAxisOrientation();  //Axis Orientation
		
		FileInfoBase[] finfo = dwi.getModelImageCopy().getFileInfo();
		FileInfoBase[] fcat = new FileInfoBase[dwi.getSlices()];
		for(int i=0; i<fcat.length; i++){
			fcat[i]=finfo[i];
		}
		
		ArrayList<ImageData> listout = new ArrayList<ImageData>(dwi.getComponents());
		
        for(int l=0; l<dwi.getComponents(); l++){
        	ImageDataMipav nextvol = new ImageDataMipav(dwis.getName()+"_Volume"+l,VoxelType.FLOAT,dwi.getRows(),
        			dwi.getCols(), dwi.getSlices());
        	
//        	dwi.getModelImage().setResolutions(0, res);
//        	dwi.getModelImage().setImageOrientation(imgOrient);
        	nextvol.getModelImageCopy().setFileInfo(fcat);
        	
        	for(int i=0;i<nextvol.getRows();i++){
        		for(int j=0;j<nextvol.getCols();j++){
        			for(int k=0;k<nextvol.getSlices();k++){
        				nextvol.set(i, j, k, dwi.get(i, j, k, l).floatValue());
        			}
        		}
        	}   
        	System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
        	listout.add(nextvol);
        }
        listout.trimToSize();
        return listout;
	}
	
	private ArrayList<ImageDataMipav> concatVols(ArrayList<ArrayList<ImageData>> biglist, FileInfoBase[] info){

		ArrayList<ImageDataMipav> listout = new ArrayList<ImageDataMipav>(biglist.size());
		
		for(ArrayList<ImageData> list : biglist){

			ImageDataMipav volout = new ImageDataMipav("DWI_MotionCorr",list.get(0).getType(),list.get(0).getRows(),
					list.get(0).getCols(),list.get(0).getSlices(), list.size());

			//Set info
			FileInfoBase[] newinfo = new FileInfoBase[volout.getSlices()*volout.getComponents()];
			for(int m=0; m<newinfo.length; m++){
				newinfo[m]=info[0];
			}

			volout.getModelImageCopy().setFileInfo(newinfo);

			for(int l=0; l<list.size(); l++){
				for(int i=0;i<volout.getRows();i++){
					for(int j=0;j<volout.getCols();j++){
						for(int k=0;k<volout.getSlices();k++){
							volout.set(i, j, k,l,list.get(l).getDouble(i, j, k));
						}
					}
				}   
			}
			
			listout.add(volout);
			
		}
		
        return listout;
	}
	
	private ImageDataMipav concatAllVols(ArrayList<ArrayList<ImageData>> biglist, FileInfoBase[] info){

		int N=0;
		for(ArrayList<ImageData> smalllist: biglist){
			N+=smalllist.size();
		}
		int M = biglist.get(0).size();
		ImageDataMipav volout = new ImageDataMipav("DWI_MotionCorr",biglist.get(0).get(0).getType(),biglist.get(0).get(0).getRows(),
				biglist.get(0).get(0).getCols(),biglist.get(0).get(0).getSlices(), N);

		int n = 0;
		for(ArrayList<ImageData> list : biglist){
			
			//Set info
			FileInfoBase[] newinfo = new FileInfoBase[volout.getSlices()*volout.getComponents()];
			for(int m=0; m<newinfo.length; m++){
				newinfo[m]=info[0];
			}

			volout.getModelImageCopy().setFileInfo(newinfo);

			for(int l=0; l<list.size(); l++){
				for(int i=0;i<volout.getRows();i++){
					for(int j=0;j<volout.getCols();j++){
						for(int k=0;k<volout.getSlices();k++){
							volout.set(i, j, k,(n*M)+l,list.get(l).getDouble(i, j, k));
						}
					}
				}   
			}
			n++;
			
		}
		
        return volout;
	}
	
	private ArrayList<File> writeTransformations(String name, ArrayList<double[][]> translist, String dir){
		ArrayDoubleTxtReaderWriter tranWriter = ArrayDoubleTxtReaderWriter.getInstance();
		ArrayList<File> filesout = new ArrayList<File>(translist.size());
		int i=0;
		for(double[][] tran : translist){
			File ithtran = new File(dir+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name)+"_Transformation_"+i+".txt");
			filesout.add(ithtran);
			tranWriter.write(tran, ithtran); 
			i++;
		}
		return filesout;
	}
	
	private String writeMultiTransforms(ArrayList<double[][]> translist){
		String alltrans = "";
		int k=1;
		for(double[][] a : translist){
			for(int i=0;i<a.length;i++){
				for(int j=0;j<a[i].length;j++){
					alltrans=alltrans+a[i][j]+" ";
				} 
				alltrans=alltrans+"\n";
			}
			alltrans=alltrans+"***** Matrix "+k+"*****\n";
			k++;
		}
		
		return alltrans;
	}

	private int[] getFirstB0index(String[] bvals){
		int N = dwis.getValue().size();
		int[] ind = new int[N];
		int j = 0;
		String ithb = "";
		for(int k=0; k<N; k++){
			ithb = "";
//			System.out.println(getClass().getCanonicalName()+"\t"+"&&");
//			System.out.println(getClass().getCanonicalName()+"\t"+bvals[k]);
//			System.out.println(getClass().getCanonicalName()+"\t"+"&&");
			/*
			 * Treat each list of bvalues seperately 
			 */
			for(int i=0; i<bvals[k].length(); i++){
				if(bvals[k].charAt(i)=='\n'){
					System.out.println(getClass().getCanonicalName()+"\t"+ithb);
					if(Float.parseFloat(ithb)==0){
						ind[k] = j;
						break;
					}
					ithb="";
					j++;
				}else{
					ithb=ithb+bvals[k].charAt(i);
				}
			}
		}
		
		return ind;
	}
	
	private int[] getNumVolsfromb0(String[] bvals){
		int N = dwis.getValue().size();
		int[] Nb0 = new int[N];
		int j = 0;
		String ithb = "";
		for(int k=0; k<N; k++){
			/*
			 * Treat each list of bvalues seperately 
			 */
			for(int i=0; i<bvals[k].length(); i++){
				if(bvals[k].charAt(i)=='\n'){
					j++;
				}
			}
			Nb0[k]=j;
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"Found " +Nb0[0] + " volumes in the first dwi");
		return Nb0;
	}

}
