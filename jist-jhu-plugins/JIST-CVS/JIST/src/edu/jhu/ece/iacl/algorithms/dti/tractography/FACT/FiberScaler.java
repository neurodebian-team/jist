package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.IOException;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;

public class FiberScaler {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String dirname = "/home/john/Research/DTIcompressedSensing/figure4paper/";
		
//		String fibersin = dirname+"CFARI_cst2.dat";
//		String fibersout = dirname+"CFARI_cst2_scaled.dat";
		
//		String fibersin = dirname+"FACT_3_cc2.dat";
//		String fibersout = dirname+"FACT_3_cc2_scaled.dat";
		
		String fibersin = dirname+"FACT_3_cst2.dat";
		String fibersout = dirname+"FACT_3_cst2_scaled.dat";
		
		System.out.println("jist.plugins"+"\t"+"Reading...");
		DTIStudioReader reader = new DTIStudioReader();
		Fiber[] in = null;
		try{
			reader.read(fibersin);
			in = reader.getFibers();
		}catch(IOException e){
			e.printStackTrace();
		}
		System.out.println("jist.plugins"+"\t"+"Scaling...");
		Fiber[] out = Fiber.scaleForVisualization(in, reader.hdr.VoxelSize[0], reader.hdr.VoxelSize[1], reader.hdr.VoxelSize[2]);
		out = Fiber.makeFibersInBounds(out, reader.hdr.ImageSize[0], reader.hdr.ImageSize[1], reader.hdr.ImageSize[2]);
//		ArrayPrinter.printFiberArray(out);
		System.out.println("jist.plugins"+"\t"+"Writing...");
		DTISFiberWriter.writeFibers(out, fibersout, reader.getHDR(),reader.numread);
		System.out.println("jist.plugins"+"\t"+"Done");

	}

}
