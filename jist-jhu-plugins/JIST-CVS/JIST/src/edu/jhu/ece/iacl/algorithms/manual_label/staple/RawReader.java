package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class RawReader {
	
	public static int[][][] readByteImg(String filename, int[] dim){
		
		int[][][] img = new int[dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			for(int k=0; k<dim[2];k++){
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=1; j<=dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][dim[1]-j][k]=imin.readByte();
						
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static int[][][] readUSByteImgB(String filename, int[] dim){
		
		int[][][] img = new int[dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			for(int k=0; k<dim[2];k++){
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=1; j<=dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][dim[1]-j][k]=(int)(imin.readByte() & 0xFF);
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static int[][][] readUSByteImgF(String filename, int[] dim){
		
		int[][][] img = new int[dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			for(int k=0; k<dim[2];k++){
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=0; j<dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][j][k]=(int)(imin.readByte() & 0xFF);
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static int[][][] readIntImg(String filename, int[] dim) {
		
		int[][][] img = new int[dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			for(int k=0; k<dim[2];k++){
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=1; j<=dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][dim[1]-j][k]=imin.readInt();
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static int[][] readIntImg2d(String filename, int[] dim) {
		
		int[][] img = new int[dim[0]][dim[1]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=1; j<=dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][dim[1]-j]=imin.readInt();
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static float[][][] readFloatImgB(String filename, int[] dim) {
		
		float[][][] img = new float[dim[0]][dim[1]][dim[1]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));


			for(int k=0; k<dim[2];k++){
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=1; j<=dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][dim[1]-j][k]=imin.readFloat();
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static float[][][] readFloatImgF(String filename, int[] dim) {
		
		float[][][] img = new float[dim[0]][dim[1]][dim[1]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));

			for(int k=0; k<dim[2];k++){
				for(int j=0; j<dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[i][j][k]=imin.readFloat();
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static float[][][][] readFloatVecImgB(String filename, int[] dim) {
		
		float[][][][] img = new float[3][dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));

			for(int k=0; k<dim[2];k++){
//				for(int j=0; j<ahdr.dim[2];j++){
				for(int j=1; j<=dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[0][i][dim[1]-j][k]=imin.readFloat();
						img[1][i][dim[1]-j][k]=imin.readFloat();
						img[2][i][dim[1]-j][k]=imin.readFloat();
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static float[][][][] readFloatVecImg(String filename, int[] dim) {
		
		float[][][][] img = new float[3][dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));

			for(int k=0; k<dim[2];k++){
				for(int j=0; j<dim[1];j++){
					for(int i=0; i<dim[0];i++){
						img[0][i][j][k]=imin.readFloat();
						img[1][i][j][k]=imin.readFloat();
						img[2][i][j][k]=imin.readFloat();
//						System.out.println(getClass().getCanonicalName()+"\t"+"At (" +(i+1)+","+(j+1)+","+(k+1)+") is: " + img[0][i][j][k]);
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
	
	public static float[][][][] readFloatVecImgMIPAV(String filename, int[] dim) {
		
		float[][][][] img = new float[3][dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			for(int l=0; l<3; l++){
				for(int k=0; k<dim[2];k++){
//					for(int j=0; j<ahdr.dim[2];j++){
					for(int j=1; j<=dim[1];j++){
						for(int i=0; i<dim[0];i++){
							img[l][i][dim[1]-j][k]=imin.readFloat();
//							img[1][i][dim[1]-j][k]=imin.readFloat();
//							img[2][i][dim[1]-j][k]=imin.readFloat();
//							System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
						}
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}	
	
	public static float[][][][] readFloatVecImgMIPAVCorrect(String filename, int[] dim) {
		
		float[][][][] img = new float[3][dim[0]][dim[1]][dim[2]];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			for(int l=0; l<3; l++){
				for(int i=0; i<dim[0];i++){
//					for(int j=0; j<dim[1];j++){
					for(int j=1; j<=dim[1];j++){
						for(int k=0; k<dim[2];k++){
//							img[0][dim[0]-i][dim[1]-j][dim[2]-k]=imin.readFloat();
//							img[1][dim[0]-i][dim[1]-j][dim[2]-k]=imin.readFloat();
//							img[2][dim[0]-i][dim[1]-j][dim[2]-k]=imin.readFloat();

//							img[0][i][j][k]=imin.readFloat();
//							img[1][i][j][k]=imin.readFloat();
//							img[2][i][j][k]=imin.readFloat();

							img[l][i][dim[1]-j][k]=imin.readFloat();
							
//							System.out.println(getClass().getCanonicalName()+"\t"+"At (" +i+","+j+","+k+") is: " + img[j][i][k]);
						}
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}	
	

}
