package edu.jhu.ece.iacl.algorithms.dti.tractography;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;


public class FiberComparator {
	
	// Computes distance 
	public static double meanDistCorr(Fiber a, Fiber b, double resX, double resY, double resZ){
		int aind = a.getseedPointIndex();
		int bind = b.getseedPointIndex();
		XYZ[] achain = a.getXYZChain();
		XYZ[] bchain = b.getXYZChain();
		double meanDist=0;
		
		int fwdnum = Math.min(achain.length-aind, bchain.length-bind);
		//Compute forward distances
		//include seed
//		System.out.println("jist.plugins"+"\t"+achain.length);
//		System.out.println("jist.plugins"+"\t"+aind);
		for(int i=0; i<fwdnum; i++){
			XYZ pta = achain[aind+i].deepClone();
			XYZ ptb = bchain[bind+i].deepClone();
			pta.scale(resX, resY, resZ);
			ptb.scale(resX, resY, resZ);
			meanDist=meanDist+pta.distance(ptb);
		}
		
		
		int revnum = Math.min(aind, bind);
		//Compute reverse distances
		//don't include seed
		for(int j=0; j<=revnum; j++){
			XYZ pta = achain[aind-j].deepClone();
			XYZ ptb = bchain[bind-j].deepClone();
			pta.scale(resX, resY, resZ);
			ptb.scale(resX, resY, resZ);
			meanDist=meanDist+pta.distance(ptb);
		}
		meanDist = meanDist/(fwdnum+revnum);
		
		return meanDist;
	}
	
	public static double maxDist(Fiber a, Fiber b, double resX, double resY, double resZ){
		int aind = a.getseedPointIndex();
		int bind = b.getseedPointIndex();
		XYZ[] achain = a.getXYZChain();
		XYZ[] bchain = b.getXYZChain();
		double maxdist=-1;
		
		int fwdnum = Math.min(achain.length-aind, bchain.length-bind);
		//Compute forward distances
		//include seed
//		System.out.println("jist.plugins"+"\t"+achain.length);
//		System.out.println("jist.plugins"+"\t"+aind);
		
		for(int i=0; i<fwdnum; i++){
			XYZ pta = achain[aind+i].deepClone();
			XYZ ptb = bchain[bind+i].deepClone();
			pta.scale(resX, resY, resZ);
			ptb.scale(resX, resY, resZ);
			if(pta.distance(ptb)>maxdist){
				maxdist=pta.distance(ptb);
			}
			
		}
		
		int revnum = Math.min(aind, bind);
		//Compute reverse distances
		//don't include seed
		for(int j=0; j<=revnum; j++){
			XYZ pta = achain[aind-j].deepClone();
			XYZ ptb = bchain[bind-j].deepClone();
			pta.scale(resX, resY, resZ);
			ptb.scale(resX, resY, resZ);
			if(pta.distance(ptb)>maxdist){
				maxdist=pta.distance(ptb);
			}
		}
		return maxdist;
	}
	
	// positive if length(a)>length(b)
	public static int lengthDiffPt(Fiber a, Fiber b){
		return a.getXYZChain().length-b.getXYZChain().length;
	}
	
	//postive if length(a)>length(b)
	public static double lengthDiffMM(Fiber a, Fiber b, double resX, double resY, double resZ){
//		System.out.println("jist.plugins"+"\t"+"Length a: " + a.length(resX, resY, resZ));
//		System.out.println("jist.plugins"+"\t"+"Length b: " + b.length(resX, resY, resZ));
		return a.length(resX, resY, resZ)-b.length(resX, resY, resZ);
	}
	

	
}
