package edu.jhu.ece.iacl.algorithms.dti.tractography;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.geom.PT;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

public class FiberStatistics {
   
    private final static double SR2 = Math.sqrt(2);
   
    //REQUIRED PARAMETERS
    Vector<Fiber> fibers;
   
    //OPTIONAL PARAMETERS
    double resX=-1;
    double resY=-1;
    double resZ=-1;
    boolean setres;
   
    //local measures
    Vector<Vector<Double>> angle;
    Vector<Vector<Double>> curvature;
    ArrayList<Vector<Double>> fromvolume;
    ArrayList<XYZ[]> direction;
   
    //global measures
    ArrayList<Double> lengths;
    ArrayList<Integer> numPts;
    ArrayList<Double> sumfromvolume;
    ArrayList<Double> avgfromvolume;
    ArrayList<Double> maxfromvolume;
    ArrayList<Double> minfromvolume;
    ArrayList<Double> medianfromvolume;
    
    //other measures
    ArrayList<GridPt> tractset;
    public FiberStatistics(){
    }
    public FiberStatistics(Vector<Fiber> fibers){
        setFibers(fibers);
    }
    public FiberStatistics(FiberCollection fibers){
        setFibers(fibers);
    }
    public void setFibers(Vector<Fiber> fibers){
        this.fibers=fibers;
    }
    public void setFibers(FiberCollection fibers){
        this.fibers=fibers;
        System.out.println("jist.plugins"+"\t"+"fibs: " +fibers);
        System.out.println("jist.plugins"+"\t"+"fib dims: " +fibers.getDimensions());
        resX = fibers.getDimensions().x;
        resY = fibers.getDimensions().y;
        resZ = fibers.getDimensions().z;
    }
    public void setRes(double resX, double resY, double resZ){
        this.resX=resY;
        this.resY=resY;
        this.resZ=resZ;
        setres = true;
    }
    public Point3f getRes(){
    	if(resX>-1 && resX>-1 && resX>-1){
    		return new Point3f((float)resX,(float)resY,(float)resZ);
    	}else{
    		return null;
    	}
    	
    }
    
    /*  REMOVE THIS METHOD FOR NOW
     * 
    public void computeCurvature(){
        angle 		= new Vector<Vector<Double>>(fibers.size());
        curvature 	= new Vector<Vector<Double>>(fibers.size());
       
        int i = 0;
        for(Fiber f : fibers){
        	curvature.set(i, f.computeCurvature());
        	angle.set(i, f.computeAngles());
        	i++;
        }
        
       
    }
    */
   
    public void computeDirection(){
        direction = new ArrayList<XYZ[]>(fibers.size());
        for(Fiber f : fibers){
        	XYZ[] chain = f.getXYZChain();
            XYZ[] dirlist = new XYZ[chain.length];
        	dirlist[0]=new XYZ(chain[0].x-chain[1].x, chain[0].y-chain[1].y, chain[0].z-chain[1].z);
            for(int i=1; i<f.getXYZChain().length-1; i++){
               
            }
        	direction.add(dirlist);
        }
       
    }
   
    public void computeLengths(){
        lengths = new ArrayList<Double>(fibers.size());
        if(setres){
            for(Fiber f : fibers){
                double l = 0;
                double seglen = 0;
                XYZ[] chain = f.getXYZChain();
                for(int i=0; i<f.getXYZChain().length-1; i++){
                	seglen = xyzLen(chain[i],chain[i+1],resX,resY,resZ);
                	if(Double.isNaN(seglen) && l>0){
                		break;
                	}else if(Double.isNaN(seglen) && l<=0){
                		//do nothing
                	}else{
                		 l=l+seglen;
                	}
                }
                
                if(l==0){
                	System.out.println("jist.plugins"+"\t"+f);
                }
                lengths.add(new Double(l));
            }
        }else{
            for(Fiber f : fibers){
                double l = 0;
                double seglen = 0;
                XYZ[] chain = f.getXYZChain();
                for(int i=0; i<f.getXYZChain().length-1; i++){
                	seglen = xyzLen(chain[i],chain[i+1]);
                	if(Double.isNaN(seglen) && l>0){
                		break;
                	}else if(Double.isNaN(seglen) && l<=0){
                		//do nothing
                	}else{
                		 l=l+seglen;
                	}
                }
                if(l==0){
                	System.out.println("jist.plugins"+"\t"+f);
                }
                lengths.add(new Double(l));
            }
        }
       
    }
    private double xyzLen(XYZ p, XYZ q){
        return Math.sqrt((p.x-q.x)*(p.x-q.x) + (p.y-q.y)*(p.y-q.y)+ (p.z-q.z)*(p.z-q.z));
    }
    private double xyzLen(XYZ p, XYZ q, double resX, double resY, double resZ){
        return SR2*Math.sqrt(resX*(p.x-q.x)*(p.x-q.x) + resY*(p.y-q.y)*(p.y-q.y)+ resZ*(p.z-q.z)*(p.z-q.z));
    }
   
    public void computeNumPts(){
        numPts = new ArrayList<Integer>(fibers.size());
        for(Fiber f : fibers){
            numPts.add(new Integer(f.getXYZChain().length));
        }
       
    }
    
    public ArrayList<GridPt> getTractVoxSet(){
        ArrayList<GridPt> tractvoxset = new ArrayList<GridPt>();

        for(Fiber f : fibers){
            for(XYZ p : f.getXYZChain()){
                tractvoxset.add(new GridPt(Math.round(p.x), Math.round(p.y), Math.round(p.z)));
            }
        }
        tractset = tractvoxset;
        return tractvoxset;
    }
   
    public double computeVolumeAverage(ImageDataMipav vol){
        double volstat = 0;
        if(tractset==null){
            getTractVoxSet();
        }
        for(GridPt p : tractset){
            volstat = volstat + vol.getDouble(p.x, p.y, p.z);
        }
        return volstat;
    }
    
    public void findTractVolumeStats(ImageData vol){
    	int rows = vol.getRows();
    	int cols = vol.getCols();
    	int slcs = vol.getSlices();
    	fromvolume = new ArrayList<Vector<Double>>();
    	sumfromvolume = new ArrayList<Double>();
    	avgfromvolume = new ArrayList<Double>();
    	maxfromvolume = new ArrayList<Double>();
    	minfromvolume = new ArrayList<Double>();
    	medianfromvolume = new ArrayList<Double>();
    	Vector<Double> list = new Vector<Double>();
    	Vector<Double> sorted = new Vector<Double>();
    	double max=0;
    	double min=0;
    	double sum=0;
    	double med=0;
    	int[] inbndpt;
    	for(Fiber f: fibers){
//    		double[] fiblist = new double[f.getXYZChain().length];
    		list = new Vector<Double>(f.getXYZChain().length);
    		sorted = new Vector<Double>(f.getXYZChain().length);
    		int i=0;
    		max = Double.MIN_VALUE;
    		min = Double.MAX_VALUE;
    		sum = 0;
    		med = Double.MIN_VALUE;
    		for(XYZ p: f.getXYZChain()){
    			GridPt pt = new GridPt(p);
    			inbndpt = GridPt.roundInBounds(pt.x,pt.y,pt.z,rows,cols,slcs);
//    			fiblist[i] = vol.getDouble(pt.x, pt.y, pt.z);
    			list.add(vol.getDouble(inbndpt[0],inbndpt[1],inbndpt[2]));
    			sorted.add(list.get(i));
    			
    			if(list.get(i)>max){
    				max = list.get(i);
    			}
    			if(list.get(i)<min){
    				min = list.get(i);
    			}
    			sum+=list.get(i);
    			i++;
    			Collections.sort(sorted);
    		}
    		
    		if(sorted.size()==1){
    			med = sorted.get(0);
    		}else if(sorted.size()<1){
    			med = Double.NaN;
    		}else{
    			if (sorted.size() % 2 == 1) {
    				med = sorted.get(sorted.size() / 2);
    			} else {
    				med = 0.5 * (sorted.get(sorted.size() / 2 - 1) + 
    						sorted.get(sorted.size() / 2));
    			}
    		}
    		
    		fromvolume.add(list);
    		sumfromvolume.add(new Double(sum));
    		avgfromvolume.add(new Double(sum/(f.getXYZChain().length)));
    		maxfromvolume.add(max);
    		minfromvolume.add(min);
    		medianfromvolume.add(med);
    	}
    }
    
    public ArrayList<Double> getSumFromVolume(){
    	return sumfromvolume;
    }
    public ArrayList<Double> getMeanFromVolume(){
    	return avgfromvolume;
    }
    public ArrayList<Double> getMaxFromVolume(){
    	return avgfromvolume;
    }
    public ArrayList<Double> getMinFromVolume(){
    	return minfromvolume;
    }
    public ArrayList<Double> getMedianFromVolume(){
    	return medianfromvolume;
    }
    public ArrayList<Double> getFiberLengths(){
    	return lengths;
    }
    
    public Vector<Fiber> filterTractByVolumeStatPercent(){
    	Vector<Fiber> out = new Vector<Fiber>();
    	
    	return out;
    }
    
    public Vector<Fiber> filterTractByVolumeStatThresh(ImageDataMipav vol, double thresh){
    	if(sumfromvolume==null){
    		findTractVolumeStats(vol);
    	}
    	Vector<Fiber> out = new Vector<Fiber>();
    	for(int i=0; i<sumfromvolume.size(); i++){
    		if(sumfromvolume.get(i).doubleValue()>thresh){
    			out.add(fibers.get(i));
    		}
    	}
    	return out;
    }
    
    public static Vector<Tract> segmentTractFromData(Vector<Fiber> fibers, double[][] data, double datthresh, int lengththresh){
    	Vector<Tract> out = new Vector<Tract>(data[0].length);
    	
    	if(data.length!=fibers.size()){
    		System.err.println("jist.plugins"+"Fiber number and data dimension must be compatible!");
    		return null;
    	}
    	System.out.println("jist.plugins"+"\t"+data.length);    //num of fibers
    	System.out.println("jist.plugins"+"\t"+data[0].length); //num labels
    	System.out.println("jist.plugins"+"\t");
    	
    	for(int k=0; k<data[0].length; k++){
    		out.add(new Tract());
    	}
    	
    	for(int i=0; i<data.length; i++){
    		int tractidx = -1;
    		double maxstat = Double.MIN_VALUE;
    		
    		for(int j=0; j<data[0].length; j++){
    			if(data[i][j]>datthresh && data[i][j]>maxstat){
        			tractidx=j;
        			maxstat = data[i][j];
        		}
    		}
    		
    		
    		if(fibers.get(i).getChain().length>=lengththresh && tractidx>-1){
    			out.get(tractidx).getFiberCollection().add(fibers.get(i));
    		}
    	}
    	
    	return out;
    }

    public static double[][] getFiberDataWithMax(Fiber f, ImageData mems){
    	double[][] out = new double[mems.getComponents()][2];
    	double max = -1;
    	int xmax = mems.getRows()-1;
    	int ymax = mems.getCols()-1;
    	int zmax = mems.getSlices()-1;
    	//for each point on the fibers
    	for(XYZ p: f.getXYZChain()){
    		double thismax = -1;
    		GridPt pt = new GridPt(p);
    		pt = pt.roundInBounds(xmax, ymax, zmax);
    		//for each component of the volume
    		for(int i=0; i<mems.getComponents(); i++){
    			
    			//get the image intensity at the fiber point
    			double val = mems.getDouble(pt.x, pt.y, pt.z,i);
    			out[i][0] += val;  //increment the first stat
    			if(val >thismax){
    				thismax = val;	//set max value
    			}
    		}
    		max+=thismax;
    		for(int i=0; i<mems.getComponents(); i++){
    			out[i][1] += thismax - out[i][0];
    		}
    	}
    	return out;
    }
    
    public static Vector<Tract> segmentTractFromMemberships(Vector<Fiber> fibers, ImageData mems, double datthresh, int lengththresh, double homogthresh){
    	
    	Vector<Tract>  out = new  Vector<Tract>();
     	for(int k=0; k<mems.getComponents(); k++){
    		out.add(new Tract());
    	}
     	
    	for(Fiber f: fibers){
    		double len = f.getXYZChain().length;
    		double[][] fdat = getFiberDataWithMax(f, mems);
    		int tractidx = -1;
    		double maxstat = Double.MIN_VALUE;
    		/*Find tract with maximum statistic*/
    		for(int i=0; i<fdat.length; i++){
    			if(fdat[i][0]>maxstat){
    				maxstat=fdat[i][0];
    				tractidx=i;
    			}
    		}
    		
    		/*Check data, homogeneity, and length criterion*/
    		if(tractidx>-1){
    			if(fdat[tractidx][0]>(datthresh*len) && fdat[tractidx][1]<homogthresh && len>lengththresh){
    				out.get(tractidx).getFiberCollection().add(f);
    			}
    		}
    	}
    	
    	return out;
    	
    }
    
    
    public ImageData fibers2Volume(String stat, ImageData template){
        ImageData fibstat;
        if(stat.equals("Count")){
            fibstat = new ImageDataMipav("FiberCount",VoxelType.INT, template.getRows(), template.getCols(), template.getSlices());
            fibstat.setHeader(template.getHeader());
            for(Fiber f : fibers){
                for(XYZ p : f.getXYZChain()){
                    fibstat.set(Math.round(p.x), Math.round(p.y), Math.round(p.z), fibstat.get(Math.round(p.x), Math.round(p.y), Math.round(p.z)).intValue()+1);
                }
            }
        }else if(stat.equals("MeanFA")){
            fibstat = new ImageDataMipav("MeanFA",VoxelType.FLOAT, template.getRows(), template.getCols(), template.getSlices());
        }else if(stat.equals("MeanADC")){
            fibstat = new ImageDataMipav("MeanADC",VoxelType.FLOAT, template.getRows(), template.getCols(), template.getSlices());
        }else if(stat.equals("LocalCurvature")){
            fibstat = new ImageDataMipav("LocalCurvature",VoxelType.FLOAT, template.getRows(), template.getCols(), template.getSlices());
        }else if(stat.equals("Mask")){
        	fibstat = new ImageDataInt(template.getRows(), template.getCols(), template.getSlices());
        	fibstat.setName("Mask");
        	System.out.println("jist.plugins"+"\t"+"Making mask");
        	for(Fiber f : fibers){
                for(XYZ p : f.getXYZChain()){
                    fibstat.set(Math.round(p.x), Math.round(p.y), Math.round(p.z), new Integer(1));
                }
            }
    	}else{
            fibstat=null;
        }
       
        return fibstat;
    }
    
    public void finalize(){
    	fibers=null;
    	angle=null;
    	curvature=null;
    	lengths=null;
    	numPts=null;
    	direction=null;
    	sumfromvolume=null;
    	avgfromvolume=null;
    	maxfromvolume=null;
    	minfromvolume=null;
    	medianfromvolume=null;
    }
}