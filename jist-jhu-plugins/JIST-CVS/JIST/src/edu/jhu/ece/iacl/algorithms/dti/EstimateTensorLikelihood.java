package edu.jhu.ece.iacl.algorithms.dti;

import Jama.Matrix;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;

public class EstimateTensorLikelihood {

	public static void estimate(float[][][][] tensors, float[][][] reference,
			float[][][] noiseField, float[][][][] DWdata, float[] bvalues,
			float[][] grads, byte[][][] mask, double []kspaceAvgsPerVoxel) {

		/****************************************************
		 * Step 1: Validate Input Arguments 
		 ****************************************************/
		int bvalList[] = null;
		int gradList[] = null;
		int Nvols = DWdata[0][0][0].length;
		if(Nvols!=bvalues.length)
			throw new RuntimeException("EstimateTensorLikelihood: Number of volumes does not match number of bvalues.");
		if(Nvols!=grads.length)
			throw new RuntimeException("EstimateTensorLikelihood: Number of volumes does not match number of gradient directions.");

		if(mask==null) {
			mask = new byte[DWdata.length][DWdata[0].length][DWdata[0][0].length];
			for(int i=0;i<DWdata.length;i++) {

				for(int j=0;j<DWdata[0].length;j++) {

					for(int k=0;k<DWdata[0][0].length;k++)
						mask[i][j][k]=1;
				}
			}
		} 
		if(mask.length!=DWdata.length)
			throw new RuntimeException("EstimateTensorLikelihood: Mask does not match data in dimension: 0.");
		if(mask[0].length!=DWdata[0].length)
			throw new RuntimeException("EstimateTensorLikelihood: Mask does not match data in dimension: 1.");
		if(mask[0][0].length!=DWdata[0][0].length)
			throw new RuntimeException("EstimateTensorLikelihood: Mask does not match data in dimension: 2.");


		int Ngrad = 0;
		int Nb0 = 0; 
		for(int i=0;i<bvalues.length;i++) {
			if(bvalues[i]==0)
				Nb0++;
			if(bvalues[i]>0 && grads[i][0]<90)
				Ngrad++;
		}

		if(Nb0==0)
			throw new RuntimeException("EstimateTensorLikelihood: No reference images specified.");

		if(Ngrad<6)
			throw new RuntimeException("EstimateTensorLikelihood: Less than 6 diffusion weighted volumes specified.");

		/****************************************************
		 * Step 2: Index b0 and DW images and normalize DW directions
		 ****************************************************/

		bvalList = new int[Nb0];
		gradList = new int[Ngrad];
		Ngrad = 0;
		Nb0 = 0; 
		for(int i=0;i<bvalues.length;i++) {
			if(bvalues[i]==0) {
				bvalList[Nb0]=i;
				Nb0++;
			}

			if(bvalues[i]>0 && grads[i][0]<90) {
				gradList[Ngrad]=i;
				float norm = (float)Math.sqrt(grads[i][0]*grads[i][0]+
						grads[i][1]*grads[i][1]+
						grads[i][2]*grads[i][2]);
				if(norm==0)
					throw new RuntimeException("EstimateTensorLikelihood: Invalid DW Direction "+i+": ("+grads[i][0]+","+grads[i][1]+","+grads[i][2]+");");
				grads[i][0]/=norm;
				grads[i][1]/=norm;
				grads[i][2]/=norm;


				Ngrad++;
			}
		}

		/****************************************************
		 * Step 3: Build the imaging and inversion matrix 
		 ****************************************************/
		Matrix reconMatrix=null;
		Matrix imagMatrix = new Matrix(gradList.length,6);
		for(int ii=0;ii<gradList.length;ii++) {
			//xx
			imagMatrix.set(ii,0,bvalues[gradList[ii]]*grads[gradList[ii]][0]*grads[gradList[ii]][0]);
			//			2xy
			imagMatrix.set(ii,1,bvalues[gradList[ii]]*grads[gradList[ii]][0]*grads[gradList[ii]][1]*2);
			//			2xz
			imagMatrix.set(ii,2,bvalues[gradList[ii]]*grads[gradList[ii]][0]*grads[gradList[ii]][2]*2);
			//			yy
			imagMatrix.set(ii,3,bvalues[gradList[ii]]*grads[gradList[ii]][1]*grads[gradList[ii]][1]);
			//			2yz
			imagMatrix.set(ii,4,bvalues[gradList[ii]]*grads[gradList[ii]][1]*grads[gradList[ii]][2]*2);
			//			zz
			imagMatrix.set(ii,5,bvalues[gradList[ii]]*grads[gradList[ii]][2]*grads[gradList[ii]][2]);
		}
		reconMatrix = imagMatrix.inverse(); // (actually, pseudoinverse)

		if(imagMatrix.rank()<6) {
			System.out.println("jist.plugins"+"\t"+"EstimateTensorLikelihood : **********WARNING**********");
			System.out.println("jist.plugins"+"\t"+"EstimateTensorLikelihood : Gradient table of rank < 6");
			System.out.println("jist.plugins"+"\t"+"EstimateTensorLikelihood : ***************************");
		}

		/****************************************************
		 * Step 5: Loop over all voxels and estimate tensors with the linear method
		 * Estimate  
		 ****************************************************/			
		//float []DW = new float[gradList.length];
		Matrix DW = new Matrix(gradList.length,1);
		int ignoreDW;
		Matrix tensorMatrix; 
		Matrix DWsubset=null;

		DTEMRLTensor tensor = new DTEMRLTensor();
		tensor.setLikelihoodThreshold(Double.MAX_VALUE);
		// First pass : Estimate Linear Tensors 
		double []obsB0 = new double[Nb0];
		double []obsDW = new double[Ngrad];
		double sumSNR = 0;
		double sumB0 = 0;
		int count = 0;
		double []LLresult = new double[2];
		double []LLswap = new double[Ngrad];
		double LLmeanSum=0;
		double LLstdSum=0;
		for(int i=0;i<DWdata.length;i++) {
			for(int j=0;j<DWdata[0].length;j++) {
				for(int k=0;k<DWdata[0][0].length;k++) {
					if(mask[i][j][k]!=0) {

						double mb0=0;		
						int cnt=0;
						for(int ii=0;ii<bvalList.length;ii++) {							
							obsB0[ii] = DWdata[i][j][k][gradList[ii]];
							if(!Float.isNaN(DWdata[i][j][k][bvalList[ii]])) {
								mb0+=DWdata[i][j][k][bvalList[ii]];
								cnt++;
							}
						}						
						mb0/=cnt;
						
						sumB0+=mb0;
						sumSNR+=mb0/noiseField[i][j][k];

						ignoreDW=0;
						for(int ii=0;ii<gradList.length;ii++) {
							obsDW[ii]=DWdata[i][j][k][gradList[ii]];
							double val=-Math.log(DWdata[i][j][k][gradList[ii]]/mb0);
							if(Double.isNaN(val)  || Double.isInfinite(val)) {
								//								ignoreDW++;
								DW.set(ii, 0, 0.5);
						} else 
								DW.set(ii,0,val); // try to minimize initialization impact
						}
						tensorMatrix = reconMatrix.times(DW); //LLMSE tensor estimation 

						Matrix model = imagMatrix.times(tensorMatrix);
						double []modelTruth =model.getColumnPackedCopy();
						for(int ii=0;ii<modelTruth.length;ii++)
							modelTruth[ii]=mb0*Math.exp(-1*modelTruth[ii]);
						DTEMRLTensor.LLvalue(modelTruth, noiseField[i][j][k], obsDW, LLswap, LLresult);
						LLmeanSum+=LLresult[0];
						LLstdSum+=LLresult[1];
						for(int c=0;c<6;c++) {
							tensors[i][j][k][c]=(float)tensorMatrix.get(c, 0);
						}
					} else { //end if(mask[i][j][k]!=0) {
						for(int c=0;c<6;c++) {
							tensors[i][j][k][c]=Float.NaN;
						}
					}
				} // end for(int i=0;i<DWdata.length;i++) {					
			} // end for(int j=0;j<DWdata[0].length;j++) {			
		} // end for(int k=0;k<DWdata[0][0].length;k++) {	

		double []kspaceAvgs = new double[Nb0+Ngrad];
		
		for(int i=0;i<bvalList.length;i++) {			
			kspaceAvgs[i]=kspaceAvgsPerVoxel[bvalList[i]];
		}
		for(int i=0;i<gradList.length;i++) {			
			kspaceAvgs[i+bvalList.length]=kspaceAvgsPerVoxel[gradList[i]];
		}
		double NFStdDev = StatisticsDouble.square(sumSNR/count)*1e-4*sumB0/count; // 1% at SNR 10:1
		LLmeanSum/=count;
		LLstdSum/=count;
		double LikelihoodThreshold=LLmeanSum+2*LLstdSum;
		tensor.setUseDirectTensorRepresentation(true);
		tensor.setLikelihoodThreshold(LikelihoodThreshold);
		count = 0;
		long startTime=System.currentTimeMillis();
		for(int i=0;i<DWdata.length;i++) {
			for(int j=0;j<DWdata[0].length;j++) {
				for(int k=0;k<DWdata[0][0].length;k++) {
					if(mask[i][j][k]!=0) {
						if(!(Float.isInfinite(tensors[i][j][k][0])||Float.isNaN(tensors[i][j][k][0]))) {
count++;
							double mb0=0;		
							int cnt=0;
							for(int ii=0;ii<bvalList.length;ii++) {							
								obsB0[ii] = DWdata[i][j][k][bvalList[ii]];
								if(!Float.isNaN(DWdata[i][j][k][bvalList[ii]])) {
									mb0+=DWdata[i][j][k][bvalList[ii]];
									cnt++;
								}
							}						
							mb0/=cnt;

							ignoreDW=0;
							for(int ii=0;ii<gradList.length;ii++) {
								obsDW[ii]=DWdata[i][j][k][gradList[ii]];								
								
							}
							
							tensor.setNoiseLevelPrior(noiseField[i][j][k], NFStdDev);
							tensor.init(imagMatrix, mb0, noiseField[i][j][k], obsB0, obsDW, tensors[i][j][k],kspaceAvgs);
//							tensor.LLvalue();
//							boolean convereged = tensor.optimize6D2D();
							boolean convereged = tensor.optimize8D(0);
							
							
							tensorMatrix = tensor.getD();
							reference[i][j][k] = (float)tensor.getB0();
							noiseField[i][j][k] = (float)tensor.getSigma();	
							for(int c=0;c<6;c++) {
								tensors[i][j][k][c]=(float)tensorMatrix.get(c, 0);
							}
						}
					} //end if(mask[i][j][k]!=0) {
				} // end for(int k=0;k<DWdata[0][0].length;k++) {
			} // end for(int j=0;j<DWdata[0].length;j++) {	
			System.out.println("jist.plugins"+"\t"+i+":"+(count/((float)(System.currentTimeMillis()-startTime))));
		
		} // end for(int i=0;i<DWdata.length;i++) {	
		float vxpsec =count/((float)(System.currentTimeMillis()-startTime));
		System.out.println("jist.plugins"+"\t"+"Finished: "+vxpsec*1000+" voxels/second ... "+1/vxpsec+" ms/voxel");
		System.out.flush();
	} // end estimate
} // end EstimateTensorLikelihood
