package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import gov.nih.mipav.view.renderer.J3D.model.structures.ModelTriangleMesh;

public class STAPLEsurf extends AbstractCalculation{

	
	List<EmbeddedSurface> surfaces;
	float[][][] truthImage;
	ArrayList<EmbeddedSurface> truth;
	
	protected PerformanceLevel pl; //performance level estimates
	protected ArrayList<Float> priors;
	protected ArrayList<Number> labels;
	protected double convergesum;
	protected int maxiters = 1000;
	protected int offset;
	protected String dir;
	protected double eps;
	protected String initType;
	
	
	public STAPLEsurf(){
		super();
		setLabel("STAPLEsurface");
	}

	public STAPLEsurf(List<EmbeddedSurface> surf){
		super();
		setLabel("STAPLEsurface");
		surfaces = surf;
		if(verifySizes()){
			getPriorProb();
		}else{
			System.err.println("jist.plugins"+"Rater images must have equal dimensions");
		}
	}
	
	public STAPLEsurf(List<EmbeddedSurface> surf, int offset){
		super();
		setLabel("STAPLEsurface");
		surfaces = surf;
		this.offset=offset;
		if(verifySizes()){
			getPriorProb();
		}else{
			System.err.println("jist.plugins"+"Rater images must have equal dimensions");
		}
	}
	
	public void setInit(String init){
		this.initType=init;
	}
	
	public void setmaxIters(int max){
		maxiters=max;
	}
	public void setEps(double eps){
		this.eps=eps;
	}
	public void setOffset(int offset){
		this.offset=offset;
	}
	
	public void setSurfaces(List<EmbeddedSurface> surf){
		surfaces=surf;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}
	
	public boolean verifySizes(){
		int verts = surfaces.get(0).getVertexCount();
		int faces = surfaces.get(0).getFaceCount();
		for(int i=1; i<surfaces.size(); i++){
			if(surfaces.get(i).getVertexCount()!=verts || surfaces.get(i).getFaceCount()!=faces){
				return false;
			}
		}
		return true;
	}

	public void findLabels(){
		labels = new ArrayList<Number>();
		if(surfaces!=null){
			for(int j=0; j<surfaces.size(); j++){
				for(int i=0; i<surfaces.get(j).getVertexCount(); i++){
					if(!labels.contains(new Double(surfaces.get(j).getVertexData(i)[offset]))){
						labels.add(new Double(surfaces.get(j).getVertexData(i)[offset]));
					}
				}
			}
		}

		else{
			System.err.println("jist.plugins"+"No data!");
		}
		labels.trimToSize();
		System.out.println("jist.plugins"+"\t"+"Found Labels: ");
		System.out.println("jist.plugins"+"\t"+labels);
		System.out.println("jist.plugins"+"\t"+"");
	}
	
	public void initialize(){
		if(initType.equals("Truth")){
			System.out.println("jist.plugins"+"\t"+"Initializing Truth");
			System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
			pl = new PerformanceLevel(labels.size(), surfaces.size(),true);
			truth = new ArrayList<EmbeddedSurface>();
			System.out.println("jist.plugins"+"\t"+"Num Rater Images: " + surfaces.size());
			System.out.println("jist.plugins"+"\t"+"Num Truth Images: " + truth.size());
			
			// Initialize the Truth Estimates
			int verts = surfaces.get(0).getVertexCount();
			ArrayList<double[][]> labelests = new ArrayList<double[][]>(verts);
			double d = 1/surfaces.size();
			
			for(int i=0; i<labels.size(); i++){
				labelests.add(new double[verts][1]);
			}
			
			for(int i=0; i<verts; i++){

				for(int l=0; l<surfaces.size(); l++){
					int t = getIndex(labels,surfaces.get(l).getVertexData(i)[offset]);
					labelests.get(t)[i][0]=labelests.get(t)[i][0]+d;
				}
			}
			
			for(int i=0; i<labels.size(); i++){
				EmbeddedSurface s = surfaces.get(0).clone();
				s.setVertexData(labelests.get(i));
				truth.add(s);
			}
			
		}else{
			double init = 0.9999;
			try{
				findLabels();
				labels.trimToSize();
				System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
				pl = new PerformanceLevel(labels.size(), surfaces.size(),true);
				pl.initialize(init);
				truth = new ArrayList<EmbeddedSurface>();
				System.out.println("jist.plugins"+"\t"+"Num Rater Images: " + surfaces.size());
				ModelTriangleMesh mesh = surfaces.get(0);
				Runtime runtime = Runtime.getRuntime();  
				for(int i=0; i<labels.size(); i++){
					System.out.println("jist.plugins"+"\t"+"allocated memory: " + runtime.totalMemory() / 1024); 
					EmbeddedSurface a = new EmbeddedSurface(mesh);
					a.setName("TruthEstimate_"+labels.get(i));
					truth.add(a);
				}
				System.out.println("jist.plugins"+"\t"+"Num Truth Surfs: " + truth.size());
				printPerformanceLevels();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	public void Estep(){
		if(surfaces!=null){
			int verts = surfaces.get(0).getVertexCount();
	
			convergesum=0;

			for(int i=0; i<verts; i++){
				double[] a = getPriorArray();
				for(int l=0; l<surfaces.size(); l++){
					//Compute 'a' (Eqn 14)
					//Compute 'b' (Eqn 15)

					for(int m=0; m<labels.size(); m++){
						
						int t = getIndex(labels,surfaces.get(l).getVertexData(i)[offset]);
						if(t>-1){
							a[m]=a[m]*pl.getD(l, t, m);
						}else{
							System.err.println("jist.plugins"+"Could not find label!");
						}
					}
				}
//				Compute weights for truth using Eqn (16)
				double sum=0;
				for(int m=0; m<labels.size(); m++){
					sum=sum+a[m];
				}
				for(int n=0; n<truth.size(); n++){
//					truth.get(n).set(i,j,k, a[n]/sum);
					double[] array = {a[n]/sum};
					truth.get(n).setVertexData(i, array);
					convergesum=convergesum+array[0];
				}

			}


		}

	}
	

	public void Mstep(){
		if(surfaces!=null){

			int verts = surfaces.get(0).getVertexCount();
			
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)

			double[] totsum = new double[labels.size()];
			
			for(int m=0; m<labels.size(); m++){
//				float totsum = 0f;
				for(int i=0; i<verts; i++){
							totsum[m]=totsum[m]+truth.get(m).getVertexData(i)[offset];
							for(int l=0; l<surfaces.size(); l++){
								
								int t = getIndex(labels,surfaces.get(l).getVertexData(i)[offset]);
								if(t>-1){
									pl.set(l, t, m, pl.getD(l, t, m)+truth.get(m).getVertexData(i)[offset]);
									
//									System.out.println("jist.plugins"+"\t"+"Point: (" +i+","+j+","+k+")");
//									System.out.println("jist.plugins"+"\t"+"Image value: " + images.get(0).get(i,j,k).intValue());
//									System.out.println("jist.plugins"+"\t"+pl);
//									System.out.println("jist.plugins"+"\t"+"");
									
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
								
//								if(i==20 && j==20 && k==6){
//									System.out.println("jist.plugins"+"\t"+"Image value: " + images.get(0).get(i,j,k).intValue());
//									System.out.println("jist.plugins"+"\t"+"labels: " + labels);
//									System.out.println("jist.plugins"+"\t"+"index: " + t);
//								}
								
							}
						
					
				}
//				System.out.println("jist.plugins"+"\t"+pl);
//				printArray(totsum);
			}
			// Store performance parameter estimates for this iteration
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
			
			System.out.println("jist.plugins"+"\t"+pl);
			
		}
	}
	
	public void getPriorProb(){
		if(surfaces!=null){
			int verts = surfaces.get(0).getVertexCount();
			float total = surfaces.size()*verts;
			labels = new ArrayList<Number>();
			priors = new ArrayList<Float>();
			for(int i=0; i<verts; i++){
				for(int l=0; l<surfaces.size(); l++){
					if(!labels.contains(surfaces.get(l).getVertexData(i)[offset])){
						labels.add(surfaces.get(l).getVertexData(i)[offset]);
						priors.add(new Float(1f));
					}else{
						int thisone = getIndex(labels,surfaces.get(l).getVertexData(i)[offset]);
						priors.set(thisone, priors.get(thisone)+1);
					}
				}
			}
//			System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//			System.out.println("jist.plugins"+"\t"+"Total" + total);
			priors.trimToSize();
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
		}else{
			System.err.println("jist.plugins"+"Rater data is null");
		}
	}
	
	public void iterate(){
		
		if(initType.equals("Truth")){
			initialize();
		}else{
			initialize();
			Estep();
		}
		
		double prevcs = convergesum;
		int iters = 0;	
		
		System.out.println("jist.plugins"+"\t"+"Priors: ");
		printArray(getPriorArray());
		
		boolean keepgoing = true;
		while(keepgoing && iters<maxiters){
//			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
			Mstep();
			Estep();
			if(Math.abs(prevcs-convergesum)<eps){
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
				printPerformanceLevels();
			}
			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
			System.out.println("jist.plugins"+"\t"+"Prev Sum: " +prevcs);
			System.out.println("jist.plugins"+"\t"+"Converge Sum: " +convergesum);
			System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(prevcs-convergesum));
			System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
//			System.out.println("jist.plugins"+"\t"+pl);
			System.out.println("jist.plugins"+"\t"+"*****************");
			
			iters++;
			
//			String iout = dir + "Iter"+iters+".raw";
//			System.out.println("jist.plugins"+"\t"+"Writing to: " +iout);
//			RawWriter.writeImgFloat(truth, iout);
			
			prevcs = convergesum;
		}
//		ParamObject<String> out = new ParamObject<String>("PerformanceLevels");
		
	}
	
	public void printPerformanceLevels(){
		System.out.println("jist.plugins"+"\t"+pl);
	}
	public String getPerformanceLevels(){
		return pl.toString();
	}
		
	private static void printArray(double[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}
	
	private double[] getPriorArray(){
		double[] a = new double[labels.size()];
		for(int i=0; i<labels.size(); i++){
			a[i]=priors.get(i).doubleValue();
		}
		return a;
	}
	
	public float[][][] getTruthImage(){
		return truthImage;
	}
	
	public ArrayList<EmbeddedSurface> getTruth(){
		truth.trimToSize();
		return truth;
	}

	public PerformanceLevel getPeformanceLevel(){
		return pl;
	}
	
	public int getIndex(ArrayList<Number> l, Number n){
		Iterator<Number> it = l.iterator();
		int i =0;
		while(it.hasNext()){
			Number ith = it.next();
			if(ith.doubleValue()==n.doubleValue()){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public EmbeddedSurface getHardSeg(){
		EmbeddedSurface seg = surfaces.get(0).clone();
		seg.setName("SurfaceSegmentation");
		double[][] data = seg.getVertexData();
		for(int i =0; i<data.length; i++){
			double max = 0;
			int label = -1;
			for(int j=0; j<truth.size(); j++){
				if(truth.get(j).getVertexData()[i][offset]>max){
					max = truth.get(j).getVertexData()[i][offset];
					label= labels.get(j).intValue();
				}
			}
			data[i][offset]=label;
		}
		return seg;
	}
	

}
