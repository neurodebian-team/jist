package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;


public class VolumeDirectoryNoIter extends ProcessingAlgorithm {

	/** The dir param. */
	protected ParamFile dirParam;
	
	/** The include param. */
	protected ParamBoolean includeParam;
	
	/** The filter rule. */
	protected ParamOption filterRule;
	
	/** The filter expression. */
	protected ParamString filterExpression;
	
	/** The search depth. */
	protected ParamInteger searchDepth;
	
	/** The vol param. */
	protected ParamVolumeCollection volcol;
	
	/** The images. */
	transient protected ArrayList<File> images;
	
	/** The index. */
	transient protected int index;
	
	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Selects volumes as 'Volume Directory' but returns a volume collection and does not iterate.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection outputParams) {
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Volume Directory No Iter");
		inputParams.setName("Volume_Directory_No_Iter");


		inputParams.add(dirParam = new ParamFile("Directory", ParamFile.DialogType.DIRECTORY));
		inputParams.add(includeParam = new ParamBoolean("Include Sub-directories", true));
		inputParams.add(searchDepth = new ParamInteger("Max Directory Depth", 0, 100000, 1));
		inputParams.add(filterRule = new ParamOption("Filter Rule", new String[] { "Contains", "Matches","Begins With", "Ends With" }));
		inputParams.add(filterExpression = new ParamString("Filter Expression"));


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);	
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(volcol = new ParamVolumeCollection("Volume", null,-1,-1,-1,1));
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		reset();
	}


	public void reset() {
		images = new ArrayList<File>();
		File dir = dirParam.getValue();
		if (dir == null) {
			return;
		}
		boolean recurse = includeParam.getValue();
		int rule = filterRule.getIndex();
		String exp = filterExpression.getValue();
		LinkedList<File> dirs = new LinkedList<File>();
		LinkedList<Integer> depths = new LinkedList<Integer>();
		int maxDepth = searchDepth.getInt();
		FileExtensionFilter extFilter = ModelImageReaderWriter.getInstance().getExtensionFilter();
		if (recurse) {
			dirs.add(dir);
			depths.add(0);
			File d;
			int depth;
			while (dirs.size() > 0) {
				d = dirs.remove();
				depth = depths.remove();
				if ((d == null) || (depth > maxDepth)) {
					continue;
				}
				File[] files = d.listFiles();
				if (files == null) {
					continue;
				}
				for (File f : files) {
					if (f.isDirectory()) {
						dirs.add(f);
						depths.add(depth + 1);
					} else {
						String ext = FileReaderWriter.getFileExtension(f);
						boolean accept = true;
						if (exp.length() > 0) {
							switch (rule) {
							case 0:
								if (FileReaderWriter.getFileName(f).indexOf(exp) < 0) {
									accept = false;
								}
								break;
							case 1:
								if (!FileReaderWriter.getFileName(f).matches(exp)) {
									accept = false;
								}
								break;
							case 2:
								if (!FileReaderWriter.getFileName(f).startsWith(exp)) {
									accept = false;
								}
								break;
							case 3:
								if (!FileReaderWriter.getFileName(f).endsWith(exp)) {
									accept = false;
								}
								break;
							}
						}
						if (accept && !ext.equals("raw") && extFilter.accept(f)) {
							images.add(f);
						}
					}
				}
			}
		} else {
			File[] files = dir.listFiles();
			for (File f : files) {
				String ext = FileReaderWriter.getFileExtension(f);
				boolean accept = true;
				if (exp.length() > 0) {
					switch (rule) {
					case 0:
						if (FileReaderWriter.getFileName(f).indexOf(exp) < 0) {
							accept = false;
						}
						break;
					case 1:
						if (!FileReaderWriter.getFileName(f).matches(exp)) {
							accept = false;
						}
						break;
					case 2:
						if (!FileReaderWriter.getFileName(f).startsWith(exp)) {
							accept = false;
						}
						break;
					case 3:
						if (!FileReaderWriter.getFileName(f).endsWith(exp)) {
							accept = false;
						}
						break;
					}
				}
				if (accept && !ext.equals("raw") && extFilter.accept(f)) {
					images.add(f);
				}
			}
		}
		Collections.sort(images);
		index = 0;
		if (images.size() > 0) {
			volcol.setValue(images);
		}
	}
}
