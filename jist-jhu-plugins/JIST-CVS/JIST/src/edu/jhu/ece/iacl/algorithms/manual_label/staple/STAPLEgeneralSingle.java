package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.List;

/**
 * 
 * @author John Bogovic
 * @date 5/31/2008
 * 
 * Simultaneous Truth and Performance Level Estimation (STAPLE)
 * 
 * Warfield, Zou, and Wells, "Simultaneous Truth and Performace Level Estimation (STAPLE):
 * An Algorithm for the Validation of Image Segmentation," 
 * IEEE Trans. Medical Imaging vol. 23, no. 7, 2004
 */

public class STAPLEgeneralSingle {

	List<double[][]> raterData;
	float[][] truth;

	protected PerformanceLevel pl; //performance level estimates
	protected float prior;
	protected Number[] labels;
	protected float convergesum;
	protected int maxiters = 1000;
	protected String dir;

	public STAPLEgeneralSingle(){
	}
	public STAPLEgeneralSingle(List<double[][]> data){
		raterData = data;
		if(verifySizes()){
			getPriorProb();
		}else{
			System.err.println("jist.plugins"+"Rater images must have equal dimensions");
		}

	}

	public void setmaxIters(int max){
		maxiters=max;
	}

	public void setImages(List<double[][]> data){
		raterData = data;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}

	public boolean verifySizes(){
		int inds = raterData.get(0).length;
		for(int i=1; i<raterData.size(); i++){
			if(raterData.get(i).length!=inds){
				return false;
			}
		}
		return true;
	}

	public void initialize(){
		double init = 0.9999f;
		try{
			pl = new PerformanceLevel(2, raterData.size(),true);
			pl.initialize(init);
			truth = new float[raterData.get(0).length][1];
			printPerformanceLevels();
		}catch(Exception e){
			e.printStackTrace();
		}

	}


	public void Estep(){
		if(raterData!=null){
			int inds = raterData.get(0).length;
			convergesum=0;
			for(int i=0; i<inds; i++){
				float a = prior;
				float b = (1-prior);
				for(int l=0; l<raterData.size(); l++){
					//Compute 'a' (Eqn 14)
					//Compute 'b' (Eqn 15)
//					if(images.get(l).get(i, j, k).intValue()>0){
//					a=a*(1-pl.get(l,0, 0));
//					b=b*(pl.get(l,1, 1));
//					}else{
//					a=a*(pl.get(l,0, 0));
//					b=b*(1-pl.get(l,1,1));
//					}
					if(raterData.get(l)[i][0]>0){
						a=a*(pl.get(l,0, 0));
						b=b*(1-pl.get(l,1, 1));
					}else{
						a=a*(1-pl.get(l,0, 0));
						b=b*(pl.get(l,1,1));
					}
				}
				//Compute weights for truth using Eqn (16)
				truth[i][0]=a/(a+b);
				convergesum = convergesum + truth[i][0];
			}
		}
	}
		

	public void Mstep(){
		if(raterData!=null){
			int inds = raterData.get(0).length;
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)
			for(int l=0; l<raterData.size(); l++){
				float ptotsum = 0;
				float psum = 0;
				float qtotsum = 0;
				float qsum = 0;

				for(int i=0; i<inds; i++){
					ptotsum=ptotsum+truth[i][0];
					qtotsum=qtotsum+(1-truth[i][0]);
					if(raterData.get(l)[i][0]>0){
						psum=psum+truth[i][0];
					}else{
						qsum=qsum+(1-truth[i][0]);
					}
				}

				// Store performance parameter estimates for this iteration
				pl.set(l,0, 0,psum/ptotsum);
				pl.set(l,1, 1, qsum/qtotsum);
			}
		}
	}

	public void getPriorProb(){
		if(raterData!=null){
			int inds = raterData.get(0).length;
			float total = raterData.size()*inds;
			int sum = 0;
			for(int i=0; i<inds; i++){
				for(int l=0; l<raterData.size(); l++){
					if(raterData.get(l)[i][0]>0){
						sum++;
					}
				}
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Sum: " + sum);
			System.out.println(getClass().getCanonicalName()+"\t"+"Total" + total);
			prior=((float)sum)/total;
			System.out.println(getClass().getCanonicalName()+"\t"+"Prior Prob: " + prior);
		}

	}

	public void iterate(){
		initialize();
		Estep();
		float prevcs = convergesum;
		float eps = 0.00000001f;
		int iters = 0;	

		boolean keepgoing = true;
		while(keepgoing && iters<maxiters){
//			System.out.println(getClass().getCanonicalName()+"\t"+"Iteration: " +iters);
			Mstep();
			Estep();
			if(Math.abs(prevcs-convergesum)<eps){
				System.out.println(getClass().getCanonicalName()+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
				printPerformanceLevels();
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Iteration: " +iters);
			System.out.println(getClass().getCanonicalName()+"\t"+"*****************");

			iters++;

//			String iout = dir + "Iter"+iters+".raw";
//			System.out.println(getClass().getCanonicalName()+"\t"+"Writing to: " +iout);
//			RawWriter.writeImgFloat(truth, iout);

			prevcs = convergesum;
		}

	}

	public void printPerformanceLevels(){
		for(int i=0; i<raterData.size(); i++){

			System.out.println(getClass().getCanonicalName()+"\t"+"Rater " + (i+1) + " \t" + pl.get(i,0,0) + "\t" + pl.get(i,1,1));
		}
	}


	public void printPerformanceLevels2(){
		System.out.println(getClass().getCanonicalName()+"\t"+"*******************");
		for(int i=0; i<raterData.size(); i++){
			System.out.println(getClass().getCanonicalName()+"\t"+"Rater " + i);
			System.out.println(getClass().getCanonicalName()+"\t"+"True Postitive Rate: " + pl.get(i,0,0));
			System.out.println(getClass().getCanonicalName()+"\t"+"True Negative Rate: " + pl.get(i,1,1));
			System.out.println(getClass().getCanonicalName()+"\t"+"*******************");
		}
	}
	public float[][] getTruth(){
		return truth;
	}
	public PerformanceLevel getPeformanceLevel(){
		return pl;
	}

}
