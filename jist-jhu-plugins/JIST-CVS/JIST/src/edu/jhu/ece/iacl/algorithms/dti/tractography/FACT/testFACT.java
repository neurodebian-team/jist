package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.util.List;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 16, 2005
 * Time: 10:32:17 AM
 * To change this template use Options | File Templates.
 */
public class testFACT {
    public static void main(String [] args)
 {
    	
//    	String filehdr = "20070727pa_10_1";
    	String filehdr = "20070727pa_9_1p";
    	String directory = "/home/john/Research/20070727/corrected_dti/processed/20070727pa_9_1p/";
    	
        FACTapi foo = new FACTapi();
        foo.setParameters(.25f,70,.25f);
        long start = System.currentTimeMillis();
//        foo.loadData("/home/john/Research/dti_cruise/data/processed/RDR1_AT1251_Kwyjibo_20061106_DTI15_10_sc.fa",
//                "/home/john/Research/dti_cruise/data/processed/RDR1_AT1251_Kwyjibo_20061106_DTI15_10_sc.vec", 
//                256,256,50,(212f/256f),(212f/256f),2.2f);
//      foo.loadData("/home/john/Research/20070727/dti/20070727pa_10_1_dtis.fa",
//      "/home/john/Research/20070727/dti/20070727pa_10_1_dtis.vec", 
//      256,256,65,(212f/256f),(212f/256f),2.2f);
//        foo.loadData("/iacl/cruise/bennett/analysis/at1003/FLT1_at1003_4_1_sc1.fa",
//                "/iacl/cruise/bennett/analysis/at1003/FLT1_at1003_4_1_sc1.vec", 
//                256,256,66,(212f/256f),(212f/256f),2.5f);
//        foo.loadData("/iacl/cruise/bennett/analysis/at1080/FLT1_at1080_3_1_sc1.fa",
//                "/iacl/cruise/bennett/analysis/at1080/FLT1_at1080_3_1_sc1.vec", 
//                256,256,12,(212f/256f),(212f/256f),2.5f);
        
      foo.loadData((directory+filehdr+".fa"),(directory+filehdr+".vec"),
    	      256,256,65,(212f/256f),(212f/256f),2.2f);
      
        long data = System.currentTimeMillis();
        foo.trackAllFibers();
        long track= System.currentTimeMillis();
        System.out.println("jist.plugins"+"\t"+"Complete: "+((data-start)/10)/100f+" s to load data, "+((track-data)/10)/100f+" s to track fibers");

        cPT pts[] = new cPT[1];
        pts[0] = new cPT((char)4,(char)4,(char)4);

        List l = foo.findFibersByPoints(pts);
        long tock= System.currentTimeMillis();
        Iterator i = l.iterator();
        
        while(i.hasNext())
            System.out.println("jist.plugins"+"\t"+"**"+i.next().toString()+"**");
        System.out.println("jist.plugins"+"\t"+"Complete: "+((tock-track)/10)/100f+" s");
        
        //System.out.println("jist.plugins"+"\t"+"foo-allfibers (0) is " + ((FACTfiber)foo.allFibers.get(0)).points[0]);
        
        foo.writeFibers(directory+filehdr+"_IACLfibers.dat");
        System.out.println("jist.plugins"+"\t"+"File was written successfully!");
    }
}
