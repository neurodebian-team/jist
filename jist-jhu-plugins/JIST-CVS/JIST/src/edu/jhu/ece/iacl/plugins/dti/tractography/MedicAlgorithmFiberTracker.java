package edu.jhu.ece.iacl.plugins.dti.tractography;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberTracker;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class MedicAlgorithmFiberTracker  extends ProcessingAlgorithm{
	//private ParamSurface cruiseSurf;
	private ParamDouble startFA;
	private ParamDouble stopFA;
	private ParamInteger turningAngle;
//	private ParamPointDouble resolution;
	private ParamVolume faVol;
	private ParamVolume vecVol;
	private ParamObject<FiberCollection> fibers;
	private ParamObject<CurveCollection> fiberLines;
	private ParamBoolean specseed;
	private ParamPointInteger seed;
	private ParamBoolean writeVtk;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "") .replace(" ", "");
	private static final String shortDescription = "FACT - Deteriministic, Streamline Tractography Algorithm.";
	private static final String longDescription = "An implementation of the FACT algorithm. By default, begins tracking at all voxels, but can optionally track from a single seed point.  Returns a fiber collection in DTIStudio and in VTK format.";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(faVol=new ParamVolume("Fractional Anisotropy",VoxelType.FLOAT,-1,-1,-1,1));
		inputParams.add(vecVol=new ParamVolume("Vector Field",VoxelType.FLOAT,-1,-1,-1,3));
		inputParams.add(startFA=new ParamDouble("Start FA",0,1,0.5));
		inputParams.add(stopFA=new ParamDouble("Stop FA",0,1,0.5));
		inputParams.add(turningAngle=new ParamInteger("Max Turn Angle",0,180,40));
//		inputParams.add(resolution=new ParamPointDouble("Resolution",new Point3d(1,1,1)));
		inputParams.add(specseed=new ParamBoolean("Specify Seed"));
		inputParams.add(seed=new ParamPointInteger("Seed Point"));
		inputParams.add(writeVtk = new ParamBoolean("Write VTK fibers?",false));
		seed.setValue(new Point3i(13,14,6));

		startFA.setDescription("The FA at which to begin tracking");
		stopFA.setDescription("The FA at which to terminate tracking");
		turningAngle.setDescription("The angle (in degrees) which terminates tracking"); 
//		resolution.setDescription("SOON TO BE REMOVED!");
		specseed.setDescription("Begin tracking from a single voxel only");
		seed.setDescription("When \"Specify Seed\" is selected, specifies the seed location as a voxel coordinate.");

		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Tractography");
		inputParams.setLabel("Fiber Tracker (FACT)");
		inputParams.setName("Fiber_Tracker_FACT");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.add(new Citation("Mori S, Crain BJ, Chacko VP, van Zijl PCM, \"Three dimensional tracking of axonal projections in the brain by magnetic resonance imaging\" Ann Neurol. 1999. 45:(265-9)"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fibers=new ParamObject<FiberCollection>("Fibers (DTI Studio)",new FiberCollectionReaderWriter()));	
		outputParams.add(fiberLines=new ParamObject<CurveCollection>("Fibers (VTK)",new CurveVtkReaderWriter()));		
		fiberLines.setMandatory(false);
	}


	protected void execute(CalculationMonitor monitor) {
		System.out.println(getClass().getCanonicalName()+"\t"+"TRACKING FIBERS");
		ImageData vol=vecVol.getImageData();
		if(vol.getRows()==3){
			System.out.println(getClass().getCanonicalName()+"\t"+"REVERSE COMPONENETS");
			int rows=vol.getCols();
			int cols=vol.getSlices();
			int slices=vol.getComponents();
			int comps=vol.getRows();
			ImageData volRev=new ImageDataMipav(vol.getName()+"_rev",vol.getType(),rows,cols,slices,comps);
			volRev.setName(vol.getName());
			for(int i=0;i<rows;i++){
				for(int j=0;j<cols;j++){
					for(int k=0;k<slices;k++){
						for(int c=0;c<comps;c++){
							volRev.set(i, j, k, c, vol.getDouble(c, i, j, k));
						}
					}
				}
			}
			vol=volRev;
		}
		ImageHeader hdr  = faVol.getImageData().getHeader();
		Point3f resolution = new Point3f();
		resolution.x = hdr.getDimResolutions()[0];
		resolution.y = hdr.getDimResolutions()[1];
		resolution.z = hdr.getDimResolutions()[2];
		System.out.println(getClass().getCanonicalName()+"\t"+"Resolution:" + resolution.x + "," + resolution.y +"," + resolution.z);
		FiberTracker ftrack=new FiberTracker(
					startFA.getDouble(),
					stopFA.getDouble(),
					turningAngle.getInt()*Math.PI/180.0,
					resolution,
					faVol.getImageData(),
					vol
				);
		monitor.observe(ftrack);
		FiberCollection fibercollection;
		if(specseed.getValue()){
			fibercollection=ftrack.solve(seed.getValue());
		}else{
			fibercollection=ftrack.solve();
		}
		fibercollection.setName(faVol.getImageData().getName()+"_fbr");
		fibers.setObject(fibercollection);
		if(writeVtk.getValue()){
			fiberLines.setObject(fibercollection.toCurveCollection());
		}
	}
}
