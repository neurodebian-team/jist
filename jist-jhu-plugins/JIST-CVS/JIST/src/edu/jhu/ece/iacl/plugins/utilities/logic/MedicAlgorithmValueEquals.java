package edu.jhu.ece.iacl.plugins.utilities.logic;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;


public class MedicAlgorithmValueEquals extends ProcessingAlgorithm{
	protected ParamDouble tolerance;
	protected ParamDouble value1;
	protected ParamDouble value2;
	protected ParamBoolean assertTrue;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(value1=new ParamDouble("Value 1"));
		inputParams.add(value2=new ParamDouble("Value 2"));
		inputParams.add(tolerance=new ParamDouble("Tolerance",1E-6));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Value Equals");
		inputParams.setName("Value_Equals");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(assertTrue=new ParamBoolean("Assertion"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		double val1=value1.getDouble();
		double val2=value2.getDouble();
		double tol=tolerance.getDouble();
		assertTrue.setValue(Math.abs(val1-val2)<=tol);
	}
}
