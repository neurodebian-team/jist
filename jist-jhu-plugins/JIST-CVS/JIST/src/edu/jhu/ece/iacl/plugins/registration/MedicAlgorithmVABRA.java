package edu.jhu.ece.iacl.plugins.registration;

import java.awt.Dimension;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import Jama.Matrix;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.vabra.VabraAlgorithm;
import edu.jhu.ece.iacl.algorithms.vabra.VabraConfiguration;
import edu.jhu.ece.iacl.algorithms.vabra.VabraSolver;
import edu.jhu.ece.iacl.algorithms.volume.CompareVolumes;
import edu.jhu.ece.iacl.algorithms.volume.TransformVolume;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;

import gov.nih.mipav.model.algorithms.utilities.AlgorithmMatchImages;
import gov.nih.mipav.model.structures.ModelImage;

public class MedicAlgorithmVABRA extends ProcessingAlgorithm {

	//Module Input Parameters
	public ParamVolumeCollection inParamSubjects;
	public ParamVolumeCollection inParamTargets;
	//ParamNumberCollection inParamChannelWeights;
	//ParamNumberCollection targetWeights;
	public ParamOption[] inParamInterpChan1to3;
	public ParamNumberCollection inParamInterpChanN;
	public ParamFile inParamConfigFile;
	public ParamDouble inParamRobustMaxT;
	public ParamDouble inParamRobustMinT;
	public ParamInteger inParamNumBins;
	public ParamInteger inParamPadSize;
	public ParamBoolean inParamUseFlirt;
	public ParamBoolean inParamFineOptimize;
	public ParamBoolean inParamOptimizeXDir;
	public ParamBoolean inParamOptimizeYDir;
	public ParamBoolean inParamOptimizeZDir;
	public ParamBoolean inParamSaveIntermResults;
	public ParamOption inParamDefFieldUpdateMode;
	

	//Module Output Parameters
	public ParamVolumeCollection outParamDeformedSubject;
	public ParamVolume outParamDeformationField;
	public ParamMatrix outParamTransformMatrices;



	//algorithm Variables
	private MedicAlgorithmFLIRT flirt;
	private VabraAlgorithm vabra;
	private List<ImageData> rawTargets, rawSubjects;
	private List<ImageData> paddedSubjects, paddedTargets, transformedSubjects;
	private int[] interpType;
	private List<Number> interpTypeN;
	private TransformVolume.Interpolation[] transformInterpType;
	private int padSize;
	private boolean[] directionsToOptmize;


	private static final String revnum = VabraAlgorithm.getVersion();

	public String[] getDefaultJVMArgs() {
		return new String[] { "-XX:MinHeapFreeRatio=60",
				"-XX:MaxHeapFreeRatio=90",
				"-XX:YoungGenerationSizeIncrement=100",
		"-XX:TenuredGenerationSizeIncrement=100" };
	}

	protected void createInputParameters(ParamCollection inputParams) {
		flirt = new MedicAlgorithmFLIRT();
		inParamConfigFile = new ParamFile("Configuration", ParamFile.DialogType.FILE);
		inParamConfigFile.setExtensionFilter(new FileExtensionFilter(
				new String[] { "xml" }));
		URL url = VabraAlgorithm.class.getResource("config.xml");
		try {
			if(url!=null) {
				inParamConfigFile.setValue(new File(url.toURI()));	
			} else {			
				inParamConfigFile.setValue(new File("")); // prevent crash when module is not found
			}
		} catch (URISyntaxException e) {
			inParamConfigFile.setValue(new File("")); // prevent crash when module is not found
			e.printStackTrace();
			JistLogger.logError(JistLogger.INFO, "Continuing with library build.");
		}

		ParamCollection mainParams = new ParamCollection("Main");
		mainParams.add(inParamSubjects = new ParamVolumeCollection("Subjects"));
		mainParams.add(inParamTargets = new ParamVolumeCollection("Targets"));
		mainParams.add(inParamConfigFile);



		ParamCollection interpParams = new ParamCollection("Interpolation Type");
		inParamInterpChan1to3 = new ParamOption[3];
		String[] interpTypes = { "0:TriLinear", "1:NNI" };
		interpParams.add(inParamInterpChan1to3[0] = new ParamOption("Type for Channel 1", interpTypes));
		interpParams.add(inParamInterpChan1to3[1] = new ParamOption("Type for Channel 2", interpTypes));
		interpParams.add(inParamInterpChan1to3[2] = new ParamOption("Type for Channel 3", interpTypes));		
		for(int i = 0; i <3; i++) inParamInterpChan1to3[i].setValue(0);
		interpParams.add(inParamInterpChanN = new ParamNumberCollection("Type for Remaining Channels (Use Index from Options Above)"));		
		inParamInterpChanN.setMandatory(false);

		ParamCollection optParams = new ParamCollection("Optimization Settings");
		optParams.add(inParamOptimizeXDir = new ParamBoolean("Optimize in X direction", true));
		optParams.add(inParamOptimizeYDir = new ParamBoolean("Optimize in Y direction", true));
		optParams.add(inParamOptimizeZDir = new ParamBoolean("Optimize in Z direction", true));
		optParams.add(inParamFineOptimize = new ParamBoolean("Use Fine Optimizer(~4x Runtime Increase)", false));

		
		ParamCollection advParams = new ParamCollection("Advanced");
		advParams.add(inParamUseFlirt = new ParamBoolean("First Run 9-DOF Affine Registration (FLIRT)", true));
		advParams.add(inParamSaveIntermResults = new ParamBoolean("Save Results Between Levels", false));
		String[] defUpdateMode = { "0:Summation(Original)", "1:Cumulatively Applied" };
		advParams.add(inParamDefFieldUpdateMode = new ParamOption("Deformation Field Update Mode", defUpdateMode));
		advParams.add(inParamRobustMaxT = new ParamDouble("Percentile of Image Above Robust Maximum (0.0-1.0)", 0.0, 1.0, 0.0));
		advParams.add(inParamRobustMinT = new ParamDouble("Percentile of Image Below Robust Minimum (0.0-1.0)", 0.0, 1.0, 0.0));
		advParams.add(inParamNumBins = new ParamInteger("Number of Bins used in Histogram", 0, 128, 64));
		advParams.add(inParamPadSize = new ParamInteger("Number of Voxels to Pad Edges", 0, 100, 5));
		
		inputParams.add(mainParams);
		inputParams.add(interpParams);
		inputParams.add(optParams);
		inputParams.add(advParams);

		inputParams.setName("VABRA");
		inputParams.setLabel("VABRA");

		setPreferredSize(new Dimension(300, 600));

		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(CommonAuthors.blakeLucas);
		info.add(CommonAuthors.bryanWheeler);
		info.add(CommonAuthors.minChen);
		info.setDescription("Vectorized Adaptive Bases Registration Algorithm - A deformable registration algorithm.  Returns registered image and the applied deformation field.");
		info.setLongDescription("This is a port of Bryan Wheeler's VABRA. The Java implementation never achieved exact numerical equivalence with the C++ version, but the final results are close enough to conclude the algorithms produce the same results.");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outParamDeformedSubject = new ParamVolumeCollection("Registered Image");
		outputParams.add(outParamDeformedSubject);
		outputParams.add(outParamDeformationField = new ParamVolume("Deformation Field", VoxelType.FLOAT, -1, -1, -1, -1));
		outputParams.add(outParamTransformMatrices = new ParamMatrix("Transformation Matrix", 4, 4));
		outParamTransformMatrices.setValue(flirt.trans.getValue());
		outputParams.setName("VABRA");
	}

	protected void execute(CalculationMonitor monitor) {
		//0.)Initialize variables;
		initialize();
		monitor.observe(vabra);
		
		//1.)Check if Subject and Targets are the same, if so then exit
		if(checkSameInputs()) return;

		//2.)Preprocess with affine and padding if needed 
		preProcessing();

		System.out.println(getClass().getCanonicalName()+"\t"+"VABRA - BEFORE VABRA");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

		//3.)Run VABRA
		vabra.solve(paddedSubjects,	paddedTargets, inParamConfigFile.getValue(), inParamRobustMaxT.getFloat(), 
				inParamRobustMinT.getFloat(), inParamNumBins.getInt(), interpType, inParamFineOptimize.getValue(), 
				this.getOutputDirectory(), inParamSaveIntermResults.getValue(), directionsToOptmize, inParamDefFieldUpdateMode.getIndex());

		System.out.println(getClass().getCanonicalName()+"\t"+"VABRA - AFTER VABRA");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

		//4.)Postprocess by cropping the original pads
		postProcessing();

		
		//These are referencing input parameters, so we can't destroy them in case of other algorithms linking to it.
		rawTargets = null;
		rawSubjects = null;

		System.out.println(getClass().getCanonicalName()+"\t"+"VABRA - FINISHED");
		edu.jhu.ece.iacl.plugins.labeling.MedicAlgorithmMultiAtlasSurfaceLabeling.generateMemoryReport();

	}

	private void initialize(){
		//initialize variables
		rawTargets = inParamTargets.getImageDataList();
		rawSubjects = inParamSubjects.getImageDataList();
		transformedSubjects = new ArrayList<ImageData>();		
		paddedSubjects = new ArrayList<ImageData>();
		paddedTargets = new ArrayList<ImageData>();
		vabra = new VabraAlgorithm();

		padSize = inParamPadSize.getInt();
		
		directionsToOptmize = new boolean[3];
		directionsToOptmize[0]=inParamOptimizeXDir.getValue();
		directionsToOptmize[1]=inParamOptimizeYDir.getValue();
		directionsToOptmize[2]=inParamOptimizeZDir.getValue();
		
		
		//initialize Interpolation Types		
		transformInterpType = new TransformVolume.Interpolation[rawSubjects.size()];
		interpType = new int[rawSubjects.size()];
		interpTypeN = inParamInterpChanN.getValue();
		for (int i = 0; i < rawSubjects.size(); i++) {
			if(i<3) interpType[i] = inParamInterpChan1to3[i].getIndex();
			else interpType[i] = interpTypeN.get(i-3).intValue();

			if(interpType[i] == RegistrationUtilities.InterpolationType.TRILINEAR)	
				transformInterpType[i] = TransformVolume.Interpolation.Trilinear;
			else transformInterpType[i] = TransformVolume.Interpolation.Nearest_Neighbor;
		}
		
		
		
	}
	
	private void preProcessing(){
		//run FLIRT and Transform first if needed
		affineAlignment();

		// Padding Subject and Targets		
		paddedSubjects = padImageList(transformedSubjects);
		paddedTargets = padImageList(rawTargets);

		for(ImageData vol : transformedSubjects) vol.dispose();//destroy, no longer needed
		transformedSubjects.clear();
		
	}


	private void postProcessing(){
		ImageData temp;
		int rows, cols, slcs, i, j, k;

		//Get rid of padded Images
		for(ImageData vol : paddedTargets)
			vol.dispose();
		for(ImageData vol : paddedSubjects)
			vol.dispose();

		// Cropping Results from Previous Padding
		List<ImageData> RegisteredResults = vabra.getRegisteredResults();
		for(int q = 0; q < RegisteredResults.size(); q++) {
			ImageData vol = RegisteredResults.get(q);
			rows = vol.getRows() - 2*padSize;
			cols = vol.getCols() - 2*padSize;
			slcs = vol.getSlices() - 2*padSize;
			temp = vol.mimic(rows, cols, slcs, vol.getComponents());
			for(i = 0; i < rows; i++) for(j = 0; j < cols; j++) for(k = 0; k < slcs; k++) {
				temp.set(i, j, k, vol.getDouble(i + padSize, j + padSize, k + padSize));
			}
			temp.setHeader(vol.getHeader());
			temp.setName(rawSubjects.get(q).getName()+"_reg");
			vol.dispose(); // The padded image is no longer needed
			outParamDeformedSubject.add(temp);//set output for registered subjects
		}

		// Cropping Deformation from Previous Padding
		ImageData vol = vabra.getDeformationField();
		rows = vol.getRows() - 2*padSize;
		cols = vol.getCols() - 2*padSize;
		slcs = vol.getSlices() - 2*padSize;
		temp = vol.mimic(rows, cols, slcs, 3);
		for(int c = 0; c < 3; c++)	{
			for(i = 0; i < rows; i++) for(j = 0; j < cols; j++) for(k = 0; k < slcs; k++) {
				temp.set(i, j, k, c, vol.getDouble(i + padSize, j + padSize, k + padSize, c));
			}
		}
		temp.setHeader(vol.getHeader());
		temp.setName(rawSubjects.get(0).getName()+"_def_field");
		vol.dispose();
		outParamDeformationField.setValue(temp);//set output for deformation field
		
	}


	private void affineAlignment(){
		//affine first if needed
		if (inParamUseFlirt.getValue() == true) {
			System.out
			.println("*********Running Affine First**************");
			//set inputs
			flirt.target.setValue(rawTargets.get(0));
			flirt.source.setValue(rawSubjects.get(0));
			flirt.dof.setValue(2);
			flirt.run();				
			outParamTransformMatrices.setValue(flirt.trans.getValue());
			flirt.registered.getImageData().dispose(); // don't need this output
			System.out.println(getClass().getCanonicalName()+"\t"+"*********Affine Finished**************");
		} else {
			System.out.println("*********Using Identity Affine Transform**************");
			Matrix xfmIdentity = new Matrix(4,4);
			for(int ii=0;ii<4;ii++)
				xfmIdentity.set(ii,ii,1);
			outParamTransformMatrices.setValue(xfmIdentity);
		}

		//transform if needed
		Matrix transMatrix = outParamTransformMatrices.getValue();
		Point3i dimensions;
		Point3f resolutions;
		float[] res;
		res = rawTargets.get(0).getHeader().getDimResolutions();
		resolutions = new Point3f(res[0], res[1], res[2]);
		dimensions = new Point3i(rawTargets.get(0).getRows(), rawTargets.get(0)
				.getCols(), rawTargets.get(0).getSlices());

		for (int q = 0; q < rawSubjects.size(); q++) {
			ModelImage tempModelImage = rawSubjects.get(q).getModelImageCopy();		
			
			
			transformedSubjects.add(q, TransformVolume.transform(tempModelImage,
					transformInterpType[q], transMatrix, resolutions, dimensions));
			
			System.out.format(transformedSubjects.get(q).getType().name());
			tempModelImage.disposeLocal();
		}
	}

	private List<ImageData> padImageList(List<ImageData> imgsToPad){
		int rows, cols, slcs, i, j, k;
		
		ImageData newVol, origVol;
		List<ImageData> paddedImages = new ArrayList<ImageData>();
		for (int ch = 0; ch < imgsToPad.size(); ch ++) {
			origVol = imgsToPad.get(ch);
			rows = origVol.getRows();
			cols = origVol.getCols();
			slcs = origVol.getSlices();
			newVol = origVol.mimic(rows + 2*padSize, cols + 2*padSize, slcs + 2*padSize, origVol.getComponents());
			for (i = 0; i < rows; i++) for (j = 0; j < cols; j++) for (k = 0; k < slcs; k++) {
				newVol.set(i + padSize, j + padSize, k + padSize, origVol.getDouble(i, j, k));
			}
			newVol.setHeader(origVol.getHeader());
			newVol.setName(origVol.getName() + "_pad");
			origVol.dispose();
			paddedImages.add(newVol);
		}

		return paddedImages;
	}

	private boolean checkSameInputs(){
		CompareVolumes vc;
		ImageData src, dest;
		for (int i = 0; i < rawSubjects.size(); i++) {
			src = rawSubjects.get(i);
			dest = rawTargets.get(i);
			vc = new CompareVolumes(src, dest, -1E30, 1);
			vc.compare();
			if (!vc.isComparable() || vc.getMinError() != 0	|| vc.getMaxError() != 0) {
				return false;
			}
		}

		//set outputs as original subject with zero deformation, and identity transform matrix
		System.out.println(getClass().getCanonicalName()+"\t"+"Same Image, no registration needed");
		outParamDeformedSubject.setValue(rawSubjects);

		ImageDataFloat tempF = new ImageDataFloat(rawTargets.get(0).getRows(), 
				rawTargets.get(0).getCols(), rawTargets.get(0).getSlices(), 3);
		tempF.setName("def_field");
		outParamDeformationField.setValue(tempF);

		Matrix xfmIdentity = new Matrix(4,4);
		for(int ii=0;ii<4;ii++)
			xfmIdentity.set(ii,ii,1);
		outParamTransformMatrices.setValue(xfmIdentity);
		return true;
	}



	public void hideVolumeInputs() {
		inParamSubjects.setHidden(true);
		inParamTargets.setHidden(true);
	}
}
