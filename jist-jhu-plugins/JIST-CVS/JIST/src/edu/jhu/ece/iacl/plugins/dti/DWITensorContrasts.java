package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;



public class DWITensorContrasts extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolume tensor4D;	// Tensor input

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume FAVolume;	// Fractional anisotropy
	private ParamVolume MDVolume;	// mean diffusivity = trace/3
	private ParamVolume RAVolume;	// relative anisotropy
	private ParamVolume VRVolume;	// volume ratio
	private ParamVolume EvalsVolume;// eigenvalues (sorted)
	private ParamVolume VEC1Volume;	// 1st eigenvector
	private ParamVolume VEC2Volume;	// 2nd eigenvector
	private ParamVolume VEC3Volume; // 3rd eigenvector
	private ParamOption eigenOptions; // estimate additional eigenvectors/eigenvalues
	private ParamOption aniOptions; // estimate additional anisotropies

	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Estimates tensor contrasts.";
	private static final String longDescription = "By default, estimates Mean Diffusion (MD), Fractional Anisotropy (FA), and the primary eigenvector (VEC1). Can optionally output all eigenvalues/vectors, as well as Relative Anisotropy (RA), and Volume Ratio (VR).\n" +
			"Note that this module takes a 4-D volume as input, while most Tensor Estimators have a file as output. Use the 'FileCollection to VolumeCollection' module to convert.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Contrasts");
		inputParams.setLabel("Tensor : Compute DTI Contrasts");
		inputParams.setName("Tensor_Compute_DTI_Contrasts");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setStatus(DevelopmentStatus.RC);
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new Citation("Basser, PJ, Jones, DK. \"Diffusion-tensor MRI: Theory, experimental design and data analysis - a technical review.\" NMR Biomed 2002; 15(7-8):456-67"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system   
		 ****************************************************/
		inputParams.add(tensor4D=new ParamVolume("Tensor Volume (4D)",null,-1,-1,-1,6));
		inputParams.add(aniOptions=new ParamOption("Additional Anisotropies:",new String[]{"<none>","RA.VR"}));
		inputParams.add(eigenOptions=new ParamOption("Additional Eigen-Contrasts:",new String[]{"<none>","Eigen vectors/values"}));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system  
		 ****************************************************/
		// Base Outputs
		FAVolume = new ParamVolume("FA",VoxelType.FLOAT,-1,-1,-1,1);
		FAVolume.setName("FA");
		outputParams.add(FAVolume);

		MDVolume = new ParamVolume("MD",VoxelType.FLOAT,-1,-1,-1,1);
		MDVolume.setName("MD");	
		outputParams.add(MDVolume);

		VEC1Volume = new ParamVolume("VEC-1",VoxelType.FLOAT,-1,-1,-1,3);
		VEC1Volume.setName("VEC1");
		outputParams.add(VEC1Volume);

		// Anisotropy options
		RAVolume = new ParamVolume("RA",VoxelType.FLOAT,-1,-1,-1,1);
		RAVolume.setName("RA");
		RAVolume.setMandatory(false);
		outputParams.add(RAVolume);

		VRVolume = new ParamVolume("VR",VoxelType.FLOAT,-1,-1,-1,1);
		VRVolume.setName("VR");
		VRVolume.setMandatory(false);
		outputParams.add(VRVolume);

		// Eigen options
		EvalsVolume = new ParamVolume("Evals",VoxelType.FLOAT,-1,-1,-1,3);
		EvalsVolume.setName("Evals");
		EvalsVolume.setMandatory(false);
		outputParams.add(EvalsVolume);


		VEC2Volume = new ParamVolume("VEC-2",VoxelType.FLOAT,-1,-1,-1,3);
		VEC2Volume.setName("VEC2");
		VEC2Volume.setMandatory(false);
		outputParams.add(VEC2Volume);

		VEC3Volume = new ParamVolume("VEC-3",VoxelType.FLOAT,-1,-1,-1,3);
		VEC3Volume.setName("VEC3");
		VEC3Volume.setMandatory(false);
		outputParams.add(VEC3Volume);

//		CMAPVolume = new ParamVolume("CMAP",VoxelType.COLOR,-1,-1,-1,1);
//		CMAPVolume.setName("CMAP");
//		outputParams.add(CMAPVolume);		
	}


	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to 
		 * 			know when the bug happened.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: Start");

		/****************************************************
		 * Step 2. Parse the input data 
		 ****************************************************/
		ImageData tensor=tensor4D.getImageData();
		ImageDataFloat vol=new ImageDataFloat(tensor);		
		int r=vol.getRows(), c=vol.getCols(), s=vol.getSlices(), t = vol.getComponents();

		/****************************************************
		 * Step 3. Setup memory for the computed volumes
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: Allocating memory.");
		float [][][]fa = new float[r][c][s];
		float [][][]md = new float[r][c][s];
		float [][][][]vec1=new float[r][c][s][3];

		float [][][]ra=null;		
		float [][][]vr=null;

		if(aniOptions.getIndex()!=0) {
			vr = new float[r][c][s];
			ra= new float[r][c][s];
		}

		float [][][][]evals=null;		
		float [][][][]vec2=null;
		float [][][][]vec3=null;
		if(eigenOptions.getIndex()>0) {
			evals= new float[r][c][s][3];
			vec2=new float[r][c][s][3];
			vec3=new float[r][c][s][3];
		}

		/****************************************************
		 * Step 4. Run the core algorithm. Note that this program 
		 * 		   has NO knowledge of the MIPAV data structure and 
		 * 		   uses NO MIPAV specific components. This dramatic 
		 * 		   separation is a bit inefficient, but it dramatically 
		 * 		   lower the barriers to code re-use in other applications.  		  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: Computing contrasts");
		ComputeTensorContrasts.allcontrasts(vol.toArray4d(), fa, md,
				ra,vr, evals,
				vec1,vec2,vec3);

		vol.dispose();
		vol=null;
		/****************************************************
		 * Step 5. Retrieve the image data and put it into a new
		 * 			data structure. Be sure to update the file information
		 * 			so that the resulting image has the correct
		 * 		 	field of view, resolution, etc.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: Setting up exports.");
		int []ext=tensor.getModelImageCopy().getExtents();
		
		//ModelImage img=null;

		ImageData out;

		out = (new ImageDataFloat(fa));
		out.setHeader(tensor.getHeader());
		/*img=out.getModelImage();		
		ext[3]=1;
		img.setExtents(ext);img.calcMinMaxNonZero();		
		FileUtil.updateFileInfo(tensor.getModelImage(),img);
		*/
		out.setName(tensor.getName()+"_FA");
		FAVolume.setValue(out);
		fa=null; // dereference to free memory


		out = (new ImageDataFloat(md));
		out.setHeader(tensor.getHeader());/*
		img=out.getModelImage();
		ext[3]=1;
		img.setExtents(ext);img.calcMinMaxNonZero();
		FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
		out.setName(tensor.getName()+"_MD");
		MDVolume.setValue(out);
		md=null;

		out = (new ImageDataFloat(vec1));
		out.setHeader(tensor.getHeader());/*
		img=out.getModelImage();		
		ext[3]=3;
		img.setExtents(ext);img.calcMinMaxNonZero();
		FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
		out.setName(tensor.getName()+"_VEC1");
		VEC1Volume.setValue(out);
		vec1=null;

		if(ra!=null) {
			out = (new ImageDataFloat(ra));
			out.setHeader(tensor.getHeader());;/*
			img=out.getModelImage();		
			ext[3]=1;
			img.setExtents(ext);img.calcMinMaxNonZero();
			FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
			out.setName(tensor.getName()+"_RA");
			RAVolume.setValue(out);
			ra=null;
		}
		if(vr!=null) {
			out = (new ImageDataFloat(vr));
			out.setHeader(tensor.getHeader());/*
			img=out.getModelImage();
			ext[3]=1;
			img.setExtents(ext);img.calcMinMaxNonZero();
			FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
			out.setName(tensor.getName()+"_VR");
			VRVolume.setValue(out);
			vr=null;
		}
		if(evals!=null) {
			out = (new ImageDataFloat(evals));
			out.setHeader(tensor.getHeader());/*
			img=out.getModelImage();		
			ext[3]=3;
			img.setExtents(ext);img.calcMinMaxNonZero();
			FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
			out.setName(tensor.getName()+"_EVALS");
			EvalsVolume.setValue(out);
			evals=null;
		}
		if(vec2!=null){ 
			out = (new ImageDataFloat(vec2));
			out.setHeader(tensor.getHeader());/*
			img=out.getModelImage();		
			ext[3]=3;
			img.setExtents(ext);img.calcMinMaxNonZero();
			FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
			out.setName(tensor.getName()+"_VEC2");
			VEC2Volume.setValue(out);
			vec2=null;
		}
		if(vec3!=null){
			out = (new ImageDataFloat(vec3));
			out.setHeader(tensor.getHeader());/*
			img=out.getModelImage();		
			ext[3]=3;
			img.setExtents(ext);img.calcMinMaxNonZero();
			FileUtil.updateFileInfo(tensor.getModelImage(),img);*/
			out.setName(tensor.getName()+"_VEC3");
			VEC3Volume.setValue(out);
			vec3=null;
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: FINISHED");
	}
}
