package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.algorithms.manual_label.FastMarchingLabelExtension;
import edu.jhu.ece.iacl.algorithms.manual_label.ROI;
import edu.jhu.ece.iacl.jist.io.*;
import edu.jhu.ece.iacl.jist.io.*;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;

public class MedicAlgorithmFibersFromSurfaceROI extends ProcessingAlgorithm {
	//input parameters
	private ParamObject<FiberCollection> fiberset;
	private ParamSurface surf;
	private ParamVolume templatevol;
	private ParamInteger label;
	private ParamFloat extendDist;
	private ParamOption mode;
	private ParamOption outputtype;

	//output parameters
	private ParamFileCollection fibersubsets;

	//other objects
	private FiberCollectionReaderWriter fcrw = FiberCollectionReaderWriter.getInstance();
	private CurveVtkReaderWriter cvrw = CurveVtkReaderWriter.getInstance();

	//version information
	private static final String rcsid =
		"$Id: MedicAlgorithmFibersFromSurfaceROI.java,v 1.7 2009/10/23 20:14:19 bennett Exp $";
	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fiberset = new ParamObject<FiberCollection>("Fibers In", new FiberCollectionReaderWriter()));
		inputParams.add(surf=new ParamSurface("Labeled Surface"));
		inputParams.add(templatevol=new ParamVolume("Template Volume"));
		inputParams.add(label=new ParamInteger("Label", -1));
		inputParams.add(extendDist=new ParamFloat("Label Extension Distance",10f));
		label.setDescription("The surface label of interest. -1 indicates that fibers hitting all labels that are present in the volume will be found and output.");
		
		inputParams.add(outputtype=new ParamOption("Output Type",new String[]{"DtiStudio dat","Vtk"}));
		inputParams.add(mode=new ParamOption("Mode",new String[]{"Default","Single","Pairs"}));
		mode.setDescription("This option changes the behavior of the default \"-1\" label.");


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Fiber Surface ROI");
		inputParams.setName("fiber_surface_roi");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription("");
		info.setLongDescription("");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fibersubsets = new ParamFileCollection("Fiber Sub-Set", new FileExtensionFilter(new String[]{"dat","vtk"})));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {

		//set the output directory and create it if it doesn't exist
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){ e.printStackTrace(); }

		ArrayList<File> filesout = new ArrayList<File>();

		//convert the surface to a volume
		ImageData labelvol = surfToVolumeVertData();
		FastMarchingLabelExtension fm = new FastMarchingLabelExtension();

		System.out.println(getClass().getCanonicalName()+"\t"+"Growing labels...");
		//grow the labels somewhat
		ImageDataInt extlabels = fm.solve(labelvol, 1, extendDist.getFloat());
		System.out.println(getClass().getCanonicalName()+"\t"+"done");

		//clean up a little
		labelvol.dispose(); 
		labelvol=null;

		System.out.println(getClass().getCanonicalName()+"\t"+"Initialize roi...");
		//create the roi that grabs the fibers
		ROI volroi = new  ROI(extlabels);
		int lab = label.getInt();

		//do the work
		String name = "";
		FiberCollection fibs =null;
		if(lab==-1){
			System.out.println(getClass().getCanonicalName()+"\t"+"Classifying fibers...");
			ArrayList<Integer> labelset = volroi.getLabelList();
			labelset.trimToSize();
			int numLabels = labelset.size();

			for(int i=0; i<numLabels; i++){
				System.out.println(getClass().getCanonicalName()+"\t"+"Label " + (i+1) + " of " + numLabels);
				System.out.println(getClass().getCanonicalName()+"\t"+"Label value: " + labelset.get(i));
				name = fiberset.getValue().getName()+"_roi-"+surf.getValue().getName()+Integer.toString(labelset.get(i));
				fibs = volroi.fiberroi(fiberset.getObject(), 0, labelset.get(i));
				fibs.setName(name);
				filesout.add(writeFibers(fibs,dir));
			}
			
		}else{
			System.out.println(getClass().getCanonicalName()+"\t"+"Segmenting fibers...");
			System.out.println(getClass().getCanonicalName()+"\t"+"Label value: " + lab);
			name = fiberset.getValue().getName()+"_roi-"+surf.getValue().getName()+Integer.toString(lab);
			fibs = volroi.fiberroi(fiberset.getObject(), 0, lab);
			fibs.setName(name);
			filesout.add(writeFibers(fibs,dir));
		}
		
		fibersubsets.setValue(filesout);
	}

	private File writeFibers(FiberCollection fibers, File dir){
		File out = null;
		if(outputtype.getIndex()==0){
			out = fcrw.write(fibers, dir);
		}else{
			out = cvrw.write(fibers.toCurveCollection(), dir);
		}
		return out;
	}

	private ImageData surfToVolumeVertData(){
		ImageData datavol = templatevol.getImageData().mimic();
		EmbeddedSurface s = surf.getObject();
		datavol.setName(s.getName()+"_voldata");
		for(int i=0; i<s.getVertexCount(); i++){
			Point3f pt = s.getVertex(i);
			datavol.set(Math.round(pt.x), Math.round(pt.y), Math.round(pt.z), s.getVertexData(i)[0]);
		}
		return datavol;
	}
}
