/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmThreshold extends ProcessingAlgorithm {
	ParamVolume inVol;
	ParamVolume outVol;
	ParamDouble lowerThresh;
	ParamDouble upperThresh;
	ParamDouble fillValue;
	ParamBoolean inverseThreshold;


	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Thresholds a volume.  Replaces the intensity all voxels with intensity less than \"Lower Threshold\"" +
			"or greater than \"Upper Threshold\" with \"Fill Value\".  If \"Invert Threshold\" is selected, voxels with intensities " +
			"greater than \"Lower Threshold\" and less than \"Upper Threshold\" are affected";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inVol = new ParamVolume("Volume", null, -1, -1, -1, -1));
		inputParams.add(lowerThresh = new ParamDouble("Lower Threshold"));
		inputParams.add(upperThresh = new ParamDouble("Upper Threshold"));
		upperThresh.setValue(256);
		inputParams.add(fillValue = new ParamDouble("Fill Value"));
		fillValue.setValue(0);
		inputParams.add(inverseThreshold = new ParamBoolean("Invert Threshold"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Volume Threshold");
		inputParams.setName("Volume_Threshold");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol = new ParamVolume("Threshold Volume", null, -1, -1, -1, -1));
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ImageData vol = inVol.getImageData();
		double lowert = lowerThresh.getDouble();
		double uppert = upperThresh.getDouble();
		double fillv = fillValue.getDouble();
		boolean invert = inverseThreshold.getValue();
		ImageDataMipav cvol = new ImageDataMipav(vol.getName() + "_thresh", vol.getType(), vol.getRows(), vol
				.getCols(), vol.getSlices(), vol.getComponents());
		if (vol.getComponents() > 1) {
			for (int i = 0; i < vol.getRows(); i++) {
				for (int j = 0; j < vol.getCols(); j++) {
					for (int k = 0; k < vol.getSlices(); k++) {
						for (int l = 0; l < vol.getComponents(); l++) {
							double val = vol.get(i, j, k, l).doubleValue();
							if (invert) {
								val = (val >= lowert && val <= uppert) ? fillv : val;
							} else {
								val = (val >= lowert && val <= uppert) ? val : fillv;
							}
							cvol.set(i, j, k, l, val);
						}
					}
				}
			}
		} else {
			for (int i = 0; i < vol.getRows(); i++) {
				for (int j = 0; j < vol.getCols(); j++) {
					for (int k = 0; k < vol.getSlices(); k++) {
						double val = vol.getDouble(i, j, k);
						if (invert) {
							val = (val >= lowert && val <= uppert) ? fillv : val;
						} else {
							val = (val >= lowert && val <= uppert) ? val : fillv;
						}
						cvol.set(i, j, k, val);
					}
				}
			}
		}
		cvol.setHeader(vol.getHeader().clone());
		//cvol.getModelImage().copyFileTypeInfo(vol.getModelImage());
		outVol.setValue(cvol);
	}
}
