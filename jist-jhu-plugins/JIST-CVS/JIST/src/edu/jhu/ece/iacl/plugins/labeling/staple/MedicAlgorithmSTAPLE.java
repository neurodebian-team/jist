package edu.jhu.ece.iacl.plugins.labeling.staple;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhmi.rad.medic.utilities.CropParameters;
import edu.jhmi.rad.medic.utilities.CubicVolumeCropper;
import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEmulti;
import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEmulti2;
import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEmultiICM;
import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEmultiRL;
import edu.jhu.ece.iacl.jist.io.ArrayIntegerTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt.Connectivity;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;



public class MedicAlgorithmSTAPLE extends ProcessingAlgorithm{
	public ParamVolumeCollection ratervols;
	public ParamDouble eps;
	public ParamInteger maxiters;
	public ParamOption init;
	public ParamOption connectivity;
	public ParamFloat beta;
	public ParamOption mrfMethod;

	public ParamBoolean cropFlagParam;
	public ParamBoolean probFlag;

	public ParamObject<String> pl;
	public ParamVolumeCollection truthOut;
	public ParamVolume labelvol;
	public ParamObject<int[][]> labellist;

	private File dir;
	private boolean cropFlag;
	private boolean runOld;
	private ImageHeader head;
	private Connectivity conn;
	private CubicVolumeCropper cropper;
	private String name;
	private CropParameters cp;
	private ArrayList<ImageData> croppedRaterList;
	private int[][][][] raters;

	private boolean preserveRaterImages = false;
	private boolean preserveProbImages = false;

	private ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();

	private static final String cvsversion = "$Revision: 1.24 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "STAPLE - Simultaneous Truth and Performance Level Estimation.";
	private static final String longDescription = "Given a number of labelings of a particular strucute, STAPLE returns a characterization of rater performance as well as an estimate of the true label configuration. Optionally, the probability of every label can be output as a 4D volume. Spatial consistency can be modeled by setting the regularization parameter equal to a value greater than zero. A value of 0.1 or less is recommended.";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(ratervols=new ParamVolumeCollection("Rater Volumes"));

		inputParams.add(init=new ParamOption("Initialization Type", new String[]{"Performance", "Truth"}));

		inputParams.add(eps = new ParamDouble("Convergence Threshold"));
		eps.setValue((double)0.00001);

		inputParams.add(maxiters=new ParamInteger("Max Iterations"));
		maxiters.setValue(new Integer(25));

		inputParams.add(mrfMethod = new ParamOption("MRF Solver", new String[]{"ICM","RL"}));

		inputParams.add(beta=new ParamFloat("MRF Regularization parameter",0.0f,Float.MAX_VALUE,0.0f));

		inputParams.add(connectivity=new ParamOption("Connectivity",new String[]{"6","18","26"}));

		inputParams.add(probFlag = new ParamBoolean("Output Label Probabilities"));
		probFlag.setValue(false);

		inputParams.add(cropFlagParam = new ParamBoolean("Crop Images for processing"));
		probFlag.setValue(false);

		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.STAPLE");
		inputParams.setLabel("STAPLE");
		inputParams.setName("STAPLE");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "http://putter.ece.jhu.edu/John"));
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.setAffiliation("Johns Hopkins University, Department of Electrical Engineering");
		info.add(new Citation("Warfield SK, Zou KH, Wells WM, \"Simultaneous Truth and Performance Level Estimation (STAPLE): An Algorithm for the Validation of Image Segmentation\" IEEE TMI 2004; 23(7):903-21  "));	
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {	
		outputParams.add(labelvol = new ParamVolume("Label Volume", null, -1,-1,-1,-1));

		outputParams.add(truthOut = new ParamVolumeCollection("Label Probabilities"));
		truthOut.setMandatory(false);
		truthOut.setLoadAndSaveOnValidate(false);

		pl = new ParamObject<String>("PerformanceLevels",new StringReaderWriter());
		outputParams.add(pl);

		outputParams.add(labellist = new ParamObject<int[][]>("Label List"));
		labellist.setReaderWriter(ArrayIntegerTxtReaderWriter.getInstance());
	}

	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ExecuteWrapper flirt=new ExecuteWrapper();
		monitor.observe(flirt);
		flirt.execute();
	}


	protected class ExecuteWrapper extends AbstractCalculation{
		public ExecuteWrapper(){
		}


		public void execute(){

			preprocess();
			if(runOld) {
				STAPLEmulti staple = new STAPLEmulti(croppedRaterList);	
				staple.setmaxIters(maxiters.getInt());
				staple.setEps(eps.getFloat());
				staple.setInit(init.getValue());
				staple.setConnectivity(conn);
				staple.setBeta(beta.getFloat());
				staple.distributeBeta();	//distribute beta?
				staple.setDir(dir.getAbsolutePath());
				staple.initialize();
				//			long t1=System.currentTimeMillis();
				staple.iterate();
				//			long t2=System.currentTimeMillis();
				//			System.out.println("TIME: "+(t2-t1));
				//			System.out.flush();
				//			System.exit(0);

				postProcess(staple);

			}else {
				STAPLEmulti2 staple = null;
				if(beta.getFloat()>0) {
					if(mrfMethod.getValue().equals("ICM"))
						staple = new STAPLEmultiICM(raters);
					else if(mrfMethod.getValue().equals("RL"))
						staple = new STAPLEmultiRL(raters);
				}
				else
					staple = new STAPLEmulti2(raters);

				staple.setmaxIters(maxiters.getInt());
				staple.setEps(eps.getFloat());
				staple.setInit(init.getValue());
				staple.setConnectivity(conn);
				staple.setBeta(beta.getFloat());
				staple.distributeBeta();	//distribute beta?
				staple.setDir(dir.getAbsolutePath());
				staple.initialize();
				long t1=System.currentTimeMillis();
				staple.iterate();
				long t2=System.currentTimeMillis();
				System.out.println("TIME: "+(t2-t1));
				System.out.flush();

				postProcess(staple);
			}

			System.out.println(getClass().getCanonicalName()+"\t"+"FINISHED ");
		}
	}

	public void preprocess(){
		//set the output directory and create it if it doesn't exist
		dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){ e.printStackTrace(); }

		name = ratervols.getImageDataList().get(0).getName();
		head = ratervols.getImageDataList().get(0).getHeader();
		//set boolean runOld to run the old version or the new one
		runOld=false;
		cropFlag = cropFlagParam.getValue();

		raters=null;

		//crop input volumes 
		//Find crop parameters - using addition of all rater images	

		System.out.println(head);

		croppedRaterList = new ArrayList<ImageData>(ratervols.getImageDataList().size());
		cropper = null;
		cp = null;
		if(cropFlag){
			ImageData cropme = volumeToCrop();
			cropper = new CubicVolumeCropper();	
			cropper.crop(cropme, 0, 5);		
			cp = cropper.getLastCropParams();					
			for(int i=0; i<ratervols.getImageDataList().size(); i++){
				croppedRaterList.add(cropper.crop(ratervols.getImageDataList().get(i), cp));
				System.out.println(getClass().getCanonicalName()+"\t"+croppedRaterList.get(i).getHeader().getImageOrientation());
			}
		}
		else {
			for(int i=0; i<ratervols.getImageDataList().size(); i++){
				croppedRaterList.add(ratervols.getImageDataList().get(i));
			}
		}

		if(!runOld) {
			int size = ratervols.getImageDataList().size();
			ImageData img;
			if(cropFlag){
				img = croppedRaterList.get(0);
			}else{
				img = ratervols.getImageDataList().get(0);
			}


			raters = new int[img.getRows()][img.getCols()][img.getSlices()][size];
			for(int i=0; i<size; i++){
				for(int r=0; r<img.getRows(); r++) {
					for(int c=0; c<img.getCols(); c++) {
						for(int s=0; s<img.getSlices(); s++) {
							raters[r][c][s][i]=img.getInt(r,c,s);
						}
					}				
				}
				if(!preserveRaterImages){
					img.dispose();
					img=null;
				}

				if(cropFlag){
					if(i!=size-1) img = croppedRaterList.get(i+1);
				}else{
					if(i!=size-1) img = ratervols.getImageDataList().get(i+1);
				}
			}


		}

		if(connectivity.getIndex()==0){
			conn=GridPt.Connectivity.SIX;
		}else if(connectivity.getIndex()==1){
			conn=GridPt.Connectivity.EIGHTEEN;
		}else{
			conn=GridPt.Connectivity.TWENTYSIX;
		}

		//clean up the old rater volumes
		ratervols.dispose();

	}



	public void postProcess(STAPLEmulti2 staple){

		if(probFlag.getValue()){
			//			ArrayList<File> fList = new ArrayList<File>(staple.getNumLabels());
			System.out.println("num labels: " + staple.getNumLabels());
			for (int i=0; i<staple.getNumLabels(); i++) {

				ImageData f = staple.writeProb(i);
				System.out.println("Prob image: " + f);
				if(cropFlag){
					if(!preserveProbImages){
						ImageData d = cropper.uncrop(f, cp);
						d.setHeader(head);
						truthOut.add(d);
						truthOut.writeAndFreeNow(this);
						f.dispose(); f = null;
						d.dispose(); d = null;
					}else{
						ImageData d = cropper.uncrop(f, cp);
						d.setHeader(head);
						truthOut.add(d);
						f.dispose(); f = null;
						rw.write(d, this.getOutputDirectory());
					}

				}else{
					if(!preserveProbImages){
						f.setHeader(head);
						truthOut.add(f);
						truthOut.writeAndFreeNow(this);
						f.dispose(); f = null;
					}else{
						f.setHeader(head);
						truthOut.add(f);
						rw.write(f, this.getOutputDirectory());
					}
				}
			}
			//			truthOut.setValue(fList);
		}
		ImageData img = staple.getHardSeg();
		img.setName(name+"_StapleLabeling");
		img.setHeader(head);
		if(cropFlag){
			labelvol.setValue(cropper.uncrop(img, cp));
			img.dispose(); img=null;
		}else{
			labelvol.setValue(img);
		}


		pl.setObject(staple.getPeformanceLevel().toString());
		pl.setFileName(name+"_StapleRaterPerformance");

		//		labellist.setV
		labellist.setObject(MedicAlgorithmSTAPLE.covert1Dto2D(staple.getLabelListArray()));
		labellist.setFileName("Labels.txt");
	}


	public void postProcess(STAPLEmulti staple){
		if(probFlag.getValue()){
			ArrayList<ImageData> truth = staple.getTruth();
			//			ArrayList<File> fList = new ArrayList<File>(truth.size());
			for(int i=0; i<truth.size(); i++){
				ImageData f = truth.get(i);
				if(cropFlag){
					ImageData d = cropper.uncrop(f, cp);
					d.setHeader(head);
					truthOut.add(d);
					truthOut.writeAndFreeNow(this);
					f.dispose(); f = null;
					d.dispose(); d = null;
				}else{
					f.setHeader(head);
					truthOut.add(f);
					truthOut.writeAndFreeNow(this);
					f.dispose(); f = null;
				}
				//				File fi=rw.write(f,getOutputDirectory());
				//				fList.add(fi);

			}
			//			truthOut.setValue(fList);
		}
		ImageData img = staple.getHardSeg();
		img.setHeader(head);
		if(cropFlag){
			labelvol.setValue(cropper.uncrop(img, cp));
			labelvol.setFileName("LabelVolume_"+this.toString());
			img.dispose(); img=null;
		}else{
			labelvol.setValue(img);
			labelvol.setFileName("LabelVolume_"+this.toString());
		}


		pl.setObject(staple.getPeformanceLevel().toString());
		pl.setFileName(name+"_StapleRaterPerformance");

		//		staple.get
		labellist.setObject(MedicAlgorithmSTAPLE.covert1Dto2D(staple.getLabelListArray()));
		labellist.setFileName("Labels.txt");
	}

	public static int[][] covert1Dto2D(int[] in){
		int[][] out = new int[in.length][1];
		for(int i=0; i<in.length; i++){
			out[i][0]=in[i];
		}
		return out;
	}

	public static int[] covert2Dto1D(int[][] in, int offset){
		int[] out = new int[in.length];
		for(int i=0; i<in.length; i++){
			out[i]=in[i][offset];
		}
		return out;
	}



	/**
	 * This method sums all the rater volumes and returns
	 * the result as a new volume.  Cropping this volume
	 * will ensure that all nonzero voxels for all raters
	 * are preserved after the cropping
	 * @return
	 */
	private ImageData volumeToCrop(){
		ImageData added = ratervols.getImageDataList().get(0).clone();
		int rows=added.getRows();
		int cols=added.getCols();
		int slices=added.getSlices();

		for(int l = 1; l<ratervols.getImageDataList().size(); l++){
			ImageData vol = ratervols.getImageDataList().get(l);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						added.set(i,j,k,added.getInt(i,j,k)+vol.getInt(i,j,k));
					}
				}
			}
		}
		return added;
	}


	public void writeVolumes(List<ImageData> volumes){
		File dir = new File("/home/john/Desktop/truthVols");
		for(int i=0; i<volumes.size(); i++){
			rw.write(volumes.get(i), dir);
		}
	}

	public void setPreserveRaterImages(boolean flag){
		preserveRaterImages = flag;
	}
	public void setPreserveProbImages(boolean flag){
		preserveProbImages = flag;
	}
}
