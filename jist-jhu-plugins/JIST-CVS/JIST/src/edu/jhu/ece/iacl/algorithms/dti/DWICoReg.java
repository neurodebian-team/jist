package edu.jhu.ece.iacl.algorithms.dti;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.algorithms.registration.AlgorithmRegOAR3D;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;

import java.util.ArrayList;
import java.util.List;


public class DWICoReg {
	List<ImageData> dwvolumes;
	ArrayList<ArrayList<ImageData>> alldwvolumes;
	ImageData strucVol;
	TransMatrix  lasttrans;
	String RegTarget;
	String RegMeth;
	int[] indexb0={0};
	FileInfoBase[] finfo;
	ArrayList<double[][]> DWItransformations;
	int transindex=0;
	double[][] structransformation;
	ImageData meanb0;

	int[] NDWb0 = {0};


	public DWICoReg(ArrayList<ImageData> dwvolumes,ImageData strucVol,String RegTarget,int[] indexb0){
		this.dwvolumes=dwvolumes;
		this.RegTarget=RegTarget;
		this.indexb0=indexb0;
		this.strucVol=strucVol;
		DWItransformations = new ArrayList<double[][]>(dwvolumes.size());
	}


	public DWICoReg(ArrayList<ArrayList<ImageData>> alldwvolumes, ImageData strucVol,String RegTarget,int[] indexb0,boolean radar){
		this.alldwvolumes=alldwvolumes;
		this.RegTarget=RegTarget;
		this.indexb0=indexb0;
		this.strucVol=strucVol;
		int N=0;
		for(ArrayList<ImageData> d : alldwvolumes){
			N=N+d.size();
		}
		DWItransformations = new ArrayList<double[][]>(N);
		System.out.println(getClass().getCanonicalName()+"\t"+"Found " + N + "DW volumes");
	}


	public void setRegMeth(String regmeth){
		this.RegMeth=regmeth;
	}


	public void setNDWb0(int[] N){
		NDWb0=N;
	}


	public ArrayList<ArrayList<ImageData>> run(){
		ArrayList<ArrayList<ImageData>> list = null;
		if(RegMeth.equals("FLIRT")){
			initTransforms1();
			if(RegTarget.equals("First B0")){
				list = new ArrayList<ArrayList<ImageData>>(1);
				list.add(regFLIRTB0(dwvolumes, indexb0[0],NDWb0[0]));
			}else if(RegTarget.equals("Structural Volume")){
				list = new ArrayList<ArrayList<ImageData>>(1);
				list.add(regFLIRTStruct(indexb0[0]));
			}else{
				System.err.println(getClass().getCanonicalName()+"Invalid RegTarget - default to First b0");
				list = new ArrayList<ArrayList<ImageData>>(1);
				list.add(regFLIRTB0(dwvolumes, indexb0[0],NDWb0[0]));
			}
		}else if(RegMeth.equals("RADAR")){
			initTransforms2();
			list = regRADAR(indexb0);
		}else{
			System.err.println(getClass().getCanonicalName()+"Invalid Registration");
			list = null;
		}

		return list;
	}

	public ImageData getStruc(){
		return strucVol;
	}

	public ImageData getb0(){
		return meanb0;
	}
	public double[][] getStrucTransform(){
		return structransformation;
	}
	public ArrayList<double[][]> getDWITransform(){
		return DWItransformations;
	}


	public void initTransforms1(){
		structransformation = new double[][]{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};
		for(int i=0; i<dwvolumes.size(); i++){
			DWItransformations.add(new double[][]{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}});
		}
	}

	public void initTransforms2(){
		structransformation = new double[][]{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};
		for(ArrayList<ImageData> list : alldwvolumes){
			for(int i=0; i<list.size(); i++){
				DWItransformations.add(new double[][]{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}});
			}
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"After init: "+ DWItransformations.size());
	}

//	 Coregistration with a specified diff-weighted volume (with index 'indexb0') as the target.
	public ArrayList<ImageData> regFLIRTB0(List<ImageData> dwvolumes, int indexb0, int numvolsb0){

		//check to see if numvolumes from b0 is consistent with the number of input volumes
		System.out.println(getClass().getCanonicalName()+"\t"+"Have " +dwvolumes.size()+" volumes, but " + numvolsb0 + " bvalues");
		if(dwvolumes.size()!=numvolsb0){
			System.out.println(getClass().getCanonicalName()+"\t"+"Warning: Number of volumes inconsistent with number of bvalues!");
		}
		ArrayList<ImageData> out = new ArrayList<ImageData>(dwvolumes.size());

		//get Fileinfo (from b0)
		finfo = dwvolumes.get(indexb0).getModelImageCopy().getFileInfo();

		//add b0 to the list..
		out.add(indexb0,dwvolumes.get(indexb0));

		//Register DW vols to b0
		for(int i=0; i<dwvolumes.size(); i++){

			if(i!=indexb0){
				System.out.println(getClass().getCanonicalName()+"\t"+"Registering the: " + i + "th volume");
				out.add(i,applyFLIRT(out.get(indexb0),dwvolumes.get(i),finfo,i));
				DWItransformations.set(transindex+i, TransMatrix2Array(lasttrans));
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Finished! " + out.size() + "of " + numvolsb0+ " volumes complete");

			System.out.println(getClass().getCanonicalName()+"\t"+"");
		}
		if(alldwvolumes!=null){
			transindex+=dwvolumes.size();
		}
		return out;
	}


	public ImageData regStrucToB0(int indexb0){
		//Register struct to b0
		strucVol = applyFLIRT(dwvolumes.get(indexb0),strucVol,finfo,-1);
		strucVol.setName("StructuralVolume");
		structransformation = TransMatrix2Array(lasttrans);

		return strucVol;
	}


	public ImageData regStrucToB0_rdr(int indexb0){
		//Register struct to b0
		strucVol = applyFLIRT(alldwvolumes.get(0).get(indexb0),strucVol,finfo,-1);
		strucVol.setName("StructuralVolume");
		structransformation = TransMatrix2Array(lasttrans);

		return strucVol;
	}


	/*
	 * BENNETT LOOK HERE
	 */
	public ArrayList<ArrayList<ImageData>> regRADAR(int[] indexb0){

		if(alldwvolumes==null){
			System.err.println(getClass().getCanonicalName()+"Invalid input");
			return null;
		}

		ArrayList<ImageData> thisdat;
		meanb0 = alldwvolumes.get(0).get(indexb0[0]).clone();

		//register other b0's to first b0 
		for(int i=1; i<alldwvolumes.size(); i++){
			FileInfoBase[] finfo = alldwvolumes.get(0).get(indexb0[0]).getModelImageCopy().getFileInfo();
			thisdat = alldwvolumes.get(i);
			thisdat.set(indexb0[i], applyFLIRT(alldwvolumes.get(0).get(indexb0[0]),alldwvolumes.get(i).get(indexb0[i]),finfo,indexb0[i]));

			//RESET THE iTH ELEMENT OF alldwvolumes TO THE REGISTERED VOLUME
			//ALSO REUSE thisdat TO REGISTER THE VOLMES
			alldwvolumes.set(i, thisdat);

			meanb0 = addVolumes(meanb0,thisdat.get(indexb0[0]));
			meanb0 = volumesDivByConst(meanb0,alldwvolumes.size());
		}

		transindex=0;

		//register all dw-volumes to their corresponding b0 images (all of which are co-registered)
		for(int i=0; i<alldwvolumes.size(); i++){
			System.out.println(getClass().getCanonicalName()+"\t"+"Coregistration on volume: " + (i+1));
			alldwvolumes.set(i, regFLIRTB0(alldwvolumes.get(i),indexb0[i],NDWb0[i]));
		}
		return alldwvolumes;
	}


	private ImageData addVolumes(ImageData vol1, ImageData vol2){
		int rows = vol1.getRows();
		int cols = vol1.getCols();
		int slices = vol1.getSlices();
		ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",rows,cols,slices);
		double tmp1,tmp2;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					tmp1=vol1.getDouble(i, j, k);
					tmp2=vol2.getDouble(i, j, k);
					resultVol.set(i, j, k, tmp1+tmp2);
				}
			}
		}
		return resultVol;
	}

	private ImageData volumesDivByConst(ImageData vol1,double d){
		int rows = vol1.getRows();
		int cols = vol1.getCols();
		int slices = vol1.getSlices();
		ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",rows,cols,slices);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					resultVol.set(i, j, k, vol1.getDouble(i, j, k)/d);
				}
			}
		}
		return resultVol;
	}



//	 Coregistration with a specified diff-weighted volume as the target.
//	 Coregistration with the structural volume as the target.
	public ArrayList<ImageData> regFLIRTStruct(int indexb0){
		ArrayList<ImageData> out = new ArrayList<ImageData>();

//		get Fileinfo (from struct)
		finfo = strucVol.getModelImageCopy().getFileInfo();

		//Register b0 to struc
		out.add(indexb0,applyFLIRT(strucVol,dwvolumes.get(indexb0),finfo,indexb0));
		DWItransformations.set(indexb0, TransMatrix2Array(lasttrans));

		//Register DW vols to b0
		for(int i=0; i<dwvolumes.size(); i++){

			if(i!=indexb0){
				System.out.println(getClass().getCanonicalName()+"\t"+"Registering the: " + i + "th volume");
				out.add(i,applyFLIRT(out.get(indexb0),dwvolumes.get(i),finfo,i));
				DWItransformations.set(i, TransMatrix2Array(lasttrans));
			}
		}
		return out;
	}


	/*
	 * BENNETT LOOK HERE
	 */
	private ImageData applyFLIRT(ImageData target, ImageData matchImage, FileInfoBase[] finfo, int i){

		//Things I picked arbitrarily (for now)
		float coarseRateX = 10f;
		float fineRateX = 6f;
		float coarseRateY = 10f;
		float fineRateY = 6f;
		float coarseRateZ = 10f;
		float fineRateZ = 6f;
		float minAngx = -20f;
		float maxAngx = 20f;
		float minAngy = -20f;
		float maxAngy = 20f;
		float minAngz = -20f;
		float maxAngz = 20f;

//		float minAngx = -15f;
//		float maxAngx = 15f;
//		float minAngy = -15f;
//		float maxAngy = 15f;
//		float minAngz = -15f;
//		float maxAngz = 15f;

		TransMatrix finalMatrix;

		// Push DOF through as an option in a future release 
		AlgorithmRegOAR3D reg3 = new AlgorithmRegOAR3D(target.getModelImageCopy(), matchImage.getModelImageCopy(), AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED,
				6, AlgorithmTransform.WSINC, -minAngx, maxAngx, coarseRateX, fineRateX, minAngy, maxAngy, coarseRateY, fineRateY,
				minAngz, maxAngz,coarseRateZ, fineRateZ, true, true, false, 10, 2, 3);

		reg3.setMultiThreadingEnabled(false);
		reg3.runAlgorithm();
		finalMatrix = reg3.getTransform();
		int xdimA = target.getModelImageCopy().getExtents()[0];
		int ydimA = target.getModelImageCopy().getExtents()[1];
		int zdimA = target.getModelImageCopy().getExtents()[2];
		float xresA = target.getModelImageCopy().getFileInfo(0).getResolutions()[0];
		float yresA = target.getModelImageCopy().getFileInfo(0).getResolutions()[1];
		float zresA = target.getModelImageCopy().getFileInfo(0).getResolutions()[2];

		AlgorithmTransform transform = new AlgorithmTransform(matchImage.getModelImageCopy(), finalMatrix, AlgorithmTransform.WSINC, xresA, yresA, zresA,
				xdimA, ydimA, zdimA, true, false, false);

		transform.setUpdateOriginFlag(true);
		transform.run();
		ModelImage resultImage = transform.getTransformedImage();

		lasttrans = finalMatrix;
		ImageDataMipav regvol = new ImageDataMipav(resultImage);
		if(i==-1){
			regvol.setName("StructuralVolume_transform");
		}else{
			regvol.setName("Volume_"+i+"_transform");
		}

		regvol.getModelImageCopy().setFileInfo(finfo);

		//set to null pointers that will not be used again.
		transform=null;
		resultImage=null;

		return regvol;
	}


	public double[][] TransMatrix2Array(TransMatrix A){
		double[][] transout = new double[A.getDim()][A.getDim()];

		for(int i=0; i<transout.length; i++){
			for(int j=0; j<transout[0].length; j++){
				transout[i][j]=A.get(i, j);
			}
		}
		return transout;
	}
}
