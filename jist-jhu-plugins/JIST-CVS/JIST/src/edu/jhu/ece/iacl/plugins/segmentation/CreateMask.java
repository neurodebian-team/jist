package edu.jhu.ece.iacl.plugins.segmentation;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class CreateMask extends ProcessingAlgorithm {
	private ParamVolume volParam;
	private ParamFloat threshParam;

	protected ParamVolume resultVolParam;
	protected ParamVolume brainResultVol;

	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Threshold an image to create a mask. Also outputs the original image with the mask applied.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("IACL");
		inputParams.setCategory("Segmentation");
		inputParams.setLabel("Threshold");
		inputParams.setName("Threshold");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(volParam=new ParamVolume("Volumes",null,-1,-1,-1,-1));
		inputParams.add(threshParam=new ParamFloat("Mask Threshhold",(float)100));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVolParam=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
		outputParams.add(brainResultVol=new ParamVolume("Brain Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		ImageData vol=volParam.getImageData();
		int rows=vol.getRows();
		int cols=vol.getCols();
		int slices=vol.getSlices();
		ImageData resultVol=new ImageDataUByte(rows,cols,slices);
		resultVol.setHeader(vol.getHeader());
		ImageData brainVol=vol.clone();
		double threshhold = threshParam.getFloat();
		int count=0;

		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				for (int k=0; k<slices; k++) {
					if (vol.getFloat(i,j,k) >= threshhold) {
						resultVol.set(i,j,k,1);
						count++;
					}
					else
						brainVol.set(i,j,k,0);
				}
			}
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"Number of voxels: "+count);
		resultVol.setName("mask_volume");
		
		resultVolParam.setValue(resultVol);
		brainVol.setName("mask_brain");
		brainResultVol.setValue(brainVol);
	}
}
