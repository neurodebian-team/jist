package edu.jhu.ece.iacl.plugins.dti;

import java.io.IOException;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;
import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.algorithms.registration.AlgorithmRegOAR3D;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;


public class ComputeMeanB0 extends ProcessingAlgorithm {
	//output params
	private ParamVolume meanb0;

	//input params
	private ParamVolumeCollection inputFiles;		// Slabs
	private ParamFile bvaluesTable;		// .b file with a list of b-values
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions
	private ParamBoolean register;		// register the inputs before averaging?

	// Controls the threaded nature of the OAR Registration.
	private ParamBoolean OARThreadedBool;


	//Variables

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.14 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Registers (6 dof) and averages all minimally diffusion weighted (b0) to the first b0. Registration is optional.";
	private static final String longDescription = 	"Inputs:\n" +
													"\t-Input Volume or Slab Collection (4D volumes)\n" +
													"\t-Table of diffusion weighting directions (file)\n" +
													"\t-Table of b-values (file)\n" +
													"Outputs:\n" +
													"\t-Mean B0 volume (3D volume)";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Mean B0");
		inputParams.setName("Mean_B0");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setAffiliation("Johns Hopkins University");		
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + "\n" +longDescription);
		info.setVersion(revnum);	
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		// Input Parameters
		inputParams.add(inputFiles= new ParamVolumeCollection("Input Slab Collection"));
		inputFiles.setLoadAndSaveOnValidate(false);
		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(register = new ParamBoolean("Register b0's ?",true));


		OARThreadedBool = new ParamBoolean("Multithreading for Registration", false);
		OARThreadedBool.setDescription("Set to false by default, this parameter controls the multithreaded behavior of the linear registration.");
		inputParams.add(OARThreadedBool);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(meanb0 = new ParamVolume("Mean B0 volume"));		
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute() {
			JistLogger.logError(JistLogger.INFO, "Mean B0: START");
			float [][]bs=null;		
			TextFileReader text = new TextFileReader(bvaluesTable.getValue());
			try {
				bs = text.parseFloatFile();
			} catch (IOException e) 
			{
				JistLogger.logError(JistLogger.WARNING, "Mean B0: Unable to parse b-file.");
				throw new RuntimeException("Unable to parse b-file");
			}

			float [][]grads=null;
			text = new TextFileReader(gradsTable.getValue());
			try {
				grads  = text.parseFloatFile();
			} catch (IOException e) { 
				JistLogger.logError(JistLogger.WARNING, "Mean B0: Unable to parse grad-file.");
				throw new RuntimeException("Unable to parse grad-file");
			}

			// If there are 4 columns in the gradient table, remove the 1st column (indecies)
			if(grads[0].length==4) {
				float [][]g2 = new float[grads.length][3];
				for(int i=0;i<grads.length;i++) 
					for(int j=0;j<3;j++)
						g2[i][j]=grads[i][j+1];
				grads=g2;
			}

			if(grads[0].length!=3){
				JistLogger.logError(JistLogger.WARNING, "Mean B0: Invalid gradient table. Must have 3 or 4 columns.");
				throw new RuntimeException("MeanB0: Invalid gradient table. Must have 3 or 4 columns.");
			}
				
			if(bs[0].length!=1){
				JistLogger.logError(JistLogger.WARNING, "Mean B0: Invalid b-value table. Must have 1 column.");
				throw new RuntimeException("MeanB0: Invalid b-value table. Must have 1 column.");
			}
			float []bval = new float[bs.length];
			for(int i=0;i<bval.length;i++)
				bval[i]=bs[i][0];

			this.setTotalUnits(bval.length);
			String baseName = null;
			ImageData mean = null;
			float count =0;
			int firstb0index = -1;

			ModelImage volA = null; 
			ImageData vol=null;
			for(int i=0;i<inputFiles.size();i++){
				if(bval[i]==0 && !(grads[i][0]==100 && grads[i][1]==100 && grads[i][2]==100)) {
					count ++;
					vol = inputFiles.getParamVolume(i).getImageData();
					ImageHeader hdr = vol.getHeader();
					if(mean==null){
						mean = new ImageDataFloat(vol);					
						firstb0index = i;
						volA = vol.getModelImageCopy();
					} else {
						// run the registration
						if(register.getValue()){
							JistLogger.logError(JistLogger.INFO, "Mean B0: Registering volume: " + count);
							//System.out.println(getClass().getCanonicalName()+"\t"+"Registering volume: " + count);
							ModelImage volB = vol.getModelImageCopy();
							vol = register(volA,volB);							
							volB.disposeLocal();
							vol.setHeader(hdr);
						}

						ImageDataMath.addFloatImage(mean, vol);
					}
					vol.dispose();
					inputFiles.getParamVolume(i).dispose();			
				}				
				this.setCompletedUnits(i+1);
			}
			volA.disposeLocal();
			ImageDataMath.scaleFloatValue(mean, (float)(1.0f/count));				
			mean.setName(mean.getName()+"_meanb0");	
			meanb0.setValue(mean);
			JistLogger.logError(JistLogger.INFO, "Mean B0: FINISH");
		}


		private ImageDataMipav register(ModelImage target, ModelImage src){
			float xresA = src.getResolutions(0)[0];
			float yresA = src.getResolutions(0)[1];
			float zresA = src.getResolutions(0)[2];
			int xdimA = src.getExtents()[0];
			int ydimA = src.getExtents()[1];
			int zdimA = src.getExtents()[2];
			AlgorithmRegOAR3D reg = new AlgorithmRegOAR3D(target,src,
					AlgorithmCostFunctions.NORMALIZED_XCORRELATION_SMOOTHED,6,AlgorithmTransform.TRILINEAR,
					-20,20,10,5,-20,20,10,5,-20,20,10,5,true,true,false,10,2,3);

			reg.setMultiThreadingEnabled(OARThreadedBool.getValue());
			reg.runAlgorithm();

			TransMatrix finalMatrix = reg.getTransform();
			AlgorithmTransform transform = new AlgorithmTransform(src, finalMatrix, AlgorithmTransform.TRILINEAR,
					xresA, yresA, zresA,xdimA, ydimA, zdimA, true, false, false);
			transform.run();
			ImageDataMipav out = new ImageDataMipavWrapper(transform.getTransformedImage());
			reg.finalize(); reg=null;
			transform.finalize(); transform = null;

			return out;
		}
	}
}
