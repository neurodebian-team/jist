package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


public class MedicAlgorithmFlipVectorVolume extends ProcessingAlgorithm{
	//input params
	private ParamVolume vecvolin;
	private ParamOption flipDim;

	//output param
	private ParamVolume vecvolout;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Flips (i.e. negates) the component of the input vector volume.  " +
			"Expects a NxMxLx3 dimensional volume.  To flip the {first, second, third} components of the volume," +
			"select (X, Y, Z) as the input \"Component to Flip\"";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vecvolin = new ParamVolume("Input Vector Volume",null,-1,-1,-1,3));
		String[] flipoptions = {"X","Y","Z"};
		inputParams.add(flipDim = new ParamOption("Component to Flip",flipoptions));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Flip Vector Volume");
		inputParams.setName("Flip_Vector_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(vecvolout = new ParamVolume("Flipped Vector Volume",null,-1,-1,-1,3));
	}


	protected void execute(CalculationMonitor monitor) {
		ImageData vol = vecvolin.getImageData();
		for(int i=0;i<vol.getRows();i++){
    		for(int j=0;j<vol.getCols();j++){
    			for(int k=0;k<vol.getSlices();k++){
    				vol.set(i,j,k,flipDim.getIndex(),-vol.get(i,j,k,flipDim.getIndex()).doubleValue());
    			}
    		}
    	}   
		vecvolout.setValue(vol);
	}
}
