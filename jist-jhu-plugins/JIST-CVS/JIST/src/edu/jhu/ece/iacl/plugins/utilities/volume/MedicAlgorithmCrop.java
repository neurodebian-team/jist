package edu.jhu.ece.iacl.plugins.utilities.volume;

import javax.vecmath.Point3i;

import edu.jhmi.rad.medic.utilities.CropParameters;
import edu.jhmi.rad.medic.utilities.CubicVolumeCropper;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * Image Cropper
 * @author Navid Shiee
 *
 */
public class MedicAlgorithmCrop extends ProcessingAlgorithm{
	private ParamVolume inputVol;

	private ParamVolume outputVol;

	private ParamInteger borderSize;

	private ParamDouble threshold;
	private ParamPointInteger offset;
	private ParamPointInteger dim;
	private ParamPointInteger dimNew;
	
	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Crops an image.  The resulting output \"Cropped Image\" contains all voxels " +
			"whose intensities are greater than the input \"Threshold\".  Also ouputs the dimensions of the original " +
			"and cropped volumes.  The dimensions of the original images should be used with the \"Uncrop\" module";
	private static final String longDescription = "";


	/*
	 * Create input parameters for Cropper :cubic volume and borderSize
	 */
	public MedicAlgorithmCrop(){
		setName("Multi-Crop");
	}


	protected void createInputParameters(ParamCollection inputParams) {
		inputVol = new ParamVolume("Volume", null,-1,-1,-1,-1);
		borderSize = new ParamInteger("BorderSize", 0, Integer.MAX_VALUE);
		borderSize.setValue(3);
		threshold = new ParamDouble("Threshold", 0, Integer.MAX_VALUE);
		threshold.setValue(0);
		inputParams.add(inputVol);
		inputParams.add(threshold);
		inputParams.add(borderSize);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Crop");
		inputParams.setName("Crop");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Navid Shiee","","iacl.ece.jhu.edu"));
		info.setWebsite("iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	/*
	 * Create output parameters for Image Cropper: cubic volume and extents
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
/*		for (int i=0; i<6; i++){
			borders[i]= new ParamInteger("border",0,Integer.MAX_VALUE);
			outputParams.add(borders[i]);
		}*/
		outputVol = new ParamVolume(null,-1,-1,-1,-1);
		outputVol.setLabel("Cropped Image");
		outputVol.setName("cropped");
		outputParams.add(outputVol);
		
		offset=new ParamPointInteger("Offset");
		dim=new ParamPointInteger("Original Dimensions");
		dimNew=new ParamPointInteger("New Dimensions");
		
		outputParams.add(offset);
		outputParams.add(dim);
		outputParams.add(dimNew);
	}


	/*
	 * Execute Image Cropper
	 */
	protected void execute(CalculationMonitor monitor) {
		CubicVolumeCropper cropper = new CubicVolumeCropper();
		monitor.observe(cropper);
		
		ImageData vol=inputVol.getImageData();
		
		Point3i origDim = new Point3i(vol.getRows(),vol.getCols(),vol.getSlices());
		ImageData cvol=cropper.crop(vol,threshold.getInt(), borderSize.getInt());
		CropParameters params=cropper.getLastCropParams();
		offset.setValue(new Point3i(params.xmin,params.ymin,params.zmin));
		dimNew.setValue(new Point3i(params.getCroppedRows(),params.getCroppedCols(),params.getCroppedSlices()));
		dim.setValue(origDim);
		System.out.println("###########################??"+cvol.getName());
		ImageDataMipav mvol=new ImageDataMipav(cvol);
		
		mvol.getModelImageCopy().copyFileTypeInfo(vol.getModelImageCopy());
		System.out.println("###########################??"+mvol.getName());
		outputVol.setValue(cvol);
	}
}
