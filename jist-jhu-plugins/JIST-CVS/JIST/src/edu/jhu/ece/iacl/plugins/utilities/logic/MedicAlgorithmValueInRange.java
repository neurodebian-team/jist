package edu.jhu.ece.iacl.plugins.utilities.logic;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;


public class MedicAlgorithmValueInRange extends ProcessingAlgorithm{
	protected ParamDouble lowerBound;
	protected ParamDouble upperBound;
	protected ParamDouble value;
	protected ParamBoolean inclusive;
	protected ParamBoolean assertTrue;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(value=new ParamDouble("Value"));
		inputParams.add(lowerBound=new ParamDouble("Lower Bound"));
		inputParams.add(upperBound=new ParamDouble("Upper Bound"));
		inputParams.add(inclusive=new ParamBoolean("Include Bounds",true));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Value in Range");
		inputParams.setName("Value_in_Range");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(assertTrue=new ParamBoolean("Assertion"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		double val=value.getDouble();
		double lower=lowerBound.getDouble();
		double upper=upperBound.getDouble();
		if(inclusive.getValue()){
			assertTrue.setValue((val>=lower&&val<=upper));
		} else {
			assertTrue.setValue((val>lower&&val<upper));
		}
	}
}
