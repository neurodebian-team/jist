package edu.jhu.ece.iacl.algorithms.manual_label.simulation;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import javax.vecmath.Point2i;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Simulates a random surface rater characterized by their
 * 1) Probability of error
 * 2) Probability of boundary error (vs global error)
 * 3) Confusion Matrix (for global error)
 * 4) Probability that boundary errors will occur on particular boundaries
 * 5) Probability that the boundary will "shrink" or "grow".
 * 
 * Note: A boundary between label i and j (i!=j, i<j) is represented as 
 * a Point2i with p(1)=i, p(2)=j.  
 * Note: We adopt the convention that p(1)<p(2).
 * 
 * If this boundary 'shrinks' it means that label j replaces label i.
 * If this boundary 'grows' it means that label i replaces label j.
 * 
 * @author John Bogovic 
 *
 */
public class VolumeRater {
	
	private ImageData truthvol;
	private int Xdim;
	private int Ydim;
	private int Zdim;
	private ImageData ratervol;
	private GridPt.Connectivity connectivity;
	
	ConfusionMatrix confusion;
	float[] perError;			
	
	private float totReliability;		//global probability of error
	private float[] growPerProb;		//probability that a certain boundary will grow or shrink.
	private float[] boundErrProb;		//probability that, given an error, it will occur on a certain boundary 
	private float[] boundErrProbCum;	//cumulative probability of the above - useful for calculations      
	
	
	private int[] labels;
	private String outputname;
	private ArrayList<Point2i> boundaries;						//boundaries corresponding to growPerProb
	private ArrayList<Integer> boundarySizes;					//the number of voxels corresponding to each boundary
	private HashMap<Point2i, ArrayList<GridPt>> boundverts;	//stores the vertex indices beloning to each boundary
	private HashSet<GridPt> changedvoxs;
	private int numboundverts;
	
	private boolean missingBoundaryWarning = true;
	
	private Random randnum = new Random();
	
	public VolumeRater(ImageData truthvol){
		this.truthvol=truthvol;
	}
	
	public VolumeRater(ImageData truthvol, float[][] confusion){
		labels = getLabelList(truthvol);
		this.truthvol=truthvol;
		Xdim = truthvol.getRows();
		Ydim = truthvol.getCols();
		Zdim = truthvol.getSlices();
		this.confusion=new ConfusionMatrix(confusion,labels);
	}
	
	public VolumeRater(ImageData truthvol, float[] perError, float totReliability){
		this.truthvol=truthvol;
		Xdim = truthvol.getRows();
		Ydim = truthvol.getCols();
		Zdim = truthvol.getSlices();
		this.perError=perError;
		this.totReliability=totReliability;
		labels = getLabelList(truthvol);
	}
	
	public VolumeRater(ImageData truthvol, float[][] confusion, float[] perError, float totReliability){

		labels = getLabelList(truthvol);
		this.confusion=new ConfusionMatrix(confusion,labels);
		this.truthvol=truthvol;
		Xdim = truthvol.getRows();
		Ydim = truthvol.getCols();
		Zdim = truthvol.getSlices();
		this.perError=perError;
		this.totReliability=totReliability;
	}
	
	public VolumeRater(ImageData truthvol, float totReliability, ArrayList<Point2i> boundaries, float[] boundErrProb, float[] growPerProb){
		this.truthvol=truthvol;
		Xdim = truthvol.getRows();
		Ydim = truthvol.getCols();
		Zdim = truthvol.getSlices();
		this.totReliability=totReliability;
		
		this.boundaries=boundaries;
		this.boundErrProb=boundErrProb;
		this.growPerProb=growPerProb;
		labels = getLabelList(truthvol);
	}
	
	public VolumeRater(ImageData truthvol, float totReliability, float[][] confusion, ArrayList<Point2i> boundaries, float[] boundErrProb, float[] growPerProb){
		this.truthvol=truthvol;
		Xdim = truthvol.getRows();
		Ydim = truthvol.getCols();
		Zdim = truthvol.getSlices();
		this.totReliability=totReliability;
		
		this.boundaries=boundaries;
		this.boundErrProb=boundErrProb;
		this.growPerProb=growPerProb;
		
		
		labels = getLabelList(truthvol);
		this.confusion=new ConfusionMatrix(confusion,labels);
	}
	public VolumeRater(ImageData truthvol, float totReliability, int[] labels, float[][] confusion){
		this.truthvol=truthvol;
		Xdim = truthvol.getRows();
		Ydim = truthvol.getCols();
		Zdim = truthvol.getSlices();
		this.totReliability=totReliability;
		
		this.labels = labels;
		this.confusion=new ConfusionMatrix(confusion,labels);
	}
	
	public void setName(String name){
		outputname = name;
	}
	public void setSeed(long seed){
		randnum = new Random(seed);
	}
	public void setRand(Random r){
		randnum = r;
	}
	public void setBoundaries(ArrayList<Point2i> boundaries){
		this.boundaries=boundaries;
	}
	public ArrayList<Integer> getBoundarySizes(){
		return boundarySizes;
	}
	public ImageData getLastRaterVolume(){
		return ratervol;
	}
	public void setConnectivity(byte c){
		if(c==6){
			connectivity=GridPt.Connectivity.SIX;
		}else if(c==18){
			connectivity=GridPt.Connectivity.EIGHTEEN;
		}else if(c==26){
			connectivity=GridPt.Connectivity.TWENTYSIX;
		}else{
			System.out.println("jist.plugins"+"\t"+"Warning: Invalid connectivity");
		}
	}
	public void setConnectivity(GridPt.Connectivity c){
		connectivity=c;
	}
	
	/**
	 * clean up everything related to the rater data to 
	 * prepare to create a new rater
	 */
	public void reset(){
		if(ratervol!=null){
			ratervol.dispose();
			ratervol = null;
		}
		boundverts = null;
		boundErrProbCum=null;
		System.gc();
		//give the GC a chance
		try{ Thread.sleep(1, 0); }catch(Exception e){}
	}
	public ImageData genConfRaterVolume(){
		ImageData out =  genConfusionRater(truthvol,confusion);
		out.setName(outputname);
		out.setHeader(truthvol.getHeader().clone());
		ratervol=out;
		return out;
	}
	
	/**
	 * Returns a rater volume by passing the label at each 
	 * point through a confusion matrix.
	 * 
	 * @param vol 'true' labeled volume
	 * @param confusion confusion matrix
	 * @return the 'confused' volume
	 */
	public static ImageData genConfusionRater(ImageData vol, ConfusionMatrix confusion){
		System.out.println("jist.plugins"+"\t"+"Generating rater with confusion matrix:");
		ImageData out = vol.clone();
		/* Generate rater data based on confusion matrix */
		confusion.printConfusion();
		for(int k=0; k<vol.getSlices(); k++){
			for(int j=0; j<vol.getCols(); j++){
				for(int i=0; i<vol.getRows();i++){
					out.set(i, j, k, confusion.genNextLabel(vol.getInt(i, j, k)));
				}
			}
		}
		System.out.println("jist.plugins"+"\t"+"Finished");
		return out;
	}
	
	/**
	 * Gets a list of all of the labels (stored as an int array)
	 * that are present on an input volume
	 * 
	 * @param vol labeled volume
	 * @return array containing all the lables found in this volume
	 */
	public static int[] getLabelList(ImageData vol){
		ArrayList<Integer> list = new ArrayList<Integer>();

		for(int k=0; k<vol.getSlices(); k++){
			for(int j=0; j<vol.getCols(); j++){
				for(int i=0; i<vol.getRows();i++){
					if(!list.contains(vol.getInt(i, j, k))){
						list.add(vol.getInt(i, j, k));
					}
				}
			}
		}
		list.trimToSize();
		int[] out = new int[list.size()];
		Iterator<Integer> it = list.iterator();
		int j=0;
		while(it.hasNext()){
			out[j]=it.next().intValue();
			j++;
		}
		return out;
	}
	
	/**
	 * Computes boundaries, and boundary vertices
	 */
	public void computeBoundaryInformation(){
		
		numboundverts = 0;
		
		int lab=-1;
		int l=-1;
		
		boundverts = new HashMap<Point2i,ArrayList<GridPt>>();
		GridPt[] nbrtable = null;
		boolean foundboundvert = false;
		int nbrlab=-1;
		for(int k=0; k<truthvol.getSlices(); k++){
			for(int j=0; j<truthvol.getCols(); j++){
				for(int i=0; i<truthvol.getRows();i++){


					lab = truthvol.getInt(i,j,k);

					/* is it a boundary voxel? */
					foundboundvert = false;
					l = 0;
					nbrtable = getNeighbors(i,j,k,this.connectivity, Xdim, Ydim, Zdim);
					while(!foundboundvert && l<nbrtable.length){
						if(nbrtable[l]!=null){
							nbrlab = truthvol.getInt(nbrtable[l].x, nbrtable[l].y, nbrtable[l].z);
							if(nbrlab!=lab){ //we've found a boundary vertex
								foundboundvert = true;

								Point2i bound;
								if(nbrlab<lab){
									bound = new Point2i(nbrlab,lab);
								}else{
									bound = new Point2i(lab,nbrlab);
								}

								if(boundaries.contains(bound)){
									if(!boundverts.containsKey(bound)){
										boundverts.put(bound, new ArrayList<GridPt>());
										boundverts.get(bound).add(new GridPt(i,j,k));
									}else{
										boundverts.get(bound).add(new GridPt(i,j,k));
									}
								}else if(missingBoundaryWarning){
									System.out.println("jist.plugins"+"\t"+"Warning: Found boundary with no error probability data!");
									missingBoundaryWarning=false;
								}
								numboundverts++;
							}else{
								l++;
							}
						}else{
							l++;
						}
					}
				}
			}
		}
		
	}
	

	
	/**
	 * Returns a simulated rater volume
	 * @return the simulated rater volume
	 */
	public ImageData genPerimRaterVolume(){
		ImageData rvol = truthvol.clone();
		if(boundverts==null){
			computeBoundaryInformation();
		}
		//set boundErrProbCum
		if(boundErrProbCum==null){
			boundErrProbCum=new float[boundErrProb.length];
			boundErrProbCum[0]=boundErrProb[0];
			for(int i=1; i<boundErrProb.length; i++){
				boundErrProbCum[i]=boundErrProbCum[i-1]+boundErrProb[i];
			}
		}
		
		//compute number of vertices that will need their labels changed
//		int V = truthvol.getRows()*truthvol.getCols()*truthvol.getSlices();
		
		changedvoxs = new HashSet<GridPt>();
		
		int tochange = Math.round((1-totReliability)*numboundverts);
		int changed = 0 ;
		int changedprev = 0;
		int bound2perturb=-1;
		GridPt vert2perturb=null;
		
		int maxiters = 20*tochange;		//just to be safe
		int iters = 0;
		
		while(changed<tochange && iters<maxiters){
			
			/* DEBUG */
//			if(iters<10){
//				if(changedprev!=changed){
//					writeBoundaryVertexSurface(iters);
//					writeIntermediateRaterSurface(rdat,iters);
//				}
//			}
//			changedprev = changed;
//			
//			System.out.println("jist.plugins"+"\t"+"BOUNDARY");
//			System.out.println("jist.plugins"+"\t"+boundverts.get(boundaries.get(0)));
//			System.out.println("jist.plugins"+"\t"+"");
			
			/* END DEBUG */
			
			//choose which boundary will be perturbed
			int tries = 0;
			boolean found = false;
			while(tries<10 && !found){
				bound2perturb = drawFromCumulativeProbabilities(boundErrProbCum);

				//make sure the boundary has some vertices
				if(boundverts.get(boundaries.get(bound2perturb))!=null && boundverts.get(boundaries.get(bound2perturb)).size()>0){ 
					//pick which vertex on the boundary to perturb
					vert2perturb = boundverts.get(boundaries.get(bound2perturb)).get(drawUniform(boundverts.get(boundaries.get(bound2perturb)).size()));
					found = true;
				}
				tries++;
			}
			
//			System.out.println("jist.plugins"+"\t"+"Perturbing vertex: " + vert2perturb);
//			System.out.println("jist.plugins"+"\t"+"Is it on the boundary? " + boundverts.get(boundaries.get(bound2perturb)).contains(vert2perturb));
			
			//grow or shrink?
			
			double grow = randnum.nextDouble();
			
			if(grow>growPerProb[bound2perturb]){ //GROW
				//"Grow" means we replace a vertex with the 1st component of the Point2i boundary
				if(rvol.getInt(vert2perturb.x,vert2perturb.y,vert2perturb.z)==boundaries.get(bound2perturb).x){
					changed+=perturbLabel(rvol, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).x);
				}else if(rvol.getInt(vert2perturb.x,vert2perturb.y,vert2perturb.z)==boundaries.get(bound2perturb).y){
					changed+=perturbLabel(rvol, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).x);
				}/*else{
					System.err.println("jist.plugins"+"Warning: vertex classified as wrong boundary");
				}*/
				
				
			}else{	//SHRINK
				//"Shrink" means we replace a vertex with the 2nd component of the Point2i boundary
				if(rvol.getInt(vert2perturb.x,vert2perturb.y,vert2perturb.z)==boundaries.get(bound2perturb).x){
					changed+=perturbLabel(rvol, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).y);
				}else if(rvol.getInt(vert2perturb.x,vert2perturb.y,vert2perturb.z)==boundaries.get(bound2perturb).y){
					changed+=perturbLabel(rvol, vert2perturb,boundaries.get(bound2perturb),boundaries.get(bound2perturb).y);
				}/*else{
					System.err.println("jist.plugins"+"Warning: vertex classified as wrong boundary");
				}*/
			}
			iters++;
		}
		
		rvol.setName(outputname);
//		System.out.println("jist.plugins"+"\t"+"Vertex data: " + rvol);
		ratervol=rvol;
		return rvol;
	}
	
	/**
	 * 
	 * @param pt The Grid Point to perturb.
	 * @param newlabel - the label that needs to take over a point 
	 * @return 0 if nothing was changed, 1 if a vertex had its label changed
	 */
	private int perturbLabel(ImageData vol, GridPt pt, Point2i boundary, int newlabel){
		if(vol.getInt(pt.x, pt.y, pt.z)==newlabel){  //we have to choose a neighbor
			
			int labeltochange = Integer.MIN_VALUE;
			if(boundary.x==newlabel){
				labeltochange=boundary.y;
			}else{
				labeltochange=boundary.x;
			}
			//a vertex that we change has to have the label 'labeltochange'
			
			//make sure that a vertex with the other label exists...
			boolean doesexist = false;
			GridPt[] nbrtable = getNeighbors(pt,this.connectivity, Xdim, Ydim, Zdim);
			for(int i=0; i<nbrtable.length; i++){
				if(nbrtable[i]!=null){
					if(vol.getInt(nbrtable[i].x, nbrtable[i].y, nbrtable[i].z)==labeltochange){
						doesexist=true;
						break;
					}
				}
			}
			
			if(doesexist){
				int nbr = -1;
				boolean found = false;
				//choose a random neighbor until we get one of the 'other' label
				while(!found){
					nbr = drawUniform(nbrtable.length);
					if(nbrtable[nbr]!=null && vol.getInt(nbrtable[nbr].x, nbrtable[nbr].y, nbrtable[nbr].z)==labeltochange){
						found=true;
						break;
					}
				}
				
				if(!changedvoxs.contains(nbrtable[nbr])){
					//change the label
					//				System.out.println("jist.plugins"+"\t"+"Changing label at: " + nbrtable[nbr] + " from " + vol.getInt(nbrtable[nbr].x,nbrtable[nbr].y,nbrtable[nbr].z));
					vol.set(nbrtable[nbr].x,nbrtable[nbr].y,nbrtable[nbr].z,newlabel);
					changedvoxs.add(nbrtable[nbr]);
					updateBoundaryList(vol, nbrtable[nbr], boundary);

					return 1;
				}
			}/*else{
				System.out.println("jist.plugins"+"\t"+"Warning: misclassifed boundary vertex");
			}*/
			
		}else{	//we can change this vertex's label
			if(!changedvoxs.contains(pt)){
				vol.set(pt.x,pt.y,pt.z,newlabel);
				changedvoxs.add(pt);
				updateBoundaryList(vol, pt, boundary);
				return 1;
			}
		}
		
		return 0;
	}

	/**
	 * 
	 * @param vol Label volume
	 * @param changedvert The GridPt whose label  changed
	 * @param boundary - boundary that was affected
	 */
	private void updateBoundaryList(ImageData vol, GridPt changedvert, Point2i boundary){
		//is the changed point part of the boundary?
		//check to see if all of its neighbors are still part of it.
		ArrayList<Point2i> vertbounds = boundariesPresent(vol,changedvert);
//		System.out.println("jist.plugins"+"\t"+"boundaries at :" +changedvert);
//		System.out.println("jist.plugins"+"\t"+vertbounds);
		if(vertbounds==null){ 
			//this point is not a boundary at all anymore  - remove it 
			if(boundverts.get(boundary).contains(changedvert)){
//				System.out.println("jist.plugins"+"\t"+"Removed " + changedvert + " from boundary");
				boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(changedvert));
			}
		}else if(!vertbounds.contains(boundary)){ 
			//this point is not a part of this boundary - remove it 
			if(boundverts.get(boundary).contains(changedvert)){
//				System.out.println("jist.plugins"+"\t"+"Removed " + changedvert + " from boundary");
				boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(changedvert));
			}
		}
		
		//check to see if all its neighbors are boundaries
		GridPt[] nbrtable = getNeighbors(changedvert,this.connectivity, Xdim, Ydim, Zdim);
		for(int i=0; i<nbrtable.length; i++){
			if(nbrtable[i]!=null){
				vertbounds = boundariesPresent(vol,nbrtable[i]);
//				System.out.println("jist.plugins"+"\t"+"boundaries at :" +nbrtable[i]);
//				System.out.println("jist.plugins"+"\t"+vertbounds);
				if(vertbounds==null){ 
					//this nbr is not part of this boundary - remove it if it's there
					if(boundverts.get(boundary).contains(nbrtable[i])){
//						System.out.println("jist.plugins"+"\t"+"Removed " + nbrtable[i] + " from boundary");
						boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(nbrtable[i]));
					}
				}else{
					if(!vertbounds.contains(boundary)){ 
						//this nbr is not a part of this boundary - remove it if it's there
						if(boundverts.get(boundary).contains(nbrtable[i])){
//							System.out.println("jist.plugins"+"\t"+"Removed " + nbrtable[i] + " from boundary");
							boundverts.get(boundary).remove(boundverts.get(boundary).indexOf(nbrtable[i]));
						}
					}else{
						//this point IS part of the boundary - add it if not already present
						if(!boundverts.get(boundary).contains(nbrtable[i])){
//							System.out.println("jist.plugins"+"\t"+"Added " + nbrtable[i] + " to boundary");
							boundverts.get(boundary).add(nbrtable[i]);
						}
					}
				}
			}
		}
		boundverts.get(boundary).trimToSize();
	} //Whew!
	
	/**
	 * Computes the boundaries present at the input GridPt
	 * @param vert The GridPt
	 * @return A list of all the boundaries present at that GridPt.  Null if the point is
	 * not a boundary point
	 */
	private ArrayList<Point2i> boundariesPresent(ImageData vol, GridPt pt){
		/* is it a boundary voxel? */
		boolean foundboundvert = false;
		int j = 0;
		int lab = vol.getInt(pt.x, pt.y, pt.z);
		ArrayList<Point2i> out = null;
		GridPt[] nbrtable = getNeighbors(pt,this.connectivity, Xdim, Ydim, Zdim);
		while(!foundboundvert && j<nbrtable.length){
			if(nbrtable[j]!=null){
				int nbrlab =  vol.getInt(nbrtable[j].x, nbrtable[j].y, nbrtable[j].z);;
				if(nbrlab!=lab){ //we've found a boundary vertex
					foundboundvert = true;
					out = new ArrayList<Point2i>();
					Point2i bound;
					if(nbrlab<lab){
						bound = new Point2i(nbrlab,lab);
					}else{
						bound = new Point2i(lab,nbrlab);
					}

					if(!out.contains(bound)){
						out.add(bound);
					}
				}else{
					j++;
				}
			}else{
				j++;
			}
		}
		return out;
	}
	
	/**
	 * 
	 * @param probs - Cumulative probabilites for drawing a particular index
	 * @return the index that was chosen
	 */
	private int drawFromCumulativeProbabilities(float[] probs){

		double r = randnum.nextDouble();
		int i=0;
		if(r<probs[0]){
			return i;
		}
		for(i=1; i<probs.length; i++){
			if(r>=probs[i-1] && r<probs[i]){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Choose a random integer from 0 to N-1, inclusive
	 * @param probs - number of
	 * @return the index that was chosen
	 */
	private int drawUniform(int N){
		double r = randnum.nextDouble();
		int i=0;
		float step = 1f/(float)N;
		if(r<step){
			return i;
		}
		for(i=1; i<N; i++){
			if(r>=step*(i) && r<step*(i+1)){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Find all boundaries that exist in a volume.  A boundary is represented by an integer pair. {i,j}=={j,i} 
	 * @param surf The labeled volume
	 * @return List of boundaries
	 */
	public static ArrayList<Point2i> findBoundaries(ImageData vol, GridPt.Connectivity connectivity){
		
		int numboundverts = 0;
		int lab=-1;
		int l=-1;
		
		ArrayList<Point2i> boundaries = new ArrayList<Point2i>();
		
		boolean foundboundvert = false;
		for(int k=0; k<vol.getSlices(); k++){
			for(int j=0; j<vol.getCols(); j++){
				for(int i=0; i<vol.getRows();i++){
					
					lab = vol.getInt(i, j, k);

					/* is it a boundary voxel? */
					foundboundvert = false;
					l = 0;
					GridPt[] nbrlist = getNeighbors(i,j,k, connectivity, vol.getRows(), vol.getCols(), vol.getSlices());
					while(!foundboundvert && l<nbrlist.length){
						if(nbrlist[l]!=null){
							int nbrlab = vol.getInt(nbrlist[l].x, nbrlist[l].y, nbrlist[l].z);


							if(nbrlab!=lab){
								foundboundvert = true;

								Point2i bound;
								if(nbrlab<lab){
									bound = new Point2i(nbrlab,lab);
								}else{
									bound = new Point2i(lab,nbrlab);
								}
								if(!boundaries.contains(bound)){
									boundaries.add(bound);
								}
								numboundverts++;
							}else{
								l++;
							}
						}else{
							l++;
						}
						
					}
				}
			}
		}
		return boundaries;
	}


	/**
	 * Find all boundaries that exist in a volume.  A boundary is represented by an integer pair. {i,j}=={j,i} 
	 * @param surf The labeled volume
	 * @return List of boundaries
	 */
	public ArrayList<Point2i> findBoundaries(){
		
		int numboundverts = 0;
		int lab=-1;
		int l=-1;
		
		boundaries = new ArrayList<Point2i>();
		boundarySizes = new ArrayList<Integer>();
		
		boolean foundboundvert = false;
		for(int k=0; k<truthvol.getSlices(); k++){
			for(int j=0; j<truthvol.getCols(); j++){
				for(int i=0; i<truthvol.getRows();i++){
					
					lab = truthvol.getInt(i, j, k);

					/* is it a boundary voxel? */
					foundboundvert = false;
					l = 0;
					GridPt[] nbrlist = getNeighbors(i,j,k, connectivity, truthvol.getRows(), truthvol.getCols(), truthvol.getSlices());
					while(!foundboundvert && l<nbrlist.length){
						if(nbrlist[l]!=null){
							int nbrlab = truthvol.getInt(nbrlist[l].x, nbrlist[l].y, nbrlist[l].z);


							if(nbrlab!=lab){
								foundboundvert = true;

								Point2i bound;
								if(nbrlab<lab){
									bound = new Point2i(nbrlab,lab);
								}else{
									bound = new Point2i(lab,nbrlab);
								}
								if(!boundaries.contains(bound)){
									boundaries.add(bound);
									boundarySizes.add(1);
								}else{
									//increment boundary size
									boundarySizes.set(boundaries.indexOf(bound), boundarySizes.get(boundaries.indexOf(bound))+1);
								}
								numboundverts++;
							}else{
								l++;
							}
						}else{
							l++;
						}
						
					}
				}
			}
		}
		return boundaries;
	}
	
	/**
	 * Find the sizes of all boundaries that exist in a volume. 
	 * This assumes that the variable boundaries has been set, either from within this
	 * class, or externally. 
	 * @return List of sizes of boundaries
	 */
	public ArrayList<Integer> findBoundarySizes(){
		
		int numboundverts = 0;
		int lab=-1;
		int l=-1;
		
		boundarySizes = new ArrayList<Integer>(boundaries.size());
		//initialize all boundary sizes to zero
		for(int i=0; i<boundaries.size();i++){
			boundarySizes.add(new Integer(0));
		}
//		System.out.println("jist.plugins"+"\t"+"Size of boundarySizes: " + boundarySizes.size());
//		System.out.println("jist.plugins"+"\t"+"Size of boundaries: " + boundaries.size());
		
		boolean foundboundvert = false;
		for(int k=0; k<truthvol.getSlices(); k++){
			for(int j=0; j<truthvol.getCols(); j++){
				for(int i=0; i<truthvol.getRows();i++){
					
					lab = truthvol.getInt(i, j, k);

					/* is it a boundary voxel? */
					foundboundvert = false;
					l = 0;
					GridPt[] nbrlist = getNeighbors(i,j,k, connectivity, truthvol.getRows(), truthvol.getCols(), truthvol.getSlices());
					while(!foundboundvert && l<nbrlist.length){
						if(nbrlist[l]!=null){
							int nbrlab = truthvol.getInt(nbrlist[l].x, nbrlist[l].y, nbrlist[l].z);


							if(nbrlab!=lab){
								foundboundvert = true;

								Point2i bound;
								if(nbrlab<lab){
									bound = new Point2i(nbrlab,lab);
								}else{
									bound = new Point2i(lab,nbrlab);
								}
								if(boundaries.contains(bound)){
									//increment boundary size
									boundarySizes.set(boundaries.indexOf(bound), boundarySizes.get(boundaries.indexOf(bound))+1);
								}
								numboundverts++;
							}else{
								l++;
							}
						}else{
							l++;
						}
						
					}
				}
			}
		}
		return boundarySizes;
	}

	
	public static GridPt[] getNeighbors(int i, int j, int k, GridPt.Connectivity connectivity, int xmax, int ymax, int zmax){
		GridPt[] nbrs;
		if(connectivity==GridPt.Connectivity.SIX){
			nbrs = GridPt.onlyInBounds(GridPt.neighborhood6C(i, j, k), xmax, ymax, zmax);
		}else if(connectivity==GridPt.Connectivity.EIGHTEEN){
			nbrs = GridPt.onlyInBounds(GridPt.neighborhood18C(i, j, k), xmax, ymax, zmax);
		}else if(connectivity==GridPt.Connectivity.TWENTYSIX){
			nbrs = GridPt.onlyInBounds(GridPt.neighborhood26C(i, j, k), xmax, ymax, zmax);
		}else{
			System.out.println("jist.plugins"+"\t"+"Warning: Invalid connectivity");
			return null;
		}
		return nbrs;
	}
	public static GridPt[] getNeighbors(GridPt pt, GridPt.Connectivity connectivity, int xmax, int ymax, int zmax){
		GridPt[] nbrs;
		if(connectivity==GridPt.Connectivity.SIX){
			nbrs = GridPt.onlyInBounds(GridPt.neighborhood6C(pt.x, pt.y, pt.z),xmax, ymax, zmax);
		}else if(connectivity==GridPt.Connectivity.EIGHTEEN){
			nbrs = GridPt.onlyInBounds(GridPt.neighborhood18C(pt.x, pt.y, pt.z),xmax, ymax, zmax);
		}else if(connectivity==GridPt.Connectivity.TWENTYSIX){
			nbrs = GridPt.onlyInBounds(GridPt.neighborhood26C(pt.x, pt.y, pt.z),xmax, ymax, zmax);
		}else{
			System.out.println("jist.plugins"+"\t"+"Warning: Invalid connectivity");
			return null;
		}
		return nbrs;
	}
	
	public File writeRaterVolume(File f){
		return ImageDataReaderWriter.getInstance().write(ratervol, f);
	}
	
}
