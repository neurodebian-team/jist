package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;


public class LocalNoiseEstimate extends ProcessingAlgorithm{	
	/*******************************************
	 * Input Parameters
	 ******************************************/
	private ParamVolumeCollection ParamVolsA;
	private ParamVolumeCollection ParamVolsB;

	private ParamOption operation;	
	private ParamInteger dim;
	private ParamNumberCollection scale;
	private ParamFloat implicitThreshold;
	/*******************************************
	 * Output Parameters
	 ******************************************/
	private ParamVolume ParamResult;
	private ParamVolume ParamWeight;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Compute a robust local noise estimate.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams){
		/**************************************
		 * Plugin Information
		 **************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Noise");
		inputParams.setLabel("Local Noise Estimate");
		inputParams.setName("Local_Noise_Estimate");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/a/jhu.edu/pami/"));
		info.add(new AlgorithmAuthor("Robert Kim", "rkim35@jhu.edu", "http://sites.google.com/a/jhu.edu/pami/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/***************************************
		 * Creating Inputs
		 ***************************************/
		inputParams.add(ParamVolsA=new ParamVolumeCollection("Input Volumes A"));
		inputParams.add(ParamVolsB=new ParamVolumeCollection("Input Volumes B"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Standard Deviation","Qn"}));
		inputParams.add(scale = new ParamNumberCollection("Number of Signal Averages",0,Integer.MAX_VALUE));
		inputParams.add(implicitThreshold =new ParamFloat("Implicit Threshold",Float.NEGATIVE_INFINITY,Float.POSITIVE_INFINITY,0));
		inputParams.add(dim = new ParamInteger("Repeated Dimension",0,3));
		dim.setValue(new Integer(3));
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(ParamResult=new ParamVolume("Noise Field Estimate Result"));
		outputParams.add(ParamWeight=new ParamVolume("Spatial Confidence Weighting - sigma"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		MetaImageData4D volA = new MetaImageData4D(ParamVolsA.getImageDataList());
		MetaImageData4D volB = new MetaImageData4D(ParamVolsB.getImageDataList());
		//		List<ImageData> volAList = ParamVolsA.getImageDataList();
		//		List<ImageData> volBList = ParamVolsB.getImageDataList();
		//		ArrayList<ImageData> listout = new ArrayList<ImageData>();
		//		ArrayList<ImageData> listCountOut = new ArrayList<ImageData>();
		//		int N = volAList.size();
		double []scaleFactor;
		if(scale.size()<=1) {
			scaleFactor = new double[volA.getComponents()];
			for(int i=0;i<scaleFactor.length;i++)
				scaleFactor[i] = 1;
		} else { 

			scaleFactor = new double[scale.size()];
			for(int i=0;i<scaleFactor.length;i++)
				scaleFactor[i] = Math.sqrt(scale.getValue(i).doubleValue());
		}

		/*Iterator<ImageData> ii = volAList.iterator();
			while(ii.hasNext()) {
				System.out.println(getClass().getCanonicalName()+"\t"+ii.next()); System.out.flush();
			}*/
		//		for(int l=0;l<N;l++){
	
		//		ImageData volA=volAList.get(0);
		//		ImageData volB=volBList.get(0);
		System.out.println(getClass().getCanonicalName()+"\t"+volA); System.out.flush();
		System.out.println(getClass().getCanonicalName()+"\t"+volB); System.out.flush();
		int rows=volA.getRows();
		int cols=volA.getCols();
		int slices=volA.getSlices();
		int components = volA.getComponents();
		if(slices<1) slices=1;
		if(components<1) components=1;

		int a=0;int b=0;int c=0;int d=0;
		int i=0;int j=0;int k=0;int m=0;			
		int dim1=1,dim2=2,dim3=3,dim4=4;
		switch(dim.getValue().intValue()) {
		case 0: 
			dim1=1;dim2=2;dim3=3;dim4=0;
			d=rows;a=cols;b=slices;c=components;
			break;
		case 1:
			dim1=0;dim2=3;dim3=1;dim4=2;
			d=cols;a=rows;b=slices;c=components;				
			break;
		case 2:
			dim1=0;dim2=1;dim3=3;dim4=2;
			d=slices;a=rows;b=cols;c=components;
			break;
		case 3: 
			dim1=0;dim2=1;dim3=2;dim4=3;
			d=components;a=rows;b=cols;c=slices;
			break;
		default: 
			throw new RuntimeException("Invalid argument - should NEVER be here due to input validation.");
		}		

		System.out.println(getClass().getCanonicalName()+"\t"+"Allocating result volumes...");
		ImageHeader copyme = volA.getHeader();

		ImageDataFloat resultVol=new ImageDataFloat(volA.getName()+"_NFEstimate",a,b,c);	
		resultVol.getHeader().copyGeometry(copyme);

		ImageDataFloat weightVol=new ImageDataFloat(volA.getName()+"_NFSpatialConfidence",a,b,c);
		weightVol.getHeader().copyGeometry(copyme);
		System.out.println(getClass().getCanonicalName()+"\t"+"Finsihed allocating result volumes.");
		double []tmpA = new double[d];
		double []tmpB = new double[d];
		double []tmpDiff = new double[d];
		double []tmpAvg = new double[d];
		double NL;
		double implicitThresholdValue = implicitThreshold.getDouble();
		double sqrt2 = Math.sqrt(2);
		for (i = 0; i < a; i++) {
			for (j = 0; j < b; j++) {
				for (k = 0; k < c; k++) {
					double avg=0;
					for(m = 0; m < d; m++) {
						int []cur={i,j,k,m};
						tmpA[m]=volA.get(cur[dim1], cur[dim2], cur[dim3], cur[dim4]).doubleValue();															
						tmpB[m]=volB.get(cur[dim1], cur[dim2], cur[dim3], cur[dim4]).doubleValue();
						tmpDiff[m] = (tmpA[m]-tmpB[m])/sqrt2 // correct for increased variance of the difference
						*scaleFactor[(m>=scaleFactor.length)?0:m];
						tmpAvg[m] = (tmpA[m]+tmpB[m])/2.;
						avg+=tmpAvg[m];
					}
					avg/=m;
					if(avg<=implicitThresholdValue) {
						for(m=0;m<d;m++)
							tmpDiff[m]=Double.NaN;							
					}

					double initSigma;
					double thres;
					int nonFinite;
					double medSig;
					switch(operation.getIndex()){					
					case 0://std
						initSigma = StatisticsDouble.std(tmpDiff);
						medSig=(StatisticsDouble.medianRobust(tmpAvg));
						thres = initSigma*(medSig/initSigma-3);
						if(thres<5*initSigma)
							thres=5*initSigma;

						// Apply the data adaptive threshold
						nonFinite =0;
						for(m=0;m<d;m++) {
							if(tmpAvg[m]<thres || Double.isInfinite(tmpAvg[m]) || Double.isNaN(tmpAvg[m])){
								tmpDiff[m]=Double.NaN;
								nonFinite++;
							}									
						}

						NL=StatisticsDouble.std(tmpDiff);

						if(!(Double.isInfinite(NL)||Double.isNaN(NL))) {
							double wt = Math.sqrt(tmpA.length-nonFinite)*
							(1-Math.exp(-(Math.pow(medSig/NL/2,2))));
							weightVol.set(i,j,k,(wt<0.01?Double.NaN:wt));
							resultVol.set(i,j,k, NL);
						} else { 
							weightVol.set(i,j,k,Double.NaN);
							resultVol.set(i,j,k, Double.NaN);
						}
						break;							
					case 1://Qn							
						initSigma = StatisticsDouble.QnRobust(tmpDiff);
						medSig=StatisticsDouble.medianRobust(tmpAvg);
						thres = initSigma*(medSig/initSigma-3);
						if(thres<5*initSigma)
							thres=5*initSigma;

						// Apply the data adaptive threshold
						nonFinite =0;
						for(m=0;m<d;m++) {
							if(tmpAvg[m]<thres || Double.isInfinite(tmpAvg[m]) || Double.isNaN(tmpAvg[m])){
								tmpDiff[m]=Double.NaN;
								nonFinite++;
							}									
						}

						NL=StatisticsDouble.QnRobust(tmpDiff);
						if(!(Double.isInfinite(NL)||Double.isNaN(NL))) {
							double wt = Math.sqrt(tmpA.length-nonFinite)*
							(1-Math.exp(-(Math.pow(medSig/NL/2,2))));
							weightVol.set(i,j,k,(wt<0.01?Double.NaN:wt));
							resultVol.set(i,j,k, NL);
						} else { 
							weightVol.set(i,j,k,Double.NaN);
							resultVol.set(i,j,k, Double.NaN);
						}
					default:
					}
				}
			}
		}		

		ParamResult.setValue(resultVol);
		ParamWeight.setValue(weightVol);
	}
}
