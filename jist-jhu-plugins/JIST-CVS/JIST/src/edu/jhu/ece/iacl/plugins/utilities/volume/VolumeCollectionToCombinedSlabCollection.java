package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class VolumeCollectionToCombinedSlabCollection extends ProcessingAlgorithm {
	//output params
	private ParamVolumeCollection outputFiles;	// Slabs

	//input params
	private ParamVolumeCollection inputFiles;		// Files to be split
	private ParamInteger numslabs;


	//Variables
	ArrayList<File> subVols;
	File outDir;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.11 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts VolumeCollection to CombinedSlabCollection.  Splits each" +
			" volume in the input collection into a number of smaller volumes \"slabs\".  The user can specify the number of slabs" +
			"with the \"Number of Slabs\" input.  The output collection will have numSlabs volumes, each of which will have N components in the " +
			"fourth image dimension (where N is the number of input volumes).";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("VolCollect to Combined4DSlabCollect");
		inputParams.setName("VolCollect_to_Combined4DSlabCollect");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		// Input Parameters
		inputParams.add(inputFiles= new ParamVolumeCollection("Input Volume Files"));
		inputFiles.setLoadAndSaveOnValidate(false);
		inputParams.add(numslabs= new ParamInteger("Number of Slabs",1,100,4));
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outputFiles = new ParamVolumeCollection("Output Combined Slabs"));
		outputFiles.setLoadAndSaveOnValidate(false);
	}


	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		outDir = this.getOutputDirectory();//MipavController.getDefaultWorkingDirectory();
		List<ParamVolume> pvollist = inputFiles.getParamVolumeList();
		
		subVols = new ArrayList<File>();
		ImageData outputSlab;
		VoxelType type=null;

		int lastSlabEndSlice=-1;
		for(int jSlab=0;jSlab<this.numslabs.getValue().intValue();jSlab++) {
			outputSlab = null;
			int startSlice=0, endSlice=0, nSlices;
			int jFile=0;
			for(ParamVolume pv: pvollist){
				ImageData vol = pv.getImageData();
				type=vol.getType();
				if(outputSlab==null) {
					int totalSlices = vol.getSlices();
					int slicesPerSlab = (int)Math.ceil(totalSlices/(float)numslabs.getValue().intValue());
					startSlice = lastSlabEndSlice+1;
					endSlice = startSlice+slicesPerSlab-1;
					endSlice = (endSlice>=vol.getSlices()?vol.getSlices()-1:endSlice);
					lastSlabEndSlice=endSlice;
					nSlices = endSlice-startSlice+1;
					String jSlabNum = String.format("%02d", jSlab);
					if (type.name().compareTo("UBYTE")==0)
						outputSlab=new ImageDataUByte(vol.getRows(), vol.getCols(),nSlices,inputFiles.getValue().size());
					else if (type.name().compareTo("FLOAT")==0)
						outputSlab=new ImageDataFloat(vol.getRows(), vol.getCols(),nSlices,inputFiles.getValue().size());
					else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
						outputSlab=new ImageDataInt(vol.getRows(), vol.getCols(),nSlices,inputFiles.getValue().size());
					else
						outputSlab=new ImageDataDouble(vol.getRows(), vol.getCols(),nSlices,inputFiles.getValue().size());
					outputSlab.setName(vol.getName()+"_Slab"+jSlabNum);
					outputSlab.setHeader(vol.getHeader());
					System.out.println(getClass().getCanonicalName()+"\t"+"Image Type: "+outputSlab.getType().name());
				}
				System.out.println(getClass().getCanonicalName()+"\t"+"Slices: "+startSlice+"-"+endSlice);
				System.out.flush();
				for(int i=0;i<vol.getRows();i++) {
					for(int j=0;j<vol.getCols();j++) {
						int jSlice=0;
						for(int k=startSlice;k<=endSlice;k++) {
							if (type.name().compareTo("UBYTE")==0)
								outputSlab.set(i,j,jSlice,jFile,vol.getUByte(i, j, k,0));
							else if (type.name().compareTo("FLOAT")==0)
								outputSlab.set(i,j,jSlice,jFile,vol.getFloat(i, j, k,0));
							else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
								outputSlab.set(i,j,jSlice,jFile,vol.getInt(i, j, k,0));
							else
								outputSlab.set(i,j,jSlice,jFile,vol.getDouble(i, j, k,0));
							jSlice++;
						}
					}
				}
				vol.dispose();
				vol=null;
				pv.dispose();
				jFile++;
			}
			outputFiles.add(outputSlab);
			outputFiles.writeAndFreeNow(this);
		}
	}
}
