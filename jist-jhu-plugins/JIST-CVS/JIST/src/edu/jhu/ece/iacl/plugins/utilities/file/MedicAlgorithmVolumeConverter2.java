package edu.jhu.ece.iacl.plugins.utilities.file;

import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.PipeDestination;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceLocationCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile.DialogType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class MedicAlgorithmVolumeConverter2 extends PipeDestination{
//	private ParamVolume newVol;
	private ParamFile outputDir;
	private ParamOption outputCompression;
	private ParamOption outputType;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected boolean xmlEncodeModule(Document document, Element parent) {		
		return super.xmlEncodeModule(document, parent);

	}
	public void xmlDecodeModule(Document document, Element el) {
		super.xmlDecodeModule(document, el);
		outputDir  = (ParamFile)inputParams.getFirstChildByName("Output Directory");
		outputType = (ParamOption)inputParams.getFirstChildByName("File Type");
		outputCompression = (ParamOption)inputParams.getFirstChildByName("Compression");
		}
	
	
	protected void createInputParameters(ParamCollection inputParams) {		
		inputParams.add(outputDir=new ParamFile("Output Directory",DialogType.DIRECTORY));
		ImageDataReaderWriter readerWriter=ImageDataReaderWriter.getInstance();
//		new ImageDataReaderWriter();
		Vector<String> exts=readerWriter.getExtensionFilter().getExtensions();
		inputParams.add(outputType=new ParamOption("File Type",exts));
		outputType.setValue("xml");
		Vector<String> comp = readerWriter.getCompressionOptions();
		inputParams.add(outputCompression=new ParamOption("Compression",comp));
		outputCompression.setValue(comp.firstElement());

		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Volume Conversion 2");
		inputParams.setName("Volume_Conversion_2");


//		AlgorithmInformation info = getAlgorithmInformation();
//		info.setWebsite("");
//		info.setDescription(shortDescription);
//		info.setLongDescription(shortDescription + longDescription);
//		info.setVersion(revnum);
//		info.setEditable(false);
//		info.setStatus(DevelopmentStatus.RC);
	}



	protected void doConvert(ParamVolume p) {
		ParamVolume newVol = new ParamVolume();
		newVol.setLoadAndSaveOnValidate(false);
		newVol.getReaderWriter().getExtensionFilter().setPreferredExtension(outputType.getValue());
		try {
			((ImageDataReaderWriter)newVol.getReaderWriter()).setPreferredCompression(outputCompression.getValue());
		} catch (InvalidParameterException e) {
			e.printStackTrace();
		}
		newVol.setValue(p.getImageData());
		newVol.writeAndFreeNow(this.outputDir.getValue());
		
	}


	@Override
	protected void complete() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected ParamCollection createInputParams() {
		selectedParams = new ParamCollection("Volumes");
		ParamCollection inputParams = new ParamCollection();
		createInputParameters(inputParams);
		return inputParams;
	}


	@Override
	protected ParamCollection createOutputParams() {
		ParamCollection outputParams = new ParamCollection();
//		createOutputParameters(outputParams);
		return outputParams;

	}


	@Override
	protected void iterate(ProgressMonitor monitor) {
		Vector<ParamModel> allParams = ((ParamCollection)getInputParam()).getAllDescendants();
		for(ParamModel p:allParams) {
			if(p instanceof ParamVolume) {
				doConvert((ParamVolume)p);
			}
			else if(p instanceof ParamVolumeCollection) {
				for(ParamVolume pv : ((ParamVolumeCollection) p).getParamVolumeList())
				doConvert(pv);
			} 
			else {
				JistLogger.logError(JistLogger.WARNING, "Ignoring conversion of:"+p.getName());
			}
		}
		
		
	}


	@Override
	protected void reset() {
		// TODO Auto-generated method stub
		
	}
}
