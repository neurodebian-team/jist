package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringArrayXMLReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;


public class DWICombineBvalTables extends ProcessingAlgorithm {
	private ParamFileCollection b0valsin;	

	//output parameter
	private ParamFile b0valsout;	


	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	private static final String shortDescription = "Combine multiple b-value tables into one larger b-value list.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(b0valsin=new ParamFileCollection("b0 Values", new FileExtensionFilter(new String[]{"b","txt","lst","bs"})));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Combine b0 Lists");
		inputParams.setName("Combine_b0_Lists");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);

	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(b0valsout=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		String ext = b0valsin.getValue(0).getName().substring(b0valsin.getValue(0).getName().lastIndexOf('.')+1);
		String name = b0valsin.getValue(0).getName().substring(0,b0valsin.getValue(0).getName().lastIndexOf('.'));
		System.out.println("Name: "+ name);
		System.out.println("Ext: " + ext);
		if(ext.equals("b")){
			b0valsout.setValue(getOutputDirectory().getAbsoluteFile()+File.separator+"CombinedTable_"+b0valsin.getValue(0).getName());
		}else{
			b0valsout.setValue(getOutputDirectory().getAbsoluteFile()+File.separator+"CombinedTable_"+name+".b");
		}
		
		PrintWriter fp;
		try {
			fp = new PrintWriter(new FileWriter(b0valsout.getValue()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			throw new RuntimeException("CombineBvalTables: Cannout open output file:"+b0valsout.getValue());
		}
		for(int i=0;i<b0valsin.size();i++) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Loading:"+b0valsin.getValue(i));
			/* Read the b values */
			float [][]bs=null;		
			TextFileReader text = new TextFileReader(b0valsin.getValue(i));
			try {
				bs = text.parseFloatFile();
			} catch (IOException e) 
			{
				throw new RuntimeException("CombineBvalTables: Unable to parse b-file:"+b0valsin.getValue(i));
			}
			float []bval ;
			if(bs.length>bs[0].length) {
			bval = new float[bs.length];
			for(int j=0;j<bval.length;j++)
				bval[j]=bs[j][0];
			} else {
				bval = new float[bs[0].length];
				for(int j=0;j<bval.length;j++)
					bval[j]=bs[0][j];
			}
			for(int j=0;j<bval.length;j++) {
				fp.println(bval[j]);
			}
		}
		fp.close();
		System.out.println(getClass().getCanonicalName()+"\t"+"Done.");
	}
}
