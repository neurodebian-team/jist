package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.util.List;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import gov.nih.mipav.model.structures.ModelImage;


public class MedicAlgorithmExtractFileCollectionComponents extends ProcessingAlgorithm{
	private ParamFileCollection in;
	private ParamInteger dim;
	private ParamVolumeCollection out;
//	private ParamObject<String> filelist;

	private static final String cvsversion = "$Revision: 1.8 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(in=new ParamFileCollection("Mult-Component Volume FileCollection",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(dim=new ParamInteger("Dimension to Extract"));
		dim.setValue(new Integer(3));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Extract FileCollection Components");
		inputParams.setName("Extract_FileCollection_Components");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(out=new ParamVolumeCollection("Volumes Out"));
		out.setLoadAndSaveOnValidate(false);
//		outputParams.add(filelist=new ParamObject<String>("File List",new StringReaderWriter()));
	}


	protected void execute(CalculationMonitor monitor) {
		ImageDataReaderWriter rrw  = ImageDataReaderWriter.getInstance();

		String outDir = this.getOutputDirectory().getAbsolutePath();
		File outFile;
		List<File> srcfiles = in.getValue();
		for(File f: srcfiles){
			ImageData vol  = rrw.read(f);
			//        for(int k=0; k<vol.getComponents(); k++){
			//          //For a 3d volume, make sure the component is set to 1 and not 0
			//            listout.add(new CubicVolumeMipav("Volume"+k,vol.getType(),vol.getRows(), vol.getCols(), vol.getSlices(),1));
			//        }

			if(dim.getValue().intValue()==3){
				System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
				for(int l=0; l<vol.getComponents(); l++){
					String comp = "";
					if(l<10) comp = "0"+l;
					else comp = ""+l;
					ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+comp,vol.getType(),vol.getRows(), vol.getCols(), vol.getSlices());
					for(int i=0;i<vol.getRows();i++){
						for(int j=0;j<vol.getCols();j++){
							for(int k=0;k<vol.getSlices();k++){

								//      					if(vol.get(i, j, k, l).shortValue()!=0){
								//      					System.out.println(getClass().getCanonicalName()+"\t"+vol.get(i, j, k, l));
								//      					}
								nextvol.set(i, j, k, vol.getDouble(i, j, k, l));
							}
						}
					}
					System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");

					/* Update Header information*/
					nextvol.setHeader(vol.getHeader());
					
					out.add(nextvol);					
					out.writeAndFreeNow(this);					
					nextvol.dispose();
					nextvol=null;
				}
			}else if(dim.getValue().intValue()==2){
				System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
				for(int l=0; l<vol.getSlices(); l++){
					//        	for(int l=86; l<88; l++){
					String comp = "";
					if(l<10) comp = "0"+l;
					else comp = ""+l;
					ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+comp,vol.getType(),vol.getRows(), vol.getCols(), vol.getComponents());
					for(int i=0;i<vol.getRows();i++){
						for(int j=0;j<vol.getCols();j++){
							for(int k=0;k<vol.getComponents();k++){

								nextvol.set(i, j, k, vol.getDouble(i, j, l, k));
							}
						}
					}
					System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
					/* Update Header information*/
					nextvol.setHeader(vol.getHeader());

					out.add(nextvol);
					
					out.writeAndFreeNow(this);	
					nextvol.dispose();
					nextvol=null;
				}
			}else if(dim.getValue().intValue()==1){
				System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
				for(int l=0; l<vol.getCols(); l++){
					String comp = "";
					if(l<10) comp = "0"+l;
					else comp = ""+l;
					ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+comp,vol.getType(),vol.getRows(), vol.getSlices(), vol.getComponents());
					for(int i=0;i<vol.getRows();i++){
						for(int j=0;j<vol.getSlices();j++){
							for(int k=0;k<vol.getComponents();k++){

								//      					if(vol.get(i, j, k, l).shortValue()!=0){
								//      					System.out.println(getClass().getCanonicalName()+"\t"+vol.get(i, j, k, l));
								//      					}
								nextvol.set(i, j, k, vol.getDouble(i, l, j, k));
							}
						}
					}
					System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
					/* Update Header information*/
					nextvol.setHeader(vol.getHeader());

					out.add(nextvol);
					
					out.writeAndFreeNow(this);	
					nextvol.dispose();
					nextvol=null;
				}
			}
			else if(dim.getValue().intValue()==0){
				System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
				for(int l=0; l<vol.getRows(); l++){
					String comp = "";
					if(l<10) comp = "0"+l;
					else comp = ""+l;
					ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+comp,vol.getType(),vol.getCols(), vol.getSlices(), vol.getComponents());
					for(int i=0;i<vol.getCols();i++){
						for(int j=0;j<vol.getSlices();j++){
							for(int k=0;k<vol.getComponents();k++){

								//      					if(vol.get(i, j, k, l).shortValue()!=0){
								//      					System.out.println(getClass().getCanonicalName()+"\t"+vol.get(i, j, k, l));
								//      					}
								nextvol.set(i, j, k, vol.getDouble(l, i, j, k));
							}
						}
					}
					System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
					/* Update Header information*/
					nextvol.setHeader(vol.getHeader());

					out.add(nextvol);
					
					out.writeAndFreeNow(this);
					nextvol.dispose();
					nextvol=null;
				}

			}
//			MipavController.getImageByName(vol.getName());
//			MipavController.removeFromReigstry(vol.getModelImage());
//			MipavController.
//			vol.getModelImage().disposeLocal();
			vol.dispose();
			vol=null;

		}
//		listout.trimToSize();
//		out.setValue(listout);

		//Specify the object for the output file
//		filelist.setObject("FileList");
		//specify the file name for the output file
//		filelist.setFileName("filelist.txt");
	}
}
