package edu.jhu.ece.iacl.plugins.labeling;

import edu.jhu.ece.iacl.algorithms.manual_label.FastMarchingLabelExtension;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;


/*
 *   Wrapper for fast-marching volumetric label extension 
 *
 *	@version    Nov 2008
 *	@author     John Bogovic
 */
public class MedicAlgorithmFMLabelExtension extends ProcessingAlgorithm {
	protected ParamVolume labelsin, extlabelsout;
	protected ParamInteger minlabel;
	protected ParamFloat maxdistance;

	private static final String revnum = FastMarchingLabelExtension.get_version();
	private static final String shortDescription = "Uses fast marching to extend volumetric labels to a particular distance given by the \"Max Distance\" input.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(labelsin = new ParamVolume("Input Label Volume",null,-1,-1,-1,1));
		inputParams.add(maxdistance = new ParamFloat("Max Distance"));
		maxdistance.setValue(20);
		inputParams.add(minlabel = new ParamInteger("Min Label"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling");
		inputParams.setName("Fast Marching Label Extension");
		inputParams.setName("Extend Labels FM");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setAffiliation("Johns Hopkins University, Department of Electrical and Computer Engineering");
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(extlabelsout = new ParamVolume("Output Label Volume",null,-1,-1,-1,1));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		
		FastMarchingLabelExtension fm = new FastMarchingLabelExtension();
		ImageDataInt out = fm.solve(labelsin.getObject(), minlabel.getInt(), maxdistance.getFloat());
		extlabelsout.setValue(out);

	}


//	protected void execute(CalculationMonitor monitor)
//			throws AlgorithmRuntimeException {
//		
//		float[][][] labs = new ImageDataFloat(labelsin.getImageData()).toArray3d();
//		int []ext = labelsin.getModelImage().getExtents();
////		labelsin.dispose();
//		
//		/** FAST MARCH OUT THE LABELS */
//		float[][][] out =  LabelExtensionFastMarching.extendLabels(labs, maxdistance.getFloat());
//		ImageDataFloat labelsout=new ImageDataFloat(out);
//		
//		/****************************************************
//		 * Retrieve the image data and put it into a new
//		 * data structure. Be sure to update the file information
//		 * so that the resulting image has the correct
//		 * field of view, resolution, etc.  
//		 ****************************************************/
//		ModelImage img=(labelsout).getModelImage();
//		img.setExtents(ext);
//		FileUtil.updateFileInfo(labelsin.getModelImage(),img);
//		img.calcMinMax();		
//		labelsout.setName(labelsin.getImageData().getName() + "_FM-extend");
//		extlabelsout.setValue(labelsout);	
//
//	}
}
