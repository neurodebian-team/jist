/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.surface;

import java.util.List;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmCombineSurfaceComponents extends ProcessingAlgorithm{
	ParamSurfaceCollection inSurfs;
	ParamSurface outSurf;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Take a SurfaceCollection and combine the embedded data, assumes that the surfaces have the same number of vertices.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inSurfs=new ParamSurfaceCollection("Surfaces"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Combine Surface Components");
		inputParams.setName("Combine_Surface_Components");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Multi-Component Surface"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		List<EmbeddedSurface> surfs=inSurfs.getSurfaceList();
		int comp=surfs.size();
		EmbeddedSurface first=surfs.get(0);
		int vertCount=first.getVertexCount();
		double[][] data=new double[vertCount][comp];
		Point3f[] verts=first.getVertexCopy();
		int[] indexes=first.getIndexCopy();
		for(int i=0;i<vertCount;i++){
			for(int l=0;l<comp;l++){
				data[i][l]=surfs.get(l).getVertexDataAtOffset(i,0);
			}
		}

		EmbeddedSurface csurf=new EmbeddedSurface(verts,indexes,data);
		csurf.setName(first.getName()+"_vec");
		outSurf.setValue(csurf);
	}
}
