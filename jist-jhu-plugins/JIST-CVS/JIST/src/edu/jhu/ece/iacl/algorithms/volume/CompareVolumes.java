/**
 * 
 */
package edu.jhu.ece.iacl.algorithms.volume;

import edu.jhu.ece.iacl.algorithms.VersionUtil;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataNumber;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;

/**
 * Compare two volumes using a variety of different metrics.
 * 
 * @author Blake Lucas
 */
public class CompareVolumes extends AbstractCalculation {
	public static String getVersion() {
		return VersionUtil.parseRevisionNumber("$Revision: 1.1 $");
	}


	boolean reverseComponents = false;
	ImageDataDouble diffVol = null;
	ImageDataUByte maskVol;
	double minError, maxError, avgError, stdError, skewError, kurtosisError, absAvgError, percent1Error, percent2Error;
	double corrcoeff;
	double min1;
	double max1;
	double min2;
	double max2;
	double std1;
	double std2;
	double mutualInfo;
	double mean1 = 0, mean2 = 0;
	boolean comparable = false;
	ImageData v1;
	ImageData v2;
	int numBin;
	double backgroundThresh;
	int m00, m11, m01, m10;
	public double getMutualInformation(){
		return mutualInfo;
	}
	public double getMinError() {
		return minError;
	}

	public double getMaxError() {
		return maxError;
	}

	public double getAverage() {
		return avgError;
	}

	public double getStdDev() {
		return stdError;
	}

	public double getSkewness() {
		return skewError;
	}

	public double getCorrelationCoefficient() {
		return corrcoeff;
	}

	public double getJaccard() {
		return m11 / (double) (m11 + m01 + m10);
	}

	public double getDice() {
		return 2 * m11 / (double) (2 * m11 + m01 + m10);
	}

	public double getKurtosisExcess() {
		return kurtosisError;
	}

	public double getAbsoluteAverage() {
		return absAvgError;
	}

	public double getPercent1Error() {
		return percent1Error;
	}

	public double getPercent2Error() {
		return percent2Error;
	}

	public CompareVolumes(ImageData v1, ImageData v2, double bg, double binaryThresh, int histBins) {
		this(v1, v2, binaryThresh, histBins);
	}

	public CompareVolumes(ImageData v1, ImageData v2, double backgroundThresh, int histBins) {
		super();
		this.v1 = v1;
		this.v2 = v2;
		this.numBin = histBins;
		this.backgroundThresh = backgroundThresh;
		setLabel("Compare Volumes");
	}

	public void compare() {
		setTotalUnits(6 + ((reverseComponents) ? 1 : 0));
		if (!(comparable = isComparable(v1, v2))) {
			markCompleted();
			return;
		}
		diffVol = new ImageDataDouble(v1.getRows(), v1.getCols(), v1.getSlices());
		maskVol=new ImageDataUByte(v1.getRows(), v1.getCols(), v1.getSlices());
		for (int i = 0; i < v1.getRows(); i++) {
			for (int j = 0; j < v1.getCols(); j++) {
				for (int k = 0; k < v1.getSlices(); k++) {
					diffVol.set(i, j, k, v1.getDouble(i, j, k) - v2.getDouble(i, j, k));
					if (Math.abs(v1.get(i, j, k).doubleValue()) > backgroundThresh
							&& Math.abs(v2.get(i, j, k).doubleValue()) > backgroundThresh) {
						maskVol.set(i, j, k,1);
					} else {
						maskVol.set(i, j, k,0);
					}
				}
			}
		}
		incrementCompletedUnits();
		diffVol.setName(v1.getName() +"_"+v2.getName() + "_diff");
		maskVol.setName(v1.getName() +"_"+v2.getName() + "_mask");
		calcAllStatistics();
	}

	public ImageData getDifferenceVolume() {
		return diffVol;
	}
	public ImageData getMaskVolume() {
		return maskVol;
	}
	public boolean isComparable() {
		return comparable;
	}

	public boolean isComparable(ImageData v1, ImageData v2) {
		return (v1.getRows() == v2.getRows() && v1.getCols() == v2.getCols() && v1.getSlices() == v2.getSlices() && v1
				.getComponents() == v2.getComponents());
	}

	private void calcAllStatistics() {
		if (diffVol == null)
			return;
		double m1 = calcRawMoment(1);
		incrementCompletedUnits();
		double m2 = calcRawMoment(2);
		incrementCompletedUnits();
		double m3 = calcRawMoment(3);
		incrementCompletedUnits();
		double m4 = calcRawMoment(4);
		incrementCompletedUnits();
		calcVolStatistics();
		
		double cm2 = m2 - m1 * m1;
		double cm3 = 2 * m1 * m1 * m1 - 3 * m1 * m2 + m3;
		double cm4 = -3 * m1 * m1 * m1 * m1 + 6 * m1 * m1 * m2 - 4 * m3 * m1 + m4;
		skewError = (cm2 > 0) ? cm3 / Math.pow(cm2, 1.5) : 0;
		stdError = Math.sqrt(cm2);
		absAvgError = m1;
		kurtosisError = (cm2 > 0) ? (cm4 / (cm2 * cm2) - 3) : 0;
		minError = 1E100;
		maxError = -1E100;
		avgError = 0;
		int count1 = 0;
		int count2 = 0;
		m00 = 0;
		m11 = 0;
		m01 = 0;
		m10 = 0;
		int count = 0;
		double val, val1, val2;
		double vol[][][] = diffVol.toArray3d();
		for (int i = 0; i < diffVol.getRows(); i++) {
			for (int j = 0; j < diffVol.getCols(); j++) {
				for (int k = 0; k < diffVol.getSlices(); k++) {
					val = vol[i][j][k];
					val1 = v1.getDouble(i, j, k);
					val2 = v2.getDouble(i, j, k);
					m11 += (val1 >= backgroundThresh && val2 >= backgroundThresh) ? 1 : 0;
					m01 += (val1 < backgroundThresh && val2 >= backgroundThresh) ? 1 : 0;
					m10 += (val1 >= backgroundThresh && val2 < backgroundThresh) ? 1 : 0;
					m00 += (val1 < backgroundThresh && val2 < backgroundThresh) ? 1 : 0;
					minError = Math.min(val, minError);
					maxError = Math.max(val, maxError);
					if (Math.abs(val1) > backgroundThresh && Math.abs(val2) > backgroundThresh) {
						avgError += val;
						count++;
					}
					if (Math.abs(val1) > backgroundThresh) {
						percent1Error += Math.abs(val / val1);
						count1++;
					}
					if (Math.abs(val2) > backgroundThresh) {
						percent2Error += Math.abs(val / val2);
						count2++;
					}
				}
			}
		}
		long sz = diffVol.getRows() * diffVol.getCols() * diffVol.getSlices() * diffVol.getComponents();
		avgError /= count;
		percent1Error /= count1;
		percent2Error /= count2;
		markCompleted();
	}

	private double calcRawMoment(int order) {
		double moment = 0;
		int count = 0;
		double vol[][][] = diffVol.toArray3d();
		for (int i = 0; i < diffVol.getRows(); i++) {
			for (int j = 0; j < diffVol.getCols(); j++) {
				for (int k = 0; k < diffVol.getSlices(); k++) {
					if (Math.abs(v1.get(i, j, k).doubleValue()) > backgroundThresh
							&& Math.abs(v2.get(i, j, k).doubleValue()) > backgroundThresh) {
						moment += Math.pow(Math.abs(vol[i][j][k]), order);
						count++;
					}
				}
			}
		}
		moment /= count;
		return moment;
	}

	private void calcVolStatistics() {

		std1 = 0;
		std2 = 0;
		int count = 0;
		double val1, val2;
		double cc=0;
		double vol[][][] = diffVol.toArray3d();
		mean1=0;
		mean2=0;
		min1=Double.MAX_VALUE;
		min2=Double.MAX_VALUE;
		max1=Double.MIN_VALUE;
		max2=Double.MIN_VALUE;
		
		for (int i = 0; i < diffVol.getRows(); i++) {
			for (int j = 0; j < diffVol.getCols(); j++) {
				for (int k = 0; k < diffVol.getSlices(); k++) {
					if (Math.abs(v1.getDouble(i, j, k)) > backgroundThresh
							&& Math.abs(v2.getDouble(i, j, k)) > backgroundThresh) {
						mean1 += val1=v1.getDouble(i, j, k);
						mean2 += val2=v2.getDouble(i, j, k);
						min1=Math.min(min1, val1);
						min2=Math.min(min2, val2);
						max1=Math.max(max1, val1);
						max2=Math.max(max2, val2);
						
						count++;
					}
				}
			}
		}
		mean1 = (count > 0) ? mean1 / (double) count : 0;
		mean2 = (count > 0) ? mean2 / (double) count : 0;
		for (int i = 0; i < diffVol.getRows(); i++) {
			for (int j = 0; j < diffVol.getCols(); j++) {
				for (int k = 0; k < diffVol.getSlices(); k++) {
					if (Math.abs(v1.getDouble(i, j, k)) > backgroundThresh
							&& Math.abs(v2.getDouble(i, j, k)) > backgroundThresh) {
						val1 = v1.getDouble(i, j, k);
						val2 = v2.getDouble(i, j, k);
						cc += (val1 - mean1) * (val2 - mean2);
						std1+= (val1 - mean1) * (val1 - mean1);
						std2+= (val2 - mean2) * (val2 - mean2);
					}
				}
			}
		}
		corrcoeff= Math.sqrt((cc/std1)*(cc/std2));

		std1 = Math.sqrt((count > 1) ? std1 / (double) (count-1) : 0);
		std2 = Math.sqrt((count > 1) ? std2 / (double) (count-1) : 0);

		mutualInfo=NMI();
	}
	
	private int[][] JointHistogram3D() {
		int i, j, k, binA, binB;
		int[][] jointHist = new int[numBin][numBin];
		int rows = v1.getRows();
		int cols = v1.getCols();
		int slices = v1.getSlices();
		for (k = 0; k < slices; k++) {
			for (j = 0; j < cols; j++) {
				for (i = 0; i < rows; i++) {
					if (Math.abs(v1.getDouble(i, j, k)) > backgroundThresh
							&& Math.abs(v2.getDouble(i, j, k)) > backgroundThresh) {
						binA = (int) Math.round((v1.getDouble(i, j, k) - min1) / (max1 - min1));
						if (binA >= numBin)
							binA = numBin - 1;
						if (binA < 0)
							binA = 0;
						binB = (int) Math.round((v2.getDouble(i, j, k) - min2) / (max2 - min2));
						if (binB >= numBin)
							binB = numBin - 1;
						if (binB < 0)
							binB = 0;
						jointHist[binA][binB]++;
					}
				}
			}
		}
		return jointHist;
	}
	
	private double NMI() {
		int i = 0, j = 0;
		double HA = 0, HB = 0, HAB = 0;
		int numVoxelA = 0, numVoxelB = 0, numVoxelAB = 0;
		double tmp = 0;
		int[] histA=Histogram3Dv1();
		int[] histB=Histogram3Dv2();
		int[][] histAB=JointHistogram3D();
		for (i = 0; i < numBin; i++) {
			numVoxelA += histA[i];
			numVoxelB += histB[i];
			for (j = 0; j < numBin; j++) {
				numVoxelAB += histAB[j][i];
			}
		}
		for (i = 0; i < numBin; i++) {
			if (histA[i] > 0) {
				tmp = (double) histA[i] / numVoxelA;
				HA -= tmp * Math.log(tmp);
			}
			if (histB[i] > 0) {
				tmp = (double) histB[i] / numVoxelB;
				HB -= tmp * Math.log(tmp);
			}
			for (j = 0; j < numBin; j++) {
				if (histAB[j][i] > 0) {
					tmp = (double) histAB[j][i] / numVoxelAB;
					HAB -= tmp * Math.log(tmp);
				}
			}
		}
		return 0.5*(HA + HB) / HAB;
	}
	private int[] Histogram3Dv2() {
		int i, j, k;
		int bin;
		int[] hist = new int[numBin];
		int rows = v2.getRows();
		int cols = v2.getCols();
		int slices = v2.getSlices();
		
		for (k = 0; k < slices; k++) {
			for (j = 0; j < cols; j++) {
				for (i = 0; i < rows; i++) {
					if (Math.abs(v1.getDouble(i, j, k)) > backgroundThresh
							&& Math.abs(v2.getDouble(i, j, k)) > backgroundThresh) {
						bin = (int)Math.round((v2.getDouble(i, j, k)-min2)/(max2-min2));
						if (bin >= numBin)
							bin = numBin - 1;
						if (bin < 0)
							bin = 0;
						hist[bin] ++;
					}
				}
			}
		}
		return hist;
	}
	private int[] Histogram3Dv1() {
		int i, j, k;
		int bin;
		int[] hist = new int[numBin];
		int rows = v1.getRows();
		int cols = v1.getCols();
		int slices = v1.getSlices();
		for (k = 0; k < slices; k++) {
			for (j = 0; j < cols; j++) {
				for (i = 0; i < rows; i++) {
					if (Math.abs(v1.getDouble(i, j, k)) > backgroundThresh
							&& Math.abs(v2.getDouble(i, j, k)) > backgroundThresh) {
						bin = (int)Math.round((v1.getDouble(i, j, k)-min1)/(max1-min1));
						if (bin >= numBin)
							bin = numBin - 1;
						if (bin < 0)
							bin = 0;
						hist[bin] ++;
					}
				}
			}
		}
		return hist;
	}
	/**
	 * @return the max1
	 */
	public double getMax1() {
		return max1;
	}
	/**
	 * @return the max2
	 */
	public double getMax2() {
		return max2;
	}
	/**
	 * @return the min1
	 */
	public double getMin1() {
		return min1;
	}
	/**
	 * @return the min2
	 */
	public double getMin2() {
		return min2;
	}
	/**
	 * @return the std1
	 */
	public double getStd1() {
		return std1;
	}
	/**
	 * @return the std2
	 */
	public double getStd2() {
		return std2;
	}
	/**
	 * @return the mean1
	 */
	public double getMean1() {
		return mean1;
	}
	/**
	 * @return the mean2
	 */
	public double getMean2() {
		return mean2;
	}
}
