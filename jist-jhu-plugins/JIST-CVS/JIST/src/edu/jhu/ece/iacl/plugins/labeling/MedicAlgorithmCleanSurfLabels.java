package edu.jhu.ece.iacl.plugins.labeling;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;

public class MedicAlgorithmCleanSurfLabels extends ProcessingAlgorithm{
	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = 
		cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = 
		"Clean up the Surface Labels.";
	private static final String longDescription = "Maintain the largest connected component and fill up holes";
	
	
	private ParamSurface surfParam;
	private ParamSurface outSurfParam;

	@Override
	protected void createInputParameters(ParamCollection arg0) {
		inputParams.add(surfParam=new ParamSurface("Labeled Surface"));
		inputParams.setLabel("Cleanup Surface Labels");
		inputParams.setName("cleanlabels");
		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.Surface");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.Release);
	}

	@Override
	protected void createOutputParameters(ParamCollection arg0) {
		outputParams.add(outSurfParam=new ParamSurface("Cleaned Labels"));
		
	}

	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		EmbeddedSurface mesh=surfParam.getSurface().clone(); // Don't reuse inputs for outputs
		double[][] labels = mesh.getVertexData();
		ArrayList<Double> ulabels = uniqueValues(labels);
		int[][] nbrTable = EmbeddedSurface.buildNeighborVertexVertexTable(mesh, EmbeddedSurface.Direction.COUNTER_CLOCKWISE);
		double[][] outlabels = new double [mesh.getVertexCount()][1];		
		boolean[] flag = new boolean[mesh.getVertexCount()];
		
		
		for(int i = 0; i<mesh.getVertexCount();i++){
			outlabels[i][0] = labels[i][0]; // Initialization
		}
		int[] tmpregion = new int [5000];
		int[] largest = new int [5000]; // don't store the values if it's larger than 2000
		int larea = 0; // area of the largest connected area
		for (int i = 0; i<ulabels.size(); i++){
			larea = 0;
			double nl = ulabels.get(i);
			System.out.println(getClass().getCanonicalName()+"\t"+"Start processing label "+nl);
			for(int j = 0; j<mesh.getVertexCount();j++){
				flag[j] = false; // initialize flag
			}
			Queue<Integer> q1 = new LinkedList<Integer>();
			int tmparea = 0;
			// find the largest connected component for each label
			for(int j = 0; j<mesh.getVertexCount();j++){
				if(labels[j][0] == nl && !flag[j]){
					q1.add(j);
					flag[j] = true;
					tmpregion[0] = j; // store the region (vertex) that is being examined.
					tmparea = 1;
				}
				if(!q1.isEmpty()){
					while(!q1.isEmpty()){
						Integer vi = q1.remove();
						for(int k = 0; k< nbrTable[vi].length; k++) {
							int nb = nbrTable[vi][k];
							if(labels[nb][0] == nl & !flag[nb]){
								q1.add(nb);
								flag[nb] = true;
								if(tmparea<5000) // only record for area < 5000
									tmpregion[tmparea] = nb;
								tmparea ++;
							}
						}
					}
				// compare tmparea with larea
					if(tmparea>larea) {// update larea
//						System.out.println(getClass().getCanonicalName()+"\t"+"tmparea"+tmparea);
						for(int k = 0; k< larea; k++){
							outlabels[largest[k]][0] = 0;	
						}
						if(tmparea>5000)
							larea = 5000;
						else
							larea = tmparea;
						for(int k = 0; k< larea; k++){
							largest[k] = tmpregion[k];	
						}
						
					}
					else if (tmparea > 0){
//						System.out.println(getClass().getCanonicalName()+"\t"+"tmparea"+tmparea);
						for(int k = 0; k< tmparea; k++){
							outlabels[tmpregion[k]][0] = 0;
						}
					}
	//				if(tmparea < 500 && tmparea > 0){ // Eliminate the part with the area smaller than 500 (vertex)
	//					for(int k = 0; k< tmparea; k++){
	//						outlabels[tmpregion[k]][0] = 0;
	//					}
//					}
				}
			}
			q1.clear();
			tmparea = 0;
		}
		// filling up holes
		System.out.println(getClass().getCanonicalName()+"\t"+"Start filling up holes");
		Queue<Integer> q2 = new LinkedList<Integer>();
		for(int i = 0; i<mesh.getVertexCount(); i++){
			flag[i] = false;
		}
		
		for(int i = 0; i<mesh.getVertexCount(); i++){
			int tmparea = 0;
			if(outlabels[i][0] == 0 & !flag[i]){
				q2.add(i);
				flag[i] = true;
				tmpregion[0] = i;
				tmparea = 1;
				
			}
			while(!q2.isEmpty()){
				Integer vi = q2.remove();
				for(int j = 0; j< nbrTable[vi].length;j++){
					int nb = nbrTable[vi][j];
					if(outlabels[nb][0] == 0 & !flag[nb]){
						q2.add(nb);
						flag[nb] = true;
						if(tmparea < 5000){
							tmpregion[tmparea] = nb;
						}
						tmparea ++;
					}
					else if(outlabels[nb][0]>0){ // pick up a non-zero label
						while(!q2.isEmpty()){
							vi = q2.remove();
							tmpregion[tmparea] = vi;
							tmparea ++;
						}
						double nl = outlabels[nb][0];
						for(int k = 0; k< tmparea; k++){
							outlabels[tmpregion[k]][0] = nl; // assign this non-zero label
						}
						tmparea = 0;
						break;
					}
				}
				
			}
			tmparea = 0;
		}
		q2.clear();
		outSurfParam.setFileName(mesh.getName()+"_cleaned");
		EmbeddedSurface surfOut = mesh;
		surfOut.setVertexData(outlabels);
		outSurfParam.setValue(surfOut);
		
		
	}
	private ArrayList<Double> uniqueValues(double[][] vals){ // find the unique label numbers and return the sorted list
		ArrayList<Double> unique = new ArrayList<Double>();
		for(int i=0; i<vals.length;i++){
			if(!unique.contains(new Double(vals[i][0]))){
				unique.add(new Double(vals[i][0]));
//				System.out.println(getClass().getCanonicalName()+"\t"+"Added " + vals[i][0]);
			}
		}
		unique.trimToSize();
		bsort(unique);
		return unique;
	}

	private void bsort(ArrayList<Double> data) {
		// bubble sort
		int i, j;
		double tmp;
		for(i = 0; i < data.size()-1; i++){
			for(j = 0; j< data.size()-1-i; j++){
				if(data.get(j+1)<data.get(j)){
					tmp = data.get(j);
					data.set(j, data.get(j+1));
					data.set(j+1, tmp);
				}
			}
		}
		
	}
}
