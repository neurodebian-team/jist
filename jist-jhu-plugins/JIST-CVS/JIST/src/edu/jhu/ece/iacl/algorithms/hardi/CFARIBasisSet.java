package edu.jhu.ece.iacl.algorithms.hardi;

import imaging.Scheme;

import java.util.Vector;

import Jama.Matrix;

public class CFARIBasisSet {

	Scheme acquisition;
	Matrix compressedSensingMatrix;
	Vector<CFARIBasisFunction> basisSet;
	
	public void setDWScheme(Scheme ischeme) {
		acquisition = ischeme;
	}
	
	public Scheme getDWScheme() {
		return acquisition;
	}

	public void setSensingMatrix(Matrix sensingMatrix) { 
		compressedSensingMatrix = (Matrix)sensingMatrix.clone();
	} 
	
	public void setBasisSet(Vector<CFARIBasisFunction> set) {
		basisSet = set;
	}

	public int getNumberOfBasisFunctions() {
		return basisSet.size();
	}

	public Matrix getSensingMatrix() {
		return (Matrix)(compressedSensingMatrix.clone());
	}
	
	public Vector<CFARIBasisFunction> getBasisSet() {
		return (Vector<CFARIBasisFunction>)(basisSet.clone());
	}
	
}
