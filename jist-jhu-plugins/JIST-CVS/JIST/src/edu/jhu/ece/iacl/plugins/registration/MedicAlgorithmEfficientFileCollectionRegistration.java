package edu.jhu.ece.iacl.plugins.registration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavPointer;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmFLIRTCollection.AlgorithmRegOAR3DWrapper;
import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;
import gov.nih.mipav.view.dialogs.JDialogBase;

/**
 * 
 * @author John Bogovic
 *
 */
public class MedicAlgorithmEfficientFileCollectionRegistration extends
ProcessingAlgorithm {

	//output params
	private ParamVolumeCollection regVols;	// 

	//input params
	private ParamVolumeCollection sourceVols;		// Files to be split
	private ParamVolume targetVol;

	ParamOption dof;
	ParamOption inputInterpolation;
	ParamOption outputInterpolation;
	ParamOption costFunction;
	ParamBoolean useMaxOfMinRes;
	ParamOption rotDim;
	ParamDouble minAngle;
	ParamDouble maxAngle;
	ParamDouble coarseAngleIncrement;
	ParamDouble fineAngleIncrement;
	ParamInteger bracketMinimum;
	ParamInteger iters;
	ParamInteger level8to4;
	ParamBoolean subsample;
	ParamBoolean skipMultilevelSearch;
	ParamFileCollection transformations;
	ParamFileCollection gradTables;
	ParamFile gradTableOut; 

	// Controls the threaded nature of the OAR Registration.
	private ParamBoolean OARThreadedBool;

	ArrayList<double[][]> xfms;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String rcsid = "$Id: MedicAlgorithmEfficientFileCollectionRegistration.java,v 1.11 2010/03/09 16:07:06 bennett Exp $";
	private static final String cvsversion = "$Revision: 1.11 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");

	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setLabel("File Collection Efficient Registration");
		inputParams.setName("FileColReg");
		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));		
		info.setDescription("Extract Components of each input volume.  Organize the output as a FileCollection");
		info.setAffiliation("Johns Hopkins University");		
		info.setVersion(revnum);	

		// Input Parameters
		// Main Parameters
		ParamCollection mainParams=new ParamCollection("Main");	
		mainParams.add(sourceVols= new ParamVolumeCollection("Source Volumes"));
		sourceVols.setLoadAndSaveOnValidate(false);
		mainParams.add(targetVol= new ParamVolume("Target Volume"));

		mainParams.add(gradTables = new ParamFileCollection("Gradient Table",new FileExtensionFilter(new String[]{"grad"})));
		gradTables.setMandatory(false);
		mainParams.add(dof = new ParamOption("Degrees of freedom", new String[] { "Rigid - 6", "Global rescale - 7",
				"Specific rescale - 9", "Affine - 12" }));
		dof.setValue(3);
		mainParams.add(costFunction = new ParamOption("Cost function", new String[] { "Correlation ratio",
				"Least squares", "Normalized cross correlation", "Normalized mutual information" }));
		mainParams.add(inputInterpolation = new ParamOption("Registration interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc"}));
		mainParams.add(outputInterpolation = new ParamOption("Output interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc","Nearest Neighbor" }));
		mainParams.add(rotDim = new ParamOption("Apply rotation", new String[] { "All", "X", "Y", "Z" }));
		mainParams.add(minAngle = new ParamDouble("Minimum angle", -360, 360, -30));
		mainParams.add(maxAngle = new ParamDouble("Maximum angle", -360, 360, 30));
		mainParams.add(coarseAngleIncrement = new ParamDouble("Coarse angle increment", 0, 360, 15));
		mainParams.add(fineAngleIncrement = new ParamDouble("Fine angle increment", 0, 360, 6));

		// Advanced Parameters
		ParamCollection advParams=new ParamCollection("Advanced");
		advParams.add(bracketMinimum = new ParamInteger("Multiple of tolerance to bracket the minimum", 10));
		advParams.add(iters = new ParamInteger("Number of iterations", 2));
		advParams.add(level8to4 = new ParamInteger("Number of minima from Level 8 to test at Level 4", 3));
		advParams.add(useMaxOfMinRes = new ParamBoolean(
				"Use the max of the min resolutions of the two datasets when resampling", true));
		advParams.add(subsample = new ParamBoolean("Subsample image for speed", true));
		advParams.add(skipMultilevelSearch = new ParamBoolean(
		"Skip multilevel search (Assume images are close to alignment)"));
		advParams.setLabel("Advanced");


		OARThreadedBool = new ParamBoolean("Multithreading for Registration", false);
		OARThreadedBool.setDescription("Set to false by default, this parameter controls the multithreaded behavior of the linear registration.");
		advParams.add(OARThreadedBool);


		inputParams.add(mainParams);
		inputParams.add(advParams);
	}

	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(transformations = new ParamFileCollection("Transformation Matrices", new FileExtensionFilter(new String[]{"mtx"})));
		outputParams.add(regVols= new ParamVolumeCollection("Registered Volumes"));
		regVols.setLoadAndSaveOnValidate(false);
		outputParams.add(gradTableOut = new ParamFile("Corrected Gradient Table",new FileExtensionFilter(new String[]{"grad"})));
		gradTableOut.setMandatory(false);
	}

	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {

		// Load the gradient tables (if not null)
		double [][]myGrads=null;
		int nGrads=0;
		int gradI=0;
		List<File> gradList=gradTables.getValue();
		if(gradList!=null) {
			nGrads=0;
			for(File f : gradList ) {
				double [][]grads=readTextFile(f);
				nGrads+=grads.length;
			}

			myGrads=new double[nGrads][3];
			gradI=0;
			for(File f : gradList ) {
				double [][]grads=readTextFile(f);
				nGrads+=grads.length;
				// If there are 4 columns in the gradient table, remove the 1st column (indexes)
				if(grads[0].length==4) {
					double [][]g2 = new double[grads.length][3];
					for(int i=0;i<grads.length;i++) 
						for(int j=0;j<3;j++)
							g2[i][j]=grads[i][j+1];
					grads=g2;
				}
				for(int i=0;i<grads.length;i++)  {
					for(int j=0;j<grads[0].length;j++) {
						//					System.out.println(getClass().getCanonicalName()+"\t"+i+" "+j+" "+gradI);
						myGrads[gradI][j]=grads[i][j];					
					}
					gradI++;
				}					
			}
		};
		//		ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
		ImageData target = targetVol.getImageData();//rw.read(targetVol.getValue());
		ModelImage modelTar = null;
		if(target instanceof ImageDataMipav) {
			modelTar = ((ImageDataMipav)target).extractModelImage();
			target=null;
		} else {
			modelTar = target.getModelImageCopy();
			target.dispose();target=null;
		}
		xfms = new ArrayList<double[][]>();		

		/* DO THE REGISTRATIONS */
		List<ParamVolume> srcfiles = sourceVols.getParamVolumeList();		
		for(ParamVolume pv: srcfiles){
			//read the source file
			ImageData src = pv.getImageData();
			//			VoxelType type = (srcRaw.getType());
			//			ImageData src = srcRaw;
			/*if(type==(VoxelType.BOOLEAN) ||
					type == VoxelType.INT ||
					type == VoxelType.SHORT || 
					type == VoxelType.UBYTE ||
					type == VoxelType.USHORT) {
				System.out.println(getClass().getCanonicalName()+"\t"+"Coverted voxel type to FLOAT");
				src = new ImageDataFloat(srcRaw); 
				srcRaw.dispose(); srcRaw=null;
			}*/
			ModelImage modelSrc = null;
			if(src instanceof ImageDataMipav) {
				modelSrc = ((ImageDataMipav)src).extractModelImage();
				src=null;
			} else {
				modelSrc = src.getModelImageCopy();
				src.dispose();src=null;
			}
			FlirtWrapper flirt=new FlirtWrapper();
			monitor.observe(flirt);
			// run the registration
			ImageDataMipav thisreg = flirt.execute(modelSrc, modelTar);
			modelSrc.disposeLocal();modelSrc=null;

			//			String name = thisreg.getName();
			regVols.add(thisreg);
			regVols.writeAndFreeNow(this);
			//			File f1 = ImageDataReaderWriter.getPreferredImageDataFileName(thisreg, dir);
			//			File f1 = new File(dir, edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(name) + "."+rw.getExtensionFilter().getPreferredExtension());
			// write out the result
			//			System.out.println(getClass().getCanonicalName()+"\t"+"Writing to file: " + f1);
			//			File f2 = rw.write(thisreg, f1);
			//			if(f2!=null){
			//				regout.add(f2);
			//				System.out.print("Success!\n");
			//			}
			// clean up
			pv.dispose();

		}
		modelTar.disposeLocal();

		List<File> xfmfiles = null;
		/* WRITE THE TRANSFORMATIONS TO FILE */
		try{
			File destdir = new File(this.getOutputDirectory().getCanonicalFile()+File.separator+"FileColReg");
			//			File destdir = new File(outputdir.getCanonicalFile().toString());
			if(!destdir.isDirectory()){
				(new File(destdir.getCanonicalPath())).mkdir();
			}
			xfmfiles = writeXfms(destdir,sourceVols.getValue(), targetVol.getValue());
		}catch(IOException e){
			e.printStackTrace();
		}


		/* CORRECT THE GRADIENT TABLES, IF PRESENT*/
		if(gradList!=null && gradList.size()>0) {
			gradI =0 ;
			ArrayDoubleMtxReaderWriter rwmtx = new ArrayDoubleMtxReaderWriter();

			for(File f : xfmfiles) {
				double [][]xfm = rwmtx.read(f);
				double []val = applyCorrection(myGrads[gradI],xfm);
				for(int i=0;i<3;i++)
					myGrads[gradI][i]=val[i];
				gradI++;
			}


			/* WRITE OUT THE NEW GRADIENT TABLE */
			String filename = this.getOutputDirectory().toString() 
			+ File.separatorChar + this.gradTables.getName() + "_corrected"+".grad";
			File gfile= new File(filename);
			java.io.FileWriter frw;
			try {
				frw = new java.io.FileWriter(gfile);
				for(int i=0;i<myGrads.length;i++)
					frw.write(myGrads[i][0]+" "+myGrads[i][1]+" "+myGrads[i][2]+"\n");
				frw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			this.gradTableOut.setValue(gfile);

			//		grad.add(gfile);
		}

		transformations.setValue(xfmfiles);
		

	}
	private double[][] readTextFile(File file) {
		double [][]x=null;

		try {
			x  = new TextFileReader(file).parseDoubleFile();
		} catch (IOException e) { 

			throw new RuntimeException("Define Scheme: Unable to parse double file: "+file.getName()+" at "+file.getAbsolutePath());
		}
		return x;
	}

	public double[] applyCorrection(double []grad, double [][]A){
		double []angCorrGT = new double[3];



		double[][] row = {{grad[0]},{grad[1]},{grad[2]}};
		if((row[0][0]==0 && row[1][0]==0 && row[2][0]==0)||(row[0][0]==100 && row[1][0]==100 && row[2][0]==100)){
			angCorrGT[0]=row[0][0];
			angCorrGT[1]=row[1][0];
			angCorrGT[2]=row[2][0];
		}else{			
			double[][] newrow = matrixMultiply(A,row);
			angCorrGT[0]=newrow[0][0];
			angCorrGT[1]=newrow[1][0];
			angCorrGT[2]=newrow[2][0];
		}
		return angCorrGT;
	}

	public static double[][] matrixMultiply(double[][] A, double[][] B){
		double[][] C = new double[A.length][B[0].length];
		for(int i=0; i<C.length; i++){
			for(int j=0; j<C[0].length; j++){
				C[i][j]=A[i][0]*B[0][j] + A[i][1]*B[1][j]+A[i][2]*B[2][j];
			}
		}
		return C;
	}


	protected class FlirtWrapper extends AbstractCalculation{
		public FlirtWrapper(){
			setLabel("OAR");
		}

		public ImageDataMipav execute(ModelImage source, ModelImage target){
			boolean maxOfMinResol = useMaxOfMinRes.getValue();
			int cost = 0;

			/* SET THE COST FUNCTION */
			switch (costFunction.getIndex()) {
			case 0:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
				// case 0: cost = AlgorithmCostFunctions.CORRELATION_RATIO; break;
			case 1:
				cost = AlgorithmCostFunctions.LEAST_SQUARES_SMOOTHED;
				// cost = AlgorithmCostFunctions.LEAST_SQUARES;
				// costName = "LEAST_SQUARES_SMOOTHED";
				break;
				// case 2: cost = AlgorithmCostFunctions.MUTUAL_INFORMATION_SMOOTHED;
				// break;
			case 2:
				cost = AlgorithmCostFunctions.NORMALIZED_XCORRELATION_SMOOTHED;
				break;
				// case 3: cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION;
				// break;
			case 3:
				cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION_SMOOTHED;
				break;
			default:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
			}
			int DOF = 0;
			switch (dof.getIndex()) {
			case 0:
				DOF = 6;
				break;
			case 1:
				DOF = 7;
				break;
			case 2:
				DOF = 9;
				break;
			case 3:
				DOF = 12;
				break;
			default:
				DOF = 12;
				break;
			}
			int interp = 0;
			switch (inputInterpolation.getIndex()) {
			case 0:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp = AlgorithmTransform.WSINC;
				break;
			default:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			}
			int interp2 = 0;
			switch (outputInterpolation.getIndex()) {
			case 0:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp2 = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp2 = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp2 = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp2 = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp2 = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp2 = AlgorithmTransform.WSINC;
				break;
			case 7:
				interp2 = AlgorithmTransform.NEAREST_NEIGHBOR;
				break;
			default:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			}
			/* SET OTHER PARAMETERS */
			boolean fastMode = skipMultilevelSearch.getValue();
			float rotateBeginX = minAngle.getFloat();
			float rotateEndX = maxAngle.getFloat();
			float coarseRateX = coarseAngleIncrement.getFloat();
			float fineRateX = fineAngleIncrement.getFloat();
			boolean doSubsample = subsample.getValue();
			float rotateBeginY = 0;
			float rotateEndY = 0;
			float coarseRateY = 0;
			float fineRateY = 0;
			float rotateBeginZ = 0;
			float rotateEndZ = 0;
			float coarseRateZ = 0;
			float fineRateZ = 0;

			switch (rotDim.getIndex()) {
			case 0:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = rotateBeginX;
				rotateEndY = rotateEndX;
				rotateEndZ = rotateEndX;
				coarseRateY = coarseRateX;
				coarseRateZ = coarseRateX;
				fineRateY = fineRateX;
				fineRateZ = fineRateX;
				break;
			case 1:
				rotateBeginY = 0;
				rotateBeginZ = 0;
				rotateEndY = 0;
				rotateEndZ = 0;
				coarseRateY = 0;
				coarseRateZ = 0;
				fineRateY = 0;
				fineRateZ = 0;
				break;
			case 2:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = 0;
				rotateEndY = rotateEndX;
				rotateEndZ = 0;
				coarseRateY = coarseRateX;
				coarseRateZ = 0;
				fineRateY = fineRateX;
				fineRateZ = 0;
				rotateBeginX = 0;
				rotateEndX = 0;
				coarseRateX = 0;
				fineRateX = 0;
				break;
			case 3:
				rotateBeginZ = rotateBeginX;
				rotateBeginY = 0;
				rotateEndZ = rotateEndX;
				rotateEndY = 0;
				coarseRateZ = coarseRateX;
				coarseRateY = 0;
				fineRateZ = fineRateX;
				fineRateY = 0;
				rotateBeginX = 0;
				rotateEndX = 0;
				coarseRateX = 0;
				fineRateX = 0;
				break;
			}
			int bracketBound = bracketMinimum.getInt();
			int maxIterations = iters.getInt();
			int numMinima = level8to4.getInt();
			TransMatrix finalMatrix;
			AlgorithmRegOAR3DWrapper reg3;

			ModelImage matchImage = source;
			ModelImage refImage = target;

			/* INSTANTIATE THE REGISTRATION CLASS */
			reg3 = new AlgorithmRegOAR3DWrapper(refImage, matchImage, cost, DOF, interp, rotateBeginX, rotateEndX,
					coarseRateX, fineRateX, rotateBeginY, rotateEndY, coarseRateY, fineRateY,
					rotateBeginZ, rotateEndZ, coarseRateZ, fineRateZ, maxOfMinResol,
					doSubsample, fastMode, bracketBound, maxIterations, numMinima);


			reg3.setObserver(this);

			/* RUN THE ALGORITHM AND GET THE TRANSFORMATION */
			reg3.setMultiThreadingEnabled(OARThreadedBool.getValue());
			reg3.run();
			finalMatrix = reg3.getTransform();

			int xdimA = refImage.getExtents()[0];
			int ydimA = refImage.getExtents()[1];
			int zdimA = refImage.getExtents()[2];
			float xresA = refImage.getFileInfo(0).getResolutions()[0];
			float yresA = refImage.getFileInfo(0).getResolutions()[1];
			float zresA = refImage.getFileInfo(0).getResolutions()[2];
			String name = matchImage.getImageName()+ "_reg";

			/* COPY THE TRANSFORMATION TO AN ARRAY FOR OUTPUT*/
			double[][] tmp=new double[4][4];
			for(int i=0;i<4;i++){
				for(int j=0;j<4;j++){
					tmp[i][j] = finalMatrix.get(i, j);
				}
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Flirt Method "+finalMatrix.matrixToString(4, 4)+" ("+xresA+","+yresA+","+zresA+") ("+xdimA+","+ydimA+","+zdimA+")");

			/* APPLY THE TRANSFORMATION */
			AlgorithmTransform transform = new AlgorithmTransform(matchImage, finalMatrix, interp2, xresA, yresA, zresA,xdimA, ydimA, zdimA, true, false, false);
			transform.setUpdateOriginFlag(true);
			transform.run();
			ModelImage resultImage = transform.getTransformedImage();
			transform.finalize();
			resultImage.getFileInfo(0).setAxisOrientation(refImage.getFileInfo(0).getAxisOrientation());
			resultImage.getFileInfo(0).setImageOrientation(refImage.getFileInfo(0).getImageOrientation());
			resultImage.calcMinMax(); 			
			MipavController.setModelImageName(resultImage,name);

			xfms.add(tmp);

			return new ImageDataMipavWrapper(resultImage);
		}
	}

	/**
	 * Writes the transformations produced by the algorithm to file
	 * @param dir - The directory in which the files will be written
	 * @param sourcefiles
	 * @param targetfile
	 * @return
	 * @throws IOException
	 */
	private List<File> writeXfms(File dir, List<File> sourcefiles, File targetfile) throws IOException{
		ArrayList<File> xfm = new ArrayList<File>();

		int i=0;
		for(double[][] s : xfms){
			String f = dir.getCanonicalPath()+File.separator+sourcefiles.get(i).getName();
			f = f.substring(0, f.lastIndexOf('.'));
			f = f+"_to_"+targetfile.getName();
			f = f.substring(0, f.lastIndexOf('.'));
			File out = new File(f+".mtx");
			System.out.println(getClass().getCanonicalName()+"\t"+"Writing transform to : "+out);
			ArrayDoubleMtxReaderWriter rw = new ArrayDoubleMtxReaderWriter();
			rw.write(s, out);
			i++;
			xfm.add(out);
		}
		return xfm;
	}

}
