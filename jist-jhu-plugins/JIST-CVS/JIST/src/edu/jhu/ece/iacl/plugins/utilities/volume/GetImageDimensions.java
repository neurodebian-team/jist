package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;


public class GetImageDimensions extends ProcessingAlgorithm {
	private ParamVolume Vol;
	private ParamInteger dimX, dimY, dimZ, dimT, resX, resY, resZ, resT;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Demostration plugin to get the dimensions of the input volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(Vol=new ParamVolume("Volume"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Get Image Dimensions");
		inputParams.setName("Get_Image_Dimensions");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(dimX = new ParamInteger("X dimension"));
		outputParams.add(dimY = new ParamInteger("Y dimension"));
		outputParams.add(dimZ = new ParamInteger("Z dimension"));
		outputParams.add(dimT = new ParamInteger("T dimension"));

		outputParams.add(resX = new ParamInteger("X resolution"));
		outputParams.add(resY = new ParamInteger("Y resolution"));
		outputParams.add(resZ = new ParamInteger("Z resolution"));
		outputParams.add(resT = new ParamInteger("T resolution"));
	}


	protected void execute(CalculationMonitor monitor) {
		ImageData vol = Vol.getImageData();
		
		dimX.setValue(vol.getRows());
		dimY.setValue(vol.getCols());
		dimZ.setValue(vol.getSlices());
		dimT.setValue(vol.getComponents());

		ImageHeader hdr = vol.getHeader();
		resX.setValue(hdr.getDimResolutions()[0]);
		resY.setValue(hdr.getDimResolutions()[1]);
		resZ.setValue(hdr.getDimResolutions()[2]);
		resT.setValue(hdr.getDimResolutions()[3]);

		System.out.println(getClass().getCanonicalName()+"\t"+dimX);
		System.out.println(getClass().getCanonicalName()+"\t"+dimY);
		System.out.println(getClass().getCanonicalName()+"\t"+dimZ);
		System.out.println(getClass().getCanonicalName()+"\t"+dimT);

		System.out.println(getClass().getCanonicalName()+"\t"+resX);
		System.out.println(getClass().getCanonicalName()+"\t"+resY);
		System.out.println(getClass().getCanonicalName()+"\t"+resZ);
		System.out.println(getClass().getCanonicalName()+"\t"+resT);
	}
}
