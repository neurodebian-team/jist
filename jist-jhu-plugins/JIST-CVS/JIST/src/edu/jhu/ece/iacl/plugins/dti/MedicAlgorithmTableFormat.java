package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.dti.GradientTable;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;


public class MedicAlgorithmTableFormat extends ProcessingAlgorithm{
	//input parameters
	private ParamObject<String> tablein;
	private ParamOption tabletype;

	//output parameters
	private ParamObject<String> gradOut;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Input: a gradient table of any compatible format.   Output: a gradient table in the specified format.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(tablein = new ParamObject<String>("Gradient Table",new StringReaderWriter()));
		String[] tt = {"IACL","DTIS","FSL"};
		inputParams.add(tabletype = new ParamOption("Output Table Type",tt));

		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Format Gradient Table");
		inputParams.setName("Format_Gradient_Table");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {	
		outputParams.add(gradOut = new ParamObject<String>("Gradient Table Out",new StringReaderWriter()));
	}


	protected void execute(CalculationMonitor monitor) {
		GradientTable gt = new GradientTable();
		gt.readTable(tablein.getValue());

		if(tabletype.getIndex()==0){
			gradOut.setObject(gt.toArrayTxt());
		}else if(tabletype.getIndex()==1){
			gradOut.setObject(gt.toDtisTable());
		}else if(tabletype.getIndex()==2){
			gradOut.setObject(gt.toFslTable());
		}else{
			gradOut.setObject("Problem!");
		}
		String fname = tablein.getValue().getName();
		String basename = fname.substring(0,fname.lastIndexOf('.'));
		gradOut.setFileName(basename+tabletype.getValue()+"_format.grad");
	}
}
