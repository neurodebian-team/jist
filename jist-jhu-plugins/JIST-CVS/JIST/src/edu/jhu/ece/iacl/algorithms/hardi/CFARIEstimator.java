package edu.jhu.ece.iacl.algorithms.hardi;

import imaging.Scheme;
import edu.jhu.bme.smile.commons.math.L1LSCompressedSensing;
import edu.jhu.bme.smile.commons.math.L1LSCompressedSensingNonNeg;
import edu.jhu.bme.smile.commons.math.L1LSCompressedSensingUnconstrained;

public class CFARIEstimator {
	private static int count = 0;
	private static long totalTime = 0;
	
	public static void estimateCFARI(float [][][][]DWdata, byte [][][]mask, CFARIBasisSet basisSet, 
			float [][][][]result, float [][][][]mixture, boolean requirePos, double lambda) {

		/****************************************************
		 * Step 1: Validate Input Arguments 
		 ****************************************************/		
		if(mask==null) {
			mask = new byte[DWdata.length][DWdata[0].length][DWdata[0][0].length];
			for(int i=0;i<DWdata.length;i++) {
				for(int j=0;j<DWdata[0].length;j++) {
					for(int k=0;k<DWdata[0][0].length;k++)
						mask[i][j][k]=1;
				}
			}
		} 
		if(mask.length!=DWdata.length)
			throw new RuntimeException("estimateCFARI: Mask does not match data in dimension: 0.");
		if(mask[0].length!=DWdata[0].length)
			throw new RuntimeException("estimateCFARI: Mask does not match data in dimension: 1.");
		if(mask[0][0].length!=DWdata[0][0].length)
			throw new RuntimeException("estimateCFARI: Mask does not match data in dimension: 2.");


		L1LSCompressedSensing l1ls = null;
		if (requirePos)
			l1ls = new L1LSCompressedSensingNonNeg(basisSet.getSensingMatrix());
		else
			l1ls = new L1LSCompressedSensingUnconstrained(basisSet.getSensingMatrix());
		
		/****************************************************
		 * Step 2: Loop over all voxels and estimate CFARI 
		 ****************************************************/
		Scheme acqScheme = basisSet.getDWScheme();
	
		count = 0;
		totalTime = 0;
		double data[] = new double[DWdata[0][0][0].length];
		int nReturnValues = result[0][0][0].length;
		for(int k=0;k<DWdata[0][0].length;k++) {
			for(int i=0;i<DWdata.length;i++) {
				for(int j=0;j<DWdata[0].length;j++) {
					if(mask[i][j][k]!=0) {						
						for(int l=0;l<data.length;l++) {
							data[l]=DWdata[i][j][k][l];
						}				
						double mean = acqScheme.geoMeanZeroMeas(data);
						if(mean>0)  {
							System.out.print("["+i+" "+j+" "+k+"]\t");
							estimateCFARIModel(l1ls, acqScheme.normalizeData(data),
									basisSet,result[i][j][k],mixture[i][j][k],lambda);
						}
						else {
							for(int l=0;l<nReturnValues;l++) {
								result[i][j][k][l]=-1;
								mixture[i][j][k][l]=Float.NaN;
							}
						}
					} 
					else {
						for(int l=0;l<nReturnValues;l++) {
							result[i][j][k][l]=-1;
							mixture[i][j][k][l]=Float.NaN;
						}
					}
				}				
			}
		}
		double t=0;
		if (count!=0) t=totalTime/count;
		System.out.println("jist.plugins"+"\t"+"\nNumber of voxels: " + count);
		System.out.println("jist.plugins"+"\t"+"Avg time per voxel: " + t);
		return;
	}

	private static void estimateCFARIModel(L1LSCompressedSensing l1ls, double[] data,
			CFARIBasisSet basisSet, float[] resultIndex, float[] resultMixture,	double lambda) {
		long tic=System.currentTimeMillis();
		l1ls.solve(data, lambda, 1e-3, 10.0, true);
		long toc=System.currentTimeMillis();
		totalTime+=toc-tic;
		count++;
		System.out.println("jist.plugins"+"\t"+"est time="+(toc-tic));
		System.out.flush();
		double []mixtures = l1ls.getResult();
		int nOut = resultIndex.length;
		for(int i=0;i<nOut;i++) { 
			resultMixture[i]=Float.NEGATIVE_INFINITY;
			resultIndex[i]=-1;
		}
		for(int i=0;i<mixtures.length;i++) {
			insertElement((float)(mixtures[i]),i,resultMixture,resultIndex,nOut);
		}
	}

	private static void insertElement(float f, int x, float[] resultMixture,
			float[] resultIndex, int nOut) {
	
		int i;
		int loc = nOut;
		while((loc>0) && (f>resultMixture[loc-1])) {						
			loc--;
		}
		if(loc<nOut) {
			for(i=nOut-1;i>loc;i--) {
				resultMixture[i]=resultMixture[i-1];
				resultIndex[i]=resultIndex[i-1];
			}
			resultMixture[loc]=f;
			resultIndex[loc]=x;
		}				
	}

}
