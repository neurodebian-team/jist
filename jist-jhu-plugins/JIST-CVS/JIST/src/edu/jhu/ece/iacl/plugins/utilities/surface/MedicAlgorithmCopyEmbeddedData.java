package edu.jhu.ece.iacl.plugins.utilities.surface;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class MedicAlgorithmCopyEmbeddedData extends ProcessingAlgorithm{
	ParamSurface srcSurf;
	ParamSurface destSurf;
	ParamSurface outSurf;
	ParamOption replaceorappend;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Copy embedded surface data from one surface mesh to another.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(srcSurf=new ParamSurface("Source Surface"));
		inputParams.add(destSurf=new ParamSurface("Destination Surface"));
		inputParams.add(replaceorappend=new ParamOption("Method",new String[]{"Append","Replace"}));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Copy Embedded Data");
		inputParams.setName("Copy_Embedded_Data");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Embedded Surface"));
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		if(replaceorappend.getIndex()==0){
			EmbeddedSurface surfDup=new EmbeddedSurface(destSurf.getSurface(),srcSurf.getSurface().getVertexData());
			surfDup.setName(destSurf.getSurface().getName()+"_copy");
			outSurf.setValue(surfDup);
		} else {
			EmbeddedSurface surfDup=destSurf.getSurface();
			surfDup.setName(destSurf.getSurface().getName()+"_copy");
			surfDup.setVertexData(srcSurf.getSurface().getVertexData());
			outSurf.setValue(surfDup);
		}
	}
}
