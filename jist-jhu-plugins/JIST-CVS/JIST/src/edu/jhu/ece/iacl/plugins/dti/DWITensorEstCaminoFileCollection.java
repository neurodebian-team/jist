package edu.jhu.ece.iacl.plugins.dti;

import imaging.SchemeV1;
import inverters.AlgebraicDT_Inversion;
import inverters.DT_Inversion;
import inverters.LinearDT_Inversion;
import inverters.NonLinearDT_Inversion;
import inverters.RestoreDT_Inversion;
import inverters.WeightedLinearDT_Inversion;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLLMSE;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class DWITensorEstCaminoFileCollection extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamFileCollection DWdata4D; 		// Imaging Data
	private ParamFileCollection Mask3D;			// Binary mask to indicate computation volume
	private ParamOption estOptions;		// Option to attempt to estimate with missing data
	private ParamFloat noiseLevel;		// Used for restore
	//	private ParamFile bvaluesTable;		// .b file with a list of b-values
	//	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions
	private ParamFile SchemeFile;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFileCollection tensorVolume;	// A 4D volume with one tensor estimated per pixel
	private ParamFileCollection exitCodeVolume;	// A 3D volume 
	private ParamFileCollection intensityVolume;// A 3D volume 

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	private static final String shortDescription = "Tensor estimation provided by CAMINO.";
	private static final String longDescription = "Various tensor estimators. See Camino webpage for more details.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("Camino");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Single Tensor Estimation (v2)");
		inputParams.setName("Single_Tensor_Estimation_(v2)");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Computer Science Department - University College London");
		info.add(new Citation("Basser PJ, Mattielo J, and Lebihan D, Estimation of the effective self-diffusion tensor from the NMR spin echo, Journal of Magnetic Resonance, 103, 247-54, 1994."));
		info.add(new Citation("Jones DK and Basser PJ, Squashing peanuts and smashing pumpkins: How noise distorts diffusion-weighted MR data, Magnetic Resonance in Medicine, 52(5), 979-993, 2004."));
		info.add(new Citation("Alexander DC and Barker GJ, Optimal imaging parameters for fibre-orientation estimation in diffusion MRI, NeuroImage, 27, 357-367, 2005"));
		info.add(new Citation("Chang L-C, Jones DK and Pierpaoli C, RESTORE: Robust estimation of tensors by outlier rejection, Magnetic Resonance in Medicine, 53(5), 1088-1095, 2005."));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);	
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamFileCollection("DWI and Reference Image(s) Data (4D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(SchemeFile=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"scheme","schemev1"})));
		//		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		//		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(Mask3D=new ParamFileCollection("Mask Volume to Determine Region of Tensor Estimation (3D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.	
		inputParams.add(estOptions=new ParamOption("Select CAMINO Tensor Estimator?",new String[]{"Algebraic","Linear","NonLinear","RESTORE","WeightedLinear"}));
		estOptions.setValue("Linear");
		inputParams.add(noiseLevel=new ParamFloat("Noise Level (RESTORE only)"));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		tensorVolume = new ParamFileCollection("Tensor Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		tensorVolume.setName("Tensor (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensorVolume);
		exitCodeVolume = new ParamFileCollection("Estimation Exit Code",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));		 
		exitCodeVolume.setName("Exit Code");
		outputParams.add(exitCodeVolume);			 
		intensityVolume = new ParamFileCollection("Intensity Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));		 
		intensityVolume.setName("Intensity");
		outputParams.add(intensityVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute() {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: Start");System.out.flush();

			/****************************************************
			 * Step 2. Loop over input slabs
			 ****************************************************/
			List<File> dwList = DWdata4D.getValue();
			List<File> maskList = Mask3D.getValue();
			ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
			ArrayList<File> outTensorVols = new ArrayList<File>();
			ArrayList<File> outExitVols = new ArrayList<File>();
			ArrayList<File> outIntensityVols = new ArrayList<File>();
			this.addTotalUnits(dwList.size());
			for(int jSlab=0;jSlab<dwList.size();jSlab++) {

				/****************************************************
				 * Step 2. Parse the input data 
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Load data.");System.out.flush();
				this.setLabel("Load");
				ImageData dwd=rw.read(dwList.get(jSlab));//DWdata4D.getImageData();
				String imageName = dwd.getName();
				ImageDataFloat DWFloat=new ImageDataFloat(dwd);dwd.dispose();

				ImageData maskVol=null;
				if(maskList.size()>jSlab)
					maskVol=rw.read(maskList.get(jSlab));//Mask3D.getImageData();
				byte [][][]mask=null;
				if(maskVol!=null) {
					ImageDataUByte maskByte = new ImageDataUByte (maskVol);
					mask = maskByte.toArray3d();
					maskVol.dispose(); maskVol=null;
					maskByte.dispose(); maskByte=null;
				}


				System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
				setLabel("Scheme");
				SchemeV1 DTIscheme = null;

				XStream xstream = new XStream();
				xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
				try {
					ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
					DTIscheme=(SchemeV1)in.readObject();
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				}


				/****************************************************
				 * Step 3. Perform limited error checking 
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Error checking."); System.out.flush();
				setLabel("Error checking");
				DT_Inversion dtiFit=null;
				String code = estOptions.getValue();
				if(estOptions.getValue().compareToIgnoreCase("Algebraic")==0) {
					dtiFit=new AlgebraicDT_Inversion(DTIscheme);			
				}
				if(estOptions.getValue().compareToIgnoreCase("Linear")==0) {
					dtiFit=new LinearDT_Inversion(DTIscheme);			
				}
				if(estOptions.getValue().compareToIgnoreCase("NonLinear")==0) {
					dtiFit=new NonLinearDT_Inversion(DTIscheme);			
				}
				if(estOptions.getValue().compareToIgnoreCase("RESTORE")==0) {
					dtiFit=new RestoreDT_Inversion(DTIscheme,noiseLevel.getValue().floatValue());			
				}
				if(estOptions.getValue().compareToIgnoreCase("WeightedLinear")==0) {
					dtiFit=new WeightedLinearDT_Inversion(DTIscheme);			
				}
				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program 
				 * 		   has NO knowledge of the MIPAV data structure and 
				 * 		   uses NO MIPAV specific components. This dramatic 
				 * 		   separation is a bit inefficient, but it dramatically 
				 * 		   lower the barriers to code re-use in other applications.  		  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Allocate memory."); System.out.flush();
				setLabel("Allocate");
				float [][][][]data=DWFloat.toArray4d();
				int rows = data.length;
				int cols= data[0].length;
				int slices= data[0][0].length;
				int components= data[0][0][0].length;
				float [][][][]tensors = new float[rows][cols][slices][6];
				float [][][][]exitCode= new float[rows][cols][slices][1];
				float [][][][]intensity= new float[rows][cols][slices][1];


				this.setLabel("Estimate");
				System.out.println(getClass().getCanonicalName()+"\t"+"Run CAMINO estimate."); System.out.flush();
				EstimateTensorLLMSE.estimateCamino(data,mask,dtiFit,tensors,exitCode,intensity);

				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
				this.setLabel("Save");
				//			int []ext=DWFloat.getModelImage().getExtents();
				//			ModelImage img=null;
				ImageDataFloat out=new ImageDataFloat(tensors);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Tensor"+code);
				File outputSlab = rw.write(out, getOutputDirectory());			
				outTensorVols.add(outputSlab);
				//tensorVolume.setValue(out);		
				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();


				out=new ImageDataFloat(exitCode);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_ExitCode"+code);

				outputSlab = rw.write(out, getOutputDirectory());
				//			exitCodeVolume.setValue(out);
				outExitVols.add(outputSlab);
				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();

				out=new ImageDataFloat(intensity);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Intensity"+code);			
				//			intensityVolume.setValue(out);	
				outputSlab = rw.write(out, getOutputDirectory());
				outIntensityVols.add(outputSlab);
				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();
				/****************************************************
				 * Step 6. Let the user know that your code is finished.  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: FINISHED");System.out.flush();
				DWFloat.dispose(); 
				if(maskVol!=null) {
					maskVol.dispose();
					maskVol=null;
				}
				this.incrementCompletedUnits();
			}

			tensorVolume.setValue(outTensorVols);
			exitCodeVolume.setValue(outExitVols);
			intensityVolume.setValue(outIntensityVols);
		}
	}
}
