package edu.jhu.ece.iacl.plugins.utilities.file;

import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.ArrayDoubleListTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;


public class MedicAlgorithmConcatArrayDoubleTextLists extends ProcessingAlgorithm {
	ParamObject<ArrayList<double[][]>> obja, objb, objout;
	ParamInteger insertidx;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(obja = new ParamObject<ArrayList<double[][]>>("Input Array", new ArrayDoubleListTxtReaderWriter()));
		inputParams.add(objb = new ParamObject<ArrayList<double[][]>>("Array to Concatenate", new ArrayDoubleListTxtReaderWriter()));
		inputParams.add(insertidx = new ParamInteger("Insertion Indexs"));
		insertidx.setValue(-1);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Concat Array Double Lists");
		inputParams.setName("Concat_Array_Double_Lists");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(objout = new ParamObject<ArrayList<double[][]>>("Resulting Array", new ArrayDoubleListTxtReaderWriter()));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		int idx = insertidx.getInt();
		ArrayList<double[][]> a= obja.getObject();
		ArrayList<double[][]> b= objb.getObject();
		ArrayList<double[][]> out;
		if(idx<0){
			for(double[][] d: b){
				a.add(d);
			}
			out=a;
		}else{
			out = new ArrayList<double[][]>();
			int i=0;
			int ai=0;
			int bi=0;
			
			while(i<(a.size()+b.size())){
				System.out.println(getClass().getCanonicalName()+"\t"+"i: " +i);
				if(i>=idx && i<(idx+b.size())){
					System.out.println(getClass().getCanonicalName()+"\t"+"Add from b");
					out.add(b.get(bi));
					bi++;
				}else{
					System.out.println(getClass().getCanonicalName()+"\t"+"Add from a");
					out.add(a.get(ai));
					ai++;
				}
				i++;
			}
		}
		objout.setObject(out);
		objout.setFileName(obja.getName()+"_cat");
	}
}
