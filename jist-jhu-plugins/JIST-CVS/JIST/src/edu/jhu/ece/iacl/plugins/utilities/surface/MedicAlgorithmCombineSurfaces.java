/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.surface;

import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmCombineSurfaces extends ProcessingAlgorithm{
	ParamSurfaceCollection inSurfs;
	ParamSurface outSurf;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inSurfs=new ParamSurfaceCollection("Surfaces"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Combine Surfaces");
		inputParams.setName("Combine_Surfaces");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Combined Surface"));

	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		List<EmbeddedSurface> surfs=inSurfs.getSurfaceList();
		int vertCount=0;
		int indexCount=0;
		int dim=0;
		for(EmbeddedSurface surf:surfs){
			vertCount+=surf.getVertexCount();
			indexCount+=surf.getIndexCount();
			double[][] dat=surf.getVertexData();
			if(dat!=null&&dat.length>0){
				dim=Math.max(dim,dat[0].length);
			}
		}
		Point3f[] verts=new Point3f[vertCount];
		Vector3f[] norms=new Vector3f[vertCount];
		double[][] data=new double[vertCount][dim];
		int[] indices=new int[indexCount];
		int iv=0,in=0;
		int talley=0;
		for(EmbeddedSurface surf:surfs){
			Point3f[] vs=surf.getVertexCopy();
			Vector3f[] ns=surf.getNormalCopy();
			int[] ids=surf.getIndexCopy();
			double[][] dat=surf.getVertexData();
			talley=iv;
			for(int i=0;i<vs.length;i++){
				if(dat!=null&&dat.length>0){
					for(int j=0;j<dat[i].length;j++){
						data[iv][j]=dat[i][j];
					}
				}
				norms[iv]=ns[i];
				verts[iv]=vs[i];
				iv++;
			}
			for(int i=0;i<ids.length;i++){
				indices[in++]=ids[i]+talley;
			}
		}
		EmbeddedSurface result=new EmbeddedSurface(verts,norms,indices,data);
		result.setName(surfs.get(0).getName()+"_combo");
		outSurf.setValue(result);
	}
}
