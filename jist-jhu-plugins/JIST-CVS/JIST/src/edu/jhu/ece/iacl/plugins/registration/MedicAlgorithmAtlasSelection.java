package edu.jhu.ece.iacl.plugins.registration;

import java.awt.Dimension;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.vabra.VabraAlgorithm;
import edu.jhu.ece.iacl.algorithms.vabra.VabraConfiguration;
import edu.jhu.ece.iacl.algorithms.vabra.VabraSolver;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

import gov.nih.mipav.model.algorithms.utilities.AlgorithmMatchImages;
import gov.nih.mipav.model.structures.ModelImage;


public class MedicAlgorithmAtlasSelection extends ProcessingAlgorithm {
	ParamFileCollection atlasPool;
	ParamFileCollection atlasPoolLabels;
	ParamFileCollection atlasPoolReged;
	ParamVolumeCollection selectedAtlases;
	ParamVolumeCollection selectedAtlasesLabels;

	private MedicAlgorithmVABRA vabra;
	ParamBoolean boundByLabels;
	ParamOption selectionMethod;
	ParamVolume subject;
	ParamVolume template;
	ParamVolume templateLabels;
	ParamInteger numOfAtlases;
	ParamFile config;

	private static final String cvsversion = "$Revision: 1.4 $".replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String revnum = VabraAlgorithm.getVersion();
	private static final String shortDescription = "Selects N Atlases from potential atlases by comparing the subject in template space against all potential atlases also in template space.\nAlgorithm Version: " + revnum + "\nGUI Version: " + cvsversion + "\n";
	private static final String longDescription = "NMI is used currently used as the comparison meteric.";


	public String[] getDefaultJVMArgs() {
		return new String[]{"-XX:MinHeapFreeRatio=60", "-XX:MaxHeapFreeRatio=90", "-XX:YoungGenerationSizeIncrement=100", "-XX:TenuredGenerationSizeIncrement=100"};
	}


	protected void createInputParameters(ParamCollection inputParams) {
		ParamCollection mainParams = new ParamCollection("Main");
		mainParams.add(subject = new ParamVolume("Subject"));
		mainParams.add(template = new ParamVolume("Template"));
		mainParams.add(templateLabels = new ParamVolume("Template Label"));

		mainParams.add(selectionMethod = new ParamOption("Selection method", new String[] { "NMI in Template Space", "NMI in Subject Space(Affined)"}));

		mainParams.add(boundByLabels = new ParamBoolean("Compare only within Label",false));

		selectionMethod.setValue(0);

		FileExtensionFilter volumefilter = ImageDataReaderWriter.getInstance().getExtensionFilter();

		mainParams.add(atlasPool = new ParamFileCollection("Potential Atlases"));
		atlasPool.setExtensionFilter(volumefilter);

		mainParams.add(atlasPoolLabels = new ParamFileCollection("Potential Atlases Labels"));
		atlasPoolLabels.setExtensionFilter(volumefilter);

		mainParams.add(atlasPoolReged = new ParamFileCollection("Potential Atlases Pre-Registered to Template"));
		atlasPoolReged.setExtensionFilter(volumefilter);

		mainParams.add(numOfAtlases = new ParamInteger("Number of Atlases to Select"));

		vabra = new MedicAlgorithmVABRA();
		vabra.inParamSubjects.setHidden(true);
		vabra.inParamTargets.setHidden(true);

		inputParams.add(mainParams);
		inputParams.add(vabra.getInput());




		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");
		inputParams.setLabel("Atlas Selection by Comparison in Template Space");
		inputParams.setName("Atlas_Selection_by_Comparison_in_Template_Space");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Computer Engineering");
		info.add(CommonAuthors.minChen);
		info.setDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(selectedAtlases = new ParamVolumeCollection("Selected Atlases"));
		outputParams.add(selectedAtlasesLabels = new ParamVolumeCollection("Selected Atlases Labels"));
		outputParams.setName("AtlasSelect");
	}


	protected void execute(CalculationMonitor monitor) {
		ImageData sub = subject.getImageData();
		ImageData tar = template.getImageData();

		List<File> atlasPoolFiles = atlasPool.getValue();
		List<File> atlasLabelsFiles = atlasPoolLabels.getValue();
		List<File> atlasRegedFiles = atlasPoolReged.getValue();
		ArrayList<ImageData> matchedSubject= new ArrayList<ImageData>();
		ArrayList<ImageData> matchedTarget = new ArrayList<ImageData>();
		List<ImageData> results;
		int boundingBox[] = new int[6];
		int topAtlases[] = new int[numOfAtlases.getInt()];
		double topAtlasesNMI[] = new double[numOfAtlases.getInt()];
		ImageData temp, subReged,subRegedNorm, atlasReged, atlasRegedNorm, resultVol;
		int rows, cols, slcs, i, j , k ,q;
		double currentNMI;
		ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();
		double MinandMaxValD[];

		//hardcoded parameters
		int N = 10;
		int numBins = 64;

		//initialize histograms
		int HistSub[] = new int[numBins];
		int HistAtlas[] = new int[numBins];
		int HistJoint[][] = new int[numBins][numBins];

		if(selectionMethod.getIndex() == 0)
		{
			//1.)Register Subject to Template space via VABRA
			//weights hard coded to one, since we're only using single channel VABRA
			List<Number> subjectWeight = new ArrayList<Number>(1);
			subjectWeight.add(1);
			List<Number> targetWeight = new ArrayList<Number>(1);
			targetWeight.add(1);


			matchedTarget.add(tar.clone());
			matchedSubject.add(sub.clone());


			vabra.inParamSubjects.setValue(matchedSubject);
			vabra.inParamTargets.setValue(matchedTarget);

			vabra.run();

			subReged = vabra.outParamDeformedSubject.getImageDataList().get(0);

			System.out.format("****************Subject Registration to Template Complete****************\n");
		} else {// if(selectionMethod.getIndex() == 1){
			subReged = sub;
		}


		//2.)Go through all atlases and compare against registered subject "subReged"


		for (q=0;q<Math.min(atlasPoolFiles.size(),atlasRegedFiles.size());q++) {
		
			if(selectionMethod.getIndex() == 0)	atlasReged = rw.read(atlasRegedFiles.get(q));
			else atlasReged = rw.read(atlasPoolFiles.get(q));
			
			MinandMaxValD = calculateMinAndMaxVals(subReged, atlasReged);
			subRegedNorm = subReged.clone();
			atlasRegedNorm = atlasReged.clone();
			normalize(subRegedNorm, MinandMaxValD[0], MinandMaxValD[1], numBins);
			normalize(atlasRegedNorm, MinandMaxValD[0], MinandMaxValD[1], numBins);
			
			if(boundByLabels.getValue() == true && selectionMethod.getIndex() == 0){		
				boundingBox = calculateBoundingBox(templateLabels.getImageData(),templateLabels.getImageData());
			}
			else boundingBox = calculateBoundingBox(subReged, atlasReged);
				
			RegistrationUtilities.Histogram3D(subRegedNorm, numBins, boundingBox, HistSub);
			RegistrationUtilities.Histogram3D(atlasRegedNorm, numBins, boundingBox,	HistAtlas);
			RegistrationUtilities.JointHistogram3D(subRegedNorm, atlasRegedNorm, numBins, boundingBox, HistJoint);
				
			currentNMI = RegistrationUtilities.NMI(HistSub, HistAtlas, HistJoint, numBins);

			System.out.format("NMI Between Atlas " + q + " and subject:" + currentNMI +"\n");
			for (i = 0; i < numOfAtlases.getInt();i++){
				if(currentNMI > topAtlasesNMI[i]){
				
					for(k = (numOfAtlases.getInt()-1); k > i; k--){
						topAtlases[k] = topAtlases[k-1];
		              	topAtlasesNMI[k] = topAtlasesNMI[k-1];
					}
					topAtlases[i] = q;
					topAtlasesNMI[i] = currentNMI;
					break;
					
				}
			}
		}
		
		for (i = 0; i < numOfAtlases.getInt();i++){
			temp = rw.read(atlasPoolFiles.get(topAtlases[i]));
			selectedAtlases.add(temp);
			temp = rw.read(atlasLabelsFiles.get(topAtlases[i]));
			selectedAtlasesLabels.add(temp);
			
		}
	}


	void normalize(ImageData sub, double minVals, double maxVals, int bins) {
		int k, j, i;
		float intervals = ((float) (maxVals - minVals + 1)) / bins;
		int XN = sub.getRows();
		int YN = sub.getCols();
		int ZN = sub.getSlices();
		
		for (i = 0; i < XN; i++) {
			for (j = 0; j < YN; j++) {
				for (k = 0; k < ZN; k++) {
					sub.set(i, j, k, RegistrationUtilities.calculateBin(intervals,
							minVals, sub.getDouble(i, j, k)));
				}
			}
		}
	}


	public double[] calculateMinAndMaxVals(ImageData sub, ImageData tar) {
		int ch;
		int CH;
		int i, j, k;
		int x = 0, y = 0, z = 0;
		int mx = 0, my = 0, mz = 0;

		int XN, YN, ZN;
		XN = sub.getRows();
		YN = sub.getCols();
		ZN = sub.getSlices();
		double MinandMaxValsD[] = new double[2];
		double max = Double.NEGATIVE_INFINITY;
		double min = Double.POSITIVE_INFINITY;
			for (i = 0; i < XN; i++) {
				for (j = 0; j < YN; j++) {
					for (k = 0; k < ZN; k++) {

						if (sub.getDouble(i, j, k) > max) {
							max = sub.getDouble(i, j, k);
							mx = i;
							my = j;
							mz = k;
						}
						if (sub.getDouble(i, j, k) < min) {
							min = sub.getDouble(i, j, k);
							x = i;
							y = j;
							z = k;
						}
						if (tar.getDouble(i, j, k) > max) {
							max = tar.getDouble(i, j, k);
							mx = i;
							my = j;
							mz = k;
						}
						if (tar.getDouble(i, j, k) < min) {
							min = tar.getDouble(i, j, k);
							x = i;
							y = j;
							z = k;
						}

					}
				}
			}
		
			MinandMaxValsD[0] = min;
			MinandMaxValsD[1] = max;
			//System.out.format("Max: " + MinandMaxValsD[0] + " Min" + MinandMaxValsD[1] + "\n");
			return MinandMaxValsD;
	}


	int[] calculateBoundingBox(ImageData sub, ImageData tar) {
		int k, j, i;
		int XN, YN, ZN, ext;
		int boundingBox[] = new int[6];
		XN = sub.getRows();
		YN = sub.getCols();
		ZN = sub.getSlices();
		ext = 5;
		
		boundingBox[1] = 0;
		boundingBox[0] = XN;
		boundingBox[3] = 0;
		boundingBox[2] = YN;
		boundingBox[5] = 0;
		boundingBox[4] = ZN;
		
		int count=0;
			for (i = 0; i < XN; i++){
				for (j = 0; j < YN; j++){
					for (k = 0; k < ZN; k++){
						// if (subject.data[k][j][i][ch] > 0 ||
						// target.data[k][j][i][ch] > 0)
						if ((Math.abs(sub.getDouble(i, j, k)) > 0.0000001) ||
								(Math.abs(tar.getDouble(i, j, k)) > 0.0000001))
							// changed to match Xiao Feng's abra implementation
						{
							count++;
							if (i < boundingBox[0])
								boundingBox[0] = i;
							if (i > boundingBox[1])
								boundingBox[1] = i;
							if (j < boundingBox[2])
								boundingBox[2] = j;
							if (j > boundingBox[3])
								boundingBox[3] = j;
							if (k < boundingBox[4])
								boundingBox[4] = k;
							if (k > boundingBox[5])
								boundingBox[5] = k;
						}
					}
				}
			}
		

		
		boundingBox[0]=Math.max(0, boundingBox[0]-ext); 			
		boundingBox[1]=Math.min(XN-1, boundingBox[1]+ext);
		boundingBox[2]=Math.max(0, boundingBox[2]-ext); 			
		boundingBox[3]=Math.min(YN-1, boundingBox[3]+ext);
		boundingBox[4]=Math.max(0, boundingBox[4]-ext); 			
		boundingBox[5]=Math.min(ZN-1, boundingBox[5]+ext);
			
		return boundingBox;
		
		/*for (i = 0; i < 6; i++) {
			System.out.format("bb[%d]:%d\n",i,boundingBox[i]);
		}*/

	}
}
