package edu.jhu.ece.iacl.algorithms.manual_label.staple;

/**
 * 
 * @author John Bogovic
 * @date 5/31/2008
 * 
 * Simultaneous Truth and Performance Level Estimation (STAPLE)
 * 
 * Warfield, Zou, and Wells, "Simultaneous Truth and Performace Level Estimation (STAPLE):
 * An Algorithm for the Validation of Image Segmentation," 
 * IEEE Trans. Medical Imaging vol. 23, no. 7, 2004
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt.Connectivity;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;

public class STAPLEmulti extends AbstractCalculation{

	protected int[][][][] imagesArray;
	protected float[][][][] imagesArrayFloat;
	protected List<ImageData> images;
	protected float[][][][] truthImage;
	protected ArrayList<ImageData> truth;
	protected ArrayList<ImageData> truthclone; //used by mrf
	protected ImageDataInt hardseg;
	
	//variables for efficiency
	protected int MaxUniqueLabels=0;
	protected int[][][][] uniqueLabels;
	int labelshere = 0;
	protected HashMap<Integer,Integer> labelstoindex;
	
	protected PerformanceLevel pl; //performance level estimates
	protected ArrayList<Float> priors;
	protected ArrayList<Integer> labels;
	protected double convergesum;
	protected float normtrace;
	protected int maxiters = 1000;
	protected String dir;
	protected double eps=.00001;
	protected String initType;
	protected int rows, cols, slices, raters;
	
	//MRF parameters
	private float beta =0f;
	private Connectivity connectivity;
	private int connNbrs;
	private int maxMRFiters = 15;
	private int minMRFiters = 5;
	StapleMRFUtil mrfUtil;
	protected String mrfMethod = "ICM";
	
	
	private boolean keepgoing = true;
	private boolean efficient = false;
	private ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();
	
	public STAPLEmulti(){
		super();
		setLabel("STAPLE");
	}
	public STAPLEmulti(int[][][][] img){
		super();
		setLabel("STAPLE");
		
		imagesArray=img;
		truthImage = new float[imagesArray.length][imagesArray[0].length][imagesArray[0][0].length][imagesArray[0][0][0].length];
		rows = imagesArray.length;
		cols = imagesArray[0].length;
		slices = imagesArray[0][0].length;
		raters = imagesArray[0][0][0].length;
		getPriorProb();
	}
	public STAPLEmulti(float[][][][] img){
		super();
		setLabel("STAPLE");
		
		imagesArrayFloat=img;
		truthImage = new float[imagesArrayFloat.length][imagesArrayFloat[0].length][imagesArrayFloat[0][0].length][imagesArrayFloat[0][0][0].length];
		rows = imagesArrayFloat.length;
		cols = imagesArrayFloat[0].length;
		slices = imagesArrayFloat[0][0].length;
		raters = imagesArrayFloat[0][0][0].length;
		getPriorProb();
	}
	public STAPLEmulti(List<ImageData> img){
		super();
		setLabel("STAPLE");
		images = img;
		rows = images.get(0).getRows();
		cols = images.get(0).getCols();
		slices = images.get(0).getSlices();
		raters = images.size();
		if(verifySizes()){
			getPriorProb();
		}else{
			System.err.println("jist.plugins"+"Rater images must have equal dimensions");
		}
		
		System.out.println("jist.plugins"+"\t"+"Priors: ");
		printArray(getPriorArray());
	}
	
	public void setmaxIters(int max){
		maxiters=max;
	}
	public void setEps(double eps){
		this.eps=eps;
	}
	
	public void setImages(int[][][][] img){
		imagesArray=img;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}
	public void setInit(String init){
		initType=init;
	}
	public void setBeta(float beta){
		this.beta=beta;
	}
	public void distributeBeta(){
		if(connectivity==Connectivity.SIX){
			beta=beta/6;
		}else if(connectivity==Connectivity.EIGHTEEN){
			beta=beta/18;
		}else if(connectivity==Connectivity.TWENTYSIX){
			beta=beta/26;
		}else{
			System.out.println("jist.plugins"+"\t"+"Invalid Connectivity!");
		}
	}	
	public void setConnectivity(Connectivity c){
		connectivity = c;
		if(connectivity==Connectivity.SIX){
			connNbrs=6;
		}else if(connectivity==Connectivity.EIGHTEEN){
			connNbrs=18;
		}else if(connectivity==Connectivity.TWENTYSIX){
			connNbrs=26;
		}else{
			connNbrs=-1;
		}
	}
	public GridPt[] getNeighbors(int i, int j, int k){
		if(connectivity==Connectivity.SIX){
			return GridPt.onlyInBounds(GridPt.neighborhood6C(i, j, k), rows, cols, slices);
		}else if(connectivity==Connectivity.EIGHTEEN){
			return GridPt.onlyInBounds(GridPt.neighborhood18C(i, j, k), rows, cols, slices);
		}else if(connectivity==Connectivity.TWENTYSIX){
			return  GridPt.onlyInBounds(GridPt.neighborhood26C(i, j, k), rows, cols, slices);
		}else{
			System.out.println("jist.plugins"+"\t"+"Invalid Connectivity!");
			return null;
		}
		
	}
	public boolean verifySizes(){
		int rows = images.get(0).getRows();
		int cols = images.get(0).getCols();
		int slcs = images.get(0).getSlices();
		for(int i=1; i<images.size(); i++){
			if(images.get(i).getRows()!=rows || images.get(i).getCols()!=cols ||
			   images.get(i).getSlices()!=slcs){
				return false;
			}
		}
		return true;
	}
	
	public ArrayList<Integer> getLabels(){
		return labels;
	}
	
	public int[] getLabelListArray(){
		int[] out = new int[labels.size()];
		for(int i=0; i<out.length; i++){
			out[i]=labels.get(i);
		}
		return out;
	}
	
	public void findLabels(){
		
		labels = new ArrayList<Integer>();
		MaxUniqueLabels = 0;
		HashSet<Integer> labelshere = new HashSet<Integer>();
		labelstoindex = new HashMap<Integer,Integer>();
		if(imagesArray!=null){
			int num = 0;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						labelshere.clear();
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							labelshere.add(imagesArray[i][j][k][l]);
							if(!labels.contains(imagesArray[i][j][k][l])){
								labels.add(imagesArray[i][j][k][l]);
								labelstoindex.put(imagesArray[i][j][k][l],num);
								num++;
							}
							if(labelshere.size()>MaxUniqueLabels){ MaxUniqueLabels = labelshere.size(); } 
						}
					}
				}
			}
		}else if(imagesArrayFloat!=null){
			int num = 0;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						labelshere.clear();
						for(int l=0; l<imagesArrayFloat[0][0][0].length; l++){
							labelshere.add((int)imagesArrayFloat[i][j][k][l]);
							if(!labels.contains((int)imagesArrayFloat[i][j][k][l])){
								labels.add((int)imagesArrayFloat[i][j][k][l]);
								labelstoindex.put((int)imagesArrayFloat[i][j][k][l],num);
								num++;
							}
							if(labelshere.size()>MaxUniqueLabels){ MaxUniqueLabels = labelshere.size(); } 
						}
					}
				}
			}
		}else if(images!=null){
			int num =0;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						labelshere.clear();
						for(int l=0; l<images.size(); l++){
							labelshere.add(images.get(l).getInt(i,j,k));
							if(!labels.contains(images.get(l).getInt(i,j,k))){
								labels.add(images.get(l).getInt(i,j,k));
								labelstoindex.put(images.get(l).getInt(i,j,k),num);
								num++;
							}
							if(labelshere.size()>MaxUniqueLabels){ MaxUniqueLabels = labelshere.size(); }
						}
					}
				}
			}
		}else{
			System.err.println("jist.plugins"+"No input rater data");
			return;
		}
		labels.trimToSize();
		Collections.sort(labels);
		System.out.println("jist.plugins"+"\t"+"Found Labels: ");
		System.out.println("jist.plugins"+"\t"+labels);
		System.out.println("jist.plugins"+"\t"+"");
	}
	
//	public void initializeSingle(){
//		float init = 0.9999f;
//		try{
//			if(imagesArray!=null){
//				pl=new float[imagesArray[0][0][0].length][2];
//				for(int l=0; l<pl.length; l++){
//					pl[l][0]=init;
//					pl[l][1]=init;
//				}
//			}
//			else if(images!=null){
//				pl = new float[images.size()][2];
//				for(int l=0; l<pl.length; l++){
//					pl[l][0]=init;
//					pl[l][1]=init;
//				}
//			}
//			else{
//				System.err.println("jist.plugins"+"Rater data is null");
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	}
	
	public void initialize(){
		if(initType.equals("Truth")){

			
			System.out.println("jist.plugins"+"\t"+"Initializing Truth");
			findLabels();
			if(efficient){
				initUniqueLabelList();
			}
			System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
			pl = new PerformanceLevel(labels.size(), raters);
			truth = new ArrayList<ImageData>();
			System.out.println("jist.plugins"+"\t"+"Num Rater Images: " + raters);
			for(int i=0; i<labels.size(); i++){
				truth.add(new ImageDataFloat("TruthEstimate_"+labels.get(i),images.get(0).getRows(),images.get(0).getCols(),images.get(0).getSlices()));
				if(images!=null){
					truth.get(i).setHeader(images.get(0).getHeader().clone());
				}
			}
			System.out.println("jist.plugins"+"\t"+"Num Truth Images: " + truth.size());
			
			// Initialize the Truth Estimates
			
			double d = 1/raters;

			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<raters; l++){
							if(images!=null){
								int t = getIndex(labels,images.get(l).getInt(i, j, k));
								truth.get(t).set(i, j, k, truth.get(t).getFloat(i, j,k)+d);
							}else if(imagesArray!=null){
								int t = getIndex(labels,imagesArray[i][j][k][l]);
								truth.get(t).set(i, j, k, truth.get(t).getFloat(i, j,k)+d);
							}else if(imagesArrayFloat!=null){
								int t = getIndex(labels,(int)imagesArrayFloat[i][j][k][l]);
								truth.get(t).set(i, j, k, truth.get(t).getFloat(i, j,k)+d);
							}
						}
					}
				}
			}
		
			
		}else{
			System.out.println("jist.plugins"+"\t"+"Initializing Performance Levels");
			float init = 0.9999f;
			try{
				findLabels();
				labels.trimToSize();
				System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
				pl = new PerformanceLevel(labels.size(), raters);
				//Initialize the Performance Level Estimates
				pl.initialize(init);
				if(efficient){
					initUniqueLabelList();
					truth = new ArrayList<ImageData>(MaxUniqueLabels);
					for(int i=0; i<MaxUniqueLabels; i++){
						truth.add(new ImageDataFloat("TruthEstimate_"+labels.get(i),rows,cols,slices));
					}
				}else{
					truth = new ArrayList<ImageData>(labels.size());
					for(int i=0; i<labels.size(); i++){
						truth.add(new ImageDataFloat("TruthEstimate_"+labels.get(i),rows,cols,slices));
						if(images!=null){
							truth.get(i).setHeader(images.get(0).getHeader().clone());
						}
					}
				}
				System.out.println("jist.plugins"+"\t"+"Num Truth Images: " + truth.size());
				printPerformanceLevels();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void initUniqueLabelList(){
		HashSet<Integer> labelshere = new HashSet<Integer>();
		if(images!=null){
			System.out.println("jist.plugins"+"\t"+"initializing unique label lists from raters");
			uniqueLabels = new int[images.get(0).getRows()][images.get(0).getCols()][images.get(0).getSlices()][MaxUniqueLabels];
			int num = images.size();
			int index = -1;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						labelshere.clear();
						index = 0;
						for(int l=0; l<num; l++){
//							System.out.println("jist.plugins"+"\t"+"labelshere " + labelshere);
							if(!labelshere.contains(images.get(l).getInt(i, j, k))){
//								System.out.println("jist.plugins"+"\t"+"adding");
								labelshere.add(images.get(l).getInt(i, j, k));
								uniqueLabels[i][j][k][index]=images.get(l).getInt(i, j, k);
								index++;
							}
						}
						for(int a = index; a<MaxUniqueLabels; a++){
							uniqueLabels[i][j][k][a]=-1;
						}
						
					}
				}
			}
		}else if(imagesArray!=null){
			System.out.println("jist.plugins"+"\t"+"initializing unique label lists from rater array");
			uniqueLabels = new int[imagesArray.length][imagesArray[0].length][imagesArray[0][0].length][MaxUniqueLabels];

			int num = images.size();
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						labelshere.clear();
						int index = 0;
						//assign the values of uniqueLabels the elements of 
						for(int l=0; l<num; l++){
							if(!labels.contains(imagesArray[i][j][k][l])){
								labelshere.add(imagesArray[i][j][k][l]);
								uniqueLabels[i][j][k][index]=imagesArray[i][j][k][l];
								index++;
							}
						}
						for(int a = index; a<MaxUniqueLabels; a++){
							uniqueLabels[i][j][k][a]=-1;
						}
					}
				}
			}
		}
	}
	
	public void EstepEfficient(){
		if(imagesArray!=null){
//			System.err.println("jist.plugins"+"Not yet implemented");
			convergesum=0;
			int mindex = -1;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						float[] a = getPriorArray(uniqueLabels[i][j][k]);
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							//Compute 'a' (Eqn 14)
							//Compute 'b' (Eqn 15)
							
							for(int m=0; m<labelshere; m++){
								mindex = getIndex(labels,uniqueLabels[i][j][k][m]);
								if(mindex>-1){
									System.out.println("jist.plugins"+"\t"+"Label: " );
									int t = getIndex(labels,imagesArray[i][j][k][l]);
									if(t>-1){
										a[m]=a[m]*pl.get(l, t, mindex);
									}else{
										System.err.println("jist.plugins"+"Could not find label!");
									}
								}else{
									break;
								}
							}
						}
//						Compute weights for truth using Eqn (16)
						double sum=0;
						for(int m=0; m<labelshere; m++){
							sum=sum+a[m];
						}
						for(int n=0; n<labelshere; n++){
							truth.get(n).set(i,j,k, a[n]/sum);
							convergesum=convergesum+truth.get(n).getFloat(i,j,k);
						}

					}
				}
			}
		}else if(images!=null){
			convergesum=0;
			int mindex = -1;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						float[] a = getPriorArray(uniqueLabels[i][j][k]);
						for(int l=0; l<images.size(); l++){
							//Compute 'a' (Eqn 14)
							//Compute 'b' (Eqn 15)
							
							for(int m=0; m<labelshere; m++){
								if(uniqueLabels[i][j][k][m]>-1){
									mindex = getIndex(labels,uniqueLabels[i][j][k][m]);
									if(mindex>-1){
										int t = getIndex(labels,images.get(l).getInt(i, j, k));
										if(t>-1){
											a[m]=a[m]*pl.get(l, t, mindex);
										}else{
											System.err.println("jist.plugins"+"Could not find label!");
										}
									}else{
										break;
									}
								}else{
									break;
								}
							}
						}
//						System.out.println("jist.plugins"+"\t"+"Local probs");
//						printArray(a);
//						System.out.println("jist.plugins"+"\t"+"");
//						Compute weights for truth using Eqn (16)
						float sum=0;
						for(int m=0; m<labelshere; m++){
							sum=sum+a[m];
						}
						for(int n=0; n<labelshere; n++){
							truth.get(n).set(i,j,k, a[n]/sum);
//							if(Float.isNaN(truth.get(n).get(i, j, k).intValue())){
//								System.out.println("jist.plugins"+"\t"+"U.n. prob: " + a[n]);
//								System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//							}else if(truth.get(n).getFloat(i, j, k)==0){
//								System.out.println("jist.plugins"+"\t"+"local label index: " + n);
//								System.out.println("jist.plugins"+"\t"+"labelshere: " + labelshere);
//								System.out.println("jist.plugins"+"\t"+"U.n. prob: " + a[n]);
//								System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//								printArray(uniqueLabels[i][j][k]);
//								System.out.println("jist.plugins"+"\t"+" ");
//							}
							convergesum=convergesum+truth.get(n).getFloat(i,j,k);
							
						}

					}
				}
			}
		}
		
	}
	
	public void Estep(){
		if(imagesArray!=null){
			convergesum=0;

			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						float[] a = getPriorArray();
						for(int l=0; l<raters; l++){
							//Compute 'a' (Eqn 14)
							//Compute 'b' (Eqn 15)
							int t = getIndex(labels,imagesArray[i][j][k][l]);
							for(int m=0; m<labels.size(); m++){
//								System.out.println("jist.plugins"+"\t"+"Label: " );
								if(t>-1){
									a[m]=a[m]*pl.get(l, t, m);
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
							}
						}
//						Compute weights for truth using Eqn (16)
						float sum=0;
						for(int m=0; m<labels.size(); m++){
							sum=sum+a[m];
						}
						for(int n=0; n<truth.size(); n++){
							truth.get(n).set(i,j,k, a[n]/sum);
							convergesum=convergesum+truth.get(n).getFloat(i,j,k);
						}
					}
				}
			}
		}else if(imagesArrayFloat!=null){
			convergesum=0;

			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						float[] a = getPriorArray();
						for(int l=0; l<raters; l++){
							//Compute 'a' (Eqn 14)
							//Compute 'b' (Eqn 15)
							int t = getIndex(labels,(int)imagesArrayFloat[i][j][k][l]);
							for(int m=0; m<labels.size(); m++){
//								System.out.println("jist.plugins"+"\t"+"Label: " );
								if(t>-1){
									a[m]=a[m]*pl.get(l, t, m);
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
							}
						}
//						Compute weights for truth using Eqn (16)
						float sum=0;
						for(int m=0; m<labels.size(); m++){
							sum=sum+a[m];
						}
						for(int n=0; n<truth.size(); n++){
							truth.get(n).set(i,j,k, a[n]/sum);
							convergesum=convergesum+truth.get(n).getFloat(i,j,k);
						}
					}
				}
			}
		}else if(images!=null){
			convergesum=0;

			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						float[] a = getPriorArray();
						for(int l=0; l<raters; l++){
							//Compute 'a' (Eqn 14)
							//Compute 'b' (Eqn 15)

							for(int m=0; m<labels.size(); m++){
//								System.out.println("jist.plugins"+"\t"+"Label: " );
								int t = getIndex(labels,images.get(l).getInt(i, j, k));
								if(t>-1){
									a[m]=a[m]*pl.get(l, t, m);
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
							}
						}
//						Compute weights for truth using Eqn (16)
						float sum=0;
						for(int m=0; m<labels.size(); m++){
							sum=sum+a[m];
						}
						for(int n=0; n<truth.size(); n++){
							truth.get(n).set(i,j,k, a[n]/sum);
							convergesum=convergesum+truth.get(n).getFloat(i,j,k);
						}

					}
				}
			}
		}

	}
	
	
	public void MstepEfficient(){
		if(imagesArray!=null){
			
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)

			float[] totsum = new float[labels.size()];
			
			//DO I WANT TO CLEAR THE PERFORMANCE LEVELS HERE? 
			//YES I DO
			pl.clear();
			int mtindex = -1;
//			int ltindex = -1;
			int t = -1;
			for(int m=0; m<labels.size(); m++){
				
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slices; k++){
							mtindex = getIndex(uniqueLabels[i][j][k],labels.get(m));
							if(mtindex>-1){
								totsum[m]+=truth.get(mtindex).getFloat(i,j,k);
								for(int l=0; l<imagesArray[0][0][0].length; l++){
//									ltindex = getIndex(uniqueLabels[i][j][k],imagesArray[i][j][k][l]);
									t = getIndex(labels,imagesArray[i][j][k][l]);
									if(t>-1){
										pl.set(l, t, m, pl.get(l, t, m)+truth.get(mtindex).getFloat(i,j,k));
										if(Float.isNaN(pl.get(l, t, m))){ keepgoing = false; }
									}else{
										System.err.println("jist.plugins"+"Could not find label!");
									}

								}
							}else{
								break;
							}
						}
					}
				}
			}
			// Store performance parameter estimates for this iteration
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
			
//			System.out.println("jist.plugins"+"\t"+pl);
		}else if(images!=null){

			
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)

			float[] totsum = new float[labels.size()];
			
			//DO I WANT TO CLEAR THE PERFORMANCE LEVELS HERE?
			//YES I DO
			pl.clear();
			int mtindex = -1;
//			int ltindex = -1;
			int t = -1;
			for(int m=0; m<labels.size(); m++){
				System.out.println("jist.plugins"+"\t"+"Working on label " + m + ": " + labels.get(m));
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slices; k++){
							mtindex = getIndex(uniqueLabels[i][j][k],labels.get(m));
							if(m>0){
								System.out.println("jist.plugins"+"\t"+"label: " + labels.get(m) + "   mindex: "+mtindex);
								printArray(uniqueLabels[i][j][k]);
							}
							
							if(mtindex>-1){
								totsum[m]+=truth.get(mtindex).getFloat(i,j,k);
								
								if(m>0){
									System.out.println("jist.plugins"+"\t"+"In here");
									System.out.println("jist.plugins"+"\t"+truth.get(mtindex).getFloat(i,j,k));
									printArray(uniqueLabels[i][j][k]);
									printArray(totsum);
								}
								for(int l=0; l<images.size(); l++){
									t = getIndex(labels,images.get(l).getInt(i, j, k));
//									ltindex = getIndex(uniqueLabels[i][j][k],images.get(l).getInt(i, j, k));
									if(t>-1){
										pl.set(l, t, m, pl.get(l, t, m)+truth.get(mtindex).getFloat(i,j,k));
										if(Float.isNaN(pl.get(l, t, m))){ keepgoing = false; }
									}else{
										System.err.println("jist.plugins"+"Could not find label!");
									}

								}
							}else{
								break;
							}
						}
					}
				}
			}
			System.out.println("jist.plugins"+"\t"+"Totsum:");
			printArray(totsum);
			// Store performance parameter estimates for this iteration
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
			
//			System.out.println("jist.plugins"+"\t"+pl);
			
		}
	}

	public void Mstep(){
		if(imagesArray!=null){
			
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)

			float[] totsum = new float[labels.size()];
			
			//DO I WANT TO CLEAR THE PERFORMANCE LEVELS HERE?
			pl.clear();
			
			for(int m=0; m<labels.size(); m++){
//				float totsum = 0f;
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slices; k++){
							totsum[m]=totsum[m]+truth.get(m).getFloat(i,j,k);
							for(int l=0; l<raters; l++){
								
								int t = getIndex(labels,imagesArray[i][j][k][l]);
								if(t>-1){
									pl.set(l, t, m, pl.get(l, t, m)+truth.get(m).getFloat(i,j,k));
									if(Float.isNaN(pl.get(l, t, m))){ keepgoing = false; }
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
								
							}
						}
					}
				}
			}
			// Store performance parameter estimates for this iteration
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
			
			System.out.println("jist.plugins"+"\t"+pl);
		}else if(imagesArrayFloat!=null){
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)

			float[] totsum = new float[labels.size()];
			
			//DO I WANT TO CLEAR THE PERFORMANCE LEVELS HERE?
			pl.clear();
			
			for(int m=0; m<labels.size(); m++){
//				float totsum = 0f;
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slices; k++){
							totsum[m]=totsum[m]+truth.get(m).getFloat(i,j,k);
							for(int l=0; l<raters; l++){
								
								int t = getIndex(labels,(int)imagesArrayFloat[i][j][k][l]);
								if(t>-1){
									pl.set(l, t, m, pl.get(l, t, m)+truth.get(m).getFloat(i,j,k));
									if(Float.isNaN(pl.get(l, t, m))){ keepgoing = false; }
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
								
							}
						}
					}
				}
			}
			// Store performance parameter estimates for this iteration
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
			
			System.out.println("jist.plugins"+"\t"+pl);
		}else if(images!=null){

			
//			Compute performance parameters given the truth
			// using Eqns (18) & (19)

			float[] totsum = new float[labels.size()];
			
			//DO I WANT TO CLEAR THE PERFORMANCE LEVELS HERE?
			pl.clear();
			
			for(int m=0; m<labels.size(); m++){
//				float totsum = 0f;
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slices; k++){
							totsum[m]=totsum[m]+truth.get(m).getFloat(i,j,k);
							for(int l=0; l<raters; l++){
								
								int t = getIndex(labels,images.get(l).getInt(i, j, k));
								if(t>-1){
									pl.set(l, t, m, pl.get(l, t, m)+truth.get(m).getFloat(i,j,k));
									if(Float.isNaN(pl.get(l, t, m))){ keepgoing = false; }
								}else{
									System.err.println("jist.plugins"+"Could not find label!");
								}
								
							}
						}
					}
				}
			}
			// Store performance parameter estimates for this iteration
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
			
			System.out.println("jist.plugins"+"\t"+pl);
			
		}
	}
	
	public void getPriorProb(){
		labels = new ArrayList<Integer>();
		priors = new ArrayList<Float>();
		float total = raters*rows*cols*slices;
		if(imagesArray!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							if(!labels.contains(imagesArray[i][j][k][l])){
								labels.add(imagesArray[i][j][k][l]);
								priors.add(new Float(1f));
							}else{
								int thisone = getIndex(labels,imagesArray[i][j][k][l]);
								priors.set(thisone, priors.get(thisone)+1);
							}
						}
					}
				}
			}
//			System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//			System.out.println("jist.plugins"+"\t"+"Total" + total);
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
			priors.trimToSize();
		}else if(imagesArrayFloat!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<imagesArrayFloat[0][0][0].length; l++){
							if(!labels.contains((int)imagesArrayFloat[i][j][k][l])){
								labels.add((int)imagesArrayFloat[i][j][k][l]);
								priors.add(new Float(1f));
							}else{
								int thisone = getIndex(labels,(int)imagesArrayFloat[i][j][k][l]);
								priors.set(thisone, priors.get(thisone)+1);
							}
						}
					}
				}
			}
//			System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//			System.out.println("jist.plugins"+"\t"+"Total" + total);
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
			priors.trimToSize();
		}else if(images!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<images.size(); l++){
							if(!labels.contains(images.get(l).getInt(i, j, k))){
								labels.add(images.get(l).getInt(i, j, k));
								priors.add(new Float(1f));
							}else{
								int thisone = getIndex(labels,images.get(l).getInt(i, j, k));
								priors.set(thisone, priors.get(thisone)+1);
							}
						}
					}
				}
			}
//			System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//			System.out.println("jist.plugins"+"\t"+"Total" + total);
			priors.trimToSize();
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
			priors.trimToSize();
		}else{
			System.err.println("jist.plugins"+"Rater data is null");
		}
	}
	
	public void iterate(){
		if(initType.equals("Truth")){
			initialize();
		}else{
			initialize();
			if(efficient){
				EstepEfficient();
			}else{
				Estep();
			}
		}
		
		double prevcs = convergesum;
		int iters = 0;	
		
		float ntrace = 0;
		while(keepgoing && iters<maxiters){
//			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
			if(efficient){
				MstepEfficient();
				EstepEfficient();
			}else{
				Mstep();
				Estep();
			}
//			writeTruth(new File(dir),"TruthEst_Iteration_"+iters);
//			writeHardSeg(new File(dir),"SegEst_Iteration_"+iters);
			if(beta>0){
				if(mrfMethod.equals("ICM")){
					computeHardSeg();
//					writeHardSeg(new File(dir),"SegEst2_Iteration_"+iters);
					mrfICM();
				}else if(mrfMethod.equals("RL")){
					mrfRL();
				}
//				writeTruth(new File(dir),"TruthEstMrf_Iteration_"+iters);
//				writeHardSeg(new File(dir),"SegEstMrf_Iteration_"+iters);
			}
			ntrace = pl.normalizedTrace();
			if(Math.abs(ntrace-normtrace)<eps){
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
//				printPerformanceLevels();
			}else{
				System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Converge Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);

				System.out.println("jist.plugins"+"\t"+"*****************");
			}
//			if(Math.abs(prevcs-convergesum)<eps){
//				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
//				keepgoing=false;
//				printPerformanceLevels();
//			}
//			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
//			System.out.println("jist.plugins"+"\t"+"Prev Sum: " +prevcs);
//			System.out.println("jist.plugins"+"\t"+"Converge Sum: " +convergesum);
//			System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(prevcs-convergesum));
//			System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
//			System.out.println("jist.plugins"+"\t"+"*****************");

			
			iters++;
			prevcs = convergesum;
			normtrace=ntrace;
		}
		
	}
	
	private void mrfRL(){
		System.out.print("Apply MRF, RL optimization...");
//		float max=0f;
		float[] pot=null;
		float[] w =null;
		float normpot =0f; //normalization factor
//		int label=0;
		int it=0;
		while(it<maxMRFiters){
			deepCloneTruth();
			System.out.print(it+"...");
			GridPt[] nbrs;
			for(int i=0; i<rows; i++){  	//for each voxel
				for(int j=0; j<cols; j++){ 
					for(int k=0; k<slices; k++){ 
						
						//compute weights for each neighbor
						w = computeUniformNbrWeights(connNbrs);

						//compute the potential for each label choose the max
//						max = Float.MIN_VALUE;
						pot = new float[truth.size()];
						normpot=0;
//						label = -1;
						//loop over labels
						for(int l=0; l<truth.size(); l++){
							//add the contribution of this
							pot[l]=truth.get(l).getFloat(i,j,k);
							normpot+=pot[l];
							//add contribution for each neighbor
							nbrs = getNeighbors(i,j,k);
							for(int n=0; n<nbrs.length; n++){
								if(nbrs[n]!=null){
									pot[l]+=w[n]*truthclone.get(l).getFloat(nbrs[n].x,nbrs[n].y,nbrs[n].z);
									normpot+=pot[l];
								}
							}
							//the label with the max potential is the true label
//							if(pot[j]>max){
//								label=j;
//								max=pot[j];
//							}
						}

						//set newly computed label probabilities at this location
						for(int l=0; l<truth.size(); l++){
							truth.get(l).set(i,j,k,(pot[l]/normpot));
						}
					}
				}
			}
			it++;
		}
		System.out.print("\n");
		
	}
	
	/**
	 * Applies spatial consistency to the rater data in the form of an MRF.
	 * Optimization is performed with the ICM algorithm.
	 */
	private void mrfICM(){
		if(mrfUtil==null){
			mrfUtil=new StapleMRFUtil(rows,cols,slices,true);
		}
		
		System.out.print("Apply MRF, ICM optimization...");

		//init variables 
		float[] problist = new float[labels.size()];
		int i =-1;	//coord indices
		int j=-1;
		int k=-1;
		int numchanged = 1;
		GridPt[] nbrs = null;	//list of neighbors of the current voxel
		int l = 0;
		int m = 0;
		int n = 0;
		int nbrlabind = 0;
		float maxprob = -1f;
		int maxproblabelindex = -1;
		double convergesum = 2;
		double prevconverge = 1;
		
		if(connectivity==Connectivity.SIX){		//we can split the volume into two non-overlapping portions
												//and can save some memory 
			while(n<maxMRFiters && Math.abs(convergesum-prevconverge)>0.00001){
				System.out.print(n+"...");
				numchanged = 0;
				convergesum = 0;
				computeHardSeg();
				boolean[] tf = new boolean[]{true, false};
				//first iteration handles even voxels
				//second iteration handles odd voxels
				for(int b = 0; b<2; b++){
					
					mrfUtil.reset();
					mrfUtil.setEvenSlice(tf[b]);
					while(mrfUtil.hasNext()){
						mrfUtil.updateCoords();
						i=mrfUtil.thisrow;
						j=mrfUtil.thiscol;
						k=mrfUtil.thisslice;
						
						nbrs = getNeighbors(i,j,k);
						
//						//compute the probability contribution from this point
						for(l=0; l<labels.size(); l++){
							problist[l]=truth.get(l).getFloat(i, j, k);
						}
//						//add contributions from neighbors
						for(l=0; l<nbrs.length; l++){
							if(nbrs[l]!=null){
								nbrlabind = labelstoindex.get(hardseg.get(nbrs[l].x, nbrs[l].y, nbrs[l].z));
								problist[nbrlabind]+=beta*truth.get(nbrlabind).getFloat(nbrs[l].x, nbrs[l].y, nbrs[l].z);
							}
							
						}
						
						//find max prob
						maxprob = problist[0];
						maxproblabelindex = 0;
						for(l=1; l<labels.size(); l++){
							if(problist[l]>maxprob){
								maxproblabelindex=l;
								maxprob=problist[l];
							}
						}
						
						if(labels.get(maxproblabelindex)!=hardseg.getFloat(i, j, k)){
							hardseg.set(i,j,k,labels.get(maxproblabelindex));
							numchanged++;
						}
						
						//need to readjust truth so that it agrees with the new hard seg
						//do this by setting the truth values equal to their probabilities
						//obtained using all the neighbors - be sure to normalize!
						for(l=0; l<labels.size(); l++){
							truth.get(l).set(i,j,k,problist[l]);
						}
						renormalizeTruth(i,j,k);
						prevconverge = convergesum;
						convergesum+= truth.get(0).getDouble(i, j, k);
					}
				}
//				System.out.println("jist.plugins"+"\t"+"Iteration " +n);
//				System.out.println("jist.plugins"+"\t"+"Voxels changed: " +numchanged);
//				System.out.println("jist.plugins"+"\t"+"Converge diff: " + Math.abs(convergesum-prevconverge));
				n++;
				System.out.print("done\n");
			}
		}else{
			while(n<maxMRFiters &&  Math.abs(convergesum-prevconverge)>0.00001){
				numchanged = 0;
				deepCloneTruth();	//make a deep copy of the truth
				computeHardSeg();
				
				for(i=0; i<rows; i++){
					for(j=0; j<cols; j++){
						for(k=0; k<slices; k++){
							
							nbrs = getNeighbors(i,j,k);
							
//							//compute the probability contribution from this point
							for(l=0; l<labels.size(); l++){
								problist[l]=truthclone.get(l).getFloat(i, j, k);
							}
//							//add contributions from neighbors
							for(l=0; l<nbrs.length; l++){
								if(nbrs[l]!=null){
									nbrlabind = labelstoindex.get(hardseg.get(nbrs[l].x, nbrs[l].y, nbrs[l].z));
									problist[nbrlabind]+=beta*truthclone.get(nbrlabind).getFloat(nbrs[l].x, nbrs[l].y, nbrs[l].z);
								}
							}
							
							//find max prob
							maxprob = problist[0];
							maxproblabelindex = 0;
							for(l=1; l<labels.size(); l++){
								if(problist[l]>maxprob){
									maxproblabelindex=l;
									maxprob=problist[l];
								}
							}
							
							if(labels.get(maxproblabelindex)!=hardseg.getFloat(i, j, k)){
								hardseg.set(i,j,k,labels.get(maxproblabelindex));
								numchanged++;
							}
							
							//need to readjust truth so that it agrees with the new hard seg
							//do this by setting the truth values equal to their probabilities
							//obtained using all the neighbors - be sure to normalize!
							for(l=0; l<labels.size(); l++){
								truth.get(l).set(i,j,k,problist[l]);
							}
							renormalizeTruth(i,j,k);
							prevconverge = convergesum;
							convergesum+= truth.get(0).getDouble(i, j, k);
							
						}
					}
				}
//				System.out.println("jist.plugins"+"\t"+"Iteration " +n);
//				System.out.println("jist.plugins"+"\t"+"Voxels changed: " +numchanged);
//				System.out.println("jist.plugins"+"\t"+"Converge diff: " + Math.abs(convergesum-prevconverge));
				n++;
			}
		}
	}
	
	/**
	 * Normalizes the truth value at the voxel given by its coordinate indices
	 * @param i	coord index 1
	 * @param j coord index 2
	 * @param k coord index 3
	 */
	private void renormalizeTruth(int i, int j, int k){
		float sum = 0f;
		for(int l=0; l<truth.size(); l++){
			sum+=truth.get(l).getFloat(i, j, k);
		}
		for(int l=0; l<truth.size(); l++){
			truth.get(l).set(i,j,k,truth.get(l).getFloat(i, j, k)/sum);
		}
	}
	
	/**
	 * Compute the weights for each neighbor if 
	 * @param num Number of neighbors
	 * @return array containing the weight for each neighbor
	 */
	private float[] computeUniformNbrWeights(int num){
		float[] weights = new float[num];
//		float w = (1-beta)/num;
		float w = beta;
		for(int i=0; i<num; i++){ weights[i]=w; }
		return weights;
	}
	
	private void deepCloneTruth(){
		if(truthclone==null){
			truthclone = new ArrayList<ImageData>(truth.size());
			for(int i=0; i<truth.size(); i++){
				truthclone.add(truth.get(i).clone());
			}
		}else{
			for(int l=0; l<truthclone.size(); l++){
				for(int k=0; k<truthclone.get(0).getSlices(); k++){
					for(int j=0; j<truthclone.get(0).getCols(); j++){
						for(int i=0; i<truthclone.get(0).getCols(); i++){
							truthclone.get(l).set(i,j,k,truth.get(l).getFloat(i,j,k));
						}
					}
				}
			}
		}
		
	}
	
	private boolean isTruthNormalized(int i, int j, int k){
		float eps = 0.0001f;
		float sum =0f;
		for(int l=0; l<labels.size(); l++){
			sum+=truth.get(l).getFloat(i, j, k);
		}
		return (Math.abs(sum-1f)<eps);
	}
	
	/**
	 * Returns true if the hard segmentation has changed here.
	 * @param i first coord
	 * @param j second coord
	 * @param k third coord
	 * @return true if the hard segmentation has changed
	 */
	private boolean updateHardSeg(int i, int j, int k){
		int current = hardseg.getInt(i,j,k);
		float max = truth.get(0).getFloat(i, j, k);
		int label = labels.get(0);
		for(int l=1; l<truth.size(); l++){
			if(truth.get(l).getFloat(i, j, k)>max){
				max = truth.get(l).getFloat(i,j,k);
				label= labels.get(l);
			}
		}
		if(label==-1){
			System.out.println("jist.plugins"+"\t"+i+","+j+","+k);
			return false;
		}else{
			hardseg.set(i,j,k,label);
		}
		return (current!=label);
	}
	
	public void printPerformanceLevels(){
		System.out.println("jist.plugins"+"\t"+pl);
	}
	public String getPerformanceLevels(){
		return pl.toString();
	}
		
	private static void printArray(float[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}
	private static void printArray(int[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}

	private float[] getPriorArray(){
		float[] a = new float[labels.size()];
		for(int i=0; i<labels.size(); i++){
			a[i]=priors.get(i).floatValue();
		}
		return a;
	}
	
	private float[] getPriorArray(int[] locallabels){
		int i = 0;
		float[] priorshere = new float[locallabels.length];
		while(i<locallabels.length && locallabels[i]>-1){
			priorshere[i]=priors.get(getIndex(labels,locallabels[i])).floatValue();
			i++;
		}
		labelshere=i;
		return priorshere;
	}
	
	public float[][][][] getTruthImage(){
		return truthImage;
	}
	
	public ArrayList<ImageData> getTruth(){
		truth.trimToSize();
		return truth;
	}

	public PerformanceLevel getPeformanceLevel(){
		return pl;
	}
	
	public int getIndex(ArrayList<Integer> l, Integer n){
		Iterator<Integer> it = l.iterator();
		int i =0;
		while(it.hasNext()){
			if(it.next().intValue()==n.intValue()){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public int getIndex(int[] a, int n){
		for(int i=0; i<a.length; i++){
			if(a[i]==n){
				return i;
			}
		}
		return -1;
	}
	public ImageData getHardSeg(){
		if(hardseg==null){
			computeHardSeg();
		}
		return hardseg;
	}
	public void computeHardSeg(){
		if(hardseg==null){
			if(images!=null){
				hardseg = new ImageDataInt(rows, cols, slices);
				hardseg.setName(images.get(0).getName()+"_StapleLabeling");
				//copy header info
				hardseg.getHeader().setAxisOrientation(images.get(0).getHeader().getAxisOrientation());
				hardseg.getHeader().setImageOrientation(images.get(0).getHeader().getImageOrientation());
				hardseg.getHeader().setSliceThickness(images.get(0).getHeader().getSliceThickness());
				hardseg.getHeader().setDimResolutions(images.get(0).getHeader().getDimResolutions());
			}else if(imagesArray!=null){
				hardseg = new ImageDataInt(rows, cols, slices);
				hardseg.setName("StapleLabeling");
			}
		}
		
		if(truth!=null){
			for(int k =0; k<hardseg.getSlices(); k++){
				for(int j =0; j<hardseg.getCols(); j++){
					for(int i =0; i<hardseg.getRows(); i++){

						float max = truth.get(0).getFloat(i, j, k);
						int label = labels.get(0);
						for(int l=1; l<labels.size(); l++){
							if(truth.get(l).getFloat(i, j, k)>max){
								max = truth.get(l).getFloat(i,j,k);
								label= labels.get(l);
							}
						}
						if(!labels.contains(label)){
							System.out.println("jist.plugins"+"\t"+"Problem!");
							System.out.println("jist.plugins"+"\t"+"label: " + label);
//							System.out.println("jist.plugins"+"\t"+"index " + l);
							System.out.println("jist.plugins"+"\t"+"label list: " + labels);
						}
						hardseg.set(i, j, k, label);
						
						if(!labels.contains(hardseg.getInt(i, j, k)) || hardseg.getInt(i, j, k)!=label){
							System.out.println("jist.plugins"+"\t"+"Problem!");
							System.out.println("jist.plugins"+"\t"+"set label: " + label);
							System.out.println("jist.plugins"+"\t"+"label: " + hardseg.getInt(i, j, k));
//							System.out.println("jist.plugins"+"\t"+"index " + l);
							System.out.println("jist.plugins"+"\t"+"label list: " + labels);
						}
						if(hardseg.getInt(i, j, k)==-1){
							System.out.println("jist.plugins"+"\t"+"Negative one");
						}
					}
				}
			}
		}else if(truthImage!=null){
			for(int k =0; k<hardseg.getSlices(); k++){
				for(int j =0; j<hardseg.getCols(); j++){
					for(int i =0; i<hardseg.getRows(); i++){
						float max = truth.get(0).getFloat(i, j, k);
						int label = labels.get(0);
						for(int l=1; l<truthImage[0][0][0].length; l++){
							if(truthImage[i][j][k][l]>max){
								max = truthImage[i][j][k][l];
								label= labels.get(l);
							}
						}
						
						hardseg.set(i, j, k, label);
					}
				}
			}
		}
	}
	
	public int[][][] getHardSegArray(){

		int[][][] segout = null;
		if(images!=null){
			segout = new int[images.get(0).getRows()][images.get(0).getCols()][images.get(0).getSlices()];
		}else if(imagesArray!=null){
			segout = new int[imagesArray.length][imagesArray[0].length][imagesArray[0][0].length];
		}
		if(truth!=null){
			for(int k =0; k<segout[0][0].length; k++){
				for(int j =0; j<segout[0].length; j++){
					for(int i =0; i<segout.length; i++){

						double max = 0;
						int label = -1;
						for(int l=0; l<truth.size(); l++){
							if(truth.get(l).getFloat(i, j, k)>max){
								max = truth.get(l).getFloat(i,j,k);
								label= labels.get(l);
							}
						}
						segout[i][j][k]=label;
					}
				}
			}
		}else if(truthImage!=null){
			for(int k =0; k<segout[0][0].length; k++){
				for(int j =0; j<segout[0].length; j++){
					for(int i =0; i<segout.length; i++){
						double max = 0;
						int label = -1;
						for(int l=0; l<truthImage[0][0][0].length; l++){
							if(truthImage[i][j][k][l]>max){
								max = truthImage[i][j][k][l];
								label= labels.get(l);
							}
						}
						segout[i][j][k]=label;
					}
				}
			}
		}
		return segout;
	}
	
	public void writeTruth(File dir){
		
		for(int i=0; i<truth.size(); i++){
			rw.write(truth.get(i), dir);
		}
		
	}
	
	public void writeTruth(File dir, String name){
		
		for(int i=0; i<truth.size(); i++){
			rw.write(truth.get(i), new File(dir.getAbsolutePath()+File.separator+name+"_"+i+".xml"));
		}
		
	}

	public void writeUniqueLabels(File dir){
		
		if(uniqueLabels!=null){
			ImageDataInt unique = new ImageDataInt(uniqueLabels);
			ImageHeader hdr = truth.get(0).getHeader().clone();
			unique.setHeader(hdr);
			unique.setName("UniqueValues");
			rw.write(unique, dir);
		}
	}
	
	public void writeHardSeg(File dir, String name){
		
			rw.write(getHardSeg(), new File(dir.getAbsolutePath()+"/"+name+"_"+".xml"));
	}
	
	private class StapleMRFUtil{
		private int rows, cols, slices;
		private boolean evenslice;
		private boolean evencol;
		
		public int thisrow = -1;
		public int thiscol = -1;
		public int thisslice =-1;
		
		public StapleMRFUtil(int rows, int cols, int slices, boolean evenslice){
			this.rows=rows;
			this.cols=cols;
			this.slices=slices;	
			setEvenSlice(evenslice);
		}
		
		public void setEvenSlice(boolean evenslice){
			this.evenslice=evenslice;
		}
		
		public void reset(){
			thisrow = -1;
			thiscol = -1;
			thisslice =-1;
		}
		
		public void updateCoords(){

			if(thisslice<0){
				if(evenslice){
					thisrow=-1;
					thiscol=0;
					thisslice=0;
					evencol = true;
				}else{
					thisrow=-1;
					thiscol=0;
					thisslice=0;
					evencol = false;
				}
				updateCoords2d();
			}else{
				if(hasNext2d()){
//					System.out.println("jist.plugins"+"\t"+"Working on slice: " + thisslice);
					updateCoords2d();
				}else{
					thisslice++;
					thisrow=-1;
					evenslice=!evenslice;
					updateCoords2d();
				}
			}
		}
		
		
		public void updateCoords2d(){
			if(thisrow<0){
				if(evenslice){
					thisrow=0;
					thiscol=0;
					evencol = true;
				}else{
					thisrow=0;
					thiscol=1;
					evencol = false;
				}
			}else{

				if(thiscol+1==cols){
					thisrow++;
					if(evencol){
						thiscol=1;
					}else{
						thiscol=0;
					}
					evencol = !evencol;
				}else if(thiscol+2==cols){
					thisrow++;
					if(evencol){
						thiscol=1;
					}else{
						thiscol=0;
					}
					evencol = !evencol;
				}else{
					thiscol+=2;
				}
			}
		}
		
		public boolean hasNext(){
			if(thisrow+1>=rows && thiscol+2>=cols && thisslice+1==slices){
				return false;
			}
			return true;
		}
		public boolean hasNext2d(){
			if(thisrow+1>=rows && thiscol+2>=cols){
				return false;
			}
			return true;
		}

	}

}
