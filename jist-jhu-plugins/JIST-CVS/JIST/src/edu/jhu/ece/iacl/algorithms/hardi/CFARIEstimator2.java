package edu.jhu.ece.iacl.algorithms.hardi;

import imaging.Scheme;

import java.util.ArrayList;
import java.util.Arrays;

import edu.jhu.bme.smile.commons.math.L1LSCompressedSensing;
import edu.jhu.bme.smile.commons.math.L1LSCompressedSensingNonNeg;
import edu.jhu.ece.iacl.plugins.hardi.CFARIEstimation2.CFARIWrapper;

/**
 * Used to process each voxel and returns the results.
 * This algorithm is still in development.
 * @author Hanlin Wan <hanlinwan@gmail.com>
 *
 */
public class CFARIEstimator2 {
	
	public static void estimateCFARI(float [][][][]DWdata, byte [][][]mask, CFARIBasisSet basisSet, 
			double tol, int [][][][]result, float [][][][]mixture, double lambda, int []location, 
			int[] slices, CFARIWrapper cw) {
		
		if(mask==null) {
			mask = new byte[DWdata.length][DWdata[0].length][DWdata[0][0].length];
			for(int i=0;i<DWdata.length;i++) {
				for(int j=0;j<DWdata[0].length;j++) {
					for(int k=0;k<DWdata[0][0].length;k++)
						mask[i][j][k]=1;
				}
			}
		} 
		if(mask.length!=DWdata.length)
			throw new RuntimeException("estimateCFARI: Mask does not match data in dimension: 0.");
		if(mask[0].length!=DWdata[0].length)
			throw new RuntimeException("estimateCFARI: Mask does not match data in dimension: 1.");
		if(mask[0][0].length!=DWdata[0][0].length)
			throw new RuntimeException("estimateCFARI: Mask does not match data in dimension: 2.");
		L1LSCompressedSensing l1ls=new L1LSCompressedSensingNonNeg(basisSet.getSensingMatrix());
		Scheme acqScheme=basisSet.getDWScheme();
		
		double data[] = new double[DWdata[0][0][0].length];
		for (int zDim=slices[0]; zDim<slices[1]; zDim++) {
			for (int xDim=0; xDim<DWdata.length; xDim++) {
				for (int yDim=0; yDim<DWdata[0].length; yDim++) {
					if (mask[xDim][yDim][zDim]!=0) {
						for(int l=0;l<data.length;l++)
							data[l]=DWdata[xDim][yDim][zDim][l];
						double mean = acqScheme.geoMeanZeroMeas(data);
						if(mean>0)  {
							estimateCFARIModel(l1ls, acqScheme.normalizeData(data),basisSet,tol,
									result[xDim][yDim][zDim],mixture[xDim][yDim][zDim],lambda,location,cw);
						}
						else {
							Arrays.fill(result[xDim][yDim][zDim], -1);
							Arrays.fill(mixture[xDim][yDim][zDim], Float.NaN);
						}
					} 
					else {
						Arrays.fill(result[xDim][yDim][zDim], -1);
						Arrays.fill(mixture[xDim][yDim][zDim], Float.NaN);
					}
				}				
			}
		}
		return;
	}

	public static void estimateCFARIListRelativeLambda(float [][][][]DWdata, byte [][][]mask, CFARIBasisSet basisSet, 
			double tol, int [][][][]result, float [][][][]mixture, double lambdaRel, int []location, 
			ArrayList<int[]> voxels, CFARIWrapper cw) {

		L1LSCompressedSensing l1ls = new L1LSCompressedSensingNonNeg(basisSet.getSensingMatrix());
		double d[] = new double[DWdata[0][0][0].length];
		for (int c=0; c<voxels.size(); c++) {
			int xDim=voxels.get(c)[0];
			int yDim=voxels.get(c)[1];
			int zDim=voxels.get(c)[2];
			for(int l=0;l<d.length;l++)
				d[l]=DWdata[xDim][yDim][zDim][l];
			Scheme DTIscheme = basisSet.getDWScheme();
			double mean = DTIscheme.geoMeanZeroMeas(d);
			if(mean>0) {				
				CFARIEstimator2.estimateCFARIModelRelativeLambda(l1ls, DTIscheme.normalizeData(d),basisSet,
						1e-3, result[xDim][yDim][zDim],mixture[xDim][yDim][zDim],lambdaRel,location,cw);
			} else {
				Arrays.fill(result[xDim][yDim][zDim], -1);
				Arrays.fill(mixture[xDim][yDim][zDim], Float.NaN);
			}
		}
	}
	
	public static void estimateCFARIList(float [][][][]DWdata, byte [][][]mask, CFARIBasisSet basisSet, 
			double tol, int [][][][]result, float [][][][]mixture, double lambda, int []location, 
			ArrayList<int[]> voxels, CFARIWrapper cw) {

		L1LSCompressedSensing l1ls = new L1LSCompressedSensingNonNeg(basisSet.getSensingMatrix());
		double d[] = new double[DWdata[0][0][0].length];
		for (int c=0; c<voxels.size(); c++) {
			int xDim=voxels.get(c)[0];
			int yDim=voxels.get(c)[1];
			int zDim=voxels.get(c)[2];
			for(int l=0;l<d.length;l++)
				d[l]=DWdata[xDim][yDim][zDim][l];
			Scheme DTIscheme = basisSet.getDWScheme();
			double mean = DTIscheme.geoMeanZeroMeas(d);
			if(mean>0)
				CFARIEstimator2.estimateCFARIModel(l1ls, DTIscheme.normalizeData(d),basisSet,
						1e-3, result[xDim][yDim][zDim],mixture[xDim][yDim][zDim],lambda,location,cw);
			else {
				Arrays.fill(result[xDim][yDim][zDim], -1);
				Arrays.fill(mixture[xDim][yDim][zDim], Float.NaN);
			}
		}
	}
	
	public static void estimateCFARIModel(L1LSCompressedSensing l1ls, double[] data, CFARIBasisSet basisSet, 
			double tol, int[] resultIndex, float[] resultMixture, double lambda, int[] location, CFARIWrapper cw) {

		l1ls.solve(data, lambda, tol, 10.0, true);
		double []mixtures = l1ls.getResult();
		int nOut = resultIndex.length;
		Arrays.fill(resultMixture, Float.NEGATIVE_INFINITY);
		Arrays.fill(resultIndex, -1);
		for(int i=0;i<mixtures.length;i++)
			insertElement((float)(mixtures[i]),i,resultMixture,resultIndex,nOut,location);
		if (cw!=null)
			cw.incrementCompletedUnits();
	}
	
	public static void estimateCFARIModelRelativeLambda(L1LSCompressedSensing l1ls, double[] data, CFARIBasisSet basisSet, 
			double tol, int[] resultIndex, float[] resultMixture, double lambdaRel, int[] location, CFARIWrapper cw) {

		l1ls.solve(data, lambdaRel*l1ls.findLambdaMax(data), tol, 10.0, true);
		double []mixtures = l1ls.getResult();
		int nOut = resultIndex.length;
		Arrays.fill(resultMixture, Float.NEGATIVE_INFINITY);
		Arrays.fill(resultIndex, -1);
		for(int i=0;i<mixtures.length;i++)
			insertElement((float)(mixtures[i]),i,resultMixture,resultIndex,nOut,location);
		if (cw!=null)
			cw.incrementCompletedUnits();
	}


	private static void insertElement(float f, int x, float[] resultMixture, int[] resultIndex, 
			int nOut, int[] location) {

		int i;
		int loc = nOut;
		if (location==null || location[x]!=-1) {
			while((loc>0) && (f>resultMixture[loc-1]))					
				loc--;
			if(loc<nOut) {
				for(i=nOut-1;i>loc;i--) {
					resultMixture[i]=resultMixture[i-1];
					resultIndex[i]=resultIndex[i-1];
				}
				resultMixture[loc]=f;
				if (location==null)
					resultIndex[loc]=x;
				else
					resultIndex[loc]=location[x];
			}
		}
	}
}
