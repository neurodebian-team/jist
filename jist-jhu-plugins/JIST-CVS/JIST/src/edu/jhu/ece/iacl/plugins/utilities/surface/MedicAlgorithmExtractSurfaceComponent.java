/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.surface;

import edu.jhu.ece.iacl.jist.io.ArrayDoubleReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmExtractSurfaceComponent extends ProcessingAlgorithm{
	ParamInteger stIndex;
	ParamInteger endIndex;
	ParamSurface inSurf;
	ParamSurface outSurf;
	ParamObject<double[][]> dataArray;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Extract embedded surface data from VTK mesh.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inSurf=new ParamSurface("Mult-Component Surface"));
		inputParams.add(stIndex=new ParamInteger("Start Index",0,1000000,0));
		inputParams.add(endIndex=new ParamInteger("End Index",0,1000000,0));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Extract Surface Component");
		inputParams.setName("Extract_Surface_Component");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Extracted Surface"));
		outputParams.add(dataArray=new ParamObject<double[][]>("Data",new ArrayDoubleReaderWriter()));
	}


	protected void execute(CalculationMonitor monitor) {
		EmbeddedSurface surf=inSurf.getSurface();
		int st=stIndex.getInt();
		int end=endIndex.getInt();

		int vertCount=surf.getVertexCount();
		double[][] data=new double[vertCount][end-st+1];
		for(int id=0;id<vertCount;id++){
			for(int l=st;l<=end;l++){
				data[id][l-st]=surf.getVertexDataAtOffset(id,l);
			}
		}
		EmbeddedSurface newSurf=new EmbeddedSurface(surf.getVertexCopy(),surf.getIndexCopy(),data);
		newSurf.setName(surf.getName()+"_"+st+"-"+end);

		outSurf.setValue(newSurf);
		dataArray.setObject(data);
		dataArray.setFileName(surf.getName()+"_"+st+"-"+end);
	}
}
