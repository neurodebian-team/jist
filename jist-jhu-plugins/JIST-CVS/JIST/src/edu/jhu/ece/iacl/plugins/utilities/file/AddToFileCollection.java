package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;


public class AddToFileCollection extends ProcessingAlgorithm {
	ParamFileCollection a;
	ParamFileCollection b;
	ParamFileCollection out;
	ParamInteger startindex;
	
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "") .replace(" ", "");
	private static final String shortDescription = "Add the given file collection to an existing file collection";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		FileExtensionFilter fef = new FileExtensionFilter();
		inputParams.add(a = new ParamFileCollection("File Collection", fef));
		inputParams.add(b = new ParamFileCollection("File Collection to add",fef));
		inputParams.add(startindex = new ParamInteger("Index to insert"));
		startindex.setValue(-1);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Add to File Collection");
		inputParams.setName("Add_to_File_Collection");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		FileExtensionFilter fef = new FileExtensionFilter();
		outputParams.add(out = new ParamFileCollection("Output File Collection",fef));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		List<File> alist = a.getValue();
		List<File> blist = b.getValue();
		int N = alist.size()+blist.size();
		int ind = startindex.getInt();
		if(ind<0){
			ind = alist.size();
		}
		ArrayList<File> filesout = new ArrayList<File>(N);

		int j = 0;
		int k = 0;
		for(int i=0;i<N;i++){
			if(i<ind){
				filesout.add(alist.get(j));
				j++;
			}else if(i>=(ind+blist.size())){
				filesout.add(alist.get(j));
				j++;
			}else{
				filesout.add(blist.get(k));
				k++;
			}
		}
		out.setValue(filesout);
	}
}
