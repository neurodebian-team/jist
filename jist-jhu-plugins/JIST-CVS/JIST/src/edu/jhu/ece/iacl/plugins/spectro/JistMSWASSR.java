package edu.jhu.ece.iacl.plugins.spectro;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.ece.iacl.algorithms.spectro.MSWASSR;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class JistMSWASSR extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume wassrVolume;	// 4-D Volume containing MTR data with a range of offsets	
	private ParamFile shiftlist; 

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume shiftMapVolume;	// Estimate F0 shift map

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "MSWASSR";
	private static final String longDescription = ": The Maximum-Symmetry Water Saturation Shift Referencing Algorithm" +
			"\nInput1: 4D Volume (x,y,z,t): " +
			"[rows,columns,slices,offset frequences] " +
			"\n\t can consist of the signal intensity images or the normalized S/S_0 images." +
			"\nInput2: List of Offset Frequencies [Hz] (.txt)" +
			"\n\t Note: if the Input1 4D Volume contains reference (S_0) images, then" +
			"\n\t label the 'offset frequency' associated with the S_0 images as 'NaN'." +
			"\nOutput1: B0 Map [Hz]; " +
			"\n\t (x,y,z) image showing shift in center frequency per voxel." +
			"\nOutput2: Execution Time [sec]; " +
			"\n\t the time to run the algorithm.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("Kirby");
		inputParams.setCategory("Spectro");
		inputParams.setLabel("MSWASSR");
		inputParams.setName("MSWASSR");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://mri.kennedykrieger.org");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.add(new AlgorithmAuthor("Issel Anne Lim","issel@jhu.edu","http://alum.mit.edu/www/issel"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription + longDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.add(new Citation("M.Kim, J.Gillen, B.Landman, J.Zhou, P.C.M.van Zijl. 'Water saturation shift referencing (WASSR) for chemical exchange saturation transfer (CEST) experiments.' Magnetic Resonance in Medicine. Volume 61, Issue 6: 1441-1450, 2009."));
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
		info.add(new Citation("Mina Kim et al., �Water saturation shift referencing (WASSR) for chemical exchange saturation transfer (CEST) experiments,� Magnetic Resonance in Medicine 61, no. 6 (2009): 1441-1450."));


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(wassrVolume=new ParamVolume("WASSR Data (4D)",null,-1,-1,-1,-1));
		inputParams.add(shiftlist=new ParamFile("List of Shifts (Hz)",new FileExtensionFilter(new String[]{"txt"})));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		shiftMapVolume = new ParamVolume("Estimated Frequency Shift (Hz)",VoxelType.FLOAT,-1,-1,-1,1);
		shiftMapVolume.setName("FreqShiftMap");
		outputParams.add(shiftMapVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			this.setLabel("Estimating MS-WASSR");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t START");
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageDataFloat scalarVol=new ImageDataFloat(wassrVolume.getImageData());			
			int r=scalarVol.getRows(), c=scalarVol.getCols(), s=scalarVol.getSlices(), t = scalarVol.getComponents();
			this.setTotalUnits(r);

			Vector<Boolean> ignore = new Vector<Boolean>();
			Vector<Double> shift = new Vector<Double>();
			int ignoreCnt=0;
			try{
				BufferedReader rdr = new BufferedReader(new FileReader(shiftlist.getValue()));
				String thisline = " ";
				thisline = rdr.readLine();
				while(thisline!=null && !thisline.isEmpty()){
					Double val = Double.valueOf(thisline);
					if(val==null)
						val = Double.NaN;
					if(val.isNaN() || val.isInfinite()) {
						val=Double.NaN;
						ignore.add(true);
						ignoreCnt++;
					} else
						ignore.add(false);
					shift.add(val);
					System.out.println("Shift: "+val);
					thisline = rdr.readLine();
				}
			}catch(IOException e){
				JistLogger.logError(JistLogger.SEVERE, "Cannot parse input shift file");				
			}
			
			boolean []ignoreArray= new boolean[ignore.size()];
			double[]shiftArray= new double[ignore.size()-ignoreCnt];
			int idx =0;
			for(int i=0;i<ignoreArray.length;i++) {
				ignoreArray[i] = ignore.get(i);
				if(!ignoreArray[i]) {
					shiftArray[idx]=shift.get(i);
					idx++;
				}
			}
			double []y= new double[shiftArray.length];
			
			// Check if t and shiftArray.length are the same
			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t Number of MTW volumes: "+t+"("+ignoreCnt+")"+"\t Number of offset frequencies found in file: "+shiftArray.length );
			if (shiftArray.length != (t-ignoreCnt)){
				JistLogger.logError(JistLogger.SEVERE, "The number of MTW volumes and shift values do not match, aborting.");
				return;
			}
			
			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/			

			ImageData WASSRestimate = new ImageDataFloat(r,c,s,1);
			WASSRestimate.setName(scalarVol.getName()+"_mswassr");
			WASSRestimate.setHeader(scalarVol.getHeader());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/	
			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t Running Core");
			MSWASSR core = new MSWASSR();
			for(int i=0;i<r;i++) {
				this.setCompletedUnits(i);
				for(int j=0;j<c;j++)
					for(int k=0;k<s;k++) {
						if((k==15)&&(i==28)&&(j==63)){
							int dummy = 1;
						}
						idx=0;
						for(int m=0;m<t;m++) {
							if(!ignoreArray[m]) {
								y[idx]=scalarVol.getDouble(i, j,k,m);
								idx++;
							}
						}
//						System.out.println(idx);
						
						WASSRestimate.set(i,j,k,(float)(
								core.centerFreq(shiftArray, y)
								));
					}
			}


			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t Setting up exports.");
			shiftMapVolume.setValue(WASSRestimate);

			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t  FINISHED");
		}
	}
}
