package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 15, 2005
 * Time: 4:56:47 PM
 * To change this template use Options | File Templates.
 */
// Goal:
// Implement FACT in JAVA (faster and able to memory manage)
// Provide fiber lookup table in Java (hash indecies by point locations)

import java.io.*;
import java.util.*;

import javax.vecmath.*;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurvePath;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.LEFileWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;

public class FACTapi {
     FACTparameters dataConfig;
    Hashtable voxelwiseHash;
     Vector allFibers;
    int minLen, maxlen, cnt, len;
    float mnLen;

    public FACTapi() {
        dataConfig = new FACTparameters();
    }

    public void hello() {
        System.out.println("jist.plugins"+"\t"+"hi there 3");
    }
    public void mySize(Double d[]) {
        System.out.println("jist.plugins"+"\t"+d.length);
    }
    public FACTparameters getParameters(){
    	return dataConfig;
    }
    public Vector getFibers(){
    	return allFibers;
    }
    public boolean setParameters(float setStopFA, float setMaxTurnAng, float setStartFA) {
        dataConfig.stopFA=setStopFA;
        dataConfig.maxTurnAngle=(float)(setMaxTurnAng*Math.PI/180f);
        dataConfig.startFA=setStartFA;
        return true;
    }
    public boolean readParamFile(String file){
    	dataConfig.readParameters(file);
    	return true;
    }
    public int readLEInt(InputStream in) throws IOException {

       int byte1, byte2, byte3, byte4;

       synchronized (this) {
         byte1 = in.read();
         byte2 = in.read();
         byte3 = in.read();
         byte4 = in.read();
       }
       if (byte4 == -1) {
         throw new EOFException();
       }
       return (byte4 << 24)
        + ((byte3 << 24) >>> 8)
        + ((byte2 << 24) >>> 16)
        + ((byte1 << 24) >>> 24);
     }


    public boolean loadData(String faFile, String vecFile, int setNx, int setNy, int setNz,double setResX,
                            double setResY,double setResZ){
        dataConfig.Nx=setNx;
        dataConfig.Ny=setNy;
        dataConfig.Nz=setNz;
        dataConfig.resX=setResX;
        dataConfig.resY=setResY;
        dataConfig.resZ=setResZ;

        try {
            System.out.println("jist.plugins"+"\t"+"Allocating Memory...");
            dataConfig.vecData = new float[dataConfig.Nz][dataConfig.Ny][dataConfig.Nx][3];
            dataConfig.faData = new float[dataConfig.Nz][dataConfig.Ny][dataConfig.Nx];
            System.out.println("jist.plugins"+"\t"+"Loading data (little endian)...");
            System.out.println("jist.plugins"+"\t"+"FA: "+faFile);
            System.out.println("jist.plugins"+"\t"+"VEC: "+vecFile);

            InputStream fp = new BufferedInputStream(new FileInputStream(new File(faFile)), dataConfig.Nx*dataConfig.Ny);;
            for(int i=0;i<dataConfig.Nz;i++) {
                for(int j=0;j<dataConfig.Ny;j++) {
                    for(int k=0;k<dataConfig.Nx;k++)  {
                        dataConfig.faData[i][j][k]=Float.intBitsToFloat(readLEInt(fp));
//                        //System.out.println("jist.plugins"+"\t"+"Loaded next point of FA: " + k + " " + j + " " + i + " is: " + dataConfig.faData[i][j][k]);
//                        if (dataConfig.faData[i][j][k]!=0){
//                        	System.out.println("jist.plugins"+"\t"+"Non-zero FA at pt: " + k + " " + j + " " + i +" is: " + dataConfig.faData[i][j][k]);
//                        }
                    }
                }
            }
            fp.close();

            fp = new BufferedInputStream(new FileInputStream(new File(vecFile)), dataConfig.Nx*dataConfig.Ny);;

            for(int i=0;i<dataConfig.Nz;i++) {
                for(int j=0;j<dataConfig.Ny;j++) {
                    for(int k=0;k<dataConfig.Nx;k++)  {
                        for(int l=0;l<3;l++) {
                            dataConfig.vecData[i][j][k][l]=Float.intBitsToFloat(readLEInt(fp));
//                            if (dataConfig.vecData[i][j][k][l]!=0){
//                            	System.out.println("jist.plugins"+"\t"+"Non-zero VEC at pt: " + k + " " + j + " " + i + " " +l +" is: " + "("+dataConfig.vecData[i][j][k][l]);
//                            }
                        }
                    }
                }
            }
            fp.close();

            return true;
        } catch(Exception e){
            System.out.println("jist.plugins"+"\t"+"Exception in loadData: "+e);
            return false;
        }
    }
    public int trackAllFibers() {
        int numFibers =0;
        System.out.println("jist.plugins"+"\t"+"Max turn angle is " +dataConfig.maxTurnAngle);
        System.out.println("jist.plugins"+"\t"+"stopFA is " +dataConfig.stopFA);
         for(int i=0;i<dataConfig.Nz;i++) {
                for(int j=0;j<dataConfig.Ny;j++) {
                    for(int k=0;k<dataConfig.Nx;k++)  {
                        if(dataConfig.faData[i][j][k]>=dataConfig.startFA) {
                            numFibers++;
                        }
                    }
                }
            }
        System.out.println("jist.plugins"+"\t"+"Tracking "+numFibers+" fibers...");
        
        //voxelwiseHash = new Hashtable(numFibers*80,0.01f);
        allFibers = new Vector(numFibers);
        numFibers=0;
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;
         for(short i=0;i<dataConfig.Nz;i++) {
                for(short j=0;j<dataConfig.Ny;j++) {
                    for(short k=0;k<dataConfig.Nx;k++)  {
                        if(dataConfig.faData[i][j][k]>=dataConfig.startFA) {
//                        	System.out.println("jist.plugins"+"\t"+"Tracking another fiber from "+k+" " +j+" "+i);
                        	FACTfiber newFiber = new FACTfiber();
                        	//track fiber here
                        	long mem1 = Runtime.getRuntime().freeMemory();
                        	
                        	int myLen=newFiber.track(dataConfig,(float)k+0.5f,(float)j+0.5f,(float)i+0.5f);
//                        	int myLen=newFiber.track(dataConfig,(float)k-0.5f,(float)j-0.5f,(float)i-0.5f);
                        	
                        	long mem2 = Runtime.getRuntime().freeMemory();
                        	
//                        	IGNORE FIBERS OF LENGTH < 7
                        	//System.out.println("jist.plugins"+"\t"+"Length of this fiber is: " + myLen);
                        	if (myLen>dataConfig.minLength){
                        		leng7++;

                        		diff += (mem2-mem1);

                        		if(myLen<minLen) {
                        			minLen=myLen;
                        		} else{
                        			len2+=myLen;
                        			cnt++;
                        		}
                        		if (myLen>maxlen) { maxlen=myLen; }
                        		len+=myLen;
                        		
                        		allFibers.add(numFibers,newFiber);
//                        		System.out.println("jist.plugins"+"\t"+"Added fiber from: " + k + " " + j + " " + i);
                        		numFibers++;
                        	}
                        	
                            //System.out.println("jist.plugins"+"\t"+"Allfibers is length " + allFibers.size());

                            //Add Points fiber to hash table to lookup by
                            //newFiber.addVxToLookupTable(voxelwiseHash,numFibers,dataConfig.Nx,dataConfig.Ny);
                            
                        }
                        //System.out.println("jist.plugins"+"\t"+"####### Moving to next voxel #######");
                    }
                }
            }
        mnLen = (float)len/(float)numFibers;
        
        System.out.println("jist.plugins"+"\t"+"Finished, and tracked: " + numFibers + " fibers");
        System.out.println("jist.plugins"+"\t"+"Mean length: "+mnLen);
        System.out.println("jist.plugins"+"\t"+"Min length: "+minLen);
        System.out.println("jist.plugins"+"\t"+"Max length: "+maxlen);
        		
        return numFibers;
    }
    
    public int trackFromSeed(int k, int j, int i) {

    	//voxelwiseHash = new Hashtable(numFibers*80,0.01f);
    	int numFibers=0;
    	allFibers = new Vector(1);
    	int len2=0;
    	long diff=0;
    	int leng7=0;
    	int minLen = 10000;
    	int maxlen =0;

    	if(dataConfig.faData[i][j][k]>=dataConfig.startFA) {
    		//System.out.println("jist.plugins"+"\t"+"Tracking another fiber from "+k+" " +j+" "+i);
    		FACTfiber newFiber = new FACTfiber();
    		//track fiber here
    		long mem1 = Runtime.getRuntime().freeMemory();

    		int myLen=newFiber.track(dataConfig,(float)k+0.5f,(float)j+0.5f,(float)i+0.5f);

    		long mem2 = Runtime.getRuntime().freeMemory();

//  		IGNORE FIBERS OF LENGTH < 7
//			System.out.println("jist.plugins"+"\t"+"Length of this fiber is: " + myLen);
    		if (myLen>2){
    			leng7++;

    			diff += (mem2-mem1);

    			if(myLen<minLen) {
    				minLen=myLen;
    			} else{
    				len2+=myLen;
    				cnt++;
    			}
    			if (myLen>maxlen) { maxlen=myLen; }
    			len+=myLen;

    			allFibers.add(numFibers,newFiber);
    			numFibers++;
    		}

    		//System.out.println("jist.plugins"+"\t"+"Allfibers is length " + allFibers.size());

    		//Add Points fiber to hash table to lookup by
    		//newFiber.addVxToLookupTable(voxelwiseHash,numFibers,dataConfig.Nx,dataConfig.Ny);

    	}
    	//System.out.println("jist.plugins"+"\t"+"####### Moving to next voxel #######");

    	mnLen = (float)len/(float)numFibers;

//    	System.out.println("jist.plugins"+"\t"+"Finished, and tracked: " + numFibers + " fibers");
//    	System.out.println("jist.plugins"+"\t"+"Mean length: "+mnLen);
//    	System.out.println("jist.plugins"+"\t"+"Min length: "+minLen);
//    	System.out.println("jist.plugins"+"\t"+"Max length: "+maxlen);

    	return numFibers;
    }

    public List findFibersByPoints(cPT pts[]) {
        LinkedList lst = new LinkedList();
        for(int i=0;i<allFibers.size();i++) {
            if(((FACTfiber)allFibers.get(i)).containsPoints(pts))
            	//System.out.println("jist.plugins"+"\t"+"Fiber passing through point...");
                lst.add(allFibers.get(i));
        }
        return lst;
    }
    
    public FiberCollection toFiberCollection(){
    	FiberCollection fc = new FiberCollection();
    	fc.getFromFiberArray(toFiberArray());
    	return fc;
    }
    
    public Fiber[] toFiberArray(){
    	allFibers.trimToSize();
    	Iterator it = allFibers.iterator();
    	Fiber[] out = new Fiber[allFibers.size()];
    	int i=0;
    	while(it.hasNext()){
    		FACTfiber f = (FACTfiber)it.next();
    		out[i]=f.toFiber();
    		i++;
    	}
    	return out;
    }

    
    //write Fibers according to DTISTudio format
    public void writeFibers(String filename){
    	try {
    		LEFileWriter bos = new LEFileWriter(filename);
    		
    		//WRITE HEADER
    		//DTIStudioHDR hdr = new DTIStudioHDR(); 
        	bos.writeBytes("FiberDat"); 
        	
        	bos.writeInt(allFibers.size());	//Number of Fibers
        	
        	bos.writeInt(maxlen);		//FLM
        	bos.writeFloat(mnLen);	//FLMn

        	//bos.writeFloat(0);
        	
        	bos.writeInt(dataConfig.Nx);
        	bos.writeInt(dataConfig.Ny);
        	bos.writeInt(dataConfig.Nz);
        	
        	bos.writeFloat((float)dataConfig.resX);
        	bos.writeFloat((float)dataConfig.resY);
        	bos.writeFloat((float)dataConfig.resZ);
        	
        	bos.writeInt(1);
        	bos.writeInt(0);
        	
        	bos.seek(128);
        	
        	//FIBER TEST
//    		XYZ[] xyz = new XYZ[3];
//    		xyz[0] = new XYZ(110f,110f,5f);
//    		xyz[1] = new XYZ(110f,111,5f);
//    		xyz[2] = new XYZ(110f,111.5f,5.1f);
        	
        	//WRITE FIBERS
        	USbyte usb = new USbyte();
    		for(int i=0;i<allFibers.size();i++){
    			
    			FACTfiber f = (FACTfiber)allFibers.get(i);		//GET THE iTH FIBER
    			Point3f fbrs[] = f.points;
    			
        		bos.writeInt(fbrs.length);	//fiber length
        		//System.out.println("jist.plugins"+"\t"+"Lenth of " +i+"th fiber is: " +fbrs.length);
//        		if (i==1){System.out.println("jist.plugins"+"\t"+"Fiber length: " +fbrs.length);}
        		//System.out.println("jist.plugins"+"\t"+"Fiber length: " +fbrs.length);
        		bos.writeByte(0);			//cReserved
        		
        		//HOW TO COME UP WITH COLORS?
        		bos.writeByte(usb.unsigned2signed(255));			//Color - Red
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Green
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Blue
        		
        		bos.writeInt(0);			//nSelectFiberStartPoint
        		bos.writeInt(fbrs.length);	//nSelec
        		
        		//WRITE EVERY POINT IN THE FIBER 
        		for (int j=0;j<fbrs.length;j++){
        			Point3f pt = (Point3f)fbrs[j];
        			bos.writeFloat(pt.x);
        			bos.writeFloat(pt.y);
        			bos.writeFloat(pt.z);
//        			if (i>=0 & i<=5){
//        				System.out.println("jist.plugins"+"\t"+"x written: " + pt.x);
//        				System.out.println("jist.plugins"+"\t"+"y written: " + pt.y);
//        				System.out.println("jist.plugins"+"\t"+"z written: " + pt.z);
//        			}
//        			if (i>=0 & i<=5){ 
//        				System.out.println("jist.plugins"+"\t"+"Finished writing fiber " + j);
//        				System.out.println("jist.plugins"+"\t"+"");
//        			}
        			
        			//System.out.println("jist.plugins"+"\t"+"Finished writing fiber point " + j);
        		}
        	}
        	bos.close();
        	
    	}catch (IOException e){
    		System.out.println("jist.plugins"+"\t"+"Error Writing File");
    		e.printStackTrace();
    	}
    	
    }
    
    //write Fibers according to VTK format
    public void writeFibersVTK(String filename){
    	MipavController.setQuiet(true);
    	CurveVtkReaderWriter.getInstance().write(toFiberCollection().toCurveCollection(), new File(filename));
    
    }
    
    //int i is the max
    private int roundinbound(float f, int max){
    	int ff = Math.round(f);
    	if(ff<0){ ff=0; }
    	else if(ff>max){ ff=max; }
    	return ff;
    }
    
}
