package edu.jhu.ece.iacl.algorithms.vabra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

public class VabraVolumeCollection implements Cloneable {

	public ImageData data[];
	private int XN, YN, ZN, numOfCh;
	private int[] chInterpType;
	

	public VabraVolumeCollection(List<ImageData> vols, int[] interpType) {
		this.numOfCh = vols.size();
		data = new ImageDataFloat[numOfCh];
		for (int i = 0; i < vols.size(); i++) {
			data[i] = new ImageDataFloat(vols.get(i));
		}
		this.XN = data[0].getRows();
		this.YN = data[0].getCols();
		this.ZN = data[0].getSlices();
		this.chInterpType = interpType;
		
	}

	// this constructor only allocates memory -- no read from disk
	public VabraVolumeCollection(int XN, int YN, int ZN, int channels, int[] interpType) {
		this.XN = XN;
		this.YN = YN;
		this.ZN = ZN;
		this.numOfCh = channels;
		this.chInterpType = interpType;
		data = new ImageDataFloat[channels];
	}



	public void downSample(int newSizeX, int newSizeY, int newSizeZ, double sigma) {

		int i, j, k;
		int ax, ay, az, x, y, z;
		int cx, cy, cz;
		double tempVal;
		int filterSize;
		double gaussKernel[][][];
		double gaussKernel1D[];

		int oldSizeX = XN, oldSizeY = YN, oldSizeZ = ZN;

		// current image data will be replace by new data
		ImageData[] newData = new ImageData[numOfCh];

		if (data.length > 0 && newSizeX == data[0].getRows()
				&& newSizeY == data[0].getCols()
				&& newSizeZ == data[0].getSlices()) {
			return;
		}
		for (int n = 0; n < numOfCh; n++) {
			newData[n] = new ImageDataFloat(newSizeX, newSizeY, newSizeZ);
		}
		ImageData oldData[] = data;

		// the following Gaussian blurring code was cut and pasted from Xiao
		// Feng Liu's
		// ABRA implementation
		ax = (int) Math.floor(oldSizeX / (float) newSizeX + 0.5);
		ay = (int) Math.floor(oldSizeY / (float) newSizeY + 0.5);
		az = (int) Math.floor(oldSizeZ / (float) newSizeZ + 0.5);

		if (ax == 1) {
			filterSize = 2;

		} else {
			filterSize = ax;
		}
		sigma = Math.pow(2, Math.log(ax) / (2 * Math.log(2.0)) - 1) * sigma;

		gaussKernel1D = new double[2 * filterSize + 1];
		gaussKernel = new double[2 * filterSize + 1][2 * filterSize + 1][2 * filterSize + 1];
		for (i = -filterSize; i <= filterSize; i++) {
			gaussKernel1D[i + filterSize] = (1.0 / (Math.sqrt(2 * Math.PI) * sigma))
			* Math.exp(-i * i / (2 * sigma * sigma));
		}

		tempVal = 0.0;
		for (k = -filterSize; k <= filterSize; k++) {
			for (j = -filterSize; j <= filterSize; j++) {
				for (i = -filterSize; i <= filterSize; i++) {
					gaussKernel[k + filterSize][j + filterSize][i + filterSize] = gaussKernel1D[k+ filterSize]
					                                                                            * gaussKernel1D[j + filterSize] * gaussKernel1D[i + filterSize];
					tempVal += gaussKernel[k + filterSize][j + filterSize][i+ filterSize];
				}
			}
		}
		for (k = -filterSize; k <= filterSize; k++) {
			for (j = -filterSize; j <= filterSize; j++) {
				for (i = -filterSize; i <= filterSize; i++) {
					gaussKernel[k + filterSize][j + filterSize][i + filterSize] /= tempVal;
				}
			}
		}
		
		ImageData tmpOld;
		ImageData tmpNew;

		for (i = 0; i < newSizeX; i++) {
			cx = i * ax;
			for (j = 0; j < newSizeY; j++) {
				cy = j * ay;
				for (k = 0; k < newSizeZ; k++) {
					cz = k * az;
					for (int ch = 0; ch < numOfCh; ch++) {
						tmpOld = oldData[ch];
						tmpNew = newData[ch];
						tempVal = 0;
						for (z = -filterSize; z <= filterSize; z++) {
							if (z + cz < 0 || z + cz > oldSizeZ - 1)
								continue;
							for (y = -filterSize; y <= filterSize; y++) {
								if (y + cy < 0 || y + cy > oldSizeY - 1)
									continue;
								for (x = -filterSize; x <= filterSize; x++) {
									if (x + cx < 0 || x + cx > oldSizeX - 1)
										continue;
									tempVal += gaussKernel[filterSize + z][filterSize + y][filterSize + x]
									                                            * tmpOld.getDouble(cx + x, cy + y, cz + z);
								}
							}
						}
						tmpNew.set(i, j, k, (int) Math.floor(tempVal + 0.5));
					}
				}
			}
		}
		// System.out.format("DOWN SAMPLE %d %d %d %d %d
		// %n",newSizeX,newSizeY,newSizeZ, data[0].talley(),
		// newData[0].talley());
		data = newData;
		XN = newSizeX;
		YN = newSizeY;
		ZN = newSizeZ;

	}
	
	public void interpolate(double x, double y, double z, double[] results) {
		
		for(int ch = 0; ch < numOfCh; ch++){
			results[ch] = RegistrationUtilities.Interpolation(data[ch], XN, YN, ZN, x, y,z, chInterpType[ch]);
		}
	}

	// rescale the image data to fall between 0 and bins-1
	public void normalize(double minVals[], double maxVals[], int bins) {
		int k, j, i, ch;

		float intervals[] = new float[numOfCh];
		for (ch = 0; ch < numOfCh; ch++) {
			intervals[ch] = ((float) (maxVals[ch] - minVals[ch] + 1)) / bins;
		}
		for (ch = 0; ch < numOfCh; ch++) {
			for (i = 0; i < XN; i++) for (j = 0; j < YN; j++) for (k = 0; k < ZN; k++) {
				data[ch].set(i, j, k, RegistrationUtilities.calculateBin(intervals[ch],
						minVals[ch], data[ch].getDouble(i, j, k)));
			}
		}
	}

	// for convenience -- make a copy
	public VabraVolumeCollection clone() {
		VabraVolumeCollection copy = new VabraVolumeCollection(XN, YN, ZN, numOfCh, chInterpType);
		// a.printInfo();
		for (int ch = 0; ch < numOfCh; ch++) {
			copy.data[ch] = data[ch].clone();
		}
		
		return copy;
	}

	public VabraVolumeCollection returnDeformedCopy(ImageDataFloat defField) {
		VabraVolumeCollection a = new VabraVolumeCollection(XN, YN, ZN, numOfCh, chInterpType);
		ImageData oldVol;
		ImageData newVol = new ImageDataFloat(XN, YN, ZN);
		// a.printInfo();
		int k, j, i, ch;
		for (ch = 0; ch < numOfCh; ch++) {
			oldVol = this.data[ch].clone();
			RegistrationUtilities.DeformImage3D(oldVol, newVol, defField, XN, YN, ZN, chInterpType[ch]);
			a.data[ch] = newVol.clone();
		}
		return a;
	}
	
	public int getXN() {return XN;}
	public int getYN() {return YN;}
	public int getZN() {return ZN;}
	public int getNumOfCh() {return numOfCh;}

	public void printInfo() {
		System.out.format("%d %d %d %d\n", XN, YN, ZN, numOfCh);
	}

}
