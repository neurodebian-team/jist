package edu.jhu.ece.iacl.algorithms.manual_label.staple;

/**
 * 
 * @author John Bogovic
 * @date 5/31/2008
 * 
 * Simultaneous Truth and Performance Level Estimation (STAPLE)
 * 
 * Warfield, Zou, and Wells, "Simultaneous Truth and Performace Level Estimation (STAPLE):
 * An Algorithm for the Validation of Image Segmentation," 
 * IEEE Trans. Medical Imaging vol. 23, no. 7, 2004
 */

import java.util.ArrayList;
import java.util.Iterator;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;



public class STAPLEgeneral extends AbstractCalculation{

	ArrayList<double[][]> raterData;
	ArrayList<double[][]> truth;
	
	EmbeddedSurface surf;			//contains connectivity info if required
	int[][] nbrtable;				//vertex connectivity information
	
	protected PerformanceLevel pl; //performance level estimates
	protected ArrayList<Float> priors;
	protected ArrayList<Number> labels;
	protected double convergesum;
	protected float normtrace;
	protected int maxiters = 1000;
	protected int offset;
	protected String dir;
	protected double eps=.00001;
	protected float beta=0f;			//regularization term
	protected float sigma=0f;			//kernel width for spatial smoothing
	protected int mrfiters = 10;
	protected String init;
	protected String mrfMethod = "ICM";
	
//	private final double sqrt2pi = Math.sqrt(2*Math.PI);
	
	public STAPLEgeneral(){
		super();
		setLabel("STAPLE");
	}
	public STAPLEgeneral(ArrayList<double[][]> data){
		super();
		setLabel("STAPLE");
		
		setData(data);
		
//		combineAllRaterLabs();
		getPriorProb();
	}
	
	public STAPLEgeneral(ArrayList<double[][]> data, int offset){
		super();
		setLabel("STAPLE");
		
		this.offset=offset;
		setData(data);
		
//		combineAllRaterLabs();
		getPriorProb();
	}

	public void setInit(String init){
		this.init=init;
	}
	
	public void setmaxIters(int max){
		maxiters=max;
	}
	public void setBeta(float beta){
		this.beta=beta;
	}
	public void setSigma(float sigma){
		this.sigma=sigma;
	}
	public void setEps(double eps){
		this.eps=eps;
	}
	public void setSurface(EmbeddedSurface surf){
		this.surf=surf;
	}
	
	public void setData(ArrayList<double[][]> data){
		raterData=data;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}
	public ArrayList<Number> getLabels(){
		return labels;
	}
	
	public boolean verifySizes(){
		int inds = raterData.get(0).length;
		for(int i=1; i<raterData.size(); i++){
			if(raterData.get(i).length!=inds){
				return false;
			}
		}
		return true;
	}
	
	public void findLabels(){
		labels = new ArrayList<Number>();
		if(raterData!=null){
			int inds = raterData.get(0).length;
			for(int i=0; i<inds; i++){
				for(int l=0; l<raterData.size(); l++){
					if(!labels.contains(new Double(raterData.get(l)[i][offset]))){
						labels.add(new Double(raterData.get(l)[i][offset]));
					}
				}
			}
		}else{
			System.err.println("jist.plugins"+"No data!");
		}
		labels.trimToSize();
		System.out.println("jist.plugins"+"\t"+"Found Labels: ");
		System.out.println("jist.plugins"+"\t"+labels);
		System.out.println("jist.plugins"+"\t"+"");
	}
	
//	public void initializeSingle(){
//		float init = 0.9999f;
//		try{
//			if(imagesArray!=null){
//				pl=new float[imagesArray[0][0][0].length][2];
//				for(int l=0; l<pl.length; l++){
//					pl[l][0]=init;
//					pl[l][1]=init;
//				}
//			}
//			else if(images!=null){
//				pl = new float[images.size()][2];
//				for(int l=0; l<pl.length; l++){
//					pl[l][0]=init;
//					pl[l][1]=init;
//				}
//			}
//			else{
//				System.err.println("jist.plugins"+"Rater data is null");
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	}
	
	public void initialize(){
		if(init.equals("Truth")){
			System.err.println("jist.plugins"+"NOT YET IMPLEMENTED!");
		}else {
			double init = 0.9999;
			findLabels();
			labels.trimToSize();
			System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
			pl = new PerformanceLevel(labels.size(), raterData.size(),true);
			pl.initialize(init);
			truth = new ArrayList<double[][]>();
			System.out.println("jist.plugins"+"\t"+"Num Rater Images: " + raterData.size());
			for(int i=0; i<labels.size(); i++){
				truth.add(new double[raterData.get(0).length][1]);
			}
			System.out.println("jist.plugins"+"\t"+"Num Truth Images: " + truth.size());
//			printPerformanceLevels();
		}
		if(beta>0){
			if(surf==null){
				System.err.println("jist.plugins"+"Non-zero beta requires a surface object");
			}else{
				nbrtable = EmbeddedSurface.buildNeighborVertexVertexTable(surf, EmbeddedSurface.Direction.COUNTER_CLOCKWISE);
			}
			
		}
		
	}
	
	/*
	 *	THIS METHOD MIGHT MAKE SENSE TO TRY IN SOME SITUATIONS, BUT FIRST ESTIMATING THE 
	 * PROBABILITIES POINT-WISE AND THEM ADJUSTING THEM WITH ICM OR RL MAKES MORE SENSE 
	 * TO ME RIGHT NOW (7/16/2009)
	 */
/*	public void EstepMRF(int iters){
		if(raterData!=null && beta>0){
			int inds = raterData.get(0).length;
			convergesum=0;
			int it = 0;
			int t = -1;
			int t2 = -1;
			int[] nbrs =null;
			double[] a = null;
			while(it<iters){

				for(int i=0; i<inds; i++){
					a = getPriorArray();
					nbrs = nbrtable[i];
					float[] w = computeDistanceNbrWeights(nbrs,i,sigma);
					for(int l=0; l<raterData.size(); l++){
						//Compute 'a' (Eqn 14)
						//Compute 'b' (Eqn 15)
						
						
						
						for(int m=0; m<labels.size(); m++){
							t = getIndex(labels,raterData.get(l)[i][offset]);

							if(t>-1){
								a[m]=a[m]*pl.getD(l, t, m);
							}else{
								System.err.println("jist.plugins"+"Could not find label!");
							}

							for(int ni=0; ni<w.length; ni++){
								t2 = getIndex(labels,raterData.get(l)[nbrs[ni]][offset]);
//								if(i==1 && m==1 && l ==1){
//									System.out.println("jist.plugins"+"\t"+"Neighbor weight is: " + (beta*w[ni]));
//									System.out.print("Truth was: " + a[m]);
//								}
								a[m]=(1-beta)*a[m] + beta*w[ni]*(pl.getD(l, t2, m));
//								if(i==1 && m==1 && l ==1){
//									System.out.print(" is: " + a[m] + "\n");
//								}
							}

						}

					}
					//				Compute weights for truth using Eqn (16)
					double sum=0;
					for(int m=0; m<labels.size(); m++){
						sum=sum+a[m];
					}
					for(int n=0; n<truth.size(); n++){
						truth.get(n)[i][0]=a[n]/sum;
						convergesum+=truth.get(n)[i][0];
					}
				}
				
				it++;
			}
		}else{
			Estep();
		}
	}*/
	
	public void Estep(){
		if(raterData!=null){
			int inds = raterData.get(0).length;
			convergesum=0;

			for(int i=0; i<inds; i++){
				double[] a = getPriorArray();
				for(int l=0; l<raterData.size(); l++){
					//Compute 'a' (Eqn 14)
					//Compute 'b' (Eqn 15)

					for(int m=0; m<labels.size(); m++){
						//						System.out.println("jist.plugins"+"\t"+"Label: " );
						int t = getIndex(labels,raterData.get(l)[i][offset]);

						if(t>-1){
							a[m]=a[m]*pl.getD(l, t, m);
						}else{
							System.err.println("jist.plugins"+"Could not find label!");
						}
					}
				}
//				Compute weights for truth using Eqn (16)
				double sum=0;
				for(int m=0; m<labels.size(); m++){
					sum=sum+a[m];
				}
				for(int n=0; n<truth.size(); n++){
					truth.get(n)[i][0]=a[n]/sum;
					convergesum+=truth.get(n)[i][0];
				}

			}
			if(beta>0){
				if (mrfMethod.equals("RL")){
					mrfRL(mrfiters);
				}else{
					mrfICM(mrfiters);
				}
			}
		}
	}
	
	/**
	 * Attempt to solve the MRF using Iterated Conditional Modes
	 * @param iters Max numer of iterations allowed in the solving the MRF
	 */
	public void mrfICM(int iters){
		System.out.print("Smoothing...");
		int num=0;
//		float max=0f;
		float[] pot=null;
		float normpot =0f; //normalization factor
//		int label=0;
		int it=0;
		ArrayList<double[][]> prevtruth = null;
		while(it<iters){
			prevtruth = deepCloneTruth();
			System.out.print(it+"...");
			for(int i=0; i<nbrtable.length; i++){  	//for each vertex
				//count number of neighbors
				num=0;
				for(int j=0; j<nbrtable[i].length; j++){
					if(nbrtable[i][j]>=0){
						num++;
					}
				}

				//compute weights for each neighbor
//				float[] w = computeUniformNbrWeights(num);
				float[] w;

				//compute the potential for each label choose the max
//				max = Float.MIN_VALUE;
				pot = new float[truth.size()];
				normpot=0;
//				label = -1;
				//loop over labels
				for(int j=0; j<truth.size(); j++){
					w = computeDistanceNbrWeights(nbrtable[i],i,sigma);
					//add the contribution of this
					pot[j]=(float)truth.get(j)[i][0];
					normpot+=pot[j];
					//add contribution for each neighbor
					for(int k=0; k<num; k++){
						if(nbrtable[i][k]>-1){
							pot[j]+=w[k]*prevtruth.get(j)[nbrtable[i][k]][0];
						}
					}
					//the label with the max potential is the true label
//					if(pot[j]>max){
//						label=j;
//						max=pot[j];
//					}

				}

				//set newly computed label probabilities at this location
				for(int j=0; j<truth.size(); j++){
					truth.get(j)[i][0]=(pot[j]/normpot);
				}
			}
			it++;
		}
		System.out.print("\n");
	}
	
	public ArrayList<double[][]> deepCloneTruth(){
		ArrayList<double[][]> clone = new ArrayList<double[][]>(truth.size());
		for(int i=0; i<truth.size(); i++){
			double[][] a = new double[truth.get(i).length][truth.get(i)[0].length];
			for(int j=0; j<a.length; j++){
				for(int k=0; k<a[0].length; k++){
					a[j][k]=truth.get(i)[j][k];
				}
			}
			clone.add(a);
		}
		return clone;
	}
	
	/**
	 * Attempt to solve the MRF using a Relaxation Labeling technique
	 * @param iters Max numer of iterations allowed in the solving the MRF
	 */
	public void mrfRL(int iters){
		System.out.print("Smoothing...");
		int num=0;
//		float max=0f;
		float[] pot=null;
		float normpot =0f; //normalization factor
//		int label=0;
		int it=0;
		while(it<iters){
			System.out.print(it+"...");
			for(int i=0; i<nbrtable.length; i++){  	//for each vertex
				//count number of neighbors
				num=0;
				for(int j=0; j<nbrtable[i].length; j++){
					if(nbrtable[i][j]>=0){
						num++;
					}
				}

				//compute weights for each neighbor
//				float[] w = computeUniformNbrWeights(num);
				float[] w;

				//compute the potential for each label choose the max
//				max = Float.MIN_VALUE;
				pot = new float[truth.size()];
				normpot=0;
//				label = -1;
				//loop over labels
				for(int j=0; j<truth.size(); j++){
					w = computeDistanceNbrWeights(nbrtable[i],i,sigma);
					//add the contribution of this
					pot[j]=(float)truth.get(j)[i][0];
					normpot+=pot[j];
					//add contribution for each neighbor
					for(int k=0; k<num; k++){
						if(nbrtable[i][k]>-1){
							pot[j]+=w[k]*truth.get(j)[nbrtable[i][k]][0];
						}
					}
					//the label with the max potential is the true label
//					if(pot[j]>max){
//						label=j;
//						max=pot[j];
//					}

				}

				//set newly computed label probabilities at this location
				for(int j=0; j<truth.size(); j++){
					truth.get(j)[i][0]=(pot[j]/normpot);
				}
			}
			it++;
		}
		System.out.print("\n");
	}
	
	/**
	 * Compute the weights for each neighbor if 
	 * @param num Number of neighbors
	 * @return array containing the weight for each neighbor
	 */
	private float[] computeUniformNbrWeights(int num){
		float[] weights = new float[num];
//		float w = (1-beta)/num;
		float w = beta;
		for(int i=0; i<num; i++){ weights[i]=w; }
		return weights;
	}
	
	//NEED TO THINK ABOUT HOW I WANT TO DO THIS
//	/**
//	 * Compute the weights for each neighbor where the weights depend on the distance
//	 * to the vertex of interest.  The closes neighbor will be given weight beta, while
//	 * all other neighbors will be given weight Beta*exp(-D) 
//	 * @param vert the index of the vertex under consideration
//	 * @param nbrs array of indices of neighbors
//	 * @return array containing the weight for each neighbor
//	 */
	private float[] computeDistanceNbrWeights(int[] nbrs, int vert, float sigma){

		if(beta>0){
			float[] weights = new float[nbrs.length];
			
			Point3f thisvert = new Point3f(surf.getVertex(vert));
			Point3f neighbor = new Point3f();
			for(int i=0; i<nbrs.length; i++){
				neighbor = surf.getVertex(nbrs[i]);
				weights[i]=(float)(beta*Math.exp(-thisvert.distanceSquared(neighbor)/sigma));
			}
			
//			for(int l=0; l<weights.length; l++){
//				System.out.print(weights[l]+", ");
//			}
//			System.out.print("\n");
			
			return weights;
			
		}else{
			System.err.println("jist.plugins"+"Neighborhood weights only relevent for beta>0");
			return null;
		}
	}
	

	public void Mstep(){
		if(raterData!=null){

			int inds = raterData.get(0).length;
			
			//Estimate performance parameters given the truth
			// using Eqns (18) & (19)
			int t =-1;
			double[] totsum = new double[labels.size()];
			//clear old performance level
			pl.clearD();
			for(int m=0; m<labels.size(); m++){
				for(int i=0; i<inds; i++){
					totsum[m]=totsum[m]+truth.get(m)[i][0];
					for(int l=0; l<raterData.size(); l++){
						t = getIndex(labels,raterData.get(l)[i][offset]);
						if(t>-1){
							pl.set(l, t, m, pl.getD(l, t, m)+truth.get(m)[i][0]);
						}else{
							System.err.println("jist.plugins"+"Could not find label!");
						}
					}
				}
			}
			// Store performance parameter estimates for this iteration
			// and divide by totals to allow interpretation as probability
			for(int n=0; n<labels.size(); n++){
				pl.divideByTots(n, totsum[n]);
			}
//			System.out.println("jist.plugins"+"\t"+pl);
		}
	}
	
	public void getPriorProb(){
		if(raterData!=null){
			int inds = raterData.get(0).length;
			float total = raterData.size()*inds;
			labels = new ArrayList<Number>();
			priors = new ArrayList<Float>();
			for(int i=0; i<inds; i++){
				for(int l=0; l<raterData.size(); l++){
					if(!labels.contains(raterData.get(l)[i][offset])){
						labels.add(raterData.get(l)[i][offset]);
						priors.add(new Float(1f));
					}else{
						int thisone = getIndex(labels,raterData.get(l)[i][offset]);
						priors.set(thisone, priors.get(thisone)+1);
					}
				}


			}
//			System.out.println("jist.plugins"+"\t"+"Sum: " + sum);
//			System.out.println("jist.plugins"+"\t"+"Total" + total);
			priors.trimToSize();
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
//				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
		}else{
			System.err.println("jist.plugins"+"Rater data is null");
		}
	}
	
	public void iterate(){
		if(init.equals("Truth")){
			initialize();
		}else{
			initialize();
			Estep();
//			EstepMRF(5);
		}
		
		double prevcs = convergesum;
		int iters = 0;	
		
//		System.out.println("jist.plugins"+"\t"+"Priors: ");
//		printArray(getPriorArray());
		
		boolean keepgoing = true;
		float ntrace = 0;
		while(keepgoing && iters<maxiters){
//			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
			Mstep();
			Estep();
//			EstepMRF(5);
			ntrace = pl.normalizedTrace();
			if(Math.abs(ntrace-normtrace)<eps){
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
//				printPerformanceLevels();
			}
			
//			if(Math.abs(prevcs-convergesum)<eps){
//				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
//				keepgoing=false;
//				printPerformanceLevels();
//			}
//			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
//			System.out.println("jist.plugins"+"\t"+"Prev Sum: " +prevcs);
//			System.out.println("jist.plugins"+"\t"+"Converge Sum: " +convergesum);
//			System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(prevcs-convergesum));
//			System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
//			System.out.println("jist.plugins"+"\t"+pl);
			
			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
			System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
			System.out.println("jist.plugins"+"\t"+"This Trace: " +ntrace);
			System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
//			System.out.println("jist.plugins"+"\t"+pl);
			System.out.println("jist.plugins"+"\t"+"*****************");
			iters++;
			
			
//			String iout = dir + "Iter"+iters+".raw";
//			System.out.println("jist.plugins"+"\t"+"Writing to: " +iout);
//			RawWriter.writeImgFloat(truth, iout);
			
			prevcs = convergesum;
			normtrace=ntrace;
		}
//		ParamObject<String> out = new ParamObject<String>("PerformanceLevels");
		
	}
	
	public void printPerformanceLevels(){
//		System.out.println("jist.plugins"+"\t"+pl);
	}
	public String getPerformanceLevels(){
		return pl.toString();
	}
		
	private static void printArray(double[] a){
		String ans = " ";
		for(int i=0; i<a.length; i++){
			ans = ans + a[i]+" ";
		}
		System.out.println("jist.plugins"+"\t"+ans);
	}
	
	private double[] getPriorArray(){
		double[] a = new double[labels.size()];
		for(int i=0; i<labels.size(); i++){
			a[i]=priors.get(i).floatValue();
		}
		return a;
	}
	
//	public void printPerformanceLevels2(){
//		System.out.println("jist.plugins"+"\t"+"*******************");
//		for(int i=0; i<pl.length; i++){
//			System.out.println("jist.plugins"+"\t"+"Rater " + i);
//			System.out.println("jist.plugins"+"\t"+"True Postitive Rate: " + pl[i][0]);
//			System.out.println("jist.plugins"+"\t"+"True Negative Rate: " + pl[i][1]);
//			System.out.println("jist.plugins"+"\t"+"*******************");
//		}
//		
//	}
	
	public ArrayList<double[][]> getTruth(){
		return truth;
	}


	public PerformanceLevel getPeformanceLevel(){
		return pl;
	}
	
	public int getIndex(ArrayList<Number> l, Number n){
		Iterator<Number> it = l.iterator();
		int i =0;
		while(it.hasNext()){
			Number ith = it.next();
			if(ith.doubleValue()==n.doubleValue()){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	
	//method to combine labels for testing
	public double[][] combineRaterLabels(double[][] raterin){
		double[][] raterout = new double[raterin.length][raterin[0].length];
		
		/*
		 * Three Label Case
		 */
//		for(int i=0; i<raterin.length; i++){
//			for(int j=0; j<raterin[0].length; j++){
//				if(raterin[i][j]>100){
//					raterout[i][j]=2;
//				}else if (raterin[i][j]==0){
//					raterout[i][j]=0;
//				}else{
//					raterout[i][j]=1;
//				}
//			}
//		}
		
		/*
		 * Two Label Case
		 */
		for(int i=0; i<raterin.length; i++){
			for(int j=0; j<raterin[0].length; j++){
				if(raterin[i][j]>100){
					raterout[i][j]=2;
				}else{
					raterout[i][j]=1;
				}
			}
		}
		
		return raterout;
	}
	
	public void combineAllRaterLabs(){
		ArrayList<double[][]> newlist = new ArrayList<double[][]>(raterData.size());
		for(double[][] rater: raterData){
			newlist.add(combineRaterLabels(rater));
		}
		raterData=newlist;
	}
	
	public double[][] getHardSeg(){
		double[][] seg = new double[truth.get(0).length][truth.get(0)[0].length];
		for(int i =0; i<seg.length; i++){
			double max = 0;
			int label = -1;
			for(int j=0; j<truth.size(); j++){
				if(truth.get(j)[i][0]>max){
					max = truth.get(j)[i][0];
					label= labels.get(j).intValue();
				}
			}
			seg[i][0]=label;
		}
		return seg;
	}


}
