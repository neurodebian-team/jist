package edu.jhu.ece.iacl.plugins.labeling.simulation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.vecmath.Point2i;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.manual_label.simulation.VolumeRater;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


public class AlgorithmSimulateVolumeRater extends ProcessingAlgorithm {
	//input params
	private ParamVolume volParam;
	private ParamInteger numRaters;
	private ParamOption connectivity;
	private ParamFloat performance;

	private ParamOption typeErr;

	private ParamFileCollection confusionfile;
	private ParamFileCollection perimerrfile;
	private ParamFileCollection growprobfile;
	private ParamFile labelfile;
	private ParamFile boundaryfile;
	private ParamInteger seed;

	//output params
	private ParamVolumeCollection raterVols;
	private ParamFile confusionfileout;
	private ParamFile perimerrfileout;
	private ParamFile growprobfileout;
	private ParamFile labelfileout;
	private ParamFile boundaryfileout;

	//other useful objects
	private TextFileReader text;
	private Random rand;


	private static final String cvsversion = "$Revision: 1.9 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Simulate Volume Raters.";
	private static final String longDescription = "Simulates volume raters either by a confusion matrix or perimeter error.";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(volParam=new ParamVolume("Truth Volume"));
		inputParams.add(numRaters=new ParamInteger("Number of Raters to Generate",1));

		inputParams.add(labelfile=new ParamFile("Labels", new FileExtensionFilter(new String[]{"txt","csv"})));
		labelfile.setMandatory(false);
		inputParams.add(confusionfile=new ParamFileCollection("Confusion Matrix", new FileExtensionFilter(new String[]{"txt","csv"})));
		confusionfile.setMandatory(false);

		inputParams.add(performance=new ParamFloat("Rater Performance",0.0f,1.0f,0.9f));

		inputParams.add(boundaryfile=new ParamFile("Boundaries", new FileExtensionFilter(new String[]{"txt","csv"})));
		boundaryfile.setMandatory(false);
		inputParams.add(perimerrfile=new ParamFileCollection("Permimeter Boundary Error", new FileExtensionFilter(new String[]{"txt","csv"})));
		perimerrfile.setMandatory(false);
		inputParams.add(growprobfile=new ParamFileCollection("Boundary Grow Probabilty File", new FileExtensionFilter(new String[]{"txt","csv"})));
		growprobfile.setMandatory(false);
		
		inputParams.add(typeErr=new ParamOption("Error Type",new String[]{"Perimeter","Confusion"}));
		inputParams.add(connectivity=new ParamOption("Connectivity",new String[]{"6","18","26"}));
		inputParams.add(seed=new ParamInteger("Random Seed",-1));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.Sim");
		inputParams.setLabel("Simulate Volume Raters");
		inputParams.setName("simVolRaters");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(raterVols=new ParamVolumeCollection("Rater Volumes"));
		raterVols.setLoadAndSaveOnValidate(false);
		outputParams.add(labelfileout=new ParamFile("Labels", new FileExtensionFilter(new String[]{"txt","csv"})));
		labelfileout.setMandatory(false);
		outputParams.add(boundaryfileout=new ParamFile("Boundaries", new FileExtensionFilter(new String[]{"txt","csv"})));
		boundaryfileout.setMandatory(false);
		
		outputParams.add(confusionfileout=new ParamFile("Confusion Matrix", new FileExtensionFilter(new String[]{"txt","csv"})));
		confusionfileout.setMandatory(false);
		
		outputParams.add(perimerrfileout=new ParamFile("Permimeter Boundary Error", new FileExtensionFilter(new String[]{"txt","csv"})));
		perimerrfileout.setMandatory(false);
		outputParams.add(growprobfileout=new ParamFile("Boundary Grow Probabilty File", new FileExtensionFilter(new String[]{"txt","csv"})));
		growprobfileout.setMandatory(false);
	}

	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		System.out.println(getClass().getCanonicalName()+"\t"+"Starting");

		//set the output directory and create it if it doesn't exist
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){ e.printStackTrace(); }

		//get ready

		GridPt.Connectivity conn;
		if(connectivity.getIndex()==0){
			conn=GridPt.Connectivity.SIX;
		}else if(connectivity.getIndex()==1){
			conn=GridPt.Connectivity.EIGHTEEN;
		}else{
			conn=GridPt.Connectivity.TWENTYSIX;
		}

		int num = numRaters.getInt();
		float totproberr = performance.getFloat();
		ImageData truthvol = volParam.getImageData();

		/* Parse Labels*/ 
		int[] labels =null;
		if(labelfile.getValue()!=null){
			float[][] labellist =null;
			text = new TextFileReader(labelfile.getValue());
			try {
				labellist  = text.parseFloatFile();
			} catch (IOException e) { 
				throw new RuntimeException("Unable to parse grad-file");
			}
			//check the format of the label file and error check
			if(labellist.length>0 && labellist[0].length==1){
				labels = new int[labellist.length];
				for(int i=0; i<labellist.length; i++){
					labels[i]=(int)labellist[i][0];
				}
			}else if(labellist[0].length>0 && labellist.length==1){
				labels = new int[labellist[0].length];
				for(int i=0; i<labellist[0].length; i++){
					labels[i]=(int)labellist[0][i];
				}
			}else{
				System.err.println(getClass().getCanonicalName()+"Invalid label file");
			}
		}else{
			labels = VolumeRater.getLabelList(truthvol);
		}


		/* Parse Boundaries*/ 
		ArrayList<Point2i> bounds=null;
		int[][] boundarray = null;
		if(boundaryfile.getValue()!=null){
			text = new TextFileReader(boundaryfile.getValue());
			try{
				boundarray  = text.parseIntFile();
			}catch(IOException e){
				e.printStackTrace();
			}
			System.out.println(getClass().getCanonicalName()+"\t"+boundarray.length +", " + boundarray[0].length);
			if(boundarray.length>0 && boundarray[0].length==2){
				bounds = new ArrayList<Point2i>(boundarray.length);
				for(int i=0; i<boundarray.length; i++){
					bounds.add(new Point2i(boundarray[i][0],boundarray[i][1]));
				}
			}else if(boundarray[0].length>0 && boundarray.length==2){
				bounds = new ArrayList<Point2i>(boundarray[0].length);
				for(int i=0; i<boundarray[0].length; i++){
					bounds.add(new Point2i(boundarray[0][i],boundarray[1][i]));
				}
			}else{
				System.err.println(getClass().getCanonicalName()+"Invalid Boundary file - must be Nx2 or 2xN");
			}
		}

		/*Ensure the lengths of relevant file collections are the same*/
		int numFiles=1;
		if(typeErr.getIndex()==0){
			if(perimerrfile!=null && growprobfile !=null){
				if(perimerrfile.size()>0|| growprobfile.size()>0){
					if(perimerrfile.size()==growprobfile.size()){
						numFiles=perimerrfile.size();
					}else{
						System.err.println(getClass().getCanonicalName()+"Perimeter error parameters must have the same number of files input");
					}
				}else{
					System.err.println(getClass().getCanonicalName()+"No perimeter error parameter files found.");
				}
			}
		}else{
			if(confusionfile!=null && confusionfile.size()>0){
				numFiles=confusionfile.size();
			}
		}

		int N = labels.length; //number of labels

		System.out.println(getClass().getCanonicalName()+"\t"+"NUMFILES: " +numFiles);

		ArrayList<File> vollist = null;
		if(numFiles==0){
			vollist = new ArrayList<File>(num);
		}else{
			vollist = new ArrayList<File>(num*numFiles);
		}
	

		for(int n=0; n<numFiles; n++){
			System.out.println(getClass().getCanonicalName()+"\t"+"file: " +n);
			float[] perErr = null;
			float[] growProb = null;
			float[][] conf=null;
			String addtoname = "";
			if(typeErr.getIndex()==1){
				/* Parse Confusion Matrix */ 

				if(confusionfile.getValue(n)!=null){
					text = new TextFileReader(confusionfile.getValue(n));
					addtoname=confusionfile.getValue(n).getName();
					addtoname=addtoname.substring(0, addtoname.length()-4);
					try {
						conf  = text.parseFloatFile();
					} catch (IOException e) { 
						throw new RuntimeException("Unable to parse grad-file");
					}
				}else{
					/* fake it */
					float max = performance.getFloat();
					float offterm = (1-max)/(N-1);
					conf = new float[N][N];
					for(int i=0; i<N; i++){
						for(int j=0; j<N; j++){
							if(i==j){
								conf[i][j]=max;
							}else{
								conf[i][j]=offterm;
							}
						}
					}
				}
			}else{
				/* Parse Permimeter Error Vector 
				 * and grow probability vector*/ 
				
				if(perimerrfile.getValue(n)!=null){
					//boundErrProb
					float[][] pererlist = null;
					text = new TextFileReader(perimerrfile.getValue(n));

					try{
						pererlist  = text.parseFloatFile();
					}catch(IOException e){
						e.printStackTrace();
					}
					if(pererlist.length>0 && pererlist[0].length==1){
						perErr = new float[pererlist.length];
						for(int i=0; i<pererlist.length; i++){
							perErr[i]=pererlist[i][0];
						}
					}else if(pererlist[0].length>0 && pererlist.length==1){
						perErr = new float[pererlist[0].length];
						for(int i=0; i<pererlist[0].length; i++){
							perErr[i]=pererlist[0][i];
						}
					}else{
						System.err.println(getClass().getCanonicalName()+"Invalid Perimeter Error Prob file");
					}

					//grow prob file
					pererlist = null;
					text = new TextFileReader(growprobfile.getValue(n));
					addtoname=growprobfile.getValue(n).getName();
					addtoname=addtoname.substring(0, addtoname.length()-4);
					try{
						pererlist  = text.parseFloatFile();
					}catch(IOException e){
						e.printStackTrace();
					}
					if(pererlist.length>0 && pererlist[0].length==1){
						growProb = new float[pererlist.length];
						for(int i=0; i<pererlist.length; i++){
							growProb[i]=pererlist[i][0];
						}
					}else if(pererlist[0].length>0 && pererlist.length==1){
						growProb = new float[pererlist[0].length];
						for(int i=0; i<pererlist[0].length; i++){
							growProb[i]=pererlist[0][i];
						}
					}else{
						System.err.println(getClass().getCanonicalName()+"Invalid Perimeter Growth Prob file");
					}
				}else{
					/* create a reasonable boundary error */

					VolumeRater tvr = new VolumeRater(truthvol);
					tvr.setConnectivity(conn);
					ArrayList<Integer> boundSizes = null;
					if(bounds==null){
						bounds = tvr.findBoundaries();
						boundSizes = tvr.getBoundarySizes();
					}else{
						tvr.setBoundaries(bounds);
						boundSizes = tvr.findBoundarySizes();
					}

					int total = 0;
					for(int i=0; i<boundSizes.size(); i++){ total+=boundSizes.get(i); } 

					bounds.trimToSize();
					int boundnum = bounds.size();
					perErr = new float[boundnum];

					//set the perimeter Error equal to the 'size' of each bondary
					//i.e. large boundaries have a proportionally higher probability
					//of an error occuring on them.

					//set the grow probability equal to 0.5 for all boundaries.
					growProb = new float[boundnum];
					for(int i=0; i<boundnum; i++){
						growProb[i]=0.5f;
						perErr[i] = boundSizes.get(i)/((float)total);
					}
					tvr = null;
				}
			}


			//init volume rater object
			VolumeRater vr = null;
			String filenameprefix = volParam.getValue().getName().substring(0, volParam.getValue().getName().length()-3);
			if(typeErr.getIndex()==0){
				vr = new VolumeRater(truthvol, totproberr, bounds, perErr, growProb);

				File pfile = writeVector(perErr,new File(dir.getAbsolutePath()+File.separator+filenameprefix+"_BoundaryError.txt"));
				File gfile = writeVector(growProb,new File(dir.getAbsolutePath()+File.separator+filenameprefix+"_GrowProbability.txt"));
				File boundfile = writeArrayListPoint(bounds,new File(dir.getAbsolutePath()+File.separator+filenameprefix+"_Boundaries.txt"));
				perimerrfileout.setValue(pfile);
				growprobfileout.setValue(gfile);
				boundaryfileout.setValue(boundfile);
			}else{
				vr = new VolumeRater(truthvol, totproberr, labels, conf);
				File cfile = writeArray(conf,new File(dir.getAbsolutePath()+File.separator+filenameprefix+"_ConfusionMatrix.txt"));
				confusionfileout.setValue(cfile);
			}
			File labfile = writeVector(labels,new File(dir.getAbsolutePath()+File.separator+filenameprefix+"_Labels.txt"));
			labelfileout.setValue(labfile);
			/********************************************* 
			 * set all the parameters if not specified.
			 *********************************************/

			vr.setConnectivity(conn);
			if(seed.getInt()>=0){
				vr.setSeed(seed.getLong());
			}
			//generate rater volumes
			for(int i=0; i<num; i++){	
				vr.setName(truthvol.getName()+"_"+addtoname+"_"+Integer.toString(i));
				//File volout = null;
				System.out.println(getClass().getCanonicalName()+"\t"+"ErrorType: " + typeErr.getValue());
				if(typeErr.getIndex()==0){
					System.out.println(getClass().getCanonicalName()+"\t"+"Perim");
					vr.genPerimRaterVolume();
					
//					volout=vr.writeRaterVolume(dir);
				}else{
					System.out.println(getClass().getCanonicalName()+"\t"+"Conf");
					vr.genConfRaterVolume();
					//volout=vr.writeRaterVolume(dir);
				}
				raterVols.add(vr.getLastRaterVolume());
				raterVols.writeAndFreeNow(this);
				vr.reset();
				
			}
		}

		//write output files
//		if(conf!=null){
//		}
		
//		System.out.println(getClass().getCanonicalName()+"\t"+"vollist");
//		System.out.println(getClass().getCanonicalName()+"\t"+vollist);
//		raterVols.setValue(vollist);
	}


	private File writeVector(float[] a, File f){
		StringReaderWriter srw = StringReaderWriter.getInstance(); 
		// Array to text
		String totxt = "";
		for(int i=0; i<a.length; i++){
			totxt = totxt+Float.toString(a[i])+"\n";
		}
		File fout = srw.write(totxt, f);
		return fout;
	}


	private File writeVector(int[] a, File f){
		StringReaderWriter srw = StringReaderWriter.getInstance(); 
		// Array to text
		String totxt = "";
		for(int i=0; i<a.length; i++){
			totxt = totxt+Integer.toString(a[i])+"\n";
		}
		File fout = srw.write(totxt, f);
		return fout;
	}


	private File writeArray(float[][] a, File f){
		StringReaderWriter srw = StringReaderWriter.getInstance(); 
		// Array to text
		String totxt = "";
		for(int i=0; i<a.length; i++){
			for(int j=0; j<a.length; j++){
				totxt = totxt+Float.toString(a[i][j])+"\t";
			}
			totxt = totxt+"\n";
		}
		File fout = srw.write(totxt, f);
		return fout;
	}


	private File writeArrayListPoint(ArrayList<Point2i> a, File f){
		StringReaderWriter srw = StringReaderWriter.getInstance(); 
		// Array to text
		String totxt = "";
		for(int i=0; i<a.size(); i++){
			totxt = totxt+ a.get(i).x+"\t";
			totxt = totxt+ a.get(i).y+"\n";
		}
		File fout = srw.write(totxt, f);
		return fout;
	}
}
