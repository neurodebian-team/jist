package edu.jhu.ece.iacl.plugins.quantitative;

import java.util.List;

import edu.jhu.ece.iacl.algorithms.quantitative.T1Fitting;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class T1Mapping_SliceShift extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolumeCollection T1volumes; 		//Unshuffled T1 mapping volumes
	private ParamVolume SliceTimes;		// times along inversion curve, one per slice
	private ParamVolumeCollection Mask;			// volume with mask of volume of interest (optional)
	private ParamOption NrParams;		// either "par2" or "par3"
	private ParamFloat RepTime;				// Repetition time of the scan
	
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolumeCollection T1map;
	private ParamVolumeCollection T1map_I0;
	private ParamVolumeCollection T1map_k;
	private ParamVolumeCollection T1map_ssqErr;
	//other quality flags?
	
	
	private static final String cvsversion = "$Revision: 1.2 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "T1 fitting of multiple volumes along an inversion curve.";
	private static final String longDescription = 	"Fits volumes to either a 2 parameter or 3 parameter T1 model:\n" +
													"  I(t)= I0*(1+(k-1)exp(-t/T1)-k*exp(-TR/T1)) where the following input and fitted parameters are defined:\n" +
													"  ~ I(t) is the measured intensities at different time intervals after the inversion pulse (input: 4D volume (collection)),\n" +
													"  ~ I0 is the intensity of the fully relaxed proton signal (fitted),\n" +
													"  ~ k is cos(phi) the cosine of the inversion angle, allowing for imperfect 180 degree pulses (fitted in 3 parameter fit),\n" +
													"  ~ t is the time after the inversion pulse (input: one row of timepoints for each slice (2D matrix))\n" +
													"  ~ T1 is the longitudinal relaxation rate (fitted)\n" +
													"  ~ TR is the repetition time used in the acquisition of the data (input: float in ms).\n" +
													"Magnitude data is assumed, which will be flipped at the minimum, resulting in an inversion curve from -I0 to +I0.\n" +
													"The volume may be masked using a 3D volume (collection) of the same size as the input volumes.\n" +
													"Outputs are the fitted parameters, as well as the sum of squared error from the measured data and the fit.\n" +
													"N.B.: all time units are assumed equal, so if you give the time axes in ms, the fitted T1 will be in ms as well.";
	

	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("UMCU");
		inputParams.setCategory("Quantitative");
		inputParams.setLabel("T1 sliceshift: T1 fitting");
		inputParams.setName("T1 sliceshift: T1 fitting");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com",""));
		info.setAffiliation("UMC Utrecht, Department of Radiology, 7 Tesla Group");
		info.setDescription(shortDescription +"\n"+longDescription);
		info.setLongDescription(longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
		info.add(new Citation("Lu H, van Zijl P, Hendrikse J, Golay X. \"Multiple acquisitions with global inversion cycling (MAGIC): a multislice technique for vascular-space-occupancy dependent fMRI\"Magn Reson Med, 51:9-15 (2004)"));


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/

		inputParams.add(T1volumes = new ParamVolumeCollection("Unshuffled T1 mapping volumes (4D)"));
		//T1volumes.setLoadAndSaveOnValidate(false);
		inputParams.add(SliceTimes = new ParamVolume("Matrix with inversion times, one per slice(2D image)", null,-1,-1,1,1));
		inputParams.add(Mask = new ParamVolumeCollection("Masking Volume (3D)"));
		Mask.setMandatory(false);
		String[] fitoptions = {"Two Parameters", "Three Parameters"}; 
		inputParams.add(NrParams = new ParamOption("Number of parameters of fit",fitoptions));
		inputParams.add(RepTime = new ParamFloat("Repetition Time (ms)", 1,30000, 10000));
	}
	
	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		T1map = new ParamVolumeCollection("fitted T1 map");
		T1map.setLoadAndSaveOnValidate(false);
		T1map_I0 = new ParamVolumeCollection("fitted I_0 map");
		T1map_I0.setLoadAndSaveOnValidate(false);
		T1map_k = new ParamVolumeCollection("fitted Inversion angle (cos(alpha))");
		T1map_k.setLoadAndSaveOnValidate(false);
		T1map_ssqErr = new ParamVolumeCollection("Sum of Squares Error of fit");
		T1map_ssqErr.setLoadAndSaveOnValidate(false);
		
		outputParams.add(T1map);
		outputParams.add(T1map_I0);
		outputParams.add(T1map_k);
		outputParams.add(T1map_ssqErr);

	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {
			
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 ****************************************************/
			this.setLabel("Initializing");
			JistLogger.logOutput(4, "T1Mapping_SliceShift: Start");
			
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			JistLogger.logMIPAVRegistry();
			String nrpar = NrParams.getValue();
			float TR= RepTime.getFloat();
			ImageData slicetimes = SliceTimes.getImageData();	
			List<ParamVolume> T1vollist = T1volumes.getParamVolumeList();
			List<ParamVolume> masklist = Mask.getParamVolumeList();
			boolean maskboo = false;
			if(Mask.size()!=0){
				maskboo = true;
			}
			
			int totcount = T1vollist.size();
			int count = 1;
			for(ParamVolume pv: T1vollist){  // Start of volume collection loop
				ImageData T1vols = pv.getImageData();
				ImageData maskim = null;
				if(maskboo){
					ParamVolume msk = masklist.get(count-1);
					maskim = msk.getImageData();
				}
					
				
				// Get sizes of input volumes
				int r=T1vols.getRows(), c=T1vols.getCols(), s=T1vols.getSlices(), t = T1vols.getComponents();
				int r1=slicetimes.getRows(), c1=slicetimes.getCols(), s1=slicetimes.getSlices(), t1=slicetimes.getComponents();
				if(maskboo){
					int r2=maskim.getRows(), c2=maskim.getCols(), s2=maskim.getSlices(), t2=maskim.getComponents();
					
					if((r!=r2)||(c!=c2)||(s!=s2)){
						JistLogger.logError(JistLogger.SEVERE, "Input dimensions do not match. The following inputs were found:\n" +
																"T1volumes ("+T1volumes.getName()+") (r,c,s,t) = ("+r+" , "+c+" , "+s+" , "+t+")\n"+
																"mask ("+Mask.getName()+") (r,c,s,t) = ("+r2+" , "+c2+" , "+s2+" , "+t2+").\n" +
																		"ABORTING");
						return;
					}
				}
				if((s!=r1)||(t!=c1)){
					JistLogger.logError(JistLogger.SEVERE, "Input dimensions do not match. The following inputs were found:\n" +
															"T1volumes ("+T1volumes.getName()+") (r,c,s,t) = ("+r+" , "+c+" , "+s+" , "+t+")\n"+
															"slicetimes ("+SliceTimes.getName()+") (r,c,s,t) = ("+r1+" , "+c1+" , "+s1+" , "+t1+")\n"+
															"ABORTING");
					return;
				}
				this.setTotalUnits(s*r);
	
				/****************************************************
				 * Step 3. Setup memory for the computed volumes
				 ****************************************************/
				this.setLabel("Slab "+count+" of "+ totcount + ": Allocating memory");
				JistLogger.logOutput(4, "T1Mapping_SliceShift: Allocating memory");
				
				ImageData Fit_T1 = new ImageDataFloat(r, c, s);
				Fit_T1.setHeader(T1vols.getHeader());
				ImageData Fit_I0 = new ImageDataFloat(r, c, s);
				Fit_I0.setHeader(T1vols.getHeader());
				ImageData Fit_k = new ImageDataFloat(r,c,s);
				Fit_k.setHeader(T1vols.getHeader());
				ImageData Fit_ssqErr = new ImageDataFloat(r, c, s);
				Fit_ssqErr.setHeader(T1vols.getHeader());
				
				if (nrpar.contentEquals("Two Parameters")){
					Fit_T1.setName(T1vols.getName()+"_2pT1Fit");
					Fit_I0.setName(T1vols.getName()+"_2pT1Fit_I0");
					Fit_ssqErr.setName(T1vols.getName()+"_2pT1Fit_ssqErr");
					Fit_k.setName(T1vols.getName()+"_2pT1Fit_k");			
				}
				else if (nrpar.contentEquals("Three Parameters")){
					Fit_T1.setName(T1vols.getName()+"_3pT1Fit");
					Fit_I0.setName(T1vols.getName()+"_3pT1Fit_I0");
					Fit_ssqErr.setName(T1vols.getName()+"_3pT1Fit_ssqErr");
					Fit_k.setName(T1vols.getName()+"_3pT1Fit_k");			
				}
	
				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program
				 * 		   has NO knowledge of the MIPAV data structure and
				 * 		   uses NO MIPAV specific components. This dramatic
				 * 		   separation is a bit inefficient, but it dramatically
				 * 		   lower the barriers to code re-use in other applications.
				 ****************************************************/
				this.setLabel("Slab "+count+" of "+ totcount + ": Fitting T1's");
				JistLogger.logOutput(4, "T1Mapping_SliceShift: Fitting T1's");
				
								// OUTLINE
				T1Fitting core = new T1Fitting();
				double []x;
				double []y;
				for (int k=0; k<s; k++){		//LOOP SLICES
					for (int i=0; i<r; i++){		//LOOP ROWS
						for (int j=0; j<c; j++){		// LOOP COLLUMS
							if(((maskboo) && (maskim.getInt(i, j, k) != 0)) || (!maskboo)){
								x = new double[t];
								y = new double[t];
								for(int l = 0; l<t; l++){
									x[l] = slicetimes.getFloat(k, l);				//get x[]
									y[l] = T1vols.getFloat(i, j, k, l);				//get y[]
								}
								
								// Find zero-crossing in magnitude data
								double minimum = Float.MAX_VALUE;
								int indx = x.length; //init
								for(int n=0; n<y.length; n++){
									if(y[n] < minimum){
										minimum = y[n];
										indx = n;
									}
								}
								double [] x_new = new double [x.length-1];
								double [] y_new = new double [y.length-1];
								// First part (to be negative)
								for (int n=0; n<indx; n++){
									x_new[n] = x[n];
									y_new[n] = -y[n];
								}
								for (int n=indx; n < x_new.length; n++){
									x_new[n] = x[n+1];
									y_new[n] = y[n+1];
								}
								
								double[] FitResult = core.T1Fit(x_new, y_new, TR, nrpar);	//Fit
								
								Fit_I0.set(i,j,k,(float)FitResult[0]);			// Store
								Fit_T1.set(i,j,k,(float)FitResult[1]);
								Fit_k.set(i,j,k,(float)FitResult[2]);
								Fit_ssqErr.set(i,j,k,(float)FitResult[3]);		
							}
								
						}
						this.setCompletedUnits(k*r +i );
					}
				}			
								
						
				
				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.
				 ****************************************************/
	
				this.setLabel("Slab "+count+" of "+ totcount + ": Saving Fit results");
				JistLogger.logOutput(4, "T1Mapping_SliceShift: Setting up exports.");
	
				T1map.add(Fit_T1);
				T1map.writeAndFreeNow(alg);
				T1map_I0.add(Fit_I0);
				T1map_I0.writeAndFreeNow(alg);
				T1map_k.add(Fit_k);
				T1map_k.writeAndFreeNow(alg);
				T1map_ssqErr.add(Fit_ssqErr);
				T1map_ssqErr.writeAndFreeNow(alg);
				count += 1;
				System.gc();
			}
			JistLogger.logOutput(4, "T1Mapping_SliceShift: FINISHED.");
		}
	}
}
