package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import edu.jhu.ece.iacl.jist.pipeline.ProcessingApplication;
import edu.jhu.ece.iacl.jist.test.MipavTestHarness;
import edu.jhu.ece.iacl.plugins.labeling.staple.MedicAlgorithmSTAPLE;


public class StaplePlugin extends ProcessingApplication{
	public StaplePlugin(){
		super(new MedicAlgorithmSTAPLE());
	}
	public static void main(String args[]){		
		MipavTestHarness harness=new MipavTestHarness(args);
		ProcessingApplication plug=new StaplePlugin();
		harness.invoke(plug);
	}
}