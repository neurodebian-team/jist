package edu.jhu.ece.iacl.plugins.utilities.file;

import java.util.Vector;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;


public class MedicAlgorithmVolumeConverter4D extends ProcessingAlgorithm{
	private ParamVolume volParam;
	private ParamVolume newVol;
	private ParamOption outputType;
	private ParamOption outputCompression;

	private ImageDataReaderWriter readerWriter;

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Convert a generic volume into a 4D volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(volParam=new ParamVolume("Volume",null,-1,-1,-1,-1));
		readerWriter=new ImageDataReaderWriter();
		Vector<String> exts=readerWriter.getExtensionFilter().getExtensions();
		inputParams.add(outputType=new ParamOption("File Type",exts));
		outputType.setValue("xml");
		Vector<String> comp = readerWriter.getCompressionOptions();
		inputParams.add(outputCompression=new ParamOption("Compression",comp));
		outputCompression.setValue(comp.firstElement());
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Volume Conversion");
		inputParams.setName("Volume_Conversion_4D");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(newVol=new ParamVolume("VolumeOut",null,-1,-1,-1,-1));
		newVol.setReaderWriter(readerWriter);
		newVol.setLoadAndSaveOnValidate(false);
	}


	protected void execute(CalculationMonitor monitor) {
//		String priorExt = readerWriter.getExtensionFilter().getPreferredExtension();
		
		newVol.getReaderWriter().getExtensionFilter().setPreferredExtension(outputType.getValue());
		try {
			readerWriter.setPreferredCompression(outputCompression.getValue());
		} catch (InvalidParameterException e) {
			e.printStackTrace();
		}
		newVol.setValue(volParam.getImageData());
		newVol.writeAndFreeNow(this);
//		readerWriter.getExtensionFilter().setPreferredExtension(priorExt);
		
	}
}
