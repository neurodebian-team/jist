package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.bme.smile.commons.math.FieldRegularize;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;


/*
 * public class ChebyShevFitting
 * 
 * This plugin performs ChebyShev polynomial fitting on an input data set.
 * 
 * @author Robert Kim
 *
 */
public class RegularizeNoiseField extends ProcessingAlgorithm{
	/*
	 * Input Parameters
	 * ParamVolume img - Image volume upon which Chebyshev polynomial fitting will be applied.
	 * ParamInteger degX - the number of degrees of freedom in the polynomial for the x (1st) dimension
	 * ParamInteger degY - the number of degrees of freedom in the polynomial for the y (2nd) dimension
	 */	
	private ParamVolume img;
	private ParamInteger degX;
	private ParamInteger degY;
	private ParamVolume weights;
	private ParamBoolean applyMask;

	/*
	 * Output Parameters
	 * ParamVolume result - Result image volume after Chebyshev polynomial fitting
	 */
	private ParamVolume result;

	/*
	 * CVS
	 */
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	private static final String shortDescription = "hebyshev polynomial fitting on an image volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/*
		 * Plugin Information
		 */
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Noise");
		inputParams.setName("Regularize Noise Field");
		inputParams.setLabel("Regularize Noise Field");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/a/jhu.edu/pami/"));
		info.add(new AlgorithmAuthor("Robert Kim", "rkim35@jhu.edu", "http://sites.google.com/a/jhu.edu/pami/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/*
		 * Input Initialization
		 */
		inputParams.add(img=new ParamVolume("Image for Chebyshev Fitting"));
		inputParams.add(weights=new ParamVolume("Corresponding importance weightings"));
		weights.setMandatory(false);
		inputParams.add(applyMask=new ParamBoolean("Mask Noise Field",true));
		inputParams.add(degX=new ParamInteger("Degrees of Freedom for x"));
		degX.setValue(new Integer(3));
		inputParams.add(degY=new ParamInteger("Degrees of Freedom for y"));
		degY.setValue(new Integer(3));
	}


	protected void createOutputParameters(ParamCollection outputParams){
		outputParams.add(result=new ParamVolume("Image Volume After Chebyshev Fitting",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException{
		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:Started"); System.out.flush();
		ImageData IMG = img.getImageData();
		ImageData wt = weights.getImageData();

		int DEG_X = degX.getInt();
		int DEG_Y = degY.getInt();
		int rows=IMG.getRows(), cols=IMG.getCols(), slices=IMG.getSlices(),
		components = IMG.getComponents();
		if(slices<1) slices=1;
		if(components<1) components=1;
		ImageData out = new ImageDataFloat(IMG);		
		out.setName(IMG.getName()+"_ChebyshevRegularized");
		float[][]tmp = new float[rows][cols];
		float[][]rst = new float[rows][cols];
		float [][]WT = new float[rows][cols];
		for(int p=0;p<components;p++){
			for(int q=0;q<slices;q++)
			{
				for(int i=0;i<rows;i++)
				{
					for(int j=0;j<cols;j++)
					{
						tmp[i][j] = IMG.getFloat(i, j, q,p);
						if(tmp[i][j]<=0)
							tmp[i][j]=Float.NaN;
					}
				}
				System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:Slice "+q); System.out.flush();
				if(wt==null) {
					rst = FieldRegularize.regularize2DChebyshev(DEG_X, DEG_Y, tmp);
				} else {
					int cnt =0;
					for(int i=0;i<rows;i++)
					{
						for(int j=0;j<cols;j++)
						{
							WT[i][j] = wt.getFloat(i, j, q,p);
							if(!(Double.isInfinite(WT[i][j])||Double.isNaN(WT[i][j])||WT[i][j]<=0))
								cnt++;
							else
								tmp[i][j]=Float.NaN;
						}
					}
					System.out.println(getClass().getCanonicalName()+"\t"+cnt);System.out.flush();

					//					rst = FieldRegularize.regularize2DChebyshevWLS(DEG_X, DEG_Y, tmp,WT);
					//					rst = FieldRegularize.regularize2DChebyshev(DEG_X, DEG_Y, tmp);
					rst = FieldRegularize.regularize2DChebyshevWLS(DEG_X, DEG_Y, tmp,WT);				
				}

				for(int i=0;i<rows;i++)
				{
					for(int j=0;j<cols;j++)
					{
						out.set(i,j,q,p,(rst[i][j]));		
					}
				}	

			}	
		}				
		result.setValue(out);
		System.out.println(getClass().getCanonicalName()+"\t"+"RegularizeNoiseField:Done"); System.out.flush();
	}
}
