package edu.jhu.ece.iacl.algorithms.hardi;

import JSci.maths.Complex;
import JSci.maths.matrices.ComplexMatrix;
import Jama.Matrix;
import edu.jhu.bme.smile.commons.math.specialFunctions.ComplexVector;
import edu.jhu.bme.smile.commons.math.specialFunctions.Legendre;
import edu.jhu.bme.smile.commons.math.specialFunctions.SphericalHarmonicRepresentation;
import edu.jhu.bme.smile.commons.math.specialFunctions.SphericalHarmonics;

public class QBall {

	public static int getNumberOfCoefficients(int order) {
		return SphericalHarmonics.numberOfCoefficientsInEvenOrder(order);
	}

/*************************************
 *************************************
 * Proportional to b0
 *************************************
 *************************************/	
	public static float [][][][] estimateSphericalHarmonics(float[][][][] coeff_real,float[][][][] coeff_imag,
			int[][] lm, float[][][][] DWdata, float[] bval, float[][] grads,
			byte[][][] mask, int SHorder, float b0Threshold) {

		/****************************************************
		 * Step 1: Validate Input Arguments 
		 ****************************************************/
		int bvalList[] = null;
		int gradList[] = null;
		int Nvols = DWdata[0][0][0].length;
		if(Nvols!=bval.length)
			throw new RuntimeException("estimateSphericalHarmonics: Number of volumes does not match number of bvalues.");
		if(Nvols!=grads.length)
			throw new RuntimeException("estimateSphericalHarmonics: Number of volumes does not match number of gradient directions.");

		if(mask==null) {
			mask = new byte[DWdata.length][DWdata[0].length][DWdata[0][0].length];
			for(int i=0;i<DWdata.length;i++) {
				for(int j=0;j<DWdata[0].length;j++) {
					for(int k=0;k<DWdata[0][0].length;k++)
						mask[i][j][k]=1;
				}
			}
		} 
		if(mask.length!=DWdata.length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 0.");
		if(mask[0].length!=DWdata[0].length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 1.");
		if(mask[0][0].length!=DWdata[0][0].length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 2.");


		int Ngrad = 0;
		int Nb0 = 0; 
		for(int i=0;i<bval.length;i++) {
			if(bval[i]==0)
				Nb0++;
			if(bval[i]>0 && grads[i][0]<90)
				Ngrad++;
		}

		/****************************************************
		 * Step 2: Index b0 and DW images and normalize DW directions
		 ****************************************************/

		bvalList = new int[Nb0];
		gradList = new int[Ngrad];
		Ngrad = 0;
		Nb0 = 0; 
		for(int i=0;i<bval.length;i++) {
			if(bval[i]==0) {
				bvalList[Nb0]=i;
				Nb0++;
			}

			if(bval[i]>0 && grads[i][0]<90) {
				gradList[Ngrad]=i;
				float norm = (float)Math.sqrt(grads[i][0]*grads[i][0]+
						grads[i][1]*grads[i][1]+
						grads[i][2]*grads[i][2]);
				if(norm==0)
					throw new RuntimeException("estimateSphericalHarmonics: Invalid DW Direction "+i+": ("+grads[i][0]+","+grads[i][1]+","+grads[i][2]+");");
				grads[i][0]/=norm;
				grads[i][1]/=norm;
				grads[i][2]/=norm;
				Ngrad++;
			}
		}
		
		float [][]gradonly = new float[Ngrad][3];
		for(int i=0;i<Ngrad;i++)
			for(int j=0;j<3;j++)
				gradonly[i][j]=grads[gradList[i]][j];

		/****************************************************
		 * Step 3: Build the imaging and inversion matrix 
		 ****************************************************/
		System.out.println("jist.plugins"+"\t"+"Init SHT");
		SphericalHarmonics sht=SphericalHarmonics.evenOrderTransform(SHorder,gradonly);
		sht.recordLM(lm);
		double []dataForOneVoxel = new double[gradList.length]; 
		/****************************************************
		 * Step 4: Loop over all voxels and estimate  
		 ****************************************************/
		float mb0=0;
		int cnt;

		for(int i=0;i<DWdata.length;i++) {
			for(int j=0;j<DWdata[0].length;j++) {
				for(int k=0;k<DWdata[0][0].length;k++) {
					if(mask[i][j][k]!=0) {
						mb0=0;
						cnt=0;
						for(int ii=0;ii<bvalList.length;ii++) {
							if(!Float.isNaN(DWdata[i][j][k][bvalList[ii]])) {
								mb0+=DWdata[i][j][k][bvalList[ii]];
								cnt++;
							}
						}						
						mb0/=cnt;
						if(mb0<=b0Threshold) {
							mask[i][j][k]=0;
						}
					}
					if(mask[i][j][k]!=0) {
						for(int ii=0;ii<gradList.length;ii++) {
							dataForOneVoxel[ii] = DWdata[i][j][k][gradList[ii]]/mb0;								
						}
						SphericalHarmonicRepresentation result = sht.transform(dataForOneVoxel);
						for(int m=0;m<result.length();m++) {
							coeff_real[i][j][k][m]=(float)result.getReal(m);
							coeff_imag[i][j][k][m]=(float)result.getImag(m);
						}
					} else {
						for(int m=0;m<coeff_real[i][j][k].length;m++) {
							coeff_real[i][j][k][m]=Float.NaN;
							coeff_imag[i][j][k][m]=Float.NaN;
						}
					}						
				}
			}
		}
		return coeff_imag;
	}

	
/*************************************
 *************************************
 * ADC: Apparent Diffusivity
 *************************************
 *************************************/		
	public static float [][][][] estimateSphericalHarmonicsADC(float[][][][] coeff_real,float[][][][] coeff_imag,
			int[][] lm, float[][][][] DWdata, float[] bval, float[][] grads,
			byte[][][] mask, int SHorder, float b0Threshold) {

		/****************************************************
		 * Step 1: Validate Input Arguments 
		 ****************************************************/
		int bvalList[] = null;
		int gradList[] = null;
		int Nvols = DWdata[0][0][0].length;
		if(Nvols!=bval.length)
			throw new RuntimeException("estimateSphericalHarmonics: Number of volumes does not match number of bvalues.");
		if(Nvols!=grads.length)
			throw new RuntimeException("estimateSphericalHarmonics: Number of volumes does not match number of gradient directions.");

		if(mask==null) {
			mask = new byte[DWdata.length][DWdata[0].length][DWdata[0][0].length];
			for(int i=0;i<DWdata.length;i++) {
				for(int j=0;j<DWdata[0].length;j++) {
					for(int k=0;k<DWdata[0][0].length;k++)
						mask[i][j][k]=1;
				}
			}
		} 
		if(mask.length!=DWdata.length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 0.");
		if(mask[0].length!=DWdata[0].length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 1.");
		if(mask[0][0].length!=DWdata[0][0].length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 2.");


		int Ngrad = 0;
		int Nb0 = 0; 
		for(int i=0;i<bval.length;i++) {
			if(bval[i]==0)
				Nb0++;
			if(bval[i]>0 && grads[i][0]<90)
				Ngrad++;
		}

		/****************************************************
		 * Step 2: Index b0 and DW images and normalize DW directions
		 ****************************************************/

		bvalList = new int[Nb0];
		gradList = new int[Ngrad];
		Ngrad = 0;
		Nb0 = 0; 
		for(int i=0;i<bval.length;i++) {
			if(Math.abs(bval[i])<1e-3) {
				bvalList[Nb0]=i;
				Nb0++;
			}

			if(bval[i]>0 && grads[i][0]<90) {
				gradList[Ngrad]=i;
				float norm = (float)Math.sqrt(grads[i][0]*grads[i][0]+
						grads[i][1]*grads[i][1]+
						grads[i][2]*grads[i][2]);
				if(norm==0)
					throw new RuntimeException("estimateSphericalHarmonics: Invalid DW Direction "+i+": ("+grads[i][0]+","+grads[i][1]+","+grads[i][2]+");");
				grads[i][0]/=norm;
				grads[i][1]/=norm;
				grads[i][2]/=norm;
				Ngrad++;
			}
		}
		
		float [][]gradonly = new float[Ngrad][3];
		for(int i=0;i<Ngrad;i++)
			for(int j=0;j<3;j++)
				gradonly[i][j]=grads[gradList[i]][j];

		/****************************************************
		 * Step 3: Build the imaging and inversion matrix 
		 ****************************************************/
		System.out.println("jist.plugins"+"\t"+"Init SHT");
		SphericalHarmonics sht=SphericalHarmonics.evenOrderTransform(SHorder,gradonly);
		sht.recordLM(lm);
		double []dataForOneVoxel = new double[gradList.length]; 
		/****************************************************
		 * Step 4: Loop over all voxels and estimate  
		 ****************************************************/
		float mb0=0;
		int cnt;

		for(int i=0;i<DWdata.length;i++) {
			for(int j=0;j<DWdata[0].length;j++) {
				for(int k=0;k<DWdata[0][0].length;k++) {
					if(mask[i][j][k]!=0) {
						mb0=0;
						cnt=0;
						for(int ii=0;ii<bvalList.length;ii++) {
							if(!Float.isNaN(DWdata[i][j][k][bvalList[ii]])) {
								mb0+=DWdata[i][j][k][bvalList[ii]];
								cnt++;
							}
						}						
						mb0/=cnt;
						if(mb0<=b0Threshold) {
							mask[i][j][k]=0;
						}
					}
					if(mask[i][j][k]!=0) {
						for(int ii=0;ii<gradList.length;ii++) {
							// - ln(dw/ref)
							dataForOneVoxel[ii] = Math.log(mb0/DWdata[i][j][k][gradList[ii]]);								
						}
						SphericalHarmonicRepresentation result = sht.transform(dataForOneVoxel);
						for(int m=0;m<result.length();m++) {
							coeff_real[i][j][k][m]=(float)result.getReal(m);
							coeff_imag[i][j][k][m]=(float)result.getImag(m);
						}
					} else {
						for(int m=0;m<coeff_real[i][j][k].length;m++) {
							coeff_real[i][j][k][m]=Float.NaN;
							coeff_imag[i][j][k][m]=Float.NaN;
						}
					}						
				}
			}
		}
		return coeff_imag;
	}
	
	
/*************************************
 *************************************
 * Funk-Radon Transform
 *************************************
 *************************************/
	public static float [][][][] estimateSphericalHarmonicsFRT(float[][][][] coeff_real,
			int[][] LM, float[][][][] DWdata, float[] bval, float[][] grads,
			byte[][][] mask, int SHorder, float lambda, float b0Threshold) {

		/****************************************************
		 * Step 1: Validate Input Arguments 
		 ****************************************************/
		int bvalList[] = null;
		int gradList[] = null;
		int Nvols = DWdata[0][0][0].length;
		if(Nvols!=bval.length)
			throw new RuntimeException("estimateSphericalHarmonics: Number of volumes does not match number of bvalues.");
		if(Nvols!=grads.length)
			throw new RuntimeException("estimateSphericalHarmonics: Number of volumes does not match number of gradient directions.");

		if(mask==null) {
			mask = new byte[DWdata.length][DWdata[0].length][DWdata[0][0].length];
			for(int i=0;i<DWdata.length;i++) {
				for(int j=0;j<DWdata[0].length;j++) {
					for(int k=0;k<DWdata[0][0].length;k++)
						mask[i][j][k]=1;
				}
			}
		} 
		if(mask.length!=DWdata.length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 0.");
		if(mask[0].length!=DWdata[0].length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 1.");
		if(mask[0][0].length!=DWdata[0][0].length)
			throw new RuntimeException("estimateSphericalHarmonics: Mask does not match data in dimension: 2.");


		int Ngrad = 0;
		int Nb0 = 0; 
		for(int i=0;i<bval.length;i++) {
			if(bval[i]==0)
				Nb0++;
			if(bval[i]>0 && grads[i][0]<90)
				Ngrad++;
		}

		/****************************************************
		 * Step 2: Index b0 and DW images and normalize DW directions
		 ****************************************************/

		bvalList = new int[Nb0];
		gradList = new int[Ngrad];
		Ngrad = 0;
		Nb0 = 0; 
		for(int i=0;i<bval.length;i++) {
			if(Math.abs(bval[i])<1e-3) {
				bvalList[Nb0]=i;
				Nb0++;
			}

			if(bval[i]>0 && grads[i][0]<90) {
				gradList[Ngrad]=i;
				float norm = (float)Math.sqrt(grads[i][0]*grads[i][0]+
						grads[i][1]*grads[i][1]+
						grads[i][2]*grads[i][2]);
				if(norm==0)
					throw new RuntimeException("estimateSphericalHarmonics: Invalid DW Direction "+i+": ("+grads[i][0]+","+grads[i][1]+","+grads[i][2]+");");
				grads[i][0]/=norm;
				grads[i][1]/=norm;
				grads[i][2]/=norm;
				Ngrad++;
			}
		}
		
		float [][]gradonly = new float[Ngrad][3];
		for(int i=0;i<Ngrad;i++)
			for(int j=0;j<3;j++)
				gradonly[i][j]=grads[gradList[i]][j];

		/****************************************************
		 * Step 3: Build the imaging and inversion matrix 
		 ****************************************************/
		System.out.println("jist.plugins"+"\t"+"Init SHT");
		int N = gradList.length;
		int R = SphericalHarmonics.numberOfCoefficientsInEvenOrder(SHorder);
		SphericalHarmonics.createLM(SHorder, LM); // order is the same for modified and complex SH coefficients  
		double TP[][] = SphericalHarmonics.cart2sph(SHorder,gradonly);
		Matrix S = new Matrix(N,1);	// Nx1 matrix of the input signal
		Matrix B = new Matrix(N,R);	// NxR matrix of SH basis elements using modified SH basis
		Matrix L = new Matrix(R,R);	// RxR Laplace-Beltrami smoothing matrix
		Matrix P = new Matrix(R,R);	// RxR FRT matrix
		Matrix C;					// coefficients of the ODF
		
		//debug
//		System.out.println("jist.plugins"+"\t"+"THPH=[");
//		for(int i=0;i<TP.length;i++) {
//			System.out.println("jist.plugins"+"\t"+TP[i][0]+"\t"+TP[i][1]);
//		}
//		System.out.println("jist.plugins"+"\t"+"];");
		/****************************************************
		 * Step 3.A: Initialize B matrix  
		 ****************************************************/
		for(int r=0;r<R;r++) {
			for(int n=0;n<N;n++) {
				B.set(n,r,SphericalHarmonics.getFRTCoeff(LM[r][0],LM[r][1],TP[n][0],TP[n][1])); // TODO: Problem!!!
			}			
		}
//		System.out.println("jist.plugins"+"\t"+"BB=[");
//		B.print(5,3);
//		System.out.println("jist.plugins"+"\t"+"];");
		/****************************************************
		 * Step 3.B: Initialize L matrix  
		 ****************************************************/
		for(int r=0;r<R;r++) {			
			L.set(r,r,Math.pow(LM[r][0],2)*Math.pow(LM[r][0]+1,2));			
		}
//		System.out.println("jist.plugins"+"\t"+"LL=[");
//		L.print(5,3);
//		System.out.println("jist.plugins"+"\t"+"];");
		/****************************************************
		 * Step 3.C: Initialize P matrix  
		 ****************************************************/
		for(int r=0;r<R;r++) {		
			P.set(r,r,2*Math.PI*Legendre.val(LM[r][0],0,0));
		}
//		System.out.println("jist.plugins"+"\t"+"PP=[");
//		P.print(5,3);
//		System.out.println("jist.plugins"+"\t"+"];");
		/****************************************************
		 * Step 3.D: Form T matrix  
		 ****************************************************/
		Matrix T = P.times((B.transpose().times(B).plus(L.times(lambda))).inverse()).times(B.transpose());
		
		
		// Debug: 
//		System.out.println("jist.plugins"+"\t"+"TT=[");
//		T.print(5,3);
//		System.out.println("jist.plugins"+"\t"+"];");
		/****************************************************
		 * Step 4: Loop over all voxels and estimate  
		 ****************************************************/
		float mb0=0;
		int cnt;

		for(int i=0;i<DWdata.length;i++) {
			for(int j=0;j<DWdata[0].length;j++) {
				for(int k=0;k<DWdata[0][0].length;k++) {
					if(mask[i][j][k]!=0) {
						mb0=0;
						cnt=0;
						for(int ii=0;ii<bvalList.length;ii++) {
							if(!Float.isNaN(DWdata[i][j][k][bvalList[ii]])) {
								mb0+=DWdata[i][j][k][bvalList[ii]];
								cnt++;
							}
						}						
						mb0/=cnt;
						if(mb0<=b0Threshold) {
							mask[i][j][k]=0;
						}
					}
					if(mask[i][j][k]!=0) {
						for(int ii=0;ii<gradList.length;ii++) {
							S.set(ii,0,DWdata[i][j][k][gradList[ii]]);								
						}						
						// T=P(B'B+lam*L)^-1*B';	C=TS
						C=T.times(S);
						for(int m=0;m<coeff_real[i][j][k].length;m++) {
							coeff_real[i][j][k][m]=(float) C.get(m,0);							
						}
					} else {
						for(int m=0;m<coeff_real[i][j][k].length;m++) {
							coeff_real[i][j][k][m]=Float.NaN;							
						}
					}						
				}
			}
		}
		return coeff_real;
	}

	public static int getOrderFromNumberOfCoeff(int components) {
		int order =0;
		int comp = SphericalHarmonics.numberOfCoefficientsInEvenOrder(order);
		System.out.println("jist.plugins"+"\t"+"A order: "+order+" comp:"+comp);
		while(components>comp) {
			System.out.println("jist.plugins"+"\t"+"C order: "+order+" comp:"+comp);
			order+=2;
			comp = SphericalHarmonics.numberOfCoefficientsInEvenOrder(order);
			System.out.println("jist.plugins"+"\t"+"B order: "+order+" comp:"+comp);
		}
		if(comp==components)
			return order;
		throw new RuntimeException("Invalid number of SH coefficients for an even order:"+components);

	}

	public static void projectSphericalHarmonics(float[][][][] reconReal,
			float[][][][] reconImag, float[][][][] realCoef,
			float[][][][] imagCoef, float[][] grads, int SHorder) {

		/****************************************************
		 * Step 2: Index b0 and DW images and normalize DW directions
		 ****************************************************/
		for(int i=0;i<grads.length;i++) {

			float norm = (float)Math.sqrt(grads[i][0]*grads[i][0]+
					grads[i][1]*grads[i][1]+
					grads[i][2]*grads[i][2]);
			if(norm==0)
				throw new RuntimeException("projectSphericalHarmonics: Invalid DW Direction "+i+": ("+grads[i][0]+","+grads[i][1]+","+grads[i][2]+");");
			grads[i][0]/=norm;
			grads[i][1]/=norm;
			grads[i][2]/=norm;
		}

		/****************************************************
		 * Step 3: Build the imaging and inversion matrix 
		 ****************************************************/

		SphericalHarmonics sht=SphericalHarmonics.evenOrderTransform(SHorder,grads);
		double []realDataForOneVoxel = new double[realCoef[0][0][0].length];
		double []imagDataForOneVoxel = new double[realCoef[0][0][0].length];
		/****************************************************
		 * Step 4: Loop over all voxels and estimate  
		 ****************************************************/

		for(int i=0;i<realCoef.length;i++) {
			for(int j=0;j<realCoef[0].length;j++) {
				for(int k=0;k<realCoef[0][0].length;k++) {
					if(!(Float.isInfinite(realCoef[i][j][k][0])||
							Float.isNaN(realCoef[i][j][k][0]))) {
						for(int ii=0;ii<realCoef[0][0][0].length;ii++) {
							realDataForOneVoxel[ii] = realCoef[i][j][k][ii];
							imagDataForOneVoxel[ii] = imagCoef[i][j][k][ii];
						}
						ComplexVector result = sht.inverseTransform(realDataForOneVoxel, imagDataForOneVoxel);
						for(int m=0;m<result.length();m++) {
							reconReal[i][j][k][m]=(float)result.getReal(m);
						}
						if(reconImag!=null) {
							for(int m=0;m<result.length();m++) {
								reconImag[i][j][k][m]=(float)result.getImag(m);
							}
						}
					} else {
						for(int m=0;m<reconReal[i][j][k].length;m++) {
							reconReal[i][j][k][m]=Float.NaN;
						}
						if(reconImag!=null) {
							for(int m=0;m<reconReal[i][j][k].length;m++) {
								reconImag[i][j][k][m]=Float.NaN;
							}
						}

					}						
				}
			}
		}

	}

	
	public static void projectRealSphericalHarmonics(float[][][][] reconReal,
			float[][][][] realCoef,
			float[][] grads, int SHorder) {

		/****************************************************
		 * Step 2: Index b0 and DW images and normalize DW directions
		 ****************************************************/
		for(int i=0;i<grads.length;i++) {

			float norm = (float)Math.sqrt(grads[i][0]*grads[i][0]+
					grads[i][1]*grads[i][1]+
					grads[i][2]*grads[i][2]);
			if(norm==0)
				throw new RuntimeException("projectSphericalHarmonics: Invalid DW Direction "+i+": ("+grads[i][0]+","+grads[i][1]+","+grads[i][2]+");");
			grads[i][0]/=norm;
			grads[i][1]/=norm;
			grads[i][2]/=norm;
		}

		/****************************************************
		 * Step 3: Build the imaging and inversion matrix 
		 ****************************************************/

		SphericalHarmonics sht=SphericalHarmonics.evenOrderTransform(SHorder,grads);
//		double []realDataForOneVoxel = new double[realCoef[0][0][0].length];
		Matrix realDataForOneVoxel  = new Matrix(realCoef[0][0][0].length, 1);
		
		

		

		
		
		/****************************************************
		 * Step 3.A: Initialize B matrix  
		 ****************************************************/
		int [][]LM = new int[QBall.getNumberOfCoefficients(SHorder)][2];
		SphericalHarmonics.createLM(SHorder, LM); // order is the same for modified and complex SH coefficients
		double TP[][] = SphericalHarmonics.cart2sph(SHorder,grads);
		Matrix B = new Matrix(TP.length,LM.length);	// NxR matrix of SH basis elements using modified SH basis
		for(int r=0;r<LM.length;r++) {
			for(int n=0;n<TP.length;n++) {
				B.set(n,r,SphericalHarmonics.getFRTCoeff(LM[r][0],LM[r][1],TP[n][0],TP[n][1])); // TODO: Problem!!!
			}			
		}
		
//		System.out.println("jist.plugins"+"\t"+"BB=[");
//		B.print(5,3);
//		System.out.println("jist.plugins"+"\t"+"];");
		
		
		/****************************************************
		 * Step 4: Loop over all voxels and estimate  
		 ****************************************************/

		for(int i=0;i<realCoef.length;i++) {
			for(int j=0;j<realCoef[0].length;j++) {
				for(int k=0;k<realCoef[0][0].length;k++) {
					if(!(Float.isInfinite(realCoef[i][j][k][0])||
							Float.isNaN(realCoef[i][j][k][0]))) {
						for(int ii=0;ii<realCoef[0][0][0].length;ii++) {
							realDataForOneVoxel.set(ii,0, realCoef[i][j][k][ii]);							
						}
						Matrix result = B.times(realDataForOneVoxel);
//						ComplexVector result = sht.inverseTransform(realDataForOneVoxel, imagDataForOneVoxel);
						for(int m=0;m<reconReal[i][j][k].length;m++) {
							reconReal[i][j][k][m]=(float)result.get(m,0);
						}						
					} else {
						for(int m=0;m<reconReal[i][j][k].length;m++) {
							reconReal[i][j][k][m]=Float.NaN;
						}						

					}						
				}
			}
		}

	}

}


