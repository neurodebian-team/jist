package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


public class MedicAlgorithmReverseComponents extends ProcessingAlgorithm{
	private ParamVolume inVol;
	private ParamVolume outVol;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Turn an XxYxZxT volume into a TxZxYxX volume.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inVol=new ParamVolume("Input Volume",null,-1,-1,-1,-1));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Reverse Components");
		inputParams.setName("Reverse_Components");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Output Volume",null,-1,-1,-1,-1));
		outputParams.setLabel("Reverse Components");
		outputParams.setName("revcomp");
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		ImageData vol=inVol.getImageData();
		int rows=vol.getCols();
		int cols=vol.getSlices();
		int slices=vol.getComponents();
		int comps=vol.getRows();
		ImageData volOut=new ImageDataMipav(vol.getName()+"_rev",vol.getType(),rows,cols,slices,comps);
		volOut.setName(vol.getName()+"_rev");
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				for(int k=0;k<slices;k++){
					for(int c=0;c<comps;c++){
						volOut.set(i, j, k, c, vol.getDouble(c, i, j, k));
					}
				}
			}
		}
		outVol.setValue(volOut);
	}
}
