package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;


/*
 * This module zips files (uzing GZip) not ending in .gz or .zip
 * and unzips files ending in .gz or .zip
 */
public class MedicAlgorithmZip extends ProcessingAlgorithm{
	private ParamFileCollection FilesIn;
	private ParamFile tempdir;

	private ParamFileCollection FilesOut;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Unzips '.zip' and '.gz' files. Zips files and file collections.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(FilesIn=new ParamFileCollection("File In"));
		inputParams.add(tempdir=new ParamFile("Destination Directory"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Zip/Unzip Files");
		inputParams.setName("Zip/Unzip_Files");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		FilesOut = new ParamFileCollection("File out");
		outputParams.add(FilesOut);

	}



	protected void execute(CalculationMonitor monitor) {
		String fileinname =FilesIn.getValue(0).getName();
		String fileinpath = FilesIn.getValue(0).getAbsolutePath();

		String dir = tempdir.getValue().getParentFile()+"/";

		System.out.println(getClass().getCanonicalName()+"\t"+fileinname);
		System.out.println(getClass().getCanonicalName()+"\t"+dir);

		ArrayList<File> filesout = new ArrayList<File>();

		//Zip if there are multiple input files
		if(FilesIn.size()>1){

		}else{
			//unzip if filename ends in "zip" or "gz".  Otherwise zip
		System.out.println(getClass().getCanonicalName()+"\t"+fileinname);
		System.out.println(getClass().getCanonicalName()+"\t"+fileinname.substring(fileinname.length()-3, fileinname.length()));
		System.out.println(getClass().getCanonicalName()+"\t"+fileinname.substring(fileinname.length()-2, fileinname.length()));
			if(fileinname.substring(fileinname.length()-3, fileinname.length()).equals("zip")){
				System.out.println(getClass().getCanonicalName()+"\t"+"unzipping .zip");
				try {
			        // Open the ZIP file
			        ZipInputStream in = new ZipInputStream(new FileInputStream(fileinpath));

			        while(in.available()>0){
			        	// Get the first entry
			        	ZipEntry entry = in.getNextEntry();

			        	// Open the output file
			        	String outFilename = dir+entry.getName();
			        	filesout.add(new File(outFilename));
			        	FileOutputStream out = new FileOutputStream(outFilename);

			        	// Transfer bytes from the ZIP file to the output file
			        	byte[] buf = new byte[1024];
			        	int len;
			        	while ((len = in.read(buf)) > 0) {
			        		out.write(buf, 0, len);
			        	}
			        	 //Close this output stream
				        out.close();
			        }

			        //Close the input stream
			        in.close();
			    } catch (IOException e) {
			    	e.printStackTrace();
			    }
			}else if(fileinname.substring(fileinname.length()-2, fileinname.length()).equals("gz")){
				System.out.println(getClass().getCanonicalName()+"\t"+"unzipping .gz");
				try {
			        // Open the compressed file
			        GZIPInputStream in = new GZIPInputStream(new FileInputStream(fileinpath));

			        // Open the output file
			        String outFilename = dir+fileinname.substring(0, fileinname.length()-3);
			        filesout.add(new File(outFilename));
			        FileOutputStream out = new FileOutputStream(outFilename);

			        // Transfer bytes from the compressed file to the output file
			        byte[] buf = new byte[1024];
			        int len;
			        while ((len = in.read(buf)) > 0) {
			            out.write(buf, 0, len);
			        }

			        // Close the file and stream
			        in.close();
			        out.close();
			    } catch (IOException e) {
			    	e.printStackTrace();
			    }
			}//Otherwise, zip the file
			else{
				System.out.println(getClass().getCanonicalName()+"\t"+"zipping to .gz");
				try {
			        // Create the GZIP output stream
			        String outFilename = dir+fileinname+".gz";
			        filesout.add(new File(outFilename));
			        GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(outFilename));

			        // Open the input file
			        System.out.println(getClass().getCanonicalName()+"\t"+fileinname);
			        FileInputStream in = new FileInputStream(fileinpath);

			        // Transfer bytes from the input file to the GZIP output stream
			        byte[] buf = new byte[1024];
			        int len;
			        while ((len = in.read(buf)) > 0) {
			            out.write(buf, 0, len);
			        }
			        in.close();

			        // Complete the GZIP file
			        out.finish();
			        out.close();
			    } catch (IOException e) {
			    	e.printStackTrace();
			    }
			}
		}

		FilesOut.setValue(filesout);

	}
}
