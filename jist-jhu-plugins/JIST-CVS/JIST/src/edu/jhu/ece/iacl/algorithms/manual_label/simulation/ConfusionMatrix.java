package edu.jhu.ece.iacl.algorithms.manual_label.simulation;

import java.util.HashMap;

public class ConfusionMatrix {
	
	/**
	 * Represents an NxN matrix where N is the number of labels.
	 * The i-j^th element is the probability that this rater will decide 
	 * label j is present when label i is the truth. 
	 */
	
	private 	float[][] 	confusion;		// confusion matrix
	private 	float[][] 	lookup;			// lookup confusion matrix
	private		int[] 		labels;			// labels 
	
	private 	int			N;				// number of labels
	
	private	HashMap<Integer,Integer> labeltoind;	//map from label numbers to 
	
	//constants 
	private final 	int 		INVALID=Integer.MIN_VALUE;

	/**
	 * contructor with labels only
	 *  @param labels - list of labels
	 */
	public ConfusionMatrix(int[] labels){
		this.labels=labels;
		N=labels.length;
		genMap();
	}
	
	/**
	 * contructor - with default labels
	 * @param confusion - confusion matrix as float array
	 */
	public ConfusionMatrix(float[][] confusion){
		if(confusion.length!=confusion[0].length){
			System.err.println("jist.plugins"+"Invalid confusion matrix - must be square!");
		}else{
			this.confusion=confusion;
			N = confusion.length;
			
			labels = new int[N];  //default labels 1-N
			for(int i=0; i<N; i++){ labels[i]=i; }
			
			genLookup();
			genMap();
		}
	}
	
	/**
	 * contructor with input labels
	 * @param confusion - confusion matrix as float array
	 * @param labels - list of labels
	 */
	public ConfusionMatrix(float[][] confusion, int[] labels){
		if(confusion.length!=confusion[0].length){
			System.err.println("jist.plugins"+"Invalid confusion matrix - must be square!");
		}else if(confusion.length!=labels.length){
			System.err.println("jist.plugins"+"Confusion matrix dimension must equal # of labels");
		}else{
			this.confusion=confusion;
			this.labels=labels;
			N=labels.length;
			genLookup();
			genMap();
		}
	}
	
	/**
	 * Generates the lookup to make genNextLabel quick and easy.
	 * The lookup is just the confusion matrix summed across rows
	 */
	private void genLookup(){
		lookup = new float[N][N];
		for(int i=0; i<N; i++){
			lookup[i][0]=confusion[i][0];
			for(int j=1; j<N; j++){
				lookup[i][j]=lookup[i][j-1]+confusion[i][j];
			}
		}
	}
	
	/**
	 * Generates the map from label numbers to indices into 
	 * the label array
	 */
	private void genMap(){
		labeltoind = new HashMap<Integer,Integer>();
		for(int i=0; i<N; i++){
			labeltoind.put(labels[i], i);
		}
	}
	
	/**
	 * Get the map.
	 */
	private HashMap getMap(){
		return labeltoind;
	}
	
	/**
	 * Get the confusion matrix.
	 */
	private float[][] getMatrix(){
		return confusion;
	}
	
	/**
	 * Given a true label, outputs this rater's label based
	 * on the confusion matrix
	 */
	public int genNextLabel(int truelabel){
		int truelabelind = -1;
		if(labeltoind.containsKey(truelabel)){
			truelabelind = labeltoind.get(truelabel);
		}else{ 
			return truelabel;
		}
		
		double r = Math.random();
		if(r<lookup[truelabelind][0]){
			return labels[0];
		}
		for(int i=1; i<N; i++){
			if(r>=lookup[truelabelind][i-1] && r<lookup[truelabelind][i]){
				return labels[i];
			}
		}
		
		return truelabel;
	}
	
	/**
	 * Given a true label's index, outputs this rater's label based
	 * on the confusion matrix
	 */
	public int genNextLabelFromIndex(int truelabelind){
		
		double r = Math.random();
		if(r<lookup[truelabelind][0]){
			return labels[0];
		}
		for(int i=1; i<N; i++){
			if(r>=lookup[truelabelind][i-1] && r<lookup[truelabelind][i]){
				return labels[i];
			}
		}
		return INVALID;
	}

	/**
	 * outpus a string representation of the lookup Array
	 */
	public String lookuptoString(){
		String out = "";
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				out = out+ "\t" + lookup[i][j];
			}
			out = out+"\n";
		}
		return out;
	}
	
	/**
	 * Prints the lookup Array
	 */
	public void printLookup(){
		System.out.print(lookuptoString());
	}
	
	public String toString(){
		String out = "";
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				out = out+ "\t" + confusion[i][j];
			}
			out = out+"\n";
		}
		return out;
	}
	
	/**
	 * Prints the confusion Matrix
	 */
	public void printConfusion(){
		System.out.print(this);
	}
	
	/* for testing purposes */
	public static void main(String[] args){
		System.out.println("jist.plugins"+"\t"+"Starting");
		
		float[][] mat = {{0.85f,0.1f,0.05f},{0.1f, 0.85f, 0.05f},{0.05f, 0.1f, 0.85f}};
		int[] labels = {3,4,10};
		ConfusionMatrix cm = new ConfusionMatrix(mat,labels);

		int lab=-1;
		int[] counts = new int[labels.length];
		
//		cm.printConfusion();
//		System.out.println("jist.plugins"+"\t"+);
//		cm.printLookup();
		
//		int truelabind = 2;
		int truelab = 4;
		int num = 100000;
		
		for(int i=0; i<num; i++){
			lab = cm.genNextLabel(truelab);
//			lab = cm.genNextLabelFromIndex(2);
			for(int j=0; j<labels.length; j++){
				if(lab==labels[j]){
					counts[j]++;
				}
			}
		}
		
		System.out.println("jist.plugins"+"\t");
		System.out.println("jist.plugins"+"\t"+"label 1: " + ((float)counts[0]/num));
		System.out.println("jist.plugins"+"\t"+"label 2: " + ((float)counts[1]/num));
		System.out.println("jist.plugins"+"\t"+"label 3: " + ((float)counts[2]/num));
		
	}
	

}
