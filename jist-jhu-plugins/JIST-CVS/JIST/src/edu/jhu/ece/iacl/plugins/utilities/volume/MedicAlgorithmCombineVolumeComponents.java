/*
 * 
 */
package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmCombineVolumeComponents extends ProcessingAlgorithm{
	ParamVolumeCollection inVols;
	ParamVolume outVol;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Concatenates volumes in the input Volume Collection into a " +
			"single multi-component Volume (ie. 4D Volume).";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inVols=new ParamVolumeCollection("Volumes"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Concatenate Volumes");
		inputParams.setName("Concatenate_Volumes");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("IACL","",""));
		info.setWebsite("iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Multi-Component Volume",null,-1,-1,-1,-1));

	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		List<ParamVolume> vols=inVols.getParamVolumeList();
		int comp=vols.size();
		
		ImageDataMipav cvol = null;
		for(int l=0;l<comp;l++){
			ImageData thisvol = vols.get(l).getImageData();
			if(l==0){
				cvol=new ImageDataMipav(vols.get(0).getName()+"_vec",thisvol.getType(),
						thisvol.getRows(),thisvol.getCols(),thisvol.getSlices(),comp);
				cvol.setHeader(thisvol.getHeader());
			}else {
				if(!thisvol.getHeader().isSameOrientation(cvol.getHeader())){
					System.err.println( "**********************************************\n" +
										"WARNING: Images have different orientations...\n" + 
										"**********************************************\n");
				}
				if(!thisvol.getHeader().isSameResolution(cvol.getHeader())){
					System.err.println( "**********************************************\n" +
										"WARNING: Images have different resolutions ...\n" + 
										"**********************************************\n");
				}
					
			}
			for(int i=0;i<cvol.getRows();i++){
				for(int j=0;j<cvol.getCols();j++){
					for(int k=0;k<cvol.getSlices();k++){

						cvol.set(i,j,k,l,thisvol.getDouble(i, j, k));
					}
				}
			}
			
			thisvol.dispose();
			thisvol=null;
			vols.get(l).dispose();
			
		}
		outVol.setValue(cvol);
	}
}
