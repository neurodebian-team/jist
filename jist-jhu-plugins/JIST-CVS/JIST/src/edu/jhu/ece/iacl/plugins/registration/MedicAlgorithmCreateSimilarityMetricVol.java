package edu.jhu.ece.iacl.plugins.registration;

import java.awt.Dimension;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.vabra.VabraAlgorithm;
import edu.jhu.ece.iacl.algorithms.vabra.VabraConfiguration;
import edu.jhu.ece.iacl.algorithms.vabra.VabraSolver;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

import gov.nih.mipav.model.algorithms.utilities.AlgorithmMatchImages;
import gov.nih.mipav.model.structures.ModelImage;

public class MedicAlgorithmCreateSimilarityMetricVol extends ProcessingAlgorithm {

	ParamVolumeCollection subject;
	ParamVolume target;
	ParamVolumeCollection subjectMasks;
	ParamVolume targetMasks;	
	ParamVolumeCollection metricVol;
	ParamInteger LocalRadius;
	ParamBoolean produceCorrHist;
	ParamObject<double[][]> CorrHist;

	private static final String revnum = VabraAlgorithm.getVersion();


	protected void createInputParameters(ParamCollection inputParams) {

		inputParams.add(subject = new ParamVolumeCollection("Subjects"));
		inputParams.add(target = new ParamVolume("Target"));
		inputParams.add(LocalRadius = new ParamInteger("Radius Surrounding Voxel to Compare", 5));
		inputParams.add(produceCorrHist = new ParamBoolean("Produce Correlation Histogram", false));
		inputParams.add(subjectMasks = new ParamVolumeCollection("Subject Masks"));
		subjectMasks.setMandatory(false);
		inputParams.add(targetMasks = new ParamVolume("Target Masks"));
		targetMasks.setMandatory(false);
		

		inputParams.setLabel("Create Similarity Metric Volume");
		inputParams.setName("metricVol");

		setPreferredSize(new Dimension(300, 600));

		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(CommonAuthors.minChen);
		info.setDescription("Selects N Atlases from potential atlases by comparing the subject in " +
		"template space against all potential atlases also in template space.");
		info.setLongDescription("NMI is currently used as the comparison meteric.");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(metricVol = new ParamVolumeCollection("Similarity Metric Volume"));
		outputParams.setName("metricVol");
		outputParams.add(CorrHist = new ParamObject<double[][]>("Correct Voxels vs NMI Histogram", new ArrayDoubleTxtReaderWriter()));
		CorrHist.setMandatory(false);
	}

	protected void execute(CalculationMonitor monitor) {
		List<ImageData> subList = subject.getImageDataList();
		ImageData tar = target.getImageData();
		List<ImageDataDouble> outList = new ArrayList<ImageDataDouble>();
		ImageData sub;
		ImageDataDouble out;

		int W = LocalRadius.getInt();

		int HistBins = 100;
		double[][] CorrHistArray = new double[HistBins][4];
		int boundingBox[] = new int[6];
		double currentNMI, temp;
		double MinandMaxValD[];
		int raters, rows, cols, slices, i, j, k,r ;

		rows = tar.getRows();
		cols = tar.getCols();
		slices = tar.getSlices();
		raters = subList.size();

		
		//optional Parameters;
		List<ImageData> subMasksList;
		ImageData tarMasks, subMasks;
		subMasksList = subjectMasks.getImageDataList();
		tarMasks = targetMasks.getImageData();
		subMasks = tar.mimic();
		


		//hardcoded parameters
		int numBins = 64;

		//initialize histograms
		int HistSub[] = new int[numBins];
		int HistAtlas[] = new int[numBins];
		int HistJoint[][] = new int[numBins][numBins];

		//normalize Target
		MinandMaxValD = calculateMinAndMaxVals(tar, tar);
		normalize(tar, MinandMaxValD[0], MinandMaxValD[1], numBins);

		for(r=0; r<raters; r++){

			System.out.format("Creating Metric Volume From Rater " + r + " ");
			//pull out information for specific rater
			sub = subList.get(r);
			out = new ImageDataDouble(sub.clone());

			if(produceCorrHist.getValue() == true) subMasks = subMasksList.get(r);
			
			//Normalize subject
			MinandMaxValD = calculateMinAndMaxVals(sub, sub);
			normalize(sub, MinandMaxValD[0], MinandMaxValD[1], numBins);


			//go through all all points on subject and target and find similarity metric
			for (i = 0; i < rows;i++){
				for (j = 0; j < cols;j++){
					for (k = 0; k < slices;k++){

						boundingBox[0] = Math.max(0,i-W);
						boundingBox[1] = Math.min(rows-1,i+W);
						boundingBox[2] = Math.max(0,j-W);
						boundingBox[3] = Math.min(cols-1,j+W);
						boundingBox[4] = Math.max(0,k-W);
						boundingBox[5] = Math.min(slices-1,k+W);


						RegistrationUtilities.Histogram3D(sub, numBins, boundingBox, HistSub);
						RegistrationUtilities.Histogram3D(tar, numBins, boundingBox,	HistAtlas);
						RegistrationUtilities.JointHistogram3D(sub, tar, numBins, boundingBox, HistJoint);

						currentNMI = RegistrationUtilities.NMI(HistSub, HistAtlas, HistJoint, numBins);

						out.set(i, j, k, currentNMI);

						//only do this if we want a correlation Histogram
						if(produceCorrHist.getValue() == true){
							if(subMasks.getInt(i, j, k) != 0 && subMasks.getInt(i, j, k) != 50){
								if(subMasks.getInt(i, j, k) == tarMasks.getInt(i, j, k)){
									CorrHistArray[(int)Math.round((currentNMI-1)*(HistBins-1))][1]++;
								}
								CorrHistArray[(int)Math.round((currentNMI-1)*(HistBins-1))][2]++;
							}
						}
					}
				}
			}
			outList.add(out);
		}

		//only do this if we want a correlation Histogram
		if(produceCorrHist.getValue() == true){
			for(i = 0; i < HistBins; i++) {
				CorrHistArray[i][0] = 1 + ((double)i)/((double)HistBins);
				if(CorrHistArray[i][2] != 0)	CorrHistArray[i][3] = CorrHistArray[i][1]/CorrHistArray[i][2];
			}
			CorrHist.setObject(CorrHistArray);
			CorrHist.setFileName("Histogram");
		}

		metricVol.setValue(outList);
	}


	public double[] calculateMinAndMaxVals(ImageData sub, ImageData tar) {

		int ch;
		int CH;
		int i, j, k;
		int x = 0, y = 0, z = 0;
		int mx = 0, my = 0, mz = 0;

		int XN, YN, ZN;
		XN = sub.getRows();
		YN = sub.getCols();
		ZN = sub.getSlices();
		double MinandMaxValsD[] = new double[2];
		double max = Double.NEGATIVE_INFINITY;
		double min = Double.POSITIVE_INFINITY;
		for (i = 0; i < XN; i++) {
			for (j = 0; j < YN; j++) {
				for (k = 0; k < ZN; k++) {

					if (sub.getDouble(i, j, k) > max) {
						max = sub.getDouble(i, j, k);
						mx = i;
						my = j;
						mz = k;
					}
					if (sub.getDouble(i, j, k) < min) {
						min = sub.getDouble(i, j, k);
						x = i;
						y = j;
						z = k;
					}
					if (tar.getDouble(i, j, k) > max) {
						max = tar.getDouble(i, j, k);
						mx = i;
						my = j;
						mz = k;
					}
					if (tar.getDouble(i, j, k) < min) {
						min = tar.getDouble(i, j, k);
						x = i;
						y = j;
						z = k;
					}

				}
			}
		}

		MinandMaxValsD[0] = min;
		MinandMaxValsD[1] = max;
		//System.out.format("Max: " + MinandMaxValsD[0] + " Min" + MinandMaxValsD[1] + "\n");
		return MinandMaxValsD;

	}

	// rescale the image data to fall between 0 and bins-1
	void normalize(ImageData sub, double minVals, double maxVals, int bins) {
		int k, j, i;
		float intervals = ((float) (maxVals - minVals + 1)) / bins;
		int XN = sub.getRows();
		int YN = sub.getCols();
		int ZN = sub.getSlices();

		for (i = 0; i < XN; i++) {
			for (j = 0; j < YN; j++) {
				for (k = 0; k < ZN; k++) {
					sub.set(i, j, k, RegistrationUtilities.calculateBin(intervals,
							minVals, sub.getDouble(i, j, k)));
				}
			}
		}
	}


	int[] calculateBoundingBox(ImageData sub, ImageData tar) {
		int k, j, i;
		int XN, YN, ZN, ext;
		int boundingBox[] = new int[6];
		XN = sub.getRows();
		YN = sub.getCols();
		ZN = sub.getSlices();
		ext = 5;

		boundingBox[1] = 0;
		boundingBox[0] = XN;
		boundingBox[3] = 0;
		boundingBox[2] = YN;
		boundingBox[5] = 0;
		boundingBox[4] = ZN;

		int count=0;
		for (i = 0; i < XN; i++){
			for (j = 0; j < YN; j++){
				for (k = 0; k < ZN; k++){
					// if (subject.data[k][j][i][ch] > 0 ||
					// target.data[k][j][i][ch] > 0)
					if ((Math.abs(sub.getDouble(i, j, k)) > 0.0000001) ||
							(Math.abs(tar.getDouble(i, j, k)) > 0.0000001))
						// changed to match Xiao Feng's abra implementation
					{
						count++;
						if (i < boundingBox[0])
							boundingBox[0] = i;
						if (i > boundingBox[1])
							boundingBox[1] = i;
						if (j < boundingBox[2])
							boundingBox[2] = j;
						if (j > boundingBox[3])
							boundingBox[3] = j;
						if (k < boundingBox[4])
							boundingBox[4] = k;
						if (k > boundingBox[5])
							boundingBox[5] = k;
					}
				}
			}
		}



		boundingBox[0]=Math.max(0, boundingBox[0]-ext); 			
		boundingBox[1]=Math.min(XN-1, boundingBox[1]+ext);
		boundingBox[2]=Math.max(0, boundingBox[2]-ext); 			
		boundingBox[3]=Math.min(YN-1, boundingBox[3]+ext);
		boundingBox[4]=Math.max(0, boundingBox[4]-ext); 			
		boundingBox[5]=Math.min(ZN-1, boundingBox[5]+ext);

		return boundingBox;

		/*for (i = 0; i < 6; i++) {
			System.out.format("bb[%d]:%d\n",i,boundingBox[i]);
		}*/

	}


}
