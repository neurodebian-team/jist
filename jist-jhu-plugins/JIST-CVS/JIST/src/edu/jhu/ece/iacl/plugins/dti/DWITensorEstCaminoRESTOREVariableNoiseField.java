package edu.jhu.ece.iacl.plugins.dti;

import gov.nih.mipav.model.structures.ModelImage;
import imaging.SchemeV1;
import inverters.AlgebraicDT_Inversion;
import inverters.DT_Inversion;
import inverters.LinearDT_Inversion;
import inverters.NonLinearDT_Inversion;
import inverters.RestoreDT_Inversion;
import inverters.WeightedLinearDT_Inversion;

import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLLMSE;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.utility.FileUtil;


public class DWITensorEstCaminoRESTOREVariableNoiseField extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolumeCollection DWdata4D; 		// Imaging Data
	private ParamVolume Mask3D;			// Binary mask to indicate computation volume
	private ParamVolume noiseLevel;		// Used for restore
	//	private ParamFile bvaluesTable;		// .b file with a list of b-values
	//	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions
	private ParamFile SchemeFile;
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume tensorVolume;	// A 4D volume with one tensor estimated per pixel
	private ParamVolume exitCodeVolume;	// A 3D volume 
	private ParamVolume intensityVolume;// A 3D volume 

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	private static final String shortDescription = "RESTORE Tensor estimation with variable noise fields.";
	private static final String longDescription = "Various tensor estimators. See Camino webpage for more details.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setCategory("Modeling.Diffusion.WholeVolume");
		inputParams.setPackage("Camino");
		inputParams.setLabel("RESTORE Tensor with Var Noise");
		inputParams.setName("RESTORE_Tensor_with_Var_Noise");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.add(new AlgorithmAuthor("Dan Peterson","petersond@kennedykrieger.org",""));
		info.setAffiliation("Computer Science Department - University College London");
		info.add(new Citation("Basser PJ, Mattielo J, and Lebihan D, Estimation of the effective self-diffusion tensor from the NMR spin echo, Journal of Magnetic Resonance, 103, 247-54, 1994."));
		info.add(new Citation("Jones DK and Basser PJ, Squashing peanuts and smashing pumpkins: How noise distorts diffusion-weighted MR data, Magnetic Resonance in Medicine, 52(5), 979-993, 2004."));
		info.add(new Citation("Alexander DC and Barker GJ, Optimal imaging parameters for fibre-orientation estimation in diffusion MRI, NeuroImage, 27, 357-367, 2005"));
		info.add(new Citation("Chang L-C, Jones DK and Pierpaoli C, RESTORE: Robust estimation of tensors by outlier rejection, Magnetic Resonance in Medicine, 53(5), 1088-1095, 2005."));
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamVolumeCollection("DWI and Reference Image(s) Data (4D)",null,-1,-1,-1,-1));
		inputParams.add(SchemeFile=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"scheme","schemev1"})));
		//		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		//		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(Mask3D=new ParamVolume("Mask Volume to Determine Region of Tensor Estimation (3D)",null,-1,-1,-1,1));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.			
		inputParams.add(noiseLevel=new ParamVolume("Noise Level Field",null,-1,-1,-1,1));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		tensorVolume = new ParamVolume("Tensor Estimate",null,-1,-1,-1,6);
		tensorVolume.setName("Tensor (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensorVolume);
		exitCodeVolume = new ParamVolume("Estimation Exit Code",null,-1,-1,-1,1);
		exitCodeVolume.setName("Exit Code");
		outputParams.add(exitCodeVolume);	
		intensityVolume = new ParamVolume("Intensity Estimate",null,-1,-1,-1,1);
		intensityVolume.setName("Intensity");
		outputParams.add(intensityVolume);

	}


	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to 
		 * 			know when the bug happened.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstCaminoRESTOREVariableNoiseField: Start");

		String code = "RESTOREvarNoiseField";
		/****************************************************
		 * Step 2. Parse the input data 
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Load data.");System.out.flush();
		List<ImageData> dwds=DWdata4D.getImageDataList();
		int r=dwds.get(0).getRows();
		int c=dwds.get(0).getCols();
		int s=dwds.get(0).getSlices();
		int d=0;
		ImageDataFloat DWFloat;
		System.out.println(getClass().getCanonicalName()+"\t"+"Counting dims...");
		for(int i=0;i<dwds.size();i++) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Dims: "+d);
			d+=dwds.get(i).getComponents();
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"Total Dims: "+d);
		if(dwds.size()>1) {
			DWFloat=new ImageDataFloat(r,c,s,d);
			int jD=0;
			for(int j=0;j<dwds.size();j++) {
				ImageData img = dwds.get(j);
				for(int jjD=0;jjD<img.getComponents();jjD++) {
					for(int jr=0;jr<r;jr++) 
						for(int jc=0;jc<c;jc++)
							for(int js=0;js<s;js++){
								DWFloat.set(jr,jc,js,jD,
										img.getFloat(jr,jc,js,jjD));
							}
					jD++;
				}
			} } else {
				DWFloat=new ImageDataFloat(dwds.get(0));
			}
		ImageData maskVol=Mask3D.getImageData();
		byte [][][]mask=null;
		if(maskVol!=null) {
			ImageDataUByte maskByte = new ImageDataUByte (maskVol);
			mask = maskByte.toArray3d();
		}


		System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
		SchemeV1 DTIscheme = null;

		XStream xstream = new XStream();
		xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
		try {
			ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
			DTIscheme=(SchemeV1)in.readObject();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		ImageDataFloat noiseField=new ImageDataFloat(this.noiseLevel.getImageData());

		/****************************************************
		 * Step 3. Perform limited error checking 
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Error checking."); System.out.flush();


		/****************************************************
		 * Step 4. Run the core algorithm. Note that this program 
		 * 		   has NO knowledge of the MIPAV data structure and 
		 * 		   uses NO MIPAV specific components. This dramatic 
		 * 		   separation is a bit inefficient, but it dramatically 
		 * 		   lower the barriers to code re-use in other applications.  		  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Allocate memory."); System.out.flush();
		float [][][][]data=DWFloat.toArray4d();
		int rows = data.length;
		int cols= data[0].length;
		int slices= data[0][0].length;
		int components= data[0][0][0].length;
		float [][][][]tensors = new float[rows][cols][slices][6];
		float [][][][]exitCode= new float[rows][cols][slices][1];
		float [][][][]intensity= new float[rows][cols][slices][1];


		System.out.println(getClass().getCanonicalName()+"\t"+"Run CAMINO estimate."); System.out.flush();
		EstimateTensorLLMSE.estimateCaminoRESTORE(data,mask,noiseField.toArray3d(),tensors,exitCode,intensity,DTIscheme);

		/****************************************************
		 * Step 5. Retrieve the image data and put it into a new
		 * 			data structure. Be sure to update the file information
		 * 			so that the resulting image has the correct
		 * 		 	field of view, resolution, etc.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
		int []ext=DWFloat.getModelImageCopy().getExtents();
		ModelImage img=null;
		ImageDataFloat out=new ImageDataFloat(tensors);
		img=(out).getModelImageCopy();

		ext[3]=6;
		img.setExtents(ext);
		FileUtil.updateFileInfo(dwds.get(0).getModelImageCopy(),img);		
		img.calcMinMax();

		out.setName(dwds.get(0).getModelImageCopy().getImageName()+"_Tensor"+code);
		tensorVolume.setValue(out);		



		out=new ImageDataFloat(exitCode);
		img=(out).getModelImageCopy();

		ext[3]=1;
		img.setExtents(ext);
		FileUtil.updateFileInfo(dwds.get(0).getModelImageCopy(),img);
		img.calcMinMax();		
		out.setName(dwds.get(0).getModelImageCopy().getImageName()+"_ExitCode"+code);
		exitCodeVolume.setValue(out);

		out=new ImageDataFloat(intensity);
		img=(out).getModelImageCopy();
		ext[3]=1;
		img.setExtents(ext);
		FileUtil.updateFileInfo(dwds.get(0).getModelImageCopy(),img);
		img.calcMinMax();

		out.setName(dwds.get(0).getModelImageCopy().getImageName()+"_Intensity"+code);
		intensityVolume.setValue(out);	
		/****************************************************
		 * Step 6. Let the user know that your code is finished.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstCaminoRESTOREVariableNoiseField: FINISHED");
	}
}
