package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.dti.GradientTable;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringArrayXMLReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;


public class DWICombineGradTables extends ProcessingAlgorithm {
	private ParamFileCollection gradvalsin;	

	//output parameter
	private ParamFile gradvalsout;		

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Concatinate multiple gradient tables into one larger gradient table.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(gradvalsin=new ParamFileCollection("Gradient Tables", new FileExtensionFilter(new String[]{"txt","dpf","grad"})));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Combine Gradient Table Lists");
		inputParams.setName("Combine_Gradient_Table_Lists");


		AlgorithmInformation info = getAlgorithmInformation();		
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);

	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(gradvalsout=new ParamFile("Table of Gradient Directions",new FileExtensionFilter(new String[]{"grad"})));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {

		String ext = gradvalsin.getValue(0).getName().substring(gradvalsin.getValue(0).getName().lastIndexOf('.')+1);
		String name = gradvalsin.getValue(0).getName().substring(0,gradvalsin.getValue(0).getName().lastIndexOf('.'));
		System.out.println("Name: "+ name);
		System.out.println("Ext: " + ext);
		if(ext.equals("grad")){
			gradvalsout.setValue(getOutputDirectory().getAbsoluteFile()+File.separator+"CombinedTable_"+gradvalsin.getValue(0).getName());
		}else{
			gradvalsout.setValue(getOutputDirectory().getAbsoluteFile()+File.separator+"CombinedTable_"+name+".grad");
		}

		PrintWriter fp;
		try {
			fp = new PrintWriter(new FileWriter(gradvalsout.getValue()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			throw new RuntimeException("CombineGradientTables: Cannout open output file:"+gradvalsout.getValue());
		}
		for(int i=0;i<gradvalsin.size();i++) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Loading:"+gradvalsin.getValue(i));
			/* Read the gradient tables */
			GradientTable gt = new GradientTable();
			double [][] myGt = gt.readTable(gradvalsin.getValue(i));			
			for(int j=0;j<myGt.length;j++) {
				fp.println(myGt[j][0]+"\t"+myGt[j][1]+"\t"+myGt[j][2]);
			}
		}
		fp.close();
		System.out.println(getClass().getCanonicalName()+"\t"+"Done.");
	}
}
