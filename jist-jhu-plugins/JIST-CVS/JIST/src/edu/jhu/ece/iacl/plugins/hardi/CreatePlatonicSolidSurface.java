package edu.jhu.ece.iacl.plugins.hardi;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.algorithms.hardi.SurfaceTools;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import gov.nih.mipav.view.renderer.J3D.model.structures.ModelTriangleMesh;


public class CreatePlatonicSolidSurface extends ProcessingAlgorithm{
	/*
	 * Ported from Matlab
	function POLY = inscribePlatonicSolid(shape,r)
	% function POLY = inscribePlatonicSolid(shape,r)
	% Regular Polyhedron Generators 
	% 
	% Regular Polyhedron are three dimensional objects that have
	% one type of regular polygon on the surface. There are only
	% a few:
	%        name        faces      face type           vertices  edges
	%     Tetrahedron       4   equilateral triangles      4        6
	%     Cube              6   squares                    8       12
	%     Dodecahedron     12   pentagons                 20       30
	%     Octahedron        8   equilateral triangles      6       12
	%     Icosahedron      12   equilateral triangles     12       30	
	% This is code based on a pseudocode at:
	% Pseudocode assumes all variables of type double except for i, j, k
	% which are obviously integer. The code was extracted from OpenGL
	% test programs similar to the commonly available "cube.c". This is
	% original code based on mathematics, not copied, covered by GNU
	% General Public License. 
	% http://www.csee.umbc.edu/~squire/reference/polyhedra.shtml
	%
	% (C)opyright 2005, Bennett Landman, bennett@bme.jhu.edu
	% Revision History:
	% Created: 2/16/2005
	 */

	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamOption shape; 	
	private ParamBoolean inscribe;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamSurface solidMesh;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Creates a surface for a platonic solid.";
	private static final String longDescription = "Inscribe a platonic solid.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Inscribe Platonic Solid");
		inputParams.setName("Inscribe_Platonic_Solid");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(shape=new ParamOption("Select shape",new String[]{"Tetrahedron","Octahedron","Icosahedron","Cube-Triangulated","Dodecahedron-Triangulated"}));
		inputParams.add(inscribe=new ParamBoolean("Inscribe in Unit Sphere?",true));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputParams.add(solidMesh=new ParamSurface("Solid Mesh"));
	}


	protected void execute(CalculationMonitor monitor) {
		//		"Tetrahedron","Cube","Dodecahedron","Octahedron","Icosahedron","Sphere"
		/*EmbeddedSurface surf=new EmbeddedSurface(null, null, null);
		s8urf.scaleVertices(scaleMesh.getFloat());
		surf.scaleData(scaleData.getFloat());
		surf.translate(new Point3f(offset.getValue()));
		Jama.Matrix m=transMatrix.getValue();
		if(invert.getValue())m=m.inverse();
		surf.transform(transMatrix.getValue());
		outSurf.setValue(surf);
		 */


		EmbeddedSurface surf = null;
		if(shape.getValue().compareToIgnoreCase("Tetrahedron")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.tetrahedron()));			
		} else if(shape.getValue().compareToIgnoreCase("Cube-Triangulated")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.cube()));
		} else if(shape.getValue().compareToIgnoreCase("Dodecahedron-Triangulated")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.dodecahedron()));
		} else if(shape.getValue().compareToIgnoreCase("Octahedron")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.octahedron()));
		} else if(shape.getValue().compareToIgnoreCase("Icosahedron")==0) {
			surf=(new EmbeddedSurface(icosahedron()));
		} else {
			throw new RuntimeException("Unknown shape");
		}
		if(inscribe.getValue()) 
			surf=new EmbeddedSurface(SurfaceTools.normalizeToSphere(surf,1));
		if(inscribe.getValue())
			surf.setName(shape.getValue()+"_inscribed");
		else 
			surf.setName(shape.getValue());
		solidMesh.setValue(surf); 
//		solidMesh.setName(shape.getValue());
	}


	//	%%%% ICOSAHEDRON %%%%
	ModelTriangleMesh icosahedron() {
		double phiaa  = 26.56505; //%/ phi needed for generation *
		double phia = Math.PI*phiaa/180.0; //%/ 2 sets of four points *
		double theb = Math.PI*36.0/180.0;  //%/ offset second set 36 degrees *
		double the72 = Math.PI*72.0/180;  //% / step 72 degrees *
		Point3f []pts = new Point3f[12];
		pts[0] = new Point3f(0,0,1);
		pts[11] = new Point3f(0,0,-1);
		//	  vertices(1,:) = [0 0 r];
		//	  vertices(12,:) = [0 0 -r];
		double the = 0.0;
		for(int i=1;i<=5;i++) {
			pts[i]=new Point3f((float)(Math.cos(the)*Math.cos(phia)),(float)(Math.sin(the)*Math.cos(phia)),(float)( Math.sin(phia)));
			the = the+the72;
		}
		the=theb;
		for(int i=6;i<=10;i++) {
			pts[i]=new Point3f((float)(Math.cos(the)*Math.cos(-phia)),(float)(Math.sin(the)*Math.cos(-phia)),(float)( Math.sin(-phia)));
			//	    vertices(i+1,:) = [r*cos(the)*cos(-phia) r*sin(the)*cos(-phia) r*sin(-phia)];
			the = the+the72;
		}

		//	  % / map vertices to 20 faces *
		int []connect = {0,1,2,
				0,2,3,
				0,3,4,
				0,4,5,
				0,5,1,
				11,6,7,
				11,7,8,
				11,8,9,
				11,9,10,
				11,10,6,
				1,2,6,
				2,3,7,
				3,4,8,
				4,5,9,
				5,1,10,
				6,7,2,
				7,8,3,
				8,9,4,
				9,10,5,
				10,6,1};
		return new ModelTriangleMesh(pts,connect);
	}
}
