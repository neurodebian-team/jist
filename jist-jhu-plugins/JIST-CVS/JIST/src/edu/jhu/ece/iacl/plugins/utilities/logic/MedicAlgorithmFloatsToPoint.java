/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.logic;

import javax.vecmath.Point3d;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmFloatsToPoint extends ProcessingAlgorithm{
	ParamDouble ptx,pty,ptz;
	ParamPointDouble pt;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Convert Floats to Points.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(ptx=new ParamDouble("X"));
		inputParams.add(pty=new ParamDouble("Y"));
		inputParams.add(ptz=new ParamDouble("Z"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Floats to Point");
		inputParams.setName("Floats_to_Point");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(pt=new ParamPointDouble("Point"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		pt.setValue(new Point3d(ptx.getDouble(),pty.getDouble(),ptz.getDouble()));
	}
}
