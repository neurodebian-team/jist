package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.util.List;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


public class MedicAlgorithmFindEqualVolume extends ProcessingAlgorithm{
	private ParamVolumeCollection candidateVol;
	private ParamVolume targetVolume;

	private ParamObject<String> matchingVolume;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Find Equal Volume");
		inputParams.setName("Find_Equal_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(CommonAuthors.johnBogovic);
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.NotFunctional);


		inputParams.add(candidateVol=new ParamVolumeCollection("Candidate Volumes"));
		inputParams.add(targetVolume=new ParamVolume("Target Volumes"));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(matchingVolume=new ParamObject<String>("Matching Volume", new StringReaderWriter()));
	}


	protected void execute(CalculationMonitor monitor) {
		List<ParamVolume> candlist = candidateVol.getParamVolumeList();
		String out = "No Matches";

		System.out.println(getClass().getCanonicalName()+"\t"+targetVolume.getImageData());
		int l=0;
		boolean notfound=true;
		while(l<candlist.size() && notfound){
			System.out.println(getClass().getCanonicalName()+"\t"+candlist.get(l));
			if(candlist.get(l).getRows()==targetVolume.getImageData().getRows() &&
					candlist.get(l).getCols()==targetVolume.getImageData().getCols() &&
					candlist.get(l).getSlices()==targetVolume.getImageData().getSlices() &&
					candlist.get(l).getComponents()==targetVolume.getImageData().getComponents()){

				ImageData vol = candlist.get(l).getImageData();
				boolean same = true;
				for(int i=0;i<vol.getRows();i++){
					for(int j=0;j<vol.getCols();j++){
						for(int k=0;k<vol.getSlices();k++){
							if(vol.get(i, j, k).doubleValue()-targetVolume.getImageData().getDouble(i, j, k)<0.000001){
								same=false;
							}
						}
					}
				}

				if(same){
					System.out.println(getClass().getCanonicalName()+"\t"+targetVolume.getName() + " <- " + vol.getName());
					out = candlist.get(l).getName();
					notfound=false;
				}
				vol.dispose();
				vol=null;
				candlist.get(l).dispose();
			}
			
			l++;
		}

		matchingVolume.setObject(out);
		matchingVolume.setFileName("Match.txt");
	}
}
