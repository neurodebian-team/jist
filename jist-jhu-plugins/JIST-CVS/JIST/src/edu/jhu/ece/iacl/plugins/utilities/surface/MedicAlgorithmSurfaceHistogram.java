package edu.jhu.ece.iacl.plugins.utilities.surface;

import edu.jhu.bme.smile.commons.math.HistogramGenerator;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.plugins.JistPluginUtil;


public class MedicAlgorithmSurfaceHistogram extends ProcessingAlgorithm{
	protected ParamSurface surf;
	protected ParamInteger bins,index;
	protected ParamDouble min,max;
	protected ParamBoolean logScale,autoMinMax,robustMinMax,yLogScale;
	protected ParamString valueName;
	protected ParamInteger rows,cols;
	protected ParamObject<double[][]> histogram;
	protected ParamVolume histImg;
	protected ParamOption embeddedDataType;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Computes a histogram for a vertex embedded surface quantity.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surf=new ParamSurface("Surface"));
		inputParams.add(embeddedDataType=new ParamOption("Embedded Data",new String[]{"Vertex","Face"}));
		inputParams.add(index=new ParamInteger("Offset Index",0,10000000,0));
		inputParams.add(bins=new ParamInteger("Number of bins",1,10000000,256));
		inputParams.add(logScale=new ParamBoolean("Log Scale"));
		inputParams.add(min=new ParamDouble("Min"));
		inputParams.add(max=new ParamDouble("Max"));
		inputParams.add(autoMinMax=new ParamBoolean("Auto Min/Max",true));
		inputParams.add(robustMinMax=new ParamBoolean("Robust Min/Max"));
		inputParams.add(valueName=new ParamString("Measurement Name","value"));
		inputParams.add(rows=new ParamInteger("Image Width",0,1600,800));
		inputParams.add(cols=new ParamInteger("Image Height",0,1600,600));
		inputParams.add(yLogScale=new ParamBoolean("Probability in Log Scale",true));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Measurement.Statistics");
		inputParams.setLabel("Surface Histogram");
		inputParams.setName("Surface_Histogram");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(histogram=new ParamObject<double[][]>("Histogram",new ArrayDoubleTxtReaderWriter()));
		outputParams.add(histImg=new ParamVolume("Histogram Image",VoxelType.COLOR_FLOAT));
		histImg.getExtensionFilter().setPreferredExtension("tiff");
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		HistogramGenerator hist=new HistogramGenerator();
		monitor.observe(hist);
		hist.setIndex(index.getInt());
		hist.setBins(bins.getInt());
		hist.setLogScale(logScale.getValue());
		hist.setMin(min.getDouble());
		hist.setMax(max.getDouble());
		hist.setAutoMinMax(autoMinMax.getValue());
		hist.setRobustMinMax(robustMinMax.getValue());
		EmbeddedSurface mesh=surf.getSurface();
		double[][] histData=(embeddedDataType.getIndex()==0)?hist.solve(mesh.getVertexData()):hist.solve(mesh.getCellData());

		histogram.setFileName(mesh.getName()+"_hist");
		histogram.setObject(histData);
		ImageDataColor imgData=hist.getImage("Histogram for "+mesh.getName(),valueName.getValue(),yLogScale.getValue(),rows.getInt(),cols.getInt());
		imgData.setName(mesh.getName()+"_plot");
		histImg.setValue(imgData);
	}
}
