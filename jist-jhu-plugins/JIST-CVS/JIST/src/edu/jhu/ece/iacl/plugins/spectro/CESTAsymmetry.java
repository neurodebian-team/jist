package edu.jhu.ece.iacl.plugins.spectro;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import ptolemy.math.DoubleArrayMath;
import ptolemy.math.DoubleArrayStat;

import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.bme.smile.commons.math.Spline;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader.MeasurementUnit;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class CESTAsymmetry extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume MTRVolume;			// 4-D Volume containing MT data with a range of offsets	
	private ParamVolume WASSRshiftmap; 		// 3D-Volume containing WASSR-based F0 offsets
	private ParamVolume MaskVolume;			// 3D-Volume containing mask of the measured volume
	private ParamFile shiftlist;			// txt file containing list of input offsets
	private ParamFloat Asymfreq;			// Frequency to calc asymmetry at
	private ParamBoolean FreqRangeBoo;		// Boolean whether or not asymmetry are calculated on a range of frequencies
	

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume AsymmetryMap;	// Estimate F0 shift map
	private ParamVolume CorrectedCESTMap;	// Corrected CEST curves


	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "CEST asymmetry mapping";
	private static final String longDescription = "Calculates the assymetry of the Z-spectrum at a given offset wrt water \n" +
			"Inputs are:\n" +
			"\t4-D volume containing the CEST dataset (including the unsaturated volume as the first volume (V0))\n" +
			"\t3-D volume resulting from the WASSR frequency offset module\n" +
			"\t3-D volume containing a mask of the brain (optional)\n" +
			"\t.txt file containing a list with the frequency offsets with respect to the water frequency.\n" +
			"\tthe frequency to determine asymmetry at, or check the checkbox to calculate the asymmetry at the frequencies from the list,\n" +
			"\t\t(besides the extreme values, due to B0 shifts).";
	
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("Kirby");
		inputParams.setCategory("Spectro");
		inputParams.setLabel("CEST Asymmetry");
		inputParams.setName("CEST Asymmetry");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com",""));
		info.setAffiliation("UMC Utrecht, dep of Radiology");
		info.setDescription(shortDescription+"\n"+longDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
//		info.add(new Citation(""));

		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(MTRVolume=new ParamVolume("MTR Data (4D)",null,-1,-1,-1,-1));
		inputParams.add(WASSRshiftmap=new ParamVolume("WASSR shift map (3D)",null,-1,-1,-1,1));
		inputParams.add(MaskVolume=new ParamVolume("Mask (3D)",null,-1,-1,-1,1));
		MaskVolume.setMandatory(false);
		
		inputParams.add(shiftlist=new ParamFile("List of Shifts (Hz)",new FileExtensionFilter(new String[]{"txt"})));
		inputParams.add(Asymfreq= new ParamFloat ("Asymmetry frequency (Hz)"));
		inputParams.add(FreqRangeBoo = new ParamBoolean("Calculate assymmetry on range of frequencies",false));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		AsymmetryMap = new ParamVolume("Estimated Asymmetry Map",VoxelType.FLOAT,-1,-1,-1,-1);
		AsymmetryMap.setName("AsymmetryMap");
		outputParams.add(AsymmetryMap);		
		CorrectedCESTMap = new ParamVolume("Corrected CEST curves",VoxelType.FLOAT,-1,-1,-1,-1);
		CorrectedCESTMap.setName("CorrCESTMap");
		outputParams.add(CorrectedCESTMap);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			this.setLabel("Loading Data into RAM");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/

			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageDataFloat scalarVol=new ImageDataFloat(MTRVolume.getImageData());
			ImageDataFloat wassrVol=new ImageDataFloat(WASSRshiftmap.getImageData());			
			int r=scalarVol.getRows(), c=scalarVol.getCols(), s=scalarVol.getSlices(), t = scalarVol.getComponents();
			int rw=wassrVol.getRows(), cw=wassrVol.getCols(), sw=wassrVol.getSlices();
			double asymfreq = Asymfreq.getFloat();
			double[]shiftArray;
			this.setTotalUnits(r);
			
			//Do mask related stuff...
			//boolean maskboo = false;
			int rm = 0, cm = 0, sm = 0;
			ImageData maskvol = MaskVolume.getImageData();
			if(maskvol!= null){
				//maskboo = true;
				rm = maskvol.getRows(); cm= maskvol.getCols(); sm =maskvol.getSlices();  
			}
			
			//TODO DaPo: implement list generation feature if deemed usefull or neccesary...
			if(false){//(shiftlist.getValue()== null){						// if no frequency list file is specified...
				//shiftArray = new Double[nfreqsteps.getInt()];
				//Float stepsize = new Float( 2.0*(foffset.getFloat()/nfreqsteps.getInt())); // calculate it
				//for(int i=0; i< nfreqsteps.getInt(); i++){
				//	shiftArray[i] = (double) -foffset.getFloat()+ stepsize * i;
				//}
					
			}else{													//else try to read it in...
				Vector<Boolean> ignore = new Vector<Boolean>();
				Vector<Double> shift = new Vector<Double>();
				int ignoreCnt=0;
				
				try{ //read in all values from shiftlistfile
					BufferedReader rdr = new BufferedReader(new FileReader(shiftlist.getValue()));
					String thisline = " ";
					thisline = rdr.readLine();
					while(thisline!=null && !thisline.isEmpty()){ 	//while there is stuff to read...
						Double val = Double.valueOf(thisline);		//get values,
						if(val==null)								// and clean								
							val = Double.NaN;
						if(val.isNaN() || val.isInfinite()) {		//ignore invalid values
							val=Double.NaN;
							ignore.add(true);
							ignoreCnt++;
						} else
							ignore.add(false);
						shift.add(val);								// add to list
						System.out.println("Shift: "+val);
						thisline = rdr.readLine();
					}
				}catch(IOException e){								// check fails
					JistLogger.logError(JistLogger.SEVERE, "Cannot parse input shift file");				
				}
				
				boolean []ignoreArray= new boolean[ignore.size()];
				shiftArray= new double[ignore.size()-ignoreCnt];
				int idx =0;
				for(int i=0;i<ignoreArray.length;i++) {
					ignoreArray[i] = ignore.get(i);
					if(!ignoreArray[i]) {
						shiftArray[idx]=shift.get(i);
						idx++;
					}
				}
			}
			
			// Check if MTR and WASSR dimensions match
			if ((r!=rw)||(c!=cw)|(s!=sw)){
				JistLogger.logError(JistLogger.SEVERE, "The dimensions of MTR volumes and WASSR map do not match, aborting.");
				return;
			}
			// Check if t and shiftArray.length are the same
			JistLogger.logError(JistLogger.INFO, "Number of MTW volumes (incl. non MTw): "+t+"\t Number of offset frequencies found in file: "+shiftArray.length );
			if (shiftArray.length != t-1){
				JistLogger.logError(JistLogger.SEVERE, "The number of MTW volumes ("+ (t-1) +") and shift values ("+ shiftArray.length +") do not match, aborting.");
				return;
			}
			// Check if Asymmetry offset is within range of frequencies in ShiftArray
			double max = -Double.MAX_VALUE;
			double min = Double.MAX_VALUE;
			for(int i=0;i<shiftArray.length;i++) {
				max = (max<shiftArray[i]?shiftArray[i]:max);
				min = (min>shiftArray[i]?shiftArray[i]:min);
			}		
			if ((!FreqRangeBoo.getValue())&&((asymfreq < min) | (asymfreq > max))){
				JistLogger.logError(JistLogger.SEVERE, "Assymmetry frequency not within range of listed frequencies, aborting.");
				return;
			}
			// Check if MTR and mask dimensions match
			if ((maskvol != null) &&((r!=rm)||(c!=cm)|(s!=sm))){
				JistLogger.logError(JistLogger.SEVERE, "The dimensions of MTR volumes and mask do not match, aborting.");
				return;
			}

			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/	
			ImageData Asymestimate = null;
			ImageData corr_cest = null;
			//if(CorrectedCESTMap.isConnected()){
				corr_cest = new ImageDataFloat(r,c,s,t-1);
				corr_cest.setName(scalarVol.getName()+"_f0corr");
				corr_cest.setHeader(scalarVol.getHeader());
			//}
			

			int asymN = 0;
			if(!FreqRangeBoo.getValue()){
				Asymestimate = new ImageDataFloat(r,c,s,1);
				Asymestimate.setName(scalarVol.getName()+"_Asymmetry_"+ Asymfreq+"Hz");
				Asymestimate.setHeader(scalarVol.getHeader());
				
			}else if (FreqRangeBoo.getValue()){
				asymN = (int) Math.floor(shiftArray.length/2);
				Asymestimate = new ImageDataFloat(r,c,s,asymN);
				Asymestimate.setName(scalarVol.getName()+"_Asymmetries_"+ DoubleArrayStat.min(shiftArray)+"-0Hz");
				
				ImageHeader asymheader = scalarVol.getHeader();
				
				MeasurementUnit[] Units = asymheader.getUnitsOfMeasure();
				Units[3] = MeasurementUnit.HZ;
				asymheader.setUnitsOfMeasure(Units);
				
				float[] res = asymheader.getDimResolutions();
				res[3] = (float) Math.abs(shiftArray[1]-shiftArray[0]);
				asymheader.setDimResolutions(res);
				
				float[] origin = asymheader.getOrigin();
				origin[3] = (float) shiftArray[0];
				asymheader.setOrigin(origin);
				
				Asymestimate.setHeader(asymheader);
			}
			


			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			//MSWASSR.Spline spline = new MSWASSR.Spline();			
			if(!FreqRangeBoo.getValue()){
				this.setLabel("Est. Asymmetry at "+asymfreq+"Hz");
			}else{
				this.setLabel("Est. Asymmetry at "+min+" - "+ max+"Hz");
			}
			
			
			double S_freqminus, S_freqplus;
			
			for(int i=0;i<r;i++) {											//Loop rows			
				this.setCompletedUnits(i);				
				for(int j=0;j<c;j++){										//Loop Collums
					for(int k=0;k<s;k++) {									//Loop Slices
						if((maskvol == null)||(maskvol.getFloat(i,j,k) > 0.0)){
							double []S_sat = new double[t-1];							//get vectors for all mtw images
							double []freqs = new double[t-1];
							
							for(int m=0;m<t-1;m++){
								S_sat[m]=(double)scalarVol.getFloat(i,j,k,m+1);
								freqs[m]= shiftArray[m]-wassrVol.getFloat(i, j, k);
							}
							double freqs_min = DoubleArrayStat.min(freqs);
							double freqs_max = DoubleArrayStat.max(freqs);
							
							Float baseline = new Float((S_sat[0]+S_sat[S_sat.length-1])/2); // average signal of two edges of sweep
							Float S0 = new Float(scalarVol.getFloat(i,j,k,0));
							if(baseline > S0)
								S0 = baseline;
													
	
							float asymm;
							Spline CESTcurve = new Spline(freqs,S_sat);
							
							if(!FreqRangeBoo.getValue()){   // calc asym at one value
								S_freqminus = CESTcurve.spline_value(-1.0*asymfreq);
								S_freqplus = CESTcurve.spline_value(asymfreq);
								asymm = (float)(S_freqminus-S_freqplus)/S0;
								if((asymm < -1.0) || (asymm > 1.0)) // scrub unphysical values.
									asymm = (float) 0.0;
								Asymestimate.set(i,j,k, asymm);
							} else if(FreqRangeBoo.getValue()){ //calc asymm at multiple values
								for(int n=0; n<asymN; n++ ){
									if((shiftArray[n] < freqs_min)||(shiftArray[shiftArray.length-(1+n)] > freqs_max)){
										//check if listed value is within actual measured frequency range, taking into account WASSR correction
										asymm = (float) 0.0;
									} else{
										S_freqminus = CESTcurve.spline_value(shiftArray[n]);
										S_freqplus = CESTcurve.spline_value(shiftArray[shiftArray.length-(1+n)]);
										asymm = new Float (S_freqminus-S_freqplus)/S0;
									}	
									if(asymm < -1.0)  // scrub unphysical values.
										asymm = (float) -1.0;
									if(asymm > 1.0)  // scrub unphysical values.
										asymm = (float) 1.0;
									Asymestimate.set(i,j,k,n, asymm);
								}
								//if(CorrectedCESTMap.isConnected()){
									for(int n=0; n<t-1; n++){
										corr_cest.set(i,j,k,n, CESTcurve.spline_value(shiftArray[n])/S0);
										
									}
								//}

							}
						}
					}
				}
			}


			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/

			AsymmetryMap.setValue(Asymestimate);
			//if(CorrectedCESTMap.isConnected()){
				CorrectedCESTMap.setValue(corr_cest);
			//}
		}
	}
}
