package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.BitSet;
import java.util.Comparator;
import java.util.TreeSet;

import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;

public class STAPLEmultiICM extends STAPLEmulti2{
	private Truth[][][] truth;	

	public STAPLEmultiICM() {
		super();
	}
	public STAPLEmultiICM(int[][][][] img) {
		super(img);
	}
	public STAPLEmultiICM(float[][][][] img) {
		super(img);
	}

	public void setTruth(int i, int j, int k, int[] inds, float[]t){
		truth[i][j][k]=new Truth(inds.length);
		for(int ii=0; ii<inds.length; ii++){
			truth[i][j][k].setIndex(ii, inds[ii]);
		}
		truth[i][j][k].setTruth(t);
	}
	public void setTruthIndex(int i, int j, int k, int loc, int val){
		truth[i][j][k].setIndex(loc,val);
	}

	public void setTruth(int i, int j, int k, float[] p){
		truth[i][j][k].setTruth(p);
	}
	public float[] getTruth(int i, int j, int k){
		return truth[i][j][k].getTruth();
	}
	public int[] getTruthInds(int i, int j, int k){
		return truth[i][j][k].getIndex();
	}
	public int getTruthLength(int i, int j, int k){
		return truth[i][j][k].getIndex().length;
	}
	
	public ImageData writeProb(int n){

		ImageData tfile = new ImageDataMipav("TruthEstimate_"+labels.get(n),rows,cols,slices);
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					float[] a=truth[i][j][k].getTruth();
					int[] ind=truth[i][j][k].getIndex();
					for(int m=0; m<a.length; m++) {
						if(labels.get(n)==ind[m]) {
							tfile.set(i,j,k,a[m]);
							break;
						}
					}
				}
			}
		}
		return tfile;
	}
	public float[][][] writeProbArray(int n){

		float[][][] out = new float[rows][cols][slices];
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					float[] a=truth[i][j][k].getTruth();
					int[] ind=truth[i][j][k].getIndex();
					for(int m=0; m<a.length; m++) {
						if(n==ind[m]) {
							out[i][j][k]=a[m];
							break;
						}
					}
				}
			}
		}
		return out;
	}
	public int[][][] writeTruthIndArray(){

		int[][][] out = new int[rows][cols][slices];
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] ind=truth[i][j][k].getIndex();
					out[i][j][k]=ind.length;
				}
			}
		}
		return out;
	}
	
	public ImageData getHardSeg(){
		ImageDataInt hardseg = new ImageDataInt(rows,cols,slices);
		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				for (int k=0; k<slices; k++) {
					hardseg.set(i,j,k, labels.get(truth[i][j][k].getHardInd()));
				}
			}			
		}
		return new ImageDataInt(hardseg);
	}
	
	public void start(){
		if(initType.equals("Performance")){
			EStep();
			mrfICM();
			mStepNext = true;
		}else{
			mStepNext = false;
		}
		iters = 0;
		normtrace = 0;
	}
	
	/**
	 * A version that iterates using the "smart" methods below
	 */
	public void iterate2(){
		start();
		keepgoing = true;
		while(keepgoing){
			keepgoing = proceed();
		}
	}
	
	/**
	 * Performs the next step of the STAPLE iterations.
	 * @return false if STAPLE has converged
	 */
	public boolean proceed(){
		if(mStepNext){
			if(keepgoing && iters<maxiters){
				
				MStep();
				mStepNext = false;
				
				float ntrace = pl.normalizedTrace();
				if (Float.isNaN(ntrace)) keepgoing=false;
				if(Math.abs(ntrace-normtrace)<eps){
					System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
					System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
					System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
					System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
					keepgoing=false;
				}else{
					System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
					System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
					System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
					System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
					System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
				}
				System.out.println("jist.plugins"+"\t"+"*****************");
				iters++;
				normtrace=ntrace;
				
				return keepgoing;
				
			}else{
				return false;
			}
		}else{
			EStep();
			mStepNext = true;
			mrfICM();
			return true;
		}		
	}
	
	private void createT(){
		t = new int[rows][cols][slices][raters];
		truth = new Truth[rows][cols][slices];
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					BitSet mindex=new BitSet(labelSize);
					for(int l=0; l<raters; l++){						
						t[i][j][k][l] = getIndex(labels,imagesArray[i][j][k][l]);
						mindex.set(labels.indexOf(imagesArray[i][j][k][l]),true);
						if (!(t[i][j][k][l]>-1))
							System.err.println("jist.plugins"+"Could not find label!");
					}
					truth[i][j][k] = new Truth(mindex.cardinality());
					int c=0;
					for(int m=0; m<labelSize; m++){
						if (mindex.get(m)) {
							truth[i][j][k].setIndex(c++,m);
						}
					}
				}
			}
		}
		imagesArray=null;
		imagesArrayFloat=null;
	}
	
	public void initialize(){
		
		if(initType.equals("Truth")){
			System.out.println("jist.plugins"+"\t"+"Not Implemented Yet");
		}else{
			System.out.println("jist.plugins"+"\t"+"Initializing Performance Levels");
			float init = 0.9999f;
			try{
				findLabels();
				System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
				pl = new PerformanceLevel(labels.size(), raters);
				pl.initialize(init);
				createT();
			}catch(Exception e){
				e.printStackTrace();
			}
			if(printPerformance){
				System.out.println("\n\n"+pl.toString()+"\n\n");
			}
		}
		System.out.println("jist.plugins"+"\t"+"*****************");
	}
	
	public void iterate(){		
		
		if(initType.equals("Performance")){
			EStep();
			mrfICM();
		}
		iters = 0;		
		float ntrace = 0;
		
		while(keepgoing && iters<maxiters){
			MStep();
			EStep();

			mrfICM();
			ntrace = pl.normalizedTrace();
			if (Float.isNaN(ntrace)) keepgoing=false;
			if(Math.abs(ntrace-normtrace)<eps){
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
			}else{
				System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
			}
			System.out.println("jist.plugins"+"\t"+"*****************");
			iters++;
			normtrace=ntrace;
		}
	}
	
	private void MStep() {
		float[] totsum = new float[labelSize];
		pl.clear();
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] mindex=truth[i][j][k].getIndex();
					float[] truthArray=truth[i][j][k].getTruth();
					for(int m=0; m<mindex.length; m++){
						float tr=truthArray[m];
						totsum[mindex[m]]+=tr;
						for(int l=0; l<raters; l++){
							pl.set(l, t[i][j][k][l], mindex[m], pl.get(l, t[i][j][k][l], mindex[m])+tr);
						}
					}
				}
			}
		}
		for(int n=0; n<labelSize; n++){
			pl.divideByTots(n, totsum[n]);
		}
		if(printPerformance){
			System.out.println("\n\n"+pl.toString()+"\n\n");
		}
	}
	
	private void EStep() {	
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] mindex=truth[i][j][k].getIndex();
					int lSize=mindex.length;
					float[] a = new float[lSize];
					for(int m=0; m<lSize; m++) {
						a[m]=priorArray[mindex[m]];
					}
					
					for(int m=0; m<lSize; m++){
						for(int l=0; l<raters; l++) {
							a[m]*=pl.get(l, t[i][j][k][l], mindex[m]);
						}
					}
					truth[i][j][k].setTruth(a);					
				}
			}
		}
	}
	
	/**
	 * Applies spatial consistency to the rater data in the form of an MRF.
	 * Optimization is performed with the ICM algorithm.
	 */
	private void mrfICM(){
		System.out.println("Apply MRF, ICM optimization...");

		//init variables 
		float[] problist = null;
		int numchanged = 1;
		GridPt[] nbrs = null;	//list of neighbors of the current voxel
		int n = 0;		
		TreeSet<int[]> calc = new TreeSet<int[]>(new arrayCompare());
		TreeSet<int[]> calc2 = new TreeSet<int[]>(new arrayCompare());
		
		while(n<maxMRFiters && numchanged>0){
			System.out.print("Iter "+n+" - ");
			numchanged = 0;

			if(n==0) {	// 1st iteration: set calc
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slices; k++){							
							Truth tVoxel=truth[i][j][k];
							problist=tVoxel.getProbTruth();
							if (problist!=null) {
								//add contributions from neighbors
								nbrs = getNeighbors(i,j,k);
								for(int l=0; l<nbrs.length; l++){
									if(nbrs[l]!=null){
										Truth nVoxel=truth[nbrs[l].x][nbrs[l].y][nbrs[l].z];
										int hNum=nVoxel.getHardInd();
										int nbrlabind = tVoxel.findIndexLoc(hNum);	
										if (nbrlabind!=-1)
											problist[nbrlabind]+=beta*nVoxel.getHardTruth();
									}

								}

								//check to see if hard seg has changed
								float maxprob = problist[0];
								for(int l=1; l<problist.length; l++){
									if(problist[l]>maxprob){
										tVoxel.setTruth(problist);
										numchanged++;
										for(int m=0; m<nbrs.length; m++) {
											if(nbrs[m]!=null){
												calc.add(new int[]{nbrs[m].x, nbrs[m].y, nbrs[m].z});
											}
										}
										break;
									}
								}
							}
						}
					}
				}
			}else {	//remaining iterations: iterate over voxels in calc only
				for(int[] voxel : calc) {
					int i=voxel[0];
					int j=voxel[1];
					int k=voxel[2];					
					Truth tVoxel=truth[i][j][k];
					problist=tVoxel.getProbTruth();
					if (problist!=null) {
						//add contributions from neighbors
						nbrs = getNeighbors(i,j,k);
						for(int l=0; l<nbrs.length; l++){
							if(nbrs[l]!=null){
								Truth nVoxel=truth[nbrs[l].x][nbrs[l].y][nbrs[l].z];
								int hNum=nVoxel.getHardInd();
								int nbrlabind = tVoxel.findIndexLoc(hNum);	
								if (nbrlabind!=-1)
									problist[nbrlabind]+=beta*nVoxel.getHardTruth();
							}

						}

						//check to see if hard seg has changed
						float maxprob = problist[0];
						for(int l=1; l<problist.length; l++){
							if(problist[l]>maxprob){
								tVoxel.setTruth(problist);
								numchanged++;
								for(int m=0; m<nbrs.length; m++) {
									if(nbrs[m]!=null){
										calc2.add(new int[]{nbrs[m].x, nbrs[m].y, nbrs[m].z});
									}
								}
								break;
							}
						}
					}
				}
				calc.clear();
				calc.addAll(calc2);
				calc2.clear();
			}
			System.out.println("Voxels changed: " +numchanged);
			n++;
		}
	}
	
	private class arrayCompare implements Comparator<int[]> {
		public int compare(int[] a, int[] b) {
			for (int i=0; i<a.length; i++) {
				if (a[i] < b[i])
					return -1;
				else if (a[i] > b[i])
					return 1;
			}
			return 0;
		}
	}
	
	private class Truth{
		private int[] index;
		private float[] lTruth;
		
		public Truth(int n){
			index=new int[n];
			lTruth=new float[n];
		}
		
		public int findIndexLoc(int n) {
			for (int i=0; i<index.length; i++) {
				if(index[i]==n)
					return i;
			}
			return -1;
		}

		public int[] getIndex() {
			return index;
		}
		
		public float[] getTruth() {
			return lTruth;
		}
		
		public float[] getProbTruth() {
			if(lTruth.length>1 && lTruth[0]<lTruth[1]+betaAll)
				return lTruth.clone();
			else
				return null;
		}
		
		public float getHardTruth() {
			return lTruth[0];
		}
		
		public int getHardInd() {
			return index[0];
		}
		
		public void setIndex(int loc, int n) {
			index[loc]=n;
		}
		
		public void setTruth(float t[]) {
			float sum = 0f;
			for(int l=0; l<t.length; l++){
				sum+=t[l];
			}
			if(sum==0){
				sum=Float.MIN_VALUE;
			}
			for(int l=0; l<t.length; l++){
				lTruth[l]=t[l]/sum;
			}
			sort();
		}
		
		private void sort() {
			for (int i=0; i<2; i++){
				int max = i;
				for (int j=i+1; j<lTruth.length; j++){
					if (lTruth[j]>lTruth[max])
						max = j;
				}
				if (i!=max){
					float swapT = lTruth[i];					
					lTruth[i] = lTruth[max];					
					lTruth[max] = swapT;
					int swapI = index[i];
					index[i] = index[max];
					index[max] = swapI;
				}
			}
		}
	}
}
