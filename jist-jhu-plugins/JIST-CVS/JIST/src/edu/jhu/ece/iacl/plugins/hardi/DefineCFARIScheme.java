package edu.jhu.ece.iacl.plugins.hardi;

import imaging.SchemeV1;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Vector;

import javax.vecmath.Point3f;

import Jama.Matrix;

import com.thoughtworks.xstream.XStream;

import edu.jhu.bme.smile.commons.math.Point3fMath;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIBasisFunction;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIBasisSet;
import edu.jhu.ece.iacl.algorithms.hardi.CFARIBasisTensor;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class DefineCFARIScheme extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamFile SchemeIn;				// .SchemeV1 file definition of a DWI experiement
	private ParamSurfaceCollection orientationBasis;			// set of orientations (i.e., a "shell")
	private ParamNumberCollection shellLambdaRadial;	// Diffusivity of tensors in a shell
	private ParamNumberCollection shellLambdaAxial; // Fractional anisotropy of each diffusion shell

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFile CFARISchemeOut;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Creates a scheme definition file.";
	private static final String longDescription = "The scheme file combines a mixture model of a tensor experiement with a diffusion experiment to create a set of basis functions.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Define CFARI Scheme");
		inputParams.setName("Define_CFARI_Scheme");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(SchemeIn=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"schemeV1"})));
		inputParams.add(orientationBasis=new ParamSurfaceCollection("Orientation (Angular) Basis (surface vextex set)"));
		inputParams.add(shellLambdaAxial=new ParamNumberCollection("Axial Diffusivity (mm2/s)",new Double(1e-9),new Double(1)));
		inputParams.add(shellLambdaRadial=new ParamNumberCollection("Radial Diffusivity (mm2/s)",new Double(1e-9),new Double(1)));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputParams.add(CFARISchemeOut=new ParamFile("Description of CFARI Basis Functions (.CFARI)",new FileExtensionFilter(new String[]{"CFARI"})));
	}


	protected void execute(CalculationMonitor monitor) {
		System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
		SchemeV1 DTIscheme = null;

		XStream xstream = new XStream();
		xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
		try {
			ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeIn.getValue()));
			DTIscheme=(SchemeV1)in.readObject();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		List<EmbeddedSurface>surf = orientationBasis.getSurfaceList();
		List<Number> modelLperp = shellLambdaRadial.getValue();
		List<Number> modelLpar = shellLambdaAxial.getValue();

		int numBasisFunctions = 0;
		for(EmbeddedSurface s : surf)
			numBasisFunctions += s.getVertexCount();

		int numDWMeasurements = DTIscheme.numDWMeasurements();
		Matrix SensingMatrix = new Matrix(numDWMeasurements,numBasisFunctions);
		Vector<CFARIBasisFunction> basisTensors = new Vector<CFARIBasisFunction>();
		int shell=0;
		int basisFcn=0;

		Matrix negbG = new Matrix(numDWMeasurements,6);
		int i = 0, j=0;
		while(i<numDWMeasurements) {
			double q = DTIscheme.getModQ(j);

			if(q>0 && !DTIscheme.isIgnored(j)) {
				double b = q*q*DTIscheme.getDiffusionTime(j)/1e6; // s/m2 -> s/mm2
				double []g=DTIscheme.getDir(j);
				negbG.set(i, 0, -b*g[0]*g[0]);//xx
				negbG.set(i, 1, -b*g[0]*g[1]*2);//xy
				negbG.set(i, 2, -b*g[0]*g[2]*2);//xz
				negbG.set(i, 3, -b*g[1]*g[1]);//yy
				negbG.set(i, 4, -b*g[1]*g[2]*2);//yz
				negbG.set(i, 5, -b*g[2]*g[2]);//zz
				i++;
			}
			j++;
		}

		for(EmbeddedSurface s : surf) { // for each shell
			double l2 = modelLperp.get(shell).doubleValue();
			double l1 = modelLpar.get(shell).doubleValue();
			Point3f [] pts = s.getVertexCopy();
			for(Point3f p : pts) {
				//: build D from p, l1, l2
				Matrix V = new Matrix(3,3);
				p=Point3fMath.normalizePoint(p);
				//Set the PEV
				V.set(0,0,p.x);
				V.set(1,0,p.y);
				V.set(2,0,p.z);
				Point3f r1 = Point3fMath.normalizePoint(new Point3f((float)Math.random(),(float)Math.random(),(float)Math.random()));
				Point3f v2 = Point3fMath.normalizePoint(Point3fMath.crossProductPoint(r1,p));
				V.set(0,1,v2.x);
				V.set(1,1,v2.y);
				V.set(2,1,v2.z);
				Point3f v3 = Point3fMath.normalizePoint(Point3fMath.crossProductPoint(p,v2));
				V.set(0,2,v3.x);
				V.set(1,2,v3.y);
				V.set(2,2,v3.z);
				Matrix diag = new Matrix(3,3);
				diag.set(0,0,l1);
				diag.set(1,1,l2);
				diag.set(2,2,l2);
				Matrix D3x3 = V.times(diag.times(V.transpose()));
				Matrix D = new Matrix(6,1);
				D.set(0,0,D3x3.get(0,0));
				D.set(1,0,D3x3.get(0,1));
				D.set(2,0,D3x3.get(0,2));
				D.set(3,0,D3x3.get(1,1));
				D.set(4,0,D3x3.get(1,2));
				D.set(5,0,D3x3.get(2,2));
				basisTensors.add(basisFcn,new CFARIBasisTensor(D));

				Matrix SigExp = negbG.times(D);
				//				SigAtten = S/S_ref = Exp(-b*G*D)
				for(int k=0;k<numDWMeasurements;k++)
					SensingMatrix.set(k, basisFcn, Math.exp(SigExp.get(k, 0)));
				basisFcn++;

				/*System.out.println(getClass().getCanonicalName()+"\t"+"p");
				System.out.println(getClass().getCanonicalName()+"\t"+p.x+" "+p.y+" "+p.z);
				System.out.println(getClass().getCanonicalName()+"\t"+"d");
				for(i=0;i<D.getRowDimension();i++) {
				for(j=0;j<D.getColumnDimension();j++)
					System.out.print(D.get(i,j)+" ");
				System.out.print("\n");
				}
				*/

			}
			shell++;
		}

		CFARIBasisSet basisSet = new CFARIBasisSet();
		basisSet.setDWScheme(DTIscheme);
		basisSet.setSensingMatrix(SensingMatrix);

		// Set tensor information
		basisSet.setBasisSet(basisTensors);


		// Write to a file in a type-agnostic manner
		String filename = getOutputDirectory().toString() + File.separatorChar + "TensorBasisSet_" + basisTensors.size() + ".CFARI";
		File outFileObj = new File(filename);
		xstream = new XStream();
		try {
			ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter(outFileObj));
			out.writeObject(basisSet);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		// Set output
		this.CFARISchemeOut.setValue(filename);

		// Write a second-debugging level matrix file for the sensing basis
		filename = getOutputDirectory().toString()
		+ File.separatorChar + "SensingMatrix_" + basisTensors.size() + ".mat";
		FileWriter fw;
		try {
			fw = new FileWriter(new File(filename));

			for( i=0;i<SensingMatrix.getRowDimension();i++) {
				for(j=0;j<SensingMatrix.getColumnDimension();j++) {
					fw.write(SensingMatrix.get(i,j)+" ");

				}
				fw.write("\n");
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		filename = getOutputDirectory().toString()
		+ File.separatorChar + "negbG_" + basisTensors.size() + ".mat";
		try {
			fw = new FileWriter(new File(filename));

			for( i=0;i<negbG.getRowDimension();i++) {
				for(j=0;j<negbG.getColumnDimension();j++) {
					fw.write(negbG.get(i,j)+" ");

				}
				fw.write("\n");
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		filename = getOutputDirectory().toString()
		+ File.separatorChar + "negbG_" + basisTensors.size() + ".mat";
		try {
			fw = new FileWriter(new File(filename));

			for( i=0;i<negbG.getRowDimension();i++) {
				for(j=0;j<negbG.getColumnDimension();j++) {
					fw.write(negbG.get(i,j)+" ");

				}
				fw.write("\n");
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		filename = getOutputDirectory().toString()
		+ File.separatorChar + "grad_" + basisTensors.size() + ".mat";
		try {
			fw = new FileWriter(new File(filename));

		j = 0;
		while(j<DTIscheme.numMeasurements()) {
			double q = DTIscheme.getModQ(j);

			if(q>0) {

				double []g=DTIscheme.getDir(j);

			}
			j++;
		}
		fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
