package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.vecmath.Point3d;

import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberStatistics;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;


public class MedicAlgorithmFiberVolumeStatistics extends ProcessingAlgorithm{
	//output params
	private ParamFileCollection avgstats;
	private ParamFileCollection medianstats;
	private ParamFileCollection sumstats;
	private ParamFileCollection maxstats;
	private ParamFileCollection minstats;

//	private ParamFileCollection connMtx;


	private ParamFileCollection fiberlengths;
	private ParamFileCollection fibercounts; 

	//input params
	private ParamFileCollection fibers;
	private ParamVolume volume;
	private ParamOption comparison;
	private ParamPointDouble resolution;

	private FiberCollectionReaderWriter fcrw = FiberCollectionReaderWriter.getInstance();

	private ArrayDoubleTxtReaderWriter arw = ArrayDoubleTxtReaderWriter.getInstance();

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fibers=new ParamFileCollection("Fibers", new FileExtensionFilter(new String[]{"dat"})));
		inputParams.add(volume=new ParamVolume("Volume",null,-1,-1,-1,-1));
		volume.setMandatory(false);
//		inputParams.add(comparison = new ParamOption("Connectivity Matrix Statistic",new String[]{"All", "Mean", "Max", "Min", "Median", "None"}));
//		comparison.setDescription("");


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Fiber Stats from Volume");
		inputParams.setLabel("Fiber_Stats_from_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fiberlengths = new ParamFileCollection("Fiber Lengths", new FileExtensionFilter(new String[]{"csv","txt"})));
        outputParams.add(fibercounts = new ParamFileCollection("Fiber Counts", new FileExtensionFilter(new String[]{"csv","txt"})));

		outputParams.add(avgstats = new ParamFileCollection("Mean Volume Data",  new FileExtensionFilter(new String[]{"csv","txt"})));
		avgstats.setMandatory(false);
		outputParams.add(medianstats = new ParamFileCollection("Median Volume Data",  new FileExtensionFilter(new String[]{"csv","txt"})));
		medianstats.setMandatory(false);
		outputParams.add(sumstats = new ParamFileCollection("Sum Volume Data",  new FileExtensionFilter(new String[]{"csv","txt"})));
		sumstats.setMandatory(false);
		outputParams.add(maxstats = new ParamFileCollection("Max Volume Data",  new FileExtensionFilter(new String[]{"csv","txt"})));
		maxstats.setMandatory(false);
		outputParams.add(minstats = new ParamFileCollection("Min Volume Data",  new FileExtensionFilter(new String[]{"csv","txt"})));
		minstats.setMandatory(false);

//		outputParams.add(connMtx = new ParamFileCollection("Connectivity Matrices",  new FileExtensionFilter(new String[]{"csv","txt"})));
//		connMtx.setMandatory(false);
	}


	protected void execute(CalculationMonitor monitor) {
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){
			e.printStackTrace();
		}


//		DecimalFormat dist = new DecimalFormat("###.#######");
		FiberStatistics fstat = new FiberStatistics();
		double[][] arraylens;
		double[][] count;
		for(File f : fibers.getValue()){
			FiberCollection allfibers = fcrw.read(f);
			fstat.setFibers(allfibers);
			fstat.computeLengths();
			if(fstat.getRes()==null && volume.getValue()!=null){
				fstat.setRes(volume.getImageData().getHeader().getDimResolutions()[0],
						volume.getImageData().getHeader().getDimResolutions()[1],
						volume.getImageData().getHeader().getDimResolutions()[2]);
			}
			ArrayList<Double> lens = fstat.getFiberLengths();
			arraylens = toArray(lens);
			lens=null;
			count = new double[][]{{allfibers.size()}};

			//write fiber lengths
			try{
			fiberlengths.add(
					arw.write(arraylens, new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_lengths.csv")));

            fibercounts.add(
                    arw.write(count, new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_count.csv")));
			}catch(IOException e){
				e.printStackTrace();
			}
			arraylens = null;

			if(volume.getValue()!=null){
				fstat.findTractVolumeStats(volume.getImageData());

				ArrayList<Double> avg = fstat.getMeanFromVolume();
				ArrayList<Double> sum = fstat.getSumFromVolume();
				ArrayList<Double> max = fstat.getMaxFromVolume();
				ArrayList<Double> min = fstat.getMaxFromVolume();
				ArrayList<Double> med = fstat.getMedianFromVolume();
				double[][] avgout = toArray(avg);
				avg=null;
				double[][] sumout = toArray(sum);
				sum=null;
				double[][] maxout = toArray(max);
				max=null;
				double[][] minout = toArray(min);
				min=null;
				double[][] medout = toArray(med);
				med=null;

				//write all others stats
				try{
					avgstats.add(arw.write(avgout, 
							new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_avgdata_"+volume.getImageData().getName()+".csv")));

					sumstats.add(arw.write(sumout, 
							new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_sumdata_"+volume.getImageData().getName()+".csv")));

					maxstats.add(arw.write(maxout, 
							new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_maxdata_"+volume.getImageData().getName()+".csv")));

					minstats.add(arw.write(minout, 
							new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_mindata_"+volume.getImageData().getName()+".csv")));

					medianstats.add(arw.write(medout, 
							new File(dir.getCanonicalFile()+File.separator+allfibers.getName()+"_mediandata_"+volume.getImageData().getName()+".csv")));

				}catch(IOException e){
					e.printStackTrace();
				}
				avgout = null;
				sumout = null;
				maxout = null;
				minout = null;
				medout = null;
				allfibers = null;
				System.gc();

			}
		}
	}


	private double[][] toArray(ArrayList<Double> in ){
		double[][] out = new double[in.size()][1];
		for(int i=0; i<in.size(); i++){
			out[i][0]=in.get(i);
		}
		return out;
	}
}
