package edu.jhu.ece.iacl.plugins.utilities.file;

import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;


public class MedicAlgorithmFibersToVtk extends ProcessingAlgorithm {
	//input params
	private ParamObject<FiberCollection> fibers;

	//ouputparams
	private ParamObject<CurveCollection> fiberLines;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts a FiberCollection to VTK format.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fibers=new ParamObject<FiberCollection>("Input Parameters",new FiberCollectionReaderWriter()));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Fibers to VTK");
		inputParams.setName("Fibers_to_VTK");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(fiberLines=new ParamObject<CurveCollection>("VTK Fibers", new CurveVtkReaderWriter()));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		fiberLines.setObject(fibers.getObject().toCurveCollection());
		fiberLines.setFileName(fibers.getObject().getName()+"_tovtk");
	}
}
