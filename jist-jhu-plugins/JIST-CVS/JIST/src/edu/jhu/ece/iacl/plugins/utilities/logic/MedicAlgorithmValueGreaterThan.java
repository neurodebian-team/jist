package edu.jhu.ece.iacl.plugins.utilities.logic;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;


public class MedicAlgorithmValueGreaterThan extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	protected ParamDouble lowerBound;
	protected ParamDouble value;
	protected ParamBoolean inclusive;
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	protected ParamBoolean assertTrue;

	/****************************************************
	 * CVS
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Determines whether an input ParamDouble value is greater than (or equal to, when the ParamBoolean inclusive is true) the ParamDouble value lowerBound.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Value Greater Than");
		inputParams.setName("Value_Greater_Than");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Creating inputs
		 ****************************************************/
		setRunningInSeparateProcess(false);
		inputParams.add(value=new ParamDouble("Value"));
		inputParams.add(lowerBound=new ParamDouble("Lower Bound"));
		inputParams.add(inclusive=new ParamBoolean("Include Bound",true));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Creating outputs
		 ****************************************************/
		outputParams.add(assertTrue=new ParamBoolean("Assertion"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		double val=value.getDouble(); //gets the double value of the ParamDouble value
		double lower=lowerBound.getDouble(); //gets the double value of the ParamDouble lowerBound
		if(inclusive.getValue()){ //if the ParamBoolean inclusive is true
			assertTrue.setValue((val>=lower)); //then the ParamBoolean assertTrue is set to true when val is greater than OR equal to lower
		} else { //if the ParamBoolean inclusive is false
			assertTrue.setValue((val>lower)); //then the ParamBoolean assertTrue is set to true when val is greater than lower
		}
	}
}
