package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader.AxisOrientation;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader.ImageOrientation;


/*
 * Sets the header information.
 * 
 * Only input values that are changed will be reflected in the header of the output
 * image.  I.e. If the default parameters are kept, then the output image will have the same 
 * header information as the input.  
 * 
 * @author John Bogovic
 *
 */
public class MedicAlgorithmSetHeaders extends ProcessingAlgorithm{
//	private ParamVolumeCollection in;
	private ParamVolumeCollection in;
	private ParamPointFloat res;
	private ParamPointFloat origin;
	private ParamOption imgorient;
	private ParamOption axorx;
	private ParamOption axory;
	private ParamOption axorz;

//	private ParamVolumeCollection out;
	private ParamVolumeCollection out;

	HashMap<String,String> axorMap;


	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Set the header information for an image.  Header information will" +
			"be preserved for fields with default values (e.g. If origin  = {-1 -1 -1}, then origin information will not" +
			"be changed).";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(in=new ParamVolumeCollection("Volumes in"));
		in.setLoadAndSaveOnValidate(false);
//		inputParams.add(in=new ParamVolumeCollection("Mult-Component Volume FileCollection",null,-1,-1,-1,-1));
		inputParams.add(origin=new ParamPointFloat("Origin", new Point3f(-1f,-1f,-1f)));
		inputParams.add(res=new ParamPointFloat("Resolution", new Point3f(-1f,-1f,-1f)));
		
		inputParams.add(imgorient = new ParamOption("Image Orientation", new String[]{"AXIAL","CORONAL","SAGITTAL","UNKNOWN"}));
		imgorient.setValue(3);
		String[] orientations = new String[]{"UNKNOWN","RIGHT-LEFT", "LEFT-RIGHT", "POSTERIOR-ANTERIOR", "ANTERIOR-POSTERIOR", "INFERIOR-SUPERIOR", "SUPERIOR-INFERIOR"};
		inputParams.add(axorx = new ParamOption("X Axis Orientation", orientations));
		inputParams.add(axory = new ParamOption("Y Axis Orientation", orientations));
		inputParams.add(axorz = new ParamOption("Z Axis Orientation", orientations));
		

		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Set Volume Header");
		inputParams.setName("Set_Volume_Header");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu","putter.ece.jhu.edu/John"));
		info.setWebsite("iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(out=new ParamVolumeCollection("Volumes out"));
		out.setLoadAndSaveOnValidate(false);
//		outputParams.add(out=new ParamVolumeCollection("Volumes Out",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) {
//		ArrayList<ImageData> filesout = new ArrayList<ImageData>();
		ArrayList<File> filesout = new ArrayList<File>();

//		//set the output directory and create it if it doesn't exist
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){ e.printStackTrace(); }

		setDefaults();

		List<ParamVolume> pvlist = in.getParamVolumeList();
		
		ImageHeader hdr; 
//		for(ImageData img: in.getImageDataList()){
		for(ParamVolume pv: pvlist){
			ImageData img = pv.getImageData();
			hdr = img.getHeader();

			//set the new header information
			//resolution
			Point3f respt = res.getValue();
			if(respt.x!=-1f && respt.y!=-1f && respt.z!=-1f){
				hdr.setDimResolutions(new float[]{respt.x,respt.y,respt.z});
			}
			//origin
			Point3f origpt = origin.getValue();
			if(origpt.x!=-1f && origpt.y!=-1f && origpt.z!=-1f){
				System.out.println(origpt.z);
				hdr.setOrigin(new float[]{origpt.x,origpt.y,origpt.z});
				System.out.println(hdr.getOrigin()[2]);
			}

			//axis orientations
			if(axorx.getIndex()>0 || axory.getIndex()>0 || axorz.getIndex()>0){
				AxisOrientation[] axisOrient = new AxisOrientation[3];
				//x
				if(axorx.getIndex()>0){ axisOrient[0]=AxisOrientation.valueOf(axorMap.get(axorx.getValue())); }
				else{ axisOrient[0]=hdr.getAxisOrientation()[0]; }
				//y
				if(axory.getIndex()>0){ axisOrient[1]=AxisOrientation.valueOf(axorMap.get(axory.getValue())); }
				else{ axisOrient[1]=hdr.getAxisOrientation()[1]; }
				//z
				if(axorz.getIndex()>0){ axisOrient[2]=AxisOrientation.valueOf(axorMap.get(axorz.getValue())); }
				else{ axisOrient[2]=hdr.getAxisOrientation()[2]; }
				
				hdr.setAxisOrientation(axisOrient);
			}

			//image orientation
			if(imgorient.getIndex()<3){
				hdr.setImageOrientation(ImageOrientation.valueOf(imgorient.getValue())); 
			}

			img.setHeader(hdr);

			System.out.println(getClass().getCanonicalName()+"\t"+img.getHeader());

			
			out.add(img);
			out.writeAndFreeNow(this);
			img.dispose();
			img=null;
			pv.dispose();
		}
	}


	private void setDefaults(){
		axorMap = new HashMap<String,String>();
		axorMap.put("UNKNOWN", "UNKNOWN");
		axorMap.put("RIGHT-LEFT", "R2L_TYPE");
		axorMap.put("LEFT-RIGHT", "L2R_TYPE");
		axorMap.put("POSTERIOR-ANTERIOR", "P2A_TYPE");
		axorMap.put("ANTERIOR-POSTERIOR", "A2P_TYPE");
		axorMap.put("INFERIOR-SUPERIOR", "I2S_TYPE");
		axorMap.put("SUPERIOR-INFERIOR", "S2I_TYPE");
	}
}
