package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;

/**
 * 
 * @author John Bogovic
 * @date 5/31/2008
 * 
 * Simultaneous Truth and Performance Level Estimation (STAPLE)
 * 
 * Warfield, Zou, and Wells, "Simultaneous Truth and Performace Level Estimation (STAPLE):
 * An Algorithm for the Validation of Image Segmentation," 
 * IEEE Trans. Medical Imaging vol. 23, no. 7, 2004
 */

public class STAPLE extends AbstractCalculation{
	
	int[][][][] imagesArray;
	List<ImageData> images;
	float[][][] truthImage;
	ImageDataFloat truth;
	
//	float[][][][][] pl; //performance level estimates
	protected float[][] pl; //performance level estimates
	protected float prior;
	protected ArrayList<Number> labels;
	protected float convergesum;
	protected int maxiters = 1000;
	protected String dir;
	
	protected float nbhdWeightIn, nbhdWeight = 0;
	protected float baseWeight = 1;
//	protected Connectivity conn = ConnectiviCty.SIX;
	protected int nbhdsize = 6;
	
	public STAPLE(){
		super();
		setLabel("STAPLE");
	}
	public STAPLE(int[][][][] img){
		super();
		setLabel("STAPLE");
		
		imagesArray=img;
		truthImage = new float[imagesArray.length][imagesArray[0].length][imagesArray[0][0].length];
		getPriorProb();
	}
	public STAPLE(String[] filenames,int[] dim){
		super();
		setLabel("STAPLE");
		
		imagesArray=StapleReader.readImgs(filenames, dim);
		truthImage = new float[imagesArray.length][imagesArray[0].length][imagesArray[0][0].length];
		getPriorProb();
	}
	public STAPLE(List<ImageData> img){
		super();
		setLabel("STAPLE");
		images = img;
		if(verifySizes()){
			truth = new ImageDataFloat(img.get(0).getRows(),img.get(0).getCols(),img.get(0).getSlices());
			getPriorProb();
		}else{
			System.err.println("jist.plugins"+"Rater images must have equal dimensions");
		}
	}
	
	public void setmaxIters(int max){
		maxiters=max;
	}
	/*
	public void setConnectivity(Connectivity c){
		this.conn=c;
		switch(conn){
		case SIX: nbhdsize=6;
		case EIGHTEEN: nbhdsize=18;
		case TWENTYSIX: nbhdsize=26;
		}
		setNeighborhoodWeight(nbhdWeightIn);
	}
	public void setConnectivity(int c){
		this.conn=Connectivity.values()[c];
		switch(conn){
		case SIX: nbhdsize=6;
		case EIGHTEEN: nbhdsize=18;
		case TWENTYSIX: nbhdsize=26;
		}
		setNeighborhoodWeight(nbhdWeightIn);
	}
	public void setNeighborhoodWeight(float f){
		nbhdWeightIn = f;
		switch (conn){
		case SIX: nbhdWeight = f/(1+(6*f)); baseWeight = 1-(6*nbhdWeight);
		case EIGHTEEN: nbhdWeight = f/(1+(18*f)); baseWeight = 1-(18*nbhdWeight);
		case TWENTYSIX: nbhdWeight = f/(1+(26*f)); baseWeight = 1-(26*nbhdWeight);
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"Weights normalized to: " + baseWeight + ", " + nbhdWeight);
	}
	*/
	public void setImages(int[][][][] img){
		imagesArray=img;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}
	
	public boolean verifySizes(){
		int rows = images.get(0).getRows();
		int cols = images.get(0).getCols();
		int slcs = images.get(0).getSlices();
		for(int i=1; i<images.size(); i++){
			if(images.get(i).getRows()!=rows || images.get(i).getCols()!=cols ||
			   images.get(i).getSlices()!=slcs){
				return false;
			}
		}
		return true;
	}
	
	public void findLabels(){
		labels = new ArrayList<Number>();
		if(imagesArray!=null){
			for(int i=0; i<imagesArray.length; i++){
				for(int j=0; j<imagesArray[0].length; j++){
					for(int k=0; k<imagesArray[0][0].length; k++){
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							if(!labels.contains(new Integer(imagesArray[i][j][k][l]))){
								labels.add(new Integer(imagesArray[i][j][k][l]));
							}
						}
					}
				}
			}
		}else if(images!=null){
			int rows = images.get(0).getRows();
			int cols = images.get(0).getCols();
			int slcs = images.get(0).getSlices();
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slcs; k++){
						for(int l=0; l<images.size(); l++){
							if(!labels.contains(new Integer(images.get(l).get(i,j,k).intValue()))){
								labels.add(new Integer(images.get(l).get(i,j,k).intValue()));
							}
						}
					}
				}
			}
		}else{
			System.err.println("jist.plugins"+"No data!");
		}
		
	}
	
	public void initializeSingle(){
		float init = 0.9999f;
		try{
			if(imagesArray!=null){
				pl=new float[imagesArray[0][0][0].length][2];
				for(int l=0; l<pl.length; l++){
					pl[l][0]=init;
					pl[l][1]=init;
				}
			}
			else if(images!=null){
				pl = new float[images.size()][2];
				for(int l=0; l<pl.length; l++){
					pl[l][0]=init;
					pl[l][1]=init;
				}
			}
			else{
				System.err.println("jist.plugins"+"Rater data is null");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void initialize(){
		float init = 0.9999f;
		try{
			findLabels();
			labels.trimToSize();
			System.out.println(getClass().getCanonicalName()+"\t"+"Labels Found: " + labels.size());
			pl = new float[images.size()][labels.size()];
			for(int l=0; l<pl.length; l++){
				pl[l][0]=init;
				pl[l][1]=init;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Estimate the true segmentation based on the rater segmentations
	 * and reliabilities using Equansions (14)-(16)
	 * 
	 */
	public void Estep(){
		if(imagesArray!=null){
			convergesum=0;
			for(int i=0; i<imagesArray.length; i++){
				for(int j=0; j<imagesArray[0].length; j++){
					for(int k=0; k<imagesArray[0][0].length; k++){
						float a = prior;
						float b = (1-prior);
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							//Compute 'a' (Eqn 14)
							//Compute 'b' (Eqn 15)
							if(imagesArray[i][j][k][l]==0){
								a=a*(1-pl[l][0]);
								b=b*(pl[l][1]);
							}else{
								a=a*(pl[l][0]);
								b=b*(1-pl[l][1]);
							}
						}
						//Compute weights for truth using Eqn (16)
						truthImage[i][j][k]=a/(a+b);
						convergesum=convergesum+truthImage[i][j][k];
					}
				}
			}
		}else if(images!=null){
			if(nbhdWeight==0){		//no MRF
				int rows = images.get(0).getRows();
				int cols = images.get(0).getCols();
				int slcs = images.get(0).getSlices();
				convergesum=0;
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slcs; k++){
							float a = prior;
							float b = (1-prior);
							for(int l=0; l<images.size(); l++){
								//Compute 'a' (Eqn 14)
								//Compute 'b' (Eqn 15)
								if(images.get(l).get(i, j, k).intValue()>0){
									a=a*(1-pl[l][0]);
									b=b*(pl[l][1]);
								}else{
									a=a*(pl[l][0]);
									b=b*(1-pl[l][1]);
								}
							}
							//Compute weights for truth using Eqn (16)
							truth.set(i, j, k, a/(a+b));
							convergesum=convergesum+truth.get(i, j, k).floatValue();
						}
					}
				}
			}else{					//MRF
				int rows = images.get(0).getRows();
				int cols = images.get(0).getCols();
				int slcs = images.get(0).getSlices();
				convergesum=0;
//				GridPt pt = new GridPt(0,0,0);
//				pt.setConnectivity(conn);
//				GridPt[] nbhd = new GridPt[nbhdsize];
				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slcs; k++){
							
//							pt.x=i; pt.y=j; pt.z=k;
//							nbhd = pt.neighborhood();
							
							float a = prior;
							float b = (1-prior);
							for(int l=0; l<images.size(); l++){
								//Compute 'a' (Eqn 14)
								//Compute 'b' (Eqn 15)
								if(images.get(l).get(i, j, k).intValue()>0){
									a=a*(1-pl[l][0]);
									b=b*(pl[l][1]);
								}else{
									a=a*(pl[l][0]);
									b=b*(1-pl[l][1]);
								}
							}

							//Compute weights for truth using Eqn (16)
							truth.set(i, j, k, a/(a+b));
							convergesum=convergesum+truth.get(i, j, k).floatValue();
						}
					}
				}
			}
			
		}
	}
	
	public void Mstep(){
		if(imagesArray!=null){
			//Compute performance parameters given the truth
			// using Eqns (18) & (19)
			for(int l=0; l<imagesArray[0][0][0].length; l++){
				float ptotsum = 0;
				float psum = 0;
				float qtotsum = 0;
				float qsum = 0;

				for(int i=0; i<imagesArray.length; i++){
					for(int j=0; j<imagesArray[0].length; j++){
						for(int k=0; k<imagesArray[0][0].length; k++){
							ptotsum=ptotsum+truthImage[i][j][k];
							qtotsum=qtotsum+(1-truthImage[i][j][k]);
							if(imagesArray[i][j][k][l]>0){
								psum=psum+truthImage[i][j][k];
							}else{
								qsum=qsum+(1-truthImage[i][j][k]);
							}
						}
					}
				}
				// Store performance parameter estimates for this iteration
				pl[l][0]=psum/ptotsum;
				pl[l][1]=qsum/qtotsum;
			}
		}else if(images!=null){  

			int rows = images.get(0).getRows();
			int cols = images.get(0).getCols();
			int slcs = images.get(0).getSlices();

			//			Compute performance parameters given the truth
			// using Eqns (18) & (19)
			for(int l=0; l<images.size(); l++){
				float ptotsum = 0;
				float psum = 0;
				float qtotsum = 0;
				float qsum = 0;

				for(int i=0; i<rows; i++){
					for(int j=0; j<cols; j++){
						for(int k=0; k<slcs; k++){
							ptotsum=ptotsum+truth.get(i,j,k).floatValue();
							qtotsum=qtotsum+(1-truth.get(i,j,k).floatValue());
							if(images.get(l).get(i, j, k).intValue()>0){
								psum=psum+truth.get(i,j,k).floatValue();
							}else{
								qsum=qsum+(1-truth.get(i,j,k).floatValue());
							}
						}
					}
				}
				// Store performance parameter estimates for this iteration
				pl[l][0]=psum/ptotsum;
				pl[l][1]=qsum/qtotsum;
			}
		}
	}
	
	
	
	public void getPriorProb(){
		if(imagesArray!=null){
			float total = imagesArray.length*imagesArray[0].length*imagesArray[0][0].length*imagesArray[0][0][0].length;
			int sum = 0;
			for(int i=0; i<imagesArray.length; i++){
				for(int j=0; j<imagesArray[0].length; j++){
					for(int k=0; k<imagesArray[0][0].length; k++){
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							if(imagesArray[i][j][k][l]>0){
								sum++;
							}
						}
					}
				}
			}
//			System.out.println(getClass().getCanonicalName()+"\t"+"Sum: " + sum);
//			System.out.println(getClass().getCanonicalName()+"\t"+"Total" + total);
			prior=((float)sum)/total;
			System.out.println(getClass().getCanonicalName()+"\t"+"Prior Prob: " + prior);
		}else if(images!=null){
			int rows = images.get(0).getRows();
			int cols = images.get(0).getCols();
			int slcs = images.get(0).getSlices();
			float total = images.size()*rows*cols*slcs;
			int sum = 0;
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slcs; k++){
						for(int l=0; l<images.size(); l++){
							if(images.get(l).get(i, j, k).intValue()>0){
								sum++;
							}
						}
					}
				}
			}
//			System.out.println(getClass().getCanonicalName()+"\t"+"Sum: " + sum);
//			System.out.println(getClass().getCanonicalName()+"\t"+"Total" + total);
			prior=((float)sum)/total;
			System.out.println(getClass().getCanonicalName()+"\t"+"Prior Prob: " + prior);
		}else{
			System.err.println("jist.plugins"+"Rater data is null");
		}
	}
	
	public void iterate(){
		initialize();
		Estep();
		float prevcs = convergesum;
		float eps = 0.00000001f;
		int iters = 0;	
		
		boolean keepgoing = true;
		while(keepgoing && iters<maxiters){
//			System.out.println(getClass().getCanonicalName()+"\t"+"Iteration: " +iters);
			Mstep();
			Estep();
			if(Math.abs(prevcs-convergesum)<eps){
				System.out.println(getClass().getCanonicalName()+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
				printPerformanceLevels();
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"Iteration: " +iters);
			System.out.println(getClass().getCanonicalName()+"\t"+"*****************");
			
			iters++;
			
//			String iout = dir + "Iter"+iters+".raw";
//			System.out.println(getClass().getCanonicalName()+"\t"+"Writing to: " +iout);
//			RawWriter.writeImgFloat(truth, iout);
			
			prevcs = convergesum;
		}
		
	}
	
	public void printPerformanceLevels(){
		for(int i=0; i<pl.length; i++){
			System.out.println(getClass().getCanonicalName()+"\t"+"Rater " + (i+1) + " \t" + pl[i][0] + "\t" + pl[i][1]);
		}
	}
	public String getPerformanceLevels(){
		String perf = "";
		for(int i=0; i<pl.length; i++){
			perf=perf+"Rater " + (i+1) + " \t" + pl[i][0] + "\t" + pl[i][1]+"\n";
		}
		System.out.println(getClass().getCanonicalName()+"\t"+perf);
		return perf;
	}
		
	
	public void printPerformanceLevels2(){
		System.out.println(getClass().getCanonicalName()+"\t"+"*******************");
		for(int i=0; i<pl.length; i++){
			System.out.println(getClass().getCanonicalName()+"\t"+"Rater " + i);
			System.out.println(getClass().getCanonicalName()+"\t"+"True Postitive Rate: " + pl[i][0]);
			System.out.println(getClass().getCanonicalName()+"\t"+"True Negative Rate: " + pl[i][1]);
			System.out.println(getClass().getCanonicalName()+"\t"+"*******************");
		}
		
	}
	
	public float[][][] getTruthImage(){
		return truthImage;
	}
	
	public ImageDataFloat getTruth(){
		return truth;
	}

	public float[][] getPeformanceLevel(){
		return pl;
	}
	

}
