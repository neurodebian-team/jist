/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


import java.util.ArrayList;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmExtractVolumeComponent extends ProcessingAlgorithm{
	ParamInteger stIndex;
	ParamInteger endIndex;
	ParamString range;
	ParamBoolean split;
	ParamVolume inVol;
	ParamVolume outVol;
	ParamVolumeCollection splitVols;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inVol=new ParamVolume("Mult-Component Volume",null,-1,-1,-1,-1));
		inputParams.add(stIndex=new ParamInteger("Start Index",0,1000000,0));
		inputParams.add(endIndex=new ParamInteger("End Index",0,1000000,0));
		inputParams.add(split = new ParamBoolean("Split output volumes?"));
		split.setValue(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Extract Volume Component");
		inputParams.setName("Extract_Volume_Component");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Extracted Volume",null,-1,-1,-1,-1));
		outVol.setMandatory(false);
		outputParams.add(splitVols=new ParamVolumeCollection("Extracted and Split Volumes",null,-1,-1,-1,-1));
		splitVols.setMandatory(false);
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		JistLogger.logOutput(JistLogger.FINE, "Starting MedicAlgorithmExtractVolumeComponent");
		ImageData vol=inVol.getImageData();
		ImageData cvol = null;
		JistLogger.logOutput(JistLogger.FINE, "Data loaded");
//		ImageDataMipav cvol= new ImageDataMipav("nothing",VoxelType.BOOLEAN,1,1,1,1);
		ArrayList<ImageData> splitlist = new ArrayList<ImageData>();
		if(split.getValue()){
			JistLogger.logOutput(JistLogger.FINE, "Performing split");
			int st=stIndex.getInt();
			int end=endIndex.getInt();
			if(end>=vol.getComponents()) {
				JistLogger.logError(JistLogger.WARNING, "Selected End Index of "+end+" where only "+vol.getComponents()+" components exist. Using maximum.");
				end = vol.getComponents()-1;
			}
			splitlist = new ArrayList<ImageData>();
			for(int l=st; l<=end; l++){ /* made inclusive - 2010.03.03 bl */
				ImageDataMipav tvol=new ImageDataMipav(vol.getName()+"_"+l,vol.getType(),vol.getRows(),vol.getCols(),vol.getSlices(),1);
				for(int i=0;i<vol.getRows();i++){
					for(int j=0;j<vol.getCols();j++){
						for(int k=0;k<vol.getSlices();k++){
							tvol.set(i,j,k,0,vol.getDouble(i, j, k, l));
						}
					}
				}

				
				tvol.setHeader(vol.getHeader());

				splitlist.add(tvol);
			}
			splitVols.setValue(splitlist);
		}else{
			JistLogger.logOutput(JistLogger.FINE, "Running standard");
			int st=stIndex.getInt();
			int end=endIndex.getInt();
			if(end>=vol.getComponents()) {
				JistLogger.logError(JistLogger.WARNING, "Selected End Index of "+end+" where only "+vol.getComponents()+" components exist. Using maximum.");
				end = vol.getComponents()-1;
			}


			cvol=new ImageDataMipav(vol.getName()+"_"+st+"-"+end,vol.getType(),vol.getRows(),vol.getCols(),vol.getSlices(),end-st+1);
			JistLogger.logOutput(JistLogger.FINE, "New volume allocated");
			for(int i=0;i<vol.getRows();i++){
				for(int j=0;j<vol.getCols();j++){
					for(int k=0;k<vol.getSlices();k++){
						for(int l=st;l<=end;l++){
							cvol.set(i,j,k,l-st,vol.getDouble(i, j, k, l));
						}
					}
				}
			}
			JistLogger.logOutput(JistLogger.FINE, "Data copied");
			
			cvol.setHeader(vol.getHeader());
			JistLogger.logOutput(JistLogger.FINE, "Header copied");
			outVol.setValue(cvol);
		}
		JistLogger.logOutput(JistLogger.FINE, "Done");
	}
}
