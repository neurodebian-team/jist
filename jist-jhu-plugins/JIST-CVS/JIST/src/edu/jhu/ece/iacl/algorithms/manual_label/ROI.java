package edu.jhu.ece.iacl.algorithms.manual_label;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import javax.vecmath.Point2i;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.vector.Vector3;

public class ROI extends LabelImage{
	
	ImageData roiVolume, maskedVolume;
	Vector3[] roiVecs;
	Number[] roiNums;
	String dataout;
	HashMap<Point2i,FiberCollection> connsToFibers;
	HashMap<Point2i, HashSet<GridPt>> connsToPts;
	
	public ROI(){
		super();
	}
	public ROI(ImageData img){
		super(img);
	}
	public ROI(ImageData img, ImageData testimg){
		super(img);
		setTestVolume(testimg);
	}
	
	public void setTestVolume(ImageData vol){
		roiVolume = vol;
	}
	public ImageData getTestVolume(){
		return roiVolume;
	}
	public HashMap<Point2i, FiberCollection> getConnectionFibers(){
		return connsToFibers;
	}
	public HashMap<Point2i, HashSet<GridPt>> getConnectionPoints(){
		return connsToPts;
	}
	public Number[] applyROINum(int label){
		if(roiVolume==null){
			System.err.println("jist.plugins"+"testVolume is null");
		}
		if(img==null){
			System.err.println("jist.plugins"+"labelimg is null");
		}

		GridPt[] labellocs = this.getlabsetArray(label);
		Number[] vals = new Number[labellocs.length];
		for(int i=0; i<labellocs.length; i++){
			vals[i]=roiVolume.get(labellocs[i].x,labellocs[i].y,labellocs[i].z);
		}
		return vals;
	}
	
	public Vector3[] getROIdata(int label){
		if(roiVolume==null){
			System.err.println("jist.plugins"+"testVolume is null");
		}
		if(img==null){
			System.err.println("jist.plugins"+"labelimg is null");
		}

		GridPt[] labellocs = this.getlabsetArray(label);
		Vector3[] vals = new Vector3[labellocs.length];
		for(int i=0; i<labellocs.length; i++){
//			float x = testVolume.get(labellocs[i].x, labellocs[i].y, labellocs[i].z,0).floatValue();
//			float y = testVolume.get(labellocs[i].x, labellocs[i].y, labellocs[i].z,1).floatValue();
//			float z = testVolume.get(labellocs[i].x, labellocs[i].y, labellocs[i].z,2).floatValue();
			
			float x = roiVolume.get(0,labellocs[i].x, labellocs[i].y, labellocs[i].z).floatValue();
			float y = roiVolume.get(1,labellocs[i].x, labellocs[i].y, labellocs[i].z).floatValue();
			float z = roiVolume.get(2,labellocs[i].x, labellocs[i].y, labellocs[i].z).floatValue();
			
			vals[i] = new Vector3(x,y,z);
		}
		
		//create String representative of this data
		dataout = "";
		for(int i=0; i<vals.length; i++){
			dataout = dataout	+ vals[i].getX().floatValue()+"\t"
								+ vals[i].getY().floatValue()+"\t"
								+ vals[i].getZ().floatValue()+"\n";
		}
		
		return vals;
	}
	
	public void computeMask(int label){
		if(roiVolume.getRows()==3){
			maskedVolume=getSubsetDTISformat(roiVolume,img, new Integer(label));
		}else if(roiVolume.getComponents()==3){
			maskedVolume=getSubsetMIPAVformat(roiVolume,img, new Integer(label));
		}else{
			System.err.println("jist.plugins"+"Invalid vector image!");
		}
	}

	public ImageData getSubsetDTISformat(ImageData vec, ImageData mask, Number target){

		int xn = vec.getRows();
		int yn = vec.getCols();
		int zn = vec.getSlices();
		int cn = vec.getComponents();
		ImageData f=vec.mimic();
//		System.out.println(getClass().getCanonicalName()+"\t"+"Original");
		for(int i=0;i<cn;i++){
			for(int j=0;j<zn;j++){
				for(int k=0;k<yn;k++){
					if(mask.get(i,j,k).byteValue()==target.byteValue()){
						for(int l=0;l<xn;l++){
							f.set(i, j, k, l, vec.getDouble(i, j, k, l));
						}
					}
				}
			}
		}
		return f;
	}
	
	public ImageData getSubsetMIPAVformat(ImageData vec, ImageData mask, Number target){
		System.out.println(getClass().getCanonicalName()+"\t"+"Applying Mask...");
		int xn = vec.getRows();
		int yn = vec.getCols();
		int zn = vec.getSlices();
		int cn = vec.getComponents();
		System.out.println(getClass().getCanonicalName()+"\t"+"Vec Size: " + xn + "," + yn + "," + zn + "," + cn);
		ImageData f=vec.mimic();
		for(int i=0;i<xn;i++){
			for(int j=0;j<yn;j++){
				for(int k=0;k<zn;k++){
					if(mask.get(i,j,k).byteValue()==target.byteValue()){
						for(int l=0;l<cn;l++){
							f.set(i, j, k, l, vec.getDouble(i, j, k, l));
						}
					}
				}
			}
		}
		return f;
	}
	
	
	/**
	 * 
	 * A method to parcellate fibers based to DTIStudio like regions of interest ROIs
	 * 
	 * @param tractin	The fibercollection containing the fibers to be chosen
	 * @param op		The operation to be applied 0-OR, 1-NOT
	 * @param label		The label of interest.  (-1 indicates all labels >0 
	 * @return			A FiberCollection containing the chosen fibers
	 * @author 			John Bogovic, bogovic@jhu.edu	
	 */
	public FiberCollection fiberroi(FiberCollection tractin, int op, int label){
		
		FiberCollection tractout = new FiberCollection();
		String name ="";
		if(op==0){
			if(label == -1){
				name = tractin.getName()+"_OR_all";
			}else{
				name = tractin.getName()+"_OR_"+label;
			}
		}else{
			if(label == -1){
				name = tractin.getName()+"_NOT_all";
			}else{
				name = tractin.getName()+"_NOT_"+label;
			}
		}
		
		
		
//		for(Fiber f : tractin){
//			for(XYZ pt : f.getXYZChain()){
//				GridPt p = new GridPt(pt, 0);
//				if(roiVolume!=null){
//					tractout.setDimensions(new Point3i(roiVolume.getRows(), roiVolume.getCols(), roiVolume.getSlices()));
//					tractout.setResolutions(new Point3f(roiVolume.getHeader().getDimResolutions()[0],
//							roiVolume.getHeader().getDimResolutions()[1],
//							roiVolume.getHeader().getDimResolutions()[2]));
//					if(op==0){
//						if(label==-1){
//							if(roiVolume.getDouble(p.x, p.y, p.z)>0){
//								tractout.add(f);
//								break;
//							}
//						}else{
//							if(roiVolume.getInt(p.x, p.y, p.z)==label){
//								tractout.add(f);
//								break;
//							}
//						}
//					}else{
//						if(label==-1){
//							if(roiVolume.getDouble(p.x, p.y, p.z)==0){
//								tractout.add(f);
//								break;
//							}
//						}else{
//							if(roiVolume.getInt(p.x, p.y, p.z)!=label){
//								tractout.add(f);
//								break;
//							}
//						}
//					}
//				}else{
//					tractout.setDimensions(new Point3i(this.getImg().getRows(), this.getImg().getCols(), this.getImg().getSlices()));
//					tractout.setResolutions(new Point3f(this.getImg().getHeader().getDimResolutions()[0],
//							this.getImg().getHeader().getDimResolutions()[1],
//							this.getImg().getHeader().getDimResolutions()[2]));
//					if(op==0){
//						if(label==-1){
//							if(this.getImg().getDouble(p.x, p.y, p.z)>0){
//								tractout.add(f);
//								break;
//							}
//						}else{
//							if(this.getImg().getInt(p.x, p.y, p.z)==label){
//								tractout.add(f);
//								break;
//							}
//						}
//					}else{
//						if(label==-1){
//							if(this.getImg().getDouble(p.x, p.y, p.z)==0){
//								tractout.add(f);
//								break;
//							}
//						}else{
//							if(this.getImg().getInt(p.x, p.y, p.z)!=label){
//								tractout.add(f);
//								break;
//							}
//						}
//					}
//				}
//			}
//		}
		
		if(roiVolume!=null){
			tractout.setDimensions(new Point3i(roiVolume.getRows(), roiVolume.getCols(), roiVolume.getSlices()));
			tractout.setResolutions(new Point3f(roiVolume.getHeader().getDimResolutions()[0],
					roiVolume.getHeader().getDimResolutions()[1],
					roiVolume.getHeader().getDimResolutions()[2]));
			
			for(Fiber f : tractin){
				for(XYZ pt : f.getXYZChain()){
//					GridPt p = new GridPt(pt,0);
					
					GridPt p = new GridPt((int)Math.floor(pt.x),(int)Math.floor(pt.y),(int)Math.floor(pt.z), 0);
//					GridPt p = new GridPt((int)Math.ceil(pt.x),(int)Math.ceil(pt.y),(int)Math.ceil(pt.z), 0);
					
					if(op==0){
						if(label==-1){
							if(roiVolume.getDouble(p.x, p.y, p.z)>0){
								tractout.add(f);
								break;
							}
						}else{
							if(roiVolume.getInt(p.x, p.y, p.z)==label){
								tractout.add(f);
								break;
							}
						}
					}else{
						if(label==-1){
							if(roiVolume.getDouble(p.x, p.y, p.z)==0){
								tractout.add(f);
								break;
							}
						}else{
							if(roiVolume.getInt(p.x, p.y, p.z)!=label){
								tractout.add(f);
								break;
							}
						}
					}
				}
			}
		}else{
//			System.out.println("\n\n*****FLOOR*****\n\n");
//			System.out.println("\n\n*****CEIL*****\n\n");
			tractout.setDimensions(new Point3i(this.getImg().getRows(), this.getImg().getCols(), this.getImg().getSlices()));
			tractout.setResolutions(new Point3f(this.getImg().getHeader().getDimResolutions()[0],
					this.getImg().getHeader().getDimResolutions()[1],
					this.getImg().getHeader().getDimResolutions()[2]));
			int[] inbndpt;
			for(Fiber f : tractin){
				for(XYZ pt : f.getXYZChain()){
					inbndpt = GridPt.roundInBounds(pt.x,pt.y,pt.z,rows,cols,slcs);
//					GridPt p = new GridPt(pt,0);
//					p = p.roundInBounds(pt.x,pt.y,pt.z);
//					GridPt p = new GridPt((int)Math.floor(pt.x),(int)Math.floor(pt.y),(int)Math.floor(pt.z), 0);
//					GridPt p = new GridPt((int)Math.ceil(pt.x),(int)Math.ceil(pt.y),(int)Math.ceil(pt.z), 0);
					int x = inbndpt[0];
					int y = inbndpt[1];
					int z = inbndpt[2];
					
					try{
					if(op==0){
						if(label==-1){
							if(this.getImg().getDouble(x, y, z)>0){
								tractout.add(f);
								break;
							}
						}else{
							if(this.getImg().getInt(x, y, z)==label){
								tractout.add(f);
								break;
							}
						}
					}else{
						if(label==-1){
							if(this.getImg().getDouble(x, y, z)==0){
								tractout.add(f);
								break;
							}
						}else{
							if(this.getImg().getInt(x, y, z)!=label){
								tractout.add(f);
								break;
							}
						}
					}
					}catch(Exception e){
						System.out.println("x: "+x);
						System.out.println("y: "+y);
						System.out.println("z: "+z);
					}
				}
			}
		}
		
		tractout.setName(name);
		return tractout;
	}
	
	public synchronized HashMap<Point2i, FiberCollection> pairFiberRoi(FiberCollection f){
//		ArrayList<FiberCollection> allFiberPairs = new ArrayList<FiberCollection>();
		
		if(this.allhits==null){
			getLabelList();
		}
		
		connsToFibers = new HashMap<Point2i, FiberCollection>();
		connsToPts = new HashMap<Point2i, HashSet<GridPt>>();
//		tractout.setDimensions(new Point3i(roiVolume.getRows(), roiVolume.getCols(), roiVolume.getSlices()));
//		tractout.setResolutions(new Point3f(roiVolume.getHeader().getDimResolutions()[0],
//				roiVolume.getHeader().getDimResolutions()[1],
//				roiVolume.getHeader().getDimResolutions()[2]));
		int N = f.size();
		
		ArrayList<Point2i> pair = new ArrayList<Point2i>();	//label pair
		ArrayList<Integer> labels = new ArrayList<Integer>();
		HashSet<GridPt> points = new HashSet<GridPt>();
		for(int i=0; i<N; i++){
			
			pair.clear();
			labels.clear();
			points.clear();
			
			//assign the fiber to the appropriate label pair
			
			for(XYZ x : f.get(i).getXYZChain()){
				GridPt pt = new GridPt(x);
				if(!labels.contains(this.img.getInt(pt.x,pt.y, pt.z)) && 
						this.img.getInt(pt.x,pt.y, pt.z)>0){
					labels.add(this.img.getInt(pt.x,pt.y, pt.z));
				}
				//add this point if its not part of the path already
				points.add(pt);
				
			}
			Collections.sort(labels);
			//create all possible pairs
			for(int n1=0; n1<labels.size(); n1++){
				for(int n2=n1+1; n2<labels.size(); n2++){
					pair.add(new Point2i(labels.get(n1).intValue(),labels.get(n2).intValue()));
//					System.out.println("Added: " + new Point2i(n1,n2));
				}
			}
//			System.out.println("\n\n\n");
			//add the fiber to the fiber collection corresponding to that connections
			for(Point2i p: pair){
				if(!connsToFibers.containsKey(p)){
					FiberCollection fc = new FiberCollection();
					fc.setDimensions(new Point3i(this.getImg().getRows(), this.getImg().getCols(), this.getImg().getSlices()));
					fc.setResolutions(new Point3f(this.getImg().getHeader().getDimResolutions()[0],
							this.getImg().getHeader().getDimResolutions()[1],
							this.getImg().getHeader().getDimResolutions()[2]));
					fc.add(f.get(i));
					fc.setName("Fiber_connection_"+p.x+"_"+p.y);
					connsToFibers.put(p, fc);
					
					connsToPts.put(p,(HashSet<GridPt>)points.clone());
//					System.out.println("Added connection: "+p.x+"_"+p.y);
//					System.out.println("Num fibers: " + connsToFibers.get(p).size());
				}else{
					connsToFibers.get(p).add(f.get(i));
					connsToPts.get(p).addAll(points);
//					System.out.println("Num fibers for " + p +": " + connsToFibers.get(p).size());
//					System.out.println("Num fibers for " + p + ": " + connsToPts.get(p).size());
				}
			}
		}
		System.out.println(connsToFibers.keySet());
		return connsToFibers;
	}
	

	public HashMap<Point2i, FiberCollection> pairFiberRoiSplit(FiberCollection f){
		
		System.out.println("Splitting Fibers");
		if(this.allhits==null){
			getLabelList();
		}
		//initialize the output
		connsToFibers = new HashMap<Point2i, FiberCollection>();
		connsToPts = new HashMap<Point2i, HashSet<GridPt>>();
		
		int N = f.size();
		
		ArrayList<Point2i> pair = new ArrayList<Point2i>();	//label pairs represented by each fiber
		ArrayList<Point2i> pairflip = new ArrayList<Point2i>();	//label pairs represented by each fiber
		HashSet<GridPt> points = new HashSet<GridPt>();
		ArrayList<Integer> labels = new ArrayList<Integer>();
		boolean hitspace = false;

		int lastlabel = -1;
		int curlabel = 0;
		for(int i=0; i<N; i++){
//		for(int i=0; i<1; i++){
			//reset parameters for the next fiber
//			System.out.println("\n\n\nFiber: " + i);
			pair.clear();
			pairflip.clear();
			points.clear();
			labels.clear();
			lastlabel = -1;
			curlabel = 0;
			hitspace = false;
			
			for(int j=0; j<f.get(i).getXYZChain().length; j++){
//				int[] labelArray = new int[f.get(i).getChain().length];
//				GridPt pt = new GridPt(f.get(i).getXYZChain()[j]);
				GridPt pt = new GridPt((int)f.get(i).getXYZChain()[j].x,
						(int)f.get(i).getXYZChain()[j].y,
						(int)f.get(i).getXYZChain()[j].z);
				curlabel = this.img.getInt(pt.x,pt.y, pt.z);
//				System.out.println(curlabel);
				labels.add(curlabel);
				if(curlabel>0 && lastlabel==-1){
					lastlabel=curlabel;
				}else if(curlabel>0 && lastlabel>0 && hitspace){
					pair.add(new Point2i(lastlabel,curlabel));
					if(lastlabel<curlabel){
						pairflip.add(new Point2i(lastlabel,curlabel));
					}else{
						pairflip.add(new Point2i(curlabel,lastlabel));
					}
						
					if(lastlabel!=curlabel){
						//add the label pairs to the output maps
						if(lastlabel<curlabel){
							Point2i addme = new Point2i(lastlabel,curlabel);
							if(!connsToFibers.keySet().contains(addme)){
								System.out.println("Adding: " + addme);
								connsToFibers.put(addme,new FiberCollection());
								connsToFibers.get(addme).setName("FibersConnecting_"+addme.x+"-"+addme.y);
								connsToFibers.get(addme).setDimensions(
										new Point3i(img.getRows(),img.getCols(),img.getSlices()));
								connsToFibers.get(addme).setResolutions(
										new Point3f(img.getHeader().getDimResolutions()[0],
													img.getHeader().getDimResolutions()[1],
													img.getHeader().getDimResolutions()[2]));
								connsToPts.put(addme,new HashSet<GridPt>());
							}
						}else{
							Point2i addme = new Point2i(curlabel,lastlabel);
							if(!connsToFibers.keySet().contains(addme)){
								System.out.println("Adding: " + addme);
								connsToFibers.put(addme,new FiberCollection());
								connsToFibers.get(addme).setName("FibersConnecting_"+addme.x+"-"+addme.y);
								connsToFibers.get(addme).setDimensions(
										new Point3i(img.getRows(),img.getCols(),img.getSlices()));
								connsToFibers.get(addme).setResolutions(
										new Point3f(img.getHeader().getDimResolutions()[0],
													img.getHeader().getDimResolutions()[1],
													img.getHeader().getDimResolutions()[2]));
								connsToPts.put(addme,new HashSet<GridPt>());
//								System.out.println(addme + " maps to " + connsToFibers.get(addme));
							}
						}
					}
					lastlabel = curlabel;
					hitspace = false;
				}else if(curlabel==0 && lastlabel!=-1){
					hitspace = true;
				}
			}
			
//			System.out.println("(1,3) maps to " + connsToFibers.get(new Point2i(1,3)));
//			System.out.println(pair);
//			System.out.println(pairflip);
//			System.out.println(labels);
//			System.out.println(connsToFibers.keySet());
//			System.out.println("\n\n");
			
			/* next part - split the fibers appropriately */
			int pairind = 0;
			curlabel = 0;
			boolean start = false;		//have we hit the first label pair?
			boolean moveon = false; 	//time to move to the next label pair?
			boolean alldone = false;	//have we hit the last label pair?
			ArrayList<ArrayList<XYZ>> listFiberPoints = new ArrayList<ArrayList<XYZ>>(pair.size());
			for(int k=0; k<pair.size(); k++){
				
				ArrayList<XYZ> fiberPtList = new ArrayList<XYZ>();
				listFiberPoints.add(fiberPtList);
			}
			if(pair.size()>0){
//				System.out.println("(2,3) maps to " + connsToFibers.get(new Point2i(2,3)));
//				System.out.println("(1,3) maps to " + connsToFibers.get(new Point2i(1,3)));
//				System.out.println("Working...");
				for(int j=0; j<f.get(i).getXYZChain().length; j++){
//					System.out.println(connsToPts.get(pairflip.get(pairind)));
					GridPt pt = new GridPt((int)f.get(i).getXYZChain()[j].x,
							(int)f.get(i).getXYZChain()[j].y,
							(int)f.get(i).getXYZChain()[j].z);
					curlabel = this.img.getInt(pt.x,pt.y, pt.z);
					if(!alldone){
						if(curlabel==pair.get(pairind).x && !start){
							//at the beginning of the fiber we may have zeros
							//once we get here, we have a valid label and can add things
//							System.out.println("Starting..."+j);
							start = true;
							if(pairflip.get(pairind).x!=pairflip.get(pairind).y){
								listFiberPoints.get(pairind).add(f.get(i).getXYZChain()[j]);
								connsToPts.get(pairflip.get(pairind)).add(pt);
							}
						}else if(curlabel==0 && moveon){
//							System.out.println("Moving on..."+connsToPts.get(pairflip.get(pairind)).size()+"..."+j);
							//we're done with the previous pair and should move to the next
//							System.out.println(connsToPts.get(pairflip.get(pairind)).size());
//							System.out.println(connsToPts.get(pairflip.get(pairind)));
							pairind++;
							moveon=false;
							if(pairind==pair.size()){
								//we're all finished
//								System.out.println(pairflip.get(pairind-1));
//								System.out.println(connsToPts.get(pairflip.get(pairind-1)));
//								System.out.println("Done"+j);
								alldone=true;
							}else{
//								System.out.println(pairflip.get(pairind-1));
//								System.out.println(connsToPts.get(pairflip.get(pairind-1)));
//								System.out.println("More to do..."+j);
								//we're not done and add this point to this label pair
								if(pairflip.get(pairind).x!=pairflip.get(pairind).y){
									listFiberPoints.get(pairind).add(f.get(i).getXYZChain()[j]);
									connsToPts.get(pairflip.get(pairind)).add(pt);
								}
							}
						}else if(curlabel==pair.get(pairind).x){
//							System.out.println("Inside first label..."+connsToPts.get(pairflip.get(pairind)).size()+"..."+j);
							if(pairflip.get(pairind).x!=pairflip.get(pairind).y){
								listFiberPoints.get(pairind).add(f.get(i).getXYZChain()[j]);
								connsToPts.get(pairflip.get(pairind)).add(pt);
							}
						}else if(curlabel==pair.get(pairind).y){
//							System.out.println("Finished and inside second label..."+connsToPts.get(pairflip.get(pairind)).size()+"..."+j);
							
							moveon = true;
							//if we're in here, we are "inside" a label
							if(pairflip.get(pairind).x!=pairflip.get(pairind).y){
								listFiberPoints.get(pairind).add(f.get(i).getXYZChain()[j]);
								connsToPts.get(pairflip.get(pairind)).add(pt);
							}
							
							//need to add this point to the next label pair too
							if(pairind<pair.size()-1 && pairflip.get(pairind+1).x!=pairflip.get(pairind+1).y){
								listFiberPoints.get(pairind+1).add(f.get(i).getXYZChain()[j]);
								connsToPts.get(pairflip.get(pairind+1)).add(pt);
							}
						}else if(curlabel==0 && start){
//							System.out.println("In between two valid labels..."+connsToPts.get(pairflip.get(pairind)).size()+"..."+j);
							
							// if we're in here, we are between two valid labels
							if(pairflip.get(pairind).x!=pairflip.get(pairind).y){
								connsToPts.get(pairflip.get(pairind)).add(pt);
								listFiberPoints.get(pairind).add(f.get(i).getXYZChain()[j]);
							}
						}
					}
				}
			}
//			System.out.println("(2,3) maps to " + connsToFibers.get(new Point2i(2,3)));
//			System.out.println("(1,3) maps to " + connsToFibers.get(new Point2i(1,3)));
//			System.out.println(connsToFibers.keySet());
			for(int k=0; k<pair.size(); k++){
				if(pairflip.get(k).x!=pairflip.get(k).y){
					XYZ[] ptlist = new XYZ[listFiberPoints.get(k).size()];
					listFiberPoints.get(k).toArray(ptlist);
//					System.out.println("Adding Fiber for pair: " + pairflip.get(k));
					connsToFibers.get(pairflip.get(k)).add(new Fiber(ptlist));
				}
//				System.out.println("Tot num Fibers: " + connsToFibers.get(pairflip.get(k)).size());
//				System.out.println("Tot num Pts: " +connsToPts.get(pairflip.get(k)).size());
			}
//			System.out.println("(2,3) maps to " + connsToFibers.get(new Point2i(2,3)));
//			System.out.println("(1,3) maps to " + connsToFibers.get(new Point2i(1,3)));
//			System.out.println("Tot num Pts: " +connsToPts.get(pairflip.get(pairind)).size());
		}
//		System.out.println("Has key? " + connsToFibers.containsKey(new Point2i(2,3)));
//		System.out.println("(2,3,) maps to " + connsToFibers.get(new Point2i(2,3)));
//		System.out.println("(1,3) maps to " + connsToFibers.get(new Point2i(1,3)));
		return connsToFibers;
	}
	
	public static double[] getValuesFromGridPts(ImageData img, Collection<GridPt> pts){
		ArrayList<Double> vallist = new ArrayList<Double>(pts.size());
		for(GridPt pt: pts){
			vallist.add(img.getDouble(pt.x,pt.y,pt.z));
		}
		vallist.trimToSize();
		double[] values = new double[vallist.size()];
		for(int i=0; i<vallist.size(); i++){
			values[i]=vallist.get(i);
		}
		return values;
	}
	
	public ImageData getMaskedVol(){
		return maskedVolume;
	}
	public String getData(){
		return dataout;
	}
	
	


}
