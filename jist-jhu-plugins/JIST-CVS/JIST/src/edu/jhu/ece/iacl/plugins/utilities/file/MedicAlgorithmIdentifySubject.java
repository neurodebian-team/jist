/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.file;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class MedicAlgorithmIdentifySubject extends ProcessingAlgorithm{
	ParamObject<String> codeBook;
	ParamVolume vol;
	ParamVolume nvol;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vol=new ParamVolume("Volume"));
		inputParams.add(codeBook=new ParamObject<String>("Code Book",new StringReaderWriter()));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Identify Subject");
		inputParams.setName("Identify_Subject");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(nvol=new ParamVolume("Identified Volume"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ImageData v=vol.getImageData();
		String name=v.getName();
		String new_name=name;
		String cb=codeBook.getObject();
		Pattern id_pattern=Pattern.compile("cort\\d\\d\\d[a-zA-Z]");
		Matcher m=id_pattern.matcher(name);

		if(m.find()){
			String id=name.substring(m.start(),m.end());
			Matcher m2=Pattern.compile(id).matcher(cb);
			if(m2.find()){
				String txt=cb.substring(m2.end(),Math.min(cb.length(),m2.end()+1000));
				Matcher m3=Pattern.compile("\\s+[a-zA-Z][a-zA-Z]\\d\\d_").matcher(txt);
				if(m3.find()){
					txt=txt.substring(m3.start(),m3.end());
					String[] strs=txt.split("\\D+");
					if(strs.length>0){
						new_name=name.substring(m.start(),m.end()-1)+"_"+strs[1]+name.substring(m.end(),name.length());
					} else {
						System.out.println(getClass().getCanonicalName()+"\t"+"Colud not find numerical index");
					}
				} else {
					System.out.println(getClass().getCanonicalName()+"\t"+"Could not find decoded id in code book");
				}
			} else {
				System.out.println(getClass().getCanonicalName()+"\t"+"Could not find id in code book");
			}
		} else {
			System.out.println(getClass().getCanonicalName()+"\t"+"Could not find id in volume file name");
		}
		v.setName(new_name);
		nvol.setValue(v);
	}
}
