package edu.jhu.ece.iacl.doc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInformation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;

public class DocumentGeneratorHTML extends DocumentGenerator {
	String timeStamp;
	public DocumentGeneratorHTML() {
		SimpleDateFormat timeFormat=new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
		String time=timeFormat.format(new Date());
		String username=System.getProperty("user.name");
		if(username==null){
			timeStamp="Last modified "+time;
		} else {
			timeStamp="Last modified by "+username+ " on "+time;
		}
	}

	protected StringBuffer createCitations(ProcessingAlgorithm alg) {
		StringBuffer buffer = new StringBuffer();
		if (alg.getAlgorithmInformation().getCitations().size() > 0) {
			buffer.append("<H3>Citations</H3>\n");
			buffer.append("<UL>\n");
			for (Citation c : alg.getAlgorithmInformation().getCitations()) {
				if (c.getText().length() > 0) {
					buffer.append("<LI>" + c.getText() + "</LI>\n");
				}
			}
			buffer.append("</UL><BR>\n<HR size=\"1\">\n");
		}
		return buffer;
	}

	protected StringBuffer createAuthors(ProcessingAlgorithm alg) {
		StringBuffer buffer = new StringBuffer();
		if (alg.getAlgorithmInformation().getAuthors().size() > 0) {
			buffer.append("<H3>Authors</H3>\n");
			buffer.append("<UL>\n");
			for (AlgorithmAuthor c : alg.getAlgorithmInformation().getAuthors()) {
				if (c.toString().length() > 0) {
					buffer.append("<LI>" + c.toString() + "</LI>\n");
				}
			}
			buffer.append("</UL><HR size=\"1\">\n");
		}
		return buffer;
	}

	protected StringBuffer createFooter(ProcessingAlgorithm alg) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center><B><font size=\"2\">"+timeStamp+"</font></B></center></BODY>\n</HTML>");
		return buffer;
	}

	protected StringBuffer createHeader(ProcessingAlgorithm alg) {
		StringBuffer buffer = new StringBuffer();

		AlgorithmInformation info = alg.getAlgorithmInformation();
		buffer.append("<HTML>\n<HEAD>\n<TITLE>" + alg.getAlgorithmLabel()
				+ "</TITLE>\n</HEAD>\n<BODY>\n" + "<H2>"
				+ alg.getAlgorithmLabel() + " " + info.getVersion() + " "
				+ info.getStatusString() + " (" + info.getName() + ")</H2>\n"
				+ "<H3>" + (alg.getInput().getPackage() + "."
				+ alg.getInput().getCategory()).replace('_', ' ') + "</H3>\n" + "<PRE>["
				+ info.getAlgorithmClassName() + "]</PRE>\n<hr size=\"1\">\n");
		return buffer;
	}

	protected StringBuffer createInputInformation(ProcessingAlgorithm alg) {
		return getParamInfo(alg.getInput().getAllDescendants(), true);
	}

	protected StringBuffer createOutputInformation(ProcessingAlgorithm alg) {
		return getParamInfo(alg.getOutput().getAllDescendants(), false);
	}

	protected StringBuffer createShortDescription(ProcessingAlgorithm alg) {
		StringBuffer buffer = new StringBuffer();
		if (alg.getAlgorithmInformation().getDescription() != null) {
			buffer.append("<H3>Description</H3>\n"
					+ alg.getAlgorithmInformation().getDescription().replace("\n","<BR>\n")
					+ "<BR><BR>\n<HR size=\"1\">\n");
		}
		return buffer;
	}

	protected StringBuffer createLongDescription(ProcessingAlgorithm alg) {
		StringBuffer buffer = new StringBuffer();
		if (alg.getAlgorithmInformation().getLongDescription() != null) {
			buffer.append("<H3>Notes</H3>\n"
					+ alg.getAlgorithmInformation().getLongDescription().replace("\n","<BR>\n")
					+ "\n<hr size=\"1\">\n");
		}
		return buffer;
	}

	private StringBuffer getParamInfo(List<ParamModel> paramlist,
			boolean isInputParameters) {
		StringBuffer table = new StringBuffer();
		int start = 2;
		if (isInputParameters) {
			table.append("<H3>Inputs</H3>\n");
		} else {
			table.append("<H3>Outputs</H3>\n");
			start = 1;
		}
		table
				.append("<CENTER><TABLE width=\"90%\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\">\n");
		table
				.append("<TR><TD width=\"20%\"><B> Name </B></TD>"+
						((isInputParameters)?"<TD width=\"10%\"><B> Default </B></TD>":"") +
						"<TD width=\"10%\"><B> Type </B></TD><TD width=\"10%\"><B> File Extension </B></TD><TD><B> Description </B></TD></TR>\n");
		for (int i = start; i < paramlist.size(); i++) {
			ParamModel c = paramlist.get(i);
			if (c instanceof ParamInformation || c.isHidden())
				continue;
			// ADD IS MANDATORY
			if (c.isMandatory()) {
				table.append("<TR><TD><B>" + c.getName() + "</B></TD>\n");
			} else {
				table.append("<TR><TD><B>" + c.getName()
						+ " (Optional)</B></TD>\n");
			}
			if(isInputParameters)table.append("<TD>" + c.toString() + "</TD>");
			// ADD TYPE OF INPUT TO TABLE
			table.append("<TD>"
					+ c.getClass().getSimpleName().replaceFirst("Param", "")
					+ "\n");

			// ADD ADDITIONAL INFORMATION IF RELEVANT
			/*
			 * VOLUME
			 */
			if (c instanceof ParamVolume) {
				ParamVolume v = ((ParamVolume) c);

				String rows = "";
				if (v.getRows() == -1) {
					rows = "X";
				} else {
					rows = "" + v.getRows();
				}
				String cols = "";
				if (v.getCols() == -1) {
					cols = "Y";
				} else {
					cols = "" + v.getCols();
				}
				String slcs = "";
				if (v.getSlices() == -1) {
					slcs = "Z";
				} else {
					slcs = "" + v.getSlices();
				}
				String cmps = "";
				if (v.getComponents() == -1) {
					cmps = "T";
				} else {
					cmps = "" + v.getComponents();
				}
				table.append("<UL>");
				table.append("<LI><I>dim{" + rows + "," + cols + "," + slcs
						+ "," + cmps + "}</I></LI>\n");
				if (v.getVoxelType() == null) {
					table.append("<LI><I>type{any}</I></LI>\n");
				} else {
					table.append("<LI><I>type{" + v.getVoxelType()
							+ "}</I></LI>\n");
				}
				table.append("</UL>");
				table.append("</TD>");
				// Add file extension
				if (isInputParameters) {
					table.append("<TD>Supported Formats</TD>\n");
				} else {
					table.append("<TD><I>"
							+ v.getExtensionFilter().getPreferredExtension()
							+ "</I></TD>\n");
				}

				/*
				 * OPTION
				 */
			} else if (c instanceof ParamOption) {
				table.append("<OL>\n");
				ParamOption o = (ParamOption) c;
				for (String option : o.getOptions()) {
					table.append("<LI><I>" + option + "</I></LI>\n");
				}
				table.append("</OL></TD><TD> - </TD>\n");

				/*
				 * OBJECT
				 */
			} else if (c instanceof ParamSurface) {
				ParamSurface o = (ParamSurface) c;
				if (isInputParameters) {
					table.append("</TD><TD>Supported Formats</TD>\n");
				} else {
					table.append("</TD><TD><I>"
							+ o.getExtensionFilter().getPreferredExtension()
							+ "</I></TD>\n");
				}
			} else if (c instanceof ParamObject) {
				// System.out.println(getClass().getCanonicalName()+"\t"+c.getClass().getSimpleName());
				ParamObject o = (ParamObject) c;
				/*
				 * table.append("</TD><TD>"); if(o.getReaderWriter()!=null){
				 * String type =o.getReaderWriter().toString().replace(
				 * "edu.jhu.ece.iacl.jist.io.", "").replaceAll("ReaderWriter.*",
				 * ""); table.append( type +"\n"); }
				 */
				if (isInputParameters) {
					table.append("</TD><TD><UL>\n");
					List<String> exts = o.getExtensionFilter().getExtensions();
					for (String s : exts) {
						table.append("<LI><I>" + s + "</I></LI>\n");
					}
					table.append("</UL></TD>");
				} else {
					table.append("</TD><TD><I>"
							+ o.getExtensionFilter().getPreferredExtension()
							+ "</I></TD>\n");
				}

			} else {
				table.append("</TD><TD> - </TD>\n");
			}

			if (c.getDescription() == null) {
				table.append("<TD> " + c.getLabel() + " </TD>\n");
			} else {
				table.append("<TD>" + c.getDescription() + "</TD>\n");
			}
			table.append("</TR>\n");

		}
		table.append("</TABLE></CENTER><BR>\n<HR size=\"1\">\n");
		return table;
	}
}
