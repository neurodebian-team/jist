package edu.jhu.ece.iacl.algorithms.dti;

import java.awt.Color;

public class ComputeTensorContrasts {

	public static void allcontrasts(float [][][][]tensors, float [][][]fa, float [][][]md,
			float [][][]ra, float [][][]vr, float [][][][]evals,
			float [][][][]vec1,float [][][][]vec2,float [][][][]vec3) {
		for(int i=0;i<tensors.length;i++) 
			for(int j=0;j<tensors[0].length;j++)
				for(int k=0;k<tensors[0][0].length;k++) {
					DiffusionTensor dti = new DiffusionTensor(tensors[i][j][k]);
					if(fa!=null)
						fa[i][j][k]=dti.FA();
					if(md!=null)
						md[i][j][k]=dti.MD();
					if(ra!=null)
						ra[i][j][k]=dti.RA();
					if(vr!=null)
						vr[i][j][k]=dti.VR();					
					float []e=dti.evals();
					float []v1 = dti.vec1();
					float []v2 = dti.vec2();
					float []v3 = dti.vec3();
					for(int ii=0;ii<3;ii++) {
						if(evals!=null)
							evals[i][j][k][ii]=e[ii];
						if(vec1!=null) 
							vec1[i][j][k][ii]=v1[ii];
						if(vec2!=null) 
							vec2[i][j][k][ii]=v2[ii];
						if(vec3!=null) 
							vec3[i][j][k][ii]=v3[ii];

					}					
				}		
	}

	public static void computeOrientationEncodedColormap(float[][][] scalar, float[][][][] vector, Color[][][] rgb) {
		for(int i=0;i<scalar.length;i++) {
			for(int j=0;j<scalar[0].length;j++) {
				for(int k=0;k<scalar[0][0].length;k++) {
					rgb[i][j][k] = new Color(					
							Math.min(1,Math.max(0, Math.abs(scalar[i][j][k]*vector[i][j][k][0]))), //red
							Math.min(1,Math.max(0, Math.abs(scalar[i][j][k]*vector[i][j][k][1]))), //grn
							Math.min(1,Math.max(0, Math.abs(scalar[i][j][k]*vector[i][j][k][2]))));//blu
				}

			}
		}

	}

}


