package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import javax.vecmath.*;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.io.LEFileWriter;
import edu.jhu.ece.iacl.jist.structures.fiber.DTIStudioHDR;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;



public class DTISFiberWriter {
	
	public static boolean writeFACTFibers(Vector allFibers, String filename, FACTparameters dataConfig){
		try {
    		LEFileWriter bos = new LEFileWriter(filename);
    		
    		//WRITE HEADER
    		//DTIStudioHDR hdr = new DTIStudioHDR(); 
        	bos.writeBytes("FiberDat"); 
        	
        	bos.writeInt(allFibers.size());	//Number of Fibers
        	
        	float[] stats = getStats(allFibers);
        	
        	bos.writeInt((int)stats[1]);		//FLM
        	bos.writeFloat(stats[2]);	//FLMn

        	//bos.writeFloat(0);
        	
        	bos.writeInt(dataConfig.Nx);
        	bos.writeInt(dataConfig.Ny);
        	bos.writeInt(dataConfig.Nz);
        	
        	bos.writeFloat((float)dataConfig.resX);
        	bos.writeFloat((float)dataConfig.resY);
        	bos.writeFloat((float)dataConfig.resZ);
        	
        	bos.writeInt(1);
        	bos.writeInt(0);
        	
        	bos.seek(128);
        	
        	
        	//WRITE FIBERS
        	USbyte usb = new USbyte();
    		for(int i=0;i<allFibers.size();i++){
    			
    			FACTfiber f = (FACTfiber)allFibers.get(i);		//GET THE iTH FIBER
    			Point3f fbrs[] = f.points;
    			
        		bos.writeInt(fbrs.length);	//fiber length
        		bos.writeByte(0);			//cReserved
        		
        		//HOW TO COME UP WITH COLORS?
        		//everything red
        		bos.writeByte(usb.unsigned2signed(255));			//Color - Red
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Green
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Blue
        		
        		bos.writeInt(0);			//nSelectFiberStartPoint
        		bos.writeInt(fbrs.length);	//nSelec
        		
        		//WRITE EVERY POINT IN THE FIBER 
        		for (int j=0;j<fbrs.length;j++){
        			Point3f pt = (Point3f)fbrs[j];
        			bos.writeFloat(pt.x);
        			bos.writeFloat(pt.y);
        			bos.writeFloat(pt.z);
        		}
        	}
        	bos.close();
        	
        	return true;
    	}catch (IOException e){
    		System.out.println("jist.plugins"+"\t"+"Error Writing File");
    		e.printStackTrace();
    	}
    	return false;
	}
	/*
	public static boolean writeMTFACTFibers(Vector allFibers, String filename, FACTparameters dataConfig){
		try {
    		LEFileWriter bos = new LEFileWriter(filename);
    		
    		//WRITE HEADER
    		//DTIStudioHDR hdr = new DTIStudioHDR(); 
        	bos.writeBytes("FiberDat"); 
        	
        	bos.writeInt(allFibers.size());	//Number of Fibers
        	
        	float[] stats = getStats(allFibers);
        	
        	bos.writeInt((int)stats[1]);		//FLM
        	bos.writeFloat(stats[2]);	//FLMn

        	//bos.writeFloat(0);
        	
        	bos.writeInt(dataConfig.Nx);
        	bos.writeInt(dataConfig.Ny);
        	bos.writeInt(dataConfig.Nz);
        	
        	bos.writeFloat((float)dataConfig.resX);
        	bos.writeFloat((float)dataConfig.resY);
        	bos.writeFloat((float)dataConfig.resZ);
        	
        	bos.writeInt(1);
        	bos.writeInt(0);
        	
        	bos.seek(128);
        	
        	
        	//WRITE FIBERS
        	USbyte usb = new USbyte();
    		for(int i=0;i<allFibers.size();i++){
    			
    			MTFACTfiber f = (MTFACTfiber)allFibers.get(i);		//GET THE iTH FIBER
    			Point3f fbrs[] = f.points;
    			
        		bos.writeInt(fbrs.length);	//fiber length
        		bos.writeByte(0);			//cReserved
        		
        		//HOW TO COME UP WITH COLORS?
        		//everything red
        		bos.writeByte(usb.unsigned2signed(255));			//Color - Red
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Green
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Blue
        		
        		bos.writeInt(0);			//nSelectFiberStartPoint
        		bos.writeInt(fbrs.length);	//nSelec
        		
        		//WRITE EVERY POINT IN THE FIBER 
        		for (int j=0;j<fbrs.length;j++){
        			Point3f pt = (Point3f)fbrs[j];
        			bos.writeFloat(pt.x);
        			bos.writeFloat(pt.y);
        			bos.writeFloat(pt.z);
        		}
        	}
        	bos.close();
        	
        	return true;
    	}catch (IOException e){
    		System.out.println("jist.plugins"+"\t"+"Error Writing File");
    		e.printStackTrace();
    	}
    	return false;
	}*/
	
	private static float[] getStats(Vector allFibers){
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int mean = 0;

		for(int i=0; i<allFibers.size(); i++){

			int n = ((FACTfiber)allFibers.get(i)).numPoints;
			if(n<min){
				min=n;
			}else if(n>max){
				max = n;
			}
			mean = mean+n;
		}
		mean = mean/allFibers.size();
		
		float[] stats = {min, max, mean};
		return stats;
	}
	
	public static boolean writeFibers(Fiber[] allFibers, String filename, DTIStudioHDR dataConfig){
		int[] color = {255,0,0};
		return writeFibersColor(allFibers,filename,dataConfig,color);
	}
	
	public static boolean writeFibersColor(Fiber[] allFibers, String filename, DTIStudioHDR dataConfig, int[] color){
		try {
    		LEFileWriter bos = new LEFileWriter(filename);
    		
    		//WRITE HEADER
    		//DTIStudioHDR hdr = new DTIStudioHDR(); 
        	bos.writeBytes("FiberDat"); 
        	
        	bos.writeInt(allFibers.length);	//Number of Fibers
        	
        	float[] stats = getStats(allFibers);
        	
        	bos.writeInt((int)stats[1]);		//FLM
        	bos.writeFloat(stats[2]);	//FLMn

        	//bos.writeFloat(0);
        	
        	bos.writeInt(dataConfig.ImageSize[0]);
        	bos.writeInt(dataConfig.ImageSize[1]);
        	bos.writeInt(dataConfig.ImageSize[2]);
        	
        	bos.writeFloat(dataConfig.VoxelSize[0]);
        	bos.writeFloat(dataConfig.VoxelSize[1]);
        	bos.writeFloat(dataConfig.VoxelSize[2]);
        	
        	bos.writeInt(1);
        	bos.writeInt(0);
        	
        	bos.seek(128);
        	
        	
        	//WRITE FIBERS
        	USbyte usb = new USbyte();
    		for(int i=0;i<allFibers.length;i++){
    			
    			Fiber f = allFibers[i];		//GET THE iTH FIBER
    			XYZ[] chain = f.getXYZChain();
    			
        		bos.writeInt(chain.length);	//fiber length
        		bos.writeByte(0);			//cReserved
        		
        		//HOW TO COME UP WITH COLORS?
        		//everything red
        		bos.writeByte(usb.unsigned2signed(color[0]));				//Color - Red
        		bos.writeByte(usb.unsigned2signed(color[1]));				//Color - Green
        		bos.writeByte(usb.unsigned2signed(color[2]));				//Color - Blue
        		
        		bos.writeInt(0);			//nSelectFiberStartPoint
        		bos.writeInt(chain.length);	//nSelec
        		
        		if(chain==null){
        			System.out.println("jist.plugins"+"\t"+chain);
        		}
        		
        		//WRITE EVERY POINT IN THE FIBER 
        		for (int j=0;j<chain.length;j++){
        			XYZ pt = (XYZ)chain[j];
        			bos.writeFloat(pt.x);
        			bos.writeFloat(pt.y);
        			bos.writeFloat(pt.z);
        		}
        	}
        	bos.close();
        	
        	return true;
    	}catch (IOException e){
    		System.out.println("jist.plugins"+"\t"+"Error Writing File");
    		e.printStackTrace();
    	}
    	return false;
	}
	
	public static boolean writeFibers(Fiber[] allFibers, String filename, DTIStudioHDR dataConfig, int n){
		try {
    		LEFileWriter bos = new LEFileWriter(filename);
    		
    		//WRITE HEADER
    		//DTIStudioHDR hdr = new DTIStudioHDR(); 
        	bos.writeBytes("FiberDat"); 
        	
        	bos.writeInt(allFibers.length);	//Number of Fibers
        	
        	float[] stats = getStats(allFibers);
        	
        	bos.writeInt((int)stats[1]);		//FLM
        	bos.writeFloat(stats[2]);	//FLMn

        	//bos.writeFloat(0);
        	
        	bos.writeInt(dataConfig.ImageSize[0]);
        	bos.writeInt(dataConfig.ImageSize[1]);
        	bos.writeInt(dataConfig.ImageSize[2]);
        	
        	bos.writeFloat(dataConfig.VoxelSize[0]);
        	bos.writeFloat(dataConfig.VoxelSize[1]);
        	bos.writeFloat(dataConfig.VoxelSize[2]);
        	
        	bos.writeInt(1);
        	bos.writeInt(0);
        	
        	bos.seek(128);
        	
        	
        	//WRITE FIBERS
        	USbyte usb = new USbyte();
    		for(int i=0;i<n;i++){
    			
    			Fiber f = allFibers[i];		//GET THE iTH FIBER
    			XYZ[] chain = f.getXYZChain();
    			
        		bos.writeInt(chain.length);	//fiber length
        		bos.writeByte(0);			//cReserved
        		
        		//HOW TO COME UP WITH COLORS?
        		//everything red
        		bos.writeByte(usb.unsigned2signed(255));				//Color - Red
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Green
        		bos.writeByte(usb.unsigned2signed(0));				//Color - Blue
        		
        		bos.writeInt(0);			//nSelectFiberStartPoint
        		bos.writeInt(chain.length);	//nSelec
        		
        		if(chain==null){
        			System.out.println("jist.plugins"+"\t"+chain);
        		}
        		
        		//WRITE EVERY POINT IN THE FIBER 
        		for (int j=0;j<chain.length;j++){
        			XYZ pt = (XYZ)chain[j];
        			bos.writeFloat(pt.x);
        			bos.writeFloat(pt.y);
        			bos.writeFloat(pt.z);
        		}
        	}
        	bos.close();
        	
        	return true;
    	}catch (IOException e){
    		System.out.println("jist.plugins"+"\t"+"Error Writing File");
    		e.printStackTrace();
    	}
    	return false;
	}
	
	private static float[] getStats(Fiber[] allFibers){
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int mean = 0;

		for(int i=0; i<allFibers.length; i++){
			if(allFibers[i]!=null){
//				System.out.println("jist.plugins"+"\t"+"Whats with this good Fiber?!? at " + i);
				int n = (allFibers[i]).getLength();
				if(n<min){
					min=n;
				}else if(n>max){
					max = n;
				}
				mean = mean+n;
			}else{
//				System.out.println("jist.plugins"+"\t"+"Whats with this null Fiber?!? at " + i);
//				System.out.println("jist.plugins"+"\t"+allFibers[i]);
			}
		}
		if(allFibers.length>0){
			mean = mean/allFibers.length;
		}
		
		float[] stats = {min, max, mean};
		return stats;
	}

	
}
