package edu.jhu.ece.iacl.plugins.labeling;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import Jama.Matrix;

//import edu.jhu.ece.iacl.algorithms.vabra.VabraAlgorithm;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.volume.TransformVolume;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmMultiAtlasReg;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmMultiDeformVolume;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmTransformVolume;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmVABRA;
import edu.jhu.ece.iacl.plugins.labeling.staple.MedicAlgorithmSTAPLE;
import edu.jhu.ece.iacl.plugins.labeling.staple.MedicAlgorithmSTAPLEsurface;
import edu.jhu.ece.iacl.utility.AtlasList;

// This plugin is based MedicAlgorithmMultiAtlasLabeling, originally authored by Min Chen
public class MedicAlgorithmMultiAtlasSurfaceLabeling extends ProcessingAlgorithm {

	// Inputs
	ParamFile atlasDescription;
	ParamSurface targetSurface;
	ParamVolume targetVolume;

	// Outputs
	ParamVolumeCollection deformedAtlasesLabels;
	ParamVolumeCollection deformedAtlases;
	ParamVolumeCollection deformationFields;
	ParamFileCollection transMatrices;

	ParamSurfaceCollection atlasSurfaceLabels;
	ParamSurface cleanLabeledTarget;
	ParamSurface labeledTarget;

	//	ParamVolume targetLabels;

	ParamBoolean outputDeformedAtlasesLabels;
	ParamBoolean outputDeformedAtlases;
	ParamBoolean outputDeformationFields;

	private MedicAlgorithmVABRA vabra;
	private MedicAlgorithmSTAPLEsurface staple;
	private MedicAlgorithmMultiDeformVolume multideform;
	private MedicAlgorithmTransformVolume multitransform;
	private MedicAlgorithmVolumeToSurfaceLabeling volToSurfLabels;
	private MedicAlgorithmCleanSurfLabels labelCleanup;

	ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();

	private static final String revnum = "$Revision: 1.5 $".replace(
			"Revision: ", "").replace("$", "").replace(" ", "");
	public String[] getDefaultJVMArgs() {
		return new String[] { "-XX:MinHeapFreeRatio=70",
				"-XX:MaxHeapFreeRatio=95",
				"-XX:YoungGenerationSizeIncrement=150",
		"-XX:TenuredGenerationSizeIncrement=150","-Xms1500M" };
	}

	protected void createInputParameters(ParamCollection inputParams) {
		try {
			vabra = new MedicAlgorithmVABRA();
			staple = new MedicAlgorithmSTAPLEsurface();
			multideform = new MedicAlgorithmMultiDeformVolume();
			multitransform = new MedicAlgorithmTransformVolume();
			volToSurfLabels = new MedicAlgorithmVolumeToSurfaceLabeling();
			labelCleanup = new MedicAlgorithmCleanSurfLabels();

			//			FileExtensionFilter volumefilter = CubicVolumeReaderWriter
			//			.getInstance().getExtensionFilter();

			ParamCollection mainParams = new ParamCollection("Main");
			//		mainParams.add(atlases = new ParamFileCollection("Atlases"));		
			//		atlases.setExtensionFilter(volumefilter);
			mainParams.add(targetSurface = new ParamSurface("Target Surface to Label"));			
			mainParams.add(targetVolume = new ParamVolume("Target Volume for Labels"));
			//			targetVolume.setLoadAndSaveOnValidate(false);

			targetVolume.setMandatory(true); // why is  this needed?

			mainParams.add(atlasDescription = new ParamFile("Atlas Description"));

			ParamCollection intermParams = new ParamCollection(
			"Intermediate Results");
			intermParams.add(outputDeformedAtlasesLabels = new ParamBoolean(
					"Output Deformed Atlas Labels", false));
			intermParams.add(outputDeformedAtlases = new ParamBoolean(
					"Output Registered Atlases", false));
			intermParams.add(outputDeformationFields = new ParamBoolean(
					"Output Deformation Fields", false));


			//			multivabra.getInput().getFirstChildByName("Main").setHidden(true);

			((ParamSurfaceCollection) staple.getInput().getFirstChildByName("Rater Surfaces")).setHidden(true);
			((ParamFloat) staple.getInput().getFirstChildByName(
			"Max Delta for Convergence")).setValue(.0000001);
			((ParamInteger) staple.getInput().getFirstChildByName("Max Iterations"))
			.setValue(35);
			(staple.getInput().getFirstChildByName("Rater Surfaces")).setMandatory(false);
			volToSurfLabels.getInput().getFirstChildByName("Surface to Label").setMandatory(false);
			volToSurfLabels.getInput().getFirstChildByName("Data Volume").setMandatory(false);
			labelCleanup.getInput().getFirstChildByName("Labeled Surface").setMandatory(false);
			((ParamBoolean) staple.getInput().getFirstChildByName("Output Label Probability Surfaces?")).setHidden(true);

			//			volToSurfLabels.getInput().getFirstChildByName("Surface to Label").setHidden(true);
			//			volToSurfLabels.getInput().getFirstChildByName("Data Volume").setHidden(true);

			//			labelCleanup.getInput().getFirstChildByName("Labeled Surface").setHidden(true);


			inputParams.add(mainParams);
			inputParams.add(intermParams);
			vabra.hideVolumeInputs();
			inputParams.add(vabra.getInput());
			//			inputParams.add(multivabra.getInput());
			inputParams.add(staple.getInput());
			//			inputParams.add(volToSurfLabels.getInput());
			//			inputParams.add(labelCleanup.getInput());

			inputParams.setLabel("Surface Labeling via Multiple Atlases");
			inputParams.setName("SurfaceMultiAtlasLabel");

			setPreferredSize(new Dimension(300, 600));

			inputParams.setPackage("IACL");
			inputParams.setCategory("Labeling");

			AlgorithmInformation info = getAlgorithmInformation();
			info.setWebsite("http://www.iacl.ece.jhu.edu/");
			//		info.add(PrinceGroupAuthors.minChen);
			info
			.setDescription("Uses N Atlases to label a target image with the same labels");
			info
			.setLongDescription("This algorithm is used to label a volume by registering multiple atlases to"
					+ " the same target via multiple calls of single channel VABRA.");
			info.setVersion(revnum);
			info.setEditable(false);
			info.setStatus(DevelopmentStatus.ALPHA);
		} catch(Exception err) {
			System.out.println("jist.plugins"+"\t"+"WARNING: Stub initilization of parameters. Likely running without VABRA module");
			err.printStackTrace();
		}
	}

	protected void createOutputParameters(ParamCollection outputParams) {

		outputParams.add(cleanLabeledTarget = new ParamSurface(
		"CLEANED Label Results for Target"));

		outputParams.add(labeledTarget = new ParamSurface(
		"STAPLE Label Results for Target"));

		outputParams.add(deformedAtlasesLabels = new ParamVolumeCollection(
		"Deformed Atlas Labels (Intermediate Result)"));
		//		deformedAtlasesLabels.setLoadAndSaveOnValidate(false);
		outputParams.add(deformedAtlases = new ParamVolumeCollection(
		"Registered Atlases (Intermediate Result)"));
		//		deformedAtlases.setLoadAndSaveOnValidate(false);
		outputParams.add(deformationFields = new ParamVolumeCollection(
		"Deformation Fields (Intermediate Result)"));
		//		deformationFields.setLoadAndSaveOnValidate(false);

		FileExtensionFilter xfmfilter = new FileExtensionFilter(new String[] {
				"mtx", "xfm" });
		outputParams.add(transMatrices = new ParamFileCollection(
				"Transformation Matrices (Intermediate Result)", xfmfilter));

		deformedAtlasesLabels.setMandatory(false);
		deformedAtlases.setMandatory(false);
		deformationFields.setMandatory(false);
		transMatrices.setMandatory(false);

		outputParams.add(atlasSurfaceLabels=new ParamSurfaceCollection("Deformed Atlas Labels"));
		atlasSurfaceLabels.setMandatory(false);
		outputParams.setName("MultiAtlasLabel");
	}

	protected void execute(CalculationMonitor monitor) {
		Wrapper wrapper=new Wrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}

	protected class Wrapper extends AbstractCalculation{
		public Wrapper(){
		}

		protected void execute(MedicAlgorithmMultiAtlasSurfaceLabeling parent) {


			//			List<ImageData> tmpImageList = new ArrayList<ImageData>();

			System.out.format("*********************Step 0: Identify Atlases*********************\n");
			generateMemoryReport();

			this.setLabel("Load Atlases");

			File dir = new File(parent.getOutputDirectory()
					+ File.separator
					+ edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(parent
							.getAlgorithmName()));
			dir.mkdir();

			List<File[]> atlasFiles = AtlasList.loadFileSetAtlas(atlasDescription.getValue());
			List<File> imageFileList = new LinkedList<File>();
			List<File> labelFileList = new LinkedList<File>();
			for(int i=0;i<atlasFiles.size();i++) {
				if(atlasFiles.get(i).length!=2)
					throw new RuntimeException("Invalid atlas list file. Must have 2 columns. Image data -> label data");
				imageFileList.add(atlasFiles.get(i)[0]);
				labelFileList.add(atlasFiles.get(i)[1]);
			}


			// Step 0: 
			System.out.format("*********************Step 1: Registering All Volumetric Atlases to Target*********************\n");
			generateMemoryReport();
			this.setLabel("Register Atlases");

			/*
			((ParamFileCollection) multivabra.getInput().getFirstChildByName(
			"Atlases")).setValue(imageFileList);
			((ParamVolume) multivabra.getInput().getFirstChildByName("Target"))
			.setValue(targetVolume.getValue());
			multivabra.setOutputDirectory(parent.getOutputDirectory());
			multivabra.run();

			//check if deformed atlas output is needed
			if(outputDeformedAtlases.getValue()){
				deformedAtlases.setValue(((ParamVolumeCollection) multivabra.getOutput()
						.getFirstChildByName("Registered Atlases")).getImageDataList());
				//deformedAtlases.setName("Registered Atlases (Intermediate Result)");

				//				deformedAtlases.saveResources(dir, true);
				//				deformedAtlases.writeAndFreeNow(parent);
				//	deformedAtlases.getFactory().getOutputView();
			} else {
				((ParamVolumeCollection) multivabra.getOutput()
						.getFirstChildByName("Registered Atlases")).dispose();
			}
			 */
//			double[][] tmpMatrix;

			ImageData tar = targetVolume.getObject();
			ImageDataReaderWriter rw = ImageDataReaderWriter.getInstance();
			List<ImageDataFloat> DefFields = new ArrayList<ImageDataFloat>();
			ArrayList<ImageData> matchedAtlases = new ArrayList<ImageData>();
			ArrayList<ImageData> matchedTargets = new ArrayList<ImageData>();
			ImageData temp, atlasVol;
			ImageDataMipav tempMipav;
			ArrayList<Matrix> transMatrixes = new ArrayList<Matrix>();
			
			List<Number> atlasWeights = new ArrayList<Number>(1);
			atlasWeights.add(1);
			List<Number> targetWeight = new ArrayList<Number>(1);
			targetWeight.add(1);

			// go through all the atlases and get Deformations fields for each
			for (int q = 0; q < atlasFiles.size(); q++) {
				System.out.println("jist.plugins"+"\t"+"q="+q);
				generateMemoryReport();

				// add buffer to Target to prevent boundary issues
				atlasVol = rw.read(atlasFiles.get(q)[0]);				

				System.out.format("****************Registering atlases " + q
						+ "*********************\n");

				if (matchedAtlases.size() < 1) {
					matchedAtlases.add(atlasVol.clone());
					matchedTargets.add(tar.clone()); // why clone!?
				} else {
					matchedAtlases.set(0, atlasVol.clone());
					matchedTargets.set(0, tar.clone());
				}
				
				System.out.println("jist.plugins"+"\t"+"Duplicated in/out targets");
				generateMemoryReport();

				((ParamVolumeCollection)vabra.getInput().getFirstChildByName("Subjects")).setValue(matchedAtlases);
				((ParamNumberCollection)vabra.getInput().getFirstChildByName("Subject Weights")).setValue(atlasWeights);
				((ParamVolumeCollection)vabra.getInput().getFirstChildByName("Targets")).setValue(matchedTargets);
				((ParamNumberCollection)vabra.getInput().getFirstChildByName("Target Weights")).setValue(targetWeight);

				System.out.println("jist.plugins"+"\t"+"Set values");
				generateMemoryReport();
				
				if(true)
					throw new RuntimeException("barfoo");
				
				vabra.run();

				transMatrixes.add(((ParamMatrix)vabra.getOutput().getFirstChildByName("Transformation Matrix")).getValue());

				// Save Transformation Matrices
//				tmpMatrix = ((ParamMatrix)vabra.getOutput().getFirstChildByName("Transformation Matrix")).getValue().getArray();
				

				ImageDataFloat defField =new ImageDataFloat( ((ParamVolume)vabra.getOutput().getFirstChildByName("Deformation Field")).getObject());
				DefFields
				.add(defField);
				System.out.format("****************Atlas " + q
						+ " Registered****************\n");

				// Set Outputs and produced deformed atlases if needed

				float[]  res = tar.getHeader().getDimResolutions();
				Point3f resolutions = new Point3f(res[0], res[1], res[2]);
				Point3i dimensions = new Point3i(tar.getRows(), tar
						.getCols(), tar.getSlices());

				tempMipav = new ImageDataMipav(atlasVol);


				tempMipav = TransformVolume.transform(tempMipav,
						TransformVolume.Interpolation.Trilinear,
						transMatrixes.get(q), resolutions, dimensions);

				temp = atlasVol.mimic();
				RegistrationUtilities.DeformImage3D(tempMipav, temp, defField,
						tempMipav.getRows(), tempMipav.getCols(), tempMipav.getSlices(), 
						RegistrationUtilities.InterpolationType.TRILINEAR);
				temp.setName(atlasVol.getName() + "_reg");
				deformedAtlases.add(temp);






				deformationFields.setValue(DefFields);
			}


			System.out.format("*********************Step 2: Transforming Labels*********************\n");
			generateMemoryReport();
			this.setLabel("Transform Labels");
			((ParamVolumeCollection) multitransform.getInput()
					.getFirstChildByName("Volumes")).setValue(labelFileList);
			((ParamOption) multitransform.getInput().getFirstChildByName(
			"Registration interpolation")).setValue(7); //NN!
			((ParamVolume) multitransform.getInput().getFirstChildByName(
			"Volume to Match - Dimensions & Resolution"))
			.setValue(targetVolume.getValue());
			((ParamFileCollection) multitransform.getInput()
					.getFirstChildByName("Transformation Matrices"))
					.setValue(transMatrixes);
			((ParamBoolean) multitransform.getInput().getFirstChildByName(
			"Rotate Around Center")).setValue(false);
			multitransform.setOutputDirectory(parent.getOutputDirectory());
			multitransform.run();


			((ParamVolumeCollection) multideform.getInput()
					.getFirstChildByName("Volumes to Deform"))
					.setValue(((ParamVolumeCollection) multitransform
							.getOutput().getFirstChildByName(
									"Transformed Volumes"))
									.getImageDataList());

			System.out.format("*********************Step 3: Deforming Labels*********************\n");
			generateMemoryReport();
			this.setLabel("Deform Labels");
			((ParamVolumeCollection) multideform.getInput().getFirstChildByName(
			"Deformation Fields"))
			.setValue(deformationFields.getImageDataList());
			((ParamBoolean) multideform.getInput().getFirstChildByName(
			"Use Nearest Neighbor")).setValue(true);
			multideform.setOutputDirectory(parent.getOutputDirectory());
			multideform.run();

			// Done with transformed volumes
			((ParamVolumeCollection) multitransform
					.getOutput().getFirstChildByName(
					"Transformed Volumes")).dispose();

			if(outputDeformedAtlasesLabels.getValue()){
				deformedAtlasesLabels.setValue(((ParamVolumeCollection) multideform
						.getOutput().getFirstChildByName("Deformed Volumes")).getImageDataList());
				deformedAtlasesLabels.saveResources(dir, true);
				//	deformedAtlasesLabels.setReaderWriter(null);
			}

			//check if deformation field output is needed
		
				deformationFields.setValue(deformationFields.getImageDataList());

				//				deformationFields.saveResources(dir, true);
				//				deformationFields.writeAndFreeNow(parent);
			

			System.out.format("*********************Step 4: Extract Surface *********************\n");
			generateMemoryReport();
			// Get the surface mesh

			//			ParamSurfaceCollection targetSurfCollection = new ParamSurfaceCollection();
			//			targetSurfCollection.add();
			((ParamSurfaceCollection) volToSurfLabels.getInput().getFirstChildByName("Surface to Label")).add(targetSurface);

			// Get all the deformed volumes
			for (ImageData vol: ((ParamVolumeCollection) multideform
					.getOutput().getFirstChildByName("Deformed Volumes"))
					.getImageDataList()){
				System.out.println("jist.plugins"+"\t"+"****"+vol.getName());
				System.out.println("jist.plugins"+"\t"+"****=>"+((ParamVolumeCollection) volToSurfLabels.getInput().getFirstChildByName("Data Volume")).getName());
				((ParamVolumeCollection) volToSurfLabels.getInput().getFirstChildByName("Data Volume")).add(vol);
			}


			volToSurfLabels.run();
			ParamSurfaceCollection labeledSurfaces = (ParamSurfaceCollection)volToSurfLabels.getOutput().getFirstChildByName("Labeled Surface");

			atlasSurfaceLabels.setValue(labeledSurfaces.getSurfaceList());

			System.out.println("jist.plugins"+"\t"+"# Labeled Surfaces:"+labeledSurfaces.size());
			for(EmbeddedSurface surf : labeledSurfaces.getSurfaceList()){
				System.out.println("jist.plugins"+"\t"+"\t"+surf.getName());
				System.out.println("jist.plugins"+"\t"+"\t\t"+surf.getVertexCount());
			}

			System.out.format("*********************Step 5: Surface STAPLE*********************\n");
			generateMemoryReport();
			((ParamSurfaceCollection)staple.getInput().getFirstChildByName("Rater Surfaces")).setValue(labeledSurfaces.getSurfaceList());
			staple.run();



			System.out.format("*********************Step 6: Label Cleanup *********************\n");
			generateMemoryReport();

			//label cleanup 
			((ParamSurface)labelCleanup.getInput().getFirstChildByName("Labeled Surface")).setValue(
					((ParamSurface)staple.getOutput().getFirstChildByName("Labeled Surface")).getSurface());

			labelCleanup.run();

			System.out.format("*********************Step 7: Setup Outputs *********************\n");
			generateMemoryReport();
			EmbeddedSurface surf = ((ParamSurface)staple.getOutput().getFirstChildByName("Labeled Surface")).getSurface();
			surf.setName(targetSurface.getName()+"_labeled");
			labeledTarget.setValue(surf);
			surf = ((ParamSurface)labelCleanup.getOutput().getFirstChildByName("Cleaned Labels")).getSurface();
			surf.setName(targetSurface.getName()+"_labeledClean");			
			cleanLabeledTarget.setValue(surf);

			System.out.format("*********************Step 8: Done *********************\n");
			generateMemoryReport();
			/*
			for (ImageData vol: ((ParamVolumeCollection) multideform
					.getOutput().getFirstChildByName("Deformed Volumes"))
					.getImageDataList()){

				tmpImageList.add(vol.clone());

			}

			((ParamVolumeCollection) staple.getInput().getFirstChildByName(
			"Rater Volumes")).setValue(tmpImageList);

			//		((ParamVolumeCollection) staple.getInput().getFirstChildByName(
			//		"Rater Volumes")).setValue(((ParamVolumeCollection) multideform
			//		.getOutput().getFirstChildByName("Deformed Volumes"))
			//		.getImageDataList());

			staple.run();

			labelsOut = ((ParamVolume) staple.getOutput().getFirstChildByName(
			"Label Volume")).getObject();

			labelsOut.setHeader(tarVol.getHeader());
			labelsOut.setName(tarVol.getName() + "_labels");

			labeledTarget.setValue(labelsOut);
			labeledTarget.setMandatory(false);
			 */
		}

	}

	public static void generateMemoryReport() {
		System.gc();System.gc();System.gc();System.gc();System.gc();
		System.gc();System.gc();System.gc();System.gc();System.gc();
		Runtime runtime = Runtime.getRuntime();  

		long maxMemory = runtime.maxMemory();  
		long allocatedMemory = runtime.totalMemory();  
		long freeMemory = runtime.freeMemory();  
		System.out.println("jist.plugins"+"\t"+">###############################################################################");
		System.out.println("jist.plugins"+"\t"+"\tfree memory:       \t" + freeMemory / 1024);  
		System.out.println("jist.plugins"+"\t"+"\tallocated memory:  \t" + allocatedMemory / 1024);  
		System.out.println("jist.plugins"+"\t"+"\tmax memory:        \t" + maxMemory /1024);  
		System.out.println("jist.plugins"+"\t"+"\ttotal free memory: \t" + (freeMemory + (maxMemory - allocatedMemory)) / 1024);
		System.out.println("jist.plugins"+"\t"+"\tVolumes in memory: \t");
		for(String str: MipavController.getImageNames()) {
	    System.out.println("jist.plugins"+"\t"+"\t                   \t\t" +str);	
		}
		System.out.println("jist.plugins"+"\t"+"<###############################################################################");
		
		System.out.flush();
	}
}
