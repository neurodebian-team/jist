package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;

public class StapleTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		String dir = "C:\\Documents and Settings\\John\\My Documents\\Research\\Research_dti\\stapleTest\\";
//		String[] names = {dir+"test1.img", dir+"test2.img", dir+"test3.img"};
//		int[] dim = {32,32,16};
		
//		String dir = "C:\\Documents and Settings\\John\\My Documents\\Research\\Research_dti\\stapleTest\\";
//		String[] names = {dir+"rater1.img", dir+"rater2.img", dir+"rater3.img", dir+"rater4.img", dir+"rater5.img"};
		
		String dir = "/home/john/Research/staple/";
		String[] names = {dir+"rater1.img", dir+"rater2.img", dir+"rater3.img", dir+"rater4.img", dir+"rater5.img"};
		
		
		int[] dim = {32,32,16};
		
		STAPLE st = new STAPLE(names,dim);
		st.setDir(dir);
		st.setmaxIters(100);
		System.out.println("jist.plugins"+"\t"+"Iterating...");
		st.iterate();
		
		System.out.println("jist.plugins"+"\t"+"Writing...");
		String out = dir+"truthEstimate.raw";
		
		RawWriter.writeImgFloat(st.getTruthImage(), out);
		
		System.out.println("jist.plugins"+"\t"+"Finished!");
	}
	
	
	/*
	 * Single-label case
	 */
	public static ArrayList<double[][]> produceSyntheticRatersDX(int dim, float[] ps, float[] qs){
		ArrayList<double[][]> synthdata = new ArrayList<double[][]>(ps.length);
		
		
		
		return synthdata;
		
	}
	
	/*
	 * Single-label case
	 */
	public static ArrayList<ImageData> produceSyntheticRatersVol(int[] dim, float[] ps, float[] qs){
		ArrayList<ImageData>  synthdata = new ArrayList<ImageData> (ps.length);
		
		
		
		return synthdata;
		
	}

}
