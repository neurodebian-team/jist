package edu.jhu.ece.iacl.plugins.quantitative;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class EstimateT1FromDoubleAngle extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume lowAngleVolume;	// 3-D Volume containing low flip angle data
	private ParamVolume highAngleVolume;	// 3-D Volume containing high flip angle data
	private ParamFloat lowAngleDeg; // in degrees
	private ParamFloat highAngleDeg;// in degrees
	private ParamVolume relativeB1; // in fraction 
	private ParamFloat TRms;// in ms

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume T1MapVolume;	// Estimate T2 map

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Estimate T1 from a double flip-angle experiment.";
	private static final String longDescription = "Uses direct calculation. Corrects for spatially varying B1 field if the relative B1 map is specified.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Quantitative");
		inputParams.setLabel("T1:DoubleAngle");
		inputParams.setName("T1:DoubleAngle");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(lowAngleVolume=new ParamVolume("Low Angle Volume (3D)",null,-1,-1,-1,1));
		inputParams.add(highAngleVolume=new ParamVolume("High Angle Volume (3D)",null,-1,-1,-1,1));
		inputParams.add(lowAngleDeg=new ParamFloat("Low Flip Angle (deg)",15f));
		inputParams.add(highAngleDeg=new ParamFloat("High Flip Angle (deg)",60f));
		inputParams.add(TRms=new ParamFloat("Repetition Time (TR, ms)",100f));
		inputParams.add(relativeB1=new ParamVolume("Relative B1 Map (3D)",null,-1,-1,-1,1));
		relativeB1.setMandatory(false);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		T1MapVolume = new ParamVolume("T1 Estimate (ms)",VoxelType.FLOAT,-1,-1,-1,1);
		T1MapVolume.setName("T1Est");
		outputParams.add(T1MapVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {

		protected void execute() {
			this.setLabel("Estimating T1");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateT1FromDoubleAngle: Start");

			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageData lowAngleVol=(lowAngleVolume.getImageData());
			ImageData highAngleVol=(highAngleVolume.getImageData());
			ImageData b1Vol=relativeB1.getImageData();
			 
			float lowAngRad = (float)(lowAngleDeg.getFloat()*Math.PI/180f);
			float highAngRad = (float)(highAngleDeg.getFloat()*Math.PI/180f);
			float TR = TRms.getFloat();
			int r=lowAngleVol.getRows(), c=lowAngleVol.getCols(), s=lowAngleVol.getSlices(), t = lowAngleVol.getComponents();
			this.setTotalUnits(r);
			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateT1FromDoubleAngle: Allocating memory.");

			ImageData TEestimate = new ImageDataFloat(r,c,s,1);
			TEestimate.setName(lowAngleVol.getName()+"_quantT1");
			TEestimate.setHeader(lowAngleVol.getHeader());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			double num = -TR;
			double A = Math.sin(highAngRad);
			double B = Math.sin(lowAngRad);	
			double C = Math.cos(lowAngRad)*Math.sin(highAngRad);
			double D = Math.sin(lowAngRad)*Math.cos(highAngRad);

			for(int i=0;i<r;i++) {
				this.setCompletedUnits(i);
				for(int j=0;j<c;j++)
					for(int k=0;k<s;k++) {
						if(b1Vol!=null) {
							float b1 = b1Vol.getFloat(i,j,k);
							A = Math.sin(highAngRad*b1);
							B = Math.sin(lowAngRad*b1);
							C = Math.cos(lowAngRad*b1)*Math.sin(highAngRad*b1);
							D = Math.sin(lowAngRad*b1)*Math.cos(highAngRad*b1);
							
						}
						float S1 = lowAngleVol.getFloat(i,j,k);
						float S2 = highAngleVol.getFloat(i,j,k);
						double R = S2/S1;

						TEestimate.set(i,j,k,(float)(
								num/(Math.log(
										(A-R*B)/(C-R*D))
										)));
					}
			}

			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateT1FromDoubleAngle: Setting up exports.");
			T1MapVolume.setValue(TEestimate);

			System.out.println(getClass().getCanonicalName()+"\t"+"EstimateT1FromDoubleAngle: FINISHED");
		}
	}
}
