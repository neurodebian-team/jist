package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


/*
 *
 * @author John Bogovic
 * @author Bennett Landman
 * 
 * Retooled to use modern paramvolume syntax.
 *
 */
public class AlgorithmVolumeFileCollectionTo3D extends ProcessingAlgorithm{
	//output params
	private ParamVolumeCollection outputFiles;	// Files to be split

	//input params
	private ParamVolumeCollection inputFiles;		// Files to be split

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Extract 3D components of each 4D input volume. Organize the output as a VolumeCollection of 3D volumes.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("4D Volumes to 3D Volumes");
		inputParams.setName("4D_Volumes_to_3D_Volumes");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		// Input Parameters
		inputParams.add(inputFiles= new ParamVolumeCollection("Input Volume Files"));
		inputFiles.setLoadAndSaveOnValidate(false);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outputFiles = new ParamVolumeCollection("Output 3D Volume Files"));
		outputFiles.setLoadAndSaveOnValidate(false);
	}


	protected void execute(CalculationMonitor monitor) {
			for(ParamVolume v: inputFiles.getParamVolumeList()){
				
				JistLogger.logOutput(JistLogger.INFO, "Loading:"+v.getName());
				ImageData vol = v.getImageData();
				 extractAndWriteComponents(vol,outputFiles);
//			v.dispose();
//				vol.dispose(); //=null;
				 vol=null;
				v.dispose();
				inputFiles.dispose();
				System.gc();try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}System.gc();
				JistLogger.logMIPAVRegistry();
				/*for(ImageData d : thesefiles) {
					outputFiles.add(d);
				}
				outputFiles.writeAndFreeNow(this);*/
				
			}
	}


	private void extractAndWriteComponents(ImageData vol, ParamVolumeCollection outputFiles){
//        ArrayList<ImageData> listout = new ArrayList<ImageData>();
		for(int l=0; l<vol.getComponents(); l++){
			String num = "";
			if(l<10){
				num="0"+l;
			}else{
				num = ""+l;
			}
			ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_"+num,vol.getType(),vol.getRows(), vol.getCols(), vol.getSlices());
			nextvol.setHeader(vol.getHeader());
			System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
			for(int i=0;i<vol.getRows();i++){
				for(int j=0;j<vol.getCols();j++){
					for(int k=0;k<vol.getSlices();k++){

						nextvol.set(i, j, k, vol.get(i, j, k, l));
						
					}
				}
			}
			outputFiles.add(nextvol);
			outputFiles.writeAndFreeNow(this);
			nextvol.dispose();
			nextvol=null;
//			listout.add(nextvol);
		}

//		return listout;

	}

}
