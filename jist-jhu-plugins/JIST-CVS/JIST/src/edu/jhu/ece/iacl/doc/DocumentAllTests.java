package edu.jhu.ece.iacl.doc;

import java.awt.Color;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.io.ArrayObjectTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.ExecutionContext;
import edu.jhu.ece.iacl.jist.pipeline.JistPreferences;
import edu.jhu.ece.iacl.jist.pipeline.PerformanceSummary;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeConnector;
import edu.jhu.ece.iacl.jist.pipeline.PipeLayout;
import edu.jhu.ece.iacl.jist.pipeline.PipeModule;
import edu.jhu.ece.iacl.jist.pipeline.PipePort;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeAssertionTestDestination;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeExternalDestination;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeParamCopyDestination;
import edu.jhu.ece.iacl.jist.pipeline.gui.ProcessManager;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPoint;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceLocationCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
 * Automatically runs and documents all test cases
 * @author Blake Lucas
 *
 */
public class DocumentAllTests {
	/**
	 * Assertion description
	 * @author Blake
	 *
	 */
	protected static class Assertion implements Comparable<Assertion> {
		/**
		 * Name of assertion.
		 */
		String name;
		/**
		 * Location of assertion output description.
		 */
		File location;
		/**
		 * Flag indicating assertion evaluated to true.
		 */
		boolean passed;
		/**
		 * Constructor
		 * @param name name of assertion.
		 * @param location location of assertion of output.
		 * @param passed flag indicating assertion passed.
		 */
		public Assertion(String name, File location, boolean passed) {
			this.name = name;
			this.location = location;
			this.passed = passed;
		}
		/**
		 * Did the assertion pass?
		 * @return true if passed.
		 */
		public boolean didTestPass() {
			return passed;
		}
		/**
		 * Get location of assertion output.
		 * @return
		 */
		public File getLocation() {
			return location;
		}
		/**
		 * Get name of assertion.
		 * @return
		 */
		public String getName() {
			return name;
		}
		/**
		 * Compare assertions by name.
		 */
		public int compareTo(Assertion obj) {
			return this.name.compareTo(obj.name);
		}
	}
	/**
	 * Module description.
	 * @author Blake Lucas
	 *
	 */
	protected static class ModuleDescription implements
			Comparable<ModuleDescription> {
		/**
		 * Name of module.
		 */
		String name;
		/**
		 * Label for module.
		 */
		String label;
		/**
		 * Constructor
		 * @param name Name of module.
		 * @param label Label for module.
		 */
		public ModuleDescription(String name, String label) {
			this.name = name;
			this.label = label;
		}
		/**
		 * Get module label.
		 * @return
		 */
		public String getLabel() {
			return label;
		}
		/**
		 * Get module name.
		 * @return
		 */
		public String getName() {
			return name;
		}
		/**
		 * Compare modules by name.
		 */
		public int compareTo(ModuleDescription obj) {
			return this.label.compareTo(obj.label);
		}
	}
	/**
	 * Test case description.
	 * @author Blake Lucas
	 *
	 */
	protected static class TestCase {
		public enum Status {
			Failed, Error, Passed
		}

		protected long startTime, stopTime;
		protected String testName;
		protected int totalAssertions;
		protected int completedAssertions;
		protected Status status = Status.Passed;
		protected Vector<Assertion> assertions;
		protected Vector<ModuleDescription> modules;;
		protected File layoutFile;

		public TestCase(String testName, File layoutFile) {
			this.testName = testName;
			this.assertions = new Vector<Assertion>();
			this.modules = new Vector<ModuleDescription>();
			this.layoutFile = layoutFile;
		}

		public File getLayoutFile() {
			return layoutFile;
		}

		public void add(Assertion as) {
			assertions.add(as);
		}

		public void add(ModuleDescription mod) {
			modules.add(mod);
		}

		public List<Assertion> getAssertions() {
			return assertions;
		}

		public long getElapsedTime() {
			return (stopTime - startTime);
		}

		public String getElapsedTimeString() {
			String timeString = PerformanceSummary.formatTime(stopTime
					- startTime);
			return timeString;
		}

		public List<ModuleDescription> getModules() {
			return modules;
		}

		public String getName() {
			return testName;
		}

		public Status getStatus() {
			if (assertions.size() > 0) {
				boolean atleastOne = false;
				boolean all = true;
				for (Assertion as : assertions) {
					if (as.didTestPass()) {
						atleastOne = true;
					} else {
						all = false;
					}
				}
				if (all)
					return Status.Passed;
				if (atleastOne)
					return Status.Error;
				else
					return Status.Failed;
			} else {
				if (status != Status.Failed) {
					return Status.Passed;
				} else {
					return Status.Failed;
				}
			}
		}

		public void setCompletedAssertions(int completed) {
			this.completedAssertions = completed;
		}

		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		public void setStopTime(long stopTime) {
			this.stopTime = stopTime;
		}

		public void setTotalAssertions(int total) {
			this.totalAssertions = total;
		}
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		File testDirectory = null;
		File outputDirectory = null;
		File verifyDirectory = null;
		boolean runLayouts=true;
		if (args.length == 1) {
			testDirectory = new File(args[0]);
			outputDirectory = new File(args[0]);
			verifyDirectory = null;
		} else if (args.length == 2) {
			testDirectory = new File(args[0]);
			outputDirectory = new File(args[1]);
			verifyDirectory = null;
		} else if (args.length == 3) {

			testDirectory = new File(args[0]);
			outputDirectory = new File(args[1]);
			verifyDirectory = new File(args[2]);
		} else if(args.length==4){
			if(args[0].equalsIgnoreCase("-redo")){
				runLayouts=false;
				testDirectory = new File(args[1]);
				outputDirectory = new File(args[2]);
				verifyDirectory = new File(args[3]);
			} else {
				System.err
				.println("Usage: edu.jhu.ece.iacl.doc.DocumentAllTests -doc test_directory snapshot_directory verification_directory");
				System.exit(1);
			}
		} else {
			System.err
					.println("Usage: edu.jhu.ece.iacl.doc.DocumentAllTests test_directory output_directory verification_directory");
			System.err
					.println("Usage: edu.jhu.ece.iacl.doc.DocumentAllTests -redo test_directory snapshot_directory verification_directory");
			System.exit(1);
		}
		MipavController.setQuiet(true);
		MipavController.init();
		DocumentAllTests harness = new DocumentAllTests(testDirectory,
				outputDirectory, verifyDirectory);
		harness.init();
		harness.runTests(runLayouts,false);
		harness.complete();
		System.exit(0);
	}

	/** The layout files. */
	protected ArrayList<File> layoutFiles;

	/** The test directory. */
	protected File testDirectory;

	/** The output directory. */
	protected File outputDirectory;

	/** The verification directory */
	protected File verifyDirectory;

	/** The passed dir. */
	protected File passedDir;

	/** The failed dir. */
	protected File failedDir;

	/** The manager. */
	protected ProcessManager manager;

	/** The failed files. */
	protected ArrayList<File> failedFiles;

	/** The passed files. */
	protected ArrayList<File> passedFiles;

	/**
	 * Instantiates a new jist test harness.
	 * 
	 * @param testDirectory
	 *            the test directory
	 */
	public DocumentAllTests(File testDirectory, File outputDirectory,
			File verifyDirectory) {
		this.testDirectory = testDirectory;
		this.outputDirectory = outputDirectory;
		this.verifyDirectory = verifyDirectory;
	}

	/**
	 * Clean.
	 */
	/*
	public void clean() {
		for (File layoutFile : layoutFiles) {
			File outputDir = new File(layoutFile.getParent(), FileReaderWriter
					.getFileName(layoutFile));
			ExecutionContext.deleteDir(outputDir, null);
		}
	}
*/
	/**
	 * Complete.
	 */
	public void complete() {
		System.out.println(getClass().getCanonicalName()+"\t"+"PASSED: " + passedFiles.size() + " FAILED: "
				+ failedFiles.size() + " TOTAL: "
				+ (passedFiles.size() + failedFiles.size()));
		manager.forceQuit();
	}

	/**
	 * Discover layouts.
	 * 
	 * @param startDir
	 *            the start dir
	 * 
	 * @return the array list< file>
	 */
	protected ArrayList<File> discoverLayouts(File startDir) {
		LinkedList<File> dirs = new LinkedList<File>();
		ArrayList<File> layoutFiles = new ArrayList<File>();
		dirs.add(startDir);
		File d;
		while (dirs.size() > 0) {
			d = dirs.remove();
			if (d == null) {
				continue;
			}
			File[] files = d.listFiles();
			if (files == null) {
				continue;
			}
			for (File f : files) {
				if (!f.isHidden()) {
					if (f.isDirectory()) {
						// Add directory to search path
						dirs.add(f);
					} else {
						String ext = FileReaderWriter.getFileExtension(f);
						if (ext.equals(JistPreferences
								.getPreferences().getDefaultLayoutExtension())) {
							layoutFiles.add(f);
						}
					}
				}
			}
		}
		return layoutFiles;
	}

	/**
	 * Inits the.
	 */
	public void init() {
		layoutFiles = discoverLayouts(testDirectory);
		manager = new ProcessManager();
		failedFiles = new ArrayList<File>();
		passedFiles = new ArrayList<File>();

	}

	/**
	 * Run test.
	 * 
	 * @param layoutFile
	 *            the layout file
	 */
	public TestCase runTest(File layoutFile, File resultsDirectory,boolean clean) {
		PipeLayout layout = PipeLayout.read(layoutFile);
		String testName = FileReaderWriter.getFileName(layoutFile);
		File outputDir = new File(layoutFile.getParent(), testName);
		if (!outputDir.exists())
			outputDir.mkdir();
		//Run all pipelines single threaded
		//layout.getRunParameters().setMaxProcs(1);
		layout.getRunParameters().setOutputDirectory(outputDir);
		layout.save(false);
		System.out.println(getClass().getCanonicalName()+"\t"+"RUNNING " + layoutFile + " ...");
		TestCase test = new TestCase(testName.replace("_", " "), layoutFile);
		test.setStartTime(System.currentTimeMillis());
		boolean testCompleted = manager.runAndWait(layout, false);
		test.setStopTime(System.currentTimeMillis());
		for (PipeModule mod : layout.getAllDescendantPipes()) {
			if (mod instanceof PipeAlgorithm) {
				test.add(new ModuleDescription(mod.getName(), mod.getLabel()));
			}
		}
		//indicate whether test passed 
		if (!testCompleted) {
			test.setStatus(TestCase.Status.Failed);
		}
		boolean passed = true;
		File testResultFile = null;
		int totalDestinations = 0;
		int passedDestinations = 0;
		for (PipeModule mod : layout.getAllDescendantPipes()) {
			//Examine assertion destinations
			if (mod instanceof PipeAssertionTestDestination) {
				totalDestinations++;
				PipeAssertionTestDestination dest = ((PipeAssertionTestDestination) mod);
				passed = dest.didAllTestsPass();
				testResultFile = dest.getTestResultsFile();
				//Add assertion description
				test
						.add(new Assertion(dest.getLabel(), testResultFile,
								passed));
				if (passed) {
					passedDestinations++;
				} else {
					System.out.println(getClass().getCanonicalName()+"\t"+testName + ": TEST FAILED, SEE "
							+ testResultFile);
				}
			//Examine external destinations
			} else if (mod instanceof PipeExternalDestination) {
				totalDestinations++;
				PipeExternalDestination dest = ((PipeExternalDestination) mod);
				//Verify result
				passed = verify(dest, resultsDirectory, layoutFile);
				//Add assertion description
				test.add(new Assertion(dest.getLabel(), null, passed));
				if (passed) {
					passedDestinations++;
				}
			}
			test.setTotalAssertions(totalDestinations);
			test.setCompletedAssertions(passedDestinations);
		}

		if(clean)manager.cleanAllQuietly(layout);
		return test;
	}

	private boolean verify(PipeExternalDestination dest, File resultDirectory,
			File layoutFile) {
		try {
			ParamModel sourceMod = ((PipeExternalDestination) dest)
					.getOutputParams().getValue(0);
			boolean ret = true;
			if (sourceMod instanceof ParamSurfaceLocationCollection) {
				if(verifyDirectory==null)return true;
				ParamSurfaceLocationCollection mod = ((ParamSurfaceLocationCollection) sourceMod);
				LinkedList<EmbeddedSurface> verifySurfs = new LinkedList<EmbeddedSurface>();
				for (File f : mod.getValue()) {
					f = replacePath(f, resultDirectory, verifyDirectory);
					EmbeddedSurface surf = SurfaceReaderWriter.getInstance()
							.read(f);
					verifySurfs.add(surf);
				}
				int i = 0;
				for (File f : mod.getValue()) {
					EmbeddedSurface surfv = verifySurfs.get(i++);
					EmbeddedSurface surf = SurfaceReaderWriter.getInstance()
							.read(f);
					if (!verifySurface(surf, surfv)) {
						ret = false;
						break;
					}
				}
			}/* No longer exists
			 else if (sourceMod instanceof ParamVolumeLocationCollection) {
				if(verifyDirectory==null)return true;
				ParamVolumeLocationCollection mod = ((ParamVolumeLocationCollection) sourceMod);
				LinkedList<ImageData> verifyImages = new LinkedList<ImageData>();
				for (File f : mod.getValue()) {
					f = replacePath(f, resultDirectory, verifyDirectory);
					ImageData img = ImageDataReaderWriter.getInstance().read(
							f);
					verifyImages.add(img);
				}
				int i = 0;
				for (File f : mod.getValue()) {
					ImageData imgv = verifyImages.get(i++);
					ImageData img = ImageDataReaderWriter.getInstance().read(
							f);
					//Dispose of all images, but only verify until one image is found that doesn't verify
					if (ret&&!verifyImage(img, imgv)) {
						ret = false;
						img.dispose();
						imgv.dispose();
					} else {
						img.dispose();
						imgv.dispose();
					}
				}
			} */ else if (sourceMod instanceof ParamNumberCollection) {
				//Verify numerical result
				ParamNumberCollection mod = ((ParamNumberCollection) sourceMod);
				List<Number> nums = mod.getValue();
				Number[][] numlist = new Number[1][nums.size()];
				nums.toArray(numlist[0]);
				String fileName = edu.jhu.ece.iacl.jist.utility.FileUtil
						.forceSafeFilename(FileReaderWriter
								.getFileName(layoutFile)
								+ "-" + dest.getLabel())
						+ ".csv";
				File targetFile=new File(layoutFile.getParentFile(), fileName);
				ArrayObjectTxtReaderWriter.getInstance().write(numlist,targetFile);
				if(verifyDirectory==null)return true;
				Object[][] numsv = ArrayObjectTxtReaderWriter.getInstance()
						.read(replacePath(targetFile,resultDirectory, verifyDirectory));
				System.out.println(getClass().getCanonicalName()+"\t"+"Verify "+targetFile+" "+replacePath(targetFile,resultDirectory, verifyDirectory));
				if (numsv != null && numsv.length == 1) {
					if (numsv[0].length != numlist[0].length) {
						ret = false;
					} else {
						for (int i = 0; i < numsv[0].length; i++) {
							if (numsv[0][i] instanceof Number) {
								Number num1 = numlist[0][i];
								Number num2 = (Number) numsv[0][i];
								System.out.println(getClass().getCanonicalName()+"\t"+"Verify: " + num1
										+ " against " + num2);
								if (num1.doubleValue() != num2.doubleValue()) {
									ret = false;
									break;
								}
							} else {
								ret = false;
								break;
							}
						}
					}
				} else {
					ret = false;
				}
			} else {
				return false;
			}

			return ret;
		} catch (Exception e) {
			//Something broke while trying to verify result, indicate that result could not be verified.
			System.err.print(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	private boolean verifySurface(EmbeddedSurface surf1, EmbeddedSurface surf2) {
		if (surf1.getVertexCount() != surf2.getVertexCount()
				|| surf1.getIndexCount() != surf2.getIndexCount()) {
			System.err.println("jist.plugins"+"Structure doesn't match: ("
					+ surf1.getVertexCount() + "," + surf1.getIndexCount()
					+ ") not equal to (" + surf2.getVertexCount() + ","
					+ surf2.getIndexCount() + ")");
			return false;
		}
		int vertCount = surf1.getVertexCount();
		for (int i = 0; i < vertCount; i++) {
			Point3f p1 = surf1.getVertex(i);
			Point3f p2 = surf2.getVertex(i);
			if (!p1.equals(p2)) {
				System.err.println("jist.plugins"+"Vertices don't match: " + p1
						+ " not equal to " + p2);
				return false;
			}
		}
		int indexCount = surf1.getIndexCount();
		for (int i = 0; i < indexCount; i++) {
			if (surf1.getCoordinateIndex(i) != surf2.getCoordinateIndex(i)) {
				System.err.println("jist.plugins"+"Indicies don't match: "
						+ surf1.getCoordinateIndex(i) + " not equal to "
						+ surf2.getCoordinateIndex(i));
				return false;
			}
		}
		double[][] vertData1 = surf1.getVertexData();
		double[][] vertData2 = surf2.getVertexData();
		if ((vertData1 != null && vertData2 == null)
				|| (vertData1 == null && vertData2 != null))
			return false;
		if (vertData1 != null && vertData2 != null) {
			if (vertData1.length != vertData2.length)
				return false;
			for (int i = 0; i < vertData1.length; i++) {
				if (vertData1[i].length != vertData2[i].length) {
					System.err.println("jist.plugins"+"Vertex data lengths don't match: "
							+ vertData1[i].length + " not equal to "
							+ vertData2[i].length);
					return false;
				}
				for (int j = 0; j < vertData1[i].length; j++) {
					if (vertData1[i][j] != vertData2[i][j]) {
						System.err.println("jist.plugins"+"Vertex data doesn't match: "
								+ vertData1[i][j] + " not equal to "
								+ vertData2[i][j]);
						return false;
					}
				}
			}
		}
		double[][] faceData1 = surf1.getCellData();
		double[][] faceData2 = surf2.getCellData();
		if ((faceData1 != null && faceData2 == null)
				|| (faceData1 == null && faceData2 != null))
			return false;
		if (faceData1 != null && faceData2 != null) {
			if (faceData1.length != faceData2.length)
				return false;
			for (int i = 0; i < faceData1.length; i++) {
				if (faceData1[i].length != faceData2[i].length)
					return false;
				for (int j = 0; j < faceData1[i].length; j++) {
					if (faceData1[i][j] != faceData2[i][j])
						return false;
				}
			}
		}
		return true;
	}

	private boolean verifyImage(ImageData img1, ImageData img2) {
		if (img1.getType() != img2.getType()) {
			System.err.println("jist.plugins"+"Image types don't match: " + img1.getType()
					+ " not equal to " + img2.getType());
			return false;
		}
		if (img1.getRows() != img2.getRows()
				|| img1.getCols() != img2.getCols()
				|| img1.getSlices() != img2.getSlices()
				|| img1.getComponents() != img2.getComponents()) {
			System.err.println("jist.plugins"+"Dimensions don't match: (" + img1.getRows()
					+ "," + img1.getCols() + "," + img1.getSlices() + ","
					+ img1.getComponents() + ") not equal to ("
					+ img2.getRows() + "," + img2.getCols() + ","
					+ img2.getSlices() + "," + img2.getComponents() + ")");
			return false;
		}
		int rows = img1.getRows();
		int cols = img1.getCols();
		int slices = img1.getSlices();
		int comps = img1.getComponents();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < comps; l++) {
						if (img1.get(i, j, k, l).doubleValue() != img2.get(i,
								j, k, l).doubleValue()) {
							System.err.println("jist.plugins"+"Data doesn't match: (" + i
									+ "," + j + "," + k + "," + l + ") "
									+ img1.get(i, j, k, l).doubleValue()
									+ " not equal to "
									+ img2.get(i, j, k, l).doubleValue());
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private File replacePath(File file, File root, File newRoot) {
		return new File(file.getPath().replace(
				root.getPath() + File.separatorChar,
				newRoot.getPath() + File.separatorChar));
	}

	/**
	 * Run tests.
	 */
	public void runTests(boolean runLayouts,boolean clean) {
		StringBuffer bodyString = new StringBuffer();
		int counter = 0;
		int testCounter = 0;
		int passedCount = 0;
		int errorCount = 0;
		int failedCount = 0;
		long elapsedTime = 0;
		Date currentTime = new Date();
		bodyString
				.append("<HTML><HEAD><TITLE>Test Cases Summary</TITLE></HEAD>\n<BODY bgcolor=\"#FFFFFF\">\n"
						+ "<CENTER>\n<H1>JIST Test Case Summary</H1><BR>\n<HR size=1>\n");
		bodyString
				.append("<TABLE width=\"90%\" bgcolor=\"#FFFFFF\" cellspacing=0 border=1 bordercolor=\"#666666\" ><TR bgcolor=\"#000000\">"
						+ "<TD align=\"Center\"><font color=\"#FFFFFF\"><B>Id</B></font></font></TD>"
						+ "<TD align=\"Center\"><font color=\"#FFFFFF\"><B>Test Name</B></font></font></TD>"
						+ "<TD align=\"Center\"><font color=\"#FFFFFF\"><B>Last Modified</B></font></font></TD>"
						+ "<TD align=\"Center\"><font color=\"#FFFFFF\"><B>Execution Time</B></font></TD>"
						+ "<TD align=\"Center\"><font color=\"#FFFFFF\"><B>Assertions</B></font></TD>"
						+ "<TD align=\"Center\"><font color=\"#FFFFFF\"><B>Modules</B></font></TD></TR>\n");
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd-kkmmssz");
		//Change results directory to be SNAPSHOT directory.
		File resultsDirectory = (runLayouts) ? (new File(
				outputDirectory, File.separatorChar + "SNAPSHOT-"
						+ timeFormat.format(currentTime) + File.separatorChar))
				: outputDirectory;
		timeFormat = new SimpleDateFormat("h:mm a MM/dd/yy");

		for (File layoutFile : layoutFiles) {
			testCounter++;
			File newLayout = replacePath(layoutFile, testDirectory,
					resultsDirectory);
			if (!newLayout.getParentFile().exists()) {
				newLayout.getParentFile().mkdirs();
			}
			//Move layout to snapshot directory and run layout
			PipeParamCopyDestination.copyFile(layoutFile, newLayout);
			TestCase test = runTest(newLayout, resultsDirectory,clean);
			elapsedTime += test.getElapsedTime();
			String bgColor = "#FFFFFF";
			switch (test.getStatus()) {
			case Passed:
				bgColor = "#66FF66";
				passedCount++;
				break;
			case Failed:
				bgColor = "#FF6666";
				failedCount++;
				break;
			case Error:
				bgColor = "#FFFF66";
				errorCount++;
				break;
			}
			bodyString.append("<TR  bgcolor=\"" + bgColor + "\">"
					+ "<TD width=\"1%\" valign=\"top\" align=\"Center\"><B>"
					+ testCounter + "</B></TD>"
					+ "<TD width=\"15%\" valign=\"top\" align=\"Center\">"
					+ test.getName() + "</TD>\n"
					+ "<TD width=\"15%\" valign=\"top\" align=\"Center\">"
					+ timeFormat.format(layoutFile.lastModified()) + "</TD>\n");
			bodyString
					.append("<TD width=\"10%\" valign=\"top\" align=\"Center\">"
							+ test.getElapsedTimeString() + "</TD>\n");

			if (test.getAssertions().size() > 0) {
				counter = 0;
				bodyString
						.append("<TD width=\"30%\" valign=\"top\">"
								+ "\n<TABLE border=0 bordercolor=\"#FFFFFF\" width=\"100%\" borderwidth=1 cellspacing=2 cellpadding=0>\n");
				List<Assertion> assertions = test.getAssertions();
				Collections.sort(assertions);
				for (Assertion as : assertions) {
					counter++;
					bodyString.append("<TR bgcolor=\""
							+ ((as.didTestPass()) ? "#66FF66" : "#FF6666")
							+ "\"><TD><font size=2>" + counter + ". "
							+ as.getName() + "</font></TD></TR>\n");
				}
				bodyString.append("</TABLE>\n</TD>\n");
			} else {
				bodyString.append("<TD valign=\"top\">&nbsp;</TD>\n");
			}

			bodyString
					.append("<TD width=\"29%\" valign=\"top\">\n"
							+ "<TABLE border=0 bordercolor=\"#FFFFFF\" width=\"100%\" borderwidth=1 cellspacing=2 cellpadding=0>\n");
			counter = 0;
			List<ModuleDescription> modules = test.getModules();
			Collections.sort(modules);
			for (ModuleDescription mod : modules) {
				counter++;
				bodyString.append("<TR><TD><font size=2>" + counter + ". "
						+ mod.getLabel() + " (" + mod.getName()
						+ ") </font></TD></TR>\n");
			}
			bodyString.append("</TABLE>\n</TD>\n</TR>\n");

		}

		timeFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
		String time = timeFormat.format(currentTime);
		String username = System.getProperty("user.name");
		String timeStamp;
		if (username == null) {
			timeStamp = "Last modified " + time;
		} else {
			timeStamp = "Last modified by " + username + " on " + time;
		}

		bodyString
				.append("</TABLE>\n<HR size=1>\n"
						+ "<TABLE border=0 width=\"500px\">\n"
						+ "<TR><TD width=\"50%\"><B>Passed:</B></TD><TD width=\"50%\" align=\"Right\">"
						+ passedCount
						+ "</TD></TR>\n"
						+ "<TR><TD width=\"50%\"><B>Error:</B></TD><TD width=\"50%\" align=\"Right\">"
						+ errorCount
						+ "</TD></TR>\n"
						+ "<TR><TD width=\"50%\"><B>Failed:</B></TD><TD width=\"50%\" align=\"Right\">"
						+ failedCount
						+ "</TD></TR>\n"
						+ "<TR><TD width=\"50%\"><B>Total:</B></TD><TD width=\"50%\" align=\"Right\">"
						+ testCounter
						+ "</TD></TR>\n"
						+ "<TR><TD width=\"50%\"><B>Elapsed Time:</B></TD><TD width=\"50%\" align=\"Right\">"
						+ PerformanceSummary.formatTime(elapsedTime)
						+ "</TD></TR>\n"
						+ "<TR><TD width=\"50%\"><B>Output Directory:</B></TD><TD width=\"50%\" align=\"Right\"><font size=\"2\">"
						+ resultsDirectory.getName()
						+ "</font></TD></TR>\n"
						+ "<TR><TD width=\"50%\"><B>Verification Directory:</B></TD><TD width=\"50%\" align=\"Right\"><font size=\"2\">"
						+ ((verifyDirectory!=null)?verifyDirectory.getName():"&nbsp;")
						+ "</font></TD></TR>\n"
						+ "</TABLE>\n<HR size=1>\n<B><font size=\"2\">"
						+ timeStamp
						+ "</font></B></CENTER>\n"
						+ "</BODY></HTML>");
		StringReaderWriter.getInstance().write(
				bodyString.toString(),
				new File(resultsDirectory, File.separatorChar
						+ "all-tests.html"));
	}
}
