package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Jama.Matrix;
import edu.jhu.bme.smile.commons.math.MatrixMath;
import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.hardi.QBall;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;


public class QBallModifiedRealProjection extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamFileCollection sphericalHarmonicCoeffsReal;	// SLAB-enabled A 4D volume with one tensor estimated per pixel	
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions	
	private ParamBoolean outputProjection;	// whether or not to output the projections
	private ParamBoolean extractMax;	// whether or not to extract maximas
	private ParamFloat extractThresh;	// threshold for extraction of maximas

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFileCollection reconReal;	// SLAB-enabled A 4D volume with one tensor estimated per pixel	
	private ParamFileCollection basisIndecies;	// basis indecies of extracted maximas
	private ParamFileCollection basisMixtures;	// basis mixtures of extracted maximas

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Log-linear minimum mean squared error tensor estimation.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion.QBall");
		inputParams.setLabel("Q-Ball: Modified Real SH Projection");	
		inputParams.setName("Q-Ball:_Modified_Real_SH_Projection");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.add(new AlgorithmAuthor("Hanlin Wan","hanlinwan@gmail.com",""));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(sphericalHarmonicCoeffsReal = new ParamFileCollection("Spherical Harmonic Coefficients: Real",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(gradsTable=new ParamFile("Directions Along Which to Project the SH Model",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(outputProjection=new ParamBoolean("Export Projections?",false));
		inputParams.add(extractMax=new ParamBoolean("Extract Maximas?",true));
		inputParams.add(extractThresh=new ParamFloat("Minimum Value to be Considered at Maxima",(float).1));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/

		outputParams.add(reconReal = new ParamFileCollection("QBall Projection Real Part",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		reconReal.setMandatory(false);
		outputParams.add(basisIndecies=new ParamFileCollection("Basis Indecies",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		basisIndecies.setMandatory(false);
		outputParams.add(basisMixtures=new ParamFileCollection("Mixture Fraction",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		basisMixtures.setMandatory(false);
	}



	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to
		 * 			know when the bug happened.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Q-Ball Proj: Start");

		/****************************************************
		 * Step 2. Loop over input slabs
		 ****************************************************/
		List<File> shReal = sphericalHarmonicCoeffsReal.getValue();

		ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
		ArrayList<File> realOutVols = new ArrayList<File>();
		ArrayList<File> maxDirIndecies = new ArrayList<File>();
		ArrayList<File> maxDirMixtures = new ArrayList<File>();
		for(int jSlab=0;jSlab<shReal.size();jSlab++) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Starting Slab: "+jSlab);
			System.out.flush();
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			ImageData shRealData=rw.read(shReal.get(jSlab));
			String sourceName = shRealData.getName();
			ImageDataFloat shRealFloat=new ImageDataFloat(shRealData);
			shRealData.dispose();
			shRealData=null;

			float [][]grads=null;
			TextFileReader text = new TextFileReader(gradsTable.getValue());
			try {
				grads  = text.parseFloatFile();
			} catch (IOException e) { 
				throw new RuntimeException("QBall: Unable to parse grad-file");
			}

			/****************************************************
			 * Step 3. Perform limited error checking
			 ****************************************************/
			// If there are 4 columns in the gradient table, remove the 1st column (indecies)
			if(grads[0].length==4) {
				float [][]g2 = new float[grads.length][3];
				for(int i=0;i<grads.length;i++)
					for(int j=0;j<3;j++)
						g2[i][j]=grads[i][j+1];
				grads=g2;
			}

			if(grads[0].length!=3)
				throw new RuntimeException("QBall: Invalid gradient table. Must have 3 or 4 columns.");

			int SHorder = QBall.getOrderFromNumberOfCoeff(shRealFloat.getComponents());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			int rows = shRealFloat.getRows();
			int cols = shRealFloat.getCols();
			int slices = shRealFloat.getSlices();
			int dirs = grads.length;
			
			float [][][][]reconReal = new float[rows][cols][slices][dirs];


			QBall.projectRealSphericalHarmonics(reconReal,
					shRealFloat.toArray4d(),
					grads,SHorder);
			/****************************************************
			 * Step 5.	Extract the maximas.
			 ****************************************************/
			int maxDirInd[][][][] = null;
			float maxDirMix[][][][] = null;

			if(extractMax.getValue()) {
				int nOut=10;
				int neighborDir=dirs/20;
				maxDirInd = new int[rows][cols][slices][nOut];
				maxDirMix = new float[rows][cols][slices][nOut];
				float thresh = extractThresh.getFloat();
				// determine closest directions to a certain direction
				ArrayList<ArrayList<float[]>> angles=new ArrayList<ArrayList<float[]>>(dirs);
				for (int v=0; v<dirs; v++) {
					Matrix v1=new Matrix(new double[]{grads[v][0],grads[v][1],grads[v][2]},3);
					ArrayList<float[]> innerAngles=new ArrayList<float[]>(dirs);
					for (int w=0; w<dirs; w++) {
						Matrix v2=new Matrix(new double[]{grads[w][0],grads[w][1],grads[w][2]},3);
						float angle=(float) (Math.min(Math.acos(MatrixMath.dotProduct(v1,v2)), Math.acos(MatrixMath.dotProduct(v1,v2.times(-1))))*180/Math.PI);
						innerAngles.add(new float[]{90-angle,w});
					}
					Collections.sort(innerAngles,new dCompare());
					angles.add(innerAngles);
				}

				//iterate through each voxel
				for (int i=0; i<rows; i++) {
					for (int j=0; j<cols; j++) {
						for (int k=0; k<slices; k++) {
							if (!Float.isNaN(reconReal[i][j][k][0])) {
								int count=0;
								int maximas[] = new int[nOut];	//create maximas array which will store the extracted maximas
								Arrays.fill(maximas, -1);
								ArrayList<float[]> directions = new ArrayList<float[]>(dirs);	//directions array sorts the projected results
	
								for (int d=0; d<dirs; d++)
									directions.add(new float[]{reconReal[i][j][k][d],d});
								Collections.sort(directions, new dCompare());
	
								ArrayList<Boolean> dirEval = new ArrayList<Boolean>(dirs);	//dirEval keeps track of the directions that have been processed
								for (int d=0; d<directions.size(); d++)
									dirEval.add(false);
	
								for (int g=0; g<directions.size(); g++) {
									int loc=(int)directions.get(g)[1];
									if (directions.get(g)[0]<thresh) break;
									if (!dirEval.get(loc)) {
										dirEval.set(loc,true);
										boolean max=true;
										//compare against neighboring directions to see if it's the maximum
										for (int v=0; v<neighborDir; v++) {
											if (reconReal[i][j][k][loc]-reconReal[i][j][k][(int)angles.get((int)directions.get(g)[1]).get(v)[1]]<0)
												max=false;
											else
												dirEval.set((int)angles.get((int)directions.get(g)[1]).get(v)[1],true);
										}
										if (max) {
											maximas[count++]=loc;
											if (count==nOut)
												break;
										}
									}
								}
								//store top nOut number of maximas and fill any remaining ones with NaN
								for (int v=0; v<nOut; v++) {
									maxDirInd[i][j][k][v]=maximas[v];
									if (maximas[v]==-1)
										maxDirMix[i][j][k][v]=Float.NaN;
									else
										maxDirMix[i][j][k][v]=reconReal[i][j][k][maximas[v]];
								}
							}
							else {
								for (int v=0; v<nOut; v++) {
									maxDirMix[i][j][k][v]=Float.NaN;
									maxDirInd[i][j][k][v]=-1;
								}
							}
						}
					}
				}
			}

			/****************************************************
			 * Step 6. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			ImageData out;
			File outputSlab;
			ImageHeader head = shRealFloat.getHeader();
			shRealFloat.dispose();
			shRealFloat=null;
			
			if(outputProjection.getValue()) {
				out=(new ImageDataFloat(reconReal)); 
				reconReal=null;
				out.setHeader(head);
				out.setName(sourceName+"_QBallProjReal");		
				outputSlab = rw.write(out, getOutputDirectory());
				realOutVols.add(outputSlab);
				out.dispose();
			}
			
			if(extractMax.getValue()) {
				out= (new ImageDataInt(maxDirInd));
				out.setHeader(head);
				out.setName(sourceName+"_maxDirIndecies");		
				outputSlab = rw.write(out, getOutputDirectory());
				maxDirIndecies.add(outputSlab);
				out.dispose();
				out=null;		

				out= (new ImageDataFloat(maxDirMix));
				out.setHeader(head);
				out.setName(sourceName+"_maxDirMixtures");		
				outputSlab = rw.write(out, getOutputDirectory());
				maxDirMixtures.add(outputSlab);
				out.dispose();
				out=null;	
			}
		}

		reconReal.setValue(realOutVols);

		/****************************************************
		 * Step 7. Let the user know that your code is finished.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"QBall: FINISHED");
	}

	private class dCompare implements Comparator<float[]> {
		public int compare(float[] o1, float[] o2) {
			if (o1[0]<o2[0])
				return 1;
			else if (o1[0]>o2[0])
				return -1;
			else
				return 0;
		}
	}
}
