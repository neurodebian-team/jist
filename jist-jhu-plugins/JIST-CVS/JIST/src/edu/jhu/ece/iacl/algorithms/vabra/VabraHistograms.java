package edu.jhu.ece.iacl.algorithms.vabra;

import java.util.ArrayList;
import java.util.List;

import edu.jhmi.rad.medic.libraries.ImageFunctionsPublic;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities.InterpolationType;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamWeightedVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;

public class VabraHistograms extends AbstractCalculation{
	public int[][] defSxPlus;
	public int[][][] defSTxPlus;
	public int[][] defSxMinus;
	public int[][][] defSTxMinus;
	public int[][] defSyPlus;
	public int[][][] defSTyPlus;
	public int[][] defSyMinus;
	public int[][][] defSTyMinus;
	public int[][] defSzPlus;
	public int[][][] defSTzPlus;
	public int[][] defSzMinus;
	public int[][][] defSTzMinus;

	
	
	public int[][] origDeformedSubject;
	public int[][] origTarget;
	public int[][][] origJointST;
	public int[][] currentDeformedSubject;
	public int[][] currentTarget;
	public int[][][] currentJointST;
	
	static public int defaultBins = 64;
	
	private int numOfBins;
	private int numOfCh;
	
	
	
	public VabraHistograms(int channels, int binSize, AbstractCalculation parent) {
		super(parent);
		
		numOfCh = channels;
		numOfBins = binSize;

		allocateHistograms();

	}
	
	void copyOrigHistograms() {
		defSxPlus = ImageData.clone(origDeformedSubject);
		defSxMinus = ImageData.clone(origDeformedSubject);
		defSyPlus = ImageData.clone(origDeformedSubject);
		defSyMinus = ImageData.clone(origDeformedSubject);
		defSzPlus = ImageData.clone(origDeformedSubject);
		defSzMinus = ImageData.clone(origDeformedSubject);

		defSTxPlus = ImageData.clone(origJointST);
		defSTxMinus = ImageData.clone(origJointST);
		defSTyPlus = ImageData.clone(origJointST);
		defSTyMinus = ImageData.clone(origJointST);
		defSTzPlus = ImageData.clone(origJointST);
		defSTzMinus = ImageData.clone(origJointST);
	}
	
	private void allocateHistograms() {

		defSxPlus = new int[numOfCh][numOfBins];
		defSyPlus = new int[numOfCh][numOfBins];
		defSzPlus = new int[numOfCh][numOfBins];

		defSxMinus = new int[numOfCh][numOfBins];
		defSyMinus = new int[numOfCh][numOfBins];
		defSzMinus = new int[numOfCh][numOfBins];

		defSTxPlus = new int[numOfCh][numOfBins][numOfBins];
		defSTyPlus = new int[numOfCh][numOfBins][numOfBins];
		defSTzPlus = new int[numOfCh][numOfBins][numOfBins];

		defSTxMinus = new int[numOfCh][numOfBins][numOfBins];
		defSTyMinus = new int[numOfCh][numOfBins][numOfBins];
		defSTzMinus = new int[numOfCh][numOfBins][numOfBins];
		
		origDeformedSubject = new int[numOfCh][numOfBins];
		origTarget = new int[numOfCh][numOfBins];
		origJointST = new int[numOfCh][numOfBins][numOfBins];
		
		currentDeformedSubject = new int[numOfCh][numOfBins];
		currentTarget = new int[numOfCh][numOfBins];
		currentJointST = new int[numOfCh][numOfBins][numOfBins];
	}
	
	public void dispose(){
		origDeformedSubject = null;
		origTarget = null;
		origJointST = null;

		// used in optimization -- perhaps move to child class
		currentDeformedSubject = null;
		currentTarget = null;
		currentJointST = null;
	}

	public void adjustBins(int[][] subjectHist, int[][][] jointHist, int subBin, int tarBin, int newBin, int ch){
		subjectHist[ch][newBin] += 1;
		subjectHist[ch][subBin] -= 1;
		jointHist[ch][newBin][tarBin] += 1;
		jointHist[ch][subBin][tarBin] -= 1;
	}
	
	public void updateHistograms(VabraVolumeCollection normedTarget, VabraVolumeCollection normedDeformedSubject, int[] boundingBox) {
	    // System.out.println(getClass().getCanonicalName()+"\t"+"UPDATE HISTOGRAMS ");
		if (origDeformedSubject == null) {
			System.out.format("null Histograms");
			// initializeHistograms();
		}
		for (int ch = 0; ch < numOfCh; ch++) {
			RegistrationUtilities.Histogram3D(normedDeformedSubject.data, ch, numOfBins, boundingBox, origDeformedSubject);
			RegistrationUtilities.Histogram3D(normedTarget.data, ch, numOfBins, boundingBox, origTarget);
			RegistrationUtilities.JointHistogram3D(normedDeformedSubject.data,normedTarget.data, ch, 
					numOfBins, boundingBox, origJointST);
		}
	}
	
}
