package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

public class Volume4Dmean extends ProcessingAlgorithm {
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume inputvol;		// Files to be averaged

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume outputFile;	// Averaged 3D volume


	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Takes the (time-)average of a 4D volume";
	private static final String longDescription = "Calculates the mean value of all components for each voxel in a 4D volume, and returns the mean 3D volume.";

	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("UMCU");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Volume4Dmean");
		inputParams.setName("Volume4Dmean");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("UMC Utrecht");
		info.add(new AlgorithmAuthor("Daniel Polders", "daniel.polders@gmail.com", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription +"\n"+ longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(inputvol= new ParamVolume("Input Volume"));
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputParams.add(outputFile = new ParamVolume("Output Combined Slabs"));
	}


	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}
	
	
	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {

			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 ****************************************************/
			this.setLabel("Initializing");
			JistLogger.logOutput(4, "Volume4D: Start");
			
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			ImageData vol = inputvol.getImageData();
			VoxelType type = vol.getType();
			int r = vol.getRows(); int c = vol.getCols(); int s = vol.getSlices(); int t = vol.getComponents();
			this.setTotalUnits(s);

			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/
			ImageData outputvol = new ImageDataDouble(r,c,s,1);
			outputvol.setName(vol.getName()+"_mean");
			outputvol.setHeader(vol.getHeader());
			double tot;
			
			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			for(int i =0; i<r; i++){
				for(int j=0; j<c; j++){
					for(int k=0; k<s; k++){
						tot = 0;
						for(int l=0; l<t; l++){
							tot += vol.getDouble(i, j, k, l);
						}
						outputvol.set(i, j, k, (tot/(t+1)));						
					}
				}
				this.setCompletedUnits(i);
			}

			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			this.setLabel("Saving Volume");
			outputFile.setValue(outputvol);
		}
	}
}

	
