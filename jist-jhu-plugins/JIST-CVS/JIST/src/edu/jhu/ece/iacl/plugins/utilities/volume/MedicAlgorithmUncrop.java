package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.util.List;

import javax.vecmath.Point3i;

import edu.jhmi.rad.medic.utilities.CropParameters;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhmi.rad.medic.utilities.CubicVolumeCropper;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


/*
 * Utitlity for uncropping images by adding given rows,columns and slices to original image
 *
 * @author Navid Shiee
 *
 */
public class MedicAlgorithmUncrop extends ProcessingAlgorithm{
	private ParamVolumeCollection inputVol;
	private ParamVolumeCollection outputVol;
	private ParamPointInteger offset;
	private ParamPointInteger dim;
	private ParamDouble BackGround;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Uncrop a volume to the input \"Dimensions\" given the input \"Offset\"." +
			"  Can be used to restore a volume to the size of a volume before the Crop module was applied.  The \"Dimensions\" input" +
			" to this module should come from the output \"Original Dimensions\" of the Crop module";
	private static final String longDescription = "";


	/*
	 * Create input parameters for Uncropper :cubic volume,number of rows,
	 * columns and slices need to be added on each side and
	 * BackGround value
	 */
	protected void createInputParameters(ParamCollection inputParams){
		inputVol = new ParamVolumeCollection("Input Volumes");

		inputParams.add(inputVol);
		offset=new ParamPointInteger("Offset");
		dim=new ParamPointInteger("Dimensions",new Point3i(256,256,198));
		inputParams.add(offset);
		inputParams.add(dim);
		BackGround = new ParamDouble("Background Value", 0.0, Double.MAX_VALUE);
		BackGround.setValue(0.0);
		inputParams.add(BackGround);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Uncrop");
		inputParams.setName("Uncrop");

		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Navid Shiee","","iacl.ece.jhu.edu"));
		info.setWebsite("iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	/*
	 * Create output parameters for Image UnCropper: cubic volume
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputVol = new ParamVolumeCollection("Uncropped Images");
		outputVol.setLabel("Uncropped Image");
		outputVol.setName("uncropped");
		outputParams.add(outputVol);
	}


	/*
	 * Exectue Image UnCropper
	 */
	protected void execute(CalculationMonitor monitor) {
		CubicVolumeCropper uncrop = new CubicVolumeCropper();
		monitor.observe(uncrop);
		List<ImageData> vols=inputVol.getImageDataList();
		Point3i d=dim.getValue();
		Point3i off=offset.getValue();
		if(vols.size()>0){
			ImageData vol=vols.get(0);
			CropParameters params=new CropParameters(d.x,d.y,d.z,vol.getComponents(),off.x,vol.getRows()+off.x-1,off.y,vol.getCols()+off.y-1,off.z,vol.getSlices()+off.z-1,BackGround.getDouble());
			List<ImageData> result=uncrop.uncrop(vols, params);
			int i=0;
			for(ImageData r:result){
				r.setName(vols.get(i++).getName()+"_uncrop");
			}
			outputVol.setValue(result);
		}
	}
}
