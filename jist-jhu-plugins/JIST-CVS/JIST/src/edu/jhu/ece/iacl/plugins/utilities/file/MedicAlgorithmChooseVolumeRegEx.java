package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


public class MedicAlgorithmChooseVolumeRegEx extends ProcessingAlgorithm{
	//input Params
	ParamString regEx;
	ParamVolumeCollection candidateFiles;
	ParamVolume targetFile;

	//output Params
	ParamVolumeCollection filesout;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Choose candidates from a Volume Collection using a Regular Expression.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(regEx=new ParamString("Regular Expression"));

		inputParams.add(candidateFiles=new ParamVolumeCollection("Candidate Volumes"));
		inputParams.add(targetFile=new ParamVolume("Target Volumes"));
		targetFile.setMandatory(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Volume Chooser (RegEx)");
		inputParams.setName("Volume_Chooser_RegEx");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		filesout = new ParamVolumeCollection("Matching Volumes");
		outputParams.add(filesout);

	}


	protected void execute(CalculationMonitor monitor) {
//		System.out.println(getClass().getCanonicalName()+"\t"+targetFile.getValue().getName());
//		System.out.println(getClass().getCanonicalName()+"\t"+candidateFiles.getValue().getName());
		List<String> imagenames = candidateFiles.getImageNames();

		System.out.println(getClass().getCanonicalName()+"\t"+"Finding matches...");
		if(targetFile.getValue()==null){
			filesout.setValue(matchFile(regEx.getValue(),candidateFiles.getImageDataList()));
		}else{
			filesout.setValue(matchFile(regEx.getValue(),targetFile.getValue(),candidateFiles.getImageDataList()));
		}
		System.out.println(getClass().getCanonicalName()+"\t"+filesout.getValue());
	}


	public ArrayList<ImageData> matchFile(String expression, File target, List<ImageData> toMatch){

		Pattern p = Pattern.compile(expression);
		Matcher m = p.matcher(target.getName());
		m.find();
		String tagExp = target.getName().substring(m.start(), m.end());

		Pattern pt = Pattern.compile(tagExp);

		ArrayList<ImageData> matches = new ArrayList<ImageData>();

		for(ImageData s : toMatch){
			Matcher mat = pt.matcher(s.getName());
			if(mat.find()){
				matches.add(s);
			}
		}

		return matches;

	}


	public ArrayList<ImageData> matchFile(String expression,List<ImageData> toMatch){
		Pattern pt = Pattern.compile(expression);
		ArrayList<ImageData> matches = new ArrayList<ImageData>();
		for(ImageData s : toMatch){
			Matcher mat = pt.matcher(s.getName());
			if(mat.find()){
				matches.add(s);
			}
		}
		return matches;
	}
}
