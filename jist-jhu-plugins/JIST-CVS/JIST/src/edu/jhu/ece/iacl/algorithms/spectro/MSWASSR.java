package edu.jhu.ece.iacl.algorithms.spectro;

import edu.jhu.bme.smile.commons.math.Spline;
import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;
import edu.jhu.bme.smile.commons.optimize.FunctionNumeric1DDifferentiation;
import edu.jhu.bme.smile.commons.optimize.GoldenSectionSearch1D;
import edu.jhu.bme.smile.commons.optimize.NewtonMethod1D;
import edu.jhu.bme.smile.commons.optimize.Optimizable1DContinuous;
import edu.jhu.bme.smile.commons.optimize.Optimizer1DContinuous;
import edu.jhu.bme.smile.commons.optimize.Optimizer1DContinuousDifferentiable;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

public class MSWASSR {

	/**
	 * function centerFreq = MSWASSR(X,y)
		% Find point of maximum symmetry in graph of X and Y
		% w0= SPWASSR(X,y);
		yy=y-min(y);
		w0 = mean(X(find(yy<median(yy)/2)))+(rand()-.5)/1e3;

		centerFreq =fminsearch(@(x) symscore(X,y(:),x(:)),2*w0(1))/2;

	 * @return
	 */
	public double centerFreq(double []x, double []y) {
		Symscore fun = new Symscore(x,y);
		if(true) {
			Optimizer1DContinuous opt = new BrentMethod1D();
			opt.initialize(fun);		
			opt.optimize(true);

			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t val:"+fun.getValue(opt.getExtrema()));
			return opt.getExtrema()/2.;
		} else {
			Optimizer1DContinuousDifferentiable opt = new NewtonMethod1D();
			opt.initialize(new FunctionNumeric1DDifferentiation(fun,fun.getDomainTolerance(),3));		
			opt.optimize(true);

			//			System.out.println("val:"+fun.getValue(opt.getExtrema()));
			return opt.getExtrema()/2.;
		}

	}

	/**
	 * function score = symscore(X,y,c)		

		    yc=interp1(c-X,y,X,'v5cubic');
		    score = meannan(((y(:)-yc(:)).^2));


	 * @author bennett
	 *
	 */
	public class Symscore implements Optimizable1DContinuous
	{

		Spline fun;
		double []x;
		double []y;
		//		double []yc;
		double max,min,tol;

		public Symscore(double []x, double []y) {
			this.x=x;
			this.y=y;
			fun = new Spline(x,y);			
			max=-Double.MAX_VALUE;
			min = Double.MAX_VALUE;
			for(int i=0;i<x.length;i++) {
				max = (max<x[i]?x[i]:max);
				min = (min>x[i]?x[i]:min);
			}		
			tol = (max-min)*1e-6;
			/*max*=2;
			if(min<0)
				min*=2;
			else 
				min/=2;*/
		}

		@Override
		public double getDomainMax() {
			return max;
		}

		@Override
		public double getDomainMin() {
			return min;
		}

		@Override
		public double getDomainTolerance() {
			return tol;
		}

		@Override
		public double getValue(double c) {
			//  	yc=interp1(c-X,y,X,'v5cubic');
			//		score = meannan(((y(:)-yc(:)).^2));
			double err = 0, e;
			int cnt =0;
			for(int i=0;i<x.length;i++) {
				double v= fun.spline_value(c-x[i]);
				//				System.out.println((c-x[i])+" "+v);
				e = y[i]-v;
				if(Double.isInfinite(e) || Double.isNaN(e))
					continue;
				err+=e*e;
				cnt++;
			}
			if(cnt<x.length/2) {
				return Double.MAX_VALUE;
			}
			err= err/cnt;
			//			System.out.println(err+" "+c);
			if(Double.isNaN(err))
				return Double.MAX_VALUE;
			else 
				return err;
		}		
	}

}
