package edu.jhu.ece.iacl.doc;

import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;

public abstract class DocumentGenerator {
	public String createDocument(ProcessingAlgorithm alg){
		StringBuffer document=new StringBuffer();
		document.append(createHeader(alg));
		document.append(createShortDescription(alg));
		document.append(createLongDescription(alg));
		document.append(createInputInformation(alg));
		document.append(createOutputInformation(alg));
		document.append(createAuthors(alg));
		document.append(createCitations(alg));
		document.append(createFooter(alg));
		return document.toString();
	}
	
	protected abstract StringBuffer createHeader(ProcessingAlgorithm alg);
	protected abstract StringBuffer createShortDescription(ProcessingAlgorithm alg);
	protected abstract StringBuffer createAuthors(ProcessingAlgorithm alg);
	protected abstract StringBuffer createCitations(ProcessingAlgorithm alg);
	protected abstract StringBuffer createInputInformation(ProcessingAlgorithm alg);
	protected abstract StringBuffer createOutputInformation(ProcessingAlgorithm alg);
	protected abstract StringBuffer createLongDescription(ProcessingAlgorithm alg);
	protected abstract StringBuffer createFooter(ProcessingAlgorithm alg);
}
