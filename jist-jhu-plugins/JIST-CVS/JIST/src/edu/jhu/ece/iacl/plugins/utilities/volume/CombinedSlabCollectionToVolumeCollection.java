package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class CombinedSlabCollectionToVolumeCollection extends ProcessingAlgorithm {
	//output params
	private ParamVolumeCollection outputFiles;		// Slabs
	private ParamVolume output4d;

	//input params
	private ParamVolumeCollection inputFiles;		// Slabs


	private ParamBoolean output4D;

	//Variables
	ArrayList<File> outVols;
	File outDir;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.9 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts a 4D Slab Collection produced with \"VolCollect_to_Combined4DSlabCollect\" " +
			"to a VolumeCollection.  Can optionally produce a single 4D volume with the 4th dimension containing the original volumes.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Combined4DSlabCollect to VolCollect");
		inputParams.setName("Combined4DSlabCollect_to_VolCollect");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		// Input Parameters
		inputParams.add(inputFiles= new ParamVolumeCollection("Input Slab Collection"));
		inputFiles.setLoadAndSaveOnValidate(false);
		inputParams.add(output4D = new ParamBoolean("Use 4D Output",true));
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outputFiles = new ParamVolumeCollection("Output FileVolume Collection"));
		outputFiles.setLoadAndSaveOnValidate(false);
		outputFiles.setMandatory(false);
		outputParams.add(output4d = new ParamVolume("4D Output (optional)"));
		output4d.setMandatory(false);
	}


	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		outDir = this.getOutputDirectory();//MipavController.getDefaultWorkingDirectory();
		boolean use4D = output4D.getValue();
		// First need to count the number of volumes (for output) and the total slices in slabs (for output size)\
		int nSlices=0;
		int nVols=0;
		int nRows=0;
		int nCols=0;
		int nComp=0;
		String baseName = null;
		List<ParamVolume> pvlist = inputFiles.getParamVolumeList();
		for(ParamVolume pv: pvlist){
			ImageData vol = pv.getImageData();
			if(baseName==null)
				baseName = vol.getName().replace("_Slab00", "");
			nRows=vol.getRows();
			nCols=vol.getCols();
			nVols=vol.getComponents();
			nSlices+=vol.getSlices();
			vol.dispose();
			vol=null;
			pv.dispose();
		}
		if(use4D){
			nComp=nVols;
			nVols=1;
		} else
			nComp=1;


		// Now construct the output
		outVols = new ArrayList<File>();
		ImageData outputVol=null;
		String basename=null;
		for(int jVol=0;jVol<nVols;jVol++) {
			basename = null;
			int lastSlabEndSlice=-1;
			int startSlice=0, endSlice=0;
			outputVol=null;
			for(ParamVolume pv: pvlist){
				ImageData vol = pv.getImageData();
				VoxelType type = vol.getType();
				if(basename==null) {
					basename=vol.getName().replace("_Slab00", "");
					if (type.name().compareTo("UBYTE")==0)
						outputVol=new ImageDataUByte(nRows,nCols,nSlices,nComp);
					else if (type.name().compareTo("FLOAT")==0)
						outputVol=new ImageDataFloat(nRows,nCols,nSlices,nComp);
					else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
						outputVol=new ImageDataInt(nRows,nCols,nSlices,nComp);
					else
						outputVol=new ImageDataDouble(nRows,nCols,nSlices,nComp);
					outputVol.setName(baseName+"_Vol_"+jVol);
					outputVol.setHeader(vol.getHeader());
					System.out.println(getClass().getCanonicalName()+"\t"+"Image Type: "+outputVol.getType().name());
					System.out.flush();
				}

				startSlice = lastSlabEndSlice+1;
				endSlice = startSlice+vol.getSlices()-1;
				lastSlabEndSlice=endSlice;
				if(use4D){
					System.out.println("endSlice: " +endSlice);
					System.out.println("slices Here: " + (endSlice-startSlice+1));
					System.out.println("outputvol: " +outputVol.getSlices());
					System.out.println("vol: " +vol.getSlices());
					for(int i=0;i<vol.getRows();i++)
						for(int j=0;j<vol.getCols();j++) {
							int jSlice=0;
							for(int k=startSlice;k<=endSlice;k++) {
								for(int kComp=0;kComp<vol.getComponents();kComp++) {
									if (type.name().compareTo("UBYTE")==0)
										outputVol.set(i,j,k,kComp,vol.getUByte(i, j, jSlice, kComp));
									else if (type.name().compareTo("FLOAT")==0)
										outputVol.set(i,j,k,kComp,vol.getFloat(i, j, jSlice, kComp));
									else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
										outputVol.set(i,j,k,kComp,vol.getInt(i, j, jSlice, kComp));
									else
										outputVol.set(i,j,k,kComp,vol.getDouble(i, j, jSlice, kComp));
								}
								jSlice++;
							}
						}
				}else{
					System.out.println(vol.getSlices() + " " + endSlice);
					for(int i=0;i<vol.getRows();i++)
						for(int j=0;j<vol.getCols();j++) {
							int jSlice=0;
							for(int k=startSlice;k<=endSlice;k++) {
								if (type.name().compareTo("UBYTE")==0)
									outputVol.set(i,j,k,0,vol.getUByte(i, j, jSlice, jVol));
								else if (type.name().compareTo("FLOAT")==0)
									outputVol.set(i,j,k,0,vol.getFloat(i, j, jSlice, jVol));
								else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
									outputVol.set(i,j,k,0,vol.getInt(i, j, jSlice, jVol));
								else
									outputVol.set(i,j,k,0,vol.getDouble(i, j, jSlice, jVol));
								jSlice++;
							}
						}
				}
				vol.dispose();
				vol=null;
				pv.dispose();
			}
			if(!use4D) {
				outputFiles.add(outputVol);
				outputFiles.writeAndFreeNow(this);
				outputVol.dispose();
				outputVol=null;
			}
		}

		if(use4D && outputVol!=null) {
			outputVol.setName(basename + "_4D");
			outputFiles.add(outputVol);
			outputFiles.writeAndFreeNow(this);
			outputVol.dispose();
			outputVol=null;
		}
	}
}
