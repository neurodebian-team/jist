/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.roi;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.InvalidParameterException;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ObjectCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumber;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import gov.nih.mipav.model.algorithms.Statistics;


/*
 * @author Daniel Polders (zigurana@gmail.com)
 * This is an extension of Bennets origional AlgorithmROIStats. It now allows multiple ROIs
 * defined as mask integers, and will give results in collections rather then numbers.
 * ~Aug 17th 2010
 */
public class AlgorithmROIRangeStats extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	ParamVolume volumeParam;
	ParamVolume maskParam;
	ParamInteger maskIDmin;
	ParamInteger maskIDmax;
	ParamInteger histogramBins;
	ParamFloat histmin;
	ParamFloat histmax;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	ParamNumberCollection CollMean;
	ParamNumberCollection CollMedian;
	ParamNumberCollection CollStd;
	ParamNumberCollection CollMax;
	ParamNumberCollection CollMin;
	ParamNumberCollection CollVoxels;
	ParamNumberCollection CollMaskID;
	ParamString resultFileName;
	ParamObject<double[][]> ResultHistogram;

	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Calculate ROI-based statistics on a range of ROI input masks.";
	private static final String longDescription = "This module allows for the calculation of simple statistics of a volume based\n" +
			"on a ROI-mask that contains several integer values (ie: different segmentations).\n" +
			"Output are number collections that can be piped through to a summary destination box,\n" +
			"as well as a histogram txt with one column of bins and one column per ROI integer.\n" +
			"Note: NaNs in the volume will be ignored in the averages.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.ROI");
		inputParams.setLabel("ROI Range Statistics Calculator");
		inputParams.setName("ROI_Range_Calculator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com",""));
		info.setAffiliation("Utrecht Medical Center, department of Radiology");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
		
		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(volumeParam=new ParamVolume("Image Volume"));
		inputParams.add(maskParam=new ParamVolume("ROI Mask"));
		maskParam.setMandatory(false);

		inputParams.add(maskIDmin=new ParamInteger("Mask ID begin range",0,Integer.MAX_VALUE));
		inputParams.add(maskIDmax=new ParamInteger("Mask ID end range",0,Integer.MAX_VALUE));
		inputParams.add(histogramBins=new ParamInteger("Histogram Bins",0,Integer.MAX_VALUE));
		histogramBins.setValue(256);
		inputParams.add(histmin = new ParamFloat("Histogram minimum bin value"));
		inputParams.add(histmax = new ParamFloat("Histogram maximum bin value"));

	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputParams.add(resultFileName= new ParamString ("Name of input volume"));
		outputParams.add(CollMaskID=new ParamNumberCollection("Mask ID (collection)",0,Integer.MAX_VALUE));
		outputParams.add(CollVoxels=new ParamNumberCollection("# Voxels (collection)", 0, Integer.MAX_VALUE));
		outputParams.add(CollMean=new ParamNumberCollection("Mean (collection)", 0.0, Double.MAX_VALUE));
		outputParams.add(CollMedian=new ParamNumberCollection("Median (collection)", 0.0, Double.MAX_VALUE));
		outputParams.add(CollStd=new ParamNumberCollection("Std (collection)", 0.0, Double.MAX_VALUE));
		outputParams.add(CollMax=new ParamNumberCollection("Max (collection)", 0.0, Double.MAX_VALUE));
		outputParams.add(CollMin=new ParamNumberCollection("Min (collection)", 0.0, Double.MAX_VALUE));
		outputParams.add(ResultHistogram=new ParamObject<double[][]>("Histogram", new ArrayDoubleTxtReaderWriter()));
//		ArrayDoubleTextReaderWriter t;
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}

	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {

		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 ****************************************************/
		JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t START");
		/****************************************************
		 * Step 2. Parse the input data
		 ****************************************************/
		JistLogger.logOutput(JistLogger.INFO, "Initializing");
		this.setLabel("Initializing");
		
		ImageData vol1=volumeParam.getImageData();
		ImageData volMask=maskParam.getImageData();
		int rows=vol1.getRows();
		int cols=vol1.getCols();
		int slices=vol1.getSlices();
		int components = vol1.getComponents();
		
		if(volMask == null){
			JistLogger.logError(JistLogger.SEVERE, "No Mask has been supplied, aborting.\n " +
					"(You can use the module 'RoiStats' if no masking is required)");
			return;
		}

		if(volMask.getRows()!=rows ||
				volMask.getCols()!=cols ||
				volMask.getSlices()!=slices ||
				volMask.getComponents()!=components){
			//throw new RuntimeException("Volumes of unequal size cannot be combined.");
			JistLogger.logError(JistLogger.SEVERE, "Volumes 'Image Volume' and 'ROI Mask' are of unequal size, aborting.");
			return;
		}
		JistLogger.logOutput(JistLogger.FINEST, "Mask Begin / End range found = "+ maskIDmin.getInt()+" / "+ maskIDmax.getInt());
		int count;
		int [] MaskList;
		// get number of mask integers
		if(maskIDmax.getInt() <= maskIDmin.getInt()){
			JistLogger.logOutput(JistLogger.FINE, "Mask end range is smaller or equal to start range, only start range is used.");
			count = 1;
			MaskList = new int[count];
			MaskList[1] = maskIDmin.getInt();
		}
		else{
			count = maskIDmax.getInt() - maskIDmin.getInt() +1;		 
			MaskList = new int[count];
			for (int n=0; n<count; n++){
				MaskList[n]= maskIDmin.getInt()+n;
			}
		}
		
		// Prepare Histogram arrays
		double[][] histdata = new double[histogramBins.getInt()][MaskList.length+1];
		double[] bins = new double[histogramBins.getInt()];
		Float binstep = (histmax.getFloat() - histmin.getFloat())/ histogramBins.getFloat();
		for (int n=0; n<histogramBins.getInt(); n++){
			bins[n]= histmin.getFloat() + binstep*n;
		}
		
		
		this.setLabel("Working");
		JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t Reading in Datapoints...");
		//for all mask integers
		this.setTotalUnits(MaskList.length);
		boolean first=true;
		for (int n=0;  n < MaskList.length; n++){
			int MaskID = MaskList[n];
			double[] data;
			// get total number of voxels to sample
			count=0;
			int countNaN=0;
			
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {
								if(volMask.get(i, j, k, l).intValue()==MaskID)
									if(Double.isNaN(vol1.get(i, j, k, l).doubleValue())){
										countNaN++;
									}else{
										count++;										
									}
									
							}
						} else {
							if(volMask.get(i, j, k).intValue()==MaskID){
								if(Double.isNaN(vol1.get(i, j, k).doubleValue())){
									countNaN++;
								}else{
									count++;										
								}
							}
						}
					}
				}
			}
			JistLogger.logOutput(JistLogger.FINEST, "Masking the input volume resulted in "+countNaN+" NaNs and "+count+" regular datapoints.");
			data =new double[count];
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {

								if(volMask.get(i, j, k, l).intValue()==MaskID) {
									if(! Double.isNaN(vol1.get(i, j, k, l).doubleValue())){
										data[count] =vol1.get(i, j, k, l).doubleValue();
										count ++;
									}
								}
							}
						} else {
							if(volMask.get(i, j, k).intValue()==MaskID) {
								if(! Double.isNaN(vol1.get(i, j, k).doubleValue())){
									data[count] =vol1.get(i, j, k).doubleValue();
									count ++;
								}
							}
						}
					}
				}
			}
			//calculate statistics and clean NaNs
			JistLogger.logOutput(JistLogger.INFO, "Calculating Statistics...");
			ParamNumber voxel = new ParamInteger();
			ParamNumber mean = new ParamDouble();
			ParamNumber std = new ParamDouble();
			ParamNumber median = new ParamDouble();
			ParamNumber max = new ParamDouble();
			ParamNumber min = new ParamDouble();
			ParamNumber mask_id = new ParamInteger();
			
			voxel.setValue(count);
			mask_id.setValue(MaskID);
			mean.setValue(Double.NaN); 
			std.setValue(Double.NaN);
			median.setValue(Double.NaN); 
			max.setValue(Double.NaN); 
			min.setValue(Double.NaN); 

			if(count > 0){
				mean.setValue(StatisticsDouble.mean(data));
				std.setValue(StatisticsDouble.std(data));
				median.setValue(StatisticsDouble.median(data));
				max.setValue(StatisticsDouble.max(data));
				min.setValue(StatisticsDouble.min(data));
			}
			//store data
			
			
			CollMaskID.add(mask_id);
			CollVoxels.add(voxel);
			CollMean.add(mean);
			CollStd.add(std);
			CollMedian.add(median);
			CollMax.add(max);
			CollMin.add(min);
			
			JistLogger.logOutput(JistLogger.FINEST, "Results for "+vol1.getName()+"\n\t mask_id = "+mask_id.getValue());
			JistLogger.logOutput(JistLogger.FINEST, "*\t # voxels = "+ voxel.getValue());
			JistLogger.logOutput(JistLogger.FINEST, "*\t mean = "+mean.getValue());
			JistLogger.logOutput(JistLogger.FINEST, "*\t std = "+ std.getValue());
			JistLogger.logOutput(JistLogger.FINEST, "*\t median = "+ median.getValue());
			JistLogger.logOutput(JistLogger.FINEST, "*\t Max = "+ max.getValue());
			JistLogger.logOutput(JistLogger.FINEST, "*\t Min = "+ min.getValue());
			
			double[][] histtemp = StatisticsDouble.histogram(data, bins);
			if (first){
				first=false;
				for(int m=0; m<histogramBins.getInt(); m++){
					histdata[m][0]=histtemp[m][0];
					histdata[m][1]=histtemp[m][1];
				}
			}else
			{
				for(int m=0; m<histogramBins.getInt(); m++){
					histdata[m][n+1]=histtemp[m][1];
				}
			}
			
			this.setCompletedUnits(n);
		}
		ResultHistogram.setObject(histdata);
		ResultHistogram.setFileName(vol1.getName()+"_Histogram_Roi"+maskIDmin.getInt()+"-"+maskIDmax.getInt());
	
		resultFileName.setValue(vol1.getName());
		JistLogger.logMIPAVRegistry();
		System.gc();
		JistLogger.logOutput(JistLogger.FINE, getClass().getCanonicalName()+"\t FINISHED");
	}
	}
}
