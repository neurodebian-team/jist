package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 20, 2005
 * Time: 9:52:19 AM
 * To change this template use Options | File Templates.
 */
public class VEC {
    float x,y,z;
       public VEC(float x0,float y0,float z0) {
           x=x0;y=y0;z=z0;
       }
       int cor2ind(int sx,int  sy) {
           return Math.round(x)+sx*(Math.round(y)+Math.round(z)*sy);
       }
       public String toString() {
           return "("+Math.round(x)+","+Math.round(y)+","+Math.round(z)+")";
       }

       public boolean equals(VEC pt) {
           return (pt.x==x)&&(pt.y==y)&&(pt.z==z);
       }

}
