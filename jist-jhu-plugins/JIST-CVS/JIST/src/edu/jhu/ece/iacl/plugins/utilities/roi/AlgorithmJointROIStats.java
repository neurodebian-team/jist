/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.roi;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author John Bogovic (bogovic@jhu.edu)
 */
public class AlgorithmJointROIStats extends ProcessingAlgorithm{
	ParamVolume volulmeA;
	ParamVolume volulmeB;
	ParamVolume maskParam;
	ParamFloat resultMean;
	ParamFloat resultMedian;
	ParamFloat resultStd;
	ParamFloat resultMax;
	ParamFloat resultMin;
	ParamInteger resultVoxels;

	ParamObject<double[][]> jointHistogram;
	ParamFloat KLdiv;

	ParamInteger maskID;
	ParamInteger histogramBins;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Calculate ROI-based statistics.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(volulmeA=new ParamVolume("Image Volume A"));
		inputParams.add(volulmeB=new ParamVolume("Image Volume B"));
		inputParams.add(maskParam=new ParamVolume("ROI Mask"));
		maskParam.setMandatory(false);

		inputParams.add(maskID=new ParamInteger("Mask ID",0,Integer.MAX_VALUE));
		inputParams.add(histogramBins=new ParamInteger("Histogram Bins",0,Integer.MAX_VALUE));
		histogramBins.setValue(256);

		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.ROI");
		inputParams.setLabel("Joint ROI Calculator");
		inputParams.setName("Joint_ROI_Calculator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://sites.google.com/a/jhu.edu/neuroweb/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
//		outputParams.add(resultVoxels=new ParamInteger("# Voxels"));
//		outputParams.add(resultMean=new ParamFloat("Mean"));
//		outputParams.add(resultMedian=new ParamFloat("Median"));
//		outputParams.add(resultStd=new ParamFloat("Std"));
//		outputParams.add(resultMax=new ParamFloat("Max"));
//		outputParams.add(resultMin=new ParamFloat("Min"));
		outputParams.add(jointHistogram=new ParamObject<double[][]>("Histogram", new ArrayDoubleTxtReaderWriter()));
		outputParams.add(KLdiv=new ParamFloat("KL divergence"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ImageData vol1=volulmeA.getImageData();
		ImageData vol2=volulmeB.getImageData();
		ImageData volMask=maskParam.getImageData();
		int IDval = maskID.getInt();

		int rows=vol1.getRows();
		int cols=vol1.getCols();
		int slices=vol1.getSlices();
		int components = vol1.getComponents();

		if(vol1.getRows()!=rows ||
				vol1.getCols()!=cols ||
				vol1.getSlices()!=slices ||
				vol1.getComponents()!=components)
			throw new RuntimeException("Volumes of unequal size cannot be combined.");
		double[] dataA;
		double[] dataB;
		int count;
		ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",rows,cols,slices);
		if(volMask!=null){ 	// if a mask is provided, consider only the portion of the image overlapping with the mask
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {
								if(volMask.get(i, j, k, l).intValue()==IDval)
									count++;
							}
						} else {
							if(volMask.get(i, j, k).intValue()==IDval)
								count++;
						}
					}
				}
			}

			dataA =new double[count];
			dataB =new double[count];
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {
								if(volMask.get(i, j, k, l).intValue()==IDval) {
									dataA[count] =vol1.get(i, j, k, l).doubleValue();
									dataB[count] =vol2.get(i, j, k, l).doubleValue();
									count++;
								}
							}
						} else {
							if(volMask.get(i, j, k).intValue()==IDval) {
								dataA[count] =vol1.get(i, j, k).doubleValue();
								dataB[count] =vol2.get(i, j, k).doubleValue();
								count++;
							}
						}
					}
				}
			}
		}else{	// if no mask is provided, consider the entire image
			count = rows*cols*slices*components;
			dataA =new double[count];
			dataB =new double[count];
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {
								dataA[count] =vol1.get(i, j, k, l).doubleValue();
								dataB[count] =vol2.get(i, j, k, l).doubleValue();
								count++;
							}
						} else {
							dataA[count] =vol1.get(i, j, k).doubleValue();
							dataB[count] =vol2.get(i, j, k).doubleValue();
							count++;
						}
					}
				}
			}
		}

//		resultVoxels.setValue(count);
//		resultMean.setValue(StatisticsDouble.mean(dataA));
//		resultStd.setValue(StatisticsDouble.std(dataA));
//		resultMedian.setValue(StatisticsDouble.median(dataA));
//		resultMax.setValue(StatisticsDouble.max(dataA));
//		resultMin.setValue(StatisticsDouble.min(dataA));
		jointHistogram.setObject(StatisticsDouble.jointhistogram(dataA, dataB, histogramBins.getInt()));
		jointHistogram.setFileName(vol1.getName()+"_"+vol2.getName()+"_JointHistogram");

		KLdiv.setValue(StatisticsDouble.KLdivergence(dataA, dataB, histogramBins.getInt()));
	}
}
