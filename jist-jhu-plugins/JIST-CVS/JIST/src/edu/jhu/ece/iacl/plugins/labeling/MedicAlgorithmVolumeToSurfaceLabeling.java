package edu.jhu.ece.iacl.plugins.labeling;

import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


public class MedicAlgorithmVolumeToSurfaceLabeling extends ProcessingAlgorithm{
	//output params
	private ParamSurfaceCollection surfwlab;

	//input params
	private ParamSurfaceCollection surf;
	private ParamVolumeCollection labelvolume;

//	private ParamOption statoi; //statistic of interest

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surf=new ParamSurfaceCollection("Surface to Label"));
		inputParams.add(labelvolume = new ParamVolumeCollection("Data Volume",null,-1,-1,-1,-1));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.Surface");
		inputParams.setLabel("Volume Labels to Surface");
		inputParams.setName("Volume_Labels_to_Surface");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setDescription("Labels a surface given volumetric labels (in the same space). Returns, for each input surface, intersect with each label volume.");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
//		dwiMotionCorr = new ParamVolumeCollection("Output Diffusion-weighted Volumes");
		surfwlab = new ParamSurfaceCollection("Labeled Surface");
		outputParams.add(surfwlab);
	}


	protected void execute(CalculationMonitor monitor) {
		List<ImageData> labels = labelvolume.getImageDataList();
		List<EmbeddedSurface> surfs = surf.getSurfaceList();
		List<EmbeddedSurface> results = new LinkedList<EmbeddedSurface>();
		for(int jSurf = 0;jSurf<surfs.size();jSurf++) {
			for(int jVol = 0; jVol<labels.size();jVol++){
				EmbeddedSurface result = labelSurf(labels.get(jVol),surfs.get(jSurf).clone());
				result.setName(surfs.get(jSurf).getName()+"_x_"+labels.get(jVol).getName());
				System.out.println(getClass().getCanonicalName()+"\t"+surfs.get(jSurf).getName()+"_x_"+jVol);
				results.add(result);
			}
		}
		
		surfwlab.setValue(results);
	}


	private EmbeddedSurface labelSurf(ImageData vol,EmbeddedSurface sout){
		
		double[][] data = sout.getVertexData();

//		float[] origin = vol.getModelImage().getOrigin();

		for(int i=0; i<data.length; i++){
			Point3f p = sout.getVertex(i);

			if(isPtinBounds(vol,p)){
				data[i]=new double[]{vol.getDouble(Math.round(p.x), 
						Math.round(p.y), 
						Math.round(p.z))};
			}
		}
		sout.setVertexData(data);
		return sout;
	}


	private boolean isPtinBounds(ImageData img, Point3f pt){
		if(pt.x<0 || pt.y<0 || pt.z<0){
			return false;
		}
		if(Math.round(pt.x)>img.getRows()-1){
			return false;
		}
		if(Math.round(pt.y)>img.getCols()-1){
			return false;
		}
		if(Math.round(pt.z)>img.getSlices()-1){
			return false;
		}
		return true;
	}
}
