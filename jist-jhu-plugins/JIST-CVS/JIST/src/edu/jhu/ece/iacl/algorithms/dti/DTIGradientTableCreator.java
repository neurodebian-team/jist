package edu.jhu.ece.iacl.algorithms.dti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class DTIGradientTableCreator {
	
	GTCparams params;
	String rel;
	String OS;
	double[][] table;
	double[][] angCorrGT;
	double[][] rev_angCorrGT;
//	double[][] angCorrGT_DTIS;
//	double[][] rev_angCorrGT_DTIS;
	
	String table_str;
	String angCorrGT_str;
	String rev_angCorr_GT;
	 
	String space;
	String table_txt;
	
	public DTIGradientTableCreator(){
	}

	public DTIGradientTableCreator(GTCparams params){
		this.params=params;
	}
	
	public void run(){
		
		if(params.flag.equals("KIRBY")){
			System.out.println("jist.plugins"+"\t"+"date: " + params.date);
			determineRelease();
			System.out.println("jist.plugins"+"\t"+"FOUND RELEASE: " + rel);
		}else{
			rel=params.release;
		}
		
		if(params.grad_choice.equals("User-defined")){
			if(params.flag=="KIRBY"){
				space = "MPS";
			}else{
				space="LPH";
			}
			table_txt="IDIFF_SCHEME_USERDEFINED";
			table=parseTableFile(params.userTable);
		}else{
			tableChoice();
		}
//		System.out.println("jist.plugins"+"\t"+"FOUND TABLE:");
//		System.out.println("jist.plugins"+"\t"+DTIGradientTableCreator.tableToString(table));
		angulationCorrection(table);
	}
	
	public double[][] getCorrectedTable(){
		return angCorrGT;
	}
//	public double[][] getCorrectedTableDTIS(){
//		return angCorrGT_DTIS;
//	}
	public double[][] getOrigTable(){
		return table;
	}
	public String getbValues(){
		return params.bvals;
	}

	public void determineRelease(){

		if(params.flag.equals("KIRBY")){

			//2005,04,26->Windows 1.5T
			//2003,11,24->Windows 3.0T

			String VMS2Windows15T="20050426";
			String VMS2Windows30T="20031124";

			//2007,03,14 -> Rel 2.1
			//2007,10,27 -> Rel 2.5
			String Rel11ToRel21="20070314";
			String Rel21To25="20071027";

			if(params.scanner.equals("3.0")){
				System.out.println("jist.plugins"+"\t"+params.date);
				if(params.date.compareTo(VMS2Windows30T)<0){
					OS="3.0_VMS_OS";
				}else if(params.date.compareTo(VMS2Windows30T)>0){
					OS="3.0_Windows_OS";
				}
				System.out.println("jist.plugins"+"\t"+"OPERATING SYSTEM: "+ OS);
				System.out.println("jist.plugins"+"\t"+params.date);
				if(params.date.compareTo(Rel11ToRel21)<0){
					rel="Rel_11.x";
				}else if(params.date.compareTo(Rel11ToRel21)>0 && params.date.compareTo(Rel21To25)<0){
					rel="Rel_2.1";
				}else if(params.date.compareTo(Rel21To25)>0){
					rel="Rel_2.5";
				}

			}else if(params.scanner.equals("1.5")){
				if(params.date.compareTo(VMS2Windows15T)<0){
					OS="1.5_VMS_OS";
				}else if(params.date.compareTo(VMS2Windows15T)>0){
					OS="1.5_Windows_OS";
				}
				rel="Rel_11.x";
			}else{
				System.err.println("jist.plugins"+"Could not locate a valid release!");
			}
		}else{
			rel = params.release;
		}
		
	}
	
	
	public void tableChoice(){

		// Determine space here. (XYZ,LPH,MPH)	
		//Check that length of data and of table match
		//Needs to return the table, space, table_txt
		
		params.printParams();
	
		System.out.println("jist.plugins"+"\t"+"");

		System.out.println("jist.plugins"+"\t"+"*************");
		System.out.println("jist.plugins"+"\t"+params.grad_choice);
		System.out.println("jist.plugins"+"\t"+"*************");

		if(params.grad_choice.equals("Philips Yes Overplus Low")){
			if(params.numvolumes==8){

				if(rel.equals("Rel_1.5") || rel.equals("Rel_1.7") || rel.equals("Rel_1.5") || rel.equals("Rel_2.0") || rel.equals("Rel_2.1") || rel.equals("Rel_2.5")){
					table = GTCparams.getLowOP();
					table_txt = "IDIFF_SCHEME_LOW_OVERPLUS";
					space = "LPH";
				}else{
					table = GTCparams.getLowOP2();
					table_txt = "IDIFF_SCHEME_MEDIUM_OVERPLUS";
					space = "XYZ";
				}
			}else{
				System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " are not consistent with gradient table choice - expected 8 dimensions");
			}

		}else if(params.grad_choice.equals("Philips Yes Overplus Medium")){

			if(params.numvolumes==17){
				if(rel.equals("Rel_1.5") || rel.equals("Rel_1.7") || rel.equals("Rel_1.5") || rel.equals("Rel_2.0") || rel.equals("Rel_2.1") || rel.equals("Rel_2.5")){
					table = GTCparams.getMediumOP();
					table_txt = "IDIFF_SCHEME_MEDIUM_OVERPLUS";
					space = "LPH";
				}else{
					table = GTCparams.getMediumOP2();
					table_txt = "IDIFF_SCHEME_MEDIUM_OVERPLUS";
					space = "XYZ";
				}
			}else{
				System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " are not consistent with gradient table choice - expected 17 dimensions");
			}

		}else if(params.grad_choice.equals("Philips Yes Overplus High")){
			System.out.println("jist.plugins"+"\t"+"Inhere*****************************!");
			if(params.numvolumes==34){
				if(rel.equals("Rel_1.5") || rel.equals("Rel_1.7") || rel.equals("Rel_1.5") || rel.equals("Rel_2.0") || rel.equals("Rel_2.1")){
					table = GTCparams.getHighOP_24prev();
					table_txt = "IDIFF_SCHEME_MEDIUM_OVERPLUS";
					space = "LPH";
				}else if(rel.equals("Rel_2.5")){
					table = GTCparams.getHighOP_rel25();
					table_txt = "IDIFF_SCHEME_MEDIUM_OVERPLUS";
					space = "LPH";
				}
				else{
					table = GTCparams.getHighOP_25post();
					table_txt = "IDIFF_SCHEME_MEDIUM_OVERPLUS";
					space = "XYZ";
				}
			}else{
				System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " are not consistent with gradient table choice - expected 34 dimensions");
			}
		}else if(params.grad_choice.equals("Philips No Overplus Low")){
			if(params.numvolumes==8){
				table = GTCparams.getLow();
				table_txt = "IDIFF_SCHEME_LOW_NO_OVERPLUS";
				space = "MPS";
			}else{
				System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " are not consistent with gradient table choice - expected 8 dimensions");
			}
		}else if(params.grad_choice.equals("Philips No Overplus Medium")){
			if(params.numvolumes==17){
				table = GTCparams.getMedium();
				table_txt = "IDIFF_SCHEME_MEDIUM_NO_OVERPLUS";
				space = "MPS";
			}else{
				System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " are not consistent with gradient table choice - expected 17 dimensions");
			}
		}else if(params.grad_choice.equals("Philips No Overplus High")){
			if(params.numvolumes==34){
				table = GTCparams.getHigh();
				table_txt = "IDIFF_SCHEME_HIGH_NO_OVERPLUS";
				space = "MPS";
			}else{
				System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " are not consistent with gradient table choice - expected 34 dimensions");
			}
		}else if(params.grad_choice.contains("Jones30") && !params.grad_choice.contains("Inverted")){
			System.out.println("jist.plugins"+"\t"+"Chose Jones 30");
			System.out.println("jist.plugins"+"\t"+params.flag);
			System.out.println("jist.plugins"+"\t"+params.numvolumes);
			System.out.println("jist.plugins"+"\t"+OS);
			if(params.flag.equals("KIRBY")){
				if(params.numvolumes==32 && OS.contains("Windows")){
					table = GTCparams.getJones30();
					table_txt = "IDIFF_SCHEME_JONES30";
					space = "MPS";
				}else if(params.numvolumes==35 && OS.contains("VMS")){
					System.out.println("jist.plugins"+"\t"+"35 vols and VMS");
					table = GTCparams.getJones30VMS();
					table_txt = "IDIFF_SCHEME_VMS_JONES30";
					space = "MPS";
				}else if(params.numvolumes==31 && OS.contains("Windows")){
					table = GTCparams.getJones30();
					table_txt = "IDIFF_SCHEME_USERDEFINED_JONES30";
					space = "LPH";
				}
				else{
					System.err.println("jist.plugins"+"Image dimensions " + params.numvolumes + " or Operating System "+ OS + " are not consistent with gradient table choice - expected 8 dimensions");
				}
			}else{
				System.err.println("jist.plugins"+"Jones30 is valid only for the KIRBY scanners");
			}
		}else if(params.grad_choice.equals("Inverted Jones 30 (KKI Only)")){
			System.err.println("jist.plugins"+"Could not determine a table! - Inverted Jones30 not yet supported");
		}else{
			System.err.println("jist.plugins"+"Could not determine a table!");
		}

	}


	/**
	 * Before this - be sure we have the correct table.
	 * see lines 880-1151 in Jon's Matlab
	 * 
	 * Do checks on user defined tables
	 * @param tablein
	 */

	public void angulationCorrection(double[][] tablein){

		angCorrGT=new double[tablein.length][tablein[0].length];
//		params.patient_orientation
//		params.patient_position
//		params.slice_orient
//		params.fo
//		params.fs
//		params.sliceAng
//		params.scanner
//		params.sort_image
//		params.parversion

		//DTIstudio vs FSL format
		//yes DTIS
		
//		System.out.println("jist.plugins"+"\t"+params.sliceAng[0][0]+","+params.sliceAng[1][0]+","+params.sliceAng[2][0]);
		params.sliceAng[0][0]=Math.toRadians(params.sliceAng[0][0]);
		params.sliceAng[1][0]=Math.toRadians(params.sliceAng[1][0]);
		params.sliceAng[2][0]=Math.toRadians(params.sliceAng[2][0]);

//		==========================================================
//		TRANSFORMATION DEFINITIONS 
//		==========================================================

		// Transformations and reverse transformatins that we will use
		// Definitions for these matrices were taken from Philips documentation

		double[][] Tpo;
		double[][] rev_Tpo;
		if (params.patient_orientation=="sp"){
			Tpo = new double[][]{{1, 0, 0},{0, 1, 0}, {0, 0, 1}};
			rev_Tpo = new double[][]{{1,0,0},{0,1,0},{0,0,1}};
		}
		else if (params.patient_orientation=="pr"){
			Tpo = new double[][]{{-1, 0, 0},{0, -1, 0}, {0, 0, 1}};
			rev_Tpo = new double[][]{{-1,0,0},{0,-1,0},{0,0,1}};
		}  
		else if (params.patient_orientation=="rd"){
			Tpo = new double[][]{{0,-1, 0},{1, 0, 0}, {0, 0, 1}};
			rev_Tpo = new double[][]{{0,1,0},{-1,0,0},{0,0,1}};
		}  
		else if (params.patient_orientation=="ld"){
			Tpo = new double[][]{{0,1, 0},{-1, 0, 0}, {0, 0, 1}};
			rev_Tpo = new double[][]{{0,-1,0},{1,0,0},{0,0,1}};
		}else{
			Tpo=null;
			rev_Tpo=null;
		}

		double[][] Tpp;
		double[][] rev_Tpp;
		if (params.patient_position=="ff"){
			Tpp = new double[][]{{0, -1, 0},{-1, 0, 0}, {0, 0, 1}};
			rev_Tpp = new double[][]{{0,-1,0},{-1,0,0},{0,0,-1}};
		}
		else if (params.patient_position=="hf"){
			Tpp = new double[][]{{0, 1, 0},{-1, 0, 0}, {0, 0, -1}};
			rev_Tpp = new double[][]{{0,-1,0},{1,0,0},{0,0,-1}};
		}else{
			Tpp=null;
			rev_Tpp=null;
		}

		double ap = params.sliceAng[0][0];
		double fh = params.sliceAng[1][0];
		double rl = params.sliceAng[2][0];
		System.out.println("jist.plugins"+"\t"+"ap: " + ap);
		System.out.println("jist.plugins"+"\t"+"fh: " + fh);
		System.out.println("jist.plugins"+"\t"+"rl: " + rl);

		double[][] Tpom = matrixMultiply(Tpo,Tpp);
		double[][] rev_Tpom = matrixMultiply(rev_Tpp,rev_Tpo);

		double[][] Trl = {{1,0,0}, {0, cos(rl), -sin(rl)}, {0,sin(rl),cos(rl)}};
		double[][] Tap = {{cos(ap),0,sin(ap)}, {0,1,0}, {-sin(ap),0,cos(ap)}};
		double[][] Tfh = {{cos(fh),-sin(fh),0}, {sin(fh),cos(fh),0}, {0,0,1}};
		double[][] Tang = matrixMultiply(matrixMultiply(Trl,Tap),Tfh);

		double[][] rev_Trl = {{1,0,0}, {0, cos(rl), sin(rl)}, {0,-sin(rl),cos(rl)}};
		double[][] rev_Tap = {{cos(ap),0,-sin(ap)}, {0,1,0}, {sin(ap),0,cos(ap)}};
		double[][] rev_Tfh = {{cos(fh),sin(fh),0}, {-sin(fh),cos(fh),0}, {0,0,1}};
		double[][] rev_Tang = matrixMultiply(matrixMultiply(rev_Tfh,rev_Tap),rev_Trl);

		double[][] Tsom;
		double[][] rev_Tsom;
//		% Definitions for Tsom
		if (params.slice_orient=="sag"){
			Tsom = new double[][]{{0,0,-1},{0,-1,0},{1,0,0}};
			rev_Tsom = new double[][]{{0,0,1},{0,-1,0},{-1,0,0}};
		}
		else if (params.slice_orient=="cor"){
			Tsom = new double[][]{{0,-1,0},{0,0,1},{1,0,0}};
			rev_Tsom = new double[][]{{0,0,1},{-1,0,0},{0,1,0}};
		}
		else if (params.slice_orient=="tra"){
			Tsom = new double[][]{{0,-1,0},{-1,0,0},{0,0,1}};
			rev_Tsom = new double[][]{{0,-1,0},{-1,0,0},{0,0,1}};
		}else{
			Tsom=null;
			rev_Tsom=null;
		}



		//Definitions for Tprep_par Tprep_per & Tfsd_m, Tfsd_p, Tfsd_s

		double[][] Tprep_par = {{1,0,0},{0,1,0},{0,0,1}};
		double[][]rev_Tprep_par = {{1,0,0},{0,1,0},{0,0,1}};
		double[][]Tprep_per = {{0,-1,0},{1,0,0},{0,0,1}};
		double[][]rev_Tprep_per = {{0,1,0},{-1,0,0},{0,0,1}};

		double[][] Tfsd_m = {{-1,0,0},{0,1,0},{0,0,1}};
		double[][] rev_Tfsd_m = {{-1,0,0},{0,1,0},{0,0,1}};
		double[][] Tfsd_p = {{1,0,0},{0,-1,0},{0,0,1}};
		double[][] rev_Tfsd_p = {{1,0,0},{0,-1,0},{0,0,1}};
		double[][] Tfsd_s = {{1,0,0},{0,1,0},{0,0,-1}};
		double[][] rev_Tfsd_s = {{1,0,0},{0,1,0},{0,0,-1}};


		double[][] Tprep;
		double[][] rev_Tprep;
		double[][] Tfsd;
		double[][] rev_Tfsd;
		if(params.slice_orient=="tra"){
			if(params.fo=="AP"){
				Tprep =Tprep_per;
				rev_Tprep = rev_Tprep_per;
				if(params.fs=="A"){
					Tfsd = Tfsd_m;
					rev_Tfsd = rev_Tfsd_m;
				}else if(params.fs=="P"){
					Tfsd = Tfsd_p;
					rev_Tfsd = rev_Tfsd_p;
				}else{
					Tfsd = null;
					rev_Tfsd = null;
				}

			}
			else if(params.fo=="RL"){
				Tprep =Tprep_par;
				rev_Tprep = rev_Tprep_par;
				if(params.fs=="R"){
					Tfsd = Tfsd_p;
					rev_Tfsd = rev_Tfsd_p;
				}else if(params.fs=="L"){
					Tfsd = Tfsd_m;
					rev_Tfsd = rev_Tfsd_m;
				}else{
					Tfsd = null;
					rev_Tfsd = null;
				}

			}
			else{
				Tprep=null;
				rev_Tprep=null;
				Tfsd = null;
				rev_Tfsd = null;
			}
		}
		else if(params.slice_orient=="cor"){
			if(params.fo=="FH"){
				Tprep =Tprep_per;
				rev_Tprep = rev_Tprep_per;
				if(params.fs=="F"){
					Tfsd = Tfsd_p;
					rev_Tfsd = rev_Tfsd_p;
				}else if(params.fs=="H"){
					Tfsd = Tfsd_m;
					rev_Tfsd = rev_Tfsd_m;
				}else{
					Tfsd = null;
					rev_Tfsd = null;
				}
			}
			else if(params.fo=="RL"){
				Tprep =Tprep_par;
				rev_Tprep = rev_Tprep_par;
				if(params.fs=="R"){
					Tfsd = Tfsd_p;
					rev_Tfsd = rev_Tfsd_p;
				}else if(params.fs=="L"){
					Tfsd = Tfsd_m;
					rev_Tfsd = rev_Tfsd_m;
				}else{
					Tfsd = null;
					rev_Tfsd = null;
				}

			}
			else{
				Tprep=null;
				rev_Tprep=null;
				Tfsd = null;
				rev_Tfsd = null;
			}
		}
		else if(params.slice_orient=="sag"){
			if(params.fo=="FH"){
				Tprep =Tprep_per;
				rev_Tprep = rev_Tprep_per;
				if(params.fs=="F"){
					Tfsd = Tfsd_p;
					rev_Tfsd = rev_Tfsd_p;
				}else if(params.fs=="H"){
					Tfsd = Tfsd_m;
					rev_Tfsd = rev_Tfsd_m;
				}else{
					Tfsd = null;
					rev_Tfsd = null;
				}

			}
			else if(params.fo=="AP"){
				Tprep =Tprep_par;
				rev_Tprep = rev_Tprep_par;
				if(params.fs=="A"){
					Tfsd = Tfsd_p;
					rev_Tfsd = rev_Tfsd_p;
				}else if(params.fs=="P"){
					Tfsd = Tfsd_m;
					rev_Tfsd = rev_Tfsd_m;
				}else{
					Tfsd = null;
					rev_Tfsd = null;
				}
			}
			else{
				Tprep=null;
				rev_Tprep=null;
				Tfsd = null;
				rev_Tfsd = null;
			}
		}else{
			Tprep=null;
			rev_Tprep=null;
			Tfsd=null;
			rev_Tfsd=null;
		}
		
//		double[][] Tang = matrixMultiply(matrixMultiply(Trl,Tap),Tfh);
		
//		System.out.println("jist.plugins"+"\t"+space);
//		System.out.println("jist.plugins"+"\t"+tableToString(Trl));
//		System.out.println("jist.plugins"+"\t"+tableToString(Tap));
//		System.out.println("jist.plugins"+"\t"+tableToString(Tfh));
//		System.out.println("jist.plugins"+"\t"+tableToString(Tang));
		
		//Now in Pixel space

//		% ==========================================
//		% END OF PHILIPS TRANSFORMATION DEFINITIONS
//		% ==========================================

//		% Transformation needed to go from Philips NWV coordinate space to DTIstudio
//		% coordinate space
		double[][] DTIextra = {{0,-1,0},{-1,0,0},{0,0,1}};
		double[][] rev_DTIextra = {{0,-1,0},{-1,0,0},{0,0,1}};


		/*
		 * APPLY TRANSFORMATIONS
		 */
//		% ======================================
//		% APPLICATION OF THE TRANSFORMATIONS
//		% ======================================


		if(space=="LPH"){
			System.out.println("jist.plugins"+"\t"+"Applying LPH rotation");
			if(params.DTIstudio){
				System.out.println("jist.plugins"+"\t"+"Applying DTIS rotation");
				angCorrGT = applyRotation(matrixMultiply(DTIextra,matrixMultiply(rev_Tsom,rev_Tang)),table);
				rev_angCorrGT= applyRotation(matrixMultiply(Tang,matrixMultiply(Tsom,rev_DTIextra)),angCorrGT);
			}else{

				angCorrGT = applyRotation(matrixMultiply(rev_Tsom,rev_Tang),table);
				rev_angCorrGT = applyRotation(matrixMultiply(Tang,Tsom),angCorrGT);
			}
		}
		else if(space=="XYZ"){
			System.out.println("jist.plugins"+"\t"+"Applying XYZ rotation");
			if(params.DTIstudio){
				System.out.println("jist.plugins"+"\t"+"Applying DTIS rotation");
				angCorrGT = applyRotation(matrixMultiply(DTIextra,matrixMultiply(rev_Tsom,matrixMultiply(rev_Tang,Tpom))),table);
				rev_angCorrGT = applyRotation(matrixMultiply(rev_Tpom,matrixMultiply(Tang,matrixMultiply(Tsom,rev_DTIextra))),angCorrGT);
			}
			else{
				angCorrGT = applyRotation(matrixMultiply(rev_Tsom,matrixMultiply(rev_Tang,Tpom)),table);
				rev_angCorrGT = applyRotation(matrixMultiply(rev_Tpom,matrixMultiply(Tang,Tsom)),angCorrGT);
			}
		}

		else if(space=="MPS"){
			System.out.println("jist.plugins"+"\t"+"Applying MPS rotation");
			if(params.DTIstudio){
				System.out.println("jist.plugins"+"\t"+"Applying DTIS rotation");
				
				angCorrGT = applyRotation(matrixMultiply(DTIextra,matrixMultiply(Tprep,Tfsd)),table);
				rev_angCorrGT = applyRotation(matrixMultiply(rev_Tfsd,matrixMultiply(rev_Tprep,rev_DTIextra)),angCorrGT);
			}
			else{
				angCorrGT = applyRotation(matrixMultiply(Tprep,Tfsd),table);
				rev_angCorrGT = applyRotation(matrixMultiply(rev_Tfsd,rev_Tprep),angCorrGT);
			}
		}else{
			System.err.println("jist.plugins"+"NO CORRECTION APPLIED!");
		}

//		System.out.println("jist.plugins"+"\t"+"********* AFTER ROTATION *********");
//		System.out.println("jist.plugins"+"\t"+DTIGradientTableCreator.tableToString(angCorrGT));
//		System.out.println("jist.plugins"+"\t"+"**********************************");

//		% Normalize the non zero vectors
		angCorrGT = normalizeTable(angCorrGT);
		rev_angCorrGT = normalizeTable(rev_angCorrGT);

//		System.out.println("jist.plugins"+"\t"+"********* AFTER NORMALIZATION *********");
//		System.out.println("jist.plugins"+"\t"+DTIGradientTableCreator.tableToString(angCorrGT));
//		System.out.println("jist.plugins"+"\t"+"**********************************");

		
		
		//Glue (0,0,0) and (100,100,100) where appropriate

		System.out.println("jist.plugins"+"\t"+params.parversion + " " + params.parversion.length());
		System.out.println("jist.plugins"+"\t"+table_txt);
		if(table_txt.equals("IDIFF_SCHEME_VMS_JONES30")){
			if (params.sort_image){
				angCorrGT = append(angCorrGT,true,0);
				angCorrGT = append(angCorrGT,true,0);
				angCorrGT = append(angCorrGT,true,0);
				angCorrGT = append(angCorrGT,true,0);
				angCorrGT = append(angCorrGT,true,0);
			}else{
				System.err.println("jist.plugins"+"I have no idea how VMS works with sort images!");
			}
		}
		else if(table_txt.equals("IDIFF_SCHEME_USERDEFINED_JONES30")){
			if (params.sort_image){
				angCorrGT = append(angCorrGT,true,0);
			}else{
				angCorrGT = append(angCorrGT,true,0);
			}
		}
		else if(table_txt.equals("IDIFF_SCHEME_USERDEFINED")){
			if (params.sort_image){
				if(params.parversion.equals("V4.1") || params.parversion.equals("V4.2")){
					angCorrGT = append(angCorrGT,false,0);
				}else if(params.parversion.equals("V4")){
					angCorrGT = append(angCorrGT,true,0);
				}else if(params.parversion.equals("V3")){
					angCorrGT = append(angCorrGT,true,0);
				}
				else{
					System.err.println("jist.plugins"+"Unsupported and unknown par file vesion, must be V3, V4, V4.1 or V4.2");
				}

			}else{
				if(params.parversion.equals("V4.1") || params.parversion.equals("V4.2")){
					angCorrGT = append(angCorrGT,true,0);
				}else if(params.parversion.equals("V4")){
					angCorrGT = append(angCorrGT,true,0);
				}else if(params.parversion.equals("V3")){
					System.err.println("jist.plugins"+"I have no idea how V3 works with (sort images = no) !");
				}
				else{
					System.err.println("jist.plugins"+"Unsupported and unknown par file vesion, must be V3, V4, V4.1 or V4.2");
				}
			}
		}else{
			if(params.sort_image){
				if(params.parversion.equals("V4.1") || params.parversion.equals("V4.2")){
					angCorrGT = append(angCorrGT,false,0);
					angCorrGT = append(angCorrGT,false,100);
				}else if(params.parversion.equals("V4")){
					angCorrGT = append(angCorrGT,true,0);
					angCorrGT = append(angCorrGT,false,100);
				}else if(params.parversion.equals("V3")){
					angCorrGT = append(angCorrGT,true,0);
					angCorrGT = append(angCorrGT,false,100);
				}else{
					System.err.println("jist.plugins"+"Unsupported and unknown par file vesion, must be V3, V4, V4.1 or V4.2");
				}

			}else{
				if(params.parversion.equals("V4.1") || params.parversion.equals("V4.2")){
					angCorrGT = append(angCorrGT,true,0);
					angCorrGT = append(angCorrGT,false,100);
				}else if(params.parversion.equals("V4")){
					angCorrGT = append(angCorrGT,true,0);
					angCorrGT = append(angCorrGT,false,100);
				}else if(params.parversion.equals("V3")){
					System.err.println("jist.plugins"+"I have no idea how ver 3 works with sort images!");
				}else{
					System.err.println("jist.plugins"+"Unsupported and unknown par file vesion, must be V3, V4, V4.1 or V4.2");
				}

			}

		}
//		System.out.println("jist.plugins"+"\t"+"********* AFTER APPENDING *********");
//		System.out.println("jist.plugins"+"\t"+DTIGradientTableCreator.tableToString(angCorrGT));
//		System.out.println("jist.plugins"+"\t"+"**********************************");
	}



	
	// Returns A*B 
//	public double[][] matrixMultiply(double[][] A, double[][] B){
//		double[][] C = new double[A.length][B[0].length];
//		for(int i=0; i<C.length; i++){
//			for(int j=0; j<C[0].length; j++){
//				
//			}
//		}
//		
//	}
	
	//3x3 only
	public double[][] matrixMultiply(double[][] A, double[][] B){
		double[][] C = new double[A.length][B[0].length];
		for(int i=0; i<C.length; i++){
			for(int j=0; j<C[0].length; j++){
				C[i][j]=A[i][0]*B[0][j] + A[i][1]*B[1][j]+A[i][2]*B[2][j];
			}
		}
		return C;
	}
	
	//Apply rotation to every row of the input table
	//(helper to angulationCorrection
	public double[][] applyRotation(double[][] A, double[][] table){
		double[][] rotTable = new double[table.length][table[0].length];
		for(int i=0; i<rotTable.length; i++){
				double[][] row = {{table[i][0]},{table[i][1]},{table[i][2]}};
				double[][] newrow = matrixMultiply(A,row);
			
//				System.out.println("jist.plugins"+"\t"+DTIGradientTableCreator.tableToString(row));
//				System.out.println("jist.plugins"+"\t"+"");
//				System.out.println("jist.plugins"+"\t"+DTIGradientTableCreator.tableToString(newrow));
//				
				rotTable[i][0]=newrow[0][0];
				rotTable[i][1]=newrow[1][0];
				rotTable[i][2]=newrow[2][0];
		}
		return rotTable;
	}
	
	
	private double cos(double a){
		return Math.cos(a);
	}
	private double sin(double a){
		return Math.sin(a);
	}
	
	private double[][] normalizeTable(double[][] table){
		double[][] normTable = new double[table.length][table[0].length];
		for(int i=0; i<normTable.length; i++){
			double length = table[i][0]*table[i][0] + table[i][1]*table[i][1] + table[i][2]*table[i][2]; 
			if(length!=0){
				length=Math.sqrt(length);
				normTable[i][0]=table[i][0]/length;
				normTable[i][1]=table[i][1]/length;
				normTable[i][2]=table[i][2]/length;
			}else{
				normTable[i][0]=table[i][0];
				normTable[i][1]=table[i][1];
				normTable[i][2]=table[i][2];
			}
		}
		
		return normTable;
	}
	
	private double[][] append(double[][] table, boolean top, double elem){
		double[][] outTable = new double[table.length+1][table[0].length];
		double[] insert = {elem,elem,elem};
		if(top){
			outTable[0]=insert;
			for(int i=1; i<outTable.length; i++){
				outTable[i]=table[i-1];
			}
		}else{
			for(int i=0; i<outTable.length-1; i++){
				outTable[i]=table[i];
			}
			outTable[outTable.length-1]=insert;
		}
		return outTable;
	}
	
	public static String tableToStringLong(double[][] table){
		String table_str="";
		for(int i=0; i<table.length; i++){
			for(int j=0; j<table[0].length; j++){
				table_str=table_str+table[i][j]+"\t";
			}
			table_str=table_str+"\n";
		}
		
		return table_str;
	}
	
	public static String tableToString(double[][] table){
		DecimalFormat format = new DecimalFormat("0.0000");
		String table_str="";
		for(int i=0; i<table.length; i++){
			for(int j=0; j<table[0].length; j++){
				table_str=table_str+format.format(table[i][j])+"\t";
			}
			table_str=table_str+"\n";
		}
		
		return table_str;
	}
	
	
	public static double[][] parseTableFile(String in){
		ArrayList<String> lines = new ArrayList<String>();
		try{
			BufferedReader rdr = new BufferedReader(new FileReader(new File(in)));
			String thisline = " ";
			thisline = rdr.readLine();
			while(thisline!=null && !thisline.isEmpty()){
				
//				System.out.println("jist.plugins"+"\t"+thisline);
				String cleanline = thisline;
				if(thisline.indexOf(':')>0){
					cleanline = cleanline.substring(cleanline.indexOf(':')+1);
				}
				cleanline = cleanline.replaceAll(",", "").trim();
				
				System.out.println("jist.plugins"+"\t"+cleanline);
				
				lines.add(cleanline);
				thisline = rdr.readLine();
			}
			
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
		//Convert ArrayList to double[][]
		double[][] out;
		lines.trimToSize();
		System.out.println("jist.plugins"+"\t"+"********");
		if(lines.size()>0){
			out = new double[lines.size()][3];
			int i =0;
			for(String s : lines){
////				System.out.println("jist.plugins"+"\t"+s);
////				System.out.println("jist.plugins"+"\t"+s.substring(0, s.indexOf(' ')));
//				out[i][0]= Double.parseDouble(s.substring(0, s.indexOf(' ')).trim());
//				String b = s.substring(s.indexOf(' '), s.length()).trim();
////				System.out.println("jist.plugins"+"\t"+b);
//				out[i][1]= Double.parseDouble(b.substring(0, b.indexOf(' ')).trim());;
//				out[i][2]= Double.parseDouble(b.substring(b.indexOf(' ')).trim());;;

				
				String[] comps = s.split(" ");
				String[] compt = s.split("\t");
				if(comps.length>2){
					out[i][0]= Double.parseDouble(comps[0]);
					out[i][1]= Double.parseDouble(comps[1]);
					out[i][2]= Double.parseDouble(comps[2]);
				}else if(compt.length>2){
					out[i][0]= Double.parseDouble(compt[0]);
					out[i][1]= Double.parseDouble(compt[1]);
					out[i][2]= Double.parseDouble(compt[2]);
				}else{
					System.err.println("jist.plugins"+"INVALID TABLE DELIMITER");
				}
//				System.out.println(s);
//				out[i][0]= Double.parseDouble(s.substring(0, s.indexOf(' ')).trim());
//				String b = s.substring(s.indexOf(' '), s.length()).trim();
//				out[i][1]= Double.parseDouble(b.substring(0, b.indexOf(' ')).trim());
//				out[i][2]= Double.parseDouble(b.substring(b.indexOf(' ')).trim());
				i++;
			}
			
		}else{
			out=null;
		}
		return out;
	}
	
//	private void printall(){
//		System.out.println("jist.plugins"+"\t"+tableToString());
//	}

}
