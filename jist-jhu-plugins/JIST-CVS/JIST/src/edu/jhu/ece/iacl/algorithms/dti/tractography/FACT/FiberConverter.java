package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.*;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.geom.BndBox;
import edu.jhu.ece.iacl.jist.structures.geom.IntersectResult;
import edu.jhu.ece.iacl.jist.structures.geom.PT;
import edu.jhu.ece.iacl.jist.structures.geom.Polygon;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurvePath;

public class FiberConverter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filename = "/home/john/Research/20070727/corrected_dti/processed/jb_7/jb_7_longFIBERS_50.dat";

		DTIS2ITK(filename);
		
	}
	
	public static void DTIS2ITK(String filename){
		System.out.println("jist.plugins"+"\t"+"Reading: " +filename);
		DTIStudioReader reader = new DTIStudioReader();
		try{
			reader.read(filename);
			Fiber[] fibers = reader.getFibers();
		
			int length=(int)(fibers.length/3);
			
			Fiber[] fewerfibers = new Fiber[length];
			
			int i=0;
			int j=0;
			while(i<length){
				fewerfibers[j]=fibers[i];
				j++;
				i=i+2;
			}
			
			MipavController.setQuiet(true);
			String fileout = filename.substring(0, filename.lastIndexOf(".")) + ".vtk";
			System.out.println("jist.plugins"+"\t"+"Writing: " +fileout);
			Fiber.writeVTK(fewerfibers, fileout);
			
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
		
	}

}
