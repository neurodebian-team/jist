package edu.jhu.ece.iacl.algorithms.manual_label;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;



public class LabelSurface {
	private static final String cvsversion = "$Revision: 1.2 $";
	public static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");

	public static String get_version() {
		return revnum;
	}


	protected EmbeddedSurface surf;
	protected double[][] labels;
	protected int offset = 0;
	protected ArrayList<Integer> allhits;
	protected String name;
	
	public LabelSurface(){
		
	}
	public LabelSurface(EmbeddedSurface surf){
		setimg(surf);
	}
	public void setimg(EmbeddedSurface surf){
		this.surf = surf;
		this.name=surf.getName();
		labels = surf.getVertexData();
	}
	public EmbeddedSurface getSurface(){
		return surf;
	}
	public String getName(){
		return name;
	}
	public double getVal(int i){
		return labels[i][offset];
	}
	public void setVal(int val, int i){
		labels[i][offset]=val;
	}
	public void setOffset(int offset){
		this.offset = offset;
	}
	public boolean sameDims(LabelSurface a){
		if(a.labels.length == this.labels.length){
			return true;
		}else{
			return false;
		}
	}
	public int[] getlabsetArray(double lab){
		ArrayList<Integer> l = getlabset(lab);

		int[] list = new int[l.size()];
		for(int z=0; z<l.size(); z++){
			list[z]=l.get(z);
		}
		return list;
	}
	public double[][] getLabelDat(){
		return labels;
	}
	public ArrayList<Integer> getlabset(double lab){
		ArrayList<Integer> l = new ArrayList<Integer>();
		for(int i=0; i<labels.length;i++){
			if(labels[i][offset]==lab){
				l.add(i);
			}
		}
		return l;
	}
	
	public int getLabelVertexCount(int lab){
		ArrayList<Integer> list = getlabset(lab);
		return list.size();
	}
	
	public HashMap<Integer, LabelSurface> separateLabels(){
		
		HashMap<Integer, LabelSurface> allsurfaces = new HashMap<Integer, LabelSurface>();
		int thislab = -1;
		for(int i=0; i<labels.length;i++){
			thislab = (int)labels[i][offset];
			if(allsurfaces.containsKey(thislab)){
				allsurfaces.get(thislab).setVal(i,1);
			}else{
				System.out.println("jist.plugins"+"\t"+"Found new label: " + thislab);
				LabelSurface newSurf = new LabelSurface(surf.clone());
				newSurf.setVal(i,1);
				allsurfaces.put(thislab, newSurf);
			}
		}
		return allsurfaces;
	}
	
	public ArrayList<Integer> getAllValues(){
		
		allhits = new ArrayList<Integer>();
		for(int i=0; i<labels.length;i++){

			if(getHitIndex((int)labels[i][offset])<0){
				allhits.add((int)labels[i][offset]);
				System.out.println("jist.plugins"+"\t"+"Added " + getVal(i) + " at (" + i+")");
			}
		}
		allhits.trimToSize();
		return allhits;
	}
	
	public double[][] computeStatistics(LabelSurface a){
		allhits = getAllValues();
		allhits.trimToSize();
		
		ArrayList<Integer> allvals = a.getAllValues();
		
		for(Integer num : allhits){
			if (!allvals.contains(num)){
				allvals.add(num);
			}
		}
		
		
//		System.out.println("jist.plugins"+"\t"+"allvals: " + allvals);
		
		double[] nt = new double[allvals.size()];
		double[] na = new double[allvals.size()];
		double[] ntia =new double[allvals.size()];	//intersection
		double[] ntua =new double[allvals.size()];	//union
		int valt = -1;
		int vala = -1;
		
		System.out.println("jist.plugins"+"\t"+allhits);
		
		for(int i=0; i<labels.length;i++){

			valt = (int)labels[i][offset];
			nt[allhits.indexOf(valt)]++;

			vala = (int)a.getLabelDat()[i][offset];;
			nt[allhits.indexOf(vala)]++;

			if(valt==vala){
				ntua[allhits.indexOf(vala)]++;
				ntia[allhits.indexOf(vala)]++;
			}else{
				ntua[allhits.indexOf(vala)]++;
				ntua[allhits.indexOf(valt)]++;
			}
		}

		System.out.println("jist.plugins"+"\t"+"nt: " + nt[0]);
		System.out.println("jist.plugins"+"\t"+"na: " +na[0]);
		System.out.println("jist.plugins"+"\t"+"ntia: " +ntia[0]);
		System.out.println("jist.plugins"+"\t"+"ntua: " +ntua[0]);
		
		double[] dice = new double[allvals.size()];
		double[] jaccard = new double[allvals.size()];
		double[] TcontA =new double[allvals.size()];	//intersection
		double[] AcontT =new double[allvals.size()];	//union
		double[] mountford =new double[allvals.size()];	//mountford index
		
		for(int n=0; n<allvals.size(); n++){
			dice[n] = (2*(ntia[n]))/(nt[n]+na[n]);
			jaccard[n] = ntia[n]/ntua[n];
			TcontA[n] = ntia[n]/na[n];
			AcontT[n] = ntia[n]/nt[n];
			mountford[n] = (2 * (ntia[n]))/((2 * nt[n] * na[n]) - ((na[n] + nt[n]) * ntia[n]));
		}
		
		double[][] stats = {dice, jaccard, TcontA, AcontT, mountford};
		return stats;
	}
	

//	/**
//	 * Returns indices of vertices 'on the boundary'
//	 * @param prevBoundSet
//	 * @return
//	 */
//	private TreeSet<Integer> getBoundarySet(TreeSet<Integer> prevBoundSet){
//		TreeSet<Integer> boundSet = new TreeSet<Integer>();
//		
//		for(GridPt pt : prevBoundSet){
//			GridPt[] nbhd = pt.neighborhood6C();
//			for(int i=0; i<nbhd.length; i++){
//				if(searched.add(nbhd[i])){
//					boundSet.add(nbhd[i]);
//				}
//			}
//		}
//		return boundSet;
//	}
	
	/**
	 * Returns a new surface with labels renamed from 1-N
	 * in the order they are found in the 'allhits' array
	 */
	public LabelSurface organizeLabels(){
		LabelSurface out = new LabelSurface(surf.clone());
		if(allhits==null){
			getAllValues();
		}
		
		System.out.println("jist.plugins"+"\t"+"out: "+out);
		System.out.println("jist.plugins"+"\t"+"allhits: " +allhits);
		System.out.println("jist.plugins"+"\t"+"allhits size: " +allhits.size());
		for(int l=0; l<labels.length;l++){
			if((int)labels[l][offset]!=0){
				out.setVal((getHitIndex((int)out.getVal(l))+1), l);
//				out.setVal(getHitIndex(img.getInt(l, j, k))+1, l, j, k);
			}
		}
		return out;
	}
	
	public int getHitIndex(int hit){
		int i = 0;
		for(Integer n : allhits){
			if(n.intValue()==hit){
				return i;
			}
			i++;
		}
		return -1;
	}
	

	public static String runAllComparisons(List<EmbeddedSurface> list, int numRaters){
		int numVols = list.size();
		int numImages = numVols/numRaters;
		String out = " Label Number \t Dice \t Jaccard \t Fwd Containment \t Rev Containment \n \n";
		for(int l=0; l<numRaters; l++){
			for(int j=0; j<numImages; j++){
				for(int k=1; k<numRaters-l; k++){
					
					System.out.println("jist.plugins"+"\t"+"Index 1: " + (j+l*numImages));
					System.out.println("jist.plugins"+"\t"+"Index 2: " + (j+l*numImages+k*numImages));
					
					LabelSurface a = new LabelSurface(list.get(j+l*numImages));
					LabelSurface b = new LabelSurface(list.get(j+l*numImages+k*numImages));
					if(!a.sameDims(b)){
						System.err.println("jist.plugins"+"Corresponding images must have like dimensions");
					}
					double[][] metrics = a.computeStatistics(b);
					String ithstep = "";
					ithstep=ithstep+"Comparing: " + a.getName() + " and " + b.getName() + "\n";
					for(int i=0; i<a.allhits.size(); i++){
//						out=out+"Level "+i+" Results: \t";
//						System.out.println("jist.plugins"+"\t"+"This hit is: " + a.allhits.get(i));
						ithstep=ithstep+"" + a.allhits.get(i)+"\t";
						ithstep=ithstep+"" + metrics[0][i]+"\t";
						ithstep=ithstep+"" + metrics[1][i]+"\t";
						ithstep=ithstep+"" + metrics[2][i]+"\t";
						ithstep=ithstep+"" + metrics[3][i]+"\n";
//						ithstep=ithstep+"Mean Distance:" + metrics[4][i]+"\n";
					}
					System.out.println("jist.plugins"+"\t"+ithstep);
					out=out+ithstep+"\n";

				}
				out=out+"\n";
			}
		}
		return out;
	}
	
	

}
