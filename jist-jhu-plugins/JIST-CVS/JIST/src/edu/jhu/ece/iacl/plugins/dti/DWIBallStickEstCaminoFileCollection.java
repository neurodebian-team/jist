package edu.jhu.ece.iacl.plugins.dti;

import imaging.SchemeV1;
import inverters.BallStickInversion;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLLMSE;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class DWIBallStickEstCaminoFileCollection extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolumeCollection DWdata4D; 		// Imaging Data
	private ParamVolumeCollection Mask3D;			// Binary mask to indicate computation volume
	private ParamFile SchemeFile;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolumeCollection ballDVolume;	// A 3D volume with the diffusivity of the "ball"	
	private ParamVolumeCollection stickVecVolume;	// A 4D volume with the vector orientation of the stick
	private ParamVolumeCollection fractionVolume;	// A 3D volume with the diffusivity of the "ball"
	private ParamVolumeCollection exitCodeVolume;	// A 3D volume 
	private ParamVolumeCollection intensityVolume;// A 3D volume 

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Fit diffusion weighted imaging data with the ball and stick model.";
	private static final String longDescription = "Fits the Behrens ball and stick model to diffusion-weighted data. The model is S(g, b) = S_0 (f \\exp[-b d (-g^T v)^2] + [1-f] \\exp[-b d]), where S(g,b) is the DW signal along gradient direction g with b-value b, d is a diffusion coefficient, v is the orientation of anisotropic diffusion, and f is a mixing parameter (0 <= f <= 1).";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("Camino");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Camino Ball and Stick Estimation File Collection");
		inputParams.setName("Camino_Ball_and_Stick_Estimation_File_Collection");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("Philip Cook", "camino@cs.ucl.ac.uk", "http://www.cs.ucl.ac.uk/research/medic/camino/"));
		info.setAffiliation("Computer Science Department - University College London");		
		info.add(new Citation("Behrens et al, Magnetic Resonance in Medicine, 50:1077-1088, 2003"));	
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamVolumeCollection("DWI and Reference Image(s) Data (4D)"));
		DWdata4D.setLoadAndSaveOnValidate(false);
		inputParams.add(SchemeFile=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"scheme","schemev1"})));
		inputParams.add(Mask3D=new ParamVolumeCollection("Mask Volume to Determine Region of Tensor Estimation (3D)"));
		Mask3D.setLoadAndSaveOnValidate(false);
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.	
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		ballDVolume = new ParamVolumeCollection("Ball Diffusivity Estimate");
		ballDVolume.setName("Ball (mm2/s)");
		ballDVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(ballDVolume);
		stickVecVolume = new ParamVolumeCollection("Stick Vector Estimate");
		stickVecVolume.setName("Vector (x,y,z)");
		stickVecVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(stickVecVolume);
		fractionVolume = new ParamVolumeCollection("Ball Fraction Estimate");
		fractionVolume.setName("Ball Fraction");
		fractionVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(fractionVolume);
		exitCodeVolume = new ParamVolumeCollection("Estimation Exit Code");
		exitCodeVolume.setName("Exit Code");
		exitCodeVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(exitCodeVolume);	
		intensityVolume = new ParamVolumeCollection("Intensity Estimate");
		intensityVolume.setName("Intensity");
		intensityVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(intensityVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: Start");
			/****************************************************
			 * Step 2. Loop over input slabs
			 ****************************************************/
			List<ParamVolume> dwList = DWdata4D.getParamVolumeList();
			List<ParamVolume> maskList = Mask3D.getParamVolumeList();
			
//			ArrayList<File> outStickVecVols = new ArrayList<File>();
//			ArrayList<File> outBallFracVols = new ArrayList<File>();
//			ArrayList<File> outBallDiffVols = new ArrayList<File>();
//			ArrayList<File> outExitVols = new ArrayList<File>();
//			ArrayList<File> outIntensityVols = new ArrayList<File>();
			this.addTotalUnits(dwList.size());
			for(int jSlab=0;jSlab<dwList.size();jSlab++) {
				/****************************************************
				 * Step 2. Parse the input data 
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Load data.");System.out.flush();
				this.setLabel("Load");
				ImageData dwd=dwList.get(jSlab).getImageData();//rw.read(dwList.get(jSlab));//DWdata4D.getImageData();
				String imageName = dwd.getName();
				ImageDataFloat DWFloat=new ImageDataFloat(dwd);dwd.dispose();

				ImageData maskVol=null;
				if(maskList.size()>jSlab)
					maskVol=maskList.get(jSlab).getImageData();//rw.read(maskList.get(jSlab));//Mask3D.getImageData();
				byte [][][]mask=null;
				if(maskVol!=null) {
					ImageDataUByte maskByte = new ImageDataUByte (maskVol);
					mask = maskByte.toArray3d();
					maskVol.dispose(); maskVol=null;
					maskByte.dispose(); maskByte=null;
				}

				System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
				SchemeV1 DTIscheme = null;

				XStream xstream = new XStream();
				xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
				try {
					ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
					DTIscheme=(SchemeV1)in.readObject();
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				}


				/****************************************************
				 * Step 3. Perform limited error checking 
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Error checking."); System.out.flush();

				BallStickInversion dtiFit=new BallStickInversion(DTIscheme);
				String code = "ballAndStick";

				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program 
				 * 		   has NO knowledge of the MIPAV data structure and 
				 * 		   uses NO MIPAV specific components. This dramatic 
				 * 		   separation is a bit inefficient, but it dramatically 
				 * 		   lower the barriers to code re-use in other applications.  		  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Allocate memory."); System.out.flush();
				float [][][][]data=DWFloat.toArray4d();
				int rows = data.length;
				int cols= data[0].length;
				int slices= data[0][0].length;
				int components= data[0][0][0].length;
				float [][][][]ballDiff = new float[rows][cols][slices][1];
				float [][][][]ballFrac = new float[rows][cols][slices][1];
				float [][][][]stickVec = new float[rows][cols][slices][3];
				float [][][][]exitCode= new float[rows][cols][slices][1];
				float [][][][]intensity= new float[rows][cols][slices][1];

				this.setLabel("Estimate");
				System.out.println(getClass().getCanonicalName()+"\t"+"Run CAMINO estimate."); System.out.flush();
				EstimateTensorLLMSE.estimateBallAndStickCamino(data,mask,dtiFit,ballDiff,ballFrac,stickVec,exitCode,intensity);

				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
				this.setLabel("Save");

				ImageDataFloat out=new ImageDataFloat(stickVec);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Stick");
//				File outputSlab = rw.write(out, getOutputDirectory());
				stickVecVolume.add(out);
//				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();

				out=new ImageDataFloat(ballDiff);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_BallDiff");
//				outputSlab = rw.write(out, getOutputDirectory());
				ballDVolume.add(out);	
//				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();

				out=new ImageDataFloat(ballFrac);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_BallFrac");
//				outputSlab = rw.write(out, getOutputDirectory());
				fractionVolume.add(out);
//				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();


				out=new ImageDataFloat(exitCode);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_ExitCode"+code);
//				outputSlab = rw.write(out, getOutputDirectory());
				//			exitCodeVolume.setValue(out);
				exitCodeVolume.add(out);
//				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();

				out=new ImageDataFloat(intensity);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Intensity"+code);	
				//			intensityVolume.setValue(out);
//				outputSlab = rw.write(out, getOutputDirectory());
				intensityVolume.add(out);
//				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();

				
				ballDVolume.writeAndFreeNow(alg);
				stickVecVolume.writeAndFreeNow(alg);
				fractionVolume.writeAndFreeNow(alg);
				exitCodeVolume.writeAndFreeNow(alg);
				intensityVolume.writeAndFreeNow(alg);
				
				/****************************************************
				 * Step 6. Let the user know that your code is finished.  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: FINISHED");
				this.incrementCompletedUnits();
				
				dwList.get(jSlab).dispose();
				maskList.get(jSlab).dispose();
				
			}

			
		}
	}
}
