package edu.jhu.ece.iacl.plugins.dti.tractography;

import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberStatistics;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;


public class Fiber2Volume extends ProcessingAlgorithm{
	//output params
	private ParamVolume fibervol;

	//input params
	private ParamObject<FiberCollection> fibers;
	private ParamVolume tempvol;

	private ParamOption statoi; //statistic of interest

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fibers=new ParamObject<FiberCollection>("Fibers", new FiberCollectionReaderWriter()));
		inputParams.add(tempvol = new ParamVolume("DataVolume",null,-1,-1,-1,-1));

		String[] stats = {"Count","Mask"};
		inputParams.add(statoi = new ParamOption("Statistic of Interst",stats));
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Fibers To Volume");
		inputParams.setName("Fibers_To_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
//		dwiMotionCorr = new ParamVolumeCollection("Output Diffusion-weighted Volumes");
		fibervol = new ParamVolume("Fiber Volume",null,-1,-1,-1,1);
		outputParams.add(fibervol);
	}


	protected void execute(CalculationMonitor monitor) {
		FiberStatistics fs = new FiberStatistics(fibers.getObject());
		fibervol.setValue(fs.fibers2Volume(statoi.getValue(), tempvol.getImageData()));
	}
}
