package edu.jhu.ece.iacl.plugins.utilities.surface;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


public class MedicAlgorithmSurfaceToVolumeConverter extends ProcessingAlgorithm{
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts a Surface into a simplified volumetric representation.";
	private static final String longDescription = "Can either assign voxel value = 1 where vertices are present, or uses vertex data to assign voxel value.";

	ParamSurface surf;
	ParamVolume templatevol;
	ParamOption method;

	//output param
	ParamVolume outVol;


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surf=new ParamSurface("Surface"));

		inputParams.add(method=new ParamOption("Method",new String[]{"Mask","Vertex Data"}));
		inputParams.add(templatevol=new ParamVolume("Template Volume"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Surface to Volume");
		inputParams.setName("Surface_to_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University, Department of Electrical and Computer Engineering");
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Volume"));
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		if(method.getIndex()==0){
			outVol.setValue(surfToVolumeMask());
		}else{
			outVol.setValue(surfToVolumeVertData());
		}
	}


	private ImageData surfToVolumeMask(){
		ImageData maskvol = templatevol.getImageData().mimic();
		EmbeddedSurface s = surf.getObject();
		maskvol.setName(s.getName()+"_volmask");

		for(int i=0; i<s.getVertexCount(); i++){
			Point3f pt = s.getVertex(i);
			maskvol.set(Math.round(pt.x), Math.round(pt.y), Math.round(pt.z), 1);
		}
		return maskvol;
	}


	private ImageData surfToVolumeVertData(){
		ImageData datavol = templatevol.getImageData().mimic();
		EmbeddedSurface s = surf.getObject();
		datavol.setName(s.getName()+"_voldata");
		for(int i=0; i<s.getVertexCount(); i++){
			Point3f pt = s.getVertex(i);
			datavol.set(Math.round(pt.x), Math.round(pt.y), Math.round(pt.z), s.getVertexData(i)[0]);
		}
		return datavol;
	}
}
