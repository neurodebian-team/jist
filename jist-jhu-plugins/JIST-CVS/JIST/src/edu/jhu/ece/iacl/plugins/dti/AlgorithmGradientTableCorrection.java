package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.dti.GradTableCorrection;
import edu.jhu.ece.iacl.algorithms.dti.GradientTable;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;


public class AlgorithmGradientTableCorrection extends ProcessingAlgorithm{
	//bvaluesTable is for beauty of the layout only
//	private ParamObject<String> gradTable, bvaluesTable, bvaluesTablein;
	private ParamFile bvaluesTable;
	private ParamFileCollection transformations,gradTables, bvaluesTablein;
	private ParamInteger referenceIndex;
	private ParamOption tabletype;
	
	private ParamFile gradOut;
	
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Correct diffusion weighting tables for post-procesing motion correction. Rotate the specified tables to generate 'realized' tables.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		//input Parameters
		inputParams.add(transformations = new ParamFileCollection("Transformations", new FileExtensionFilter(new String[]{"mtx"})));
		inputParams.add(bvaluesTablein=new ParamFileCollection("b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(gradTables = new ParamFileCollection("Gradient Table",new FileExtensionFilter(new String[]{"grad"})));
		inputParams.add(referenceIndex = new ParamInteger("Registration Target Index"));
		String[] tt = {"DTIS","IACL","FSL","MedINRIA"};
		inputParams.add(tabletype = new ParamOption("Output Table Type",tt));
		tabletype.setValue(1);


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Gradient Table Correction");
		inputParams.setName("Gradient_Table_Correction");
		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(gradOut = new ParamFile("Corrected Gradient Table",new FileExtensionFilter(new String[]{"grad"})));
		outputParams.add(bvaluesTable=new ParamFile("b-values out",new FileExtensionFilter(new String[]{"b"})));
	}


	protected void execute(CalculationMonitor monitor) {
		System.out.println(getClass().getCanonicalName()+"\t"+"GRADIENT TABLE CORRECTION:");

		ArrayList<double[][]> matrices = readMultiMatrices(transformations.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"TRANSFORMATIONS READ SUCCESFULLY");
		GradTableCorrection corrector = new GradTableCorrection(gradTables.getValue().get(0), matrices);
		GradientTable gt = new GradientTable(corrector.applyCorrection());
		System.out.println(getClass().getCanonicalName()+"\t"+"TABLE CORRECTION FINISHED");
		String gradstr = "";
		if(tabletype.getIndex()==0){
			gradstr = (gt.toDtisTable());
		}else if(tabletype.getIndex()==1){
			gradstr = (gt.toArrayTxt());
		}else if(tabletype.getIndex()==2){
			gradstr = (gt.toFslTable());
		}else if(tabletype.getIndex()==3){
			gradstr = (gt.toMedINRIATable());
		}else{
			gradstr = ("Problem!");
		}


		//write Gradient Table and bValues to file
		StringReaderWriter rw = new StringReaderWriter();
		File outputdir = this.getOutputDirectory();

		File gradout = null;
		File bvalout = null;
		try{
			File destdir = new File(outputdir.getCanonicalFile()+File.separator+this.getAlgorithmName());

			if(!destdir.isDirectory()){
				(new File(destdir.getCanonicalPath())).mkdir();
			}

			String g = destdir.getCanonicalPath()+File.separator+"GradientTable";
//			if()
//			g = g.substring(0, g.lastIndexOf('.'));
			gradout = new File(g+"_concat.grad");
			System.out.println(getClass().getCanonicalName()+"\t"+"Writing bvalues to : "+gradout);

			rw.write(gradstr, gradout);

			bvalout = new File(destdir.getCanonicalPath()+File.separator+"bvalues.b");
//			String bvaluesout = readAndCatBvalues();
			rw.write(rw.read(bvaluesTablein.getValue(0)), bvalout);

		}catch(IOException e){
			e.printStackTrace();
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"FILES WRITTEN!");

		bvaluesTable.setValue(bvalout);
		gradOut.setValue(gradout);
	}

	private String readAndCatBvalues(){
		String s = "";
		StringReaderWriter rw = new StringReaderWriter();
		for(File f: bvaluesTablein.getValue()){
			s+=rw.read(f);
		}
		return s;
	}


	private ArrayList<double[][]> readMultiMatrices(List<File> files){
		ArrayDoubleMtxReaderWriter rw = new ArrayDoubleMtxReaderWriter();
		ArrayList<double[][]> allxfms = new ArrayList<double[][]>(files.size());
		int i=0;
		int j=0;
		while(i<files.size()){
			if(i==referenceIndex.getInt()){
				allxfms.add(new double[][]{{1,0,0},{0,1,0},{0,0,1}});
			}else{
				allxfms.add(rw.read(files.get(i)));
				j++;
			}
			i++;
		}

		return allxfms;
	}


	/*
	 * VERSION OF THE CODE USED BEFORE TRANSFORMATIONS WERE WRITTEN
	 * TO MULTIPLE FILES
	 * 
	private ArrayList<double[][]> readMultiMatrices(File f){
		ArrayList<double[][]> alltrans = new ArrayList<double[][]>();
		try{
			BufferedReader in = new BufferedReader(new InputStreamReader(
					new FileInputStream(f)));

			String str;
			ArrayList<ArrayList<Double>> datArray=new ArrayList<ArrayList<Double>>();
			ArrayList<Double> array=new ArrayList<Double>();
			while ((str = in.readLine()) != null) {
				// Read file as string
				if (!str.contains("*****")) {
					String[] strs=str.split(" ");
					for(String s:strs){
						try {
							array.add(Double.parseDouble(s));
						}catch(NumberFormatException e){}
					}
					datArray.add((ArrayList<Double>)array.clone());
					array.clear();
				}else{
					double[][] dat=new double[datArray.get(0).size()][datArray.size()];
					for(int i=0;i<datArray.size();i++){
						array=datArray.get(i);
						for(int j=0;j<array.size();j++){
							dat[i][j]=array.get(j);
						}
					}
					alltrans.add(dat);
					datArray.clear();
					array.clear();
				}
			}
			in.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		return alltrans;
	}*/
}
