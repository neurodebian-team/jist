package edu.jhu.ece.iacl.doc;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import javax.swing.ProgressMonitor;
import javax.swing.tree.TreeNode;

import java.util.Hashtable;

import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.pipeline.PipeAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.PipeLibrary;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.dest.PipeParamCopyDestination;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeAlgorithmFactory;
import edu.jhu.ece.iacl.jist.pipeline.graph.PipeModuleFactory;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.tree.PackageNode;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmExecutableAdapter;
import edu.jhu.ece.iacl.jist.plugins.MedicAlgorithmMipavAdapter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;

/**
 * Class automatically document module definitions
 * 
 * @author John Bogovic (bogovic@jhu.edu)
 * @author Blake Lucas
 */

public class DocumentAllModules {
	/** Location of library directory. */
	private File libraryPath = null;
	/** List of algorithms discovered from library. */
	private Vector<PipeAlgorithmFactory> algos;
	private Hashtable<String, StringBuffer> packageDocument;
	private Hashtable<String, File> packageFiles;
	/**
	 * The Class CompareFactories.
	 */
	public static class CompareFactories implements Comparator<PipeModuleFactory>{
		
		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(PipeModuleFactory fact1, PipeModuleFactory fact2) {
			return fact1.getModuleName().compareTo(fact2.getModuleName());
		}
		
	}
	/**
	 * Default constructor.
	 */
	public DocumentAllModules() {
		libraryPath = PipeLibrary.getInstance().getLibraryPath();
		System.out.println(getClass().getCanonicalName()+"\t"+"Library Path " + libraryPath);
	}
	public DocumentAllModules(File path) {
		libraryPath = path;
		System.out.println(getClass().getCanonicalName()+"\t"+"Library Path " + libraryPath);
	}
	protected class ModuleDescription implements Comparable<ModuleDescription> {
		String moduleName;
		String category;
		String url;
		File location;
		String className;

		public ModuleDescription(String moduleName, String className,String category, File location,String url) {
			this.moduleName = moduleName;
			this.category = category;
			this.url=url;
			this.className=className;
			this.location=location;
		}

		public int compareTo(ModuleDescription obj) {
			return moduleName.compareTo(obj.moduleName);
		}

	}

	/**
	 * Searches for modules and writes documentation to file.
	 */
	public void documentModules() {
		Vector<Class> classes = PipeLibrary.getAllClasses("edu");
		algos = new Vector<PipeAlgorithmFactory>();
		File documentPath = new File(libraryPath, "docs" + File.separatorChar);
		if (!documentPath.exists()) {
			documentPath.mkdir();
		}
		System.out.println(getClass().getCanonicalName()+"\t"+"DOCUMENT PATH " + documentPath);
		packageDocument = new Hashtable<String, StringBuffer>();
		packageFiles = new Hashtable<String, File>();
		Vector<ModuleDescription> modules = new Vector<ModuleDescription>();

		DocumentGenerator mww = createDocumentGenerator();
		for (Class c : classes) {
			try {
				if (ProcessingAlgorithm.class.equals(c.getSuperclass())) {
					PipeAlgorithm algo = new PipeAlgorithm(
							(ProcessingAlgorithm) c.newInstance());

					if (!containsAlgorithm(algo)) {
						String category = (algo.getPackage() + ((algo
								.getCategory() != null) ? ("." + algo
								.getCategory()) : "")).replace('_', ' ');
						String address = category.replace('.',
								File.separatorChar);

						String moduleName = algo.getAlgorithm()
								.getAlgorithmLabel();

						File rootDir = new File(documentPath.getAbsolutePath()
								+ File.separatorChar + address
								+ File.separatorChar);
						if (packageFiles.get(category) == null) {
							packageFiles.put(category, rootDir);
							packageDocument.put(category, new StringBuffer());
						}
						if (!rootDir.exists())
							rootDir.mkdirs();
						String simpleName = algo.getAlgorithm().getClass()
						.getSimpleName();

						File f = new File(rootDir, simpleName + ".html");

						modules.add(new ModuleDescription(moduleName, simpleName,category,f,fileToUrl(f, documentPath)));
						StringReaderWriter.getInstance().write(
								mww.createDocument(algo
										.getAlgorithm()), f);
					}
				}
			} catch (InstantiationException e) {
				System.err.println("jist.plugins"+e.getMessage());
			} catch (IllegalAccessException e) {
				System.err.println("jist.plugins"+e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Collections.sort(modules);
		StringBuffer allPackages = new StringBuffer();
		allPackages
				.append("<HTML><HEAD></HEAD><BODY><B>JIST Module Documentation</B><BR>" 
						+ "<font size=\"2\"><a href=\"license/JIST-License.html\" target=\"moduleFrame\">Overview</a><BR>"
						+"<a href=\"http://www.nitrc.org/projects/jist/\" target=\"_top\">JIST Project Home</a><BR>"
						+ "<a href=\"all-modules.html\" target=\"packageFrame\">All Modules</a><BR>");

		StringBuffer allModules = new StringBuffer();
		allModules
				.append("<HTML><HEAD><TITLE>All JIST Modules</TITLE></HEAD><BODY><B>All Modules</B><BR><font size=\"2\">\n");
		for (ModuleDescription module : modules) {
			String category=module.category;
			StringBuffer packageFile = packageDocument
					.get(category);
			packageFile.append("<B>- </B><a href=\"" +module.location.getName()
					+ "\" target=\"moduleFrame\">" + module.moduleName
					+ "</a><BR>\n");
			allModules.append("<B>- </B><a href=\"" + module.url
					+ "\" target=\"moduleFrame\">" + module.moduleName
					+ "</a><BR>\n");
		}
		allModules.append("</font></BODY></HTML>");
		String[] keyArray = new String[packageFiles.keySet().size()];
		packageFiles.keySet().toArray(keyArray);
		Arrays.sort(keyArray);
		for (String key : keyArray) {

			File file = packageFiles.get(key);
			File packageFile = new File(file, File.separatorChar
					+ "package.html");
			StringBuffer buffer = packageDocument.get(key);
			buffer.insert(0, "<HTML><HEAD><TITLE>" + key
					+ " Module</TITLE></HEAD><BODY>\n<B>" + key
					+ "</B><BR>\n<font size=\"2\">");
			buffer.append("</font></BODY></HTML>");
			StringReaderWriter.getInstance().write(buffer.toString(),
					packageFile);
			allPackages.append("<B>- </B><a href=\""
					+ fileToUrl(packageFile, documentPath)
					+ "\" target=\"packageFrame\">" + key + "</a><BR>\n");
		}

		allPackages.append("\n</font></BODY></HTML>");
		StringReaderWriter.getInstance()
				.write(
						allPackages.toString(),
						new File(documentPath, File.separatorChar
								+ "all-packages.html"));
		StringReaderWriter.getInstance()
				.write(
						allModules.toString(),
						new File(documentPath, File.separatorChar
								+ "all-modules.html"));

		try {
			PipeParamCopyDestination.copyFile(new File(DocumentAllModules.class
					.getResource("index.html").toURI()), new File(documentPath,
					"index.html"));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		copyLicenseInformation(documentPath);
	}

	private void copyLicenseInformation(File documentPath) {
		File licenseFile;
		try {
			licenseFile = new File(license.JISTLicenseAbout.class.getResource(
					"JIST-License.html").toURI());
			File licensePath = new File(documentPath, "license"
					+ File.separatorChar);
			if (!licensePath.exists())
				licensePath.mkdir();
			for (File f : licenseFile.getParentFile().listFiles()) {
				String ext = FileReaderWriter.getFileExtension(f);
				if (!ext.equalsIgnoreCase("class")
						&& !ext.equalsIgnoreCase("java")) {
					PipeParamCopyDestination.copyFile(f, new File(licensePath,
							f.getName()));
				}
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String fileToUrl(File file, File root) {
		String f = file.getPath().replace(root.getPath() + File.separatorChar,
				"").replace(File.separatorChar, '/');
		return f;
	}

	protected DocumentGenerator createDocumentGenerator() {
		return new DocumentGeneratorHTML();
	}

	private boolean containsAlgorithm(PipeAlgorithm algo) {
		Class algoClass = algo.getAlgorithmClass();
		if (MedicAlgorithmExecutableAdapter.class.equals(algoClass)
				|| MedicAlgorithmMipavAdapter.class.equals(algoClass)) {
			return false;
		}
		for (PipeAlgorithmFactory child : algos) {
			Class childClass = child.getModuleClass();
			if ((childClass != null) && (algoClass != null)
					&& childClass.equals(algoClass)) {
				return true;
			}
		}
		return false;
	}

	public File createPath(File discoveredPath, String category) {
		if (category == null) {
			return discoveredPath;
		}
		File f = new File(discoveredPath, category.replace('_', ' '));
		if (!f.exists()) {
			f.mkdirs();
		}
		return f;
	}

	/**
	 * Run me.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		MipavController.setQuiet(true);
		MipavController.init();
		PipeLibrary.getInstance().loadPreferences(true);
		DocumentAllModules docs; 
		if(args.length==0){
			docs=new DocumentAllModules();			
			docs.documentModules();
		} else if(args.length>0){
			//Output directory specified
			docs=new DocumentAllModules(new File(args[0]));
			docs.documentModules();
		} else {
			System.err.println("jist.plugins"+"Usage: edu.jhu.ece.iacl.doc.DocumentAllModules output_directory");
		}

		
		System.exit(0);
	}

}
