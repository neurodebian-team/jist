package edu.jhu.ece.iacl.plugins.dti;

import java.awt.Color;

import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class DWITensorColorEncodedMap extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolume scalarVolume;	// Any volume of scalar weights
	private ParamVolume vectorVolume;	// Any volume of 3-vectors

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume RGBVolume;	// Combined RGB Map

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Create an RGB colormap image from a scalar intensity weighted by a vector.";
	private static final String longDescription = "Use a scalar volume (intensity in [0,1] to weight a vector volume (4-D, 4th dimension = x,y,z, in [-1 1]) to create a single RGB color volume. This is typically used with an FA weight an PEV vector.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Contrasts");
		inputParams.setLabel("Tensor Compute Color Encoded Map");
		inputParams.setName("Tensor_Compute_Color_Encoded_Map");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new Citation("Basser, PJ, Jones, DK. \"Diffusion-tensor MRI: Theory, experimental design and data analysis - a technical review.\" NMR Biomed 2002; 15(7-8):456-67"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(scalarVolume=new ParamVolume("Scalar Weight Volume (3D)",null,-1,-1,-1,1));
		inputParams.add(vectorVolume=new ParamVolume("Vector direction to encoded color (4D)",null,-1,-1,-1,3));				
	}


	protected void createOutputParameters(ParamCollection outputParams) {		
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/

		// Base Outputs
		RGBVolume = new ParamVolume("Direction Encoded Colormap",VoxelType.COLOR,-1,-1,-1,1);
		RGBVolume.setName("RGB");
		outputParams.add(RGBVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorColorEncodedMap: Start");

			/****************************************************
			 * Step 2. Parse the input data 
			 ****************************************************/

			ImageDataFloat scalarVol=new ImageDataFloat(scalarVolume.getImageData());
			ImageDataFloat vectorVol=new ImageDataFloat(vectorVolume.getImageData());
			int r=scalarVol.getRows(), c=scalarVol.getCols(), s=scalarVol.getSlices(), t = scalarVol.getComponents();

			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: Allocating memory.");
			Color [][][]rgb = new Color[r][c][s];

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program 
			 * 		   has NO knowledge of the MIPAV data structure and 
			 * 		   uses NO MIPAV specific components. This dramatic 
			 * 		   separation is a bit inefficient, but it dramatically 
			 * 		   lower the barriers to code re-use in other applications.  		  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"Updated."+scalarVol+" "+scalarVol.toArray3d());
			System.out.println(getClass().getCanonicalName()+"\t"+"Update2."+ vectorVol+" "+vectorVol.toArray4d());
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorColorEncodedMap: Computing contrasts");
			ComputeTensorContrasts.computeOrientationEncodedColormap(scalarVol.toArray3d(),
					vectorVol.toArray4d(),rgb);


			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorContrasts: Setting up exports.");
			int []ext=scalarVol.getModelImageCopy().getExtents();
			//		ModelImage img=null;

			ImageData out;
			out = (new ImageDataColor(rgb));
			out.setHeader(scalarVol.getHeader());/*
		img=out.getModelImage();		
//		ext[3]=1; (refactored to be 3D only)
		img.setExtents(ext);img.calcMinMaxNonZero();		
		FileUtil.updateFileInfo(scalarVol.getModelImage(),img);*/
			out.setName(scalarVolume.getName()+"_DEC");
			RGBVolume.setValue(out);
			rgb=null; // dereference to free memory

			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorColorEncodedMap: FINISHED");
		}
	}
}
