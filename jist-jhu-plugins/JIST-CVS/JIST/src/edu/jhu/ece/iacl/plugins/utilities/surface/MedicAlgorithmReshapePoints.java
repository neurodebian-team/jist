package edu.jhu.ece.iacl.plugins.utilities.surface;

import java.io.File;

import javax.vecmath.Point3d;
import javax.vecmath.Point3f;

import Jama.Matrix;
import edu.jhu.ece.iacl.jist.io.FileReaderWriter;
import edu.jhu.ece.iacl.jist.io.VertexFloatDxReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;


public class MedicAlgorithmReshapePoints extends ProcessingAlgorithm{
	ParamPointDouble offset;
	ParamDouble scalePoints;
	ParamObject<Point3f[]> inPoints,outPoints;
	ParamMatrix transMatrix;
	ParamBoolean invert;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Scale and/or apply a transformation matrix to a collection of points in 3D space.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inPoints=new ParamObject<Point3f[]>("Points",new VertexFloatDxReaderWriter()));
		inputParams.add(scalePoints=new ParamDouble("Scale Vertices",0,1000,1));
		inputParams.add(offset=new ParamPointDouble("Offset"));
		inputParams.add(transMatrix=new ParamMatrix("Transformation Matrix",4,4));
		for (int i = 0; i < 4; i++) transMatrix.setValue(i, i, 1);

		inputParams.add(invert=new ParamBoolean("Invert Matrix",false));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Reshape Points");
		inputParams.setName("Reshape_Points");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outPoints=new ParamObject<Point3f[]>("Transformed Points",new VertexFloatDxReaderWriter()));
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		Point3f[] pts=inPoints.getObject();
		float scale=scalePoints.getFloat();
		Point3d translate=offset.getValue();
		Matrix m=transMatrix.getValue();
		Matrix vec=new Matrix(4,1);
		vec.set(3, 0, 1);
		Point3f[] npts=new Point3f[pts.length];
		int i=0;
		Point3f npt;
		for(Point3f pt:pts){
			npt=npts[i++]=new Point3f(pt.x,pt.y,pt.z);
			npt.x*=scale;
			npt.y*=scale;
			npt.z*=scale;
			npt.x+=translate.x;
			npt.y+=translate.y;
			npt.z+=translate.z;
			vec.set(0, 0, npt.x);
			vec.set(1, 0, npt.y);
			vec.set(2, 0, npt.z);
			if(invert.getValue())m=m.inverse();
			Matrix r=m.times(vec);
			double w=r.get(3, 0);
			npt.x=(float)(r.get(0, 0)/w);
			npt.y=(float)(r.get(1, 0)/w);
			npt.z=(float)(r.get(2, 0)/w);
		}
		File f=inPoints.getValue();
		outPoints.setFileName(FileReaderWriter.getFileName(f)+"_trans");
		outPoints.setObject(npts);
	}
}
