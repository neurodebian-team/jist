/*
 *
 */
package edu.jhu.ece.iacl.plugins.registration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import Jama.Matrix;
import WildMagic.LibFoundation.Mathematics.Vector3f;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmMultiDeformVolume.ExecuteWrapper;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class MedicAlgorithmTransformVolume extends ProcessingAlgorithm {
	public ParamMatrix matrix;
	public ParamVolumeCollection vol;
	public ParamVolumeCollection result;
	public ParamOption interpolation;
	public ParamPointInteger dims;
	public ParamPointFloat res;
	public ParamBoolean clip;
	public ParamBoolean pad;
	public ParamBoolean updateOrigin;
	public ParamBoolean useScanner;
	public ParamBoolean invert;
	public ParamInteger padAmt;
	public ParamBoolean rotAroundCenter;
	public ParamBoolean useAnatomicalCenter;
	public ParamVolume matchToMe;
	public ParamObject<double[][]> matrixFile;
	public ParamFileCollection transformations;
	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.14 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Applies a given transformation matrix to a volumetric image using the specified interpolation method.";
	private static final String longDescription = "";


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vol = new ParamVolumeCollection("Volumes"));
		vol.setLoadAndSaveOnValidate(false);
		inputParams.add(interpolation = new ParamOption("Registration interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc", "Nearest Neighbor" }));
		inputParams.add(res = new ParamPointFloat("Resolutions", new Point3f(1, 1, 1)));
		inputParams.add(dims = new ParamPointInteger("Dimensions", new Point3i(256, 256, 198)));
		inputParams.add(matchToMe = new ParamVolume("Volume to Match - Dimensions & Resolution", null,-1,-1,-1,-1));
		matchToMe.setLoadAndSaveOnValidate(false);
		matchToMe.setMandatory(false);

		inputParams.add(matrix = new ParamMatrix("Transformation Matrix", 4, 4));
		matrix.setMandatory(false);
		for (int i = 0; i < 4; i++){
			matrix.setValue(i, i, 1);
		}

		inputParams.add(transformations = new ParamFileCollection("Transformation Matrices", new FileExtensionFilter(new String[]{"mtx"})));
		transformations.setMandatory(false);

		inputParams.add(invert=new ParamBoolean("Invert Matrix",false));
		inputParams.add(rotAroundCenter=new ParamBoolean("Rotate Around Center",true));
		inputParams.add(useAnatomicalCenter=new ParamBoolean("Use Anatomical Center",false));
		inputParams.add(updateOrigin = new ParamBoolean("Update Origin", true));
		inputParams.add(useScanner = new ParamBoolean("Use Scanner Anatomical Transform"));

		inputParams.add(clip = new ParamBoolean("Crop"));
		inputParams.add(pad = new ParamBoolean("Pad"));
		inputParams.add(padAmt = new ParamInteger("Pad Value", 0));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration");
		inputParams.setLabel("Transform Volume");
		inputParams.setName("Transform_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("Blake Lucas", "", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(result = new ParamVolumeCollection("Transformed Volumes"));
		result.setLoadAndSaveOnValidate(false);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ExecuteWrapper wrapper=new ExecuteWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}
	protected class ExecuteWrapper extends AbstractCalculation{
		public void execute(ProcessingAlgorithm alg){
		List<ImageData> imglist = vol.getImageDataList();
		//ArrayList<ImageData> resultlist = new ArrayList<ImageData>();
		int i =0;

		ArrayList<double[][]> xfms = readMultiMatrices(transformations.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+transformations.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+transformations.getValue().size());
		ModelImage img = null;
		Matrix m;
		for(ImageData v : imglist){
			img = v.getModelImageCopy();
			m = new Matrix(4,4);
			if(transformations.getValue().size()>0){
				double[][] matarray = xfms.get(i);
				if(matarray.length!=4 || matarray[0].length!=4){
					System.err.println(getClass().getCanonicalName()+"Invalid transformation - must be 4x4");
				}else{
					for(int k=0; k<matarray.length; k++){
						for(int j=0; j<matarray[0].length; j++){
							m.set(k, j, matarray[k][j]);
						}
					}
				}
			}else{
				m = matrix.getValue();
			}
			result.add(new ImageDataMipavWrapper(transform(img,m)));
			result.writeAndFreeNow(alg);
			i++;
			v.dispose();
			if(img != null){
				img.disposeLocal();
			}
		}
		imglist = null; vol.dispose();vol=null;
		//result.setValue(resultlist);
	}

// TODO: This should really use the static transform class! 
	private ModelImage transform(ModelImage img, Matrix m){
		int interp = 0;
		switch (interpolation.getIndex()) {
		case 0:
			interp = AlgorithmTransform.TRILINEAR;
			break;
		case 1:
			interp = AlgorithmTransform.BSPLINE3;
			break;
		case 2:
			interp = AlgorithmTransform.BSPLINE4;
			break;
		case 3:
			interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
			break;
		case 4:
			interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
			break;
		case 5:
			interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
			break;
		case 6:
			interp = AlgorithmTransform.WSINC;
			break;
		 case 7:
			 interp = AlgorithmTransform.NEAREST_NEIGHBOR;
			 break;
		default:
			interp = AlgorithmTransform.TRILINEAR;
			break;
		}
		Point3i dimensions;
		Point3f resolution;
		if(matchToMe.getImageData()!=null){
			int[] a = matchToMe.getImageData().getModelImageCopy().getExtents();
			dimensions = new Point3i(a[0],a[1],a[2]);
			float[] b =  matchToMe.getImageData().getModelImageCopy().getResolutions(0);
			resolution = new Point3f(b[0],b[1],b[2]);
		}else{
			resolution = res.getValue();
			dimensions = dims.getValue();
		}
		TransMatrix mat = new TransMatrix(4);
//		Matrix m;
//
//		if(matrixFile.getValue()!=null){
//			double[][] arin = matrixFile.getObject();
//			m = new Matrix(arin.length, arin[0].length);
//			for(int i=0; i<arin.length; i++){
//				for(int j=0; j<arin[0].length; j++){
//					m.set(i, j, arin[i][j]);
//				}
//			}
//		}else{
//			m=matrix.getValue();
//		}


		if(invert.getValue())m=m.inverse();
		double[][] ar=m.getArray();
		System.out.println(getClass().getCanonicalName()+"\t"+m);

//		mat.setMatrix(0,ar.length-1,0,ar[0].length,ar);

		//First Set mat as the Identity
		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				mat.set(i, j, 1);
			}
		}

		for(int i=0; i<ar.length; i++){
			for(int j=0; j<ar[0].length; j++){
				mat.set(i, j, ar[i][j]);
			}
		}
		System.out.println(getClass().getCanonicalName()+"\t"+mat.matrixToString(4, 3));

		 Vector3f center = null;
        if (rotAroundCenter.getValue()) {
            center = img.getImageCentermm(useAnatomicalCenter.getValue());
        }
        int[] units = new int[img.getUnitsOfMeasure().length];
        for (int i = 0; i < units.length; i++) {
        	units[i] = img.getUnitsOfMeasure(i);
        }
		AlgorithmTransform transform = new AlgorithmTransform(img, mat, interp, resolution.x, resolution.y,
				resolution.z, dimensions.x, dimensions.y, dimensions.z, units,true,clip.getValue(), pad.getValue(),rotAroundCenter.getValue(),center);
		transform.setUpdateOriginFlag(updateOrigin.getValue());

		transform.setPadValue(padAmt.getInt());
		transform.setUseScannerAnatomical(useScanner.getValue());

		transform.run();
		ModelImage resultImage=transform.getTransformedImage();

		return resultImage;
	}


	private ArrayList<double[][]> readMultiMatrices(List<File> files){
		ArrayDoubleMtxReaderWriter rw = new ArrayDoubleMtxReaderWriter();
		ArrayList<double[][]> allxfms = new ArrayList<double[][]>(files.size());
		int i=0;
		while(i<files.size()){
			allxfms.add(rw.read(files.get(i)));
			i++;
		}
		return allxfms;
	}
	}
}
