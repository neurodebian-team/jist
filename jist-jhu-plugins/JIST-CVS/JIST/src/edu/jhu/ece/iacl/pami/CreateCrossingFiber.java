package edu.jhu.ece.iacl.pami;

import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.ArrayDoubleListTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;


public class CreateCrossingFiber extends ProcessingAlgorithm{
	private ParamVolumeCollection DWdata4D; 			// Imaging Data
	private ParamObject<ArrayList<double[][]>> smR;		// Weighting of each tensor
	private ParamVolume outVol;							// Output image after weighting

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Creates a crossing fiber pattern by weighting each tensor. Note: Does not do error checking for size mismatch. Please make sure weighting arrays are the same size as the images.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Simulation");
		inputParams.setLabel("Create a Crossing Fiber Pattern");
		inputParams.setName("Create_a_Crossing_Fiber_Pattern");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(DWdata4D=new ParamVolumeCollection("DWI and Reference Image(s) Data (4D)"));
		inputParams.add(smR=new ParamObject<ArrayList<double[][]>>("Weighting Arrays",new ArrayDoubleListTxtReaderWriter()));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ArrayList<double[][]> weight = smR.getObject();
		ImageData img = DWdata4D.getImageDataList().get(0);
		int rows = img.getRows();
		int cols = img.getCols();
		int slices = img.getSlices();
		int comps = img.getComponents();
		int n = DWdata4D.getImageDataList().size();

		ImageData resultVol = new ImageDataFloat(DWdata4D.getImageDataList().get(0));
		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				for (int k=0; k<slices; k++) {
					for (int l=0; l<comps; l++) {
						double val = 0;
						for (int m=0; m<n; m++) {
							val += DWdata4D.getImageDataList().get(m).get(i,j,k,l).doubleValue() * weight.get(m)[j][i];
						}
						resultVol.set(i, j, k, l, val);
					}
				}
			}
		}
		resultVol.setName(resultVol.getName()+"_calc");
		outVol.setValue(resultVol);
	}
}
