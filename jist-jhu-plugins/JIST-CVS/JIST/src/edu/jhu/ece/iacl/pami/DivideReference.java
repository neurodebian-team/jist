package edu.jhu.ece.iacl.pami;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;


public class DivideReference extends ProcessingAlgorithm {
	private ParamVolumeCollection volParam;
	private ParamVolumeCollection refParam;
	protected ParamVolumeCollection resultVolParam;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Divides the 3D image by the voxelwise average of the reference images.";
	private static final String longDescription = "Processes each volume in the input collections independently.";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Tools");
		inputParams.setLabel("Divide by Reference");
		inputParams.setName("Divide_by_Reference");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Hanlin Wan","hanlinwan@gmail.com",""));
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(volParam= new ParamVolumeCollection("Volumes"));
		volParam.setLoadAndSaveOnValidate(false);
		inputParams.add(refParam= new ParamVolumeCollection("Reference Volumes"));
		refParam.setLoadAndSaveOnValidate(false);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVolParam= new ParamVolumeCollection("Output Volumes"));
		resultVolParam.setLoadAndSaveOnValidate(false);
	}


	protected void execute(CalculationMonitor monitor)
	throws AlgorithmRuntimeException {

		ImageData vol = refParam.getParamVolume(0).getImageData();
		ImageHeader hdr = vol.getHeader();
		int rows=vol.getRows();
		int cols=vol.getCols();
		int slices=vol.getSlices();
		int comps = vol.getComponents();
		int refComps=refParam.getValue().size();
		vol.dispose();

		ImageData ref = new ImageDataFloat(rows,cols,slices);

		ref.setHeader(hdr);
		// Average the reference images
		for (int f=0; f<refComps; f++) {
			vol = refParam.getParamVolume(f).getImageData();
			for (int i=0; i<rows; i++) {
				for (int j=0; j<cols; j++) {
					for (int k=0; k<slices; k++) {
						for(int l=0;l<comps;l++)
							ref.set(i,j,k,l,(ref.getFloat(i,j,k,l)+vol.getFloat(i,j,k,l)));
					}
				}
			}
			System.out.flush();
			vol.dispose();			
		}

		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				for (int k=0; k<slices; k++) {
					for(int l=0;l<comps;l++)
						ref.set(i,j,k,l,ref.getFloat(i,j,k,l)/refComps);
				}
			}
		}

		// Divide by reference image
		for (int f=0; f<volParam.size(); f++) {
			vol = volParam.getParamVolume(f).getImageData();
			for (int i=0; i<rows; i++) {
				for (int j=0; j<cols; j++) {
					for (int k=0; k<slices; k++) {
						for(int l=0;l<comps;l++) {
							float s=0;
							if (ref.getFloat(i,j,k)!=0)
								s=vol.getFloat(i,j,k)/ref.getFloat(i,j,k);
							vol.set(i,j,k,s);
						}
					}
				}
			}
			vol.setName(vol.getName()+"_Attenuated");
			resultVolParam.add(vol);
			resultVolParam.writeAndFreeNow(this);
		}
		
	}
}
