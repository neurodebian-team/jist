package edu.jhu.ece.iacl.algorithms.dti.tractography;

import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
public class FiberTransformation {
   
    public static FiberCollection applyAffine(FiberCollection fibsin, double[][] T){
    	FiberCollection fibsout;
        if(T.length==3 && T[0].length==3){
        	System.out.println("jist.plugins"+"\t"+"Applying 3d transformation");
            fibsout = new FiberCollection(fibsin.size());
            fibsout.setDimensions(fibsin.getDimensions());
            fibsout.setResolutions(fibsin.getResolutions());
            for(Fiber f : fibsin){
                XYZ[] tchain = new XYZ[f.getXYZChain().length];
                int i=0;
                for(XYZ p : f.getXYZChain()){
                    tchain[i]=FiberTransformation.transformXYZ3(T, p);
                    i++;
                }
                fibsout.add(new Fiber(tchain));
            }
        }else if(T.length==4 && T[0].length==4){
        	System.out.println("jist.plugins"+"\t"+"Applying 4d transformation");
            fibsout = new FiberCollection(fibsin.size());
            fibsout.setDimensions(fibsin.getDimensions());
            fibsout.setResolutions(fibsin.getResolutions());
            
            for(Fiber f : fibsin){
                XYZ[] tchain = new XYZ[f.getXYZChain().length];
                int i=0;
                for(XYZ p : f.getXYZChain()){
                    tchain[i]=FiberTransformation.transformXYZ4(T, p);
                    
                    i++;
                }
                fibsout.add(new Fiber(tchain));
            }
        }else{
            System.err.println("jist.plugins"+"Matrix has wrong dimensions, must be 3x3 or 4x4");
            return null;
        }
        
        return fibsout;
    }
   
    private static XYZ transformXYZ3(double[][] T, XYZ p){
        return new XYZ((float)(T[0][0]*p.x + T[0][1]*p.y + T[0][2]*p.z),
        		(float)(T[1][0]*p.x + T[1][1]*p.y + T[1][2]*p.z),
        		(float)(T[2][0]*p.x + T[2][1]*p.y + T[2][2]*p.z));
    }
    private static XYZ transformXYZ4(double[][] T, XYZ p){
        return new XYZ((float)(T[0][0]*p.x + T[0][1]*p.y + T[0][2]*p.z + T[0][3]),
        		(float)(T[1][0]*p.x + T[1][1]*p.y + T[1][2]*p.z + T[1][3]),
        		(float)(T[2][0]*p.x + T[2][1]*p.y + T[2][2]*p.z + T[2][3]));
    }
    
    public static FiberCollection applyDeformation(FiberCollection fibsin, ImageDataMipav T){
    
    	if(T.getComponents()!=3){
    		System.err.println("jist.plugins"+"Improperly formatted deformation - must have 3 components");
    		return null;
    	}
    	FiberCollection fibsout = new FiberCollection(fibsin.size());
    	fibsout.setDimensions(fibsin.getDimensions());
        fibsout.setResolutions(fibsin.getResolutions());
        for(Fiber f : fibsin){
            XYZ[] tchain = new XYZ[f.getXYZChain().length];
            int i=0;
            for(XYZ p : f.getXYZChain()){
                
            	
            	//Linear interpolation
            	float[] v = new float[3];
            	for(int j=0; j<3; j++){

            		int xf = (int)Math.floor(p.x);
            		int xc = (int)Math.ceil(p.x);
            		int yf = (int)Math.floor(p.y);
            		int yc = (int)Math.ceil(p.y);
            		int zf = (int)Math.floor(p.z);
            		int zc = (int)Math.ceil(p.z);

            		float xd = p.x-xf;
            		float yd = p.y-yf;
            		float zd = p.z-zf;

            		float i1 = T.getFloat(xf, yf, zf, j)*(1-zd) + T.getFloat(xf, yf, zc, j)*zd;
            		float i2 = T.getFloat(xf, yf, zf, j)*(1-zd) + T.getFloat(xf, yc, zc, j)*zd;
            		float j1 = T.getFloat(xc, yf, zf, j)*(1-zd) + T.getFloat(xf, yc, zc, j)*zd;
            		float j2 = T.getFloat(xc, yf, zf, j)*(1-zd) + T.getFloat(xc, yc, zc, j)*zd;

            		float w1 = i1*(1-yd)+i2*yd;
            		float w2 = j1*(1-yd)+j2*yd;

            		v[j] = w1*(1-xd)+w2*xd;
            	}
            	
            	tchain[i]=new XYZ(p.x+v[0],p.y+v[1],p.z+v[2]);
            	
                i++;
            }
            fibsout.add(new Fiber(tchain));
        }
    	
    	return fibsout;
    }
    
}