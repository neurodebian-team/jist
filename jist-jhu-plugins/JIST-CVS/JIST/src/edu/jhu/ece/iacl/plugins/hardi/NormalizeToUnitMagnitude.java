package edu.jhu.ece.iacl.plugins.hardi;

import edu.jhu.ece.iacl.algorithms.hardi.SurfaceTools;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class NormalizeToUnitMagnitude extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters 
	 ****************************************************/	
	private ParamSurface surface;


	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamSurface outSurface;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Normalize to Unit Magnitude");	
		inputParams.setName("Normalize_to_Unit_Magnitude");	


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(surface=new ParamSurface("Input Surface"));				
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputParams.add(outSurface=new ParamSurface("Output Surface"));
	}


	protected void execute(CalculationMonitor monitor) {
		EmbeddedSurface surf=surface.getSurface();
		EmbeddedSurface surf2=new EmbeddedSurface(SurfaceTools.normalizeToSphere(surf, 1));
		surf2.setName(surf.getName()+"_normalized");
		outSurface.setValue(surf2);
	}
}
