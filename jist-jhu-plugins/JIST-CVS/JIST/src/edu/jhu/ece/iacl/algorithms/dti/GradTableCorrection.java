package edu.jhu.ece.iacl.algorithms.dti;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;

public class GradTableCorrection {
	
	double[][] table;
	double[][] angCorrGT;
	
//	ArrayList<Matrix> transMatrices;
	ArrayList<double[][]> matrices;
	
	public GradTableCorrection(String table, ArrayList<double[][]> transMatrices){
		this.matrices=transMatrices;
		GradientTable gt = new GradientTable();
		gt.readTable(new File(table));
		this.table=gt.getTable();
	}
	public GradTableCorrection(File table, ArrayList<double[][]> transMatrices){
		this.matrices=transMatrices;
		GradientTable gt = new GradientTable();
		gt.readTable(table);
		this.table=gt.getTable();
	}
	public GradTableCorrection(List<File> tables, ArrayList<double[][]> transMatrices){
		this.matrices=transMatrices;
		GradientTable gt = new GradientTable();
		readAndConcatTables(tables);
		this.table=gt.getTable();
	}
	public GradTableCorrection(double[][] table, ArrayList<double[][]> matrices){
		this.matrices=matrices;
		this.table=table;
	}

	public double[][] getCorrectedTable(){
		return angCorrGT;
	}
	
	public double[][] Matrix2Array(Matrix A){
		double[][] transout = new double[A.getRowDimension()][A.getColumnDimension()];
	
		for(int i=0; i<transout.length; i++){
			for(int j=0; j<transout[0].length; j++){
				transout[i][j]=A.get(i, j);
			}
		}
		return transout;
	}
	
	
	//See Jon Farrell's matlab code for how to read air files.
	// for how to rotate 1666
	//don't rotate 000 or 100,100,100 line

	
	public double[][] applyCorrection(double[][] table){
		angCorrGT = new double[table.length][table[0].length];
		for(int i=0; i<angCorrGT.length; i++){
				
				
				double[][] row = {{table[i][0]},{table[i][1]},{table[i][2]}};
				if((row[0][0]==0 && row[1][0]==0 && row[2][0]==0)||(row[0][0]==100 && row[1][0]==100 && row[2][0]==100)){
					angCorrGT[i][0]=row[0][0];
					angCorrGT[i][1]=row[1][0];
					angCorrGT[i][2]=row[2][0];
				}else{
					double[][] A = matrices.get(i);
					double[][] newrow = matrixMultiply(A,row);

					angCorrGT[i][0]=newrow[0][0];
					angCorrGT[i][1]=newrow[1][0];
					angCorrGT[i][2]=newrow[2][0];
				}
				
		}
		angCorrGT = normalizeTable(angCorrGT);
		System.out.println(getClass().getCanonicalName()+"\t"+"Corrected Table:");
		System.out.println(getClass().getCanonicalName()+"\t"+DTIGradientTableCreator.tableToString(angCorrGT));
		return angCorrGT;
	}
	
	public double[][] applyCorrection(){
		angCorrGT = new double[table.length][table[0].length];
		for(int i=0; i<angCorrGT.length; i++){
				
				
				double[][] row = {{table[i][0]},{table[i][1]},{table[i][2]}};
				if((row[0][0]==0 && row[1][0]==0 && row[2][0]==0)||(row[0][0]==100 && row[1][0]==100 && row[2][0]==100)){
					angCorrGT[i][0]=row[0][0];
					angCorrGT[i][1]=row[1][0];
					angCorrGT[i][2]=row[2][0];
				}else{
					double[][] A = matrices.get(i);
					double[][] newrow = matrixMultiply(A,row);

					angCorrGT[i][0]=newrow[0][0];
					angCorrGT[i][1]=newrow[1][0];
					angCorrGT[i][2]=newrow[2][0];
				}
				
		}
		
		System.out.println(getClass().getCanonicalName()+"\t"+"Corrected Table:");
		System.out.println(getClass().getCanonicalName()+"\t"+DTIGradientTableCreator.tableToString(angCorrGT));
		return angCorrGT;
	}
	
	public double[][] matrixMultiply(double[][] A, double[][] B){
		double[][] C = new double[A.length][B[0].length];
		for(int i=0; i<C.length; i++){
			for(int j=0; j<C[0].length; j++){
				C[i][j]=A[i][0]*B[0][j] + A[i][1]*B[1][j]+A[i][2]*B[2][j];
			}
		}
		return C;
	}
	
	private double[][] normalizeTable(double[][] table){
		double[][] normTable = new double[table.length][table[0].length];
		for(int i=0; i<normTable.length; i++){
			double length = table[i][0]*table[i][0] + table[i][1]*table[i][1] + table[i][2]*table[i][2]; 
			if(length!=0){
				length=Math.sqrt(length);
				normTable[i][0]=table[i][0]/length;
				normTable[i][1]=table[i][1]/length;
				normTable[i][2]=table[i][2]/length;
			}else{
				normTable[i][0]=table[i][0];
				normTable[i][1]=table[i][1];
				normTable[i][2]=table[i][2];
			}
		}
		
		return normTable;
	}
	
	private double[][] readAndConcatTables(List<File> tables){
		GradientTable gt = new GradientTable();
		for(File f: tables){
			GradientTable temp = new GradientTable();
			temp.readTable(f);
			gt.append(temp);
		}
		return gt.getTable();
	}

	
}
