package edu.jhu.ece.iacl.plugins.dti.tractography;

import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;


public class MedicAlgorithmDeformFibers extends ProcessingAlgorithm {
	//input parameters
	private ParamObject<FiberCollection> fibersin;
	private ParamVolume deffield;

	//output parameters
	private ParamObject<FiberCollection> deffibers;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fibersin = new ParamObject<FiberCollection>("Input Fibers",new FiberCollectionReaderWriter()));
		inputParams.add(deffield = new ParamVolume("Deformation Field",VoxelType.FLOAT,-1,-1,-1,3));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Deform Fibers");
		inputParams.setName("Deform_Fibers");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));
		info.setDescription("Applies an affine transformation to fibers obtained from white matter tractography");
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");	
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(deffibers = new ParamObject<FiberCollection>("Transformed Fibers",new FiberCollectionReaderWriter()));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
//		deffibers.setObject(FiberTransformation.applyDeformation(fibersin.getObject(), deffield.getImageData()));
		deffibers.setFileName(fibersin.getValue().getName()+"_deformed");
	}
}
