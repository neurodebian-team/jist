package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.util.List;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

import edu.jhmi.rad.medic.utilities.CropParameters;
import edu.jhmi.rad.medic.utilities.CubicVolumeCropper;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;


/*
 * Image Cropper
 * @author Navid Shiee
 */
public class MedicAlgorithmMultiCrop extends ProcessingAlgorithm{
	private ParamVolumeCollection inputVol;

	private ParamVolumeCollection outputVol;

	private ParamInteger borderSize;

	private ParamDouble threshold;
	private ParamPointInteger offset, offsetin;
	private ParamPointInteger dim, dimin;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Crop a VolumeCollection.";
	private static final String longDescription = "";


	/**
	 * Create input parameters for Cropper :cubic volume and borderSize
	 */
	public MedicAlgorithmMultiCrop(){
		setName("Multi-Crop");
	}


	protected void createInputParameters(ParamCollection inputParams) {
		inputVol = new ParamVolumeCollection("Volume Collection", null,-1,-1,-1,-1);
		borderSize = new ParamInteger("BorderSize", 0, Integer.MAX_VALUE);
		borderSize.setValue(3);
		threshold = new ParamDouble("Threshold", 0, Integer.MAX_VALUE);
		threshold.setValue(0);
		offsetin  = new ParamPointInteger("Offset");
		offsetin.setValue(new Point3d(-1,-1,-1));
		dimin  = new ParamPointInteger("Dimensions");
		dimin.setValue(new Point3d(-1,-1,-1));
		
		inputParams.add(inputVol);
		inputParams.add(threshold);
		inputParams.add(borderSize);
		inputParams.add(offsetin);
		inputParams.add(dimin);

		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Multi-Crop");
		inputParams.setName("Multi-Crop");
	}


	/**
	 * Create output parameters for Image Cropper: cubic volume and extents
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
/*		for (int i=0; i<6; i++){
			borders[i]= new ParamInteger("border",0,Integer.MAX_VALUE);
			outputParams.add(borders[i]);
		}*/
		offset=new ParamPointInteger("Offset");
		dim=new ParamPointInteger("Dimensions");
		outputParams.add(offset);
		outputParams.add(dim);
		outputVol = new ParamVolumeCollection(null,-1,-1,-1,-1);
		outputVol.setName("cropped");
		outputVol.setLabel("Cropped Image");
		outputParams.add(outputVol);
	}


	/**
	 * Execute Image Cropper
	 */
	protected void execute(CalculationMonitor monitor) {
		CubicVolumeCropper cropper = new CubicVolumeCropper();
		monitor.observe(cropper);
		Point3i offsetparam = offsetin.getValue();
		Point3i dimparam = dimin.getValue();
		if(offsetparam.x!=-1 || offsetparam.y!=-1 || offsetparam.z!=-1 || dimparam.x!=-1 || dimparam.y!=-1 || dimparam.z!=-1){
			CropParameters param = new CropParameters(offsetparam,dimparam);
			List<ImageData> result = cropper.crop(inputVol.getImageDataList(),param);
			if(result.size()>0){
				offset.setValue(offsetparam);
				dim.setValue(dimparam);
			}
			outputVol.setValue(result);
		}else{
			List<ImageData> result = cropper.crop(inputVol.getImageDataList(),threshold.getValue().floatValue(),borderSize.getValue().intValue());
			CropParameters params=cropper.getLastCropParams();
			if(result.size()>0){
				offset.setValue(new Point3i(params.xmin,params.ymin,params.zmin));
				dim.setValue(new Point3i(params.getCroppedRows(),params.getCroppedCols(),params.getCroppedSlices()));
			}
			outputVol.setValue(result);
		}
		
		
	}
}
