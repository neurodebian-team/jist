package edu.jhu.ece.iacl.algorithms.manual_label.staple;

/**
 * 
 * @author John Bogovic, Hanlin Wan
 * @date 5/31/2008
 * 
 * Simultaneous Truth and Performance Level Estimation (STAPLE)
 * Original author: John Bogovic
 * Optimized by: Hanlin Wan
 * 
 * Warfield, Zou, and Wells, "Simultaneous Truth and Performace Level Estimation (STAPLE):
 * An Algorithm for the Validation of Image Segmentation," 
 * IEEE Trans. Medical Imaging vol. 23, no. 7, 2004
 */

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.geom.GridPt.Connectivity;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

public class STAPLEmulti2 extends AbstractCalculation{
	protected int[][][][] imagesArray;
	protected float[][][][] imagesArrayFloat;
	protected int[][][][] t;
	protected Truth[][][] truth;
	
	protected PerformanceLevel pl;
	protected ArrayList<Float> priors;
	protected float[] priorArray;
	protected ArrayList<Integer> labels;
	protected float normtrace;
	protected int maxiters = 1000;
	protected int iters = -1;
	protected String dir;
	protected double eps=.00001;
	protected String initType;
	protected int rows, cols, slices, raters, labelSize, truthSize;

	protected boolean mStepNext;		//true: MStep is next, false: EStep is next
	protected boolean printPerformance = false;
	
	//MRF parameters
	protected float beta =0f, betaAll=0f;
	protected Connectivity connectivity;
	protected int connNbrs;
	protected int maxMRFiters = 25;
	protected boolean keepgoing = true;
	
	public STAPLEmulti2(){
		super();
		setLabel("STAPLE");
	}
	public STAPLEmulti2(int[][][][] img){
		super();
		setLabel("STAPLE");		
		imagesArray=img;
		rows = imagesArray.length;
		cols = imagesArray[0].length;
		slices = imagesArray[0][0].length;
		raters = imagesArray[0][0][0].length;
		getPriorProb();
	}
	public STAPLEmulti2(float[][][][] img){
		super();
		setLabel("STAPLE");		
		imagesArrayFloat=img;
		rows = imagesArrayFloat.length;
		cols = imagesArrayFloat[0].length;
		slices = imagesArrayFloat[0][0].length;
		raters = imagesArrayFloat[0][0][0].length;
		getPriorProb();
	}
	
	public void printParameters(){
		JistLogger.logOutput(JistLogger.FINEST, "maxiters: \t" + maxiters);
		JistLogger.logOutput(JistLogger.FINEST, "beta: \t" + beta);
		JistLogger.logOutput(JistLogger.FINEST, "eps: \t" + eps);
		JistLogger.logOutput(JistLogger.FINEST, "conn: \t" + connectivity);
		JistLogger.logOutput(JistLogger.FINEST, "init Type: \t" + initType);
	}
	
	public void setmaxIters(int max){
		maxiters=max;
	}
	public void setEps(double eps){
		this.eps=eps;
	}	
	public void setImages(int[][][][] img){
		imagesArray=img;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}
	public void setInit(String init){
		initType=init;
	}
	public void setBeta(float beta){
		this.betaAll=beta;
		this.beta=beta;
	}
	public void distributeBeta(){
		if(connectivity==Connectivity.SIX){
			beta=betaAll/6;
		}else if(connectivity==Connectivity.EIGHTEEN){
			beta=betaAll/18;
		}else if(connectivity==Connectivity.TWENTYSIX){
			beta=betaAll/26;
		}else{
			System.out.println("jist.plugins"+"\t"+"Invalid Connectivity!");
		}
	}	
	public void setConnectivity(Connectivity c){
		connectivity = c;
		if(connectivity==Connectivity.SIX){
			connNbrs=6;
		}else if(connectivity==Connectivity.EIGHTEEN){
			connNbrs=18;
		}else if(connectivity==Connectivity.TWENTYSIX){
			connNbrs=26;
		}else{
			connNbrs=-1;
		}
	}
	public GridPt[] getNeighbors(int i, int j, int k){
		if(connectivity==Connectivity.SIX){
			return GridPt.onlyInBounds(GridPt.neighborhood6C(i, j, k), rows, cols, slices);
		}else if(connectivity==Connectivity.EIGHTEEN){
			return GridPt.onlyInBounds(GridPt.neighborhood18C(i, j, k), rows, cols, slices);
		}else if(connectivity==Connectivity.TWENTYSIX){
			return  GridPt.onlyInBounds(GridPt.neighborhood26C(i, j, k), rows, cols, slices);
		}else{
			System.out.println("jist.plugins"+"\t"+"Invalid Connectivity!");
			return null;
		}
		
	}	
	public ArrayList<Integer> getLabels(){
		return labels;
	}	
	public int[] getLabelListArray(){
		int[] out = new int[labels.size()];
		for(int i=0; i<out.length; i++){
			out[i]=labels.get(i);
		}
		return out;
	}
	
	public void setTruth(int i, int j, int k, int[] inds, float[]t){
		truth[i][j][k]=new Truth(inds.length);
		for(int ii=0; ii<inds.length; ii++){
			truth[i][j][k].setIndex(ii, inds[ii]);
		}
		truth[i][j][k].setTruth(t);
	}
	public void setTruthIndex(int i, int j, int k, int loc, int val){
		truth[i][j][k].setIndex(loc,val);
	}

	public void setTruth(int i, int j, int k, float[] p){
		truth[i][j][k].setTruth(p);
	}
	public float[] getTruth(int i, int j, int k){
		return truth[i][j][k].getTruth();
	}
	public int[] getTruthInds(int i, int j, int k){
		return truth[i][j][k].getIndex();
	}
	public int getTruthLength(int i, int j, int k){
		return truth[i][j][k].getIndex().length;
	}
	
	protected void findLabels(){		
		labels = new ArrayList<Integer>();
		if(imagesArray!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<raters; l++){
							if(!labels.contains(imagesArray[i][j][k][l])){
								labels.add(imagesArray[i][j][k][l]);
							}					
						}
					}
				}
			}
		}else if(imagesArrayFloat!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<raters; l++){
							if(!labels.contains((int)imagesArrayFloat[i][j][k][l])){
								labels.add((int)imagesArrayFloat[i][j][k][l]);
							}
						}
					}
				}
			}
		}else{
			System.err.println("jist.plugins"+"No input rater data");
			return;
		}
		labels.trimToSize();
		labelSize=labels.size();
		System.out.println("jist.plugins"+"\t"+"Found Labels: ");
		System.out.println("jist.plugins"+"\t"+labels);
		System.out.println("jist.plugins"+"\t"+"");
	}
	
	private void MStep() {
		float[] totsum = new float[labelSize];
		pl.clear();
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] mindex=truth[i][j][k].getIndex();
					float[] truthArray=truth[i][j][k].getTruth();
					for(int m=0; m<mindex.length; m++){
						float tr=truthArray[m];
						totsum[mindex[m]]+=tr;
						for(int l=0; l<raters; l++){
							pl.set(l, t[i][j][k][l], mindex[m], pl.get(l, t[i][j][k][l], mindex[m])+tr);
						}
					}
				}
			}
		}
		for(int n=0; n<labelSize; n++){
			pl.divideByTots(n, totsum[n]);
		}
		if(printPerformance){
			System.out.println("\n\n"+pl.toString()+"\n\n");
		}
	}
	
	private void EStep() {	
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] mindex=truth[i][j][k].getIndex();
					int lSize=mindex.length;
					float[] a = new float[lSize];
					for(int m=0; m<lSize; m++) {
						a[m]=priorArray[mindex[m]];
					}
					
					for(int m=0; m<lSize; m++){
						for(int l=0; l<raters; l++) {
							a[m]*=pl.get(l, t[i][j][k][l], mindex[m]);
						}
					}
					truth[i][j][k].setTruth(a);					
				}
			}
		}
	}
	
	public void start(){
		if(initType.equals("Performance")){
			EStep();
			mStepNext = true;
		}else{
			mStepNext = false;
		}
		iters = 0;
		normtrace = 0;
	}
	
	/**
	 * A version that iterates using the "smart" methods below
	 */
	public void iterate2(){
		start();
		keepgoing = true;
		while(keepgoing){
			keepgoing = proceed();
		}
	}
	
	/**
	 * Performs the next step of the STAPLE iterations.
	 * @return false if STAPLE has converged
	 */
	public boolean proceed(){
		if(mStepNext){
			if(keepgoing && iters<maxiters){
				
				MStep();
				mStepNext = false;
				
				float ntrace = pl.normalizedTrace();
				if (Float.isNaN(ntrace)) keepgoing=false;
				if(Math.abs(ntrace-normtrace)<eps){
					System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
					System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
					System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
					System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
					keepgoing=false;
				}else{
					System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
					System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
					System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
					System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
					System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
				}
				System.out.println("jist.plugins"+"\t"+"*****************");
				iters++;
				normtrace=ntrace;
				
				return keepgoing;
				
			}else{
				return false;
			}
		}else{
			EStep();
			mStepNext = true;
			return true;
		}		
	}
	
	private void createT(){
		t = new int[rows][cols][slices][raters];
		truth = new Truth[rows][cols][slices];
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					BitSet mindex=new BitSet(labelSize);
					for(int l=0; l<raters; l++){						
						t[i][j][k][l] = getIndex(labels,imagesArray[i][j][k][l]);
						mindex.set(labels.indexOf(imagesArray[i][j][k][l]),true);
						if (!(t[i][j][k][l]>-1))
							System.err.println("jist.plugins"+"Could not find label!");
					}
					truth[i][j][k] = new Truth(mindex.cardinality());
					int c=0;
					for(int m=0; m<labelSize; m++){
						if (mindex.get(m)) {
							truth[i][j][k].setIndex(c++,m);
						}
					}
				}
			}
		}
		imagesArray=null;
		imagesArrayFloat=null;
	}
	
	public void iterate(){		
		
		if(initType.equals("Performance")){
			EStep();
		}
		iters = 0;		
		float ntrace = 0;
		
		while(keepgoing && iters<maxiters){
			MStep();
			EStep();

			ntrace = pl.normalizedTrace();
			if (Float.isNaN(ntrace)) keepgoing=false;
			if(Math.abs(ntrace-normtrace)<eps){
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
			}else{
				System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
			}
			System.out.println("jist.plugins"+"\t"+"*****************");
			iters++;
			normtrace=ntrace;
		}
	}
		
	public void initialize(){
		
		if(initType.equals("Truth")){

			
			System.out.println("jist.plugins"+"\t"+"Not Implemented Yet");
//			findLabels();
//			System.out.println("jist.plugins"+"\t"+"Labels Found: " + labelSize);
//			pl = new PerformanceLevel(labelSize,raters);
//			System.out.println("jist.plugins"+"\t"+"Num Rater Images: " + raters);
//			System.out.println("jist.plugins"+"\t"+"Num Truth Images: " + labelSize);
//			
//			// Initialize the Truth Estimates
//			
//			double d = 1/raters;
//
//			for(int i=0; i<rows; i++){
//				for(int j=0; j<cols; j++){
//					for(int k=0; k<slices; k++){
//						for(int l=0; l<raters; l++){
//							if(imagesArray!=null){
//								int t = getIndex(labels,imagesArray[i][j][k][l]);
//								truthA[i][j][k][t]+=d;
//							}else if(imagesArrayFloat!=null){
//								int t = getIndex(labels,(int)imagesArrayFloat[i][j][k][l]);
//								truthA[i][j][k][t]+=d;
//							}
//						}
//					}
//				}
//			}
		
			
		}else{
			System.out.println("jist.plugins"+"\t"+"Initializing Performance Levels");
			float init = 0.9999f;
			try{
				findLabels();
				System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
				pl = new PerformanceLevel(labels.size(), raters);
				pl.initialize(init);
				createT();
			}catch(Exception e){
				e.printStackTrace();
			}
			if(printPerformance){
				System.out.println("\n\n"+pl.toString()+"\n\n");
			}
		}
		System.out.println("jist.plugins"+"\t"+"*****************");
	}
	
	protected void getPriorProb(){
		labels = new ArrayList<Integer>();
		priors = new ArrayList<Float>();
		float total = raters*rows*cols*slices;
		if(imagesArray!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<imagesArray[0][0][0].length; l++){
							if(!labels.contains(imagesArray[i][j][k][l])){
								labels.add(imagesArray[i][j][k][l]);
								priors.add(new Float(1f));
							}else{
								int thisone = getIndex(labels,imagesArray[i][j][k][l]);
								priors.set(thisone, priors.get(thisone)+1);
							}
						}
					}
				}
			}
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
//				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
			priors.trimToSize();
			setPriorArray();
		}else if(imagesArrayFloat!=null){
			for(int i=0; i<rows; i++){
				for(int j=0; j<cols; j++){
					for(int k=0; k<slices; k++){
						for(int l=0; l<imagesArrayFloat[0][0][0].length; l++){
							if(!labels.contains((int)imagesArrayFloat[i][j][k][l])){
								labels.add((int)imagesArrayFloat[i][j][k][l]);
								priors.add(new Float(1f));
							}else{
								int thisone = getIndex(labels,(int)imagesArrayFloat[i][j][k][l]);
								priors.set(thisone, priors.get(thisone)+1);
							}
						}
					}
				}
			}
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
//				System.out.println("jist.plugins"+"\t"+"Prior Prob of label: " + labels.get(m)+" is: " + priors.get(m));
			}
			priors.trimToSize();
			setPriorArray();
		}else{
			System.err.println("jist.plugins"+"Rater data is null");
		}
	}

	public boolean mStepNext(){
		return mStepNext;
	}
	

	public void printPerformanceLevels(){
		System.out.println("jist.plugins"+"\t"+pl);
	}

	private void setPriorArray(){
		priorArray = new float[priors.size()];
		for(int i=0; i<priors.size(); i++){
			priorArray[i]=priors.get(i).floatValue();
		}
	}
	
	public ImageData writeProb(int n){

		ImageData tfile = new ImageDataMipav("TruthEstimate_"+labels.get(n),rows,cols,slices);
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					float[] a=truth[i][j][k].getTruth();
					int[] ind=truth[i][j][k].getIndex();
					for(int m=0; m<a.length; m++) {
						if(labels.get(n)==ind[m]) {
							tfile.set(i,j,k,a[m]);
							break;
						}
					}
				}
			}
		}
		return tfile;
	}
	public float[][][] writeProbArray(int n){

		float[][][] out = new float[rows][cols][slices];
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					float[] a=truth[i][j][k].getTruth();
					int[] ind=truth[i][j][k].getIndex();
					for(int m=0; m<a.length; m++) {
						if(n==ind[m]) {
							out[i][j][k]=a[m];
							break;
						}
					}
				}
			}
		}
		return out;
	}
	public int[][][] writeTruthIndArray(){

		int[][][] out = new int[rows][cols][slices];
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] ind=truth[i][j][k].getIndex();
					out[i][j][k]=ind.length;
				}
			}
		}
		return out;
	}
	
	public ImageData getHardSeg(){
		ImageDataInt hardseg = new ImageDataInt(rows,cols,slices);
		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				for (int k=0; k<slices; k++) {
					hardseg.set(i,j,k, labels.get(truth[i][j][k].getHardInd()));
				}
			}			
		}
		return new ImageDataInt(hardseg);
	}
	
	public int getNumLabels() {
		return labelSize;
	}

	public PerformanceLevel getPeformanceLevel(){
		return pl;
	}
	
	public int getIndex(ArrayList<Integer> l, Integer n){
		Iterator<Integer> it = l.iterator();
		int i =0;
		while(it.hasNext()){
			if(it.next().intValue()==n.intValue()){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public int getIndex(int[] a, int n){
		for(int i=0; i<a.length; i++){
			if(a[i]==n){
				return i;
			}
		}
		return -1;
	}
	
	private class Truth{
		private int[] index;
		private float[] lTruth;
		
		public Truth(int n){
			index=new int[n];
			lTruth=new float[n];
		}

		public int[] getIndex() {
			return index;
		}
		
		public float[] getTruth() {
			return lTruth;
		}
		
		public int getHardInd() {
			return index[sort()];
		}
		
		public void setIndex(int loc, int n) {
			index[loc]=n;
		}
		
		public void setTruth(float t[]) {
			float sum = 0f;
			for(int l=0; l<t.length; l++){
				sum+=t[l];
			}
			if(sum==0){
				sum=Float.MIN_VALUE;
			}
			for(int l=0; l<t.length; l++){
				lTruth[l]=t[l]/sum;
			}
		}
		
		public int sort() {
			int max = 0;
			for (int j=1; j<lTruth.length; j++){
				if (lTruth[j]>lTruth[max])
					max = j;
			}
			return max;
		}
	}
}