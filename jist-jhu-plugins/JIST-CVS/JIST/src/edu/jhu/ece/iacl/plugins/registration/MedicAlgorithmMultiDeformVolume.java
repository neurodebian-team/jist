/*
 *
 */
package edu.jhu.ece.iacl.plugins.registration;

import java.util.ArrayList;
import java.util.List;

//import edu.jhu.ece.iacl.algorithms.PrinceGroupAuthors;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmFLIRTCollection.FlirtWrapper;


/*
 * @author Min Chen (mchen55@jhu.edu)
 *
 */
public class MedicAlgorithmMultiDeformVolume extends ProcessingAlgorithm{
	//Inputs
	public ParamVolumeCollection inParamVolsIn;
	public ParamVolumeCollection inParamDefFields;
	public ParamBoolean inParamNNFlag;
	
	//Outputs
	public ParamVolumeCollection outParamDeformedVols;

	//Other Variables
	private static final String revnum = RegistrationUtilities.getVersion();
	private static final String shortDescription = "Apply N deformation field to M volume using trilinear or nearest-neighbor interpolation. ";
	private static final String longDescription = "The final pairing between the subjects and deformation fields is determined by mod(i,# of subject) and mod(i,# of def-fields) where i is the iteration  number going from 0-max(# of subject,# of def-fields";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inParamVolsIn=new ParamVolumeCollection("Volumes to Deform"));
		inParamVolsIn.setLoadAndSaveOnValidate(false);
		inputParams.add(inParamDefFields=new ParamVolumeCollection("Deformation Fields",null,-1,-1,-1,3));
		inParamDefFields.setLoadAndSaveOnValidate(false);
		inputParams.add(inParamNNFlag = new ParamBoolean("Use Nearest Neighbor"));
		inParamNNFlag.setValue(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");
		inputParams.setLabel("Deform Multiple Volumes");
		inputParams.setName("Deform_Multiple_Volumes");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("Min Chen", "", ""));
		info.add(new AlgorithmAuthor("M. Bryan Wheeler", "", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outParamDeformedVols=new ParamVolumeCollection("Deformed Volumes"));
		outParamDeformedVols.setLoadAndSaveOnValidate(false);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ExecuteWrapper wrapper=new ExecuteWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}


	protected class ExecuteWrapper extends AbstractCalculation{
		public void execute(ProcessingAlgorithm alg){
			this.setLabel("Deform Volumes");

			List<ParamVolume> volInImage=inParamVolsIn.getParamVolumeList();
			List<ParamVolume> defsInImage=inParamDefFields.getParamVolumeList();

			//		List<ImageDataFloat> dfs= new ArrayList<ImageDataFloat>();
			int i, IN, DN, MaxN;

			//intialize DefFields as Floats
			//		for(i=0;i<deformationFields.size();i++){
			//			dfs.add(new ImageDataFloat(deformationFields.getImageDataList().get(i)));
			//		}
			//get sizes
			IN = volInImage.size();
			DN = defsInImage.size();
			MaxN = Math.max(IN,DN);
			this.setTotalUnits(MaxN);

			for(i=0;i<MaxN;i++){
				JistLogger.logOutput(JistLogger.INFO, "********************"+"Deforming Image:" +i%IN);
				JistLogger.logOutput(JistLogger.INFO, " with Deformation Field: "+i%DN+"********************");
				ImageData vol=null;
				ImageDataFloat defField=null;
				vol =volInImage.get((i%IN)).getImageData();
				if(defField==null) {
					ImageData data =defsInImage.get((i%DN)).getImageData();
					if(data instanceof ImageDataFloat) 
						defField = (ImageDataFloat)data;
					else
						defField = new ImageDataFloat(data);
				}
				JistLogger.logOutput(JistLogger.CONFIG, "Deformation field created: "+vol.getName());
				ImageData result;
				if(inParamNNFlag.getValue()){
					result = ((RegistrationUtilities.DeformImage3D(vol, defField, RegistrationUtilities.InterpolationType.NEAREST_NEIGHTBOR)));
				}
				else{
					result = (RegistrationUtilities.DeformImage3D(vol,defField, RegistrationUtilities.InterpolationType.TRILINEAR));
				}
				JistLogger.logOutput(JistLogger.CONFIG, "Image Deformed: "+vol.getName());
				result.setName(vol.getName()+"_def_"+(i%DN));
				
				if(DN>1) {
					defsInImage.get((i%DN)).getImageData().dispose();
					defField.dispose();defField=null;
				}
				if(IN>1) {
					volInImage.get((i%IN)).getImageData().dispose(); 
					vol.dispose(); vol=null;
				}
				
				outParamDeformedVols.add(result);result=null;
				
				outParamDeformedVols.writeAndFreeNow(alg);
				
				this.setCompletedUnits(i+1);
			}
			JistLogger.logOutput(JistLogger.INFO, "Multi Deform Image: Done!");
		}
	}
}
