package edu.jhu.ece.iacl.algorithms.dti.tractography;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;

public class Tract {
	
	FiberCollection tract;
	public Fiber worst;
	public double maxmax;
	public double meanmax;
	public double meanmean;
	
	public Tract(){
		tract = new FiberCollection();
	}
	public Tract(FiberCollection tract){
		this.tract=tract;
	}
	
	public FiberCollection getFiberCollection(){
		return tract;
	}
	public Point3f findSeed(int i){
		Fiber f = tract.get(i);
		XYZ[] chain = f.getXYZChain();
		for(XYZ pt: chain){
			if(pt.x%1==0.5f &&  pt.y%1==0.5f && pt.z%1==0.5f){
				return new Point3f(pt.x,pt.y,pt.z);
			}
		}
		return new Point3f(-1,-1,-1);
	}
	public Point3f findSeed(Fiber f){
		XYZ[] chain = f.getXYZChain();
		for(XYZ pt: chain){
			if(pt.x%1==0.5f &&  pt.y%1==0.5f && pt.z%1==0.5f){
				return new Point3f(pt.x,pt.y,pt.z);
			}
		}
		return new Point3f(-1,-1,-1);
	}
	
	public int findSeedIndex(int i){
		Fiber f = tract.get(i);
		XYZ[] chain = f.getXYZChain();
		int j=0;
		for(XYZ pt: chain){
			if(pt.x%1==0.5f &&  pt.y%1==0.5f && pt.z%1==0.5f){
				return j;
			}
			j++;
		}
		return -1;
	}
	
	public int findCorrespondingFiberBySeed(int i, Tract atract){
		Point3f seed = findSeed(i);
		for(int j=0; j<atract.tract.size(); j++){
			Point3f s2 = atract.findSeed(j);
			if(seed.x==s2.x && seed.y==s2.y && seed.z==s2.z){
				return j;
			}
		}
		return -1;
	}
	public int findCorrespondingFiberBySeed(Fiber f, Tract atract){
		Point3f seed = findSeed(f);
		for(int j=0; j<atract.tract.size(); j++){
			Point3f s2 = atract.findSeed(j);
			if(seed.x==s2.x && seed.y==s2.y && seed.z==s2.z){
				return j;
			}
		}
		return -1;
	}
	
	
	public double maxTractDistanceBySeed(Tract atract,double resX, double resY, double resZ){
		double max = 0;
		int missed = 0;
		boolean print = true;
		for(Fiber thisf: tract){
			int matchfiberindex = findCorrespondingFiberBySeed(thisf, atract);
			if(matchfiberindex>0){
				double d = FiberComparator.maxDist(thisf, atract.tract.get(matchfiberindex),resX, resY, resZ);
				if(d>max){
					worst = thisf;
					atract.worst=atract.tract.get(matchfiberindex);
					max=d;
				}
			}else{
				missed++;
			}
		}
		System.out.println("jist.plugins"+"\t"+"Missed: " + missed);
		return max;
	}
	
	public void tractDistancesBySeed(Tract atract,double resX, double resY, double resZ){
		double meanmax = 0;
		double max = -1;
		double meanmean=0;
		int num = 0;
		int missed = 0;
		for(Fiber thisf: tract){
			int matchfiberindex = findCorrespondingFiberBySeed(thisf, atract);
			if(matchfiberindex>0){
				double d = FiberComparator.maxDist(thisf, atract.tract.get(matchfiberindex),resX, resY, resZ);
				meanmax=meanmax+d;
				meanmean = meanmean+FiberComparator.meanDistCorr(thisf, atract.tract.get(matchfiberindex),resX, resY, resZ);
				num++;
				if(d>max){
					worst = thisf;
					atract.worst=atract.tract.get(matchfiberindex);
					max=d;
				}
			}else{
				missed++;
			}
		}
		System.out.println("jist.plugins"+"\t"+"Missed: " + missed);
		this.maxmax=max;
		this.meanmax=meanmax/num;
		this.meanmean=meanmean/num;
	}
	
	public double maxTractDistanceBySeed(Fiber thisf, Tract atract,double resX, double resY, double resZ){
		double max = 0;
		int missed = 0;
		int matchfiberindex = this.findCorrespondingFiberBySeed(thisf, atract);
		if(matchfiberindex>0){
			double d = FiberComparator.maxDist(thisf, atract.tract.get(matchfiberindex),resX, resY, resZ);
			if(d>max){
				max=d;
			}
		}else{
			missed++;
		}
		System.out.println("jist.plugins"+"\t"+"Missed: " + missed);
		return max;
	}
	
	public void print(){
		for(Fiber f : tract){
			System.out.println("jist.plugins"+"\t"+f);
			System.out.println("jist.plugins"+"\t"+" ");
		}
	}
	
}
