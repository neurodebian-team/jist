package edu.jhu.ece.iacl.plugins.utilities.surface;

import edu.jhu.ece.iacl.jist.io.ArrayDoubleReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class MedicAlgorithmEmbedSurface extends ProcessingAlgorithm{
	ParamSurface surf;
	ParamSurface outSurf;
	ParamOption replaceorappend;
	ParamObject<double[][]> data;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Embed data in surface mesh.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surf=new ParamSurface("Surface"));
		inputParams.add(data=new ParamObject<double[][]>("Data"));
		inputParams.add(replaceorappend=new ParamOption("Method",new String[]{"Append","Replace"}));
		data.setReaderWriter(new ArrayDoubleReaderWriter());


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Embed Data in Surface");
		inputParams.setLabel("Embed_Data_in_Surface");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Embedded Surface"));
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		if(replaceorappend.getIndex()==0){
			EmbeddedSurface surfDup=new EmbeddedSurface(surf.getSurface(),(double[][])data.getObject());
			outSurf.setValue(surfDup);
		} else {
			EmbeddedSurface surfDup=surf.getSurface();
			surfDup.setVertexData((double[][])data.getObject());
			outSurf.setValue(surfDup);
		}
	}
}
