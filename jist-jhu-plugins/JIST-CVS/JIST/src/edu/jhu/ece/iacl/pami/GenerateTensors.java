package edu.jhu.ece.iacl.pami;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import Jama.Matrix;
import edu.jhu.bme.smile.commons.math.MatrixMath;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;


public class GenerateTensors extends ProcessingAlgorithm{
	//Input Variables
	private ParamMatrix evalues;
	private ParamObject<double[][]> directions;
	private ParamBoolean randDir;
	private ParamInteger param_angle;
	private ParamInteger param_dimX;
	private ParamInteger param_dimY;
	private ParamInteger param_dimZ;
	//Output Variables
	protected ParamObject<double[][]> outD1;
	protected ParamObject<double[][]> outD2;
	protected ParamObject<double[][]> outD3;

	private Random rand = new Random();
	private Matrix diagD;
	private Matrix dir;
	private boolean uniformRandom;

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Generates 3 random tensors with the specified angle between them. If angle = 0, then tensors are completely random.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams){
		//Plugin Information
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Simulation");
		inputParams.setLabel("Generate Tensors");
		inputParams.setName("Generate_Tensors");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		//Inputs
		inputParams.add(evalues = new ParamMatrix("Eigenvalues", new Matrix(new double[][]{{2e-3, 5e-4, 5e-4}})));
		inputParams.add(directions = new ParamObject<double[][]>("Directions", new ArrayDoubleTxtReaderWriter()));
		directions.setMandatory(false);
		inputParams.add(randDir = new ParamBoolean("Uniformly Random",false));
		inputParams.add(param_angle = new ParamInteger("Angle",0));
		inputParams.add(param_dimX = new ParamInteger("dimX",10));
		inputParams.add(param_dimY = new ParamInteger("dimY",10));
		inputParams.add(param_dimZ = new ParamInteger("dimZ",10));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outD1 = new ParamObject<double[][]>("D1"));
		outputParams.add(outD2 = new ParamObject<double[][]>("D2"));
		outputParams.add(outD3 = new ParamObject<double[][]>("D3"));
	}


	protected void execute(CalculationMonitor monitor) {
		int dimX = param_dimX.getInt();
		int dimY = param_dimY.getInt();
		int dimZ = param_dimZ.getInt();
		int angle = param_angle.getInt();
		int count=0;
		double[][] d1 = new double[dimX*dimY*dimZ][6];
		double[][] d2 = new double[dimX*dimY*dimZ][6];
		double[][] d3 = new double[dimX*dimY*dimZ][6];
		double[] v1, v2, v3;
		uniformRandom = randDir.getValue();
		diagD = new Matrix(3,3);
		for (int i=0; i<3; i++) diagD.set(i,i,evalues.getValue().get(0, i));
		double[][] d = (double[][])directions.getObject();
		System.out.println("SPEC'D DIRECTIONS: " + directions.getValue());
		if (d != null){
			System.out.println("\nUSING SPECIFIED DIRECTIONS\n");
			dir = new Matrix(d);
			for (int i=0; i < dimX; i++) {
				for (int j=0; j < dimY; j++) {
					for (int k=0; k < dimZ; k++) {
							// Get tensor 1
							v1 = null;
							v2 = null;
							v3 = null;
							if(dir.getRowDimension()>0){
								v1 = directionFromMatrix(1);
								System.arraycopy(getD(v1), 0, d1[count], 0, 6);
								
							}
							// Get tensor 2
							if(dir.getRowDimension()>1){
								v2 = directionFromMatrix(2);
								System.arraycopy(getD(v2), 0, d2[count], 0, 6);
								
							}else{
								v2 = getT2(v1,angle);
								System.arraycopy(getD(v2), 0, d2[count], 0, 6);
								
							}
							// Get tensor 3
							if(dir.getRowDimension()>2){
								v3 = directionFromMatrix(3);
								System.arraycopy(getD(v3), 0, d3[count], 0, 6);
								count++;
							}else{
								v3 = getT3(v1,v2,angle);
								System.arraycopy(getD(v3), 0, d3[count], 0, 6);
								count++;
							}
							
					}
				}
			}
		}else{
			dir = new Matrix(1,1);
			for (int i=0; i < dimX; i++) {
				for (int j=0; j < dimY; j++) {
					for (int k=0; k < dimZ; k++) {
						// Get tensor 1
						v1 = randTensor();
						System.arraycopy(getD(v1), 0, d1[count], 0, 6);
						// Get tensor 2
						v2 = getT2(v1,angle);
						System.arraycopy(getD(v2), 0, d2[count], 0, 6);
						// Get tensor 3
						v3 = getT3(v1,v2,angle);
						System.arraycopy(getD(v3), 0, d3[count], 0, 6);
						count++;
					}
				}
			}
		}


		// Output file
		String f1 = getOutputDirectory().toString() + File.separatorChar +  "d1.txt";
		String f2 = getOutputDirectory().toString() + File.separatorChar +  "d2.txt";
		String f3 = getOutputDirectory().toString() + File.separatorChar +  "d3.txt";
		try {
			// Tensor 1
			File d1File = new File(f1);
			FileWriter rw1;
			rw1 = new FileWriter(d1File,false);
			rw1.write(dimX+" "+dimY+" "+dimZ+" 0 0 0 \n");
			for (int i=0; i<d1.length; i++) {
				for (int j=0; j<d1[0].length; j++) {
					rw1.write(d1[i][j]+" ");
				}
				rw1.write("\n");
			}
			rw1.close();
			outD1.setValue(d1File);

			// Tensor 2
			File d2File = new File(f2);
			FileWriter rw2;
			rw2 = new FileWriter(d2File,false);
			rw2.write(dimX+" "+dimY+" "+dimZ+" 0 0 0 \n");
			for (int i=0; i<d2.length; i++) {
				for (int j=0; j<d2[0].length; j++) {
					rw2.write(d2[i][j]+" ");
				}
				rw2.write("\n");
			}
			rw2.close();
			outD2.setValue(d2File);


			File d3File = new File(f3);
			FileWriter rw3;
			rw3 = new FileWriter(d3File,false);
			rw3.write(dimX+" "+dimY+" "+dimZ+" 0 0 0 \n");
			for (int i=0; i<d3.length; i++) {
				for (int j=0; j<d3[0].length; j++) {
					rw3.write(d3[i][j]+" ");
				}
				rw3.write("\n");
			}
			rw3.close();
			outD3.setValue(d3File);
		}
		catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("I/O Error.");
		}
	}


	public double[] randTensor() {
		double[] v = new double[3];
		if (uniformRandom) {		// Random from unit sphere
			v[0] = rand.nextDouble();
			v[1] = rand.nextDouble();
			v[2] = rand.nextDouble();
			double norm = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
			v[0] /= norm; v[1] /= norm; v[2] /= norm;
		}
		else {						// Random from specifed directions
			int num = rand.nextInt(dir.getRowDimension());
			v[0] = dir.get(num, 0);
			v[1] = dir.get(num, 1);
			v[2] = dir.get(num, 2);
		}
		return v;
	}
	
	public double[] directionFromMatrix(int i){
		if(i>=dir.getRowDimension()){
			return new double[]{0,0,0};
		}
		double[] d = new double[dir.getColumnDimension()];
		for(int j=0; j<d.length; j++){
			d[j]=dir.get(i, j);
		}
		return d;
	}


	public double[] getT2(double[] v, int angle) {
		Matrix v1 = new Matrix(v,3);
		Matrix v2 = new Matrix(3,1);
		double[] d = new double[3];
		double[] ang = new double[dir.getRowDimension()];
		int angLow = angle - 1;
		int angHi = angle + 1;
		ArrayList<Integer> posAng = new ArrayList<Integer>();

		if (angle==0) {		// Random tensor
			d = randTensor();
		}
		else {				// Rotate tensor
			for (int i=0; i<dir.getRowDimension(); i++) {
				v2.set(0,0,dir.get(i,0));
				v2.set(1,0,dir.get(i,1));
				v2.set(2,0,dir.get(i,2));
				ang[i] = Math.min(Math.acos(MatrixMath.dotProduct(v1,v2)),Math.acos(MatrixMath.dotProduct(v1,v2.times(-1))))*180/Math.PI;
				if (ang[i] < angHi && ang[i] > angLow)
					posAng.add(i);
			}
			int r = rand.nextInt(posAng.size());
			int index = posAng.get(r);
			d[0] = dir.get(index,0);
			d[1] = dir.get(index,1);
			d[2] = dir.get(index,2);
		}

		return d;
	}


	public double[] getT3(double[] v_1, double[] v_2, int angle) {
		return randTensor();
	}


	public double[] getD(double[] v) {
		double v1=v[0];
		double v2=v[1];
		double v3=v[2];

		// Convert to spherical coordinates
		double hypotxy = Math.hypot(v1,v2);
		double th = Math.atan2(v2,v1);
		double phi = Math.atan2(v3,hypotxy);

		// Calculate D with given eigenvalues
		Matrix Rz = new Matrix(3,3);
		Rz.set(0,0,Math.cos(th));
		Rz.set(0,1,-Math.sin(th));
		Rz.set(1,0,Math.sin(th));
		Rz.set(1,1,Math.cos(th));
		Rz.set(2,2,1);

		Matrix Ry = new Matrix(3,3);
		Ry.set(0,0,Math.cos(phi));
		Ry.set(0,2,-Math.sin(phi));
		Ry.set(1,1,1);
		Ry.set(2,0,Math.sin(phi));
		Ry.set(2,2,Math.cos(phi));

		Matrix R = Rz.times(Ry);
		Matrix D = R.times(diagD).times(R.transpose());
		Matrix D2 = new Matrix(6,1);
		D2.set(0, 0, D.get(0,0));
		D2.set(1, 0, D.get(0,1));
		D2.set(2, 0, D.get(0,2));
		D2.set(3, 0, D.get(1,1));
		D2.set(4, 0, D.get(1,2));
		D2.set(5, 0, D.get(2,2));
		return D2.getColumnPackedCopy();
	}
}
