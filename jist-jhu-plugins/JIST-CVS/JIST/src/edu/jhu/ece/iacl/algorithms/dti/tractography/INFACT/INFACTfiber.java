package edu.jhu.ece.iacl.algorithms.dti.tractography.INFACT;

import edu.jhu.ece.iacl.algorithms.dti.tractography.FACT.cPT;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.geom.CurvePath;

import java.util.Map;
import java.util.LinkedList;
import java.util.Iterator;

import javax.vecmath.*;

/**
 * 
 * @author John Bogovic
 * @author Bennett Landman
 *
 */
public class INFACTfiber extends Fiber{
	 static final boolean FORWARD = true;
	    static final boolean BACKWARD = false;
	    static final double pi2 = Math.PI/2;
	    static float stepSize = 0.1f;
	    static float DTIStudioStepSize = 0.1f;
	    public int numPoints;
	    public Point3f points[];
	    public LinkedList<Point3f> fiberPoints;



	    private class RAY{
	    	Point3f pt;
	        float dist;
	        public RAY(float x, float y, float z,float d) {
	            pt = new Point3f(x,y,z);
	            dist = d;
	        }
	    }
	    
	    //@Overide
	    public XYZ[] getXYZChain(){ 
	    	if(super.getXYZChain()==null){
	    		XYZ[] chain = new XYZ[points.length];
	    		int i=0;
	    		for(Point3f pt: points){
	    			chain[i]=new XYZ(pt.x,pt.y,pt.z);
	    			i++;
	    		}
	    		return chain;
	    	}else{
	    		return super.getXYZChain();
	    	}
	    }

//	    public int track(float  startX, float  startY, float  startZ) {
	    public int track(INFACTparameters dat, float  startX, float  startY, float  startZ) {
	    	fiberPoints = new LinkedList<Point3f>();
	        fiberPoints.add(new Point3f(startX,startY,startZ)); numPoints=1;
	        
	        //System.out.println();System.out.println("READY TO START TRACKING...."); System.out.println();
	        
	        int szx = floorinbound(startX,dat.Nx);
	        int szy = floorinbound(startY,dat.Ny);
	        int szz = floorinbound(startZ,dat.Nz);
	        
	        track(FORWARD,dat,startX,startY,startZ,dat.basisDirs[dat.basisInds[szx][szy][szz][0]].x,
	        		dat.basisDirs[dat.basisInds[szx][szy][szz][0]].y,dat.basisDirs[dat.basisInds[szx][szy][szz][0]].z,
	                startX,startY,startZ);
	        track(BACKWARD,dat,startX,startY,startZ,-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].x,
	        		-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].y,-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].z,
	                startX,startY,startZ);
	        
//	        track(FORWARD,dat,startX,startY,startZ,dat.mt.wvecs[0][szx][szy][szz][t],
//	        		dat.mt.wvecs[1][szx][szy][szz][t],dat.mt.wvecs[2][szx][szy][szz][t],
//	                startX,startY,startZ);
//	        track(BACKWARD,dat,startX,startY,startZ,-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].x,
//	        		-dat.mt.wvecs[1][szx][szy][szz][t],-dat.mt.wvecs[2][szx][szy][szz][t],
//	                startX,startY,startZ);
	        
	        points = new Point3f[numPoints];
	        Iterator i = fiberPoints.iterator();
	        int j=0;
	        while(i.hasNext()) {
	            points[j] = (Point3f)i.next(); j++;
	        }
	        i=null;
	        fiberPoints=null;
	        return numPoints;
	    }
	    
	    public int trackRK(INFACTparameters dat, float  startX, float  startY, float  startZ) {
//		    public int track(MTFACTparameters dat, float  startX, float  startY, float  startZ, int t) {
		    	fiberPoints = new LinkedList<Point3f>();
		        fiberPoints.add(new Point3f(startX,startY,startZ)); numPoints=1;
		        
		        //System.out.println();System.out.println("READY TO START TRACKING...."); System.out.println();
		        
		        int szx = roundinbound(startX,dat.Nx);
		        int szy = roundinbound(startY,dat.Ny);
		        int szz = roundinbound(startZ,dat.Nz);
		        
		        trackRungeKutta(FORWARD,dat,startX,startY,startZ,dat.basisDirs[dat.basisInds[szx][szy][szz][0]].x,
		        		dat.basisDirs[dat.basisInds[szx][szy][szz][0]].y,dat.basisDirs[dat.basisInds[szx][szy][szz][0]].z,
		                startX,startY,startZ);
		        trackRungeKutta(BACKWARD,dat,startX,startY,startZ,-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].x,
		        		-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].y,-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].z,
		                startX,startY,startZ);
		        
//		        track(FORWARD,dat,startX,startY,startZ,dat.mt.wvecs[0][szx][szy][szz][t],
//		        		dat.mt.wvecs[1][szx][szy][szz][t],dat.mt.wvecs[2][szx][szy][szz][t],
//		                startX,startY,startZ);
//		        track(BACKWARD,dat,startX,startY,startZ,-dat.basisDirs[dat.basisInds[szx][szy][szz][0]].x,
//		        		-dat.mt.wvecs[1][szx][szy][szz][t],-dat.mt.wvecs[2][szx][szy][szz][t],
//		                startX,startY,startZ);
		        
		        points = new Point3f[numPoints];
		        Iterator i = fiberPoints.iterator();
		        int j=0;
		        while(i.hasNext()) {
		            points[j] = (Point3f)i.next(); j++;
		        }
		        i=null;
		        fiberPoints=null;
		        return numPoints;
		    }

	    private void track(boolean addToEnd,INFACTparameters dat, float  x, float  y, float  z, float last_vx, float last_vy, float last_vz,
	    		float x_c,float y_c,float z_c) {
	    	float vx,vy,vz;

	    	Point3i last = null;
	    	Point3i last2 = null;
	    	Point3i last3 = null;

	    	int xr = floorinbound(x,dat.Nx-1);
	    	int yr = floorinbound(y,dat.Ny-1);
	    	int zr = floorinbound(z,dat.Nz-1);

	    	int pts =0;
//	    	boolean firstpt = true;
	    	while(pts<dat.maxLength) { //loop until hitting a stop condition - prevent super long fibers  	

	    		pts++;

//	    		if (pts>499){System.out.println("Fiber has crossed " + pts + " points");}
	    		
//	            float[] v = dat.mt.direction_ODF_magExpDot(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle,5f);
//	            float[] v = dat.mt.direction_ODF_magExpAng(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle);    
//	            float[] v = dat.mt.direction_ODF_test(xr, yr, zr, last_vx, last_vy, last_vz);
	            Point3f v = dat.direction_ODF_magCosAng(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle);
//	            float[] v = dat.mt.direction_ODF_Pilou(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle);
	            
	            vx = v.x;
	            vy = v.y;
	            vz = v.z;
	    		
	    		double angle = vectAng(vx,vy,vz,last_vx,last_vy,last_vz);

	    		if(angle>pi2) {
	    			angle = Math.PI-angle;
	    			vx=-vx;vy=-vy;vz=-vz;
	    		}
	    		if(dat.faData[xr][yr][zr]<dat.stopFA) {
//	    			System.out.println(fiberPoints);
//	    			System.out.println("***** End of Fiber - Low FA *****");
//	    			System.out.println("*********************************");
	    			return; //too low fa
	    		}
	    		if(angle>dat.maxTurnAngle) {
//	    			System.out.println(fiberPoints);
//	    			System.out.println("***** End of Fiber - Turn Ang *****");
//	    			System.out.println("*********************************");
	    			return; //too high turn angle
	    		}
	    		if(angle<0){
	    			return; //No direction
	    		}


	    		/* Original method why stepping through the data*/
	    		/**********************************************
      // Scale v, vectors
      vx = (float)(vx/dat.resX)*stepSize;
      vy = (float)(vy/dat.resY)*stepSize;
      vz = (float)(vz/dat.resZ)*stepSize;
      while((x==Math.round(x_c))&&(y==Math.round(y_c))&&(z==Math.round(z_c))){
      x_c+=vx;
      y_c+=vy;
      z_c+=vz;
      }
      x = (char )Math.round(x_c);
      y = (char )Math.round(y_c);
      z = (char )Math.round(z_c);
	    		 **********************************************/
	    		RAY DX, DY, DZ; // Distance to nearest X Y and Z planes

	    		if((float)(vx/dat.resX)>0){
	    			DX = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),1,0,0,(float)Math.ceil(x_c+0.0001f));
	    		}else{
	    			DX = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),1,0,0,(float)Math.floor(x_c-0.0001f));
	    		}

	    		if((float)(vy/dat.resY)>0){
	    			DY = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,1,0,(float)Math.ceil(y_c+0.0001f));
	    		}else{
	    			DY = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,1,0,(float)Math.floor(y_c-0.0001f));
	    		}

	    		if((float)(vz/dat.resZ)>0){
	    			DZ = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,0,1,(float)Math.ceil(z_c+0.0001f));
	    		}else{
	    			DZ = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,0,1,(float)Math.floor(z_c-0.0001f));
	    		}

	    		if((DX.dist<DY.dist)&&(DX.dist<DZ.dist)) {
	    			//X is minimum
	    			x_c=DX.pt.x;
	    			y_c=DX.pt.y;
	    			z_c=DX.pt.z;
	    			if(vx>0){
	    				xr++;
	    			}else{
	    				xr--;
	    			}
	                if(yr!=(int)Math.floor(y_c)){
//	                	System.out.println("changed x, fixing y");
	                	yr=(int)Math.floor(y_c);
	                }
	                if(zr!=Math.floor(z_c)){
//	                	System.out.println("changed x, fixing z");
	                	zr=(int)Math.floor(z_c);
	                }
	    		}  else {
	    			if((DY.dist<DZ.dist)) {
	    				// Y is minimum
	    				x_c=DY.pt.x;
	    				y_c=DY.pt.y;
	    				z_c=DY.pt.z;
	    				if(vy>0){
	    					yr++;
	    				}else{
	    					yr--;
	    				}
	    				if(xr!=(int)Math.floor(x_c)){
//	                    	System.out.println("changed y, fixing x");
	                    	yr=(int)Math.floor(y_c);
	                    }
	                    if(zr!=Math.floor(z_c)){
//	                    	System.out.println("changed y, fixing z");
	                    	zr=(int)Math.floor(z_c);
	                    }
	    			} else {
	    				//Z is minimum
	    				x_c=DZ.pt.x;
	    				y_c=DZ.pt.y;
	    				z_c=DZ.pt.z;

	    				if(vz>0){
	    					zr++;
	    				}else{
	    					zr--;
	    				}
	    				  if(yr!=(int)Math.floor(y_c)){
//	                      	System.out.println("changed z, fixing y");
	                      	yr=(int)Math.floor(y_c);
	                      }
	                      if(xr!=Math.floor(x_c)){
//	                      	System.out.println("changed z, fixing x");
	                      	xr=(int)Math.floor(x_c);
	                      }
	    			}
	    		}

	    		Point3i thiscoord = new Point3i(xr,yr,zr);
	    		Point3f thispt = new Point3f(x_c,y_c,z_c);

	    		if(thiscoord.equals(last)){
	    			return;
	    		}
	    		if(thiscoord.equals(last2)){
	    			return;
	    		}
	    		if(thiscoord.equals(last3)){
	    			return;
	    		}

	    		if((xr>=0)&&(yr>=0)&&(zr>=0)&&(xr<dat.Nx)&&(yr<dat.Ny)&&(zr<dat.Nz)) {
	    			numPoints++;
	    			if(addToEnd){ fiberPoints.addFirst(thispt); }
	    			else{ fiberPoints.addLast(thispt); }
	    		} else {
	    			return; //out of bounds
	    		}
	    		
	    		if((x_c<0)||(y_c<0)||(z_c<0)||(x_c>=dat.Nx)||(y_c>=dat.Ny)||(z_c>=dat.Nz)) {
	            	return; //out of bounds
	            }

	    		last_vx = vx;
	    		last_vy = vy;
	    		last_vz = vz;

	    		last3 = last2;
	    		last2 = last;
	    		last = thiscoord;

	    	}
	    }
	    
	    
	    private void trackRungeKutta(boolean addToEnd,INFACTparameters dat, float  x, float  y, float  z, float last_vx, float last_vy, float last_vz,
	    		float x_c,float y_c,float z_c) {
	    	float vx,vy,vz;

	    	Vector3f vec1 = new Vector3f();
	    	Vector3f vec2 = new Vector3f();
	    	Vector3f vec3 = new Vector3f();
	    	
	    	Point3i last = null;
	    	Point3i last2 = null;
	    	Point3i last3 = null;

	    	int xr = floorinbound(x,dat.Nx-1);
	    	int yr = floorinbound(y,dat.Ny-1);
	    	int zr = floorinbound(z,dat.Nz-1);

	    	int pts =0;
//	    	boolean firstpt = true;
	    	while(pts<dat.maxLength) { //loop until hitting a stop condition - prevent super long fibers  	

	    		pts++;

//	    		if (pts>499){System.out.println("Fiber has crossed " + pts + " points");}
	    		
//	            float[] v = dat.mt.direction_ODF_magExpDot(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle,5f);
//	            float[] v = dat.mt.direction_ODF_magExpAng(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle);    
//	            float[] v = dat.mt.direction_ODF_test(xr, yr, zr, last_vx, last_vy, last_vz);
	            Point3f v = dat.direction_ODF_magCosAng(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle);
//	            float[] v = dat.mt.direction_ODF_Pilou(xr, yr, zr, last_vx, last_vy, last_vz,dat.maxTurnAngle);
	            
	    		vec1=vec2;
	    		vec2=vec3;
	    		
	    		vec3.x=v.x;
	    		vec3.y=v.y;
	    		vec3.z=v.z;

	    		
	            double angle = vectAng(vec3.x,vec3.y,vec3.z,vec2.x,vec2.y,vec2.z);

	    		if(angle>pi2) {
	    			angle = Math.PI-angle;
	    			vec3.x=-vec3.x;vec3.y=-vec3.y;vec3.z=-vec3.z;
	    		}
	    		if(dat.faData[zr][yr][xr]<dat.stopFA) {
	    			return; //too low fa
	    		}
	    		if(angle>dat.maxTurnAngle) {
	    			return; //too high turn angle
	    		}
	    		if(angle<0){
	    			return; //No direction
	    		}

	    		
	    		Vector3f vec = new Vector3f(0.166666f*vec3.x+0.666666f*vec2.x+0.166666f*vec1.x,
						0.166666f*vec3.y+0.666666f*vec2.y+0.166666f*vec1.y,
						0.166666f*vec3.z+0.666666f*vec2.z+0.166666f*vec1.z);
	    		vec.normalize();


	    		/* Original method why stepping through the data*/
	    		/**********************************************
      // Scale v, vectors
      vx = (float)(vx/dat.resX)*stepSize;
      vy = (float)(vy/dat.resY)*stepSize;
      vz = (float)(vz/dat.resZ)*stepSize;
      while((x==Math.round(x_c))&&(y==Math.round(y_c))&&(z==Math.round(z_c))){
      x_c+=vx;
      y_c+=vy;
      z_c+=vz;
      }
      x = (char )Math.round(x_c);
      y = (char )Math.round(y_c);
      z = (char )Math.round(z_c);
	    		 **********************************************/
	    		RAY DX, DY, DZ; // Distance to nearest X Y and Z planes

	    		if((float)(vec.x/dat.resX)>0){
	    			DX = distInt(x_c,y_c,z_c,(float)(vec.x/dat.resX),(float)(vec.y/dat.resY),(float)(vec.z/dat.resZ),1,0,0,(float)Math.ceil(x_c+0.0001f));
	    		}else{
	    			DX = distInt(x_c,y_c,z_c,(float)(vec.x/dat.resX),(float)(vec.y/dat.resY),(float)(vec.z/dat.resZ),1,0,0,(float)Math.floor(x_c-0.0001f));
	    		}

	    		if((float)(vec.y/dat.resY)>0){
	    			DY = distInt(x_c,y_c,z_c,(float)(vec.x/dat.resX),(float)(vec.y/dat.resY),(float)(vec.z/dat.resZ),0,1,0,(float)Math.ceil(y_c+0.0001f));
	    		}else{
	    			DY = distInt(x_c,y_c,z_c,(float)(vec.x/dat.resX),(float)(vec.y/dat.resY),(float)(vec.z/dat.resZ),0,1,0,(float)Math.floor(y_c-0.0001f));
	    		}

	    		if((float)(vec.z/dat.resZ)>0){
	    			DZ = distInt(x_c,y_c,z_c,(float)(vec.x/dat.resX),(float)(vec.y/dat.resY),(float)(vec.z/dat.resZ),0,0,1,(float)Math.ceil(z_c+0.0001f));
	    		}else{
	    			DZ = distInt(x_c,y_c,z_c,(float)(vec.x/dat.resX),(float)(vec.y/dat.resY),(float)(vec.z/dat.resZ),0,0,1,(float)Math.floor(z_c-0.0001f));
	    		}

	    		if((DX.dist<DY.dist)&&(DX.dist<DZ.dist)) {
	    			//X is minimum
	    			x_c=DX.pt.x;
	    			y_c=DX.pt.y;
	    			z_c=DX.pt.z;
	    			if(vec.x>0){
	    				xr++;
	    			}else{
	    				xr--;
	    			}
	    			 if(yr!=(int)Math.floor(y_c)){
//		                	System.out.println("changed x, fixing y");
		                	yr=(int)Math.floor(y_c);
		                }
		                if(zr!=Math.floor(z_c)){
//		                	System.out.println("changed x, fixing z");
		                	zr=(int)Math.floor(z_c);
		                }
	    		}  else {
	    			if((DY.dist<DZ.dist)) {
	    				// Y is minimum
	    				x_c=DY.pt.x;
	    				y_c=DY.pt.y;
	    				z_c=DY.pt.z;
	    				if(vec.y>0){
	    					yr++;
	    				}else{
	    					yr--;
	    				}
	    				if(xr!=(int)Math.floor(x_c)){
//	                    	System.out.println("changed y, fixing x");
	                    	yr=(int)Math.floor(y_c);
	                    }
	                    if(zr!=Math.floor(z_c)){
//	                    	System.out.println("changed y, fixing z");
	                    	zr=(int)Math.floor(z_c);
	                    }
	    			} else {
	    				//Z is minimum
	    				x_c=DZ.pt.x;
	    				y_c=DZ.pt.y;
	    				z_c=DZ.pt.z;

	    				if(vec.z>0){
	    					zr++;
	    				}else{
	    					zr--;
	    				}
	    				if(yr!=(int)Math.floor(y_c)){
//	                      	System.out.println("changed z, fixing y");
	                      	yr=(int)Math.floor(y_c);
	                      }
	                      if(xr!=Math.floor(x_c)){
//	                      	System.out.println("changed z, fixing x");
	                      	xr=(int)Math.floor(x_c);
	                      }
	    			}
	    		}

	    		Point3i thiscoord = new Point3i(xr,yr,zr);
	    		Point3f thispt = new Point3f(x_c,y_c,z_c);

	    		if(thiscoord.equals(last)){
	    			return;
	    		}
	    		if(thiscoord.equals(last2)){
	    			return;
	    		}
	    		if(thiscoord.equals(last3)){
	    			return;
	    		}

	    		if((xr>=0)&&(yr>=0)&&(zr>=0)&&(xr<dat.Nx)&&(yr<dat.Ny)&&(zr<dat.Nz)) {
	    			numPoints++;
	    			if(addToEnd){ fiberPoints.addFirst(thispt); }
	    			else{ fiberPoints.addLast(thispt); }
	    		} else {
	    			return; //out of bounds
	    		}
	    		
	    		if((x_c<0)||(y_c<0)||(z_c<0)||(x_c>=dat.Nx)||(y_c>=dat.Ny)||(z_c>=dat.Nz)) {
	            	return; //out of bounds
	            }

	    		last_vx = vec.x;
	    		last_vy = vec.y;
	    		last_vz = vec.z;

	    		last3 = last2;
	    		last2 = last;
	    		last = thiscoord;

	    	}
	    }
	    
	    private RAY distInt(float x, float y, float z, float vx, float vy, float vz, float nx, float ny, float nz, float v) {
	        float nDotv =vx*nx+vy*ny+vz*nz;
	        if(nDotv==0) {
	            //Parallel
	            return new RAY(x,y,z,Float.MAX_VALUE);
	        }
	        float s =(nx*(v*nx-x)+ny*(v*ny-y)+nz*(v*nz-z))/nDotv;
	        return new RAY((x+vx*s),(y+vy*s),(z+vz*s),s);
	    }

	    private double vectAng(double vx1, double vy1, double vz1, double vx2, double vy2, double vz2) {
	        double dv1 = Math.sqrt(vx1*vx1+vy1*vy1+vz1*vz1);
	        double dv2 = Math.sqrt(vx2*vx2+vy2*vy2+vz2*vz2);
	        if((dv1==0)||(dv2==0))
	            return -1;
	        vx1=vx1/dv1;
	        vy1=vy1/dv1;
	        vz1=vz1/dv1;
	        vx2=vx2/dv2;
	        vy2=vy2/dv2;
	        vz2=vz2/dv2;
	        //finite precision can result in normalized sums greater than +-1. catch these with the min
	        return Math.acos(Math.max(-1.0f,Math.min(vx1*vx2+vy1*vy2+vz1*vz2,1.0f)));
	    }

	    public void addVxToLookupTable(Map map,int myId,char  Nx, char  Ny) {
	        Iterator i = fiberPoints.iterator();
	        while(i.hasNext()) {
	            map.put(new Integer(((cPT)i.next()).cor2ind(Nx,Ny)),new Integer(myId));
	        }
	    }

	    boolean containsPoints(Point3f pts[]) {
	        for(int i=0;i<pts.length;i++)
	            for(int k=0;k<points.length;k++)
	                if(points[k].equals(pts[i]))
	                    return true;
	        return false;
	    }
	    
	    public CurvePath toCurvePath(){
	    	CurvePath cp=new CurvePath();
	    	for(Point3f pt:points){
	    		cp.add(pt);
	    	}
	    	return cp;
	    }
	    public Fiber toFiber(){
	    	XYZ[] chain = new XYZ[points.length];
	    	int i=0;
	    	for(Point3f pt : points){
	    		chain[i]=new XYZ(pt.x,pt.y,pt.z);
	    		i++;
	    	}
	    	Fiber f = new Fiber(chain);
	    	return f;
	    }

	    public String toString() {
	        String out="";
	        for(int k=0;k<points.length;k++)
	            out+=points[k]+",";
	        return out;
	    }
	    
	    //int i is the upper bound
	    private int roundinbound(float f, int max){
	    	int ff = Math.round(f);
	    	if(ff<0){ ff=0; }
	    	else if(ff>max){ ff=max; }
	    	return ff;
	    }
	    private int floorinbound(float f, int max){
	    	int ff = (int)f;
	    	if(ff<0){ ff=0; }
	    	else if(ff>max){ ff=max; }
	    	return ff;
	    }
	    private int ceilinbound(float f, int max){
	    	int ff = (int)Math.ceil(f);
	    	if(ff<0){ ff=0; }
	    	else if(ff>max){ ff=max; }
	    	return ff;
	    }
	    
	}