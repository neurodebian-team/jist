package edu.jhu.ece.iacl.plugins.quantitative;

import java.util.Arrays;
import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader.MeasurementUnit;
import edu.jhu.ece.iacl.jist.utility.JistLogger;

public class UnshuffleT1MappingVolumes extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	//private ParamVolume T1scan;		// 4-D Volume T1 weighted volumes
	private ParamVolumeCollection inputfiles; 
	private ParamFloat SliceTime;	// time between two 
	private ParamFloat DelayTime;	// wait time after inversion pulse
	private ParamInteger BlockSize;		// number of slices in a block
	private ParamOption SliceOrdering; // acquired slice ordering
	
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolumeCollection T1MapVolumes;	// Unshuffled T1 mapping volumes
	private ParamVolume T1SliceTimes;	// times along inversion curve, one per slice
	
	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Re-order T1 mapping volumes which were scanned using slice shift method.";
	private static final String longDescription = 	"Based on MAGIC acquisition, slices are aquired at different times after one shared inversion pulse." +
													"This module returns ordered Volume-series and accompanying time-axes, and is used in conjunction with T1mapping_sliceshift.\n" +
													"This implementation assumes that the slices are shifted as depicted in the following sequences:\n" +
													"   dyn 1: [inversion pulse][delay time] A, B, C, D, E\n" +
													"   dyn 2: [inversion pulse][delay time] B, C, D, E, A\n" +
													"   dyn 3: [inversion pulse][delay time] C, D, E, A, B\n" +
													"   dyn 4: [inversion pulse][delay time] D, E, A, B, C\n" +
													"   dyn 5: [inversion pulse][delay time] E, A, B, C, D\n" +
													"In which A-E are packs with a given 'number of slices shifted', of which each slice takes 'slice time' ms to be acquired.\n" +
													"So, the first slice of pack C at repetition 2 will be acquired at time: delay + (nslices+1)*slicetime ms.\n" +
													"As a result, the slices in one pack are imaged at different timepoints, and even after sorting the resulting time axis vary per slice.\n" +
													"The time-axes are saved in a 2D image, sized (nslices x ncomponents). N.B: Slice ordering 'default' on Philips scanners is 'interleaved'\n" +
													"See citation for more info.";
	

	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("UMCU");
		inputParams.setCategory("Quantitative");
		inputParams.setLabel("T1 sliceshift: Unshuffle");
		inputParams.setName("T1_sliceshift_Unshuffle");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com",""));
		info.setAffiliation("UMC Utrecht, Department of Radiology, 7 Tesla Group");
		info.setDescription(shortDescription +"\n"+longDescription);
		info.setLongDescription(shortDescription +"\n"+longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
		info.add(new Citation("Lu H, van Zijl P, Hendrikse J, Golay X. \"Multiple acquisitions with global inversion cycling (MAGIC): a multislice technique for vascular-space-occupancy dependent fMRI\"Magn Reson Med, 51:9-15 (2004)"));


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(inputfiles = new ParamVolumeCollection("Shuffled T1 volumes (4D)",null));
		
		//inputParams.add(T1scan=new ParamVolume("Shuffled T1 volumes (4D)",null)); //This is likely a rather large 4D volume, how to minimize RAM impact?
		//T1scan.setLoadAndSaveOnValidate(false);
		inputParams.add(SliceTime=new ParamFloat("Slice time (ms)",90.0f));
		inputParams.add(DelayTime=new ParamFloat("Delay time (ms)",20.0f));
		inputParams.add(BlockSize=new ParamInteger("Number of slices shifted",1,10,2));
		String[] SlOrderingList = {"Interleaved", "Ascending", "Descending"}; 
		inputParams.add(SliceOrdering = new ParamOption("Sequence Slice ordering",SlOrderingList));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		T1MapVolumes = new ParamVolumeCollection("Re-ordered T1 mapping volumes");
		T1MapVolumes.setLoadAndSaveOnValidate(false);
		//T1MapVolumes.setName("T1MapVolume");
		outputParams.add(T1MapVolumes);
		T1SliceTimes = new ParamVolume("T1 Slice Times");
		T1SliceTimes.setName("T1SliceTimes");
		outputParams.add(T1SliceTimes);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {
			this.setLabel("Initializing");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.FINER, "UnshuffleT1MappingVolumes: Start");
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			List<ParamVolume> pvlist = inputfiles.getParamVolumeList();
			int totcount = pvlist.size();
			int count = 1;
			boolean stboo = true;
			
			for(ParamVolume pv: pvlist){  // Start of volume collection loop
				ImageData scalarVol = pv.getImageData();
				float stime = SliceTime.getFloat();
				float dtime = DelayTime.getFloat();
				int bsize = BlockSize.getInt();
				String sordering = SliceOrdering.getValue(); 
				int r=scalarVol.getRows(), c=scalarVol.getCols(), s=scalarVol.getSlices(), t = scalarVol.getComponents();
				this.setTotalUnits(totcount);
	
				/****************************************************
				 * Step 3. Setup memory for the computed volumes
				 ****************************************************/
				this.setLabel("Slab "+count+" of "+ totcount + ": Allocating memory");
				JistLogger.logOutput(4, "UnshuffleT1MappingVolumes: Allocating memory");
				
				ImageData orderedvolumes = new ImageDataFloat(r, c, s, t);
				orderedvolumes.setName(scalarVol.getName()+"_ordered");
				orderedvolumes.setHeader(scalarVol.getHeader());
				ImageData stimes = new ImageDataFloat(s,t,1);
				stimes.setName(scalarVol.getName()+"_SliceTimes");
				
				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program
				 * 		   has NO knowledge of the MIPAV data structure and
				 * 		   uses NO MIPAV specific components. This dramatic
				 * 		   separation is a bit inefficient, but it dramatically
				 * 		   lower the barriers to code re-use in other applications.
				 ****************************************************/
				this.setLabel("Slab "+count+" of "+ totcount + ": Calculating slice order");
				JistLogger.logOutput(4, "UnshuffleT1MappingVolumes: Calculating Slice Order");
						/*********************************
						 * Figure out slice ordering
						 ************************************/
	
				float[] TI_initial = new float[s];
				for (int i=0; i<s; i++){
					TI_initial[i]= dtime + stime*i;
				}
				
				int[] slices = new int[s];
				if (sordering.equals("Ascending")){
					for (int i=0; i<s; i++){
						slices[i] = i;
					}
				}
				else if (sordering.equals("Descending")){
					for (int i=0; i<s; i++){
						slices[i] = s-i;
					}
				}
				else if (sordering.equals("Interleaved")){
					for (int i=0; i < s/2; i++){
						slices[i] = i*2;
						slices[i + s/2] = i*2+1;
					}
					if (s/2 != Math.floor(s/2.0)){  // Catch odd number of slices
						slices[s-1] = s-1;
					}
					
				}
	
						/*********************************
						 * Make array with acquisition times
						 ************************************/
				float[][] time = new float[s][t];
	
	
				for(int j=0; j<t; j++){ 					//for all dynamics...
					for (int i=0; i<s; i++){		 		//for all slices...
						int slice_idx = -1;
						for(int k=0; k<s; k++){ 			//find slice time
							if(slices[(k+j*bsize)%s] == i){ // where slice number == (k+ j*bsize) and loop around (modulo function)
								slice_idx = k;
							}
						}
						if(slice_idx > -1)
							time[i][j]= TI_initial[slice_idx];	//fill values with those found at indices
						else
							JistLogger.logError(JistLogger.WARNING, "UnshuffleT1MappingVolumes: Slice ordering failed");
					}
				}
	
						/*********************************************
						 *---reordering of data: 
						 ********************************************/
				this.setLabel("Slab "+count+" of "+ totcount + ": Re-ordering slices");
				for(int k=0; k<s; k++){									// for all slices...
					
					float[] timevec = new float[t];						
					for (int l=0; l<t; l++) 							// fill a timevector for all dynamics.
						timevec[l]=time[k][l];
					Arrays.sort(timevec);								//   and sort it
					int[] indices = new int[t];
					for(int i=0;i<t;i++){								// for all dynamics...
						for (int j=0;j<t;j++){
							if(time[k][j] == timevec[i])				//   find dynamics(j) where time matches timevec(i) 
								indices[i] = j;							// 		and store those.
						}
					}
					for(int l=0; l<t; l++){								// for all dynamics...
						stimes.set(k,l,timevec[l]);						//	set stimes to be timevec.
						
						for(int i=0; i<r; i++){							// and store slice values(x,y)
							for(int j=0; j<c; j++){						//	for in the order stored in indices
								orderedvolumes.set(i, j, k, l, scalarVol.getFloat(i, j, k, indices[l]));
							}
						}
					}
					
				}
							
				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.
				 ****************************************************/
	
				this.setLabel("Slab "+count+" of "+ totcount + ": Saving Data");
				JistLogger.logOutput(4, "UnshuffleT1MappingVolumes: Setting up exports.");
				scalarVol.dispose();
				
				// Store Data
				T1MapVolumes.add(orderedvolumes);
				T1MapVolumes.writeAndFreeNow(alg);
				
				if (stboo){
					MeasurementUnit[] units = new MeasurementUnit [2];
					units[0] = MeasurementUnit.NULL;
					units[1] = MeasurementUnit.MILLISEC;
					stimes.getHeader().setUnitsOfMeasure(units);
					T1SliceTimes.setValue(stimes);
					stboo = false;
				}				
				this.setCompletedUnits(count);
				count +=1;				
			} // end of volume collection loop
			
			System.gc();
			JistLogger.logOutput(JistLogger.FINER, getClass().getCanonicalName()+"\t FINISH");
		}
	}
}
