package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;


public class MedicAlgorithmChooseFileRegEx extends ProcessingAlgorithm{
	//input Params
	ParamString regEx;
	ParamFileCollection candidateFiles;
//	ParamFile candidateFiles;
	ParamFile targetFile;

	ParamFileCollection filesout;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Choose candidates from a File Collection using a Regular Expression.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(regEx=new ParamString("Regular Expression"));
		inputParams.add(candidateFiles=new ParamFileCollection("Candidate Files"));
		inputParams.add(targetFile=new ParamFile("Target File"));
		targetFile.setMandatory(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Regular Expression File Chooser");
		inputParams.setName("Regular_Expression_File_Chooser");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		filesout = new ParamFileCollection("Matching Files");
		outputParams.add(filesout);

	}


	protected void execute(CalculationMonitor monitor) {
//		System.out.println(getClass().getCanonicalName()+"\t"+targetFile.getValue().getName());
//		System.out.println(getClass().getCanonicalName()+"\t"+candidateFiles.getValue().getName());

		System.out.println(getClass().getCanonicalName()+"\t"+"Finding matches...");
		if(targetFile.getValue()==null){
			filesout.setValue(matchFile(regEx.getValue(),candidateFiles.getValue()));
		}else{
			filesout.setValue(matchFile(regEx.getValue(),targetFile.getValue(),candidateFiles.getValue()));
		}
		System.out.println(getClass().getCanonicalName()+"\t"+filesout.getValue());
	}


	public ArrayList<File> matchFile(String expression, File target, List<File> toMatch){

		Pattern p = Pattern.compile(expression);
		Matcher m = p.matcher(target.getName());
		m.find();
		String tagExp = target.getName().substring(m.start(), m.end());

		Pattern pt = Pattern.compile(tagExp);

		ArrayList<File> matches = new ArrayList<File>();

		for(File s : toMatch){
			Matcher mat = pt.matcher(s.getName());
			if(mat.find()){
				matches.add(s);
			}
		}
		return matches;
	}


	public ArrayList<File> matchFile(String expression,List<File> toMatch){
		Pattern pt = Pattern.compile(expression);
		ArrayList<File> matches = new ArrayList<File>();
		for(File s : toMatch){
			Matcher mat = pt.matcher(s.getName());
			if(mat.find()){
				matches.add(s);
			}
		}
		return matches;
	}
}
