package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class VolumeCensorRange extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume inVolume;
	private ParamFloat valMin;
	private ParamFloat valMax;
	private ParamBoolean NaNboo;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume outVolume;	// Estimate T2 map

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Censor the range of a volume w/o changing type.";
	private static final String longDescription = " All image intensities less than the input \"Min Value\" will be set to min value. \n" +
			"Similarly, all image intensities greater than the input \"Max Value\" will be set to Max Value.\n" +
			"Alternatively, all values outside the set range can be set to be NaN (make sure this is supported by downstream modules!)";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Volume: Censor Range");
		inputParams.setName("Volume_Censor_Range");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(inVolume=new ParamVolume("Volume",null,-1,-1,-1,-1));
		inputParams.add(valMin=new ParamFloat("Min Value",0f));
		inputParams.add(valMax=new ParamFloat("Max Value",4095f));
		inputParams.add(NaNboo = new ParamBoolean("Set outside values to NaN",false));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		outVolume = new ParamVolume("Censor Volume",VoxelType.FLOAT,-1,-1,-1,-1);
		outVolume.setName("CensoredVolume");
		outputParams.add(outVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t"+"START");

			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageDataFloat inVol=new ImageDataFloat(inVolume.getImageData());

			float min = valMin.getFloat();
			float max = valMax.getFloat();

			int r=inVol.getRows(), c=inVol.getCols(), s=inVol.getSlices(), t = inVol.getComponents();
			this.setTotalUnits(r);
			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t Allocating memory.");

			ImageData volOut = new ImageDataFloat(r,c,s,t);
			volOut.setName(inVol+"_censor");
			volOut.setHeader(inVol.getHeader());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t"+"Censoring");
			int oldNaN = 0;
			int newNaN = 0;
			int changecnt = 0;
			for(int i=0;i<r;i++) {
				this.setCompletedUnits(i);
				for(int j=0;j<c;j++){
					for(int k=0;k<s;k++) {
						for(int l=0;l<t;l++) {
							float S = inVol.getFloat(i,j,k,l);
							volOut.set(i,j,k,l,S);
							if(S<min || S>max){
								if(NaNboo.getValue()){
									volOut.set(i,j,k,l,Float.NaN);
									newNaN++;
								}else{
									volOut.set(i,j,k,l,(S<min?min:(S>max?max:S)));
									changecnt++;
								}

							//Check for pre-existing NaN in input volume (just for logging)									
							}if(Float.isNaN(S)){
								volOut.set(i,j,k,l,Float.NaN);
								oldNaN++;
							}
												
						}
					}
				}
			}
			JistLogger.logOutput(JistLogger.FINE, getClass().getCanonicalName()+"\t"+"Setting " +changecnt+ " voxels to new boundary value.");
			JistLogger.logOutput(JistLogger.FINE, getClass().getCanonicalName()+"\t"+"Found (and passed through) "+ oldNaN + " NaN's in Input Volume, and set "+newNaN+ " outlying values to NaN.");
						
			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t"+"Setting up exports.");
			outVolume.setValue(volOut);
			JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t"+"FINISHED");
		}
	}
}
