package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.util.ArrayList;

import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.FileUtil;
import gov.nih.mipav.model.structures.ModelImage;


public class MedicAlgorithmExtractAllComponents extends ProcessingAlgorithm{
	private ParamVolume in;
	private ParamInteger dim;
	private ParamVolumeCollection out;	

	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Split a multi-component volume into multiple separate volumes.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(in=new ParamVolume("Mult-Component Volume",null,-1,-1,-1,-1));
		inputParams.add(dim=new ParamInteger("Dimension to Extract"));
		dim.setValue(new Integer(3));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Extract All Components");
		inputParams.setName("Extract_All_Components");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(out=new ParamVolumeCollection("Volumes Out",null,-1,-1,-1,1));
		out.setLoadAndSaveOnValidate(false);
	}


	protected void execute(CalculationMonitor monitor) {		
        ImageData vol = in.getImageData();
        ImageHeader hdr = vol.getHeader();
//        for(int k=0; k<vol.getComponents(); k++){
//          //For a 3d volume, make sure the component is set to 1 and not 0
//            listout.add(new CubicVolumeMipav("Volume"+k,vol.getType(),vol.getRows(), vol.getCols(), vol.getSlices(),1));
//        }

        if(dim.getValue().intValue()==3){
        	System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
        	for(int l=0; l<vol.getComponents(); l++){
        		ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+l,vol.getType(),vol.getRows(), vol.getCols(), vol.getSlices());
        		for(int i=0;i<vol.getRows();i++){
        			for(int j=0;j<vol.getCols();j++){
        				for(int k=0;k<vol.getSlices();k++){

//      					if(vol.get(i, j, k, l).shortValue()!=0){
//      					System.out.println(getClass().getCanonicalName()+"\t"+vol.get(i, j, k, l));
//      					}
        					nextvol.set(i, j, k, vol.getDouble(i, j, k, l));
        				}
        			}
        		}
        		System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");

        		/* Update Header information*/
        		nextvol.setHeader(hdr);
        		out.add(nextvol);
        		out.writeAndFreeNow(this);        		
        		
        	}
        }else if(dim.getValue().intValue()==2){
        	System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
        	for(int l=0; l<vol.getSlices(); l++){
//        	for(int l=86; l<88; l++){
        		ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+l,vol.getType(),vol.getRows(), vol.getCols(), vol.getComponents());
        		for(int i=0;i<vol.getRows();i++){
        			for(int j=0;j<vol.getCols();j++){
        				for(int k=0;k<vol.getComponents();k++){
        					nextvol.set(i, j, k, vol.getDouble(i, j, l, k));
        				}
        			}
        		}
        		System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
        		/* Update Header information*/
        		nextvol.setHeader(hdr);
        		out.add(nextvol);
        		out.writeAndFreeNow(this);
        	}
        }else if(dim.getValue().intValue()==1){
        	System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
        	for(int l=0; l<vol.getCols(); l++){
        		ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+l,vol.getType(),vol.getRows(), vol.getSlices(), vol.getComponents());
        		for(int i=0;i<vol.getRows();i++){
        			for(int j=0;j<vol.getSlices();j++){
        				for(int k=0;k<vol.getComponents();k++){

//      					if(vol.get(i, j, k, l).shortValue()!=0){
//      					System.out.println(getClass().getCanonicalName()+"\t"+vol.get(i, j, k, l));
//      					}
        					nextvol.set(i, j, k, vol.getDouble(i, l, j, k));
        				}
        			}
        		}
        		System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
        		/* Update Header information*/
        		ModelImage img=(nextvol).getModelImageCopy();
        		FileUtil.updateFileInfo(vol.getModelImageCopy(),img);
        		img.calcMinMax();
        		out.add(nextvol);
        		out.writeAndFreeNow(this);
        	}
        }
        else if(dim.getValue().intValue()==0){
        	System.out.println(getClass().getCanonicalName()+"\t"+vol.getType());
        	for(int l=0; l<vol.getRows(); l++){
        		ImageDataMipav nextvol = new ImageDataMipav(vol.getName()+"_Volume"+l,vol.getType(),vol.getCols(), vol.getSlices(), vol.getComponents());
        		for(int i=0;i<vol.getCols();i++){
        			for(int j=0;j<vol.getSlices();j++){
        				for(int k=0;k<vol.getComponents();k++){
//      					if(vol.get(i, j, k, l).shortValue()!=0){
//      					System.out.println(getClass().getCanonicalName()+"\t"+vol.get(i, j, k, l));
//      					}
        					nextvol.set(i, j, k, vol.getDouble(l, i, j, k));
        				}
        			}
        		}
        		System.out.println(getClass().getCanonicalName()+"\t"+"Completed " +l+"th volume");
        		/* Update Header information*/
        		nextvol.setHeader(hdr);
        		out.add(nextvol);
        		out.writeAndFreeNow(this);
        	}
        }
        
	}
}
