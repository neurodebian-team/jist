package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLikelihood;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class DWITensorEstLikelihood extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamFileCollection DWdata4D; 		// SLAB-enabled Imaging Data
	private ParamFileCollection Mask3D;			// SLAB-enabled Binary mask to indicate computation volume
	private ParamFile bvaluesTable;		// .b file with a list of b-values
	private ParamFile kSpaceAvgTable;		// .txt file with a list of k-space averages per volume relative to the reported NF
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions
	private ParamFileCollection noiseField; 
	private ParamFloat noiseLevel;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFileCollection tensorVolume;	// SLAB-enabled A 4D volume with one tensor estimated per pixel
	private ParamFileCollection noiseFieldVolume; // refined noise field estimate after tensor estimation
	private ParamFileCollection referenceVolume; // refined reference estimate after tensor estimation 

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Diffusion Tensor Estimation by Maximizing Rician Likelihood.";
	private static final String longDescription = 	"Tensor Estimation while taking into account the joint\n" +
													"distribution of all observed data in the context of an\n" +
													"augmented tensor model to account for variable levels of \n" +
													"Rician noise.\n" +
													"Inputs: \n" +
													"\t-DWI and Reference Image(s) Data (4D file collection)\n" +
													"\t-Table of diffusion weighting directions (file)\n" +
													"\t-Table of b-values (file)\n" +
													"\t-Table of k-Space Average (file)\n" +
													"\t-Mask Volume to Determine Region of Tensor Estimation (3D file, optional)\n" +
													"\t-Noise Field (3D file, optional)\n" +
													"\t-Constant Noise Level (float)\n\n" +
													"See citation for more info.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Tensor Estimation Likelihood");
		inputParams.setName("Tensor_Estimation_Likelihood");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new Citation("  B. A. Landman, P-L. Bazin, and J. L. Prince. \"Diffusion Tensor Estimation by Maximizing Rician Likelihood\", In Proceedings of the 2007 International Conference on Computer Vision Workshop on Mathematical Methods in Biomedical Image Analysis, Rio de Janeiro, Brazil, October 2007."));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamFileCollection("DWI and Reference Image(s) Data (4D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(kSpaceAvgTable=new ParamFile("Table of k-Space Average",new FileExtensionFilter(new String[]{"txt","lst"})));
		inputParams.add(Mask3D=new ParamFileCollection("Mask Volume to Determine Region of Tensor Estimation (3D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.			
		inputParams.add(noiseField = new ParamFileCollection("Noise Field",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		noiseField.setMandatory(false);
		inputParams.add(noiseLevel = new ParamFloat("Constant Noise Level",0));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		tensorVolume = new ParamFileCollection("Tensor Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		tensorVolume.setName("Tensor (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensorVolume);		
		noiseFieldVolume = new ParamFileCollection("Noise Field Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		noiseFieldVolume.setName("Noise Field");
		outputParams.add(noiseFieldVolume);		
		referenceVolume = new ParamFileCollection("Reference Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		referenceVolume.setName("Reference");
		outputParams.add(referenceVolume);	
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute() {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: Start");
			//		MedicUtil.setQuiet(false);

			/****************************************************
			 * Step 2. Loop over input slabs
			 ****************************************************/
			List<File> dwList = DWdata4D.getValue();
			List<File> maskList = Mask3D.getValue();
			List<File> noiseFieldList =noiseField.getValue();
			ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
			ArrayList<File> outTensorVols = new ArrayList<File>();
			ArrayList<File> outNFVols = new ArrayList<File>();
			ArrayList<File> outRefVols = new ArrayList<File>();
			for(int jSlab=0;jSlab<dwList.size();jSlab++) {
				/****************************************************
				 * Step 2. Parse the input data 
				 ****************************************************/
				ImageData dwd=rw.read(dwList.get(jSlab));

				String sourceName = dwd.getName();
				ImageDataFloat DWFloat=new ImageDataFloat(dwd);
				dwd.dispose();
				dwd=null;


				ImageData maskVol=null;
				if(maskList!=null)
					if(maskList.size()>jSlab)
						if(maskList.get(jSlab)!=null)
							maskVol=rw.read(maskList.get(jSlab));

				byte [][][]mask=null;
				if(maskVol!=null) {
					ImageDataUByte maskByte = new ImageDataUByte (maskVol);
					mask = maskByte.toArray3d();
					maskByte.dispose();
					maskByte=null;
					maskVol.dispose();
					maskVol=null;
				}

				float [][]bs=null;		
				TextFileReader text = new TextFileReader(bvaluesTable.getValue());
				try {
					bs = text.parseFloatFile();
				} catch (IOException e) 
				{
					throw new RuntimeException("LLMSE: Unable to parse b-file");
				}

				float [][]grads=null;
				text = new TextFileReader(gradsTable.getValue());
				try {
					grads  = text.parseFloatFile();
				} catch (IOException e) { 

					throw new RuntimeException("LLMSE: Unable to parse grad-file");
				}

				float [][]Kavg=null;		
				text = new TextFileReader(kSpaceAvgTable.getValue());
				try {
					Kavg = text.parseFloatFile();
				} catch (IOException e) 
				{
					throw new RuntimeException("LLMSE: Unable to parse b-file");
				}

				System.out.println(getClass().getCanonicalName()+"\t"+"Kspace-avgs:"+Kavg.length);
				double []kSpaceAvgs = new double[Kavg.length];
				for(int i=0;i<Kavg.length;i++)
					kSpaceAvgs[i]=Kavg[i][0];

				/****************************************************
				 * Step 2b. Load the noise field 
				 ****************************************************/
				float[][][] noiseField = new float[DWFloat.getRows()][DWFloat.getCols()][DWFloat.getSlices()];
				System.out.println(getClass().getCanonicalName()+"\t"+"Noise Field size: "+DWFloat.getRows()+"x"+DWFloat.getCols()+"x"+DWFloat.getSlices());
				if(noiseFieldList.size()>jSlab) {
					// A noise field was specified 
					ImageData nf=rw.read(noiseFieldList.get(jSlab));
					for(int i=0;i<noiseField.length;i++)
						for(int j=0;j<noiseField[0].length;j++)
							for(int k=0;k<noiseField[0][0].length;k++)
								noiseField[i][j][k]=nf.getFloat(i,j,k);
					nf.dispose();
				} else {
					// use a constant noise file
					for(int i=0;i<noiseField.length;i++)
						for(int j=0;j<noiseField[0].length;j++)
							for(int k=0;k<noiseField[0][0].length;k++)
								noiseField[i][j][k]=noiseLevel.getFloat();
				}


				/****************************************************
				 * Step 3. Perform limited error checking 
				 ****************************************************/
				// If there are 4 columns in the gradient table, remove the 1st column (indecies)
				if(grads[0].length==4) {
					float [][]g2 = new float[grads.length][3];
					for(int i=0;i<grads.length;i++) 
						for(int j=0;j<3;j++)
							g2[i][j]=grads[i][j+1];
					grads=g2;
				}

				if(grads[0].length!=3)
					throw new RuntimeException("LLMSE: Invalid gradient table. Must have 3 or 4 columns.");
				if(bs[0].length!=1)
					throw new RuntimeException("LLMSE: Invalid b-value table. Must have 1 column.");
				float []bval = new float[bs.length];
				for(int i=0;i<bval.length;i++)
					bval[i]=bs[i][0];

				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program 
				 * 		   has NO knowledge of the MIPAV data structure and 
				 * 		   uses NO MIPAV specific components. This dramatic 
				 * 		   separation is a bit inefficient, but it dramatically 
				 * 		   lower the barriers to code re-use in other applications.  		  
				 ****************************************************/
				float[][][][] tensors = new float[DWFloat.getRows()][DWFloat.getCols()][DWFloat.getSlices()][6];			
				float[][][] reference = new float[DWFloat.getRows()][DWFloat.getCols()][DWFloat.getSlices()];;
				// tensors = EstimateTensorLLMSE.estimate(DWFloat.toArray4d(),bval,grads,mask,estOptions.getValue().compareToIgnoreCase("Yes")==0);
				EstimateTensorLikelihood.estimate(tensors,reference,noiseField,DWFloat.toArray4d(),bval,grads,mask,kSpaceAvgs);
				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.  
				 ****************************************************/		

				ImageData  out= (new ImageDataFloat(tensors));
				out.setHeader(DWFloat.getHeader());	
				tensors = null; //no longer needed
				out.setName(sourceName+"_LikelihoodTensor");	
				File outputSlab = rw.write(out, getOutputDirectory());
				outTensorVols.add(outputSlab);
				out.dispose();
				out=null;		

				out= (new ImageDataFloat(noiseField));
				out.setHeader(DWFloat.getHeader());
				noiseField = null; //no longer needed
				out.setName(sourceName+"_LikelihoodNF");
				outputSlab = rw.write(out, getOutputDirectory());
				outNFVols.add(outputSlab);
				out.dispose();
				out=null;

				out= (new ImageDataFloat(reference));
				out.setHeader(DWFloat.getHeader());	
				reference = null; //no longer needed
				out.setName(sourceName+"_LikelihoodRef");	
				outputSlab = rw.write(out, getOutputDirectory());
				outRefVols.add(outputSlab);
				out.dispose();
				out=null;

				DWFloat.dispose();
				DWFloat=null;

			}
			tensorVolume.setValue(outTensorVols);
			noiseFieldVolume.setValue(outNFVols);
			referenceVolume.setValue(outRefVols);

			/****************************************************
			 * Step 6. Let the user know that your code is finished.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: FINISHED");
		}
	}
}
