package edu.jhu.ece.iacl.plugins.dti;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.dti.ParV4Info;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringArrayXMLReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;


public class MedicAlgorithmMultiParV4Info extends ProcessingAlgorithm {
	//Input Parameter
	private ParamFileCollection parfiles;

	//Output Parameters
	private ParamObject<String> sliceAng;
	private ParamObject<String> sliceOrient;
	private ParamObject<String> scanDate;
	private ParamObject<String> foldOver;
	private ParamObject<String> patientOrient;
	private ParamObject<String> patientPosition;
	private ParamObject<String> parVer;
	private ParamFileCollection bvals;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	private static final String shortDescription = "Parses Phillips .par files (version 4 and later) and extracts information relevant for DTI Gradient Table Creation.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(parfiles=new ParamFileCollection("ParV4 file", new FileExtensionFilter(new String[]{"par","PAR","parv2","rec","REC"})));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("ParV4 DTI Info");
		inputParams.setName("ParV4_DTI_Info");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.add(new AlgorithmAuthor("Jon Farrell", "", ""));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(scanDate = new ParamObject<String>("Scan Date", new StringReaderWriter()));
		outputParams.add(parVer = new ParamObject<String>("Par Version", new StringReaderWriter()));
		outputParams.add(sliceAng = new ParamObject<String>("Slice Angulation", new StringReaderWriter()));
		outputParams.add(sliceOrient = new ParamObject<String>("Slice Orientation", new StringReaderWriter()));
		outputParams.add(foldOver = new ParamObject<String>("Fold Over", new StringReaderWriter()));
		outputParams.add(patientOrient = new ParamObject<String>("Patient Orientation", new StringReaderWriter()));
		outputParams.add(patientPosition = new ParamObject<String>("Patient Position", new StringReaderWriter()));
		outputParams.add(bvals = new ParamFileCollection("b values" , new FileExtensionFilter(new String[]{"b"})));
	}


	@Override
	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		int N = parfiles.getValue().size();
		String[] scandates = new String[N];
		String[] parVersions = new String[N];
		String[] slcAngs = new String[N];
		String[] slcOrients = new String[N];
		String[] folist = new String[N];
		String[] patposlist = new String[N];
		String[] patorientlist = new String[N];
		String[] numVollist = new String[N];
		String[] bvallist = new String[N];

		StringArrayXMLReaderWriter saxml = new StringArrayXMLReaderWriter();


		for(int i=0; i<parfiles.getValue().size(); i++){
			System.out.println(getClass().getCanonicalName()+"\t"+parfiles.getValue().get(i).getPath());

			ParV4Info info = new ParV4Info(parfiles.getValue().get(i).getPath());
			info.getInfo();

			scandates[i]=info.getDate();
			parVersions[i]=info.getParVersion();
			info.getSliceAngulation();
			slcAngs[i]=saxml.vectorToString(info.getSliceAngulation());
			slcOrients[i]=info.getSliceOrientation();
			numVollist[i]=Integer.toString(info.getNumVolumes());
			bvallist[i]=info.getbVals();
			folist[i]=info.getFoldover();
			patposlist[i]=info.getPatientPosition();
			patorientlist[i]=info.getPatientOrientation();

			System.out.println(getClass().getCanonicalName()+"\t"+" ");
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"size: " + bvallist.length);
		System.out.println(getClass().getCanonicalName()+"\t"+bvallist[0]);

		scanDate.setObject(saxml.writeStrings(scandates));
		scanDate.setFileName("date.txt");

		sliceOrient.setObject(saxml.writeStrings(slcOrients));
		sliceOrient.setFileName("sliceOrientation.txt");

		sliceAng.setObject(saxml.writeStrings(slcAngs));
		sliceAng.setFileName("SliceAngulation.txt");
		sliceAng.setName("Slice Angulation");

		foldOver.setObject(saxml.writeStrings(folist));
		foldOver.setFileName("Foldover.txt");

		patientOrient.setObject(saxml.writeStrings(patorientlist));
		patientOrient.setFileName("PatientOrientations.txt");

		patientPosition.setObject(saxml.writeStrings(patposlist));
		patientPosition.setFileName("PatientPositions.txt");

		File outputdir = this.getOutputDirectory();
		List<File> bfiles=null;
		try{
//			File destdir = new File(outputdir.getCanonicalFile()+File.separator+"mparv4info");
			File destdir = new File(outputdir.getCanonicalFile().toString()+File.separator+this.getAlgorithmName());
			if(!destdir.isDirectory()){
				(new File(destdir.getCanonicalPath())).mkdir();
			}
			bfiles= writebvals(destdir,bvallist,parfiles.getValue());
		}catch(IOException e){
			e.printStackTrace();
		}

//		bvals.setObject(saxml.writeStrings(bvallist));
//		bvals.setFileName("Bvalues.b");
		bvals.setValue(bfiles);

//		numVolumes.setObject(saxml.writeStrings(numVollist));
//		numVolumes.setFileName("NumVolumes");
		
		parVer.setObject(saxml.writeStrings(parVersions));
		parVer.setFileName("ParVersions.txt");

		System.out.println(getClass().getCanonicalName()+"\t"+"sliceOrient: "+sliceOrient.getValue());
//		System.out.println(getClass().getCanonicalName()+"\t"+numVolumes.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"bvals: "+bvals.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"parVer: "+parVer.getObject());
	}


	private List<File> writebvals(File dir, String[] bvallist, List<File> parfiles) throws IOException{
		ArrayList<File> bvalfiles = new ArrayList<File>();

		int i=0;
		for(String s : bvallist){
			String f = dir.getCanonicalPath()+File.separator+parfiles.get(i).getName();
			f = f.substring(0, f.lastIndexOf('.'));
			File out = new File(f+".b");
			System.out.println(getClass().getCanonicalName()+"\t"+"Writing bvalues to : "+out);
			StringReaderWriter rw = new StringReaderWriter();
			rw.write(bvallist[i], out);
//			rw.write(bvallist[i]);
			i++;
			bvalfiles.add(out);
		}
		return bvalfiles;
	}
}
