/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.math;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class MedicAlgorithmImageCalculator extends ProcessingAlgorithm{
	ParamVolume vol1Param;
	ParamVolume vol2Param;
	ParamVolume resultVolParam;
	ParamOption operation;

	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Perform simple image calculator operations on two images. The operations include 'Add', 'Subtract', 'Multiply', and 'Divide'";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vol1Param=new ParamVolume("Volume 1"));
		inputParams.add(vol2Param=new ParamVolume("Volume 2"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Add","Subtract","Multiply","Divide", "Min","Max"}));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Math");
		inputParams.setLabel("Image Calculator");
		inputParams.setName("Image_Calculator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVolParam=new ParamVolume("Result Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ImageData vol1=vol1Param.getImageData();
		ImageData vol2=vol2Param.getImageData();
		int rows=vol1.getRows();
		int cols=vol1.getCols();
		int slices=vol1.getSlices();
		int comps = vol1.getComponents();
		ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",rows,cols,slices,comps);
		
		double tmp1,tmp2;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < comps; l++) {
						tmp1=vol1.getDouble(i, j, k, l);
						tmp2=vol2.getDouble(i, j, k, l);

						switch(operation.getIndex()){
						case 0:resultVol.set(i, j, k, l, tmp1+tmp2);break;
						case 1:resultVol.set(i, j, k, l, tmp1-tmp2);break;
						case 2:resultVol.set(i, j, k, l, tmp1*tmp2);break;
						case 3:resultVol.set(i, j, k, l, tmp1/tmp2);break;
						case 4:resultVol.set(i, j, k, l, Math.min(tmp1,tmp2));break;
						case 5:resultVol.set(i, j, k, l, Math.max(tmp1,tmp2));break;
						default:
						}
					}
				}
			}
		}
//		resultVol.setHeader(vol1Param.getImageData().getHeader());
		resultVol.setHeader(vol1.getHeader());
		resultVolParam.setValue(resultVol);
	}
}
