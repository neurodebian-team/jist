package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.algorithms.hardi.SurfaceTools;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class SurfToGrad extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters 
	 ****************************************************/	
	private ParamSurface surface;
	private ParamBoolean selectUnique;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFile grad;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Convert Surface to Gradient");	
		inputParams.setName("Convert_Surface_to_Gradient");	


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu","http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(surface=new ParamSurface("Input Surface"));		
		inputParams.add(this.selectUnique=new ParamBoolean("Select unique directions",true));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		outputParams.add(grad = new ParamFile("Gradient Table",new FileExtensionFilter(new String[]{"grad"})));		
	}


	protected void execute(CalculationMonitor monitor) {
		EmbeddedSurface surf=surface.getSurface();
		if(selectUnique.getValue())
			surf = new EmbeddedSurface(SurfaceTools.selectUniqueDirectionsAndReflections(surf,1e-6));

		String filename = this.getOutputDirectory().toString() 
		+ File.separatorChar + surface.getSurface().getName() + ".grad";

		Point3f []vert = surf.getVertexCopy();
		File gfile= new File(filename);
		FileWriter rw;
		try {
			rw = new FileWriter(gfile);

		for(Point3f p : vert) {
			p.scale((float) (1/Math.sqrt(p.x*p.x+p.y*p.y+p.z*p.z)));
			rw.write(p.x+" \t"+p.y+" \t"+p.z+"\n");
		}
		rw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("File I/O error.");
		}
		grad.setValue(gfile);
	}
}
