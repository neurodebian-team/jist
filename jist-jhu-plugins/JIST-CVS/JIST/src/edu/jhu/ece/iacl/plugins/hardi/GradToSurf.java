package edu.jhu.ece.iacl.plugins.hardi;

import java.io.IOException;

import javax.vecmath.Point3f;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.hardi.SurfaceTools;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.plugins.dti.DWIDefineCaminoScheme;


public class GradToSurf extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters 
	 ****************************************************/	
	private ParamFile grad;
	private ParamBoolean mirrorDirections;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamSurface surface;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Convert Gradient to Surface");	


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(grad = new ParamFile("Gradient Table",new FileExtensionFilter(new String[]{"grad"})));		
		inputParams.add(this.mirrorDirections=new ParamBoolean("Mirror diffusion directions",true));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		outputParams.add(surface=new ParamSurface("Output Surface"));
	}


	protected void execute(CalculationMonitor monitor) {
		float [][]grads=null;
		TextFileReader text = new TextFileReader(grad.getValue());
		try {
			grads  = text.parseFloatFile();
		} catch (IOException e) { 
			throw new RuntimeException("LLMSE: Unable to parse grad-file");
		}
		Point3f []pts;
		if(this.mirrorDirections.getValue()) {
			pts = new Point3f[grads.length*2];
			for(int i=0;i<grads.length;i++) {
				pts[2*i]=new Point3f(grads[i][0],grads[i][1],grads[i][2]);
				pts[2*i+1]=new Point3f(-grads[i][0],-grads[i][1],-grads[i][2]);
			}
		} else {
			pts = new Point3f[grads.length];
			for(int i=0;i<grads.length;i++) {
				pts[i]=new Point3f(grads[i][0],grads[i][1],grads[i][2]);			
			}
		}
		EmbeddedSurface surf=null;
		if(pts.length>3)
			surf=new EmbeddedSurface(SurfaceTools.quickHull3D(pts));
		else 
			surf =new EmbeddedSurface((pts), new int[]{0,1,2});

		surf.setName(DWIDefineCaminoScheme.getFileNameWithoutExtension(grad.getValue().getName()));
		surface.setValue(surf);
	}
}
