package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.algorithms.hardi.ExtractPARGradientTable;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.plugins.dti.DWIDefineCaminoScheme;


public class ExtractBvalGradFromPAR extends ProcessingAlgorithm{
	//output params
	private ParamFile grad;	// gradient directions
	private ParamFile bout;		// b values

	//input params
	private ParamFileCollection parFiles;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Extract b/grad tables from new (modern 4.2+ PAR files).";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Extract b and grad from PAR files");
		inputParams.setName("Extract_b_and_grad_from_PAR_files");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(parFiles= new ParamFileCollection("Par files",new FileExtensionFilter(new String[]{"par","PAR","parv2","PARv2"})));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(bout = new ParamFile("b-Values",new FileExtensionFilter(new String[]{"b"})));
		outputParams.add(grad = new ParamFile("Gradient Tables",new FileExtensionFilter(new String[]{"grad"})));
	}


	/*
	 * The execute algorithm that implements ProcessingAlgorithms's abstract method definition.
	 */
	protected void execute(CalculationMonitor monitor) {
		String gradfilename = null;
		String bfilename = null;

		for(int ii=0;ii<parFiles.size();ii++) {
			System.out.println(getClass().getCanonicalName()+"\t"+"# "+(ii+1)+" of "+parFiles.size());
			File par = parFiles.getValue(ii);
			System.out.println(getClass().getCanonicalName()+"\t"+"PAR: "+par.toURI());
			System.out.flush();
			if(gradfilename==null)
				gradfilename = getOutputDirectory().toString()
				+ File.separatorChar + DWIDefineCaminoScheme.getFileNameWithoutExtension(par.getName()) + ".grad";
			if(bfilename==null)
				bfilename = getOutputDirectory().toString()
				+ File.separatorChar + DWIDefineCaminoScheme.getFileNameWithoutExtension(par.getName()) + ".b";

			Vector<Float>b = new Vector<Float>();

			Vector<Point3f>g = new Vector<Point3f>();
			System.out.println(getClass().getCanonicalName()+"\t"+"foo");System.out.flush();
			ExtractPARGradientTable.ExtractPARGradientTable(par.getAbsolutePath(),
					b,g);

			for(int i=0;i<b.size();i++) {
				if(b.get(i).floatValue()!=0.0f) {
					Point3f pt = g.get(i);
					if(pt.x==0.0f && pt.y==0.0f && pt.z==0.0f) {
						g.set(i, new Point3f(100,100,100));
					}
				}
			}

			System.out.println(getClass().getCanonicalName()+"\t"+"b-values:");
			for(Float f : b) {
				System.out.println(getClass().getCanonicalName()+"\t"+"\t"+f.toString());
			}
			System.out.println(getClass().getCanonicalName()+"\t"+"grad-directions:");
			for(Point3f p : g) {
				System.out.println(getClass().getCanonicalName()+"\t"+"\t"+p.x+" \t"+p.y+" \t"+p.z);
			}
			System.out.flush();
			File outDir = this.getOutputDirectory();//MipavController.getDefaultWorkingDirectory();

			System.out.println(getClass().getCanonicalName()+"\t"+bfilename); System.out.flush();

			File bfile= new File(bfilename);
			FileWriter rw;
			try {
				rw = new FileWriter(bfile,true);

				for(Float f : b) {
					rw.write(f.toString()+"\n");
				}
				rw.close();
				bout.setValue(bfile);


				File gfile= new File(gradfilename);
				rw = new FileWriter(gfile,true);
				for(Point3f p : g) {
					rw.write(p.x+" \t"+p.y+" \t"+p.z+"\n");
				}
				rw.close();

				grad.setValue(gfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException("I/O Error.");
			}
		}
	}
}
