package edu.jhu.ece.iacl.plugins.utilities.surface;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class MedicAlgorithmReshapeSurface extends ProcessingAlgorithm{
	ParamPointDouble offset;
	ParamDouble scaleMesh,scaleData;
	ParamSurface inSurf,outSurf;
	ParamMatrix transMatrix;
	ParamBoolean invert;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Reshape a surface mesh through, scaling of vertices/data, and translation.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inSurf=new ParamSurface("Surface"));
		inputParams.add(scaleMesh=new ParamDouble("Scale Vertices",0,1000,1));
		inputParams.add(scaleData=new ParamDouble("Scale Embedded Data",0,1000,1));
		inputParams.add(offset=new ParamPointDouble("Offset"));
		inputParams.add(transMatrix=new ParamMatrix("Transformation Matrix",4,4));
		for (int i = 0; i < 4; i++) transMatrix.setValue(i, i, 1);

		inputParams.add(invert=new ParamBoolean("Invert Matrix",false));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Surface");
		inputParams.setLabel("Reshape Surface");
		inputParams.setName("Reshape_Surface");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Surface"));
		outputParams.setName("reshapesurf");
		outputParams.setLabel("Reshape Surface");
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		EmbeddedSurface surf=inSurf.getSurface();
		surf.scaleVertices(scaleMesh.getFloat());
		surf.scaleData(scaleData.getFloat());
		surf.translate(new Point3f(offset.getValue()));
		Jama.Matrix m=transMatrix.getValue();
		if(invert.getValue())m=m.inverse();
		surf.transform(transMatrix.getValue());
		outSurf.setValue(surf);
	}
}
