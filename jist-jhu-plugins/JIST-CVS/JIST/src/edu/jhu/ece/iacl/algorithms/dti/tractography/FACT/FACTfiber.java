package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.util.Map;
import java.util.LinkedList;
import java.util.Iterator;

import javax.vecmath.*;

import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.geom.CurvePath;

/**
 * Created by IntelliJ IDEA.
 * User: bennett
 * Date: Nov 16, 2005
 * Time: 7:23:15 AM
 * To change this template use Options | File Templates.
 */
public class FACTfiber {
    static final boolean FORWARD = true;
    static final boolean BACKWARD = false;
    static final double pi2 = Math.PI/2;
    static float stepSize = 0.1f;
    static float DTIStudioStepSize = 0.1f;
    int numPoints;
    Point3f points[];
    LinkedList<Point3f> fiberPoints;


    private class RAY{
        Point3f pt;
        float dist;
        public RAY(float x, float y, float z,float d) {
            pt = new Point3f(x,y,z);
            dist = d;
        }
        public String toString(){
        	return "RAY: " + pt.x + "," + pt.y + "," + pt.z + " dist:" + dist;
        }
    }

    public int track(FACTparameters dat, float  startX, float  startY, float  startZ) {
        fiberPoints = new LinkedList<Point3f>();
        fiberPoints.add(new Point3f(startX,startY,startZ)); numPoints=1;
        
        //System.out.println("jist.plugins"+"\t"+);System.out.println("jist.plugins"+"\t"+"READY TO START TRACKING...."); System.out.println("jist.plugins"+"\t"+);
        
        int sfx = floorinbound(startX,dat.Nx-1);
        int sfy = floorinbound(startY,dat.Ny-1);
        int sfz = floorinbound(startZ,dat.Nz-1);
       
        
//        int scx = ceilinbound(startX,dat.Nx-1);
//        int scy = ceilinbound(startY,dat.Ny-1);
//        int scz = ceilinbound(startZ,dat.Nz-1);
        
//        System.out.println("jist.plugins"+"\t"+"TRACKING BACKWARD");
        track(BACKWARD,dat,startX,startY,startZ,-dat.vecData[sfz][sfy][sfx][0],
                -dat.vecData[sfz][sfy][sfx][1],-dat.vecData[sfz][sfy][sfx][2],
                startX,startY,startZ);
//        System.out.println("jist.plugins"+"\t"+"TRACKING FORWARD");
        track(FORWARD,dat,startX,startY,startZ,dat.vecData[sfz][sfy][sfx][0],
                dat.vecData[sfz][sfy][sfx][1],dat.vecData[sfz][sfy][sfx][2],
                startX,startY,startZ);

//        System.out.println("jist.plugins"+"\t"+"*********************************");
//        System.out.println("jist.plugins"+"\t"+"*********FIBER FINISHED**********");
//        System.out.println("jist.plugins"+"\t"+fiberPoints);
//        System.out.println("jist.plugins"+"\t"+"*********************************");
        points = new Point3f[numPoints];
        Iterator<Point3f> i = fiberPoints.iterator();
        int j=0;
        while(i.hasNext()) {
            points[j] = i.next(); j++;
        }
        i=null;
        fiberPoints=null;
        return numPoints;
    }

    private void track(boolean addToEnd,FACTparameters dat, float  x, float  y, float  z, float last_vx, float last_vy, float last_vz,
                       float x_c,float y_c,float z_c) {
        float vx,vy,vz;

        Point3i last = null;
        Point3i last2 = null;
        Point3i last3 = null;
        
        int xr = floorinbound(x,dat.Nx-1);
        int yr = floorinbound(y,dat.Ny-1);
        int zr = floorinbound(z,dat.Nz-1);
        
        int pts =0;
//        boolean firstpt = true;
        while(pts<500) { //loop until hitting a stop condition - prevent super long fibers  	
           
        	pts++;
            
//            if (pts>499){System.out.println("jist.plugins"+"\t"+"Fiber has crossed " + pts + " points");}
            vx = dat.vecData[zr][yr][xr][0];
            vy = dat.vecData[zr][yr][xr][1];
            vz = dat.vecData[zr][yr][xr][2];
            double angle = vectAng(vx,vy,vz,last_vx,last_vy,last_vz);

            if(angle>pi2) {
                angle = Math.PI-angle;
                vx=-vx;vy=-vy;vz=-vz;
            }
            if(dat.faData[zr][yr][xr]<dat.stopFA) {
//            	System.out.println("jist.plugins"+"\t"+fiberPoints);
//            	System.out.println("jist.plugins"+"\t"+"***** End of Fiber - Low FA *****");
//            	System.out.println("jist.plugins"+"\t"+"*********************************");
                return; //too low fa
            }
            if(angle>dat.maxTurnAngle) {
//            	System.out.println("jist.plugins"+"\t"+fiberPoints);
//            	System.out.println("jist.plugins"+"\t"+"***** End of Fiber - Turn Ang *****");
//            	System.out.println("jist.plugins"+"\t"+"*********************************");
                return; //too high turn angle
            }
            if(angle<0){
                return; //No direction
            }

            
            /* Original method why stepping through the data*/
            /**********************************************
             // Scale v, vectors
             vx = (float)(vx/dat.resX)*stepSize;
             vy = (float)(vy/dat.resY)*stepSize;
             vz = (float)(vz/dat.resZ)*stepSize;
             while((x==Math.round(x_c))&&(y==Math.round(y_c))&&(z==Math.round(z_c))){
             x_c+=vx;
             y_c+=vy;
             z_c+=vz;
             }
             x = (char )Math.round(x_c);
             y = (char )Math.round(y_c);
             z = (char )Math.round(z_c);
             **********************************************/
            RAY DX, DY, DZ; // Distance to nearest X Y and Z planes

            if((float)(vx/dat.resX)>0){
            	DX = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),1,0,0,(float)Math.ceil(x_c+0.000001f));
            }else{
            	DX = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),1,0,0,(float)Math.floor(x_c-0.000001f));
            }

            if((float)(vy/dat.resY)>0){
            	DY = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,1,0,(float)Math.ceil(y_c+0.000001f));
            }else{
            	DY = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,1,0,(float)Math.floor(y_c-0.000001f));
            }

            if((float)(vz/dat.resZ)>0){
            	DZ = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,0,1,(float)Math.ceil(z_c+0.000001f));
            }else{
            	DZ = distInt(x_c,y_c,z_c,(float)(vx/dat.resX),(float)(vy/dat.resY),(float)(vz/dat.resZ),0,0,1,(float)Math.floor(z_c-0.000001f));
            }
            
            if((DX.dist<DY.dist)&&(DX.dist<DZ.dist)) {
                //X is minimum
                x_c=DX.pt.x;
                y_c=DX.pt.y;
                z_c=DX.pt.z;
                if(vx>0){
                	xr++;
                }else{
                	xr--;
                }
            }  else {
                if((DY.dist<DZ.dist)) {
                    // Y is minimum
                    x_c=DY.pt.x;
                    y_c=DY.pt.y;
                    z_c=DY.pt.z;
                    if(vy>0){
                    	yr++;
                    }else{
                    	yr--;
                    }
                } else {
                    //Z is minimum
                    x_c=DZ.pt.x;
                    y_c=DZ.pt.y;
                    z_c=DZ.pt.z;
                    
                    if(vz>0){
                    	zr++;
                    }else{
                    	zr--;
                    }
                }
            }

            Point3i thiscoord = new Point3i(xr,yr,zr);
            Point3f thispt = new Point3f(x_c,y_c,z_c);
            
            if(thiscoord.equals(last)){
            	return;
            }
            if(thiscoord.equals(last2)){
            	return;
            }
            if(thiscoord.equals(last3)){
            	return;
            }
            
            if((xr>=0)&&(yr>=0)&&(zr>=0)&&(xr<dat.Nx)&&(yr<dat.Ny)&&(zr<dat.Nz)) {
            	numPoints++;
                if(addToEnd){ fiberPoints.addFirst(thispt); }
                else{ fiberPoints.addLast(thispt); }
            } else {
                return; //out of bounds
            }

            last_vx = vx;
            last_vy = vy;
            last_vz = vz;
            
            last3 = last2;
            last2 = last;
            last = thiscoord;

        }

    }

    private RAY distInt(float x, float y, float z, float vx, float vy, float vz, float nx, float ny, float nz, float v) {
    	float nDotv =vx*nx+vy*ny+vz*nz;
        if(nDotv==0) {
            //Parallel
            return new RAY(x,y,z,Float.MAX_VALUE);
        }
//        float s =(nx*(v*nx-x)+ny*(v*ny-y)+nz*(v*nz-z))/nDotv;
        float s =(nx*(v-x)+ny*(v-y)+nz*(v-z))/nDotv;
        return new RAY((x+vx*s),(y+vy*s),(z+vz*s),s);
    }

    private double vectAng(double vx1, double vy1, double vz1, double vx2, double vy2, double vz2) {
        //normalize vectors
    	double dv1 = Math.sqrt(vx1*vx1+vy1*vy1+vz1*vz1);
        double dv2 = Math.sqrt(vx2*vx2+vy2*vy2+vz2*vz2);
        if((dv1==0)||(dv2==0))
            return -1;
        vx1=vx1/dv1;
        vy1=vy1/dv1;
        vz1=vz1/dv1;
        vx2=vx2/dv2;
        vy2=vy2/dv2;
        vz2=vz2/dv2;
        //finite precision can result in normalized sums greater than +-1. catch these with the min
        return Math.acos(Math.max(-1.0f,Math.min(vx1*vx2+vy1*vy2+vz1*vz2,1.0f)));
    }
    
    public CurvePath toCurvePath(){
    	CurvePath cp=new CurvePath();
    	for(Point3f pt : points){
    		cp.add(pt);
    	}
    	return cp;
    }
    
    public Fiber toFiber(){
    	XYZ[] chain = new XYZ[points.length];
    	int i=0;
    	for(Point3f pt : points){
    		chain[i]=new XYZ(pt.x,pt.y,pt.z);
    		i++;
    	}
    	Fiber f = new Fiber(chain);
    	return f;
    }

    public void addVxToLookupTable(Map map,int myId,char  Nx, char  Ny) {
        Iterator i = fiberPoints.iterator();
        while(i.hasNext()) {
            map.put(new Integer(((cPT)i.next()).cor2ind(Nx,Ny)),new Integer(myId));
        }
    }

    boolean containsPoints(cPT pts[]) {
        for(int i=0;i<pts.length;i++)
            for(int k=0;k<points.length;k++)
                if(points[k].equals(pts[i]))
                    return true;
        return false;
    }

    public String toString() {
        String out="";
        for(int k=0;k<points.length;k++)
            out+=points[k]+",";
        return out;
    }
    
    //int max is the upper bound
    private int roundinbound(float f, int max){
    	int ff = Math.round(f);
    	if(ff<0){ ff=0; }
    	else if(ff>max){ ff=max; }
    	return ff;
    }
   //int max is the upper bound
    private int floorinbound(float f, int max){
    	int ff = (int)f;
    	if(ff<0){ ff=0; }
    	else if(ff>max){ ff=max; }
    	return ff;
    }
    private int ceilinbound(float f, int max){
    	int ff = (int)Math.ceil(f);
    	if(ff<0){ ff=0; }
    	else if(ff>max){ ff=max; }
    	return ff;
    }
    
}

