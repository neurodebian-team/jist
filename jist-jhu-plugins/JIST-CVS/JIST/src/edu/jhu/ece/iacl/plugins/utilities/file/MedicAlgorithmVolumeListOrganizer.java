package edu.jhu.ece.iacl.plugins.utilities.file;

import java.io.File;
import java.util.List;
import java.util.TreeSet;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;


public class MedicAlgorithmVolumeListOrganizer extends ProcessingAlgorithm{
	ParamVolumeCollection in, out;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(in=new ParamVolumeCollection("Volumes In"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.File");
		inputParams.setLabel("Volume List Organizer");
		inputParams.setName("Volume_List_Organizer");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(out=new ParamVolumeCollection("Volumes Out"));
	}


	protected void execute(CalculationMonitor monitor) {
		List<File> filelist = in.getFileList();
		TreeSet<String> fileset = new TreeSet<String>();
		for(File f: filelist){
			fileset.add(f.getAbsolutePath());
		}

		int i=0;
		for(String s : fileset){
			out.set(i, new File(s));
			i++;
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"Input List:");
		System.out.println(getClass().getCanonicalName()+"\t"+in.getFileList());
		System.out.println(getClass().getCanonicalName()+"\t"+"Output List:");
		System.out.println(getClass().getCanonicalName()+"\t"+out.getFileList());
		System.out.println(getClass().getCanonicalName()+"\t"+"Finished!");
	}
}
