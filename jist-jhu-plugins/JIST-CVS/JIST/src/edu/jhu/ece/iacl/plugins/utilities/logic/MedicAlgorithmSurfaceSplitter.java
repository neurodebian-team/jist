/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.logic;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmSurfaceSplitter extends ProcessingAlgorithm{
	private ParamSurface inSurf;
	private ParamSurface outSurf;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(inSurf=new ParamSurface("Surface"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Surface Splitter");
		inputParams.setName("Surface_Splitter");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outSurf=new ParamSurface("Surface"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		outSurf.setValue(inSurf.getValue());
	}
}
