package edu.jhu.ece.iacl.plugins.dti;

import imaging.SchemeV1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.thoughtworks.xstream.XStream;

import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.StringArrayXMLReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;


public class DWIDefineCaminoScheme extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamFile bvaluesTable;		// .b file with a list of b-values
	private ParamFile gradsTable;		// .grad or .dpf file with a list of gradient directions

	private ParamFile bigDelta; 
	private ParamFile smallDelta;
	private ParamFile te;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFile SchemeOut;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Creates a scheme definition file.";
	private static final String longDescription = "The scheme file represents the diffusion weighting experiment that has been carried out. Reference images are indicated by b=0 s/mm2, and may occur at any time during the sequence.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("Camino");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Define Camino Scheme");	
		inputParams.setName("Define_Camino_Scheme");	


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Computer Science Department - University College London");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions (normalized to 1)",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(bvaluesTable=new ParamFile("Table of b-values (s/mm^2)",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(bigDelta=new ParamFile("big delta (diffusion time - s: either scalar or list)",new FileExtensionFilter(new String[]{"bigDelta","txt"})));
		inputParams.add(smallDelta=new ParamFile("small delta (gradient time - s: either scalar or list)",new FileExtensionFilter(new String[]{"smallDelta","txt"})));
		inputParams.add(te=new ParamFile("TE (echo time - s: either scalar or list)",new FileExtensionFilter(new String[]{"te","txt"})));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		outputParams.add(SchemeOut=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"schemeV1"})));
	}


	private double[][] readTextFile(File file) {
		double [][]x=null;

		try {
			x  = new TextFileReader(file).parseDoubleFile();
		} catch (IOException e) { 

			throw new RuntimeException("Define Scheme: Unable to parse double file: "+file.getName()+" at "+file.getAbsolutePath());
		}
		return x;
	}


	public static String getFileNameWithoutExtension(String fileName) 
	{        
		File tmpFile = new File(fileName);       
		tmpFile.getName();        
		int whereDot = tmpFile.getName().lastIndexOf('.');
		if (0 < whereDot && whereDot <= tmpFile.getName().length() - 2 ) 
		{            
			return tmpFile.getName().substring(0, whereDot); 
			//extension = filename.substring(whereDot+1);   
		}       
		return ""; 
	}


	protected void execute(CalculationMonitor monitor) {
		StringArrayXMLReaderWriter saxml = new StringArrayXMLReaderWriter();

		double [][]grads=readTextFile(gradsTable.getValue());
		// If there are 4 columns in the gradient table, remove the 1st column (indecies)
		if(grads[0].length==4) {
			double [][]g2 = new double[grads.length][3];
			for(int i=0;i<grads.length;i++) 
				for(int j=0;j<3;j++)
					g2[i][j]=grads[i][j+1];
			grads=g2;
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"Grads loaded:"+ grads.length);System.out.flush();
		double [][]bs=readTextFile(bvaluesTable.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"B-vals loaded:"+ bs.length);System.out.flush();
		double [][]smD = readTextFile(smallDelta.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"smD loaded:"+ smD.length);System.out.flush();
		double [][]bgD = readTextFile(bigDelta.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"bgD loaded:"+ bgD.length);System.out.flush();
		double [][]teL = readTextFile(te.getValue());
		System.out.println(getClass().getCanonicalName()+"\t"+"teL loaded:"+ teL.length);System.out.flush();
		double []bval = new double[grads.length];
		double []sdval= new double[grads.length];
		double []bdval= new double[grads.length];
		double []teval= new double[grads.length];
		double []modG = new double[grads.length];
		double []halfP90Init =new double[grads.length];
		boolean []ignored = new boolean[grads.length];
		for(int i=0;i<grads.length;i++) {			
			/* s/m^2 */ bval[i]=((bs.length>i)?bs[i][0]:bs[0][0]) *1e6; /*s/mm2 -> s/m2 */
			/* s */ sdval[i]=(smD.length>i)?smD[i][0]:smD[0][0];
			/* s */ bdval[i]=(bgD.length>i)?bgD[i][0]:bgD[0][0];
			/* s */ teval[i]=(teL.length>i)?teL[i][0]:teL[0][0];

			/*  T / m */ modG[i]= Math.sqrt(bval[i]/(bdval[i]-sdval[i]/3))/sdval[i]/SchemeV1.GAMMA;
			
			// Ignore 100,100,100
			ignored[i]=(grads[i][0]==100);
				

			halfP90Init[i]=0;
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"copy done");System.out.flush();

		SchemeV1 DTIscheme = new SchemeV1(
				grads, modG,
				bdval, sdval, teval,
				halfP90Init,
				ignored);

		System.out.println(getClass().getCanonicalName()+"\t"+"Scheme created");System.out.flush();
		File outDir = this.getOutputDirectory();//MipavController.getDefaultWorkingDirectory();

		String filename = outDir.toString() 
			+ File.separatorChar + getFileNameWithoutExtension(edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(gradsTable.getValue().getName())) + ".schemeV1";

		System.out.println(getClass().getCanonicalName()+"\t"+"Bennett: "+outDir.toString());
		System.out.println(getClass().getCanonicalName()+"\t"+"Bennett: "+filename);
		System.out.flush();
		File outFileObj = new File(filename);
		System.out.println(getClass().getCanonicalName()+"\t"+"file created");System.out.flush();
		FileOutputStream fos = null;

		XStream xstream = new XStream();
		xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
		try {
			ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter(outFileObj));
			out.writeObject(DTIscheme);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		SchemeOut.setValue(filename);
	}
}
