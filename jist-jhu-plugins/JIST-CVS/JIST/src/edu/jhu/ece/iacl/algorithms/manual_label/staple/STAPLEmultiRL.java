package edu.jhu.ece.iacl.algorithms.manual_label.staple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

import edu.jhu.ece.iacl.jist.structures.geom.GridPt;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;

public class STAPLEmultiRL extends STAPLEmulti2{
	
	protected Truth[][][] truth;
	protected int RLold=0, RLnew=1;
	protected ArrayList<int[]> voxels = new ArrayList<int[]>();

	public STAPLEmultiRL() {
		super();
	}
	public STAPLEmultiRL(int[][][][] img) {
		super(img);
	}
	public STAPLEmultiRL(float[][][][] img) {
		super(img);
	}
	public void setTruth(int i, int j, int k, int[] inds, float[]t){
		truth[i][j][k]=new Truth(inds.length);
		for(int ii=0; ii<inds.length; ii++){
			truth[i][j][k].setIndex(ii, inds[ii]);
		}
		truth[i][j][k].setTruth(t);
	}
	public void setTruthIndex(int i, int j, int k, int loc, int val){
		truth[i][j][k].setIndex(loc,val);
	}

	public void setTruth(int i, int j, int k, float[] p){
		truth[i][j][k].setTruth(p);
	}
	public float[] getTruth(int i, int j, int k){
		return truth[i][j][k].getTruth();
	}
	public int[] getTruthInds(int i, int j, int k){
		return truth[i][j][k].getIndex();
	}
	public int getTruthLength(int i, int j, int k){
		return truth[i][j][k].getIndex().length;
	}
	
	public ImageData writeProb(int n){

		ImageData tfile = new ImageDataMipav("TruthEstimate_"+labels.get(n),rows,cols,slices);
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					float[] a=truth[i][j][k].getTruth();
					int[] ind=truth[i][j][k].getIndex();
					for(int m=0; m<a.length; m++) {
						if(labels.get(n)==ind[m]) {
							tfile.set(i,j,k,a[m]);
							break;
						}
					}
				}
			}
		}
		return tfile;
	}
	public float[][][] writeProbArray(int n){

		float[][][] out = new float[rows][cols][slices];
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					float[] a=truth[i][j][k].getTruth();
					int[] ind=truth[i][j][k].getIndex();
					for(int m=0; m<a.length; m++) {
						if(n==ind[m]) {
							out[i][j][k]=a[m];
							break;
						}
					}
				}
			}
		}
		return out;
	}
	public int[][][] writeTruthIndArray(){

		int[][][] out = new int[rows][cols][slices];
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] ind=truth[i][j][k].getIndex();
					out[i][j][k]=ind.length;
				}
			}
		}
		return out;
	}
	
	public ImageData getHardSeg(){
		ImageDataInt hardseg = new ImageDataInt(rows,cols,slices);
		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				for (int k=0; k<slices; k++) {
					hardseg.set(i,j,k, labels.get(truth[i][j][k].getHardInd()));
				}
			}			
		}
		return new ImageDataInt(hardseg);
	}
	
	public void start(){
		if(initType.equals("Performance")){
			EStep();
			if(beta>0){
				mrfRL();
			}
			mStepNext = true;
		}else{
			mStepNext = false;
		}
		iters = 0;
		normtrace = 0;
	}
	
	/**
	 * A version that iterates using the "smart" methods below
	 */
	public void iterate2(){
		start();
		keepgoing = true;
		while(keepgoing){
			keepgoing = proceed();
		}
	}
	
	/**
	 * Performs the next step of the STAPLE iterations.
	 * @return false if STAPLE has converged
	 */
	public boolean proceed(){
		if(mStepNext){
			if(keepgoing && iters<maxiters){
				
				MStep();
				mStepNext = false;
				
				float ntrace = pl.normalizedTrace();
				if (Float.isNaN(ntrace)) keepgoing=false;
				if(Math.abs(ntrace-normtrace)<eps){
					System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
					System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
					System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
					System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
					keepgoing=false;
				}else{
					System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
					System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
					System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
					System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
					System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
				}
				System.out.println("jist.plugins"+"\t"+"*****************");
				iters++;
				normtrace=ntrace;
				
				return keepgoing;
				
			}else{
				return false;
			}
		}else{
			EStep();
			mStepNext = true;
			if(beta>0){
				mrfRL();
			}
			return true;
		}		
	}
	
	private void createT(){
		t = new int[rows][cols][slices][raters];
		truth = new Truth[rows][cols][slices];
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					BitSet mindex=new BitSet(labelSize);
					for(int l=0; l<raters; l++){						
						t[i][j][k][l] = getIndex(labels,imagesArray[i][j][k][l]);
						mindex.set(labels.indexOf(imagesArray[i][j][k][l]),true);
						if (!(t[i][j][k][l]>-1))
							System.err.println("jist.plugins"+"Could not find label!");
					}
					truth[i][j][k] = new Truth(mindex.cardinality());
					int c=0;
					for(int m=0; m<labelSize; m++){
						if (mindex.get(m)) {
							truth[i][j][k].setIndex(c++,m);
						}
					}
					if(c!=1) {
						voxels.add(new int[]{i,j,k});
						truth[i][j][k].sortIndex();
					}
					else
						truth[i][j][k].setTruth(new float[]{1f});
				}
			}
		}
		imagesArray=null;
		imagesArrayFloat=null;
	}
	
	public void initialize(){
		
		if(initType.equals("Truth")){
			System.out.println("jist.plugins"+"\t"+"Not Implemented Yet");
		}else{
			System.out.println("jist.plugins"+"\t"+"Initializing Performance Levels");
			float init = 0.9999f;
			try{
				findLabels();
				System.out.println("jist.plugins"+"\t"+"Labels Found: " + labels.size());
				pl = new PerformanceLevel(labels.size(), raters);
				pl.initialize(init);
				createT();
			}catch(Exception e){
				e.printStackTrace();
			}
			if(printPerformance){
				System.out.println("\n\n"+pl.toString()+"\n\n");
			}
		}
		System.out.println("jist.plugins"+"\t"+"*****************");
	}
	
	public void iterate(){		
		
		if(initType.equals("Performance")){
			EStep();
			if(beta>0){
				mrfRL();
			}
		}
		iters = 0;		
		float ntrace = 0;
		
		while(keepgoing && iters<maxiters){
			MStep();
			EStep();

			if(beta>0){
				mrfRL();
			}
			ntrace = pl.normalizedTrace();
			if (Float.isNaN(ntrace)) keepgoing=false;
			if(Math.abs(ntrace-normtrace)<eps){
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
			}else{
				System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
				System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
				System.out.println("jist.plugins"+"\t"+"Current Trace: " +ntrace);
				System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
				System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
			}
			System.out.println("jist.plugins"+"\t"+"*****************");
			iters++;
			normtrace=ntrace;
		}
	}
	
	private void MStep() {
		float[] totsum = new float[labelSize];
		pl.clear();
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] mindex=truth[i][j][k].getIndex();
					float[] truthArray=truth[i][j][k].getTruth();
					for(int m=0; m<mindex.length; m++){
						float tr=truthArray[m];
						totsum[mindex[m]]+=tr;
						for(int l=0; l<raters; l++){
							pl.set(l, t[i][j][k][l], mindex[m], pl.get(l, t[i][j][k][l], mindex[m])+tr);
						}
					}
				}
			}
		}

		for(int n=0; n<labelSize; n++){
			pl.divideByTots(n, totsum[n]);
		}
		if(printPerformance){
			System.out.println("\n\n"+pl.toString()+"\n\n");
		}
	}
	
	private void EStep() {	
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				for(int k=0; k<slices; k++){
					int[] mindex=truth[i][j][k].getIndex();
					int lSize=mindex.length;
					float[] a = new float[lSize];
					for(int m=0; m<lSize; m++) {
						a[m]=priorArray[mindex[m]];
					}
		
					for(int m=0; m<lSize; m++){
						for(int l=0; l<raters; l++) {
							a[m]*=pl.get(l, t[i][j][k][l], mindex[m]);
						}
					}
					truth[i][j][k].setTruth(a);
				}
			}
		}
	}

	private void mrfRL(){
		System.out.println("Apply MRF, RL optimization...");
		int it=0;
		float initChange=1f, maxChange=1f;
		long t1=System.currentTimeMillis();
//		ArrayList<int[]> voxels2 = new ArrayList<int[]>();
		while(it<maxMRFiters && maxChange>initChange/2){
			System.out.print("Iter "+it+" - ");
			RLnew=1-RLnew;
			RLold=1-RLold;
			maxChange=0f;
			GridPt[] nbrs;
//			if(it==0){
				for (int[] v : voxels) {
					int i=v[0], j=v[1], k=v[2];
					Truth tVoxel = truth[i][j][k];
					nbrs = getNeighbors(i,j,k);		//get neighbors
					float[] problist=tVoxel.getOldTruth().clone();
					for(int n=0; n<nbrs.length; n++){
						if(nbrs[n]!=null){
							Truth nVoxel=truth[nbrs[n].x][nbrs[n].y][nbrs[n].z];
							float[] nVoxelTruth=nVoxel.getOldTruth();
							for(int l=0; l<nVoxelTruth.length; l++){	//loop through each label
								int nbrlabind = tVoxel.findIndexLoc(nVoxel.getIndex(l));	
								if (nbrlabind>=0) {
									problist[nbrlabind]+=beta*nVoxelTruth[l];
								}
							}
						}
					}
					//normalize and set newly computed label probabilities at this location
					float diff = tVoxel.setTruthGetDiff(problist);
					if (diff>maxChange)
						maxChange=diff;
//					if (diff>1e-3) {
//						voxels2.add(new int[]{i,j,k});
//					}
				}
//			}
//			else {
//				for (int[] v : voxels2) {
//					int i=v[0], j=v[1], k=v[2];
//					Truth tVoxel = truth[i][j][k];
//					nbrs = getNeighbors(i,j,k);		//get neighbors
//					float[] problist=tVoxel.getOldTruth().clone();
//					for(int n=0; n<nbrs.length; n++){
//						if(nbrs[n]!=null){
//							Truth nVoxel=truth[nbrs[n].x][nbrs[n].y][nbrs[n].z];
//							float[] nVoxelTruth=nVoxel.getOldTruth();
//							for(int l=0; l<nVoxelTruth.length; l++){	//loop through each label
//								int nbrlabind = tVoxel.findIndexLoc(nVoxel.getIndex(l));	
//								if (nbrlabind>=0) {
//									problist[nbrlabind]+=beta*nVoxelTruth[l];
//								}
//							}
//						}
//					}
//					//normalize and set newly computed label probabilities at this location
//					float diff = tVoxel.setTruthGetDiff(problist);
//					if (diff>maxChange)
//						maxChange=diff;
//				}
//			}
			System.out.println("Max Change: "+maxChange);
			if(it++==0)
				initChange=maxChange;
		}
		long t2=System.currentTimeMillis();
		System.out.println("TIME: "+(t2-t1));
		System.out.flush();
	}
	
	private class Truth{
		private int[] index;
		private float[][] lTruth;
		
		public Truth(int n){
			index=new int[n];
			lTruth=new float[2][n];
		}
		
		public int findIndexLoc(int n) {
			return Arrays.binarySearch(index, n);
		}

		public int[] getIndex() {
			return index;
		}
		
		public int getIndex(int n) {
			return index[n];
		}
		
		public float[] getOldTruth() {
			return lTruth[RLold];
		}
		
		public int getHardInd() {
			return index[sort()];
		}
		
		public void setIndex(int loc, int n) {
			index[loc]=n;
		}
		
		public float setTruthGetDiff(float[] t) {
			float sum = 0f;
			for(int l=0; l<t.length; l++){
				sum+=t[l];
			}
			float diff = 0f;
			for(int l=0; l<t.length; l++){
				lTruth[RLnew][l]=t[l]/sum;
				diff+=Math.abs(lTruth[RLnew][l]-lTruth[RLold][l]);
			}
			return diff;
		}
		
		public void setTruth(float[] t) {
			float sum = 0f;
			for(int l=0; l<t.length; l++){
				sum+=t[l];
			}
			for(int l=0; l<t.length; l++){
				lTruth[RLnew][l]=t[l]/sum;
			}
		}
		
		public float[] getTruth() {
			return lTruth[RLnew];
		}
		
		public int sort() {
			int max = 0;
			for (int j=1; j<lTruth[RLnew].length; j++){
				if (lTruth[RLnew][j]>lTruth[RLnew][max])
					max = j;
			}
			return max;
		}
		
		public void sortIndex() {
			Arrays.sort(index);
		}
	}
}
