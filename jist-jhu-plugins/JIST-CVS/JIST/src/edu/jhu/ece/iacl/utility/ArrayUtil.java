package edu.jhu.ece.iacl.utility;

import java.util.List;

import Jama.Matrix;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;

/**
*
*  This class computes various basic functionality for dealing with arrays.
*	<p> 
*	Includes indexing, some arithmetic, normalization, reshaping, conversion to Jama Matrix or JIST ImageData
*
*	@version    July 28, 2010
*	@author     John Bogovic
*/

public class ArrayUtil {


	public static final int indexOf(int x, int[] list){
		for(int i=0; i<list.length; i++){
			if(x==list[i])
				return i;
		}
		return -1;
	}
	public static final int indexOf(float x, float[] list){
		for(int i=0; i<list.length; i++){
			if(x==list[i])
				return i;
		}
		return -1;
	}
	

	
	/**
	 * 
	 * @param x 
	 * @param list
	 * @return True if x is contained in the list
 	 */
	public static final boolean contains(int[] list, int x){
		return (ArrayUtil.indexOf(x,list)>-1);
	}
	/**
	 * 
	 * @param x 
	 * @param list
	 * @return True if x is contained in the list
 	 */
	public static final boolean contains(float[] list, float x){
		return (ArrayUtil.indexOf(x,list)>-1);
	}
	
	/**
	 * If list contains x, returns a new array without x.
	 * Otherwise returns list (not a clone)
	 * @param list
	 * @param el
	 * @return
	 */
	public static final int[] removeElement(int[] list, int x){
		if(ArrayUtil.contains(list,x)){
			int[] out = new int[list.length-1];
			int j=0;
			for(int i=0; i<list.length; i++){
				if(list[i]!=x){
					out[j]=list[i];
					j++;
				}
			}
			return out;
		}else{
			return list;
		}
	}
	
	public static final float[] removeIndex(float[] in, int ind){
		float[] out = new float[in.length-1];
		int j=0;
		for(int i=0; i<in.length; i++){
			if(i!=ind){
				out[j]=in[i];
				j++;
			}
		}
		return out;
	}
	public static final int[] removeIndex(int[] in, int ind){
		int[] out = new int[in.length-1];
		int j=0;
		for(int i=0; i<in.length; i++){
			if(i!=ind){
				out[j]=in[i];
				j++;
			}
		}
		return out;
	}
	
	
	public static final float[] normalize(float[] in){
		float sum = 0;
		float[] out = new float[in.length];
		for(int i=0; i<in.length; i++) {	sum+=in[i]; 		}
		for(int i=0; i<in.length; i++) {	out[i]=in[i]/sum; 	}
		return out;
	}
	public static final double[] normalize(double[] in){
		double sum = 0;
		double[] out = new double[in.length];
		for(int i=0; i<in.length; i++) {	sum+=in[i]; 		}
		for(int i=0; i<in.length; i++) {	out[i]=in[i]/sum; 	}
		return out;
	}
	
	public static final float[] normalizeTo(float[] in, float normsum){
		float sum = 0;
		float[] out = new float[in.length];
		for(int i=0; i<in.length; i++) {	sum+=in[i]; 		}
		for(int i=0; i<in.length; i++) {	out[i]=normsum*in[i]/sum; 	}
		return out;
	}
	public static final double[] normalizeTo(double[] in, double normsum){
		double sum = 0;
		double[] out = new double[in.length];
		for(int i=0; i<in.length; i++) {	sum+=in[i]; 		}
		for(int i=0; i<in.length; i++) {	out[i]=normsum*in[i]/sum; 	}
		return out;
	}
	public static final float sum(float[] in){
		float sum = 0;
		for(int i=0; i<in.length; i++) {	sum+=in[i]; 		}
		return sum;
	}
	public static final double sum(double[] in){
		double sum = 0;
		for(int i=0; i<in.length; i++) {	sum+=in[i]; 		}
		return sum;
	}

	public static final String printArray(float[] in){
		String out = "";
		for(int i=0; i<in.length; i++){
			out += in[i] +" ";
		}
		return out;
	}

	public static final String printArray(double[] in){
		String out = "";
		for(int i=0; i<in.length; i++){
			out += in[i] +" ";
		}
		return out;
	}

	public static final String printArray(int[] in){
		String out = "";
		for(int i=0; i<in.length; i++){
			out += in[i] +" ";
		}
		return out;
	}
	
	public static final String printArray(float[][] in){
		String out = "";
		for(int i=0; i<in.length; i++){
			for(int j=0; j<in[0].length; j++){
				out += in[i][j] +" ";
			}
			out +="\n";
		}
		return out;
	}
	
	public static final String printArray(int[][] in){
		String out = "";
		for(int i=0; i<in.length; i++){
			for(int j=0; j<in[0].length; j++){
				out += in[i][j] +" ";
			}
			out +="\n";
		}
		return out;
	}
	
	public static final String printArray(double[][] in){
		String out = "";
		for(int i=0; i<in.length; i++){
			for(int j=0; j<in[0].length; j++){
				out += in[i][j] +" ";
			}
			out +="\n";
		}
		return out;
	}
	
	public static final String printsubArray(byte[][][] in, int x, int y){
		String out = "";
		for(int i=0; i<in.length; i++){
			out += in[x][y][i] +" ";
		}
		return out;
	}
	
	/**
	 * 
	 * @param in Input array
	 * @param x
	 * @param y
	 * @param dim The dimension to vary
	 * @return
	 */
	public static final byte[] get1dSubArray(byte[][][] in, int x, int y, int dim){
		byte[] out = null;
		int len = -1;
		if(dim==0){
			len = in.length;
			out = new byte[len];
			for(int i=0; i<len; i++){
				out[i]=in[i][x][y];
			}
		}else if(dim==1){
			len = in[0].length;
			out = new byte[len];
			for(int i=0; i<len; i++){
				out[i]=in[x][i][y];
			}
		}else if(dim==2){
			len = in[0][0].length;
			out = new byte[len];
			for(int i=0; i<len; i++){
				out[i]=in[x][y][i];
			}
		}else{
			System.out.println("Invalid dim");
		}
		return out;
	}
	
	/**
	 * 
	 * @param in Input array
	 * @param x
	 * @param y
	 * @param dim The dimension to vary
	 * @return
	 */
	public static final int[] get1dSubArray(int[][][] in, int x, int y, int dim){
		int[] out = null;
		int len = -1;
		if(dim==0){
			len = in.length;
			out = new int[len];
			for(int i=0; i<len; i++){
				out[i]=in[i][x][y];
			}
		}else if(dim==1){
			len = in[0].length;
			out = new int[len];
			for(int i=0; i<len; i++){
				out[i]=in[x][i][y];
			}
		}else if(dim==2){
			len = in[0][0].length;
			out = new int[len];
			for(int i=0; i<len; i++){
				out[i]=in[x][y][i];
			}
		}else{
			System.out.println("Invalid dim");
		}
		return out;
	}
	
	/**
	 * 
	 * @param in Input array
	 * @param x
	 * @param y
	 * @param dim The dimension to vary
	 * @return
	 */
	public static final float[] get1dSubArray(float[][][] in, int x, int y, int dim){
		float[] out = null;
		int len = -1;
		if(dim==0){
			len = in.length;
			out = new float[len];
			for(int i=0; i<len; i++){
				out[i]=in[i][x][y];
			}
		}else if(dim==1){
			len = in[0].length;
			out = new float[len];
			for(int i=0; i<len; i++){
				out[i]=in[x][i][y];
			}
		}else if(dim==2){
			len = in[0][0].length;
			out = new float[len];
			for(int i=0; i<len; i++){
				out[i]=in[x][y][i];
			}
		}else{
			System.out.println("Invalid dim");
		}
		return out;
	}
	
	/**
	 * 
	 * @param in
	 * @param x 
	 * @param dim The dimension to hold constant
	 * @return
	 */
	public static final byte[][] get2dSubArray(byte[][][] in, int x, int dim){
		byte[][] out = null;
		int dim1 = -1;
		int dim2 = -1;
		if(dim==0){
			dim1 = in[0].length;
			dim2 = in[0][0].length;
			out = new byte[dim1][dim2];
			for(int i=0; i<dim1; i++) for(int j=0; j<dim2; j++){
				out[i][j] = in[x][i][j];
			}
		}else if(dim==2){
			dim1 = in.length;
			dim2 = in[0][0].length;
			out = new byte[dim1][dim2];
			for(int i=0; i<dim1; i++) for(int j=0; j<dim2; j++){
				out[i][j] = in[i][x][j];
			}
		}else if(dim==3){
			dim1 = in.length;
			dim2 = in[0].length;
			out = new byte[dim1][dim2];
			for(int i=0; i<dim1; i++) for(int j=0; j<dim2; j++){
				out[i][j] = in[i][j][x];
			}
		}else{
			System.out.println("Invalid dim");
		}
		return out;
	}
	
	public static final float[] reshape1D(float[][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		float[] out = new float[nx*ny*nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				out[i]=in[x][y][z];
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[i]=in[x][y][z];
				i++;
			}
		}
		return out;
	}
	
	public static final int[] reshape1D(int[][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		int[] out = new int[nx*ny*nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				out[i]=in[x][y][z];
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[i]=in[x][y][z];
				i++;
			}
		}
		return out;
	}
	
	public static final double[] reshape1D(double[][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		double[] out = new double[nx*ny*nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				out[i]=in[x][y][z];
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[i]=in[x][y][z];
				i++;
			}
		}
		return out;
	}
	
	public static final float[][][] reshape3D(float[] in, int nx, int ny, int nz, boolean rowwise){
		float[][][] out = new float[nx][ny][nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				out[x][y][z]=in[i];
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y][z]=in[i];
				i++;
			}
		}
		return out;
	}
	public static final int[][][] reshape3D(int[] in, int nx, int ny, int nz, boolean rowwise){
		int[][][] out = new int[nx][ny][nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				out[x][y][z]=in[i];
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y][z]=in[i];
				i++;
			}
		}
		return out;
	}
	public static final double[][][] reshape3D(double[] in, int nx, int ny, int nz, boolean rowwise){
		double[][][] out = new double[nx][ny][nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				out[x][y][z]=in[i];
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y][z]=in[i];
				i++;
			}
		}
		return out;
	}
	
	public static final Matrix toVector(double[][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		Matrix vecout = new Matrix(nx*ny*nz,1);
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				vecout.set(i,0,in[x][y][z]);
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				vecout.set(i,0,in[x][y][z]);
				i++;
			}
		}
		return vecout;
	}
	public static final Matrix toVector(int[][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		Matrix vecout = new Matrix(nx*ny*nz,1);
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				vecout.set(i,0,in[x][y][z]);
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				vecout.set(i,0,in[x][y][z]);
				i++;
			}
		}
		return vecout;
	}
	public static final Matrix toVector(float[][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		Matrix vecout = new Matrix(nx*ny*nz,1);
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				vecout.set(i,0,in[x][y][z]);
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				vecout.set(i,0,in[x][y][z]);
				i++;
			}
		}
		return vecout;
	}
	
	public static final Matrix toMatrix(double[][][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		int nc = in[0][0][0].length;
		Matrix vecout = new Matrix(nx*ny*nz,nc);
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				for(int n=0; n<nc; n++){
					vecout.set(i,n,in[x][y][z][n]);
				}
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				for(int n=0; n<nc; n++){
					vecout.set(i,n,in[x][y][z][n]);
				}
				i++;
			}
		}
		return vecout;
	}
	public static final Matrix toMatrix(int[][][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		int nc = in[0][0][0].length;
		Matrix vecout = new Matrix(nx*ny*nz,nc);
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				for(int n=0; n<nc; n++){
					vecout.set(i,n,in[x][y][z][n]);
				}
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				for(int n=0; n<nc; n++){
					vecout.set(i,n,in[x][y][z][n]);
				}
				i++;
			}
		}
		return vecout;
	}
	public static final Matrix toMatrix(float[][][][] in, boolean rowwise){
		int nx=in.length;
		int ny=in[0].length;
		int nz=in[0][0].length;
		int nc = in[0][0][0].length;
		Matrix vecout = new Matrix(nx*ny*nz,nc);
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				for(int n=0; n<nc; n++){
					vecout.set(i,n,in[x][y][z][n]);
				}
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				for(int n=0; n<nc; n++){
					vecout.set(i,n,in[x][y][z][n]);
				}
				i++;
			}
		}
		return vecout;
	}
	
	public static float[][][][] fromMatrixFloat(Matrix m, int nx, int ny, int nz, int nc, boolean rowwise){
		float[][][][] out = new float[nx][ny][nz][nc];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				for(int n=0; n<nc; n++){
					out[x][y][z][n] = (float)m.get(i, n);
				}
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				for(int n=0; n<nc; n++){
					out[x][y][z][n] = (float)m.get(i, n);
				}
				i++;
			}
		}
		return out;
	}
	public static int[][][][] fromMatrixInt(Matrix m, int nx, int ny, int nz, int nc, boolean rowwise){
		int[][][][] out = new int[nx][ny][nz][nc];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				for(int n=0; n<nc; n++){
					out[x][y][z][n] = (int)m.get(i, n);
				}
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				for(int n=0; n<nc; n++){
					out[x][y][z][n] = (int)m.get(i, n);
				}
				i++;
			}
		}
		return out;
	}
	public static double[][][][] fromMatrixDouble(Matrix m, int nx, int ny, int nz, int nc, boolean rowwise){
		double[][][][] out = new double[nx][ny][nz][nc];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
				for(int n=0; n<nc; n++){
					out[x][y][z][n] = m.get(i, n);
				}
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				for(int n=0; n<nc; n++){
					out[x][y][z][n] = m.get(i, n);
				}
				i++;
			}
		}
		return out;
	}
	
	public static float[][][] fromVectorFloat(Matrix m, int nx, int ny, int nz, boolean rowwise){
		float[][][] out = new float[nx][ny][nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
					out[x][y][z] = (float)m.get(i,0);
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y][z] = (float)m.get(i, 0);
				i++;
			}
		}
		return out;
	}
	public static int[][][] fromVectorInt(Matrix m, int nx, int ny, int nz, boolean rowwise){
		int[][][] out = new int[nx][ny][nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
					out[x][y][z] =(int)m.get(i,0);
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y][z] = (int)m.get(i, 0);
				i++;
			}
		}
		return out;
	}
	public static double[][][] fromVectorDouble(Matrix m, int nx, int ny, int nz, boolean rowwise){
		double[][][] out = new double[nx][ny][nz];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) for(int z=0; z<nz; z++) {
					out[x][y][z] = m.get(i,0);
				i++;
			}
		}else{
			for(int z=0; z<nz; z++)  for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y][z] = m.get(i, 0);
				i++;
			}
		}
		return out;
	}
	
	public static float[][] fromVectorFloat(Matrix m, int nx, int ny, boolean rowwise){
		float[][] out = new float[nx][ny];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++){
					out[x][y]=  (float)m.get(i,0);
				i++;
			}
		}else{
			for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y] = (float)m.get(i, 0);
				i++;
			}
		}
		return out;
	}
	public static int[][] fromVectorInt(Matrix m, int nx, int ny, boolean rowwise){
		int[][] out = new int[nx][ny];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) {
					out[x][y] =(int)m.get(i,0);
				i++;
			}
		}else{
			for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y] = (int)m.get(i, 0);
				i++;
			}
		}
		return out;
	}
	public static double[][] fromVectorDouble(Matrix m, int nx, int ny, boolean rowwise){
		double[][] out = new double[nx][ny];
		int i=0;
		if(rowwise){
			for(int x=0; x<nx; x++)  for(int y=0; y<ny; y++) {
					out[x][y] = m.get(i,0);
				i++;
			}
		}else{
			for(int y=0; y<ny; y++) for(int x=0; x<nx; x++){
				out[x][y] = m.get(i, 0);
				i++;
			}
		}
		return out;
	}
	
	public static float[] fromVectorFloat(Matrix m){
		int nx = m.getRowDimension();
		int ny = m.getColumnDimension();
		float[] out;
		if(nx==1){
			out = new float[ny];
			for(int i=0; i<ny; i++){
				out[i] = (float)m.get(0, i);
			}
		}else if(ny==1){
			out = new float[nx];
			for(int i=0; i<nx; i++){
				out[i] = (float)m.get(i, 0);
			}
		}else{
			System.err.println("fromVector method requires that either row or column" +
					"dimension equals one.");
			return null;
		}
		return out;
	}
	
	public static int[] fromVectorInt(Matrix m){
		int nx = m.getRowDimension();
		int ny = m.getColumnDimension();
		int[] out;
		if(nx==1){
			out = new int[ny];
			for(int i=0; i<ny; i++){
				out[i] = (int)m.get(0, i);
			}
		}else if(ny==1){
			out = new int[nx];
			for(int i=0; i<nx; i++){
				out[i] = (int)m.get(i, 0);
			}
		}else{
			System.err.println("fromVector method requires that either row or column" +
					"dimension equals one.");
			return null;
		}
		return out;
	}
	

	public static double[] fromVectorDouble(Matrix m){
		int nx = m.getRowDimension();
		int ny = m.getColumnDimension();
		double[] out;
		if(nx==1){
			out = new double[ny];
			for(int i=0; i<ny; i++){
				out[i] = m.get(0, i);
			}
		}else if(ny==1){
			out = new double[nx];
			for(int i=0; i<nx; i++){
				out[i] = m.get(i, 0);
			}
		}else{
			System.err.println("fromVector method requires that either row or column" +
					"dimension equals one.");
			return null;
		}
		return out;
	}
	
	public static final int[] arrayFromIntegerList(List<Integer> l){
		int[] out = new int[l.size()];
		int i=0;
		for(int n: l) { out[i]=n; i++; }
		return out;
	}
	public static final double[] arrayFromDoubleList(List<Double> l){
		double[] out = new double[l.size()];
		int i=0;
		for(double n: l) { out[i]=n; i++; }
		return out;
	}
	public static final float[] arrayFromFloatList(List<Float> l){
		float[] out = new float[l.size()];
		int i=0;
		for(float n: l) { out[i]=n; i++; }
		return out;
	}
	public static int[][][] getIntArray3d(ImageData img){
		int rows = img.getRows();
		int cols = img.getCols();
		int slices = img.getSlices();
		int[][][] vol = new int[rows][cols][slices];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					vol[i][j][k] = img.get(i, j, k).intValue();
				}
			}
		}
		return vol;
	}
	
	public static float[][][] getFloatArray3d(ImageData img){
		int rows = img.getRows();
		int cols = img.getCols();
		int slices = img.getSlices();
		float[][][] vol = new float[rows][cols][slices];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					vol[i][j][k] = img.get(i, j, k).floatValue();
				}
			}
		}
		return vol;
	}
	
	public static int[][][][] getIntArray4d(ImageData img){
		int rows = img.getRows();
		int cols = img.getCols();
		int slices = img.getSlices();
		int components = img.getComponents();
		int[][][][] vol4d = new int[rows][cols][slices][components];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						vol4d[i][j][k][l] = img.get(i, j, k, l).intValue();
					}
				}
			}
		}
		return vol4d;
	}
	
	public static float[][][][] getFloatArray4d(ImageData img){
		int rows = img.getRows();
		int cols = img.getCols();
		int slices = img.getSlices();
		int components = img.getComponents();
		float[][][][] vol4d = new float[rows][cols][slices][components];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						vol4d[i][j][k][l] = img.get(i, j, k, l).floatValue();
					}
				}
			}
		}
		return vol4d;
	}

	public static void main(String[] args){
		
		int[][][] test = new int[2][2][2];
		test[0][0][0]=0;
		test[0][0][1]=1;
		test[0][1][0]=2;
		test[0][1][1]=3;
		test[1][0][0]=4;
		test[1][0][1]=5;
		test[1][1][0]=6;
		test[1][1][1]=7;
		
		System.out.println(printArray(get1dSubArray(test,0,0,0)));
		System.out.println(printArray(get1dSubArray(test,0,0,0)));
	}
}
