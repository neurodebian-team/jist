package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.vecmath.Point3d;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberStatistics;
import edu.jhu.ece.iacl.algorithms.dti.tractography.Tract;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;


public class MedicAlgorithmFiberComparison extends ProcessingAlgorithm{
	//output params
	private ParamObject<String> stats;
	private ParamObject<FiberCollection> worstcase;
	private ParamObject<CurveCollection> worstcasevtk;

	//
	ParamObject<double[][]> jointHistogram;
	ParamFloat KLdiv;

	//input params
	private ParamObject<FiberCollection> fibersa;
	private ParamObject<FiberCollection> fibersb;
	private ParamOption comparison;
	private ParamPointDouble resolution;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(fibersa=new ParamObject<FiberCollection>("Fibers", new FiberCollectionReaderWriter()));
		inputParams.add(fibersb=new ParamObject<FiberCollection>("Fibers To Compare", new FiberCollectionReaderWriter()));
		inputParams.add(comparison = new ParamOption("Comparison",new String[]{"Max Distance", "Mean Distance", "Length Histogram KL distance"}));
		inputParams.add(resolution=new ParamPointDouble("Resolution",new Point3d(1,1,1)));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Compare Fibers");
		inputParams.setName("Compare_Fibers");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
//		dwiMotionCorr = new ParamVolumeCollection("Output Diffusion-weighted Volumes");
		outputParams.add(stats = new ParamObject<String>("Difference Stats", new StringReaderWriter()));
		outputParams.add(worstcase = new ParamObject<FiberCollection>("Worst Fibers", new FiberCollectionReaderWriter()));
		worstcase.setMandatory(false);
		outputParams.add(worstcasevtk =new ParamObject<CurveCollection>("Fibers (VTK)",new CurveVtkReaderWriter()));
		worstcasevtk.setMandatory(false);
		outputParams.add(jointHistogram=new ParamObject<double[][]>("Joint Histogram of Fiber Lengths", new ArrayDoubleTxtReaderWriter()));
		jointHistogram.setMandatory(false);
	}


	protected void execute(CalculationMonitor monitor) {
		String out = "";
		DecimalFormat dist = new DecimalFormat("###.#######");

		if(comparison.getIndex()<2){
			Tract a  = new Tract(fibersa.getObject());
			Tract b = new Tract(fibersb.getObject());
//			t.print();
//			System.out.println(getClass().getCanonicalName()+"\t"+"######");
//			new Tract(fibersb.getObject()).print();

			Point3d res = resolution.getValue();
//			double d = t.maxTractDistanceBySeed(fibersa.getObject().get(0),new Tract(fibersb.getObject()), res.x, res.y, res.z);
			a.tractDistancesBySeed(b, res.x, res.y, res.z);

			double maxmax = a.maxmax;
			double meanmax = a.meanmax;
			double meanmean = a.meanmean;

			FiberCollection worst = new FiberCollection();
			worst.setDimensions(fibersa.getObject().getDimensions());
			worst.setResolutions(fibersa.getObject().getResolutions());
			worst.add(a.worst);
			worst.add(b.worst);

			System.out.println(getClass().getCanonicalName()+"\t"+"Worst Cases");
			System.out.println(getClass().getCanonicalName()+"\t"+a.worst);
			System.out.println(getClass().getCanonicalName()+"\t"+"****");
			System.out.println(getClass().getCanonicalName()+"\t"+b.worst);

			worstcase.setObject(worst);
			worstcase.setFileName("WorstCaseFibers");

			out = out +"max dist: " +dist.format(maxmax) + "\n";
			out = out + "meanmax dist: " + dist.format(meanmax)+ "\n";
			out = out + "meanmean dist: " + dist.format(meanmean);

			System.out.println(getClass().getCanonicalName()+"\t"+out);

			stats.setObject(out);
			stats.setFileName(fibersa.getValue().getName()+"_compare_"+fibersb.getValue().getName());

			worstcasevtk.setObject(worst.toCurveCollection());
			worstcasevtk.setFileName("vtkWorstFibers");
		}else{
			FiberStatistics fstata = new FiberStatistics(fibersa.getObject());
			fstata.setRes(resolution.getParamX().getDouble(), resolution.getParamY().getDouble(), resolution.getParamZ().getDouble());
			fstata.computeLengths();

			ArrayList<Double> lengthslista = fstata.getFiberLengths();
			double[] lengthsa = toArray(lengthslista);
			lengthslista = null;

			fstata.finalize(); fstata=null;

			FiberStatistics fstatb = new FiberStatistics(fibersa.getObject());
			fstatb.setRes(resolution.getParamX().getDouble(), resolution.getParamY().getDouble(), resolution.getParamZ().getDouble());
			fstatb.computeLengths();

			ArrayList<Double> lengthslistb = fstatb.getFiberLengths();
			double[] lengthsb = toArray(lengthslistb);
			lengthslistb = null;

			double[][] jointhist = StatisticsDouble.jointhistogram(lengthsa, lengthsb, 256);
			double kld = StatisticsDouble.KLdivergence(lengthsa, lengthsb, 256);

			out = Double.toString(kld);
			stats.setObject(out);
			stats.setFileName(fibersa.getValue().getName()+"_"+fibersb.getValue().getName()+"_jointHist.txt");

			jointHistogram.setObject(jointhist);
			jointHistogram.setFileName(fibersa.getValue().getName()+"_"+fibersb.getValue().getName()+"_KLD.txt");
		}
	}


	private double[] toArray(ArrayList<Double> in ){
		double[] out = new double[in.size()];
		for(int i=0; i<in.size(); i++){
			out[i]=in.get(i);
		}
		return out;
	}
}
