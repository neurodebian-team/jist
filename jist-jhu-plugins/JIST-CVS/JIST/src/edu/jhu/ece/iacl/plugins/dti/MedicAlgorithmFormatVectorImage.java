package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


public class MedicAlgorithmFormatVectorImage extends ProcessingAlgorithm{
	//input params
	private ParamVolume vecvolin;
	private ParamOption format;

	//output param
	private ParamVolume vecvolout;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts the format of 3-d vector volumes: XxYxZx3 to 3xXxYxZ or" +
			"vice versa.  WARNING: HEADER INFORMATION IS NOT PRESERVED";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vecvolin = new ParamVolume("Input Vector Volume",null,-1,-1,-1,-1));
		String[] flipoptions = {"3xNxMxL to NxMxLx3","NxMxLx3 to 3xNxMxL"};
		inputParams.add(format = new ParamOption("Volume Format",flipoptions));


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Format_Vector_Volume");
		inputParams.setName("Format_Vector_Volume");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(vecvolout = new ParamVolume("Formated Vector Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) {
		ImageData vol = vecvolin.getImageData();
		ImageData volout;
		if(format.getIndex()==0){
			if(vol.getRows()==3){
				volout = new ImageDataMipav(vol.getName()+"_"+format.getValue(),
						vol.getType(), vol.getCols(), vol.getSlices(), vol.getComponents(), vol.getRows());

				for(int i=0;i<vol.getRows();i++){
					for(int j=0;j<vol.getCols();j++){
						for(int k=0;k<vol.getSlices();k++){
							for(int l=0;l<vol.getComponents();l++){
								volout.set(j,k,l,i,vol.getFloat(i,j,k,l));
							}
						}
					}
				}   
			}else{
				volout = vol;
			}
		}else{
			if(vol.getComponents()==3){
				volout = new ImageDataMipav(vol.getName()+"_"+format.getValue(),
						vol.getType(), vol.getComponents(), vol.getRows(), vol.getCols(), vol.getSlices());
				for(int i=0;i<vol.getRows();i++){
					for(int j=0;j<vol.getCols();j++){
						for(int k=0;k<vol.getSlices();k++){
							for(int l=0;l<vol.getComponents();l++){
								volout.set(l,i,j,k,vol.getFloat(i,j,k,l));
							}
						}
					}
				}   
			}else{
				volout = vol;
			}
		}

		vecvolout.setValue(volout);
	}
}
