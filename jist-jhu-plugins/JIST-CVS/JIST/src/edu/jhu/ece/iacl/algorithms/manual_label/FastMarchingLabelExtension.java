package edu.jhu.ece.iacl.algorithms.manual_label;

import java.util.Hashtable;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.algorithms.volume.DistanceField;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.data.BinaryMinHeap;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.MaskVolume6;
import edu.jhu.ece.iacl.jist.structures.image.VoxelFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelIndexed;

public class FastMarchingLabelExtension extends AbstractCalculation {
	EmbeddedSurface surf;
	Hashtable<Long, Integer> hash = null;
	int rows, cols, slices;
	int offset = 0;
	public static final byte UNVISITED = 0;
	public static final byte VISITED = 1;
	public static final byte SOLVED = 2;
	float[][][] levelSetM;
	float[][][][] grad;
	int[][][] levelLabels;
	byte[][][] labelM;
	BinaryMinHeap heap;
	byte[] neighborsX;
	byte[] neighborsY;
	byte[] neighborsZ;

	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");
	
	public FastMarchingLabelExtension() {
		super();

		setLabel("Fast-Marching Label Extension");
		MaskVolume6 mask = new MaskVolume6();
		neighborsX = mask.getNeighborsX();
		neighborsY = mask.getNeighborsY();
		neighborsZ = mask.getNeighborsZ();
	}

	public FastMarchingLabelExtension(AbstractCalculation parent) {
		super(parent);
		setLabel("Fast-Marching Label Extension");
		MaskVolume6 mask = new MaskVolume6();
		neighborsX = mask.getNeighborsX();
		neighborsY = mask.getNeighborsY();
		neighborsZ = mask.getNeighborsZ();
	}

	public static final String get_version(){
		return revnum;
	}
	
	protected void initializeBoundary(float maxDist) {
		int i, j, k;
		int ni, nj, nk;
		int count = 0;
		labelM = new byte[rows][cols][slices];
		for (i = 0; i < rows; i++) {
			for (j = 0; j < cols; j++) {
				for (k = 0; k < slices; k++) {
					labelM[i][j][k] = (levelLabels[i][j][k] == -1) ? UNVISITED
							: SOLVED;
					if (Math.abs(levelSetM[i][j][k]) < maxDist) {
						count++;
					}
				}
			}
		}
		setTotalUnits(count);
		heap = new BinaryMinHeap(count, rows, cols, slices);

		for (i = 0; i < rows; i++) {
			for (j = 0; j < cols; j++) {
				for (k = 0; k < slices; k++) {
					if (labelM[i][j][k] == SOLVED) {
						for (int koff = 0; koff < MaskVolume6.length; koff++) {
							ni = i + neighborsX[koff];
							nj = j + neighborsY[koff];
							nk = k + neighborsZ[koff];
							if (nj < 0 || nj >= cols || nk < 0 || nk >= slices
									|| ni < 0 || ni >= rows)
								continue; // Out of boundary
							if (labelM[ni][nj][nk] == UNVISITED) {
								VoxelIndexed<VoxelFloat> vox = new VoxelIndexed<VoxelFloat>(
										new VoxelFloat((float) Math
												.abs(levelSetM[i][j][k])));
								vox.setRefPosition(i, j, k);
								heap.add(vox);
								break;
							}
						}
					}
				}
			}
		}
	}

	public ImageDataInt solve(ImageData labels, int minLabel, float maxDist) {
		rows = labels.getRows();
		cols = labels.getCols();
		slices = labels.getSlices();
		ImageDataFloat extLevelSet = new ImageDataFloat(rows, cols, slices);
		levelSetM = extLevelSet.toArray3d();
		levelLabels = new int[rows][cols][slices];
		int l;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					l = labels.getInt(i, j, k);
					levelSetM[i][j][k] = (l > minLabel) ? 0 : 1;
					levelLabels[i][j][k] = (l > minLabel) ? l : -1;
				}
			}
		}
		DistanceField df = new DistanceField(this);
		extLevelSet = df.solve(extLevelSet, maxDist+0.1);
		levelSetM=extLevelSet.toArray3d();
		initializeBoundary(maxDist);
		computeMap(maxDist);
		ImageDataInt labelVol=new ImageDataInt(levelLabels);
		labelVol.setName(labels.getName()+"_ext");
		return labelVol;
	}

	public void computeMap(float maxDist) {
		Point3f q;
		int i, j, k, ni, nj, nk;
		int label;
		while (heap.size() > 0) {
			VoxelIndexed<VoxelFloat> he = (VoxelIndexed<VoxelFloat>) heap
					.remove();
			i = he.getRow();
			j = he.getColumn();
			k = he.getSlice();
			labelM[i][j][k] = SOLVED;
			incrementCompletedUnits();
			for (int koff = 0; koff < MaskVolume6.length; koff++) {
				ni = i + neighborsX[koff];
				nj = j + neighborsY[koff];
				nk = k + neighborsZ[koff];
				if (nj < 0 || nj >= cols || nk < 0 || nk >= slices || ni < 0
						|| ni >= rows)
					continue; // Out of boundary
				if (Math.abs(levelSetM[ni][nj][nk]) >= maxDist)
					continue;

				byte l = labelM[ni][nj][nk];
				if (l == VISITED) {
					label = updateNode(ni, nj, nk);
					if (label != -1) {
						levelLabels[ni][nj][nk] = label;
					}
				} else if (l == UNVISITED) {
					label = updateNode(ni, nj, nk);
					if (label != -1) {
						levelLabels[ni][nj][nk] = label;
						VoxelIndexed<VoxelFloat> vox = new VoxelIndexed<VoxelFloat>(
								new VoxelFloat((float) Math
										.abs(levelSetM[ni][nj][nk])));
						vox.setRefPosition(ni, nj, nk);
						labelM[ni][nj][nk] = VISITED;
						heap.add(vox);
					}
				}
			}
		}

		heap = null;
		System.gc();

		labelM = null;

		hash = null;
		heap = null;
		surf = null;
		System.gc();
		markCompleted();
	}

	protected int updateNode(int i, int j, int k) {
		int ni, nj, nk;
		float center = levelSetM[i][j][k];
		float bestDist = 0;
		int bestLabel = -1;
		for (int koff = 0; koff < MaskVolume6.length; koff++) {
			ni = i + neighborsX[koff];
			nj = j + neighborsY[koff];
			nk = k + neighborsZ[koff];
			if (nj < 0 || nj >= cols || nk < 0 || nk >= slices || ni < 0
					|| ni >= rows)
				continue; /* Out of boundary */
			if (labelM[ni][nj][nk] != SOLVED)
				continue;
			double d = center - levelSetM[ni][nj][nk];
			if (d >= bestDist) {
				bestDist = (float) d;
				bestLabel = levelLabels[ni][nj][nk];
			}
		}
		return bestLabel;
	}

}
