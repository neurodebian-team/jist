package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

import java.io.*;

public class AnalyzeDists {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String distfile = "/home/john/Research/20070727/corrected_dti/processed/jb_8/dists.raw";

		double[] dists = readDoubleVecHdr(distfile);

		double min=500;
		double max=0;
		double sum=0;
		int hitcount=0;
		for(int i=0; i<dists.length; i++){
//			System.out.println("jist.plugins"+"\t"+dists[i]);
			
			if(dists[i]>0){
				if(dists[i]>max){
					max=dists[i];
				}else if(dists[i]<min){
					min=dists[i];
				}
				sum=sum+dists[i];
				hitcount++;
			}
		}
		sum=sum/hitcount;

		System.out.println("jist.plugins"+"\t"+"The Max dist is: " + max);
		System.out.println("jist.plugins"+"\t"+"The Min dist is: " + min);
		System.out.println("jist.plugins"+"\t"+"The Mean dist is: " + sum);
	}

	private static double[] readDoubleVecHdr(String filename){
		double[] img = new double[0];
		try{
			DataInputStream imin = new DataInputStream(new FileInputStream(filename));
			int dim = imin.readInt();
			System.out.println("jist.plugins"+"\t"+dim);
			
			img = new double[dim];
			for(int i=0; i<dim;i++){
				img[i]=imin.readDouble();
			}
				
		}catch(IOException e){
			e.printStackTrace();
		}
		return img;
	}
}
