package edu.jhu.ece.iacl.algorithms.dti;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import edu.jhu.bme.smile.commons.math.RotationMatrix;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.bme.smile.commons.optimize.BrentMethod1D;
import edu.jhu.bme.smile.commons.optimize.DownhillSimplexND;
import edu.jhu.bme.smile.commons.optimize.FunctionNumericNDDifferentiation;
import edu.jhu.bme.smile.commons.optimize.GoldenSectionSearch1D;
import edu.jhu.bme.smile.commons.optimize.LevenbergMarquardt;
import edu.jhu.bme.smile.commons.optimize.LineSearchND;
import edu.jhu.bme.smile.commons.optimize.LineSearchNDDifferentiable;
import edu.jhu.bme.smile.commons.optimize.NewtonMethod1D;
import edu.jhu.bme.smile.commons.optimize.OptimizableNDContinuous;

/**
 * This class is used to minimize the least likelihood estimator from data from diffusion weighted imaging.
 * It appears that all the algorithms but Line Search using Newton's method are very stable. The Line Searches
 * are fairly slow since they have to iterate through the optimizer so many times. Both Downhill Simplex
 * and LM are very fast, with Downhill Simplex only slightly faster with this function. With nicer functions
 * LM can be significantly faster than Downhill Simplex as demonstrated by the optimization of simpler functions
 * in TestNDOptimizations. 
 * 
 * @author Hanlin Wan
 * 
 * Added support for non-Euler parameterization.
 * @author Bennett Landman
 *
 */
public class DTEMRLTensor  {

	Matrix imgMatrix;
	private double[] mat, obsDW;
	private double[] truth, sig, obs;
	private double[] LLresult;
	private double LLthreshold;
	private boolean useSigmaPrior;
	private double sigmaPriorMean;
	private double sigmaPriorNormCoeff; 
	private double[] dMin, dMax;
	private double []obsB0; 
	private double []sqrtKSpaceAvgs;
	private double sigmaFactor;	
	private double initialLikelihood=1;
	//used to match the scale of the optimization criteria
	private double dFactor = 1e5, dFactor2 = 1e5, tol = 1e-5, myStep = 1;
	private int iterations, myMethod = 3;
	private Matrix myD;
	private boolean useDirectRepresenation = true;
	public static double PLUS_EPSILON = 1e-6;
	private RotationMatrix rm;
	
	public DTEMRLTensor() {
		mat = new double[8];
		myD = new Matrix(6,1);
		useSigmaPrior = false;
		LLthreshold = Double.MAX_VALUE;
	}
	
	public void setLikelihoodThreshold(double t) {
		LLthreshold=t;
	}
	
	public void setNoiseLevelPriorOff() {
		useSigmaPrior=false;
	}
	
	public void setNoiseLevelPrior(double mean,double std) {
		//-log(2*(estVarS )^2)*(SIGMA-abs(x(2))).^2/(2*(estVarS)^2)
		useSigmaPrior=true;
		sigmaPriorMean=mean;
		sigmaPriorNormCoeff = -1/(2*std*std);
	}
	
	public void setUseDirectTensorRepresentation(boolean useDirect) {
		Matrix D = getD();
		useDirectRepresenation = useDirect;
		setD(D.getColumnPackedCopy());
		if(useDirect) {
			dFactor = 1e5;
			dFactor2 = 1e5;
		} else {
			dFactor = 1e2;
			dFactor2 = 1e3;
		}		
	}
	
	/**
	 * Initializes MLTensor with these parameters
	 * @param imgMatrix image matrix
	 * @param b0 initial b0
	 * @param sigma initial standard deviation
	 * @param obsB0 observed b0's
	 * @param obsDW observed diffusion weighted tensor
	 */
	public void init(Matrix imgMatrix, double b0, double sigma, 
			double []obsB0, double []obsDW, double []dInit, double []kspaceAverages) {
		setD(dInit);
		baseInit(imgMatrix, b0, sigma, 
				obsB0, obsDW, kspaceAverages);
		
		initialLikelihood=LLvalue();
//		setTolerance(Math.abs(0.001*initialLikelihood)); 
	
	}
	
	public void init(Matrix imgMatrix, double b0, double sigma, 
			double []obsB0, double []obsDW, float []dInit, double []kspaceAverages) {
		setD(dInit);	
		baseInit(imgMatrix, b0, sigma, 
				obsB0, obsDW, kspaceAverages);
//		setTolerance(0.01); 
		initialLikelihood=LLvalue();
//		setTolerance(0.01*initialLikelihood);
//		setTolerance(Math.abs(0.001*initialLikelihood)); 
	}
	public void baseInit(Matrix imgMatrix, double b0, double sigma, 
			double []obsB0, double []obsDW, double []kspaceAverages) {
	
		int size = obsDW.length + obsB0.length;
		truth = new double[size];
		sig = new double[size];
		obs = new double[size];
		
		System.arraycopy(obsB0, 0, obs, 0, obsB0.length);
		System.arraycopy(obsDW, 0, obs, obsB0.length, obsDW.length);

		
		LLresult = new double[size];
		
		this.imgMatrix = imgMatrix;
		sigmaFactor = b0 / sigma;
				
		setB0(b0);
		setSigma(sigma);
//		for (int i=2; i<8; i++) 
//			mat[i] = 0;
		this.obsB0 = obsB0;
		this.obsDW = obsDW.clone();
		this.iterations = 0;
		

		if(useDirectRepresenation) {
			// Setup the default scaling and valid range

			// set default domain minimum and maximum
			dMax = new double[8];
			dMax[0] = mat[0] * 2;
			dMax[1] = mat[1] * 2;
			for (int i=0; i<6; i++) {
				if (mat[2+i]*2 < 1e-3) dMax[i+2] = 1e-3 * dFactor;
				else dMax[i+2] = mat[2+i] * 2 * dFactor;
			}
			dMin = new double[8];
			dMin[0] = mat[0] * 0.5;
			for (int i=1; i<8; i++) 
				dMin[i] = 0;
			
		} else {
			dMax = new double[8];
			dMax[0] = mat[0] * 2;
			dMax[1] = mat[1] * 2;
			for (int i=0; i<6; i++) {
				if (mat[2+i]*2 < 1e-3) dMax[i+2] = 1e-3 * dFactor;
				else dMax[i+2] = mat[2+i] * 2 * dFactor;
			}
			dMin = new double[8];
			dMin[0] = mat[0] * 0.5;
			for (int i=1; i<8; i++) 
				dMin[i] = 0;
		}
		sqrtKSpaceAvgs = new double[kspaceAverages.length];
		for(int i=0;i<kspaceAverages.length;i++)
			sqrtKSpaceAvgs[i]=Math.sqrt(kspaceAverages[i]);

	}
	


	/**
	 * Lets you specify which algorithm to use to optimize all 8 parameters at once. 
	 * @param method 0 - Downhill Simplex; 1 - LM; 2 - Line Search using Golden Section Search 
	 * 3 - Line Search using Brent's Method; 4 - Line Search using Newton
	 * @return whether the optimization was successful
	 */
	public boolean optimize8D() {return optimize8D(0);};
	public boolean optimize8D(int method) {		
		boolean status = false;
		

		System.arraycopy(obsB0, 0, obs, 0, obsB0.length);
		System.arraycopy(obsDW, 0, obs, obsB0.length, obsDW.length);

		opt8D opt = new opt8D(this);
		


		switch(method) {
		case 0: 
			// Downhill Simplex
			DownhillSimplexND alg = new DownhillSimplexND();
			alg.initialize(opt, mat, obsB0[0]/20);			
			status = alg.optimize(true);
			iterations = alg.getIterations();					
			break;
		case 1:					// LM
			LevenbergMarquardt alg2 = new LevenbergMarquardt();			
			alg2.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), mat);
			status = alg2.optimize(true);
			iterations = alg2.getIterations();
			break;
		case 2: // Line Search - Golden Section Search and Brent's Method
			LineSearchND alg3 = new LineSearchND(new GoldenSectionSearch1D());
			alg3.initialize(opt);
			status = alg3.optimize(true);
			iterations = alg3.getIterations();
			break;
		case 3: // Line Search - Golden Section Search and Brent's Method
			LineSearchND alg4 = new LineSearchND(new BrentMethod1D());			
			alg4.initialize(opt);
			status = alg4.optimize(true);
			iterations = alg4.getIterations();
			break;
		case 4:					// Line Search - Newton
			LineSearchNDDifferentiable alg5 = new LineSearchNDDifferentiable(new NewtonMethod1D());			
			alg5.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod));
			status = alg5.optimize(true);
			iterations = alg5.getIterations();
			break;
		default:
			throw new RuntimeException("Unkwown optimization method.");
		}
		return status;
	}

	/**
	 * Lets you specify which algorithm to use and the maximum number of iterations.
	 * It will optimize alternate between optimizing D and optimizing b0 and sigma.
	 * @param method 0 - Downhill Simplex; 1 - LM; 2 - Line Search using Golden Section Search
	 * 3 - Line Search using Brent's; 4 - Line Search using Newton
	 * @param maxIter maximum number of iterations
	 * @return whether the optimization was successful
	 */
	public boolean optimize6D2D() {return optimize6D2D(0,2);};
	public boolean optimize6D2D(int method) {return optimize6D2D(method,2);};
	public boolean optimize6D2D(int method, int maxIter) {
		boolean status = false;
	

		System.arraycopy(obsB0, 0, obs, 0, obsB0.length);
		System.arraycopy(obsDW, 0, obs, 1, obsDW.length);
		double extremaCompare[] = new double[8];
		opt6D2D opt = new opt6D2D(this);
		double[] D6 = new double[6];
		double[] D2 = new double[2];

		switch(method) {
		case 0:		// Downhill Simplex
			DownhillSimplexND alg = new DownhillSimplexND();
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg.initialize(opt, D6, 20);
				alg.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg.initialize(opt, D2, 20);
				alg.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}
			break;
		case 1:		// LM
			LevenbergMarquardt alg2 = new LevenbergMarquardt();
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, 6);
				alg2.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D6);
				alg2.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, 2);
				alg2.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D2);
				alg2.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}	
			break;
		case 2: // Line Search - Golden Section Search and Brent's Method
			LineSearchND alg3;
			alg3 = new LineSearchND(new GoldenSectionSearch1D(), 20);

			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg3.initialize(opt, D6);
				alg3.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg3.initialize(opt, D2);
				alg3.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}
			break;
		case 3:	// Line Search - Golden Section Search and Brent's Method
			LineSearchND alg4;
			alg4 = new LineSearchND(new BrentMethod1D(), 20);
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg4.initialize(opt, D6);
				alg4.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg4.initialize(opt, D2);
				alg4.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}
			}
			break;
		case 4:		// Line Search - Newton
			LineSearchNDDifferentiable alg5 = new LineSearchNDDifferentiable(new NewtonMethod1D(), 20);
			for (iterations=0; iterations<maxIter; iterations++) {
				extremaCompare = new double[8];
				System.arraycopy(mat, 0, extremaCompare, 0, mat.length);

				//optimize 6D
				opt.setDim(true);
				System.arraycopy(mat, 2, D6, 0, D6.length);
				alg5.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D6);
				alg5.optimize(true);
				//optimize 2D
				opt.setDim(false);
				System.arraycopy(mat, 0, D2, 0, D2.length);
				alg5.initialize(new FunctionNumericNDDifferentiation(opt,myStep,myMethod), D2);
				alg5.optimize(true);

				double diff = Math.abs(extremaCompare[0] - mat[0]);
				diff += Math.abs(extremaCompare[1] - mat[1]) / sigmaFactor;
				for(int j=2; j<8; j++)
					diff += Math.abs(extremaCompare[j] - mat[j]) / dFactor;
				diff /= 8;
				if (diff < 0.1) {
					status = true;
					break;
				}

			}		
			break;
		default:
			throw new RuntimeException("Invalid optimization type.");
		}
		return status;
	}	

	/**
	 * Calculates the log-likelihood (LL) value
	 * @return LLvalue
	 */
	public double LLvalue() {
		for(int i=0;i<obsB0.length;i++)
			truth[i] = getB0();
		Matrix modelDW = (imgMatrix.times(getD().times(-1)));		
		double b0 = getB0();
		for (int i=0; i<modelDW.getRowDimension(); i++) 
			truth[i+obsB0.length] = b0 * Math.exp(modelDW.get(i,0));
		double sigma = getSigma();
		for (int i=0; i<(obsB0.length+obsDW.length); i++) 
			sig[i] = sigma/sqrtKSpaceAvgs[i];
		StatisticsDouble.robustLogRicianPDF(truth, sig, obs,LLresult);
		double val=0;
		for(int i=0;i<LLresult.length;i++) {
			val-=(LLresult[i]<-LLthreshold?LLthreshold:LLresult[i]);
		}
		if(useSigmaPrior)
			val+=StatisticsDouble.square(sigma-sigmaPriorMean)*sigmaPriorNormCoeff;
		if(val==Double.NaN)
			return Double.MAX_VALUE;
		
		return val;
	}
	
	public static void LLvalue(double []modelTruth, double sigma, double []obs, double []swap, double []result) {
		for(int i=0;i<swap.length;i++)
			swap[i]=sigma;
		StatisticsDouble.robustLogRicianPDF(modelTruth, swap, obs,swap);
		result[0]=StatisticsDouble.mean(swap);
		result[1]=StatisticsDouble.std(swap);		
	}

	/**
	 * Gets the value of b0
	 * @return b0
	 */
	public double getB0() {
		return Math.abs(mat[0]);
	}

	public void setB0(double b0) {
		mat[0] = b0;
	}

	/**
	 * Gets the value of sigma. If sigma is less than 1e-6, 1e-6 is returned.
	 * @return sigma
	 */
	public double getSigma() {
		double s = (mat[1] / sigmaFactor);
		return (s<PLUS_EPSILON?PLUS_EPSILON:s); // sigma MUST be finite and positive
	}

	public void setSigma(double s) {
		if(s<PLUS_EPSILON)
			s=PLUS_EPSILON;
		mat[1] = s*sigmaFactor;
	}

	/**
	 * Gets the D matrix
	 * @return D
	 */
	public Matrix getD() {
		if(this.useDirectRepresenation) {
			for (int i=2; i<8; i++) 
				myD.set(i-2,0, mat[i] / dFactor);
			return myD;
		} else {			
			double e1 = Math.exp(mat[0+2]/ dFactor);
			double e2 = Math.exp(mat[1+2]/ dFactor);
			double e3 = Math.exp(mat[2+2]/ dFactor);
			rm.setParameters(mat[3+2]/dFactor2, mat[4+2]/dFactor2, mat[5+2]/dFactor2);
			Matrix V = rm.getMatrix();
			Matrix e = new Matrix(new double[][]{{e1,0,0},{0,e2,0},{0,0,e3}});
			Matrix T = V.times(e).times(V.transpose());
			myD.set(0,0,T.get(0,0));
			myD.set(1,0,T.get(0,1));
			myD.set(2,0,T.get(0,2));
			myD.set(3,0,T.get(1,1));
			myD.set(4,0,T.get(1,2));
			myD.set(5,0,T.get(2,2));
			return myD;
		}
	}

	public void setD(double []dInit) {
		if(this.useDirectRepresenation) {
			for (int i=0; i<dInit.length; i++) 
				mat[i+2] = dInit[i] * dFactor;
			rm = null;
		} else {
		
			Matrix T = new Matrix(3,3);
			T.set(0,0,dInit[0]);
			T.set(1,0,dInit[1]);T.set(0,1,dInit[1]);
			T.set(2,0,dInit[2]);T.set(0,2,dInit[2]);
			T.set(1,1,dInit[3]);
			T.set(1,2,dInit[4]);T.set(2,1,dInit[4]);
			T.set(2,2,dInit[5]);
			SingularValueDecomposition svd = new SingularValueDecomposition(T);
			double []e2= svd.getSingularValues();
			mat[0+2] = Math.log((e2[0]))*dFactor;
			mat[1+2] = Math.log((e2[1]))*dFactor;
			mat[2+2] = Math.log((e2[2]))*dFactor;			
			Matrix V = svd.getV();
			if(V.det()<0)
				V = V.times(-1);			
			rm = new RotationMatrix();
			rm.setMatrix(V.getArray());
			double []ang =rm.getParameters();
			mat[3+2]=(double)ang[0]*dFactor2;
			mat[4+2]=(double)ang[1]*dFactor2;
			mat[5+2]=(double)ang[2]*dFactor2;
			rm.setParameters(ang[0], ang[1], ang[2]);
		}
	}
	
	public void setD(float []dInit) {
		if(this.useDirectRepresenation) {
			for (int i=0; i<dInit.length; i++) 
				mat[i+2] = dInit[i] * dFactor;
			rm = null;
		} else {
		
			Matrix T = new Matrix(3,3);
			T.set(0,0,dInit[0]);
			T.set(1,0,dInit[1]);T.set(0,1,dInit[1]);
			T.set(2,0,dInit[2]);T.set(0,2,dInit[2]);
			T.set(1,1,dInit[3]);
			T.set(1,2,dInit[4]);T.set(2,1,dInit[4]);
			T.set(2,2,dInit[5]);
			SingularValueDecomposition svd = new SingularValueDecomposition(T);
			double []e2= svd.getSingularValues();
			mat[0+2] = Math.log((e2[0]))*dFactor;
			mat[1+2] = Math.log((e2[1]))*dFactor;
			mat[2+2] = Math.log((e2[2]))*dFactor;			
			Matrix V = svd.getV();
			if(V.det()<0)
				V = V.times(-1);			
			rm = new RotationMatrix();
			rm.setMatrix(V.getArray());
			double []ang =rm.getParameters();
			mat[3+2]=(double)ang[0]*dFactor2;
			mat[4+2]=(double)ang[1]*dFactor2;
			mat[5+2]=(double)ang[2]*dFactor2;
			rm.setParameters(ang[0], ang[1], ang[2]);
		}
	}

	/**
	 * Sets the scaling factor for sigma. You want a value that when multiplied by
	 * sigma, you get a number in the same order of magnitude as b0. This is to 
	 * ensure that when optimizing, the parameters are not too far apart. The default 
	 * sigmaFactor is set to be b0/(10*sigma).
	 * @param s number to scale sigma by
	 */
	public void setSigmaFactor(double s) {
		mat[1] = (mat[1] / sigmaFactor) * s;
		sigmaFactor = s;
	}

	/**
	 * Sets the scaling factor for D. The default is set to 1e6.
	 * @param d number to scale D by
	 */
	public void setDFactor(double d) {
		for (int i=2; i<8; i++) 
			myD.set(i-2,0, mat[i] / dFactor * d);
		dFactor = d;
	}

	/**
	 * Sets the tolerance. Default value is 0.001. 
	 * @param t tolerance
	 */
	public void setTolerance(double t) {
		tol = t;
	}

	/**
	 * Sets the step size used in calculating derivatives. The default is set to 0.1.
	 * @param s step size
	 */
	public void setStepSize(double s) {
		myStep = s;
	}

	/**
	 * Sets the method used to calculate the derivatives. The default is set to 3-point.
	 * @param m method
	 */
	public void setMethod(int m) {
		myMethod = m;
	}

	/**
	 * Sets the domain minimum and maximum values. The default values are as follows:
	 * b0 and sigma - 0.5 to 2 times the initial value
	 * D - minimum is 0. Maximum is 2 times the initial value but is at least 1e-3.
	 * @param min domain minimum array of size 8
	 * @param max domain maximum array of size 8
	 */
	public void setDomain(double []min, double []max) {
		dMin[0] = min[0];
		dMax[0] = max[0];
		dMin[1] = min[1] * sigmaFactor;
		dMax[1] = max[1] * sigmaFactor;
		if(useDirectRepresenation) {
		for (int i=2; i<8; i++) {
			dMin[i] = min[i] * dFactor;
			dMax[i] = max[i] * dFactor;
		}
		} else {
			for (int i=2; i<5; i++) {
				dMin[i] = min[i] * dFactor;
				dMax[i] = max[i] * dFactor;
			}
			for (int i=5; i<8; i++) {
				dMin[i] = min[i] * dFactor2;
				dMax[i] = max[i] * dFactor2;
			}	
		}
	}

	/**
	 * Gets the number of iterations required to calculate the result.
	 * @return number of iterations
	 */
	public int getIterations() {
		return iterations + 1;
	}

	/**
	 * Optimizes using all 8 parameters at the same time.
	 */
	private class opt8D implements OptimizableNDContinuous {
		DTEMRLTensor parent;	

		/**
		 * Constructor to create the object to optimize 8 dimensions at once.
		 * @param par parent MLTensor object
		 */
		public opt8D(DTEMRLTensor par) {
			parent = par;
		}

		/**
		 * Gets the tolerance.
		 * @return tolerance
		 */
		public double getDomainTolerance() {
			return tol;
		}

		/**
		 * Gets the domain maximum.
		 * @return domain max
		 */
		public double[] getDomainMax() {
			return dMax;
		}

		/**
		 * Gets the domain minimum
		 * @return domain min
		 */
		public double[] getDomainMin() {
			return dMin;
		}

		/**
		 * Gets the number of dimensions.
		 * @return number of dimensions
		 */
		public int getNumberOfDimensions() {
			return 8;
		}

		/**
		 * Gets the value of the function at location x.
		 * @param x location to evaluate function
		 * @return value at x
		 */
		public double getValue(double[] x) {
			System.arraycopy(x, 0, mat, 0, x.length);
			return LLvalue();
		}
	}

	/**
	 * Pptimizes using D then b0 and sigma.
	 */
	private class opt6D2D implements OptimizableNDContinuous {
		DTEMRLTensor parent;	
		boolean sixD;

		/**
		 * Constructor to create the object to optimize 6 dimensions and then 2 dimensions.
		 * @param par parent MLTensor object
		 */
		public opt6D2D(DTEMRLTensor par) {
			this.parent = par;
		}

		/**
		 * Gets the tolerance.
		 * @return tolerance
		 */
		public double getDomainTolerance() {
			return tol;
		}

		/**
		 * Sets which set of variables to optimize.
		 * @param sixD true if optimizing D, false if optimizing b0 and sigma
		 */
		public void setDim(boolean sixD) {
			this.sixD = sixD;
		}

		/**
		 * Gets the domain maximum.
		 * @return domain max
		 */
		public double[] getDomainMax() {
			if (sixD) {
				double[] max = new double[6];
				System.arraycopy(dMax, 2, max, 0, 6);
				return max;
			}
			else {
				double[] max = new double[2];
				System.arraycopy(dMax, 0, max, 0, 2);
				return max;
			}
		}

		/**
		 * Gets the domain minimum.
		 * @return domain min
		 */
		public double[] getDomainMin() {
			if (sixD) {
				double[] min = new double[6];
				System.arraycopy(dMin, 2, min, 0, 6);
				return min;
			}
			else {
				double[] min = new double[2];
				System.arraycopy(dMin, 0, min, 0, 2);
				return min;
			}
		}

		/**
		 * Gets the number of dimensions.
		 * @return number of dimensions
		 */
		public int getNumberOfDimensions() {
			if (sixD) return 6;
			else return 2;
		}

		/**
		 * Gets the value of the function at location x.
		 * @param x location to evaluate function
		 * @return value at x
		 */
		public double getValue(double[] x) {
			if (sixD) 
				System.arraycopy(x, 0, mat, 2, x.length);
			else 
				System.arraycopy(x, 0, mat, 0, x.length);
			return LLvalue();
		}
	}


}