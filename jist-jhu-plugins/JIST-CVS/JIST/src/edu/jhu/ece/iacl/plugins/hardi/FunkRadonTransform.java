package edu.jhu.ece.iacl.plugins.hardi;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import edu.jhu.bme.smile.commons.math.specialFunctions.SphericalHarmonics;
import edu.jhu.bme.smile.commons.textfiles.TextFileReader;
import edu.jhu.ece.iacl.algorithms.hardi.QBall;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class FunkRadonTransform extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamFileCollection DWdata4D; 		// SLAB-enabled Imaging Data
	private ParamFileCollection Mask3D;			// SLAB-enabled Binary mask to indicate computation volume
	private ParamOption sphericalHarmonicOrder;	// Select supported spherical harmonic orders
	private ParamFile bvaluesTable;			// .b file with a list of b-values
	private ParamFile gradsTable;			// .grad or .dpf file with a list of gradient directions
	private ParamFloat lambdaOption;		// lambda value for FRT
	private ParamFloat b0ThresholdOption; 	// implicit masking

	ParamObject<String> orderDegreeList;
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFileCollection modifiedSphericalHarmonicCoeffsReal;	// SLAB-enabled A 4D volume with one tensor estimated per pixel

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Funk Radon Transform.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion.QBall");
		inputParams.setLabel("Q-Ball: Funk Radon Transform");
		inputParams.setName("Q-Ball:_Funk_Radon_Transform");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamFileCollection("DWI and Reference Image(s) Data (4D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		inputParams.add(bvaluesTable=new ParamFile("Table of b-values (b=0 are excluded)",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(sphericalHarmonicOrder=new ParamOption("Spherical Harmonic Order",new String[]{"0","2","4","6","8","10","12","14"}));
		inputParams.add(Mask3D=new ParamFileCollection("Mask Volume to Determine Region of Tensor Estimation (3D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.
		inputParams.add(lambdaOption=new ParamFloat("Lambda value of FRT",0,Float.MAX_VALUE,(float)0.006));
		inputParams.add(b0ThresholdOption=new ParamFloat("Implicit Mask Threshold on Mean B0",-1,Float.MAX_VALUE,0));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		modifiedSphericalHarmonicCoeffsReal = new ParamFileCollection("Modified Spherical Harmonic Coefficients",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		modifiedSphericalHarmonicCoeffsReal.setName("QBall-Modified SH");
		outputParams.add(modifiedSphericalHarmonicCoeffsReal);
		outputParams.add(orderDegreeList = new ParamObject<String>("Coefficient degree and order",new StringReaderWriter(new FileExtensionFilter(new String[]{"lm"}))));
	}


	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to
		 * 			know when the bug happened.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Analytic Q-Ball: Start");

		/****************************************************
		 * Step 2. Loop over input slabs
		 ****************************************************/
		List<File> dwList = DWdata4D.getValue();
		List<File> maskList = this.Mask3D.getValue();
		ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
		ArrayList<File> outVolsReal = new ArrayList<File>();
		ArrayList<File> outVolsImag = new ArrayList<File>();
		for(int jSlab=0;jSlab<dwList.size();jSlab++) {
			System.out.println(getClass().getCanonicalName()+"\t"+"Starting Slab: "+jSlab);
			System.out.flush();
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			ImageData dwd=rw.read(dwList.get(jSlab));

			String sourceName = dwd.getName();
			ImageDataFloat DWFloat=new ImageDataFloat(dwd);
			dwd.dispose();
			dwd=null;


			ImageData maskVol=null;
			if(maskList!=null)
				if(maskList.size()>jSlab)
					if(maskList.get(jSlab)!=null)
						maskVol=rw.read(maskList.get(jSlab));

			byte [][][]mask=null;
			if(maskVol!=null) {
				ImageDataUByte maskByte = new ImageDataUByte (maskVol);
				mask = maskByte.toArray3d();
				maskByte.dispose();
				maskByte=null;
				maskVol.dispose();
				maskVol=null;
			}

			float [][]bs=null;
			TextFileReader text = new TextFileReader(bvaluesTable.getValue());
			try {
				bs = text.parseFloatFile();
			} catch (IOException e)
			{
				throw new RuntimeException("QBall: Unable to parse b-file");
			}

			float [][]grads=null;
			text = new TextFileReader(gradsTable.getValue());
			try {
				grads  = text.parseFloatFile();
			} catch (IOException e) {

				throw new RuntimeException("QBall: Unable to parse grad-file");
			}

			/****************************************************
			 * Step 3. Perform limited error checking
			 ****************************************************/
			// If there are 4 columns in the gradient table, remove the 1st column (indecies)
			if(grads[0].length==4) {
				float [][]g2 = new float[grads.length][3];
				for(int i=0;i<grads.length;i++)
					for(int j=0;j<3;j++)
						g2[i][j]=grads[i][j+1];
				grads=g2;
			}

			if(grads[0].length!=3)
				throw new RuntimeException("QBall: Invalid gradient table. Must have 3 or 4 columns.");
			if(bs[0].length!=1)
				throw new RuntimeException("QBall: Invalid b-value table. Must have 1 column.");
			float []bval = new float[bs.length];
			for(int i=0;i<bval.length;i++)
				bval[i]=bs[i][0];

			int SHorder = Integer.valueOf(sphericalHarmonicOrder.getValue());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			float [][][][]SHCoeff_real = new float[DWFloat.getRows()][DWFloat.getCols()][DWFloat.getSlices()][QBall.getNumberOfCoefficients(SHorder)];
			int [][]LM = new int[QBall.getNumberOfCoefficients(SHorder)][2];

			QBall.estimateSphericalHarmonicsFRT(SHCoeff_real,LM,DWFloat.toArray4d(),bval,grads,mask,SHorder,lambdaOption.getFloat(),b0ThresholdOption.getFloat());


			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/

			ImageData  out= (new ImageDataFloat(SHCoeff_real)); SHCoeff_real=null;
			out.setHeader(DWFloat.getHeader());
			out.setName(sourceName+"_QBallCoeffReal");
			File outputSlab = rw.write(out, getOutputDirectory());
			outVolsReal.add(outputSlab);
			out.dispose();

			out=null;

			if(jSlab==0){
				orderDegreeList.getReaderWriter().setFileName("QBallLMListForOrder"+SHorder);
				StringWriter LMstring = new StringWriter();
				for(int i=0;i<LM.length;i++) {
					LMstring.append(LM[i][0]+"\t"+LM[i][1]+"\n");
				}
				orderDegreeList.setObject(LMstring.toString());
			}

		}
		modifiedSphericalHarmonicCoeffsReal.setValue(outVolsReal);

		/****************************************************
		 * Step 6. Let the user know that your code is finished.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"QBall: FRT FINISHED");
	}
}
