/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.roi;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Bennett Landman (landman@jhu.edu)
 */
public class AlgorithmROIStats extends ProcessingAlgorithm{
	ParamVolume volulmeParam;
	ParamVolume maskParam;
	ParamFloat resultMean;
	ParamFloat resultMedian;
	ParamFloat resultStd;
	ParamFloat resultMax;
	ParamFloat resultMin;
	ParamInteger resultVoxels;
	ParamObject<double[][]> resultHistogram;
	ParamInteger maskID;
	ParamInteger histogramBins;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Calculate ROI-based statistics.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(volulmeParam=new ParamVolume("Image Volume"));
		inputParams.add(maskParam=new ParamVolume("ROI Mask"));
		maskParam.setMandatory(false);

		inputParams.add(maskID=new ParamInteger("Mask ID",0,Integer.MAX_VALUE));
		inputParams.add(histogramBins=new ParamInteger("Histogram Bins",0,Integer.MAX_VALUE));
		histogramBins.setValue(256);

		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.ROI");
		inputParams.setLabel("ROI Calculator");
		inputParams.setName("ROI_Calculator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://sites.google.com/a/jhu.edu/neuroweb/");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/a/jhu.edu/neuroweb/"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVoxels=new ParamInteger("# Voxels"));
		outputParams.add(resultMean=new ParamFloat("Mean"));
		outputParams.add(resultMedian=new ParamFloat("Median"));
		outputParams.add(resultStd=new ParamFloat("Std"));
		outputParams.add(resultMax=new ParamFloat("Max"));
		outputParams.add(resultMin=new ParamFloat("Min"));
		outputParams.add(resultHistogram=new ParamObject<double[][]>("Histogram", new ArrayDoubleTxtReaderWriter()));
//		ArrayDoubleTextReaderWriter t;
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ImageData vol1=volulmeParam.getImageData();
		ImageData volMask=maskParam.getImageData();
		int IDval = maskID.getInt();

		int rows=vol1.getRows();
		int cols=vol1.getCols();
		int slices=vol1.getSlices();
		int components = vol1.getComponents();

		if(vol1.getRows()!=rows ||
				vol1.getCols()!=cols ||
				vol1.getSlices()!=slices ||
				vol1.getComponents()!=components)
			throw new RuntimeException("Volumes of unequal size cannot be combined.");
		double[] data;
		int count;
		ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",rows,cols,slices);
		if(volMask!=null){ 	// if a mask is provided, consider only the portion of the image overlapping with the mask
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {
								if(volMask.get(i, j, k, l).intValue()==IDval)
									count++;
							}
						} else {
							if(volMask.get(i, j, k).intValue()==IDval)
								count++;
						}
					}
				}
			}

			data =new double[count];
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {

								if(volMask.get(i, j, k, l).intValue()==IDval) {
									data[count] =vol1.get(i, j, k, l).doubleValue();
									count++;
								}
							}
						} else {
							if(volMask.get(i, j, k).intValue()==IDval) {
								data[count] =vol1.get(i, j, k).doubleValue();
								count++;
							}
						}
					}
				}
			}
		}else{	// if no mask is provided, consider the entire image
			count = rows*cols*slices*components;
			data =new double[count];
			count=0;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components>1) {
							for(int l=0;l<components;k++) {
								data[count] =vol1.get(i, j, k, l).doubleValue();
								count++;
							}
						} else {
							data[count] =vol1.get(i, j, k).doubleValue();
							count++;
						}
					}
				}
			}
		}

		resultVoxels.setValue(count);
		resultMean.setValue(StatisticsDouble.mean(data));
		resultStd.setValue(StatisticsDouble.std(data));
		resultMedian.setValue(StatisticsDouble.median(data));
		resultMax.setValue(StatisticsDouble.max(data));
		resultMin.setValue(StatisticsDouble.min(data));
		resultHistogram.setObject(StatisticsDouble.histogram(data, histogramBins.getInt()));
		resultHistogram.setFileName(vol1.getName()+"_histogram");
	}
}
