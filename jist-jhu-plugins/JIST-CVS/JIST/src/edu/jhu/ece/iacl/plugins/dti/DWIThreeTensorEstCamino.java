package edu.jhu.ece.iacl.plugins.dti;

import gov.nih.mipav.model.structures.ModelImage;
import imaging.SchemeV1;
import inverters.ModelIndex;
import inverters.ThreeTensorInversion;

import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLLMSE;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.utility.FileUtil;


public class DWIThreeTensorEstCamino extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamVolume DWdata4D; 		// Imaging Data
	private ParamVolume Mask3D;			// Binary mask to indicate computation volume
	private ParamOption threeTensorFitOption;		// Option to attempt to estimate with missing data
	private ParamOption oneTensorInitOption;		// Option to attempt to estimate with missing data
	private ParamFloat noiseLevel;		// Used for restore
	private ParamFile SchemeFile;
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume tensor1Volume;	// A 4D volume with one tensor estimated per pixel
	private ParamVolume tensor2Volume;	// A 4D volume with one tensor estimated per pixel
	private ParamVolume tensor3Volume;	// A 4D volume with one tensor estimated per pixel
	private ParamVolume mix1Volume;	// A 3D volume with the mixture component of tensor 1
	private ParamVolume mix2Volume;	// A 3D volume with the mixture component of tensor 2
	private ParamVolume mix3Volume;	// A 3D volume with the mixture component of tensor 3
	private ParamVolume exitCodeVolume;	// A 3D volume 
	private ParamVolume intensityVolume;// A 3D volume 

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.8 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Log-linear minimum mean squared error tensor estimation.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("Camino");		
		inputParams.setCategory("Modeling.Diffusion.WholeVolume");
		inputParams.setLabel("Camino Three Tensor Estimation");
		inputParams.setName("Camino_Three_Tensor_Estimation");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new Citation("Landman BA, Farrell JA, Jones CK, Smith SA, Prince JL, Mori S. Effects of diffusion weighting schemes on the reproducibility of DTI-derived fractional anisotropy, mean diffusivity, and principal eigenvector measurements at 1.5T. Neuroimage 2007;36(4):1123-1138."));
		info.add(new Citation("Basser, PJ, Jones, DK. \"Diffusion-tensor MRI: Theory, experimental design and data analysis - a technical review.\" NMR Biomed 2002; 15(7-8):456-67"));		
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamVolume("DWI and Reference Image(s) Data (4D)",null,-1,-1,-1,-1));
		inputParams.add(SchemeFile=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"scheme","schemev1"})));
		//		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		//		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
		inputParams.add(Mask3D=new ParamVolume("Mask Volume to Determine Region of Tensor Estimation (3D)",null,-1,-1,-1,1));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.
		inputParams.add(threeTensorFitOption=new ParamOption("Two-Tensor Model Fitter",
				new String[]{"Cylindrically Symmetric","Cylindrically Symmetric (equal fraction)","Positive Definite","Positive Definite (equal fraction)",
				"1xCyl. Sym. + 2xPos. Def.","1xCyl. Sym. + 2xPos. Def. (equal fraction)",
				"2xCyl. Sym. + 1xPos. Def.","2xCyl. Sym. + 1xPos. Def. (equal fraction)"}));
		threeTensorFitOption.setValue("Positive Definite");
		inputParams.add(oneTensorInitOption=new ParamOption("One-Tensor Initialization",new String[]{"Algebraic","Linear","NonLinear","RESTORE","WeightedLinear"}));
		oneTensorInitOption.setValue("Linear");

		inputParams.add(noiseLevel=new ParamFloat("Noise Level (RESTORE only)"));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		mix1Volume = new ParamVolume("Mixture Fraction of Tensor 1",null,-1,-1,-1,1);
		mix1Volume.setName("Mixture Fraction for Tensor 1");
		outputParams.add(mix1Volume);
		tensor1Volume = new ParamVolume("First Tensor Estimate",null,-1,-1,-1,6);
		tensor1Volume.setName("Tensor 1 (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensor1Volume);
		mix2Volume = new ParamVolume("Mixture Fraction of Tensor 2",null,-1,-1,-1,1);
		mix2Volume.setName("Mixture Fraction for Tensor 2");
		outputParams.add(mix2Volume);
		tensor2Volume = new ParamVolume("Second Tensor Estimate",null,-1,-1,-1,6);
		tensor2Volume.setName("Tensor 2 (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensor2Volume);
		mix3Volume = new ParamVolume("Mixture Fraction of Tensor 3",null,-1,-1,-1,1);
		mix3Volume.setName("Mixture Fraction for Tensor 3");
		outputParams.add(mix3Volume);
		tensor3Volume = new ParamVolume("Third Tensor Estimate",null,-1,-1,-1,6);
		tensor3Volume.setName("Tensor 3 (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensor3Volume);
		exitCodeVolume = new ParamVolume("Estimation Exit Code",null,-1,-1,-1,1);
		exitCodeVolume.setName("Exit Code");
		outputParams.add(exitCodeVolume);	
		intensityVolume = new ParamVolume("Intensity Estimate",null,-1,-1,-1,1);
		intensityVolume.setName("Intensity");
		outputParams.add(intensityVolume);
	}


	protected void execute(CalculationMonitor monitor) {
		/****************************************************
		 * Step 1. Indicate that the plugin has started.
		 * 		 	Tip: Use limited System.out.println statements
		 * 			to allow end users to monitor the status of
		 * 			your program and report potential problems/bugs
		 * 			along with information that will allow you to 
		 * 			know when the bug happened.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: Start");

		/****************************************************
		 * Step 2. Parse the input data 
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Load data.");System.out.flush();
		ImageData dwd=DWdata4D.getImageData();
		ImageDataFloat DWFloat=new ImageDataFloat(dwd);

		ImageData maskVol=Mask3D.getImageData();
		byte [][][]mask=null;
		if(maskVol!=null) {
			ImageDataUByte maskByte = new ImageDataUByte (maskVol);
			mask = maskByte.toArray3d();
		}

		System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
		SchemeV1 DTIscheme = null;

		XStream xstream = new XStream();
		xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
		try {
			ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
			DTIscheme=(SchemeV1)in.readObject();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
			

		/****************************************************
		 * Step 3. Perform limited error checking 
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Error checking."); System.out.flush();

		ThreeTensorInversion dtiFit=null;
		String code = "";
	
		ModelIndex threeTensorModelIndex=ModelIndex.POSPOSPOS;
		ModelIndex oneTensorModelIndex=ModelIndex.LDT;

		if(oneTensorInitOption.getValue().compareToIgnoreCase("Algebraic")==0) {
			oneTensorModelIndex=ModelIndex.ALGDT;
			code = "ALGDT";
		}
		if(oneTensorInitOption.getValue().compareToIgnoreCase("Linear")==0) {
			oneTensorModelIndex=ModelIndex.LDT;
			code = "LDT";
		}
		if(oneTensorInitOption.getValue().compareToIgnoreCase("NonLinear")==0) {
			oneTensorModelIndex=ModelIndex.NLDT;
			code = "NLDT";
		}
		if(oneTensorInitOption.getValue().compareToIgnoreCase("RESTORE")==0) {
			oneTensorModelIndex=ModelIndex.RESTORE;
			code = "RESTORE";
		}
		if(oneTensorInitOption.getValue().compareToIgnoreCase("WeightedLinear")==0) {
			oneTensorModelIndex=ModelIndex.LDT_WTD;
			code = "WLDT";
		}

		if(threeTensorFitOption.getValue().compareToIgnoreCase("Cylindrically Symmetric")==0) {
			threeTensorModelIndex=ModelIndex.CYLCYLCYL;
			code = code+"CYLCYLCYL";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("Cylindrically Symmetric (equal fraction)")==0) {
			threeTensorModelIndex=ModelIndex.CYLCYLCYL_EQ;
			code = code+"CYLCYLCYLEQ";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("Positive Definite")==0) {
			threeTensorModelIndex=ModelIndex.POSPOSPOS;
			code = code+"POSPOSPOS";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("Positive Definite (equal fraction)")==0) {
			threeTensorModelIndex=ModelIndex.POSPOSPOS_EQ;
			code = code+"POSPOSPOSEQ";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("1xCyl. Sym. + 2xPos. Def.")==0) {
			threeTensorModelIndex=ModelIndex.POSPOSCYL;
			code = code+"POSPOSCYL";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("1xCyl. Sym. + 2xPos. Def. (equal fraction)")==0) {
			threeTensorModelIndex=ModelIndex.POSPOSCYL_EQ;
			code = code+"POSPOSCYLEQ";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("2xCyl. Sym. + 1xPos. Def.")==0) {
			threeTensorModelIndex=ModelIndex.POSCYLCYL;
			code = code+"POSCYLCYL";
		}
		if(threeTensorFitOption.getValue().compareToIgnoreCase("2xCyl. Sym. + 1xPos. Def. (equal fraction)")==0) {
			threeTensorModelIndex=ModelIndex.POSCYLCYL_EQ;
			code = code+"POSCYLCYLEQ";
		}
		dtiFit=new ThreeTensorInversion(DTIscheme,threeTensorModelIndex,oneTensorModelIndex);
		/****************************************************
		 * Step 4. Run the core algorithm. Note that this program 
		 * 		   has NO knowledge of the MIPAV data structure and 
		 * 		   uses NO MIPAV specific components. This dramatic 
		 * 		   separation is a bit inefficient, but it dramatically 
		 * 		   lower the barriers to code re-use in other applications.
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Allocate memory."); System.out.flush();
		float [][][][]data=DWFloat.toArray4d();
		int rows = data.length;
		int cols= data[0].length;
		int slices= data[0][0].length;
		int components= data[0][0][0].length;
		float [][][][]mix1 = new float[rows][cols][slices][1];
		float [][][][]tensors1 = new float[rows][cols][slices][6];
		float [][][][]mix2 = new float[rows][cols][slices][1];
		float [][][][]tensors2 = new float[rows][cols][slices][6];
		float [][][][]mix3 = new float[rows][cols][slices][1];
		float [][][][]tensors3 = new float[rows][cols][slices][6];
		float [][][][]exitCode= new float[rows][cols][slices][1];
		float [][][][]intensity= new float[rows][cols][slices][1];


		System.out.println(getClass().getCanonicalName()+"\t"+"Run CAMINO estimate."); System.out.flush();
		EstimateTensorLLMSE.estimateCaminoThreeTensor(data,mask,dtiFit,mix1,tensors1,mix2,tensors2,mix3,tensors3,exitCode,intensity);

		/****************************************************
		 * Step 5. Retrieve the image data and put it into a new
		 * 			data structure. Be sure to update the file information
		 * 			so that the resulting image has the correct
		 * 		 	field of view, resolution, etc.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
//		int []ext=DWFloat.getModelImageCopy().getExtents();
//		ModelImage img=null;
		ImageDataFloat out=new ImageDataFloat(tensors1);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Tensor1"+code);
		tensor1Volume.setValue(out);

		out=new ImageDataFloat(tensors2);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Tensor2"+code);
		tensor2Volume.setValue(out);

		out=new ImageDataFloat(tensors3);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Tensor3"+code);	
		tensor3Volume.setValue(out);

		out=new ImageDataFloat(mix1);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Mix1"+code);
		mix1Volume.setValue(out);

		out=new ImageDataFloat(mix2);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Mix2"+code);	
		mix2Volume.setValue(out);

		out=new ImageDataFloat(mix3);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Mix3"+code);	
		mix3Volume.setValue(out);
		
		out=new ImageDataFloat(exitCode);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_ExitCode");	
		exitCodeVolume.setValue(out);

		out=new ImageDataFloat(intensity);
		out.setHeader(DWFloat.getHeader());		
		out.setName(DWdata4D.getName()+"_Intensity");	
		intensityVolume.setValue(out);
		/****************************************************
		 * Step 6. Let the user know that your code is finished.  
		 ****************************************************/
		System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: FINISHED");
	}
}
