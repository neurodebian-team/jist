package edu.jhu.ece.iacl.plugins.dti;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.dti.DTIGradientTableCreator;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;


public class MedicAlgorithmFlipVectorTable extends ProcessingAlgorithm{
	private ParamObject<String> tablein;
	private ParamOption flipDim;
	private ParamObject<String> tableout;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Flips (i.e. negates) the component of the input gradient table.  " +
			"";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(tablein = new ParamObject<String>("Input Gradient Table", new StringReaderWriter()));
		String[] flipoptions = {"X","Y","Z"};
		inputParams.add(flipDim = new ParamOption("Gradient to Flip",flipoptions));

		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI");
		inputParams.setLabel("Flip Gradient Table");
		inputParams.setName("Flip_Gradient_Table");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://iacl.ece.jhu.edu");
		info.add(CommonAuthors.johnBogovic);
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(tableout = new ParamObject<String>("Output Gradient Table", new StringReaderWriter()));
	}


	protected void execute(CalculationMonitor monitor) {
		double[][] table = DTIGradientTableCreator.parseTableFile(tablein.getValue().getAbsolutePath());
		for(int i=0; i<table.length; i++){
			table[i][flipDim.getIndex()]=-table[i][flipDim.getIndex()];
		}
		tableout.setObject(DTIGradientTableCreator.tableToString(table));
		tableout.setFileName(tablein.getValue().getName()+"_Flipped"+flipDim.getValue());
	}
}
