/**
 *
 */
package edu.jhu.ece.iacl.algorithms.volume;

import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;

/**
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class IsotropicResample {
	public enum InterpolationMethod{TRILINEAR,BILINEAR,NEAREST_NEIGHBOR,BSPLINE3,BSPLINE4,CUBIC_LAGRANGIAN,QUINTIC_LAGRANGIAN,HEPTIC_LAGRANGIAN,WSINC};


	public static ImageData resample(ImageData img,InterpolationMethod method){
		ImageHeader header = img.getHeader().clone();
		float[] res = header.getDimResolutions();
		for (int i = 0; i < res.length; i++){
			res[i]=(float)(Math.round(res[i]*1E5)*1E-5);
		}
		
		if(res[0]!=res[1]||res[0]!=res[2]||res[1]!=res[2]){
			ModelImage reslicedImg=reslice(img.getModelImageCopy(),method.ordinal());
			MipavController.setModelImageName(reslicedImg,img.getName()+"_iso");
			ImageDataMipav resultImg = new ImageDataMipavWrapper(reslicedImg);
			float[] newRes = resultImg.getModelImageCopy().getFileInfo(0).getResolutions();
			header.setDimResolutions(newRes);
			resultImg.setHeader(header);
			return resultImg;
		} else {
			return img;
		}
	}


	public static ImageDataMipav resample(ImageDataMipav img, InterpolationMethod method){
		ImageHeader header = img.getHeader().clone();
		float[] res = header.getDimResolutions();
		for (int i = 0; i < res.length; i++){
			res[i]=(float)(Math.round(res[i]*1E5)*1E-5);
		}
		
		if (res[0] != res[1] || res[0] != res[2] || res[1] != res[2]){
			ModelImage reslicedImg = reslice(img.getModelImageCopy(), method.ordinal());
			MipavController.setModelImageName(reslicedImg,img.getName()+"_iso");
			ImageDataMipav resultImg = new ImageDataMipavWrapper(reslicedImg);
//			reslicedImg.disposeLocal();
			reslicedImg = null; // ImageDataMipavWrapper consumes ModelImage objects and handles disposal!
			float[] newRes = resultImg.getModelImageCopy().getFileInfo(0).getResolutions();
			header.setDimResolutions(newRes);
			resultImg.setHeader(header);
			return resultImg;
		} else {
			return img;
		}
	}


	private static ModelImage reslice(ModelImage image,int interp){
		int i, j;
		boolean found;
		int newOrient;
		float ri[] = new float[3];
		int   ni[] = new int[3];
		FileInfoBase fileInfo = (FileInfoBase)(image.getFileInfo()[0].clone());
	
		// set resampled resolutions, dimensions
		ri[0] = image.getFileInfo()[0].getResolutions()[0];
		ri[1] = image.getFileInfo()[0].getResolutions()[1];
		ri[2] = image.getFileInfo()[0].getResolutions()[2];

		ni[0] = image.getExtents()[0];
		ni[1] = image.getExtents()[1];
		ni[2] = image.getExtents()[2];
	
		float r[] = new float[3];
		int   n[] = new int[3];
		for (i = 0; i <= 2; i++) {
			r[i] = ri[i];
			n[i] = ni[i];
		}

		float rn = Math.min(r[0], Math.min(r[1], r[2]));
		n[0] = (int)Math.ceil(n[0]*r[0]/rn);
		r[0] = rn;
		n[1] = (int)Math.ceil(n[1]*r[1]/rn);
		r[1] = rn;
		n[2] = (int)Math.ceil(n[2]*r[2]/rn);
		r[2] = rn;

		double X[][] = new double[4][4];
		for (i = 0; i < 4; i++){
			X[i][i]=1;
		}
		
		for (i = 0; i <= 2; i++) {
			fileInfo.setResolutions(r[i], i);
			fileInfo.setExtents(n[i], i);
		}
		
		TransMatrix transform = new TransMatrix(4);
		transform.setMatrix(0, 2, 0, 3, X);
		AlgorithmTransform algoTrans = new AlgorithmTransform(image, transform, interp, r[0], r[1], r[2], n[0], n[1], n[2], true, false, false);
		algoTrans.setUpdateOriginFlag(true);
		algoTrans.run();
		ModelImage resultImage = algoTrans.getTransformedImage();
		resultImage.calcMinMax();
		return resultImage;
	}
}
