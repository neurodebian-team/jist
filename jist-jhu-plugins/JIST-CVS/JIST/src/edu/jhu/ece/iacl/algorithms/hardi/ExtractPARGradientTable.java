package edu.jhu.ece.iacl.algorithms.hardi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.vecmath.Point3f;

import Jama.Matrix;
import edu.jhu.ece.iacl.algorithms.dti.ParV4Info;

public class ExtractPARGradientTable {
	
	static Matrix getAngCorrection(double ap, double fh, double rl) {
		rl = rl*Math.PI/180;
		fh = fh*Math.PI/180;
		ap = ap*Math.PI/180;
		Matrix Trl = new Matrix(new double[][]{{1.0,0,0},{0,Math.cos(rl),-Math.sin(rl)},{0,Math.sin(rl),Math.cos(rl)}});
		Matrix Tap = new Matrix(new double[][]{{Math.cos(ap),0,Math.sin(ap)},{0,1,0},{-Math.sin(ap),0,Math.cos(ap)}});
		Matrix Tfh = new Matrix(new double[][]{{Math.cos(fh),-Math.sin(fh),0},{Math.sin(fh),Math.cos(fh),0,},{0,0,1}});
		return Tfh.times(Tap.times(Trl));
	}
	static public void ExtractPARGradientTable(String filename, Vector<Float> bval, Vector<Point3f> grad) {
		filename = ParV4Info.toPAR(filename); // rec->par, just in case
		try { 
			BufferedReader rdr = new BufferedReader(new FileReader(new File(filename)));
			String thisline = rdr.readLine();
			boolean readingDefinitions = false;
			int offset = 0;
			int diffusionWeightingOffset =-1;
			int diffusionDirectionOffset =-1;
			int orientationOffset=-1;
			
			int angulationOffset=-1;
			Matrix angCorrection=null;
//			Vector<Float> bval = new Vector<Float>();
//			Vector<Point3f> grad = new Vector<Point3f>();
			while(!thisline.contains("END OF DATA DESCRIPTION FILE")){
				thisline = rdr.readLine();
				if(!readingDefinitions) {
					if(thisline.contains("IMAGE INFORMATION DEFINITION")) 
						readingDefinitions=true;
				} else {
					if(thisline.startsWith("#")) {

						if(thisline.contains("diffusion_b_factor"))
							diffusionWeightingOffset=offset;
						if(thisline.contains("diffusion (ap, fh, rl)"))
							diffusionDirectionOffset=offset;
						
						if(thisline.contains("slice orientation ( TRA/SAG/COR )"))
							orientationOffset = offset;
						if(thisline.contains("image angulation"))
							angulationOffset=offset;
						if(thisline.contains("(integer)")) {
							offset+=1;
						} else if(thisline.contains("(2*integer)")) {
							offset+=2;
						} else if(thisline.contains("(3*integer)")) {
							offset+=3;
						} else if(thisline.contains("(float)")) {
							offset+=1;
						} else if(thisline.contains("(2*float)")) {
							offset+=2;
						} else if(thisline.contains("(3*float)")) {
							offset+=3;
						} else if(thisline.contains("(string)")) {
							offset+=1;
						} 
					} else {
						if(thisline.trim().length()>0) {
							
							String []nums = thisline.trim().split("[^\\-\\.0-9\\w]+");

							if((diffusionWeightingOffset<0 || diffusionWeightingOffset>=nums.length))
								throw new IOException("File does not contain diffusion weighting info.");
							if((diffusionDirectionOffset<0 || diffusionDirectionOffset>=nums.length))
								throw new IOException("File does not contain diffusion direction info.");
							if((orientationOffset<0 || orientationOffset>=nums.length))
								throw new IOException("File does not contain slice orientation info.");
							if(new Float(nums[0]).floatValue()==1) {
								int x,y,z;
								switch(Integer.valueOf(nums[orientationOffset])) {
								case 2: //COR
									x = diffusionDirectionOffset+2; //rl
									y = diffusionDirectionOffset+1; //fh
									z = diffusionDirectionOffset; //ap
								case 1: //SAG
									z = diffusionDirectionOffset+2; //rl
									y = diffusionDirectionOffset+1; //fh
									x = diffusionDirectionOffset; //ap
								case 0: //TRA
									default:
									x = diffusionDirectionOffset+2; //rl
									z = diffusionDirectionOffset+1; //fh
									y = diffusionDirectionOffset; //ap
								}
								if(angCorrection==null) {
									angCorrection=getAngCorrection(Double.valueOf(nums[angulationOffset]), 
											Double.valueOf(nums[angulationOffset+1]),
											Double.valueOf(nums[angulationOffset+2])).inverse();
								}
								
								Matrix ang =angCorrection.times((new Matrix(new double[][]{{Double.valueOf(nums[x]).doubleValue(),
									Double.valueOf(nums[y]).doubleValue(),Double.valueOf(nums[z]).doubleValue()}})).transpose()); 
								
								//we're only looking at the 1st slice
								bval.add(new Float(nums[diffusionWeightingOffset]));
								grad.add(new Point3f(
										(float)ang.get(0,0),
										(float)ang.get(1,0),
										(float)ang.get(2,0)));
							}
						}
					}
				}
			}
			rdr.close();

		} catch (IOException e) {
			throw new RuntimeException("File I/O Error:"+filename+"\n"+e.toString());
		}
	}

//	public static void main(String []args) {
//		ExtractPARGradientTable p = new ExtractPARGradientTable("C:\\Users\\bennett\\Desktop\\20080216qr_10_1.par");
//	}
}
