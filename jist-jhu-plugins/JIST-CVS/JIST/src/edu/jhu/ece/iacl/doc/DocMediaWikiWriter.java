package edu.jhu.ece.iacl.doc;

import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamModel;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;

/**
 * Class that writes module information in a mediawiki-text format
 * 
 * @author John Bogovic (bogovic@jhu.edu)
 */

public class DocMediaWikiWriter {

	ProcessingAlgorithm alg;
	boolean pictureavailable = false;
	String algname;
	
	public DocMediaWikiWriter(ProcessingAlgorithm alg){
		this.alg=alg;
		algname = alg.getAlgorithmLabel();
	}
	
	public String getDocument(){
		String doc = "<meta name=\"title\" content=\""+ algname +"\"/>\n{{TOCright}}\n";;
		doc += getHierLinks();
		doc += "{{h2|"+algname+"}}\n";
		doc += getDescription();
		doc += getStatus();
		doc += getCitations();
		doc += getParamInfo(alg.getInput().getValue(),true);
		doc += getParamInfo(alg.getOutput().getValue(),false);
		doc += getLongDescription();
		return doc;
		
	}
	
	public String getHierLinks(){
		//use CATEGORY!
		String links = "package ";
		String category = alg.getInput().getCategory();
		if(category!=null){
			String[] folders;
			if(category.contains(".")){
				folders = alg.getInput().getCategory().replace('.', '@').split("@");
			}else{
				folders = new String[]{alg.getInput().getCategory()};
			}
			
			//create a link for each level of the hierarchy that this
			//algorithm belongs to
			for(int i=0; i<folders.length; i++){
				links += "[["+folders[i]+"|"+folders[i]+"]]";
				if(i<folders.length-1){
					links += ".";
				}
			}
			links += "\n";
		}

		return links;
	}
	public String addPicture(String imagename){
		if(pictureavailable){
			return "[[Image:"+imagename+"]]<br\\>\n";
		}else{
			return "";
		}
	}
	public String getStatus(){
		return "{{h3|STATUS}}\n";
	}
	public String getDescription(){
		if(alg.getAlgorithmInformation().getDescription()!=null){
			return alg.getAlgorithmInformation().getDescription()+"<br\\><br\\>\n";
		}else{
			return "";
		}
	}
	public String getCitations(){
		String cit =  "{{h3|PREFERRED CITATIONS}}\n";
		for(Citation c : alg.getAlgorithmInformation().getCitations()){
			if(c.getText().length()>0){
				cit += "*"+c.getText()+"<br\\>\n";
			}
		}
		cit+="<br\\><br\\>\n";
		return cit;
	}
	public String getParamInfo(List<ParamModel> paramlist, boolean input){
		String table = "";
		int start = 2;
		if(input){
			table += "{{h3|INPUTS}}\n";
		}else{
			table += "{{h3|OUTPUTS}}\n";
			start = 1;
		}
		table+="{| border=\"1\" cellpadding=\"5\" cellspacing=\"0\"\n";
		table+="! Name !! Type !! File Extension !! Mandatory  !! Description\n|-\n";
		
		
		for(int i=start; i<paramlist.size(); i++){
			ParamModel c = paramlist.get(i);
			table += "|"+c.getName()+"\n";

			//ADD TYPE OF INPUT TO TABLE
			table += "|" + c.getClass().getSimpleName().replace("Param", "")+"\n";

			//ADD ADDITIONAL INFORMATION IF RELEVANT
			/*
			 *  VOLUME 
			 */
			if(c.getClass().getSimpleName().equals("ParamVolume")){
				ParamVolume v = ((ParamVolume)c);
				String rows = "";
				if(v.getRows()==-1){
					rows="X";
				}else{
					rows=""+v.getRows();
				}
				String cols = "";
				if(v.getCols()==-1){
					cols="Y";
				}else{
					cols=""+v.getCols();
				}
				String slcs = "";
				if(v.getSlices()==-1){
					slcs="Z";
				}else{
					slcs=""+v.getSlices();
				}
				String cmps = "";
				if(v.getComponents()==-1){
					cmps="T";
				}else{
					cmps=""+v.getComponents();
				}
				table += "*''dim{"+rows+","+cols+","+slcs+","+cmps+"}''\n";
				if(v.getVoxelType()==null){
					table += "*''type{any}''\n";
				}else{
					table += "*''type{"+v.getVoxelType()+"}''\n";
				}
				
				// Add file extension
				if(input){
					table += "|[[VolumeFileExtensions|Valid Extensions]]\n";
				}else{
					table += "|" + v.getExtensionFilter().getPreferredExtension() + "\n";
				}
				
				
				
				/*
				 *  OPTION 
				 */
			}else if(c.getClass().getSimpleName().equals("ParamOption")){
				table += "\n";
				ParamOption o = (ParamOption)c;
				for(String option : o.getOptions()){
					table += "#''"+option+"''\n";
				}
				table += "|.output\n";
				
				
				/*
				 *  OBJECT 
				 */
			}else if(c.getClass().getSimpleName().equals("ParamObject")){
//				System.out.println(getClass().getCanonicalName()+"\t"+c.getClass().getSimpleName());
				ParamObject o = (ParamObject)c;
				if(o.getReaderWriter()!=null){
					String type = o.getReaderWriter().toString().replace("edu.jhu.ece.iacl.io.", "").replaceAll("ReaderWriter.*", "");
					table += "*''" + type +"''\n";
				}

				if(input){
					table += "|\n";
					List<String> exts = o.getExtensionFilter().getExtensions();
					for(String s : exts){
						table += "*"+s+"\n";
					}
				}else{
					table += "|" + o.getExtensionFilter().getPreferredExtension() + "\n";
				}
			
			}else{
				table += "|\n";
			}

			//ADD FILE EXTENSIONS TO TABLE
//			input += "|"+c.
//			c.get
			
			//ADD IS MANDATORY
			table += "|" + c.isMandatory()+"\n";
			if(c.getDescription()==null){
				table += "|\n";
			}else if(c.getDescription().equals(c.getName())){
				table += "| - \n";
			}
			else{
				table += "|"+c.getDescription()+"\n";
			}
			table+="|-\n";

		}
		table+="|}<br\\>\n";
		table+="\n";
		return table;
	}
	
	
	public String getLongDescription(){
		if(alg.getAlgorithmInformation().getLongDescription()!=null){
			return "{{h3|DESCRIPTION}}\n"+alg.getAlgorithmInformation().getLongDescription()+"<br\\><br\\>\n";
		}else{
			return "";
		}
	}
	
	public String getSeeAlsos(){
		return "{{h3|SEE ALSO}}\n";
	}
	
}
