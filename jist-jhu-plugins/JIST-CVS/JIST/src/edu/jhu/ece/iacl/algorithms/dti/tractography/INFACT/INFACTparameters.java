package edu.jhu.ece.iacl.algorithms.dti.tractography.INFACT;


import java.io.*;

import javax.vecmath.Point3f;


public class INFACTparameters {
//    public MultiTensor mt;
    public float[][][] 		faData;
    public float[][][][] 	mixFrac;
    public int[][][][]		basisInds;
    public Point3f[]		basisDirs;	
    
    //Parameters:
    public float stopFA=0.4f;
    public float maxTurnAngle=(float)(70*Math.PI/180);
    public float startFA=0.4f;
    public int Nx,Ny,Nz,Nt,minLength,maxLength,numBasisDirs;
    public float resX,resY,resZ;
    public boolean useFA, useRK, seedMulti;
    
    //helpful variables
    private static final Point3f origin = new Point3f();
	private static final double pi =Math.PI;
	private static final double pi2 =pi/2;
    
    public void readParameters(String file){
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			for(int i=0; i<15; i++){
				String s = in.readLine();
				String val = s.substring(s.indexOf(":")+1);
				if(i==0){
					startFA = Float.valueOf(val);
				}else if(i==1){
					stopFA = Float.valueOf(val);
				}
				else if(i==2){
					maxTurnAngle = (float)(Float.valueOf(val)*Math.PI/180);
				}
				else if(i==3){
					Nx = Integer.valueOf(val);
				}
				else if(i==4){
					Ny = Integer.valueOf(val);
				}
				else if(i==5){
					Nz = Integer.valueOf(val);
				}
				else if(i==6){
					Nt = Integer.valueOf(val);
				}
				else if(i==7){
					resX = Float.valueOf(val);
				}
				else if(i==8){
					resY = Float.valueOf(val);
				}
				else if(i==9){
					resZ = Float.valueOf(val);
				}
				else if(i==10){
					minLength = Integer.valueOf(val);
				}
				else if(i==11){
					maxLength = Integer.valueOf(val);
				}
				else if (i==12){
					if(Integer.valueOf(val)==1){
						useFA=true;
					}else{ useFA=false; }
				}
				else if(i==13){
					if(Integer.valueOf(val)==1){
						useRK=true;
					}else{ useRK=false; }
				}
				else if(i==14){
					if(Integer.valueOf(val)==1){
						seedMulti=true;
					}else{ seedMulti=false; }
				}
				
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	    System.out.println(startFA);
	    System.out.println(stopFA);
	    System.out.println(maxTurnAngle);
	    System.out.println(Nx);
	    System.out.println(Ny);
	    System.out.println(Nz);
	    System.out.println(Nt);
	    System.out.println(resX);
	    System.out.println(resY);
	    System.out.println(resZ);
	    System.out.println(minLength);
	    System.out.println(maxLength);
	    System.out.println(useFA);
	    System.out.println(useRK);
	    System.out.println(seedMulti);
	    
    }
    
    public void setDefaultParameters(){
    	startFA = 0.2f;
    	stopFA = 0.2f;
    	maxTurnAngle = (float)(70*Math.PI/180);
    	minLength=6;
    	maxLength=1000;
    	Nx = 256;
    	Ny = 256;
    	Nz = 65;
    	Nt = 10;
    	resX = 0.828125f;
    	resY = 0.828125f;
    	resZ = 0.828125f;
    	useFA=true;
    	useRK=false;
    	seedMulti=true;

    }
    
	public Point3f direction_minAngle(int x, int y, int z, float[] vecin){
		Point3f vecout = new Point3f();
		double minang = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			double ang = vectAng(vecin[0],vecin[1],vecin[2],basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(ang<minang){
				minang = ang;
				vecout.x=basisDirs[basisInds[x][y][z][i]].x;
				vecout.y=basisDirs[basisInds[x][y][z][i]].y;
				vecout.z=basisDirs[basisInds[x][y][z][i]].z;
			}
		}
		return normalize(vecout);
	}
	
	public Point3f direction_minAngle(int x, int y, int z, float vecinx, float veciny, float vecinz){
		Point3f vecout = new Point3f();
		double minang = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			double ang = vectAng(vecinx,veciny,vecinz,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(ang<minang){
				minang = ang;
				vecout.x=basisDirs[basisInds[x][y][z][i]].x;
				vecout.y=basisDirs[basisInds[x][y][z][i]].y;
				vecout.z=basisDirs[basisInds[x][y][z][i]].z;
			}
		}
		return normalize(vecout);
	}
	
	public Point3f direction_maxMag(int x, int y, int z){
		Point3f vecout = new Point3f();
		vecout.x=basisDirs[basisInds[x][y][z][0]].x;
		vecout.y=basisDirs[basisInds[x][y][z][0]].y;
		vecout.z=basisDirs[basisInds[x][y][z][0]].z;
		return vecout;
	}
	
	public Point3f direction_ODF_magang(int x, int y, int z, float[] vecin, float maxang){
		Point3f vecout = new Point3f();
		double maxweight = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			float thisang = vectAng(vecin[0],vecin[1],vecin[2],basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(thisang<maxang){
				float thisweight = mixFrac[x][y][z][i]*thisang;
				if(thisweight>maxweight){
					maxweight=thisweight;
					vecout.x=basisDirs[basisInds[x][y][z][i]].x;
					vecout.y=basisDirs[basisInds[x][y][z][i]].y;
					vecout.z=basisDirs[basisInds[x][y][z][i]].z;
				}
			}
		}
		return vecout;
	}
	
	public Point3f direction_ODF_magL2(int x, int y, int z, Point3f vecin, float maxang){
		Point3f vecout = new Point3f();
		double maxweight = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			Point3f vec = new Point3f(basisDirs[basisInds[x][y][z][i]].x, basisDirs[basisInds[x][y][z][i]].y, basisDirs[basisInds[x][y][z][i]].z);
			double thisang = vectAng(vecin.x,vecin.y,vecin.z,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(thisang<maxang){
				vec.negate();
				Point3f magme = ((Point3f)vecin.clone());
				magme.add(vec);
				double magdiff = magnitude(magme);
				double thisweight = mixFrac[x][y][z][i]*magdiff;
				if(thisweight>maxweight){
					maxweight=thisweight;
					vecout.x=basisDirs[basisInds[x][y][z][i]].x;
					vecout.y=basisDirs[basisInds[x][y][z][i]].y;
					vecout.z=basisDirs[basisInds[x][y][z][i]].z;
				}
			}
		}
		return vecout;
	}
	
	public Point3f direction_ODF_magExpAng(int x, int y, int z, float[] vecin, float maxang){
		Point3f vecout = new Point3f();
		double maxweight = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			double thisang = vectAng(vecin[0],vecin[1],vecin[2],basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(thisang<maxang){
				double thisweight = mixFrac[x][y][z][i]*Math.exp(-(thisang*thisang));
				if(thisweight>maxweight){
					maxweight=thisweight;
					vecout.x=basisDirs[basisInds[x][y][z][i]].x;
					vecout.y=basisDirs[basisInds[x][y][z][i]].y;
					vecout.z=basisDirs[basisInds[x][y][z][i]].z;
				}
			}
		}
		return vecout;
	}
	
	public Point3f direction_ODF_magExpAng(int x, int y, int z, float vxin, float vyin, float vzin , float maxang){
		Point3f vecout = new Point3f();
		double maxweight = Double.MIN_VALUE;

		for(int i=0; i<basisDirs.length; i++){
			double thisang = vectAng(vxin,vyin,vzin,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
            float vx = basisDirs[basisInds[x][y][z][i]].x;
            float vy = basisDirs[basisInds[x][y][z][i]].y;
            float vz = basisDirs[basisInds[x][y][z][i]].z;
            if(vx==0 && vy==0 && vz==0){
            	vecout.x=0;
				vecout.y=0;
				vecout.z=0;
				return vecout;
            }
			if(thisang>pi2) {
                thisang = pi-thisang;
                vx=-vx;
                vy=-vy;
                vz=-vz;
            }
			if(thisang<maxang){
				double thisweight = mixFrac[x][y][z][i]*Math.exp(-(thisang*thisang))*(1/Math.exp(1));
				
				if(thisweight>maxweight){
					maxweight=thisweight;
					vecout.x=vx;
					vecout.y=vy;
					vecout.z=vz;
				}
			}
		}

		return vecout;
	}
	
	public Point3f direction_ODF_test(int x, int y, int z, float vxin, float vyin, float vzin){
		Point3f vecout = new Point3f();
		double maxweight = Double.MIN_VALUE;

		for(int i=0; i<basisDirs.length; i++){
			double thisang = vectAng(vxin,vyin,vzin,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
            float vx = basisDirs[basisInds[x][y][z][i]].x;
            float vy = basisDirs[basisInds[x][y][z][i]].y;
            float vz = basisDirs[basisInds[x][y][z][i]].z;
            if(vx==0 && vy==0 && vz==0){
            	vecout.x=0;
				vecout.y=0;
				vecout.z=0;
				return vecout;
            }
			if(thisang>pi2) {
                thisang = pi-thisang;
                vx=-vx;
                vy=-vy;
                vz=-vz;
			}
//			System.out.println(10);
			double thisweight = mixFrac[x][y][z][i]+Math.exp(-(thisang*thisang))*(1/Math.exp(1));
			if(thisweight>maxweight){
				maxweight=thisweight;
				vecout.x=vx;
				vecout.y=vy;
				vecout.z=vz;
			}

		}

		return vecout;
	}
	
	public Point3f direction_ODF_magGaussL2(int x, int y, int z, Point3f vecin, float maxang){
		Point3f vecout = new Point3f();
		double minweight = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			Point3f vec = new Point3f(basisDirs[basisInds[x][y][z][i]].x, 
					basisDirs[basisInds[x][y][z][i]].y, basisDirs[basisInds[x][y][z][i]].z);
			double thisang = vectAng(vecin.x,vecin.x,vecin.z,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(thisang<maxang){
				vec.negate();
				Point3f magme = ((Point3f)vecin.clone());
				magme.add(vec);
				double magdiff = magnitude(magme);
				double thisweight = mixFrac[x][y][z][i]*Math.exp(magdiff*magdiff);
				if(thisweight<minweight){
					minweight=thisweight;
					vecout.x=basisDirs[basisInds[x][y][z][i]].x;
					vecout.y=basisDirs[basisInds[x][y][z][i]].y;
					vecout.z=basisDirs[basisInds[x][y][z][i]].z;
				}
			}
		}
		return vecout;
	}
	public Point3f direction_ODF_magGaussL2(int x, int y, int z, float vxin, float vyin, float vzin, float maxang){
		return direction_ODF_magGaussL2(x,y,z,new Point3f(vxin,vyin,vzin),maxang);
	}
	
	public Point3f direction_ODF_magGaussL2eps(int x, int y, int z, Point3f vecin, float maxang, float eps){
		Point3f vecout = new Point3f();
		double minweight = Double.MAX_VALUE;
		for(int i=0; i<basisDirs.length; i++){
			Point3f vec = new Point3f(basisDirs[basisInds[x][y][z][i]].x,
					basisDirs[basisInds[x][y][z][i]].y, basisDirs[basisInds[x][y][z][i]].z);
			double thisang = vectAng(vecin.x,vecin.y,vecin.z,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			if(thisang<maxang){
				vec.negate();
				Point3f magme = ((Point3f)vecin.clone());
				magme.add(vec);
				double magdiff = magnitude(magme);
				double thisweight = 0;
				if(magdiff*magdiff<eps){
					thisweight = mixFrac[x][y][z][i];
				}else{
					thisweight = mixFrac[x][y][z][i]*Math.exp(magdiff*magdiff);
				}
				if(thisweight<minweight){
					minweight=thisweight;
					vecout.x=basisDirs[basisInds[x][y][z][i]].x;
					vecout.y=basisDirs[basisInds[x][y][z][i]].y;
					vecout.z=basisDirs[basisInds[x][y][z][i]].z;
				}
			}
		}
		return vecout;
	}
	
	public Point3f direction_ODF_magCosAng(int x, int y, int z, float vxin, float vyin, float vzin , float maxang){
		Point3f vecout = new Point3f();
		double maxweight = Double.MIN_VALUE;

		for(int i=0; i<basisDirs.length; i++){
			double thisang = vectAng(vxin,vyin,vzin,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
            float vx = basisDirs[basisInds[x][y][z][i]].x;
            float vy = basisDirs[basisInds[x][y][z][i]].y;
            float vz = basisDirs[basisInds[x][y][z][i]].z;
            if(vx==0 && vy==0 && vz==0){
            	vecout.x=0;
				vecout.y=0;
				vecout.z=0;
				return vecout;
            }
			if(thisang>pi2) {
                thisang = pi-thisang;
                vx=-vx;
                vy=-vy;
                vz=-vz;
            }
			if(thisang<maxang){
				double ca = Math.cos((thisang));
				double thisweight = mixFrac[x][y][z][i]*ca*ca*ca*ca;
				if(thisweight>maxweight){
					maxweight=thisweight;
					vecout.x=vx;
					vecout.y=vy;
					vecout.z=vz;
				}
			}
		}

		return vecout;
	}
	
	public Point3f direction_ODF_Pilou(int x, int y, int z, float vxin, float vyin, float vzin , float maxang){
		Point3f vecout = new Point3f();
		double maxweight = Double.MIN_VALUE;

		for(int i=0; i<basisDirs.length; i++){
			double thisang = vectAng(vxin,vyin,vzin,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
            float vx = basisDirs[basisInds[x][y][z][i]].x;
            float vy = basisDirs[basisInds[x][y][z][i]].y;
            float vz = basisDirs[basisInds[x][y][z][i]].z;
            if(vx==0 && vy==0 && vz==0){
            	vecout.x=0;
				vecout.y=0;
				vecout.z=0;
				return vecout;
            }
			if(thisang>pi2) {
                thisang = pi-thisang;
                vx=-vx;
                vy=-vy;
                vz=-vz;
            }
			if(thisang<maxang){
				double ca = Math.cos((thisang));
				double thisweight = mixFrac[x][y][z][i]*ca*ca*ca*ca;
				if(thisweight>maxweight){
					maxweight=thisweight;
					vecout.x=vecout.x+(float)thisweight*vx;
					vecout.y=vecout.y+(float)thisweight*vy;;
					vecout.z=vecout.z+(float)thisweight*vz;;
				}
			}
		}

		return normalize(vecout);
	}
	
	public Point3f direction_ODF_magExpDot(int x, int y, int z, float vxin, float vyin, float vzin , float maxang, float Bet){
		Point3f vecout = new Point3f();
		double maxweight = Double.MIN_VALUE;

		for(int i=0; i<basisDirs.length; i++){
//			double thisang = vectAng(vxin,vyin,vzin,basisDirs[basisInds[x][y][z][i]].x,basisDirs[basisInds[x][y][z][i]].y,basisDirs[basisInds[x][y][z][i]].z);
			float vx = basisDirs[basisInds[x][y][z][i]].x;
			float vy = basisDirs[basisInds[x][y][z][i]].y;
			float vz = basisDirs[basisInds[x][y][z][i]].z;
			if(vx==0 && vy==0 && vz==0){
				vecout.x=0;
				vecout.y=0;
				vecout.z=0;
				return vecout;
			}
			double adot = Math.abs(vx*vxin +vy*vyin+vz*vzin);
//			if(thisang<maxang)
			float thisweight = (float)(mixFrac[x][y][z][i]*Math.exp(-adot*adot));
//			System.out.println(thisweight);
//			System.out.println("***********");
			if(thisweight>maxweight){
				maxweight=thisweight;
				vecout.x=vx;
				vecout.y=vy;
				vecout.z=vz;
			}
		}
//		}
//		System.out.println("***********");
		return vecout;
	}
	
    private static float vectAng(double vx1, double vy1, double vz1, double vx2, double vy2, double vz2) {
        double dv1 = Math.sqrt(vx1*vx1+vy1*vy1+vz1*vz1);
        double dv2 = Math.sqrt(vx2*vx2+vy2*vy2+vz2*vz2);
        if((dv1==0)||(dv2==0))
            return -1;
        vx1=vx1/dv1;
        vy1=vy1/dv1;
        vz1=vz1/dv1;
        vx2=vx2/dv2;
        vy2=vy2/dv2;
        vz2=vz2/dv2;
        //finite precision can result in normalized sums greater than +-1. catch these with the min
        return (float)Math.acos(Math.max(-1.0f,Math.min(vx1*vx2+vy1*vy2+vz1*vz2,1.0f)));
    }
    
	public Point3f normalize(Point3f in){
		float norm2 = in.distance(origin);
		in.scale(1/norm2);
		return in;
	}
	public float magnitude(Point3f in){
		return in.distance(origin);
	}

}

