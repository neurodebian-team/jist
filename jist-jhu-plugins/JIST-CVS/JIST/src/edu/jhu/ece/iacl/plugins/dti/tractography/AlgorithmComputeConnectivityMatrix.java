package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.vecmath.Point2i;

import edu.jhu.ece.iacl.algorithms.dti.tractography.ConnectivityMatrix;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;


public class AlgorithmComputeConnectivityMatrix extends ProcessingAlgorithm {
	//input params
	private ParamFileCollection labelPairStats;
	private ParamObject<double[][]> labelList;
	private ParamOption tractStats;

	//output params
	private ParamFileCollection connMtxs;
	private ParamObject<double[][]> labelsout;

	// params to be passed into algorithm
	ArrayList<double[]> fiberdat;
	ArrayList<Point2i> labelpairs;
	ArrayList<Integer> alllabels;

	//were the labels of interest input?
	boolean labelsinput = false;

	//maps
	private HashMap<String,String> fiberstatmap = new HashMap<String,String>();

	//io
	private ArrayDoubleTxtReaderWriter adrw = ArrayDoubleTxtReaderWriter.getInstance();

	//version info 
	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Given the output of the label pair fibers from  the \"Apply Fiber ROI\" module, organizes the statistic into a connectivity matrix.  A statistic (given by the \"Connection Statistic\" input) is computed for the set of input fibers data provided for each label pair.";
	private static final String longDescription = "";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(labelPairStats = new ParamFileCollection("Fiber Collection Statistics", new FileExtensionFilter(new String[]{"csv","txt"})));
		labelPairStats.setDescription("This module was designed to accept the ouput of the \"Apply Fiber ROI\" module with " +
				"in the \"Pair\" mode");

		inputParams.add(labelList = new ParamObject<double[][]>("Label List"));
		labelList.setMandatory(false);
		inputParams.add(tractStats = new ParamOption("Connection Statistics", 
				new String[]{"Mean","Median" ,"Max","Min", "Std Dev"}));
		tractStats.setValue(0);


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Compute Connectivity Matrix");
		inputParams.setName("Compute_Connectivity_Matrix");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu","http://putter.ece.jhu.edu/John"));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(connMtxs = new ParamFileCollection("Connectivity Matrices", new FileExtensionFilter(new String[]{"csv","txt"})));
		outputParams.add(labelsout = new ParamObject<double[][]>("Label List", new ArrayDoubleTxtReaderWriter()));
	}


	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		File dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){
			e.printStackTrace();
		}

//		initMap();

		//read label list if present
		if(labelList.getValue()!=null){
			double[][] templabels = labelList.getObject();
			for(int i=0; i<templabels.length; i++){
				alllabels.add((int)templabels[i][0]);
			}
			labelsinput = true;
		}else{
			alllabels = new ArrayList<Integer>();
		}

		getAllPairs(tractStats.getValue());

		Collections.sort(alllabels);

		System.out.println(getClass().getCanonicalName()+"\t"+"Labels: " + alllabels);
		double[][] cmtx = ConnectivityMatrix.compute(fiberdat, labelpairs, alllabels, (byte)tractStats.getIndex());
//		double[][] d = new double[][]{{1,2},{3,4}};
		connMtxs.add(adrw.write(cmtx, new File(dir+File.separator+"testfile.csv")));

		labelsout.setObject(toArray(alllabels));
		labelsout.setFileName("labels.csv");
	}


	private void getAllPairs(String fibstat){
		//look at the input file collection and pick out the different pairs for various
		//fiber statistics
		
		//get the list of files matching the statistic
//		ArrayList<File> matchfibstat = new ArrayList<File>();
//		System.out.println(getClass().getCanonicalName()+"\t"+fibstat + " goes to " + fiberstatmap.get(fibstat));
//		Pattern p = Pattern.compile(fiberstatmap.get(fibstat));
//		
//		for(File f : labelPairStats.getValue()){
//			Matcher m = p.matcher(f.getName());
//			if(m.find()){
//				System.out.println(getClass().getCanonicalName()+"\t"+"found match");
//				matchfibstat.add(f);
//			}
//			
//		}
//		matchfibstat.trimToSize();
//		System.out.println(getClass().getCanonicalName()+"\t"+matchfibstat);

		//get the list of data for each file
		//and the list of label pairs
		fiberdat = new ArrayList<double[]>(labelPairStats.getValue().size());
		labelpairs = new ArrayList<Point2i>(labelPairStats.getValue().size());
		for(File f : labelPairStats.getValue()){
			fiberdat.add(getFiberDat(f));
			labelpairs.add(getPair(f));
		}

		System.out.println(getClass().getCanonicalName()+"\t"+labelpairs);
	}


	private double[] getFiberDat(File f){
		double[][] alldat = adrw.read(f);
		if(alldat.length>0){
			if(alldat[0].length>1){
				System.err.println("jist.plugins"+"Warning: more than 1 column");
			}
			double[] fibdat = new double[alldat.length];
			for(int i=0; i<alldat.length; i++){
				fibdat[i]=alldat[i][0];
			}
			return fibdat;
		}else{
			return null;
		}
	}


	private Point2i getPair(File f){
		String exp1 = "_OR_\\d+";
		String exp2 = "\\d+";
		Pattern p1 = Pattern.compile(exp1);
		Pattern p2 = Pattern.compile(exp2);
		Matcher m1 = p1.matcher(f.getName());
		int i=0;
		int lab = -1;
		Point2i labelpair = new Point2i();

		if(labelsinput){
			//Here we only add labels if they 
			//are in the input list of labels
			while(m1.find()){
				Matcher m2 = p2.matcher(m1.group());
				if(m2.find()){
					lab = Integer.parseInt(m2.group());
//					System.out.println(getClass().getCanonicalName()+"\t"+"Considering label: " + lab);
					if(alllabels.contains(lab)){
						if(i==0){
							labelpair.x = lab;
						}else if(i==1){
							labelpair.y = lab;
						}else{
							System.err.println("jist.plugins"+"More than one label match");
						}
					}else{
						System.out.println(getClass().getCanonicalName()+"\t"+"Label found not in list");
					}
				}
				i++;
			}
		}else{
			//Here we modify the list of labels using 
			//the labels we find in the file names

			while(m1.find()){
				Matcher m2 = p2.matcher(m1.group());
				if(m2.find()){
					lab = Integer.parseInt(m2.group());
//					System.out.println(getClass().getCanonicalName()+"\t"+"Considering label: " + lab);

					//update the label list if we've found a new label
					if(!alllabels.contains(lab)){
						alllabels.add(lab);
					}

					//add the label pair
					if(i==0){
						labelpair.x = lab;
					}else if(i==1){
						labelpair.y = lab;
					}else{
						System.err.println("jist.plugins"+"More than one label match");
					}
				}
				i++;
			}
		}

		return labelpair;
	}


	private double[][] toArray(ArrayList<Integer> in ){
		double[][] out = new double[in.size()][1];
		for(int i=0; i<in.size(); i++){
			out[i][0]=in.get(i);
		}
		return out;
	}

//	private void initMap(){
//		fiberstatmap.put("Mean", "meandata");
//		fiberstatmap.put("Min", "mindata");
//		fiberstatmap.put("Max", "maxdata");
//		fiberstatmap.put("Median", "mediandata");
//		fiberstatmap.put("Std Dev", "stdevdata");
//		
//	}

}
