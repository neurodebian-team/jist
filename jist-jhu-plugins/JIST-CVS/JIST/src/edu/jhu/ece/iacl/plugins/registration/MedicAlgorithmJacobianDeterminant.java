package edu.jhu.ece.iacl.plugins.registration;

import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.utility.ArrayUtil;

public class MedicAlgorithmJacobianDeterminant extends ProcessingAlgorithm{
	
	ParamVolume defvol;
	
	ParamVolume jacdetmap;
	
	private static final String rcsid =
		"$Id: MedicAlgorithmJacobianDeterminant.java,v 1.2 2010/07/23 20:07:19 bogovic Exp $";
	private static final String cvsversion =
		"$Revision: 1.2 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "");


	private static final String shortDescription = "Computes the determinant of the jacobian" +
			"of an input deformation field.\n";
	private static final String longDescription = "";
	
	
	
	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		
		inputParams.add(defvol = new ParamVolume("Deformation Field",VoxelType.FLOAT,-1,-1,-1,3));
		
		inputParams.setLabel("Deformation Jacobian Determinant");
		inputParams.setName("deformation_jacobian_det");
		
		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");
		
		AlgorithmInformation info=getAlgorithmInformation();
		info.setStatus(DevelopmentStatus.ALPHA);
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.com","http://iacl.ece.jhu.edu/John"));
		
	}
	
	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(jacdetmap = new ParamVolume("Jacobian Determinant Map", VoxelType.FLOAT,-1,-1,-1,1));
		
	}
	
	@Override
	protected void execute(CalculationMonitor monitor) {
		
		ImageData defimg = defvol.getImageData();
		ImageHeader hdr = defimg.getHeader();
		String name = defimg.getName();
		float[][][][] def = ArrayUtil.getFloatArray4d(defimg);
		defimg.dispose();
		defvol.dispose();
		
		ImageDataFloat jacdet = new ImageDataFloat(RegistrationUtilities.computeJacobianDet(def));
		jacdet.setHeader(hdr);
		jacdet.setName(name+"_jacdet");
		jacdetmap.setValue(jacdet);
		
	}

}
