package edu.jhu.ece.iacl.plugins.dti;

import java.util.List;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;

public class MetaImageData4D {

	List<ImageData> data;
	int nComponents; 
	int []componentToVolumeMap;
	int []volumeComponentIndex;
	
	public MetaImageData4D(List<ImageData> imageDataList) {
		data = imageDataList;
		ImageHeader hdr = data.get(0).getHeader();
		for(int i=0;i<data.size();i++) {
			ImageData dataI = data.get(i);
			if(dataI==null) {
				throw new RuntimeException("MetaImageData4D: Cannot merge null dataset");
			}
			if(!dataI.getHeader().hasComparableGeometry(hdr)) {
				throw new RuntimeException("MetaImageData4D: Incompatible geometry in "+dataI.getName());
			}
			int c = dataI.getComponents();
			nComponents+=(c<1?1:c);
		}
		componentToVolumeMap = new int[nComponents];
		volumeComponentIndex = new int[nComponents];
		int i =0;
		int jVol = 0;
		int jIndex = 0;
		while(i<nComponents) {
			ImageData dataI = data.get(jVol);
			int c = dataI.getComponents();
			if(jIndex>=c) {
				jVol++; jIndex=0;
				dataI = data.get(jVol);
				c = dataI.getComponents();				
			}
			componentToVolumeMap[i]=jVol;
			volumeComponentIndex[i]=jIndex;
			jIndex++;
			i++;
		}
	}

	public int getRows() {
		return data.get(0).getRows();		
	}

	public int getCols() {
		return data.get(0).getCols();
	}

	public int getSlices() {
		return data.get(0).getSlices();
	}

	public int getComponents() {
		return nComponents;
	}

	public ImageHeader getHeader() {
		return data.get(0).getHeader();
	}

	public String getName() {	
		return data.get(0).getName();
	}

	public Number get(int i, int j, int k, int l) {				
		return data.get(componentToVolumeMap[l]).get(i,j,k,volumeComponentIndex[l]);
	}

}
