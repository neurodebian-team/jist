package edu.jhu.ece.iacl.plugins.quantitative;

import java.awt.Color;

import edu.jhu.ece.iacl.algorithms.dti.ComputeTensorContrasts;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataColor;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class EstimateT2FromDualEcho extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolume dualEchoVolume;	// 4-D Volume containing dual echo data
	private ParamFloat firstTE;
	private ParamFloat secondTE;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume T2MapVolume;	// Estimate T2 map

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Estimate T2 from a two-point fit using dual echo imaging.";
	private static final String longDescription = "Uses direct calculation from the log of the ratio of the observed signals.";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Quantitative");
		inputParams.setLabel("T2:DualEcho");
		inputParams.setName("T2:DualEcho");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist");
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(dualEchoVolume=new ParamVolume("Dual Echo Volume (4D)",null,-1,-1,-1,2));
		inputParams.add(firstTE=new ParamFloat("First Echo TE (ms)",28.2f));
		inputParams.add(secondTE=new ParamFloat("Second Echo TE (ms)",80f));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		// Base Outputs
		T2MapVolume = new ParamVolume("T2 Estimate (ms)",VoxelType.FLOAT,-1,-1,-1,1);
		T2MapVolume.setName("T2Est");
		outputParams.add(T2MapVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute() {
			this.setLabel("Estimating T2");
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to
			 * 			know when the bug happened.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t"+"START");
			
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/

			ImageDataFloat scalarVol=new ImageDataFloat(dualEchoVolume.getImageData());
			float TE1 = firstTE.getFloat();
			float TE2 = secondTE.getFloat();

			int r=scalarVol.getRows(), c=scalarVol.getCols(), s=scalarVol.getSlices(), t = scalarVol.getComponents();
			this.setTotalUnits(r);

			/****************************************************
			 * Step 3. Setup memory for the computed volumes
			 ****************************************************/

			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t"+"Allocating memory.");
			ImageData TEestimate = new ImageDataFloat(r,c,s,1);
			TEestimate.setName(scalarVol.getName()+"_quantT2");
			TEestimate.setHeader(scalarVol.getHeader());

			/****************************************************
			 * Step 4. Run the core algorithm. Note that this program
			 * 		   has NO knowledge of the MIPAV data structure and
			 * 		   uses NO MIPAV specific components. This dramatic
			 * 		   separation is a bit inefficient, but it dramatically
			 * 		   lower the barriers to code re-use in other applications.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t"+"Calculation in progress...");
			
			float deltaTE = TE2-TE1;
			int countNaN = 0;
			for(int i=0;i<r;i++) {
				this.setCompletedUnits(i);
				for(int j=0;j<c;j++)
					for(int k=0;k<s;k++) {
						float S1 = scalarVol.getFloat(i,j,k,0);
						float S2 = scalarVol.getFloat(i,j,k,1);
						// Scrub unphysical values
						if ((S2>=S1)||(S1<0)||(S2<0)){
							TEestimate.set(i,j,k,Float.NaN);
							countNaN++;
						}							
						else{
							TEestimate.set(i,j,k,(float)(deltaTE/Math.log(S1/S2)));
						}
							
					}
			}
			if(countNaN>0)
				JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t"+"Found "+countNaN+" voxels with invalid inputs, result set to NaN." );
			/****************************************************
			 * Step 5. Retrieve the image data and put it into a new
			 * 			data structure. Be sure to update the file information
			 * 			so that the resulting image has the correct
			 * 		 	field of view, resolution, etc.
			 ****************************************************/
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t"+"Setting up exports.");
			T2MapVolume.setValue(TEestimate);
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t"+"FINISHED");
		}
	}
}
