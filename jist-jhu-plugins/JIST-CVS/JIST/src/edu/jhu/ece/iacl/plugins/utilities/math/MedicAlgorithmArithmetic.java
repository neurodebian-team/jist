package edu.jhu.ece.iacl.plugins.utilities.math;


import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;


public class MedicAlgorithmArithmetic extends ProcessingAlgorithm {
	ParamDouble a, b, sum, difference, product, quotient;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Simple arithmetic demo module.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(a = new ParamDouble("a"));
		inputParams.add(b = new ParamDouble("b"));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Demos");
		inputParams.setLabel("Arithmetic");
		inputParams.setName("Arithmetic");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(sum = new ParamDouble("a+b"));
		outputParams.add(difference = new ParamDouble("a-b"));
		outputParams.add(product = new ParamDouble("a*b"));
		outputParams.add(quotient = new ParamDouble("a/b"));
	}


	@Override
	protected void execute(CalculationMonitor monitor) {
		sum.setValue(a.getFloat()+b.getFloat());
		difference.setValue(a.getFloat()-b.getFloat());
		product.setValue(a.getFloat()*b.getFloat());
		quotient.setValue(a.getFloat()/b.getFloat());
	}
}
