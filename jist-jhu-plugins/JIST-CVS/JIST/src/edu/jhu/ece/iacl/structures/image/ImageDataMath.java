package edu.jhu.ece.iacl.structures.image;

import javax.vecmath.Point3i;
import edu.jhu.ece.iacl.jist.structures.image.*;
// TODO: Auto-generated Javadoc
/**
 * The Class ImageDataMath.
 */
public class ImageDataMath {

	/**
	 * Vector magnitude4 d.
	 * 
	 * @param vec4D the vec4 d
	 * 
	 * @return the image data float
	 */
	static public ImageDataFloat vectorMagnitude4D(ImageData  vec4D) {
		int rows=vec4D.getRows(), cols=vec4D.getCols(), slices=vec4D.getSlices(),
		components = vec4D.getComponents();
		ImageDataFloat M = new ImageDataFloat(rows,cols,slices);
		double sum = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					sum = 0;
					for (int l = 0; l < components; l++) {
						sum += Math.pow(vec4D.getFloat(i, j, k, l), 2);
					}
					M.set(i,j,k,(float) Math.sqrt(sum));
				}
			}
		}
		return M;
	}
	
	/**
	 * Scale float value.
	 * 
	 * @param img the img
	 * @param scaleFactor the scale factor
	 */
	static public void scaleFloatValue(ImageData img, float scaleFactor) {
		int rows=img.getRows(), cols=img.getCols(), slices=img.getSlices(),
		components = img.getComponents();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						img.set(i, j, k, l, img.getFloat(i, j, k, l)* scaleFactor);
					}
				}
			}
		}
	}
	
	/**
	 * Normalize to unit intensity.
	 * 
	 * @param img the img
	 */
	static public void normalizeToUnitIntensity(ImageData img) {
		int rows=img.getRows(), cols=img.getCols(), slices=img.getSlices(),
		components = img.getComponents();
		float min = Float.MAX_VALUE;
		float max = -Float.MAX_VALUE;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						float val = img.getFloat(i, j, k, l);
						min = (min>val?val:min);
						max = (max<val?val:max);											
					}
				}
			}
		}
		float scale = ((max - min) > 0.0f) ? (1.0f / (max - min)) : 1.0f;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						img.set(i,j,k,l,(img.getFloat(i, j, k, l)-min)*scale);												
					}
				}
			}
		}
	}
	
	/**
	 * Adds the float image.
	 * 
	 * @param dst the dst
	 * @param src the src
	 */
	static public void addFloatImage(ImageData dst, ImageData src) {
		int rows=dst.getRows(), cols=dst.getCols(), slices=dst.getSlices(),
		components = dst.getComponents();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						dst.set(i,j,k,l,dst.getFloat(i, j, k, l)+src.getFloat(i, j, k, l));												
					}
				}
			}
		}
	}
	
	/**
	 * Multiply float image.
	 * 
	 * @param dst the dst
	 * @param src the src
	 */
	static public void multiplyFloatImage(ImageData dst, ImageData src) {
		int rows=dst.getRows(), cols=dst.getCols(), slices=dst.getSlices(),
		components = dst.getComponents();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						dst.set(i,j,k,l,dst.getFloat(i, j, k, l)*src.getFloat(i, j, k, l));												
					}
				}
			}
		}
	}
	
	/**
	 * Negate float image.
	 * 
	 * @param src the src
	 */
	static public void negateFloatImage(ImageData src) {
		int rows=src.getRows(), cols=src.getCols(), slices=src.getSlices(),
		components = src.getComponents();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				for (int k = 0; k < slices; k++) {
					for (int l = 0; l < components; l++) {
						src.set(i,j,k,l,-src.getFloat(i, j, k, l));												
					}
				}
			}
		}
	}

	/**
	 * Translate by integer.
	 * 
	 * @param img the img
	 * @param p the p
	 * @param bg the bg
	 */
	static public void translateByInteger(ImageData img, Point3i p, double bg) {
		int rows=img.getRows(), cols=img.getCols(), slices=img.getSlices(),
		components = img.getComponents();
		int startRows=0, endRows=rows-1, incRows=1;
		int startCols=0, endCols=cols-1, incCols=1;
		int startSlices=0, endSlices=slices-1, incSlices=1;
		if(p.x<0) {
			startRows=rows-1; endRows=0; incRows=-1;
		}
		if(p.y<0) {
			startCols=cols-1; endCols=0; incCols=-1;
		}
		if(p.z<0) {
			startSlices=slices-1; endSlices=0; incSlices=-1;
		}
		for (int i = startRows; i <= endRows; i+=incRows) {
			for (int j = startCols; j <= endCols; j+=incCols) {
				for (int k = startSlices; k <= endSlices; k+=incSlices) {
					for (int l = 0; l < components; l++) {
						img.set(i,j,k,l,((i - p.x >= 0) && (j - p.y >= 0) && (k - p.z >= 0) && (i - p.x < rows)
								&& (j - p.y < cols) && (k - p.z < slices)) ? img.getDouble(i - p.x, j - p.y, k - p.z, l)
								: bg);
					}
				}
			}
		}
		/*
//		System.out.println(getClass().getCanonicalName()+"\t"+p.x + " " + p.y + " " + p.z);
		if (components < 2) {
			double[][][] tmp = new double[rows][cols][slices];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						tmp[i][j][k] = ((i - p.x >= 0) && (j - p.y >= 0) && (k - p.z >= 0) && (i - p.x < rows)
								&& (j - p.y < cols) && (k - p.z < slices)) ? getDouble(i - p.x, j - p.y, k - p.z) : bg;
					}
				}
			}
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						set(i, j, k, tmp[i][j][k]);
					}
				}
			}
		} else {
			double[][][][] tmp = new double[rows][cols][slices][components];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						for (int l = 0; l < components; l++) {
							tmp[i][j][k][l] = ((i - p.x >= 0) && (j - p.y >= 0) && (k - p.z >= 0) && (i - p.x < rows)
									&& (j - p.y < cols) && (k - p.z < slices)) ? get(i - p.x, j - p.y, k - p.z, l)
									.doubleValue() : bg;
						}
					}
				}
			}
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						for (int l = 0; l < components; l++) {
							set(i, j, k, l, tmp[i][j][k][l]);
						}
					}
				}
			}
		}*/
	}

	
	static public ImageDataFloat extract3DVolume(ImageData img, int index) {
		if(img.getComponents()>=index)
			return null;
		ImageDataFloat ret = new ImageDataFloat(img.getName()+"vol"+index, 
				img.getRows(), img.getCols(), img.getSlices()); 
		ret.setHeader(img.getHeader());
		for(int i=0;i<img.getRows();i++){
			for(int j=0;j<img.getCols();j++){
				for(int k=0;k<img.getSlices();k++){
					ret.set(i,j,k,index,img.getFloat(i, j, k));
				}
			}
		}
		return ret;			
	}
}
