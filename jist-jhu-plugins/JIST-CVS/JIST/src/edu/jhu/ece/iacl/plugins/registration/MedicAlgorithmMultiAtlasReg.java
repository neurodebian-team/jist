package edu.jhu.ece.iacl.plugins.registration;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import Jama.Matrix;

import edu.jhu.ece.iacl.algorithms.CommonAuthors;
import edu.jhu.ece.iacl.algorithms.registration.RegistrationUtilities;
import edu.jhu.ece.iacl.algorithms.volume.TransformVolume;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.*;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;

public class MedicAlgorithmMultiAtlasReg extends ProcessingAlgorithm {

	//inputs
	public ParamVolumeCollection inParamAtlases;
	public ParamVolume inParamTarget;
	public ParamBoolean inParamSaveDefAtlases;
	public ParamBoolean inParamSaveDefFields;
	
	//outputs
	public ParamVolumeCollection outParamDeformedAtlases;
	public ParamVolumeCollection outParamDefFields;
	public ParamFileCollection outParamTransformMatrices;

	//Collections
	public MedicAlgorithmVABRA vabra;
	
	private File dir;
	
	private static final String revnum = "$Revision: 1.9 $".replace(
			"Revision: ", "").replace("$", "").replace(" ", "");

	public String[] getDefaultJVMArgs() {
		return new String[] {"-XX:MinHeapFreeRatio=70",
				"-XX:MaxHeapFreeRatio=95",
				"-XX:YoungGenerationSizeIncrement=150",
				"-XX:TenuredGenerationSizeIncrement=150" };
	}

	protected void createInputParameters(ParamCollection inputParams) {

		//Manage my own memory for these
		ParamCollection mainParams = new ParamCollection("Main");
		mainParams.add(inParamAtlases = new ParamVolumeCollection("Atlases"));
		inParamAtlases.setLoadAndSaveOnValidate(false);
		
		mainParams.add(inParamTarget = new ParamVolume("Target"));
		mainParams.add(inParamSaveDefAtlases = new ParamBoolean("Output Registered Atlases",true));
		mainParams.add(inParamSaveDefFields = new ParamBoolean("Output Deformation Fields", true));
		
		
		vabra = new MedicAlgorithmVABRA();
		vabra.inParamSubjects.setHidden(true);
		vabra.inParamTargets.setHidden(true);
		
		inputParams.add(mainParams);
		inputParams.add(vabra.getInput());

		inputParams.setLabel("Multiple Atlas Registrations via VABRA");
		inputParams.setName("MultiAtlasReg");

		setPreferredSize(new Dimension(300, 600));

		inputParams.setPackage("IACL");
		inputParams.setCategory("Registration.Volume");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(CommonAuthors.minChen);
		info.setDescription("Registers N Atlases to a target image");
		info.setLongDescription("This algorithm is used to register multiple atlases to"
						+ " the same target via multiple calls of single channel VABRA."
						+ " Can be used with MultiDeformVolume to streamline multi-atlas labeling");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		
		//Manage my own memory for these
		
		outputParams.add(outParamDeformedAtlases = new ParamVolumeCollection("Registered Atlases"));
		outParamDeformedAtlases.setLoadAndSaveOnValidate(false);
		outParamDeformedAtlases.setMandatory(false);

		outputParams.add(outParamDefFields = new ParamVolumeCollection("Deformation Fields", VoxelType.FLOAT, -1, -1, -1, -1));
		outParamDefFields.setLoadAndSaveOnValidate(false);
		outParamDefFields.setMandatory(false);

		FileExtensionFilter xfmfilter = new FileExtensionFilter(new String[] {"mtx", "xfm" });
		outputParams.add(outParamTransformMatrices = new ParamFileCollection("Transformation Matrices", xfmfilter));
		outParamTransformMatrices.setMandatory(false);

		outputParams.setName("MultiAtlasReg");
	}

	protected void execute(CalculationMonitor monitor) {

		// setup for saving files
		dir = new File(this.getOutputDirectory()
				+ File.separator
				+ edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this
						.getAlgorithmName()));
		try {
			if (!dir.isDirectory()) {
				(new File(dir.getCanonicalPath())).mkdir();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayDoubleMtxReaderWriter mtxrw = new ArrayDoubleMtxReaderWriter();
		double[][] tmpMatrix;

		ImageData targetClone;
		List<ParamVolume> atlasParamList = inParamAtlases.getParamVolumeList();
		//List<ParamVolume> defFieldParamList = outParamDefFields.getParamVolumeList();
		//List<ParamVolume> defAtlasesParamList = outParamDeformedAtlases.getParamVolumeList();
		ParamVolume atlasParam, defAtlasParam, defFieldParam;

		
		//List<ImageDataFloat> DefFields = new ArrayList<ImageDataFloat>();
		ArrayList<ImageData> matchedAtlases = new ArrayList<ImageData>();
		ArrayList<ImageData> matchedTargets = new ArrayList<ImageData>();
		
		ArrayList<Matrix> transMatrixes = new ArrayList<Matrix>();
		ArrayList<File> mtxs = new ArrayList<File>();
		
		
		
		// go through all the atlases and get Deformations fields for each
		for (int q = 0; q < atlasParamList.size(); q++) {

			atlasParam = atlasParamList.get(q);
			targetClone = inParamTarget.getImageData().clone();//Clone so target doesn't accidentally get destroyed

			System.out.format("****************Registering atlases " + q + "*********************\n");

			matchedAtlases.add(atlasParam.getImageData());
			matchedTargets.add(targetClone);
			
			vabra.inParamSubjects.setValue(matchedAtlases);
			vabra.inParamTargets.setValue(matchedTargets);
			vabra.run();
			
			//Cleanup for next iteration

			matchedAtlases.clear();
			matchedTargets.clear();
			targetClone.dispose();
			atlasParam.dispose();
			
			transMatrixes.add(vabra.outParamTransformMatrices.getValue());

			// Save Transformation Matrices
			tmpMatrix = vabra.outParamTransformMatrices.getValue().getArray();
			File f = null;
			File ff = null;
			try {
				f = new File(dir.getCanonicalPath() + File.separator + atlasParam.getImageData().getName() + "_xfm.mtx");
				ff = mtxrw.write(tmpMatrix, f);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (ff != null) {
				mtxs.add(ff);
			}

			//save and free deformation field
			if (inParamSaveDefFields.getValue()){
				defFieldParam = new ParamVolume("Deformation Field", VoxelType.FLOAT, -1, -1, -1, -1);
				defFieldParam.setValue(vabra.outParamDeformationField.getImageData());
				defFieldParam.setLoadAndSaveOnValidate(false);
				outParamDefFields.add(defFieldParam);
				defFieldParam.writeAndFreeNow(this);
			}
		
			if (inParamSaveDefAtlases.getValue()) {
				defAtlasParam = new ParamVolume("Deformed Subjects"); 
				defAtlasParam.setValue(vabra.outParamDeformedSubject.getParamVolume(0).getImageData());
				vabra.outParamDeformedSubject.clear();
				defAtlasParam.setLoadAndSaveOnValidate(false);
				outParamDeformedAtlases.add(defAtlasParam);
				defAtlasParam.writeAndFreeNow(this);
			}
			System.out.format("****************Atlas " + q + " Registered****************\n");
		}

		outParamTransformMatrices.setValue(mtxs);
	}

}
