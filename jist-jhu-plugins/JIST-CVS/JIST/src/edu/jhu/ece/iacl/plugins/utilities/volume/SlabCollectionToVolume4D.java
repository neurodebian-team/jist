package edu.jhu.ece.iacl.plugins.utilities.volume;

import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataDouble;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class SlabCollectionToVolume4D extends ProcessingAlgorithm {

	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamVolumeCollection inputFiles;		// Slab collection
	private ParamOption dimOpt;
	
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume outputVolume;		// 4D volume

//	//Variables
//	ArrayList<File> outVols;
//	File outDir;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Converts a (4D) Slab Collection produced with 'Volume4DToSlabCollection' to a single (4D) Volume.";
	private static final String longDescription = "The dimension along which to concatenate can be indicated by the user, and will depend on the dimension \n" +
													"chosen in 'Volume4DToSlabCollection'.";


	@Override
	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("UMCU");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("SlabCollectionToVolume4D");
		inputParams.setName("SlabCollection_To_Volume4D");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", ""));
		info.add(new AlgorithmAuthor("Daniel Polders", "daniel.polders@gmail.com", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription +"\n"+ longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);

		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(inputFiles= new ParamVolumeCollection("Input Slab Collection"));
		inputFiles.setLoadAndSaveOnValidate(false);
		String[] dimlist = {"Rows", "Collumns", "Slices", "Components"}; 
		inputParams.add(dimOpt = new ParamOption("Dimension to be concatenated",dimlist));
	}


	@Override
	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		outputVolume= new ParamVolume("Concatenated Volume", null, -1,-1,-1,-1);
		outputVolume.setLoadAndSaveOnValidate(false);
		outputParams.add(outputVolume);
	}


	@Override
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		AlgorithmWrapper wrapper=new AlgorithmWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}
	
	protected class AlgorithmWrapper extends AbstractCalculation {
		protected void execute(ProcessingAlgorithm alg) {
			
			
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 ****************************************************/
			this.setLabel("Initializing");
			JistLogger.logOutput(JistLogger.INFO,getClass().getCanonicalName()+"\t START");
		//outDir = this.getOutputDirectory();//MipavController.getDefaultWorkingDirectory();
			//boolean use4D = output4D.getValue();
			// First need to count the number of volumes (for output) and the total slices in slabs (for output size)\
			
			String dim = dimOpt.getValue();
			int tSlices=0;
			int tRows=0;
			int tCols=0;
			int tComp=0;
			int nSlices=0, nRows = 0, nCols=0, nComp=0;
			VoxelType type = null;
			String baseName = null;
			ImageData outputVol;
			ImageHeader header = new ImageHeader();
			
			
			/****************************************************
			 * Step 2. Parse the input data
			 ****************************************************/
			JistLogger.logOutput(JistLogger.FINE,getClass().getCanonicalName()+"\t Parsing input...");
			// Get the size of final volume, add all indices, but use only the total dimension chosen in dim
			// This assumes that all other dimensions are equal!!
			List<ParamVolume> pvlist = inputFiles.getParamVolumeList();
			for(ParamVolume pv: pvlist){
				ImageData vol = pv.getImageData();
				if(baseName==null){ //i.e. first time in this loop
					baseName = vol.getName().replace("_"+dim+"_Slab00", "");
					nRows = vol.getRows();
					nCols = vol.getCols();
					nSlices = vol.getSlices();
					nComp = vol.getComponents();
					type = vol.getType();
					header = vol.getHeader();
				}
				tRows += vol.getRows();
				tCols += vol.getCols();
				tSlices += vol.getSlices();
				tComp += vol.getComponents();
				vol.dispose();
				vol=null;
				pv.dispose();
			}
			
			if(dim.contentEquals("Rows")){
				nRows = tRows;
			}else if(dim.contentEquals("Columns")){
				nCols = tCols;
			}else if(dim.contentEquals("Slices")){
				nSlices = tSlices;
			}else if(dim.contentEquals("Components")){
				nComp = tComp;
			}
			JistLogger.logOutput(JistLogger.FINE, "Input overview:");
			JistLogger.logOutput(JistLogger.FINE, ("Number of input files in collection = "+pvlist.size()));
			JistLogger.logOutput(JistLogger.FINE, ("basename = "+baseName));
			JistLogger.logOutput(JistLogger.FINE, ("Concatenation dimension = "+dim));
			JistLogger.logOutput(JistLogger.FINE, ("Final dimensions:"));
			JistLogger.logOutput(JistLogger.FINE, ("  nRows = "+nRows));
			JistLogger.logOutput(JistLogger.FINE, ("  nCols = "+nCols));
			JistLogger.logOutput(JistLogger.FINE, ("  nSlices = "+nSlices));
			JistLogger.logOutput(JistLogger.FINE, ("  nComp = "+nComp));
			JistLogger.logOutput(JistLogger.FINE, ("  type = "+type.name()));
			
			this.setLabel("Allocating memory");
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t Allocating memory...");
			JistLogger.logMIPAVRegistry();
			// Now construct the output
			if (type.name().compareTo("UBYTE")==0)
				outputVol=new ImageDataUByte(nRows,nCols,nSlices,nComp);
			else if (type.name().compareTo("FLOAT")==0)
				outputVol=new ImageDataFloat(nRows,nCols,nSlices,nComp);
			else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
				outputVol=new ImageDataInt(nRows,nCols,nSlices,nComp);
			else
				outputVol=new ImageDataDouble(nRows,nCols,nSlices,nComp);
			
			outputVol.setName(baseName+"_Conc");
			outputVol.setHeader(header);
			
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t Allocating memory...DONE");
			JistLogger.logMIPAVRegistry();
			
			int counttot = pvlist.size();
			int count = 1;
			this.setTotalUnits(counttot);
			
			
			if(dim.contentEquals("Rows")){	
				int lastSlabEndRow=-1;
				int startRow=0, endRow=0;
				for(ParamVolume pv: pvlist){ //go through all slabs in list...
					this.setLabel("Concatenating Slab "+count+ " of "+counttot);
					JistLogger.logOutput(JistLogger.FINE, "Slab "+count+"...");
					JistLogger.logMIPAVRegistry();
					ImageData vol = pv.getImageData();
					startRow = lastSlabEndRow+1;
					endRow = startRow+vol.getRows()-1;
					lastSlabEndRow=endRow;
					for(int j=0;j<nCols;j++){
						for(int k=0;k<nSlices;k++) {
							for (int l=0; l<nComp; l++){
								int jRow=0;
								for(int i=startRow;i<=endRow;i++) {
									if (type.name().compareTo("UBYTE")==0)
											outputVol.set(i,j,k,l,vol.getUByte(jRow, j, k, l));
										else if (type.name().compareTo("FLOAT")==0)
											outputVol.set(i,j,k,l,vol.getFloat(jRow, j, k, l));
										else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
											outputVol.set(i,j,k,l,vol.getInt(jRow, j, k, l));
										else
											outputVol.set(i,j,k,l,vol.getDouble(jRow, j, k, l));
									jRow++;
									}
								}	
							}
					}
					vol.dispose();
					vol=null;
					pv.dispose();
					count++;
					this.setCompletedUnits(count);
				}
			}else if(dim.contentEquals("Columns")){	
				int lastSlabEndCollumn=-1;
				int startCollumn=0, endCollumn=0;
				for(ParamVolume pv: pvlist){ //go through all slabs in list...
					this.setLabel("Concatenating Slab "+count+ " of "+counttot);
					JistLogger.logOutput(JistLogger.FINE, getClass().getCanonicalName()+"\t Processing slab "+count+"...");
					JistLogger.logMIPAVRegistry();
					ImageData vol = pv.getImageData();
					startCollumn = lastSlabEndCollumn+1;
					endCollumn = startCollumn+vol.getCols()-1;
					lastSlabEndCollumn=endCollumn;
					for(int i=0;i<nRows;i++){
						for(int k=0;k<nSlices;k++) {
							for (int l=0; l<nComp; l++){
								int jCollumn=0;
								for(int j=startCollumn;j<=endCollumn;j++) {
									if (type.name().compareTo("UBYTE")==0)
										outputVol.set(i,j,k,l,vol.getUByte(i, jCollumn, k, l));
									else if (type.name().compareTo("FLOAT")==0)
										outputVol.set(i,j,k,l,vol.getFloat(i, jCollumn, k, l));
									else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
										outputVol.set(i,j,k,l,vol.getInt(i, jCollumn, k, l));
									else
										outputVol.set(i,j,k,l,vol.getDouble(i, jCollumn, k, l));
									}
								jCollumn++;
								}
							}
					}
					vol.dispose();
					vol=null;
					pv.dispose();
					count++;
					this.setCompletedUnits(count);
				}
			}else if(dim.contentEquals("Slices")){	
				int lastSlabEndSlice=-1;
				int startSlice=0, endSlice=0;
				for(ParamVolume pv: pvlist){ //go through all slabs in list...
					this.setLabel("Concatenating Slab "+count+ " of "+counttot);
					JistLogger.logOutput(JistLogger.FINE, getClass().getCanonicalName()+"\t Processing slab "+count+"...");
					JistLogger.logMIPAVRegistry();
					ImageData vol = pv.getImageData();
					startSlice = lastSlabEndSlice+1;
					endSlice = startSlice+vol.getSlices()-1;
					lastSlabEndSlice=endSlice;
					for(int i=0;i<nRows;i++){
						for(int j=0;j<nCols;j++) {
							for (int l=0; l<nComp; l++){
								int jSlice=0;
								for(int k=startSlice;k<=endSlice;k++) {
									if (type.name().compareTo("UBYTE")==0)
											outputVol.set(i,j,k,l,vol.getUByte(i, j, jSlice, l));
										else if (type.name().compareTo("FLOAT")==0)
											outputVol.set(i,j,k,l,vol.getFloat(i, j, jSlice, l));
										else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
											outputVol.set(i,j,k,l,vol.getInt(i, j, jSlice, l));
										else
											outputVol.set(i,j,k,l,vol.getDouble(i, j, jSlice, l));
									jSlice++;
									}
								}	
							}
					}
					vol.dispose();
					vol=null;
					pv.dispose();
					count++;
					this.setCompletedUnits(count);
				}
			}else if(dim.contentEquals("Components")){	
				
				int lastSlabEndComp=-1;
				int startComp=0, endComp=0;
				for(ParamVolume pv: pvlist){ //go through all slabs in list...
					this.setLabel("Concatenating Slab "+count+ " of "+counttot);
					JistLogger.logOutput(JistLogger.FINE, getClass().getCanonicalName()+"\t Processing slab "+count+"...");
					JistLogger.logMIPAVRegistry();
					ImageData vol = pv.getImageData();
					startComp = lastSlabEndComp+1;
					endComp = startComp+vol.getComponents()-1;
					lastSlabEndComp=endComp;
					for(int i=0;i<nRows;i++){
						for(int j=0;j<nCols;j++) {
							for (int k=0; k<nSlices; k++){
								int jComp=0;
								for(int l=startComp;l<=endComp;l++) {
									if (type.name().compareTo("UBYTE")==0)
											outputVol.set(i,j,k,l,vol.getUByte(i, j, k, jComp));
										else if (type.name().compareTo("FLOAT")==0)
											outputVol.set(i,j,k,l,vol.getFloat(i, j, k, jComp));
										else if (type.name().endsWith("SHORT") || type.name().compareTo("INT")==0)
											outputVol.set(i,j,k,l,vol.getInt(i, j, k, jComp));
										else
											outputVol.set(i,j,k,l,vol.getDouble(i, j, k, jComp));
									jComp++;
									}
								}	
							}
					}
					vol.dispose();
					vol=null;
					pv.dispose();
					count++;
					this.setCompletedUnits(count);
				}
			}
			
			this.setLabel("Saving Data");
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t Setting up outputs.");
			JistLogger.logMIPAVRegistry();
			System.gc();
			outputVolume.setValue(outputVol);
			outputVolume.writeAndFreeNow(alg);
//			outputParams.add(outputVolume);
			
			JistLogger.logOutput(JistLogger.INFO, getClass().getCanonicalName()+"\t FINISHED");
			}
	}
}


