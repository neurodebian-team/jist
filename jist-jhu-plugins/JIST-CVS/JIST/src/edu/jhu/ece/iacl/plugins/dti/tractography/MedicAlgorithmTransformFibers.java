package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;

import Jama.Matrix;
import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberTransformation;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;


public class MedicAlgorithmTransformFibers extends ProcessingAlgorithm {
	//input parameters
	private ParamObject<FiberCollection> fibersin;
	private ParamMatrix matrix;
	private ParamFile mtxxfm;
	//output parameters
	private ParamObject<FiberCollection> transfibers;

	private static final String cvsversion = "$Revision: 1.5 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Applies an affine transformation to fibers obtained from white matter tractography.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		FileExtensionFilter xfmfilter = new FileExtensionFilter(new String[]{"mtx","xfm"});

		inputParams.add(fibersin = new ParamObject<FiberCollection>("Input Fibers",new FiberCollectionReaderWriter()));
		inputParams.add(matrix = new ParamMatrix("Transformation Matrix",4,4));
		matrix.setMandatory(false);
		inputParams.add(mtxxfm = new ParamFile("Transformation",xfmfilter));
		mtxxfm.setMandatory(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("Transform Fibers");
		inputParams.setName("Transform_Fibers");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic","bogovic@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University, Departments of Electrical and Biomedical Engineering");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(transfibers = new ParamObject<FiberCollection>("Transformed Fibers", new FiberCollectionReaderWriter()));
	}


	protected void execute(CalculationMonitor monitor)
			throws AlgorithmRuntimeException {
		double[][] xfm;
		if(mtxxfm.getValue()!=null){
			ArrayDoubleMtxReaderWriter mtxrw = new ArrayDoubleMtxReaderWriter();
			xfm = mtxrw.read(mtxxfm.getValue());
		}else{
			Matrix m = matrix.getValue();
			xfm = new double[m.getRowDimension()][m.getColumnDimension()];
			for(int i=0; i<m.getRowDimension(); i++){
				for(int j=0; j<m.getColumnDimension(); j++){
					xfm[i][j]=m.get(i, j);
				}
			}
		}

		String file = fibersin.getValue().toString();
		file = file.substring((file.lastIndexOf(File.separator))+1, (file.length()-4));
//		file = file.substring(beginIndex, file.l)
		System.out.println(getClass().getCanonicalName()+"\t"+file);
		FiberCollection fibers = FiberTransformation.applyAffine(fibersin.getObject(), xfm);
		fibers.setName(file+"_transf.dat");
//		transfibers.setFileName(file+"_transf.dat");
		transfibers.setObject(fibers);

		System.out.println(getClass().getCanonicalName()+"\t"+"Finished");
	}
}
