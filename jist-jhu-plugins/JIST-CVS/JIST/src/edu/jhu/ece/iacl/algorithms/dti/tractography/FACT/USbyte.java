package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

//Unsigned Byte
public class USbyte {
	
	byte signed;
	int unsigned;
	
	public USbyte(byte b){
		signed = b;
		unsigned = signed2unsigned(b);
	}
	public USbyte(){
		signed=0; unsigned=0;
	}
	public USbyte(int i){
		unsigned =i;
		//try{
		signed = unsigned2signed(i);
		//}catch(Exception e){ e.printStackTrace(); }
	}
	
	public static void main(String [] args){
//		byte s = -5;
//		USbyte usb = new USbyte(s);
//		System.out.println("jist.plugins"+"\t"+"unsigned is " + usb.unsigned);
//		try{
//			byte ns = usb.unsigned2signed(128);
//			System.out.println("jist.plugins"+"\t"+"new signed byte is: " +ns);
//		}catch(Exception e){ e.printStackTrace(); }
		
		int a=255;
		USbyte usb = new USbyte(a);
		System.out.println("jist.plugins"+"\t"+usb.signed);
		
	}
	
	public int signed2unsigned(byte signed){
		if (signed<0){
			unsigned=256+signed;
		}else{
			unsigned=(int)signed;
		}
		return unsigned;
	}
	
	public byte unsigned2signed(int uns){// throws Exception{
		byte snd=0;
		if (uns>255 | uns<0){
			//throw new Exception("Unsigned out of bounds: 0<uns<255");
			System.out.println("jist.plugins"+"\t"+"Unsigned out of bounds: 0<uns<255 ... snd = 0");
			return snd;
		}
		if (uns<=127){ snd = (byte)uns; }
		else { snd= (byte)(uns-256); }
		
		return snd;
	}

}
