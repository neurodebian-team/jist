package edu.jhu.ece.iacl.algorithms.registration;

import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

//N-Dimensional Histogram - Sparse Hashmap Implementation
//If it is a Joint Histogram between subjects and targets, by convention subjects are placed first
public class NDimHistogram{
	private HashMap<Integer,MutableInt> data;
	private int numOfSub;
	private int numOfTar;
	private int numOfBins;
	private int totalVol;
	private double entropy;
	private boolean histModified;
	
	class MutableInt implements Cloneable{
		int value;
		MutableInt(int value){
			this.value = value;
		}
		
		MutableInt(){
			this.value = 0;
		}
		
		protected MutableInt clone(){
			MutableInt clone = new MutableInt();
			clone.value = this.value;
			return clone;
		}
		
	}
	
	public NDimHistogram(int numOfSub,int numOfTar,  int numOfBins) {		
		this.numOfSub = numOfSub;
		this.numOfTar = numOfTar;
		this.numOfBins = numOfBins;
		this.histModified = true;
		data = new HashMap<Integer,MutableInt>(10000);
		totalVol=0;
		System.out.format("Currently Using Sparse Harsh Histograms");
	}
		
	public int convertIndex(int[] subIndexes, int[] tarIndexes){
		int index1D = 0;
		int encode = 0;
		for(int i = 0; i < subIndexes.length; i++){
			index1D += subIndexes[i]*(int)Math.pow(numOfBins,encode);
			encode++;
		}
		
		for(int i = 0; i < tarIndexes.length; i++){
			index1D += tarIndexes[i]*(int)Math.pow(numOfBins,encode);
			encode++;
		}
		
		return index1D;
	}
	
	//get bin
	public int get(int[] subIndexes, int[] tarIndexes){
		return data.get(convertIndex(subIndexes, tarIndexes)).value; 
	}

	//set bin
	public void set(int[] subIndexes, int[] tarIndexes, int val){
		int index1D = convertIndex(subIndexes, tarIndexes);
		MutableInt stored = data.get(index1D);
		if(stored != null){
			totalVol -= stored.value;
			stored.value = val;
		}else{		
			data.put(index1D, new MutableInt(val));
		}
		totalVol += val;
		histModified = true;
	}
	
	//raise bin by one
	public void increment(int[] subIndexes, int[] tarIndexes){
		int index1D = convertIndex(subIndexes, tarIndexes);
		MutableInt stored = data.get(index1D);
		if(stored != null) stored.value++;
		else{		
			data.put(index1D, new MutableInt(1));
		}
		totalVol++;
		histModified = true;
	}
	
	//drop bin by one	
	public void decrement(int[] subIndexes, int[] tarIndexes){
		int index1D = convertIndex(subIndexes, tarIndexes);
		MutableInt stored = data.get(index1D);
		if(stored != null){ 
			stored.value--;
			totalVol--;
			if(stored.value <=0 ){
				data.remove(index1D);
			}
			histModified = true;
		}		
	}	
	
	//given subject and target images, fill up the joint histogram
	public void fillJointHistogram(ImageData[] subjects, ImageData[] targets, int[] boundingBox){
		if((subjects.length != numOfSub) || (targets.length != numOfTar)){
			System.out.format("Subjects/Target Length Does Not Match Histogram Size\n");
			return;
		}
		
		int[] subIndexes = new int[subjects.length];
		int[] tarIndexes = new int[targets.length];
		int bin; 
		clearHist();
		
		for (int i = boundingBox[0]; i <= boundingBox[1]; i++) 
			for (int j = boundingBox[2]; j <= boundingBox[3]; j++) 
				for (int k = boundingBox[4]; k <= boundingBox[5]; k++) {

					//get bin index for all subjects
					for(int ch = 0; ch < subjects.length; ch++ ){
						bin = subjects[ch].getUByte(i, j, k);
						
						//Make sure is in bounds
						if (bin >= numOfBins){
							bin = numOfBins - 1;
							System.out.format("Value outside of bin range\n");
						}
						if (bin < 0) {
							bin = 0;
							System.out.format("Value outside of bin range\n");
						}
						subIndexes[ch] = bin;
					}
					
					//get bin index for all subjects and targets
					for(int ch = 0; ch < subjects.length; ch++ ){
						bin = targets[ch].getUByte(i, j, k);
						
						//Make sure is in bounds
						if (bin >= numOfBins){
							bin = numOfBins - 1;
							System.out.format("Value outside of bin range\n");
						}
						if (bin < 0) {
							bin = 0;
							System.out.format("Value outside of bin range\n");
						}
						tarIndexes[ch] = bin;
					}
					
					//increment bin
					increment(subIndexes,tarIndexes);
		}		
		histModified = true;
	}
	
	private void evalEntropy(){
		entropy = 0;
		double tmp;
		Iterator<MutableInt> itr = data.values().iterator();
		while(itr.hasNext()){
			tmp = ((double) itr.next().value) / totalVol;
			if(tmp <= 0){System.out.print("RUOOHOH " + tmp + " AHHH");}
			entropy -= tmp * Math.log(tmp);
		}
		histModified = false;
	}
	
	public NDimHistogram clone(){
		NDimHistogram clone = new NDimHistogram(numOfSub, numOfTar, numOfBins);
		clone.copy(this);
		return clone;
	}

	public void copy(NDimHistogram copyFrom){
		if((this.numOfSub != copyFrom.numOfSub) || (this.numOfTar != copyFrom.numOfTar) || (this.numOfBins != copyFrom.numOfBins)){
			System.out.format("Invalid Copy - Objects Don't Match\n");
			return;
		}
		
		clearHist();
		Iterator<Entry<Integer,MutableInt>> itr = copyFrom.data.entrySet().iterator();
		while(itr.hasNext()){
			Entry<Integer,MutableInt> entryToCopy = itr.next();
			this.data.put(entryToCopy.getKey().intValue(), new MutableInt(entryToCopy.getValue().value));
		}
		totalVol = copyFrom.totalVol;
		histModified = true;
		//System.out.format("HIIIIIII!"+this.data.values().size() +"\n");
	}
	
	public int getNumOfBins() {return numOfBins;}
	public int getTotalVol() {return totalVol;}
	public double getEntropy() {
		if(!histModified){
			return entropy;
		}else{
			evalEntropy();
			return entropy;
		}
	}
	
	public void clearHist(){
		totalVol = 0;
		data.clear();
		histModified = true;
	}

	
}
