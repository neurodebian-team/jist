package edu.jhu.ece.iacl.plugins.hardi;

import edu.jhu.ece.iacl.algorithms.hardi.SurfaceTools;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class TesselateSurface extends ProcessingAlgorithm{
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamInteger order;
	private ParamSurface surface;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamSurface outputSurface;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Tesselates a triangulated mesh.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Tesselate Surface");	
		inputParams.setName("Tesselate_Surface");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(surface=new ParamSurface("Input Surface"));
		inputParams.add(order=new ParamInteger("Tesselation Order",2,500,2));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		outputParams.add(outputSurface=new ParamSurface("Solid Mesh"));
	}


	protected void execute(CalculationMonitor monitor) {
		EmbeddedSurface surf =new EmbeddedSurface(SurfaceTools.tesselate(surface.getSurface(),order.getInt()));
		surf.setName(surface.getSurface().getName()+"_tesselate"+order.getInt());
		outputSurface.setValue(surf);
	}
}
