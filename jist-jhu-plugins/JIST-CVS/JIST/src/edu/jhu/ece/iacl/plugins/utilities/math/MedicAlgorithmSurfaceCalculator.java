/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.math;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class MedicAlgorithmSurfaceCalculator extends ProcessingAlgorithm{
	ParamSurface surf1Param;
	ParamSurface surf2Param;
	ParamSurface resultSurfParam;
	ParamOption operation;

	private static final String cvsversion = "";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Perform simple image calculator operations on two surfaces. The operations include 'Add', 'Subtract', 'Multiply', and 'Divide'";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(surf1Param=new ParamSurface("Surface 1"));
		inputParams.add(surf2Param=new ParamSurface("Surface 2"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Add","Subtract","Multiply","Divide"}));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Math");
		inputParams.setLabel("Surface Calculator");
		inputParams.setName("Surface_Calculator");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultSurfParam=new ParamSurface("Result Volume"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		EmbeddedSurface surf1=surf1Param.getSurface();
		EmbeddedSurface surf2=surf2Param.getSurface();
		int vert=surf1.getVertexCount();
		double tmp1,tmp2;
		for (int i = 0; i < vert; i++) {
			tmp1=surf1.getVertexDataAtOffset(i, 0);
			tmp2=surf2.getVertexDataAtOffset(i, 0);

			switch(operation.getIndex()){
			case 0:surf1.setVertexData(i, tmp1+tmp2);break;
			case 1:surf1.setVertexData(i, tmp1-tmp2);break;
			case 2:surf1.setVertexData(i, tmp1*tmp2);break;
			case 3:surf1.setVertexData(i, tmp1/tmp2);break;
			default:
			}
		}
		resultSurfParam.setValue(surf1);
	}
}
