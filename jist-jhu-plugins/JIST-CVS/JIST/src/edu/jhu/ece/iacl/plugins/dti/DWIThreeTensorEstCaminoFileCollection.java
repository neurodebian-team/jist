package edu.jhu.ece.iacl.plugins.dti;

import imaging.SchemeV1;
import inverters.ModelIndex;
import inverters.ThreeTensorInversion;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import edu.jhu.ece.iacl.algorithms.dti.EstimateTensorLLMSE;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataUByte;


public class DWIThreeTensorEstCaminoFileCollection extends ProcessingAlgorithm{ 
	/****************************************************
	 * Input Parameters 
	 ****************************************************/
	private ParamFileCollection DWdata4D; 		// Imaging Data
	private ParamFileCollection Mask3D;			// Binary mask to indicate computation volume
	private ParamOption threeTensorFitOption;		// Option to attempt to estimate with missing data
	private ParamOption oneTensorInitOption;		// Option to attempt to estimate with missing data
	private ParamFloat noiseLevel;		// Used for restore
	private ParamFile SchemeFile;

	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamFileCollection tensor1Volume;	// A 4D volume with one tensor estimated per pixel
	private ParamFileCollection tensor2Volume;	// A 4D volume with one tensor estimated per pixel
	private ParamFileCollection tensor3Volume;	// A 4D volume with one tensor estimated per pixel
	private ParamFileCollection mix1Volume;	// A 3D volume with the mixture component of tensor 1
	private ParamFileCollection mix2Volume;	// A 3D volume with the mixture component of tensor 2
	private ParamFileCollection mix3Volume;	// A 3D volume with the mixture component of tensor 3
	private ParamFileCollection exitCodeVolume;	// A 3D volume 
	private ParamFileCollection intensityVolume;// A 3D volume 

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.7 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Log-linear minimum mean squared error tensor estimation.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information 
		 ****************************************************/
		inputParams.setPackage("Camino");
		inputParams.setCategory("Modeling.Diffusion");
		inputParams.setLabel("Camino Three Tensor Estimation");
		inputParams.setName("Camino_Three_Tensor_Estimation");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", "http://sites.google.com/site/bennettlandman/"));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new Citation("Landman BA, Farrell JA, Jones CK, Smith SA, Prince JL, Mori S. Effects of diffusion weighting schemes on the reproducibility of DTI-derived fractional anisotropy, mean diffusivity, and principal eigenvector measurements at 1.5T. Neuroimage 2007;36(4):1123-1138."));
		info.add(new Citation("Basser, PJ, Jones, DK. \"Diffusion-tensor MRI: Theory, experimental design and data analysis - a technical review.\" NMR Biomed 2002; 15(7-8):456-67"));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		/****************************************************
		 * Step 2. Add input parameters to control system 
		 ****************************************************/
		inputParams.add(DWdata4D=new ParamFileCollection("DWI and Reference Image(s) Data (4D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
	inputParams.add(SchemeFile=new ParamFile("CAMINO DTI Description (SchemeV1)",new FileExtensionFilter(new String[]{"scheme","schemev1"})));
		//		inputParams.add(gradsTable=new ParamFile("Table of diffusion weighting directions",new FileExtensionFilter(new String[]{"grad","dpf"})));
		//		inputParams.add(bvaluesTable=new ParamFile("Table of b-values",new FileExtensionFilter(new String[]{"b"})));
	inputParams.add(Mask3D=new ParamFileCollection("Mask Volume to Determine Region of Tensor Estimation (3D)",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		Mask3D.setMandatory(false); // Not required. A null mask will estimate all voxels.
		inputParams.add(threeTensorFitOption=new ParamOption("Two-Tensor Model Fitter",
				new String[]{"Cylindrically Symmetric","Cylindrically Symmetric (equal fraction)","Positive Definite","Positive Definite (equal fraction)",
				"1xCyl. Sym. + 2xPos. Def.","1xCyl. Sym. + 2xPos. Def. (equal fraction)",
				"2xCyl. Sym. + 1xPos. Def.","2xCyl. Sym. + 1xPos. Def. (equal fraction)"}));
		threeTensorFitOption.setValue("Positive Definite");
		inputParams.add(oneTensorInitOption=new ParamOption("One-Tensor Initialization",new String[]{"Algebraic","Linear","NonLinear","RESTORE","WeightedLinear"}));
		oneTensorInitOption.setValue("Linear");

		inputParams.add(noiseLevel=new ParamFloat("Noise Level (RESTORE only)"));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system 
		 ****************************************************/
		mix1Volume = new ParamFileCollection("Mixture Fraction of Tensor 1",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		mix1Volume.setName("Mixture Fraction for Tensor 1");
		outputParams.add(mix1Volume);
		tensor1Volume = new ParamFileCollection("First Tensor Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		tensor1Volume.setName("Tensor 1 (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensor1Volume);
		mix2Volume = new ParamFileCollection("Mixture Fraction of Tensor 2",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		mix2Volume.setName("Mixture Fraction for Tensor 2");
		outputParams.add(mix2Volume);
		tensor2Volume = new ParamFileCollection("Second Tensor Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		tensor2Volume.setName("Tensor 2 (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensor2Volume);
		mix3Volume = new ParamFileCollection("Mixture Fraction of Tensor 3",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		mix3Volume.setName("Mixture Fraction for Tensor 3");
		outputParams.add(mix3Volume);
		tensor3Volume = new ParamFileCollection("Third Tensor Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		tensor3Volume.setName("Tensor 3 (xx,xy,xz,yy,yz,zz)");
		outputParams.add(tensor3Volume);
		exitCodeVolume = new ParamFileCollection("Estimation Exit Code",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		exitCodeVolume.setName("Exit Code");
		outputParams.add(exitCodeVolume);	
		intensityVolume = new ParamFileCollection("Intensity Estimate",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions));
		intensityVolume.setName("Intensity");
		outputParams.add(intensityVolume);
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {		
		TensorEstimationWrapper wrapper=new TensorEstimationWrapper();
		monitor.observe(wrapper);
		wrapper.execute();
	}


	protected class TensorEstimationWrapper extends AbstractCalculation {
		protected void execute() {
			/****************************************************
			 * Step 1. Indicate that the plugin has started.
			 * 		 	Tip: Use limited System.out.println statements
			 * 			to allow end users to monitor the status of
			 * 			your program and report potential problems/bugs
			 * 			along with information that will allow you to 
			 * 			know when the bug happened.  
			 ****************************************************/
			System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: Start");
			/****************************************************
			 * Step 2. Loop over input slabs
			 ****************************************************/
			List<File> dwList = DWdata4D.getValue();
			List<File> maskList = Mask3D.getValue();
			ImageDataReaderWriter rw  = ImageDataReaderWriter.getInstance();
			ArrayList<File> outTensor1Vols = new ArrayList<File>();
			ArrayList<File> outTensor2Vols = new ArrayList<File>();
			ArrayList<File> outTensor3Vols = new ArrayList<File>();
			ArrayList<File> outMix1Vols = new ArrayList<File>();
			ArrayList<File> outMix2Vols = new ArrayList<File>();
			ArrayList<File> outMix3Vols = new ArrayList<File>();
			ArrayList<File> outExitVols = new ArrayList<File>();
			ArrayList<File> outIntensityVols = new ArrayList<File>();
			this.addTotalUnits(dwList.size());
			for(int jSlab=0;jSlab<dwList.size();jSlab++) {
				/****************************************************
				 * Step 2. Parse the input data 
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Load data.");System.out.flush();
				this.setLabel("Load");
				ImageData dwd=rw.read(dwList.get(jSlab));//DWdata4D.getImageData();
				String imageName = dwd.getName();
				ImageDataFloat DWFloat=new ImageDataFloat(dwd);dwd.dispose();

				ImageData maskVol=null;
				if(maskList.size()>jSlab)
					maskVol=rw.read(maskList.get(jSlab));
				byte [][][]mask=null;
				if(maskVol!=null) {
					ImageDataUByte maskByte = new ImageDataUByte (maskVol);
					mask = maskByte.toArray3d();
					maskVol.dispose(); maskVol=null;
					maskByte.dispose(); maskByte=null;
				}

				System.out.println(getClass().getCanonicalName()+"\t"+"Load scheme.");System.out.flush();
				setLabel("Load scheme");
				SchemeV1 DTIscheme = null;

				XStream xstream = new XStream();
				xstream.alias("CaminoDWScheme-V1",imaging.SchemeV1.class);
				try {
					ObjectInputStream in = xstream.createObjectInputStream(new FileReader(SchemeFile.getValue()));
					DTIscheme=(SchemeV1)in.readObject();
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				}


				/****************************************************
				 * Step 3. Perform limited error checking 
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Error checking."); System.out.flush();

				ThreeTensorInversion dtiFit=null;
				String code = "";

				ModelIndex threeTensorModelIndex=ModelIndex.POSPOSPOS; 
				ModelIndex oneTensorModelIndex=ModelIndex.LDT;

				if(oneTensorInitOption.getValue().compareToIgnoreCase("Algebraic")==0) {
					oneTensorModelIndex=ModelIndex.ALGDT;
					code = "ALGDT";
				}
				if(oneTensorInitOption.getValue().compareToIgnoreCase("Linear")==0) {
					oneTensorModelIndex=ModelIndex.LDT;
					code = "LDT";
				}
				if(oneTensorInitOption.getValue().compareToIgnoreCase("NonLinear")==0) {
					oneTensorModelIndex=ModelIndex.NLDT;
					code = "NLDT";
				}
				if(oneTensorInitOption.getValue().compareToIgnoreCase("RESTORE")==0) {
					oneTensorModelIndex=ModelIndex.RESTORE;
					code = "RESTORE";
				}
				if(oneTensorInitOption.getValue().compareToIgnoreCase("WeightedLinear")==0) {
					oneTensorModelIndex=ModelIndex.LDT_WTD;
					code = "WLDT";
				}

				if(threeTensorFitOption.getValue().compareToIgnoreCase("Cylindrically Symmetric")==0) {
					threeTensorModelIndex=ModelIndex.CYLCYLCYL;
					code = code+"CYLCYLCYL";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("Cylindrically Symmetric (equal fraction)")==0) {
					threeTensorModelIndex=ModelIndex.CYLCYLCYL_EQ;
					code = code+"CYLCYLCYLEQ";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("Positive Definite")==0) {
					threeTensorModelIndex=ModelIndex.POSPOSPOS;
					code = code+"POSPOSPOS";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("Positive Definite (equal fraction)")==0) {
					threeTensorModelIndex=ModelIndex.POSPOSPOS_EQ;
					code = code+"POSPOSPOSEQ";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("1xCyl. Sym. + 2xPos. Def.")==0) {
					threeTensorModelIndex=ModelIndex.POSPOSCYL;
					code = code+"POSPOSCYL";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("1xCyl. Sym. + 2xPos. Def. (equal fraction)")==0) {
					threeTensorModelIndex=ModelIndex.POSPOSCYL_EQ;
					code = code+"POSPOSCYLEQ";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("2xCyl. Sym. + 1xPos. Def.")==0) {
					threeTensorModelIndex=ModelIndex.POSCYLCYL;
					code = code+"POSCYLCYL";
				}
				if(threeTensorFitOption.getValue().compareToIgnoreCase("2xCyl. Sym. + 1xPos. Def. (equal fraction)")==0) {
					threeTensorModelIndex=ModelIndex.POSCYLCYL_EQ;
					code = code+"POSCYLCYLEQ";
				}
				dtiFit=new ThreeTensorInversion(DTIscheme,threeTensorModelIndex,oneTensorModelIndex);
				/****************************************************
				 * Step 4. Run the core algorithm. Note that this program 
				 * 		   has NO knowledge of the MIPAV data structure and 
				 * 		   uses NO MIPAV specific components. This dramatic 
				 * 		   separation is a bit inefficient, but it dramatically 
				 * 		   lower the barriers to code re-use in other applications.
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Allocate memory."); System.out.flush();
				float [][][][]data=DWFloat.toArray4d();
				int rows = data.length;
				int cols= data[0].length;
				int slices= data[0][0].length;
				int components= data[0][0][0].length;
				float [][][][]mix1 = new float[rows][cols][slices][1];
				float [][][][]tensors1 = new float[rows][cols][slices][6];
				float [][][][]mix2 = new float[rows][cols][slices][1];
				float [][][][]tensors2 = new float[rows][cols][slices][6];
				float [][][][]mix3 = new float[rows][cols][slices][1];
				float [][][][]tensors3 = new float[rows][cols][slices][6];
				float [][][][]exitCode= new float[rows][cols][slices][1];
				float [][][][]intensity= new float[rows][cols][slices][1];


				System.out.println(getClass().getCanonicalName()+"\t"+"Run CAMINO estimate."); System.out.flush();
				setLabel("Estimate");
				EstimateTensorLLMSE.estimateCaminoThreeTensor(data,mask,dtiFit,mix1,tensors1,mix2,tensors2,mix3,tensors3,exitCode,intensity);

				/****************************************************
				 * Step 5. Retrieve the image data and put it into a new
				 * 			data structure. Be sure to update the file information
				 * 			so that the resulting image has the correct
				 * 		 	field of view, resolution, etc.  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
				System.out.println(getClass().getCanonicalName()+"\t"+"Data export."); System.out.flush();
				setLabel("Export");
				ImageDataFloat out=new ImageDataFloat(tensors1);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Tensor1"+code);
				File outputSlab = rw.write(out, getOutputDirectory());
				outTensor1Vols.add(outputSlab);

				out=new ImageDataFloat(tensors2);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Tensor2"+code);
				outputSlab = rw.write(out, getOutputDirectory());
				outTensor2Vols.add(outputSlab);

				out=new ImageDataFloat(tensors3);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Tensor3"+code);
				outputSlab = rw.write(out, getOutputDirectory());
				outTensor3Vols.add(outputSlab);


				out=new ImageDataFloat(mix1);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Mix1"+code);
				outputSlab = rw.write(out, getOutputDirectory());
				outMix1Vols.add(outputSlab);

				out=new ImageDataFloat(mix2);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Mix2"+code);
				outputSlab = rw.write(out, getOutputDirectory());
				outMix2Vols.add(outputSlab);

				out=new ImageDataFloat(mix3);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Mix3"+code);
				outputSlab = rw.write(out, getOutputDirectory());
				outMix3Vols.add(outputSlab);

				out=new ImageDataFloat(exitCode);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_ExitCode"+code);
				outputSlab = rw.write(out, getOutputDirectory());
				outExitVols.add(outputSlab);
				System.out.println(getClass().getCanonicalName()+"\t"+outputSlab);System.out.flush();

				out=new ImageDataFloat(intensity);
				out.setHeader(DWFloat.getHeader());
				out.setName(imageName+"_Intensity"+code);					
				outputSlab = rw.write(out, getOutputDirectory());
				outIntensityVols.add(outputSlab);
				/****************************************************
				 * Step 6. Let the user know that your code is finished.  
				 ****************************************************/
				System.out.println(getClass().getCanonicalName()+"\t"+"DWITensorEstLLMSE: FINISHED");

				this.incrementCompletedUnits();
			}
			tensor1Volume.setValue(outTensor1Vols);
			tensor2Volume.setValue(outTensor2Vols);
			tensor3Volume.setValue(outTensor2Vols);
			mix1Volume.setValue(outMix1Vols);
			mix2Volume.setValue(outMix2Vols);
			mix3Volume.setValue(outMix2Vols);
			exitCodeVolume.setValue(outExitVols);
			intensityVolume.setValue(outIntensityVols);
		}
	}
}
