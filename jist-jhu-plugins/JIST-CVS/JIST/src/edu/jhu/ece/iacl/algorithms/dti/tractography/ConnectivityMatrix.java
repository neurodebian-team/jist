package edu.jhu.ece.iacl.algorithms.dti.tractography;

import java.util.List;

import javax.vecmath.Point2i;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;

/**
 * This class organizes connectivity data into a connectivity matrix.
 * 
 * @author John Bogovic
 * 07/30/2009
 * 
 */
public class ConnectivityMatrix {
	
	// Variables indicating which statistic is
	// to be used across streamlines
	private static final byte mean = 0;
	private static final byte median = 1;
	private static final byte max = 2;
	private static final byte min = 3;
	private static final byte std = 4;
	
	
	/**
	 * This method organizes fiber data into a connectivity matrix.  
	 * The first input is a list of double[].  Each element of the list corresponds to a single connection.
	 * Each double[] in this list contains statistics for each fiber that is part of the connection.
	 * If the double[] is null, the corresponding entry in the connecvitity matrix will be NaN.  This may occur
	 * when no fibers are found between two regions.
	 * 
	 * Each connection is represented by a Point2i (a pair of Integer labels).  These must be contained in
	 * the input list "labelpairs" in which the ith element represents the connection for the ith fiberstatistics.
	 * Note the convension that each label pair should be organized such that x<y for each connection (Point2i).
	 * 
	 * Finally, the output connection matrix is organized by a particular set of labels, given in the input List "labels."
	 * Every x and y coord in every connection in "labelpairs" should be an element in "labels".
	 * 
	 * @param fiberstatistic The list of fiber statistics for each connection.
	 * @param labelpair The list of connections represented by each set of fiber statistics. 
	 * @param labels The list of labels.
	 * @param stat The statistic to compute across fibers for every connection.
	 * @return The connectivity matrix.
	 */
	public static double[][] compute(List<double[]> fiberstats, List<Point2i> labelpairs, List<Integer> labels, byte stat){
		
		//ensure things are the same sizes
		if(fiberstats.size()!=labelpairs.size()){
			System.err.println("jist.plugins"+"List of fibers tatistics must be the same length as " +
					"the list of label pairs!");
		}
		
		//ensure the statistic is valid
		if(stat!=mean && stat!=median && stat!=max && stat!=min && stat!=std){
			System.err.println("jist.plugins"+"Invalid statistic input: Setting equal to mean");
			stat = mean;
		}
		int N = fiberstats.size();
		int L = labels.size();
		double[][] cmtx = new double[L][L];
		double s = Double.NaN;
		for(int i=0; i<N; i++){
			if(fiberstats.get(i)==null){
				cmtx[labels.indexOf(labelpairs.get(i).x)][labels.indexOf(labelpairs.get(i).y)] = Double.NaN;
			}else{
				switch(stat){
				case mean: 		s = StatisticsDouble.mean(fiberstats.get(i)); 	break;
				case median: 	s = StatisticsDouble.median(fiberstats.get(i));	break;
				case max:		s = StatisticsDouble.max(fiberstats.get(i));	break;
				case min:		s = StatisticsDouble.min(fiberstats.get(i));	break;
				case std:		s = StatisticsDouble.std(fiberstats.get(i));	break;
				default: System.err.println("jist.plugins"+"INVALID CONNECTION STATISTIC!"); s= Double.NaN; break;
				}
				if(labels.contains(labelpairs.get(i).x) && labels.contains(labelpairs.get(i).y)){
					cmtx[labels.indexOf(labelpairs.get(i).x)][labels.indexOf(labelpairs.get(i).y)] = s;
				}else{
					System.err.println("jist.plugins"+"Label found as part of a connection, not in label list!");
				}
			}
			
		}
		
		return cmtx;
	}

}
