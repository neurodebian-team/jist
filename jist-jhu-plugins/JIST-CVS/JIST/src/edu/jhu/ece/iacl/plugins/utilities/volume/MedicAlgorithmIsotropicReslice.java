/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.volume;

import edu.jhu.ece.iacl.algorithms.volume.IsotropicResample;
import edu.jhu.ece.iacl.algorithms.volume.IsotropicResample.InterpolationMethod;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 */
public class MedicAlgorithmIsotropicReslice extends ProcessingAlgorithm{
	ParamVolume inVol;
	ParamVolume outVol;
	ParamOption interpMethod;

	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Resamples a volumetric image (using the specified interpolation method) such that the resulting volume has isotropic volumes." +
			"Output voxel size will be the finest resolution present in the original image.  (e.g. a (2x3x4)mm volume will be resampled to (2x2x2)mm";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(inVol=new ParamVolume("Volume",null,-1,-1,-1,-1));
		inputParams.add(interpMethod=new ParamOption("Interpolation Method",new String[]{"Nearest Neighbor",
				 "Trilinear",
                 "Bspline 3rd order",
                 "Bspline 4th order",
                 "Cubic Lagrangian",
                 "Quintic Lagrangian",
                 "Heptic Lagrangian",
				 "Windowed Sinc"}));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("Isotropic Resample");
		inputParams.setName("Isotropic_Resample");


		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("IACL","",""));
		info.setWebsite("iacl.ece.jhu.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription+ longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolume("Isotropic Volume",null,-1,-1,-1,-1));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		InterpolationMethod interp=null;
		switch(interpMethod.getIndex()){
			case 0:interp=InterpolationMethod.NEAREST_NEIGHBOR;break;
			case 1:interp=InterpolationMethod.TRILINEAR;break;
			case 2:interp=InterpolationMethod.BSPLINE3;break;
			case 3:interp=InterpolationMethod.BSPLINE4;break;
			case 4:interp=InterpolationMethod.CUBIC_LAGRANGIAN;break;
			case 5:interp=InterpolationMethod.QUINTIC_LAGRANGIAN;break;
			case 6:interp=InterpolationMethod.HEPTIC_LAGRANGIAN;break;
			case 7:interp=InterpolationMethod.WSINC;break;
		}
		outVol.setValue(IsotropicResample.resample(inVol.getImageData(),interp));
	}
}
