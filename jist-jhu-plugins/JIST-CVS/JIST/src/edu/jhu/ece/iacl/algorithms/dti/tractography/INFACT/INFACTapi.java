package edu.jhu.ece.iacl.algorithms.dti.tractography.INFACT;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import javax.vecmath.*;

import java.io.*;
import java.util.*;

/**
 * 
 * @author John Bogovic
 * @author Bennett Landman
 *
 */

public class INFACTapi extends AbstractCalculation{
    INFACTparameters dataConfig;
    Hashtable voxelwiseHash;
    FiberCollection allFibers;
    int minLen, maxlen, count, len;
    float mnLen;

    public INFACTapi(INFACTparameters dataConfig) {
        this.dataConfig = dataConfig;
        this.setLabel("INFACT Fiber Tracking");
    }
    
    public INFACTapi(float startFA,float stopFA,float maxTurnAngle,Point3f resolution) {
        dataConfig = new INFACTparameters();
        setParameters(startFA,stopFA,maxTurnAngle);
        this.setLabel("Fiber Tracking");
    }

    public INFACTparameters getINFACTparams(){
    	return dataConfig;
    }
    public FiberCollection getFiberCollection(){
    	return allFibers;
    }
    public void mySize(Double d[]) {
        System.out.println(d.length);
    }
    public FiberCollection toFiberCollection(){
    	FiberCollection fc = new FiberCollection();
    	fc.getFromFiberArray(toFiberArray());
    	return fc;
    }
    
    public Fiber[] toFiberArray(){
    	allFibers.trimToSize();
    	Iterator it = allFibers.iterator();
    	Fiber[] out = new Fiber[allFibers.size()];
    	int i=0;
    	while(it.hasNext()){
    		INFACTfiber f = (INFACTfiber)it.next();
    		out[i]=f.toFiber();
    		i++;
    	}
    	return out;
    }
    public boolean setParameters(float setStartFA, float setStopFA, float setMaxTurnAng) {
        dataConfig.stopFA=setStopFA;
        dataConfig.maxTurnAngle=(float)(setMaxTurnAng*Math.PI/180f);
        dataConfig.startFA=setStartFA;
        return true;
    }
    public boolean readParamFile(String file){
    	dataConfig.readParameters(file);
    	return true;
    }
    public int readLEInt(InputStream in) throws IOException {

       int byte1, byte2, byte3, byte4;

       synchronized (this) {
         byte1 = in.read();
         byte2 = in.read();
         byte3 = in.read();
         byte4 = in.read();
       }
       if (byte4 == -1) {
         throw new EOFException();
       }
       return (byte4 << 24)
        + ((byte3 << 24) >>> 8)
        + ((byte2 << 24) >>> 16)
        + ((byte1 << 24) >>> 24);
     }

    
    public FiberCollection track(){
    	if(dataConfig.useFA){
    		if(dataConfig.seedMulti){
    			System.out.println("Multi + FA");
    			return trackAllFibersUseFAMulti();
    		}else{
    			System.out.println("FA");
    			return trackAllFibersUseFA();
    		}
    	}else{
    		if(dataConfig.seedMulti){
    			System.out.println("Multi");
    			return trackAllFibersMulti();
    		}else{
    			System.out.println("Standard");
    			return trackAllFibers();
    		}
    	}
    }

    
    public FiberCollection trackAllFibers() {
    	int numFibers =0;

    	
    	
    	//voxelwiseHash = new Hashtable(numFibers*80,0.01f);
    	allFibers = new FiberCollection();
    	allFibers.setResolutions(new Point3f(dataConfig.resX,dataConfig.resY,dataConfig.resZ));
    	allFibers.setDimensions(new Point3i(dataConfig.Nx,dataConfig.Ny,dataConfig.Nz));
    	numFibers=0;
    	int len2=0;
    	long diff=0;
    	int leng7=0;
    	int minLen = 10000;
    	int maxlen =0;
    	for(short i=0;i<dataConfig.Nx;i++) {
    		for(short j=0;j<dataConfig.Ny;j++) {
    			for(short k=0;k<dataConfig.Nz;k++)  {
    				int t=0;
//    				System.out.println("BasisInds at: " + i +","+j+","+k+" is: " + dataConfig.basisInds[i][j][k][0]);
    				if(dataConfig.basisInds[i][j][k][0]>=0){
    					
//    					System.out.println("Tracking from: " + i +","+j+","+k);
    					
    					INFACTfiber newFiber = new INFACTfiber();
    					//track fiber here
    					long mem1 = Runtime.getRuntime().freeMemory();
    					int myLen=0;
    					if(dataConfig.useRK){
    						myLen=newFiber.trackRK(dataConfig,(float)i,(float)j,(float)k);
    					}else{
    						myLen=newFiber.track(dataConfig,(float)i,(float)j,(float)k);
    					}

    					long mem2 = Runtime.getRuntime().freeMemory();

  					//IGNORE FIBERS OF LENGTH < 7

    					if (myLen>dataConfig.minLength){
    						leng7++;

    						diff += (mem2-mem1);

    						if(myLen<minLen) {
    							minLen=myLen;
    						}else{
    							len2+=myLen;
    							count++;
    						}
    						if (myLen>maxlen) { maxlen=myLen; }
    						len+=myLen;

    						allFibers.add(numFibers,newFiber);
    						numFibers++;
    					}
    					//Add Points fiber to hash table to lookup by
    					//newFiber.addVxToLookupTable(voxelwiseHash,numFibers,dataConfig.Nx,dataConfig.Ny);

    				}
    				t++;
    			}
    		}
    	}
    	mnLen = (float)len/(float)numFibers;

    	System.out.println("Finished, and tracked: " + numFibers + " fibers");
    	System.out.println("Mean length: "+mnLen);
    	System.out.println("Min length: "+minLen);
    	System.out.println("Max length: "+maxlen);

    	return allFibers;
    }
    
    public FiberCollection trackAllFibersMulti() {
        int numFibers =0;

//        System.out.println("Tracking "+numFibers+" fibers...");
        
        //voxelwiseHash = new Hashtable(numFibers*80,0.01f);
        allFibers = new FiberCollection();
        numFibers=0;
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;
         for(short i=0;i<dataConfig.Nx;i++) {
                for(short j=0;j<dataConfig.Ny;j++) {
                    for(short k=0;k<dataConfig.Nz;k++)  {
                    	int t=0;
                    	while(dataConfig.mixFrac[i][j][k][t]>0.3 || t==0){
                    		if(dataConfig.basisInds[i][j][k][0]>=0){
//                  			System.out.println("Tracking another fiber from "+k+" " +j+" "+i);
                    			INFACTfiber newFiber = new INFACTfiber();
                    			//track fiber here
                    			long mem1 = Runtime.getRuntime().freeMemory();

//                    			int myLen=newFiber.track(dataConfig,(float)k,(float)j,(float)i);
//                    			int myLen=newFiber.track(dataConfig,(float)k,(float)j,(float)i,t);
                    			int myLen=0;
                    			if(dataConfig.useRK){
                    				myLen=newFiber.trackRK(dataConfig,(float)i,(float)j,(float)k);
                    			}else{
                    				myLen=newFiber.track(dataConfig,(float)i,(float)j,(float)k);
                    			}
                    			long mem2 = Runtime.getRuntime().freeMemory();

//                  			IGNORE FIBERS OF LENGTH < 7
                    			//System.out.println("Length of this fiber is: " + myLen);
                    			if (myLen>dataConfig.minLength){
                    				leng7++;

                    				diff += (mem2-mem1);

                    				if(myLen<minLen) {
                    					minLen=myLen;
                    				}else{
                    					len2+=myLen;
                    					count++;
                    				}
                    				if (myLen>maxlen) { maxlen=myLen; }
                    				len+=myLen;

                    				allFibers.add(numFibers,newFiber);
                    				numFibers++;
                    			}
                    			//Add Points fiber to hash table to lookup by
                    			//newFiber.addVxToLookupTable(voxelwiseHash,numFibers,dataConfig.Nx,dataConfig.Ny);

                    		}
                    		t++;
//                    		if(t>500){
//                    			return allFibers;
//                    		}
                    	}
                    }
                }
            }
        mnLen = (float)len/(float)numFibers;
        
        System.out.println("Finished, and tracked: " + numFibers + " fibers");
        System.out.println("Mean length: "+mnLen);
        System.out.println("Min length: "+minLen);
        System.out.println("Max length: "+maxlen);
        		
        return allFibers;
    }
    
    public FiberCollection trackAllFibersUseFAMulti() {
    	System.out.println("Tracking multiseeds using FA!");
        int numFibers =0;
        allFibers = new FiberCollection();
        numFibers=0;
        System.out.println("Here we go!");
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen = 0;
         for(short i=0;i<dataConfig.Nx;i++) {
                for(short j=0;j<dataConfig.Ny;j++) {
                    for(short k=0;k<dataConfig.Nz;k++)  {
                    	int t=0;
                    	if(dataConfig.faData[i][j][k]>dataConfig.startFA){
                    		while(dataConfig.mixFrac[i][j][k][t]>0.3 || t==0){
//                  			System.out.println("Tracking another fiber from "+k+" " +j+" "+i);
                    			INFACTfiber newFiber = new INFACTfiber();
                    			//track fiber here
                    			long mem1 = Runtime.getRuntime().freeMemory();
                    			int myLen=0;
                    			
                    			if(dataConfig.useRK){
                    				myLen=newFiber.trackRK(dataConfig,(float)i,(float)j,(float)k);
                    			}else{
                    				myLen=newFiber.track(dataConfig,(float)i,(float)j,(float)k);
                    			}
                    			
                    			long mem2 = Runtime.getRuntime().freeMemory();

//                  			IGNORE FIBERS OF LENGTH < minLength
                    			if (myLen>dataConfig.minLength){
                    				leng7++;

                    				diff += (mem2-mem1);

                    				if(myLen<minLen) {
                    					minLen=myLen;
                    				}else{
                    					len2+=myLen;
                    					count++;
                    				}
                    				if (myLen>maxlen) { maxlen=myLen; }
                    				len+=myLen;

                    				allFibers.add(numFibers,newFiber);
                    				numFibers++;
                    			}
                    			t++;
                    		}
                    	}
                    }
                }
            }
        mnLen = (float)len/(float)numFibers;
        
        System.out.println("Finished, and tracked: " + numFibers + " fibers");
        System.out.println("Mean length: "+mnLen);
        System.out.println("Min length: "+minLen);
        System.out.println("Max length: "+maxlen);
        		
        return allFibers;
    }
    
    public FiberCollection trackAllFibersUseFA() {
    	int numFibers =0;
    	allFibers = new FiberCollection();
    	numFibers=0;
    	int len2=0;
    	long diff=0;
    	int leng7=0;
    	int minLen = 10000;
    	int maxlen =0;
    	for(short i=0;i<dataConfig.Nx;i++) {
    		for(short j=0;j<dataConfig.Ny;j++) {
    			for(short k=0;k<dataConfig.Nz;k++)  {

    				if(dataConfig.faData[i][j][k]>dataConfig.startFA){
//  					System.out.println("Tracking another fiber from "+k+" " +j+" "+i);
    					INFACTfiber newFiber = new INFACTfiber();
    					//track fiber here
    					long mem1 = Runtime.getRuntime().freeMemory();
    					int myLen=0;
            			if(dataConfig.useRK){
            				myLen=newFiber.trackRK(dataConfig,(float)i,(float)j,(float)k);
            			}else{
            				myLen=newFiber.track(dataConfig,(float)i,(float)j,(float)k);
            			}
    					long mem2 = Runtime.getRuntime().freeMemory();

//  					IGNORE FIBERS OF LENGTH < minLength
    					if (myLen>dataConfig.minLength){
    						leng7++;

    						diff += (mem2-mem1);

    						if(myLen<minLen) {
    							minLen=myLen;
    						}else{
    							len2+=myLen;
    							count++;
    						}
    						if (myLen>maxlen) { maxlen=myLen; }
    						len+=myLen;

    						allFibers.add(numFibers,newFiber);
    						numFibers++;
    					}
    				}


    			}
    		}
    	}
    	mnLen = (float)len/(float)numFibers;

    	System.out.println("Finished, and tracked: " + numFibers + " fibers");
    	System.out.println("Mean length: "+mnLen);
    	System.out.println("Min length: "+minLen);
    	System.out.println("Max length: "+maxlen);

    	return allFibers;
    }
    
    public FiberCollection trackFromSeed(int i, int j, int k){        
    	
    	int numFibers =0;

//        System.out.println("Tracking "+numFibers+" fibers...");
        
        //voxelwiseHash = new Hashtable(numFibers*80,0.01f);
        allFibers = new FiberCollection();
        numFibers=0;
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;

//      while(dataConfig.mt.fracs[k][j][i][t]>0.3 || t==0){
        if(dataConfig.basisInds[i][j][k][0]>=0){
        	
//      	System.out.println("Tracking another fiber from "+k+" " +j+" "+i);
        	INFACTfiber newFiber = new INFACTfiber();
        	//track fiber here
        	long mem1 = Runtime.getRuntime().freeMemory();

//        	int myLen=newFiber.track(dataConfig,(float)k,(float)j,(float)i);
//      	int myLen=newFiber.track(dataConfig,(float)k,(float)j,(float)i,t);
        	int myLen=0;
			if(dataConfig.useRK){
				myLen=newFiber.trackRK(dataConfig,(float)i,(float)j,(float)k);
			}else{
				myLen=newFiber.track(dataConfig,(float)i,(float)j,(float)k);
			}
        	
        	long mem2 = Runtime.getRuntime().freeMemory();

//      	IGNORE FIBERS OF LENGTH < 7
        	//System.out.println("Length of this fiber is: " + myLen);
        	if (myLen>dataConfig.minLength){
        		leng7++;

        		diff += (mem2-mem1);

        		if(myLen<minLen) {
        			minLen=myLen;
        		}else{
        			len2+=myLen;
        			count++;
        		}
        		if (myLen>maxlen) { maxlen=myLen; }
        		len+=myLen;

        		allFibers.add(numFibers,newFiber);
        		numFibers++;
        	}

        	//System.out.println("Allfibers is length " + allFibers.size());

        	//Add Points fiber to hash table to lookup by
        	//newFiber.addVxToLookupTable(voxelwiseHash,numFibers,dataConfig.Nx,dataConfig.Ny);

        }

//      }
//      System.out.println("Seeded " + t);
        //System.out.println("####### Moving to next voxel #######");

        mnLen = (float)len/(float)numFibers;
        
        System.out.println("Finished, and tracked: " + numFibers + " fibers");
        System.out.println("Mean length: "+mnLen);
        System.out.println("Min length: "+minLen);
        System.out.println("Max length: "+maxlen);
        		
        return allFibers;
    }
    
    public FiberCollection trackFromSeedFA(int i, int j, int k){        
    	
    	int numFibers = 1;
        
        //voxelwiseHash = new Hashtable(numFibers*80,0.01f);
        allFibers = new FiberCollection();
        numFibers=0;
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;

//      while(dataConfig.mt.fracs[k][j][i][t]>0.3 || t==0){
        if(dataConfig.faData[i][j][k]>dataConfig.startFA){
//      	System.out.println("Tracking another fiber from "+k+" " +j+" "+i);
        	INFACTfiber newFiber = new INFACTfiber();
        	//track fiber here
        	long mem1 = Runtime.getRuntime().freeMemory();

//        	int myLen=newFiber.track(dataConfig,(float)k,(float)j,(float)i);
//      	int myLen=newFiber.track(dataConfig,(float)k,(float)j,(float)i,t);
        	int myLen=0;
			if(dataConfig.useRK){
				myLen=newFiber.trackRK(dataConfig,(float)i,(float)j,(float)k);
			}else{
				myLen=newFiber.track(dataConfig,(float)i,(float)j,(float)k);
			}
			
        	long mem2 = Runtime.getRuntime().freeMemory();

//      	IGNORE FIBERS OF LENGTH < 7
        	//System.out.println("Length of this fiber is: " + myLen);
        	if (myLen>dataConfig.minLength){
        		leng7++;

        		diff += (mem2-mem1);

        		if(myLen<minLen) {
        			minLen=myLen;
        		}else{
        			len2+=myLen;
        			count++;
        		}
        		if (myLen>maxlen) { maxlen=myLen; }
        		len+=myLen;

        		allFibers.add(numFibers,newFiber);
        		numFibers++;
        	}
        }


        mnLen = (float)len/(float)numFibers;
        
//        System.out.println("Finished, and tracked: " + numFibers + " fibers");
//        System.out.println("Mean length: "+mnLen);
//        System.out.println("Min length: "+minLen);
//        System.out.println("Max length: "+maxlen);
        		
        return allFibers;
    }

    private Fiber seedFiber(int x, int y, int z){
    	
        int len2=0;
        long diff=0;
        int leng7=0;
        int minLen = 10000;
        int maxlen =0;
        
    	INFACTfiber newFiber = new INFACTfiber();
    	//track fiber here
    	
    	long mem1 = Runtime.getRuntime().freeMemory();
    	
    	int myLen=0;
		if(dataConfig.useRK){
			myLen=newFiber.trackRK(dataConfig,(float)x,(float)y,(float)z);
		}else{
			myLen=newFiber.track(dataConfig,(float)x,(float)y,(float)z);
		}
		
    	long mem2 = Runtime.getRuntime().freeMemory();

     	// IGNORE FIBERS OF LENGTH < 7
    	if (myLen>dataConfig.minLength){
    		leng7++;

    		diff += (mem2-mem1);

    		if(myLen<minLen) {
    			minLen=myLen;
    		}else{
    			len2+=myLen;
    			count++;
    		}
    		if (myLen>maxlen) { maxlen=myLen; }
    		len+=myLen;

    		return newFiber;
    	}
    	return null;
    }

    public List findFibersByPoints(Point3f pts[]) {
        LinkedList<Fiber> lst = new LinkedList<Fiber>();
        for(int i=0;i<allFibers.size();i++) {
            if(((INFACTfiber)allFibers.get(i)).containsPoints(pts))
            	//System.out.println("Fiber passing through point...");
                lst.add(allFibers.get(i));
        }
        return lst;
    }
    
    //int i is the max
    private int roundinbound(float f, int max){
    	int ff = Math.round(f);
    	if(ff<0){ ff=0; }
    	else if(ff>max){ ff=max; }
    	return ff;
    }
    
}