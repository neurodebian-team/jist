package edu.jhu.ece.iacl.algorithms.dti.tractography.FACT;

public class RunFACT {

	/**
	 * FA file
	 * VEC file
	 * Parameter file
	 */
	
	public static void main(String[] args) {
		
//		String faname = args[0];
//		String vecname = args[1];
//		String paramname = args[2];
		
//		String subname = "ac_15";
//		String dirname = "/home/john/Research/20070727/corrected_dti/processed/ac_15/";
//		String faname = dirname + subname+".fa";
//		String vecname = dirname + subname+".vec";
//		String paramname = "/home/john/Research/20070727/FACT/FACTparam3 .txt";
		
//		String faname = "/iacl/cruise07/bennett/projects/CSdeconv/data2/dti/RDR1_20080313_cerebellum_3_1_sc.fa";
//		String vecname= "/iacl/cruise07/bennett/projects/CSdeconv/data/FLT1_at1085_A_3_1_sc1.vec";
//		String vecname= "/iacl/cruise07/bennett/projects/CSdeconv/data2/dti/RDR1_20080313_cerebellum_3_1_sc.vec";
		
		String faname = "/home/john/Research/DTIcompressedSensing/RDR1_20080313_cerebellum_3_1_sc.fa";
		String vecname = "/home/john/Research/DTIcompressedSensing/RDR1_20080313_cerebellum_3_1_sc.vec";
			
		String paramname = "/home/john/Research/20070727/FACT/FACTparam2.txt";
			
//		String fiberfile = "/home/john/Research/DTIcompressedSensing/ac_15_iaclTEST.dat";
		
		
		int x = 107;
		int y = 124;
		int z = 36;
		
		System.out.println("jist.plugins"+"\t"+faname);
		System.out.println("jist.plugins"+"\t"+vecname);
		System.out.println("jist.plugins"+"\t"+paramname);
//		System.out.println("jist.plugins"+"\t"+fiberfile);
		
		long start = System.currentTimeMillis();
		System.out.println("jist.plugins"+"\t"+"Running...");
		
//		/home/john/Resarch/dti_cruise/factparamtest.txt
		
		FACTapi ftracker = new FACTapi();
		ftracker.dataConfig.setDefaultParameters();
		System.out.println("jist.plugins"+"\t"+"Reading Parameters: " + paramname);
		ftracker.readParamFile(paramname);
		
		System.out.println("jist.plugins"+"\t"+"Loading files...");
	    ftracker.loadData(faname,vecname,
	    		ftracker.dataConfig.Nx,ftracker.dataConfig.Ny,ftracker.dataConfig.Nz,
	    		ftracker.dataConfig.resX,ftracker.dataConfig.resY,ftracker.dataConfig.resZ);
	    System.out.println("jist.plugins"+"\t"+"Finished Loading!");
	    long data = System.currentTimeMillis();
	    
	    System.out.println("jist.plugins"+"\t"+"Fiber Tracking...");
//	    ftracker.trackAllFibers();
	    ftracker.trackFromSeed(x, y, z);
	    System.out.println("jist.plugins"+"\t"+"Finished Tracking!");
	    long track= System.currentTimeMillis();
        System.out.println("jist.plugins"+"\t"+"Complete: "+((data-start)/10)/100f+" s to load data, "+((track-data)/10)/100f+" s to track fibers");

        
        if(ftracker.allFibers!=null){
        	System.out.println("jist.plugins"+"\t"+"Writing Fibers...");
//        	String fiberfile = faname.substring(0, faname.lastIndexOf(".")) + "_FACTfibsTEST.vtk";
        	String fiberfile = "/home/john/Research/DTIcompressedSensing/FACTvsINFACT/FACT_"+x+"_"+y+"_"+z+".vtk";
        	System.out.println("jist.plugins"+"\t"+fiberfile);
        	ftracker.writeFibersVTK(fiberfile);
        	
        	System.out.println("jist.plugins"+"\t"+"Finished Writing!");
        }
	    
		System.out.println("jist.plugins"+"\t"+"Done!");
		

	}

}
