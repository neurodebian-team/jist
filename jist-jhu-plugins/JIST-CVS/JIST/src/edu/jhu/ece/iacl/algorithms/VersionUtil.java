package edu.jhu.ece.iacl.algorithms;

public class VersionUtil {
	public static String parseRevisionNumber(String cvsversion){
		return cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	}
}
