package edu.jhu.ece.iacl.pami;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.algorithms.hardi.SurfaceTools;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import gov.nih.mipav.view.renderer.J3D.model.structures.ModelTriangleMesh;


public class CreateTesselatedSurface extends ProcessingAlgorithm{
	/*
	 * Combines the following plugins to simplify things:
	 * 		CreatePlatonicSolidSurface
	 * 		TesselateSurface
	 * 		NormalizeToUnitMagnitude
	 * 		SelectUniqueDiffusionDirectoins
	 */

	// Input parameters
	private ParamOption shape;
	private ParamInteger order;

	// Output parameters
	private ParamSurface outputSurface;

	private static final String cvsversion = "$Revision: 1.6 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Creates a tesselated surface on the unit sphere.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.setPackage("PAMI");
		inputParams.setCategory("Tools");
		inputParams.setLabel("Create Tesselated Surface");
		inputParams.setName("Create_Tesselated_Surface");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("http://www.nitrc.org/projects/jist/");
		info.setAffiliation("Johns Hopkins University");
		info.add(new AlgorithmAuthor("Hanlin Wan", "hanlinwan@gmail.com", ""));
		info.add(new AlgorithmAuthor("Bennett Landman", "landman@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);


		inputParams.add(shape=new ParamOption("Select shape",new String[]{"Tetrahedron","Octahedron","Icosahedron","Cube-Triangulated","Dodecahedron-Triangulated"}));
		inputParams.add(order=new ParamInteger("Tesselation Order",2,500,2));
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outputSurface=new ParamSurface("Solid Mesh"));
	}


	protected void execute(CalculationMonitor monitor) {
		// Create Platonic Solid
		EmbeddedSurface surf = null;
		if(shape.getValue().compareToIgnoreCase("Tetrahedron")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.tetrahedron()));
		} else if(shape.getValue().compareToIgnoreCase("Cube-Triangulated")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.cube()));
		} else if(shape.getValue().compareToIgnoreCase("Dodecahedron-Triangulated")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.dodecahedron()));
		} else if(shape.getValue().compareToIgnoreCase("Octahedron")==0) {
			surf=(new EmbeddedSurface(SurfaceTools.octahedron()));
		} else if(shape.getValue().compareToIgnoreCase("Icosahedron")==0) {
			surf=(new EmbeddedSurface(icosahedron()));
		} else {
			throw new RuntimeException("Unknown shape");
		}

		// Tesselate the surface, normalize it, and pick out the unique directions
		surf = new EmbeddedSurface(SurfaceTools.normalizeToSphere(surf,1));
		EmbeddedSurface surf2 = new EmbeddedSurface(SurfaceTools.tesselate(surf,order.getInt()-1));
		EmbeddedSurface surf3 = new EmbeddedSurface(SurfaceTools.normalizeToSphere(surf2, 1));
		EmbeddedSurface surf4 = new EmbeddedSurface(SurfaceTools.selectUniqueDirectionsAndReflections(surf3,1e-3));
		surf4.setName(shape.getValue()+"_inscribed"+"_tesselate"+order.getInt());
		outputSurface.setValue(surf4);
	}


	ModelTriangleMesh icosahedron() {
		double phiaa  = 26.56505; //%/ phi needed for generation *
		double phia = Math.PI*phiaa/180.0; //%/ 2 sets of four points *
		double theb = Math.PI*36.0/180.0;  //%/ offset second set 36 degrees *
		double the72 = Math.PI*72.0/180;  //% / step 72 degrees *
		Point3f []pts = new Point3f[12];
		pts[0] = new Point3f(0,0,1);
		pts[11] = new Point3f(0,0,-1);
		//	  vertices(1,:) = [0 0 r];
		//	  vertices(12,:) = [0 0 -r];
		double the = 0.0;
		for(int i=1;i<=5;i++) {
			pts[i]=new Point3f((float)(Math.cos(the)*Math.cos(phia)),(float)(Math.sin(the)*Math.cos(phia)),(float)( Math.sin(phia)));
			the = the+the72;
		}
		the=theb;
		for(int i=6;i<=10;i++) {
			pts[i]=new Point3f((float)(Math.cos(the)*Math.cos(-phia)),(float)(Math.sin(the)*Math.cos(-phia)),(float)( Math.sin(-phia)));
			//	    vertices(i+1,:) = [r*cos(the)*cos(-phia) r*sin(the)*cos(-phia) r*sin(-phia)];
			the = the+the72;
		}

		//	  % / map vertices to 20 faces *
		int []connect = {0,1,2,
				0,2,3,
				0,3,4,
				0,4,5,
				0,5,1,
				11,6,7,
				11,7,8,
				11,8,9,
				11,9,10,
				11,10,6,
				1,2,6,
				2,3,7,
				3,4,8,
				4,5,9,
				5,1,10,
				6,7,2,
				7,8,3,
				8,9,4,
				9,10,5,
				10,6,1};
		return new ModelTriangleMesh(pts,connect);
	}
}
