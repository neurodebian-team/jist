package edu.jhu.ece.iacl.plugins.utilities.logic;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;


public class MedicAlgorithmValueLessThan extends ProcessingAlgorithm{
	protected ParamDouble upperBound;
	protected ParamDouble value;
	protected ParamBoolean inclusive;
	protected ParamBoolean assertTrue;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		setRunningInSeparateProcess(false);
		inputParams.add(value=new ParamDouble("Value"));
		inputParams.add(upperBound=new ParamDouble("Upper Bound"));
		inputParams.add(inclusive=new ParamBoolean("Include Bound",true));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Logic");
		inputParams.setLabel("Value Less Than");
		inputParams.setName("Value_Less_Than");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(assertTrue=new ParamBoolean("Assertion"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		double val=value.getDouble();
		double upper=upperBound.getDouble();
		if(inclusive.getValue()){
			assertTrue.setValue((val<=upper));
		} else {
			assertTrue.setValue((val<upper));
		}
	}
}
