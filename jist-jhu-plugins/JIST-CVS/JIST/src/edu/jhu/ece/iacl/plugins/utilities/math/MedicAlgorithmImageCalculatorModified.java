/*
 *
 */
package edu.jhu.ece.iacl.plugins.utilities.math;

import java.util.ArrayList;
import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;


/*
 * This is the modified version of MedicAlgorithmImageCalculator written by Blake Lucas (bclucas@jhu.edu).
 * It takes two ParamVolumeCollection inputs instead of two ParamVolume inputs.
 * Modified by Robert Kim (rkim35@jhu.edu) on 10/24/08.
 * @author Robert Kim
 */
public class MedicAlgorithmImageCalculatorModified extends ProcessingAlgorithm{
	ParamVolumeCollection vol1Param;
	ParamVolumeCollection vol2Param;
	ParamVolumeCollection resultVolParam;
	ParamOption operation;

	private static final String cvsversion = "$Revision: 1.4 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Image Calculator which takes VolumeCollections instead of volumes as inputs. The operation include 'Add', 'Subtract', 'Multiply', and 'Divide' which are performed pairwise on the VolumeCollections.";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(vol1Param=new ParamVolumeCollection("Volume Collection 1"));
		inputParams.add(vol2Param=new ParamVolumeCollection("Volume Collection 2"));
		inputParams.add(operation=new ParamOption("Operation",new String[]{"Add","Subtract","Multiply","Divide"}));


		inputParams.setPackage("IACL");
		inputParams.setCategory("Utilities.Math");
		inputParams.setLabel("Image Calculator VolumeCollections");
		inputParams.setName("Image_Calculator_VolumeCollections");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Robert Kim", "rkim35@jhu.edu", ""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(resultVolParam=new ParamVolumeCollection("Result Volume"));
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		List<ImageData> vol1List = vol1Param.getImageDataList();
		List<ImageData> vol2List = vol2Param.getImageDataList();
		ArrayList<ImageData> listout = new ArrayList<ImageData>();
		int N = vol2List.size();
		for(int l=0;l<N;l++){
			ImageData vol1=vol1List.get(l);
			ImageData vol2=vol2List.get(l);
			int rows=vol1.getRows();
			int cols=vol1.getCols();
			int slices=vol1.getSlices();
			int components = vol1.getComponents();
			if(vol2.getRows()!=rows ||
					vol2.getCols()!=cols ||
					vol2.getSlices()!=slices ||
					vol2.getComponents()!=components)
				throw new RuntimeException("Volumes of unequal size cannot be combined.");
			ImageDataFloat resultVol=new ImageDataFloat(vol1.getName()+"_calc",rows,cols,slices,components);
			double tmp1,tmp2;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					for (int k = 0; k < slices; k++) {
						if(components<=1) {
							tmp1=vol1.getDouble(i, j, k);
							tmp2=vol2.getDouble(i, j, k);

							switch(operation.getIndex()){
							case 0:resultVol.set(i, j, k, tmp1+tmp2);break;
							case 1:resultVol.set(i, j, k, tmp1-tmp2);break;
							case 2:resultVol.set(i, j, k, tmp1*tmp2);break;
							case 3:resultVol.set(i, j, k, tmp1/tmp2);break;
							default:
							}
						} else {
							for(int m=0;m<components;m++) {
								tmp1=vol1.get(i, j, k, m).doubleValue();
								tmp2=vol2.get(i, j, k, m).doubleValue();

								switch(operation.getIndex()){
								case 0:resultVol.set(i, j, k, m, tmp1+tmp2);break;
								case 1:resultVol.set(i, j, k, m, tmp1-tmp2);break;
								case 2:resultVol.set(i, j, k, m, tmp1*tmp2);break;
								case 3:resultVol.set(i, j, k, m, tmp1/tmp2);break;
								default:
								}
							}
						}

					}
				}
			}
			resultVol.setHeader(vol1.getHeader());
			listout.add(resultVol);
		}
		resultVolParam.setValue(listout);
	}
}
