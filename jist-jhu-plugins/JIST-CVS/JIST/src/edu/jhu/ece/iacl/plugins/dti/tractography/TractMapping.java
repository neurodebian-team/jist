package edu.jhu.ece.iacl.plugins.dti.tractography;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.algorithms.dti.tractography.FiberStatistics;
import edu.jhu.ece.iacl.jist.io.FiberCollectionReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.fiber.Fiber;
import edu.jhu.ece.iacl.jist.structures.fiber.FiberCollection;
import edu.jhu.ece.iacl.jist.structures.fiber.XYZ;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataFloat;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.JistLogger;


public class TractMapping extends ProcessingAlgorithm{

	/****************************************************
	 * Input Parameters
	 ****************************************************/
	private ParamObject<FiberCollection> fibers;
	private ParamVolumeCollection contrasts;
	private ParamInteger interpPoints; 
	
	/****************************************************
	 * Output Parameters
	 ****************************************************/
	private ParamVolume TractMapVol;
	private ParamFile TractMapVolDescription;
	private ParamFile TractMapStatsCSV;
	//private ParamVolume TractMapStatsVol;


	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = 	"Interpolated Tract Based Parameter Mapping";
	private static final String longDescription = 	"Interpolates fibers trajectories to a fixed number of points.\n" +
													"Then maps these tracts onto a volume of interest and reports the values per tract, as well means.\n" +
													
													"\nInputs are:\n" +
														"\tFiber trajectory collection consisting of the trajectories from a single fiber bundle. \n" +
														"\tVolume(s) of contrast of interest, in the same space as where the fibers originate from\n" +
														"\t (i.e. the same b=0 image). Allows multiple volumes to be sampled with one set of tracts.\n" +
														"\tNumber of points to interpolate the trajectories to.\n" +
														
													"\nOutput consists of:\n" +
														"\tImage volume containing resampled fibers and the mapped data, dimensions (#pts, #tracts, XYZ+#contrasts)\n" +
														"\t.txt file with full description of data volume\n" +
														"\t.csv file with data summary; dimensions (#points, means and std dev of (XYZ and # contrasts))";


	protected void createInputParameters(ParamCollection inputParams) {
		/****************************************************
		 * Step 1. Set Plugin Information
		 ****************************************************/
		inputParams.setPackage("IACL");
		inputParams.setCategory("DTI.Fiber");
		inputParams.setLabel("TractMapping : Interpolated Tract Based Parameter Mapping");
		inputParams.setName("TractMapping");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("");
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription +"\n" + longDescription);
		info.add(new AlgorithmAuthor("Bennett Landman","landman@jhu.edu",""));
		info.setAffiliation("Johns Hopkins University, Department of Biomedical Engineering");
		info.add(new AlgorithmAuthor("Daniel Polders","daniel.polders@gmail.com", ""));
		info.setAffiliation("UMC Utrecht, Department of Radiology, 7 Tesla Group. Radiology, the Netherlands");
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.ALPHA);
		
		/****************************************************
		 * Step 2. Add input parameters to control system
		 ****************************************************/
		inputParams.add(fibers=new ParamObject<FiberCollection>("Fibers", new FiberCollectionReaderWriter()));
		inputParams.add(contrasts = new ParamVolumeCollection("DataVolume",null,-1,-1,-1,-1));
		contrasts.setLoadAndSaveOnValidate(false);

		inputParams.add(interpPoints = new ParamInteger("Points Per Curve",1,10000,100));
	}

	public Fiber resample(Fiber in, int N, double resX, double resY, double resZ) {
		/* This function calculates the new coordinates of the input fibers, interpolated using a spline function 
		 */
		XYZ []newXYZ = new XYZ[N];
		double []len = new double[in.getLength()];
		double []x= new double[in.getLength()];;
		double []y= new double[in.getLength()];;
		double []z= new double[in.getLength()];;
		//double length=0;
		len[0]=0;
		XYZ[] xyzChain = in.getXYZChain();
		x[0]=xyzChain[0].x;
		y[0]=xyzChain[0].y;
		z[0]=xyzChain[0].z;
		for(int i=1; i<xyzChain.length; i++){
			x[i] = xyzChain[i].x;
			y[i] = xyzChain[i].y;
			z[i] = xyzChain[i].z;
			XYZ pt1=xyzChain[i].deepClone();
			XYZ pt2=xyzChain[i-1].deepClone();
			pt1.scale(resX, resY, resZ);
			pt2.scale(resX, resY, resZ);
			len[i]=len[i-1]+pt1.distance(pt2);
		}
		// Calculate spline coefficients of interpolated pathways
		edu.jhu.bme.smile.commons.math.Spline spX = new edu.jhu.bme.smile.commons.math.Spline(len,x);
		edu.jhu.bme.smile.commons.math.Spline spY = new edu.jhu.bme.smile.commons.math.Spline(len,y);
		edu.jhu.bme.smile.commons.math.Spline spZ = new edu.jhu.bme.smile.commons.math.Spline(len,z);

		// double t = 0;
		//  dt = len[len.length]/(N-1);
		double dt =len[len.length-1]/(N-1);
		/*  get value of spline 'spX' at location dt which is */
		for(int i=0;i<N;i++) {
			newXYZ[i]=new XYZ();
			newXYZ[i].setX((float)spX.spline_value(dt*i));
			newXYZ[i].setY((float)spY.spline_value(dt*i));
			newXYZ[i].setZ((float)spZ.spline_value(dt*i));
		}
		return new Fiber(newXYZ);
	}

	protected void createOutputParameters(ParamCollection outputParams) {
		/****************************************************
		 * Step 1. Add output parameters to control system
		 ****************************************************/
		TractMapVol = new ParamVolume("Tract Mapping Volume",null,-1,-1,-1,1);
		outputParams.add(TractMapVol);
		TractMapVolDescription = new ParamFile("Tract Mapping Volume Description (*.txt)");
		TractMapStatsCSV = new ParamFile("Tract Mapping Statistics(*.csv)");
		//TractMapStatsVol = new ParamVolume("Tract Mapping Statistics Volume",null,-1,-1,-1,1);
		//outputParams.add(TractMapStatsVol);

	}
	


	protected void execute(CalculationMonitor monitor) {
		//		FiberStatistics fs = new FiberStatistics(fibers.getObject());
		FiberCollection dti = fibers.getObject();

		//		fibervol.setValue(fs.fibers2Volume(statoi.getValue(), t.empvol.getImageData()));
		int Npts = interpPoints.getValue().intValue();
		int Nfibers = dti.size();
		int Ncontrasts = 1;
		int contrastOffset =3;
		String name = (fibers.getObject()).getName();//Get filename of fibercollection
		
		
		ImageData simg = new ImageDataFloat(Npts,Nfibers,Ncontrasts+contrastOffset,1);
		ImageData simgStats = new ImageDataFloat(Npts,2*(Ncontrasts+contrastOffset),1,1);
		
		JistLogger.logError(JistLogger.INFO, "Starting TractMapping, Input parameters are:\n" +
				"\tnumber of fibers: "+Nfibers + "\n" +
				"\tNpts: "+Npts + "\n" +
				"\tfilename: "+ name +"\n");
		
		name = name + "_TractMapped";
		
		String filename = this.getOutputDirectory().toString()+ File.separatorChar + name + ".txt";
		File descFile= new File(filename);
		String filenameStats = this.getOutputDirectory().toString() + File.separatorChar + name + "_stats.csv";
		File FileStats= new File(filenameStats);
		JistLogger.logError(JistLogger.INFO, "Output base filename: " + name);
		
		PrintWriter fp;
		PrintWriter fp2;
		
		String nls= System.getProperty("line.separator"); // get newline string (PLATFORM DEPENDEND)

		try {
			JistLogger.logError(JistLogger.INFO, "Writing Tract info to output file: " + name+".txt");
			fp = new PrintWriter(descFile);
			fp.write("Dim 0: Equally Sampled Points on Fiber Curves"+nls);
			fp.write("Dim 1: One Entry Per Fiber"+nls);
			fp.write("Dim 2: As Follows"+nls);
			fp.write("\tDim 2.1: 'x' of resampled fibers in voxels"+nls);
			fp.write("\tDim 2.2: 'y' of resampled fibers in voxels"+nls);
			fp.write("\tDim 2.3: 'z' of resampled fibers in voxels"+nls);
			int jContrast =0 ;
			for(int i=0;i<contrasts.size();i++) {
				ImageData img = contrasts.getParamVolume(i).getImageData();
				for(int jComp=0;jComp<img.getComponents();jComp++){
					fp.write("\tDim 2."+(3+jContrast)+": "+img.getName()+", component: "+ jComp + nls);
					jContrast++;
				}
			}	
			fp.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				JistLogger.logError(JistLogger.WARNING, "Cannot write to output file.\n"+e.getStackTrace());
			} 
			
		
		ImageHeader hdr = contrasts.getParamVolume(0).getImageData().getHeader();
		double resX = hdr.getDimResolutions()[0];
		double resY = hdr.getDimResolutions()[1];
		double resZ = hdr.getDimResolutions()[2];
		
		// Re-sample all fibers to equal #points
		for(int jFiber=0;jFiber<Nfibers;jFiber++ ) {
			Fiber fiber = dti.get(jFiber);
			JistLogger.logError(JistLogger.INFO, "Resampling " + contrasts.getParamVolume(0).getName() + " to fiber no."+jFiber);
			Fiber resampledFiber = resample(fiber,Npts,resX,resY,resZ);
			XYZ[] xyz = resampledFiber.getXYZChain();
			// Report resampled X,Y,Z in dim 0-2
			for(int jPt = 0;jPt<xyz.length;jPt++) {
				simg.set(jPt,jFiber,0,xyz[jPt].x);
				simg.set(jPt,jFiber,1,xyz[jPt].y);
				simg.set(jPt,jFiber,2,xyz[jPt].z);
			}
			int jContrast =0 ;

			// Get values of contrasts for new fibers
			for(int i=0;i<contrasts.size();i++) {
				ImageData img = contrasts.getParamVolume(i).getImageData();
				for(int jComp=0;jComp<img.getComponents();jComp++){
					for(int jPt = 0;jPt<xyz.length;jPt++) {
						simg.set(jPt,jFiber,jContrast+contrastOffset,img.get(Math.round(xyz[jPt].x), Math.round(xyz[jPt].y),Math.round(xyz[jPt].z),jComp));					
					}
					jContrast++;
				}
			}			
		}
		
		// Get means and stds for all fibers from simg
		// simg dimensions: [interpol pts / fibers / (X, Y, Z, Comp1, Comp2, etc)] i.e. 3-D
		// simgStats: [interpol pts / (mean X, std X, mean Y, std Y, mean Z, std Z, mean Comp1, std Comp1, etc)] i.e. 2-D

		// get values of simg over all fibers, for each point in interpolated list, and for all components
		for(int i=0; i< Npts; i++ ){  							//for all nPts
			for(int k=0; k<Ncontrasts+contrastOffset;k++){ 		// all components (X,Y,Z,C1, C2 etc)
				double[] vec = new double[Nfibers];
				for(int j= 0; j< simg.getCols(); j++){ 			// for all fibers
					vec[j] = simg.getDouble(i, j, k);	
				}
			simgStats.set(i, 2*k, StatisticsDouble.mean(vec));
			simgStats.set(i, 2*k+1, StatisticsDouble.std(vec));
			}
		}
		
		//start writing CSV to fp2, per column
		// Actually, make that a semi-csv (semi-colon separated values) as that seems to prevent localization based problems,
		// commas for decimal signs and such, plus Excel seems to like it better
		try {
			fp2 = new PrintWriter(FileStats);
			JistLogger.logError(JistLogger.INFO, "Writing tract statistics to output file: " + filenameStats);
			//write csv header (first line)
			fp2.write("Location" +
					";mean X" +
					";std X" +
					";mean Y" +
					";std Y" +
					";mean Z" +
					";std Z");
			for(int n=0 ;n<Ncontrasts;n++){
				String contrastString = contrasts.getParamVolume(n).getName();
				fp2.write(";mean "+ contrastString+"[" + n + "]" );
				fp2.write(";std "+ contrastString+"[" + n + "]" );
			}
			for(int i = 0; i < Npts; i++) {
				fp2.append(nls);
				fp2.print(i);
		    	for(int k = 0; k < simgStats.getCols(); k++){ //get line
		    		fp2.append(";");
		    		fp2.print(simgStats.getFloat(i, k));		 
		    	}
		    }
		    fp2.close();
		}
		catch(Exception e)
		{
			JistLogger.logError(JistLogger.WARNING, "Could not write statistics to output csv file.\n"+e.getStackTrace());
		}
		
		simg.setName(name);
		TractMapVol.setValue(simg);
		//simgStats.setName(name+"_Stats");
		//TractMapVolStatsVol.setValue(simgStats);
		TractMapVolDescription.setValue(descFile);
		TractMapStatsCSV.setValue(FileStats);
	}
}
