package edu.jhu.ece.iacl.algorithms.manual_label.staple;

/**
 * 
 * @author John Bogovic, Hanlin Wan
 * @date 5/31/2008
 * 
 * Simultaneous Truth and Performance Level Estimation (STAPLE)
 * 
 * Warfield, Zou, and Wells, "Simultaneous Truth and Performace Level Estimation (STAPLE):
 * An Algorithm for the Validation of Image Segmentation," 
 * IEEE Trans. Medical Imaging vol. 23, no. 7, 2004
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;

import javax.vecmath.Point3f;

import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataInt;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;

public class STAPLEgeneral2 extends AbstractCalculation{

	protected double[][] raterData;
	protected Truth[] truth;
	protected int[][] t;
	protected double[] priorArray;
	
	EmbeddedSurface surf;			//contains connectivity info if required
	int[][] nbrtable;				//vertex connectivity information
	
	protected PerformanceLevel pl; //performance level estimates
	protected ArrayList<Float> priors;
	protected ArrayList<Double> labels;
	protected float normtrace;
	protected int maxiters = 1000;
	protected String dir;
	protected double eps=.00001;
	protected float beta=0f;			//regularization term
	protected float sigma=0f;			//kernel width for spatial smoothing
	protected int mrfiters = 15;
	protected String init;
	protected String mrfMethod = "RL";
	protected int sortSize = 1;
	protected int inds, raters, labelSize;
		
	public STAPLEgeneral2(){
		super();
		setLabel("STAPLE");
	}
	public STAPLEgeneral2(double[][] data){
		super();
		setLabel("STAPLE");
		setData(data);
	}
	
	public void setMRFMethod(String mrf){
		this.mrfMethod=mrf;
		if (mrf.compareTo("RL")==0)
			sortSize=1;
		else
			sortSize=2;
	}

	public void setInit(String init){
		this.init=init;
	}
	
	public void setmaxIters(int max){
		maxiters=max;
	}
	public void setBeta(float beta){
		this.beta=beta;
	}
	public void setSigma(float sigma){
		this.sigma=sigma;
	}
	public void setEps(double eps){
		this.eps=eps;
	}
	public void setSurface(EmbeddedSurface surf){
		this.surf=surf;
	}
	
	public void setData(double[][] data){
		raterData=data;
		inds=data.length;
		raters=data[0].length;
		getPriorProb();
	}
	public void setDir(String dir){
		this.dir = dir;
	}
	public ArrayList<Double> getLabels(){
		return labels;
	}
	
	public int getNumLabels() {
		return labelSize;
	}
		
	public void findLabels(){
		labels = new ArrayList<Double>();
		if(raterData!=null){
			for(int i=0; i<inds; i++){
				for(int j=0; j<raters; j++){
					if(!labels.contains(raterData[i][j])){
						labels.add(raterData[i][j]);
					}
				}
			}
		}else{
			System.err.println("jist.plugins"+"No data!");
		}
		labels.trimToSize();
		labelSize=labels.size();
		System.out.println("jist.plugins"+"\t"+"Found Labels: ");
		System.out.println("jist.plugins"+"\t"+labels);
		System.out.println("jist.plugins"+"\t"+"");
	}
	
	public void initialize(){
		if(init.equals("Truth")){
			System.err.println("jist.plugins"+"NOT YET IMPLEMENTED!");
		}else {
			double init = 0.9999;
			findLabels();
			System.out.println("jist.plugins"+"\t"+"Labels Found: " + labelSize);
			System.out.println("jist.plugins"+"\t"+"Num Rater Images: " + raters);
			System.out.flush();
			pl = new PerformanceLevel(labelSize,raters,true);
			pl.initialize(init);
			createT();
			if (beta==0) mrfMethod="none";
			System.out.println("jist.plugins\tMRF Method: " + mrfMethod);
		}
		if(beta>0){
			if(surf==null){
				System.err.println("jist.plugins"+"Non-zero beta requires a surface object");
			}else{
				nbrtable = EmbeddedSurface.buildNeighborVertexVertexTable(surf, EmbeddedSurface.Direction.COUNTER_CLOCKWISE);
				for (int i = 0; i < inds; i++) {
					int[] nbrs=nbrtable[i];					
					Arrays.sort(nbrs);
					float[] w = computeDistanceNbrWeights(nbrs,i,sigma);
					truth[i].setNeighbors(nbrs,w);					
				}
				nbrtable=null;
			}
			
		}
		
	}

	/**
	 * Attempt to solve the MRF using Iterated Conditional Modes
	 * @param iters Max numer of iterations allowed in the solving the MRF
	 */
	public void mrfICM(){
		System.out.println("Apply MRF, ICM optimization...");
		int it=0;
		int numchanged=1;
		
		while(it<mrfiters && numchanged>0){
			System.out.print("Iter "+it+" - ");
			numchanged = 0;
			for(Truth t : truth){  	//for each vertex
				double[] problist = t.getProbTruth();
				if (problist!=null) {
					// add contribution from neighbors
					int[] n = t.getNeighbors();
					float[] w = t.getNeighborWeights();
					for(int l=0; l<n.length; l++){
						Truth nVoxel=truth[n[l]];
						int hNum=nVoxel.getHardInd();
						int nbrlabind = t.findIndexLoc(hNum);	
						if (nbrlabind!=-1)
							problist[nbrlabind]+=w[l]*nVoxel.getHardTruth();
					}
					
					//check to see if hard seg has changed
					double maxprob = problist[0];
					for(int l=1; l<problist.length; l++){
						if(problist[l]>maxprob){
							t.setTruth(problist);
							numchanged++;
							break;
						}
					}
				}
			}
			System.out.println("Voxels changed: " +numchanged);
			it++;
		}
	}
	
	/**
	 * Attempt to solve the MRF using a Relaxation Labeling technique
	 * @param iters Max numer of iterations allowed in the solving the MRF
	 */
	public void mrfRL(){
		System.out.println("Apply MRF, RL optimization...");
		int it=0;
		int numchanged=1;
		
		while(it<mrfiters && numchanged>0){
			System.out.print("Iter "+it+" - ");
			numchanged=0;
			for(Truth t : truth){  	//for each vertex
				double[] problist = t.getTruth().clone();
				int prevHard=t.getHardInd();
				// add contribution from neighbors
				int[] n = t.getNeighbors();
				float[] w = t.getNeighborWeights();
				for(int l=0; l<n.length; l++){
					Truth nVoxel=truth[n[l]];
					int hNum=nVoxel.getHardInd();
					int nbrlabind = t.findIndexLoc(hNum);	
					if (nbrlabind!=-1)
						problist[nbrlabind]+=w[l]*nVoxel.getHardTruth();
				}
				
				t.setTruth(problist);
				if (t.getHardInd()!=prevHard)
					numchanged++;
			}
			System.out.println("Voxels changed: " +numchanged);
			it++;
		}
	}
	
	/**
	 * Compute the weights for each neighbor if 
	 * @param num Number of neighbors
	 * @return array containing the weight for each neighbor
	 */

	private float[] computeUniformNbrWeights(int num){
		float[] weights = new float[num];
//		float w = (1-beta)/num;
		float w = beta;
		for(int i=0; i<num; i++){ weights[i]=w; }
		return weights;
	}
	
	//NEED TO THINK ABOUT HOW I WANT TO DO THIS
//	/**
//	 * Compute the weights for each neighbor where the weights depend on the distance
//	 * to the vertex of interest.  The closes neighbor will be given weight beta, while
//	 * all other neighbors will be given weight Beta*exp(-D) 
//	 * @param vert the index of the vertex under consideration
//	 * @param nbrs array of indices of neighbors
//	 * @return array containing the weight for each neighbor
//	 */
	private float[] computeDistanceNbrWeights(int[] nbrs, int vert, float sigma){

		if(beta>0){
			float[] weights = new float[nbrs.length];
			
			Point3f thisvert = new Point3f(surf.getVertex(vert));
			Point3f neighbor = new Point3f();
			for(int i=0; i<nbrs.length; i++){
				neighbor = surf.getVertex(nbrs[i]);
				weights[i]=(float)(beta*Math.exp(-thisvert.distanceSquared(neighbor)/sigma));
			}

			float sum=0;
			for(float w : weights)
				sum+=w;
			for(int i=0; i<weights.length; i++)
				weights[i]/=sum;
			return weights;
			
		}else{
			System.err.println("jist.plugins"+"Neighborhood weights only relevent for beta>0");
			return null;
		}
	}
	

	public void getPriorProb(){
		if(raterData!=null){
			float total = raters*inds;
			labels = new ArrayList<Double>();
			priors = new ArrayList<Float>();
			for(int i=0; i<inds; i++){
				for(int j=0; j<raters; j++){				
					if(!labels.contains(raterData[i][j])){
						labels.add(raterData[i][j]);
						priors.add(1f);
					}else{
						int thisone = getIndex(labels,raterData[i][j]);
						priors.set(thisone, priors.get(thisone)+1);
					}
				}


			}
			priors.trimToSize();
			for(int m=0; m<priors.size(); m++){
				priors.set(m, priors.get(m)/total);
			}
			setPriorArray();
		}else{
			System.err.println("jist.plugins"+"Rater data is null");
		}
	}
	
	public void createT(){
		t = new int[inds][raters];
		truth = new Truth[inds];
		
		for(int i=0; i<inds; i++){
			BitSet mindex = new BitSet(labelSize);
			for(int j=0; j<raters; j++){
				t[i][j] = getIndex(labels,raterData[i][j]);
				mindex.set(labels.indexOf(raterData[i][j]),true);
				if (!(t[i][j]>-1))
					System.err.println("jist.plugins"+"Could not find label!");
			}
			truth[i] = new Truth(mindex.cardinality());
			int c=0;
			for(int m=0; m<labelSize; m++){
				if (mindex.get(m)) {
					truth[i].setIndex(c++,m);
				}
			}	
		}
		raterData=null;
	}
	
	private void MStep() {
		double[] totsum = new double[labelSize];
		pl.clearD();
		
		for(int i=0; i<inds; i++){
			int[] mindex=truth[i].getIndex();
			double[] truthArray=truth[i].getTruth();
			for(int m=0; m<mindex.length; m++){
				double tr=truthArray[m];
				totsum[mindex[m]]+=tr;
				for(int r=0; r<raters; r++){
					pl.set(r, t[i][r], mindex[m], pl.getD(r, t[i][r], mindex[m])+tr);
				}
			}
		}
		for(int n=0; n<labelSize; n++){
			pl.divideByTots(n, totsum[n]);
		}
	}
	
	private void EStep() {
		for(int i=0; i<inds; i++){
			int[] mindex=truth[i].getIndex();
			int lSize=mindex.length;
			double[] a = new double[lSize];
			for(int m=0; m<lSize; m++) {
				a[m]=priorArray[mindex[m]];
			}
			
			for(int m=0; m<lSize; m++){
				for(int r=0; r<raters; r++){				
					a[m]*=pl.getD(r, t[i][r], mindex[m]);
				}
			}
			truth[i].setTruth(a);
		}
	}
	
	public void iterate(){
		
		if(init.equals("Performance"))
			EStep();

		int iters = 0;	
		boolean keepgoing = true;
		float ntrace = 0;
		
		while(keepgoing && iters<maxiters){
			MStep();
			EStep();

			if(beta>0){
				if (mrfMethod.equals("RL")){
					mrfRL();
				}else{
					mrfICM();
				}
			}
			
			ntrace = pl.normalizedTrace();
			if(Math.abs(ntrace-normtrace)<eps){
				System.out.println("jist.plugins"+"\t"+"Converged, Total Iterations: " + iters);
				keepgoing=false;
			}
				
			System.out.println("jist.plugins"+"\t"+"Iteration: " +iters);
			System.out.println("jist.plugins"+"\t"+"Prev Trace: " +normtrace);
			System.out.println("jist.plugins"+"\t"+"This Trace: " +ntrace);
			System.out.println("jist.plugins"+"\t"+"Diff: " + Math.abs(ntrace-normtrace));
			System.out.println("jist.plugins"+"\t"+"Need to get below: "+eps);
//			System.out.println("jist.plugins"+"\t"+pl);
			System.out.println("jist.plugins"+"\t"+"*****************");
			
			iters++;
			normtrace=ntrace;
		}		
	}
	
	public void printPerformanceLevels(){
		System.out.println("jist.plugins"+"\t"+pl);
	}
	public String getPerformanceLevels(){
		return pl.toString();
	}
		
	private void setPriorArray(){
		priorArray= new double[priors.size()];
		for(int i=0; i<priors.size(); i++){
			priorArray[i]=priors.get(i).floatValue();
		}
	}
	
	public EmbeddedSurface writeProb(EmbeddedSurface surf, int n) {
		EmbeddedSurface s = new EmbeddedSurface(surf);
		double[][] prob = new double[inds][1];
		for(int i=0; i<inds; i++){
			double[] a=truth[i].getTruth();
			int[] ind=truth[i].getIndex();
			for(int m=0; m<a.length; m++) {
				if(labels.get(n)==ind[m]) {
					prob[i][0]=a[m];
					break;
				}
			}			
		}
		s.setVertexData(prob);
		s.setName("LabelProbability_"+labels.get(n).intValue());
		return s;
	}

	public PerformanceLevel getPeformanceLevel(){
		return pl;
	}
	
	public int getIndex(ArrayList<Double> l, Number n){
		Iterator<Double> it = l.iterator();
		int i =0;
		while(it.hasNext()){
			Number ith = it.next();
			if(ith.doubleValue()==n.doubleValue()){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	
	//method to combine labels for testing
	public double[][] combineRaterLabels(double[][] raterin){
		double[][] raterout = new double[raterin.length][raterin[0].length];

		for(int i=0; i<raterin.length; i++){
			for(int j=0; j<raterin[0].length; j++){
				if(raterin[i][j]>100){
					raterout[i][j]=2;
				}else{
					raterout[i][j]=1;
				}
			}
		}
		
		return raterout;
	}
	
	public double[][] getHardSeg(){
		double[][] seg = new double[inds][1];
		for(int i=0; i<inds; i++){
			seg[i][0]=labels.get(truth[i].getHardInd());
		}
		return seg;
	}


	private class Truth{
		private int[] index;
		private double[] lTruth;
		private int[] nbrs;
		private float[] weights;
		
		public Truth(int n){
			index=new int[n];
			lTruth=new double[n];
			nbrs=null;
			weights=null;
		}

		public int findIndexLoc(int n) {
			for (int i=0; i<index.length; i++) {
				if(index[i]==n)
					return i;
			}
			return -1;
		}

		public int[] getIndex() {
			return index;
		}
		
		public double[] getTruth() {
			return lTruth;
		}
		
		public double[] getProbTruth() {
			if(lTruth.length>1 && lTruth[0]<lTruth[1]+beta)
				return lTruth.clone();
			else
				return null;
		}
		
		public double getHardTruth() {
			return lTruth[0];
		}
		
		public int getHardInd() {
			return index[0];
		}
		
		public void setIndex(int loc, int n) {
			index[loc]=n;
		}
		
		public void setTruth(double t[]) {
			double sum = 0;
			for(int l=0; l<t.length; l++){
				sum+=t[l];
			}
			for(int l=0; l<t.length; l++){
				lTruth[l]=t[l]/sum;
			}
			sort();
		}
		
		public void setNeighbors(int[] nbrs, float[] nbrW) {
			this.nbrs=nbrs;
			this.weights=nbrW;
		}
		
		public int[] getNeighbors() {
			return nbrs;
		}
		
		public float[] getNeighborWeights() {
			return weights;
		}
		
		public void sort(){
			for (int i=0; i<sortSize; i++){
				int max = i;
				for (int j=i+1; j<lTruth.length; j++){
					if (lTruth[j]>lTruth[max])
						max = j;
				}
				if (i!=max){
					double swapT = lTruth[i];					
					lTruth[i] = lTruth[max];					
					lTruth[max] = swapT;
					int swapI = index[i];
					index[i] = index[max];
					index[max] = swapI;
				}
			}
		}
	}
}
