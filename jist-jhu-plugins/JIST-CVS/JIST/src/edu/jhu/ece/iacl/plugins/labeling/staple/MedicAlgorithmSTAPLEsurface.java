package edu.jhu.ece.iacl.plugins.labeling.staple;

import java.io.File;
import java.util.ArrayList;

import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEgeneral;
import edu.jhu.ece.iacl.algorithms.manual_label.staple.STAPLEgeneral2;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.io.StringReaderWriter;
import edu.jhu.ece.iacl.jist.io.SurfaceReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;


public class MedicAlgorithmSTAPLEsurface extends ProcessingAlgorithm{
	private ParamSurfaceCollection ratersurfs;
	ParamObject<String> pl;
	private ParamFloat eps;
	private ParamFloat beta;
	private ParamFloat sigma;
	private ParamInteger maxiters;
	private ParamInteger offset;
	private ParamOption init;
	private ParamBoolean memsFlag;
	private ParamOption mrfMethod;
	
	private ParamFileCollection memsOut;
	private ParamSurface segout;
	private SurfaceReaderWriter rw = SurfaceReaderWriter.getInstance();
	
	private static final String cvsversion = "$Revision: 1.10 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "STAPLE - Simultaneous Truth and Performance Level Estimation applied to surface labels.";
	private static final String longDescription = "Given a number of surface labelings of a particular strucute, STAPLE returns membership functions of the Truth.  This implementation requires that each element of the input collection be a full surface object, and must therefore include all vertex/edge/face data.  For a more light-weight implementation, see STAPLEgeneral.";

	
	protected void createInputParameters(ParamCollection inputParams) {
		inputParams.add(ratersurfs=new ParamSurfaceCollection("Rater Surfaces"));
		String[] initops = {"Performance", "Truth"};
		inputParams.add(init=new ParamOption("Initialization Type", initops));
		inputParams.add(beta = new ParamFloat("Regularization",0.0f));
		inputParams.add(sigma = new ParamFloat("Kernel Size",2.0f));
		inputParams.add(mrfMethod = new ParamOption("MRF Solver", new String[]{"RL","ICM"}));
		inputParams.add(eps = new ParamFloat("Max Delta for Convergence"));
		eps.setValue(new Float(0.00001));
		
		inputParams.add(maxiters=new ParamInteger("Max Iterations"));
		maxiters.setValue(new Integer(30));
		inputParams.add(offset=new ParamInteger("Data Offset",0));
		inputParams.add(memsFlag = new ParamBoolean("Output Label Probability Surfaces?"));
		memsFlag.setValue(false);


		inputParams.setPackage("IACL");
		inputParams.setCategory("Labeling.STAPLE");
		inputParams.setLabel("STAPLE Surface");
		inputParams.setName("STAPLE_Surface");


		AlgorithmInformation info=getAlgorithmInformation();
		info.setWebsite("");
		info.add(new AlgorithmAuthor("John Bogovic", "bogovic@jhu.edu", "http://putter.ece.jhu.edu/John"));
		info.setAffiliation("Johns Hopkins University, Department of Electrical Engineering");
		info.add(new Citation("Warfield SK, Zou KH, Wells WM, \"Simultaneous Truth and Performance Level Estimation (STAPLE): An Algorithm for the Validation of Image Segmentation\" IEEE TMI 2004; 23(7):903-21  "));	
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	protected void createOutputParameters(ParamCollection outputParams) {	
		outputParams.add(segout = new ParamSurface("Labeled Surface"));

		outputParams.add(memsOut = new ParamFileCollection("Label Probability Surfaces",new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions)));
		memsOut.setMandatory(false);

		pl = new ParamObject<String>("PerformanceLevels",new StringReaderWriter());
		pl.setDescription("Returns a description of the performance levels for each rater in matrix form." +
				"The i-jth element represents the probability that label i was selected when label j was the truth.");
		outputParams.add(pl);
	}


	protected void execute(CalculationMonitor monitor) {
		/* A lighter weight implementation */
		
		//copy one of the input surfaces.
		EmbeddedSurface surfout = ratersurfs.getSurfaceList().get(0).clone(); // Don't reuse inputs!
		
		//TODO: change runOld to false to try new algorithm
		boolean runOld=false;
		if (runOld)	{
			STAPLEgeneral staple = new STAPLEgeneral(getSurfaceDataList(),offset.getInt());
			/*
			 * If there is neighbor interaction, inform staple about the 
			 * surface connectivity and weight of interaction
			 */
			if(beta.getFloat()>0){
				staple.setSurface(surfout);
				staple.setBeta(beta.getFloat());
				staple.setSigma(sigma.getFloat());
			}
			staple.setmaxIters(maxiters.getInt());
			staple.setEps(eps.getDouble());
			staple.setInit(init.getValue());
			long t1=System.currentTimeMillis();
			staple.iterate();
			long t2=System.currentTimeMillis();
			System.out.println("TIME: "+(t2-t1));
			System.out.flush();

			surfout.setVertexData(staple.getHardSeg());
			segout.setValue(surfout);
			segout.setFileName("HardSegmentation_"+this.toString());


			if(memsFlag.getValue()){
//				memsOut.setValue(staple.getTruth());
				ArrayList<double[][]> memdata = staple.getTruth();
				ArrayList<File> fList = new ArrayList<File>(memdata.size());
				ArrayList<Number> labellist = staple.getLabels();
				int i =0;
				for(double[][] labmem : memdata){
					EmbeddedSurface s = new EmbeddedSurface(surfout);
					s.setName("LabelProbability_"+labellist.get(i).intValue());
					s.setVertexData(labmem);
					File f=rw.write(s,getOutputDirectory());
					fList.add(f);
					s.disposeAllTables();
					i++;
				}
				memsOut.setValue(fList);
			}

			pl.setObject(staple.getPeformanceLevel().toString());
			pl.setFileName("Performance");
		} 
		else {
			STAPLEgeneral2 staple = new STAPLEgeneral2(getSurfaceDataList2());
			/*
			 * If there is neighbor interaction, inform staple about the 
			 * surface connectivity and weight of interaction
			 */
			if(beta.getFloat()>0){
				staple.setSurface(surfout);
				staple.setBeta(beta.getFloat());
				staple.setSigma(sigma.getFloat());
			}
			
			staple.setmaxIters(maxiters.getInt());
			staple.setEps(eps.getDouble());
			staple.setInit(init.getValue());	
			staple.setMRFMethod(mrfMethod.getValue());
			staple.initialize();
			long t1=System.currentTimeMillis();
			staple.iterate();
			long t2=System.currentTimeMillis();
			System.out.println("TIME: "+(t2-t1));
			System.out.flush();

			if(memsFlag.getValue()){
				ArrayList<File> fList = new ArrayList<File>(staple.getNumLabels());
				for (int i=0; i<staple.getNumLabels(); i++) {
					EmbeddedSurface s = staple.writeProb(surfout, i);
					File f=rw.write(s,getOutputDirectory());
					fList.add(f);
					s.disposeAllTables();
				}
				memsOut.setValue(fList);
			}
			
			surfout.setVertexData(staple.getHardSeg());
			surfout.setName("HardSegmentation_"+surfout.getName());
			segout.setValue(surfout);

			pl.setObject(staple.getPeformanceLevel().toString());
			pl.setFileName("Performance");
		}
		
		System.out.println(getClass().getCanonicalName()+"\t"+"FINISHED");
	}


	private ArrayList<double[][]> getSurfaceDataList(){
		System.out.println(getClass().getCanonicalName()+"\t"+ratersurfs);
		ArrayList<double[][]> datlist = new ArrayList<double[][]>();
		for(EmbeddedSurface surf : ratersurfs.getSurfaceList()){
			System.out.println(getClass().getCanonicalName()+"\t"+"SURFACE:"+surf.getName());
//			System.out.println(getClass().getCanonicalName()+"\t"+"SURFACE:"+surf.getVertexCount());
			datlist.add(surf.getVertexData());
		}
		return datlist;
	}
	
	private double[][] getSurfaceDataList2(){
		System.out.println(getClass().getCanonicalName()+"\t"+ratersurfs);
		double[][] datlist=new double[ratersurfs.getSurfaceList().get(0).getVertexCount()][ratersurfs.size()];
		int rater=0;
		for(EmbeddedSurface surf : ratersurfs.getSurfaceList()){
			System.out.println(getClass().getCanonicalName()+"\t"+"SURFACE:"+surf.getName());
//			System.out.println(getClass().getCanonicalName()+"\t"+"SURFACE:"+surf.getVertexCount());
			for(int i=0; i<datlist.length; i++) {
				datlist[i][rater]=surf.getVertexDataAtOffset(i, offset.getInt());
			}
			rater++;
			surf.disposeAllTables();
		}
		return datlist;
	}
}
