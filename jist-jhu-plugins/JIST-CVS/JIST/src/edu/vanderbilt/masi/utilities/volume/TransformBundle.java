package edu.vanderbilt.masi.utilities.volume;

import java.io.File;

import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;

public class TransformBundle {
	public TransformBundle() {;};
	public TransMatrix xfm;
	float []res;
	int[]ext;
	int []newOr;
	private boolean[] flipNewOr;

	static public boolean[] directionToFlip(int []dir) {
		boolean []b = new boolean[dir.length];
		for(int i=0;i<dir.length;i++)
			b[i]=dir[i]<0;
		return b;
	}


	static public TransformBundle getMatchOrientationTransform(ModelImage srcImage, ModelImage tarImage) {
		ModelImage image =  srcImage;
		//	ImageHeader hdr = source.getImageData().getHeader();
		//	ImageHeader.AxisOrientation[] axor = hdr.getAxisOrientation();
		//	ImageHeader.ImageOrientation imgor = hdr.getImageOrientation();

		ModelImage templ = tarImage;


		int i, j;
		boolean found;
		int newOrient;
		float ri[] = new float[3];
		int   ni[] = new int[3];
		float r0[] = new float[3];
		int   n0[] = new int[3];

		int outDir[] = new int[3];
		int inDir[] = new int[3];

		//			FileInfoBase fileInfo = (FileInfoBase)(image.getFileInfo()[0].clone());

		// set resampled resolutions, dimensions
		ri[0] = image.getFileInfo()[0].getResolutions()[0];
		ri[1] = image.getFileInfo()[0].getResolutions()[1];
		ri[2] = image.getFileInfo()[0].getResolutions()[2];

		ni[0] = image.getExtents()[0];
		ni[1] = image.getExtents()[1];
		ni[2] = image.getExtents()[2];

		float r[] = new float[3];
		int   n[] = new int[3];
		for (i = 0; i <= 2; i++) {
			r[i] = ri[i];
			n[i] = ni[i];
		}

		int or[] = new int[3];
		or[0] = image.getFileInfo()[0].getAxisOrientation()[0];
		or[1] = image.getFileInfo()[0].getAxisOrientation()[1];
		or[2] = image.getFileInfo()[0].getAxisOrientation()[2];
		inDir[0] = image.getFileInfo()[0].getAxisDirection()[0];
		inDir[1] = image.getFileInfo()[0].getAxisDirection()[1];
		inDir[2] = image.getFileInfo()[0].getAxisDirection()[2];

		int newOr[] = new int[3];
		int newOrientIndex = templ.getImageOrientation();
		if (newOrientIndex == 0) {  // DICOM AXIAL
			newOr[0] = FileInfoBase.ORI_R2L_TYPE; 
			newOr[1] = FileInfoBase.ORI_A2P_TYPE;
			newOr[2] = FileInfoBase.ORI_I2S_TYPE;		
		}
		else if (newOrientIndex == 1) {// DICOM CORONAL
			newOr[0] = FileInfoBase.ORI_R2L_TYPE;;
			newOr[1] = FileInfoBase.ORI_S2I_TYPE;
			newOr[2] = FileInfoBase.ORI_A2P_TYPE;

		}
		else if (newOrientIndex == 2) { // DICOM SAGITTAL
			newOr[0] = FileInfoBase.ORI_A2P_TYPE;
			newOr[1] = FileInfoBase.ORI_S2I_TYPE;
			newOr[2] = FileInfoBase.ORI_L2R_TYPE;

		}
		else { // USER DEFINED
			newOr[0] = templ.getFileInfo()[0].getAxisOrientation(0); 			
			newOr[1] = templ.getFileInfo()[0].getAxisOrientation(1);
			newOr[2] = templ.getFileInfo()[0].getAxisOrientation(2);		

		}
		outDir[0] = templ.getFileInfo()[0].getAxisDirection()[0];
		outDir[1] = templ.getFileInfo()[0].getAxisDirection()[1];
		outDir[2] = templ.getFileInfo()[0].getAxisDirection()[2];
		boolean isChanged = false;
		for( i=0;i<3;i++) {
			if(newOr[i]!=image.getFileInfo(0).getAxisOrientation(i))
				isChanged=true;
			if(outDir[i]!=inDir[i])
				isChanged=true;
		}
		if(!isChanged) 
			return null;


		double X[][] = new double[4][4];
		for (j = 0; j <= 2; j++) {
			switch (or[j]) {
			case FileInfoBase.ORI_R2L_TYPE:
				found = false;
				for (i = 0; (i <= 2) && (!found); i++) {
					if (newOr[i] == FileInfoBase.ORI_R2L_TYPE) {

						found = true;
						X[i][j] = 1.0;
						r0[i] = r[j];
						n0[i] = n[j];
					}
					else if (newOr[i] == FileInfoBase.ORI_L2R_TYPE) {
						found = true;
						X[i][j] = -1.0;
						X[i][3] = ri[j]*(ni[j] - 1);
						r0[i] = r[j];
						n0[i] = n[j];
					}
				}
				break;
			case FileInfoBase.ORI_L2R_TYPE:
				found = false;
				for (i = 0; (i <= 2) && (!found); i++) {
					if (newOr[i] == FileInfoBase.ORI_L2R_TYPE) {
						found = true;
						X[i][j] = 1.0;
						r0[i] = r[j];
						n0[i] = n[j];
					}
					else if (newOr[i] == FileInfoBase.ORI_R2L_TYPE) {
						found = true;
						X[i][j] = -1.0;
						X[i][3] = ri[j]*(ni[j] - 1);
						r0[i] = r[j];
						n0[i] = n[j];
					}
				}
				break;
			case FileInfoBase.ORI_A2P_TYPE:
				found = false;
				for (i = 0; (i <= 2) && (!found); i++) {
					if (newOr[i] == FileInfoBase.ORI_A2P_TYPE) {
						found = true;
						X[i][j] = 1.0;
						r0[i] = r[j];
						n0[i] = n[j];
					}
					else if (newOr[i] == FileInfoBase.ORI_P2A_TYPE) {
						found = true;
						X[i][j] = -1.0;
						X[i][3] = ri[j]*(ni[j] - 1);
						r0[i] = r[j];
						n0[i] = n[j];
					}
				}
				break;
			case FileInfoBase.ORI_P2A_TYPE:
				found = false;
				for (i = 0; (i <= 2) && (!found); i++) {
					if (newOr[i] == FileInfoBase.ORI_P2A_TYPE) {
						found = true;
						X[i][j] = 1.0;
						r0[i] = r[j];
						n0[i] = n[j];
					}
					else if (newOr[i] == FileInfoBase.ORI_A2P_TYPE) {
						found = true;
						X[i][j] = -1.0;
						X[i][3] = ri[j]*(ni[j] - 1);
						r0[i] = r[j];
						n0[i] = n[j];
					}
				}
				break;
			case FileInfoBase.ORI_I2S_TYPE:
				found = false;
				for (i = 0; (i <= 2) && (!found); i++) {
					if (newOr[i] == FileInfoBase.ORI_I2S_TYPE) {
						found = true;
						X[i][j] = 1.0;
						r0[i] = r[j];
						n0[i] = n[j];
					}
					else if (newOr[i] == FileInfoBase.ORI_S2I_TYPE) {
						found = true;
						X[i][j] = -1.0;
						X[i][3] = ri[j]*(ni[j] - 1);
						r0[i] = r[j];
						n0[i] = n[j];
					}
				}
				break;
			case FileInfoBase.ORI_S2I_TYPE:
				found = false;
				for (i = 0; (i <= 2) && (!found); i++) {
					if (newOr[i] == FileInfoBase.ORI_S2I_TYPE) {
						found = true;
						X[i][j] = 1.0;
						r0[i] = r[j];
						n0[i] = n[j];
					}
					else if (newOr[i] == FileInfoBase.ORI_I2S_TYPE) {
						found = true;
						X[i][j] = -1.0;
						X[i][3] = ri[j]*(ni[j] - 1);
						r0[i] = r[j];
						n0[i] = n[j];
					}
				}
				break;
			}
		} // for (j = 0; j <= 2; j++)


		if ((newOr[2] == FileInfoBase.ORI_I2S_TYPE) || (newOr[2] == FileInfoBase.ORI_S2I_TYPE)) {
			newOrient = FileInfoBase.AXIAL;
		}
		else if ((newOr[2] == FileInfoBase.ORI_A2P_TYPE) || (newOr[2] == FileInfoBase.ORI_P2A_TYPE)) {
			newOrient = FileInfoBase.CORONAL;
		}
		else if ((newOr[2] == FileInfoBase.ORI_L2R_TYPE) || (newOr[2] == FileInfoBase.ORI_R2L_TYPE)) {
			newOrient = FileInfoBase.SAGITTAL;
		}
		else {
			newOrient = FileInfoBase.UNKNOWN_ORIENT;
		} 

		TransMatrix transform = new TransMatrix(4);
		transform.setMatrix(0, 2, 0, 3, X);

		System.out.println(transform.toString());

		TransformBundle result =  new TransformBundle();
		result.xfm = transform;
		result.res = r0;
		result.ext = n0;
		result.newOr = newOr;



		result.flipNewOr = new boolean[newOr.length];
		for(i=0;i<inDir.length;i++) {
			result.flipNewOr[i] = inDir[i]!=outDir[i]; 
		}
		return result;

		//		AlgorithmTransform algoTrans = new AlgorithmTransform(image, transform, AlgorithmTransform.NEAREST_NEIGHBOR, r0[0], r0[1], r0[2], n0[0], n0[1], n0[2], 
		//					true, false, false);
		//			algoTrans.setUpdateOriginFlag(true);
		//			algoTrans.run();
		//			ModelImage trans = algoTrans.getTransformedImage();
		//			
		//			ImageDataMipavWrapper img = new ImageDataMipavWrapper();
		//			hdr.setAxisOrientation(axor);
		//			img.setHeader(hdr);

		//			result.setValue(img);\
	}

	public static ModelImage resample(ModelImage src,
			TransformBundle bundle) {
		AlgorithmTransform algoTrans = new AlgorithmTransform(src, bundle.xfm, AlgorithmTransform.TRILINEAR, 
				bundle.res[0], bundle.res[1], bundle.res[2], bundle.ext[0], bundle.ext[1], bundle.ext[2], 
				true, false, false);
		algoTrans.setUpdateOriginFlag(true);
		algoTrans.run();


		ModelImage trans = algoTrans.getTransformedImage();
		for(FileInfoBase f : trans.getFileInfo()) {
			f.setAxisOrientation(bundle.newOr.clone());
		}
		//		ModelImageReaderWriter.getInstance().write(trans,new File("c:\\temp\\ExportTest.xml")); 
		int []newOrder = new int[bundle.newOr.length];
		int []curOri = src.getAxisOrientation();
		int []newOr = bundle.newOr;
		for(int i=0;i<newOr.length;i++) {
			for(int j=0;j<curOri.length;j++) {
				if(isSameAxis(newOr[i],curOri[j])) 
					newOrder[i]=j;
			}
		}



//		ModelImage out =trans.export(newOrder, bundle.flipNewOr);
//		ModelImageReaderWriter.getInstance().write(trans,new File("c:\\temp\\trans.xml"));
//		ModelImageReaderWriter.getInstance().write(out,new File("c:\\temp\\out.xml"));
//		trans.disposeLocal();
		return trans;
	}

	private static boolean isSameAxis(int i, int j) {
		if(
				((i == FileInfoBase.ORI_I2S_TYPE) || (i == FileInfoBase.ORI_S2I_TYPE))
				&& 
				((j == FileInfoBase.ORI_I2S_TYPE) || (j == FileInfoBase.ORI_S2I_TYPE))
		)
			return true;
		if(
				((i == FileInfoBase.ORI_A2P_TYPE) || (i == FileInfoBase.ORI_P2A_TYPE))
				&& 
				((j == FileInfoBase.ORI_A2P_TYPE) || (j == FileInfoBase.ORI_P2A_TYPE))
		)
			return true;
		if(
				((i == FileInfoBase.ORI_L2R_TYPE) || (i == FileInfoBase.ORI_R2L_TYPE))
				&& 
				((j == FileInfoBase.ORI_L2R_TYPE) || (j == FileInfoBase.ORI_R2L_TYPE))
		)
			return true;
		if(
				((i == FileInfoBase.UNKNOWN_ORIENT) || (i == FileInfoBase.UNKNOWN_ORIENT))
				&& 
				((j == FileInfoBase.UNKNOWN_ORIENT) || (j == FileInfoBase.UNKNOWN_ORIENT))
		)
			return true;


		return false;
	}
}