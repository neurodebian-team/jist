/*
 *
 */
package edu.vanderbilt.masi.plugins.labeling;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import tractography.OutsideLUTRangeException;

import Jama.Matrix;
import WildMagic.LibFoundation.Mathematics.Vector3f;
import edu.jhu.bme.smile.commons.math.StatisticsDouble;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.ImageDataReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.vanderbilt.masi.plugins.registration.PluginAffine4D;
import edu.vanderbilt.masi.plugins.registration.PluginTransformVolume;
import edu.vanderbilt.masi.utilities.volume.TransformBundle;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;


/*
 * @author Bennett Landman (bennett.landman@vanderbilt.edu)
 *
 */
public class PluginVolumeLabelStats extends ProcessingAlgorithm {
	ParamVolume labels;
	ParamVolumeCollection vol;
	ParamFile output;
	File identityTransform;
	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.2 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Determines various statistics for each label in a labelset for each volume in the volume collection." +
	"If necessary, interpolates the target image to match label image dimensions.";
	private static final String longDescription = "";

	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {

		inputParams.add(labels = new ParamVolume("Labels",null,-1,-1,-1,1));
		labels.setLoadAndSaveOnValidate(false);
		inputParams.add(vol = new ParamVolumeCollection("Volumes"));
		vol.setLoadAndSaveOnValidate(false);

		inputParams.setPackage("MASI");
		inputParams.setCategory("Labeling");
		inputParams.setLabel("Volume Label Stats");
		inputParams.setName("Volume_Label_Stats");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("https://masi.vuse.vanderbilt.edu/");
		info.add(new AlgorithmAuthor("MASI","Bennett Landman",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(output = new ParamFile("Target Sumamry File"));		
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		File outfile = new File(this.getOutputDirectory() + File.separator + "LabelStats.txt");
		identityTransform = new File(this.getOutputDirectory() + File.separator + "identity.mtx");
		try {
//			OutputStream outputS = (new FileOutputStream(outfile));
			ExecuteWrapper wrapper=new ExecuteWrapper();
			monitor.observe(wrapper);
			wrapper.execute(this,new PrintStream(outfile));
			output.setValue(outfile);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected class ExecuteWrapper extends AbstractCalculation{
		public void execute(ProcessingAlgorithm alg, PrintStream outStream) throws FileNotFoundException{

			
			ImageData labelVol = labels.getImageData();
			HashMap<Integer,Integer> uniqueLabels = getUniqueLabelColors(labelVol);
			
			// Report Label Summary
			for(Integer l : uniqueLabels.keySet()) {
				Integer count = uniqueLabels.get(l);
				String str = "Label"+"\t"+l+"\t"+count;
				JistLogger.logOutput(JistLogger.INFO,str);				
				outStream.println(str);	
			}

			JistLogger.logOutput(JistLogger.INFO, "Found "+uniqueLabels.size()+" unique labels.");
			int volCount = 0;
						
			for(ParamVolume pv : vol.getParamVolumeList()) { 
				ImageData dat = pv.getImageData();											

				Set<Integer>labelKeys = uniqueLabels.keySet();

				for(int m=0;m<dat.getComponents();m++) {
						
					//  Need to process ONE COMPONENT AT A TIME FOR RESAMPLING
					ImageData datTarget;
					File tempFile = null;
					boolean resampled = false;
					if(!dat.getHeader().hasComparableGeometry(labelVol.getHeader()) ||
							!dat.hasComparableExtents(labelVol) ) {
						JistLogger.logOutput(JistLogger.INFO, "Resampling Volume Component: "+m);
						PluginTransformVolume tran = new PluginTransformVolume();
						tran.setOutputDirectory(alg.getOutputDirectory());
						tran.getInput().getFirstChildByName("Volume to Match - Dimensions & Resolution").setValue(labels.getValue());
						if(dat.getComponents()<2 && m==0)
							((ParamVolumeCollection)tran.getInput().getFirstChildByName("Volumes")).add(dat);
						else {
							if(!(dat instanceof ImageDataMipav))
								dat = new ImageDataMipav(dat);
							datTarget = new ImageDataMipavWrapper(PluginAffine4D.extract3DVolume((ImageDataMipav)dat, m));							
							((ParamVolumeCollection)tran.getInput().getFirstChildByName("Volumes")).add(datTarget);
						}
						if(!identityTransform .exists()) {
							PrintWriter pi = new PrintWriter(identityTransform);
							pi.println("4");
							pi.println("4");
							pi.println("1 0 0 0");
							pi.println("0 1 0 0");
							pi.println("0 0 1 0");
							pi.println("0 0 0 1");
							pi.close();
						}
						((ParamFileCollection)tran.getInput().getFirstChildByName("Transformation Matrixes")).add(identityTransform);
						tran.run();
						datTarget=((ParamVolumeCollection)tran.getOutput().getFirstChildByName("Transformed Volumes")).getParamVolume(0).getImageData();
						tempFile = ((ParamVolumeCollection)tran.getOutput().getFirstChildByName("Transformed Volumes")).getParamVolume(0).getValue();
						JistLogger.logOutput(JistLogger.INFO, "Finished Resampling Volume Component: "+m);
						tran=null;
						resampled = true;
					} else {
						datTarget = dat;
					}
					

					String ss = "VolName\tComponent";
					String rr = dat.getName()+"\t"+m;				
					for(Integer l : labelKeys) {
						Integer count = uniqueLabels.get(l);			
						JistLogger.logOutput(JistLogger.INFO, "Label "+l+" with "+count+" voxels");
						double []vals = new double[count];
						int vx=0;
						for(int i=0;i<labelVol.getRows();i++)
							for(int j=0;j<labelVol.getCols();j++)
								for(int k=0;k<labelVol.getSlices();k++)
									if(labelVol.getInt(i, j, k)==l) {
										vals[vx]=datTarget.getDouble(i, j, k, resampled?0:m);
										vx++;
									}


						double max = StatisticsDouble.max(vals);
						double min = StatisticsDouble.min(vals);
						double mean = StatisticsDouble.mean(vals);
						double median = StatisticsDouble.median(vals);
						double std = StatisticsDouble.std(vals);

						String caption = l+"-min"+"\t"+l+"-max"+"\t"+l+"-mean"+"\t"+l+"-median"+"\t"+l+"-std";
						String report = min+"\t"+max+"\t"+mean+"\t"+median+"\t"+std;
						ss = ss+"\t"+caption;
						rr = rr+"\t"+report;

					}
					JistLogger.logOutput(JistLogger.INFO,ss);
					JistLogger.logOutput(JistLogger.INFO,rr);
					if(volCount==0) {
						outStream.println(ss);
						volCount++;
					}
					outStream.println(rr);	

					datTarget=null;
					if(tempFile!=null) {
						ImageDataReaderWriter.deleteImageFile(tempFile);
						tempFile = null;
					}
				}
				dat.dispose();
				pv.dispose();
			}


			//			List<ParamVolume> imglist = vol.getParamVolumeList();
			//			//ArrayList<ImageData> resultlist = new ArrayList<ImageData>();
			//			int i =0;
			//
			//			ArrayList<double[][]> xfms = readMultiMatrices(transformations.getValue());
			//			System.out.println(getClass().getCanonicalName()+"\t"+transformations.getValue());
			//			System.out.println(getClass().getCanonicalName()+"\t"+transformations.getValue().size());
			//			ModelImage img = null;
			//			Matrix m;
			//			for(ParamVolume pv : imglist){
			//				ImageData v = pv.getImageData();
			//
			//				if(v instanceof ImageDataMipav) { 
			//					img = ((ImageDataMipav)v).extractModelImage();
			//				} else {
			//					img = v.getModelImageCopy();
			//				};
			//				v.dispose();
			//				pv.dispose();
			//
			//				m = new Matrix(4,4);
			//				if(transformations.getValue().size()>0){
			//					double[][] matarray = xfms.get(i);
			//					if(matarray.length!=4 || matarray[0].length!=4){
			//						System.err.println(getClass().getCanonicalName()+"Invalid transformation - must be 4x4");
			//					}else{
			//						for(int k=0; k<matarray.length; k++){
			//							for(int j=0; j<matarray[0].length; j++){
			//								m.set(k, j, matarray[k][j]);
			//							}
			//						}
			//					}
			//				}
			//				result.add(new ImageDataMipavWrapper(transform(img,m)));
			//				result.writeAndFreeNow(alg);
			//				img.disposeLocal();
			//				i++;
			//
			//			}		
			//			imglist = null; vol.dispose();vol=null;

		}

		private HashMap<Integer, Integer> getUniqueLabelColors(ImageData labelVol) {
			HashMap<Integer, Integer> foo = new HashMap<Integer, Integer>();
			for(int i=0;i<labelVol.getRows();i++)
				for(int j=0;j<labelVol.getCols();j++)
					for(int k=0;k<labelVol.getSlices();k++)
						for(int l=0;l<labelVol.getComponents();l++) {
							int v = labelVol.getInt(i, j, k, l);
							if(v==0)
								continue;
							if(!foo.containsKey(v)) {
								foo.put(v,1);
							} else {
								Integer vv = foo.get(v);
								foo.put(v,1+vv);								
							}
						}
			return foo; 
		}


	}
}
