/*
 *
 */
package edu.vanderbilt.masi.plugins.registration;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import Jama.Matrix;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleListTxtReaderWriter;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.io.MipavController;
import edu.jhu.ece.iacl.jist.io.ModelImageReaderWriter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.jhu.ece.iacl.jist.structures.image.ImageHeader;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.vanderbilt.masi.utilities.volume.TransformBundle;
import gov.nih.mipav.model.algorithms.AlgorithmCostFunctions;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.algorithms.registration.AlgorithmRegOAR3D;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;
import gov.nih.mipav.view.dialogs.JDialogBase;


/*
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class PluginAffine4D extends ProcessingAlgorithm {
	ParamOption dof;
	ParamOption inputInterpolation;
	ParamOption outputInterpolation;
	ParamOption costFunction;
	ParamBoolean useMaxOfMinRes;
	ParamOption rotDim;
	ParamDouble minAngle;
	ParamDouble maxAngle;
	ParamDouble coarseAngleIncrement;
	ParamDouble fineAngleIncrement;
	ParamInteger bracketMinimum;
	ParamInteger iters;
	ParamInteger level8to4;
	ParamBoolean subsample;
	ParamBoolean skipMultilevelSearch;
	//	ParamVolume source, target, registered,refWeight,inputWeight;
	ParamVolume target,refWeight,inputWeight;
	ParamVolumeCollection sources, allregistered;
	ParamMatrix trans;
	//	ParamObject<String> mipavMatrix;
	ParamObject<ArrayList<double[][]>> transformations;
	ParamFileCollection mtxxfms;


	ParamInteger multiTarget;
	ParamBoolean useMultiStage;
	ParamBoolean useSingleTransform;
	ParamBoolean overrideResolution;
	ParamDouble resX, resY, resZ;
	// Controls the threaded nature of the OAR Registration.
	private ParamBoolean OARThreadedBool;

	File dir;

	private static final String cvsversion = "$Revision: 1.8 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Linear Registration algorithm based on FLIRT (registers a collection of images to the target). ";
	private static final String longDescription = "Finds a linear transformation bringing the source volume into the space of the target volume. Degrees of freedom can be specified.";


	protected void createInputParameters(ParamCollection inputParams) {

		ParamCollection mainParams=new ParamCollection("Main");
		//		mainParams.add(source = new ParamVolume("Source volume"));
		mainParams.add(sources = new ParamVolumeCollection("Source Volumes"));
		mainParams.add(target = new ParamVolume("Target volume"));
		mainParams.add(refWeight = new ParamVolume("Reference Weighted volume"));
		refWeight.setMandatory(false);
		mainParams.add(inputWeight = new ParamVolume("Input Weighted volume"));
		inputWeight.setMandatory(false);
		mainParams.add(dof = new ParamOption("Degrees of freedom", new String[] { "Rigid - 6", "Global rescale - 7",
				"Specific rescale - 9", "Affine - 12" }));
		dof.setValue(3);
		mainParams.add(costFunction = new ParamOption("Cost function", new String[] { "Correlation ratio",
				"Least squares", "Normalized cross correlation", "Normalized mutual information" }));
		mainParams.add(inputInterpolation = new ParamOption("Registration interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc"}));
		mainParams.add(outputInterpolation = new ParamOption("Output interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc","Nearest Neighbor" }));
		mainParams.add(rotDim = new ParamOption("Apply rotation", new String[] { "All", "X", "Y", "Z" }));
		mainParams.add(minAngle = new ParamDouble("Minimum angle", -360, 360, -30));
		mainParams.add(maxAngle = new ParamDouble("Maximum angle", -360, 360, 30));
		mainParams.add(coarseAngleIncrement = new ParamDouble("Coarse angle increment", 0, 360, 15));
		mainParams.add(fineAngleIncrement = new ParamDouble("Fine angle increment", 0, 360, 6));

		ParamCollection advParams=new ParamCollection("Advanced");

		advParams.add(bracketMinimum = new ParamInteger("Multiple of tolerance to bracket the minimum", 10));
		advParams.add(iters = new ParamInteger("Number of iterations", 2));
		advParams.add(level8to4 = new ParamInteger("Number of minima from Level 8 to test at Level 4", 3));
		advParams.add(useMaxOfMinRes = new ParamBoolean(
				"Use the max of the min resolutions of the two datasets when resampling", true));
		advParams.add(subsample = new ParamBoolean("Subsample image for speed", true));
		advParams.add(skipMultilevelSearch = new ParamBoolean(
		"Skip multilevel search (Assume images are close to alignment)"));
		advParams.setLabel("Advanced");

		OARThreadedBool = new ParamBoolean("Multithreading for Registration", false);
		OARThreadedBool.setDescription("Set to false by default, this parameter controls the multithreaded behavior of the linear registration.");
		advParams.add(OARThreadedBool);

		ParamCollection multiParams=new ParamCollection("Multi-Stage");
		multiParams.add(useMultiStage =  new ParamBoolean("Register 4D volumes to internal target?", false));
		multiParams.add(multiTarget = new ParamInteger("Image target within 4D volumes (0 indexed)",0));
		multiParams.add(useSingleTransform =  new ParamBoolean("Use the same transform for all volumes?", false));
		multiParams.add(overrideResolution =  new ParamBoolean("Override target sampling resolution?", false));
		multiParams.add(resX =  new ParamDouble("Override resX?", 1.0));
		multiParams.add(resY =  new ParamDouble("Override resY?", 1.0));
		multiParams.add(resZ =  new ParamDouble("Override resZ?", 1.0));

		setPreferredSize(new Dimension(500,500));
		inputParams.add(mainParams);
		inputParams.add(advParams);
		inputParams.add(multiParams);


		inputParams.setPackage("MASI");
		inputParams.setCategory("Registration.Volume");
		inputParams.setLabel("Affine 4D to 3D");
		inputParams.setName("Affine 4Dto3D");


		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://masi.vuse.vanderbilt.edu/");
		info.add(new AlgorithmAuthor("Bennett Landman", "bennett.landman@vanderbilt.edu", "http://sites.google.com/site/bennettlandman/"));
		info.add(new AlgorithmAuthor("Neva Cherniavsky", " ", "http://mipav.cit.nih.gov/"));
		info.add(new AlgorithmAuthor("Matthew McAuliffe", " ", "http://mipav.cit.nih.gov/"));
		info.add(new Citation("Jenkinson, M. and Smith, S. \"A global optimisation method for robust affine registration of brain images.\" Medical Image Analysis, 5(2):143-156, 2001."));
		// irrelevant? 		info.add(new Citation("Bjórn Hamre \"Three-dimensional image registration of magnetic resonance (MRI) head volumes\" Section for Medical Image Analysis and Informatics Department of Physiology & Department of Informatics University of Bergen, Norway."));
		info.add(new Citation("FLIRT, at http://www.fmrib.ox.ac.uk/fsl/flirt/ "));
		info.add(new Citation("Powell method to find the global minimum: http://math.fullerton.edu/ mathews/n2003/PowellMethodMod.html "));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.RC);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		FileExtensionFilter xfmfilter = new FileExtensionFilter(new String[]{"mtx","xfm"});
		FileExtensionFilter volfilter = new FileExtensionFilter(ModelImageReaderWriter.supportedFileExtensions);
		outputParams.add(trans = new ParamMatrix("Transformation Matrix", 4, 4));
		trans.setMandatory(false);
		outputParams.add(transformations = new ParamObject<ArrayList<double[][]>>("Transformation Matrices", new ArrayDoubleListTxtReaderWriter()));
		transformations.setMandatory(false);
		outputParams.add(mtxxfms = new ParamFileCollection("Transformations",xfmfilter));
		//		outputParams.add(registered = new ParamVolume("Registered Volume"));
		outputParams.add(allregistered = new ParamVolumeCollection("Registered Volumes"));
		allregistered.setLoadAndSaveOnValidate(false);
		//		outputParams.add(mipavMatrix = new ParamObject<String>("Mipav Matrix", new StringReaderWriter()));
		//		mipavMatrix.setExtensionFilter(filter);
		//		mipavMatrix.setName("mipavMatrix");
	}


	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		dir = new File(this.getOutputDirectory()+File.separator+edu.jhu.ece.iacl.jist.utility.FileUtil.forceSafeFilename(this.getAlgorithmName()));
		try{
			if(!dir.isDirectory()){
				(new File(dir.getCanonicalPath())).mkdir();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		FlirtWrapper flirt=new FlirtWrapper();
		monitor.observe(flirt);
		flirt.execute(this);
	}


	protected class FlirtWrapper extends AbstractCalculation{
		public FlirtWrapper(){
		}


		public void execute(ProcessingAlgorithm parent){
			List<ImageData> sourcelist = sources.getImageDataList();
			ModelImage refImage = target.getImageData().getModelImageCopy();
			
			ImageHeader hdr = target.getImageData().getHeader();
			target.dispose();sources.dispose();
			ModelImage refWeightImage = null;
			if(refWeight.getImageData()!=null) {
				refWeightImage = refWeight.getImageData().getModelImageCopy();						
			}
			ModelImage inputWeightImage = null;
			if(inputWeight.getImageData()!=null)
				inputWeightImage=inputWeight.getImageData().getModelImageCopy();
			boolean maxOfMinResol = useMaxOfMinRes.getValue();
			int cost = 0;
			switch (costFunction.getIndex()) {
			case 0:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
				// case 0: cost = AlgorithmCostFunctions.CORRELATION_RATIO; break;
			case 1:
				cost = AlgorithmCostFunctions.LEAST_SQUARES_SMOOTHED;
				// cost = AlgorithmCostFunctions.LEAST_SQUARES;
				// costName = "LEAST_SQUARES_SMOOTHED";
				break;
				// case 2: cost = AlgorithmCostFunctions.MUTUAL_INFORMATION_SMOOTHED;
				// break;
			case 2:
				cost = AlgorithmCostFunctions.NORMALIZED_XCORRELATION_SMOOTHED;
				break;
				// case 3: cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION;
				// break;
			case 3:
				cost = AlgorithmCostFunctions.NORMALIZED_MUTUAL_INFORMATION_SMOOTHED;
				break;
			default:
				cost = AlgorithmCostFunctions.CORRELATION_RATIO_SMOOTHED;
				break;
			}
			int DOF = 0;
			switch (dof.getIndex()) {
			case 0:
				DOF = 6;
				break;
			case 1:
				DOF = 7;
				break;
			case 2:
				DOF = 9;
				break;
			case 3:
				DOF = 12;
				break;
			default:
				DOF = 12;
				break;
			}
			int interp = 0;
			switch (inputInterpolation.getIndex()) {
			case 0:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp = AlgorithmTransform.WSINC;
				break;
			default:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			}
			int interp2 = 0;
			switch (outputInterpolation.getIndex()) {
			case 0:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp2 = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp2 = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp2 = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp2 = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp2 = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp2 = AlgorithmTransform.WSINC;
				break;
			case 7:
				interp2 = AlgorithmTransform.NEAREST_NEIGHBOR;
				break;
			default:
				interp2 = AlgorithmTransform.TRILINEAR;
				break;
			}
			boolean fastMode = skipMultilevelSearch.getValue();
			float rotateBeginX = minAngle.getFloat();
			float rotateEndX = maxAngle.getFloat();
			float coarseRateX = coarseAngleIncrement.getFloat();
			float fineRateX = fineAngleIncrement.getFloat();
			boolean doSubsample = subsample.getValue();
			float rotateBeginY = 0;
			float rotateEndY = 0;
			float coarseRateY = 0;
			float fineRateY = 0;
			float rotateBeginZ = 0;
			float rotateEndZ = 0;
			float coarseRateZ = 0;
			float fineRateZ = 0;

			switch (rotDim.getIndex()) {
			case 0:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = rotateBeginX;
				rotateEndY = rotateEndX;
				rotateEndZ = rotateEndX;
				coarseRateY = coarseRateX;
				coarseRateZ = coarseRateX;
				fineRateY = fineRateX;
				fineRateZ = fineRateX;
				break;
			case 1:
				rotateBeginY = 0;
				rotateBeginZ = 0;
				rotateEndY = 0;
				rotateEndZ = 0;
				coarseRateY = 0;
				coarseRateZ = 0;
				fineRateY = 0;
				fineRateZ = 0;
				break;
			case 2:
				rotateBeginY = rotateBeginX;
				rotateBeginZ = 0;
				rotateEndY = rotateEndX;
				rotateEndZ = 0;
				coarseRateY = coarseRateX;
				coarseRateZ = 0;
				fineRateY = fineRateX;
				fineRateZ = 0;
				rotateBeginX = 0;
				rotateEndX = 0;
				coarseRateX = 0;
				fineRateX = 0;
				break;
			case 3:
				rotateBeginZ = rotateBeginX;
				rotateBeginY = 0;
				rotateEndZ = rotateEndX;
				rotateEndY = 0;
				coarseRateZ = coarseRateX;
				coarseRateY = 0;
				fineRateZ = fineRateX;
				fineRateY = 0;
				rotateBeginX = 0;
				rotateEndX = 0;
				coarseRateX = 0;
				fineRateX = 0;
				break;
			}
			int bracketBound = bracketMinimum.getInt();
			int maxIterations = iters.getInt();
			int numMinima = level8to4.getInt();
			TransMatrix finalMatrix=new TransMatrix(4,4);
			TransMatrix lastMatrix = null;
			AlgorithmRegOAR3DWrapper reg3;
			boolean weighted=(refWeightImage!=null);

			//			ArrayList<ImageDataMipav> registeredvols = new ArrayList<ImageDataMipav>();
			ArrayList<double[][]> xfms = new ArrayList<double[][]>();
			ArrayList<File> mtxs = new ArrayList<File>();
			int n =0;

			ArrayDoubleMtxReaderWriter mtxrw = new ArrayDoubleMtxReaderWriter();

			for(ImageData data : sourcelist){
				System.out.println(getClass().getCanonicalName()+"\t"+"Registering volume " + n +" of " + sourcelist.size());
				ImageDataMipav matchImageParent;
				if(data instanceof ImageDataMipav)
					matchImageParent = ((ImageDataMipav)data);//.getModelImageDirect();
				else 
					matchImageParent = new ImageDataMipav(data);//.getModelImageCopy();

				int extents[] = matchImageParent.getModelImageDirect().getExtents();
				int comp = 1; 
				if(extents.length>3) {
					if(extents[3]>1)
						comp = extents[3];
				}
				ModelImage combineOutputImage=null;
				int []extOut=null;

				int []order = new int[comp];
				if(useMultiStage.getValue()) {
					order[0] = multiTarget.getInt();
					int offset=1;
					for(int jComp=0;jComp<comp;jComp++) {
						if(order[0]==jComp) {
							continue; 
						} else {
							order[offset]=jComp;
							offset++;
						}
					}
				} else {
					for(int jComp=0;jComp<comp;jComp++) {
						order[jComp]=jComp;
					}
				}


				for(int jC=0;jC<comp;jC++) {
					int jComp = order[jC];
					ModelImage matchImage;
					if(comp<=1) {
						matchImage = matchImageParent.extractModelImage();
						combineOutputImage=null;
						int []ext =  refImage.getExtents();
						float[]res =  refImage.getResolutions(0);
						extOut = new int[3];
						if(overrideResolution.getValue()==false) {
							for(int ii=0;ii<3;ii++)extOut[ii]=ext[ii];
						} else {
							extOut[0]=(int)Math.round(ext[0]*res[0]/resX.getDouble());
							extOut[1]=(int)Math.round(ext[1]*res[1]/resY.getDouble());
							extOut[2]=(int)Math.round(ext[2]*res[2]/resZ.getDouble());
						}
					} else {
						matchImage = extract3DVolume(matchImageParent, jComp);
						if(jC==0) {
							int []ext =  refImage.getExtents();
							float[]res =  refImage.getResolutions(0);
							extOut = new int[4];
							if(overrideResolution.getValue()==false) {
								for(int ii=0;ii<3;ii++)
									extOut[ii]=ext[ii];
							} else {
								extOut[0]=(int)Math.round(ext[0]*res[0]/resX.getDouble());
								extOut[1]=(int)Math.round(ext[1]*res[1]/resY.getDouble());
								extOut[2]=(int)Math.round(ext[2]*res[2]/resZ.getDouble());

								res = new float[4];
								res[0]=resX.getFloat();
								res[1]=resY.getFloat();
								res[2]=resZ.getFloat();
								res[3] = matchImageParent.getHeader().getDimResolutions()[3];

							}

							extOut[3] = comp;

							combineOutputImage = new ModelImage(refImage.getType(),extOut,matchImage.getImageName()+"_regFull");

							for (FileInfoBase base : combineOutputImage.getFileInfo()) {

								hdr.copyBasicInfoTo(base);
								base.setResolutions(res);
							} 

						}
					}

					if((jC>0)&&useSingleTransform.getValue()) {
						// don't perform estimation
						finalMatrix = lastMatrix;
					} else {

						TransformBundle bundle = TransformBundle.getMatchOrientationTransform(matchImage, refImage);
						if(bundle!=null){
							ModelImage trans = TransformBundle.resample(matchImage,bundle);
							matchImage.disposeLocal();
							matchImage  = trans;
						}

						if (weighted) {
								reg3 = new AlgorithmRegOAR3DWrapper(refImage, matchImage, refWeightImage, inputWeightImage, cost, DOF, interp,
										rotateBeginX, rotateEndX, coarseRateX, fineRateX, rotateBeginY, rotateEndY,
										coarseRateY, fineRateY, rotateBeginZ, rotateEndZ, coarseRateZ, fineRateZ,
										maxOfMinResol, doSubsample, fastMode, bracketBound, maxIterations,
										numMinima);
							} else {
								reg3 = new AlgorithmRegOAR3DWrapper(refImage, matchImage, cost, DOF, interp, rotateBeginX, rotateEndX,
										coarseRateX, fineRateX, rotateBeginY, rotateEndY, coarseRateY, fineRateY,
										rotateBeginZ, rotateEndZ, coarseRateZ, fineRateZ, maxOfMinResol,
										doSubsample, fastMode, bracketBound, maxIterations, numMinima);

							}
						reg3.setObserver(this);
						reg3.setMultiThreadingEnabled(OARThreadedBool.getValue());
						reg3.run();

						finalMatrix = reg3.getTransform();					
					}
					lastMatrix = finalMatrix.clone();
					int xdimA = refImage.getExtents()[0];
					int ydimA = refImage.getExtents()[1];
					int zdimA = refImage.getExtents()[2];
					float xresA = refImage.getFileInfo(0).getResolutions()[0];
					float yresA = refImage.getFileInfo(0).getResolutions()[1];
					float zresA = refImage.getFileInfo(0).getResolutions()[2];
					if(overrideResolution.getValue()) {
						xdimA = extOut[0];
						ydimA = extOut[1];
						zdimA = extOut[2];
						xresA = (float)resX.getDouble();
						yresA = (float)resY.getDouble();
						zresA = (float)resZ.getDouble();
					}
					String name = matchImage.getImageName()+ "_reg";
					double[][] tmp=new double[4][4];
					for(int i=0;i<4;i++){
						for(int j=0;j<4;j++){
							tmp[i][j] = finalMatrix.get(i, j);
						}
					}

					System.out.println(getClass().getCanonicalName()+"\t"+"Flirt Method "+finalMatrix.matrixToString(4, 4)+" ("+xresA+","+yresA+","+zresA+") ("+xdimA+","+ydimA+","+zdimA+")");

					TransformBundle bundle = TransformBundle.getMatchOrientationTransform(matchImage, refImage);
					if(bundle!=null){
						ModelImage trans = TransformBundle.resample(matchImage,bundle);
						matchImage.disposeLocal();
						matchImage  = trans;
					}
					
					AlgorithmTransform transform = new AlgorithmTransform(matchImage, finalMatrix, interp2, xresA, yresA, zresA,xdimA, ydimA, zdimA, true, false, false);
					transform.setUpdateOriginFlag(true);
					transform.run();
					ModelImage resultImage = transform.getTransformedImage();
					transform.finalize();
					//					resultImage.getFileInfo(0).setAxisOrientation(refImage.getFileInfo(0).getAxisOrientation());
					//					resultImage.getFileInfo(0).setImageOrientation(refImage.getFileInfo(0).getImageOrientation());
					resultImage.calcMinMax();				
					MipavController.setModelImageName(resultImage,name);

					File f=null;
					File ff=null;
					try {
						f = new File(dir.getCanonicalPath()+File.separator+resultImage.getImageName()+"_xfm.mtx");
						ff = mtxrw.write(tmp, f);
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(ff!=null){
						mtxs.add(ff);
					}
					if(combineOutputImage==null) {
						allregistered.add(new ImageDataMipavWrapper(resultImage));
						allregistered.writeAndFreeNow(parent);
						resultImage=null;
					} else {
						for(int ii=0;ii<extOut[0];ii++)
							for(int jj=0;jj<extOut[1];jj++)
								for(int kk=0;kk<extOut[2];kk++)
									combineOutputImage.set(ii,jj,kk,jComp,resultImage.get(ii,jj,kk));

						if(jC==0 && useMultiStage.getValue()) {
							refImage.disposeLocal();
							JistLogger.logOutput(JistLogger.INFO, "Using intra-scan target");
							refImage = resultImage;
						} else {
							resultImage.disposeLocal();
							resultImage=null;
						}
					}


					matchImage.disposeLocal();				
					matchImage=null;
					xfms.add(tmp);
					n++;
				}
				if(combineOutputImage!=null) {
					allregistered.add(new ImageDataMipavWrapper(combineOutputImage));
					allregistered.writeAndFreeNow(parent);
				}

				mtxxfms.setValue(mtxs);

				if(sources.getValue().size()==1){
					Matrix m = new Matrix(4,4);
					for(int i=0;i<4;i++){
						for(int j=0;j<4;j++){
							m.set(i, j, finalMatrix.get(i, j));
						}
					}
					trans.setValue(m);


				}else{
					transformations.setObject(xfms);
					transformations.setFileName(target.getImageData().getName()+"transformations");
				}
			}
			refImage.disposeLocal();
			if(refWeightImage!=null)
				refWeightImage.disposeLocal();
			if(inputWeightImage!=null)
				inputWeightImage.disposeLocal();

		}
	}

	public static  ModelImage extract3DVolume(ImageDataMipav vol, int cIndex) {
		JistLogger.logOutput(JistLogger.FINE, "Starting MedicAlgorithmExtractVolumeComponent");

		ImageDataMipav cvol = null;
		JistLogger.logOutput(JistLogger.FINE, "Data loaded");
		cvol=new ImageDataMipav(vol.getName()+"_"+cIndex,vol.getType(),vol.getRows(),vol.getCols(),vol.getSlices());
		JistLogger.logOutput(JistLogger.FINE, "New volume allocated");
		for(int i=0;i<vol.getRows();i++){
			for(int j=0;j<vol.getCols();j++){
				for(int k=0;k<vol.getSlices();k++){						
					cvol.set(i,j,k,0,vol.getDouble(i, j, k, cIndex));						
				}
			}
		}
		JistLogger.logOutput(JistLogger.FINE, "Data copied");
		cvol.setHeader(vol.getHeader());
		JistLogger.logOutput(JistLogger.FINE, "Header copied");
		return cvol.extractModelImage();

	}

	protected static class AlgorithmRegOAR3DWrapper extends AlgorithmRegOAR3D{
		public AlgorithmRegOAR3DWrapper(ModelImage _imagea, ModelImage _imageb, int choice, int _dof, int _interp,
				float beginX, float endX, float rateX, float rateX2, float beginY, float endY, float rateY,
				float rateY2, float beginZ, float endZ, float rateZ, float rateZ2, boolean resol, boolean subsample,
				boolean mode, int bound, int numIter, int minima) {
			super(_imagea, _imageb, choice, _dof, _interp, beginX, endX, rateX, rateX2, beginY, endY, rateY, rateY2, beginZ, endZ,
					rateZ, rateZ2, resol, subsample, mode, bound, numIter, minima);
			// TODO Auto-generated constructor stub
		}

		/**
		 * @param arg0
		 * @param arg1
		 * @param arg2
		 * @param arg3
		 * @param arg4
		 * @param arg5
		 * @param arg6
		 * @param arg7
		 * @param arg8
		 * @param arg9
		 * @param arg10
		 * @param arg11
		 * @param arg12
		 * @param arg13
		 * @param arg14
		 * @param arg15
		 * @param arg16
		 * @param arg17
		 * @param arg18
		 * @param arg19
		 * @param arg20
		 * @param arg21
		 * @param arg22
		 * @param arg23
		 * @param arg24
		 */
		public AlgorithmRegOAR3DWrapper(ModelImage arg0, ModelImage arg1, ModelImage arg2, ModelImage arg3, int arg4,
				int arg5, int arg6, float arg7, float arg8, float arg9, float arg10, float arg11, float arg12,
				float arg13, float arg14, float arg15, float arg16, float arg17, float arg18, boolean arg19,
				boolean arg20, boolean arg21, int arg22, int arg23, int arg24) {
			super(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16,
					arg17, arg18, arg19, arg20, arg21, arg22, arg23, arg24);
			// TODO Auto-generated constructor stub
		}

		/**
		 * @param _imagea
		 * @param _imageb
		 * @param weight
		 * @param weight2
		 * @param choice
		 * @param _dof
		 * @param _interp
		 * @param beginX
		 * @param endX
		 * @param rateX
		 * @param rateX2
		 * @param beginY
		 * @param endY
		 * @param rateY
		 * @param rateY2
		 * @param beginZ
		 * @param endZ
		 * @param rateZ
		 * @param rateZ2
		 * @param resol
		 * @param subsample
		 * @param mode
		 * @param bound
		 * @param numIter
		 * @param minima
		 */
		protected AbstractCalculation observer;

		public void setObserver(AbstractCalculation observer){
			this.observer=observer;
		}
		public void runAlgorithm(){
			observer.setTotalUnits(100);
			super.runAlgorithm();
			observer.markCompleted();
		}
		/**
		 * Notifies all listeners that have registered interest for notification on this event type.
		 *
		 * @param  value  the value of the progress bar.
		 */
		protected void fireProgressStateChanged(int value) {
			super.fireProgressStateChanged(value);
			observer.setCompletedUnits(value);
		}

		/**
		 * Updates listeners of progress status. Without actually changing the numerical value
		 *
		 * @param  imageName  the name of the image
		 * @param  message    the new message to display
		 */
		protected void fireProgressStateChanged(String imageName, String message) {
			super.fireProgressStateChanged(imageName, message);
			observer.setLabel(message);
		}
	}
	



	
}
