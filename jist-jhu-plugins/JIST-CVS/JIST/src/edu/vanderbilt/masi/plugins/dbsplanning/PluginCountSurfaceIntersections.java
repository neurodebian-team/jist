package edu.vanderbilt.masi.plugins.dbsplanning;


import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;


import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import edu.jhu.ece.iacl.jist.io.CurveVtkReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamLong;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamNumberCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamString;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurface;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamSurfaceCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.pipeline.view.input.ParamNumberSliderInputView;
import edu.jhu.ece.iacl.jist.structures.geom.CurveCollection;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface.Edge;
import edu.jhu.ece.iacl.jist.structures.geom.EmbeddedSurface.Face;
import gov.nih.mipav.view.renderer.J3D.model.structures.ModelTriangleMesh;


/**
 * @author Blake Lucas (bclucas@jhu.edu)
 *
 */
public class PluginCountSurfaceIntersections extends ProcessingAlgorithm{

	ParamSurface surfParam;
	ParamFloat floatConeDeg;
	ParamPointFloat pointTargetR;
	ParamPointFloat pointEntryR;
	ParamPointFloat pointTargetL;
	ParamPointFloat pointEntryL;
	ParamSurface labeledSurfParam;
	ParamInteger option;
	

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Determine number of hits that every ray within the uncertainty cone intersects the cortical surface.\nAlgorithm Version: " + revnum + "\n";


	/**
	 * Create input parameters for this plug-in
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		/*
		 * Set the Package group and category.
		 * Set the name and label for the algorithm.
		 */
		inputParams.setPackage("MASI");
		inputParams.setCategory("DBS-Planning");
		inputParams.setLabel("Count Surface Intersections");
		inputParams.setName("Count Surface Intersections");


		//Set the algorithm's information
		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("Latifah","latifah.mat.nen@vanderbilt.edu",""));
		info.setDescription(shortDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);


		inputParams.add(pointTargetR=new ParamPointFloat("Right Target Point",new Point3f(0,0,0)));
		inputParams.add(pointEntryR=new ParamPointFloat("Right Entry Point",new Point3f(0,0,0)));
		inputParams.add(pointTargetL=new ParamPointFloat("Left Target Point",new Point3f(0,0,0)));
		inputParams.add(pointEntryL=new ParamPointFloat("Left Entry Point",new Point3f(0,0,0)));
		inputParams.add(floatConeDeg=new ParamFloat("Cone of Uncertainty (deg)",0,90,0));
		inputParams.add(option=new ParamInteger("Type of labeled surface",0));
		
		inputParams.add(surfParam=new ParamSurface("Surface"));

	}


	/**
	 * Create output parameters for this plug-in
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		//Create output parameters
		outputParams.add(labeledSurfParam=new ParamSurface("Labeled Surface"));
		
	}

	/**
	 * Execute algorithm
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		//Initialize algorithm using parameter value
		MyAlgorithm algo=new MyAlgorithm(this);
		monitor.observe(algo);
		algo.execute();
	}


	/**
	 * Example algorithm
	 * @author Blake Lucas (bclucas@jhu.edu)
	 *
	 */
	protected class MyAlgorithm extends AbstractCalculation{
		/**
		 * Construct algorithm
		 */		

		ProcessingAlgorithm parent;
		/**
		 * Construct algorithm
		 * @param parent parent algorithm
		 */
		public MyAlgorithm(ProcessingAlgorithm p){
			super();		
			parent=p;
		}

		
		/*
		 * Perform vectors subtraction (a-b)
		 *	@return			Yield vector
		 */
		public Vector3f minusV(Point3f a, Point3f b)
		{
			Vector3f sub = new Vector3f();
			sub.x = a.x - b.x;
			sub.y = a.y - b.y;
			sub.z = a.z - b.z;
			return sub;

		}
		
		public ArrayList lineParam(Point3f a, Point3f b)			
		{
			ArrayList param = new ArrayList(2);
			Vector3f dir = minusV(b,a);			//starts frm a
			dir.normalize();
			param.add(0,dir);						//stores parametric coeff
			param.add(1,a);						//stores starting pt
			return param;
		}
		
		
		/*
		 * Determine whether 2 lines intersect in 3d space (ray intersects triangle's edge)
		 * @param	p0			Starting point of ray1 that is intersecting
		 * @param	d			Direction vector of ray1
		 * @param	PA,PB,		Endpoints per edge (starts at A)	
		 * @return				Flag: 0 for no intersection and 1 otherwise.
		 */
		public ArrayList lineIntersect(Point3f p0, Vector3f d, Point3f PA, Point3f PB)
		{
			ArrayList result = new ArrayList(2);
			float a2 = d.x;
			float b2 = d.y;			//param coeff for ray
			float c2 = d.z;
			float xo2 = p0.x;
			float yo2 = p0.y;
			float zo2 = p0.z;
			float e1,e2;
			float t,s;
									
			ArrayList ed1 = lineParam(PA,PB);
			Point3f startPt = new Point3f();
			Vector3f coeff = new Vector3f();
			startPt = (Point3f) ed1.get(1);
			coeff = (Vector3f) ed1.get(0);
			float a1 = 	coeff.getX();
			float b1 = 	coeff.getY();
			float c1 = 	coeff.getZ();
			float xo1 = startPt.getX();
			float yo1 = startPt.getY();
			float zo1 = startPt.getZ();
			
			//solving simultaneous eqn for s & t
			
			if ((b1==0 && b2==0) && (a1!=0 && a2!=0))
			{
				yo1 = zo1;
				yo2 = zo2;
				b1 = c1;
				b2 = c2;
			}
			
			if ((a1==0) && (b1!=0 && b2!=0))
			{
				xo1 = zo1;
				xo2 = zo2;
				a1 = c1;
				a2 = c2;
			}
											
			s = ((a1*(yo2-yo1))-(b1*(xo2-xo1)))/((b1*a2)-(a1*b2));
			t = ((xo2-xo1)+(s*a2))/a1;
						
			float boundt = 0;
			
			if (coeff.x !=0)
			{
				boundt = (PB.x-startPt.x)/coeff.x;
			}
			
			if (coeff.y !=0)
			{
				boundt = (PB.y-startPt.y)/coeff.y;
			}
			
			if (coeff.z!=0)
			{
				boundt = (PB.z-startPt.z)/coeff.z;
			}
			
		
			//Substitute into z to get equality
			e1 = zo1 + (c1*t);
			
			e2 = zo2 + (c2*s);
			
			float ee1 = startPt.getY() + coeff.getY()*t;
			
			float ee2 = p0.y + d.y*s;
			
			float eee1 = startPt.getX() + coeff.getX()*t;
			
			float eee2 = p0.x + d.x*s;
			
			Point3f pit = new Point3f(eee1,ee1,e1);			
			Vector3f myD = minusV((Point3f) ed1.get(1),pit);
			myD.normalize();
						
			if (Math.abs(e1-e2)<1.0E-3 && Math.abs(ee2-ee1)<1.0E-3 && Math.abs(eee1-eee2)<1.0E-3)				//intersects
			{	
				if (t<0 || t>boundt)
				{
					result.add(0, 0);				//only accept intersection within edge's line
				}
				
				else
				{
					result.add(0, 1);		//flag
					result.add(1, pit);	
				}
				
			}
			
			else
			{
				result.add(0, 0);
			}
									
			
			return result;
		}
				
		public static final float EPS = 1e-4f;
		
		/*
		 * Determine whether a line segment intersects a triangle or not
		 * @param	p0			Starting point of ray that is intersecting
		 * @param	d			Direction vector 
		 * @param	PA,PB,PC	All three vertices of the triangle			
		 * @return				Flag: 0 for no intersection and 1 otherwise.
		 */
		public ArrayList rayTriangleIntersect(Point3f p0, Vector3f d, Point3f PA, Point3f PB, Point3f PC)
		{	
			ArrayList result = new ArrayList(2);
			Vector3f e1 = minusV(PB,PA);
			Vector3f e2 = minusV(PC,PA);
			Vector3f normal = new Vector3f();
			normal.cross(e1, e2); normal.normalize();
			Vector3f q = new Vector3f() ;
			q.cross(d, e2);
			float a = e1.dot(q);			//determinant of matrix
			int flag; Point3f pit = new Point3f(); 
			
			ArrayList edge1 = lineIntersect(p0,d, PA, PB);
			
			ArrayList edge2 = lineIntersect(p0,d, PB,PC);
					
			ArrayList edge3 = lineIntersect(p0,d,PA,PC);					
									
			if (a>-EPS && a<EPS)
			{
				result.add(0,0);
				return result;				// dot product ==0; vector is parallel to planes
			}
			
			float f = 1/a;
			Vector3f s = minusV(p0,PA);
			float u = f*(s.dot(q));			//barycentric coordinates
			
			if (u<=0.0f)					//intersection at edge
			{
				
				if (edge1.get(0).equals(1) || edge2.get(0).equals(1) || edge3.get(0).equals(1))			//either 1 edge must intersect or none
				{
					result.add(0,2);
					if (edge1.get(0) .equals(1))
					{					
						result.add(1,edge1.get(1));
						
						return result;						
					}
					if (edge2.get(0).equals(1))
					{
						result.add(1,edge2.get(1));
						
						return result;
					}
					if (edge3.get(0).equals(1))
					{
						result.add(1,edge3.get(1));
						
						return result;
					}					
					
				}
				
				result.add(0,0);
				return result;					//intersection is outside of triangle
			}
			
			Vector3f r = new Vector3f(); r.cross(s, e1);
			float v = f*(d.dot(r));				//barycentric coordinates
			
			if (v<=0.0f || (u+v)>1.0f)
			{
				if (edge1.get(0).equals(1) || edge2.get(0).equals(1) || edge3.get(0).equals(1))			//either 1 edge must intersect or none
				{
					result.add(0,2);
					if (edge1.get(0) .equals(1))
					{
						result.add(1,edge1.get(1));
						
					}
					if (edge2.get(0).equals(1))
					{
						result.add(1,edge2.get(1));
						
					}
					if (edge3.get(0).equals(1))
					{
						result.add(1,edge3.get(1));
						
					}
					return result;
				}
				result.add(0,0);
				return result;	
								//intersection outside of triangle
			}
			
			float t = f*(e2.dot(r));			//distance frm ray origin
			if (t<0)							//dont accept negative distance
			{
				result.add(0,0);
				return result;	
			}
			
			Vector3f po = new Vector3f(p0.x,p0.y,p0.z);			
			Vector3f td = new Vector3f(t*(d.x),t*(d.y),t*(d.z));
			po.add(td);
						
			pit = new Point3f(po.x,po.y,po.z);			//point of intersection (on surface)
			result.add(0,1);
			result.add(1,pit);
			
			return result;				
			
		}
		
		public boolean equals(ArrayList a, ArrayList b)
		{
			boolean flag = false;
			if (a.size() != b.size())
				flag = false;
			
			for (int i=0; i<a.size(); i++)
			{
				Tuple3f eA = (Tuple3f) a.get(i);
				Tuple3f eB = (Tuple3f) b.get(i);
				
				if (eA.x == eB.x && eA.y==eB.y && eA.z == eB.z)
					flag = true;
			}
				
			return flag;			
			
		}
		
		public boolean equalP(Point3f pa, Point3f p)
		{
			boolean flag = false;
			
			
			if (Math.abs(pa.x-p.x)<=1.5E-3 && Math.abs(pa.y-p.y)<=1.5E-3 && Math.abs(pa.z-p.z)<=1.5E-3)
				flag = true;
				
			return flag;
			
		}
		
		/*
		 * Determine whether a triangle is within the uncertainty cone
		 * @param	target			Target pt
		 * @param	a,b,c			Vertices of the triangle
		 * @param	d				Direction vector (entry -> target)	
		 * @param	cone			Uncertainty cone angle
		 * @return					Flag: true if triangle is in cone
		 */
		public boolean faceInCone(Point3f target, Point3f a, Point3f b, Point3f c, Vector3f d, float cone)
		{
			boolean flag = false;
			
			Vector3f da = minusV(a,target); da.normalize();
			Vector3f db = minusV(b,target); db.normalize();
			Vector3f dc = minusV(c,target); dc.normalize();
			float angle1 = (float) ((float) Math.acos(d.dot(da)));  
			float angle2 = (float) ((float) Math.acos(d.dot(db))); 
			float angle3 = (float) ((float) Math.acos(d.dot(dc))); 
			if (angle1 <=cone && angle2<= cone && angle3 <=cone)
			{
				flag = true;
			}
				
			return flag;
		}
		
		/*
		 * Determine the current point of intersection is already in the set of intersection pts
		 * @param	pit				Point of intersection
		 * @param	pitV			A vector of points of intersection
		 * @return					Flag: true if pt is already in the set
		 */
		public boolean isIntersectionPt(Point3f pit, Vector<Point3f> pitV)
		{
			boolean flag = false;
			
			Outer:
			for (int i =0; i<pitV.size(); i++)
			{
				if (equalP(pit, (Point3f) pitV.get(i)))
				{
					flag = true;
					break Outer;
				}
				
			}
			
			return flag;		
		
		}
		
		
		/**
		 * Call algorithm
		 */
		public void execute(){

			Point3f targetPtR = pointTargetR.getValue();
			Point3f entryPtR = pointEntryR.getValue();
			Point3f targetPtL = pointTargetL.getValue();
			Point3f entryPtL = pointEntryL.getValue();
			EmbeddedSurface surf = surfParam.getSurface();
			int optionType = option.getInt();
			surf.buildAllTables();
			
			this.setTotalUnits(surf.getVertexCount());
			this.setLabel("Latifah");
			
			Face[] faces = surf.getFaces();
			float cone = floatConeDeg.getValue().floatValue();
			cone = (float) Math.toRadians(cone);							
			
			
			Vector3f targetEntryVectorR = new Vector3f(minusV(entryPtR,targetPtR));
			Vector3f targetEntryVectorL = new Vector3f(minusV(entryPtL,targetPtL));
			targetEntryVectorR.normalize();
			targetEntryVectorL.normalize();
					
			Vector<Face> faceR = new Vector<Face>();
			Vector<Face> faceL = new Vector<Face>();
			Vector<Point3f> pitVR = new Vector<Point3f>();	
			Vector<Point3f> pitVL = new Vector<Point3f>();	
			
			
			for (Face f : faces)
			{
				int []fv = f.getVertexes();
				Point3f A,B,C;
				A = surf.getVertex(fv[0]);
				B = surf.getVertex(fv[1]);
				C = surf.getVertex(fv[2]);
				if (faceInCone(targetPtR,A,B,C,targetEntryVectorR,cone))
					faceR.add(f);
				if (faceInCone(targetPtL,A,B,C,targetEntryVectorL,cone))
					faceL.add(f);
			}
			
						
			for(int jVert=0;jVert<surf.getVertexCount();jVert++) 							
			{
				
				this.setCompletedUnits(jVert);
				Point3f pt = surf.getVertex(jVert); //vertex at iteration i
				
				//deal with cone's degree

				Vector3f targetJVertexVector = new Vector3f(minusV(pt,targetPtR));
				targetEntryVectorR.normalize();
				targetJVertexVector.normalize();
				float angle = (float) ((float) Math.acos(targetEntryVectorR.dot(targetJVertexVector)));    //rad
				Vector3f dR = targetJVertexVector;
				
				Vector3f targetJVertexVectorL = new Vector3f(minusV(pt,targetPtL));
				targetEntryVectorL.normalize();
				targetJVertexVectorL.normalize();
				float angleL = (float) ((float) Math.acos(targetEntryVectorL.dot(targetJVertexVectorL)));  
				Vector3f dL = targetJVertexVectorL;
														
				if (angle<=cone)
				{	
					int hit=0;
					
					for(Face f : faceR) 		//find how many triangles intersect the ray
					{
						
						int []fv = f.getVertexes();
						Point3f A,B,C;
						A = surf.getVertex(fv[0]);
						B = surf.getVertex(fv[1]);
						C = surf.getVertex(fv[2]);
						
											
						ArrayList intersect = rayTriangleIntersect(targetPtR,dR,A,B,C);
								
						Outer:
						if(intersect.get(0).equals(2))			//intersects on edge
						{
							Vector3f myD = minusV((Point3f) intersect.get(1),targetPtR);
							myD.normalize();
							
							if (isIntersectionPt((Point3f) intersect.get(1),pitVR)){					//already hit this pt
								break Outer;}
							
							if (Math.abs(myD.x-dR.x)>1E-3 || Math.abs(myD.y-dR.y)>1E-3 || Math.abs(myD.z-dR.z)>1E-3){			//dont allow backward direction
								break Outer;}
							
							
							hit++;	
							pitVR.add((Point3f) intersect.get(1));
										
					
						}
									
						
						Outer:
						if (intersect.get(0).equals(1))			//point on face
						{
							Vector3f myD = minusV((Point3f) intersect.get(1),targetPtR);
							myD.normalize();
							if ((equalP((Point3f) intersect.get(1),A) || equalP((Point3f) intersect.get(1),B) || equalP((Point3f) intersect.get(1),C)) && hit==1)		//for intersection on vertex
							{
								break Outer;
							}
							if (isIntersectionPt((Point3f) intersect.get(1),pitVR))		
								break Outer;
							if (Math.abs(myD.x-dR.x)>1E-3 || Math.abs(myD.y-dR.y)>1E-3 || Math.abs(myD.z-dR.z)>1E-3){			//dont allow backward direction
								break Outer;}
							
							
							hit++;
							pitVR.add((Point3f) intersect.get(1));
						}
									
						
					}	//end face
				
					switch (optionType)
					{
						case 0:
							surf.setVertexData(jVert,hit); 
							break;
						case 1:
							if (hit==1)
								surf.setVertexData(jVert,hit); 
							else
								surf.setVertexData(jVert,-1); 
							break;
							
					} 
													
						
				} 
				

				else if (angleL<=cone)			//LEFT HEMISPHERE
				{
					
					int hit=0;
					for(Face f : faceL) 		//find how many triangles intersect the ray
					{
						
						int []fv = f.getVertexes();
						Point3f A,B,C;
						A = surf.getVertex(fv[0]);
						B = surf.getVertex(fv[1]);
						C = surf.getVertex(fv[2]);
						
												
						ArrayList intersect = rayTriangleIntersect(targetPtL,dL,A,B,C);
								
						Outer:
						if(intersect.get(0).equals(2))			//intersects on edge
						{
							Vector3f myD = minusV((Point3f) intersect.get(1),targetPtL);
							myD.normalize();
							
							if (isIntersectionPt((Point3f) intersect.get(1),pitVL)){					//already hit this pt
								break Outer;}
							
							if (Math.abs(myD.x-dL.x)>1E-3 || Math.abs(myD.y-dL.y)>1E-3 || Math.abs(myD.z-dL.z)>1E-3){			//dont allow backward direction
								break Outer;}
							
							
							hit++;	
							pitVL.add((Point3f) intersect.get(1));
							
										
					
						}
									
						
						Outer:
						if (intersect.get(0).equals(1))			//point on face
						{
							Vector3f myD = minusV((Point3f) intersect.get(1),targetPtL);
							myD.normalize();
							if ((equalP((Point3f) intersect.get(1),A) || equalP((Point3f) intersect.get(1),B) || equalP((Point3f) intersect.get(1),C)) && hit==1)		//for intersection on vertex
							{
								break Outer;
							}
							if (isIntersectionPt((Point3f) intersect.get(1),pitVL))		
								break Outer;
							if (Math.abs(myD.x-dL.x)>1E-3 || Math.abs(myD.y-dL.y)>1E-3 || Math.abs(myD.z-dL.z)>1E-3){			//dont allow backward direction
								break Outer;}
							
							
							hit++;
							pitVL.add((Point3f) intersect.get(1));
						}
									
						
					}	//end face
				
					switch (optionType)
					{
						case 0:
							surf.setVertexData(jVert,hit); 
							break;
						case 1:
							if (hit==1)
								surf.setVertexData(jVert,hit); 
							else
								surf.setVertexData(jVert,-1); 
							break;
							
					}
								
				}
				
				
				else
					surf.setVertexData(jVert,-1); 
																						
			} 
				
			labeledSurfParam.setValue(surf);		

		}
	}

}
