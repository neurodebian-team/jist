package edu.vanderbilt.masi.plugins.dti;

import java.io.File;

import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamDouble;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFile;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.VoxelType;
import edu.jhu.ece.iacl.jist.utility.JistLogger;
import edu.jhu.ece.iacl.plugins.dti.ComputeMeanB0;
import edu.jhu.ece.iacl.plugins.dti.ComputeMeanDW;
import edu.jhu.ece.iacl.plugins.dti.DWITensorColorEncodedMap;
import edu.jhu.ece.iacl.plugins.dti.DWITensorContrasts;
import edu.jhu.ece.iacl.plugins.dti.DWITensorEstLLMSE;
import edu.jhu.ece.iacl.plugins.dti.tractography.MedicAlgorithmFiberTracker;
import edu.jhu.ece.iacl.plugins.registration.MedicAlgorithmEfficientFileCollectionRegistration;
import edu.jhu.ece.iacl.plugins.utilities.volume.AlgorithmVolumeFileCollectionTo3D;
import edu.jhu.ece.iacl.plugins.utilities.volume.SlabCollectionToVolume4D;
import edu.jhu.ece.iacl.plugins.utilities.volume.VolumeCollectionToCombinedSlabCollection;

public class CATNAP  extends ProcessingAlgorithm  {


	//input params
	private ParamVolumeCollection sourceVols;		
	private ParamVolume targetVol;
	private ParamVolume tensor;
	ParamFile gradTable;
	ParamFile bvalTable;
	private DWITensorContrasts pluginContrasts;
	private DWITensorColorEncodedMap pluginDEC;

	// Internal Plugins
	private AlgorithmVolumeFileCollectionTo3D plugin4Dto3D;
	private MedicAlgorithmEfficientFileCollectionRegistration pluginReg;
	private VolumeCollectionToCombinedSlabCollection pluginToSlab; 
	private MedicAlgorithmFiberTracker pluginFACT;
	private ComputeMeanB0 pluginMeanB0;
	private ComputeMeanDW pluginMeanDW;

	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "") .replace(" ", "");

	@Override
	protected void createInputParameters(ParamCollection inputParams) {

		inputParams.setName("CATNAP");
		inputParams.setLabel("CATNAP");

		inputParams.setPackage("MASI");
		inputParams.setCategory("DTI");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://masi.vuse.vanderbilt.edu");
		info.add(new AlgorithmAuthor("Bennett Landman", "bennett.landman@vanderbilt.edu", "http://masi.vuse.vanderbilt.edu"));
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);



		ParamCollection mainParams=new ParamCollection("Main");
		mainParams.add(sourceVols=new ParamVolumeCollection("Diffusion Weighted Volumes"));
		sourceVols.setLoadAndSaveOnValidate(false);
		mainParams.add(targetVol=new ParamVolume("Target Volumes (mean b0 if null)",null,-1,-1,-1,1));
		targetVol.setMandatory(false);

		mainParams.add(gradTable = new ParamFile("Gradient Directions (unit vectors)",new FileExtensionFilter(new String[]{"grad"})));
		mainParams.add(bvalTable = new ParamFile("B-values (s/mm2)",new FileExtensionFilter(new String[]{"b"})));

		inputParams.add(mainParams);

		// Step 1: plugin4Dto3D
		plugin4Dto3D = new AlgorithmVolumeFileCollectionTo3D();

		// Step 3: pluginReg
		pluginReg = new MedicAlgorithmEfficientFileCollectionRegistration();
		ParamCollection regParams = pluginReg.getInput();
		regParams.setName("Registration");
		regParams.getFirstChildByLabel("Cost function").setValue("Normalized mutual information");
		regParams.getFirstChildByLabel("Output interpolation").setValue("Bspline 3rd order");
		regParams.getFirstChildByLabel("Registration interpolation").setValue("Bspline 3rd order");
		regParams.getFirstChildByLabel("Skip multilevel search (Assume images are close to alignment)").setValue(true);
		regParams.getFirstChildByLabel("Source Volumes").setHidden(true);
		regParams.getFirstChildByLabel("Target Volume").setHidden(true);
		regParams.getFirstChildByLabel("Gradient Table").setHidden(true);

		inputParams.add(regParams);
		// Step 4: pluginToSlab
		pluginToSlab= new VolumeCollectionToCombinedSlabCollection();
		ParamCollection slabParams = pluginToSlab.getInput();
		slabParams.setName("Slab");
		slabParams.getFirstChildByLabel("Input Volume Files").setHidden(true);
		inputParams.add(slabParams);

		// Step 9: FACT
		pluginFACT = new MedicAlgorithmFiberTracker();
		ParamCollection paramFACT = (ParamCollection)pluginFACT.getInput();
		paramFACT.setName("FACT");
		paramFACT.getFirstChildByLabel("Fractional Anisotropy").setHidden(true);		
		paramFACT.getFirstChildByLabel("Vector Field").setHidden(true);
		inputParams.add(paramFACT);

		pluginContrasts = new DWITensorContrasts ();
		pluginContrasts.getInput().getFirstChildByLabel("Additional Anisotropies:").setValue("RA.VR");
		pluginContrasts.getInput().getFirstChildByLabel("Additional Eigen-Contrasts:").setValue("Eigen vectors/values");		

		pluginDEC = new  DWITensorColorEncodedMap();

		pluginMeanB0 = new ComputeMeanB0();
		pluginMeanDW = new ComputeMeanDW();

	}

	@Override
	protected void createOutputParameters(ParamCollection outputParams) {

		outputParams.add(tensor = new ParamVolume("Tensor Volume"));
		outputParams.add(pluginMeanB0.getOutputPluginSpecific());
		outputParams.add(pluginMeanDW.getOutputPluginSpecific());
		outputParams.add(pluginDEC.getOutputPluginSpecific());
		outputParams.add(pluginContrasts.getOutputPluginSpecific());		
		outputParams.add(pluginFACT.getOutputPluginSpecific());


	}

	@Override
	protected void execute(CalculationMonitor monitor)
	throws AlgorithmRuntimeException {

		// Step 1: plugin4Dto3D
		plugin4Dto3D.getInput().getFirstChildByLabel("Input Volume Files").setValue(sourceVols.getValue());
		plugin4Dto3D.setOutputDirectory(getOutputDirectory());
		plugin4Dto3D.runAlgorithm(monitor);
		sourceVols.dispose();
		ParamVolumeCollection src3D = (ParamVolumeCollection)plugin4Dto3D.getOutput().getFirstChildByLabel("Output 3D Volume Files");
		plugin4Dto3D = null;

		// Step 2: Get a target		
		File fileTarget = null;
		if(targetVol.getImageData()==null) {
			JistLogger.logOutput(JistLogger.INFO, "Computing mean b0.");
			// Compute Mean B0	

			pluginMeanB0.getInput().getFirstChildByLabel("Input Slab Collection").setValue(src3D.getValue());
			pluginMeanB0.getInput().getFirstChildByLabel("Table of diffusion weighting directions").setValue(gradTable.getValue());
			pluginMeanB0.getInput().getFirstChildByLabel("Table of b-values").setValue(bvalTable.getValue());
			pluginMeanB0.setOutputDirectory(getOutputDirectory());
			pluginMeanB0.getOutput().getFirstChildByLabel("Mean B0 volume").setLoadAndSaveOnValidate(false);
			pluginMeanB0.runAlgorithm(monitor);
			ParamVolume vol = (ParamVolume)pluginMeanB0.getOutput().getFirstChildByLabel("Mean B0 volume");
			vol.writeAndFreeNow(this);
			fileTarget = vol.getValue();
			vol=null;pluginMeanB0=null;						
		} else {
			// Use the specified target
			JistLogger.logOutput(JistLogger.INFO, "Using specified target.");
			fileTarget = targetVol.getValue();
		}

		// Step 3: Perform Motion Correction
		ParamCollection regParams = pluginReg.getInput();

		regParams.getFirstChildByLabel("Source Volumes").setValue(src3D.getValue());
		//		((ParamVolume)regParams.getFirstChildByLabel("Target Volume")).setValue(fileTarget);
		(regParams.getFirstChildByLabel("Target Volume")).setValue(fileTarget);
		((ParamFileCollection)regParams.getFirstChildByLabel("Gradient Table")).add(gradTable.getValue());
		pluginReg.setOutputDirectory(getOutputDirectory());
		pluginReg.runAlgorithm(monitor);
		src3D.dispose();		
		ParamVolumeCollection regSrc3D = (ParamVolumeCollection )pluginReg.getOutput().getFirstChildByLabel("Registered Volumes");
		ParamFile newGradTable = (ParamFile)pluginReg.getOutput().getFirstChildByLabel("Corrected Gradient Table");		
		pluginReg=null;

		// Step 10: Mean DW
		pluginMeanDW.getInput().getFirstChildByLabel("Input Slab Collection").setValue(regSrc3D.getValue());
		pluginMeanDW.getInput().getFirstChildByLabel("Table of diffusion weighting directions").setValue(gradTable.getValue());
		pluginMeanDW.getInput().getFirstChildByLabel("Table of b-values").setValue(bvalTable.getValue());
		pluginMeanDW.getInput().getFirstChildByLabel("Register to first b0?").setValue(false);
		pluginMeanDW.runAlgorithm(monitor);

		// Step 4: To Slabs
		pluginToSlab.getInput().getFirstChildByLabel("Input Volume Files").setValue(regSrc3D.getValue());
		pluginToSlab.setOutputDirectory(getOutputDirectory());
		pluginToSlab.runAlgorithm(monitor);
		regSrc3D.dispose();
		regSrc3D=null;
		ParamVolumeCollection srcSlab = (ParamVolumeCollection)pluginToSlab.getOutput().getFirstChildByLabel("Output Combined Slabs");
		pluginToSlab=null;


		// Step 5: Estimate Tensors
		DWITensorEstLLMSE pluginLLMSE = new DWITensorEstLLMSE ();
		pluginLLMSE.getInput().getFirstChildByLabel("DWI and Reference Image(s) Data (4D)").setValue(srcSlab.getValue());
		pluginLLMSE.getInput().getFirstChildByLabel("Table of diffusion weighting directions").setValue(newGradTable.getValue());
		pluginLLMSE.getInput().getFirstChildByLabel("Table of b-values").setValue(bvalTable.getValue());
		pluginLLMSE.setOutputDirectory(getOutputDirectory());
		pluginLLMSE.runAlgorithm(monitor);
		srcSlab.dispose(); srcSlab =null;
		ParamVolumeCollection col = (ParamVolumeCollection)pluginLLMSE.getOutput().getFirstChildByName("Tensor (xx,xy,xz,yy,yz,zz)");
		pluginLLMSE=null;

		// Step 6: From Slabs
		SlabCollectionToVolume4D pluginFromSlab = new SlabCollectionToVolume4D();
		pluginFromSlab.getInput().getFirstChildByLabel("Input Slab Collection").setValue(col.getValue());
		pluginFromSlab.getInput().getFirstChildByLabel("Dimension to be concatenated").setValue("Slices");
		pluginFromSlab.setOutputDirectory(getOutputDirectory());
		pluginFromSlab.getOutput().getFirstChildByLabel("Concatenated Volume").setLoadAndSaveOnValidate(false);
		pluginFromSlab.runAlgorithm(monitor);
		pluginFromSlab.getOutput().getFirstChildByLabel("Concatenated Volume").writeAndFreeNow(this);
		tensor.setValue(((ParamVolume)pluginFromSlab.getOutput().getFirstChildByLabel("Concatenated Volume")).getValue());
		pluginFromSlab=null;

		// Step 7: Compute Contrasts

		pluginContrasts.getInput().getFirstChildByLabel("Tensor Volume (4D)").setValue(tensor.getValue());
		pluginContrasts.setOutputDirectory(getOutputDirectory());
		pluginContrasts.getOutput().getFirstChildByLabel("FA").setLoadAndSaveOnValidate(false);
		pluginContrasts.getOutput().getFirstChildByLabel("VEC1").setLoadAndSaveOnValidate(false);
		pluginContrasts.runAlgorithm(monitor);
		ParamVolume fa = (ParamVolume)pluginContrasts.getOutput().getFirstChildByLabel("FA");fa.writeAndFreeNow(this);
		ParamVolume vec = (ParamVolume)pluginContrasts.getOutput().getFirstChildByLabel("VEC1");vec.writeAndFreeNow(this);
		pluginContrasts=null;

		// Step 8: DEC

		pluginDEC.getInput().getFirstChildByLabel("Scalar Weight Volume (3D)").setValue(fa.getValue()); 
		pluginDEC.getInput().getFirstChildByLabel("Vector direction to encoded color (4D)").setValue(vec.getValue());
		pluginDEC.setOutputDirectory(getOutputDirectory());
		pluginDEC.runAlgorithm(monitor);


		// Step 9: FACT
		pluginFACT.getInput().getFirstChildByLabel("Fractional Anisotropy").setValue(fa.getValue()); 
		pluginFACT.getInput().getFirstChildByLabel("Vector Field").setValue(vec.getValue());
		pluginFACT.setOutputDirectory(getOutputDirectory());
		pluginFACT.runAlgorithm(monitor);







	}

}

