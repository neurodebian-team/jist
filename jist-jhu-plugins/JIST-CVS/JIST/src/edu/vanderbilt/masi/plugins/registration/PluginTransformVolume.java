/*
 *
 */
package edu.vanderbilt.masi.plugins.registration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Point3i;

import Jama.Matrix;
import WildMagic.LibFoundation.Mathematics.Vector3f;
import edu.jhu.ece.iacl.jist.io.ArrayDoubleMtxReaderWriter;
import edu.jhu.ece.iacl.jist.io.FileExtensionFilter;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.Citation;
import edu.jhu.ece.iacl.jist.pipeline.AbstractCalculation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamFileCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamMatrix;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamObject;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointFloat;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamPointInteger;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipavWrapper;
import edu.vanderbilt.masi.utilities.volume.TransformBundle;
import gov.nih.mipav.model.algorithms.AlgorithmTransform;
import gov.nih.mipav.model.file.FileInfoBase;
import gov.nih.mipav.model.structures.ModelImage;
import gov.nih.mipav.model.structures.TransMatrix;


/*
 * @author Bennett Landman (bennett.landman@vanderbilt.edu)
 *
 */
public class PluginTransformVolume extends ProcessingAlgorithm {	
	ParamVolumeCollection vol;
	ParamVolumeCollection result;
	ParamOption interpolation;
	ParamPointInteger dims;
	ParamPointInteger ori;
	ParamPointFloat res;
	ParamBoolean clip;
	ParamBoolean pad;
	ParamBoolean updateOrigin;
	ParamBoolean useScanner;
	ParamBoolean invert;
	ParamInteger padAmt;
	ParamBoolean rotAroundCenter;
	ParamBoolean useAnatomicalCenter;
	ParamVolume matchToMe;
	ParamObject<double[][]> matrixFile;
	ParamFileCollection transformations;
	/****************************************************
	 * CVS Version Control
	 ****************************************************/
	private static final String cvsversion = "$Revision: 1.3 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Accepts a list of volumes and a list of transformation matrices." +
			"Each list must be of the same length. For each pair, this plugin applies a given transformation matrix " +
			"to a volumetric image using the specified interpolation method." +
			"Interpolation resolution is EITHER set by matching a target image or setting manually.";
	private static final String longDescription = "";


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createInputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createInputParameters(ParamCollection inputParams) {
		
		ParamCollection main = new ParamCollection("Main");
		
		main.add(vol = new ParamVolumeCollection("Volumes"));
		vol.setLoadAndSaveOnValidate(false);
		
		FileExtensionFilter xfmfilter = new FileExtensionFilter(new String[]{"mtx","xfm"});
		main.add(transformations = new ParamFileCollection("Transformation Matrixes", xfmfilter));
		main.add(interpolation = new ParamOption("Registration interpolation", new String[] { "Trilinear",
				"Bspline 3rd order", "Bspline 4th order", "Cubic Lagrangian", "Quintic Lagrangian",
				"Heptic Lagrangian", "Windowed sinc", "Nearest Neighbor" }));

		main.add(matchToMe = new ParamVolume("Volume to Match - Dimensions & Resolution", null,-1,-1,-1,-1));
		matchToMe.setLoadAndSaveOnValidate(false);
		matchToMe.setMandatory(false);
		
		inputParams.add(main);

		ParamCollection tar = new ParamCollection("Target");
		tar.add(res = new ParamPointFloat("Resolutions", new Point3f(1, 1, 1)));
		tar.add(dims = new ParamPointInteger("Dimensions", new Point3i(256, 256, 198)));
		tar.add(ori = new ParamPointInteger("Orientations", new Point3i(FileInfoBase.ORI_R2L_TYPE
				, FileInfoBase.ORI_A2P_TYPE, FileInfoBase.ORI_I2S_TYPE)));		
		tar.add(clip = new ParamBoolean("Crop"));
		tar.add(pad = new ParamBoolean("Pad"));
		tar.add(padAmt = new ParamInteger("Pad Value", 0));
		inputParams.add(tar);

		
		
		ParamCollection adv = new ParamCollection("Advanced");
		adv.add(invert=new ParamBoolean("Invert Matrix",false));
		adv.add(rotAroundCenter=new ParamBoolean("Rotate Around Center",false));
		adv.add(useAnatomicalCenter=new ParamBoolean("Use Anatomical Center",false));
		adv.add(updateOrigin = new ParamBoolean("Update Origin", true));
		adv.add(useScanner = new ParamBoolean("Use Scanner Anatomical Transform"));
		inputParams.add(adv);
		
		inputParams.setPackage("MASI");
		inputParams.setCategory("Registration.Volume");
		inputParams.setLabel("Transform Volumes");
		inputParams.setName("Transform_Volumes");

		AlgorithmInformation info = getAlgorithmInformation();
		info.setWebsite("http://www.iacl.ece.jhu.edu/");
		info.add(new AlgorithmAuthor("MASI","Bennett Landman",""));
		info.setDescription(shortDescription);
		info.setLongDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#createOutputParameters(edu.jhu.ece.iacl.pipeline.parameter.ParamCollection)
	 */
	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(result = new ParamVolumeCollection("Transformed Volumes"));
		result.setLoadAndSaveOnValidate(false);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		ExecuteWrapper wrapper=new ExecuteWrapper();
		monitor.observe(wrapper);
		wrapper.execute(this);
	}
	protected class ExecuteWrapper extends AbstractCalculation{
		public void execute(ProcessingAlgorithm alg){
			List<ParamVolume> imglist = vol.getParamVolumeList();
			//ArrayList<ImageData> resultlist = new ArrayList<ImageData>();
			int i =0;

			ArrayList<double[][]> xfms = readMultiMatrices(transformations.getValue());
			System.out.println(getClass().getCanonicalName()+"\t"+transformations.getValue());
			System.out.println(getClass().getCanonicalName()+"\t"+transformations.getValue().size());
			ModelImage img = null;
			Matrix m;
			for(ParamVolume pv : imglist){
				ImageData v = pv.getImageData();

				if(v instanceof ImageDataMipav) { 
					img = ((ImageDataMipav)v).extractModelImage();
				} else {
					img = v.getModelImageCopy();
				};
				v.dispose();
				pv.dispose();

				m = new Matrix(4,4);
				if(transformations.getValue().size()>0){
					double[][] matarray = xfms.get(i);
					if(matarray.length!=4 || matarray[0].length!=4){
						System.err.println(getClass().getCanonicalName()+"Invalid transformation - must be 4x4");
					}else{
						for(int k=0; k<matarray.length; k++){
							for(int j=0; j<matarray[0].length; j++){
								m.set(k, j, matarray[k][j]);
							}
						}
					}
				}
				result.add(new ImageDataMipavWrapper(transform(img,m)));
				result.writeAndFreeNow(alg);
				img.disposeLocal();
				i++;

			}		
			imglist = null; vol.dispose();vol=null;
			//result.setValue(resultlist);
		}

		// TODO: This should really use the static transform class! 
		private ModelImage transform(ModelImage img, Matrix m){
			int interp = 0;
			switch (interpolation.getIndex()) {
			case 0:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			case 1:
				interp = AlgorithmTransform.BSPLINE3;
				break;
			case 2:
				interp = AlgorithmTransform.BSPLINE4;
				break;
			case 3:
				interp = AlgorithmTransform.CUBIC_LAGRANGIAN;
				break;
			case 4:
				interp = AlgorithmTransform.QUINTIC_LAGRANGIAN;
				break;
			case 5:
				interp = AlgorithmTransform.HEPTIC_LAGRANGIAN;
				break;
			case 6:
				interp = AlgorithmTransform.WSINC;
				break;
			case 7:
				interp = AlgorithmTransform.NEAREST_NEIGHBOR;
				break;
			default:
				interp = AlgorithmTransform.TRILINEAR;
				break;
			}
			Point3i dimensions;
			Point3f resolution;
			if(matchToMe.getValue()!=null){
				ModelImage tar = matchToMe.getImageData().getModelImageCopy();
				TransformBundle bundle = TransformBundle.getMatchOrientationTransform(img, tar);
				if(bundle!=null){
					ModelImage trans = TransformBundle.resample(img,bundle);
					img.disposeLocal();
					img = trans;
				}				
				int[] a = tar.getExtents();
				dimensions = new Point3i(a[0],a[1],a[2]);
				float[] b = tar.getResolutions(0);
				resolution = new Point3f(b[0],b[1],b[2]);
				tar.disposeLocal();
			}else{
				
				resolution = res.getValue();
				dimensions = dims.getValue();
			}
			TransMatrix mat = new TransMatrix(4);

			if(invert.getValue())m=m.inverse();
			double[][] ar=m.getArray();
			System.out.println(getClass().getCanonicalName()+"\t"+m);

			//		mat.setMatrix(0,ar.length-1,0,ar[0].length,ar);

			//First Set mat as the Identity
			for(int i=0; i<4; i++){
				for(int j=0; j<4; j++){
					mat.set(i, j, 1);
				}
			}

			for(int i=0; i<ar.length; i++){
				for(int j=0; j<ar[0].length; j++){
					mat.set(i, j, ar[i][j]);
				}
			}
			System.out.println(getClass().getCanonicalName()+"\t"+mat.matrixToString(4, 3));

			Vector3f center = null;
			if (rotAroundCenter.getValue()) {
				center = img.getImageCentermm(useAnatomicalCenter.getValue());
			}
			int[] units = new int[img.getUnitsOfMeasure().length];
			for (int i = 0; i < units.length; i++) {
				units[i] = img.getUnitsOfMeasure(i);
			}
			AlgorithmTransform transform = new AlgorithmTransform(img, mat, interp, resolution.x, resolution.y,
					resolution.z, dimensions.x, dimensions.y, dimensions.z, units,true,clip.getValue(), pad.getValue(),rotAroundCenter.getValue(),center);
			transform.setUpdateOriginFlag(updateOrigin.getValue());

			transform.setPadValue(padAmt.getInt());
			transform.setUseScannerAnatomical(useScanner.getValue());

			transform.run();
			ModelImage resultImage=transform.getTransformedImage();

			return resultImage;
		}


		private ArrayList<double[][]> readMultiMatrices(List<File> files){
			ArrayDoubleMtxReaderWriter rw = new ArrayDoubleMtxReaderWriter();
			ArrayList<double[][]> allxfms = new ArrayList<double[][]>(files.size());
			int i=0;
			while(i<files.size()){
				allxfms.add(rw.read(files.get(i)));
				i++;
			}
			return allxfms;
		}
	}
}
