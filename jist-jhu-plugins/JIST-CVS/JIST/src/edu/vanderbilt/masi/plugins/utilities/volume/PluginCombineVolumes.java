/*
 * 
 */
package edu.vanderbilt.masi.plugins.utilities.volume;

import java.util.List;

import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmInformation.AlgorithmAuthor;
import edu.jhu.ece.iacl.jist.pipeline.AlgorithmRuntimeException;
import edu.jhu.ece.iacl.jist.pipeline.CalculationMonitor;
import edu.jhu.ece.iacl.jist.pipeline.DevelopmentStatus;
import edu.jhu.ece.iacl.jist.pipeline.ProcessingAlgorithm;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamBoolean;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamCollection;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamOption;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolume;
import edu.jhu.ece.iacl.jist.pipeline.parameter.ParamVolumeCollection;
import edu.jhu.ece.iacl.jist.structures.image.ImageData;
import edu.jhu.ece.iacl.jist.structures.image.ImageDataMipav;
import edu.jhu.ece.iacl.structures.image.ImageDataMath;


/*
 * @author Bennett Landman (bennett.landman@vanderbilt.edu)
 * 
 */
public class PluginCombineVolumes extends ProcessingAlgorithm{
	static int NMax = 10;
	ParamVolumeCollection []inVols;
	ParamVolumeCollection outVol;
	static String []options = new String[]{"Single 4-D Volume","Collection of 3-D Volumes","Collection of Mixed 3-D/4-D Volumes"};

	ParamOption combineMethod;  
	private static final String cvsversion = "$Revision: 1.1 $";
	private static final String revnum = cvsversion.replace("Revision: ", "").replace("$", "").replace(" ", "");
	private static final String shortDescription = "Concatenates volumes in the input Volume Collection into a " +
	"single multi-component Volume (ie. 4D Volume).";
	private static final String longDescription = "";


	protected void createInputParameters(ParamCollection inputParams) {

		inputParams.add(combineMethod = new ParamOption("Output Method", options));
		inVols = new ParamVolumeCollection[NMax];
		for(int i=0;i<NMax;i++){			
			inVols[i]=new ParamVolumeCollection("Input Volume(s) "+(i+1));
			if(i>0)
				inVols[i].setMandatory(false);
			else
				inVols[i].setMandatory(true);
			inVols[i].setLoadAndSaveOnValidate(false);
			inputParams.add(inVols[i]);
			
		}

		inputParams.setPackage("MASI");
		inputParams.setCategory("Utilities.Volume");
		inputParams.setLabel("PowerTool: Concatenate");
		inputParams.setName("PowerTool_Concatenate");

		AlgorithmInformation info = getAlgorithmInformation();
		info.add(new AlgorithmAuthor("MASI","Bennett Landman",""));
		info.setWebsite("masi.vanderbilt.edu");
		info.setAffiliation("");
		info.setDescription(shortDescription + longDescription);
		info.setVersion(revnum);
		info.setEditable(false);
		info.setStatus(DevelopmentStatus.BETA);
	}


	protected void createOutputParameters(ParamCollection outputParams) {
		outputParams.add(outVol=new ParamVolumeCollection("Combined Output",null,-1,-1,-1,-1));
		outVol.setLoadAndSaveOnValidate(false);
	}


	/* (non-Javadoc)
	 * @see edu.jhu.ece.iacl.pipeline.ProcessingAlgorithm#execute(edu.jhu.ece.iacl.pipeline.CalculationMonitor)
	 */
	protected void execute(CalculationMonitor monitor) throws AlgorithmRuntimeException {
		switch(combineMethod.getIndex()) {
		case 0: // Single 4D
			executeCombineSingle4D(monitor);
			break;
		case 1: // Collection of 3D
			executeCombineCollection3D(monitor);
			break;
		case 2: // Mixed Collection of 3D/4D
			executeCombineCollection3D4D(monitor);
			break;		
		}
	}


	private void executeCombineCollection3D4D(CalculationMonitor monitor) {
		for(int i=0;i<NMax;i++) { 
			List<ParamVolume> vols=inVols[i].getParamVolumeList();
			if(vols==null)
				continue;
			for(ParamVolume vol : vols)
				outVol.add(vol);			
		}
	}


	private void executeCombineCollection3D(CalculationMonitor monitor) {
		for(int i=0;i<NMax;i++) { 
			List<ParamVolume> vols=inVols[i].getParamVolumeList();
			if(vols==null)
				continue;
			for(ParamVolume vol : vols) {
				ImageData img = vol.getImageData();
				if(img.getComponents()<=1) { 
					outVol.add(vol);
				System.out.println("add single");
				} else {
					for(int j=0;j<img.getComponents();j++) {
						outVol.add(ImageDataMath.extract3DVolume(img, j));
						System.out.println("add extract");
					}
					outVol.writeAndFreeNow(this);
				}					
				img=null;
//				vol.dispose();
			}
		}

	}


	private void executeCombineSingle4D(CalculationMonitor monitor) {
		int totalNumberOfVolumes = countVolumes();

		ImageData thisvol = inVols[0].getParamVolume(0).getImageData();

		ImageDataMipav cvol=new ImageDataMipav(thisvol.getName()+"_combined",thisvol.getType(),
				thisvol.getRows(),thisvol.getCols(),thisvol.getSlices(),totalNumberOfVolumes);
		cvol.setHeader(thisvol.getHeader());


		int jComp = 0;
		for(int m=0;m<NMax;m++) { 
			List<ParamVolume> vols=inVols[m].getParamVolumeList();
			if(vols==null)
				continue;
			for(ParamVolume vol : vols) {
				ImageData img = vol.getImageData();
				if(img.getComponents()<=1) { 
					for(int i=0;i<img.getRows();i++){
						for(int j=0;j<img.getCols();j++){
							for(int k=0;k<img.getSlices();k++){
								cvol.set(i,j,k,jComp,img.getFloat(i, j, k));
							}
						}
					}
					jComp++;
				}else {
					for(int l=0;l<img.getComponents();l++) {
						for(int i=0;i<img.getRows();i++){
							for(int j=0;j<img.getCols();j++){
								for(int k=0;k<img.getSlices();k++){
									cvol.set(i,j,k,jComp,img.getFloat(i, j, k,l));
								}
							}
						}
						jComp++; 
					}
				}					
				img=null;
				vol.dispose();
			}
		}
		outVol.add(cvol);
	}


	private int countVolumes() {
		int N3Dvols =0;
		for(int i=0;i<NMax;i++) { 
			List<ParamVolume> vols=inVols[i].getParamVolumeList();
			for(ParamVolume vol : vols) {
				ImageData img = vol.getImageData();
				if(img.getComponents()<=1) 
					N3Dvols++;
				else {
					N3Dvols+=img.getComponents();
				}					
				img=null;
				vol.dispose();
			}
		}
		return N3Dvols;
	}
}
