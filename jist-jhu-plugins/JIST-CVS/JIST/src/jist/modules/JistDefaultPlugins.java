package jist.modules;

public class JistDefaultPlugins implements JistModulesLocation {

	final static String[] default_paths = new String[]{
		"edu.jhu.bme.smile.demo",
		"edu.jhu.cs.cisst",
		"edu.jhu.ece.iacl.pami",
		"edu.jhu.ece.iacl.plugins",
		"edu.vanderbilt.masi.plugins"};
	
	@Override
	public String[] getValidModuleJavaPaths() {

		return default_paths;
	}
 
}
