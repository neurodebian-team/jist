#!/bin/bash
#emacs: -*- mode: shell-script; c-basic-offset: 4; tab-width: 4; indent-tabs-mode: t -*-
#ex: set sts=4 ts=4 sw=4 noet:
#
# Little script to purify upstream sources of JIST. Shipped within
# DFSG-ed upstream-sources for convenience (contained within
# upstream-dfsg git branch)
#
#-----------------\____________________________________/------------------

act=$1

case $act in
	'clean')
		find -iname CVS | xargs rm -rf
        ## Remove all .jars, excluding opencsv for now
		/bin/ls jist/JIST/lib/*jar | grep -v -e opencsv-1.8.jar -e jgraph-5.10.1.2.jar | xargs -r rm
		;;
	'merge')
		git merge -s recursive -X ours master
		$0 clean				# to assure
		;;
	*)
		echo >&2 "Unknown action to perform $act"
esac
